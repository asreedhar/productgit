package biz.firstlook.model.ie.ref;

public enum DisplayFormat {
	NONE,
	normal,
	bold,
	red,
	redBold;
}
