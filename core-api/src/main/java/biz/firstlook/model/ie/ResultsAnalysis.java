package biz.firstlook.model.ie;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import biz.firstlook.model.ie.ref.DisplayFormat;

@Entity
@Table(name="Results_Analysis")
public class ResultsAnalysis {

	@Id
	@Column(name="AnalysisID")
	private Integer id;
	
	@Column(name="BusinessUnitID")
	private Integer dealerId;
	
	@Column(name="DT")
	@Temporal(TemporalType.DATE)
	private Date date;
	
	private String category;
	private String text;
	
	@Column(name="DisplayFormatID")
	private DisplayFormat displayFormat;
	
	private Integer priority;
	
	@ManyToOne
	@JoinColumn(name="ParentAnalysisId")
	private ResultsAnalysis parent;
	
	private Integer ruleId;

	@Column(name="TimeStamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	public ResultsAnalysis() {}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getDealerId() {
		return dealerId;
	}

	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}

	public DisplayFormat getDisplayFormat() {
		return displayFormat;
	}

	public void setDisplayFormat(DisplayFormat displayFormat) {
		this.displayFormat = displayFormat;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ResultsAnalysis getParent() {
		return parent;
	}

	public void setParent(ResultsAnalysis parent) {
		this.parent = parent;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
