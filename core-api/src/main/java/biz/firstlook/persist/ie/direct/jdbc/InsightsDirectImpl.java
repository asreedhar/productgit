package biz.firstlook.persist.ie.direct.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;

import biz.firstlook.persist.ie.direct.InsightsDirect;

public class InsightsDirectImpl extends JdbcTemplate implements InsightsDirect {

	private String insightTestAllSQL;
	private String insightTestOneSQL;
	
	public boolean haveInsightsWithin(Integer days) {
		return queryForInt( insightTestAllSQL, new Object[] { days } ) > 0;
	}

	public boolean haveInsightsWithin(Integer days, Integer businessUnitId) {
		return queryForInt( insightTestOneSQL, new Object[] { days, businessUnitId } ) > 0;
	}

	public String getInsightTestAllSQL() {
		return insightTestAllSQL;
	}

	public void setInsightTestAllSQL(String insightTestAllSQL) {
		this.insightTestAllSQL = insightTestAllSQL;
	}

	public String getInsightTestOneSQL() {
		return insightTestOneSQL;
	}

	public void setInsightTestOneSQL(String insightTestOneSQL) {
		this.insightTestOneSQL = insightTestOneSQL;
	}
	
	
}
