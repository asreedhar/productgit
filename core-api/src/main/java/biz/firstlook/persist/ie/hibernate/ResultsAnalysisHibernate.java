package biz.firstlook.persist.ie.hibernate;

import biz.firstlook.model.ie.ResultsAnalysis;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.ie.ResultsAnalysisDao;

public class ResultsAnalysisHibernate extends GenericHibernate<ResultsAnalysis,Integer> implements
		ResultsAnalysisDao {

	public ResultsAnalysisHibernate() {
		super();
	}

}
