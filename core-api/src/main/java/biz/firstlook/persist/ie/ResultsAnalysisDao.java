package biz.firstlook.persist.ie;

import biz.firstlook.model.ie.ResultsAnalysis;
import biz.firstlook.persist.common.GenericDAO;

public interface ResultsAnalysisDao extends GenericDAO<ResultsAnalysis, Integer> {
}
