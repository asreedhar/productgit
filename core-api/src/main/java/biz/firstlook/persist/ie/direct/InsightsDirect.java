package biz.firstlook.persist.ie.direct;

public interface InsightsDirect {
	public boolean haveInsightsWithin( Integer days );
	public boolean haveInsightsWithin( Integer days, Integer businessUnitId );
}
