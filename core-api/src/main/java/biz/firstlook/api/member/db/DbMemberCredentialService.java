package biz.firstlook.api.member.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.lang.StringUtils;

import biz.firstlook.api.member.MemberCredentialService;
import biz.firstlook.model.imt.CredentialType;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.MemberCredential;
import biz.firstlook.persist.imt.MemberDao;
import biz.firstlook.persist.imt.MemberCredentialDao;

public class DbMemberCredentialService implements MemberCredentialService
{

    private MemberDao memberDao;
	private MemberCredentialDao  memberCredentialDao;
	
	public DynaBean getMemberCredentials(Integer memberId) {
		DynaBean result = createDynaBean();
		Member member = memberDao.findById(memberId);
		populateDynaBean(member, result, false);
		return result;
	}

	public DynaBean getMemberCredentialsForEdit(Integer memberId) {
		DynaBean result = createDynaBean();
		Member member = memberDao.findById(memberId);
		populateDynaBean(member, result, true);
		return result;
	}
	
	public void saveMemberCredentials( DynaBean dynaBean) {
		Integer memberId = (Integer)dynaBean.get( "id" );
		
		Member member = memberDao.findById(memberId);
		
		Set<MemberCredential> credentials = member.getMemberCredentials();
		
		persist(credentials, (String)dynaBean.get( "gmacUsername" ), CredentialType.GMAC, (String)dynaBean.get( "gmacPassword" ), member);
	}	

	private void persist(Set<MemberCredential> credentials, String username, CredentialType credentialType, String pass, Member member) {
		MemberCredential credential = null;
		for (MemberCredential tempCredential : credentials) {
			if (tempCredential.getCredentialTypeId().intValue() == credentialType.getId()) {
				credential = tempCredential;
				credential.setUsername(username.trim());
				credential.setPassword(pass.trim());
				break;
			}
		}		
		if ( credential == null )
		{
			credential = new MemberCredential();
			credential.setMemberId(member.getId());
			credential.setCredentialTypeId(credentialType.getId());
			credential.setUsername(username.trim());
			credential.setPassword(pass.trim());
		}
		
		// if credential existed but user deleted username - delete it
		if ( StringUtils.isBlank( credential.getUsername() ) && credential.getId() != null )
		{
			memberCredentialDao.makeTransient( credential );
		}
		else if ( StringUtils.isNotBlank( credential.getUsername() ))
		{
			memberCredentialDao.makePersistent(credential);
		}
	}

	private DynaBean createDynaBean() {
		DynaClass clazz = createDynaClass();
		DynaBean result;
		try
		{
			result = clazz.newInstance();
		}
		catch ( Exception e )
		{
			throw new RuntimeException( "Cannot create DynaBean instance" );
		}
		return result;
	}
	
	private void populateDynaBean(Member member, DynaBean result, boolean isEdit) {
		result.set( "id", member.getId() );
		
		Set<MemberCredential> credentials = member.getMemberCredentials();
		for (MemberCredential credential : credentials) {
			if (credential.getCredentialTypeId().intValue() == CredentialType.GMAC.getId().intValue()) {
				result.set( "gmacUsername", credential.getUsername());
				result.set( "gmacPassword", credential.getPassword());
			}
		}
		result.set( "isEdit", isEdit);
	}
	
	private DynaClass createDynaClass()
	{
		List< DynaProperty > dynaProperties = new ArrayList< DynaProperty >();
		dynaProperties.add( new DynaProperty( "id", Integer.class ) );
		dynaProperties.add( new DynaProperty( "isEdit", Boolean.class ) );
		dynaProperties.add( new DynaProperty( "gmacUsername", String.class ) );
		dynaProperties.add( new DynaProperty( "gmacPassword", String.class ) );
		
		DynaClass dynaClass = new BasicDynaClass( "MemberCredentials", BasicDynaBean.class, dynaProperties.toArray( new DynaProperty[0] ) );
		return dynaClass;
	}


	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}

	public void setMemberCredentialDao(MemberCredentialDao memberCredentialDao) {
		this.memberCredentialDao = memberCredentialDao;
	}
}
