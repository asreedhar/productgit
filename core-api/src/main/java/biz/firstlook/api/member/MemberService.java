package biz.firstlook.api.member;

import javax.jws.WebService;

import biz.firstlook.model.imt.Member;

@WebService
public interface MemberService {

	public abstract Member retrieve( Integer memberId );
	
}