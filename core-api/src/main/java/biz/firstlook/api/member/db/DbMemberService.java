	package biz.firstlook.api.member.db;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import biz.firstlook.api.member.MemberService;
import biz.firstlook.model.imt.Member;
import biz.firstlook.persist.imt.MemberDao;

@WebService( serviceName = "MemberService", endpointInterface = "biz.firstlook.api.member.MemberService" ) 
public class DbMemberService implements MemberService {

	private Logger log = Logger.getLogger( DbMemberService.class );
	
	private MemberDao memberDao;

	public Member retrieve(Integer memberId) {
		log.debug( "Retreiving Member: " + memberId );
		Member member = memberDao.findById( memberId );
		member.setDealers( null );
		return member;
	}

	public MemberDao getMemberDao() {
		return memberDao;
	}

	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}
	
	
}
