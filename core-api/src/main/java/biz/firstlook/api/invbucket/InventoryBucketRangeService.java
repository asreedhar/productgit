package biz.firstlook.api.invbucket;

import org.apache.commons.beanutils.DynaBean;

import biz.firstlook.model.imt.ref.BucketRangeType;

public interface InventoryBucketRangeService {

	public DynaBean getInventoryBucketRanges(Integer businessUnitId);
	public DynaBean getPingIIBucketRanges(Integer businessUnitId);
	public DynaBean getPingIIMileageAdjustmentBucketRanges(Integer businessUnitId);
	public void deleteAllBucketsForDealer(Integer businessUnitId, BucketRangeType bucketType);

	public void saveBucketRanges( DynaBean dynaBean );
	public void savePingIIBucketRanges( DynaBean dynaBean );
	public void savePingIIMileageaAjdustmentBucketRanges( DynaBean dynaBean );
}
