package biz.firstlook.api.invbucket.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.PropertyUtils;

import biz.firstlook.api.invbucket.InventoryBucketRangeService;
import biz.firstlook.model.imt.InventoryBucketRange;
import biz.firstlook.model.imt.ref.BucketRangeType;
import biz.firstlook.persist.imt.InventoryBucketRangeDao;

public class InventoryBucketRangeServiceImpl implements InventoryBucketRangeService
{
public static final int MAX_BUCKET_COUNT = 8;
public static final int RED_YELLOW_LIST_LIGHTS = 3; // watchlist is all the red/yellow lights in the first time bucket
public static final int GREEN_BUCKET_LIGHTS = 4; // the first time bucket is the same range as the watch list but with green lights
public static final int ALL_LIGHTS_BUCKET_LIGHTS = 7;
public static final int INVENTORY_BUCKET_RANGE_ID = 4;
private InventoryBucketRangeDao inventoryBucketRangeDao;

// currently this description is the only way to identify the watch list bucket. All watch lists need this description
private static final String WATCH_LIST_DESCRIPTION = "WATCH LIST";

public InventoryBucketRangeServiceImpl()
{
	super();
}


public DynaBean getPingIIBucketRanges(Integer businessUnitId) {
	return getBucketRanges( businessUnitId, BucketRangeType.PINGII_PRICING_AGE);
}

public DynaBean getPingIIMileageAdjustmentBucketRanges(Integer businessUnitId) {
	return getBucketRanges( businessUnitId, BucketRangeType.PINGII_MILEAGE_ADJUSTMENT);
}

public DynaBean getInventoryBucketRanges( Integer businessUnitId ) {
	return getBucketRanges( businessUnitId, BucketRangeType.NEW_AGING_INVENTORY_PLAN);
}

private DynaBean getBucketRanges(Integer businessUnitId, BucketRangeType bucketType)
{
	DynaClass clazz = createDynaClass();
	DynaBean result;
	try
	{
		result = clazz.newInstance();
	}
	catch ( Exception e )
	{
		throw new RuntimeException( "Cannot create DynaBean instance" );
	}

	List< InventoryBucketRange > bucketRanges = getInventoryBucketRangeDao().findByBusinessUnitIdAndBucketRangeType( businessUnitId, bucketType );
	populateDynaBean( bucketRanges, businessUnitId, result );
	return result;
}

/**
 * This is kinda weird.
 * BucketRanges are dynamic, both in value and the number that exist. A user can create up to MAX_BUCKET_COUNT
 * BucketRanges by entering a non-zero value for its high/low on the jsp. 
 * 		1) We only want to persist those these non-zero ranges and not just everything. 
 * 		2) We also want to delete those which the user sets to zero values.  (or else you get 0 ranged buckets on the AIP)
 * 		
 * The watchList is a special *optional* bucket that only has a highRange and turned on by a checkbox on the jsp. 
 * 		1) We want to update the values for the watch list if it is selected and a range for it exists.
 * 		2) If it is selected but none exists, we want to create one.
 * 
 * @param dynaBean
 * 
 */
@SuppressWarnings("unchecked")
public void saveBucketRanges( DynaBean dynaBean ) {
	saveBucketRangesByType(dynaBean, BucketRangeType.NEW_AGING_INVENTORY_PLAN);
}

@SuppressWarnings("unchecked")
public void savePingIIBucketRanges( DynaBean dynaBean ) {
	saveBucketRangesByType(dynaBean, BucketRangeType.PINGII_PRICING_AGE);
}

@SuppressWarnings("unchecked")
public void savePingIIMileageaAjdustmentBucketRanges( DynaBean dynaBean ) {
	saveBucketRangesByType(dynaBean, BucketRangeType.PINGII_MILEAGE_ADJUSTMENT);
}

// don't need to worry about watch list for PingII buckets as they'll just be ignored
private void saveBucketRangesByType( DynaBean dynaBean, BucketRangeType bucketRangetype)
{ 
	Integer businessUnitId = (Integer)dynaBean.get( "id" );
	Boolean useWatchList = (Boolean)dynaBean.get( "useWatchList" );
	Map<String, Object> properties = getBeanProperties(dynaBean);
	
	properties = stripPrefix(properties, bucketRangetype); /// FUCK!!! FUCK!!! FUCK!!! hard dependencies on form value names, PM

	// get those rangeIds that have been assigned valid values from the jsp
	ArrayList<Integer> validRangeIdsFromPage = getValidBucketRangeIds(properties);
	// get all rangeBuckets currently in the DB
	List< InventoryBucketRange > bucketRangesInDB = getInventoryBucketRangeDao().findByBusinessUnitIdAndBucketRangeType( businessUnitId, bucketRangetype );
	
	// determine if watchList exists in the DB
	boolean watchListExists = watchListExists(bucketRangesInDB);
	
	// keep a reference to those ranges we will update 
	ArrayList<InventoryBucketRange> rangesToSave = new ArrayList<InventoryBucketRange>();
	
	// perform basic update 
	updateBuckets(properties, validRangeIdsFromPage, bucketRangesInDB, rangesToSave);
	
	// go through all valid ranges from page and create the ones that don't exists in the DB
	createNewBuckets(businessUnitId, properties, validRangeIdsFromPage, rangesToSave);
	
	// create or remove watchlist and update sortOrders, lights accordingly
	updateWatchList(businessUnitId, useWatchList, properties, watchListExists, rangesToSave);
	
	// go through and delete all ranges currently in the DB and not valid in the jsp
	deleteUnusedBuckets(bucketRangesInDB, rangesToSave);
	
	// finally, save all left
	for (int j = 0, s = rangesToSave.size(); j < s; j++) {
		getInventoryBucketRangeDao().makePersistent( (InventoryBucketRange)rangesToSave.get(j) ); 
	}

}

public void deleteAllBucketsForDealer(Integer businessUnitId, BucketRangeType bucketType) {
	List< InventoryBucketRange > bucketRanges = getInventoryBucketRangeDao().findByBusinessUnitIdAndBucketRangeType( businessUnitId, bucketType );
	if (bucketRanges != null && !bucketRanges.isEmpty()) {
		for (InventoryBucketRange toDel : bucketRanges) {
			if (toDel.getId() != null) { 
				// This is the result of us building the entity from a function call
				// Crappy check to see if InventoryBucketRanges for this dealer really exist or if we've been returned the default buckets
				getInventoryBucketRangeDao().makeTransient(toDel);
			}
		}
	}
}

private void deleteUnusedBuckets(List<InventoryBucketRange> bucketRangesInDB, ArrayList<InventoryBucketRange> rangesToSave) {
	for (int i = 0; i < bucketRangesInDB.size(); i++) {
		boolean rangeValid = false;
		InventoryBucketRange bucketRangeFromDB = bucketRangesInDB.get(i);
		for (int j = 0; j < rangesToSave.size(); j++) {
			InventoryBucketRange validRange = rangesToSave.get(j);
			if (validRange.getRangeID().intValue() == bucketRangeFromDB.getRangeID().intValue()) {
				rangeValid = true;
				break;
			}
		}
		if (!rangeValid) {
			// delete range
			if (bucketRangeFromDB.getId() != null) { 
				// only try to delete those in the db. only those previously saved will be. because we use a function to get default buckets.
				getInventoryBucketRangeDao().makeTransient( bucketRangeFromDB );
			}
		}
	}
}

@SuppressWarnings("unchecked")
private void updateWatchList(Integer businessUnitId, Boolean useWatchList, Map properties, boolean watchListExists, ArrayList rangesToSave) {
	if (useWatchList) {
		// if watchList already exists in the db it will have been updated with the above
		if (!watchListExists) {
			InventoryBucketRange watchList = createWatchList(properties, businessUnitId);
			// watchList didn't exist before so all rangeIds and sortOrders need incrementing
			for (int i = 0; i < rangesToSave.size(); i++) {
				InventoryBucketRange rangeToIncement = (InventoryBucketRange)rangesToSave.get(i);
				rangeToIncement.setSortOrder(rangeToIncement.getSortOrder() + 1);
				if (rangeToIncement.getRangeID().intValue() == 2) {
					rangeToIncement.setLights(GREEN_BUCKET_LIGHTS); // watch list, 0-X bucket needs to do only green lights
				}
			}
			rangesToSave.add(watchList);
		}
	} else {
		if (watchListExists) {
			InventoryBucketRange watchList = null;
			// decrement all existing ranges and sortOrders
			for (int i = 0; i < rangesToSave.size(); i++) {
				InventoryBucketRange rangeToIncement = (InventoryBucketRange)rangesToSave.get(i);
				if (isWatchListBucketRange(rangeToIncement)) {
					watchList = rangeToIncement;
					continue;
				}
				if (rangeToIncement.getRangeID().intValue() == 2) {
					rangeToIncement.setLights(ALL_LIGHTS_BUCKET_LIGHTS); // no watch list, 0-X bucket needs to do all lights
				}
			}
			// don't need to delete watchList here, it will get delted in deleteUnusedBuckets method
			rangesToSave.remove(watchList);
		}
	}
}

@SuppressWarnings("unchecked")
private void createNewBuckets(Integer businessUnitId, Map properties, ArrayList validRangeIdsFromPage, ArrayList rangesToSave) {
	for (int i = 0; i < validRangeIdsFromPage.size(); i++) {
		Integer validRangeIdFromPage = (Integer)validRangeIdsFromPage.get(i);
		// the range from the jsp is valid but does not exist in the DB. Create new range with default values
		// except for WATCH LIST - that's created below
		if (validRangeIdFromPage.intValue() != 1 && !rangeExists(rangesToSave, validRangeIdFromPage)) {
			rangesToSave.add(createNewInventoryBucketRange(validRangeIdFromPage, properties, businessUnitId ));
		}
	}
}

private void updateBuckets(Map<String, Object> properties, ArrayList<Integer> validRangeIdsFromPage, List<InventoryBucketRange> bucketRangesInDB, ArrayList<InventoryBucketRange> rangesToSave) {
	for (int i = 0, s = validRangeIdsFromPage.size(); i < s; i++) {
		Integer validRangeIdFromPage = (Integer)validRangeIdsFromPage.get(i);
		for (int j = 0; j < bucketRangesInDB.size(); j++) {
			InventoryBucketRange bucketRangeFromDB = (InventoryBucketRange)bucketRangesInDB.get(j);
			if (bucketRangeFromDB.getRangeID().intValue() ==  validRangeIdFromPage.intValue()) {
				// if the bucket is valid from the jsp and already in the DB
				// Update its range values and keep a reference
				updateBucketRangeData( bucketRangeFromDB, properties);
				rangesToSave.add(bucketRangeFromDB);
				break;
			}
		}
	}
}

private boolean rangeExists(ArrayList<InventoryBucketRange> rangesToSave, Integer validRangeIdFromPage) {
	boolean rangeIdExistsInDB = false;
	for (int j = 0; j < rangesToSave.size(); j++) {
		int updatedRangeId = (rangesToSave.get(j)).getRangeID().intValue();
		if (validRangeIdFromPage.intValue() == updatedRangeId) {
			rangeIdExistsInDB = true;
			break;
		}
	}
	return rangeIdExistsInDB;
}


private Map<String, Object> getBeanProperties(DynaBean dynaBean) {
	Map<String, Object> properties = null;
	// Get all of the bean properties
	try
	{
		properties = PropertyUtils.describe( dynaBean );
	}
	catch ( Exception e )
	{
		throw new RuntimeException( "Error retrieving properties from bean", e );
	}
	return properties;
}

private Map<String, Object> stripPrefix(Map<String, Object> map, BucketRangeType type) {
	String prefix = (type == BucketRangeType.PINGII_PRICING_AGE) ? "age_" : "mileage_";
	HashMap<String,Object> m = new HashMap<String,Object>();
	for (Map.Entry<String, Object> entry : map.entrySet()) {
		String key = entry.getKey();
		if (key.startsWith(prefix)) {
			key = key.substring(prefix.length());
		}
		m.put(key, entry.getValue());
	}
	return m;
}


/**
 * Creates and returns a new InventoryBucketRange with default values.
 * If special settings for lights and RangeId are needed this must 
 * be applied independently to this method's results
 *  
 * @param rangeId
 * @param properties
 * @param businessUnitId
 * @param sortOrder 
 * @return
 */
private InventoryBucketRange createNewInventoryBucketRange(Integer rangeId, Map<String, Object> properties, Integer businessUnitId ) {
	InventoryBucketRange newBucketRange = new InventoryBucketRange();
	
	newBucketRange.setRangeID(rangeId);
	newBucketRange.setLow((Integer)properties.get( "range" + rangeId + "Low" ));
	newBucketRange.setHigh((Integer)properties.get( "range" + rangeId + "High" ));
	newBucketRange.setBusinessUnitID(businessUnitId);
	newBucketRange.setInventoryBucketID(INVENTORY_BUCKET_RANGE_ID);
	newBucketRange.setDescription(properties.get( "range" + rangeId + "Low" )
			+ "-" + properties.get( "range" + rangeId + "High" ) + " Days" );
	// default to time bucket lights. If watch list exists this logic is applied later
	newBucketRange.setLights(ALL_LIGHTS_BUCKET_LIGHTS); 
	newBucketRange.setSortOrder( 8 - rangeId );
	newBucketRange.setValue1((Integer)properties.get( "range" + rangeId + "Value1"));
	newBucketRange.setValue2((Integer)properties.get( "range" + rangeId + "Value2"));
	newBucketRange.setValue3((Integer)properties.get( "range" + rangeId + "Value3"));
	return newBucketRange;
	
}

/**
 * Any bucket with rangeId between 1 and MAX_BUCKET_COUNT (6)
 * in the jsp with range > 0 represents a range that needs to be
 * saved/updated.  
 * 
 * @param properties
 * @return
 */
private ArrayList<Integer> getValidBucketRangeIds(Map<String, Object> properties) {
	ArrayList<Integer> validRangeIds = new ArrayList<Integer>();
	for (int i = 1, s = properties.size() + 1; i <= s; i++) {
		Integer range = (Integer)properties.get( "range" + i );
		Integer rangeLow = (Integer)properties.get( "range" + i + "Low");
		if ( (range != null && range.intValue() != 0) || (rangeLow != null && rangeLow.intValue() != 0) ){
			// if its range is a non-zero value, or its lowRange is non zero
			validRangeIds.add(new Integer(i));
		} 
	}
	return validRangeIds;
}
		

/**
 * Creates a watchList InventoryBuckletRange using the high, low and range values 
 * from the property map and saves it to the DB. Regardless of whether watchList is 
 * selected or not, the admin jsp always communicates the watchList bucket values, 
 * which are identical to the 'most recent (e.g. 0-29)' range, as rangeId 1.
 * 
 * @param properties
 * @param businessUnitId
 * @return
 */
private InventoryBucketRange createWatchList(Map<String, Object> properties, Integer businessUnitId) {
	InventoryBucketRange watchList = new InventoryBucketRange();
	int rangeId  = 1; // watchList, if it exists, always has rangeId 1, sortOrder 1
	watchList.setRangeID(rangeId);
	watchList.setLow((Integer)properties.get( "range" + rangeId + "Low" ));
	watchList.setHigh((Integer)properties.get( "range" + rangeId + "High" ));
	watchList.setBusinessUnitID(businessUnitId);
	watchList.setInventoryBucketID(INVENTORY_BUCKET_RANGE_ID);
	watchList.setDescription(WATCH_LIST_DESCRIPTION);
	watchList.setLights(RED_YELLOW_LIST_LIGHTS); // watch list always contains red & yellow lights
	watchList.setSortOrder(new Integer(1)); // watch list is always the first in the sort Order
	return watchList;
}

/**
 * Returns true if the bucketRange list contains a watchList and false otherwise.
 * 
 * Currently the only way to identify a bucketRange as a watchList is to check its 
 * description. If descriptions need to become dynamic for the buckets we should create 
 * and 'isWatchList' column in the DB.
 * @param bucketRanges
 */
private boolean watchListExists(List<InventoryBucketRange> bucketRanges) {
	boolean hasExisitingWatchList = false;
	for (InventoryBucketRange rangeFromDB : bucketRanges) {
		if (isWatchListBucketRange(rangeFromDB)) {
			hasExisitingWatchList = true;
			break;
		}
	}
	return hasExisitingWatchList;
}

private boolean isWatchListBucketRange(InventoryBucketRange rangeFromDB) {
	return rangeFromDB.getLights().intValue() == RED_YELLOW_LIST_LIGHTS;
}

/**
 * Updates bucketRange low, high and range values based on the set values in the properties.
 * and stores the new data to the DB. 
 * 
 * IF a new watchList bucket was created then we need to increment sort orders
 * @param bucketRange
 * @param properties
 * @param newWatchListCreated 
 */
public void updateBucketRangeData(InventoryBucketRange bucketRange, Map<String, Object> properties)
{
	int rangeId  = bucketRange.getRangeID();
	bucketRange.setLow((Integer)properties.get( "range" + rangeId + "Low" ));
	Integer rangeHigh = (Integer)properties.get( "range" + rangeId + "High" );
	if (rangeHigh.intValue() == 0) {
		bucketRange.setHigh(null);
	} else {
		bucketRange.setHigh(rangeHigh);
	}
	if (isWatchListBucketRange(bucketRange)) {
		bucketRange.setDescription(WATCH_LIST_DESCRIPTION);
		bucketRange.setSortOrder( 1 );
	} else {
		if (rangeHigh.intValue() == 0) {
			bucketRange.setDescription(properties.get( "range" + rangeId + "Low" )
					+ "+ Days" );
		} else {
			bucketRange.setDescription(properties.get( "range" + rangeId + "Low" )
					+ "-" + properties.get( "range" + rangeId + "High" ) + " Days" );
		}
		bucketRange.setSortOrder( 8 - rangeId );
	}
	bucketRange.setValue1((Integer)properties.get("range" + rangeId + "Value1"));
	bucketRange.setValue2((Integer)properties.get("range" + rangeId + "Value2"));
	bucketRange.setValue3((Integer)properties.get("range" + rangeId + "Value3"));
}

private DynaClass createDynaClass()
{
	List< DynaProperty > dynaProperties = new ArrayList< DynaProperty >();

	int i = 1;

	while ( i <= MAX_BUCKET_COUNT ) 
	{
		dynaProperties.add( new DynaProperty( "range" + i, Integer.class ) );
		dynaProperties.add( new DynaProperty( "range" + i + "High", Integer.class ) );
		dynaProperties.add( new DynaProperty( "range" + i + "Low", Integer.class ) );
		dynaProperties.add( new DynaProperty( "range" + i + "Value1", Integer.class ) );
		dynaProperties.add( new DynaProperty( "range" + i + "Value2", Integer.class ) );
		dynaProperties.add( new DynaProperty( "range" + i + "Value3", Integer.class ) );
		
		i++;
	}

	dynaProperties.add( new DynaProperty( "useWatchList", Boolean.class ) );
	dynaProperties.add( new DynaProperty( "id", Integer.class ) );
	DynaClass dynaClass = new BasicDynaClass( "InventoryBucketRanges", BasicDynaBean.class, dynaProperties.toArray( new DynaProperty[0] ) );
	return dynaClass;
}

public void populateDynaBean( List< InventoryBucketRange > bucketRanges, Integer businessUnitId, DynaBean bucketRangesBean )
{
	bucketRangesBean.set( "useWatchList", false );
	bucketRangesBean.set( "id", businessUnitId );
	int timeBasedBucketCount = 1; 
	if (!watchListExists(bucketRanges)) {
		// watch list implicitly holds rangeId 1 spot 
		timeBasedBucketCount++; 
	}

	for ( InventoryBucketRange bucketRange : bucketRanges )
	{
		if ( isWatchListBucketRange( bucketRange ) )
		{
			bucketRangesBean.set( "useWatchList", true );
		}
		bucketRangesBean.set( "range" + bucketRange.getRangeID(), calculateRange( bucketRange ) );
		bucketRangesBean.set( "range" + bucketRange.getRangeID() + "Low", bucketRange.getLow() );
		
		if ( bucketRange.getHigh() == null )
		{
			// we hit the last bucket, except for the watchlist
			bucketRangesBean.set( "range" + bucketRange.getRangeID() + "High", 0 );
		}
		else
		{
			bucketRangesBean.set( "range" + bucketRange.getRangeID() + "High", bucketRange.getHigh() );
		}
		
		bucketRangesBean.set("range"+bucketRange.getRangeID()+"Value1", bucketRange.getValue1());
		bucketRangesBean.set("range"+bucketRange.getRangeID()+"Value2", bucketRange.getValue2());
		bucketRangesBean.set("range"+bucketRange.getRangeID()+"Value3", bucketRange.getValue3());
		
		timeBasedBucketCount++;
	}

    // set unused buckets' values to zero
	while ( timeBasedBucketCount <= MAX_BUCKET_COUNT )
	{
		bucketRangesBean.set( "range" + timeBasedBucketCount, 0 );
		bucketRangesBean.set( "range" + timeBasedBucketCount + "High", 0 );
		bucketRangesBean.set( "range" + timeBasedBucketCount + "Low", 0 );
		timeBasedBucketCount++;
	}
}

public int calculateRange( InventoryBucketRange bucketRange )
{
	if ( bucketRange.getHigh() != null )
	{
		return ( bucketRange.getHigh() - bucketRange.getLow() ) + 1;
	}

	return 0;
}

public InventoryBucketRangeDao getInventoryBucketRangeDao()
{
	return inventoryBucketRangeDao;
}

public void setInventoryBucketRangeDao( InventoryBucketRangeDao inventoryBucketRangeDao )
{
	this.inventoryBucketRangeDao = inventoryBucketRangeDao;
}

} 
