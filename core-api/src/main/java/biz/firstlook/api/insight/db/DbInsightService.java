	package biz.firstlook.api.insight.db;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import biz.firstlook.api.insight.InsightService;
import biz.firstlook.persist.ie.direct.InsightsDirect;

@WebService( serviceName = "InsightService", endpointInterface = "biz.firstlook.api.insight.InsightService" ) 
public class DbInsightService implements InsightService {

	private Logger logger = Logger.getLogger( DbInsightService.class );
	
	private Integer recentPeriod;	
	private InsightsDirect insightDirect;
	
	public DbInsightService() {
	}

	public boolean hasRecentInsights() {
		boolean result = insightDirect.haveInsightsWithin( recentPeriod );
		logger.info( "Does the system have recent insights? " + result );
		return result;
	}

	public boolean hasRecentInsights(Integer businessUnitId) {
		boolean result = insightDirect.haveInsightsWithin( recentPeriod, businessUnitId );
		logger.info( "Does the dealer(" + businessUnitId + ") have recent insights?" + result );
		return result;
	}

	public InsightsDirect getInsightDirect() {
		return insightDirect;
	}

	public void setInsightDirect(InsightsDirect insightDirect) {
		this.insightDirect = insightDirect;
	}

	public Integer getRecentPeriod() {
		return recentPeriod;
	}

	public void setRecentPeriod(Integer recentPeriod) {
		this.recentPeriod = recentPeriod;
	}
}
