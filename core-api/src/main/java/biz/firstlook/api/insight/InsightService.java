package biz.firstlook.api.insight;

import javax.jws.WebService;

@WebService
public interface InsightService {

	public abstract boolean hasRecentInsights();
	public abstract boolean hasRecentInsights(Integer businessUnitId);
	

}