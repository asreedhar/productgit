package biz.firstlook.api.matrix;

import java.util.ArrayList;
import java.util.List;

public class LightDescriptor {

	public final static int POWERZONE = 1;

	public final static int WINNERS = 2;

	public final static int GOOD_BETS = 3;

	public final static int MARKET_PERFORMER_OR_OTHER_GREEN = 4;

	public final static int MARKET_PERFORMER_OR_MANAGERS_CHOICE = 5;

	public final static int MANAGERS_CHOICE = 6;

	public static int RED_LIGHT = 1;

	public static int YELLOW_LIGHT = 2;

	public static int GREEN_LIGHT = 3;

	public static LightDescriptor RED_LIGHT_MC = new LightDescriptor(1,
			RED_LIGHT, MANAGERS_CHOICE, "R-MC");

	public static LightDescriptor YELLOW_LIGHT_MC_MP = new LightDescriptor(2,
			YELLOW_LIGHT, MARKET_PERFORMER_OR_MANAGERS_CHOICE,
			"Y-MC");

	public static LightDescriptor YELLOW_LIGHT_POWERZONE = new LightDescriptor(
			3, YELLOW_LIGHT, POWERZONE, "Y-PZ");

	public static LightDescriptor GREEN_LIGHT_MP_OG = new LightDescriptor(4,
			GREEN_LIGHT, MARKET_PERFORMER_OR_OTHER_GREEN,
			"G-MC-OTHER");

	public static LightDescriptor GREEN_LIGHT_GOOD_BETS = new LightDescriptor(
			5, GREEN_LIGHT, GOOD_BETS, "G-GB");

	public static LightDescriptor GREEN_LIGHT_WINNER = new LightDescriptor(6,
			GREEN_LIGHT, WINNERS, "G-W");

	public static LightDescriptor GREEN_LIGHT_POWERZONE = new LightDescriptor(
			7, GREEN_LIGHT, POWERZONE, "G-PZ");

	private int id;

	private int ciaTypeValue;

	private int lightValue;

	private String description;

	public LightDescriptor() {
	}

	public LightDescriptor(int id, String description) {
		setId(id);
		setDescription(description);
	}

	public LightDescriptor(int id, int lightValue, int ciaType,
			String description) {
		setId(id);
		setLightValue(lightValue);
		setCiaTypeValue(ciaType);
		setDescription(description);
	}

	public String getDescription() {
		return description;
	}

	public int getLightValue() {
		return lightValue;
	}

	public void setDescription(String string) {
		description = string;
	}

	public int getCiaTypeValue() {
		return ciaTypeValue;
	}

	public int getId() {
		return id;
	}

	public void setId(int i) {
		id = i;
	}

	public void setCiaTypeValue(int i) {
		ciaTypeValue = i;
	}

	public void setLightValue(int i) {
		lightValue = i;
	}

	public static List<LightDescriptor> createLightDescriptors()
	{
		List<LightDescriptor> values = 
			new ArrayList<LightDescriptor>();
		values.add( LightDescriptor.RED_LIGHT_MC );
		values.add( LightDescriptor.YELLOW_LIGHT_MC_MP );
		values.add( LightDescriptor.YELLOW_LIGHT_POWERZONE );
		values.add( LightDescriptor.GREEN_LIGHT_MP_OG );
		values.add( LightDescriptor.GREEN_LIGHT_GOOD_BETS );
		values.add( LightDescriptor.GREEN_LIGHT_WINNER );
		values.add( LightDescriptor.GREEN_LIGHT_POWERZONE );
		return values;
	}

	public static LightDescriptor determineId( int lightValue, int ciaTypeValue )
	{
	    if ( lightValue == LightDescriptor.RED_LIGHT )
	    {
	        return LightDescriptor.RED_LIGHT_MC;
	    } else if ( lightValue == LightDescriptor.YELLOW_LIGHT )
	    {
	        if ( ciaTypeValue == LightDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE )
	        {
	            return LightDescriptor.YELLOW_LIGHT_MC_MP;
	        } else if ( ciaTypeValue == LightDescriptor.POWERZONE )
	        {
	            return LightDescriptor.YELLOW_LIGHT_POWERZONE;
	        }
	    } else if ( lightValue == LightDescriptor.GREEN_LIGHT )
	    {
	        if ( ciaTypeValue == LightDescriptor.MARKET_PERFORMER_OR_OTHER_GREEN )
	        {
	            return LightDescriptor.GREEN_LIGHT_MP_OG;
	        } else if ( ciaTypeValue == LightDescriptor.GOOD_BETS )
	        {
	            return LightDescriptor.GREEN_LIGHT_GOOD_BETS;
	        } else if ( ciaTypeValue == LightDescriptor.WINNERS )
	        {
	            return LightDescriptor.GREEN_LIGHT_WINNER;
	        } else if ( ciaTypeValue == LightDescriptor.POWERZONE )
	        {
	            return LightDescriptor.GREEN_LIGHT_POWERZONE;
	        }
	    }
	    return LightDescriptor.RED_LIGHT_MC;

	}

	public static LightDescriptor retrieveById( int id )
	{
	    LightDescriptor descriptor = LightDescriptor.RED_LIGHT_MC;
	    switch (id)
	    {
	        case 1:
	            descriptor = LightDescriptor.RED_LIGHT_MC;
	            break;
	        case 2:
	            descriptor = LightDescriptor.YELLOW_LIGHT_MC_MP;
	            break;
	        case 3:
	            descriptor = LightDescriptor.YELLOW_LIGHT_POWERZONE;
	            break;
	        case 4:
	            descriptor = LightDescriptor.GREEN_LIGHT_MP_OG;
	            break;
	        case 5:
	            descriptor = LightDescriptor.GREEN_LIGHT_GOOD_BETS;
	            break;
	        case 6:
	            descriptor = LightDescriptor.GREEN_LIGHT_WINNER;
	            break;
	        case 7:
	            descriptor = LightDescriptor.GREEN_LIGHT_POWERZONE;
	            break;
	        default:
	    }
	    return descriptor;
	}

}
