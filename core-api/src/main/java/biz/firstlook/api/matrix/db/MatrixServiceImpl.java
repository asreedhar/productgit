package biz.firstlook.api.matrix.db;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import biz.firstlook.api.matrix.LightDescriptor;
import biz.firstlook.api.matrix.LightMatrix;
import biz.firstlook.api.matrix.MatrixService;
import biz.firstlook.model.imt.DealerGridThresholds;
import biz.firstlook.model.imt.DealerGridValues;
import biz.firstlook.persist.imt.DealerGridThresholdsDao;
import biz.firstlook.persist.imt.DealerGridValuesDao;

public class MatrixServiceImpl implements MatrixService {

	private DealerGridThresholdsDao dealerGridThresholdsDao;
	private DealerGridValuesDao dealerGridValuesDao;
	
	public MatrixServiceImpl() {}

	public DealerGridThresholdsDao getDealerGridThresholdsDao() {
		return dealerGridThresholdsDao;
	}

	public void setDealerGridThresholdsDao(
			DealerGridThresholdsDao dealerGridThresholdsDao) {
		this.dealerGridThresholdsDao = dealerGridThresholdsDao;
	}

	public DealerGridValuesDao getDealerGridValuesDao() {
		return dealerGridValuesDao;
	}

	public void setDealerGridValuesDao(DealerGridValuesDao dealerGridValuesDao) {
		this.dealerGridValuesDao = dealerGridValuesDao;
	}

	public LightMatrix getLightMatrix(Integer dealerId) {
		DealerGridThresholds thresholds = dealerGridThresholdsDao.findByDealerId( dealerId );
		List<DealerGridValues> values = dealerGridValuesDao.findByDealerId( dealerId );
		
		LightMatrix lightMatrix = new LightMatrix();
		lightMatrix.setDealerId( dealerId );
		populateLightMatrix( lightMatrix, thresholds, values );
		return lightMatrix;
	}
	
	public void populateLightMatrix( LightMatrix lightMatrix, DealerGridThresholds thresholds, List<DealerGridValues> values) {
		try {
			PropertyUtils.copyProperties( lightMatrix, thresholds );
		} catch( Exception e ) {
		}
		
		int i = 0;
		for( DealerGridValues value : values ) {
			i++;
			LightDescriptor desc = LightDescriptor.determineId( value.getLightValue(), value.getTypeValue() );
			try {
				PropertyUtils.setProperty( lightMatrix, "gridValue" + i, desc.getId() );
			} catch( Exception e ) {
			}
		}
	}
}
