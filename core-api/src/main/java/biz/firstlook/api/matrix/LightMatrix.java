package biz.firstlook.api.matrix;

import java.util.List;

public class LightMatrix {

	private Integer dealerId;
	
	private Integer gridValue1;

	private Integer gridValue2;

	private Integer gridValue3;

	private Integer gridValue4;

	private Integer gridValue5;

	private Integer gridValue6;

	private Integer gridValue7;

	private Integer gridValue8;

	private Integer gridValue9;

	private Integer gridValue10;

	private Integer gridValue11;

	private Integer gridValue12;

	private Integer gridValue13;

	private Integer gridValue14;

	private Integer gridValue15;

	private Integer gridValue16;

	private Integer gridValue17;

	private Integer gridValue18;

	private Integer gridValue19;

	private Integer gridValue20;

	private Integer gridValue21;

	private Integer gridValue22;

	private Integer gridValue23;

	private Integer gridValue24;

	private Integer gridValue25;

	private Integer firstValuationThreshold;

	private Integer secondValuationThreshold;

	private Integer thirdValuationThreshold;

	private Integer firstDealThreshold;

	private Integer secondDealThreshold;

	private Integer thirdDealThreshold;

	private Integer fourthDealThreshold;

	private Integer fourthValuationThreshold;
	
	private List<LightDescriptor> lightDescriptors = LightDescriptor.createLightDescriptors();

	public LightMatrix() {
	}

/*	public LightGridForm(DealerGridPreference dlp) {
		super();

		setFirstDealThreshold(dlp.getFirstDealThreshold());
		setSecondDealThreshold(dlp.getSecondDealThreshold());
		setThirdDealThreshold(dlp.getThirdDealThreshold());
		setFourthDealThreshold(dlp.getFourthDealThreshold());

		setFirstValuationThreshold(dlp.getFirstValuationThreshold());
		setSecondValuationThreshold(dlp.getSecondValuationThreshold());
		setThirdValuationThreshold(dlp.getThirdValuationThreshold());
		setFourthValuationThreshold(dlp.getFourthValuationThreshold());

		List gridValues = dlp.getGridLightValues();
		if (gridValues != null && gridValues.size() == 25) {
			ConvertGridValuesService convertService = new ConvertGridValuesService();
			setGridValue1((convertService.determineId(dlp.getGridLightValue(0),
					dlp.getGridCIATypeValue(0))).getId());
			setGridValue2((convertService.determineId(dlp.getGridLightValue(1),
					dlp.getGridCIATypeValue(1))).getId());
			setGridValue3((convertService.determineId(dlp.getGridLightValue(2),
					dlp.getGridCIATypeValue(2))).getId());
			setGridValue4((convertService.determineId(dlp.getGridLightValue(3),
					dlp.getGridCIATypeValue(3))).getId());
			setGridValue5((convertService.determineId(dlp.getGridLightValue(4),
					dlp.getGridCIATypeValue(4))).getId());
			setGridValue6((convertService.determineId(dlp.getGridLightValue(5),
					dlp.getGridCIATypeValue(5))).getId());
			setGridValue7((convertService.determineId(dlp.getGridLightValue(6),
					dlp.getGridCIATypeValue(6))).getId());
			setGridValue8((convertService.determineId(dlp.getGridLightValue(7),
					dlp.getGridCIATypeValue(7))).getId());
			setGridValue9((convertService.determineId(dlp.getGridLightValue(8),
					dlp.getGridCIATypeValue(8))).getId());
			setGridValue10((convertService.determineId(
					dlp.getGridLightValue(9), dlp.getGridCIATypeValue(9)))
					.getId());
			setGridValue11((convertService.determineId(dlp
					.getGridLightValue(10), dlp.getGridCIATypeValue(10)))
					.getId());
			setGridValue12((convertService.determineId(dlp
					.getGridLightValue(11), dlp.getGridCIATypeValue(11)))
					.getId());
			setGridValue13((convertService.determineId(dlp
					.getGridLightValue(12), dlp.getGridCIATypeValue(12)))
					.getId());
			setGridValue14((convertService.determineId(dlp
					.getGridLightValue(13), dlp.getGridCIATypeValue(13)))
					.getId());
			setGridValue15((convertService.determineId(dlp
					.getGridLightValue(14), dlp.getGridCIATypeValue(14)))
					.getId());
			setGridValue16((convertService.determineId(dlp
					.getGridLightValue(15), dlp.getGridCIATypeValue(15)))
					.getId());
			setGridValue17((convertService.determineId(dlp
					.getGridLightValue(16), dlp.getGridCIATypeValue(16)))
					.getId());
			setGridValue18((convertService.determineId(dlp
					.getGridLightValue(17), dlp.getGridCIATypeValue(17)))
					.getId());
			setGridValue19((convertService.determineId(dlp
					.getGridLightValue(18), dlp.getGridCIATypeValue(18)))
					.getId());
			setGridValue20((convertService.determineId(dlp
					.getGridLightValue(19), dlp.getGridCIATypeValue(19)))
					.getId());
			setGridValue21((convertService.determineId(dlp
					.getGridLightValue(20), dlp.getGridCIATypeValue(20)))
					.getId());
			setGridValue22((convertService.determineId(dlp
					.getGridLightValue(21), dlp.getGridCIATypeValue(21)))
					.getId());
			setGridValue23((convertService.determineId(dlp
					.getGridLightValue(22), dlp.getGridCIATypeValue(22)))
					.getId());
			setGridValue24((convertService.determineId(dlp
					.getGridLightValue(23), dlp.getGridCIATypeValue(23)))
					.getId());
			setGridValue25((convertService.determineId(dlp
					.getGridLightValue(24), dlp.getGridCIATypeValue(24)))
					.getId());
		}
	}*/

	public Integer getGridValue1() {
		return gridValue1;
	}

	public Integer getGridValue10() {
		return gridValue10;
	}

	public Integer getGridValue11() {
		return gridValue11;
	}

	public Integer getGridValue12() {
		return gridValue12;
	}

	public Integer getGridValue13() {
		return gridValue13;
	}

	public Integer getGridValue14() {
		return gridValue14;
	}

	public Integer getGridValue15() {
		return gridValue15;
	}

	public Integer getGridValue16() {
		return gridValue16;
	}

	public Integer getGridValue2() {
		return gridValue2;
	}

	public Integer getGridValue3() {
		return gridValue3;
	}

	public Integer getGridValue4() {
		return gridValue4;
	}

	public Integer getGridValue5() {
		return gridValue5;
	}

	public Integer getGridValue6() {
		return gridValue6;
	}

	public Integer getGridValue7() {
		return gridValue7;
	}

	public Integer getGridValue8() {
		return gridValue8;
	}

	public Integer getGridValue9() {
		return gridValue9;
	}

	public void setGridValue1(Integer i) {
		gridValue1 = i;
	}

	public void setGridValue10(Integer i) {
		gridValue10 = i;
	}

	public void setGridValue11(Integer i) {
		gridValue11 = i;
	}

	public void setGridValue12(Integer i) {
		gridValue12 = i;
	}

	public void setGridValue13(Integer i) {
		gridValue13 = i;
	}

	public void setGridValue14(Integer i) {
		gridValue14 = i;
	}

	public void setGridValue15(Integer i) {
		gridValue15 = i;
	}

	public void setGridValue16(Integer i) {
		gridValue16 = i;
	}

	public void setGridValue2(Integer i) {
		gridValue2 = i;
	}

	public void setGridValue3(Integer i) {
		gridValue3 = i;
	}

	public void setGridValue4(Integer i) {
		gridValue4 = i;
	}

	public void setGridValue5(Integer i) {
		gridValue5 = i;
	}

	public void setGridValue6(Integer i) {
		gridValue6 = i;
	}

	public void setGridValue7(Integer i) {
		gridValue7 = i;
	}

	public void setGridValue8(Integer i) {
		gridValue8 = i;
	}

	public void setGridValue9(Integer i) {
		gridValue9 = i;
	}

	public Integer getFirstDealThreshold() {
		return firstDealThreshold;
	}

	public Integer getFirstValuationThreshold() {
		return firstValuationThreshold;
	}

	public Integer getSecondDealThreshold() {
		return secondDealThreshold;
	}

	public Integer getSecondValuationThreshold() {
		return secondValuationThreshold;
	}

	public Integer getThirdDealThreshold() {
		return thirdDealThreshold;
	}

	public Integer getThirdValuationThreshold() {
		return thirdValuationThreshold;
	}

	public void setFirstDealThreshold(Integer i) {
		firstDealThreshold = i;
	}

	public void setFirstValuationThreshold(Integer i) {
		firstValuationThreshold = i;
	}

	public void setSecondDealThreshold(Integer i) {
		secondDealThreshold = i;
	}

	public void setSecondValuationThreshold(Integer i) {
		secondValuationThreshold = i;
	}

	public void setThirdDealThreshold(Integer i) {
		thirdDealThreshold = i;
	}

	public void setThirdValuationThreshold(Integer i) {
		thirdValuationThreshold = i;
	}

	public Integer getFourthDealThreshold() {
		return fourthDealThreshold;
	}

	public Integer getFourthValuationThreshold() {
		return fourthValuationThreshold;
	}

	public void setFourthDealThreshold(Integer i) {
		fourthDealThreshold = i;
	}

	public void setFourthValuationThreshold(Integer i) {
		fourthValuationThreshold = i;
	}

	public Integer getGridValue17() {
		return gridValue17;
	}

	public Integer getGridValue18() {
		return gridValue18;
	}

	public Integer getGridValue19() {
		return gridValue19;
	}

	public Integer getGridValue20() {
		return gridValue20;
	}

	public Integer getGridValue21() {
		return gridValue21;
	}

	public Integer getGridValue22() {
		return gridValue22;
	}

	public Integer getGridValue23() {
		return gridValue23;
	}

	public Integer getGridValue24() {
		return gridValue24;
	}

	public Integer getGridValue25() {
		return gridValue25;
	}

	public void setGridValue17(Integer i) {
		gridValue17 = i;
	}

	public void setGridValue18(Integer i) {
		gridValue18 = i;
	}

	public void setGridValue19(Integer i) {
		gridValue19 = i;
	}

	public void setGridValue20(Integer i) {
		gridValue20 = i;
	}

	public void setGridValue21(Integer i) {
		gridValue21 = i;
	}

	public void setGridValue22(Integer i) {
		gridValue22 = i;
	}

	public void setGridValue23(Integer i) {
		gridValue23 = i;
	}

	public void setGridValue24(Integer i) {
		gridValue24 = i;
	}

	public void setGridValue25(Integer i) {
		gridValue25 = i;
	}

	public List<LightDescriptor> getLightDescriptors() {
		return lightDescriptors;
	}

	public void setLightDescriptors(List<LightDescriptor> lightDescriptors) {
		this.lightDescriptors = lightDescriptors;
	}

	public Integer getDealerId() {
		return dealerId;
	}

	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}
	
	public Integer getId() {
		return getDealerId();
	}
	
	public void setId(Integer dealerId) {
		setDealerId( dealerId );
	}
}
