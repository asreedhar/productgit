package biz.firstlook.api.matrix;

public interface MatrixService {

	public LightMatrix getLightMatrix(Integer dealerId);
	
}
