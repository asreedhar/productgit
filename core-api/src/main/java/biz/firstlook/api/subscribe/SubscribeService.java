package biz.firstlook.api.subscribe;

import java.util.List;

import javax.jws.WebService;

import biz.firstlook.model.imt.SubscriberType;
import biz.firstlook.model.imt.Subscription;
import biz.firstlook.model.imt.SubscriptionDeliveryType;
import biz.firstlook.model.imt.SubscriptionFrequencyDetail;
import biz.firstlook.model.imt.SubscriptionType;

@WebService
public interface SubscribeService {

	public abstract MemberSubscription retrieve(Integer memberId, Integer subscriptionTypeId  );
	public abstract void save(MemberSubscription memberSubscription);
	
	/**
	 * Find by subscriptionTypeId, and frequencyDetail description (return all the subscriptions for 
	 * the Used Scorecard on a Tuesday).  This is used in the periodic report generator.  We retrieve all the 
	 * subscriptions, then we iterate through them and send off the reports to the appropriate people.
	 * 
	 * @param type A SubscriptionType "Redistribution Hot Sheets, etc."
	 * @param detail - Corresponds to the detail, "monday, tuesday, etc."
	 * @return A list of MemberSubscriptions
	 */
	public abstract List<Subscription> retrieve(SubscriptionType subscriptionType, SubscriptionFrequencyDetail detail, SubscriberType type );
	public abstract List<SubscriptionType> allSubscriptionTypes();
	public abstract List<SubscriptionFrequencyDetail> allSubscriptionFrequencyDetails();

	public abstract List<SubscriptionFrequencyDetail> getSubscriptionFrequencyDetails(Integer subscriptionTypeId);
	public abstract List<SubscriptionDeliveryType> getSubscriptionDeliveryTypes(Integer subscriptionTypeId);

	public abstract SubscriptionFrequencyDetail forDay( Integer weekDay );	
	public abstract SubscriptionType getType( String description );
	public abstract SubscriberType getSubscriberType( String description );

}