	package biz.firstlook.api.subscribe.db;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.api.subscribe.MemberSubscription;
import biz.firstlook.api.subscribe.SubscribeService;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.DealerUpgradeCode;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.SubscriberType;
import biz.firstlook.model.imt.Subscription;
import biz.firstlook.model.imt.SubscriptionDeliveryType;
import biz.firstlook.model.imt.SubscriptionFrequencyDetail;
import biz.firstlook.model.imt.SubscriptionType;
import biz.firstlook.persist.imt.MemberDao;
import biz.firstlook.persist.imt.SubscriberTypeDao;
import biz.firstlook.persist.imt.SubscriptionDao;
import biz.firstlook.persist.imt.SubscriptionDeliveryTypeDao;
import biz.firstlook.persist.imt.SubscriptionFrequencyDetailDao;
import biz.firstlook.persist.imt.SubscriptionTypeDao;

@WebService( serviceName = "SubscribeService", endpointInterface = "biz.firstlook.api.subscribe.SubscribeService" ) 
public class DbSubscribeService implements SubscribeService {

	private Logger logger = Logger.getLogger( DbSubscribeService.class );
	
	private SubscriptionDao subscriptionDao;
	private SubscriptionDeliveryTypeDao subscriptionDeliveryTypeDao;
	private SubscriptionFrequencyDetailDao subscriptionFrequencyDetailDao;
	private SubscriptionTypeDao subscriptionTypeDao;
	private SubscriberTypeDao subscriberTypeDao;
	private MemberDao memberDao;
	
	private Map<Integer, Integer> weekDayToFrequencyDetail;
	
	public DbSubscribeService() {
		weekDayToFrequencyDetail = new HashMap<Integer,Integer>();
		populateWeekDayToFrequencyMap();
	}
	
	private void populateWeekDayToFrequencyMap() {
		weekDayToFrequencyDetail.put( Calendar.SUNDAY, 4 );
		weekDayToFrequencyDetail.put( Calendar.MONDAY, 5 );
		weekDayToFrequencyDetail.put( Calendar.TUESDAY, 6 );
		weekDayToFrequencyDetail.put( Calendar.WEDNESDAY, 7 );
		weekDayToFrequencyDetail.put( Calendar.THURSDAY, 8 );
		weekDayToFrequencyDetail.put( Calendar.FRIDAY, 9 );
		weekDayToFrequencyDetail.put( Calendar.SATURDAY, 10 );
	}

	/**
	 * Builds a MemberSubscription for this member/subscriptionType.
	 * 
	 */
	public MemberSubscription retrieve(Integer memberId, Integer subscriptionTypeId) {

		SubscriptionType subscriptionType = subscriptionTypeDao.findById( subscriptionTypeId, false );
		logger.debug( "Retrieved Subscription Type: " + subscriptionType.getId() + ", " + subscriptionType.getDescription() );

		// current subscriptions
		List<Subscription> subscriptions = retrieveSubscriptions(memberId, subscriptionType);
			
		MemberSubscription memberSubscription = new MemberSubscription();
		memberSubscription.setMemberId( memberId );
		memberSubscription.setSubscriptionTypeId( subscriptionTypeId );
		memberSubscription.setSubscriptionTypeName( subscriptionType.getDescription() );
		
		// set all the BU's for which this member has this subscription type
		for( Subscription subscription : subscriptions ) {
			// set selected subscription delivery types
			SubscriptionDeliveryType deliveryType = subscription.getSubscriptionDeliveryType();
			memberSubscription.getSubscriptionDeliveryTypeIds().add( deliveryType.getId() );

			// this just set the selected frequency. all subscriptions of a certain type for a member need
			// to be set to the same freq - so this is a redundant set call but it gets it done.
			SubscriptionFrequencyDetail detail = subscription.getSubscriptionFrequencyDetail();
			memberSubscription.setSubscriptionFrequencyDetailId( detail.getId() );
			
			memberSubscription.getBusinessUnitIds().add( subscription.getBusinessUnitId() );
		}
		
		
		// set all dealers that are allowed this subsubscription type - i.e. have corresponding upgrade
		Member member = memberDao.findById( memberId, false );
		Set<Dealer> dealers = member.getDealers();
		for( Dealer dealer : dealers ) {
			boolean addDealer = false;
			for (DealerUpgradeCode upgradeRequiredForSubscription : subscriptionType.getDealerUpgradeCodes()) {
				for (DealerUpgrade dealersUpgrade: dealer.getDealerUpgrade()) {
					if (dealersUpgrade.getDealerUpgradeCD().intValue() == upgradeRequiredForSubscription.getId().intValue()) {
						addDealer = true;
						break;
					}
				}
				if (addDealer) break;
			}
			if (addDealer) {
				memberSubscription.getEligibleDealers().add( dealer );
				if( subscriptionType.getId().equals(Integer.valueOf(15))) {
					memberSubscription.getEligibleDealerGroups().add(dealer.getDealerGroup());
					break;
				}
			}
		}
		
		return memberSubscription;
	}
	
	
	
	

	private List<Subscription> retrieveSubscriptions(Integer memberId, SubscriptionType subscriptionType) {
		List<Subscription> subscriptions = subscriptionDao.findByMemberIdAndSubscriptionTypeId( memberId, subscriptionType.getId() );
		logger.debug( "Retrieved Subscriptions for memberId: " + memberId + "," + subscriptions.size() );
		return subscriptions;
	}

	@Transactional(readOnly=false)
	public void save(MemberSubscription memberSubscription) {
		Integer subscriptionTypeId = memberSubscription.getSubscriptionTypeId();
		SubscriptionType subscriptionType = subscriptionTypeDao.findById( subscriptionTypeId, false );
		logger.debug( "Retrieved Subscription Type: " + subscriptionType.getId() + ", " + subscriptionType.getDescription() );

		Integer subscriptionFrequencyDetailId = memberSubscription.getSubscriptionFrequencyDetailId();
		SubscriptionFrequencyDetail frequencyDetail = subscriptionFrequencyDetailDao.findById( subscriptionFrequencyDetailId, false );
		
		Integer memberId = memberSubscription.getMemberId();
		List<Subscription> subscriptions = retrieveSubscriptions(memberId, subscriptionType);

		logger.debug( "Deleting all subscriptions for memberId : " + memberId +", subType: " + subscriptionType.getDescription() );
		for( Subscription subscription : subscriptions ) {
			subscriptionDao.makeTransient( subscription );
		}
		
		for( Integer businessUnitId : memberSubscription.getBusinessUnitIds() ) {
			for( Integer deliveryTypeId : memberSubscription.getSubscriptionDeliveryTypeIds() ) {
				SubscriptionDeliveryType deliveryType = subscriptionDeliveryTypeDao.findById( deliveryTypeId, false );
				logger.debug( "Making Subscription for buId: " + businessUnitId +", " + deliveryType );
				SubscriberType subscriberType = subscriberTypeDao.findById( 1, false );
				Subscription subscription = new Subscription();
				subscription.setSubscriberId( memberId );
				subscription.setSubscriberType( subscriberType );
				subscription.setBusinessUnitId( businessUnitId );
				subscription.setSubscriptionDeliveryType( deliveryType );
				subscription.setSubscriptionType( subscriptionType );
				subscription.setSubscriptionFrequencyDetail( frequencyDetail );
				subscriptionDao.makePersistent( subscription );
				logger.debug( "Saving Subscription" );
			}
		}
	}
	
	public List<SubscriptionDeliveryType> getSubscriptionDeliveryTypes(Integer subscriptionTypeId) {
		SubscriptionType subType = subscriptionTypeDao.findById(subscriptionTypeId);
		return subType.getSubscriptionDeliveryTypes();
	}

	public List<SubscriptionFrequencyDetail> getSubscriptionFrequencyDetails(Integer subscriptionTypeId) {
		SubscriptionType subType = subscriptionTypeDao.findById(subscriptionTypeId);
		return subType.getSubscriptionFrequencyDetails();
	}

	public List<SubscriptionFrequencyDetail> allSubscriptionFrequencyDetails() {
		return subscriptionFrequencyDetailDao.findAll();
	}

	public List<SubscriptionType> allSubscriptionTypes() {
		return subscriptionTypeDao.findAll();
	}

	public List<Subscription> retrieve(SubscriptionType subscriptionType, SubscriptionFrequencyDetail detail, SubscriberType subscriberType) {
		return subscriptionDao.findBy( subscriptionType.getId(), detail.getId(), subscriberType.getId() );
	}	

	@Transactional(readOnly=false)
	public SubscriptionFrequencyDetail forDay(Integer weekDay) {
		SubscriptionFrequencyDetail detail =
			this.subscriptionFrequencyDetailDao.findById( weekDayToFrequencyDetail.get( weekDay ) );
		return detail;
	}

	public SubscriptionType getType(String description) {
		SubscriptionType type = new SubscriptionType();
		type.setDescription( description );
		return this.subscriptionTypeDao.findOneByExample( type );
	}

	public SubscriberType getSubscriberType(String description) {
		SubscriberType type = new SubscriberType();
		type.setDescription( description );
		return this.subscriberTypeDao.findOneByExample( type );
	}


	
	public SubscriptionDao getSubscriptionDao() {
		return subscriptionDao;
	}

	public void setSubscriptionDao(SubscriptionDao subscriptionDao) {
		this.subscriptionDao = subscriptionDao;
	}

	public SubscriptionTypeDao getSubscriptionTypeDao() {
		return subscriptionTypeDao;
	}

	public void setSubscriptionTypeDao(SubscriptionTypeDao subscriptionTypeDao) {
		this.subscriptionTypeDao = subscriptionTypeDao;
	}

	public MemberDao getMemberDao() {
		return memberDao;
	}

	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}

	public SubscriptionDeliveryTypeDao getSubscriptionDeliveryTypeDao() {
		return subscriptionDeliveryTypeDao;
	}

	public void setSubscriptionDeliveryTypeDao(
			SubscriptionDeliveryTypeDao subscriptionDeliveryTypeDao) {
		this.subscriptionDeliveryTypeDao = subscriptionDeliveryTypeDao;
	}

	public SubscriberTypeDao getSubscriberTypeDao() {
		return subscriberTypeDao;
	}

	public void setSubscriberTypeDao(SubscriberTypeDao subscriberTypeDao) {
		this.subscriberTypeDao = subscriberTypeDao;
	}

	public SubscriptionFrequencyDetailDao getSubscriptionFrequencyDetailDao() {
		return subscriptionFrequencyDetailDao;
	}

	public void setSubscriptionFrequencyDetailDao(
			SubscriptionFrequencyDetailDao subscriptionFrequencyDetailDao) {
		this.subscriptionFrequencyDetailDao = subscriptionFrequencyDetailDao;
	}



}
