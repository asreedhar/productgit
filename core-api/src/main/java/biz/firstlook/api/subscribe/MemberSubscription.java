package biz.firstlook.api.subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;

public class MemberSubscription {
	private Integer memberId;
	private Integer subscriptionTypeId;
	private String subscriptionTypeName;
	private Integer subscriptionFrequencyDetailId;
	private Set<Integer> subscriptionDeliveryTypeIds = new HashSet<Integer>();
	private Set<Integer> businessUnitIds = new HashSet<Integer>();
	private List<Dealer> eligibleDealers = new ArrayList<Dealer>();
	private Set<DealerGroup> eligibleDealerGroups = new LinkedHashSet<DealerGroup>();
	
	public MemberSubscription() {}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Set<Integer> getSubscriptionDeliveryTypeIds() {
		return subscriptionDeliveryTypeIds;
	}

	public void setSubscriptionDeliveryTypeIds(
			Set<Integer> subscriptionDeliveryTypeIds) {
		this.subscriptionDeliveryTypeIds = subscriptionDeliveryTypeIds;
	}

	public Integer getSubscriptionFrequencyDetailId() {
		return subscriptionFrequencyDetailId;
	}

	public void setSubscriptionFrequencyDetailId(
			Integer subscriptionFrequencyDetailId) {
		this.subscriptionFrequencyDetailId = subscriptionFrequencyDetailId;
	}

	public Integer getSubscriptionTypeId() {
		return subscriptionTypeId;
	}

	public void setSubscriptionTypeId(Integer subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}

	public String getSubscriptionTypeName() {
		return subscriptionTypeName;
	}

	public void setSubscriptionTypeName(String subscriptionTypeName) {
		this.subscriptionTypeName = subscriptionTypeName;
	}

	public Set<Integer> getBusinessUnitIds() {
		return businessUnitIds;
	}

	public void setBusinessUnitIds(Set<Integer> businessUnitIds) {
		this.businessUnitIds = businessUnitIds;
	}

	public List<Dealer> getEligibleDealers() {
		return eligibleDealers;
	}

	public void setEligibleDealers(List<Dealer> eligibleDealers) {
		this.eligibleDealers = eligibleDealers;
	}

	public Set<DealerGroup> getEligibleDealerGroups() {
		return eligibleDealerGroups;
	}

	public void setEligibleDealerGroups(Set<DealerGroup> eligibleDealerGroups) {
		this.eligibleDealerGroups = eligibleDealerGroups;
	}
	
}
