package biz.firstlook.api.dealer;

import javax.jws.WebService;

@WebService
public interface DealerService {

	public abstract String getShortName( Integer dealerId );
	
}	