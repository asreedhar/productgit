package biz.firstlook.api.dealer.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.lang.StringUtils;

import biz.firstlook.api.dealer.BusinessUnitCredentialService;
import biz.firstlook.model.imt.BusinessUnitCredential;
import biz.firstlook.model.imt.CredentialType;
import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.DealerUpgradeCode;
import biz.firstlook.persist.imt.BusinessUnitCredentialDao;
import biz.firstlook.persist.imt.DealerUpgradeDao;

/**
 * This is not ideal.
 * 
 * At this time the front end has been implemented with JSTL, Struts and DynaBeans. 
 * This totally screws the use of Collections as form vars. If you don't believe me and 
 * have some time to waste, give it a go.
 * 
 * Anyways, in the interest of my sanity, and because we add new the management of 
 * new Credentials only rarely, each possible option is set as a unique string. 
 * Hence, wirelessSource0, wirelessSource1,... etc 

 * @author nkeen
 *
 */
public class BusinessUnitCredentialServiceImpl implements BusinessUnitCredentialService {

	private static final String NONE = "NONE";
	private static final String WAVIS = CredentialType.WAVIS.getName();
	private static final String AUTOBASE = CredentialType.AUTOBASE.getName();
	private static final String DEALERSOCKET = CredentialType.DEALERSOCKET.getName();
	private static final String DEALERPEAK = CredentialType.DEALERPEAK.getName();
	private static final String LOWBOOKSALES = CredentialType.LOWBOOKSALES.getName();
	private static final String APPRAISAL_LOCKOUT = CredentialType.APPRAISAL_LOCK_OUT.getName();
	private static final String HIGHERGEAR = CredentialType.HIGHERGEAR.getName();
	private static final String REYNOLDS = CredentialType.REYNOLDS.getName();
	
	private BusinessUnitCredentialDao  businessUnitCredentialDao;
	private DealerUpgradeDao dealerUpgradeDao;
	
	public DynaBean getBusinessUnitCredentials(Integer businessUnitId) {
		DynaBean result = createDynaBean();
		populateDynaBean(businessUnitId, result, false);
		return result;
	}

	public DynaBean getBusinessUnitCredentialsForEdit(Integer businessUnitId) {
		DynaBean result = createDynaBean();
		populateDynaBean(businessUnitId, result, true);
		return result;
	}
	
	public void saveThirdPartyAppraisalCredentials( DynaBean dynaBean) {
		Integer businessUnitId = (Integer)dynaBean.get( "id" );
		String wirelessPass = (String)dynaBean.get( "wirelessPass" );
		String wirelessSource = (String)dynaBean.get( "wirelessSource" );
		String crmPass = (String)dynaBean.get( "crmPass" );
		String crmSource = (String)dynaBean.get( "crmSource" );
		
		List<BusinessUnitCredential> credentials = businessUnitCredentialDao.findByBusinessUnitId(businessUnitId);
		
		if (!crmSource.equalsIgnoreCase(NONE)) {
			if (crmSource.equals(CredentialType.AUTOBASE.getName())) {
				updateOrInsertCRMSource(businessUnitId, crmPass, CredentialType.AUTOBASE,credentials);
			} else if (crmSource.equals(CredentialType.DEALERSOCKET.getName())){
				updateOrInsertCRMSource(businessUnitId, crmPass, CredentialType.DEALERSOCKET,credentials);
			} else if (crmSource.equals(CredentialType.DEALERPEAK.getName())){
				updateOrInsertCRMSource(businessUnitId, crmPass, CredentialType.DEALERPEAK,credentials);
			} else if (crmSource.equals(CredentialType.LOWBOOKSALES.getName())){
				updateOrInsertCRMSource(businessUnitId, crmPass, CredentialType.LOWBOOKSALES,credentials);
			} else if (crmSource.equals(CredentialType.HIGHERGEAR.getName())){
				updateOrInsertCRMSource(businessUnitId, crmPass, CredentialType.HIGHERGEAR,credentials);
			} else if (crmSource.equals(CredentialType.REYNOLDS.getName())){
				String dealerNumber = (String)dynaBean.get( "reynoldsDealerNumber" );
				Integer storeNumber = (Integer)dynaBean.get( "reynoldsStoreNumber" );
				Integer areaNumber = (Integer)dynaBean.get( "reynoldsAreaNumber" );
				updateOrInsertReynoldsCRMSource(businessUnitId, crmPass, dealerNumber, storeNumber, areaNumber, CredentialType.REYNOLDS,credentials);
			}
		} else {
			//delete 
			delete(credentials, AUTOBASE);
			delete(credentials, DEALERSOCKET);
			delete(credentials, DEALERPEAK);
			delete(credentials, LOWBOOKSALES);
		}
		
		if (!wirelessSource.equalsIgnoreCase(NONE)) {
			if (contains(credentials, CredentialType.WAVIS)) {
				// update
				update(credentials, CredentialType.WAVIS, wirelessPass);
			} else {
				// insert
				insert(businessUnitId, CredentialType.WAVIS, wirelessPass);
			}
		} else {
			//delete 
			delete(credentials, WAVIS);
		}
	}

	private void updateOrInsertReynoldsCRMSource(Integer businessUnitId, String crmPass, String dealerNumber, Integer storeNumber, Integer areaNumber
		,CredentialType credentialType, List<BusinessUnitCredential> credentials) {
			
			if (contains(credentials, credentialType)) {
				// update
				update(credentials, credentialType, crmPass, dealerNumber, storeNumber, areaNumber);
			} else {
				insert(businessUnitId, credentialType, crmPass, dealerNumber, storeNumber, areaNumber);
			}
	}
	
	private void updateOrInsertCRMSource(Integer businessUnitId, String crmPass, CredentialType credentialType, List<BusinessUnitCredential> credentials) {
		if (contains(credentials, credentialType)) {
			// update
			update(credentials, credentialType, crmPass);
		} else {
			// insert
			insert(businessUnitId, credentialType, crmPass);
		}
	}	
	
	public void saveBookoutLockCredentials( DynaBean dynaBean) {
		Integer businessUnitId = (Integer)dynaBean.get( "id" );
		String appLockoutPass = (String)dynaBean.get("appLockPass");
		Boolean appLockEnabled = (Boolean)dynaBean.get("appLockEnabled");
		
		List<BusinessUnitCredential> credentials = businessUnitCredentialDao.findByBusinessUnitId(businessUnitId);
		
		DealerUpgrade upgrade = dealerUpgradeDao.findByBusinessUnitAndUpgradeType(businessUnitId, DealerUpgradeCode.APPRAISL_LOCKOUT.getId() );
		if ( upgrade == null && appLockEnabled )
		{	// make a new upgrade row
			upgrade = new DealerUpgrade();
			upgrade.setActive( appLockEnabled );
			upgrade.setBusinessUnitId( businessUnitId );
			upgrade.setDealerUpgradeCD( DealerUpgradeCode.APPRAISL_LOCKOUT.getId() );
			dealerUpgradeDao.makePersistent( upgrade );
		} else if ( upgrade != null )
		{	// update existing upgrade row
			upgrade.setActive( appLockEnabled );
			dealerUpgradeDao.makePersistent( upgrade );
		}
		
		// save the appraisal lock password regaurdless as to whether it is enabled
		if ( StringUtils.isNotBlank( appLockoutPass) ) {
			if (contains(credentials, CredentialType.APPRAISAL_LOCK_OUT)) {
				// update
				update(credentials, CredentialType.APPRAISAL_LOCK_OUT, appLockoutPass);
			} else {
				// insert
				insert(businessUnitId, CredentialType.APPRAISAL_LOCK_OUT, appLockoutPass);
			}
		} else {
			//delete 
			delete(credentials, APPRAISAL_LOCKOUT);
		}
	}
	
	public void delete(List<BusinessUnitCredential> credentials, String source) {
		CredentialType type = getCredentialType(source);
		if (contains(credentials, type)) {
			//delete 
			for (BusinessUnitCredential credential : credentials) {
				if (credential.getCredentialTypeId().intValue() == type.getId() ) {
					businessUnitCredentialDao.makeTransient(credential);
				}
			}
		}
	}

	private CredentialType getCredentialType(String source) {
		if (source.equalsIgnoreCase(WAVIS)) {
			return CredentialType.WAVIS;
		} else if (source.equalsIgnoreCase(AUTOBASE)) {
			return CredentialType.AUTOBASE;
		}else if (source.equalsIgnoreCase(DEALERSOCKET)) {
			return CredentialType.DEALERSOCKET;
		}else if (source.equalsIgnoreCase(DEALERPEAK)) {
			return CredentialType.DEALERPEAK;
		}else if (source.equalsIgnoreCase(LOWBOOKSALES)) {
			return CredentialType.LOWBOOKSALES;
		}else if (source.equalsIgnoreCase(APPRAISAL_LOCKOUT)) {
			return CredentialType.APPRAISAL_LOCK_OUT;
		}else if (source.equalsIgnoreCase(HIGHERGEAR)) {
			return CredentialType.HIGHERGEAR;
		}else if (source.equalsIgnoreCase(REYNOLDS)) {
				return CredentialType.REYNOLDS;
		}else {
			return null;
		}
	}

	private void insert(Integer businessUnitId, CredentialType credentialType, String pass) {
		BusinessUnitCredential credential = new BusinessUnitCredential();
		credential.setBusinessUnitId(businessUnitId);
		credential.setCredentialTypeId(credentialType.getId());
		credential.setPassword(pass);
		credential.setUpdatedDate(new Date());
		businessUnitCredentialDao.makePersistent(credential);
	}
	
	private void insert(Integer businessUnitId, CredentialType credentialType, String pass, String dealerNumber, Integer storeNumber, Integer areaNumber) {
		BusinessUnitCredential credential = new BusinessUnitCredential();
		credential.setBusinessUnitId(businessUnitId);
		credential.setCredentialTypeId(credentialType.getId());
		credential.setPassword(pass);
		credential.setReynoldsDealerNumber(dealerNumber);
		credential.setReynoldsStoreNumber(storeNumber);
		credential.setReynoldsAreaNumber(areaNumber);
		credential.setUpdatedDate(new Date());
		businessUnitCredentialDao.makePersistent(credential);
	}

	private void update(List<BusinessUnitCredential> credentials, CredentialType credentialType, String pass) {
		for (BusinessUnitCredential credential : credentials) {
			if (credential.getCredentialTypeId().intValue() == credentialType.getId()) {
				credential.setPassword(pass);
				credential.setUpdatedDate(new Date());
				businessUnitCredentialDao.makePersistent(credential);
			}
		}		
	}
	
	private void update(List<BusinessUnitCredential> credentials, CredentialType credentialType, String pass, String dealerNumber, Integer storeNumber, Integer areaNumber) {
		for (BusinessUnitCredential credential : credentials) {
			if (credential.getCredentialTypeId().intValue() == credentialType.getId()) {
				credential.setPassword(pass);
				credential.setReynoldsDealerNumber(dealerNumber);
				credential.setReynoldsStoreNumber(storeNumber);
				credential.setReynoldsAreaNumber(areaNumber);
				credential.setUpdatedDate(new Date());
				businessUnitCredentialDao.makePersistent(credential);
			}
		}		
	}

	private boolean contains(List<BusinessUnitCredential> credentials, CredentialType credentialType) {
		for (BusinessUnitCredential credential : credentials) {
			if (credential.getCredentialTypeId().intValue() == credentialType.getId().intValue()) {
				return true;
			}
		}
		return false;
	}

	private DynaBean createDynaBean() {
		DynaClass clazz = createDynaClass();
		DynaBean result;
		try
		{
			result = clazz.newInstance();
		}
		catch ( Exception e )
		{
			throw new RuntimeException( "Cannot create DynaBean instance" );
		}
		return result;
	}
	
	private void populateDynaBean(Integer businessUnitId, DynaBean result, boolean isEdit) {
		result.set( "id", businessUnitId );
		
		List<BusinessUnitCredential> credentials = businessUnitCredentialDao.findByBusinessUnitId(businessUnitId);
		List<String> assignedCrmSource =new ArrayList<String>();
		List<BusinessUnitCredential> nonCrmSource =new ArrayList<BusinessUnitCredential>();
		
		for (BusinessUnitCredential credential : credentials) {
			
			if (credential.getCredentialTypeId().intValue() == CredentialType.REYNOLDS.getId().intValue()) {
				//needed for dynaform
				result.set( "reynoldsDealerNumber", credential.getReynoldsDealerNumber());
				result.set( "reynoldsStoreNumber", credential.getReynoldsStoreNumber());
				result.set( "reynoldsAreaNumber", credential.getReynoldsAreaNumber());
				assignedCrmSource.add(CredentialType.getName(credential.getCredentialTypeId()));
			}
			else if (credential.getCredentialTypeId().intValue() == CredentialType.WAVIS.getId().intValue()) {
				result.set( "wirelessSource", WAVIS);
				result.set( "wirelessPass", credential.getPassword());
				nonCrmSource.add(credential);
			}else if (credential.getCredentialTypeId().intValue() == CredentialType.APPRAISAL_LOCK_OUT.getId().intValue()) {
				result.set( "appLockPass", credential.getPassword());
				nonCrmSource.add(credential);
			}
			else
				assignedCrmSource.add(CredentialType.getName(credential.getCredentialTypeId()));
		}
		credentials.removeAll(nonCrmSource);
		if(!credentials.isEmpty())
		{
			result.set( "crmPass", credentials.get(0).getPassword());
			result.set( "crmSource", CredentialType.getName(credentials.get(0).getCredentialTypeId()));
		}
		result.set("assignedCrmSource", assignedCrmSource);
		result.set("credentials", credentials);
		DealerUpgrade upgrade = dealerUpgradeDao.findByBusinessUnitAndUpgradeType(businessUnitId, DealerUpgradeCode.APPRAISL_LOCKOUT.getId() );
		
		result.set( "appLockEnabled", (upgrade != null) ? upgrade.getActive() : Boolean.FALSE );
		result.set( "wirelessSource0", NONE);
		result.set( "wirelessSource1", WAVIS);
		result.set( "crmSource0", NONE);
		result.set( "crmSource1", AUTOBASE);
		result.set( "crmSource2", DEALERSOCKET);
		result.set( "crmSource3", DEALERPEAK);
		result.set( "crmSource4", LOWBOOKSALES);
		result.set( "crmSource5", HIGHERGEAR);
		result.set( "crmSource6", REYNOLDS);
		result.set( "isEdit", isEdit);
	}
	
	private DynaClass createDynaClass()
	{
		List< DynaProperty > dynaProperties = new ArrayList< DynaProperty >();
		dynaProperties.add( new DynaProperty( "wirelessSource", String.class ) );
		dynaProperties.add( new DynaProperty( "wirelessPass", String.class ) );
		dynaProperties.add( new DynaProperty( "wirelessSource0", String.class ) );
		dynaProperties.add( new DynaProperty( "wirelessSource1", String.class ) );

		dynaProperties.add( new DynaProperty( "crmSource", String.class ) );
		dynaProperties.add( new DynaProperty( "crmPass", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource0", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource1", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource2", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource3", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource4", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource5", String.class ) );
		dynaProperties.add( new DynaProperty( "crmSource6", String.class ) );
		dynaProperties.add( new DynaProperty( "assignedCrmSource", List.class ) );
		dynaProperties.add( new DynaProperty( "credentials", List.class ) );
		
		dynaProperties.add( new DynaProperty( "appLockEnabled", Boolean.class ) );
		dynaProperties.add( new DynaProperty( "appLockPass", String.class ) );
		
		dynaProperties.add( new DynaProperty( "isEdit", Boolean.class ) );
		dynaProperties.add( new DynaProperty( "id", Integer.class ) );
		
		dynaProperties.add( new DynaProperty("reynoldsDealerNumber", String.class));
		dynaProperties.add( new DynaProperty("reynoldsStoreNumber", Integer.class));
		dynaProperties.add( new DynaProperty("reynoldsAreaNumber", Integer.class));
		
		DynaClass dynaClass = new BasicDynaClass( "BusinessUnitCredentials", BasicDynaBean.class, dynaProperties.toArray( new DynaProperty[0] ) );
		return dynaClass;
	}

	public void setBusinessUnitCredentialDao(BusinessUnitCredentialDao businessUnitCredentialDao) {
		this.businessUnitCredentialDao = businessUnitCredentialDao;
	}

	public void setDealerUpgradeDao(DealerUpgradeDao dealerUpgradeDao) {
		this.dealerUpgradeDao = dealerUpgradeDao;
	}
}
