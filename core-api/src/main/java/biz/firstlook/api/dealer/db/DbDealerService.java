package biz.firstlook.api.dealer.db;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import biz.firstlook.api.dealer.DealerService;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.imt.DealerDao;

@WebService( serviceName = "DealerService", endpointInterface = "biz.firstlook.api.dealer.DealerService" ) 
public class DbDealerService implements DealerService {

	private Logger log = Logger.getLogger( DbDealerService.class );
	
	private DealerDao dealerDao;

	public DealerDao getDealerDao() {
		return dealerDao;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}

	public String getShortName(Integer dealerId) {
		log.debug( "Retreiving Dealer: " + dealerId );
		Dealer dealer = dealerDao.findById( dealerId );
		return dealer.getShortName();
	}

	
}
