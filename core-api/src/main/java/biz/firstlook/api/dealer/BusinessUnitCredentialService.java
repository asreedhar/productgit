package biz.firstlook.api.dealer;

import java.util.List;

import org.apache.commons.beanutils.DynaBean;

import biz.firstlook.model.imt.BusinessUnitCredential;

public interface BusinessUnitCredentialService {

	public DynaBean getBusinessUnitCredentials(Integer businessUnitId);
	public void delete(List<BusinessUnitCredential> credentials, String source);

}
