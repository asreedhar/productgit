package biz.firstlook.api.target.scorecard;

import org.apache.commons.beanutils.DynaBean;

public interface STService {

	public abstract DynaBean retrieveTargets(Integer dealerId);
	public abstract void saveTargets(DynaBean dynaBean);

}