package biz.firstlook.api.target.scorecard.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.PropertyUtils;

import biz.firstlook.api.target.scorecard.STService;
import biz.firstlook.model.imt.DealerTarget;
import biz.firstlook.model.imt.Target;
import biz.firstlook.persist.imt.DealerTargetDao;
import biz.firstlook.persist.imt.TargetDao;

public class STServiceDb implements STService {

	private DealerTargetDao dealerTargetDao;
	private TargetDao targetDao;
	
	public STServiceDb() {}
	
	/* (non-Javadoc)
	 * @see biz.firstlook.api.target.scorecard.db.ScorecardTargetService#retrieveTargets(java.lang.Integer)
	 */
	public DynaBean retrieveTargets(Integer dealerId) {
		DynaClass clazz = createDynaClass();
		DynaBean result;
		try {
			result = clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException( "Cannot create DynaBean instance" );
		} 

		List<DealerTarget> dTargets = dealerTargetDao.retrieveActive( dealerId );
		
		if( dTargets != null && dTargets.size() > 0 ) {
			for( DealerTarget dTarget : dTargets ) {
				Target target = dTarget.getTarget();
				result.set( "target" + target.getCode(), dTarget.getValue() );
			}
		}
		result.set( "id", dealerId );
		
		return result;
	}

	private DynaClass createDynaClass() {
		List<DynaProperty> dynaProperties = new ArrayList<DynaProperty>();
		List<Target> targets = targetDao.findAll(); 
		for( Target target : targets ) {
			dynaProperties.add( new DynaProperty("target"+target.getCode(),Integer.class));
		}
		dynaProperties.add(new DynaProperty("id",Integer.class));
		DynaClass dynaClass = new BasicDynaClass("ScorecardTargets", BasicDynaBean.class, 
				dynaProperties.toArray(new DynaProperty[0]));
		return dynaClass;
	}
	
	public void saveTargets(DynaBean dynaBean) {
		Integer dealerId = (Integer) dynaBean.get("id");
		Map properties = null;
		// Get all of the bean properties
		try {
			properties = PropertyUtils.describe( dynaBean );
		} catch (Exception e) {
			throw new RuntimeException("Error retrieving properties from bean", e );
		} 
		
		// For each of the bean properties
		for( Object keyObj : properties.keySet() ) {
			String key = (String) keyObj;
			Integer targetId = parsePropertyName( key );
			if( targetId != null ) {
				Integer value = (Integer) dynaBean.get(key);
				Target target = targetDao.findById( targetId, false );
				saveTarget( dealerId, target, value );
			}
		}
	}
	
	private void saveTarget(Integer dealerId, Target target, Integer value) {
		DealerTarget dealerTarget = dealerTargetDao.retrieveActive( dealerId, target ); 

		if( dealerTarget != null &&
			dealerTarget.getValue() != null &&
			!dealerTarget.getValue().equals( value ) ) {
			dealerTarget.setActive( false );
			dealerTargetDao.makePersistent( dealerTarget );

			DealerTarget newTarget = new DealerTarget();
			newTarget.setActive( true );
			newTarget.setBusinessUnitId( dealerId );
			newTarget.setStart( new Date() );
			newTarget.setTarget( target );
			newTarget.setValue( value );
			dealerTargetDao.makePersistent( newTarget );
		}
	}

	protected Integer parsePropertyName(String key) {
		Integer targetCd = null;
		if( key.startsWith("target") ) {
			targetCd = new Integer( key.substring(6) );
		}
		return targetCd;
	}

	public DealerTargetDao getDealerTargetDao() {
		return dealerTargetDao;
	}

	public void setDealerTargetDao(DealerTargetDao dealerTargetDao) {
		this.dealerTargetDao = dealerTargetDao;
	}

	public TargetDao getTargetDao() {
		return targetDao;
	}

	public void setTargetDao(TargetDao targetDao) {
		this.targetDao = targetDao;
	}
	
}
