<%@include file="/WEB-INF/jsp/include/tag-import.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 	<head>
	
	<title><d:title default="Marketplace Pricing Analyzer"/></title>
	<link rel="stylesheet" media="all" type="text/css" href="resources/styles/global.css"/>
	<link rel="stylesheet" media="all" type="text/css" href="resources/styles/analysis.css"/>
	
    <c:url var="prototype" value="/resources/js/ajax/prototype.js"/>
	<script src="${prototype}" type="text/javascript"></script>

   	<c:url var="effects" value="/resources/js/ajax/effects.js"/>
	<script src="${effects}" type="text/javascript"></script>
	
<d:head />

	
<style type="text/css">
.marketresults { width:300px;margin-right:5px;padding:5px;border:1px solid #666;background-color:#CCC;float:left; }
.resultwrapper { float:left;margin-right:10px; }

.results { width:301px;margin-top:10px;background-color:#CCC; }
.listeven, .listodd { border-bottom:1px solid #666;background-color:#EEE;line-height:18px; }
.listeven { background-color:#FFF; }
.listhead { background-color:#EEE;text-align:center;line-height:10px;font-size:10px;color:#333; }
.head { background:#003 url(/resources/images/b-header.gif) repeat-x;height:20px;color:#EEE;padding:5px 5px 5px 8px;border-bottom:1px solid #000; }
.sub { height:18px;background-color:#CCC;padding-top:5px;border-bottom:1px solid #333; }
.price { height:18px;background-color:#EEE;padding-top:5px; }
.col { width:33%;text-align:center;float:left;border-right:1px solid #333;font-size:12px;border-bottom:1px solid #333; }

.hide { display: none; }
.show { border-collapse:collapse;margin-top:-18px; }
a:link, a:hover, a:visited { color:#013366;font-weight:bold;font-size:11px;text-decoration:none; }
a:hover { color:#365daa;text-decoration:underline; }
table { border-collapse:collapse;border:1px solid #000; }
#marketone { position:absolute;z-index:10;top:253px;left:15px; }
#markettwo { position:absolute;z-index:10;top:253px;left:326px; }
#marketthree { position:absolute;z-index:10;top:253px;left:637px; }
</style>

<script type="text/JavaScript">
<!--
function detailReveal(targetID) {
	target = eval("'"+targetID+"'");
	var detailstate = document.getElementById(target).className;
	if (detailstate == 'hide') {
	document.getElementById(target).className='show';
	}
	else {
	document.getElementById(target).className='hide';
	}
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}



//-->
</script>


<script>
function PostCarSoupForm(url)
{
 	var carsoupForm = document.createElement("FORM");
	document.body.appendChild(carsoupForm);
	carsoupForm.method = "POST";
	carsoupForm.target = "_blank";
	carsoupForm.action = url.substring(0, url.indexOf("&minyear"));

	var paramString = url.substring(url.indexOf("&minyear"));
	var params = paramString.split("&");
	
	for(i = 0; i < params.length; i++)
	{
		if(params[i] != "" && params[i].indexOf("=") > -1)
		{
			var newElement = document.createElement("<input name='"+ params[i].split("=")[0] +"' type='hidden'>");
			carsoupForm.appendChild(newElement);
			newElement.value = params[i].split("=")[1];
		}
	}
	carsoupForm.submit();
	return false;	
}
</script>



<c:url var="atPrice" value="/price.html?provider-code=autotrader&mmg=${makeModelGrouping.id}&year=${year}&zip=${zip}&distance=${distance}">
</c:url>

<c:url var="cdcPrice" value="/price.html?provider-code=carsDotCom&mmg=${makeModelGrouping.id}&year=${year}&zip=${zip}&distance=${distance}">
</c:url>

<c:url var="csPrice" value="/price.html?provider-code=carsoup&mmg=${makeModelGrouping.id}&year=${year}&zip=${zip}&distance=${distance}">
</c:url>

<c:url var="blank" value="/blank.jsp"/>

<script language="javascript">

var ping_counter = 0;

function prices() {
  new Ajax.Updater('at', '${atPrice}', {asynchronous:true, evalScripts:true, onComplete: hidesonar } );
  new Ajax.Updater('cdc', '${cdcPrice}', {asynchronous:true, evalScripts:true, onComplete: hidesonar } );
  new Ajax.Updater('cs', '${csPrice}', {asynchronous:true, evalScripts:true, onComplete: hidesonar } );
}

function hidesonar() {
  ping_counter++;
  if( ping_counter == 2 ) {
  
    $('sonar3').StopPlay();
  new Ajax.Updater('sonar', '${blank}', {asynchronous:true, evalScripts:true } );

//    Element.remove($('sonar'));
  }
}
</script>


	</head>
<!-- /// sectionId: analysis / pageId: pricingAnalyzer /// -->
<body id="body" onload="prices();" class="analysis">	

<div id="title">
	<span style="float:right;padding:15px 25px 0 0;"><img src="resources/images/FL_logo.gif" /></span>
	<img src="resources/images/PING_rev.gif" style="margin:5px 0 0 15px;" />
</div>

<div id="content">
<d:body />	
<br /> 




<span class="screenOnly"><br clear="all"/></span>
<div id="footer">
	<cite class="disc">Vehicle Data copyright 1986 - 2008 Chrome Systems Inc.</cite>	
	<em>SECURE AREA</em> <tt>|</tt> <cite>&copy;2008 INCISENT Technologies, Inc. All rights reserved.</cite>
</div>
	</div>
	
	</div>
	</body>
</html>
