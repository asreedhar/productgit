<%@include file="/WEB-INF/jsp/include/tag-import.jsp" %>


<div class="resultwrapper">
<div class="results">
<p class="head"><span style="float:left;font-size:18px;"><strong>${provider.name}</strong></span><span style="float:right;padding-top:8px;">${prices.matches} matches</span></p>
<div class="col" style="border-left:1px solid #003;">
<p class="sub">High Price</p>
<p class="price"><f:formatNumber value="${prices.highest}" type="currency" maxFractionDigits="0"/></p>
</div>
<div class="col">
<p class="sub"><strong>Average Price</strong></p>
<p class="price"><strong><f:formatNumber value="${prices.average}" type="currency" maxFractionDigits="0"/></strong></p>
</div>
<div class="col">
<p class="sub">Low Price</p>
<p class="price"><f:formatNumber value="${prices.lowest}" type="currency" maxFractionDigits="0"/></p>
</div>
</div>
<c:url var="showR" value="/resources/images/show-r.gif"/>
<p style="clear:both;padding:5px 0 0 3px;"><a href="${prices.url}" target="_blank">Go to ${provider.name} overview of results</a>&nbsp; <img src="${showR}" /></p>
</div>