<%@include file="/WEB-INF/jsp/include/tag-import.jsp" %>

<div class="resultwrapper">
<div class="results">
<p class="head"><span style="float:left;font-size:18px;"><strong>${provider.name}</strong></span><span style="float:right;padding-top:8px;">no matches</span></p>
<div style="border:1px solid #003; padding: 16px;">
  <center><p>Prices currently unavailable</p></center>
</div>
</div>
<c:url var="showR" value="/resources/images/show-r.gif"/>
  <c:choose>
    <c:when test="${not empty providerUrl}">
      <p style="clear:both;padding:5px 0 0 3px;"><a href="${providerUrl}" target="_blank">Go to ${provider.name} overview of results</a>&nbsp; <img src="${showR}" /></p>
    </c:when>
  </c:choose>
</div>
