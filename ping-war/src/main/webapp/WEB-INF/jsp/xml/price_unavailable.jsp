<?xml version="1.0"?>
<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page"
    xmlns:c="http://java.sun.com/jsp/jstl/core"
    xmlns:f="http://java.sun.com/jsp/jstl/fmt"
    xmlns:x="http://java.sun.com/jsp/jstl/xml">

    <prices>
        <provider code="${provider.code}" available="${provider.available}" sortOrder="${provider.sortOrder}">
            <name>${provider.name}</name>
            <url><x:out select="$providerUrl"/></url>
        </provider>
        <makeModelGrouping id="${makeModelGrouping.id}">
            <groupingDescriptionId>${makeModelGrouping.groupingDescriptionId}</groupingDescriptionId>
            <make>${makeModelGrouping.make}</make>
            <model>${makeModelGrouping.model}</model>
            <segmentId>${makeModelGrouping.segmentId}</segmentId>
        </makeModelGrouping>
        <modelCode id="${modelCode.id}">
            <name>${modelCode.name}</name>
            <value>${modelCode.value}</value>
            <makeCode id="${modelCode.make.id}">
                <name>${modelCode.make.name}</name>
                <value>${modelCode.make.value}</value>
            </makeCode>
        </modelCode>
    </prices>
        
</jsp:root>

