<%@include file="/WEB-INF/jsp/include/tag-import.jsp" %>


<table>
  <tr>
    <td># Vehicles</td>
    <td>MMG ID</td>
    <td>Make</td>
    <td>Model</td>
    <td>Segment</td>
    <td>Codes Mapped</td>
  </tr>
<c:forEach items="${mappingStats}" var="stat">
  <tr>
    <td>${stat.numVehicles}</td>
    <td>${stat.makeModelGroupingId}</td>
    <td>${stat.make}</td>
    <td>${stat.model}</td>
    <td>${stat.segment}</td>
	<c:choose>
	  <c:when test="${stat.codeCount < 3}">
	    <td style="color: red;">${stat.codeCount}</td>
	  </c:when>
	  <c:otherwise>
	    <td>${stat.codeCount}</td>
	  </c:otherwise>
	</c:choose>
  </tr>
</c:forEach>
  <tr>
    <td># Vehicles</td>
    <td>MMG ID</td>
    <td>Make</td>
    <td>Model</td>
    <td>Segment</td>
    <td>Codes Mapped</td>
  </tr>
</table>
