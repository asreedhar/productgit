package biz.firstlook.app.web.ping;

import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.api.ping.PingService;
import biz.firstlook.api.ping.impl.PriceProvider;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Provider;

public class PopulateCodesController implements Controller {
	
	private static Logger log = Logger.getLogger( PopulateCodesController.class );
	
	private PingService pingService;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String providerCode = request.getParameter("provider-code");
		
		if( StringUtils.isEmpty(providerCode) ) {
			throw new RuntimeException( "provider-code is a required parameter for the price controller" );
		}
		
		final Provider provider = pingService.getProvider(providerCode);
		
		log.info( "Removing all codes for provider: " + provider );
		pingService.resetProviderCodes( provider );

		PriceProvider priceProvider = pingService.getPriceProvider( providerCode );
		log.info( "Fetching Makes" );
		List<MakeCode> makes = priceProvider.fetchMakes();
		log.info( makes.size() + " make codes retrieved" );
		CollectionUtils.transform( makes, new Transformer() {
			public Object transform(Object value) {
				MakeCode code = (MakeCode) value;
				code.setProvider( provider );
				return code;
			}		
		});
		
		for( MakeCode make : makes ) {
			log.info( "Fetching Models for Make: " + make );
			List<ModelCode> models = priceProvider.fetchModels(make);
			log.info( models.size() + " models retrieved" );
			CollectionUtils.transform( models, new Transformer() {
				public Object transform(Object value) {
					ModelCode code = (ModelCode) value;
					code.setProvider( provider );
					return code;
				}		
			});
			make.setModels( new HashSet<ModelCode>( models ) );
		}

		log.info( "Storing Makes and Models" );
		pingService.storeCodes( makes );

		return new ModelAndView("populateCodes","makes", makes);
	}

	public PingService getPingService() {
		return pingService;
	}

	public void setPingService(PingService pingService) {
		this.pingService = pingService;
	}
}
