package biz.firstlook.app.web.ping;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.api.ping.impl.PriceProvider;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Provider;

public class PriceController extends PINGController {
	
	private static final String PROVIDER_CODE_PARAM = "provider-code";
	private static Logger log = Logger.getLogger(PriceController.class);

	private String priceView;
	private String unavailableView;
	
	@SuppressWarnings("deprecation")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		// Get Parameters
		String providerCode = getProviderCode(request);
		String mmgId = getMakeModelGrouping(request);
		Integer year = getYear(request);
		String zip = getZip(request);		
		Integer distance = getDistance(request);

		// create and populate context (model)
		Map<String,Object> context = new HashMap<String,Object>();
		context.put( "year", year );
		context.put( "zip", zip );
		context.put( "distance", distance);
		
		// Retrieve  the Provider
		Provider provider = getPingService().getProvider( providerCode );
		System.out.println("Checking prices for provider: " + providerCode);
		context.put( "provider", provider );

		// Retrieve the approprate make model grouping
		MakeModelGrouping makeModelGrouping = getPingService().getMakeModelGrouping( Integer.parseInt( mmgId ) );
		context.put( "makeModelGrouping", makeModelGrouping );
		
		// Retrieve the Provider-specific Make Model Code
		ModelCode modelCode = getPingService().getModelCode( makeModelGrouping, provider );
		context.put( "modelCode", modelCode );
		
		log.debug( "Fetching PING Price for: mmg: " + makeModelGrouping.getMake() + ", " + makeModelGrouping.getModel() );
		log.debug( "Fetching Prices for Year: " + year + ", Zip: " + zip + ", Distance: " + distance );

		// Retrieve the PriceProvider for the specific provider
		PriceProvider priceProvider = getPingService().getPriceProvider( providerCode );

		try {
			// Get the Quote, this method calls off to the provider's web site
			Prices prices = priceProvider.getQuote(modelCode, year, distance, zip);
			context.put( "prices", prices );

			String providerUrl = priceProvider.subUrl(priceProvider.getSearchUrl(), modelCode.getMake().getValue(), modelCode.getMake().getName(), modelCode.getValue(), year, distance, zip);
			context.put( "providerUrl", providerUrl);
			
			return new ModelAndView(getPriceView(), context);
		} catch( Exception e ) {
			if( modelCode != null ) {
				// If we have a model code, this means that we have a proper mapping for this specific Make Model Grouping
				// If we have a Model Code, we can place the provider Url in the model and provide a link
				String providerUrl = priceProvider.subUrl(priceProvider.getSearchUrl(), modelCode.getMake().getValue(),modelCode.getMake().getName(), modelCode.getValue(), year, distance, zip);
				log.error( "Error parsing a price quote: " + providerUrl, e );
   			  	context.put( "providerUrl", providerUrl);
			}
			context.put( "provider", provider );
			return new ModelAndView(getUnavailableView(), context);			
		}
	}

	private String getProviderCode(HttpServletRequest request) {
		String providerCode = request.getParameter(PROVIDER_CODE_PARAM);		
		if( StringUtils.isEmpty(providerCode) ) {
			throw new RuntimeException( "provider-code is a required parameter for the price controller" );
		}
		return providerCode;
	}

	public String getPriceView() {
		return priceView;
	}

	public void setPriceView(String priceView) {
		this.priceView = priceView;
	}

	public String getUnavailableView() {
		return unavailableView;
	}

	public void setUnavailableView(String unavailableView) {
		this.unavailableView = unavailableView;
	}

}
