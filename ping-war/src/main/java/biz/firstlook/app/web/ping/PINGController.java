 	package biz.firstlook.app.web.ping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.api.ping.PingService;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.model.Provider;

public class PINGController implements Controller {
	

	private static final String DISTANCE_PARAM = "distance";
	private static final String ZIP_PARAM = "zip";
	private static final String YEAR_PARAM = "year";
	private static final String MMG_PARAM = "mmg";

	private static final Integer[] DISTANCE_ARRAY = new Integer[] { 25, 50, 75, 100, 200 };

	private static final String DEFAULT_DISTANCE = "50";

	private Logger log = Logger.getLogger( PINGController.class );
	
	private PingService pingService;
	
	public PINGController() {}
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// Read Parameters
		String mmgId = getMakeModelGrouping(request);
		Integer year = getYear(request);
		String zip = getZip(request);		
		Integer distance = getDistance(request);
		
		// Retrieve the Make Model Grouping
		MakeModelGrouping makeModelGrouping = pingService.getMakeModelGrouping( Integer.parseInt( mmgId ) );
		
		log.debug( "Fetching PING Price for: mmg: " + makeModelGrouping.getMake() + ", " + makeModelGrouping.getModel()  );
		log.debug( "Fetching Prices for Year: " + year + ", Zip: " + zip + ", Distance: " + distance );
		
		// Figure out which providers are available.
		List<Provider> providers = pingService.getAvailableProviders();
		
		// Create and Populate the context (the Model)
		Map<String,Object> context = new HashMap<String,Object>();
		context.put( "makeModelGrouping", makeModelGrouping );
		context.put( YEAR_PARAM, year );
		context.put( ZIP_PARAM, zip );
		context.put( DISTANCE_PARAM, distance );
		context.put( "distances", DISTANCE_ARRAY );
		
		// If no providers are available, just render a "PING is currently unavailable"
		if( !pingService.isPingActive() || providers == null || providers.size() == 0 ) {
			log.info( "No Providers are available, redirecting to unavailable.");
			return new ModelAndView("unavailable");
		} else {				
			// Else, put the providers in the context and render the PING page
			log.info( "Redirecting to PING, providers: " + providers.size() );
			context.put( "providers", providers );
			return new ModelAndView("ping", context);
		}
	}

	protected Integer getDistance(HttpServletRequest request) {
		// Get the Distance
		String distanceStr = request.getParameter(DISTANCE_PARAM);
		if( StringUtils.isEmpty(distanceStr) ) {
			distanceStr = DEFAULT_DISTANCE;
		}
		Integer distance = Integer.parseInt( distanceStr );
		return distance;
	}

	protected String getZip(HttpServletRequest request) {
		// Get the Zip Code
		String zip = request.getParameter(ZIP_PARAM);
		if( StringUtils.isEmpty(zip) ) {
			throw new RuntimeException( "zip is a required parameter" );
		}
		return zip;
	}

	protected Integer getYear(HttpServletRequest request) {
		// Get the Year
		String yearStr = request.getParameter(YEAR_PARAM);
		if( StringUtils.isEmpty(yearStr) ) {
			throw new RuntimeException( "year is a required parameter" );
		}
		Integer year = Integer.parseInt( yearStr );
		return year;
	}

	protected String getMakeModelGrouping(HttpServletRequest request) {
		// Get the Make Model Grouping ID
		String mmgId = request.getParameter(MMG_PARAM);
		if( StringUtils.isEmpty(mmgId) ) {
			throw new RuntimeException( "mmg is a required parameter, makeModelGroupingId" );
		}
		return mmgId;
	}

	public PingService getPingService() {
		return pingService;
	}
		
	public void setPingService(PingService pingService) {
		this.pingService = pingService;
	}
	
}
