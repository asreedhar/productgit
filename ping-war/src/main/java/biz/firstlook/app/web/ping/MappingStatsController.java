package biz.firstlook.app.web.ping;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.ping.model.MappingStat;
import biz.firstlook.ping.persist.direct.PingStatsDirect;

public class MappingStatsController implements Controller {
	
	private static Logger log = Logger.getLogger( MappingStatsController.class );
	
	private PingStatsDirect pingStatsDirect;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug( "Generating Mapping Statistics" );
		List<MappingStat> mappingStats = pingStatsDirect.getMappingStats();
		
		return new ModelAndView("mappingStats","mappingStats", mappingStats);
	}

	public PingStatsDirect getPingStatsDirect() {
		return pingStatsDirect;
	}

	public void setPingStatsDirect(PingStatsDirect pingStatsDirect) {
		this.pingStatsDirect = pingStatsDirect;
	}

}
