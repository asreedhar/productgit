package biz.firstlook.app.web.ping;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.api.ping.PingService;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Provider;

public class MapCodesController implements Controller {
	
	private static Logger log = Logger.getLogger( MapCodesController.class );
	
	private PingService pingService;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String providerCode = request.getParameter("provider-code");
		
		if( StringUtils.isEmpty(providerCode) ) {
			throw new RuntimeException( "provider-code is a required parameter for the price controller" );
		}
		
		final Provider provider = pingService.getProvider(providerCode);
		
		log.info( "Retrieving Model Codes for Provider");
		List<ModelCode> models = pingService.getModelCodes( provider );
		for( ModelCode model : models ) {
			log.info( "Mapping model Code: " + model );
			pingService.mapModelCode( model );
		}

		return new ModelAndView("mapCodes","models", models);
	}

	public PingService getPingService() {
		return pingService;
	}

	public void setPingService(PingService pingService) {
		this.pingService = pingService;
	}
}
