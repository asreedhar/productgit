package biz.firstlook.app.web.admin.action.dealer;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.DMSExportPreference;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.imt.DealerDao;

public class LoadDMSWritebackPreferences extends Action {

	private DealerDao dealerDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Dealer dealer = dealerDao.findById( Integer.parseInt(request.getParameter("id")) );
		DMSExportPreference dmsExportDisplayPreference = null;
		if (request.getParameter("exportType") != null && request.getParameter("exportType").trim().length() > 0) {
			Integer exportType = new Integer(request.getParameter("exportType"));
			Set<DMSExportPreference> dmsPrefs = dealer.getDmsExportPreferences();
			for(DMSExportPreference dmsPref : dmsPrefs) {
				if (dmsPref.getDmsExportId().intValue() == exportType.intValue()) {
					dmsExportDisplayPreference = dmsPref;
					break;
				}
			}
		}
		request.setAttribute("dealer", dealer);
		request.setAttribute("isEdit", Boolean.TRUE);
		
		if ( dmsExportDisplayPreference != null) {
			DynaActionForm dynaForm = (DynaActionForm)form;
			dynaForm.set("isActive", dmsExportDisplayPreference.isActive());
			dynaForm.set("clientSystemId", dmsExportDisplayPreference.getClientSystemId());
			dynaForm.set("ipAddress", dmsExportDisplayPreference.getIpAddress());
			dynaForm.set("dialUpPhone1", dmsExportDisplayPreference.getDialUpPhone1());
			dynaForm.set("dialUpPhone2", dmsExportDisplayPreference.getDialUpPhone2());
			dynaForm.set("login", dmsExportDisplayPreference.getLogin());
			dynaForm.set("password", dmsExportDisplayPreference.getPassword());
			dynaForm.set("accountLogon", dmsExportDisplayPreference.getAccountLogon());
			dynaForm.set("financeLogon", dmsExportDisplayPreference.getFinanceLogon());
			dynaForm.set("areaNumber", dmsExportDisplayPreference.getAreaNumber());
			dynaForm.set("storeNumber", dmsExportDisplayPreference.getStoreNumber());
			dynaForm.set("branchNumber", dmsExportDisplayPreference.getBranchNumber());
			dynaForm.set("reynoldsDealerNo", dmsExportDisplayPreference.getReynoldsDealerNo());
			dynaForm.set("frequencyId", dmsExportDisplayPreference.getFrequencyId());
			dynaForm.set("dailyExportHourOfDay", dmsExportDisplayPreference.getDailyExportHourOfDay());
			dynaForm.set("dealerName", dmsExportDisplayPreference.getDealerName());
			dynaForm.set("dealerPhoneOffice", dmsExportDisplayPreference.getDealerPhoneOffice());
			dynaForm.set("dealerPhoneCell", dmsExportDisplayPreference.getDealerPhoneCell());
			dynaForm.set("dealerEmail", dmsExportDisplayPreference.getDealerEmail());
		}		
		return mapping.findForward("success");
	}
	
	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}

}
