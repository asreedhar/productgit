package biz.firstlook.app.web.admin.action.corporation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Corporation;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.ref.BusinessUnitType;
import biz.firstlook.persist.imt.DealerGroupDao;

@SuppressWarnings("unchecked")
public class GroupCreate extends Load {

	private DealerGroupDao dealerGroupDao;
	
	@Override
	protected void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		
		
		String code = request.getParameter( "code" );
		String shortName = request.getParameter( "shortName" );
		String name = request.getParameter( "name" );
		
		String returnMessage = null;
		
		if(exists(code) && code.length() > Dealer.MAX_CODE_LEN){
			returnMessage = "Code length must be less than " + Dealer.MAX_CODE_LEN;
		}
		else if(exists(shortName) && shortName.length() > Dealer.MAX_SHORT_NAME_LEN){
			returnMessage = "Short Name length must be less than " + Dealer.MAX_SHORT_NAME_LEN;
		}
		else if(exists(name) && name.length() > Dealer.MAX_NAME_LEN){
			returnMessage = "Name length must be less than " + Dealer.MAX_NAME_LEN;
		}
		else if(!exists(name)){
			returnMessage = "Must have a Business Unit Name";
		}
		else{
			DealerGroup group = new DealerGroup();
			
			Corporation corporation = (Corporation) request.getAttribute( getResultAttribute() );
			group.setCorporation( corporation );
			
			group.setCode( request.getParameter( "code" ) );
			group.setShortName( request.getParameter( "shortName" ) );
			group.setName( request.getParameter( "name" ) );
			group.setActive(true);
			group.setType( BusinessUnitType.DealerGroup );
			
			DealerGroup savedGroup = dealerGroupDao.makePersistent( group );
			corporation.getGroups().add( savedGroup );
			getGenericDAO().makePersistent( corporation );
		}
		
		request.setAttribute("createGroup", returnMessage);
	}

	private boolean exists(String check) {
		boolean result = false;
		if (check != null && check.length() > 0) {
			result = true;
		}
		return result;
	}
	
	public DealerGroupDao getDealerGroupDao() {
		return dealerGroupDao;
	}

	public void setDealerGroupDao(DealerGroupDao dealerGroupDao) {
		this.dealerGroupDao = dealerGroupDao;
	}

}
