package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import biz.firstlook.app.web.common.struts.util.BreadCrumbUtil;
import biz.firstlook.model.imt.Dealer;

/**
 * This action backs /dealer/dynamic/profile URL.
 * 
 * @author bfung
 */
public class LoadProfile extends LoadDealer {

    // 6:18pm before night of push! properties files please.
    private static final String VIP_HOME = "/IMT/StoreAction.go?dealerId=";
    private static final String EDGE_HOME = "/IMT/DealerHomeSetCurrentDealerAction.go?currentDealerId=";
    private static final String FIRSTLOOK_HOME = "/IMT/DealerHomeSetCurrentDealerAction.go?currentDealerId=";
    private static final String DEALERS_RESOURCES_HOME = "/PrivateLabel/StoreAction.go?dealerId=";

    private BreadCrumbUtil<Integer, LabelValueBean> visitedDealers = new BreadCrumbUtil<Integer, LabelValueBean>();

    @Override
    protected void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response,
	    Dealer dealer) throws ServletException {

	visitedDealers.put(request, dealer.getId(), new LabelValueBean(dealer
		.getName(), dealer.getId().toString()));
    }

    @Override
    protected ActionForward findForward(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, Dealer dealer) throws ServletException {
	String home = request.getParameter("home");
	String url = null;
	if (home != null && home.equalsIgnoreCase("home")) {
	    switch (dealer.getPreference().getProgramType()) {
	    case VIP:
		url = VIP_HOME;
		break;
	    case DEALERS_RESOURCES:
		url = DEALERS_RESOURCES_HOME;
		break;
	    case FIRSTLOOK:
		url = FIRSTLOOK_HOME;
		break;
	    default:
		url = EDGE_HOME;
	    }
	    request.setAttribute("url", url);
	    request.setAttribute("dealerId", dealer.getId());
	    return mapping.findForward("redirect");
	} else {
	    return mapping.findForward(getSuccessForward());
	}
    }
}
