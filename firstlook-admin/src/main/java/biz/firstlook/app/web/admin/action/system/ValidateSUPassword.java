package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ValidateSUPassword extends Action {

	private static final String password = "wr1tedms";
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		if( password.equals(request.getParameter("suPassword"))) {
			response.addHeader("isSuperUser", "true");
		}
		
		return null;
	}

	
}
