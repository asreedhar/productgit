package biz.firstlook.app.web.common.struts.util;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A bounded LRUCache implementation
 * 
 * @author bfung
 * 
 * @param <K>
 *                key for cache entries
 * @param <V>
 *                the type of cache entry
 */
public class LRUCache<K extends Serializable, V extends Serializable>
	implements Serializable {

    private static final long serialVersionUID = -5839701889086205088L;

    private final Map<K, V> map;
    private final int maxCapacity;

    public LRUCache(final int maxCapacity) {
	this.maxCapacity = maxCapacity;
	map = Collections.synchronizedMap(new LinkedHashMap<K, V>() {

	    private static final long serialVersionUID = 8971237496850712754L;

	    @Override
	    protected boolean removeEldestEntry(Entry<K, V> eldest) {
		return size() > maxCapacity;
	    }
	});
    }

    public V put(K key, V value) {
	//reset the list order
	if(map.containsKey(key)) {
	    map.remove(key);
	}
	return map.put(key, value);
    }

    public V get(K key) {
	return map.get(key);
    }

    public int size() {
	return map.size();
    }

    public int maxCapacity() {
	return maxCapacity;
    }

    /**
     * Returns a copy of entries in the cache in least recently used order.
     * 
     * @return a Queue of the entries. The head of the queue was the last
     *         inserted element.
     */
    public List<V> getEntries() {
	LinkedList<V> view = new LinkedList<V>(map.values());
	Collections.reverse(view);
	return view;
    }
}
