package biz.firstlook.app.web.admin.action.group;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.admin.action.client.DealerLocationClient;
import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.persist.imt.DealerDao;



@SuppressWarnings("unchecked")
public class DealerCreate extends Load {

	private DealerDao dealerDao;
	
	@Override
	protected void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
	
		String code = request.getParameter( "code" );
		String shortName = request.getParameter( "shortName" );
		String name = request.getParameter( "name" );
		String zip = request.getParameter( "zip" ); 
		BigDecimal latitude = BigDecimal.ZERO,longitude = BigDecimal.ZERO;
		String returnMessage = null;
		
		DealerLocationClient client = new DealerLocationClient();
		
		if(exists(code) && code.length() > Dealer.MAX_CODE_LEN){
			returnMessage = "Code length must be less than " + Dealer.MAX_CODE_LEN;
		}
		else if(exists(shortName) && shortName.length() > Dealer.MAX_SHORT_NAME_LEN){
			returnMessage = "Short Name length must be less than " + Dealer.MAX_SHORT_NAME_LEN;
		}
		else if(exists(name) && name.length() > Dealer.MAX_NAME_LEN){
			returnMessage = "Name length must be less than " + Dealer.MAX_NAME_LEN;
		}
		else if(!exists(name)){
			returnMessage = "Must have a Business Unit Name";
		}
		else if(exists(zip) && zip.length() > Dealer.MAX_ZIP_LEN){
			returnMessage = "Zip Code length must be less than " +  Dealer.MAX_ZIP_LEN;
		}
		else{
			Dealer dealer = new Dealer();
			DealerGroup dealerGroup = (DealerGroup) request.getAttribute( getResultAttribute() );
			dealer.setDealerGroup( dealerGroup );
			dealer.setCode( code );
			dealer.setShortName( shortName);
			dealer.setName( shortName );
			dealer.setZip(zip);
			dealer.setActive(false);
			try {
				String address=null;
				Map<String,BigDecimal> dealerGeoCode = client.getGeoCodeFromGoogle(zip,address);
				latitude = dealerGeoCode.get("lat");
				longitude =dealerGeoCode.get("long");
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Google API for MAPS encountered a problem " + e.getMessage());
			}
			dealer.setLatitude(latitude);
			dealer.setLongitude(longitude);
			Dealer savedDealer = dealerDao.makePersistent( dealer );
			dealerGroup.getDealers().add( savedDealer );
			getGenericDAO().makePersistent( dealerGroup );
		}
		request.setAttribute("createDealer", returnMessage);
	}

	private boolean exists(String check) {
		boolean result = false;
		if (check != null && check.length() > 0) {
			result = true;
		}
		return result;
	}
	
	
	public DealerDao getDealerDao() {
		return dealerDao;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}

}
