package biz.firstlook.app.web.common.struts.util;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.DynaActionForm;

public class PersistFormUtil {

	private static Logger log = Logger.getLogger(PersistFormUtil.class);
	
	@SuppressWarnings("unchecked")
	public static void copyFormToBean(ActionForm form, Object bean, String targetProperty, String paramName, Map nullValueMap) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ServletException, InstantiationException {
		DynaActionForm dynaform = (DynaActionForm)form;
		if( !StringUtils.isEmpty( targetProperty ) ) {
			Object property = PropertyUtils.getProperty( bean, targetProperty );
			if( property == null ) {
				Object newValue;
					newValue = PropertyUtils.getPropertyType( bean, targetProperty ).newInstance();
					PropertyUtils.setProperty( bean, targetProperty, newValue);
				property = newValue;				
			}
			bean = property;
		}
		
    	Map<String,Object> properties = new HashMap<String,Object>( PropertyUtils.describe(dynaform) );
    	if( !StringUtils.isEmpty( paramName ) ) {
        	properties.remove(paramName);
    	}
    	for( String property : properties.keySet() ) {
    		if( PropertyUtils.isWriteable( bean, property ) && PropertyUtils.isReadable( bean, property ) ) {
    			Object formValue = properties.get( property );
        		Class formType = PropertyUtils.getPropertyType( dynaform, property );
	    		Class destinationType = PropertyUtils.getPropertyType( bean, property );
	    		
	    		formValue = catchNullValues(nullValueMap, property, formValue);
    			
    			if( destinationType.isEnum() ) {
    				if( formValue instanceof String ) {
    					PropertyUtils.setProperty( bean, property, Enum.valueOf( destinationType, (String) formValue ) );
    				} else {
    					log.error( "Cannot convert formValue " + formValue + " to Enum value for property: " + property );
    					throw new ServletException( "Cannot convert formValue " + formValue + " to Enum value for property: " + property );
    				}
	    		} else if( destinationType.isAssignableFrom( Date.class ) ) {
	    			if(StringUtils.isNotBlank((String)formValue)) {
		    			SimpleDateFormat sdf = new SimpleDateFormat( "M/d/yyyy" );
		    			Date date;
						try {
							date = sdf.parse( (String) formValue );
						} catch (ParseException e) {
							log.error( "Error parsing date format" );
							// note: if date is a required field, a javascript check is done to make sure it is 
							// populated, therefore, if we get here on a null date, we know it is a nullable field
							date = null;
						}
						PropertyUtils.setProperty( bean, property, date );
					// NK
					// Special case for DealerUpgrades. They CAN have a blank endDate. DM/Accounting use this as a flag.
					// If a dealership has an endDate, it means this upgrade has been given to the dealership as a temporary upgrade.
					// If there is no endDate, it means the dealership has purchased the upgrade and should be billed.
					// This is not how Eng would recommend billing being done, but we weren't asked.
	    			} else if (dynaform.getDynaClass().getName().equalsIgnoreCase("dealer_upgrade") &&  property.equalsIgnoreCase("endDate") ) {
	    				log.info( MessageFormat.format("End date is being written as null for bean {0}.", bean.getClass().getSimpleName()) );
	    				PropertyUtils.setProperty( bean, property, null);
	    			} else {
	    				log.info( MessageFormat.format("A date value was expected but the field {0} for bean {1} was blank.", property, bean.getClass().getSimpleName()) );	    				
	    			}
	    		} else if( destinationType.isArray() && destinationType.getComponentType().isEnum() ) {

	    			String[] formValues = ((String[]) formValue)[0].split(",");
	    			Enum[] enumArray = (Enum[]) Array.newInstance( destinationType.getComponentType(), formValues.length );
	    			int i = 0;
	    			for( String fValue : formValues ) {
	    				enumArray[i] = Enum.valueOf( destinationType.getComponentType(), (String) fValue );
	    				i++;
	    			}
	    			// TODO: This requires that there be a setter which takes an array of Enum
	    			PropertyUtils.setProperty( bean, property, enumArray );

	    		} else if( formValue != null && !destinationType.isAssignableFrom( formValue.getClass() ) ) {
    				Object instance;
					try {
						instance = destinationType.newInstance();
					} catch (Exception e) {
						log.error( "Error instantiating instance of type: " + destinationType.getName() );
						throw new ServletException( "Error instantiating instance of type: " + destinationType.getName() );
					}
    				BeanUtils.setProperty( instance, "id", formValue );
    				PropertyUtils.setProperty( bean, property, instance );
	    		} else if( formType.isAssignableFrom( Integer.class ) && !destinationType.isAssignableFrom(Integer.class) ) { 
	    			
	    			try {	
	    				Object instance = destinationType.newInstance();
	    				PropertyUtils.setProperty( instance, "id", formValue );
	    				PropertyUtils.setProperty( bean, property, instance );
	    			} catch( Exception e ) {
	    				log.error( "Problem setting id of child object" );
	    				throw new ServletException( "Problem setting id of child object" );
	    			}
	    			
    			} else if( formValue == null && destinationType.isAssignableFrom( Boolean.class ) ) {
	    			PropertyUtils.setProperty( bean, property, Boolean.FALSE );
	    		} else {
	    			PropertyUtils.setProperty( bean, property, formValue );
	    		}
    		}
    	}
	}

	private static Object catchNullValues(Map nullValueMap, String property, Object formValue) {
		if( nullValueMap != null && nullValueMap.containsKey( property ) ) {
			Object nullValue = nullValueMap.get( property );
			if( formValue.equals( nullValue ) ) {
				formValue = null;
			}
		}
		return formValue;
	}

}
