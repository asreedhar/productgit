package biz.firstlook.app.web.admin.action.client;

public class Geometry
{
    private Bounds bounds;

    private Viewport viewport;

    private String locationType;

    private Location location;

    public Bounds getBounds ()
    {
        return bounds;
    }

    public void setBounds (Bounds bounds)
    {
        this.bounds = bounds;
    }

    public Viewport getViewport ()
    {
        return viewport;
    }

    public void setViewport (Viewport viewport)
    {
        this.viewport = viewport;
    }

    public String getLocationType ()
    {
        return locationType;
    }

    public void setLocationType (String locationType)
    {
        this.locationType = locationType;
    }

    public Location getLocation ()
    {
        return location;
    }

    public void setLocation (Location location)
    {
        this.location = location;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bounds = "+bounds+", viewport = "+viewport+", location_type = "+locationType+", location = "+location+"]";
    }
}