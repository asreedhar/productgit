package biz.firstlook.app.web.admin.action.dealer.helper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.util.LabelValueBean;

import biz.firstlook.app.web.common.struts.action.helper.AfterLoadEvent;
import biz.firstlook.model.imt.ref.DatabaseEnum;
import biz.firstlook.persist.imt.NaaaAuctionDao;

public class NaaaOptionsAfterLoadEvent implements AfterLoadEvent {

    private static NaaaOptionsAfterLoadEvent instance; 
    
    public static synchronized NaaaOptionsAfterLoadEvent getInstance() {
	if( instance == null ) {
	    instance = new NaaaOptionsAfterLoadEvent();
	}
	return instance;
    }
    
    private NaaaOptionsAfterLoadEvent() {
	super();
    }

    private NaaaAuctionDao naaaAuctionDao;

    public void handleEvent(ActionForm form, HttpServletRequest request)
	    throws ServletException {

	List<LabelValueBean> regionOptions = adapt(naaaAuctionDao.getRegions());
	List<LabelValueBean> timePeriodOptions = adapt(naaaAuctionDao
		.getTimePeriods());

	request.setAttribute("naaaRegions", regionOptions);
	request.setAttribute("naaaTimePeriods", timePeriodOptions);
    }

    private List<LabelValueBean> adapt(List<DatabaseEnum> dataEnums) {
	List<LabelValueBean> options = new ArrayList<LabelValueBean>();
	for (DatabaseEnum dataEnum : dataEnums) {
	    options.add(new LabelValueBean(dataEnum.getDescription(), dataEnum
		    .getId().toString()));
	}
	return options;
    }

    public void setNaaaAuctionDao(NaaaAuctionDao naaaAuctionDao) {
	this.naaaAuctionDao = naaaAuctionDao;
    }
}
