package biz.firstlook.app.web.admin.action.corporation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Corporation;
import biz.firstlook.model.imt.ref.BusinessUnitType;
import biz.firstlook.persist.imt.CorporationDao;

public class CorporationCreate extends Action {

	private CorporationDao corporationDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Corporation corporation = new Corporation();
		
		corporation.setCode( request.getParameter( "code" ) );
		corporation.setShortName( request.getParameter( "shortName" ) );
		corporation.setName( request.getParameter( "name" ) );
		corporation.setActive(true);
		corporation.setType( BusinessUnitType.Corporate );
		corporation.setPhone( request.getParameter( "phone" ) );
		corporation.setFax( request.getParameter( "fax" ) );
		
		Corporation savedCorporation = corporationDao.makePersistent( corporation );
		request.setAttribute( "corporation", savedCorporation );
		return mapping.findForward("success");
	}

	public CorporationDao getCorporationDao() {
		return corporationDao;
	}

	public void setCorporationDao(CorporationDao corporationDao) {
		this.corporationDao = corporationDao;
	}

}