package biz.firstlook.app.web.admin.action.group;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.DealerTransferPriceUnitCostRule;
import biz.firstlook.model.imt.TransferPriceInventoryAgeRange;
import biz.firstlook.persist.imt.TransferPriceInventoryAgeRangeDao;

public class DealerTransferPriceUnitCostRuleUpdate extends AbstractTransferPriceAction {

	private TransferPriceInventoryAgeRangeDao transferPriceInventoryAgeRangeDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, DealerGroup group, List<Dealer> dealers ) 
			throws Exception {		
		
		Integer dealerId = Integer.valueOf(request.getParameter("dealerId"));
		Integer transferPriceUnitCostValueId = Integer.valueOf(request.getParameter("transferPriceUnitCostValueId"));
		Integer transferPriceInventoryAgeRangeId = null;
		try {
			transferPriceInventoryAgeRangeId = Integer.valueOf(request.getParameter("transferPriceInventoryAgeRangeId"));
		} catch (NumberFormatException e) {
			//do nothing;, null is a valid value.
		}
		
		Dealer dealer = group.getDealer(dealerId);
		
		Set<DealerTransferPriceUnitCostRule> rules = dealer.getDealerTransferPriceUnitCostRules();
		
		TransferPriceInventoryAgeRange newValue = null;
		if(transferPriceInventoryAgeRangeId != null) {
			newValue = transferPriceInventoryAgeRangeDao.findById(transferPriceInventoryAgeRangeId);
		}
		
		DealerTransferPriceUnitCostRule theRuleToUpdate = null;
		for(DealerTransferPriceUnitCostRule rule : rules) {
			if(rule.getTransferPriceUnitCostValue().getId().equals(transferPriceUnitCostValueId)) {
				theRuleToUpdate = rule;
			}
		}
		
		if(theRuleToUpdate != null) {
			theRuleToUpdate.setTransferPriceInventoryAgeRange(newValue);
		}
		
		dealerGroupDao.makePersistent(group);
		
		request.setAttribute("message", "saved");
		
		return mapping.findForward("success");
	}

	public void setTransferPriceInventoryAgeRangeDao(
			TransferPriceInventoryAgeRangeDao transferPriceInventoryAgeRangeDao) {
		this.transferPriceInventoryAgeRangeDao = transferPriceInventoryAgeRangeDao;
	}
	
}
