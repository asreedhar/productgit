package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.Franchise;

public class RemoveFranchise extends LoadDealer {

    @Override
    public void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response,
	    Dealer dealer) throws ServletException {
	String franchiseIdStr = (String) request.getParameter("franchiseId");
	Integer franchiseId = new Integer(franchiseIdStr);

	Franchise toRemove = null;
	for (Franchise franchise : dealer.getFranchises()) {
	    if (franchise.getId().equals(franchiseId)) {
		toRemove = franchise;
	    }
	}
	if (toRemove != null) {
	    dealer.getFranchises().remove(toRemove);
	}
	getDealerDao().makePersistent(dealer);
    }

}
