package biz.firstlook.app.web.admin.action.client;

public class Results
{
    private String place_id;

    private AddressComponents[] addressComponents;

    private String formattedAddress;

    private String[] types;

    private Geometry geometry;

    public String getPlace_id ()
    {
        return place_id;
    }

    public void setPlace_id (String place_id)
    {
        this.place_id = place_id;
    }

    public AddressComponents[] getAddressComponents ()
    {
        return addressComponents;
    }

    public void setAddress_components (AddressComponents[] addressComponents)
    {
        this.addressComponents = addressComponents;
    }

    public String getFormattedAddress ()
    {
        return formattedAddress;
    }

    public void setFormatted_address (String formattedAddress)
    {
        this.formattedAddress = formattedAddress;
    }

    public String[] getTypes ()
    {
        return types;
    }

    public void setTypes (String[] types)
    {
        this.types = types;
    }

    public Geometry getGeometry ()
    {
        return geometry;
    }

    public void setGeometry (Geometry geometry)
    {
        this.geometry = geometry;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [place_id = "+place_id+", address_components = "+addressComponents+", formatted_address = "+formattedAddress+", types = "+types+", geometry = "+geometry+"]";
    }
}