package biz.firstlook.app.web.admin.action.dealer;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.BusinessUnitMarketDealer;
import biz.firstlook.model.imt.Dealer;

public class MarketMappingUpdateForm extends MarketMappingPopulateForm {

    @Override
    protected void afterLoad(HttpServletRequest request, DynaActionForm form, Dealer dealer)
	    throws ServletException {

	List<String> states = marketMappingDao.findStates();
	// Since this fl-admin framework couples hibernate mapped entities with
	// the forms, I'm setting data lists just to the request as opposed to
	// adding more transient fields on the mapped objects.
	request.setAttribute("states", states
		.toArray(new String[states.size()]));
	try {
	    String selectedState = form.getString("selectedState");
	    int index = states.indexOf(dealer.getState());
	    if (index > -1) {
		selectedState = states.get(index);
	    }

	    populateForm(request, form, states, selectedState, form
		    .getString("selectedCounty"), (Integer) form
		    .get("dealerNumber"));

	    // update the transient fields
	    BusinessUnitMarketDealer marketDealer = dealer
		    .getBusinessUnitMarketDealer();
	    if (marketDealer == null) {
		marketDealer = new BusinessUnitMarketDealer();
		marketDealer.setDealer(dealer);
		dealer.setBusinessUnitMarketDealer(marketDealer);
	    }
	    marketDealer
		    .setDealerNumber((Integer) form.get("dealerNumber"));
	    marketDealer.setSelectedState(form.getString("selectedState"));
	    marketDealer
		    .setSelectedCounty(form.getString("selectedCounty"));
	} catch (IllegalAccessException e1) {
	    throw new ServletException(e1);
	} catch (InvocationTargetException e1) {
	    throw new ServletException(e1);
	} catch (NoSuchMethodException e1) {
	    throw new ServletException(e1);
	}
    }
}

