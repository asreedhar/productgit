package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.GlobalParam;
import biz.firstlook.model.imt.ref.ExportType;
import biz.firstlook.persist.imt.GlobalParamDao;

public class LoadExportSettings extends Action {

	private GlobalParamDao globalParamDao;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String exportTypeName = request.getParameter("exportType");
		ExportType exportType = ExportType.getExportTypeByName( exportTypeName );
		
		switch (exportType) { 
			case EBIZ:
				GlobalParam eBizIdParam = globalParamDao.forName(GlobalParam.PARAM_EBIZID);
				GlobalParam eBizAuthParam = globalParamDao.forName(GlobalParam.PARAM_EBIZAUTH);
				request.setAttribute("eBizId", (eBizIdParam == null ? "" : eBizIdParam.getValue()) );
				request.setAttribute("eBizAuth", (eBizAuthParam == null ? "" : eBizAuthParam.getValue()) );
				break;
			case SIS:
				GlobalParam sisParam = globalParamDao.forName(GlobalParam.PARAM_SIS);
				request.setAttribute(GlobalParam.PARAM_SIS, (sisParam == null ? "" : sisParam.getValue()) );
				break;
			case REY_DIRECT:
				GlobalParam reyDirect = globalParamDao.forName(GlobalParam.PARAM_REY_DIRECT);
				request.setAttribute(GlobalParam.PARAM_REY_DIRECT, (reyDirect == null ? "" : reyDirect.getValue()) );
				break;
		}
		
		return mapping.findForward("success");
	}

	public GlobalParamDao getGlobalParamDao() {
		return globalParamDao;
	}

	public void setGlobalParamDao(GlobalParamDao globalParamDao) {
		this.globalParamDao = globalParamDao;
	}
	
	
	
}
