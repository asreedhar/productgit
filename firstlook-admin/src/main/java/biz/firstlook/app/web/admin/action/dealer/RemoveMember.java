package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.Member;

public class RemoveMember extends LoadDealer {

	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, Dealer dealer) throws ServletException {
		String memberIdStr = (String) request.getParameter( "memberId" );
		Integer memberId = new Integer( memberIdStr );
		Member toRemove = null;
		for( Member member : dealer.getMembers() ) {
			if( member.getId().equals( memberId ) ) {
				toRemove = member;
			}
		}
		if( toRemove != null ) {
			dealer.getMembers().remove( toRemove );
		}
		getDealerDao().makePersistent( dealer );
		return;
	}

}
