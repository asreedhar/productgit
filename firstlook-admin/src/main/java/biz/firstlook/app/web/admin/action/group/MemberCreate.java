package biz.firstlook.app.web.admin.action.group;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.app.web.admin.action.member.BaseMemberCreate;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.Member;

public class MemberCreate extends BaseMemberCreate {

	@SuppressWarnings("unchecked")
	@Override
	protected void associate( HttpServletRequest request, Member member ) {
		DealerGroup group = (DealerGroup) request.getAttribute(getResultAttribute());
		
		member.setDefaultDealerGroupId(group.getId());
		group.add( member );
		//need this line because members don't cascade from groups, but dealers
		getGenericDAO().makePersistent( group ); 
	}
}
