package biz.firstlook.app.web.admin.action.dealer;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.BusinessUnitMarketDealer;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.MarketMapping;

public class MarketMappingInitForm extends MarketMappingPopulateForm {

    @Override
    protected void afterLoad(HttpServletRequest request, DynaActionForm form,
	    Dealer dealer) throws ServletException {

	List<String> states = marketMappingDao.findStates();
	request.setAttribute("states", states
		.toArray(new String[states.size()]));

	BusinessUnitMarketDealer marketDealer = dealer
		.getBusinessUnitMarketDealer();
	if (marketDealer != null) {
	    MarketMapping marketMapping = marketMappingDao.find(marketDealer
		    .getDealerNumber());

	    if (marketMapping != null) {
		try {
		    populateForm(request, form, states, marketMapping
			    .getState(), marketMapping.getCounty(),
			    marketDealer.getDealerNumber());
		} catch (IllegalAccessException e) {
		    throw new ServletException(e);
		} catch (InvocationTargetException e) {
		    throw new ServletException(e);
		} catch (NoSuchMethodException e) {
		    throw new ServletException(e);
		}
		marketDealer.setSelectedState(marketMapping.getState());
		marketDealer.setSelectedCounty(marketMapping.getCounty());
		marketDealer.setMarketDealerName(marketMapping.getName());
	    } else {
		try {
		    populateForm(request, form, states, null, null,
			    marketDealer.getDealerNumber());
		} catch (IllegalAccessException e) {
		    throw new ServletException(e);
		} catch (InvocationTargetException e) {
		    throw new ServletException(e);
		} catch (NoSuchMethodException e) {
		    throw new ServletException(e);
		}
	    }
	} else {
	    try {
		populateForm(request, form, states, null, null, null);
	    } catch (IllegalAccessException e) {
		throw new ServletException(e);
	    } catch (InvocationTargetException e) {
		throw new ServletException(e);
	    } catch (NoSuchMethodException e) {
		throw new ServletException(e);
	    }
	}
    }
}
