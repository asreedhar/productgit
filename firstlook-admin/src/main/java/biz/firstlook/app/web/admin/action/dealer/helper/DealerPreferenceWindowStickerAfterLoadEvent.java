package biz.firstlook.app.web.admin.action.dealer.helper;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.util.LabelValueBean;

import biz.firstlook.app.web.common.struts.action.helper.AfterLoadEvent;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreferenceWindowSticker;
import biz.firstlook.model.imt.ref.WindowStickerBuyersGuideTemplate;
import biz.firstlook.model.imt.ref.WindowStickerTemplate;
import biz.firstlook.persist.imt.DealerPreferenceDao;

public class DealerPreferenceWindowStickerAfterLoadEvent implements
		AfterLoadEvent {

	private static final DealerPreferenceWindowStickerAfterLoadEvent instance = new DealerPreferenceWindowStickerAfterLoadEvent();

	public static DealerPreferenceWindowStickerAfterLoadEvent getInstance() {
		return instance;
	}

	private DealerPreferenceDao dealerPreferenceDao;

	private DealerPreferenceWindowStickerAfterLoadEvent() {
		/* force factory methods */
	}

	public void handleEvent(ActionForm form, HttpServletRequest request)
			throws ServletException {
		Dealer dealer = (Dealer) request.getAttribute("dealer"); // hack

		DealerPreferenceWindowSticker stickerPref = dealerPreferenceDao
				.findWindowStickerPrefs(dealer.getId());
		request.setAttribute("windowStickerPref", stickerPref);
		if(form != null) {
			try {
				PropertyUtils.setProperty(form, "windowStickerTemplateId", stickerPref.getWindowStickerTemplateId());
				PropertyUtils.setProperty(form, "buyersGuideTemplateId", stickerPref.getBuyersGuideTemplateId());
			} catch (Exception e) {
				//expected that these properties exist, since i bother to retrieve them from the database.
				throw new ServletException(e);
			}
		}

		List<LabelValueBean> windowStickerTemplates = adaptWindowSticker(EnumSet.allOf(WindowStickerTemplate.class));
		List<LabelValueBean> buyerGuideTemplates = adaptBuyersGuide(EnumSet.allOf(WindowStickerBuyersGuideTemplate.class));

		request.setAttribute("windowStickerTemplates", windowStickerTemplates);
		request.setAttribute("buyerGuideTemplates", buyerGuideTemplates);
	}

	private static List<LabelValueBean> adaptWindowSticker(EnumSet<WindowStickerTemplate> es) {
		List<LabelValueBean> options = new ArrayList<LabelValueBean>();
		for (WindowStickerTemplate template : es) {
			options.add(new LabelValueBean(template.getName(), template
					.getId().toString()));
		}
		return options;
	}
	
	private static List<LabelValueBean> adaptBuyersGuide(EnumSet<WindowStickerBuyersGuideTemplate> es) {
		List<LabelValueBean> options = new ArrayList<LabelValueBean>();
		for (WindowStickerBuyersGuideTemplate template : es) {
			options.add(new LabelValueBean(template.name(), template
					.getId().toString()));
		}
		return options;
	}

	public void setDealerPreferenceDao(DealerPreferenceDao dealerPreferenceDao) {
		this.dealerPreferenceDao = dealerPreferenceDao;
	}
}
