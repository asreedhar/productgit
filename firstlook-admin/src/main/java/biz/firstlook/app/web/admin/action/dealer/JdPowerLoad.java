package biz.firstlook.app.web.admin.action.dealer;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.ref.JdPowerRegion;
import biz.firstlook.persist.imt.JdPowerRegionDao;

public class JdPowerLoad extends LoadDealer
{

private JdPowerRegionDao jdPowerRegionDao;

@Override
protected void afterLoad(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response, Dealer dealer)
    throws ServletException {
	
	List<JdPowerRegion> regions = jdPowerRegionDao.findAll();
	request.setAttribute( "regions", regions );
}

public JdPowerRegionDao getJdPowerRegionDao()
{
	return jdPowerRegionDao;
}

public void setJdPowerRegionDao( JdPowerRegionDao jdPowerRegionDao )
{
	this.jdPowerRegionDao = jdPowerRegionDao;
}

}
