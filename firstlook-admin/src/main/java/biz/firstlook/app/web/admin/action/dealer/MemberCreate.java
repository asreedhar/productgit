package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.app.web.admin.action.member.BaseMemberCreate;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.Member;

public class MemberCreate extends BaseMemberCreate {

	@Override
	protected void associate( HttpServletRequest request, Member member ) {
		Dealer dealer = (Dealer) request
		.getAttribute(getResultAttribute());
		DealerGroup group = dealer.getDealerGroup();
		
		if( group != null ) {
			member.setDefaultDealerGroupId(group.getId());
			String allDealers = request.getParameter("allDealers");
			if (allDealers != null && allDealers.equals("true")) {
				group.add( member );
			} else {
				dealer.getMembers().add(member);
			}
		}
	}
}
