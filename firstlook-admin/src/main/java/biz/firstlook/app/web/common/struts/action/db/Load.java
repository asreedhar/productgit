package biz.firstlook.app.web.common.struts.action.db;

import java.io.Serializable;

import biz.firstlook.persist.common.GenericDAO;

public class Load extends biz.firstlook.app.web.common.struts.action.Load {
	
	private GenericDAO genericDAO;
	
	@SuppressWarnings("unchecked")
	@Override
	protected Object loadObject(Serializable id) {
    	Object result = genericDAO.findById( id, false );
    	return result;
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}
	
	
}