package biz.firstlook.app.web.common.struts.action.db;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.app.web.admin.action.client.DealerLocationClient;
import biz.firstlook.app.web.common.struts.util.PersistFormUtil;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreference;
import biz.firstlook.model.imt.DealerPreferenceWindowSticker;
import biz.firstlook.model.imt.SalesChannel;
import biz.firstlook.persist.imt.DealerPreferenceDao;
import biz.firstlook.persist.imt.SalesChannelDao;
import biz.firstlook.persist.imt.direct.jdbc.GuidebookCorrectionForMaxStoredProcedure;


public class DealerPersistForm extends PersistForm {
	
	private static Logger log = Logger.getLogger( DealerPersistForm.class );

	private GuidebookCorrectionForMaxStoredProcedure fixGuideBookDirect;
	private DealerPreferenceDao dealerPreferenceDao;
	private SalesChannelDao salesChannelDao;
	
	@Override
	protected void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	Dealer bean = (Dealer)request.getAttribute( getResultAttribute() );
    	try {
			PersistFormUtil.copyFormToBean(form, bean, getTargetProperty(), getParamName(), getNullValueMap());
		} catch (InstantiationException e) {
			log.debug( "Attempt to illegally instantiate an instance of a property or object in PersistForm", e );
			throw new ServletException( "Error trying to fetch property or access object", e );
		} catch (IllegalAccessException e) {
			log.debug( "Attempt to illegally access a property or object in PersistForm", e );
			throw new ServletException( "Error trying to fetch property or access object", e );
		} catch (InvocationTargetException e) {
			log.debug( "Error invoke target in PersistForm", e );
			throw new ServletException( "Error invoke taget in persist form", e );
		} catch (NoSuchMethodException e) {
			//don't care if a property on the form doesn't exist in my model, that's the point of mvc!
		} 
		
		// the window sticker key/value is not in all posts to this action
		DynaActionForm dynaForm = (DynaActionForm)form; 
		processWindowSticker(form, bean, dynaForm); 
		processSalesChannel(form, bean, dynaForm);
		saveGeoLocation(form, bean, dynaForm);
		
		prePersist( bean );
		persist(bean);
    	bean = (Dealer)loadObject( getIdentifier( form, request) );
    	request.setAttribute( getResultAttribute(), bean );
	}

	private void saveGeoLocation(ActionForm form, Dealer bean,
			DynaActionForm dynaForm) throws ServletException {
		DealerLocationClient client = new DealerLocationClient();
		if (dynaForm.getMap().containsKey("zip")) {
			try {
						String street1 = (String) PropertyUtils.getProperty(form, "street1");
						String street2 = (String) PropertyUtils.getProperty(form, "street2");
						String city = (String) PropertyUtils.getProperty(form, "city");
						String state = (String) PropertyUtils.getProperty(form, "state");
						String address = formAddress(street1,street2,city,state);
						String zipCode = (String) PropertyUtils.getProperty(form, "zip");
						Map<String, BigDecimal> dealerGeoCode = client.getGeoCodeFromGoogle(zipCode,address);
						bean.setLatitude(dealerGeoCode.get("lat"));
						bean.setLongitude(dealerGeoCode.get("long"));
				}
			catch (Exception e) {
			throw new ServletException( "Error occurred when fetching from Google ", e );
			
		}
	}
}

	private String  formAddress(String street1, String street2, String city,
			String state) {
			String address ="";
			if(street1.length()!=0){
				address+=street1.trim();
			}
			if(street2.length()!=0){
				address+=" "+street2.trim();
			}
			if(city.length()!=0){
				address+=" "+city.trim();
			}
			if(state.length()!=0){
				address+=" "+state.trim();
			}
			return (address.equals("")?null:address);
	}

	private void processWindowSticker(ActionForm form, Dealer dealer,
			DynaActionForm dynaForm) throws ServletException {
		if (dynaForm.getMap().containsKey("windowStickerTemplateId")) {
			DealerPreferenceWindowSticker stickerPref = new DealerPreferenceWindowSticker(dealer.getId());
			try {
				Integer windowStickerTemplateId = (Integer)PropertyUtils.getProperty(form, "windowStickerTemplateId");
				Integer buyersGuideTemplateId = (Integer)PropertyUtils.getProperty(form, "buyersGuideTemplateId");
				
				stickerPref.setWindowStickerTemplateId(windowStickerTemplateId);
				stickerPref.setBuyersGuideTemplateId(buyersGuideTemplateId);
				
				dealerPreferenceDao.save(stickerPref);
				
			} catch (Exception e) {
				throw new ServletException( "Error marshalling data from form to object model (DealerPreferenceWindowSticker related).", e );
			}
		}
	}

	private void processSalesChannel(ActionForm form, Dealer dealer,
			DynaActionForm dynaForm) throws ServletException {
		if (dynaForm.getMap().containsKey("salesChannelId")) {
			try {
				Integer selectedId = (Integer)PropertyUtils.getProperty(form, "salesChannelId");
				List<SalesChannel> salesChannels = salesChannelDao.findAll();
				SalesChannel selected = null; 
				for(SalesChannel salesChannel : salesChannels) {
					if(salesChannel.getId().equals(selectedId)) {
						selected = salesChannel;
						break;
					}
				}
				
				if(selected != null) {
					dealer.setSalesChannel(selected);
				}
				
			} catch (Exception e) {
				throw new ServletException( "Error marshalling data from form to object model (SalesChannel related).", e );
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void persist(Object bean) {
		Dealer dealer = (Dealer)bean;
		getGenericDAO().makePersistent(dealer);
		DealerPreference pref = dealer.getPreference();
		if (pref.isPrimaryBookChanged()) {
			fixGuideBookDirect.execute(dealer.getId());
			pref.resetPrimaryBookChangedFlag();
		}
	}
	
	public void setFixGuideBookDirect(
			GuidebookCorrectionForMaxStoredProcedure fixGuideBookDirect) {
		this.fixGuideBookDirect = fixGuideBookDirect;
	}

	public void setDealerPreferenceDao(DealerPreferenceDao dealerPreferenceDao) {
		this.dealerPreferenceDao = dealerPreferenceDao;
	}
	
	public void setSalesChannelDao(SalesChannelDao salesChannelDao) {
		this.salesChannelDao = salesChannelDao;
	}
}
