package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoadVehicleHistoryReportSettings extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.sendRedirect("/VehicleHistoryReportWebApp/Pages/Carfax/Account.aspx?DealerId=" + request.getParameter("id"));
		return null;
	}
}
