package biz.firstlook.app.web.admin.action.client;

public class Viewport
{
    private SouthWest southWest;

    private NorthEast northEast;

    public SouthWest getSouthwest ()
    {
        return southWest;
    }

    public void setSouthwest (SouthWest southWest)
    {
        this.southWest = southWest;
    }

    public NorthEast getNortheast ()
    {
        return northEast;
    }

    public void setNortheast (NorthEast northeast)
    {
        this.northEast = northeast;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [southwest = "+southWest+", northeast = "+northEast+"]";
    }
}