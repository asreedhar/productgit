package biz.firstlook.app.web.common.struts.action.custom;

import java.io.Serializable;

import org.apache.commons.beanutils.MethodUtils;

public class PersistForm extends biz.firstlook.app.web.common.struts.action.PersistForm {

	private Object service;
	private String loadMethod;
	private String saveMethod;

	@Override
	protected void persist(Object bean) {
		try {
			MethodUtils.invokeMethod( service, saveMethod, bean );
		} catch( Exception e ) {
			throw new RuntimeException( "Error invoking save method: " + saveMethod + " from: " + service, e );
		}
	}

	@Override
	protected Object loadObject(Serializable id) {
		Object result;
		try {
			result = MethodUtils.invokeMethod( service, loadMethod, id );
		} catch( Exception e ) {
			throw new RuntimeException( "Error invoking load method: " + loadMethod + " from: " + service, e );
		}
		return result;
	}

	public String getLoadMethod() {
		return loadMethod;
	}

	public void setLoadMethod(String loadMethod) {
		this.loadMethod = loadMethod;
	}

	public String getSaveMethod() {
		return saveMethod;
	}

	public void setSaveMethod(String saveMethod) {
		this.saveMethod = saveMethod;
	}

	public Object getService() {
		return service;
	}

	public void setService(Object service) {
		this.service = service;
	}

	
}
