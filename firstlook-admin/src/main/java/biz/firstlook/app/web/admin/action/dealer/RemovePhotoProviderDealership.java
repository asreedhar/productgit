package biz.firstlook.app.web.admin.action.dealer;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.PhotoProviderDealership;
import biz.firstlook.persist.imt.PhotoProviderDealershipDao;

public class RemovePhotoProviderDealership extends LoadDealer{
	
	PhotoProviderDealershipDao photoProviderDealershipDao;
	
	    @Override
		public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, Dealer dealer) throws ServletException {
			Integer photoProviderIdToRemove = new Integer(request.getParameter( "photoProviderId" ));
			Integer id = dealer.getId();
			
	
	        List<PhotoProviderDealership> photoProviders = photoProviderDealershipDao.findByBusinessUnitId(id);
		        for ( PhotoProviderDealership provider : photoProviders ) {
		        	if ( provider.getPhotoProviderId().equals( photoProviderIdToRemove ) ) {
		        		photoProviderDealershipDao.makeTransient( provider );
		        		break;
		        	}
	        }
	        request.setAttribute("addedPhotoProviders", photoProviderDealershipDao.findByBusinessUnitId(id));
	        request.setAttribute("dealerId", id );
	        return;
		}

	public PhotoProviderDealershipDao getPhotoProviderDealershipDao() {
		return photoProviderDealershipDao;
	}

	public void setPhotoProviderDealershipDao(
			PhotoProviderDealershipDao photoProviderDealershipDao) {
		this.photoProviderDealershipDao = photoProviderDealershipDao;
	}
}