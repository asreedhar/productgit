package biz.firstlook.app.web.admin.action.dealer;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.LoadByFilter;
import biz.firstlook.app.web.common.struts.util.PersistFormUtil;
import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.DealerUpgradeCode;
import biz.firstlook.persist.imt.direct.PingIIDirect;
import biz.firstlook.persist.imt.direct.jdbc.ConfigureDealershipForMAXStoredProcedure;

public class SaveUpgrade extends LoadByFilter {

	private PingIIDirect pingIIDirect;
	private ConfigureDealershipForMAXStoredProcedure configMaxDirect;

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			Object example) throws Exception {
		Object bean = request.getAttribute(getResultAttribute());
		if (bean instanceof Collection) {
			// We've been handed an empty list
			bean = new DealerUpgrade();
		}
		PersistFormUtil.copyFormToBean(form, bean, (String) null,
				(String) null, null);
		getGenericDAO().makePersistent(bean);

		DealerUpgrade upgrade = (DealerUpgrade) bean;
		// special case if this is saving ping 2, need to set up default search
		// criteria
		if (upgrade.getActive()) {
			if (upgrade.getDealerUpgradeCD().equals(DealerUpgradeCode.PING2.getId())) {
				pingIIDirect.setupDefaultDealerSearch(((DealerUpgrade) bean)
						.getBusinessUnitId());
			} else if (upgrade.getDealerUpgradeCD().equals(
					DealerUpgradeCode.MAX.getId())
					|| upgrade.getDealerUpgradeCD().equals(
							DealerUpgradeCode.MAX_TEST.getId())) {
				configMaxDirect.execute(upgrade.getBusinessUnitId());
			}
		}
	}

	public void setPingIIDirect(PingIIDirect pingIIDirect) {
		this.pingIIDirect = pingIIDirect;
	}

	public void setConfigMaxDirect(
			ConfigureDealershipForMAXStoredProcedure configMaxDirect) {
		this.configMaxDirect = configMaxDirect;
	}
}
