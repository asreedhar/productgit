package biz.firstlook.app.web.common.struts.util;

import org.apache.struts.action.ActionForward;

public class ForwardUtil {

	public static ActionForward addParam( ActionForward actionForward, String name, String value ) {

		String forwardPath = actionForward.getPath();
		StringBuffer sBuffer = new StringBuffer( forwardPath );
		sBuffer.append( "?" );
		sBuffer.append( name );
		sBuffer.append( "=" );
		sBuffer.append( value );
		ActionForward returnForward = new ActionForward( sBuffer.toString() );
		returnForward.setModule( actionForward.getModule() );
		returnForward.setName( actionForward.getPath() );
		returnForward.setRedirect( actionForward.getRedirect() );
		return returnForward;
		
	}

	public static ActionForward addParam( ActionForward actionForward, String[] names, String[] values ) {

		String forwardPath = actionForward.getPath();
		StringBuffer sBuffer = new StringBuffer( forwardPath );
		sBuffer.append( "?" );
		for( int i = 0; i < names.length; i++ ) {
			sBuffer.append( names[i] );
			sBuffer.append( "=" );
			sBuffer.append( values[i] );
			if( i != names.length - 1 ) {
				sBuffer.append( "&" );
			}
		}
		ActionForward returnForward = new ActionForward( sBuffer.toString() );
		returnForward.setModule( actionForward.getModule() );
		returnForward.setName( actionForward.getPath() );
		returnForward.setRedirect( actionForward.getRedirect() );
		return returnForward;
		
	}

}
