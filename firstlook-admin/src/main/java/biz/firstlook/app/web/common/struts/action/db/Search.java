package biz.firstlook.app.web.common.struts.action.db;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.persist.common.SearchableDAO;

public class Search extends Action {
	
	private SearchableDAO searchableDao;
	
    public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ActionForward forward = null;
    	
    	String searchStr = "";
    	Enumeration<String> paramNames = request.getParameterNames();
    	while( paramNames.hasMoreElements() ) {
    		String paramName = (String) paramNames.nextElement();
    		if( !paramName.equals("_") ) {
    			searchStr = (String) paramName;
    		}
    	}
    	
    	if( StringUtils.isEmpty(searchStr) ) {
    		forward = mapping.findForward("success");
    	} else {
    		List results = searchableDao.search( searchStr );
    		request.setAttribute( "results", results );
    		forward = mapping.findForward("success");
    	}

		return forward;
	}

	public SearchableDAO getSearchableDao() {
		return searchableDao;
	}

	public void setSearchableDao(SearchableDAO searchableDao) {
		this.searchableDao = searchableDao;
	}
}