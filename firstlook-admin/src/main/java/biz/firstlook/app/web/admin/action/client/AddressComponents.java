package biz.firstlook.app.web.admin.action.client;

public class AddressComponents
{
    private String longname;

    private String[] types;

    private String shortname;

    public String getLong_name ()
    {
        return longname;
    }

    public void setLong_name (String longname)
    {
        this.longname = longname;
    }

    public String[] getTypes ()
    {
        return types;
    }

    public void setTypes (String[] types)
    {
        this.types = types;
    }

    public String getShort_name ()
    {
        return shortname;
    }

    public void setShort_name (String shortname)
    {
        this.shortname = shortname;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [long_name = "+longname+", types = "+types+", short_name = "+shortname+"]";
    }
}