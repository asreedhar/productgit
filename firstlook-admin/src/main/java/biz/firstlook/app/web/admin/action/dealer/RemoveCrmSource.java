package biz.firstlook.app.web.admin.action.dealer;

import java.io.Serializable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.api.dealer.BusinessUnitCredentialService;
import biz.firstlook.app.web.common.struts.action.Load;
import biz.firstlook.model.imt.BusinessUnitCredential;
import biz.firstlook.model.imt.CredentialType;

public class RemoveCrmSource  extends Load {
	private BusinessUnitCredentialService service;
	
	protected void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		DynaBean thirdPartyCredentials = (DynaBean)request.getAttribute(getResultAttribute());
		List<String> crmSources = (List<String>)thirdPartyCredentials.get("assignedCrmSource");
		String removeCrmSource=request.getParameter("crmSource");
		List<BusinessUnitCredential> credentials=(List<BusinessUnitCredential>)thirdPartyCredentials.get("credentials");
		service.delete(credentials, removeCrmSource);
		crmSources.remove(removeCrmSource);
		if(!crmSources.isEmpty()){
			for(BusinessUnitCredential buc:credentials){
				if(crmSources.get(0).equals(CredentialType.getName(buc.getCredentialTypeId()))){
					thirdPartyCredentials.set( "crmPass", buc.getPassword());
					thirdPartyCredentials.set( "crmSource", CredentialType.getName(buc.getCredentialTypeId()));
				}
			}
		}
		else{
			thirdPartyCredentials.set( "crmPass", "");
			thirdPartyCredentials.set( "crmSource", "");
		}
	}
	@Override
	protected Object loadObject(Serializable id) {
		return service.getBusinessUnitCredentials((Integer) id);
	}
	public void setService(BusinessUnitCredentialService service) {
		this.service = service;
	}
}
