package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.persist.imt.DealerGroupDao;

public class SaveGroup extends LoadDealer {

	private DealerGroupDao dealerGroupDao;

	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, Dealer dealer) throws ServletException {
		String groupId = request.getParameter("groupId");
		DealerGroup group = dealerGroupDao.findById( Integer.parseInt( groupId ), false );
		dealer.setDealerGroup( group );
		getDealerDao().makePersistent( dealer );
	}

	public DealerGroupDao getDealerGroupDao() {
		return dealerGroupDao;
	}

	public void setDealerGroupDao(DealerGroupDao dealerGroupDao) {
		this.dealerGroupDao = dealerGroupDao;
	}
}
