package biz.firstlook.app.web.admin.action.dealer;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.ref.JdPowerRegion;
import biz.firstlook.persist.imt.JdPowerRegionDao;

public class JdPowerSave extends LoadDealer
{

private JdPowerRegionDao jdPowerRegionDao;

@Override
protected void afterLoad(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response, Dealer dealer)
    throws ServletException {
	
	DynaActionForm actionForm = (DynaActionForm) form;
	Integer powerRegionId = (Integer)actionForm.get( "powerRegionId" );
	List<JdPowerRegion> regions = jdPowerRegionDao.findAll();
	
	JdPowerRegion selectedRegion = null;
	for(JdPowerRegion region : regions) {
		if(powerRegionId.equals( region.getPowerRegionId() )) {
			selectedRegion = region;
			break;
		}
	}
	
	dealer.setJdPowerRegion( selectedRegion );
	getDealerDao().makePersistent( dealer );
}

public JdPowerRegionDao getJdPowerRegionDao()
{
	return jdPowerRegionDao;
}

public void setJdPowerRegionDao( JdPowerRegionDao jdPowerRegionDao )
{
	this.jdPowerRegionDao = jdPowerRegionDao;
}

}
