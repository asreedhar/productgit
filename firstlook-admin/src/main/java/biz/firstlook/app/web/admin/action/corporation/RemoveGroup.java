package biz.firstlook.app.web.admin.action.corporation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Corporation;
import biz.firstlook.model.imt.DealerGroup;

public class RemoveGroup extends Load {

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String dealerGroupIdStr = (String) request.getParameter( "dealerGroupId" );
		Integer dealerGroupId = new Integer( dealerGroupIdStr );
		Corporation corporation = (Corporation) request.getAttribute( getResultAttribute() );
		DealerGroup toRemove = null;
		for( DealerGroup group : corporation.getGroups() ) {
			if( group.getId().equals( dealerGroupId ) ) {
				toRemove = group;
			}
		}
		if( toRemove != null ) {
			corporation.getGroups().remove( toRemove );
		}
		getGenericDAO().makePersistent( corporation );
		return;
	}

}
