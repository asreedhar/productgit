package biz.firstlook.app.web.admin.action.member;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.Member;

public class RemoveDealer extends Load {

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String dealerIdStr = (String) request.getParameter( "dealerId" );
		Integer dealerId = new Integer( dealerIdStr );
		Member member = (Member) request.getAttribute( getResultAttribute() );
		Dealer toRemove = null;
		for( Dealer dealer : member.getDealers() ) {
			if( dealer.getId().equals( dealerId ) ) {
				toRemove = dealer;
			}
		}
		if( toRemove != null ) {
			member.getDealers().remove( toRemove );
		}
		getGenericDAO().makePersistent( member );
		return;
	}

}
