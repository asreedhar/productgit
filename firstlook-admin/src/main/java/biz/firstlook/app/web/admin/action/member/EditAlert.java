package biz.firstlook.app.web.admin.action.member;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.api.subscribe.MemberSubscription;
import biz.firstlook.api.subscribe.SubscribeService;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;

public class EditAlert extends Action {

	private Logger logger = Logger.getLogger( EditAlert.class );
	
	private SubscribeService subscribeService;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		Integer memberId = Integer.valueOf( request.getParameter("memberId") );
		Integer subscriptionTypeId = Integer.valueOf( request.getParameter("subscriptionTypeId") );
		MemberSubscription subscription = subscribeService.retrieve( memberId, subscriptionTypeId );
		request.setAttribute( "subscription", subscription );
		
		request.setAttribute("isEdit", Boolean.TRUE);
		logger.debug( "Populating Form" );
		DynaActionForm dForm = (DynaActionForm) form;
		dForm.set( "memberId", subscription.getMemberId() );
		dForm.set( "subscriptionTypeId", subscription.getSubscriptionTypeId() );
		dForm.set( "subscriptionFrequencyDetailId", subscription.getSubscriptionFrequencyDetailId() );
		
		if( subscription.getSubscriptionDeliveryTypeIds() != null  ) {
			Set<Integer> delivIds = subscription.getSubscriptionDeliveryTypeIds();
			Integer[] ids = new Integer[delivIds.size()];
			ids = delivIds.toArray(ids);
			dForm.set( "subscriptionDeliveryTypeIds", ids );
		}
		if( subscription.getBusinessUnitIds() != null  ) {
			dForm.set( "businessUnitIds", subscription.getBusinessUnitIds().toArray(new Integer[0]) );
		}
		if( subscription.getEligibleDealers() != null  ) {
			dForm.set( "eligibleDealers", subscription.getEligibleDealers().toArray(new Dealer[0]) );
		}
		if( subscription.getEligibleDealerGroups() != null  ) {
			dForm.set( "eligibleDealerGroups", subscription.getEligibleDealerGroups().toArray(new DealerGroup[0]) );
		}
		
		// get all delivery types for this subscription type 
		request.setAttribute("deliveryTypes", subscribeService.getSubscriptionDeliveryTypes(subscriptionTypeId));
		
		// get all frequency types for this subscription type
		request.setAttribute("frequencies", subscribeService.getSubscriptionFrequencyDetails(subscriptionTypeId));
		
		//PropertyUtils.copyProperties( form, subscription );
		return mapping.findForward("success");
	}

	public SubscribeService getSubscribeService() {
		return subscribeService;
	}

	public void setSubscribeService(SubscribeService subscribeService) {
		this.subscribeService = subscribeService;
	}
	
}
