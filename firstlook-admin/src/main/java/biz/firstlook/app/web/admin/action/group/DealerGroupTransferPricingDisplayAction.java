package biz.firstlook.app.web.admin.action.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.DealerTransferAllowRetailOnlyRule;
import biz.firstlook.model.imt.DealerTransferPriceUnitCostRule;
import biz.firstlook.model.imt.TransferPriceInventoryAgeRange;
import biz.firstlook.model.imt.TransferPriceUnitCostValue;
import biz.firstlook.persist.imt.TransferPriceInventoryAgeRangeDao;
import biz.firstlook.persist.imt.TransferPriceUnitCostValueDao;

public class DealerGroupTransferPricingDisplayAction extends AbstractTransferPriceAction {

	private static final String ALLOW_TRANSFER_FOR_RETAIL_ONLY_DESCRIPTION = "Allow Transfer For Retail Only between";
	
	private TransferPriceInventoryAgeRangeDao transferPriceInventoryAgeRangeDao;
	private TransferPriceUnitCostValueDao transferPriceUnitCostValueDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, DealerGroup group, List<Dealer> dealers )
			throws Exception {
		List<TransferPriceInventoryAgeRange> ageRanges = transferPriceInventoryAgeRangeDao.findAll();
		List<TransferPriceUnitCostValue> unitCostValues = transferPriceUnitCostValueDao.findAll();
		
		List<TransferPriceRuleBean> transferPriceRuleBeans = new ArrayList<TransferPriceRuleBean>();
		
		for(Dealer dealer : dealers) {
			if(dealer.getActive()) {
				Integer dealerId = dealer.getId();
				String dealerName = dealer.getName();
				Set<DealerTransferAllowRetailOnlyRule> roRules = dealer.getDealerTransferAllowRetailOnlyRules();
				for(DealerTransferAllowRetailOnlyRule roRule : roRules) {
					transferPriceRuleBeans.add(new TransferPriceRuleBean(dealerId
													, dealerName
													, roRule.getId()
													, roRule.getTransferPriceInventoryAgeRange().getId()
													, ALLOW_TRANSFER_FOR_RETAIL_ONLY_DESCRIPTION
													, Integer.valueOf(1)));
				}
				Set<DealerTransferPriceUnitCostRule> ucRules = dealer.getDealerTransferPriceUnitCostRules();
				for(DealerTransferPriceUnitCostRule ucRule : ucRules) {
					transferPriceRuleBeans.add(new TransferPriceRuleBean(dealerId
													, dealerName
													, ucRule.getTransferPriceUnitCostValue().getId()
													, ucRule.getTransferPriceInventoryAgeRange().getId()
													, ucRule.getTransferPriceUnitCostValue().getDescription()
													, Integer.valueOf(0)));
				}
			}
		}

		request.setAttribute("transferPriceRuleBeans", transferPriceRuleBeans);
		request.setAttribute("ageRanges", ageRanges);
		request.setAttribute("unitCostValues", unitCostValues);
		return mapping.findForward("success");
	}

	public void setTransferPriceInventoryAgeRangeDao(
			TransferPriceInventoryAgeRangeDao transferPriceRuleResultDao) {
		this.transferPriceInventoryAgeRangeDao = transferPriceRuleResultDao;
	}

	public void setTransferPriceUnitCostValueDao(
			TransferPriceUnitCostValueDao transferPriceUnitCostValueDao) {
		this.transferPriceUnitCostValueDao = transferPriceUnitCostValueDao;
	}
	
	public class TransferPriceRuleBean implements Serializable {

		private static final long serialVersionUID = -7914568336462889147L;
		
		private Integer dealerId;
		private String dealerName;
		private Integer ruleId;
		private Integer transferPriceInventoryAgeRangeId;
		private String description;
		private Integer ruleGroupId;

		public TransferPriceRuleBean(Integer dealerId, String dealerName, Integer ruleId,
				Integer transferPriceInventoryAgeRangeId, String description, Integer ruleGroupId) {
			super();
			this.dealerId = dealerId;
			this.dealerName = dealerName;
			this.ruleId = ruleId;
			this.transferPriceInventoryAgeRangeId = transferPriceInventoryAgeRangeId;
			this.description = description;
			this.ruleGroupId = ruleGroupId;
		}
		public Integer getDealerId() {
			return dealerId;
		}
		public String getDealerName() {
			return dealerName;
		}
		public Integer getRuleId() {
			return ruleId;
		}
		public Integer getTransferPriceInventoryAgeRangeId() {
			return transferPriceInventoryAgeRangeId;
		}
		public String getDescription() {
			return description;
		}
		public Integer getRuleGroupId() {
			return ruleGroupId;
		}
	}
}
