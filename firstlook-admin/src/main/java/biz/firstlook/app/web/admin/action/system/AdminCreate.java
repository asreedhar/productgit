package biz.firstlook.app.web.admin.action.system;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.JobTitle;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.ref.MemberType;
import biz.firstlook.model.imt.ref.NewRole;
import biz.firstlook.model.imt.ref.UsedRole;
import biz.firstlook.model.imt.ref.UserRole;
import biz.firstlook.persist.imt.MemberDao;

public class AdminCreate extends Action {

	private MemberDao memberDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Member member = new Member();
		
		String firstName = request.getParameter( "firstName" );
		String lastName = request.getParameter( "lastName" );
		String login = request.getParameter( "login" );
		String password = request.getParameter( "password" );
		
		String returnMessage = null;
		Member existingMember = memberDao.byLogin( login );
		if( existingMember == null) {
			member.setFirstName( firstName );
			member.setLastName( lastName );
			member.setLogin( login );
			member.setPassword( password );
			member.setUserRole(UserRole.ALL);
			member.setUsedRole( UsedRole.MANAGER);
			member.setNewRole( NewRole.STANDARD ); 
			member.setMemberType(MemberType.ADMIN);
			member.setCreateDate(new Date());
			member.setOfficePhoneNumber( "8888888888" );
			member.setGroupHomePageId(1); //default to bubble page
			member.setDashboardRowDisplay(10);
			JobTitle jt = new JobTitle();
			jt.setId( 6 );
			member.setJobTitle( jt );
			memberDao.makePersistent( member );
			returnMessage = new StringBuilder( login ).append(" was created successfully.").toString();
		} else {
			returnMessage = "Login already exists, please assign a different login.";
		}
		
		request.setAttribute( "createMemberLogin", returnMessage );

		return mapping.findForward("success");
	}

	public MemberDao getMemberDao() {
		return memberDao;
	}

	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}

}
