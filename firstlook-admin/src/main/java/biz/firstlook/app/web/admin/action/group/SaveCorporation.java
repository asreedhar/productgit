package biz.firstlook.app.web.admin.action.group;

import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.persist.imt.direct.BusinessUnitDirect;

public class SaveCorporation extends Load {

	private BusinessUnitDirect businessUnitDirect;
	
	@Override
	protected Serializable getIdentifier(ActionForm form, HttpServletRequest request) throws ServletException {
		Integer id = new Integer( request.getParameter("id") );
		return id;
	}

	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String corporationIdStr = request.getParameter("corporationId");

		if( !StringUtils.isEmpty( corporationIdStr ) ) {
			DealerGroup group = (DealerGroup) request.getAttribute( getResultAttribute() );
			Integer corporationId = new Integer( corporationIdStr );
			businessUnitDirect.setParent( group.getId(), corporationId );
			retrieveObject( form, request );
		}
		return;
	}

	public BusinessUnitDirect getBusinessUnitDirect() {
		return businessUnitDirect;
	}

	public void setBusinessUnitDirect(BusinessUnitDirect businessUnitDirect) {
		this.businessUnitDirect = businessUnitDirect;
	}


	

}
