package biz.firstlook.app.web.common.struts.util;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * A facade for accessing a LRU cache and storing it in the session.
 * @author bfung
 *
 * @param <K>
 * @param <V>
 */
public class BreadCrumbUtil<K extends Serializable, V extends Serializable> {
        
    public V put(HttpServletRequest request, K key, V value) {
	LRUCache<K, V> cache = getCache(request);
	return cache.put(key, value);
    }
    
    public V get(HttpServletRequest request, K key) {
	LRUCache<K, V> cache = getCache(request);
	return cache.get(key);
    }
    
    public List<V> getTrail(HttpServletRequest request) {
	LRUCache<K, V> cache = getCache(request);
	return cache.getEntries();
    }

    @SuppressWarnings("unchecked")
    private LRUCache<K, V> getCache(
	    HttpServletRequest request) {
	HttpSession session = request.getSession(false);
	LRUCache<K, V> cache = (LRUCache<K, V>) session
		.getAttribute(BreadCrumbUtil.class.getCanonicalName());
	if(cache==null) {
	    cache = new LRUCache<K, V>(5); //wooo! hard code to 5 entries
	    session.setAttribute(BreadCrumbUtil.class.getCanonicalName(), cache);
	}
	return cache; 
    }
}
