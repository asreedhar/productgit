package biz.firstlook.app.web.admin.action.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.api.subscribe.MemberSubscription;
import biz.firstlook.api.subscribe.SubscribeService;

public class LoadAlert extends Action {

	private Logger logger = Logger.getLogger( LoadAlert.class );
	
	private SubscribeService subscribeService;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		Integer memberId = new Integer( request.getParameter("memberId") );
		Integer subscriptionTypeId = new Integer( request.getParameter("subscriptionTypeId") );
	
		logger.debug( "Loader member subscription by memberId: " + memberId + ", and subscriptionTypeId: " + subscriptionTypeId );
		MemberSubscription subscription = subscribeService.retrieve( memberId, subscriptionTypeId );
		request.setAttribute( "subscription", subscription );

		// get all delivery types for this subscription type 
		request.setAttribute("deliveryTypes", subscribeService.getSubscriptionDeliveryTypes(subscriptionTypeId));
		
		// get all frequency types for this subscription type
		request.setAttribute("frequencies", subscribeService.getSubscriptionFrequencyDetails(subscriptionTypeId));
		
		return mapping.findForward("success");
	}

	public SubscribeService getSubscribeService() {
		return subscribeService;
	}

	public void setSubscribeService(SubscribeService subscribeService) {
		this.subscribeService = subscribeService;
	}

}
