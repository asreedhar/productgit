package biz.firstlook.app.web.admin.action.dealer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import biz.firstlook.model.imt.DealerExtractConfiguration;

public class DealerExtractConfigurationForm extends ActionForm {

	private static final long serialVersionUID = -8498330797929390102L;
	
	private Integer dealerId;
	private String destinationName;
	private Boolean active;
	private Date startDate;
	private Date endDate;
	private String externalIdentifier;
	
	private ActionErrors errors = new ActionErrors();
	
	private static String FORMAT = "M/d/yyyy";
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(FORMAT);
		
	public Integer getDealerId() {
		return dealerId;
	}
	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public Boolean getActive() {
		if(active == null) {
			return Boolean.FALSE;
		}
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getStartDate() {
		if(startDate != null) {
			return DATE_FORMAT.format(startDate);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public void setStartDate(String startDate) {
		if(startDate != null && StringUtils.isNotBlank(startDate)) {
			try {
				this.startDate = DATE_FORMAT.parse(startDate);
			} catch (Exception e) {
				boolean addMessage = true;
				Iterator<ActionMessage> iter = (Iterator<ActionMessage>)errors.get("startDate");
				while(iter.hasNext()) {
					ActionMessage message = iter.next();
					if(message.getKey().equals("errors.date.format")) {
						addMessage = false;
						break;
					}
				}
				
				if(addMessage) {
					errors.add("startDate", new ActionMessage("errors.date.format", "Start Date", FORMAT));
				}
			}
		}
	}
	public String getEndDate() {
		if(endDate != null) {
			return DATE_FORMAT.format(endDate);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public void setEndDate(String endDate) {
		if(endDate != null && StringUtils.isNotBlank(endDate)) {
			try {
				this.endDate = DATE_FORMAT.parse(endDate);
			} catch (Exception e) {
				boolean addMessage = true;
				Iterator<ActionMessage> iter = (Iterator<ActionMessage>)errors.get("endDate");
				while(iter.hasNext()) {
					ActionMessage message = iter.next();
					if(message.getKey().equals("errors.date.format")) {
						addMessage = false;
						break;
					}
				}
				
				if(addMessage) {
					errors.add("endDate", new ActionMessage("errors.date.format", "End Date", FORMAT));
				}
			}
		}
	}
	
	public String getExternalIdentifier() {
		return externalIdentifier;
	}
	public void setExternalIdentifier(String externalIdentifier) {
		this.externalIdentifier = externalIdentifier;
	}
	
	public DealerExtractConfiguration getConfiguration(String login) {
		DealerExtractConfiguration config = new DealerExtractConfiguration(dealerId, destinationName);
		config.setActive(active == null ? false : active);
		config.setStartDate(startDate);
		config.setEndDate(endDate);
		config.setExternalIdentifier(externalIdentifier);
		config.setUpdateDate(new Date());
		config.setUpdateUser(login);
		return config;
	}
	
	public void populate(DealerExtractConfiguration config) {
		active = config.isActive();
		dealerId = config.getDealerId();
		destinationName = config.getDestinationName();
		endDate = config.getEndDate();
		externalIdentifier = config.getExternalIdentifier();
		startDate = config.getStartDate();
	}
	
	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		
		if(active != null ) {
			if(active.booleanValue()) {
				if(startDate == null) {
					errors.add("startDate", new ActionMessage("errors.required", "Start Date"));
				} else {
					if(endDate != null && endDate.before(startDate)) {
						errors.add("endDate", new ActionMessage("errors.date.after","End Date", "Start Date"));
					}
				}
			}
			
			if(externalIdentifier == null) {
				errors.add("externalIdentifier", new ActionMessage("errors.required"));
			} else {
				if(externalIdentifier.length() < 1) {
					errors.add("externalIdentifier", new ActionMessage("errors.minlength", "External ID",1));
				} else if(externalIdentifier.length() > 30) {
					errors.add("externalIdentifier", new ActionMessage("errors.maxlength", "External ID",30));
				}
			}
		}
		
		return errors;
	}

}
