package biz.firstlook.app.web.admin.action.group;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.TransferPriceInventoryAgeRange;
import biz.firstlook.persist.imt.TransferPriceInventoryAgeRangeDao;

public class TransferPriceInventoryAgeRangeAddAction extends
		AbstractTransferPriceAction {

	private static final String SUCCESS = "saved";
	private static final String ERROR = "Failed adding range, please check the low and high values.";
	
	private TransferPriceInventoryAgeRangeDao transferPriceInventoryAgeRangeDao;

	@Override
	protected ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			DealerGroup group, List<Dealer> dealers) throws Exception {
		
		TransferPriceInventoryAgeRangeForm theForm = (TransferPriceInventoryAgeRangeForm)form;
		TransferPriceInventoryAgeRange range = new TransferPriceInventoryAgeRange(theForm.getLow(), theForm.getHigh());
		
		ActionForward forward = null;
		String message = null;
		try {
			transferPriceInventoryAgeRangeDao.makePersistent(range);
			message = SUCCESS;
			forward = mapping.findForward("success");
		} catch (Exception e) {
			message = ERROR;
			forward = mapping.findForward("error");
		}
		request.setAttribute("message", message);
		
		return forward;
	}

	public void setTransferPriceInventoryAgeRangeDao(
			TransferPriceInventoryAgeRangeDao transferPriceInventoryAgeRangeDao) {
		this.transferPriceInventoryAgeRangeDao = transferPriceInventoryAgeRangeDao;
	}

}
