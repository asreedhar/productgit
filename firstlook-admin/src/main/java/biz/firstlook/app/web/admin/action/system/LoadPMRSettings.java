package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoadPMRSettings extends Action {

	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//request.setPort( 443 );
		String h = request.getServerName();
		String u = "https://"+ h;
		response.sendRedirect(u+"/command_center/");
		return null;
	}


}
