package biz.firstlook.app.web.admin.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import biz.firstlook.app.web.admin.AdminSession;
import biz.firstlook.app.web.admin.security.SecurityManager;

import com.discursive.cas.extend.client.CASFilter;

public class AdminSecurityFilter implements Filter {

	private static Logger log = Logger.getLogger( AdminSecurityFilter.class );
	
	private SecurityManager securityManager;
	
	public void init(FilterConfig config) throws ServletException {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
		log.debug( "Associating this filter with securityManager" );
		securityManager = (SecurityManager) context.getBean("securityManager");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
		throws IOException, ServletException {

		// Does this session have an AdminSession yet?
		HttpSession session = ((HttpServletRequest) request).getSession();
		if( !sessionPopulated( session ) ) {
			populateSession(session);
		}
		
		AdminSession adSess = (AdminSession)session.getAttribute(AdminSession.ADMIN_SESSION); 
		if ( !SecurityManager.isAdmin( adSess ) )
		{
			return;
		}
		chain.doFilter( request, response );
	}

	private void populateSession(HttpSession session) throws ServletException {
		String username = (String) session.getAttribute( CASFilter.CAS_FILTER_USER );
		if( username == null && StringUtils.isEmpty( username ) ) {
			throw new ServletException( "CAS User Name not populated" );
		}
		
		AdminSession adminSession = securityManager.createAdminSession( username );
		session.setAttribute( AdminSession.ADMIN_SESSION, adminSession );
	}

	boolean sessionPopulated(HttpSession session) {
		boolean populated = false;
		if( session != null ) {
			Object asession = session.getAttribute( AdminSession.ADMIN_SESSION );
			if( asession != null ) {
				populated = true;
			}
		}
		return populated;
	}

	public void destroy() {
		
	}

}
