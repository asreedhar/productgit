package biz.firstlook.app.web.common.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public abstract class LoadAll extends Action implements MethodInterceptor {
	
	private String resultAttribute = "results";
	private String successForward = "success";
	
    @SuppressWarnings("unchecked")
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
       	List results = loadAll();
    	request.setAttribute( resultAttribute, results );
		return findForward(mapping,form,request);
	}

	protected abstract List loadAll();

    protected ActionForward findForward(ActionMapping mapping, ActionForm form,
			HttpServletRequest request) {
		return mapping.findForward(successForward);
	}

    // Implementation of the MethodInterceptor
	public Object invoke(MethodInvocation inv) throws Throwable {
		if( inv.getMethod().getName().equals( "execute" ) ) {
			Object[] args = inv.getArguments();
			this.execute( (ActionMapping) args[0], (ActionForm) args[1], 
						  (HttpServletRequest) args[2], 
						  (HttpServletResponse) args[3] );
		}
		return inv.proceed();
	}

	public String getResultAttribute() {
		return resultAttribute;
	}

	public void setResultAttribute(String resultAttribute) {
		this.resultAttribute = resultAttribute;
	}

	public String getSuccessForward() {
		return successForward;
	}

	public void setSuccessForward(String successForward) {
		this.successForward = successForward;
	}		    
}