package biz.firstlook.app.web.admin.action.corporation;

import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Corporation;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.persist.imt.DealerGroupDao;

public class AddGroups extends Load {

	private DealerGroupDao dealerGroupDao;
	
	@Override
	protected Serializable getIdentifier(ActionForm form, HttpServletRequest request) throws ServletException {
		Integer id = new Integer( request.getParameter("id") );
		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String[] dealerGroupIds = request.getParameterValues("dealerGroupIds");

		Corporation corporation = (Corporation) request.getAttribute( getResultAttribute() );
		for( String dealerGroupId : dealerGroupIds ) {
			DealerGroup group = dealerGroupDao.findById( Integer.parseInt( dealerGroupId ), false );
			corporation.getGroups().add( group );
		}
		getGenericDAO().makePersistent( corporation );
		
		return;
	}

	public DealerGroupDao getDealerGroupDao() {
		return dealerGroupDao;
	}

	public void setDealerGroupDao(DealerGroupDao dealerGroupDao) {
		this.dealerGroupDao = dealerGroupDao;
	}

}
