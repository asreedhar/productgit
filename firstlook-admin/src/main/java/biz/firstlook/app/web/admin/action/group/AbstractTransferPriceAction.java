package biz.firstlook.app.web.admin.action.group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.persist.imt.DealerGroupDao;

public abstract class AbstractTransferPriceAction extends Action {

	protected DealerGroupDao dealerGroupDao;
	
	@Override
	public final ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception {
		String stuff = request.getParameter("id"); //if you use a form, this input field clashes with getting the form's id in javascript!
		Integer dealerGroupId = null;
		try {
			dealerGroupId = Integer.valueOf(stuff);
		} catch (NumberFormatException e) {
			dealerGroupId = Integer.valueOf(request.getParameter("groupId"));
		}
		DealerGroup group = dealerGroupDao.findById(dealerGroupId);
		
		List<Dealer> dealers = new ArrayList<Dealer>(group.getDealers());
		Collections.sort(dealers, new Comparator<Dealer>(){
			public int compare(Dealer d1, Dealer d2) {
				return d1.getName().toLowerCase().compareTo(d2.getName().toLowerCase());
			}
		});
		
		request.setAttribute("dealers", dealers);
		request.setAttribute("group", group);
		request.setAttribute("corporation", group.getCorporation());
		request.setAttribute("id", group.getId());
		return execute(mapping, form, request, response, group, dealers);
	}
	
	protected abstract ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, DealerGroup group, List<Dealer> orderedDealers)
		throws Exception;
	
	public void setDealerGroupDao( DealerGroupDao dealerGroupDao ) {
		this.dealerGroupDao = dealerGroupDao;
	}
}
