package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.LoadByFilter;
import biz.firstlook.app.web.common.struts.util.PopulateFormUtil;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreference;
import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.ThirdPartyEnum;
import biz.firstlook.persist.common.GenericDAO;

public class EditUpgrade extends LoadByFilter {

	private GenericDAO dealerDAO;
	
	
	
	public GenericDAO getDealerDAO() {
		return dealerDAO;
	}



	public void setDealerDAO(GenericDAO dealerDAO) {
		this.dealerDAO = dealerDAO;
	}



	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, Object example) throws Exception {
		PopulateFormUtil.copyBeanToForm( form, request, getResultAttribute(), (String) null, (String) null);
        if (example instanceof DealerUpgrade) {
            DealerUpgrade upgrade = (DealerUpgrade)example;
            
            Boolean bool=false;
            @SuppressWarnings({ "unchecked", "unused" })
			Dealer dealer = (Dealer) getDealerDAO().findById(upgrade.getBusinessUnitId());
            DealerPreference prefs= dealer.getPreference();
            
            
            if((prefs.getGuideBookId()!=null && prefs.getGuideBookId().equals(ThirdPartyEnum.NADA.getThirdPartyId()))||( prefs.getGuideBook2Id()!=null&&prefs.getGuideBook2Id().equals(ThirdPartyEnum.NADA.getThirdPartyId()))){
            	bool=true;
            }

            
            request.setAttribute("isNADADealer", bool);
            request.setAttribute("businessUnitId", upgrade.getBusinessUnitId());
            request.setAttribute("dealerUpgradeCD", upgrade.getDealerUpgradeCD());
        }
        
	}
}
