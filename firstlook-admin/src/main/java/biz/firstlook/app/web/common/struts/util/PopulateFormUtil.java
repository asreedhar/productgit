package biz.firstlook.app.web.common.struts.util;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

public class PopulateFormUtil {

	private static Logger log = Logger.getLogger( PopulateFormUtil.class );
	
	@SuppressWarnings("unchecked")
	public static void copyBeanToForm(ActionForm form, HttpServletRequest request, String resultAttribute, String targetProperty, String paramName) throws IllegalAccessException, InvocationTargetException, ServletException {
		request.setAttribute( "isEdit", Boolean.TRUE );
    	Object bean = request.getAttribute( resultAttribute );
    	if( bean instanceof Collection ) {
    		Collection col = (Collection) bean;
    		if( col.isEmpty() ) {
    			return;
    		}
    	}
    	if( !StringUtils.isEmpty( targetProperty ) ) {
    		try {
				bean = PropertyUtils.getProperty( bean, targetProperty );
			} catch (NoSuchMethodException e) {
				throw new IllegalAccessException(e.getMessage());
			}
    	}
    	Map<String, Object> properties;
		try {
			properties = new HashMap<String,Object>( PropertyUtils.describe(form) );
		} catch (NoSuchMethodException e1) {
			throw new IllegalAccessException(e1.getMessage());
		}
    	if( !StringUtils.isEmpty( paramName ) ) {
    		properties.remove( paramName );
    	}
    	if( bean != null ) {
    	for( String property : properties.keySet() ) {
    		try {
	    		Object value = PropertyUtils.getProperty( bean, property );
	    		Class type = PropertyUtils.getPropertyType( bean, property );
	    		Class targetType = PropertyUtils.getPropertyType( form, property);
	    		if( type.isEnum() ) {
		    		Enum enumVal = (Enum) value;
		    		if( value != null ) {
		    			PropertyUtils.setProperty( form, property, enumVal.name() );
		    		}
		    		populateConstants( request, property, type );
		    	} else if( type.isArray() && type.getComponentType().isEnum() ) {
		    		String[] strValues = new String[((Enum[]) value).length];
		    		if( value != null ) {
			    		int i = 0;
		    			for( Enum enumVal : (Enum[]) value ) {
		    				strValues[i] = enumVal.name();
		    				i++;
		    			}
		    		}
	    			PropertyUtils.setProperty( form, property, strValues );
		    		populateConstants( request, property, type.getComponentType());
		    	} else if( type.isAssignableFrom( Date.class ) ) {
		    		SimpleDateFormat sdf = new SimpleDateFormat( "M/d/yyyy" );
		    		if( value != null ) {
		    			String dateStr = sdf.format( value );
		    			PropertyUtils.setProperty( form, property, dateStr );
		    		}
		    	} else if( targetType.isAssignableFrom( Integer.class ) && !type.isAssignableFrom( Integer.class ) && PropertyUtils.isReadable( bean, "id") ) {
		    		PropertyUtils.setProperty( form, property, PropertyUtils.getProperty( bean, "id" ) );
		    	} else {
		    		try {
		    			PropertyUtils.setProperty( form, property, value );
		    		} catch( ConversionException ce ) { 			
		    			log.debug( "Conversion problem while called set property", ce );
		    		}
		    	}
    		} catch (NoSuchMethodException e) {
    			log.debug(MessageFormat.format("Warning, {0} on bean: {1}.  This property may be populated by another model object.", 
    					e.getMessage(), property));
    		}
    	}
    		
    	}
	}
    
    public static void populateConstants( HttpServletRequest request, String property, Class enumClass) {
    	Enum[] constants = (Enum[]) enumClass.getEnumConstants();
    	String[] names = new String[constants.length];
    	int i = 0;
    	for( Enum constant : constants ) {
    		names[i] = constant.name();
    		i++;
    	}
    	request.setAttribute( property + "Enum", names );
    }

}
