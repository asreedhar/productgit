package biz.firstlook.app.web.common.struts.action.db;

import java.util.List;

import biz.firstlook.persist.common.GenericDAO;

public class LoadAll extends biz.firstlook.app.web.common.struts.action.LoadAll {
	
	private GenericDAO genericDAO;
	
	@SuppressWarnings("unchecked")
	@Override
	protected List loadAll() {
       	return genericDAO.findAll();
	}

	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}		
	
	
    
}