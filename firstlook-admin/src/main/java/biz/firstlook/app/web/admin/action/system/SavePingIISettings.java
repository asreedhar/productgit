package biz.firstlook.app.web.admin.action.system;

import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.api.invbucket.InventoryBucketRangeService;
import biz.firstlook.model.imt.DealerPreferencePricing;
import biz.firstlook.model.market.MileagePerYearBucket;
import biz.firstlook.persist.imt.DealerPreferencePricingDao;
import biz.firstlook.persist.market.MileagePerYearBucketDao;

public class SavePingIISettings extends Action {

	private DealerPreferencePricingDao dealerPreferencePricingDao;
	private InventoryBucketRangeService invBucketService;
	private MileagePerYearBucketDao mileagePerYearBucketDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		Integer dealerId = Integer.parseInt(request.getParameter("dealerId"));
		DealerPreferencePricing dealerPricingPrefs = dealerPreferencePricingDao.findByDealerId(dealerId);
		if (dealerPricingPrefs == null) {
			dealerPricingPrefs = new DealerPreferencePricing();
			dealerPricingPrefs.setId(dealerId);
		}
		
		DynaActionForm dynaForm = (DynaActionForm)form;	
		dealerPricingPrefs.setPingII_DefaultSearchRadius((Integer)dynaForm.get("defaultSearchRadius") );
		dealerPricingPrefs.setPingII_MaxSearchRadius((Integer)dynaForm.get("maxSearchRadius") );
		dealerPricingPrefs.setPingII_SupressSellerName( (Boolean)dynaForm.get("supressSellerName") == null ? false:true );
		dealerPricingPrefs.setPingII_ExcludeNoPriceFromCalc( (Boolean)dynaForm.get("excludeNoPriceFromCalc") == null ? false:true );
		dealerPricingPrefs.setPingII_ExcludeLowPriceOutliersMultiplier( (Double)dynaForm.get("excludeLowPriceOutliersMultiplier") );
		dealerPricingPrefs.setPingII_ExcludeHighPriceOutliersMultiplier( (Double)dynaForm.get("excludeHighPriceOutliersMultiplier") );
		dealerPricingPrefs.setPingII_NewPing((Boolean)dynaForm.get("newPing"));
		dealerPricingPrefs.setPingII_MarketListing(!((String)dynaForm.get("marketListing")).equals("") ? (String)dynaForm.get("marketListing") : null) ;
		dealerPricingPrefs.setPingII_RedRange1Value1((Integer)dynaForm.get("red_range1Value1"));
	    dealerPricingPrefs.setPingII_RedRange1Value2((Integer)dynaForm.get("red_range1Value2"));
	    dealerPricingPrefs.setPingII_YellowRange1Value1((Integer)dynaForm.get("yellow_range1Value1"));
	    dealerPricingPrefs.setPingII_YellowRange1Value2((Integer)dynaForm.get("yellow_range1Value2"));
	    dealerPricingPrefs.setPingII_YellowRange2Value1((Integer)dynaForm.get("yellow_range2Value1"));
	    dealerPricingPrefs.setPingII_YellowRange2Value2((Integer)dynaForm.get("yellow_range2Value2"));
	    dealerPricingPrefs.setPingII_GreenRange1Value1((Integer)dynaForm.get("green_range1Value1"));
	    dealerPricingPrefs.setPingII_GreenRange1Value2((Integer)dynaForm.get("green_range1Value2"));
		if (dynaForm.getMap().containsKey("packageId")) {// Kills Ping II system settings so test for it, PM
				dealerPricingPrefs.setPingII_PackageId( (Integer)dynaForm.get("packageId") ); 
		}
		
		if (dynaForm.getMap().containsKey("matchCertifiedByDefault")) {
			Boolean matchCertifiedByDefault = (Boolean)dynaForm.get("matchCertifiedByDefault");
			if (matchCertifiedByDefault == null || matchCertifiedByDefault.equals(Boolean.FALSE)) {
				dealerPricingPrefs.setPingII_MatchCertifiedByDefault( Boolean.FALSE );
			}
			else {
				dealerPricingPrefs.setPingII_MatchCertifiedByDefault( Boolean.TRUE );
			}
		}		
		
		Boolean suppressTrimMatchStatus = (Boolean)dynaForm.get("suppressTrimMatchStatus");
		if (suppressTrimMatchStatus == null || suppressTrimMatchStatus.equals(Boolean.FALSE)) {
			dealerPricingPrefs.setPingII_SuppressTrimMatchStatus( Boolean.FALSE );
		}
		else {
			dealerPricingPrefs.setPingII_SuppressTrimMatchStatus( Boolean.TRUE );
		}
		
		if (dynaForm.getMap().containsKey("guideBookId")) { // default pingII system settings do not have a GB pref
			dealerPricingPrefs.setPingII_GuideBookId((Integer)dynaForm.get("guideBookId") );			
		}

		dealerPreferencePricingDao.makePersistent(dealerPricingPrefs);
		
		dynaForm.set("id", dealerId);
		
		//age bucket min and max market values are between 0 and 255		
		for(int i = 1; i <= 6; i++){
			for(int j = 1; j <= 2; j++){
				Integer value = (Integer)dynaForm.get("age_range" + i + "Value" + j); 
				if (value < 0){
					dynaForm.set("age_range" + i + "Value" + j, new Integer(0));
				}
				else if (value > 255){
					dynaForm.set("age_range" + i + "Value" + j, new Integer(255));
				}
			}
		}
		invBucketService.savePingIIBucketRanges( dynaForm );
		
		//mileage bucket min and max market values are between -99 and 99
		for(int i = 1; i <= 7; i++){
			for(int j = 1; j <= 2; j++){
				Integer value = (Integer)dynaForm.get("mileage_range" + i + "Value" + j);				
				if (value < -99){
					dynaForm.set("mileage_range" + i + "Value" + j, new Integer(-99));
				}
				else if (value > 99){
					dynaForm.set("mileage_range" + i + "Value" + j, new Integer(99));
				}
			}
		}
		invBucketService.savePingIIMileageaAjdustmentBucketRanges( dynaForm );
		
		//save mileage bands only when coming as system setting
		if(mileagePerYearBucketDao != null){
			List<MileagePerYearBucket> marketPerYearBucketList = mileagePerYearBucketDao.findAll();
			for(int i = 1; i <= marketPerYearBucketList.size(); i++){
				marketPerYearBucketList.get(i - 1).setLowMileage((Integer)dynaForm.get("mileageLow" + i));
				//cannot set the last HighMileage value since it is assumed to go to inifinity
				if(i < 4){
					marketPerYearBucketList.get(i - 1).setHighMileage((Integer)dynaForm.get("mileageHigh" + i));
				}	
			}
			mileagePerYearBucketDao.makePersistent(marketPerYearBucketList);
		}
		
		return mapping.findForward("success");
	}

	public void setInvBucketService(InventoryBucketRangeService invBucketService) {
		this.invBucketService = invBucketService;
	}

	public void setMileagePerYearBucketDao(
			MileagePerYearBucketDao mileagePerYearBucketDao) {
		this.mileagePerYearBucketDao = mileagePerYearBucketDao;
	}

	public void setDealerPreferencePricingDao(
			DealerPreferencePricingDao dealerPreferencePricingDao) {
		this.dealerPreferencePricingDao = dealerPreferencePricingDao;
	}

}
