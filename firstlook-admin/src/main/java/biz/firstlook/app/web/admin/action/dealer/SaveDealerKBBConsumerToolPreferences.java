package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.model.imt.DealerPreferenceKBBConsumerTool;
import biz.firstlook.persist.imt.DealerPreferenceKBBConsumerToolDao;

public class SaveDealerKBBConsumerToolPreferences extends Action {
	DealerPreferenceKBBConsumerToolDao kbbConsumerToolDao;
	private DealerDao dealerDao;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Dealer dealer = dealerDao.findById( Integer.parseInt(request.getParameter("id")));
		
		DynaActionForm dynaForm = (DynaActionForm)form;
		
		DealerPreferenceKBBConsumerTool kbbConsumerToolPreferences = dealer.getKBBConsumerToolPreferences();
		
		if(kbbConsumerToolPreferences == null){
			kbbConsumerToolPreferences = new DealerPreferenceKBBConsumerTool();
		}
		
		kbbConsumerToolPreferences.setShowRetail((Boolean)dynaForm.get("showRetail"));
		kbbConsumerToolPreferences.setShowTradein((Boolean)dynaForm.get("showTradeIn"));
		kbbConsumerToolPreferences.setBusinessUnitId(dealer.getId());
		
		dealer.setKBBConsumerToolPreferences(kbbConsumerToolPreferences);
		
		dealerDao.makePersistent(dealer);
		
		
		request.setAttribute("showRetail", kbbConsumerToolPreferences.getShowRetail());
		request.setAttribute("showTradeIn",kbbConsumerToolPreferences.getShowTradeIn());
		return mapping.findForward("success");
	}
	
	public DealerPreferenceKBBConsumerToolDao getKBBConsumerToolPreferencesDao() {
		return kbbConsumerToolDao;
	}

	public void setKBBConsumerToolPreferencesDao(
			DealerPreferenceKBBConsumerToolDao kbbConsumerToolPreferencesDao) {
		this.kbbConsumerToolDao = kbbConsumerToolPreferencesDao;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}
}
