package biz.firstlook.app.web.common.struts.action.db;

import java.io.Serializable;

import biz.firstlook.persist.common.GenericDAO;

public class PersistForm extends biz.firstlook.app.web.common.struts.action.PersistForm {
	
	private GenericDAO genericDAO;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void persist(Object bean) {
		getGenericDAO().makePersistent( bean );
	}
	
	public GenericDAO getGenericDAO() {
		return genericDAO;
	}


	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Object loadObject(Serializable id) {
		return getGenericDAO().findById(id,false);
	}
	
}