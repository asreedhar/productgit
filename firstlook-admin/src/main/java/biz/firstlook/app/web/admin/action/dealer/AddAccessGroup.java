package biz.firstlook.app.web.admin.action.dealer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.admin.action.member.EditAlert;
import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.ATCAccessGroup;
import biz.firstlook.model.imt.AccessGroup;
import biz.firstlook.persist.imt.ATCAccessGroupDao;
import biz.firstlook.persist.imt.AccessGroupDao;

public class AddAccessGroup extends Load {

    private Logger logger = Logger.getLogger(EditAlert.class);
    private ATCAccessGroupDao atcAccessGroupDao;
    private AccessGroupDao accessGroupDao;
    
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		
		Integer id = new Integer( request.getParameter("id") );
		request.setAttribute("dealerId", id );

		// jsut display the access group info 
		if( StringUtils.isBlank( request.getParameter("accessGroupId") ) ) {
			return;
		}

        Integer newAccessGroupId = new Integer( request.getParameter( "accessGroupId" ) );
        
        logger.debug( "Loading access groups for business unit " + id );
        List<ATCAccessGroup> accessGroups = atcAccessGroupDao.findByBusinessUnitId(id);
        
        ATCAccessGroup newGroup = new ATCAccessGroup();
        newGroup.setAccessGroupId( newAccessGroupId );
        newGroup.setBusinessUnitId( id );
        accessGroups.add( newGroup );
        atcAccessGroupDao.makePersistent( accessGroups );
        
        request.setAttribute("activeAccessGroups", retrieveActiveAcessGroups(id) );
        
        return;
    }

	@Override
	protected Object loadObject(Serializable id) {
		return retrieveActiveAcessGroups(id);
	}


	private List<AccessGroup> retrieveActiveAcessGroups(Serializable id) {
		List<ATCAccessGroup> atcGroups = atcAccessGroupDao.findByBusinessUnitId((Integer)id);
		List<AccessGroup> accessGroups = accessGroupDao.findAll();
		List<AccessGroup> toBeReturned = new ArrayList<AccessGroup>();
		for ( ATCAccessGroup atcGroup : atcGroups ) {
			for ( AccessGroup accessGroup : accessGroups ) {
				if (atcGroup.getAccessGroupId().equals(accessGroup.getId() ) ) {
					toBeReturned.add( accessGroup );
				}
			}
		}
		return toBeReturned;
	}
	
	public void setAtcAccessGroupDao(ATCAccessGroupDao atcAccessGroupDao) {
        this.atcAccessGroupDao = atcAccessGroupDao;
    }

	public void setAccessGroupDao(AccessGroupDao accessGroupDao) {
		this.accessGroupDao = accessGroupDao;
	}
    
}
