package biz.firstlook.app.web.common.struts.action;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.util.PopulateFormUtil;

public abstract class PopulateForm extends Load {

	private static Logger log = Logger.getLogger( PopulateForm.class );
	
	private String targetProperty;
	
	@Override
	protected void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		//exception is thrown when a property exists on the form but not on the bean.
		//I don't care if the form wants a property that doesn't exist in my model!
		//something else might fill the form in afterwards!
    	try {
    		PopulateFormUtil.copyBeanToForm(form, request, getResultAttribute(), getTargetProperty(), getParamName() );
		} catch (IllegalAccessException e) {
			log.debug( "Attempt to illegally access a property or object in PersistForm", e );
		} catch (InvocationTargetException e) {
			log.debug( "warning invoke target in PersistForm");
		}
    }


	public String getTargetProperty() {
		return targetProperty;
	}

	public void setTargetProperty(String targetProperty) {
		this.targetProperty = targetProperty;
	}
    
    

}
