package biz.firstlook.app.web.admin.action.member;

import java.util.Arrays;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.api.subscribe.MemberSubscription;
import biz.firstlook.api.subscribe.SubscribeService;
import biz.firstlook.app.web.common.struts.util.ForwardUtil;

public class SaveAlert extends Action {

	private Logger logger = Logger.getLogger( SaveAlert.class );
	
	private SubscribeService subscribeService;
	
	@SuppressWarnings("unchecked")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		Integer memberId = new Integer( request.getParameter("memberId") );
		Integer subscriptionTypeId = new Integer( request.getParameter("subscriptionTypeId") );
		
		logger.debug( "Loader member subscription by memberId: " + memberId + ", and subscriptionTypeId: " + subscriptionTypeId );
		MemberSubscription subscription = subscribeService.retrieve( memberId, subscriptionTypeId );
		request.setAttribute( "subscription", subscription );
	
		logger.debug( "Copying Form to Subscription" );
		DynaActionForm dForm = (DynaActionForm) form;
		subscription.setMemberId( (Integer) dForm.get("memberId") );
		subscription.setSubscriptionTypeId( (Integer) dForm.get("subscriptionTypeId") );
		subscription.setSubscriptionFrequencyDetailId( (Integer) dForm.get("subscriptionFrequencyDetailId") );
		
		
		Integer[] deliveryTypeIds = (Integer[]) dForm.get("subscriptionDeliveryTypeIds");
		subscription.setSubscriptionDeliveryTypeIds( new HashSet(Arrays.asList( deliveryTypeIds )) );
		
		Integer[] businessUnitIds = (Integer[]) dForm.get("businessUnitIds");
		subscription.setBusinessUnitIds( new HashSet(Arrays.asList( businessUnitIds ) ) );
		
//				PropertyUtils.copyProperties( subscription, form );

		logger.debug( "Saving subscription" );
		subscribeService.save( subscription );
	
		ActionForward forward = ForwardUtil.addParam( mapping.findForward("success"), 
				new String[] { "memberId", "subscriptionTypeId" },
				new String[] { request.getParameter("memberId"), request.getParameter("subscriptionTypeId") } );
		return forward;
	}

	public SubscribeService getSubscribeService() {
		return subscribeService;
	}

	public void setSubscribeService(SubscribeService subscribeService) {
		this.subscribeService = subscribeService;
	}
	
	

}
