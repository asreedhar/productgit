package biz.firstlook.app.web.admin.action.group;

public enum TransferPriceRuleBeanType {
	UNIT_COST, FOR_RETAIL_ONLY;
}
