package biz.firstlook.app.web.admin.action.dealer;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.util.LabelValueBean;

import biz.firstlook.app.web.common.struts.action.PopulateForm;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.MarketMapping;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.MarketMappingDao;

public abstract class MarketMappingPopulateForm extends PopulateForm {

    protected MarketMappingDao marketMappingDao;

    public void setMarketMappingDao(MarketMappingDao marketMappingDao) {
	this.marketMappingDao = marketMappingDao;
    }

    private DealerDao dealerDao;

    @Override
    protected Object loadObject(Serializable id) {
	Integer dealerId = (Integer) id;
	return dealerDao.findById(dealerId);
    }

    public void setDealerDao(DealerDao dealerDao) {
	this.dealerDao = dealerDao;
    }

    @Override
    protected final void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws ServletException {
	// something magical is happening here to get the ajax on the page to
	// flip between display and input fields.
	super.afterLoad(mapping, form, request, response);

	Dealer dealer = (Dealer) loadObject(getIdentifier(form, request));

	// hack the form because fl-admin thought the fields on
	// businessUnitMarketDealer map
	// 1-to-1 to the form.
	DynaActionForm dynaForm = (DynaActionForm) form;
	dynaForm.set("selectedState", request.getParameter("selectedState"));
	dynaForm.set("selectedCounty", request.getParameter("selectedCounty"));

	afterLoad(request, dynaForm, dealer);

	response.setHeader("Cache-control", "no-cache");
    }

    protected abstract void afterLoad(HttpServletRequest request,
	    DynaActionForm form, Dealer dealer) throws ServletException;

    protected void populateForm(HttpServletRequest request, ActionForm form,
	    List<String> states, String selectedState, String selectedCounty,
	    Integer dealerNumber) throws IllegalAccessException,
	    InvocationTargetException, NoSuchMethodException {

	selectedState = getSelectedState(selectedState, states);

	List<String> counties = marketMappingDao.findCounties(selectedState);
	request.setAttribute("counties", counties.toArray(new String[counties
		.size()]));

	selectedCounty = getSelectedCounty(selectedCounty, counties);

	// need the list of these to populate the page.
	List<MarketMapping> marketMappings = marketMappingDao.find(
		selectedState, selectedCounty);
	List<LabelValueBean> marketMappingOptions = new ArrayList<LabelValueBean>();
	for (MarketMapping mMapping : marketMappings) {
	    marketMappingOptions.add(new LabelValueBean(mMapping.getName(),
		    mMapping.getId().toString()));
	}
	request.setAttribute("marketMappings", marketMappingOptions);

	dealerNumber = getDealerNumber(form, marketMappings, dealerNumber);

	PropertyUtils.setProperty(form, "selectedState", selectedState);
	PropertyUtils.setProperty(form, "selectedCounty", selectedCounty);
	PropertyUtils.setProperty(form, "dealerNumber", dealerNumber);
    }

    private String getSelectedState(String selectedState, List<String> states)
	    throws IllegalAccessException, InvocationTargetException,
	    NoSuchMethodException {
	// default to first state if not set.
	if (StringUtils.isBlank(selectedState)) {
	    if (states != null && !states.isEmpty()) {
		selectedState = states.get(0);
	    }
	}
	return selectedState;
    }

    private String getSelectedCounty(String selectedCounty,
	    List<String> counties) throws IllegalAccessException,
	    InvocationTargetException, NoSuchMethodException {
	boolean countyInList = counties.contains(selectedCounty);
	// if no county set, or county specified isn't in the county list
	// provide a default.
	if (StringUtils.isBlank(selectedCounty) || !countyInList) {
	    if (counties != null && !counties.isEmpty()) {
		selectedCounty = counties.get(0);
	    }
	}

	return selectedCounty;
    }

    private Integer getDealerNumber(ActionForm form,
	    List<MarketMapping> marketMappings, Integer dealerNumber)
	    throws IllegalAccessException, InvocationTargetException,
	    NoSuchMethodException {

	boolean numberInMapping = false;
	for (MarketMapping mapping : marketMappings) {
	    if (mapping.getId().equals(dealerNumber)) {
		numberInMapping = true;
		break;
	    }
	}

	// default to first dealer mapping if no dealerNumber, or
	// if the dealerNumber isn't in the list of possible mappings.
	if (dealerNumber == null || !numberInMapping) {
	    if (marketMappings != null && !marketMappings.isEmpty()) {
		MarketMapping marketMapping = marketMappings.get(0);
		dealerNumber = marketMapping.getId();
	    }
	}
	return dealerNumber;
    }
}
