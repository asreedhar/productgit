package biz.firstlook.app.web.admin.action.group;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.ref.BusinessUnitType;
import biz.firstlook.persist.imt.DealerGroupDao;

public class GroupCreate extends Action {

	private DealerGroupDao dealerGroupDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		DealerGroup dealerGroup = new DealerGroup();
		
		dealerGroup.setCode( request.getParameter( "code" ) );
		dealerGroup.setShortName( request.getParameter( "shortName" ) );
		dealerGroup.setName( request.getParameter( "name" ) );
		dealerGroup.setActive(true);
		dealerGroup.setType( BusinessUnitType.DealerGroup );
		dealerGroup.setPhone( request.getParameter( "phone" ) );
		dealerGroup.setFax( request.getParameter( "fax" ) );
		
		DealerGroup savedDealerGroup = dealerGroupDao.makePersistent( dealerGroup );
		request.setAttribute( "group", savedDealerGroup );
		return mapping.findForward("success");
	}

	public DealerGroupDao getDealerGroupDao() {
		return dealerGroupDao;
	}

	public void setDealerGroupDao(DealerGroupDao dealerGroupDao) {
		this.dealerGroupDao = dealerGroupDao;
	}
}