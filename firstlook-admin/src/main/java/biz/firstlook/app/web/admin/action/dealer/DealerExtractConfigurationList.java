package biz.firstlook.app.web.admin.action.dealer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.DealerExtractConfigurationDao;

public class DealerExtractConfigurationList extends Action {

	private DealerDao dealerDao;
	private DealerExtractConfigurationDao dealerExtractConfigurationDao;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Integer dealerId = Integer.parseInt(request.getParameter("id"));
		Dealer dealer = dealerDao.findById(dealerId);
		request.setAttribute("dealer", dealer);
		
		List<String> destinations = dealerExtractConfigurationDao.findDestinations();
		request.setAttribute("destinations", destinations);
		
		return mapping.findForward("success");
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}
	
	public void setDealerExtractConfigurationDao(
			DealerExtractConfigurationDao dealerExtractConfigurationDao) {
		this.dealerExtractConfigurationDao = dealerExtractConfigurationDao;
	}
}
