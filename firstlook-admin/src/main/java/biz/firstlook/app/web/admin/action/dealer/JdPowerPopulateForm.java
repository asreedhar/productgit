package biz.firstlook.app.web.admin.action.dealer;

import java.io.Serializable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.PopulateForm;
import biz.firstlook.model.imt.ref.JdPowerRegion;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.JdPowerRegionDao;

public class JdPowerPopulateForm extends PopulateForm
{

private DealerDao dealerDao;
private JdPowerRegionDao jdPowerRegionDao;

@Override
protected Object loadObject( Serializable id ) throws ServletException
{
	Integer dealerId = (Integer) id;
	return dealerDao.findById(dealerId);
}

@Override
protected final void afterLoad(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws ServletException {
	
	super.afterLoad( mapping, form, request, response );
	
	List<JdPowerRegion> regions = jdPowerRegionDao.findAll();
	request.setAttribute( "regions", regions );
}

public DealerDao getDealerDao()
{
	return dealerDao;
}

public void setDealerDao( DealerDao dealerDao )
{
	this.dealerDao = dealerDao;
}

public JdPowerRegionDao getJdPowerRegionDao()
{
	return jdPowerRegionDao;
}

public void setJdPowerRegionDao( JdPowerRegionDao jdPowerRegionDao )
{
	this.jdPowerRegionDao = jdPowerRegionDao;
}

}
