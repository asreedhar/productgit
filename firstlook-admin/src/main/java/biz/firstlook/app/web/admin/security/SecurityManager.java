package biz.firstlook.app.web.admin.security;

import biz.firstlook.app.web.admin.AdminSession;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.ref.MemberType;
import biz.firstlook.persist.imt.MemberDao;

public class SecurityManager {

	private MemberDao memberDao;
	
	public SecurityManager() {}

	public AdminSession createAdminSession(String login) {
		AdminSession adminSession = new AdminSession();
		
		Member member = memberDao.byLogin( login );
		adminSession.setMemberId( member.getId() );
		adminSession.setFirstName( member.getFirstName() );
		adminSession.setLastName( member.getLastName() );
		adminSession.setLogin( member.getLogin() );
		adminSession.setMemberType( member.getMemberType() );
		
		return adminSession;
	}
	
	public static boolean isAdmin(AdminSession adminSession) {
		return adminSession.getMemberType() == MemberType.ADMIN;
	}
	
	public MemberDao getMemberDao() {
		return memberDao;
	}

	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}
	
	
}
