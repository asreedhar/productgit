package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.PhotoProviderDealership;
import biz.firstlook.persist.imt.PhotoProviderDealershipDao;

public class AddPhotoProviderDealership extends LoadDealer {

    PhotoProviderDealershipDao photoProviderDealershipDao;

    @Override
    public void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response,
	    Dealer dealer) throws ServletException {

	Integer id = dealer.getId();
	request.setAttribute("dealerId", id);
	if (StringUtils.isBlank(request.getParameter("photoProviderId"))) {
	    request.setAttribute("addedPhotoProviders",
		    photoProviderDealershipDao.findByBusinessUnitId(id));
	    return;
	}

	String photoProviderIdStr = (String) request
		.getParameter("photoProviderId");
	Integer photoProviderId = new Integer(photoProviderIdStr);

	String code = null;
	String checked = (String) request.getParameter("codeType");
	if (checked.equals("businessUnitCode")) {
	    code = dealer.getCode();
	} else {
	    code = (String) request.getParameter("code");
	}
	Integer urlType = new Integer(request.getParameter("urlType"));

	PhotoProviderDealership ppdexample = new PhotoProviderDealership();
	ppdexample.setBusinessUnitId(id);
	ppdexample.setPhotoProviderId(photoProviderId);
	String[] excludes = { "photoProviderDealershipCode",
		"photoStorageCode_ovverride" };

	PhotoProviderDealership ppd = photoProviderDealershipDao
		.findOneByExample(ppdexample, excludes);

	if (ppd == null) {
	    ppd = new PhotoProviderDealership();
	    ppd.setBusinessUnitId(id);
	    ppd.setPhotoProviderId(photoProviderId);
	}
	ppd.setPhotoProvidersDealershipCode(code);
	ppd.setPhotoStorageCodeOverride(urlType);
	photoProviderDealershipDao.makePersistent(ppd);
	request.setAttribute("addedPhotoProviders", photoProviderDealershipDao
		.findByBusinessUnitId(id));
    }

    public PhotoProviderDealershipDao getPhotoProviderDealershipDao() {
	return photoProviderDealershipDao;
    }

    public void setPhotoProviderDealershipDao(
	    PhotoProviderDealershipDao photoProviderDealershipDao) {
	this.photoProviderDealershipDao = photoProviderDealershipDao;
    }
}
