package biz.firstlook.app.web.admin;
import java.io.Serializable;

import biz.firstlook.model.imt.ref.MemberType;

public class AdminSession implements Serializable {

	private static final long serialVersionUID = 3996060338097546518L;

	public static final String ADMIN_SESSION = "adminSession";
		
	private Integer memberId;
	private String login;
	private String firstName;
	private String lastName;
	private MemberType memberType;
	
	public AdminSession() {}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public MemberType getMemberType() {
		return memberType;
	}

	public void setMemberType(MemberType memberType) {
		this.memberType = memberType;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
}
