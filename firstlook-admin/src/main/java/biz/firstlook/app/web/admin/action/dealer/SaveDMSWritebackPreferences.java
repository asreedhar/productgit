package biz.firstlook.app.web.admin.action.dealer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.DMSExportPreference;
import biz.firstlook.model.imt.DMSExportPriceMapping;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.ThirdPartyEntity;
import biz.firstlook.persist.imt.DMSExportPriceMappingDao;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.ThirdPartyEntityDao;

public class SaveDMSWritebackPreferences extends Action {
	
	private DMSExportPriceMappingDao dmsExportPriceMappingDao;
	private DealerDao dealerDao;
	private ThirdPartyEntityDao thirdPartyEntityDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		Dealer dealer = dealerDao.findById( Integer.parseInt(request.getParameter("id")));
		updateDealerDMSSettings( dealer, (DynaActionForm)form );
		dealerDao.makePersistent(dealer);
		
		return mapping.findForward("success");
	}

	private void updateDealerDMSSettings(Dealer dealer, DynaActionForm form) {
		Integer incomingExportId = (Integer)form.get("exportId");
		DMSExportPreference dmsPref = null;

		if (dealer.getDmsExportPreferences() != null && !dealer.getDmsExportPreferences().isEmpty()) {
			for (DMSExportPreference pref : dealer.getDmsExportPreferences()) {
				if (pref.getDmsExportId().intValue() == incomingExportId.intValue()) {
					dmsPref = pref;
				}
			}
		}
		if (dmsPref == null) {
			dmsPref = new DMSExportPreference();
			dmsPref.setDealer(dealer);
			if (dealer.getDmsExportPreferences() == null) {
				Set<DMSExportPreference> prefs = new HashSet<DMSExportPreference>();
				dealer.setDmsExportPreferences( prefs );			
			}				
			dealer.getDmsExportPreferences().add(dmsPref);
		}
		dmsPref.setActive( (form.get("isActive") == null) ? false : true );
		dmsPref.setDmsExportId( incomingExportId);
		dmsPref.setClientSystemId((String)form.get("clientSystemId") + "");
		dmsPref.setIpAddress(form.get("ipAddress") + "");
		dmsPref.setDialUpPhone1(form.get("dialUpPhone1") + "" );
		dmsPref.setDialUpPhone2(form.get("dialUpPhone2") + "" );
		dmsPref.setLogin(form.get("login") + "" );
		dmsPref.setPassword(form.get("password") + "" );
		dmsPref.setAccountLogon(form.get("accountLogon") + "" );
		dmsPref.setFinanceLogon(form.get("financeLogon") + "" );
		dmsPref.setAreaNumber( (Integer)form.get("areaNumber"));
		dmsPref.setStoreNumber( (Integer)form.get("storeNumber"));
		dmsPref.setBranchNumber((Integer)form.get("branchNumber"));
		dmsPref.setFrequencyId((Integer)form.get("frequencyId"));
		dmsPref.setDailyExportHourOfDay((Integer)form.get("dailyExportHourOfDay"));
		dmsPref.setDealerName(form.get("dealerName") + "");
		dmsPref.setDealerPhoneOffice(form.get("dealerPhoneOffice") + "");
		dmsPref.setDealerPhoneCell(form.get("dealerPhoneCell") + "");
		dmsPref.setDealerEmail(form.get("dealerEmail") + "");
		
		if ((form.get("reynoldsDealerNo") != null) && (form.get("reynoldsDealerNo").equals("") == false))
		{
			dmsPref.setReynoldsDealerNo(form.get("reynoldsDealerNo") + "");
		}
		
		int internetMappingId = Integer.parseInt(form.get("internetPriceMappingId") + "");
		if ( internetMappingId > -1) {
			DMSExportPriceMapping internetMapping = dmsExportPriceMappingDao.findById( internetMappingId );
			dmsPref.setInternetPricingMapping(internetMapping);
		} else {
			dmsPref.setInternetPricingMapping( null );
		}

		int lotPriceMappingId = Integer.parseInt(form.get("lotPriceMappingId") + "");
		if ( lotPriceMappingId > -1) {
			DMSExportPriceMapping windowStickerMapping = dmsExportPriceMappingDao.findById(lotPriceMappingId);
			dmsPref.setLotPriceMapping(windowStickerMapping);
		} else {
			dmsPref.setLotPriceMapping(null);
		}
		
		
		// clean up any mutually exclusive prefs 
		// ... this just sucks.
		List<DMSExportPreference> toRemove = new ArrayList<DMSExportPreference>();
		for (DMSExportPreference preference : dealer.getDmsExportPreferences()) {
			if (preference.getDmsExportId().intValue() != incomingExportId.intValue()) {
				ThirdPartyEntity existingName = thirdPartyEntityDao.findById(preference.getDmsExportId());
				ThirdPartyEntity newName = thirdPartyEntityDao.findById(incomingExportId);
				if (newName.getName().indexOf("SIS") > -1 && existingName.getName().indexOf("SIS") > -1) {
					toRemove.add(preference);
				}
			}
		}
		for (DMSExportPreference remove : toRemove) {
			dealer.getDmsExportPreferences().remove(remove);
			remove = null;
		}
		
		
		
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}

	public void setDmsExportPriceMappingDao(
			DMSExportPriceMappingDao dmsExportPriceMappingDao) {
		this.dmsExportPriceMappingDao = dmsExportPriceMappingDao;
	}

	public void setThirdPartyEntityDao(ThirdPartyEntityDao thirdPartyEntityDao) {
		this.thirdPartyEntityDao = thirdPartyEntityDao;
	}
}
