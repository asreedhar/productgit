package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.GlobalParam;
import biz.firstlook.model.imt.ref.ExportType;
import biz.firstlook.persist.imt.GlobalParamDao;

public class EditExportSettings extends Action {
	private GlobalParamDao globalParamDao;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String exportTypeName = request.getParameter("exportType");
		ExportType exportType = ExportType.getExportTypeByName( exportTypeName );
		
		switch (exportType) { 
		case EBIZ:
			saveGlobalParam( GlobalParam.PARAM_EBIZID, request.getParameter(GlobalParam.PARAM_EBIZID) );
			saveGlobalParam( GlobalParam.PARAM_EBIZAUTH, request.getParameter(GlobalParam.PARAM_EBIZAUTH) );
			break;
		case SIS:
			saveGlobalParam( GlobalParam.PARAM_SIS, request.getParameter(GlobalParam.PARAM_SIS) );
			break;
		case REY_DIRECT:
			saveGlobalParam( GlobalParam.PARAM_REY_DIRECT, request.getParameter(GlobalParam.PARAM_REY_DIRECT) );
			break;
	}
		
		

		
		request.setAttribute("message", "Your changes have been saved");
		return mapping.findForward("success");
	}
	
	private GlobalParam saveGlobalParam( String name, String value ) {
		GlobalParam globalParam = globalParamDao.forName( name );
		if( globalParam == null )
			globalParam = new GlobalParam( name );
		globalParam.setValue( value );
		return globalParamDao.makePersistent( globalParam );
	}

	public void setGlobalParamDao(GlobalParamDao globalParamDao) {
		this.globalParamDao = globalParamDao;
	}

}
