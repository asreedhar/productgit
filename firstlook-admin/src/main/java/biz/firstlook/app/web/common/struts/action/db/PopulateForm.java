package biz.firstlook.app.web.common.struts.action.db;

import java.io.Serializable;

import biz.firstlook.persist.common.GenericDAO;

public class PopulateForm extends biz.firstlook.app.web.common.struts.action.PopulateForm {

	private GenericDAO genericDAO;

	@SuppressWarnings("unchecked")
	@Override
	protected Object loadObject(Serializable id) {
		return genericDAO.findById(id,false);
	}
	
	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}

}
