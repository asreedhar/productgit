package biz.firstlook.app.web.admin.action.member;

import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.Member;
import biz.firstlook.persist.imt.DealerDao;

/* Dry *>
 * 
 */
public class AddDealers extends Load {

	private DealerDao dealerDao;
	
	@Override
	protected Serializable getIdentifier(ActionForm form, HttpServletRequest request) throws ServletException {
		Integer id = new Integer( request.getParameter("id") );
		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String dealerId = request.getParameter("dealerId");

		Member member = (Member) request.getAttribute( getResultAttribute() );
			Dealer dealer = dealerDao.findById( Integer.parseInt( dealerId ), false );
			member.getDealers().add( dealer );
		getGenericDAO().makePersistent( member );
		
		return;
	}

	public DealerDao getDealerDao() {
		return dealerDao;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}
	
	

}
