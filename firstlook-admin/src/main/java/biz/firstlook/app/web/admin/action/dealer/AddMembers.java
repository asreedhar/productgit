package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.Member;
import biz.firstlook.persist.imt.MemberDao;

public class AddMembers extends LoadDealer {

    private MemberDao memberDao;

    @Override
    public void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response,
	    Dealer dealer) throws ServletException {
	String memberId = request.getParameter("memberId");

	Member member = memberDao.findById(Integer.parseInt(memberId), false);
	dealer.getMembers().add(member);
	getDealerDao().makePersistent(dealer);
    }

    public MemberDao getMemberDao() {
	return memberDao;
    }

    public void setMemberDao(MemberDao memberDao) {
	this.memberDao = memberDao;
    }

}
