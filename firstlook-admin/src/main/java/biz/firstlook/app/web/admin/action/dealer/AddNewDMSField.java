package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.DMSExportPriceMapping;
import biz.firstlook.persist.imt.DMSExportPriceMappingDao;

public class AddNewDMSField extends Action {

	private DMSExportPriceMappingDao dmsExportPriceMappingDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DMSExportPriceMapping newMapping = new DMSExportPriceMapping( request.getParameter("newFieldName") );
		newMapping = dmsExportPriceMappingDao.makePersistent( newMapping );
		
		response.addHeader("newMappingDesc", newMapping.getDescription() + "");
		response.addHeader("newMappingId", newMapping.getId() + "");
		
		return null;
	}

	public void setDmsExportPriceMappingDao(
			DMSExportPriceMappingDao dmsExportPriceMappingDao) {
		this.dmsExportPriceMappingDao = dmsExportPriceMappingDao;
	}
	
}
