package biz.firstlook.app.web.common.struts.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import biz.firstlook.app.web.common.struts.action.helper.AfterLoadEvent;

public class AfterLoadEventChain implements Serializable {
	
	private static final long serialVersionUID = -3664681859566547897L;
	
	private List<AfterLoadEvent> events = new ArrayList<AfterLoadEvent>();

	public boolean add(AfterLoadEvent event) {
		return events.add(event);
	}

	public void clear() {
		events.clear();
	}

	public int size() {
		return events.size();
	}

	public boolean addAll(Collection<AfterLoadEvent> afterLoadEvents) {
		return events.addAll(afterLoadEvents);
	}

	public void processEventChain(ActionForm form, HttpServletRequest request)
			throws ServletException {

		for (AfterLoadEvent event : events) {
			event.handleEvent(form, request);
		}
	}
}
