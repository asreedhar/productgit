package biz.firstlook.app.web.admin.action.group;

import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.Member;
import biz.firstlook.persist.imt.MemberDao;

public class AddMembers extends Load {

	private Log log = LogFactory.getLog( AddMembers.class );
	private MemberDao memberDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String memberId = request.getParameter("memberId");

		DealerGroup dealerGroup = (DealerGroup) request.getAttribute( getResultAttribute() );
		Member member = memberDao.findById( Integer.parseInt( memberId ) );
		Collection<String> errors = dealerGroup.add( member );
		if( !errors.isEmpty() ) { 
			log.warn( errors );
		}
		
		getGenericDAO().makePersistent( dealerGroup );
	}

	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}

	
	

}
