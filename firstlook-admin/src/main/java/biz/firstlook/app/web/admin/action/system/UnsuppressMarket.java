package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.GroupingDescription;
import biz.firstlook.persist.imt.GroupingDescriptionDao;

public class UnsuppressMarket extends Action {

	private GroupingDescriptionDao groupingDescriptionDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String idStr = request.getParameter("id");
		
		GroupingDescription gdesc = groupingDescriptionDao.findById( Integer.parseInt(idStr), false);
		gdesc.setSuppress(false);
		groupingDescriptionDao.makePersistent( gdesc );
		
		return mapping.findForward("success");
	}

	public GroupingDescriptionDao getGroupingDescriptionDao() {
		return groupingDescriptionDao;
	}

	public void setGroupingDescriptionDao(
			GroupingDescriptionDao groupingDescriptionDao) {
		this.groupingDescriptionDao = groupingDescriptionDao;
	}
}
