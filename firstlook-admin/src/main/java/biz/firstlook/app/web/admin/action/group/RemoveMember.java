package biz.firstlook.app.web.admin.action.group;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.Member;
import biz.firstlook.persist.imt.MemberDao;

public class RemoveMember extends Load {

	private MemberDao memberDao;

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String memberIdStr = (String) request.getParameter( "memberId" );
		Integer memberId = new Integer( memberIdStr );
		DealerGroup dealerGroup = (DealerGroup) request.getAttribute( getResultAttribute() );
		Member toRemove = memberDao.findById( memberId );
		if( toRemove != null ) {
			dealerGroup.remove( toRemove );
		}
		getGenericDAO().makePersistent( dealerGroup );
	}

	public void setMemberDao( MemberDao memberDao )
	{
		this.memberDao = memberDao;
	}
}
