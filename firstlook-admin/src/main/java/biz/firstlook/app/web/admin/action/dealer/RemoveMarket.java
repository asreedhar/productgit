package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.LoadByFilter;
import biz.firstlook.model.imt.MarketToZip;
import biz.firstlook.model.imt.MarketToZipPK;

public class RemoveMarket extends LoadByFilter {

	@SuppressWarnings("unchecked")
	@Override
	public void beforeLoad(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response, Object example) throws Exception {
		String businessUnitId = request.getParameter( "businessUnitId" );
		String ring = request.getParameter("ring");
		String zipcode = request.getParameter("zipcode");
		
		PropertyUtils.setProperty( example, "zipcode", null );
		
		MarketToZipPK exampleMarket = new MarketToZipPK();
		BeanUtils.setProperty( exampleMarket, "businessUnitId", businessUnitId );
		BeanUtils.setProperty( exampleMarket, "ring", ring );
		BeanUtils.setProperty( exampleMarket, "zipcode", zipcode );

		MarketToZip marketToZip = (MarketToZip) getGenericDAO().findById( exampleMarket, false );
		getGenericDAO().makeTransient( marketToZip );
		return;
	}

}
