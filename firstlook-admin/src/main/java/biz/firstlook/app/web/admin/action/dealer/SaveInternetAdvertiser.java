package biz.firstlook.app.web.admin.action.dealer;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.LoadByFilter;
import biz.firstlook.app.web.common.struts.util.PersistFormUtil;
import biz.firstlook.model.imt.InternetAdvertiserDealership;

public class SaveInternetAdvertiser extends LoadByFilter{
    @SuppressWarnings("unchecked")
    @Override
    public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, Object example) throws Exception {
        Object bean = request.getAttribute( getResultAttribute() );
        if( bean instanceof Collection ) {
            // We've been handed an empty list
            bean = new InternetAdvertiserDealership();
        } 
        PersistFormUtil.copyFormToBean( form, bean, (String) null, (String) null, null);
        InternetAdvertiserDealership advertiser = (InternetAdvertiserDealership) bean;
        
        if (!advertiser.getActive()) {
            updateAdvertiser(advertiser);
        }
        if ((advertiser.getStatus() != null && !advertiser.getStatus()) || advertiser.getExporting() == null) {
            advertiser.setExporting(Boolean.FALSE);
        }
        
        if ((advertiser.getStatus() != null && !advertiser.getStatus()) || advertiser.getIncrementalExport() == null) {
            advertiser.setIncrementalExport(Boolean.FALSE);
        }
        
        getGenericDAO().makePersistent( bean );
    }

    //  If it was set to inactive clear everything.
    private void updateAdvertiser(InternetAdvertiserDealership advertiser) {
        advertiser.setContactEmail(null);
        advertiser.setDealerCode(null);
        advertiser.setNotes(null);
        advertiser.setContactLastName(null);
        advertiser.setContactFirstName(null);
        advertiser.setContactPhoneNumber(null);
        advertiser.setStatus(null);
        advertiser.setVerifiedBy(null);
        advertiser.setVerifiedDate(null);
        advertiser.setExporting(Boolean.FALSE);
        advertiser.setIncrementalExport( Boolean.FALSE );
    }
}
