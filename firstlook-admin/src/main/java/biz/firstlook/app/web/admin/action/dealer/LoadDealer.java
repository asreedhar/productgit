package biz.firstlook.app.web.admin.action.dealer;

import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.common.GenericDAO;
import biz.firstlook.persist.imt.DealerDao;

/**
 * This class is intended to be an incremental refactor to the whole fl-admin
 * Action structure. This should be the base class when needing information from
 * the Dealer.
 * 
 * @author bfung
 * 
 */
public class LoadDealer extends Load {

	private static final String DEPRECATED = "This method is deprecated and unsupported from this context.";

	private DealerDao dealerDao;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Dealer dealer = retrieveDealer(form, request);
		afterLoad(mapping, form, request, response, dealer);
		getAfterLoadEventChain().processEventChain(form, request);
		return findForward(mapping, form, request, dealer);
	}

	/**
	 * Override super class method and made final. Do not use this method
	 * anymore.
	 * 
	 * @deprecated
	 * @throws UnsupportedOperationException
	 */
	protected final void retrieveObject(ActionForm form,
			HttpServletRequest request) throws ServletException {
		throw new UnsupportedOperationException(DEPRECATED);
	}

	/**
	 * Retrieves the dealer in a compatible way with the old code.
	 * 
	 * @param form
	 * @param request
	 * @return Dealer requested
	 * @throws ServletException
	 */
	private Dealer retrieveDealer(ActionForm form, HttpServletRequest request)
			throws ServletException {
		Integer dealerId = Integer
				.valueOf(request.getParameter(getParamName()));
		Dealer result = dealerDao.findById(dealerId);
		request.setAttribute(getResultAttribute(), result);
		return (Dealer) result;
	}

	/**
	 * Override super class method and made final. Do not use this method
	 * anymore.
	 * 
	 * @deprecated
	 * @throws UnsupportedOperationException
	 */
	protected final void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		throw new UnsupportedOperationException(DEPRECATED);
	}

	/**
	 * Replacement framework method for afterLoad(ActionMapping, ActionForm,
	 * HttpServletRequest, HttpServletResponse) with better context information.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @param dealer
	 */
	protected void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response,
			Dealer dealer) throws ServletException {
		super.afterLoad(mapping, form, request, response);
	}

	/**
	 * Override super class method and made final. Do not use this method
	 * anymore.
	 * 
	 * @deprecated
	 * @throws UnsupportedOperationException
	 */
	protected final Object loadObject(Serializable id) {
		throw new UnsupportedOperationException(DEPRECATED);
	}

	/**
	 * Override super class method and made final. Do not use this method
	 * anymore.
	 * 
	 * @deprecated
	 * @throws UnsupportedOperationException
	 */
	protected final Serializable getIdentifier(ActionForm form,
			HttpServletRequest request) throws ServletException {
		throw new UnsupportedOperationException(DEPRECATED);
	}

	/**
	 * Added Dealer to method signature wrapping super class method.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param dealer
	 * @return
	 * @throws ServletException
	 */
	protected ActionForward findForward(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, Dealer dealer) throws ServletException {
		return super.findForward(mapping, form, request);
	}

	/**
	 * User context sensitive version instead.
	 * 
	 * @deprecated
	 */
	protected final ActionForward findForward(ActionMapping mapping,
			ActionForm form, HttpServletRequest request)
			throws ServletException {
		throw new UnsupportedOperationException(DEPRECATED);
	}

	/**
	 * Use the type specific DAO instead of super class DAO.
	 * 
	 * @deprecated
	 */
	public final GenericDAO<Dealer, Integer> getGenericDAO() {
		return dealerDao;
	}

	/**
	 * Set the specific DAO instead of super class DAO.
	 * 
	 * @deprecated
	 */
	@SuppressWarnings("unchecked")
	public final void setGenericDAO(GenericDAO genericDAO) {
		if (genericDAO instanceof DealerDao) {
			this.dealerDao = (DealerDao) genericDAO;
		} else {
			throw new IllegalArgumentException(
					"DealerLoad expects a DealerDao but got a "
							+ genericDAO.getClass().getName() + " instead.");
		}
	}

	public DealerDao getDealerDao() {
		return dealerDao;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}
}
