package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.api.invbucket.InventoryBucketRangeService;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreferencePricing;
import biz.firstlook.model.imt.ref.BucketRangeType;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.DealerPreferenceDao;
import biz.firstlook.persist.imt.DealerPreferencePricingDao;
import biz.firstlook.persist.imt.ThirdPartyDao;
import biz.firstlook.persist.market.MileagePerYearBucketDao;


public class LoadPingIISettings extends Action {

	public static final int FIRSTLOOK_BUSINESS_UNIT_ID = 100150; 

	
	private DealerDao dealerDao;
	private InventoryBucketRangeService invBucketService;
	private MileagePerYearBucketDao mileagePerYearBucketDao;
	private DealerPreferencePricingDao dealerPreferencePricingDao;
	private DealerPreferenceDao dealerPreferenceDao;
	private ThirdPartyDao thirdPartyDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Dealer dealer = null; // need this here to pop sub sections
		DealerPreferencePricing dealerPref = null;

		Integer dealerId = Integer.parseInt(request.getParameter("dealerId"));
		
		// dual purpose this action to also allow them to reset to the defaults on the load
		String action = request.getParameter("action");
		if (action != null && action.trim().equalsIgnoreCase("restoreDefault")) {
 			DealerPreferencePricing toDel = dealerPreferencePricingDao.findByDealerId(dealerId);
			if (toDel != null) {
				dealerPreferencePricingDao.makeTransient(toDel);
			}
			invBucketService.deleteAllBucketsForDealer(dealerId, BucketRangeType.PINGII_PRICING_AGE);
			invBucketService.deleteAllBucketsForDealer(dealerId, BucketRangeType.PINGII_MILEAGE_ADJUSTMENT);
		}
		
		
		DealerPreferencePricing systemPref = dealerPreferencePricingDao.findDefaultSystemDealerPreference();
		
		// if doing system wide changes need to go straight to the dealerPreference table
		if( dealerId.intValue() == FIRSTLOOK_BUSINESS_UNIT_ID) {
			dealer = dealerDao.findById(FIRSTLOOK_BUSINESS_UNIT_ID);
			dealerPref = systemPref;
		} else {
			dealer = dealerDao.findById(dealerId);
			dealerPref  = dealerPreferencePricingDao.findByDealerId(dealerId);
			if (dealerPref == null) {
				dealerPref = new DealerPreferencePricing();
				dealerPref.setId(dealerId);
			}
			if(dealerPref.getPingII_ExcludeHighPriceOutliersMultiplier() == null) {
				dealerPref.setPingII_ExcludeHighPriceOutliersMultiplier( systemPref.getPingII_ExcludeHighPriceOutliersMultiplier());
			}
			if(dealerPref.getPingII_ExcludeLowPriceOutliersMultiplier() == null) {
				dealerPref.setPingII_ExcludeLowPriceOutliersMultiplier( systemPref.getPingII_ExcludeLowPriceOutliersMultiplier());
			}
			if(dealerPref.getPingII_ExcludeNoPriceFromCalc() == null) {
				dealerPref.setPingII_ExcludeNoPriceFromCalc( systemPref.getPingII_ExcludeNoPriceFromCalc());
			}
			if(dealerPref.getPingII_DefaultSearchRadius() == null) {
				dealerPref.setPingII_DefaultSearchRadius(150);
			}
			if(dealerPref.getPingII_MaxSearchRadius() == null) {
				dealerPref.setPingII_MaxSearchRadius( systemPref.getPingII_MaxSearchRadius());
			}
			if(dealerPref.getPingII_SupressSellerName() == null) {
				dealerPref.setPingII_SupressSellerName( systemPref.getPingII_SupressSellerName());
			}
			if (dealerPref.getPingII_GuideBookId() == null) {
				dealerPref.setPingII_GuideBookId((dealerPreferenceDao.findByDealerId(dealerId).getGuideBookId()));
			}
			if (dealerPref.getPingII_PackageId() == null) {
				dealerPref.setPingII_PackageId(systemPref.getPingII_PackageId());
			}
			if (dealerPref.getPingII_MatchCertifiedByDefault() == null) {
				dealerPref.setPingII_MatchCertifiedByDefault(systemPref.getPingII_MatchCertifiedByDefault());
			}
			if (dealerPref.getPingII_SuppressTrimMatchStatus() == null) {
				dealerPref.setPingII_SuppressTrimMatchStatus(systemPref.getPingII_SuppressTrimMatchStatus());
			}
			if(dealerPref.getPingII_NewPing() == null)
			{
			dealerPref.setPingII_NewPing(systemPref.getPingII_NewPing());
			}
			if(dealerPref.getPingII_MarketListing()== null)
			{
			dealerPref.setPingII_MarketListing(systemPref.getPingII_MarketListing());
			}
			if(dealerPref.getPingII_RedRange1Value1()== null)
			{
			dealerPref.setPingII_RedRange1Value1(systemPref.getPingII_RedRange1Value1());
			}
			if(dealerPref.getPingII_RedRange1Value2()== null)
			{
			dealerPref.setPingII_RedRange1Value2(systemPref.getPingII_RedRange1Value2());
			}
			if(dealerPref.getPingII_YellowRange1Value1()== null)
			{
			dealerPref.setPingII_YellowRange1Value1(systemPref.getPingII_YellowRange1Value1());
			}
			if(dealerPref.getPingII_YellowRange1Value2()== null)
			{
			dealerPref.setPingII_YellowRange1Value2(systemPref.getPingII_YellowRange1Value2());
			}
			if(dealerPref.getPingII_YellowRange2Value1()== null)
			{
			dealerPref.setPingII_YellowRange2Value1(systemPref.getPingII_YellowRange2Value1());
			}
			if(dealerPref.getPingII_YellowRange2Value2()== null)
			{
			dealerPref.setPingII_YellowRange2Value2(systemPref.getPingII_YellowRange2Value2());
			}
			if(dealerPref.getPingII_GreenRange1Value1()== null)
			{
			dealerPref.setPingII_GreenRange1Value1(systemPref.getPingII_GreenRange1Value1());
			}
			if(dealerPref.getPingII_GreenRange1Value2()== null)
			{
			dealerPref.setPingII_GreenRange1Value2(systemPref.getPingII_GreenRange1Value2());
			}
		}
		
		if ( "edit".equals(request.getParameter("state"))) {
			request.setAttribute( "isEdit", Boolean.TRUE );
		}
		request.setAttribute("dealer", dealer );
		request.setAttribute("preference", dealerPref );
		request.setAttribute("guideBooks", thirdPartyDao.getBookOutThirdPartyForBusinessUnit(dealerId));
		
		request.setAttribute("age_ranges", invBucketService.getPingIIBucketRanges( dealerId ) );
		DynaBean pingIIMileageAdjustmentBucketRanges = invBucketService.getPingIIMileageAdjustmentBucketRanges( dealerId );
		request.setAttribute("mileage_ranges", pingIIMileageAdjustmentBucketRanges );
		
		if(mileagePerYearBucketDao != null){
			//load and save mileage bucket as system level, so for dealers this will be null
			request.setAttribute("mileagePerYearBuckets", mileagePerYearBucketDao.findAll());
		}
		
		
		
		return mapping.findForward("success");
	}

	public void setInvBucketService(InventoryBucketRangeService invBucketService) {
		this.invBucketService = invBucketService;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}

	public void setMileagePerYearBucketDao(
			MileagePerYearBucketDao mileagePerYearBucketDao) {
		this.mileagePerYearBucketDao = mileagePerYearBucketDao;
	}

	public void setDealerPreferencePricingDao(
			DealerPreferencePricingDao dealerPreferencePricingDao) {
		this.dealerPreferencePricingDao = dealerPreferencePricingDao;
	}

	public void setThirdPartyDao(ThirdPartyDao thirdPartyDao) {
		this.thirdPartyDao = thirdPartyDao;
	}

	public void setDealerPreferenceDao(DealerPreferenceDao dealerPreferenceDao) {
		this.dealerPreferenceDao = dealerPreferenceDao;
	}
	
	
	
}
