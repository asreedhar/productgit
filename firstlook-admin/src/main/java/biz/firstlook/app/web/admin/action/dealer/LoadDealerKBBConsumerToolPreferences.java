package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreferenceKBBConsumerTool;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.DealerPreferenceKBBConsumerToolDao;



public class LoadDealerKBBConsumerToolPreferences extends Action {

	DealerPreferenceKBBConsumerToolDao kbbConsumerToolPreferencesDao;
	private DealerDao dealerDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Dealer dealer = dealerDao.findById( Integer.parseInt(request.getParameter("id")) );
		
		DynaActionForm dynaForm = (DynaActionForm)form;
		DealerPreferenceKBBConsumerTool kbbConsumerToolPreferences = dealer.getKBBConsumerToolPreferences();
		if(kbbConsumerToolPreferences != null){
			dynaForm.set("showTradeIn", kbbConsumerToolPreferences.getShowTradeIn());
			dynaForm.set("showRetail",kbbConsumerToolPreferences.getShowRetail());
		}
		else{
			kbbConsumerToolPreferences = new DealerPreferenceKBBConsumerTool();
			dynaForm.set("showTradeIn", kbbConsumerToolPreferences.getShowTradeIn());
			dynaForm.set("showRetail",kbbConsumerToolPreferences.getShowRetail());
		}

		request.setAttribute("dealer", dealer);
		request.setAttribute("isEdit", Boolean.TRUE);
		return mapping.findForward("success");
	}

	public void setKBBConsumerToolPreferencesDao(
			DealerPreferenceKBBConsumerToolDao KBBConsumerToolPreferencesDao) {
		this.kbbConsumerToolPreferencesDao = KBBConsumerToolPreferencesDao;
	}
	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}
}
