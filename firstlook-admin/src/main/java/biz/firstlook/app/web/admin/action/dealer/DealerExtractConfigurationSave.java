package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.DealerExtractConfiguration;
import biz.firstlook.persist.imt.DealerExtractConfigurationDao;

import com.discursive.cas.extend.client.CASFilter;

public class DealerExtractConfigurationSave extends Action {

	private DealerExtractConfigurationDao dealerExtractConfigurationDao;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		DealerExtractConfigurationForm extractForm = (DealerExtractConfigurationForm)form;
		
		String user = null;
		HttpSession session = request.getSession(false);
		if(session != null) {
			Object login = session.getAttribute(CASFilter.CAS_FILTER_USER);
			if(login != null) {
				user = login.toString();
			}
		}
		DealerExtractConfiguration config = extractForm.getConfiguration(user);
		extractForm.populate(config);
		try {
			dealerExtractConfigurationDao.save(config);
		} catch (Exception e) {
			config = dealerExtractConfigurationDao.findBy(config.getDealerId(), config.getDestinationName());
		}
		
		request.setAttribute("configuration", config);
		request.setAttribute("isEdit", false);
		return mapping.findForward("success");
	}
	
	public void setDealerExtractConfigurationDao(
			DealerExtractConfigurationDao dealerExtractConfigurationDao) {
		this.dealerExtractConfigurationDao = dealerExtractConfigurationDao;
	}
}
