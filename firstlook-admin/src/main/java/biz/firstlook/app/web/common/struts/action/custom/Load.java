package biz.firstlook.app.web.common.struts.action.custom;

import java.io.Serializable;

import org.apache.commons.beanutils.MethodUtils;

public class Load extends biz.firstlook.app.web.common.struts.action.Load {
	
	private Object service;
	private String loadMethod;
	
	protected Object loadObject(Serializable id) {
		Object result;
		try {
			result = MethodUtils.invokeMethod( service, loadMethod, id );
		} catch( Exception e ) {
			throw new RuntimeException( "Error invoking " + loadMethod + " from " + service, e );
		}
		return result;
	}

	public String getLoadMethod() {
		return loadMethod;
	}

	public void setLoadMethod(String loadMethod) {
		this.loadMethod = loadMethod;
	}

	public Object getService() {
		return service;
	}

	public void setService(Object service) {
		this.service = service;
	}   
	
	
}