package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.Franchise;

public class AddFranchise extends LoadDealer {

    private static final Integer LEXUS_FRANCHISE_ID = 31;
    private static final Integer TOYOTA_FRANCHISE_ID = 53;

    @Override
    protected void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response,
	    Dealer dealer) {
	String franchiseIdStr = (String) request.getParameter("franchiseId");
	Integer franchiseId = new Integer(franchiseIdStr);
	Franchise franchise = new Franchise();
	franchise.setId(franchiseId);
	dealer.getFranchises().add(franchise);

	// if the franchise is Toyota or Lexus - enable TFS
	if (franchise.getId().equals(LEXUS_FRANCHISE_ID)
		|| franchise.getId().equals(TOYOTA_FRANCHISE_ID)) {
	    dealer.getPreference().setTfsEnabled(true);
	}

	getDealerDao().makePersistent(dealer);
    }

}
