package biz.firstlook.app.web.common.struts.action.helper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

public interface AfterLoadEvent {
    void handleEvent(ActionForm form, HttpServletRequest request) throws ServletException;
}
