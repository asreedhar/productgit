package biz.firstlook.app.web.common.struts.action;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public abstract class LoadByFilter extends Action {
	
	private String className;
	private String resultAttribute = "result";
	private String successForward = "success";
	private String excludes = "";
	private Boolean single = Boolean.FALSE;
	
    @SuppressWarnings("unchecked")
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

    	// Get the class
    	Class clazz = Class.forName( className );

    	// Create instance of class
    	Object o = clazz.newInstance();
    	
    	String excludesParam = request.getParameter("excludes");
    	if( !StringUtils.isEmpty( excludesParam ) ) {
    		excludes = excludesParam;
    	}
    	String[] excludeArray = StringUtils.split( excludes, "," );

    	// Cycle through the request parameters if they are bean properties
    	// populate on instance
    	Enumeration paramNames = request.getParameterNames();
    	while( paramNames.hasMoreElements() ) {
    		String paramName = (String) paramNames.nextElement();
    		if( !ArrayUtils.contains( excludeArray, paramName ) && 
    			PropertyUtils.isWriteable( o, paramName ) ) {
        		Class propertyType = PropertyUtils.getPropertyType( o, paramName );
        		String param = request.getParameter(paramName);
        		if( propertyType.isEnum() ) {
    				if( param instanceof String ) {
    					PropertyUtils.setProperty( o, paramName, Enum.valueOf( propertyType, (String) param ) );
    				} else {
    					throw new RuntimeException( "Cannot convert param " + param + " to Enum value for property: " + paramName );
    				}
	    		} else {
	    			BeanUtils.setProperty( o, paramName, request.getParameter(paramName) );
	    		}
    			request.setAttribute(paramName, request.getParameter(paramName));
    		}
    	}

    	beforeLoad(mapping,form,request,response,o);

    	
    	// Pass the instance to find by example
    	List results = loadByBean(o);
    	if( results != null && results.size() >= 1 && single.booleanValue() ) {
    		request.setAttribute( resultAttribute, results.get(0) );
    	} else if( results != null && results.size() < 1 && single.booleanValue() ) {
    		request.setAttribute( resultAttribute, Class.forName( className ).newInstance() );
    	} else {
    		request.setAttribute( resultAttribute, results );
    	}
    	afterLoad(mapping,form,request,response,o);
		return findForward(mapping,form,request);
	}

	protected abstract List loadByBean(Object o);
    
    public void beforeLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response, Object example) throws Exception {
    }

    public void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response, Object example) throws Exception {
    }

	public ActionForward findForward(ActionMapping mapping, ActionForm form,
			HttpServletRequest request) {
		return mapping.findForward(successForward);
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getResultAttribute() {
		return resultAttribute;
	}

	public void setResultAttribute(String resultAttribute) {
		this.resultAttribute = resultAttribute;
	}

	public String getSuccessForward() {
		return successForward;
	}

	public void setSuccessForward(String successForward) {
		this.successForward = successForward;
	}

	public String getExcludes() {
		return excludes;
	}

	public void setExcludes(String excludes) {
		this.excludes = excludes;
	}

	public Boolean getSingle() {
		return single;
	}

	public void setSingle(Boolean single) {
		this.single = single;
	}
    
}