package biz.firstlook.app.web.common.struts.action;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.util.ForwardUtil;
import biz.firstlook.app.web.common.struts.util.PersistFormUtil;

public abstract class PersistForm extends Load {
	
	private static Logger log = Logger.getLogger( PersistForm.class );
	private String targetProperty;
	private Map nullValueMap;
	
	public PersistForm() {
		super();
		nullValueMap = new HashMap();
	}

	@Override
	public Serializable getIdentifier(ActionForm form, HttpServletRequest request) {
    	Integer id;
		try {
			id = (Integer) PropertyUtils.getProperty(form, getParamName());
		} catch (Exception e) {
			log.error( "Error getting identifier property from form", e );
			throw new RuntimeException( "Error getting identifier property from form", e );
		}
    	return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	Object bean = request.getAttribute( getResultAttribute() );
    	try {
			PersistFormUtil.copyFormToBean(form, bean, getTargetProperty(), getParamName(), nullValueMap);
		} catch (InstantiationException e) {
			log.error( "Attempt to illegally instantiate an instance of a property or object in PersistForm", e );
			throw new ServletException( "Error trying to fetch property or access object", e );
		} catch (IllegalAccessException e) {
			log.error( "Attempt to illegally access a property or object in PersistForm", e );
			throw new ServletException( "Error trying to fetch property or access object", e );
		} catch (InvocationTargetException e) {
			log.error( "Error invoke target in PersistForm", e );
			throw new ServletException( "Error invoke taget in persist form", e );
		} catch (NoSuchMethodException e) {
			log.error( "Error, method specified doesn't exist", e );
			throw new ServletException( "Error, method specified doesn't exist", e);
		} 
		
		prePersist( bean );
		persist(bean);
    	bean = loadObject( getIdentifier( form, request) );
    	request.setAttribute( getResultAttribute(), bean );
	}

	protected abstract void persist(Object bean); 

	@Override
	public ActionForward findForward(ActionMapping mapping, ActionForm form, HttpServletRequest request) throws ServletException {
    	Object bean = request.getAttribute( getResultAttribute() );
    	ActionForward forward;
		try {
			forward = ForwardUtil.addParam( mapping.findForward("success"), "id", BeanUtils.getProperty( bean, "id" ) );
		} catch (Exception e) {
			log.error( "Error getting identifier property from bean", e );
			throw new ServletException( "Error getting identifier property from bean", e );
		}
    	return forward;
	}

    protected void prePersist(Object bean) throws ServletException {
    }



	public String getTargetProperty() {
		return targetProperty;
	}

	public void setTargetProperty(String targetProperty) {
		this.targetProperty = targetProperty;
	}

	public Map getNullValueMap() {
		return nullValueMap;
	}

	public void setNullValueMap(Map nullValueMap) {
		this.nullValueMap = nullValueMap;
	}
	
	
}
