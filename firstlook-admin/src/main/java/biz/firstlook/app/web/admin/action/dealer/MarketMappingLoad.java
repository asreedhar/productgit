package biz.firstlook.app.web.admin.action.dealer;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import biz.firstlook.model.imt.BusinessUnitMarketDealer;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.MarketMapping;
import biz.firstlook.persist.imt.MarketMappingDao;

public class MarketMappingLoad extends LoadDealer {

    private MarketMappingDao marketMappingDao;
    
    @Override
    protected void afterLoad(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	    throws ServletException {
	
	BusinessUnitMarketDealer marketDealer = dealer.getBusinessUnitMarketDealer();
	if( marketDealer != null ) {
	    MarketMapping marketMapping = marketMappingDao.find(marketDealer.getDealerNumber());
	    List<String> states = marketMappingDao.findStates();
	    request.setAttribute("states", states.toArray(new String[states.size()]));
	    
	    if( marketMapping != null ) {
		marketDealer.setSelectedState(marketMapping.getState());
		marketDealer.setSelectedCounty(marketMapping.getCounty());
		marketDealer.setMarketDealerName(marketMapping.getName());
		List<String> counties = marketMappingDao.findCounties(marketMapping.getState());
		request.setAttribute("counties", counties.toArray(new String[counties.size()]));
		
		//need the list of these to populate the page.
		List<MarketMapping> marketMappings = marketMappingDao.find(marketMapping.getState(), marketMapping.getCounty());
		List<LabelValueBean> marketMappingOptions = new ArrayList<LabelValueBean>();
		for( MarketMapping mMapping : marketMappings ) {
		    marketMappingOptions.add( new LabelValueBean(mMapping.getName(), mMapping.getId().toString()));
		}
		request.setAttribute("marketMappings", marketMappingOptions);
	    }
	}
    }

    public void setMarketMappingDao(MarketMappingDao marketMappingDao) {
        this.marketMappingDao = marketMappingDao;
    }
}
