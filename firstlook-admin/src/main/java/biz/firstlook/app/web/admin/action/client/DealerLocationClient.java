package biz.firstlook.app.web.admin.action.client;

import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.HttpURL;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class DealerLocationClient {
	static Logger log = Logger.getLogger(DealerLocationClient.class);
	String url = "http://maps.googleapis.com/maps/api/geocode/json?address=";
	String zipCode;
	
/* public static void main(String[] args) {
	 DealerLocationClient client = new DealerLocationClient();
	 String zipCode = "90210";
	 try {
		client.getGeoCodeFromGoogle(zipCode);
	} catch (Exception e) {
		e.printStackTrace();
	}
}*/	
	
	
public Map<String,BigDecimal> getGeoCodeFromGoogle(String zipCode, String address) throws Exception //pass zipcode as parameter
	{	
		
		DefaultHttpClient httpClient = null;
		DealerGeoCodeInformationResponse dealerGeoCodeInformationResp = null;
		if(address!=null)
		{
			
			url+=address+" "+zipCode;
		}
		else
		{
			url+=zipCode; 
		}
		HttpURL httpnewUrl= new HttpURL(url);
		HttpGet request = new HttpGet(httpnewUrl.getEscapedURI());
		try 
		{
			httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				Gson gson = new Gson();
				dealerGeoCodeInformationResp = gson.fromJson(reader, DealerGeoCodeInformationResponse.class);
			}
			Map<String,BigDecimal> mapGeoCode = new HashMap<String, BigDecimal>();
			if(dealerGeoCodeInformationResp!=null && !dealerGeoCodeInformationResp.getStatus().equalsIgnoreCase("ZERO_RESULTS"))
			{
				mapGeoCode.put("lat",  dealerGeoCodeInformationResp.getResults()[0].getGeometry().getLocation().getLat());
				mapGeoCode.put("long", dealerGeoCodeInformationResp.getResults()[0].getGeometry().getLocation().getLng());
			}
			return mapGeoCode;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.err.println("Google API for MAPS encountered a problem " + e.getMessage());
			log.error("Exception getting location information" , e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
}
