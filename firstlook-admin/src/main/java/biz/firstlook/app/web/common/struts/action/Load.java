package biz.firstlook.app.web.common.struts.action;

import java.io.Serializable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.helper.AfterLoadEvent;

public abstract class Load extends Action {

	private String paramName = "id";
	private String resultAttribute = "result";
	private String successForward = "success";

	private AfterLoadEventChain afterLoadEventChain = new AfterLoadEventChain();

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		retrieveObject(form, request);
		afterLoad(mapping, form, request, response);
		afterLoadEventChain.processEventChain(form, request);
		return findForward(mapping, form, request);
	}

	protected void retrieveObject(ActionForm form, HttpServletRequest request)
			throws ServletException {
		Serializable id = getIdentifier(form, request);
		Object result = loadObject(id);
		request.setAttribute(resultAttribute, result);
	}

	protected abstract Object loadObject(Serializable id)
			throws ServletException;

	protected Serializable getIdentifier(ActionForm form,
			HttpServletRequest request) throws ServletException {
		return new Integer(request.getParameter(paramName));
	}

	protected void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
	}

	protected ActionForward findForward(ActionMapping mapping, ActionForm form,
			HttpServletRequest request) throws ServletException {
		return mapping.findForward(successForward);
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getResultAttribute() {
		return resultAttribute;
	}

	public void setResultAttribute(String resultAttribute) {
		this.resultAttribute = resultAttribute;
	}

	public String getSuccessForward() {
		return successForward;
	}

	public void setSuccessForward(String successForward) {
		this.successForward = successForward;
	}

	protected final AfterLoadEventChain getAfterLoadEventChain() {
		return afterLoadEventChain;
	}

	public final void setAfterLoadEvents(List<AfterLoadEvent> events) {
		afterLoadEventChain.clear();
		afterLoadEventChain.addAll(events);
	}

}