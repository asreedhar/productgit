package biz.firstlook.app.web.admin.action.client;

import java.math.BigDecimal;

public class Location
{
    private BigDecimal lng;

    private BigDecimal lat;

    public BigDecimal getLng ()
    {
        return lng;
    }

    public void setLng (BigDecimal lng)
    {
        this.lng = lng;
    }

    public BigDecimal getLat ()
    {
        return lat;
    }

    public void setLat (BigDecimal lat)
    {
        this.lat = lat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lng = "+lng+", lat = "+lat+"]";
    }
}
