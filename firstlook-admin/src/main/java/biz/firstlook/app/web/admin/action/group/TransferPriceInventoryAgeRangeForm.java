package biz.firstlook.app.web.admin.action.group;

import org.apache.struts.action.ActionForm;

public class TransferPriceInventoryAgeRangeForm extends ActionForm {
	
	static final long serialVersionUID = -3125318051897147786L;
	
	private Integer low;
	private Integer high;
	
	public Integer getLow() {
		return low;
	}
	public void setLow(Integer low) {
		this.low = low;
	}
	public Integer getHigh() {
		return high;
	}
	public void setHigh(Integer high) {
		this.high = high;
	}
}
