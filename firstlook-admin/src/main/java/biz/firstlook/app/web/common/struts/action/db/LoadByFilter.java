package biz.firstlook.app.web.common.struts.action.db;

import java.util.List;

import biz.firstlook.persist.common.GenericDAO;

public class LoadByFilter extends biz.firstlook.app.web.common.struts.action.LoadByFilter{
	
	private GenericDAO genericDAO;
	
	@SuppressWarnings("unchecked")
	@Override
	protected List loadByBean(Object o) {
    	return genericDAO.findByExample( o );
	}
	
	public GenericDAO getGenericDAO() {
		return genericDAO;
	}

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}
}