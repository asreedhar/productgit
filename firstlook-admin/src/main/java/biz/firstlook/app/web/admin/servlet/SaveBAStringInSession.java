package biz.firstlook.app.web.admin.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * 
 * @author dpatton
 *
 * Servlet used to save the basic auth string into the session. 
 */
public class SaveBAStringInSession  extends HttpServlet {
	static Logger log = Logger.getLogger( SaveBAStringInSession.class );
	/**
	 * 
	 */
	private static final long serialVersionUID = 3738554421093742269L;
	private static final String NAME_IN_SESSION = ".basicAuthString.";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    log.debug( "########################################################################################################" );
	    log.debug( "We are done!!!!! ");
		
		String basicAuthString = request.getParameter( "basicAuth" );
		
		log.debug( "BasicAuth=" + basicAuthString );
//		HttpSession session = request.getSession();
		
		request.getSession().getServletContext().getContext("/fl-admin").setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString);

		
	//	session.setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString );
	    log.debug( "########################################################################################################" );
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)	throws ServletException, IOException {
	    log.debug( "########################################################################################################" );
	    log.debug( "We are done!!!!! ");
		
		String basicAuthString = request.getParameter( "basicAuth" );
		
		log.debug( "BasicAuth=" + basicAuthString );
//		HttpSession session = request.getSession();
		
		request.getSession().getServletContext().getContext("/fl-admin").setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString);

		
	//	session.setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString );
	    log.debug( "########################################################################################################" );
	}
}
