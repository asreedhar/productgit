package biz.firstlook.app.web.admin.action.group;

import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.persist.imt.DealerDao;

public class AddDealers extends Load {

	private DealerDao dealerDao;
	
	@Override
	protected Serializable getIdentifier(ActionForm form, HttpServletRequest request) throws ServletException {
		Integer id = new Integer( request.getParameter("id") );
		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterLoad(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String dealerId = request.getParameter("dealerId");

		DealerGroup group = (DealerGroup) request.getAttribute( getResultAttribute() );
		Dealer dealer = dealerDao.findById( Integer.parseInt( dealerId ), false );
		group.getDealers().add( dealer );
		getGenericDAO().makePersistent( group );
		
		return;
	}

	public DealerDao getDealerDao() {
		return dealerDao;
	}

	public void setDealerDao(DealerDao dealerDao) {
		this.dealerDao = dealerDao;
	}

}
