package biz.firstlook.app.web.admin.action.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.FirstLookRegion;
import biz.firstlook.persist.imt.FirstLookRegionDao;

public class RemoveZipCode extends Action {

	private FirstLookRegionDao firstLookRegionDao;
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String regionIdStr = request.getParameter( "regionId" );
		String zipCodeStr = request.getParameter( "zipCode" );
		
		FirstLookRegion region = firstLookRegionDao.findById( Integer.parseInt( regionIdStr ), false );
		region.getZipCodes().remove( zipCodeStr );
		firstLookRegionDao.makePersistent( region );
		
		return mapping.findForward("success");
	}

	public FirstLookRegionDao getFirstLookRegionDao() {
		return firstLookRegionDao;
	}

	public void setFirstLookRegionDao(FirstLookRegionDao firstLookRegionDao) {
		this.firstLookRegionDao = firstLookRegionDao;
	}
}
