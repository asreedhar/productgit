package biz.firstlook.app.web.admin.action.member;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.app.web.common.struts.action.db.Load;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.ref.MemberType;
import biz.firstlook.persist.imt.JobTitleDao;
import biz.firstlook.persist.imt.MemberDao;

public abstract class BaseMemberCreate extends Load {
	protected MemberDao memberDao;
	
	protected JobTitleDao jobTitleDao;
	
	@Override
	protected final void afterLoad(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {

		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		Integer jobTitleId = new Integer(request.getParameter("jobTitleId"));
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String officePhoneNumber = request.getParameter("officePhoneNumber");
		String officePhoneExtension = request
				.getParameter("officePhoneExtension");
		String officeFaxNumber = request.getParameter("officeFaxNumber");
		String reportMethod = request.getParameter("reportMethod");
		String userRole = request.getParameter("userRole");
		String usedRole = request.getParameter("usedRole");
		String newRole = request.getParameter("newRole");
		String emailAddress = request.getParameter("emailAddress");
		String memberType=request.getParameter("memberType");
		
		String returnMessage = null;
		if (!exists(firstName) || !exists(lastName) || !exists(login)
				|| !exists(password) || !exists(officePhoneNumber)
				|| !exists(officeFaxNumber)) {
			returnMessage = "Missing Required Fields. Please Recheck values and resubmit.";
		} else {
			Member existingMember = memberDao.byLogin(login);
			if (existingMember == null) {
				Member member = new Member();
				member.setEmailAddress(emailAddress);
				member.setFirstName(firstName);
				member.setLastName(lastName);
				member.setJobTitle(jobTitleDao.findById(jobTitleId, false));
				member.setLogin(login);
				member.setPassword(password);
				member.setMemberType(MemberType.valueOf(memberType));
				member.setCreateDate(new Date());
				member.setOfficePhoneNumber(officePhoneNumber);
				member.setOfficePhoneExtension(officePhoneExtension);
				member.setOfficeFaxNumber(officeFaxNumber);
				member.setReportMethod(reportMethod);
				member.setUserRole(userRole);
				member.setUsedRole(usedRole);
				member.setNewRole(newRole);
				member.setInventoryOverviewSortOrderType(0); //default to all aphabetically
				member.setActiveInventoryLaunchTool(0); //default to eStock
				member.setGroupHomePageId(1); //default to bubble page
				member.setDashboardRowDisplay(10);
				
				Member savedMember = memberDao.makePersistent(member);
				associate( request, savedMember );
				memberDao.makePersistent(savedMember);
				returnMessage = new StringBuilder(login).append(
						" was created successfully.").toString();
			} else {
				returnMessage = "Login already exists, please assign a different login.";
			}
		}
		request.setAttribute("createMemberLogin", returnMessage);
	}

	private boolean exists(String check) {
		boolean result = false;
		if (check != null && check.length() > 0) {
			result = true;
		}
		return result;
	}
	
	protected abstract void associate(HttpServletRequest request, Member member);
	
	public MemberDao getMemberDao() {
		return memberDao;
	}

	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}

	public JobTitleDao getJobTitleDao() {
		return jobTitleDao;
	}

	public void setJobTitleDao(JobTitleDao jobTitleDao) {
		this.jobTitleDao = jobTitleDao;
	}

}
