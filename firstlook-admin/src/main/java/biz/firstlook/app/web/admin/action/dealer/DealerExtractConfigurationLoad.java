package biz.firstlook.app.web.admin.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.model.imt.DealerExtractConfiguration;
import biz.firstlook.persist.imt.DealerExtractConfigurationDao;

public class DealerExtractConfigurationLoad extends Action {

	private DealerExtractConfigurationDao dealerExtractConfigurationDao;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Integer dealerId = Integer.parseInt(request.getParameter("dealerId"));
		String destionationName = request.getParameter("destinationName");
		boolean isEdit = false;
		try {
			isEdit = Boolean.valueOf(request.getParameter("isEdit"));
		} catch(Exception e) {
			//ignore;
		}
				
		DealerExtractConfiguration config = dealerExtractConfigurationDao.findBy(dealerId, destionationName);
		if(config == null) {
			return mapping.findForward("error");
		}
		isEdit = isEdit && config.isEditable();
		
		DealerExtractConfigurationForm theForm = (DealerExtractConfigurationForm)form;
		if(theForm != null) {
			theForm.populate(config);
		}
		
		request.setAttribute("configuration", config);
		request.setAttribute("isEdit", isEdit);
		return mapping.findForward("success");
	}
	
	public void setDealerExtractConfigurationDao(
			DealerExtractConfigurationDao dealerExtractConfigurationDao) {
		this.dealerExtractConfigurationDao = dealerExtractConfigurationDao;
	}
}
