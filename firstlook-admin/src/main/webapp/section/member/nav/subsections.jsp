<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>

<ul id="tablist">
  <c:choose>
    <c:when test="${not empty member}">
      <c:url var="profileURL" value="/member/dynamic/profile.do">
        <c:param name="id" value="${member.id}"/>
      </c:url>

      <c:url var="alertsURL" value="/member/dynamic/alerts.do">
        <c:param name="id" value="${member.id}"/>
      </c:url>
      
      <c:url var="dealersURL" value="/member/dynamic/dealers.do">
        <c:param name="id" value="${member.id}"/>
      </c:url>

      <c:url var="vipAlertsURL" value="/member/dynamic/vipAlerts.do">
        <c:param name="id" value="${member.id}"/>
      </c:url>

      <c:url var="eliteAlertsURL" value="/member/dynamic/eliteAlerts.do">
        <c:param name="id" value="${member.id}"/>
      </c:url>

      <c:choose>
        <c:when test="${subsectionName == 'profile'}">
          <li id="active">
            <a href="${profileURL}" id="current" accesskey="1">profile</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${profileURL}" accesskey="1">profile</a> 
          </li>
        </c:otherwise>
      </c:choose>

      <c:choose>
        <c:when test="${subsectionName == 'alerts'}">
          <li id="active">
            <a href="${alertsURL}" id="current" accesskey="2">alerts</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${alertsURL}" accesskey="2">alerts</a> 
          </li>
        </c:otherwise>
      </c:choose>

      <c:choose>
        <c:when test="${subsectionName == 'vipAlerts'}">
          <li id="active">
            <a href="${vipAlertsURL}" id="current" accesskey="3">vip alerts</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${vipAlertsURL}" accesskey="3">vip alerts</a> 
          </li>
        </c:otherwise>
      </c:choose>

      <c:choose>
        <c:when test="${subsectionName == 'eliteAlerts'}">
          <li id="active">
            <a href="${eliteAlertsURL}" id="current" accesskey="3">l33t alerts</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${eliteAlertsURL}" accesskey="3">1337 alerts</a> 
          </li>
        </c:otherwise>
      </c:choose>
      
      <c:choose>
        <c:when test="${subsectionName == 'dealers'}">
          <li id="active">
            <a href="${dealersURL}" id="current" accesskey="4">dealers</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${dealersURL}" accesskey="4">dealers</a> 
          </li>
        </c:otherwise>
      </c:choose>
            
    </c:when>
    <c:otherwise>
      <c:url var="memberSearch" value="/member/page/search.do"/>
      <li id="active">
        <a href="${memberSearch}" id="current">search members</a> 
      </li> 	
    </c:otherwise>
  </c:choose>	
</ul>
				
