<%@include file="/include/tag-import.jsp" %>

<c:url var="profileURL" value="/member/dynamic/profile.do">
  <c:param name="id" value="${member.id}"/>
</c:url>

<c:url var="notesURL" value="/member/dynamic/notes.do">
  <c:param name="id" value="${member.id}"/>
</c:url>

<c:url var="reportURL" value="/member/dynamic/report.do">
  <c:param name="id" value="${member.id}"/>
</c:url>

<table cellspacing="0" align="center" class="NavBar">
	<tr valign="top">
		<td>

			<c:choose>
			<c:when test="${not empty member}">
				<p class="topnavi">
					MEMBER: ${member.firstName} ${member.lastName}: 
					<b>
					<a href="${profileURL}">profile</a> <span style="color: #ccc;">&bull;</span> 				
					<a href="${notesURL}">notes</a> <span style="color: #ccc;">&bull;</span>
					<a href="${reportURL}">reporting preferences</a> 				
		
					</b>
				</p>
			</c:when>
			<c:otherwise>
				<p class="topnavi">
					No Member Selected:
					<b>
					profile <span style="color: #ccc;">&bull;</span> 				
					notes <span style="color: #ccc;">&bull;</span> 				
					reporting preferences
		
					</b>
				</p>
			</c:otherwise>
			</c:choose>	
				
		</td>
		<td align="right">
		</td>	
	</tr>
</table>
