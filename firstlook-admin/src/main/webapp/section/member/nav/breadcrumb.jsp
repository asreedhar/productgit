<%@include file="/include/tag-import.jsp" %>

<ul id="crumblist">
  <li>MEMBER</li>

  <c:choose>
    <c:when test="${not empty member}">
      <c:url var="profileURL" value="/member/dynamic/profile.do">
        <c:param name="id" value="${member.id}"/>
      </c:url>
      <li id="active"><a href="${profileURL}" id="current">${member.firstName} ${member.lastName}</a></li>
    </c:when>
    <c:otherwise>
      <li>Member Search Page</li>
    </c:otherwise>
  </c:choose>
</ul>

