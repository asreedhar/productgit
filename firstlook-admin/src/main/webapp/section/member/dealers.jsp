<%@include file="/include/tag-import.jsp" %>


<br/>
<div class="row">
  <div class="left-content">
    <div id="dealers" class="section">
      <t:insert page="/section/member/div/dealers.jsp"/>
    </div>
  </div>
  <div class="right-content">
	<div id="add" class="section">
	  <jsp:include page="/section/member/div/addExistingDealer.jsp"/>
	</div>
  </div>
</div>
