<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function saveAllAlerts(id) {	
	for (i=0; i<document.forms.length; i++) {
		var formName = document.forms[i].name;
		subId = formName.charAt(formName.length-1);
		formName = formName.substring(0, formName.length-1);
		var path = createPath(formName);
 		var pars = Form.serialize(document.forms[i])+ '&memberId='+id+'&subscriptionTypeId='+subId;
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}
</script>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:saveAllAlerts(${member.id});">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="alert1" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=1"/>
    </div><br/>
    <div id="alert2" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=2"/>
    </div><br/>
    <div id="alert3" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=3"/>
    </div><br/>
    <div id="alert7" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=7"/>
    </div><br/>
    <div id="alert8" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=8"/>
    </div><br/>
	<div id="alert15" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=15"/>
    </div><br/>
  </div>
  <div class="right-content">
    <div id="alert4" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=4"/>
    </div><br/>
    <div id="alert5" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=5"/>
    </div><br/>
    <div id="alert6" class="section">
    <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=6"/>
    </div><br/>
    <div id="alert13" class="section">
    <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=13"/>
    </div><br/>
    <div id="alert14" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=14"/>
    </div><br/>
  </div>
</div>
