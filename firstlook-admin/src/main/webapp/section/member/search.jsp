<%@include file="/include/tag-import.jsp" %>

<script type="text/javascript">
function searchMember() {
	var member = $F('member');
	
	new Ajax.Updater(	'memberSearchResults', 
						'/fl-admin/member/div/search.do?' + member, 
						{asynchronous:true, evalScripts:true, method:'post'} ); 
}
</script>

<h2>Search for a Member</h2>

<p>Search by name or code: <input type="text" id="member" size="30"/>
<input type="submit"  onclick="searchMember();" value="Search" />
<SCRIPT language="JavaScript">
 $('member').focus();
</SCRIPT>

<c:url var="allMembersURL" value="/member/div/searchAll.do"/>
<h3><proto:linkRemote href="${allMembersURL}" update="memberSearchResults">list all members</proto:linkRemote></h3>
<br/>

<div id="memberSearchResults" style="border: 1px solid black;">
  <center>
    <p>No search results</p>
  </center>
</div>

