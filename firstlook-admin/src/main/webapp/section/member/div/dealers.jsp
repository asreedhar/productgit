<%@include file="/include/tag-import.jsp" %>

<h2>Member's Dealerships</h2>

<div class="content">
  <table id="dealer_table" class="datagrid">
    <thead>
      <tr>
        <th mochi:format="str">Nickname</th>
        <th mochi:format="str">Account #</th>
        <th mochi:format="str">Group</th>
        <th mochi:format="str"></th>
      </tr>
    </thead>
    <tfoot class="invisible">
      <tr>
        <th mochi:format="str">Nickname</th>
        <th mochi:format="str">Account #</th>
        <th mochi:format="str">Group</th>
        <th mochi:format="str"></th>
      </tr>
    </tfoot>
    <tbody id="dealer_table_body">
      <c:forEach items="${member.dealers}" var="dealer">
        <tr>
          <td>
            <c:url var="profileUrl" value="/dealer/dynamic/profile.do">
		      <c:param name="id" value="${dealer.id}"/>
            </c:url>
            <a href="${profileUrl}">
              ${dealer.shortName}
            </a>
          </td>
          <td>${dealer.code}</td>
        <td>
          <c:url var="groupUrl" value="/group/dynamic/profile.do">
		    <c:param name="id" value="${dealer.dealerGroup.id}"/>
          </c:url>
          <a href="${groupUrl}">${dealer.dealerGroup.name}</a>
        </td>
          <td>
            <c:url var="removeUrl" value="/member/custom/removeDealer.do">
			  <c:param name="id" value="${member.id}"/>
		      <c:param name="dealerId" value="${dealer.id}"/>
            </c:url>
            <c:url var="delete" value="/resources/images/icon_reject_up.gif"/>
			<proto:linkRemote update="dealers" href="${removeUrl}" 
                              confirm="Are you sure you want to remove ${dealer.name} from this member">
              <img src="${delete}" border="0"/>
            </proto:linkRemote>
          </td>
        </tr>
      </c:forEach>
      <c:if test="${empty member.dealers}">
        <tr>
          <td colspan="5">No Matching Results</td>
        </tr>
      </c:if>
    </tbody>
  </table> 
</div>

<c:url var="addDealers" value="/member/custom/addDealers.do"/>
	<script>
 function addDealer(element) {
    Element.remove(element);
    new Ajax.Request('${addDealers}', {parameters: 'id=${member.id}&dealerId=' + element.id});
  	new Insertion.Top('dealer_table_body', '<tr id="' + element.id + '"><td>' + element.innerHTML + '</td><td colspan="3">Dealer Added, please refresh...</td></tr>');
 }
</script>

    <script type="text/javascript" language="javascript">

 	  Droppables.add('dealer_table_body',
 	  	{onDrop: function(element) {
 	  		addDealer(element);
 	  	}});
 	  	
	</script>
