<%@include file="/include/tag-import.jsp" %>

<tag:element title="Contact Information" id="${member.id}" 
             section="member" element="contactInfo" 
             formBean="${member_contactInfo}">

  <div>
    <label for="officePhone" class="fieldLabel">Office Phone</label>
    <span id="officePhone">
      <tag:text size="15" property="officePhoneNumber" value="${member.officePhoneNumber}"/>
      <tag:text size="5" property="officePhoneExtension" value="${member.officePhoneExtension}" prefix="x" omitEmpty="${true}"/>
    </span>
  </div>
  <tag:text title="Office Fax:" property="officeFaxNumber" value="${member.officeFaxNumber}"/>
  <tag:text title="Mobile Phone:" property="mobilePhoneNumber" value="${member.mobilePhoneNumber}"/>
  <tag:text title="Pager Number:" property="pagerNumber" value="${member.pagerNumber}"/>
  <tag:text title="SMS Address:" property="smsAddress" value="${member.smsAddress}"/>

</tag:element>