<%@include file="/include/tag-import.jsp" %>

<tag:element title="Reporting Preferences" id="${member.id}" 
             section="member" element="reportPreference" 
             formBean="${member_reportPreference}">

  <tag:text title="# Rows on Dashboard:" size="5" property="dashboardRowDisplay" value="${member.dashboardRowDisplay}"/>
  
    <tag:select title="Prefered Reporting Method:" property="reportingMethod" value="${member.reportingMethod}"
              selectedValue="${member.reportingMethod}" isEnum="${true}" enumValues="${reportingMethodEnum}"
  			  omitValue="NONE"/>
  
</tag:element>
