<%@include file="/include/tag-import.jsp" %>

<table id="member_table" class="datagrid">
  <thead>
    <tr>
      <th mochi:format="str">First</th>
      <th mochi:format="str">Last</th>
      <th mochi:format="str">Login</th>
      <th mochi:format="str">Type</th>
      <th mochi:format="str">Default Dealer Group</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tfoot class="invisible">
    <tr>
      <th mochi:format="str">Name</th>
    </tr>
  </tfoot>
  <tbody>
    <c:forEach items="${results}" var="member">
      <tr>
        <td>
            ${member.firstName}
        </td>
        <td>
            ${member.lastName}
        </td>
        <td>
            <str:truncateNicely lower="13" upper="17">${member.login}</str:truncateNicely>
        </td>
        <td>
        	${member.memberType}
        </td>
        <td>
            ${member.defaultDealerGroupName}
        </td>
        <td>
          <c:url var="profileUrl" value="/member/dynamic/profile.do">
		    <c:param name="id" value="${member.id}"/>
          </c:url>
          <a href="${profileUrl}">
			view
          </a>
        </td>
      </tr>
    </c:forEach>
    <c:if test="${empty results}">
      <tr>
        <td>No Matching Results</td>
      </tr>
    </c:if>
  </tbody>
</table>  

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('member_table'));
</script>

      