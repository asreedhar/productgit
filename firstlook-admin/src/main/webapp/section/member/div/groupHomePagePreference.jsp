<%@include file="/include/tag-import.jsp" %>

<tag:element title="Group Homepage Preference" id="${member.id}" 
             section="member" element="groupHomePagePreference" 
             formBean="${member_groupHomePagePreference}">

</br></br></br>

<tag:radio title="Multi-Dealer Access Page (bubble page)" property="groupHomePageId" value="1" beanPropertyValue="${member.groupHomePageId}"></tag:radio>
</br>
<tag:radio title="Performance Management Center (PMC)" property="groupHomePageId" value="2" beanPropertyValue="${member.groupHomePageId}"></tag:radio>

</tag:element> 