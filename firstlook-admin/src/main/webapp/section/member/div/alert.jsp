<%@include file="/include/tag-import.jsp" %>
<h2>${subscription.subscriptionTypeName}</h2>

<c:url var="saveURL" value="/member/save/alert.do?memberId=${param.memberId}&subscriptionTypeId=${subscription.subscriptionTypeId}">
</c:url>

<proto:formRemote href="${saveURL}" 
	update="alert${subscription.subscriptionTypeId}" 
	name="member_alert${subscription.subscriptionTypeId}" 
	id="member_alert${subscription.subscriptionTypeId}">
	<c:set var="org.apache.struts.taglib.html.BEAN" value="${member_alert}" scope="request" /> 

<c:choose>
	<c:when test="${isEdit}">
		<span class="formButtons">
		<h:submit value="Save"/> 
			<c:url var="cancelURL" value="/member/display/alert.do?memberId=${param.memberId}&subscriptionTypeId=${subscription.subscriptionTypeId}" />
				<proto:linkRemote href="${cancelURL}" update="alert${subscription.subscriptionTypeId}">Cancel</proto:linkRemote></span>
	</c:when>
		<c:otherwise>
			<c:url var="editURL"
				value="/member/edit/alert.do?memberId=${param.memberId}&subscriptionTypeId=${subscription.subscriptionTypeId}" />
			<span class="editLink"><proto:linkRemote href="${editURL}"
				update="alert${subscription.subscriptionTypeId}">Edit</proto:linkRemote></span>
		</c:otherwise>
	</c:choose>

  <c:choose>
    <c:when test="${empty subscription.subscriptionFrequencyDetailId && !isEdit}">
      <div style="clear: both">
        No Subscription to ${subscription.subscriptionTypeName}
      </div>
    </c:when>
    <c:otherwise>
      <c:if test="${not empty subscription.subscriptionFrequencyDetailId}">
        <ref:single beanName="imt-persist-subscriptionFrequencyDetailDao" scope="request" idVal="${subscription.subscriptionFrequencyDetailId}"/>
      </c:if>
      <tag:select title="Day of Week:" property="subscriptionFrequencyDetailId" 
              value="${value.description}" 
              nameProperty="description"
              options="frequencies" 
              selectedValue="${subscription.subscriptionFrequencyDetailId}"/>
      <c:choose>
        <c:when test="${isEdit}">
          <tag:multibox title="Delivery Format:" property="subscriptionDeliveryTypeIds"
                value="${subscription.subscriptionDeliveryTypeIds}"
                valueProperty="id"
                nameProperty="description"
                options="${deliveryTypes}"
                selectedValue="${subscription.subscriptionDeliveryTypeIds}"/><br/>
        </c:when>
        <c:otherwise>
          <div>
            <label for="subType" class="fieldLabel">Delivery Format:</label>
<div style="float: left; padding-left: 135px; margin-top: -15px;">
            <c:forEach items="${subscription.subscriptionDeliveryTypeIds}" var="subTypeId">
              <ref:single beanName="imt-persist-subscriptionDeliveryTypeDao" scope="request" idVal="${subTypeId}"/>
              ${value.description}<br/>
            </c:forEach>
</div>
          </div>
        </c:otherwise>
      </c:choose>

		<c:choose>
			<c:when test="${subscription.subscriptionTypeId eq 15}">
				<c:choose>
		        <c:when test="${isEdit}">
		          <c:set var="dealerGroups" value="${subscription.eligibleDealerGroups}" scope="request"/>
		          <tag:multibox title="DealerGroups:" property="businessUnitIds"
		  				value="${subscription.businessUnitIds}"
		  				nameProperty="shortName"
		                valueProperty="id"
		  				options="${dealerGroups}"
		  				selectedValue="${memberSubscription.businessUnitIds}"/><br/>
		  		</c:when>
		  		<c:otherwise>
		            <label for="subType" class="fieldLabel" style="width: 70px">DealerGroups:</label><br/>
		            <c:forEach items="${subscription.businessUnitIds}" var="buId">
		              <ref:single beanName="imt-persist-dealerGroupDao" scope="request" idVal="${buId}"/>
		              * ${value.shortName}<br/>
		            </c:forEach>
		  		</c:otherwise>  		
		      </c:choose>
			</c:when>
			<c:otherwise>
		      <c:choose>
		        <c:when test="${isEdit}">
		          <c:set var="dealers" value="${subscription.eligibleDealers}" scope="request"/>
		          <tag:multibox title="Dealers:" property="businessUnitIds"
		  				value="${subscription.businessUnitIds}"
		  				nameProperty="shortName"
		                valueProperty="id"
		  				options="${dealers}"
		  				selectedValue="${memberSubscription.businessUnitIds}"/><br/>
		  		</c:when>
		  		<c:otherwise>
		            <label for="subType" class="fieldLabel" style="width: 70px">Dealers:</label><br/>
		            <c:forEach items="${subscription.businessUnitIds}" var="buId">
		              <ref:single beanName="imt-persist-dealerDao" scope="request" idVal="${buId}"/>
		              * ${value.shortName}<br/>
		            </c:forEach>
		  		</c:otherwise>  		
		      </c:choose>
		    </c:otherwise>
	    </c:choose>
  	</c:otherwise>
  </c:choose>
</proto:formRemote>

<br/>
<br/>