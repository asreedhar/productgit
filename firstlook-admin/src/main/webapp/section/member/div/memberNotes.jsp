<%@include file="/include/tag-import.jsp" %>
<tag:element title="Member Notes" id="${member.id}" 
             section="member" element="memberNotes" 
             formBean="${member_memberNotes}">

  <tag:textarea cols="80" rows="20" property="notes">${member.notes}</tag:textarea>

</tag:element>
