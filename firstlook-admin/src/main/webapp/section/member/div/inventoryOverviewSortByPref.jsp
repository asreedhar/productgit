<%@include file="/include/tag-import.jsp" %>

<tag:element title="Inventory Overview Preference" id="${member.id}" 
             section="member" element="inventoryOverviewSortByPref" 
             formBean="${member_inventoryOverviewSortByPref}">
        
    <tag:select title="Default Sort:" property="inventoryOverviewSortOrderType" value="${member.inventoryOverviewSortOrderType}" showOptions="${true}">
	<c:set var="selected">${member.inventoryOverviewSortOrderType}</c:set>
	<tag:option value="0" selected="${selected}">All Alphabetically</tag:option>
	<tag:option value="1" selected="${selected}">Segment</tag:option>
	</tag:select>        
              
              
</tag:element>  

