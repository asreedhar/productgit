<%@include file="/include/tag-import.jsp" %>

<tag:element title="Inventory Launch Tool" id="${member.id}" 
             section="member" element="activeInventoryLaunchTool" 
             formBean="${member_activeInventoryLaunchTool}">

<c:set var="toolLabel" value="${member.activeInventoryLaunchTool == 1 ? 'VPA' : 'eStock'}"/>

	<tag:select title="Tool to launch for an active inventory item" property="activeInventoryLaunchTool"
		value="${toolLabel}"
		selectedValue="${member.activeInventoryLaunchTool}"
		emptyContent="No tool set.">
			<tag:option value="0" selected="${member.activeInventoryLaunchTool}">EStock</tag:option>
			<tag:option value="1" selected="${member.activeInventoryLaunchTool}">VPA</tag:option>
	</tag:select>
  
</tag:element>
