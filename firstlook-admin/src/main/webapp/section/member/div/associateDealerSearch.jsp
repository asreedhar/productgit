<%@include file="/include/tag-import.jsp" %>

<c:url var="add" value="/resources/images/add.gif"/>

<p>Click <img src="${add}" border="0"/> to add Dealer</p>

<ul id="associateDealers">
  <c:forEach items="${results}" var="dealer">
    <li id="${dealer.id}">
      <a href="#" 
         onclick="addDealer($('${dealer.id}'));">
        <img src="${add}" border="0"/>
      </a>&nbsp;&nbsp;${dealer.name}</li>
  </c:forEach>
  <c:if test="${empty results}">
    <li>No Matching Dealers</li>
  </c:if>
</ul>