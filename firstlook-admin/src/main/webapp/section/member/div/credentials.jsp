<%@include file="/include/tag-import.jsp"%>

<c:set var="credentialsId">
	<b:write name="credentials" property="id" />
</c:set>
<c:set var="gmacUsername">
	<b:write name="credentials" property="gmacUsername" />
</c:set>
<c:set var="gmacPassword">
	<b:write name="credentials" property="gmacPassword" />
</c:set>
<tag:element title="Member Credentials" id="${credentialsId}"
	section="member" element="credentials"  formBean="${member_credentials}">

	<tag:text title="GMAC Username: " property="gmacUsername" value="${gmacUsername}" size="15" />
	<tag:password title="GMAC Password: " property="gmacPassword" value="${gmacPassword}" size="15" />
	<br />
</tag:element>
