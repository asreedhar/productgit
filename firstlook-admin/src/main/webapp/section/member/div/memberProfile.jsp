<%@include file="/include/tag-import.jsp" %>

<tag:element title="Member Profile" id="${member.id}" 
             section="member" element="memberProfile" 
             formBean="${member_memberProfile}">
             
  <div>
    <label for="name" class="fieldLabel">Member Name</label>
    <span id="name">
      <tag:text size="3" property="salutation" value="${member.salutation}"/>
      <tag:text size="10" property="firstName" value="${member.firstName}"/>
      <tag:text size="1" property="middleInitial" value="${member.middleInitial}"/> 
      <tag:text size="10" property="lastName" value="${member.lastName}"/>
    </span>
  </div>
  <tag:text title="Preferred Name:" property="preferredFirstName" value="${member.preferredFirstName}"/>

  <ref:ref beanName="imt-persist-jobTitleDao" scope="request"/>
  <tag:select title="Job Title:" property="jobTitle" value="${member.jobTitle.name}" options="values" selectedValue="${member.jobTitle.id}"/>

  <tag:text title="Email Address:" property="emailAddress" value="${member.emailAddress}"/>

  <tag:text title="Login:" value="${member.login}"/>
  <tag:password title="Password:" property="password" value="${member.password}"/>
  <tag:select title="Login Status:" property="loginStatus" value="${member.loginStatus}" 
              selectedValue="${member.loginStatus}" isEnum="${true}" enumValues="${loginStatusEnum}"
              omitValue="NONE"/>
  <tag:select title="Member Type:" property="memberType" value="${member.memberType}"  
              selectedValue="${member.memberType}" isEnum="${true}" enumValues="${memberTypeEnum}"
			  omitValue="NONE"/>
  <tag:select title="User Role:" property="userRole" value="${member.userRole}"
              selectedValue="${member.userRole}" isEnum="${true}" enumValues="${userRoleEnum}"
  			  omitValue="NONE"/>
  <tag:select title="Used Role:" property="usedRole" value="${member.usedRole}"
              selectedValue="${member.usedRole}" isEnum="${true}" enumValues="${usedRoleEnum}"
  			  omitValue="NONE"/>
  <tag:select title="New Role:" property="newRole" value="${member.newRole}"
              selectedValue="${member.newRole}" isEnum="${true}" enumValues="${newRoleEnum}"
  			  omitValue="NONE"/>
  <tag:text title="Member Since:" value="${member.createDate}"/>

  <tag:checkbox title="Active:" property="active" value="${member.active}"/>



</tag:element>