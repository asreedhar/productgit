<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function saveAllAlerts(id) {	
	for (i=0; i<document.forms.length; i++) {
		var formName = document.forms[i].name;
		subId = formName.charAt(formName.length-1);
		formName = formName.substring(0, formName.length-1);
		var path = createPath(formName);
 		var pars = Form.serialize(document.forms[i])+ '&memberId='+id+'&subscriptionTypeId='+subId;
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}
</script>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:saveAllAlerts(${member.id});">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="alert16" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=16"/>
    </div><br/>
    <div id="alert17" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=17"/>
    </div><br/>
  </div>
  <div class="right-content">
    <div id="alert18" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=18"/>
    </div><br/>
    <div id="alert19" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=19"/>
    </div><br/>
  </div>
</div>
