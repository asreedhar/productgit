<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function saveAllAlerts(id) {	
	for (i=0; i<document.forms.length; i++) {
		var formName = document.forms[i].name;
		if (i==0){
			subId = formName.charAt(formName.length-1);
			formName = formName.substring(0, formName.length-1);
		}
		else {
			subId = formName.substring(formName.length-2, formName.length);
			formName = formName.substring(0, formName.length-2);
		}
		var path = createPath(formName);
 		var pars = Form.serialize(document.forms[i])+ '&memberId='+id+'&subscriptionTypeId='+subId;
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}
</script>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:saveAllAlerts(${member.id});">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="alert9" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=9"/>
    </div><br/>
  </div>
  <div class="right-content">
    <div id="alert10" class="section">
      <t:insert page="/member/${action}/alert.do?memberId=${member.id}&subscriptionTypeId=10"/>
    </div>
  </div>
</div>
