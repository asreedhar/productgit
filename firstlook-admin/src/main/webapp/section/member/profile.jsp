<%@include file="/include/tag-import.jsp" %>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:submitAll();">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="memberProfile" class="section">
      <t:insert page="/member/${action}/memberProfile.do"/>
    </div>
    <br/>
    <div id="credentials" class="section">
      <t:insert page="/member/${action}/credentials.do"/>
    </div>
    <br/>
  </div>
  <div class="right-content">
    <div id="contactInfo" class="section">
      <t:insert page="/member/${action}/contactInfo.do"/>
    </div>
    <br/>
  </div>
  <div class="right-content">
  	<div id="activeInventoryLaunchTool" class="section">
  		<t:insert page="/member/${action}/activeInventoryLaunchTool.do"/>
  	</div>
  	<br/>
  </div>
  <div class="right-content">
    <div id="reportPreference" class="section">
      <t:insert page="/member/${action}/reportPreference.do"/>
    </div>
    <br/>
  </div>
  <div class="right-content">
    <div id="inventoryOverviewSortByPref" class="section">
      <t:insert page="/member/${action}/inventoryOverviewSortByPref.do"/>
    </div>
    <br/>
  </div>
</div>
  <div class="right-content">
    <div id="groupHomePagePreference" class="section">
      <t:insert page="/member/${action}/groupHomePagePreference.do"/>
    </div>
    <br/>
  </div>
</div>

  <div style="clear: both">
    <div id="memberNotes">
      <t:insert page="/member/${action}/memberNotes.do"/>
    </div>
  </div>
