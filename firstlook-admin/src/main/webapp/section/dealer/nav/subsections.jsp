<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>
<ul id="tablist">
  <c:choose>
    <c:when test="${not empty dealer}">
      <c:url var="profileURL" value="/dealer/dynamic/profile.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="generalURL" value="/dealer/dynamic/general.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="marketsURL" value="/dealer/dynamic/markets.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="upgradesURL" value="/dealer/dynamic/upgrades.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="lightsURL" value="/dealer/dynamic/lights.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="membersURL" value="/dealer/dynamic/members.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="riskURL" value="/dealer/dynamic/risk.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="scorecardURL" value="/dealer/dynamic/scorecard.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="matrixURL" value="/dealer/dynamic/matrix.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="edt" value="/dealer/dynamic/edt.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="extractConfigUrl" value="/dealer/extracts/list.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="thirdPartyAppraisal" value="/dealer/dynamic/thirdPartyAppraisal.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="photoProvider" value="/dealer/dynamic/photoProvider.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="pingIISettings" value="/dealer/dynamic/pingIISettings.do">
        <c:param name="dealerId" value="${dealer.id}"/>
      </c:url>
      <c:url var="dmsWriteback" value="/dealer/dynamic/dmsWriteback.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="kbbConsumerTool" value="/dealer/dynamic/KBBConsumerTool.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="vehicleHistoryReportURL" value="/dealer/display/vhrSettings.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:url var="windowStickerAdminURL" value="../../../templating/Pages/Admin/Home.aspx">
        <c:param name="dealerId" value="${dealer.id}"/>
      </c:url>
      <c:choose>
        <c:when test="${subsectionName == 'profile'}">
          <li id="active">
            <a href="${profileURL}" id="current" accesskey="1">prof</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${profileURL}" accesskey="1">prof</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'general'}">
          <li id="active">
            <a href="${generalURL}" id="current" accesskey="2">general</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${generalURL}" accesskey="2">general</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'markets'}">
          <li id="active">
            <a href="${marketsURL}" id="current" accesskey="3">markets</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${marketsURL}" accesskey="3">markets</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'upgrades'}">
          <li id="active">
            <a href="${upgradesURL}" id="current" accesskey="4">upgrd</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${upgradesURL}" accesskey="4">upgrd</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'lights'}">
          <li id="active">
            <a href="${lightsURL}" id="current" accesskey="5">lights/risk</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${lightsURL}" accesskey="5">lights/risk</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'members'}">
          <li id="active">
            <a href="${membersURL}" id="current" accesskey="6">members</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${membersURL}" accesskey="6">members</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'scorecard'}">
          <li id="active">
            <a href="${scorecardURL}" id="current" accesskey="8">scorecard</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${scorecardURL}" accesskey="8">scorecard</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'matrix'}">
          <li id="active">
            <a href="${matrixURL}" id="current" accesskey="9">matrix</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${matrixURL}" accesskey="9">matrix</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'edt'}">
          <li id="active">
            <a href="${edt}" id="current" accesskey="0">edt</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${edt}" accesskey="0">edt</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
      	 <c:when test="${subsectionName == 'extractDealerConfiguration'}">
          <li id="active">
            <a href="${extractConfigUrl}" id="current" accesskey="0">Extract Config</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${extractConfigUrl}" accesskey="0">Extract Config</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'thirdPartyAppraisal'}">
          <li id="active">
            <a href="${thirdPartyAppraisal}" id="current" accesskey="0">3rd Party Appraisal</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${thirdPartyAppraisal}" accesskey="0">3rd Party Appraisal</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'photoProvider'}">
          <li id="active">
            <a href="${photoProvider}" id="current" accesskey="0">Photo Provider</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${photoProvider}" accesskey="0">Photo Provider</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'pingIISettings'}">
          <li id="active">
            <a href="${pingIISettings}" id="current" accesskey="0">Ping II Settings</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${pingIISettings}" accesskey="0">Ping II Settings</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'dmsWriteback'}">
          <li id="active">
            <a href="${dmsWriteback}" id="current" accesskey="0">DMS Writeback</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${dmsWriteback}" accesskey="0">DMS Writeback</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
       <c:choose>
      	 <c:when test="${subsectionName == 'kbbConsumerTool'}">
          <li id="active">
            <a href="${kbbConsumerTool}" id="current" accesskey="0">KBB Consumer Tool</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${kbbConsumerTool}" accesskey="0">KBB Consumer Tool</a> 
          </li> 	
        </c:otherwise>
      </c:choose>
      <li>
        <a target="VehicleHistoryReport" href="${vehicleHistoryReportURL}" accesskey="1">VHR</a> 
      </li>
      <li>
        <a target="WindowSticker" href="${windowStickerAdminURL}" title="Window Sticker">WS</a>
      </li>
    </c:when>
    <c:otherwise>
      <c:url var="dealerSearch" value="/dealer/page/search.do"/>
      <li id="active">
        <a href="${dealerSearch}" id="current">search dealers</a> 
      </li> 	
    </c:otherwise>
  </c:choose>	
</ul>
			
<div id="subsections">
    <ul>
    </ul> 				
</div>
