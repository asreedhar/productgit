<%@include file="/include/tag-import.jsp" %>

<ul id="crumblist">
  <c:choose>
    <c:when test="${not empty dealer}">
      <c:url var="profileURL" value="/dealer/dynamic/profile.do">
        <c:param name="id" value="${dealer.id}"/>
      </c:url>
      <c:choose>
        <c:when test="${not empty dealer.dealerGroup}">
          <c:choose>
            <c:when test="${not empty dealer.dealerGroup.corporation}">
          		<c:choose>
         			<c:when test="${dealer.dealerGroup.corporation.id == 100150}">
         				<li>CRP: ${dealer.dealerGroup.corporation.name}</li>
        			</c:when>
        			<c:otherwise>
           				<c:url var="corpLink" value="/corporation/dynamic/profile.do">
            				<c:param name="id" value="${dealer.dealerGroup.corporation.id}"/>
          				</c:url>
          				<li id="active">CRP: <a href="${corpLink}" id="current">
            				<str:truncateNicely upper="10">${dealer.dealerGroup.corporation.name}</str:truncateNicely>
            			</a></li>
         			</c:otherwise>
      			</c:choose>	
            </c:when>
            <c:otherwise>
              <li>No Corporation</li>
            </c:otherwise>
          </c:choose>
          <c:url var="groupLink" value="/group/dynamic/profile.do">
            <c:param name="id" value="${dealer.dealerGroup.id}"/>
          </c:url>
          <li id="active">GRP: 
          <a href="${groupLink}" id="current"><str:truncateNicely upper="25">${dealer.dealerGroup.name}</str:truncateNicely></a></li>
        </c:when>
        <c:otherwise>
          <li>No Group</li>
        </c:otherwise>
      </c:choose>
      <li>
        DLR: <a href="${profileURL}">${dealer.shortName}</a> &nbsp;
        <c:choose>
	   	 <c:when test="${dealer.active}">
	        	<c:url var="homeUrl" value="/dealer/dynamic/profile.do">
					<c:param name="id" value="${dealer.id}"/>
					<c:param name="home" value="home" />
				</c:url>
				(<a href="${homeUrl}">home</a>)	
		     </c:when>
	     <c:otherwise>
	     	(inactive)
	     </c:otherwise>
     </c:choose>
      </li>
    </c:when>
    <c:otherwise>
      <li>Dealer Search Page</li>
    </c:otherwise>
  </c:choose>
</ul>

