<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function toggleDivs(thirdPartyId) {
	var kids = $('holder').children;
	for(var i = 0; i < kids.length; i++){
		kids[i].style.display='none';
	}
	$(thirdPartyId).style.display = 'block';
}

function toggleDiv() {
	var dd = document.getElementById('internetAdvertisersDropDown');
	if(dd) {
		var divId = 'advertiser' + dd.options[dd.selectedIndex].value;
		toggleDivs(divId);
	}
}
</script>
<c:set var="entityTypeId" value="${4}"/>
<ref:ref beanName="imt-persist-thirdPartyEntityDao" scope="request" var="internetAdvertisers" method="findAllInternetAdvertisers"/>
<div id="advertisingPrefs" class="section left-content">
	<t:insert page="/dealer/display/advertisingPrefs.do"/>
</div>
<div id="advertisers" class="section wide" style="text-align: left;">
	<select id="internetAdvertisersDropDown" 
			name="internetAdvertisersDropDown"
			onchange="toggleDiv();return false;">
		<c:forEach items="${internetAdvertisers}" var="advertiser" varStatus="count">
			<c:if test="${advertiser.typeId == entityTypeId}">
				<c:choose>
				<c:when test="${advertiser.name eq 'AutoTrader'}"><option value="${advertiser.thirdPartyEntityId}" selected="selected">${advertiser.name}</option></c:when>
				<c:otherwise><option value="${advertiser.thirdPartyEntityId}">${advertiser.name}</option></c:otherwise>
				</c:choose>
			</c:if> 
		</c:forEach>
	</select>
<div id="holder">
	<c:forEach items="${internetAdvertisers}" var="advertiser" varStatus="count">
		<c:if test="${advertiser.typeId == 4}">
			<%-- 
				wow, major hack, this basically initializes the detail pane ('holder') to display autotrader by default.
				Hence, i use the <c:choose> above to select autoTrader by default.
			--%>
			<div id="advertiser${advertiser.thirdPartyEntityId}" style="${advertiser.name == 'AutoTrader'?'':'display:none' };clear:none;padding-bottom:0px">
				<t:insert page="/dealer/display/internetAds.do?businessUnitId=${dealer.id}&thirdPartyEntityId=${advertiser.thirdPartyEntityId}"/>
			</div>
		</c:if> 
	</c:forEach>
</div>
</div>

<div class="section wide">
<c:url var="DistributionURL" value="../../../DistributionAdmin/Account.aspx">
	<c:param name="DealerId" value="${dealer.id}"/>
</c:url>
	<a href="${DistributionURL}" onclick="window.open(this.href, this.target, 'width=900,height=500,resizable=yes'); return false;">Distribution  Setup</a>
</div>