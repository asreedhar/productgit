<%@include file="/include/tag-import.jsp" %>
<div id="general">
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:submitAll();">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="generalInfo" class="section">
      <t:insert page="/dealer/${action}/generalInfo.do"/>
    </div>
    <div id="purchasing" class="section">
      <t:insert page="/dealer/${action}/purchasing.do"/>
    </div>
    <div id="accessGroup" class="section">
      <t:insert page="/dealer/custom/addAccessGroup.do"/>
    </div>
    <div id="adesaPurchasing" class="section">
      <t:insert page="/dealer/${action}/adesaPurchasing.do"/>
    </div>
    <div id="ciaEngine" class="section">
      <t:insert page="/dealer/${action}/ciaEngine.do"/>
    </div>
    <div id="dataLoad" class="section">
      <t:insert page="/dealer/${action}/dataLoad.do"/>
    </div>
    <div id="scorecard" class="section">
      <t:insert page="/dealer/${action}/scorecard.do"/>
    </div>
    <div id="redistribution" class="section">
      <t:insert page="/dealer/${action}/redistribution.do"/>
    </div>
  </div>
  
  <div class="right-content">
   	 <div id="guidebook" class="section">
        <t:insert page="/dealer/${action}/guidebook.do"/>
    </div>
   	 <div id="bookoutLock" class="section">
        <t:insert page="/dealer/${action}/bookoutLock.do"/>
    </div>
    <div id="managementCenter" class="section">
      <t:insert page="/dealer/${action}/managementCenter.do"/>
    </div>
    <div id="inventoryBucketRanges" class="section">
        <t:insert page="/dealer/${action}/inventoryBucketRanges.do"/>
    </div>
    <div id="pageSettings" class="section">
      <t:insert page="/dealer/${action}/pageSettings.do"/>
    </div>
  </div>  
</div>
	
	
