<%@include file="/include/tag-import.jsp" %>

<c:forEach items="${destinations}" var="destination">
	<div id="extract_${destination}" class="section wide">
		<t:insert page="/dealer/extracts/show.do?dealerId=${dealer.id}&destinationName=${destination}"/>
	</div>
</c:forEach>

<%--
<div id="extract_${configuration.destinationName}" class="section wide">
	<h2>${configuration.destinationName}</h2>
	<proto:formRemote href="" id="extract_${configuration.destinationName}_form" update="" name="extract_${configuration.destinationName}_form">
		<c:set var="org.apache.struts.taglib.html.BEAN" value="extract_${configuration.destinationName}_form" scope="request" />
		<c:choose>
			<c:when test="${isEdit or param.isEdit eq 'true'}">
				<span class="formButtons"><img id="indicator" src="../../resources/images/rotating-line-load.gif" style="display: none" alt="loading" />
				<h:submit value="Save"
					onclick="${onSubmit};document.getElementById('indicator').style.display = 'inline';" /> 
				<c:url var="cancelURL" value="/dealer/display/upgrade.do?businessUnitId=${param.businessUnitId}&dealerUpgradeCD=${param.dealerUpgradeCD}" />
				<proto:linkRemote href="${cancelURL}" update="dealerUpgrade${param.dealerUpgradeCD}">Cancel</proto:linkRemote></span>
			</c:when>
			<c:otherwise>
				<c:url var="editURL"
					value="/dealer/extracts/edit.do?id=${configuration.dealerId}" />
				<span class="editLink"><proto:linkRemote href="${editURL}"
					update="extract_${configuration.destinationName}">Edit</proto:linkRemote></span>
			</c:otherwise>
		</c:choose>
	
		<h:hidden property="dealerId" value="${configuration.dealerId}"/>
		<h:hidden property="destinationName" value="${configuration.destinationName}"/>
		<tag:checkbox value="${configuration.active}" property="active" title="Active?" />
		<tag:date title="Start Date" property="startDate" value="${configuration.startDate}" />
		<tag:date title="End Date" property="endDate" value="${configuration.endDate}" />
	</proto:formRemote>
</div>
--%>