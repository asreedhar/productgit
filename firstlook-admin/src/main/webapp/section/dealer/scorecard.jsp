<%@include file="/include/tag-import.jsp" %>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:submitAll();">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="wide-content">
    <div id="scorecardNewPerformance" class="section wide">
      <t:insert page="/dealer/${action}/scorecardNewPerformance.do"/>
    </div>
  </div>
</div>

<div class="row">
  <div class="left-content">
    <div id="scorecardUsedOverall" class="section">
      <t:insert page="/dealer/${action}/scorecardUsedOverall.do?id=${dealer.id}"/>
    </div>
    <div id="scorecardUsedAging" class="section">
      <t:insert page="/dealer/${action}/scorecardUsedAging.do?id=${dealer.id}"/>
    </div>
    <div id="scorecardUsedPurchased" class="section">
      <t:insert page="/dealer/${action}/scorecardUsedPurchased.do?id=${dealer.id}"/>
    </div>
  </div>
  <div class="right-content">
    <div id="scorecardNewOverall" class="section">
      <t:insert page="/dealer/${action}/scorecardNewOverall.do?id=${dealer.id}"/>
    </div>
    <div id="scorecardNewAging" class="section">
      <t:insert page="/dealer/${action}/scorecardNewAging.do?id=${dealer.id}"/>
    </div>
    <div id="scorecardUsedTradeIn" class="section">
      <t:insert page="/dealer/${action}/scorecardUsedTradeIn.do?id=${dealer.id}"/>
    </div>
  </div>
</div>
