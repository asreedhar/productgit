<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function checkNadaUpgrade(){
	
	if($("NadaUpgradeCheckBox").checked){
		alert("This upgrade is not available if NADA is already a primary or secondary book for the store")
		return false;
	}
	else{
		return true;
	}
	
}
function saveAllUpgrades(id) {	
	for (i=0; i<document.forms.length; i++) {
		var path = createPath(document.forms[i].name);
		var pars = Form.serialize(document.forms[i])+ '&businessUnitId='+id+'&excludes=startDate,endDate,active';
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
	
}
</script>


  <c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
	<c:choose>
		<c:when test="${param.isEdit}">
			<span style="float:right">
				<a href="javascript:saveAllUpgrades(${dealer.id});">Save All</a>
				<a href="javascript:cancelAll();">Close</a>
			</span>
		</c:when>
		<c:otherwise>
			<span style="float:right">
				<a href="javascript:editAll();">Edit All</a>
			</span>
		</c:otherwise>
	</c:choose>
  <div class="section wide" style="background-color:#e5e5e5">
  	<div style="float:left"><h3>Program Type: ${dealer.preference.programType}</h3></div>
  </div>
  <c:choose>
  	<c:when test="${dealer.preference.programType == 'FIRSTLOOK'}">
  		<div class="left-content">
		    <div id="dealerUpgrade14" class="section">
	     		 <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=14"/>
	    	</div>
	    </div>
	    <div class="right-content">
	    	<div id="dealerUpgrade5" class="section">
	      		<t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=5"/>
	    	</div>
	    </div>
  	</c:when>
	<c:otherwise>
	  <div class="left-content">
	    <div id="dealerUpgrade12" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=12"/>
	    </div>
	    <div id="dealerUpgrade1" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=1"/>
	    </div>
	    <div id="dealerUpgrade2" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=2"/>
	    </div>
	    <div id="dealerUpgrade3" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=3"/>
	    </div>
	    <div id="dealerUpgrade14" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=14"/>
	    </div>
	    <div id="dealerUpgrade11" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=11"/>
	    </div>
	    <div id="dealerUpgrade15" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=15"/>
	    </div>
	    <div id="dealerUpgrade16" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=16"/>
	    </div>
	    <div id="dealerUpgrade21" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=21"/>
	    </div>
	    <div id="dealerUpgrade22" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=22"/>
	    </div>
	    <div id="dealerUpgrade24" class="section">
	    <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=24"/>
	    </div>
	    <div id="dealerUpgrade28" class="section">
	    <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=28"/>
	    </div>
	  </div>
	  <div class="right-content">
	    <div id="dealerUpgrade5" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=5"/>
	    </div>
	    <div id="dealerUpgrade4" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=4"/>
	    </div>
	    <div id="dealerUpgrade7" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=7"/>
	    </div>
	    <div id="dealerUpgrade8" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=8"/>
	    </div>
<%-- 	    <div id="dealerUpgrade18" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=18"/>
	    </div> --%>
	    <!-- Hiding above div as per the case 31118 Remove JD Power Upgrades from FLAdmin -->
	    <div id="dealerUpgrade9" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=9"/>
	    </div>
	    <div id="dealerUpgrade17" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=17"/>
	    </div>
	    <div id="dealerUpgrade19" class="section">
	      <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=19"/>
	    </div>
	    <div id="dealerUpgrade20" class="section">
	    <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=20"/>
	    </div>
	    <div id="dealerUpgrade23" class="section">
	    <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=23"/>
	    </div>
	    <div id="dealerUpgrade25" class="section">
	    <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=25"/>
	    </div>
	    <div id="dealerUpgrade26" class="section">
	    <t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=26"/>
	    </div>
	    <c:if test="${dealer.preference.programType == 'VIP'}">
	      <div id="dealerUpgrade13" class="section">
	      	<t:insert page="/dealer/${action}/upgrade.do?businessUnitId=${dealer.id}&dealerUpgradeCD=13"/>
	      </div>
	    </c:if>
	  </div>
	</c:otherwise>  
</c:choose>



