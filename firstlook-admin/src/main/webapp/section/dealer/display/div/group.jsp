<tag:element title="Dealer Group" id="${dealer.id}" 
             section="dealer" element="group" 
             formBean="${dealer_group}">

  <tag:text title="Group:" property="dealerGroup" value="${dealer.dealerGroup.name}"/><br/>

</tag:element>