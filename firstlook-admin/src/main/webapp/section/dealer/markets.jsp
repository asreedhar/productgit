<%@include file="/include/tag-import.jsp" %>

<div class="row">
<%--   <div id="jdPowerRegion" class="section" style="width: 772px;">
	<t:insert page="/dealer/display/jdPowerRegion.do?id=${dealer.id}"/>
  </div> --%>
  <!-- Hiding above div as per the case 31118 Remove JD Power Upgrades from FLAdmin -->
  <div id="businessUnitMarketDealer" class="section" style="width: 772px;">
	<t:insert page="/dealer/display/businessUnitMarketDealer.do?id=${dealer.id}"/>
  </div>
  <div class="left-content">
    <div id="markets1" class="section">
      <t:insert page="/dealer/display/markets.do?ring=1&businessUnitId=${dealer.id}"/>
    </div>
  </div>
  <div class="right-content">
    <div id="markets2" class="section">
      <t:insert page="/dealer/display/markets.do?ring=2&businessUnitId=${dealer.id}"/>
    </div>
  </div>
</div>
