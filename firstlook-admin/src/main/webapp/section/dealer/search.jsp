<%@include file="/include/tag-import.jsp" %>

<script type="text/javascript">
function searchDealer() {
	var dealer = $F('dealer');
	
	new Ajax.Updater(	'dealerSearchResults', 
						'/fl-admin/dealer/div/search.do?' + dealer, 
						{asynchronous:true, evalScripts:true, method:'post'} ); 
}
</script>

<h2>Search for a Dealer</h2>
Name or Code: <input type="text" id="dealer" size="30"/>
<input type="submit"  onclick="searchDealer();" value="Search" /> <br><br>
<script type="text/javascript">
	$('dealer').focus();	
</script>

<c:set var="breadcrumbHolder" value="${sessionScope['biz.firstlook.app.web.common.struts.util.BreadCrumbUtil']}" />
<c:if test="${not empty breadcrumbHolder}">
	<c:set var="breadcrumbs" value="${breadcrumbHolder.entries}" />
	<b>Most Recently Used Dealers:</b><br />(closing the browser will lose the history)<br />
	<ul>
	<c:forEach items="${breadcrumbs}" var="crumb">
		<c:url var="profileUrl" value="/dealer/dynamic/profile.do">
			<c:param name="id" value="${crumb.value}"/>
		</c:url>
		<c:url var="homeUrl" value="/dealer/dynamic/profile.do">
			<c:param name="id" value="${crumb.value}"/>
			<c:param name="home" value="home" />
		</c:url>
		<li><a href="${profileUrl}">${crumb.label}</a>(<a href="${homeUrl}">home</a>)</li>
	</c:forEach>
	</ul>
</c:if>

<c:url var="allDealersURL" value="/dealer/div/searchAll.do"/>
<h3><proto:linkRemote href="${allDealersURL}" update="dealerSearchResults">list all dealers</proto:linkRemote></h3>
<br />

<div id="dealerSearchResults" style="border: 1px solid black;">
 <center>
   <p>No Search Results</p>
 </center>
</div>

