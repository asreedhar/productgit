<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function saveDealer() {	
	for (i=0; i<document.forms.length-1; i++) {
		if (i == 0){var path = '/fl-admin/dealer/save/dealerProfile.do?'; }
		else {
			var formName =  document.forms[i].name;
			if (formName == ''){ formName = 'dealer_group';}
			var path = createPath(formName); 
		}
		var pars = Form.serialize(document.forms[i]);
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}
</script>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:saveDealer();">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
  <div class="left-content">
    <div id="homePage" class="section">
      <h2>Goto Dealer Home Page:</h2>
      <c:choose>
	        <c:when test="${dealer.active}">
	        	<c:url var="homeUrl" value="/dealer/dynamic/profile.do">
					<c:param name="id" value="${dealer.id}"/>
					<c:param name="home" value="home" />
				</c:url>
				(<a href="${homeUrl}">home</a>)	
		     </c:when>
	     <c:otherwise>
	     	(inactive)
	     </c:otherwise>
   </c:choose>
    </div>

    <div id="dealerProfile" class="section">
      <t:insert page="/dealer/${action}/dealerProfile.do"/>
    </div>
    <div id="address" class="section">
      <t:insert page="/dealer/${action}/address.do"/>
    </div>
  </div>


  <div class="right-content">
    <div id="general" class="section">
      <t:insert page="/dealer/${action}/general.do"/>
    </div>
    <div id="group" class="section">
      <t:insert page="/dealer/${action}/group.do"/>
    </div>
    <div id="franchises" class="section">
      <t:insert page="/dealer/display/franchise.do"/>
    </div>
  </div>



