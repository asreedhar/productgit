<%@include file="/include/tag-import.jsp" %>

<br/>
<div class="row">
  <div class="left-content">
    <div id="members" class="section">
      <t:insert page="/dealer/display/members.do"/>
    </div>
  </div>
  <div class="right-content">
	<div id="create" class="section">
	  <jsp:include page="/section/dealer/div/createMember.jsp"/>
	</div>
	<div id="add" class="section">
	  <jsp:include page="/section/dealer/div/addExistingMember.jsp"/>
	</div>
  </div>
</div>