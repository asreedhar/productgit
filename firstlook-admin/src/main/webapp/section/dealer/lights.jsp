<%@include file="/include/tag-import.jsp" %>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:submitAll();">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="lights" class="section">
      <t:insert page="/dealer/${action}/lights.do"/>
    </div>
    <div id="ciaPreferences" class="section">
      <t:insert page="/dealer/${action}/ciaPreferences.do"/>
    </div>
  </div>
  <div class="right-content">
    <div id="yearThresholds" class="section">
      <t:insert page="/dealer/${action}/yearThresholds.do"/>
    </div>
    <div id="riskAssessment" class="section">
      <t:insert page="/dealer/${action}/riskAssessment.do"/>
    </div>
  </div>
</div>
