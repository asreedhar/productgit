<%@include file="/include/tag-import.jsp" %>

<h2>Dealer Group</h2>

<c:url var="saveURL" value="/dealer/save/group.do"/>
<proto:formRemote href="${saveURL}" update="group" id="groupForm">
  <input type="hidden" name="id" value="${dealer.id}"/>

  <span class="formButtons">
     <input type="submit" value="Save"/>
  </span>
  <ref:ref beanName="imt-persist-dealerGroupDao" scope="request"/>
  <div>
  <label for="group" class="fieldLabel">Group:</label>
  <select name="groupId" width="40" style="font-family: courier">
    <c:forEach items="${values}" var="group">
      <option value="${group.id}" ${group.id == dealer.dealerGroup.id ? 'selected':'no'}>
        ${group.name}
      </option>
    </c:forEach>
  </select>
  </div>     
</proto:formRemote>