<%@include file="/include/tag-import.jsp"%>
<tag:element title="JD Power PowerRegion" id="${dealer.id}" section="dealer"
	element="jdPowerRegion"
	formBean="${dealer_jdPowerRegion}">
	
<div>
	<tag:select title="Power Region" property="powerRegionId"
				value="${dealer.jdPowerRegion.description}"
				selectedValue="${dealer.jdPowerRegion.id}"
				emptyContent="No Mappings set.">
		<c:forEach items="${regions}" var="region">
			<tag:option value="${region.id}" selected="${dealer.jdPowerRegion.id}">${region.description}</tag:option>
		</c:forEach>
	</tag:select>
</div>
</tag:element>

