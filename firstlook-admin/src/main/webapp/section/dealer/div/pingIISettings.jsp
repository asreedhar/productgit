<%@include file="/include/tag-import.jsp" %>

<c:url var="validation" value="/resources/js/bucketRanges.js"/>
<script src="${validation}" type="text/javascript"></script>

<script type="text/javascript">
function submitForm(dealerId){
	var i=1;
	var flag=true;
	var largestbucket=6;
	for(i=1;i<7;i++)
	{
	
	var max=eval(document.getElementById("age"+i+"MaxMarketRange").value);
	var min=eval(document.getElementById("age"+i+"MinMarketRange").value);
	var bucketvalue=document.getElementById("agerange"+i).value;
		
		if(max<=min&&(bucketvalue!=''))
		{
			alert("The Max Value must be greater than the Min Value\n Please enter different values for Bucket #"+i);
			return false;
		
		}
		
	var bucketvaluerev=document.getElementById("agerange"+(7-i)).value;
	if(eval(bucketvaluerev)===0|| bucketvaluerev==='')
		{
		flag=false;
		largestbucket=7-i;
		}
	
	}


	for(i=largestbucket+1;i<7;i++)
	{
		var bucketvalue=document.getElementById("agerange"+i).value;
		
		if(bucketvalue!=0 || bucketvalue!='')
		{
		alert("There cannot be age buckets after an Open Ended Bucket");
		return false;
		}
	}
	if(flag)
		{alert("The Largest Age Bucket Must be open ended");
		
		return false;
		}
	
	var defaultRadiusOptions = document.getElementById("defaultSearchRadiusId");
    var selectedRadiusValue = defaultRadiusOptions.options[defaultRadiusOptions.selectedIndex].value;
    
    var maxSearchRadiusOptions = document.getElementById("maxSearchRadiusId");
    var maxSearchRadiusIdValue = maxSearchRadiusOptions.options[maxSearchRadiusOptions.selectedIndex].value;
    

	if( parseInt(selectedRadiusValue) > 100 || parseInt(selectedRadiusValue) == -1 || parseInt(maxSearchRadiusIdValue) > 100 || parseInt(maxSearchRadiusIdValue) == -1 ) {
		if(!confirm('Increasing the search to this radius may negatively impact system performance for this dealer.  You should understand the dealership, their market, their general geographic area, and top selling brands before making this change.'))
			return false;
	}
	
	if( parseInt(selectedRadiusValue) > parseInt(maxSearchRadiusIdValue) ) {
		alert('The max search radius must be greater than the default radius. Please modify and resubmit');
		return false;
	}
	
	
	var pars = Form.serialize(document.forms['pingIIForm']); 
	new Ajax.Updater(	'pingIIPrefs', 
						'/fl-admin/dealer/save/pingIISettings.do?dealerId=' + dealerId, 
						{asynchronous:true, evalScripts:true, method:'post', parameters:pars} ); 
	
	return false;
}
</script>

<div id="pingIIPrefs">

<%-- Age Buckets --%>
<c:set var="age_range1"><b:write name="age_ranges" property="range1"/></c:set> 
<c:set var="age_range1MinMarket"><b:write name="age_ranges" property="range1Value1"/></c:set> 
<c:set var="age_range1MaxMarket"><b:write name="age_ranges" property="range1Value2"/></c:set> 
<c:set var="age_range1MinGross"><b:write name="age_ranges" property="range1Value3"/></c:set> 
<c:set var="age_range1High"><b:write name="age_ranges" property="range1High"/></c:set> 
<c:set var="age_range2"><b:write name="age_ranges" property="range2"/></c:set>
<c:set var="age_range2MinMarket"><b:write name="age_ranges" property="range2Value1"/></c:set>
<c:set var="age_range2MaxMarket"><b:write name="age_ranges" property="range2Value2"/></c:set>
<c:set var="age_range2MinGross"><b:write name="age_ranges" property="range2Value3"/></c:set>
<c:set var="age_range2Low"><b:write name="age_ranges" property="range2Low"/></c:set> 
<c:set var="age_range2High"><b:write name="age_ranges" property="range2High"/></c:set> 
<c:set var="age_range3"><b:write name="age_ranges" property="range3"/></c:set>
<c:set var="age_range3MinMarket"><b:write name="age_ranges" property="range3Value1"/></c:set>
<c:set var="age_range3MaxMarket"><b:write name="age_ranges" property="range3Value2"/></c:set>
<c:set var="age_range3MinGross"><b:write name="age_ranges" property="range3Value3"/></c:set>
<c:set var="age_range3Low"><b:write name="age_ranges" property="range3Low"/></c:set> 
<c:set var="age_range3High"><b:write name="age_ranges" property="range3High"/></c:set> 
<c:set var="age_range4"><b:write name="age_ranges" property="range4"/></c:set>
<c:set var="age_range4MinMarket"><b:write name="age_ranges" property="range4Value1"/></c:set>
<c:set var="age_range4MaxMarket"><b:write name="age_ranges" property="range4Value2"/></c:set>
<c:set var="age_range4MinGross"><b:write name="age_ranges" property="range4Value3"/></c:set>
<c:set var="age_range4Low"><b:write name="age_ranges" property="range4Low"/></c:set> 
<c:set var="age_range4High"><b:write name="age_ranges" property="range4High"/></c:set> 
<c:set var="age_range5"><b:write name="age_ranges" property="range5"/></c:set>
<c:set var="age_range5MinMarket"><b:write name="age_ranges" property="range5Value1"/></c:set>
<c:set var="age_range5MaxMarket"><b:write name="age_ranges" property="range5Value2"/></c:set>
<c:set var="age_range5MinGross"><b:write name="age_ranges" property="range5Value3"/></c:set>
<c:set var="age_range5Low"><b:write name="age_ranges" property="range5Low"/></c:set> 
<c:set var="age_range5High"><b:write name="age_ranges" property="range5High"/></c:set>
<c:set var="age_range6"><b:write name="age_ranges" property="range6"/></c:set>
<c:set var="age_range6MinMarket"><b:write name="age_ranges" property="range6Value1"/></c:set>
<c:set var="age_range6MaxMarket"><b:write name="age_ranges" property="range6Value2"/></c:set>
<c:set var="age_range6MinGross"><b:write name="age_ranges" property="range6Value3"/></c:set>
<c:set var="age_range6Low"><b:write name="age_ranges" property="range6Low"/></c:set> 
<c:set var="age_range6High"><b:write name="age_ranges" property="range6High"/></c:set>

<%-- Mileage Buckets --%>
<c:set var="mileage_range1"><b:write name="mileage_ranges" property="range1" /></c:set>
<c:set var="mileage_range1LowAdj"><b:write name="mileage_ranges" property="range1Value1" /></c:set>
<c:set var="mileage_range1HighAdj"><b:write name="mileage_ranges" property="range1Value2" /></c:set>
<c:set var="mileage_range1Low"><b:write name="mileage_ranges" property="range1Low" /></c:set>
<c:set var="mileage_range1High"><b:write name="mileage_ranges" property="range1High" /></c:set>
<c:set var="mileage_range2"><b:write name="mileage_ranges" property="range2" /></c:set>
<c:set var="mileage_range2LowAdj"><b:write name="mileage_ranges" property="range2Value1" /></c:set>
<c:set var="mileage_range2HighAdj"><b:write name="mileage_ranges" property="range2Value2" /></c:set>
<c:set var="mileage_range2Low"><b:write name="mileage_ranges" property="range2Low" /></c:set>
<c:set var="mileage_range2High"><b:write name="mileage_ranges" property="range2High" /></c:set>
<c:set var="mileage_range3"><b:write name="mileage_ranges" property="range3" /></c:set>
<c:set var="mileage_range3LowAdj"><b:write name="mileage_ranges" property="range3Value1" /></c:set>
<c:set var="mileage_range3HighAdj"><b:write name="mileage_ranges" property="range3Value2" /></c:set>
<c:set var="mileage_range3Low"><b:write name="mileage_ranges" property="range3Low" /></c:set>
<c:set var="mileage_range3High"><b:write name="mileage_ranges" property="range3High" /></c:set>
<c:set var="mileage_range4"><b:write name="mileage_ranges" property="range4" /></c:set>
<c:set var="mileage_range4LowAdj"><b:write name="mileage_ranges" property="range4Value1" /></c:set>
<c:set var="mileage_range4HighAdj"><b:write name="mileage_ranges" property="range4Value2" /></c:set>
<c:set var="mileage_range4Low"><b:write name="mileage_ranges" property="range4Low" /></c:set>
<c:set var="mileage_range4High"><b:write name="mileage_ranges" property="range4High" /></c:set>
<c:set var="mileage_range5"><b:write name="mileage_ranges" property="range5" /></c:set>
<c:set var="mileage_range5LowAdj"><b:write name="mileage_ranges" property="range5Value1" /></c:set>
<c:set var="mileage_range5HighAdj"><b:write name="mileage_ranges" property="range5Value2" /></c:set>
<c:set var="mileage_range5Low"><b:write name="mileage_ranges" property="range5Low" /></c:set>
<c:set var="mileage_range5High"><b:write name="mileage_ranges" property="range5High" /></c:set>
<c:set var="mileage_range6"><b:write name="mileage_ranges" property="range6" /></c:set>
<c:set var="mileage_range6LowAdj"><b:write name="mileage_ranges" property="range6Value1" /></c:set>
<c:set var="mileage_range6HighAdj"><b:write name="mileage_ranges" property="range6Value2" /></c:set>
<c:set var="mileage_range6Low"><b:write name="mileage_ranges" property="range6Low" /></c:set>
<c:set var="mileage_range6High"><b:write name="mileage_ranges" property="range6High" /></c:set>
<c:set var="mileage_range7"><b:write name="mileage_ranges" property="range7" /></c:set>
<c:set var="mileage_range7LowAdj"><b:write name="mileage_ranges" property="range7Value1" /></c:set>
<c:set var="mileage_range7HighAdj"><b:write name="mileage_ranges" property="range7Value2" /></c:set>
<c:set var="mileage_range7Low"><b:write name="mileage_ranges" property="range7Low" /></c:set>
<c:set var="mileage_range7High"><b:write name="mileage_ranges" property="range7High" /></c:set>

<!--///////////////// NON-EDITABLE MODE //////////////-->
<c:if test="${!isEdit}">
	<h1>Ping II Settings</h1>
	<h3>
	<a onclick="new Ajax.Updater('pingIIPrefs', '/fl-admin/dealer/edit/pingIISettings.do?state=edit&dealerId=${dealer.id}', 
	{asynchronous:true, evalScripts:true } ); return false;" href="#" >edit</a>
	</h3>
	<br/>
	Default Search Radius: <c:choose><c:when test="${preference.pingII_DefaultSearchRadius == -1}">Unlimited</c:when><c:otherwise>${preference.pingII_DefaultSearchRadius} mi</c:otherwise></c:choose><br/>
	<span style="display:none">Max Search Radius: <c:choose><c:when test="${preference.pingII_MaxSearchRadius == -1}">750</c:when><c:otherwise>${preference.pingII_MaxSearchRadius}</c:otherwise></c:choose> mi<br/></span>
	Supress Sellers Name: ${preference.pingII_SupressSellerName}<br/>
	Exclude No Price &amp; Zero Price from Calculations: ${preference.pingII_ExcludeNoPriceFromCalc}<br/>
	Match Certified By Default: ${preference.pingII_MatchCertifiedByDefault}<br/>
	Suppress Trim Match Data: ${preference.pingII_SuppressTrimMatchStatus}<br/><br/>
	Turn on New Ping : ${preference.pingII_NewPing}<br/>  
  
  	How to default Competitive Set Search in the MAX Pricing Tool? : ${preference.pingII_MarketListing == 'T'? "Automatically matched to Trim" : preference.pingII_MarketListing == 'O' ? "Automatically matched to Trim & Options":"None" }
	<h2>New Ping Pricing Indicator Settings</h2>
	<table border="1" width="700px" align="center">
		<tr>
			<td><span id="color_red">Red</span> 
			<td>Above ${preference.pingII_RedRange1Value1}%</td><td>Below ${preference.pingII_RedRange1Value2}%</td><td></td><td></td>
		</tr>		
		<tr>
			<td><span id="color_yellow">Yellow</span> 
			<td>From ${preference.pingII_YellowRange1Value1}%</td><td>To ${preference.pingII_YellowRange1Value2}%</td><td> From ${preference.pingII_YellowRange2Value1}%</td><td>To ${preference.pingII_YellowRange2Value2}%</td>
		</tr>
		<tr>
			<td><span id="color_green">Green</span> 
			<td>From ${preference.pingII_GreenRange1Value1}%</td><td>To ${preference.pingII_GreenRange1Value2}%</td><td></td><td></td>
		</tr>	
    </table>
	
	
	<h3>Remove External Listing Price Outliers</h3>
	Low Price Multiplier: ${preference.pingII_ExcludeLowPriceOutliersMultiplier}<br/>
	High Price Multiplier: ${preference.pingII_ExcludeHighPriceOutliersMultiplier}<br/>
	
	<h3>Guide Book To Use For Options</h3>
	<ref:single beanName="imt-persist-thirdPartyDao" scope="request" var="guidebook1" idVal="${preference.pingII_GuideBookId}"/>
	<tag:select title="Book:" property="guideBookId" value="${guidebook1.name}"
	              options="guideBooks" selectedValue="${preference.pingII_GuideBookId}"/>
	
	
	<h2>Default Ping Age Buckets</h2>
	<table border="1" align="center">
		<tr><td>Age Bucket</td><td>Min Market %</td><td>Max Market %</td><td>Min Gross</td><tr>
		<tr>
			<td><span id="age_range1LowDisp">0</span> - <span id="age_range1HighDisp">${age_range1High}</span> Days
			<td>${age_range1MinMarket}</td><td>${age_range1MaxMarket}</td><td>${age_range1MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range2LowDisp">${age_range2Low}</span> <c:if test="${age_range2High > 0}"> - <span id="age_range2HighDisp">${age_range2High}</span></c:if><c:if test="${age_range2High eq 0}">+</c:if> Days
			<td>${age_range2MinMarket}</td><td>${age_range2MaxMarket}</td><td>${age_range2MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range3LowDisp">${age_range3Low}</span> <c:if test="${age_range3High > 0}"> - <span id="age_range3HighDisp">${age_range3High}</span></c:if><c:if test="${age_range3High eq 0}">+</c:if> Days
			<td>${age_range3MinMarket}</td><td>${age_range3MaxMarket}</td><td>${age_range3MinGross}</td>
		</tr>	
		<tr>
			<td><span id="age_range4LowDisp">${age_range4Low}</span> <c:if test="${age_range4High > 0}"> - <span id="age_range4HighDisp">${age_range4High}</span></c:if><c:if test="${age_range4High eq 0}">+</c:if> Days
			<td>${age_range4MinMarket}</td><td>${age_range4MaxMarket}</td><td>${age_range4MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range5LowDisp">${age_range5Low}</span> <c:if test="${age_range5High > 0}"> - <span id="age_range5HighDisp">${age_range5High}</span></c:if><c:if test="${age_range5High eq 0}">+</c:if> Days
			<td>${age_range5MinMarket}</td><td>${age_range5MaxMarket}</td><td>${age_range5MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range6LowDisp">${age_range6Low}</span> <c:if test="${age_range6High > 0}"> - <span id="age_range6HighDisp">${age_range6High}</span></c:if><c:if test="${age_range6High eq 0}">+</c:if> Days
			<td>${age_range6MinMarket}</td><td>${age_range6MaxMarket}</td><td>${age_range6MinGross}</td>
		</tr>		
    </table>

    <h2>Ping II Mileage Adjustments</h2>
    <table border="1" align="center">
        <tr><td>Vehicle Mileage / (Current Year - Vehicle Year + 1)</td><td>Low Adjustment</td><td>High Adjustment</td></tr>
        <tr>
            <td><span>0</span> - <span>${mileage_range1High}</span></td>
            <td>${mileage_range1LowAdj}%</td><td>${mileage_range1HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range2Low}</span> - <span>${mileage_range2High}</span></td>
            <td>${mileage_range2LowAdj}%</td><td>${mileage_range2HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range3Low}</span> - <span>${mileage_range3High}</span></td>
            <td>${mileage_range3LowAdj}%</td><td>${mileage_range3HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range4Low}</span> - <span>${mileage_range4High}</span></td>
            <td>${mileage_range4LowAdj}%</td><td>${mileage_range4HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range5Low}</span> - <span>${mileage_range5High}</span></td>
            <td>${mileage_range5LowAdj}%</td><td>${mileage_range5HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range6Low}</span> - <span>${mileage_range6High}</span></td>
            <td>${mileage_range6LowAdj}%</td><td>${mileage_range6HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range7Low}</span> +</td>
            <td>${mileage_range7LowAdj}%</td><td>${mileage_range7HighAdj}%</td>
        </tr>
    </table>
	
	<h2>Ping II Package Matrix</h2>
	
	<table border="1">
		<tr>
			<th>&nbsp;</th>
			<th>Package</th>
			<th>Page Brand</th>
			<th>Notes Brand</th>
			<th>Has Pricing Information</th>
			<th>Has Mystery Shopping</th>
			<th>Has Notes</th>
			<th>Has Merchandising</th>
			<th>Has One-Click Toolbar</th>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 1 ? "X" : "&nbsp;"}</td>
			<td>1</td>
			<td>PING II Internet Pricing Analyzer</td>
			<td>-</td>
			<td>No</td><td>No</td><td>No</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 2 ? "X" : "&nbsp;"}</td>
			<td>2</td>
			<td>PING II with Internet Mystery Shopping</td>
			<td>-</td>
			<td>No</td><td>Yes</td><td>No</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 3 ? "X" : "&nbsp;"}</td>
			<td>3</td>
			<td>360� Pricing with Internet Pricing Analyzer</td>
			<td>Notes</td>
			<td>Yes</td><td>No</td><td>Yes</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 4 ? "X" : "&nbsp;"}</td>
			<td>4</td>
			<td>360� Pricing with Internet Mystery Shopping</td>
			<td>Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 5 ? "X" : "&nbsp;"}</td>
			<td>5</td>
			<td>360� Internet Advertising Accelerator</td>
			<td>Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>No</td>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 6 ? "X" : "&nbsp;"}</td>
			<td>6</td>
			<td>360� Internet Advertising Accelerator</td>
			<td>Selling Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td>
		</tr>
		<!--
		<tr>
			<td>${preference.pingII_PackageId == 7 ? "X" : "&nbsp;"}</td>
			<td>7</td>
			<td>Market Pricing Power</td>
			<td>Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>No</td>
		</tr>
		<tr>
			<td>${preference.pingII_PackageId == 8 ? "X" : "&nbsp;"}</td>
			<td>8</td>
			<td>Market Pricing Power with Internet Advertising Accelerator</td>
			<td>Selling Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td>
		</tr> -->
	</table>
	
</c:if>

<!--///////////////// EDITABLE MODE //////////////-->
<c:if test="${isEdit}">
	<h1>Ping II Settings</h1>
	<h3>
		<a onclick=" new Ajax.Updater('pingIIPrefs', '/fl-admin/dealer/display/pingIISettings.do?state=display&dealerId=${dealer.id}', 
			{asynchronous:true, evalScripts:true } ); return false;" href="#" >cancel</a>
	</h3><br/>
	
	<form name="pingIIForm" action="#">
		Default Search Radius: 
			<select id="defaultSearchRadiusId" name="defaultSearchRadius" >
			<option value="10" ${preference.pingII_DefaultSearchRadius == 10 ? 'selected=""':'' }>10</option>
			<option value="25" ${preference.pingII_DefaultSearchRadius == 25 ? 'selected=""':'' }>25</option>
			<option value="50" ${preference.pingII_DefaultSearchRadius == 50 ? 'selected=""':'' }>50</option>
			<option value="75" ${preference.pingII_DefaultSearchRadius == 75 ? 'selected=""':'' }>75</option>
			<option value="100" ${preference.pingII_DefaultSearchRadius == 100 ? 'selected=""':'' }>100</option>
			<option value="150" ${preference.pingII_DefaultSearchRadius == 150 ? 'selected=""':'' }>150</option>
			<option value="250" ${preference.pingII_DefaultSearchRadius == 250 ? 'selected=""':'' }>250</option>
			<option value="500" ${preference.pingII_DefaultSearchRadius == 500 ? 'selected=""':'' }>500</option>
			<option value="750" ${preference.pingII_DefaultSearchRadius == 750 ? 'selected=""':'' }>750</option>
			<option value="1000" ${preference.pingII_DefaultSearchRadius == 1000 ? 'selected=""':'' }>1000</option>
			<option value="-1" ${preference.pingII_DefaultSearchRadius == -1 ? 'selected=""':'' }>Unlimited</option>

		</select><br/>
			<span style="display:none">
			Max Search Radius: 
			<select id="maxSearchRadiusId" name="maxSearchRadius" >
		    	<option value="1000" selected="selected">1000</option>
			</select></span>
		Supress Sellers Name: 
		<input type="checkbox" name="supressSellerName" ${preference.pingII_SupressSellerName?'checked="checked"':'' }/><br/>
		Exclude No Price &amp; Zero Price from Calculations:
		<input type="checkbox" name="excludeNoPriceFromCalc" ${preference.pingII_ExcludeNoPriceFromCalc?'checked="checked"':'' }/><br/>
		Match Certified By Default:
		<input type="checkbox" name="matchCertifiedByDefault" ${preference.pingII_MatchCertifiedByDefault?'checked="checked"':'' }/><br/>
		Suppress Trim Match Data:
		<input type="checkbox" name="suppressTrimMatchStatus" ${preference.pingII_SuppressTrimMatchStatus?'checked="checked"':'' }/><br/>
		<br/>
  <br/>
  
  Turn on New Ping :
  <%-- <select name="newPing" >
			<option value="1" ${preference.pingII_NewPing == true ? 'selected=""':'' }>Yes</option>
			<option value="0" ${preference.pingII_NewPing == true ? 'selected=""':'' }>No</option>
		</select> --%>
<select name="newPing" >
  <c:choose>
  <c:when test="${not empty preference.pingII_NewPing  && preference.pingII_NewPing == true}">
   <option value="1" selected="selected" >Yes</option>
			<option value="0">No</option>
  </c:when>
  <c:otherwise>
    <option value="1">Yes</option>
			<option value="0" selected="selected" >No</option>
  </c:otherwise>
  </c:choose>
  </select>
  <br/><br/>
  How to default Competitive Set Search in the MAX Pricing Tool? :
  <select name = "marketListing">
  <option value = 'T' ${preference.pingII_MarketListing == 'T' ? 'selected=""':'' }>Trim Only</option>
  <option value = 'O' ${preference.pingII_MarketListing == 'O' ? 'selected=""':'' }>Trim & Equipment </option> 
  
  </select>
  <h2>New Ping Pricing Indicator Settings</h2>
	<span id="pricing"></span>	

    <table width="700px" align="center" >
     <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
    
	    <td>
	    Red Light % :
	     </td>
	    <td>Above: <input type="text" name="red_range1Value1" id="redMinMarketRange" value="${preference.pingII_RedRange1Value1}" size="4"/></td>
	    <td>Below: <input type="text" name="red_range1Value2" id="redMaxMarketRange" value="${preference.pingII_RedRange1Value2}" size="2"/></td>
	    <td></td>
	    <td></td>

    </tr>
     <tr>
	    <td>
	    Yellow Light %:
	    </td>
	    <td> From : <input type="text" name="yellow_range1Value1" id="yellowMinMarketRange" value="${preference.pingII_YellowRange1Value1}" size="4"/></td>
	    <td> To :  <input type="text" name="yellow_range1Value2" id="yellowMaxMarketRange" value="${preference.pingII_YellowRange1Value2}" size="5"/></td>
	    <td> & From: <input type="text" name="yellow_range2Value1" id="yellowMinMarketRange" value="${preference.pingII_YellowRange2Value1}" size="4"/></td>
	    <td> To: <input type="text" name="yellow_range2Value2" id="yellowMinMarketRange" value="${preference.pingII_YellowRange2Value2}" size="4"/></td>
    </tr>
     <tr>
	    <td>
	    Green Light %:
	    </td>
	    <td> From  : <input type="text" name="green_range1Value1" id="greenMinMarketRange" value="${preference.pingII_GreenRange1Value1}" size="4"/></td>
	    <td> To : <input type="text" name="green_range1Value2" id="greenMaxMarketRange" value="${preference.pingII_GreenRange1Value2}" size="5"/></td>
	  	<td></td>
	    <td></td>
    </tr>
    
    </table> 
  		
  		<h3>Remove External Listing Price Outliers</h3>
		Low Price Multiplier:
		<input type="text" name="excludeLowPriceOutliersMultiplier" value="${preference.pingII_ExcludeLowPriceOutliersMultiplier}" size="4"/><br/>
		High Price Multiplier:
		<input type="text" name="excludeHighPriceOutliersMultiplier" value="${preference.pingII_ExcludeHighPriceOutliersMultiplier}" size="4" /><br/>

		<h3>Guide Book To Use For Options</h3>
		<ref:single beanName="imt-persist-thirdPartyDao" scope="request" var="guidebook1" idVal="${preference.pingII_GuideBookId}"/>
		<tag:select title="Book:" property="guideBookId" value="${guidebook1.name}"
		              options="guideBooks" selectedValue="${preference.pingII_GuideBookId}"/>


		<h2>Ping Age Buckets</h2>

	<span id="message"></span>	

    <table width="540px" align="center">
    <tr>
        <td>Bucket Size</td>
        <td>Bucket Range</td>
        <td>Min Market %</td>
        <td>Max Market %</td>
        <td>Min Gross</td>
    </tr>
    <tr>
	    <td>
	    Bucket #1*:
	    <input type="text" name="age_range1" value="${age_range1}" id="agerange1" size="3" onchange="recalculateDateRanges(1, false, 'age_');"/>
	    </td>
	    <td>
	    	<span id="age_range1LowDisp">0</span> - <span id="age_range1HighDisp">${age_range1High}</span> Days
	    </td>
	    <td><input type="text" name="age_range1Value1" id="age1MinMarketRange" value="${age_range1MinMarket}" size="4"/></td>
	    <td><input type="text" name="age_range1Value2" id="age1MaxMarketRange" value="${age_range1MaxMarket}" size="4"/></td>
	    <td><input type="text" name="age_range1Value3" value="${age_range1MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #2*:
	    <input type="text" name="age_range2"value="${age_range2}" id="agerange2" size="3" onchange="recalculateDateRanges(2, false, 'age_');"/>
    	</td><td>
	    	<span id="age_range2LowDisp">${age_range2Low}</span> - <span id="age_range2HighDisp">${age_range2High}</span> Days
   		</td>
       	<td><input type="text" name="age_range2Value1" id="age2MinMarketRange" value="${age_range2MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range2Value2" id="age2MaxMarketRange" value="${age_range2MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range2Value3" value="${age_range2MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #3*:
	    	<input type="text" name="age_range3" value="${age_range3}" id="agerange3" size="3" onchange="recalculateDateRanges(3, false, 'age_');"/>
	    </td><td>
	    	<span id="age_range3LowDisp">${age_range3Low}</span> - <span id="age_range3HighDisp">${age_range3High}</span> Days
    	</td>
    	<td><input type="text" name="age_range3Value1" id="age3MinMarketRange" value="${age_range3MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range3Value2" id="age3MaxMarketRange" value="${age_range3MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range3Value3" value="${age_range3MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #4:
	    	<input type="text" name="age_range4" value="${age_range4}" id="agerange4" size="3" onchange="recalculateDateRanges(4, false, 'age_');"/>
	    </td><td>
		    <c:choose>
		        <c:when test="${age_range4Low == 0 && age_range4High == 0}"><span id="age_range4LowDisp"></span> - <span id="age_range4HighDisp"></span> Days</c:when>
		        <c:when test="${age_range4Low != 0 && age_range4High == 0}"><span id="age_range4LowDisp">${age_range4Low}</span> - <span id="age_range4HighDisp">+</span> Days</c:when>
		        <c:otherwise><span id="age_range4LowDisp">${age_range4Low}</span> - <span id="age_range4HighDisp">${age_range4High}</span> Days</c:otherwise>
		    </c:choose>  
    	</td>
    	<td><input type="text" name="age_range4Value1" id="age4MinMarketRange" value="${age_range4MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range4Value2" id="age4MaxMarketRange" value="${age_range4MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range4Value3" value="${age_range4MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #5:        
	    	<input type="text" name="age_range5" value="${age_range5}" id="agerange5" size="3" onchange="recalculateDateRanges(5, false, 'age_');"/>
	    </td><td>  
		    <c:choose>
		        <c:when test="${age_range5Low == 0 && age_range5High == 0}"><span id="age_range5LowDisp"></span> - <span id="age_range5HighDisp"></span> Days</c:when>
		        <c:when test="${age_range5Low != 0 && age_range5High == 0}"><span id="age_range5LowDisp">${age_range5Low}</span> - <span id="age_range5HighDisp">+</span> Days</c:when>
		        <c:otherwise><span id="age_range5LowDisp">${age_range5Low}</span> - <span id="age_range5HighDisp">${age_range5High}</span> Days</c:otherwise>
		    </c:choose>  
    	</td>
       	<td><input type="text" name="age_range5Value1" id="age5MinMarketRange" value="${age_range5MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range5Value2" id="age5MaxMarketRange" value="${age_range5MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range5Value3" value="${age_range5MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #6:        
	    	<input type="text" name="age_range6" value="${age_range6}" id="agerange6" size="3" onchange="recalculateDateRanges(6, false, 'age_');"/>
	    </td><td>  
		    <c:choose>
		        <c:when test="${age_range6Low == 0 && age_range6High == 0}"><span id="age_range6LowDisp"></span> - <span id="age_range6HighDisp"></span> Days</c:when>
		        <c:when test="${age_range6Low != 0 && age_range6High == 0}"><span id="age_range6LowDisp">${age_range6Low}</span> - <span id="age_range6HighDisp">+</span> Days</c:when>
		        <c:otherwise><span id="age_range6LowDisp">${age_range6Low}</span> - <span id="age_range6HighDisp">${age_range6High}</span> Days</c:otherwise>
		    </c:choose>  
    	</td>
       	<td><input type="text" name="age_range6Value1" id="age6MinMarketRange" value="${age_range6MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range6Value2" id="age6MaxMarketRange"value="${age_range6MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range6Value3" value="${age_range6MinGross}" size="4"/></td>    	
    </tr>
    </table> 
    
    <h:hidden property="useWatchList" styleId="useWatchList" value="false"/>
    <h:hidden property="age_range1Low" styleId="age_range1Low" value="${age_range1Low}"/>
    <h:hidden property="age_range1High" styleId="age_range1High" value="${age_range1High}"/>
    <h:hidden property="age_range2Low" styleId="age_range2Low" value="${age_range2Low}"/>
    <h:hidden property="age_range2High" styleId="age_range2High" value="${age_range2High}"/>
    <h:hidden property="age_range3Low" styleId="age_range3Low" value="${age_range3Low}"/>
    <h:hidden property="age_range3High" styleId="age_range3High" value="${age_range3High}"/>
    <h:hidden property="age_range4Low" styleId="age_range4Low" value="${age_range4Low}"/>
    <h:hidden property="age_range4High" styleId="age_range4High" value="${age_range4High}"/>
    <h:hidden property="age_range5Low" styleId="age_range5Low" value="${age_range5Low}"/>
    <h:hidden property="age_range5High" styleId="age_range5High" value="${age_range5High}"/>
    <h:hidden property="age_range6Low" styleId="age_range6Low" value="${age_range6Low}"/>
    <h:hidden property="age_range6High" styleId="age_range6High" value="${age_range6High}"/>

    <h2>Ping II Mileage Adjustments</h2>
    <table border="1" align="center">
        <tr><td>Vehicle Mileage / (Current Year - Vehicle Year + 1)</td><td>Low Adjustment</td><td>High Adjustment</td></tr>
        <tr>
            <td>Bucket #1:<input type="text" name="mileage_range1" value="${mileage_range1}" size="5" onchange="recalculateDateRanges(1, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range1Value1" value="${mileage_range1LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range1Value2" value="${mileage_range1HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #2:<input type="text" name="mileage_range2" value="${mileage_range2}" size="5" onchange="recalculateDateRanges(2, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range2Value1" value="${mileage_range2LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range2Value2" value="${mileage_range2HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #3:<input type="text" name="mileage_range3" value="${mileage_range3}" size="5" onchange="recalculateDateRanges(3, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range3Value1" value="${mileage_range3LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range3Value2" value="${mileage_range3HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #4:<input type="text" name="mileage_range4" value="${mileage_range4}" size="5" onchange="recalculateDateRanges(4, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range4Value1" value="${mileage_range4LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range4Value2" value="${mileage_range4HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #5:<input type="text" name="mileage_range5" value="${mileage_range5}" size="5" onchange="recalculateDateRanges(5, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range5Value1" value="${mileage_range5LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range5Value2" value="${mileage_range5HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #6:<input type="text" name="mileage_range6" value="${mileage_range6}" size="5" onchange="recalculateDateRanges(6, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range6Value1" value="${mileage_range6LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range6Value2" value="${mileage_range6HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #7:<input type="text" name="mileage_range7" value="${mileage_range7}" size="5" onchange="recalculateDateRanges(7, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range7Value1" value="${mileage_range7LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range7Value2" value="${mileage_range7HighAdj}" size="2" /></td>
        </tr>
    </table>

    <h:hidden property="mileage_range1Low" styleId="mileage_range1Low" value="${mileage_range1Low}"/>
    <h:hidden property="mileage_range1High" styleId="mileage_range1High" value="${mileage_range1High}"/>
    <h:hidden property="mileage_range2Low" styleId="mileage_range2Low" value="${mileage_range2Low}"/>
    <h:hidden property="mileage_range2High" styleId="mileage_range2High" value="${mileage_range2High}"/>
    <h:hidden property="mileage_range3Low" styleId="mileage_range3Low" value="${mileage_range3Low}"/>
    <h:hidden property="mileage_range3High" styleId="mileage_range3High" value="${mileage_range3High}"/>
    <h:hidden property="mileage_range4Low" styleId="mileage_range4Low" value="${mileage_range4Low}"/>
    <h:hidden property="mileage_range4High" styleId="mileage_range4High" value="${mileage_range4High}"/>
    <h:hidden property="mileage_range5Low" styleId="mileage_range5Low" value="${mileage_range5Low}"/>
    <h:hidden property="mileage_range5High" styleId="mileage_range5High" value="${mileage_range5High}"/>
    <h:hidden property="mileage_range6Low" styleId="mileage_range6Low" value="${mileage_range6Low}"/>
    <h:hidden property="mileage_range6High" styleId="mileage_range6High" value="${mileage_range6High}"/>
    <h:hidden property="mileage_range7Low" styleId="mileage_range7Low" value="${mileage_range7Low}"/>
    <h:hidden property="mileage_range7High" styleId="mileage_range7High" value="${mileage_range7High}"/>
		
	<h2>Ping II Package Matrix</h2>
	
	<table border="1">
		<tr>
			<th>&nbsp;</th>
			<th>Package</th>
			<th>Page Brand</th>
			<th>Notes Brand</th>
			<th>Has Pricing Information</th>
			<th>Has Mystery Shopping</th>
			<th>Has Notes</th>
			<th>Has Merchandising</th>
			<th>Has One-Click Toolbar</th>
		</tr>
		<tr>
			<td><input type="radio" name="packageId" value="1" ${preference.pingII_PackageId == 1 ? "checked" : "" } /></td>
			<td>1</td>
			<td>PING II Internet Pricing Analyzer</td>
			<td>-</td>
			<td>No</td><td>No</td><td>No</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td><input type="radio" name="packageId" value="2" ${preference.pingII_PackageId == 2 ? "checked" : "" } /></td>
			<td>2</td>
			<td>PING II with Internet Mystery Shopping</td>
			<td>-</td>
			<td>No</td><td>Yes</td><td>No</td><td>No</td><td>No</td>
		</tr>
		<tr>
		
			<td><input type="radio" name="packageId" value="3" ${preference.pingII_PackageId == 3 ? "checked" : "" } /></td>
			<td>3</td>
			<td>360� Pricing with Internet Pricing Analyzer</td>
			<td>Notes</td>
			<td>Yes</td><td>No</td><td>Yes</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td><input type="radio" name="packageId" value="4" ${preference.pingII_PackageId == 4 ? "checked" : "" } /></td>
			<td>4</td>
			<td>360� Pricing with Internet Mystery Shopping</td>
			<td>Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>No</td><td>No</td>
		</tr>
		<tr>
			<td><input type="radio" name="packageId" value="5" ${preference.pingII_PackageId == 5 ? "checked" : "" } /></td>
			<td>5</td>
			<td>360� Internet Advertising Accelerator</td>
			<td>Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>No</td>
		</tr>
		<tr>
			<td><input type="radio" name="packageId" value="6" ${preference.pingII_PackageId == 6 ? "checked" : "" } /></td>
			<td>6</td>
			<td>360� Internet Advertising Accelerator</td>
			<td>Closing Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td>
		</tr>
		<!--
		<tr>
			<td><input type="radio" name="packageId" value="7" ${preference.pingII_PackageId == 7 ? "checked" : "" } /></td>
			<td>7</td>
			<td>Market Pricing Power</td>
			<td>Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>No</td>
		</tr>
		<tr>
			<td><input type="radio" name="packageId" value="8" ${preference.pingII_PackageId == 8 ? "checked" : "" } /></td>
			<td>8</td>
			<td>Market Pricing Power with Internet Advertising Accelerator</td>
			<td>Closing Notes</td>
			<td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td>
		</tr> -->
	</table>
		
	<input type="button" onclick="submitForm(${dealer.id});" value="Save All" />
	</form>
	
</c:if>

<a href="/fl-admin/dealer/dynamic/pingIISettings.do?action=restoreDefault&dealerId=${dealer.id}" >Restore Defaults</a>
</h3>

</div>
