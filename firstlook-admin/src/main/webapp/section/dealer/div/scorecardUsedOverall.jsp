<%@include file="/include/tag-import.jsp" %>

<tag:element title="Overall Used Targets" id="${param.id}" 
             section="dealer" element="scorecardUsedOverall" 
             formBean="${dealer_scorecardUsedOverall}">
             
  <div style="clear: both">
  <table width="100%">
    <tr>
      <td>Retail AGP</td>
      <td>F&amp;I AGP</td>
      <td>Inv. Age</td>
      <td>Days Sply</td>
      <td>Days 2 Sale</td>
      <td>Sell Thru</td>
    </tr>
    <tr>
      <td>
        <c:set var="target1"><b:write name="scorecard" property="target1"/></c:set>
	    <tag:text size="2" property="target1" emptyVal="u" value="${target1}"/>
      </td>
      <td>
        <c:set var="target2"><b:write name="scorecard" property="target2"/></c:set>
	    <tag:text size="2" property="target2" emptyVal="u" value="${target2}"/>
      </td>
      <td>
        <c:set var="target46"><b:write name="scorecard" property="target46"/></c:set>
	    <tag:text size="2" property="target46" emptyVal="u" value="${target46}"/>
      </td>
      <td>
        <c:set var="target5"><b:write name="scorecard" property="target5"/></c:set>
	    <tag:text size="2" property="target5" emptyVal="u" value="${target5}"/>
      </td>
      <td>
        <c:set var="target4"><b:write name="scorecard" property="target4"/></c:set>
	    <tag:text size="2" property="target4" emptyVal="u" value="${target4}"/>
      </td>
      <td>
        <c:set var="target3"><b:write name="scorecard" property="target3"/></c:set>
	    <tag:text size="2" property="target3" emptyVal="u" value="${target3}"/>
      </td>
    </tr>
  </table>
  </div>
</tag:element>
