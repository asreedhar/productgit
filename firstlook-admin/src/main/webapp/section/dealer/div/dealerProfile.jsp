<%@include file="/include/tag-import.jsp" %>

<ref:ref beanName="imt-persist-salesChannelDao" var="salesChannels" />
<tag:element title="Identity/Status" id="${dealer.id}" 
             section="dealer" element="dealerProfile" 
             formBean="${dealer_dealerProfile}">

  <h3>Dealer Identity</h3>

  <tag:text size="35" title="Code:" property="code" value="${dealer.code}" readOnly="true"/><br/>
  <tag:text size="35" title="Name:" property="name" value="${dealer.name}"/><br/>
  <tag:text size="35" title="Short Name:" property="shortName" value="${dealer.shortName}"/><br/>
  <tag:checkbox title="Active Status:" property="active" value="${dealer.active}"/><br/>
  <tag:checkbox title="Is Live:" property="goLive" value="${dealer.goLive}"/><br/> 
  <b>Go Live Date:</b> ${dealer.preference.goLiveDate}<br/> <c:if test="${dealer.goLive}">(to reset uncheck Is Live -> Save -> re-check Is Live) <br/></c:if><br />
  
  <tag:select title="Sales Channel" value="${dealer.salesChannel.name}"
  	options="salesChannels" property="salesChannelId" selectedValue="${dealer.salesChannel.id}"/>
  
  <h3>Contact Information</h3>

  <tag:text title="Office Phone:" property="phone" value="${dealer.phone}"/><br/>
  <tag:text title="Office Fax:" property="fax" value="${dealer.fax}"/><br/>

</tag:element>