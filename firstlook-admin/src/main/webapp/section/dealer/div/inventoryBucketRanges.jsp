<%@include file="/include/tag-import.jsp" %>

 <c:url var="validation" value="/resources/js/bucketRanges.js"/>
 <script src="${validation}" type="text/javascript"></script>

<c:set var="rangesId"><b:write name="ranges" property="id"/></c:set>
<tag:element title="Aging Inventory Bucket Ranges" id="${rangesId}" 
             section="dealer" element="inventoryBucketRanges" 
             formBean="${dealer_inventoryBucketRanges}">
    
    <table width="100%" align="left">
    <tr>
        <td colspan="2"><span id="message"></span></td>
    </tr>
    <tr>
    	<%-- Watch List is Range1 and matches Range2 when selected --%>
	    <c:set var="range1"><b:write name="ranges" property="range1"/></c:set> 
	    <c:set var="range1High"><b:write name="ranges" property="range1High"/></c:set> 
    	<td valign="top" colspan="2"><c:set var="useWatchList"><b:write name="ranges" property="useWatchList"/></c:set><tag:checkbox title="Watch List: " property="useWatchList" value="${useWatchList}"/></td> 
    </tr>
    <tr>
    <td>
    <c:set var="range2"><b:write name="ranges" property="range2"/></c:set> 
    <c:set var="range2High"><b:write name="ranges" property="range2High"/></c:set> 
    <tag:text title="Bucket #1*:" id="range2" property="range2" value="${range2}" size="3" onchange="recalculateDateRanges(2, true);"/>
    </td>
    <td><span id="range2LowDisp">0</span> - <span id="range2HighDisp">${range2High}</span> Days
  
    <c:set var="checked"><b:write name="ranges" property="range2"/></c:set>

    </td></tr>

    <tr><td>    
    <c:set var="range3"><b:write name="ranges" property="range3"/></c:set>
    <c:set var="range3Low"><b:write name="ranges" property="range3Low"/></c:set> 
    <c:set var="range3High"><b:write name="ranges" property="range3High"/></c:set>     
    <tag:text title="Bucket #2*:" id="range3" property="range3" value="${range3}" size="3" onchange="recalculateDateRanges(3, true);"/>
    </td><td>
    <span id="range3LowDisp">${range3Low}</span> - <span id="range3HighDisp">${range3High}</span> Days
    </td></tr>
    
    <tr><td>
    <c:set var="range4"><b:write name="ranges" property="range4"/></c:set>
    <c:set var="range4Low"><b:write name="ranges" property="range4Low"/></c:set> 
    <c:set var="range4High"><b:write name="ranges" property="range4High"/></c:set>    
    <tag:text title="Bucket #3*:" id="range4" property="range4" value="${range4}" size="3" onchange="recalculateDateRanges(4, true);"/>
    </td><td>
    <span id="range4LowDisp">${range4Low}</span> - <span id="range4HighDisp">${range4High}</span> Days
    </td></tr>
    
    <tr><td>
    <c:set var="range5"><b:write name="ranges" property="range5"/></c:set>
    <c:set var="range5Low"><b:write name="ranges" property="range5Low"/></c:set> 
    <c:set var="range5High"><b:write name="ranges" property="range5High"/></c:set> 
    <tag:text title="Bucket #4:" id="range5" property="range5" value="${range5}" size="3" onchange="recalculateDateRanges(5, true);"/>
    </td><td>
    <c:choose>
        <c:when test="${range5Low == 0 && range5High == 0}"><span id="range5LowDisp"></span> - <span id="range5HighDisp"></span> Days</c:when>
        <c:when test="${range5Low != 0 && range5High == 0}"><span id="range5LowDisp">${range5Low}</span> - <span id="range5HighDisp">+</span> Days</c:when>
        <c:otherwise><span id="range5LowDisp">${range5Low}</span> - <span id="range5HighDisp">${range5High}</span> Days</c:otherwise>
    </c:choose>  
    </td></tr>
    
    <tr><td>
    <c:set var="range6"><b:write name="ranges" property="range6"/></c:set>
    <c:set var="range6Low"><b:write name="ranges" property="range6Low"/></c:set> 
    <c:set var="range6High"><b:write name="ranges" property="range6High"/></c:set>
        
    <tag:text title="Bucket #5:" id="range6" property="range6" value="${range6}" size="3" onchange="recalculateDateRanges(6, true);"/>
    </td><td>  
    <c:choose>
        <c:when test="${range6Low == 0 && range6High == 0}"><span id="range6LowDisp"></span> - <span id="range6HighDisp"></span> Days</c:when>
        <c:when test="${range6Low != 0 && range6High == 0}"><span id="range6LowDisp">${range6Low}</span> - <span id="range6HighDisp">+</span> Days</c:when>
        <c:otherwise><span id="range6LowDisp">${range6Low}</span> - <span id="range6HighDisp">${range6High}</span> Days</c:otherwise>
    </c:choose>  
    </td></tr>

    
    
    </table> 
    
    <h:hidden property="range1" styleId="range1" value="${range2}"/>
    <h:hidden property="range1Low" styleId="range1Low" value="${range2Low}"/>
    <h:hidden property="range1High" styleId="range1High" value="${range2High}"/>
    <h:hidden property="range2Low" styleId="range2Low" value="${range2Low}"/>
    <h:hidden property="range2High" styleId="range2High" value="${range2High}"/>
    <h:hidden property="range3Low" styleId="range3Low" value="${range3Low}"/>
    <h:hidden property="range3High" styleId="range3High" value="${range3High}"/>
    <h:hidden property="range4Low" styleId="range4Low" value="${range4Low}"/>
    <h:hidden property="range4High" styleId="range4High" value="${range4High}"/>
    <h:hidden property="range5Low" styleId="range5Low" value="${range5Low}"/>
    <h:hidden property="range5High" styleId="range5High" value="${range5High}"/>
    <h:hidden property="range6Low" styleId="range6Low" value="${range6Low}"/>
    <h:hidden property="range6High" styleId="range6High" value="${range6High}"/>
</tag:element> 