<%@include file="/include/tag-import.jsp" %>

<h2>Access Groups</h2>

<ul style="clear:both">
  <c:forEach items="${activeAccessGroups}" var="accessGroup">
    <li>
      ${accessGroup.name} 
      <c:url var="removeAccessGroup" value="/dealer/custom/removeAccessGroup.do">
        <c:param name="id" value="${dealerId}"/>
        <c:param name="accessGroupId" value="${accessGroup.id}"/>
      </c:url>
      <proto:linkRemote href="${removeAccessGroup}" update="accessGroup"
         confirm="Are you sure you want to remove ${franchise.description} for this dealer?">
        remove
      </proto:linkRemote>
    </li>
  </c:forEach>
</ul>

<c:url var="addAccessGroup" value="/dealer/custom/addAccessGroup.do"/>
<proto:formRemote href="${addAccessGroup}" update="accessGroup" id="accessGroupForm">
  <input type="hidden" name="id" value="${dealerId}"/>
  <ref:ref beanName="imt-persist-accessGroupDao" method="findAll" scope="request"/>
  <select name="accessGroupId">
    <c:forEach items="${values}" var="current">
      <option value="${current.id}">${current.name}</option>
    </c:forEach>
  </select>
  <input type="submit" value="Add Access Group..."/>
</proto:formRemote>
