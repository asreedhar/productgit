<%@include file="/include/tag-import.jsp"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script>
function crmTypeChange(select) {
	if (select.options[select.selectedIndex].text.indexOf("reynolds") > -1) {
		document.getElementById("reynoldsCRMValues").style.display = 'block';
	} else {
		document.getElementById("reynoldsCRMValues").style.display = 'none';
	}
}
</script>

<c:set var="isEdit">
	<b:write name="credentials" property="isEdit" />
</c:set>
<c:set var="wirelessSource">
	<b:write name="credentials" property="wirelessSource" />
</c:set>
<c:set var="crmSource">
	<b:write name="credentials" property="crmSource" />
</c:set>
<c:set var="wirelessSource0">
	<b:write name="credentials" property="wirelessSource0" />
</c:set>
<c:set var="wirelessSource1">
	<b:write name="credentials" property="wirelessSource1" />
</c:set>
<c:set var="crmSource0">
	<b:write name="credentials" property="crmSource0" />
</c:set>
<c:set var="crmSource1">
<b:write name="credentials" property="crmSource1" />
</c:set>
<c:set var="crmSource2">
	<b:write name="credentials" property="crmSource2" />
</c:set>
<c:set var="crmSource3">
	<b:write name="credentials" property="crmSource3" />
</c:set>
<c:set var="crmSource4">
	<b:write name="credentials" property="crmSource4" />
</c:set>
<c:set var="crmSource5">
	<b:write name="credentials" property="crmSource5" />
</c:set>
<c:set var="crmSource6">
	<b:write name="credentials" property="crmSource6" />
</c:set>

<c:set var="reynoldsDealerNumber">
	<b:write name="credentials" property="reynoldsDealerNumber" />
</c:set>
<c:set var="reynoldsStoreNumber">
	<b:write name="credentials" property="reynoldsStoreNumber" />
</c:set>
<c:set var="reynoldsAreaNumber">
	<b:write name="credentials" property="reynoldsAreaNumber" />
</c:set>



<c:set var="credentialsId">
	<b:write name="credentials" property="id" />
</c:set>

<tag:element title="Credentials" id="${credentialsId}" section="dealer"
	element="thirdPartyAppraisal"
	formBean="${dealer_thirdPartyAppraisal}">

	<table width="100%" align="left">
		<tr>
			<td><hr></td>
		</tr>
		<tr>
			<th>WIRELESS</th>
		</tr>
		<tr>
			<td>
			<div><label for="wirelessSource" class="fieldLabel">Wireless
			Source:</label> <c:choose>
				<c:when test="${isEdit}">
					<select name="wirelessSource">
				</c:when>
				<c:otherwise>
					<select disabled="true" name="wirelessSource">
				</c:otherwise>
			</c:choose>
			<option value="${wirelessSource0}">${wirelessSource0}</option>
			<c:choose>
				<c:when test="${wirelessSource1 eq wirelessSource}">
					<option selected="true" value="${wirelessSource1}">${wirelessSource1}</option>
				</c:when>
				<c:otherwise>
					<option value="${wirelessSource1}">${wirelessSource1}</option>
				</c:otherwise>
			</c:choose> </select></div>
			</td>
		</tr>
		<tr>
			<c:set var="wirelessPass">
				<b:write name="credentials" property="wirelessPass" />
			</c:set>
			<td><tag:password title="Wireless Pass:" withLabel="true"
				property="wirelessPass" value="${wirelessPass}" size="15" /></td>
		</tr>
		<tr>
			<td>
			<hr>
			</td>
		</tr>
		<tr>
			<th>CRM</th>
		</tr>
		<tr>
			<td><div >
				<ul>
				<logic:iterate name="credentials" id="source" property="assignedCrmSource">
					<li>
						<c:set var="source"><b:write name="source"/></c:set>
						<span class="fieldLabel sourceLabel">${source}:</span>
					 	<c:url var="removeCrmSource" value="/dealer/custom/removeCrmSource.do">
				        	<c:param name="id" value="${credentialsId}"/>
				        	<c:param name="crmSource" value="${source}"/>
				      	</c:url>
				      	<proto:linkRemote href="${removeCrmSource}" update="thirdPartyAppraisal">
				        	remove
				     	 </proto:linkRemote>
				    </li>
				</logic:iterate>
				</ul>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			<div><label for="crmSource" class="fieldLabel">CRM
			Source:</label> <c:choose>
				<c:when test="${isEdit}">
					<select name="crmSource" onchange="crmTypeChange(this);return false;">
				</c:when>
				<c:otherwise>
					<select disabled="true" name="crmSource">
				</c:otherwise>
			</c:choose>
			<option value="${crmSource0}">${crmSource0}</option>
			<c:choose>
			<c:when test="${crmSource1 eq crmSource}">
					<option selected="true" value="${crmSource1}">${crmSource1}</option>
					<option value="${crmSource2}">${crmSource2}</option>
					<option value="${crmSource3}">${crmSource3}</option>
					<option value="${crmSource4}">${crmSource4}</option>
					<option value="${crmSource5}">${crmSource5}</option>
					<option value="${crmSource6}">${crmSource6}</option>
			</c:when >
			<c:when test="${crmSource2 eq crmSource}">
					<option selected="true" value="${crmSource2}">${crmSource2}</option>
					<option value="${crmSource1}">${crmSource1}</option>
					<option value="${crmSource3}">${crmSource3}</option>
					<option value="${crmSource4}">${crmSource4}</option>
					<option value="${crmSource5}">${crmSource5}</option>
					<option value="${crmSource6}">${crmSource6}</option>
			</c:when>
			<c:when test="${crmSource3 eq crmSource}">
					<option selected="true" value="${crmSource3}">${crmSource3}</option>
					<option value="${crmSource1}">${crmSource1}</option>
					<option value="${crmSource2}">${crmSource2}</option>
					<option value="${crmSource4}">${crmSource4}</option>
					<option value="${crmSource5}">${crmSource5}</option>
					<option value="${crmSource6}">${crmSource6}</option>
			</c:when>
			<c:when test="${crmSource4 eq crmSource}">
					<option selected="true" value="${crmSource4}">${crmSource4}</option>
					<option value="${crmSource1}">${crmSource1}</option>
					<option value="${crmSource2}">${crmSource2}</option>
					<option value="${crmSource3}">${crmSource3}</option>
					<option value="${crmSource5}">${crmSource5}</option>
					<option value="${crmSource6}">${crmSource6}</option>
			</c:when>
			<c:when test="${crmSource5 eq crmSource}">
					<option selected="true" value="${crmSource5}">${crmSource5}</option>
					<option value="${crmSource1}">${crmSource1}</option>
					<option value="${crmSource2}">${crmSource2}</option>
					<option value="${crmSource3}">${crmSource3}</option>
					<option value="${crmSource4}">${crmSource4}</option>
					<option value="${crmSource6}">${crmSource6}</option>
			</c:when>
			<c:when test="${crmSource6 eq crmSource}">
					<option selected="true" value="${crmSource6}">${crmSource6}</option>
					<option value="${crmSource1}">${crmSource1}</option>
					<option value="${crmSource2}">${crmSource2}</option>
					<option value="${crmSource3}">${crmSource3}</option>
					<option value="${crmSource4}">${crmSource4}</option>
					<option value="${crmSource5}">${crmSource5}</option>
			</c:when>
			<c:otherwise>
				<option value="${crmSource1}">${crmSource1}</option>
				<option value="${crmSource2}">${crmSource2}</option>
				<option value="${crmSource3}">${crmSource3}</option>
				<option value="${crmSource4}">${crmSource4}</option>
				<option value="${crmSource5}">${crmSource5}</option>
				<option value="${crmSource6}">${crmSource6}</option>
			</c:otherwise>
			</c:choose>
			</select></div>
			</td>
		</tr>
		<tr>
			<c:set var="crmPass">
				<b:write name="credentials" property="crmPass" />
			</c:set>
			<td><tag:password title="CRM Pass:" withLabel="true"
				property="crmPass" value="${crmPass}" size="15" /></td>
			<td>
				<div id="reynoldsCRMValues" <c:if test="${crmSource != crmSource6}"> style="display:none;" </c:if> >
	  				<br/><tag:text title="DealerNumber:" value="${reynoldsDealerNumber }" property="reynoldsDealerNumber" size="20"/>
					<br/><tag:text title="StoreNumber:" value="${reynoldsStoreNumber}" property="reynoldsStoreNumber" size="5"/>
					<br/><tag:text title="AreaNumber" value="${reynoldsAreaNumber}" property="reynoldsAreaNumber" size="5"/>
				</div>	
			</td>
		</tr>
		
	</table>

</tag:element>
