<%@include file="/include/tag-import.jsp" %>

<table id="dealer_table" class="datagrid">
  <thead>
    <tr>
      <th mochi:format="str">Name</th>
      <th mochi:format="str">Nick Name</th>
      <th mochi:format="str">Account #</th>
      <th mochi:format="str">Group</th>
      <th mochi:format="city">City</th>
      <th mochi:format="str">Dealer <br/>Home Page</th>
    </tr>
  </thead>
  <tfoot class="invisible">
    <tr>
      <th mochi:format="str">Name</th>
      <th mochi:format="str">Nick Name</th>
      <th mochi:format="str">Account #</th>
      <th mochi:format="str">Group</th>
      <th mochi:format="city">City</th>
      <th mochi:format="str">Dealer <br/>Home Page</th>
    </tr>
  </tfoot>
  <tbody>
    <c:forEach items="${results}" var="dealer">
      <tr>
        <td>
          <c:url var="profileUrl" value="/dealer/dynamic/profile.do">
		    <c:param name="id" value="${dealer.id}"/>
          </c:url>
          <a href="${profileUrl}">
          <str:truncateNicely upper="40">${dealer.name}</str:truncateNicely>
          </a>
        </td>
        <td>${dealer.shortName}</td>
        <td>${dealer.code}</td>
        <td>
          <c:url var="groupUrl" value="/group/dynamic/profile.do">
		    <c:param name="id" value="${dealer.dealerGroup.id}"/>
          </c:url>
            <a href="${groupUrl}">
              <str:truncateNicely upper="15">${dealer.dealerGroup.name}</str:truncateNicely>
            </a>
        </td>
        <td>${dealer.city}, ${dealer.state}</td>
        <td>
        <c:choose>
	        <c:when test="${dealer.active}">
				<c:url var="homeUrl" value="/dealer/dynamic/profile.do">
					<c:param name="id" value="${dealer.id}"/>
					<c:param name="home" value="home" />
				</c:url>
				(<a href="${homeUrl}">home</a>)
		     </c:when>
		     <c:otherwise>
		     	(inactive)
		     </c:otherwise>
	     </c:choose>
        
	     </td>
      </tr>
    </c:forEach>
    <c:if test="${empty results}">
      <tr>
        <td colspan="5">No Matching Results</td>
      </tr>
    </c:if>
  </tbody>
</table>  

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('dealer_table'));
</script>
      
