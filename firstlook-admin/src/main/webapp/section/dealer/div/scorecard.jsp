<%@include file="/include/tag-import.jsp" %>

<tag:element title="Scorecard" id="${dealer.id}" 
             section="dealer" element="scorecard" 
             formBean="${dealer_scorecard}">

  <h3>Units Sold Thresholds</h3>
  <div style="clear: both">
  <table width="100%">
    <tr>
      <td># Weeks:</td>
      <td>4</td>
      <td>8</td>
      <td>12</td>
      <td>13</td>
      <td>26</td>
      <td>52</td>
    </tr>
    <tr>
      <td>Threshold:</td>
      <td><tag:text size="2" property="unitsSoldThreshold4Wks" value="${dealer.preference.unitsSoldThreshold4Wks}"/></td>
      <td><tag:text size="2" property="unitsSoldThreshold8Wks" value="${dealer.preference.unitsSoldThreshold8Wks}"/></td>
      <td><tag:text size="2" property="unitsSoldThreshold12Wks" value="${dealer.preference.unitsSoldThreshold12Wks}"/></td>
      <td><tag:text size="2" property="unitsSoldThreshold13Wks" value="${dealer.preference.unitsSoldThreshold13Wks}"/></td>
      <td><tag:text size="2" property="unitsSoldThreshold26Wks" value="${dealer.preference.unitsSoldThreshold26Wks}"/></td>
      <td><tag:text size="2" property="unitsSoldThreshold52Wks" value="${dealer.preference.unitsSoldThreshold52Wks}"/></td>
    </tr>
  </table>
  </div>

</tag:element>