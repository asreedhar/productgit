<%@include file="/include/tag-import.jsp" %>



<tag:element title="KBBConsumerTool" id="${dealer.id}"
     section="dealer" element="KBBConsumerTool" 
     formBean="${dealer_KBBConsumerTool}">

	<tag:checkbox title="Display TradeIn Values on the KBB Consumer Tool" property="showTradeIn" value="${dealer.KBBConsumerToolPreferences.showTradeIn}"/><br/>
	<tag:checkbox title="Display Retail Values on the KBB Consumer Tool" property="showRetail" value="${dealer.KBBConsumerToolPreferences.showRetail}"/><br/>

</tag:element>
