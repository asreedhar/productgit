<%@include file="/include/tag-import.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<h2>${configuration.destinationName}</h2>

<c:set var="configDestNoDash" value="${fn:replace(configuration.destinationName, '-', '_')}" scope="request"/>

<proto:formRemote href="save.do" id="extract_${configDestNoDash}_form"
					update="extract_${configuration.destinationName}"
					name="dealer_extract_form">
	<c:set var="org.apache.struts.taglib.html.BEAN" value="${dealer_extract_form}" scope="request" />
	<c:choose>
		<c:when test="${isEdit}">
			<span class="formButtons"><img id="indicator" src="../../resources/images/rotating-line-load.gif" style="display: none" alt="loading" />
			<h:submit value="Save"
				onclick="${onSubmit};document.getElementById('indicator').style.display = 'inline';" /> 
			<c:url var="cancelURL" value="/dealer/extracts/show.do?dealerId=${configuration.dealerId}&destinationName=${configuration.destinationName}" />
			<proto:linkRemote href="${cancelURL}" update="extract_${configuration.destinationName}">Cancel</proto:linkRemote></span>
		</c:when>
		<c:otherwise>
			<%-- Ideally, when the Edmunds TMV upgrade turns on/off, this piece of data should turn on and off as well. --%>
			<c:choose><c:when test="${configuration.editable}">
				<c:url var="editURL"
					value="/dealer/extracts/edit.do?dealerId=${configuration.dealerId}&destinationName=${configuration.destinationName}&isEdit=true" />
				<span class="editLink"><proto:linkRemote href="${editURL}" update="extract_${configuration.destinationName}">Edit</proto:linkRemote></span>
			</c:when> 
			<c:otherwise>Not editable: ${configuration.editRuleMessage}</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>

	<h:hidden property="dealerId" value="${configuration.dealerId}"/>
	<h:hidden property="destinationName" value="${configuration.destinationName}"/>
	<tag:checkbox title="Active?" property="active" value="${configuration.active}"/>
	<tag:date id="${configuration.destinationName}_startDate" title="Start Date" property="startDate" value="${configuration.startDate}" />
	<tag:date id="${configuration.destinationName}_endDate" title="End Date" property="endDate" value="${configuration.endDate}" />
	<tag:text title="External ID" property="externalIdentifier" value="${configuration.externalIdentifier}" maxlength="30"/>
</proto:formRemote>