<%@include file="/include/tag-import.jsp" %>

<tag:element title="Address" id="${dealer.id}"
             section="dealer" element="address"
             formBean="${dealer_address}">

<div style="clear: both; padding-left: 15px;">
  <tag:text size="50" maxlength="35" property="street1" value="${dealer.street1}"/><br/>
  <tag:text size="50" maxlength="35" property="street2" omitEmpty="${true}" value="${dealer.street2}"/><br/>
  <tag:text size="25" maxlength="20" property="city" value="${dealer.city}"/>, 
  <tag:text size="4" maxlength="2" property="state" value="${func:toUpperCase(dealer.state)}"/>
  <tag:text size="10" maxlength="5" property="zip" value="${dealer.zip}"/>
</div>

</tag:element>

