<%@include file="/include/tag-import.jsp" %>

<tag:element title="CIA Preferences" id="${dealer.id}" 
             section="dealer" element="ciaPreferences" 
             formBean="${dealer_ciaPreferences}">

  <tag:text title="Target Days Supply:" property="targetDaysSupply" size="4"
  			value="${dealer.ciaPreferences.targetDaysSupply}"/><br/>
  

  <h3>Unit Cost Buckets</h3>
  <tag:text title="Creation Threshold:" property="powerZoneGroupingThreshold" size="4"
  			value="${dealer.ciaPreferences.powerZoneGroupingThreshold}"/><br/>
  <tag:text title="Allocation Threshold:" property="bucketAllocationMinimumThreshold" size="4"
  			value="${dealer.ciaPreferences.bucketAllocationMinimumThreshold}"/><br/>

  <h3>Market Performers</h3>
  <tag:text title="Display Threshold:" property="marketPerformersDisplayThreshold" size="4"
  			value="${dealer.ciaPreferences.marketPerformersDisplayThreshold}"/><br/>
  			
  <tag:text title="In Stock Threshold:" property="marketPerformersInStockThreshold" size="4"
  			value="${dealer.ciaPreferences.marketPerformersInStockThreshold}"/><br/>

  <tag:text title="Zip Code Threshold:" property="marketPerformersZipCodeThreshold" size="4"
  			value="${dealer.ciaPreferences.marketPerformersZipCodeThreshold}"/><br/>

  <tag:text title="Units Threshold:" property="marketPerformersUnitsThreshold" size="4"
  			value="${dealer.ciaPreferences.marketPerformersUnitsThreshold}"/><br/>

  <h3>Basis Periods</h3> 
  <tag:select title="Lights Determination:" property="gdLightProcessorTimePeriodId" 
              value="${dealer.ciaPreferences.gdLightProcessorTimePeriodId}"
              showOptions="${true}">
     <c:set var="selected">${dealer.ciaPreferences.gdLightProcessorTimePeriodId}</c:set>
     <tag:option value="1" selected="${selected}">13/13</tag:option>
     <tag:option value="2" selected="${selected}">26/26</tag:option>
     <tag:option value="3" selected="${selected}">26/13</tag:option>
     <tag:option value="4" selected="${selected}">13/26</tag:option>
     <tag:option value="5" selected="${selected}">26/0</tag:option>
     <tag:option value="6" selected="${selected}">26/0</tag:option>
     <tag:option value="7" selected="${selected}">26/0</tag:option>
     <tag:option value="8" selected="${selected}">13x2/0</tag:option>
  </tag:select><br/>
  
  <tag:select title="Sales History:" property="salesHistoryDisplayTimePeriodId" 
              value="${dealer.ciaPreferences.salesHistoryDisplayTimePeriodId}"
              showOptions="${true}">
     <c:set var="selected">${dealer.ciaPreferences.salesHistoryDisplayTimePeriodId}</c:set>
     <tag:option value="1" selected="${selected}">13/13</tag:option>
     <tag:option value="2" selected="${selected}">26/26</tag:option>
     <tag:option value="3" selected="${selected}">26/13</tag:option>
     <tag:option value="4" selected="${selected}">13/26</tag:option>
     <tag:option value="5" selected="${selected}">26/0</tag:option>
     <tag:option value="6" selected="${selected}">26/0</tag:option>
     <tag:option value="7" selected="${selected}">26/0</tag:option>
     <tag:option value="8" selected="${selected}">13x2/0</tag:option>
  </tag:select><br/>
  
  <h3>CIA Basis Periods</h3>
  <tag:select title="Store Target Inventory:" property="ciaStoreTargetInventoryBasisPeriodId" 
              value="${dealer.ciaPreferences.ciaStoreTargetInventoryBasisPeriodId}"
              showOptions="${true}">
     <c:set var="selected">${dealer.ciaPreferences.ciaStoreTargetInventoryBasisPeriodId}</c:set>
     <tag:option value="1" selected="${selected}">13/13</tag:option>
     <tag:option value="2" selected="${selected}">26/26</tag:option>
     <tag:option value="3" selected="${selected}">26/13</tag:option>
     <tag:option value="4" selected="${selected}">13/26</tag:option>
     <tag:option value="5" selected="${selected}">26/0</tag:option>
     <tag:option value="6" selected="${selected}">26/0</tag:option>
     <tag:option value="7" selected="${selected}">26/0</tag:option>
     <tag:option value="8" selected="${selected}">13x2/0</tag:option>
  </tag:select><br/>
  
  <tag:select title="Core Model Determination:" property="ciaCoreModelDeterminationBasisPeriodId" 
              value="${dealer.ciaPreferences.ciaCoreModelDeterminationBasisPeriodId}"
              showOptions="${true}">
     <c:set var="selected">${dealer.ciaPreferences.ciaCoreModelDeterminationBasisPeriodId}</c:set>
     <tag:option value="1" selected="${selected}">13/13</tag:option>
     <tag:option value="2" selected="${selected}">26/26</tag:option>
     <tag:option value="3" selected="${selected}">26/13</tag:option>
     <tag:option value="4" selected="${selected}">13/26</tag:option>
     <tag:option value="5" selected="${selected}">26/0</tag:option>
     <tag:option value="6" selected="${selected}">26/0</tag:option>
     <tag:option value="7" selected="${selected}">26/0</tag:option>
     <tag:option value="8" selected="${selected}">13x2/0</tag:option>
  </tag:select><br/>
  
  <tag:select title="Powerzone Target Inventory:" property="ciaPowerzoneModelTargetInventoryBasisPeriodId" 
              value="${dealer.ciaPreferences.ciaPowerzoneModelTargetInventoryBasisPeriodId}"
              showOptions="${true}">
     <c:set var="selected">${dealer.ciaPreferences.ciaPowerzoneModelTargetInventoryBasisPeriodId}</c:set>
     <tag:option value="1" selected="${selected}">13/13</tag:option>
     <tag:option value="2" selected="${selected}">26/26</tag:option>
     <tag:option value="3" selected="${selected}">26/13</tag:option>
     <tag:option value="4" selected="${selected}">13/26</tag:option>
     <tag:option value="5" selected="${selected}">26/0</tag:option>
     <tag:option value="6" selected="${selected}">26/0</tag:option>
     <tag:option value="7" selected="${selected}">26/0</tag:option>
     <tag:option value="8" selected="${selected}">13x2/0</tag:option>
  </tag:select><br/>
    
  <tag:select title="Core Model Year Allocation:" property="ciaCoreModelYearAllocationBasisPeriodId" 
              value="${dealer.ciaPreferences.ciaCoreModelYearAllocationBasisPeriodId}"
              showOptions="${true}">
     <c:set var="selected">${dealer.ciaPreferences.ciaCoreModelYearAllocationBasisPeriodId}</c:set>
     <tag:option value="1" selected="${selected}">13/13</tag:option>
     <tag:option value="2" selected="${selected}">26/26</tag:option>
     <tag:option value="3" selected="${selected}">26/13</tag:option>
     <tag:option value="4" selected="${selected}">13/26</tag:option>
     <tag:option value="5" selected="${selected}">26/0</tag:option>
     <tag:option value="6" selected="${selected}">26/0</tag:option>
     <tag:option value="7" selected="${selected}">26/0</tag:option>
     <tag:option value="8" selected="${selected}">13x2/0</tag:option>
  </tag:select><br/>
  
</tag:element>
