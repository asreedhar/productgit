<%@include file="/include/tag-import.jsp" %>

<tag:element title="Adesa Purchasing" id="${dealer.id}" 
             section="dealer" element="adesaPurchasing" 
             formBean="${dealer_adesaPurchasing}">

  <tag:checkbox title="Adesa Enabled:" property="auctionSearchEnabled"
            value="${dealer.auctionPreference.auctionSearchEnabled}"/><br/>

  <tag:select title="Distance from Dealer (miles):" property="maxMilesAway" 
              value="${dealer.auctionPreference.maxMilesAway} miles"
              showOptions="${true}">
     <c:set var="selected">${dealer.auctionPreference.maxMilesAway}</c:set>
     <tag:option value="50" selected="${selected}"><= 50</tag:option>
     <tag:option value="100" selected="${selected}"><= 100</tag:option>
     <tag:option value="150" selected="${selected}"><= 150</tag:option>
     <tag:option value="200" selected="${selected}"><= 200</tag:option>
     <tag:option value="250" selected="${selected}"><= 250</tag:option>
     <tag:option value="-1" selected="${selected}">No Preference</tag:option>
  </tag:select>

  <tag:select title="Search Timeframe:" property="maxDaysAhead" 
              value="${dealer.auctionPreference.maxDaysAhead} days"
              showOptions="${true}">
     <c:set var="selected">${dealer.auctionPreference.maxDaysAhead}</c:set>
     <tag:option value="7" selected="${selected}">One Week</tag:option>
     <tag:option value="30" selected="${selected}">Two Weeks</tag:option>
     <tag:option value="60" selected="${selected}">One Month</tag:option>
     <tag:option value="183" selected="${selected}">Six Months</tag:option>
     <tag:option value="365" selected="${selected}">One Year</tag:option>
     <tag:option value="-1" selected="${selected}">Unlimited</tag:option>
  </tag:select>

</tag:element>