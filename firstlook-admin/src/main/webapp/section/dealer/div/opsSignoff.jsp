<%@include file="/include/tag-import.jsp"%>
<script type="text/javascript" language="javascript">

function enableTransmitDiv(bSelected, thirdPartyId) {
	Element.toggle('enableTransmission'+thirdPartyId);
	if($('enableBox'+thirdPartyId).checked == 'true') {
		if (bSelected == 'false') {
			$('enableBox'+thirdPartyId).checked=false;
		} else {
			$('enableBox'+thirdPartyId).checked=true;
		}
	} 
}

function popRulesApp(businessUnitId) {
	var path = 'http://qa.firstlook.biz/DataManagement/ExportRuleAdmin.asp?BusinessUnitID=${businessUnitId}';
	var winName = 'ruleApp';
	var attr = 'width=800,height=600';
	window.open(path, winName, attr);
}
</script>

<c:set var="businessUnitId"  value="${param.businessUnitId}" />
<c:set var="thirdPartyEntityId"  value="${param.thirdPartyEntityId}" />

<c:url var="saveURL" value="/dealer/save/opsSignoff.do">
	<c:param name="businessUnitId" value="${businessUnitId}" />
	<c:param name="thirdPartyEntityId" value="${thirdPartyEntityId}" />
	<c:param name="excludes" value="verifiedBy,verifiedDate,status" />
</c:url>
<proto:formRemote href="${saveURL}" id="opsForm${thirdPartyEntityId}" name="dealer_opsSignoff" update="opsSignoff${thirdPartyEntityId}">
<c:set var="org.apache.struts.taglib.html.BEAN" value="${dealer_opsSignoff}" scope="request" />
	<c:if test="${isEdit}">
		<h:hidden property="businessUnitId" />
		<h:hidden property="thirdPartyEntityId" />
	</c:if>
		<div style="float:left;width:400px;padding-top:5px">
			<h3>IT Ops Signoff</h3>
			<tag:select title="Status:" property="status" value="${advertiser.status}" showOptions="${true}" onchange="enableTransmitDiv(this.value,${thirdPartyEntityId})">
    			<c:set var="selected">${advertiser.status}</c:set>
    			<tag:option value="false" selected="${selected}">Not Complete</tag:option>
    			<tag:option value="true" selected="${selected}">Ready For Sending</tag:option>
			</tag:select>
			<tag:text title="By:" property="verifiedBy" value="${advertiser.verifiedBy}" size="25" maxlength="35"/>
			<c:set var="verifiedDate">
				<f:formatDate value="${advertiser.verifiedDate}" type="date" dateStyle="short" />
			</c:set>
			<tag:date id="verifiedDate${thirdPartyEntityId}" title="Date:" property="verifiedDate" value="${verifiedDate}" />
		</div>
	<c:choose>
		<c:when test="${isEdit}">
			<span class="cancelURL" style="float:right"><a href="#"onclick="popRulesApp('${businessUnitId}');return false;" />Rules App</a>
			<c:url var="cancelURL"
				value="/dealer/display/internetAds.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}" />
			<proto:linkRemote href="${cancelURL}" update="advertiserPrefs${thirdPartyEntityId}">Cancel</proto:linkRemote></span>
			<h:submit value="Save" onclick="${onSubmit}" style="float:right;background-color:#e8ebf0; border:1px solid #ccc; padding:0px; color:#666666; margin-right:10px; cursor:hand;font-family: Verdana;" onmouseout="this.style.color='#666666'" onmouseover="this.style.color='red'" styleId="saveButton"/> 
		</c:when>
		<c:otherwise>
			<span class="editLink"><a href="#"onclick="popRulesApp();return false;" />Rules App</a>
			<c:url var="editURL"
				value="/dealer/edit/opsSignoff.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}" />
			<proto:linkRemote href="${editURL}" update="opsSignoff${thirdPartyEntityId}">Edit</proto:linkRemote></span>
		</c:otherwise>
	</c:choose>
</proto:formRemote>