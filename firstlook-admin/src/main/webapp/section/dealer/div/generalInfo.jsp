<%@include file="/include/tag-import.jsp" %>

<tag:element title="General Info" id="${dealer.id}" 
             section="dealer" element="generalInfo" 
             formBean="${dealer_generalInfo}">

 <tag:select title="Dashboard Display:" property="dashboardColumnDisplayPreference"
              value="${dealer.preference.dashboardColumnDisplayPreference}">
	<c:set var="selected">${dealer.preference.dashboardColumnDisplayPreference}</c:set>
     <tag:option value="AVERAGE_MILEAGE" selected="${selected}">Avg. Mileage</tag:option>
     <tag:option value="UNITS_IN_STOCK" selected="${selected}">Units in Stock</tag:option>
  </tag:select>              
       <br/>


  <tag:select title="Forecaster Weeks:" property="defaultForecastingWeeks" 
              value="${dealer.preference.defaultForecastingWeeks} Weeks"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.defaultForecastingWeeks}</c:set>
     <tag:option value="4" selected="${selected}">4 weeks</tag:option>
     <tag:option value="8" selected="${selected}">8 weeks</tag:option>
     <tag:option value="13" selected="${selected}">13 weeks</tag:option>
  </tag:select><br/>

  <tag:select title="PerfAnalyzer Weeks:" property="defaultTrendingWeeks" 
              value="${dealer.preference.defaultTrendingWeeks} Weeks"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.defaultTrendingWeeks}</c:set>
     <tag:option value="4" selected="${selected}">4 weeks</tag:option>
     <tag:option value="8" selected="${selected}">8 weeks</tag:option>
     <tag:option value="13" selected="${selected}">13 weeks</tag:option>
  </tag:select><br/>

  <tag:select title="PerfAnalyzer View:" property="defaultTrendingView"
              value="${dealer.preference.defaultTrendingView}">
	<c:set var="selected">${dealer.preference.defaultTrendingView}</c:set>
     <tag:option value="TOP_SELLER" selected="${selected}">Top Seller</tag:option>
     <tag:option value="FASTEST_SELLER" selected="${selected}">Fastest Seller</tag:option>
     <tag:option value="MOST_PROFITABLE" selected="${selected}">Most Profitable</tag:option>
  </tag:select>              
  <br/>
  
  <tag:checkbox title="View List Price:" property="listPricePreference"
            value="${dealer.preference.listPricePreference}"/><br/>
            <br/>
  <tag:text title="Search Inactive Inventory Days Back Threshold:" property="searchInactiveInventoryDaysBackThreshold"
            value="${dealer.preference.searchInactiveInventoryDaysBackThreshold}" size="4"/><br/><br/>
            <br/>
  <tag:checkbox title="Lot Location and Status:" property="showLotLocationStatus"
            value="${dealer.preference.showLotLocationStatus}"/><br/>
            <br/>
  <tag:checkbox title="Show Inactive Appraisals in Trade Manager:" property="showInactiveAppraisals"
                value="${dealer.preference.showInactiveAppraisals}"/><br/>            
                <br/>
    <tag:text title="Search Inactive Appraisals Days Back Threshold:" property="searchAppraisalDaysBackThreshold" value="${dealer.preference.searchAppraisalDaysBackThreshold}" size="4"/><br/>                
    <br/>               
    <tag:text title="Closing Rate Appraisal Look Back Period:" property="appraisalLookBackPeriod" value="${dealer.preference.appraisalLookBackPeriod}" size="4"/><br/>                
    <br/>               
   <tag:text title="Closing Rate Appraisal Look Ahead Period:" property="appraisalLookForwardPeriod" value="${dealer.preference.appraisalLookForwardPeriod}" size="4"/><br/>                
   <br/>               
  <tag:text title="Showroom days filter:" property="showroomDaysFilter"
            value="${dealer.preference.showroomDaysFilter}" size="4"/><br/>
            <br/>         
   <tag:select title="Trade Manager Days Filter:" property="tradeManagerDaysFilter" 
     value="${dealer.preference.tradeManagerDaysFilter}"
     showOptions="${true}">
     <c:set var="selected">${dealer.preference.tradeManagerDaysFilter}</c:set>
     <tag:option value="5" selected="${selected}">5</tag:option>
     <tag:option value="10" selected="${selected}">10</tag:option>
     <tag:option value="15" selected="${selected}">15</tag:option>
     <tag:option value="20" selected="${selected}">20</tag:option>
     <tag:option value="25" selected="${selected}">25</tag:option>
     <tag:option value="30" selected="${selected}">30</tag:option>
     <tag:option value="35" selected="${selected}">35</tag:option>
     <tag:option value="40" selected="${selected}">40</tag:option>
     <tag:option value="45" selected="${selected}">45</tag:option>
     <tag:option value="50" selected="${selected}">50</tag:option>
     <tag:option value="55" selected="${selected}">55</tag:option>
     <tag:option value="60" selected="${selected}">60</tag:option>
     <tag:option value="65" selected="${selected}">65</tag:option>
     <tag:option value="70" selected="${selected}">70</tag:option>
     <tag:option value="75" selected="${selected}">75</tag:option>
     <tag:option value="80" selected="${selected}">80</tag:option>
     <tag:option value="85" selected="${selected}">85</tag:option>
     <tag:option value="90" selected="${selected}">90</tag:option>
  </tag:select><br/>
  <br/>          
   <tag:select title="Appraisal Value Requirement <br/>on Trade Analyzer:" property="appraisalRequirementLevel" 
     value="${dealer.preference.appraisalRequirementLevel}"
     showOptions="${true}">
     <c:set var="selected">${dealer.preference.appraisalRequirementLevel}</c:set>
     <tag:option value="0" selected="${selected}">No appraisal value required</tag:option>
     <tag:option value="1" selected="${selected}">Warn if no appraisal value entered</tag:option>
     <tag:option value="2" selected="${selected}">Require appraisal value</tag:option>
  </tag:select><br/><br/>
  <tag:checkbox title="Require Appraiser Name on All Appraisals:" property="requireNameOnAppraisals"
                value="${dealer.preference.requireNameOnAppraisals}"/><br/>            
                <br/>
  <tag:checkbox title="Require Estimated Recon Cost on All Appraisals:" property="requireEstReconCostOnAppraisals"
                value="${dealer.preference.requireEstReconCostOnAppraisals}"/><br/>            
                <br/>
  <tag:checkbox title="Require Recon Notes on All Appraisals:" property="requireReconNotesOnAppraisals"
                value="${dealer.preference.requireReconNotesOnAppraisals}"/><br/>            
                <br/>
          <h3>Flash Locator</h3>
  <tag:text title="Unit Cost Delay:" property="flashLocatorHideUnitCostDays"
            value="${dealer.preference.flashLocatorHideUnitCostDays} Days" size="4"/>
  (0 days = disabled)<br/>
          
  <br/>      
          <h3>NAAA Auction Data</h3>
   <tag:select title="Auction Default Area:" property="auctionAreaId" 
     value="${dealer.preference.auctionAreaId}"
     showOptions="${true}">
     <c:set var="selected">${dealer.preference.auctionAreaId}</c:set>
     <c:forEach items="${naaaRegions}" var="naaaRegion">
     	<tag:option value="${naaaRegion.value}" selected="${selected}">${naaaRegion.label}</tag:option>
     </c:forEach>
  </tag:select><br/>
  <tag:select title="Auction Default Time Period:" property="auctionTimePeriodId" 
     value="${dealer.preference.auctionTimePeriodId}"
     showOptions="${true}">
     <c:set var="selected">${dealer.preference.auctionTimePeriodId}</c:set>
     <c:forEach items="${naaaTimePeriods}" var="naaaTimePeriod">
     	<tag:option value="${naaaTimePeriod.value}" selected="${selected}">${naaaTimePeriod.label}</tag:option>
     </c:forEach>
  </tag:select><br/>
            
         <h3>Reprice Confirmation</h3>
 <tag:checkbox title="Confirm List Price Reprices:" property="repriceConfirmation"
            value="${dealer.preference.repriceConfirmation}"/><br/>
                        
  <tag:text title="List Price Change % Threshold:" property="repricePercentChangeThreshold"
            value="${dealer.preference.repricePercentChangeThreshold}" size="4"/><br/> 
            
         <h3>Group Appraisals</h3>
  <tag:text title="Weeks back to search other store appraisals:" property="groupAppraisalSearchWeeks"
            value="${dealer.preference.groupAppraisalSearchWeeks}" size="4"/><br/>   <br/>
  <tag:checkbox title="Show customer offer to group:" property="showAppraisalFormOfferGroup"
            value="${dealer.preference.showAppraisalFormOfferGroup}"/><br/><br/>
  <tag:checkbox title="Show appraisal value to group:" property="showAppraisalValueGroup"
            value="${dealer.preference.showAppraisalValueGroup}"/><br/>  <br/>           
            
            <h6>Note group appraisal preferences are overridden by they dealer group counterparts.</h6>                      

    <c:if test="${dealer.dealerGroup.preference.lithiaStore}">        
	    <ref:ref beanName="imt-persist-memberDao" method="findLithiaCarCenterMembers" scope="request"/>
		<c:set var="selectedLithiaMemberName" value="unselected"/>

		<c:forEach items="${values}" var="carCenterMember">
			<c:if test="${carCenterMember.id eq dealer.preference.defaultLithiaCarCenterMemberId}">
				<c:set var="selectedLithiaMemberName" value="${carCenterMember.firstName} ${carCenterMember.lastName}"/>
			</c:if>
		</c:forEach>

		<c:set var="selected">${dealer.preference.defaultLithiaCarCenterMemberId}</c:set>

		<tag:select title="Default Litha Car Center Member" property="defaultLithiaCarCenterMemberId" value="${selectedLithiaMemberName}">
		<c:forEach items="${values}" var="carCenterMember">
			<!-- only include car center members who have access to this dealership in list -->
			<c:forEach items="${carCenterMember.dealers}" var="membersDealer">
				<c:if test="${membersDealer.id eq dealer.id}">
					<tag:option selected="${selected}" value="${carCenterMember.id}" >${carCenterMember.firstName} ${carCenterMember.lastName}</tag:option>
				</c:if>
			</c:forEach>
		</c:forEach>
		</tag:select><br/>
	</c:if>
	<c:if test="${!dealer.dealerGroup.preference.lithiaStore}">
		<h6>Note: Litha Car Center is only available if the dealer is a member of a Lithia dealer Group.</h6>
	</c:if>
	<hr>
	<c:choose>
		<c:when test="${not empty dealer.preference.twixURL}">
			<tag:checkbox title="Enable TWIX:" property="twixEnabled" value="true"/>
		</c:when>
		<c:otherwise>
			<tag:checkbox title="Enable TWIX:" property="twixEnabled" value="false"/>
		</c:otherwise>
	</c:choose>
		<tag:text title="TWIX URL:" property="twixURL" value="${dealer.preference.twixURL}" size="50"/>
		<h6>(use http:// or https://)</h6>
		<br/><br/>   
    <hr>
    <tag:select title="Window Sticker Template:" property="windowStickerTemplateId" 
		value="${windowStickerPref.windowStickerTemplateId}"
		showOptions="${true}">
		<c:set var="selected">${windowStickerPref.windowStickerTemplateId}</c:set> 
		<c:forEach items="${windowStickerTemplates}" var="windowStickerTemplate">
			<tag:option value="${windowStickerTemplate.value}" selected="${selected}">${windowStickerTemplate.label}</tag:option>
		</c:forEach>
	</tag:select><br/>
	
    <tag:checkbox title="Use Window Sticker Price:" property="useLotPrice"
            value="${dealer.preference.useLotPrice}"/><br/>
    
    <tag:select title="Buyers Guide:" property="buyersGuideTemplateId" 
		value="${windowStickerPref.buyersGuideTemplateId}"
		showOptions="${true}">
		<c:set var="selected">${windowStickerPref.buyersGuideTemplateId}</c:set> 
		<c:forEach items="${buyerGuideTemplates}" var="buyerGuideTemplate">
			<tag:option value="${buyerGuideTemplate.value}" selected="${selected}">${buyerGuideTemplate.label}</tag:option>
		</c:forEach>
	</tag:select><br/>             
            <hr>
    
    <tag:checkbox title="Exclude Wholesale From Days Supply:" property="excludeWholesaleFromDaysSupply"
    	value="${dealer.preference.excludeWholesaleFromDaysSupply}"/><br/>
    		
</tag:element>