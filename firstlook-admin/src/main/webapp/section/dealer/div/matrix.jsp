<%@include file="/include/tag-import.jsp" %>

<h2>Dealer Members</h2>

<div class="content">

  <style type="text/css">
.grayBg1
{
    BACKGROUND-COLOR: #999999
}
.whtBgBlackBorder
{
    BACKGROUND-COLOR: #FFFFFF;
    border:1px solid black;
}
.grayBg2
{
    BACKGROUND-COLOR: #cccccc
}
.tableTitleLeft
{
    PADDING-RIGHT: 2px;
    PADDING-LEFT: 2px;
    PADDING-BOTTOM: 2px;
    PADDING-TOP: 2px;
    FONT-FAMILY: Verdana, Arial, Helvetica, Sans-serif;
    FONT-WEIGHT: bold;
    FONT-SIZE: 9px;
    COLOR: #000000;
    TEXT-ALIGN: left;
    VERTICAL-ALIGN: bottom
}
.blkBg
{
    BACKGROUND-COLOR: #000000
}

.grayBg4
{
    BACKGROUND-COLOR: #666666
}
.dataLeft
{
    PADDING-RIGHT: 2px;
    PADDING-LEFT: 2px;
    FONT-WEIGHT: normal;
    FONT-SIZE: 11px;
    PADDING-BOTTOM: 2px;
    COLOR: #000000;
    PADDING-TOP: 2px;
    FONT-FAMILY: Arial, Helvetica, Sans-serif;
    TEXT-ALIGN: left;
    VERTICAL-ALIGN: baseline
}

.cell
{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	text-align: center;
}

.cell-right
{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-right: 1px solid #000000;
	text-align: center;
}

.cell-bottom
{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
	text-align: center;
}

.cell-both
{
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-right: 1px solid #000000;
	border-bottom: 1px solid #000000;
	text-align: center;
}
</style>
<script language="javascript">
function initLights()
{
	var selBoxes = new Array("sel1_1", "sel1_2", "sel1_3", "sel1_4", "sel1_5")
	selBoxes = selBoxes.concat(new Array("sel2_1", "sel2_2", "sel2_3", "sel2_4", "sel2_5"))
	selBoxes = selBoxes.concat(new Array("sel3_1", "sel3_2", "sel3_3", "sel3_4", "sel3_5"))
	selBoxes = selBoxes.concat(new Array("sel4_1", "sel4_2", "sel4_3", "sel4_4", "sel4_5"))
	selBoxes = selBoxes.concat(new Array("sel5_1", "sel5_2", "sel5_3", "sel5_4", "sel5_5"))
	for(var intCtr=0; intCtr < selBoxes.length; intCtr++)
	{
		doChange(document.getElementById(selBoxes[intCtr]))
	}
}

function doChange(pSelObj)
{
	var bgColor = "#990000";
	switch (pSelObj.options[pSelObj.selectedIndex].value)
	{
		case "1": bgColor="#990000";
			break;
		case "2": bgColor="#EED800";
			break;
		case "3": bgColor="#EED800";
			break;
		case "4": bgColor="#008020";
			break;
		case "5": bgColor="#008020";
			break;
		case "6": bgColor="#008020";
			break;
		case "7": bgColor="#008020";
			break;
		default:  bgColor="#990000";
	}

	pSelObj.parentElement.style.backgroundColor=bgColor;

}
</script>

<c:set var="isEdit" value="${true}" scope="request"/>


<tag:element title="Light Matrix" id="${dealer.id}" 
             section="dealer" element="matrix" 
             formBean="${lightGridForm}">

<table border="0" cellspacing="0" cellpadding="2" class="whtBg">
	<tr><td><img src="images/common/shim.gif" width="200" height="1"></td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="15" border="0"></td></tr>
	<tr>
		<td class="titleBold">Dealership Lights Configuration</td>
	</tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="13" border="0"></td></tr>
 <tr class="grayBg2">
	 <td>Light Matrix</td>
 </tr>
</table>

<c:set var="lightDescriptors" value="${lightGridForm.lightDescriptors}"/>

<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
		<td><img src="/images/common/shim.gif" width="50" height="1" border="0"><br></td>
	</tr>
	<tr valign="middle">
		<td colspan="7" style="padding:5px" align="center">Average Valuation per Model (in $)</td>
	</tr>
	<tr valign="middle">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="center">-infinity to $${lightGridForm.firstValuationThreshold}</td>
		<td align="center">$<input type="text" name="firstValuationThreshold" size="5" value="${lightGridForm.firstValuationThreshold}"/> to $${lightGridForm.secondValuationThreshold}</td>
		<td align="center">$<input type="text" name="secondValuationThreshold" size="5" value="${lightGridForm.secondValuationThreshold}"/> to $${lightGridForm.thirdValuationThreshold}</td>
		<td align="center">$<input type="text" name="thirdValuationThreshold" size="5" value="${lightGridForm.thirdValuationThreshold}"/> to $${lightGridForm.fourthValuationThreshold}</td>
		<td align="center">$<input type="text" name="fourthValuationThreshold" size="5" value="${lightGridForm.fourthValuationThreshold}"/> to +infinity</td>
	</tr>
	<tr valign="middle">
		<td rowspan="99" align="center" style="line-height: 90%">
			U<br>n<br>i<br>t<br>s<br><br>S<br>o<br>l<br>d<br>
		</td>
		<td align="center" style="padding:3px" nowrap>0 to <input type="text" name="firstDealThreshold" size="5" value="${lightGridForm.firstDealThreshold}"/></td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue1" onchange="doChange(this)" styleId="sel1_1">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue2" onchange="doChange(this)" styleId="sel1_2">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue3" onchange="doChange(this)" styleId="sel1_3">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue4" onchange="doChange(this)" styleId="sel1_4">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue5" onchange="doChange(this)" styleId="sel1_5">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
	</tr>
	<tr valign="middle">
		<td align="center" style="padding:3px" nowrap>${lightGridForm.firstDealThreshold +1} to <input type="text" name="secondDealThreshold" size="5" value="${lightGridForm.secondDealThreshold}"/></td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue6" onchange="doChange(this)" styleId="sel2_1">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue7" onchange="doChange(this)" styleId="sel2_2">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue8" onchange="doChange(this)" styleId="sel2_3">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue9" onchange="doChange(this)" styleId="sel2_4">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue10" onchange="doChange(this)" styleId="sel2_5">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
	</tr>
	<tr valign="middle">
		<td align="center" style="padding:3px" nowrap>${lightGridForm.secondDealThreshold+1} to <input type="text" name="thirdDealThreshold" size="5" value="${lightGridForm.thirdDealThreshold}"/></td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue11" onchange="doChange(this)" styleId="sel3_1">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue12" onchange="doChange(this)" styleId="sel3_2">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue13" onchange="doChange(this)" styleId="sel3_3">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue14" onchange="doChange(this)" styleId="sel3_4">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue15" onchange="doChange(this)" styleId="sel3_5">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
	</tr>
	<tr valign="middle">
		<td align="center" style="padding:3px" nowrap>${lightGridForm.thirdDealThreshold +1} to <input type="text" name="fourthDealThreshold" size="5" value="${lightGridForm.fourthDealThreshold}"/></td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue16" onchange="doChange(this)" styleId="sel4_1">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue17" onchange="doChange(this)" styleId="sel4_2">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue18" onchange="doChange(this)" styleId="sel4_3">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue19" onchange="doChange(this)" styleId="sel4_4">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue20" onchange="doChange(this)" styleId="sel4_5">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
	</tr>
	<tr valign="middle">
		<td align="center" style="padding:8px" nowrap>${lightGridForm.fourthDealThreshold +1} to +infinity</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue21" onchange="doChange(this)" styleId="sel5_1">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue22" onchange="doChange(this)" styleId="sel5_2">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell">
			<h:select name="lightGridForm" property="gridValue23" onchange="doChange(this)" styleId="sel5_3">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue24" onchange="doChange(this)" styleId="sel5_4">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
		<td class="cell-right">
			<h:select name="lightGridForm" property="gridValue25" onchange="doChange(this)" styleId="sel5_5">
				<h:options collection="lightDescriptors" property="id" labelProperty="description" />
			</h:select>
		</td>
	</tr>
	<tr valign="middle">
		<td colspan="7"><img src="/images/common/shim.gif" width="1" height="20" border="0"><br></td>
	</tr>
</table>


</tag:element>
