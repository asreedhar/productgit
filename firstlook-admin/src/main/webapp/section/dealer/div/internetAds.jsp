<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function disableForm(exception, form, disable){
	if(disable){
		Form.disable(form);
	}else{
		Form.enable(form);
	}
	$(exception).disabled = false;
	$('saveButton').disabled = false;
	$('saveButton').style.borderColor = '#ccc';
}
</script>
<c:set var="businessUnitId"  value="${param.businessUnitId}" />
<c:set var="thirdPartyEntityId"  value="${param.thirdPartyEntityId}" />

<ref:single beanName="imt-persist-thirdPartyEntityDao" var="thirdPartyEntity" idVal="${thirdPartyEntityId}" />
<c:url var="saveURL" value="/dealer/save/internetAds.do" scope="request">
	<c:param name="businessUnitId" value="${businessUnitId}" />
	<c:param name="thirdPartyEntityId" value="${thirdPartyEntityId}" />
	<c:param name="excludes" value="active,dealerCode,notes,contactFirstName,contactLastName,contactEmail,contactPhoneNumber" />
</c:url>
<div id="advertiserPrefs${thirdPartyEntityId}">
<h3 style="float:left">${thirdPartyEntity.name}</h3>
<proto:formRemote href="${saveURL}" id="advertiserForm${thirdPartyEntityId}" name="dealer_internetAds" update="advertiserPrefs${thirdPartyEntityId}">
<c:set var="org.apache.struts.taglib.html.BEAN" value="${dealer_internetAds}" scope="request" />
	<c:if test="${isEdit}">
		<h:hidden property="businessUnitId" />
		<h:hidden property="thirdPartyEntityId" />
	</c:if>
	<tag:checkbox title="Active?" property="active" value="${advertiser.active}" onclick="disableForm(this, 'advertiserForm${thirdPartyEntityId }', !this.checked);"/>
	<div style="padding-bottom:5px;">
		<div style="float:left;width:400px;">
			<tag:text title="Dealer Code:" property="dealerCode" value="${advertiser.dealerCode}" disabled="${advertiser.active ? false:true}" size="20" maxlength="50"/>
			<tag:textarea cols="39" rows="8" property="notes" title="Notes:" disabled="${advertiser.active ? false:true}" > ${advertiser.notes}</tag:textarea>
		</div>
		<div style="float:right;width:350px;">
			<h3>Store contact</h3>
			<tag:text title="First Name:" property="contactFirstName" value="${advertiser.contactFirstName}" disabled="${advertiser.active ? false:true}" size="20" maxlength="25"/>
			<tag:text title="Last Name:" property="contactLastName" value="${advertiser.contactLastName}" disabled="${advertiser.active ? false:true}" size="20" maxlength="25"/>
			<tag:text title="Phone:" property="contactPhoneNumber" value="${advertiser.contactPhoneNumber}" disabled="${advertiser.active ? false:true}" size="20" maxlength="35"/>
			<tag:text title="Email:" property="contactEmail" value="${advertiser.contactEmail}" size="20" disabled="${advertiser.active ? false:true}" maxlength="35"/>
		</div>
	</div>
	<div style="float:right;clear:both"><c:choose>
		<c:when test="${isEdit}">
			<h:submit value="Save" onclick="${onSubmit}" style="float:left;background-color:#e8ebf0; border:1px solid #ccc; padding:0px; color:#666666; margin-right:10px; cursor:hand;font-family: Verdana;" onmouseout="this.style.color='#666666'" onmouseover="this.style.color='red'" styleId="saveButton"/> 
			<c:url var="cancelURL"
				value="/dealer/display/internetAds.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}" />
			<span class="cancelURL">
				<proto:linkRemote href="/IMT/NotifyOps.go?businessUnitId=${businessUnitId}&exportMarket=${thirdPartyEntity.name}" update="">Notify Ops</proto:linkRemote>
				<proto:linkRemote href="${cancelURL}" update="advertiserPrefs${thirdPartyEntityId}">Cancel</proto:linkRemote>
			</span>
		</c:when>
		<c:otherwise>${editURL }
			<c:url var="editURL"
				value="/dealer/edit/internetAds.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}" />
			<span class="editLink">
				<proto:linkRemote href="/IMT/NotifyOps.go?businessUnitId=${businessUnitId}&exportMarket=${thirdPartyEntity.name}" update="">Notify Ops</proto:linkRemote>
				<proto:linkRemote href="${editURL}" update="advertiserPrefs${thirdPartyEntityId}">Edit</proto:linkRemote>
			</span>
		</c:otherwise>
	</c:choose></div>
</proto:formRemote>
	<c:if test="${not isEdit}">
		<div id="opsSignoff${thirdPartyEntityId}" style="float:left;width:350px;<c:if test="${not advertiser.active}">display:none</c:if>" >
			<t:insert page="/dealer/display/opsSignoff.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}"/>
		</div>
		<div id="enableTransmission${thirdPartyEntityId}"  style="float:right;width:350px;<c:if test="${not advertiser.status}">display:none</c:if>" >
			<t:insert page="/dealer/display/enableTransmission.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}"/>
		</div>
	</c:if>
</div>

