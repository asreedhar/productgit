<%@include file="/include/tag-import.jsp" %>

<tag:element title="Overall New Targets" id="${param.id}" 
             section="dealer" element="scorecardNewOverall" 
             formBean="${dealer_scorecardNewOverall}">

  <div style="clear: both">
  <table width="100%">
    <tr>
      <td>Retail AGP</td>
      <td>F&amp;I AGP</td>
      <td>Inv. Age</td>
      <td>Days Sply</td>
    </tr>
    <tr>
      <td>
        <c:set var="target19"><b:write name="scorecard" property="target19"/></c:set>
	    <tag:text size="2" property="target19" emptyVal="u" value="${target19}"/>
      </td>
      <td>
        <c:set var="target20"><b:write name="scorecard" property="target20"/></c:set>
	    <tag:text size="2" property="target20" emptyVal="u" value="${target20}"/>
      </td>
      <td>
        <c:set var="target22"><b:write name="scorecard" property="target22"/></c:set>
	    <tag:text size="2" property="target22" emptyVal="u" value="${target22}"/>
      </td>
      <td>
        <c:set var="target23"><b:write name="scorecard" property="target23"/></c:set>
	    <tag:text size="2" property="target23" emptyVal="u" value="${target23}"/>
      </td> 
    </tr>
  </table>
  </div>

</tag:element>