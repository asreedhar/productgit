<%@include file="/include/tag-import.jsp" %>

<tag:element title="CIA Engine" id="${dealer.id}" 
             section="dealer" element="ciaEngine" 
             formBean="${dealer_ciaEngine}">

  <tag:text title="Unit Cost (Lower):" property="unitCostThresholdLower" size="10"
            value="${dealer.preference.unitCostThresholdLower}"/><br/>
  <tag:text title="Unit Cost (Upper):" property="unitCostThresholdUpper" size="12"
            value="${dealer.preference.unitCostThresholdUpper}"/><br/>
  <tag:text title="FE Gross T.:" property="feGrossProfitThreshold" size="12"
            value="${dealer.preference.feGrossProfitThreshold}"/><br/>
  <tag:text title="Sell Through Rate:" property="sellThroughRate" size="3"
            value="${dealer.preference.sellThroughRate} %"/><br/>
  <tag:checkbox title="Include BE in Valuation:" property="includeBackEndInValuation"
            value="${dealer.preference.includeBackEndInValuation}"/><br/>
  <tag:text title="Margin Percentile For Rank Valuation:" property="marginPercentile" size="3"
            value="${dealer.preference.marginPercentile}"/><br/>
  <tag:text title="Days To Sale Percentile Rank For Valuation:" property="daysToSalePercentile" size="3"
            value="${dealer.preference.daysToSalePercentile}"/><br/><br/><br/>
  <tag:text title="VehicleSale Threshold For Core Market Penetration:" property="vehicleSaleThresholdForCoreMarketPenetration" size="3"
            value="${dealer.preference.vehicleSaleThresholdForCoreMarketPenetration}"/><br/>

</tag:element>