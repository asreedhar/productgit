<%@include file="/include/tag-import.jsp" %>

<tag:element title="Light Targets" id="${dealer.id}" 
             section="dealer" element="lights" 
             formBean="${dealer_lights}">

  <tag:text size="2" title="Red %:" property="redLightTarget" value="${dealer.risk.redLightTarget}"/>
  <tag:text size="2" title="Yellow %:" property="yellowLightTarget" value="${dealer.risk.yellowLightTarget}"/>
  <tag:text size="2" title="Green %:" property="greenLightTarget" value="${dealer.risk.greenLightTarget}"/>

</tag:element>