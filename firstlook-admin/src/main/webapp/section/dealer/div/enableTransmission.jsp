<%@include file="/include/tag-import.jsp"%>

<br /><br />
<c:set var="businessUnitId"  value="${param.businessUnitId}" />
<c:set var="thirdPartyEntityId"  value="${param.thirdPartyEntityId}" />
<ref:single beanName="imt-persist-thirdPartyEntityDao" var="thirdPartyEntity" idVal="${thirdPartyEntityId}" />

<c:url var="saveURL" value="/dealer/save/enableTransmission.do">
	<c:param name="businessUnitId" value="${businessUnitId}" />
	<c:param name="thirdPartyEntityId" value="${thirdPartyEntityId}" />
	<c:param name="excludes" value="exporting,incrementalExport,exportClientId,exportGuid" />
</c:url>
<div id="opsSignoff${thirdPartyEntityId}">
<proto:formRemote href="${saveURL}" id="enableForm${thirdPartyEntityId}" name="dealer_enableTransmission" update="enableTransmission${thirdPartyEntityId}">
<c:set var="org.apache.struts.taglib.html.BEAN" value="${dealer_enableTransmission}" scope="request" />
	<c:if test="${isEdit}">
		<h:hidden property="businessUnitId" />
		<h:hidden property="thirdPartyEntityId" />
	</c:if>
	<div style="float:right;width:350px;padding-top:5px">
		<h3>Enable</h3>
		<tag:checkbox title="Enable Full Data Transmission?" property="exporting" id="enableBox${thirdPartyEntityId}" value="${advertiser.exporting}"/><br />
		<c:if test="${thirdPartyEntity.name eq 'eBizAutos' or thirdPartyEntity.name eq 'AutoUplink'}">
			<tag:checkbox title="Enable Incremental Data Transmission?" property="incrementalExport" id="enableBox${thirdPartyEntityId}" value="${advertiser.incrementalExport}"/><br />
		</c:if>
		<c:if test="${thirdPartyEntity.name eq 'eBizAutos'}">			
			<tag:text title="Client ID" property="exportClientId" value="${advertiser.exportClientId}" /><br />
			<tag:text title="Client Guid" property="exportGuid" value="${advertiser.exportGuid}" /><br />
		</c:if>
	</div>
	<c:choose>
		<c:when test="${isEdit}">
			<c:url var="cancelURL"
				value="/dealer/display/internetAds.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}" />
			<span class="cancelURL" style="float:right"><proto:linkRemote href="${cancelURL}" update="advertiserPrefs${thirdPartyEntityId}">Cancel</proto:linkRemote></span>
			<h:submit value="Save" onclick="${onSubmit}" style="float:right;background-color:#e8ebf0; border:1px solid #ccc; padding:0px; color:#666666; margin-right:10px; cursor:hand;font-family: Verdana;" onmouseout="this.style.color='#666666'" onmouseover="this.style.color='red'" styleId="saveButton"/> 
		</c:when>
		<c:otherwise>${editURL}
			<c:url var="editURL"
				value="/dealer/edit/enableTransmission.do?businessUnitId=${businessUnitId}&thirdPartyEntityId=${thirdPartyEntityId}" />
			<span class="editLink"><proto:linkRemote href="${editURL}" update="enableTransmission${thirdPartyEntityId}">Edit</proto:linkRemote></span>
		</c:otherwise>
	</c:choose>
</proto:formRemote>
</div>