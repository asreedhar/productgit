<%@include file="/include/tag-import.jsp" %>

<tag:element title="Redistribution Preferences" id="${dealer.id}" 
             section="dealer" element="redistribution" 
             formBean="${dealer_redistribution}">

<tag:text title="Display Top X Stores:" property="redistributionNumTopDealers" value="${dealer.preference.redistributionNumTopDealers}" size="4" />
<br />

<tag:select title="Distance" property="redistributionDealerDistance" value="${dealer.preference.redistributionDealerDistance}"
	showOptions="${true}">
	<c:set var="selected">${dealer.preference.redistributionDealerDistance}</c:set>
	<tag:option value="50" selected="${selected}">&lt;= 50</tag:option>
	<tag:option value="100" selected="${selected}">&lt;= 100</tag:option>
	<tag:option value="150" selected="${selected}">&lt;= 150</tag:option>
	<tag:option value="250" selected="${selected}">&lt;= 250</tag:option>
    <tag:option value="500" selected="${selected}">&lt;= 500</tag:option>
    <tag:option value="1000" selected="${selected}">&lt;= 1000</tag:option>
    <tag:option value="999999" selected="${selected}">Unlimited</tag:option>
</tag:select>
<br />
<h3> Sort By </h3>
<tag:select title="ROI" property="redistributionRoi" value="${dealer.preference.redistributionRoi}" showOptions="${true}">
	<c:set var="selected">${dealer.preference.redistributionRoi}</c:set>
	<tag:option value="0" selected="${selected}">0</tag:option>
	<tag:option value="1" selected="${selected}">1</tag:option>
	<tag:option value="2" selected="${selected}">2</tag:option>
</tag:select>
<br />
<tag:select title="Understock" property="redistributionUnderstock" value="${dealer.preference.redistributionUnderstock}"
	showOptions="${true}">
	<c:set var="selected">${dealer.preference.redistributionUnderstock}</c:set>
	<tag:option value="0" selected="${selected}">0</tag:option>
	<tag:option value="1" selected="${selected}">1</tag:option>
	<tag:option value="2" selected="${selected}">2</tag:option>
</tag:select>
<br />

  <h6>Note redistribution preferences are active only when the redistribution upgrade is selected.</h6>  

</tag:element>
