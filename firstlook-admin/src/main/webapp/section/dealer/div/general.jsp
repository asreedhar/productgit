<%@include file="/include/tag-import.jsp" %>

<tag:element title="General" id="${dealer.id}" 
             section="dealer" element="general" 
             formBean="${dealer_general}">

  <c:set var="inceptionDate"><f:formatDate value="${dealer.preference.inceptionDate}" dateStyle="long"/></c:set>
  <tag:text title="Inception Date:" property="inceptionDate" value="${inceptionDate}"/><br/>
  <tag:select title="Program Type:" property="programType" value="${dealer.preference.programType}"  
              selectedValue="${dealer.preference.programType}" isEnum="${true}" enumValues="${programTypeEnum}"
			  omitValue="NONE"/><br/>
			  
  <tag:text size="35" title="Pack Amount:" property="packAmount" value="${dealer.preference.packAmount}"/><br/>

  <tag:select title="Run Day:" property="runDay" value="${dealer.preference.runDay}"  
              selectedValue="${dealer.preference.runDay}" isEnum="${true}" enumValues="${runDayEnum}"
			  omitValue="NONE"/>

</tag:element>