<%@include file="/include/tag-import.jsp" %>

<h3>Add Existing Member</h3>
<c:url var="addMembers" value="/dealer/custom/addMembers.do" /> 
  <input type="hidden" name="dealerId" value="${dealer.id}" />
  <p>Search by Name (Last or First): <input type="text" id="memberName" size="30" /></p>
  <SCRIPT language="JavaScript">
    $('memberName').focus(); 
  </SCRIPT>
  <c:url var="memberSearchURL"
	     value="/dealer/div/associateMemberSearch.do" />
  <proto:observeField href="${memberSearchURL}"
	                  update="memberSearchResults" field="memberName" frequency="1" />
  <div id="memberSearchResults" />
