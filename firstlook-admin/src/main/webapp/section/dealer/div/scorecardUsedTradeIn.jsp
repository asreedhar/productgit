<%@include file="/include/tag-import.jsp" %>

<tag:element title="Used: Trade-ins" id="${param.id}" 
             section="dealer" element="scorecardUsedTradeIn" 
             formBean="${dealer_scorecardUsedTradeIn}">
             
  <div style="clear: both">
  <table>
    <tr>
      <td>Retail AGP</td>
      <td>Sell Thru %</td>
      <td>Avg Days 2 Sale</td>
      <td>Avg Inv Age</td>
    </tr>
    <tr>
      <td>
        <c:set var="target15"><b:write name="scorecard" property="target15"/></c:set>
	    <tag:text size="2" property="target15" value="${target15}"/>
      </td>
      <td>
        <c:set var="target16"><b:write name="scorecard" property="target16"/></c:set>
	    <tag:text size="2" property="target16" value="${target16}"/>
      </td>
      <td>
        <c:set var="target17"><b:write name="scorecard" property="target17"/></c:set>
	    <tag:text size="2" property="target17" value="${target17}"/>
      </td>
      <td>
        <c:set var="target18"><b:write name="scorecard" property="target18"/></c:set>
	    <tag:text size="2" property="target18" value="${target18}"/>
      </td>
    </tr>
  </table>
  <table>
    <tr>
      <td>Non-Win Wsaled %</td>
      <td>Imm Wsale AGP</td>
      <td>Aged Wsale AGP</td>
    </tr>
    <tr>
      <td>
        <c:set var="target14"><b:write name="scorecard" property="target14"/></c:set>
	    <tag:text size="2" property="target14" value="${target14}"/>
      </td>
      <td>
        <c:set var="target44"><b:write name="scorecard" property="target44"/></c:set>
	    <tag:text size="2" property="target44" value="${target44}"/>
      </td>
      <td>
        <c:set var="target45"><b:write name="scorecard" property="target45"/></c:set>
	    <tag:text size="2" property="target45" value="${target45}"/>
      </td>
    </tr>
  </table>
  </div>

</tag:element>