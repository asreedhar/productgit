<%@include file="/include/tag-import.jsp" %>

<h2>Dealer Franchise Preferences</h2>

<ul style="clear:both">
  <c:forEach items="${dealer.franchises}" var="franchise">
    <li>
      ${franchise.description} 
      <c:url var="removeFranchise" value="/dealer/custom/removeFranchise.do">
        <c:param name="id" value="${dealer.id}"/>
        <c:param name="franchiseId" value="${franchise.id}"/>
      </c:url>
      <proto:linkRemote href="${removeFranchise}" update="franchises"
         confirm="Are you sure you want to remove ${franchise.description} from this dealer?">
        remove
      </proto:linkRemote>
    </li>
  </c:forEach>
</ul>
<c:url var="addFranchise" value="/dealer/custom/addFranchise.do"/>
<proto:formRemote href="${addFranchise}" update="franchises" id="franchiseForm">
  <input type="hidden" name="id" value="${dealer.id}"/>
  <ref:ref beanName="imt-persist-franchiseDao" scope="request"/>
  <select name="franchiseId">
    <c:forEach items="${values}" var="current">
      <option value="${current.id}">${current.description}</option>
    </c:forEach>
  </select>
  <input type="submit" value="Add Franchise..."/>
</proto:formRemote>
