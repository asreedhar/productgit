<%@include file="/include/tag-import.jsp" %>

<c:choose>
  <c:when test="${isEdit}">
    <%@include file="/section/dealer/edit/div/group.jsp"%>
  </c:when>
  <c:otherwise>
    <%@include file="/section/dealer/display/div/group.jsp"%>
  </c:otherwise>
</c:choose>


