<%@include file="/include/tag-import.jsp" %>

<tag:element title="Year Thresholds" id="${dealer.id}" 
             section="dealer" element="yearThresholds" 
             formBean="${dealer_yearThresholds}">

  <tag:text title="Initial Time Period Year Offset:" property="riskLevelYearInitialTimePeriod"
            value="${dealer.risk.riskLevelYearInitialTimePeriod}"/><br/>
  <tag:text title="Secondary Time Period Year Offset:" property="riskLevelYearSecondaryTimePeriod"
            value="${dealer.risk.riskLevelYearSecondaryTimePeriod}"/><br/>
  <tag:text title="Roll Over Month:" property="riskLevelYearRollOverMonth"
            value="${dealer.risk.riskLevelYearRollOverMonth}"/><br/>

</tag:element>
