<%@include file="/include/tag-import.jsp" %>

<tag:element title="New: Aging Inventory" id="${param.id}" 
             section="dealer" element="scorecardNewAging" 
             formBean="${dealer_scorecardNewAging}">

  <div style="clear: both">
  <table width="100%">
    <tr>
      <td>Days:</td>
      <td>0-30</td>
      <td>30-60</td>
      <td>61-75</td>
      <td>76-90</td>
      <td>&gt; 90</td>
    </tr>
    <tr>
      <td>Percent:</td>
      <td>
        <c:set var="target28"><b:write name="scorecard" property="target28"/></c:set>
	    <tag:text size="2" property="target28" emptyVal="u" value="${target28}"/>
      </td>
      <td>
        <c:set var="target27"><b:write name="scorecard" property="target27"/></c:set>
	    <tag:text size="2" property="target27" emptyVal="u" value="${target27}"/>
      </td>
      <td>
        <c:set var="target26"><b:write name="scorecard" property="target26"/></c:set>
	    <tag:text size="2" property="target26" emptyVal="u" value="${target26}"/>
      </td>
      <td>
        <c:set var="target25"><b:write name="scorecard" property="target25"/></c:set>
	    <tag:text size="2" property="target25" emptyVal="u" value="${target25}"/>
      </td>
      <td>
        <c:set var="target24"><b:write name="scorecard" property="target24"/></c:set>
	    <tag:text size="2" property="target24" emptyVal="u" value="${target24}"/>
      </td>
    </tr>
  </table>
  </div>

</tag:element>