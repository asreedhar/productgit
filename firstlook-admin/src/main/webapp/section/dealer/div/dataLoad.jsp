<%@include file="/include/tag-import.jsp" %>

<tag:element title="Data Load" id="${dealer.id}" 
             section="dealer" element="dataLoad" 
             formBean="${dealer_dataLoad}">

  <tag:checkbox title="Update Code on Sale:" property="unitCostUpdateOnSale" 
  				value="${dealer.preference.unitCostUpdateOnSale}"/><br/>
  <tag:text title="Unit Cost Threshold:" property="unitCostThreshold"
            value="${dealer.preference.unitCostThreshold}"/><br/>
  <tag:text title="Unwind Days Thresh.:" property="unwindDaysThreshold"
            value="${dealer.preference.unwindDaysThreshold}"/><br/>

</tag:element>
