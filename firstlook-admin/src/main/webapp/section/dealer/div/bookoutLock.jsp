<%@include file="/include/tag-import.jsp"%>

<c:set var="appLockEnabled">
	<b:write name="credentials" property="appLockEnabled" />
</c:set>
<c:set var="appLockPass">
	<b:write name="credentials" property="appLockPass" />
</c:set>
<c:set var="credentialsId">
	<b:write name="credentials" property="id" />
</c:set>

<tag:element title="Bookout Lock" id="${credentialsId}" section="dealer"
	element="bookoutLock" formBean="${dealer_bookoutLock}">

	<table width="100%" align="left">
		<tr>
			<td><tag:checkbox title="Enable Bookout Lock:"
				property="appLockEnabled" value="${appLockEnabled}" /><br />
			</td>
		</tr>
		<tr>
			<td><tag:password title="Bookout Lock password:"
				property="appLockPass" value="${appLockPass}" size="15" /><br />
			</td>
		</tr>
	</table>
</tag:element>