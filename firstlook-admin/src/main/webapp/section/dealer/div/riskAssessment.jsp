<%@include file="/include/tag-import.jsp" %>

<tag:element title="Risk Assessment" id="${dealer.id}" 
             section="dealer" element="riskAssessment" 
             formBean="${dealer_riskAssessment}">

  <h3>Risk Level Thresholds</h3>
  <tag:text title="# Weeks:" property="riskLevelNumberOfWeeks" value="${dealer.risk.riskLevelNumberOfWeeks}"/><br/>
  <tag:text title="# Deals:" property="riskLevelDealsThreshold" value="${dealer.risk.riskLevelDealsThreshold}"/><br/>
  <tag:text title="# of Contributors:" property="riskLevelNumberOfContributors" value="${dealer.risk.riskLevelNumberOfContributors}"/><br/>
 
  <h3>Red Light Thresholds</h3>
  <tag:text title="No Sale &gt;" property="redLightNoSaleThreshold" value="${dealer.risk.redLightNoSaleThreshold}"/><br/>
  <tag:text title="Gross Profit &lt;=" property="redLightGrossProfitThreshold" value="${dealer.risk.redLightGrossProfitThreshold}"/><br/>

  <h3>Green Light Thresholds</h3>
  <tag:text title="No Sale &lt;" property="greenLightNoSaleThreshold" value="${dealer.risk.greenLightNoSaleThreshold}"/><br/>
  <tag:text title="Gross Profit &gt;=" property="greenLightGrossProfitThreshold" value="${dealer.risk.greenLightGrossProfitThreshold}"/><br/>
  <tag:text title="Margin &gt;=" property="greenLightMarginThreshold" value="${dealer.risk.greenLightMarginThreshold}"/><br/>
  <tag:text title="Days % &lt;=" property="greenLightDaysPercentage" value="${dealer.risk.greenLightDaysPercentage}"/><br/>

  <h3>Age Band Targets</h3>
<tag:text title="Year &gt;=" property="highMileagePerYearThreshold" value="${dealer.risk.highMileageThreshold}"/><br/>
  <tag:text title="Overall &gt;=" property="highMileageThreshold" value="${dealer.risk.highMileageThreshold}"/><br/>
  <tag:text title="Red Light &gt;=" property="excessiveMileageThreshold" value="${dealer.risk.highMileageThreshold}"/><br/>
  
</tag:element>