<%@include file="/include/tag-import.jsp" %>

<div id="memberCreateButton"><a href="#"
	onclick="Element.toggle('memberCreateButton'); Element.toggle('memberCreateForm');" />
	add new member to dealer</a></div>
<div id="memberCreateForm" class="wide-section" style="display:none;">
<div id="memberCreateMessage"></div>
<h3>Add New Member</h3>
<c:url var="createMember" value="/dealer/custom/memberCreate.do" />
	<proto:formRemote id="createMember"	href="${createMember}" update="members">

	<input type="hidden" name="id" value="${dealer.id}" />
	<table>
		<tr>
			<td><font color="FF0000">*</font>First Name:</td>
			<td><input type="text" size="15" name="firstName" /></td>
		</tr>
		<tr>
			<td><font color="FF0000">*</font>Last Name:</td>
			<td><input type="text" size="15" name="lastName" /></td>
		</tr>
		<tr>
			<td><font color="FF0000">*</font>Login:</td>
			<td><input type="text" size="15" name="login" /></td>
		</tr>
		<tr>
			<td><font color="FF0000">*</font>Password:</td>
			<td><input type="password" size="15" name="password" /></td>
		</tr>
		<tr>
			<td><ref:ref beanName="imt-persist-jobTitleDao" scope="request" />
			<font color="FF0000">*</font>Job Title:</td>
			<td><select name="jobTitleId">
				<c:forEach items="${values}" var="value">
					<option value="${value.id}">${value.name}</option>
				</c:forEach>
			</select></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><input type="text" size="12" name="emailAddress" />
			</td>
		</tr>
		<tr>
			<td><font color="FF0000">*</font>Office Phone:</td>
			<td><input type="text" size="12" name="officePhoneNumber" /> Ext:<input type="text" size="5" name="officePhoneExtension" />
			</td>
		</tr>
		<tr>
			<td><font color="FF0000">*</font>Office Fax:</td>
			<td><input type="text" size="15" name="officeFaxNumber" /></td>
		</tr>
		<tr>
			<td>Prefered Reporting Method:</td>
			<td>
				<select name="reportMethod"> 
					<option selected value="Email">Email</option>
					<option value="Fax">Fax</option>
				</select>
			</td>
		</tr>
		<tr>
			<td> Member Type</td>
			<td><select onchange="" size="1" name="memberType"><option value="ADMIN">ADMIN</option>
            
          
			
              <option selected="selected" value="USER">USER</option>
            
          
			
              <option value="ACCOUNT_REP">ACCOUNT_REP</option>
            
          
			
              <option value="SALESPERSON">SALESPERSON</option></select>
              </td>
		</tr>
		<tr>
		<td>User Role:</td>
			<td>
				<select name="userRole"> 
					<option value="Used">Used</option>
					<option value="New">New</option>
					<option selected value="All">All</option>
				</select>
			</td>
		</tr>
		<tr>
		<td>Used Role:</td>
			<td>
				<select name="usedRole"> 
					<option value="Standard">Standard</option>
					<option value="Appraiser">Appraiser</option>
					<option selected value="Manager">Manager</option>
					<option value="NO_ACCESS">No Access</option>
				</select>
			</td>
		</tr>				
		<td>New Role:</td>
			<td>
				<select name="newRole"> 
					<option selected value="Standard">Standard</option>
					<option value="NO_ACCESS">No Access</option>
				</select>
			</td>
		</tr>				
		<tr>
			<td></td>
			<td><input type="checkbox" name="allDealers" value="true" /> Add
			to all dealers in group</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Add Member" /></td>
		</tr>
		<tr>
			<td colspan="2"><font color="FF0000">All field with a red * are required.</font></td>
	</table>
</proto:formRemote></div>

