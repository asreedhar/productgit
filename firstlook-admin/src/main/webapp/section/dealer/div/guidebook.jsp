<%@include file="/include/tag-import.jsp" %>
<c:set var="hasKBBTradeInUpgrade" value="false" />
<c:set var="hasNadaValuesUpgrade" value="false" />
<c:forEach items="${dealer.dealerUpgrade}" var="upgrade">
	<c:if test="${upgrade.dealerUpgradeCD == 17}">
	 	<c:set var="hasKBBTradeInUpgrade" value="true" />
	</c:if>
	<c:if test="${upgrade.dealerUpgradeCD == 28}">
	 	<c:set var="hasNadaValuesUpgrade" value="true" />
	</c:if>
</c:forEach>
<script type="text/javascript" language="javascript">
function filterGuideBooks(bookId, book, hasKBBTradeInUpgrade) {
	var path = '/fl-admin/dealer/update/bookCategories.do?bookId='+bookId+'&book='+book+'&hasKBBTradeInUpgrade='+hasKBBTradeInUpgrade;
	var planRequest = new Ajax.Updater(
			'bookCategories'+book,
			path,
			{ asynchronous:true, evalScripts:true,
			onComplete: function(){ 				}
		});
}
</script>

<c:if test="${hasNadaValuesUpgrade}">
	<script>
	function checkNadaUpgrade(){
	
		if($('guideBook1').value==2||$('guideBook2').value==2)
		{
			alert("NADA cannot be selected as a primary or secondary book for the store if the NADA Values Upgrade is selected")
			return false;
		}
		return true;
	}
	</script>

 	<c:set var="onSubmit" value="if(!checkNadaUpgrade())return false;" />
</c:if>

<tag:element onSubmit="${onSubmit}" title="Guide Books" id="${dealer.id}" section="dealer" element="guidebook" formBean="${dealer_guidebook}">

  <tag:checkbox title="BookOut Agreed to?:" property="bookOut" value="${dealer.preference.bookOut}"/><br/>
  <tag:checkbox title="Calculate Average Book Values:" property="calculateAverageBookValue" value="${dealer.preference.calculateAverageBookValue}"/><br/>
  <tag:checkbox title="Set Advertising Status: " property="advertisingStatus" value="${dealer.preference.advertisingStatus}"/><br/>

  <ref:ref beanName="imt-persist-thirdPartyDao" scope="session" var="guidebooks"/>
  <ref:ref beanName="imt-persist-thirdPartyCategoryDao" scope="session" var="categories"/>
  <c:set var="bookPrefId" value="${dealer.preference.guideBookId}" />
  <c:set var="bookCatId" value="${dealer.preference.bookOutPreferenceId}" scope="session"/>
  <c:set var="bookCat2Id" value="${dealer.preference.bookOutPreferenceSecondId}" scope="session"/>


  <h3>Guide Book One</h3>
    <c:if test="${not empty dealer.preference.guideBookId}">
      <ref:single beanName="imt-persist-thirdPartyDao" scope="request" var="guidebook1" idVal="${dealer.preference.guideBookId}"/>
      <tag:select id="guideBook1" title="Book:" property="guideBookId" value="${guidebook1.name}"
                  options="guidebooks" selectedValue="${dealer.preference.guideBookId}"
                  onchange="javascript: filterGuideBooks(this.value,1,'${hasKBBTradeInUpgrade}')"/>
    </c:if>
  <div>
  <label for="bookOutPreferenceId" class="fieldLabel">1st Type:</label>
  <label for="bookOutPreferenceSecondId" class="fieldLabel">2nd Type:</label>
  <c:choose>
  <c:when test="${isEdit}">
  	<span id="bookCategories1">
	  	<h:select property="bookOutPreferenceId" value="${dealer.preference.bookOutPreferenceId}">
	  		<c:forEach items="${categories}" var="bookCats">
	          	<c:if test="${bookCats.thirdParty.id == bookPrefId}">
	          		<c:if test="${bookCats.id == 11}"> <!-- || bookCats.id == 12}" > 11 = Trade-In, 12 = private party -->
						<c:if test="${hasKBBTradeInUpgrade}">
			          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
						</c:if>
	          		</c:if>
	          		<c:if test="${bookCats.id != 11 && bookCats.id != 12 && bookCats.id != 17 && bookCats.id != 18}" >
		          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
	          		</c:if>
	          	</c:if>
	        </c:forEach>
	    </h:select><br />
			
	    <h:select property="bookOutPreferenceSecondId"  value="${bookCat2Id}">
	  		<h:option value="0">None</h:option>
	  		<c:forEach items="${categories}" var="bookCats">
	          	<c:if test="${bookCats.thirdParty.id == bookPrefId}">
	          		<c:if test="${bookCats.id == 11 }" > <!-- || bookCats.id == 12}" > 11 = Trade-In, 12 = private party -->
						<c:if test="${hasKBBTradeInUpgrade}">
			          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
						</c:if>
	          		</c:if>
	          		<c:if test="${bookCats.id != 11 && bookCats.id != 12 && bookCats.id != 17 && bookCats.id != 18}" >
		          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
	          		</c:if>
	          	</c:if>
	        </c:forEach>
	    </h:select>
    </span>
  </c:when>
  <c:otherwise>
  	<ref:single beanName="imt-persist-thirdPartyCategoryDao" scope="request" var="gb1cat1" idVal="${dealer.preference.bookOutPreferenceId}"/>
    <span id="bookOutPreferenceId">${gb1cat1.name}</span><br />
    <c:choose>
	  	<c:when test="${not empty dealer.preference.bookOutPreferenceSecondId}">
	  		<ref:single beanName="imt-persist-thirdPartyCategoryDao" scope="request" var="book1Cat2" idVal="${dealer.preference.bookOutPreferenceSecondId}"/>
	  		<c:set var="book1Cat2Name" value="${book1Cat2.name}"/>
	  	</c:when>
	  	<c:otherwise>
	  		<c:set var="book1Cat2Name" value="None"/>
	  	</c:otherwise>
  	</c:choose>
    <span id="bookOutPreferenceSecondId">${book1Cat2Name}</span>
  </c:otherwise>
</c:choose>
</div>

<c:choose>
  	<c:when test="${not empty dealer.preference.guideBook2Id}">
  		<ref:single beanName="imt-persist-thirdPartyDao" scope="request" var="book2" idVal="${dealer.preference.guideBook2Id}"/>
  		<c:set var="guidebook2" value="${book2.name}"/>
  	</c:when>
  	<c:otherwise>
  		<c:set var="guidebook2" value="None"/>
  	</c:otherwise>
</c:choose>
<c:set var="book2CatId1" value="${not empty dealer.preference.guideBook2Id ? dealer.preference.guideBook2Id:'None'}"/>
<c:choose>
	  	<c:when test="${not empty dealer.preference.guideBook2BookOutPreferenceId}">
	  		<ref:single beanName="imt-persist-thirdPartyCategoryDao" scope="request" var="book2Cat1" idVal="${dealer.preference.guideBook2BookOutPreferenceId}"/>
	  		<c:set var="book2Cat1Name" value="${book2Cat1.name}"/>
	  	</c:when>
	  	<c:otherwise>
	  		<c:set var="book2Cat1Name" value="None"/>
	  	</c:otherwise>
  	</c:choose>
<c:set var="bookCat2Id" value="${not empty dealer.preference.guideBook2BookOutPreferenceId ? dealer.preference.guideBook2BookOutPreferenceId:0}"/>

<c:choose>
	  	<c:when test="${not empty dealer.preference.guideBook2SecondBookOutPreferenceId}">
	  		<ref:single beanName="imt-persist-thirdPartyCategoryDao" scope="request" var="book2Cat2" idVal="${dealer.preference.guideBook2SecondBookOutPreferenceId}"/>
	  		<c:set var="book2Cat2Name" value="${book2Cat2.name}"/>
	  	</c:when>
	  	<c:otherwise>
	  		<c:set var="book2Cat2Name" value="None"/>
	  	</c:otherwise>
  	</c:choose>
<c:set var="book2CatId1Cat2" value="${not empty dealer.preference.guideBook2SecondBookOutPreferenceId ? dealer.preference.guideBook2SecondBookOutPreferenceId:0}"/>

<c:if test="${dealer.preference.guideBookId eq 2}">
<ref:single idVal="${dealer.preference.nadaRegionCode}" beanName="imt-persist-NadaRegionDao" var="nadaRegion"/>
NADA Region: ${nadaRegion.description}
</c:if>

<h3>Guide Book Two</h3>
  <tag:select id="guideBook2" title="Book:" property="guideBook2Id" value="${guidebook2}"
                  options="guidebooks" selectedValue="${book2CatId1}"
                  emptyOption="${true}"
                  onchange="javascript: filterGuideBooks(this.value,2,'${hasKBBTradeInUpgrade}')"/>

  <div>
  <label for="guideBook2BookOutPreferenceId" class="fieldLabel">1st Type:</label>
  <label for="guideBook2SecondBookOutPreferenceId" class="fieldLabel">2nd Type:</label>
  <c:choose>
  <c:when test="${isEdit}">
  	<span id="bookCategories2">
	  	<h:select property="guideBook2BookOutPreferenceId" value="${bookCat2Id}">
	  		<h:option value="0">None</h:option>
	  		<c:forEach items="${categories}" var="bookCats">
	          	<c:if test="${bookCats.thirdParty.id == dealer.preference.guideBook2Id}">
	          		<c:if test="${bookCats.id == 11 }" > <!-- || bookCats.id == 12}" > 11 = Trade-In, 12 = private party -->
						<c:if test="${hasKBBTradeInUpgrade}">
			          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
						</c:if>
	          		</c:if>
	          		<c:if test="${bookCats.id != 11 && bookCats.id != 12 && bookCats.id != 17 && bookCats.id != 18}" >
		          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
	          		</c:if>
	          	</c:if>
	        </c:forEach>
	    </h:select><br />
	    <h:select property="guideBook2SecondBookOutPreferenceId"  value="${book2CatId1Cat2}">
	  		<h:option value="0">None</h:option>
	  		<c:forEach items="${categories}" var="bookCats">
	          	<c:if test="${bookCats.thirdParty.id == dealer.preference.guideBook2Id}">
	          		<c:if test="${bookCats.id == 11}" > <!-- || bookCats.id == 12}" > 11 = Trade-In, 12 = private party -->
						<c:if test="${hasKBBTradeInUpgrade}">
			          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
						</c:if>
	          		</c:if>
	          		<c:if test="${bookCats.id != 11 && bookCats.id != 12 && bookCats.id != 17 && bookCats.id != 18}" >
		          		<h:option value="${bookCats.id}">${bookCats.thirdParty.name} - ${bookCats.category}</h:option>
	          		</c:if>
	          	</c:if>
	        </c:forEach>
	    </h:select>
    </span>
  </c:when>
  <c:otherwise>
    <span id="guideBook2BookOutPreferenceId">${book2Cat1Name}</span><br />
    <span id="secondBookOutPreferenceSecondId">${book2Cat2Name}</span>
  </c:otherwise>
</c:choose>
</div>

<c:if test="${dealer.preference.guideBook2Id eq 2}">
<ref:single idVal="${dealer.preference.nadaRegionCode}" beanName="imt-persist-NadaRegionDao" var="nadaRegion"/>
NADA Region: ${nadaRegion.description}
</c:if>

  <br/><br/>
  <h3>KBB Specific Settings</h3>
<tag:select title="KBB Inventory Dataset:" property="kbbInventoryBookoutDatasetPreference"
             value="${dealer.preference.kbbInventoryBookoutDatasetPreference == 1 ? 'Current':'Newest'}">
<c:set var="selected1">${dealer.preference.kbbInventoryBookoutDatasetPreference}</c:set>
     <tag:option value="1" selected="${selected1}">Current</tag:option>
     <tag:option value="0" selected="${selected1}">Newest</tag:option>
</tag:select>

 <tag:select title="KBB Appraisal Dataset:" property="kbbAppraisalBookoutDatasetPreference"
              value="${dealer.preference.kbbAppraisalBookoutDatasetPreference == 1 ? 'Current':'Newest'}">
	<c:set var="selected2">${dealer.preference.kbbAppraisalBookoutDatasetPreference}</c:set>
     <tag:option value="1" selected="${selected2}">Current</tag:option>
     <tag:option value="0" selected="${selected2}">Newest</tag:option>
  </tag:select>

   <tag:select title="Default Inventory Condition:" property="kbbInventoryDefaultCondition"
              value="${dealer.preference.kbbInventoryDefaultCondition == 1 ? 'Excellent' : (dealer.preference.kbbInventoryDefaultCondition == 2 ? 'Good' : 'Fair')}">
	<c:set var="selected3">${dealer.preference.kbbInventoryDefaultCondition}</c:set>
     <tag:option value="1" selected="${selected3}">Excellent</tag:option>
     <tag:option value="2" selected="${selected3}">Good</tag:option>
     <tag:option value="3" selected="${selected3}">Fair</tag:option>
  </tag:select>

    <tag:checkbox title="Use archived KBB dataset:" property="kbbOverrideEnabled" value="${dealer.preference.kbbOverrideEnabled}"/>
    <a href="javascript:alert('this allows a dealer to bookout inventory against an older guidebook.\nif checked - they will hit our default old guidebook until midnight tonight\nwhen they will revert back to their default book\nFor a more detailed explanation see McGarry');">
    what is an archived dataset?
    </a>
    
    <br/><br/>
  <h3>BlackBook Specific Settings</h3>
	<tag:checkbox title="Enable Mobile BlackBook:" property="blackBookMobileOptInFlag" value="${dealer.preference.blackBookMobileOptInFlag}"/>
	<tag:text readOnly="readonly" title="BlackBook Mobile Opt In Date" value="${dealer.preference.blackBookOptInDate}"/> <br>
	<tag:text readOnly="readonly" title="BlackBook Mobile Opt Out Date"  value="${dealer.preference.blackBookOptOutDate}"/> 
     <br/><br/>
    
  <h3>Galves Specific Settings</h3>  
 	<tag:checkbox title="Enable Mobile Galves:" property="galvesMobileOptInFlag" value="${dealer.preference.galvesMobileOptInFlag}"/>
	<tag:text readOnly="readonly" title="Galves Mobile Opt In Date" value="${dealer.preference.galvesOptInDate}"/> <br>
	<tag:text readOnly="readonly" title="Galves Mobile Opt Out Date"  value="${dealer.preference.galvesOptOutDate}"/>
</tag:element>
