<%@include file="/include/tag-import.jsp" %>

<tag:element title="Dealership Print and Internet Advertising" id="${dealer.id}" section="dealer" 
			 element="advertisingPrefs" formBean="${dealer_advertisingPrefs}">
 	
 	<h3 style="float:left">General Settings:</h3><br>
	<tag:checkbox title="Send 0 as null?" property="sendZeroesAsNull" value="${dealer.preference.sendZeroesAsNull}"/><br/>
 
</tag:element>

