<%@include file="/include/tag-import.jsp"%>
<script type="text/javascript" language="javascript">
function updateMarketMappingDropDowns() {
	
	var url = '/fl-admin/dealer/ajax/businessUnitMarketDealer.do';
	var planRequest = new Ajax.Updater(
			'businessUnitMarketDealer',
			url,
			{
				parameters: Form.serialize($('dealer_businessUnitMarketDealer')),
				asynchronous:true, 
				evalScripts:true,
				onLoading: function(){
					Form.disable($('dealer_businessUnitMarketDealer'));
					Element.toggle('indicator');
				},
				onComplete: function(){
					Form.enable($('dealer_businessUnitMarketDealer'));
				}
			});
	return false;
}
</script>
<tag:element title="Market Mapping" id="${dealer.id}" section="dealer"
	element="businessUnitMarketDealer"
	formBean="${dealer_businessUnitMarketDealer}">

<div>
	<c:if test="${isEdit}">
		<tag:select title="Market State" property="selectedState"
					value="${dealer.businessUnitMarketDealer.selectedState}"
					selectedValue="${dealer.businessUnitMarketDealer.selectedState}"
					isEnum="true"
					enumValues="${states}"
					onchange="javascript:updateMarketMappingDropDowns();">
		</tag:select>
		<tag:select title="Market County" property="selectedCounty"
					value="${dealer.businessUnitMarketDealer.selectedCounty}"
					selectedValue="${dealer.businessUnitMarketDealer.selectedCounty}"
					isEnum="true"
					enumValues="${counties}"
					onchange="javascript:updateMarketMappingDropDowns();">
		</tag:select>
	</c:if>
	<tag:select title="Market Mapping" property="dealerNumber"
				value="${dealer.businessUnitMarketDealer.marketDealerName}"
				selectedValue="${dealer.businessUnitMarketDealer.dealerNumber}"
				emptyContent="No Mappings set.">
		<c:forEach items="${marketMappings}" var="marketMapping">
			<tag:option value="${marketMapping.value}" selected="${dealer.businessUnitMarketDealer.dealerNumber}">${marketMapping.label} (${marketMapping.value})</tag:option>
		</c:forEach>
	</tag:select>
</div>
</tag:element>

