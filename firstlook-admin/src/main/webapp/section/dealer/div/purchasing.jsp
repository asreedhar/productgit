<%@include file="/include/tag-import.jsp" %>

<tag:element title="Purchasing" id="${dealer.id}" 
             section="dealer" element="purchasing" 
             formBean="${dealer_purchasing}">

  <tag:checkbox title="ATC Enabled:" property="atcEnabled"
            value="${dealer.preference.atcEnabled}"/><br/>
  <tag:checkbox title="GMAC Enabled:" property="gmacEnabled"
            value="${dealer.preference.gmacEnabled}"/><br/>
  <tag:checkbox title="TFS Enabled:" property="tfsEnabled"
            value="${dealer.preference.tfsEnabled}"/><br/>
  <tag:checkbox title="ADESA Dealer Block Enabled:" property="oveEnabled"
            value="${dealer.preference.oveEnabled}"/><br/>
           	
  Auction Area:<br /><br />
  
  <tag:select title="Online Auction and In-Group <br /> Distance from Dealer (miles):" property="purchasingDistanceFromDealer" 
              value="${dealer.preference.purchasingDistanceFromDealer} miles"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.purchasingDistanceFromDealer}</c:set>
     <tag:option value="50" selected="${selected}">&lt;= 50</tag:option>
     <tag:option value="100" selected="${selected}">&lt;= 100</tag:option>
     <tag:option value="150" selected="${selected}">&lt;= 150</tag:option>
     <tag:option value="250" selected="${selected}">&lt;= 250</tag:option>
     <tag:option value="500" selected="${selected}">&lt;= 500</tag:option>
     <tag:option value="1000" selected="${selected}">&lt;= 1000</tag:option>
     <tag:option value="999999" selected="${selected}">Unlimited</tag:option>
  </tag:select>         
  <br /><br /><br />
  <tag:select title="Live Auction Distance from Dealer (miles):" property="liveAuctionDistanceFromDealer" 
              value="${dealer.preference.liveAuctionDistanceFromDealer} miles"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.liveAuctionDistanceFromDealer}</c:set>
     <tag:option value="50" selected="${selected}">&lt;= 50</tag:option>
     <tag:option value="100" selected="${selected}">&lt;= 100</tag:option>
     <tag:option value="150" selected="${selected}">&lt;= 150</tag:option>
     <tag:option value="250" selected="${selected}">&lt;= 250</tag:option>
     <tag:option value="500" selected="${selected}">&lt;= 500</tag:option>
     <tag:option value="1000" selected="${selected}">&lt;= 1000</tag:option>
     <tag:option value="999999" selected="${selected}">Unlimited</tag:option>
  </tag:select>
  
  <br /><br />   

  <tag:checkbox title="Display Unit Cost To Dealer Group:" property="displayUnitCostToDealerGroup"
            value="${dealer.preference.displayUnitCostToDealerGroup}"/><br/>
</tag:element>