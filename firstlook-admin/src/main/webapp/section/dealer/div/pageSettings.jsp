<%@include file="/include/tag-import.jsp" %>

<tag:element title="Page Settings" id="${dealer.id}" 
             section="dealer" element="pageSettings" 
             formBean="${dealer_pageSettings}">
  
<h3>Aging Plan Settings</h3>
<%--  
  <tag:checkbox title="Apply Prior Aging Plan Notes:" property="applyPriorAgingNotes"
           value="${dealer.preference.applyPriorAgingNotes}"/><br/>
--%>
  <tag:select title="Aging Inventory Display:" property="agingInventoryTrackingDisplayPref" 
              value="${dealer.preference.agingInventoryTrackingDisplayPref}"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.agingInventoryTrackingDisplayPref}</c:set>
     <tag:option value="1" selected="${selected}">Stock Number</tag:option>
     <tag:option value="0" selected="${selected}">VIN</tag:option>
  </tag:select><br/>
  <tag:select title="Apply Appraisal Options to IMP Planning:" property="checkAppraisalHistoryForIMPPlanning" 
              value="${dealer.preference.checkAppraisalHistoryForIMPPlanning}"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.checkAppraisalHistoryForIMPPlanning}</c:set>
     <tag:option value="1" selected="${selected}">True</tag:option>
     <tag:option value="0" selected="${selected}">False</tag:option>
  </tag:select><br/>
  <tag:select title="Apply Default Plan to Green Lights in IMP:" property="applyDefaultPlanToGreenLightsInIMP" 
              value="${dealer.preference.applyDefaultPlanToGreenLightsInIMP}"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.applyDefaultPlanToGreenLightsInIMP}</c:set>
     <tag:option value="1" selected="${selected}">True</tag:option>
     <tag:option value="0" selected="${selected}">False</tag:option>
  </tag:select><br/>

<h3>Inventory Overview Settings</h3>

  <tag:select title="Inventory Overview Display:" property="stockOrVinPreference" 
              value="${dealer.preference.stockOrVinPreference}"
              showOptions="${true}">
     <c:set var="selected">${dealer.preference.stockOrVinPreference}</c:set>
     <tag:option value="1" selected="${selected}">Stock Number</tag:option>
     <tag:option value="0" selected="${selected}">VIN</tag:option>
  </tag:select><br/>

  <tag:text title="Units Sold Threshold Inventory Overview:" property="unitsSoldThresholdInvOverview"
           value="${dealer.preference.unitsSoldThresholdInvOverview}" size="4"/><br/><br/><br/>
  
  <tag:text title="Average Inventory Age Threshold (Red text):" property="averageInventoryAgeRedThreshold"
           value="${dealer.preference.averageInventoryAgeRedThreshold}" size="4"/><br/><br/>

  <tag:text title="Average Days Supply Threshold (Red text):" property="averageDaysSupplyRedThreshold"
           value="${dealer.preference.averageDaysSupplyRedThreshold}" size="4"/><br/><br/>		   
  
  <tag:checkbox title="Display In-Transit Inventory Form:"
				property="showInTransitInventoryForm" value="${dealer.preference.showInTransitInventoryForm}" /><br />
  
  <h3>Appraisal Form Settings </h3>
  
  
  <tag:text title="Appraisal Valid For Days:" property="appraisalFormValidDate"
  			value="${dealer.preference.appraisalFormValidDate}" size="3"/><br />
  			
  <tag:text title="Appraisal Valid For Miles:" property="appraisalFormValidMileage"
  			value="${dealer.preference.appraisalFormValidMileage}" size="4"/><br /> 
  			
  <tag:select title="Show Options By Default:" property="appraisalFormShowOptions"
  			value="${dealer.preference.appraisalFormShowOptions}" 
			showOptions="${true}">
  	<c:set var="selected">${dealer.preference.appraisalFormShowOptions}</c:set>
    <tag:option value="1" selected="${selected}">True</tag:option>
    <tag:option value="0" selected="${selected}">False</tag:option>
  </tag:select><br/>
  
 
  <tag:select title="Show Check on Appraisal Form:" property="showCheckOnAppraisalForm"
  			value="${dealer.preference.showCheckOnAppraisalForm}" 
			showOptions="${true}">
  	<c:set var="selected">${dealer.preference.showCheckOnAppraisalForm}</c:set>
    <tag:option value="1" selected="${selected}">True</tag:option>
    <tag:option value="0" selected="${selected}">False</tag:option>
  </tag:select><br/> 
  
  <tag:text title="Appraisal Form Memo:" property="appraisalFormMemo"
  			value="${dealer.preference.appraisalFormMemo}" size="50" maxlength="45"/><br />
  			
  <tag:text title="Appraisal Form Discalimer:" property="appraisalFormDisclaimer"
  			value="${dealer.preference.appraisalFormDisclaimer}" size="55" maxlength="180"/><br />
  
			
  <iframe src="/photos/PhotoManagerDisplayAction.go?parentEntityId=${dealer.id}&photoTypeId=3" 
  		  width="350" height="400" name="photoFrame" scrolling="no">
  
</tag:element>
