<%@include file="/include/tag-import.jsp" %>

<script type="text/javascript">
function submitFormRD(pars) {
	// verify not null fields are populated
	if ( $F('internetPriceMappingId') != -1 && $F('lotPriceMappingId') != -1 && $F('internetPriceMappingId') == $F('lotPriceMappingId') ) {
		alert('Internet and Window Sticker DMS fields can not be identical');
		return false;
	}
	var msg = '';
	
	if ( $F('dialUpPhone1') == '' && $F('ipAddress') == '' ) {
		msg += '- Dial UP Phone #1\n';
	}
	if ( $F('login') == '' ) {
		msg += '- Customer ID\n';
	}
	if ( $F('areaNumber') == '' ) {
		msg += '- Area Number\n';
	}
	if ( $F('reynoldsDealerNo') == '' ) {
		msg += '- Dealer No\n';
	}
	if ( $F('storeNumber') == '' ) {
		msg += '- Store Number\n';
	}
	if ( $F('dealerName') == '' ) {
		msg += '- Dealer Name\n';
	}
	if ( $F('dealerPhoneOffice') == '' ) {
		msg += '- Dealer Office Phone #\n';
	}
	if ( $F('dealerPhoneCell') == '' ) {
		msg += '- Dealer Cell Phone #\n';
	}
	if ( $F('dealerEmail') == '' ) {
		msg += '- Dealer Email Addr\n';
	}
	
	if (msg.length > 0) {
		alert('The following fields are required:\n' + msg);
		return false;
	}
	
	new Ajax.Request(	'/fl-admin/dealer/save/dmsWriteback.do', 
						{asynchronous:true, evalScripts:true, method:'post', parameters:pars,
						 onSuccess : function(xhr) 
						 			{
										document.forms[0].submit(); // this submit just toggles the disply
									}
						}
					);
	return false;
}
function submitForm(pars) {

	// verify not null fields are populated
	if ( $F('internetPriceMappingId') != -1 && $F('lotPriceMappingId') != -1 && $F('internetPriceMappingId') == $F('lotPriceMappingId') ) {
		alert('Internet and Window Sticker DMS fields can not be identical');
		return false;
	}
	var msg = '';
	if ( $F('clientSystemId') == '' ) {
		msg += '- Client System ID\n';
	}
	if ( $F('ipAddress') == '' && $F('dialUpPhone1') == '') {
		msg += '- IP Address\n';
	}
	if ( $F('dialUpPhone1') == '' && $F('ipAddress') == '' ) {
		msg += '- Dial UP Phone #1\n';
	}
	if ( $F('login') == '' ) {
		msg += '- Login\n';
	}
	if ( $F('password') == '' ) {
		msg += '- Password\n';
	}
	if ( $F('storeNumber') == '' ) {
		msg += '- Store Number\n';
	}
	if ( $F('dealerName') == '' ) {
		msg += '- Dealer Name\n';
	}
	if ( $F('dealerPhoneOffice') == '' ) {
		msg += '- Dealer Office Phone #\n';
	}
	if ( $F('dealerPhoneCell') == '' ) {
		msg += '- Dealer Cell Phone #\n';
	}
	if ( $F('dealerEmail') == '' ) {
		msg += '- Dealer Email Addr\n';
	}
	
	if (msg.length > 0) {
		alert('The following fields are required:\n' + msg);
		return false;
	}
	
	new Ajax.Request(	'/fl-admin/dealer/save/dmsWriteback.do', 
						{asynchronous:true, evalScripts:true, method:'post', parameters:pars,
						 onSuccess : function(xhr) 
						 			{
										document.forms[0].submit(); // this submit just toggles the disply
									}
						}
					);
	return false;
}

function dmsTypeChange(select) {
	if (select.options[select.selectedIndex].text != null && select.options[select.selectedIndex].text.indexOf("ADP") > 0) {
		document.getElementById("finLogin").style.display = 'block';
		document.getElementById("actLogin").style.display = 'block';
	} else {
		document.getElementById("finLogin").style.display = 'none';
		document.getElementById("actLogin").style.display = 'none';
	}
}


function frequencyChange(select) {
	Element.toggle($('hourOfDay'));
}

function submitNewDMSField() {
	var fieldName = $F('newDMSFieldName');
	if ( fieldName == '' ) {
		alert('Please enter a Field Name');
		return false;
	}
	
	for(i=0; i<document.dealer_dmsWriteback.elements.length; i++)
	{
		if (document.dealer_dmsWriteback.elements[i].name == 'internetPriceMappingId') {
			var opts = document.dealer_dmsWriteback.elements[i].options;
			for (var j = 0; j < opts.length; j++) {
				if (opts[j].text == fieldName) {
					alert('DMS Field Already Exists!');
					return false;
				}
			}
		}
	}
	
	new Ajax.Request(	'/fl-admin/dealer/custom/AddNewDMSField.do?newFieldName=' + fieldName, 
					{asynchronous:true, evalScripts:true, method:'post',
					 onSuccess : function(xhr) {
					 	option1 = document.createElement("OPTION");
						option1.text = xhr.getResponseHeader('newMappingDesc');
						option1.value = xhr.getResponseHeader('newMappingId');
						option1.selected = false;
						$('internetPriceMappingId').options[$('internetPriceMappingId').length] = option1;
					 	option2 = document.createElement("OPTION");
						option2.text = xhr.getResponseHeader('newMappingDesc');
						option2.value = xhr.getResponseHeader('newMappingId');
						option2.selected = false;
						$('lotPriceMappingId').options[$('lotPriceMappingId').length] = option2;
					 	Element.toggle($('newDMSField'));
					 	return;
					 	},
					 onFailure : function(xhr) {
					 	alert('error trying to save new Field name!');
					 	return;
					 	}
					} ); 
}
</script>

<!-- the following is really bad. it finds the proper TPEid based on the dmsType from the tab chosen and what exists in the dealers prefs -->
<!-- SIS can have one of 2 possible values. Changing from one to the other erases the old one. RD has only one value. BUT - a dealer can have  -->
<!-- both SIS and RD. -->
<c:set var="dmsType" value="${param.dmsType}"/>
<c:if test="${empty dmsType}">
	<c:set var="dmsType" value="SIS"/>
</c:if>

<c:forEach items="${dealer.dmsExportPreferences}" var="exportPref">
	<c:if test="${empty exportType}">
		<ref:ref beanName="imt-persist-thirdPartyEntityDao" method="findByDMSExportThirdPartyEntity" scope="request"/>
		<c:forEach items="${values}" var="tpe">
			<c:choose>
				<c:when test="${dmsType == 'SIS'}"> <!-- you can only have one of SIS tpes set -->
					<c:if test="${func:indexOf(tpe.name,'SIS') > -1 && tpe.thirdPartyEntityId == exportPref.dmsExportId}">
						<c:set var="exportType" value="${tpe.thirdPartyEntityId}"/>
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${func:indexOf(tpe.name,'Direct') > -1 && tpe.thirdPartyEntityId == exportPref.dmsExportId}">
						<c:set var="exportType" value="${tpe.thirdPartyEntityId}"/>
					</c:if>
				</c:otherwise>
			</c:choose>
		</c:forEach> 	
	</c:if>
</c:forEach>
<br/>

<c:if test="${empty exportType}">
<!-- default to sis -->
	<ref:ref beanName="imt-persist-thirdPartyEntityDao" method="findByDMSExportThirdPartyEntity" scope="request"/>
	<c:forEach items="${values}" var="tpe">
		<c:if test="${dmsType == 'SIS'}"> <!-- if we are on the SIS tab default to SIS REY REY -->
			<c:if test="${tpe.name == 'SIS - Rey & Rey'}">
				<c:set var="exportType" value="${tpe.thirdPartyEntityId}"/>
			</c:if>
		</c:if>
		<c:if test="${dmsType == 'RD'}"> <!-- if we are on the RD tab default to RD -->	
			<c:if test="${func:indexOf(tpe.name,'Reynolds') > -1}">
				<c:set var="exportType" value="${tpe.thirdPartyEntityId}"/>
			</c:if>
		</c:if>
	</c:forEach> 	
</c:if>

<c:set var="submitForm" value="${dmsType == 'RD' ? 'submitFormRD' : 'submitForm' }" />

<tag:element title="Writeback Settings" id="${dealer.id}" params="exportType=${exportType}&dmsType=${dmsType}"
             section="dealer" element="dmsWriteback" onSubmit="submitSuperUserForm('dealer_dmsWriteback', ${submitForm});return false;"
             formBean="${dealer_dmsWriteback}" >

<c:if test="${isEdit}">
	<br/>SuperUser Password:
	<input type="password" name="suPassword" />
</c:if>

<c:forEach items="${dealer.dmsExportPreferences}" var="exportPref">
	<c:if test="${exportPref.dmsExportId == exportType}">
		<c:set var="dmsPref" value="${exportPref}"/>
	</c:if>
</c:forEach>

<table  width="570px"><tr><td>

<!--  left column -->
<table>
<tr><td>
    <h2>DMS Selection: </h2>
</td></tr><tr><td>
	<tag:checkbox title="Active:" value="${dmsPref.active}" property="isActive" /> 
</td></tr><tr><td>

	<ref:ref beanName="imt-persist-thirdPartyEntityDao" method="findByDMSExportThirdPartyEntity" scope="request"/>
	<c:forEach items="${values}" var="tpe">
		<c:if test="${tpe.thirdPartyEntityId == dmsPref.dmsExportId}">
			<c:set var="selectedDMSName" value="${tpe.name}"/>
		</c:if>
	</c:forEach> 	

 	<tag:select title="DMS Type: " property="exportId" value="${selectedDMSName}" onchange="dmsTypeChange(this);return false;">
	  <c:forEach items="${values}" var="tpe">
	    <c:if test="${dmsType == 'SIS' && tpe.name != 'Reynolds Direct'}"> <!--  sorry - this is lame -->
		    <option value="${tpe.thirdPartyEntityId}" ${dmsPref.dmsExportId == tpe.thirdPartyEntityId ? 'selected=""':''}>
		    	${tpe.name}
		    </option>
	    </c:if>
	    <c:if test="${dmsType == 'RD' && tpe.name == 'Reynolds Direct'}"> <!--  sorry - this is lame -->
		    <option value="${tpe.thirdPartyEntityId}" ${dmsPref.dmsExportId == tpe.thirdPartyEntityId ? 'selected=""':''}>
		      	${tpe.name}
		    </option>
	    </c:if>
	  </c:forEach>
  	</tag:select> 
<c:if test="${dmsType != 'RD'}">	
</td></tr><tr><td>
  	<tag:text title="Client System ID:" property="clientSystemId"  value="${dmsPref.clientSystemId}" size="12"  maxlength="8"/>
</td></tr><tr><td>
  	<tag:text title="DMS IP Address:" property="ipAddress"  value="${dmsPref.ipAddress}" size="12" />

</td></tr><tr><td>
  	<center>OR</center>
 </c:if>
</td></tr><tr><td>
  	<tag:text title="DMS Dialup Ph#1:" property="dialUpPhone1"  value="${dmsPref.dialUpPhone1}" size="12" />
</td></tr><tr><td>
  	<tag:text title="DMS Dialup Ph#2:" property="dialUpPhone2"  value="${dmsPref.dialUpPhone2}" size="12" />
</td></tr><tr><td>
  	<tag:text title="${dmsType != 'RD' ? 'DMS Login:' : 'Customer ID:'}" property="login"  value="${dmsPref.login}" size="12" />
</td></tr><tr><td>
<c:choose>
	<c:when test="${dmsType != 'RD'}">
		<tag:text title="DMS Password:" property="password" size="12" value="${dmsPref.password}"></tag:text>
	</c:when>
	<c:otherwise>
		</td></tr><tr><td>
  		<tag:text title="Area Number:" property="areaNumber"  size="12" value="${dmsPref.areaNumber}"   />
  		</td></tr><tr><td>
  		<tag:text title="Dealer No:" property="reynoldsDealerNo"  size="12" value="${dmsPref.reynoldsDealerNo}"   />
	</c:otherwise>
</c:choose>

</td></tr><tr><td>
  	<tag:text title="Store Number:" property="storeNumber"  size="12" value="${dmsPref.storeNumber}"   />
</td></tr><tr><td>
  	<tag:text title="Branch Number:" property="branchNumber" size="12" value="${dmsPref.branchNumber}" />
</td></tr><tr><td>
  	<tag:select title="Write back frequency" property="frequencyId" value="${dmsPref.frequencyId}" onchange="frequencyChange(this);return false;">
	    <option value="1" <c:if test="${dmsPref.frequencyId == 1}"> selected="selected" </c:if> label="Daily">Daily</option>
	    <option value="2" <c:if test="${dmsPref.frequencyId == 2}"> selected="selected" </c:if> label="Incremental">Incremental</option>
  	</tag:select>  	
  	<span style="font-size:7pt;font-style:italic;">1 = Daily, 2 = Incremental <br/>Incremental writebacks every 10 minutes.<br/>Only vehicles with updated pricing are<br/>written back to the selected DMS.</span>
</td></tr><tr><td>
<div id="hourOfDay" <c:if test="${dmsPref.frequencyId == 2}"> style="display:none;" </c:if>>
  	<tag:select title="Time of Day:" property="dailyExportHourOfDay" value="${dmsPref.dailyExportHourOfDay}">
	    <option value="0" ${dmsPref.dailyExportHourOfDay == 0 ? 'selected=""':'' }>Midnight</option>
	    <option value="1" ${dmsPref.dailyExportHourOfDay == 1 ? 'selected=""':'' }>1 AM</option>
	    <option value="2" ${dmsPref.dailyExportHourOfDay == 2 ? 'selected=""':'' }>2 AM</option>
	    <option value="3" ${dmsPref.dailyExportHourOfDay == 3 ? 'selected=""':'' }>3 AM</option>
	    <option value="4" ${dmsPref.dailyExportHourOfDay == 4 ? 'selected=""':'' }>4 AM</option>
	    <option value="5" ${dmsPref.dailyExportHourOfDay == 5 ? 'selected=""':'' }>5 AM</option>
	    <option value="6" ${dmsPref.dailyExportHourOfDay == 6 ? 'selected=""':'' }>6 AM</option>
	    <option value="7" ${dmsPref.dailyExportHourOfDay == 7 ? 'selected=""':'' }>7 AM</option>
	    <option value="8" ${dmsPref.dailyExportHourOfDay == 8 ? 'selected=""':'' }>8 AM</option>
	    <option value="9" ${dmsPref.dailyExportHourOfDay == 9 ? 'selected=""':'' }>9 AM</option>
	    <option value="10" ${dmsPref.dailyExportHourOfDay == 10 ? 'selected=""':'' }>10 AM</option>
	    <option value="11" ${dmsPref.dailyExportHourOfDay == 11 ? 'selected=""':'' }>11 AM</option>
	    <option value="12" ${dmsPref.dailyExportHourOfDay == 12 ? 'selected=""':'' }>Noon</option>
	    <option value="13" ${dmsPref.dailyExportHourOfDay == 13 ? 'selected=""':'' }>1 PM</option>
	    <option value="14" ${dmsPref.dailyExportHourOfDay == 14 ? 'selected=""':'' }>2 PM</option>
	    <option value="15" ${dmsPref.dailyExportHourOfDay == 15 ? 'selected=""':'' }>3 PM</option>
	    <option value="16" ${dmsPref.dailyExportHourOfDay == 16 ? 'selected=""':'' }>4 PM</option>
	    <option value="17" ${dmsPref.dailyExportHourOfDay == 17 ? 'selected=""':'' }>5 PM</option>
	    <option value="18" ${dmsPref.dailyExportHourOfDay == 18 ? 'selected=""':'' }>6 PM</option>
	    <option value="19" ${dmsPref.dailyExportHourOfDay == 19 ? 'selected=""':'' }>7 PM</option>
	    <option value="20" ${dmsPref.dailyExportHourOfDay == 20 ? 'selected=""':'' }>8 PM</option>
	    <option value="21" ${dmsPref.dailyExportHourOfDay == 21 ? 'selected=""':'' }>9 PM</option>
	    <option value="22" ${dmsPref.dailyExportHourOfDay == 22 ? 'selected=""':'' }>10 PM</option>
	    <option value="23" ${dmsPref.dailyExportHourOfDay == 23 ? 'selected=""':'' }>11 PM</option>
  	</tag:select>
  	<span style="font-size:7pt;font-style:italic;">24hr clock. E.g. 17 = 5pm <br/> Times are in CST.  </span>
</div>  	
</td></tr>
	<tr><td>
		<div id="actLogin" <c:if test="${dmsType != 'RD'}"> style="display:none;" </c:if> >.
	  		<tag:text title="Account Logon" property="accountLogon"  value="${dmsPref.accountLogon}" size="12" />
	  	</div>
	</td></tr>
	<tr><td>
		<div id="finLogin" <c:if test="${dmsType != 'RD'}">style="display:none;" </c:if>>
	  		<tag:text title="Finance Logon" property="financeLogon"  value="${dmsPref.financeLogon}" size="12" />
	  	</div>	
	</td></tr>
</table> 

</td><td>

<!--  Right Column -->
 <table>
 <tr><td>
	<h2>Dealer Contact:</h2>
</td></tr><tr><td>
	<tag:text title="Name:" property="dealerName"  value="${dmsPref.dealerName}" size="21"  />
</td></tr><tr><td>
	<tag:text title="Phone(Office):" property="dealerPhoneOffice"  value="${dmsPref.dealerPhoneOffice}" size="12" maxlength="16" />
</td></tr><tr><td>
	<tag:text title="Phone(Cell):" property="dealerPhoneCell"  value="${dmsPref.dealerPhoneCell}" size="15" maxlength="16" />
</td></tr><tr><td>
	<tag:text title="Email:" property="dealerEmail"  value="${dmsPref.dealerEmail}" size="22" maxlength="200" />
</td></tr><tr><td>
	
	<h2>Mapping</h2>
</td></tr><tr><td>
	<ref:ref beanName="imt-persist-DMSExportPriceMapping" method="findAll" scope="request" var="mappings"/>
	<c:forEach items="${mappings}" var="mapping">
		<c:if test="${mapping.id == dmsPref.internetPricingMapping.id}">
			<c:set var="selectedInternetMappingName" value="${mapping.description}"/>
		</c:if>
		<c:if test="${mapping.id == dmsPref.lotPriceMapping.id}">
			<c:set var="selectedLotPriceMappingName" value="${mapping.description}"/>
		</c:if>
	</c:forEach> 

 	<tag:select nameProperty="internetPrice" title="Internet Price:" property="internetPriceMappingId" value="${selectedInternetMappingName}">
      <option value="-1">None</option>
	  <c:forEach items="${mappings}" var="mapping">
	    <option value="${mapping.id}" ${dmsPref.internetPricingMapping.id == mapping.id ? 'selected=""':'' }>
	      ${mapping.description}
	    </option>
	  </c:forEach>
  	</tag:select> 
</td></tr><tr><td>
 	<tag:select title="Window Sticker Price:" property="lotPriceMappingId" value="${selectedLotPriceMappingName}">
	  <option value="-1">None</option>
	  <c:forEach items="${mappings}" var="mapping">
	    <option value="${mapping.id}" ${dmsPref.lotPriceMapping.id == mapping.id ? 'selected=""':'' }>
	      ${mapping.description}
	    </option>
	  </c:forEach>
  	</tag:select> 
</td></tr><tr><td>
  	
  	<c:if test="${isEdit}">
</td></tr><tr><td>
  		<a href="#" onclick="Element.toggle($('newDMSField'));">Add New DMS Field</a></br></br>
  	</c:if>
	</td></tr><tr><td style="position:relative; ">
	
	<div id="newDMSField" style="display:none;">
	Enter new DMS Field:<br/><input type="text" name="newDMSFieldName" size="16" /><br/>
	<a href="#" onclick="submitNewDMSField();">Submit</a>&nbsp;<a href="#" onclick="Element.toggle($('newDMSField'));">Cancel</a>
	</div>

	<h3>DMS Writeback History</h3>
		not yet implemented :(
 
</td></tr></table>
</td></tr></table>
 


</tag:element>             