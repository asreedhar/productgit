<%@include file="/include/tag-import.jsp" %>

<tag:element title="Used: Purchased" id="${param.id}" 
             section="dealer" element="scorecardUsedPurchased" 
             formBean="${dealer_scorecardUsedPurchased}">
             
  <div style="clear: both">
  <table>
    <tr>
      <td>Win %</td>
      <td>Retail AGP</td>
      <td>Sell Thru %</td>
      <td>Avg Days 2 Retail</td>
      <td>Avg Inv Age</td>
    </tr>
    <tr>
      <td>
        <c:set var="target10"><b:write name="scorecard" property="target10"/></c:set>
	    <tag:text size="2" property="target10" emptyVal="u" value="${target10}"/>
      </td>
      <td>
        <c:set var="target11"><b:write name="scorecard" property="target11"/></c:set>
	    <tag:text size="2" property="target11" emptyVal="u" value="${target11}"/>
      </td>
      <td>
        <c:set var="target12"><b:write name="scorecard" property="target12"/></c:set>
	    <tag:text size="2" property="target12" emptyVal="u" value="${target12}"/>
      </td>
      <td>
        <c:set var="target13"><b:write name="scorecard" property="target13"/></c:set>
	    <tag:text size="2" property="target13" emptyVal="u" value="${target13}"/>
      </td>
      <td>
        <c:set var="target47"><b:write name="scorecard" property="target47"/></c:set>
	    <tag:text size="2" property="target47" emptyVal="u" value="${target47}"/>
      </td>
    </tr>
  </table>
  </div>

</tag:element>