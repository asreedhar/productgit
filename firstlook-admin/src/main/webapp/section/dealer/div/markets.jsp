<%@include file="/include/tag-import.jsp" %>

<h2>Dealer Markets: Ring ${ring}</h2>

<div class="content">
  <c:url var="addMarket" value="/dealer/custom/addMarket.do"/>
  <div class="row">
    <span class="label">Market:</span>
    <span class="formw">
      <proto:formRemote href="${addMarket}" update="markets${ring}" id="addMarketRing${ring}">
        <input type="hidden" name="businessUnitId" value="${businessUnitId}"/>
        <input type="hidden" name="ring" value="${ring}"/>
        <input type="text" size="8" maxlength="5" name="zipcode"/>
        <input type="submit" value="Add Market..."/>
      </proto:formRemote>
    </span>
  </div>
  <c:forEach items="${markets}" var="market">
    <div class="row">
      <span class="label">Market:</span>
      <span class="formw">
        ${market.zipcode}
        <c:url var="removeMarket" value="/dealer/custom/removeMarket.do">
          <c:param name="businessUnitId" value="${market.businessUnitId}"/>
          <c:param name="ring" value="${market.ring}"/>
          <c:param name="zipcode" value="${market.zipcode}"/>
        </c:url>
        <proto:linkRemote href="${removeMarket}" update="markets${ring}"
           confirm="Are you sure you want to remove ${market.zipcode} from ring ${ring}?">
          remove
        </proto:linkRemote>        
      </span>
    </div>
  </c:forEach>
</div>