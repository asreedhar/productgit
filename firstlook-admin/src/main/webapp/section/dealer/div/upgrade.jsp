<%@include file="/include/tag-import.jsp"%>
<ref:single beanName="imt-persist-dealerUpgradeCodeDao"
	idVal="${param.dealerUpgradeCD}" />
<c:if test="${value.id == 11}">
	<h2>Loan Value - Book Value Calculator (${value.description})</h2>
</c:if>
<c:if test="${value.id != 11}">
	<h2>${value.description}</h2>
</c:if>


<!-- NADA UPGRADE STARTS -->
<c:if test="${value.id == 28}">
	<c:set var="idOfSelect" value="NadaUpgradeCheckBox" />
</c:if>
<c:if test="${isNADADealer}">
	<c:set value="if(!checkNadaUpgrade()){return false};" var="onSubmit"/>
</c:if>

<!-- NADA UPGRADE ENDS -->

<c:url var="saveURL" value="/dealer/save/upgrade.do">
	<c:param name="businessUnitId" value="${param.businessUnitId}" />
	<c:param name="dealerUpgradeCD" value="${param.dealerUpgradeCD}" />
	<c:param name="excludes" value="startDate,endDate,active" />
</c:url>

<proto:formRemote href="${saveURL}" id="upgradeForm${param.dealerUpgradeCD}"
	update="dealerUpgrade${param.dealerUpgradeCD}" name="dealer_upgrade">
	<c:set var="org.apache.struts.taglib.html.BEAN"
		value="${dealer_upgrade}" scope="request" />
	<c:if test="${isEdit}">
		<h:hidden property="businessUnitId" value="${businessUnitId}"/>
		<h:hidden property="dealerUpgradeCD" value="${dealerUpgradeCD}"/>
	</c:if>

	<c:choose>
		<c:when test="${isEdit}">
			<span class="formButtons"><img id="indicator" src="../../resources/images/rotating-line-load.gif" style="display: none" alt="loading" />
			<h:submit value="Save"
				onclick="${onSubmit};document.getElementById('indicator').style.display = 'inline';" /> 
				<c:url var="cancelURL" value="/dealer/display/upgrade.do?businessUnitId=${param.businessUnitId}&dealerUpgradeCD=${param.dealerUpgradeCD}" />
				<proto:linkRemote href="${cancelURL}" update="dealerUpgrade${param.dealerUpgradeCD}">Cancel</proto:linkRemote></span>
		</c:when>
		<c:otherwise>
			<c:url var="editURL"
				value="/dealer/edit/upgrade.do?businessUnitId=${param.businessUnitId}&dealerUpgradeCD=${param.dealerUpgradeCD}" />
			<span class="editLink"><proto:linkRemote href="${editURL}"
				update="dealerUpgrade${param.dealerUpgradeCD}">Edit</proto:linkRemote></span>
		</c:otherwise>
	</c:choose>

	<div style="clear: both;"><c:choose>
		
		<c:when test="${(!(empty upgrade) && upgrade.active) || isEdit}">
			<c:choose>
				<c:when test="${param.dealerUpgradeCD == 15 || param.dealerUpgradeCD == 16}">
					<h3><font color="FF0000">CAUTION: Do not set MAX-Live and Max-Test to Active at the same time</font></h3><br>
				</c:when>
			<%-- 	<c:when test="${param.dealerUpgradeCD == 18}">
					<h3><font color="FF0000">Please check the "Markets" tab to set the dealership's PowerRegion</font></h3><br>
				</c:when>   Hiding this code as per the case 31118 Remove JD Power Upgrades from FLAdmin --%>
				
				<c:when test="${param.dealerUpgradeCD == 23}">
					<h3><font color="FF0000">PING II also needs to be set to a minimum of 'Package 5' for MAX for Selling &amp; Email to work</font></h3><br>
				</c:when>
			</c:choose>
			
			<tag:checkbox title="Active?" property="active"
				value="${upgrade.active}" id="${idOfSelect }"
				 />
			<c:set var="startDateVal">
				<f:formatDate value="${upgrade.startDate}" type="date"
					dateStyle="short" />
			</c:set>
			<tag:date id="startDate${param.dealerUpgradeCD}" title="Start Date" property="startDate"
				value="${startDateVal}" />
			<c:set var="endDateVal">
				<f:formatDate value="${upgrade.endDate}" type="date"
					dateStyle="short" />
			</c:set>
			<tag:date id="endDate${param.dealerUpgradeCD}" title="End Date" property="endDate"
				value="${endDateVal}" />
				
				
		</c:when>
		<c:otherwise>
            Upgrade Not Active<br /> 
		</c:otherwise>
	</c:choose></div>
</proto:formRemote>
