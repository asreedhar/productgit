<%@include file="/include/tag-import.jsp" %>

<tag:element title="New Car Performance Targets" id="${param.id}" 
             section="dealer" element="scorecardNewPerformance" 
             formBean="${dealer_scorecardNewPerformance}">

  <div style="clear: both">
  <table width="100%">
    <tr>
      <td/>
      <td>Cpe</td>
      <td>Sdn</td>
      <td>Suv</td>
      <td>Trk</td>
      <td>Van</td>
      <td>Overall</td>
    </tr>
    <tr>
      <td>Percent of Sales:</td>
      <td>
        <c:set var="target29"><b:write name="scorecard" property="target29"/></c:set>
	    <tag:text size="2" property="target29" emptyVal="u" value="${target29}"/>
      </td>
      <td>
        <c:set var="target32"><b:write name="scorecard" property="target32"/></c:set>
	    <tag:text size="2" property="target32" emptyVal="u" value="${target32}"/>
      </td>
      <td>
        <c:set var="target35"><b:write name="scorecard" property="target35"/></c:set>
	    <tag:text size="2" property="target35" emptyVal="u" value="${target35}"/>
      </td>
      <td>
        <c:set var="target38"><b:write name="scorecard" property="target38"/></c:set>
	    <tag:text size="2" property="target38" emptyVal="u" value="${target38}"/>
      </td>
      <td>
        <c:set var="target41"><b:write name="scorecard" property="target41"/></c:set>
	    <tag:text size="2" property="target41" emptyVal="u" value="${target41}"/>
      </td>
    </tr>
    <tr>
      <td>Retail Avg. Gross Sales:</td>
      <td>
        <c:set var="target30"><b:write name="scorecard" property="target30"/></c:set>
	    <tag:text size="2" property="target30" emptyVal="u" value="${target30}"/>
      </td>
      <td>
        <c:set var="target33"><b:write name="scorecard" property="target33"/></c:set>
	    <tag:text size="2" property="target33" emptyVal="u" value="${target33}"/>
      </td>
      <td>
        <c:set var="target36"><b:write name="scorecard" property="target36"/></c:set>
	    <tag:text size="2" property="target36" emptyVal="u" value="${target36}"/>
      </td>
      <td>
        <c:set var="target39"><b:write name="scorecard" property="target39"/></c:set>
	    <tag:text size="2" property="target39" emptyVal="u" value="${target39}"/>
      </td>
      <td>
        <c:set var="target42"><b:write name="scorecard" property="target42"/></c:set>
	    <tag:text size="2" property="target42" emptyVal="u" value="${target42}"/>
      </td>
    </tr>
    <tr>
      <td>Avg. Days2Sale:</td>
      <td>
        <c:set var="target31"><b:write name="scorecard" property="target31"/></c:set>
	    <tag:text size="2" property="target31" emptyVal="u" value="${target31}"/>
      </td>
      <td>
        <c:set var="target34"><b:write name="scorecard" property="target34"/></c:set>
	    <tag:text size="2" property="target34" emptyVal="u" value="${target34}"/>
      </td>
      <td>
        <c:set var="target37"><b:write name="scorecard" property="target37"/></c:set>
	    <tag:text size="2" property="target37" emptyVal="u" value="${target37}"/>
      </td>
      <td>
        <c:set var="target40"><b:write name="scorecard" property="target40"/></c:set>
	    <tag:text size="2" property="target40" emptyVal="u" value="${target40}"/>
      </td>
      <td>
        <c:set var="target43"><b:write name="scorecard" property="target43"/></c:set>
	    <tag:text size="2" property="target43" emptyVal="u" value="${target43}"/>
      </td>
      <td>
        <c:set var="target21"><b:write name="scorecard" property="target21"/></c:set>
	    <tag:text size="2" property="target21" emptyVal="u" value="${target21}"/>
      </td>
    </tr>
  </table>
  </div>

</tag:element>