<%@include file="/include/tag-import.jsp" %>

<tag:element title="Used: Aging Inventory" id="${param.id}" 
             section="dealer" element="scorecardUsedAging" 
             formBean="${dealer_scorecardUsedAging}">

  <div style="clear: both">
  <table width="100%">
    <tr>
      <td width="50"></td>
      <td>Off Whole</td>
      <td>Off Retail</td>
      <td>On Retail</td>
      <td>Appr Retail</td>
    </tr>
    <tr>
      <td>%</td>
      <td>
        <c:set var="target6"><b:write name="scorecard" property="target6"/></c:set>
	    <tag:text size="2" property="target6" emptyVal="u" value="${target6}"/>
      </td>
      <td>
        <c:set var="target7"><b:write name="scorecard" property="target7"/></c:set>
	    <tag:text size="2" property="target7" emptyVal="u" value="${target7}"/>
      </td>
      <td>
        <c:set var="target8"><b:write name="scorecard" property="target8"/></c:set>
	    <tag:text size="2" property="target8" emptyVal="u" value="${target8}"/>
      </td>
      <td>
        <c:set var="target9"><b:write name="scorecard" property="target9"/></c:set>
	    <tag:text size="2" property="target9" emptyVal="u" value="${target9}"/>
      </td>
    </tr>
  </table>
  </div>

</tag:element>