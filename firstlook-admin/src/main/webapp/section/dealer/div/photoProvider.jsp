<%@include file="/include/tag-import.jsp" %>

<h2>Photo Provider Setup </h2>
<div>
<c:url var="addPhotoProvider" value="/dealer/custom/addPhotoProvider.do"/>
<ref:ref beanName="imt-persist-photoProviderDao" scope="request" var="photoProviders"/>
<proto:formRemote href="${addPhotoProvider}" update="photoProvider" id="dealer_photoProvider">
<b>Photo Provider:</b>
<input type="hidden" name="id" value="${dealer.id}"/>
	<select name="photoProviderId">
	<c:forEach items="${photoProviders}" var="provider">
		<option value="${provider.photoProviderId}">	${provider.datafeedCode } </option>
	</c:forEach>
	</select>
	<br/>
	<br/>
<b>Dealer ID:</b> <br/>
	<input type="radio" name="codeType" value="businessUnitCode"> Dealer ID<br/>
	<input type="radio" name="codeType" value="vendorCode" checked> Vendor Code

	<input type="text" name="code" title="test" value="" size="4" /> 
	<br>
	<br>
<b>Photo(URL):</b>
	<select name="urlType">
		<option value="2"> Use Link</option>
		<option value="1"> Get Photo</option>
	</select> 
	<br/>
	<br/>
	<input type="submit" value="Save Provider for Dealership"/>
</proto:formRemote>
</div>
<br/>
<br/>
<ul style="clear:both">
<b>Current Photo Providers</b>
<br>Photo Provider / Dealer Mapping / Photo Source
  <c:forEach items="${addedPhotoProviders}" var="photoProvider">
    <li>
    <c:forEach items="${photoProviders}" var="provider">
		<c:if test="${provider.photoProviderId == photoProvider.photoProviderId }">
			${provider.datafeedCode } /
		</c:if>
	</c:forEach>
    	 ${photoProvider.photoProvidersDealershipCode} /
    	 ${photoProvider.photoStorageCodeOverride == 1 ? 'Get Photo' : 'Use Link'}
      <c:url var="removePhotoProvider" value="/dealer/custom/removePhotoProvider.do">
        <c:param name="id" value="${dealerId}"/>
        <c:param name="photoProviderId" value="${photoProvider.photoProviderId}"/>
      </c:url>
      <proto:linkRemote href="${removePhotoProvider}" update="photoProvider"
         confirm="Are you sure you want to remove  for this dealer?">
        remove
      </proto:linkRemote>
    </li>
  </c:forEach>
</ul>
