<%@include file="/include/tag-import.jsp" %>

<tag:element title="Management Center" id="${dealer.id}" 
             section="dealer" element="managementCenter" 
             formBean="${dealer_managementCenter}">

  <h3>Age Band Targets</h3>
  <table width="100%">
    <tr>
      <td>Band</td>
      <td>0-15</td>
      <td>16-29</td>
      <td>30+9</td>
      <td>40+9</td>
      <td>50+9</td>
      <td>60+</td>
      <td>Total</td>
    </tr>
    <tr>
      <td>Trgt</td>
      <td><tag:text size="2" property="ageBandTarget6" value="${dealer.preference.ageBandTarget6}"/></td>
      <td><tag:text size="2" property="ageBandTarget5" value="${dealer.preference.ageBandTarget5}"/></td>
      <td><tag:text size="2" property="ageBandTarget4" value="${dealer.preference.ageBandTarget4}"/></td>
      <td><tag:text size="2" property="ageBandTarget3" value="${dealer.preference.ageBandTarget3}"/></td>
      <td><tag:text size="2" property="ageBandTarget2" value="${dealer.preference.ageBandTarget2}"/></td>
      <td><tag:text size="2" property="ageBandTarget1" value="${dealer.preference.ageBandTarget1}"/></td>
      <td>100%</td>
    </tr>
  </table>

  <h3>Days Supply</h3>
  <tag:text title="12 Week Weight:" property="daysSupply12WeekWeight" value="${dealer.preference.daysSupply12WeekWeight}"/><br/>
  <tag:text title="26 Week Weight:" property="daysSupply26WeekWeight" value="${dealer.preference.daysSupply26WeekWeight}"/><br/>

</tag:element>
