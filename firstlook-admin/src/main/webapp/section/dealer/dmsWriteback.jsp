<%@include file="/include/tag-import.jsp" %>

<ul id="tablist">
	<li><a href="dmsWriteback.do?id=${dealer.id}&dmsType=SIS">SIS</a></li>
	<li><a href="dmsWriteback.do?id=${dealer.id}&dmsType=RD">Reynolds Direct</a></li>
</ul>
<div class="row">

  <div class="wide-content">
    <div id="dmsWriteback" class="wide-section">
    	 <t:insert page="/dealer/display/dmsWriteback.do"/>
    </div>
</div>

