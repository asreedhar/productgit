<%@include file="/include/tag-import.jsp" %>

<br/>
<div class="row">
  <div class="left-content">
    <div id="dealers" class="section">
      <t:insert page="/group/display/dealers.do"/>
    </div>
  </div>
  <div class="right-content">
	<div id="create" class="section">
	  <jsp:include page="/section/group/div/createDealer.jsp"/>
	</div>
	<div id="add" class="section">
	  <jsp:include page="/section/group/div/addExistingDealer.jsp"/>
	</div>
  </div>
</div>