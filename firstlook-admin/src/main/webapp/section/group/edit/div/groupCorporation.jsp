<%@include file="/include/tag-import.jsp" %>

<h2>Corporation</h2>

<c:url var="saveURL" value="/group/save/groupCorporation.do"/>

<proto:formRemote href="${saveURL}" update="groupCorporation" id="groupCorp">
  <input type="hidden" name="id" value="${group.id}"/>

  <span class="formButtons">
     <input type="submit" value="Save"/>
  </span>

  <ref:ref beanName="imt-persist-corporationDao" scope="request"/>

  <div>
  <label for="corporationId" class="fieldLabel">Corporation:</label>
  <select name="corporationId" width="40" style="font-family: courier">
    <option value="100150">FirstLook</option>
    <c:forEach items="${values}" var="corporation">
      <option value="${corporation.id}"
            <c:if test="${group.corporation.id == corporation.id}">selected</c:if>
      >
        ${corporation.name}
      </option>
    </c:forEach>
  </select>
  </div>            
</proto:formRemote>