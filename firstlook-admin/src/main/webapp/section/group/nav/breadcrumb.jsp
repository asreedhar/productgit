<%@include file="/include/tag-import.jsp"%>

<ul id="crumblist">
	<c:choose>
		<c:when test="${not empty group}">
			<c:choose>
				<c:when test="${not empty group.corporation}">
					<c:choose>
         				<c:when test="${group.corporation.id == 100150}">
         					 <li>CRP: ${group.corporation.name}</li>
        				</c:when>
        				<c:otherwise>
           					<c:url var="corpLink" value="/corporation/dynamic/profile.do">
								<c:param name="id" value="${group.corporation.id}" />
							</c:url>
							<li id="active">CRP: <a href="${corpLink}" id="current"> <str:truncateNicely
							upper="10">${group.corporation.name}</str:truncateNicely> </a></li>
         				</c:otherwise>
      				 </c:choose>
				</c:when>
				<c:otherwise>
					<li>No Corporation</li>
				</c:otherwise>
			</c:choose>
			<c:url var="groupLink" value="/group/dynamic/profile.do">
				<c:param name="id" value="${group.id}" />
			</c:url>
			<li id="active">GRP: <a href="${groupLink}" id="current">${group.name}</a></li>
		</c:when>
		<c:otherwise>
			<li>Group Search Page</li>
		</c:otherwise>
	</c:choose>
</ul>

