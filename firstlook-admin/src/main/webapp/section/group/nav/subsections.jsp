<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>

<ul id="tablist">
  <c:choose>
    <c:when test="${not empty group}">
      <c:url var="profileURL" value="/group/dynamic/profile.do">
        <c:param name="id" value="${group.id}"/>
      </c:url>
      <c:url var="dealersURL" value="/group/dynamic/dealers.do">
        <c:param name="id" value="${group.id}"/>
      </c:url>
      <c:url var="membersURL" value="/group/dynamic/members.do">
        <c:param name="id" value="${group.id}"/>
      </c:url>
      <c:url var="transferPriceURL" value="/group/static/transferPrice/show.do">
        <c:param name="id" value="${group.id}"/>
      </c:url>
      <c:choose>
        <c:when test="${subsectionName == 'profile'}">
          <li id="active">
            <a href="${profileURL}" id="current" accesskey="1">profile</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${profileURL}" accesskey="1">profile</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'dealers'}">
          <li id="active">
            <a href="${dealersURL}" id="current" accesskey="2">dealers</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${dealersURL}" accesskey="2">dealers</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:choose>
        <c:when test="${subsectionName == 'members'}">
          <li id="active">
            <a href="${membersURL}" id="current" accesskey="3">members</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${membersURL}" accesskey="3">members</a> 
          </li>
        </c:otherwise>
      </c:choose>
      <c:if test="${group.preference.includeTransferPricing}">
	      <c:choose>
	        <c:when test="${subsectionName == 'transferPrice'}">
	          <li id="active">
	            <a href="${transferPriceURL}" id="current" accesskey="4">transfer price</a> 
	          </li> 	
	        </c:when>
	        <c:otherwise>
	          <li>
	            <a href="${transferPriceURL}" accesskey="4">transfer price</a> 
	          </li>
	        </c:otherwise>
	      </c:choose>
      </c:if>
    </c:when>
    <c:otherwise>
      <c:url var="groupSearch" value="/group/page/search.do"/>
      <li id="active">
        <a href="${groupSearch}" id="current">search groups</a> 
      </li> 	
    </c:otherwise>
  </c:choose>	
</ul>
			
