<%@include file="/include/tag-import.jsp"%>
<script type="text/javascript">

function hideButtons(form) {
	var _form = form;
	var _formLength = _form.length;
	for(var i=0; i < _formLength; i++) {
		var element = _form.elements[i];
		switch(element.name) {
			case 'submit':
			case 'reset':
				element.style.display = 'none';
				break;
			default:
				break;
		}
	}
}

function showButtons(form) {
	var _form = form;
	var _formLength = _form.length;
	for(var i=0; i < _formLength; i++) {
		var element = _form.elements[i];
		switch(element.name) {
			case 'submit':
			case 'reset':
				var messageElement = getMessageElement(_form);
				messageElement.innerHTML = '';
				element.style.display = 'inline';
				break;
			default:
				break;
		}
	}
}

function saveUnitCostRule(form) {
	var _form = form;
	var containerId = getMessageElement(form).id;
	hideButtons(_form);
	var xhr = 
		new Ajax.Updater({success: containerId}, 'unitCostRule/update.do', {
			asynchronous:true
			, evalScripts:true
 			, parameters:Form.serialize(_form)
 		});
 	return false;
}

function saveRetailOnlyRule(form) {
	var _form = form;
	var containerId = getMessageElement(form).id;
	hideButtons(_form);
	var xhr = 
		new Ajax.Updater({success: containerId}, 'retailOnlyRule/update.do', {
			asynchronous:true
			, evalScripts:true
 			, parameters:Form.serialize(_form)
 		});
 	return false;
}

function saveRange(form) {
	var _form = form;
	var containerId = getMessageElement(form).id;
	var _high = _form.high
	if(_high.value.replace(/^\s+|\s+$/g, '') == '') {
		_high.value = '2147483647'
	}
	var xhr = 
		new Ajax.Updater({success: containerId}, 'inventoryAgeRange/add.do', {
			asynchronous:true
			, evalScripts:true
 			, parameters:Form.serialize(_form)
 		});
 	return false;
}

function getMessageElement(form) {
	var eleId = form.id + '_messages';
	return $(eleId);
}

</script>
<div class="wide">
	<h2>${group.name}</h2>
	
	<fieldset style="float: right;">
		<legend>Inventory Age Range</legend>
		<form id="inventoryAgeRangeForm" 
			  action="/group/static/transferPrice/inventoryAgeRange/add" 
			  method="post"
			  onsubmit="javascript:saveRange(this);return false;">
			<label for="low">Low:</label>
			<input type="text" id="low" name="low" size="4" />
			<label for="high">High:</label>
			<input type="text" id="high" name="high" size="4" />
			<input type="submit" value="add" />
			<input type="hidden" name="groupId" value="${group.id}" />
		</form>
		<p>Only the high value may be left blank for 'Unlimited'.</p>
		<span id="inventoryAgeRangeForm_messages"></span>
	</fieldset>
	
	<table class="datagrid">
		<thead>
			<tr>
				<th mochi:format="str">Dealer</th>
				<th mochi:format="str">Rule</th>
				<th mochi:format="str">Inventory Age Range</th>
			</tr>
		</thead>
		<tbody>
			<c:set var="currDealerId" value="" />
			<c:if test="${func:length(transferPriceRuleBeans) > 0}">
				<c:set var="currDealerId" value="${transferPriceRuleBeans[0].dealerId}" />
			</c:if>
			<c:forEach items="${transferPriceRuleBeans}" var="rule" varStatus="loopStatus">
			<tr>
				<c:if test="${currDealerId ne rule.dealerId or loopStatus.first}">
					<c:url var="dealerProfileURL" value="/dealer/dynamic/profile.do">
						<c:param name="id">${rule.dealerId}</c:param>
					</c:url>
					<th style="border-bottom: thin solid black;" rowspan="4">
						<a href="${dealerProfileURL}">${rule.dealerName}</a>
					</th>
					<c:set var="currDealerId" value="${rule.dealerId}" />
				</c:if>
				<c:set var="borderStyle" value="dotted" />
				<c:if test="${loopStatus.last}"><c:set var="borderStyle" value="solid" /></c:if>
				<c:choose>
					<c:when test="${rule.ruleGroupId eq 0}">
						<td style="border-bottom: thin ${borderStyle} black;">Transfer Price: ${rule.description}</td>
						<td style="border-bottom: thin ${borderStyle} black;">
							<c:set var="ucFormId" value="dealer_transferPriceRuleForm_${rule.dealerId}_${rule.ruleId}" />
							<form action="/group/static/transferPrice/unitCostRule/update" 
								  id="${ucFormId}" 
								  onsubmit="javascript:saveUnitCostRule(this); return false;"
								  onreset="javascript:hideButtons(this);">
								<input type="hidden" name="groupId" value="${group.id}" />
								<input type="hidden" name="dealerId" value="${rule.dealerId}" />
								<input type="hidden" name="transferPriceUnitCostValueId" value="${rule.ruleId}" />
								<select name="transferPriceInventoryAgeRangeId" 
										onchange="javascirpt:showButtons(this.form)">
										<option value="null">Select a range...</option>
									<c:forEach items="${ageRanges}" var="ageRange">
										<c:choose>
											<c:when test="${ageRange.id == rule.transferPriceInventoryAgeRangeId}">
												<option value="${ageRange.id}" selected="selected">${ageRange.description}</option>
											</c:when>
											<c:otherwise>
												<option value="${ageRange.id}">${ageRange.description}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
								<input type="submit" name="submit" value="save" style="display: none;">
								<input type="reset" name="reset" value="reset" style="display: none;">
								<span id="${ucFormId}_messages"></span>
							</form>
						</td>
					</c:when>
					<c:otherwise>
						<td style="border-bottom: thin ${borderStyle} black;">${rule.description}</td>
						<td style="border-bottom: thin ${borderStyle} black;">
							<c:set var="roFormId" value="dealer_transferRetailOnlyRuleForm_${rule.dealerId}_${rule.ruleId}" />
							<form action="/group/static/transferPrice/retailOnlyRule/update" 
								  id="${roFormId}" 
								  onsubmit="javascript:saveRetailOnlyRule(this); return false;"
								  onreset="javascript:hideButtons(this);">
								<input type="hidden" name="groupId" value="${group.id}" />
								<input type="hidden" name="dealerId" value="${rule.dealerId}" />
								<input type="hidden" name="ruleId" value="${rule.ruleId}" />
								<select name="transferPriceInventoryAgeRangeId" 
										onchange="javascirpt:showButtons(this.form)">
										<option value="null">Select a range...</option>
									<c:forEach items="${ageRanges}" var="ageRange">
										<c:choose>
											<c:when test="${ageRange.id == rule.transferPriceInventoryAgeRangeId}">
												<option value="${ageRange.id}" selected="selected">${ageRange.description}</option>
											</c:when>
											<c:otherwise>
												<option value="${ageRange.id}">${ageRange.description}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select><br />
								<input type="submit" name="submit" value="save" style="display: none;">
								<input type="reset" name="reset" value="reset" style="display: none;">
								<span id="${roFormId}_messages"></span>
							</form>
						</td>
					</c:otherwise>
				</c:choose>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	
</div>