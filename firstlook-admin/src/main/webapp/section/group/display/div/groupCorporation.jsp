<tag:element title="Corporation" id="${group.id}" 
             section="group" element="groupCorporation" 
             formBean="${group_groupCorporation}">

  
  <div>
    <label for="corporation" class="fieldLabel">Corporation:</label>
    <span id="corporation">
	  <c:choose>
	    <c:when test="${not empty group.corporation}">
          <c:choose>
            <c:when test="${group.corporation.id == 100150}">
              ${group.corporation.name}
            </c:when>
            <c:otherwise>
              <c:url var="profileURL" value="/corporation/dynamic/profile.do">
                <c:param name="id" value="${group.corporation.id}"/>
              </c:url>
              <a href="${profileURL}">
                ${group.corporation.name}
              </a>
            </c:otherwise>
          </c:choose>
        </c:when>
        <c:otherwise>None</c:otherwise>
      </c:choose>
    </span>
  </div>
</tag:element>