<%@include file="/include/tag-import.jsp" %>
<script type="text/javascript" language="javascript">
function saveGroup() {
	for (i=0; i<document.forms.length; i++) {
		if (i == 0){var path = '/fl-admin/group/save/groupProfile.do'; }
		else {
			var formName =  document.forms[i].name;
			if (formName == ''){ formName = 'group_groupCorporation';}
			var path = createPath(formName);
		}
		var pars = Form.serialize(document.forms[i]);
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}
</script>
<c:set var="action" value="${param.isEdit ? 'edit':'display'}"/>
<c:choose>
	<c:when test="${param.isEdit}">
		<span style="float:right">
			<a href="javascript:saveGroup();">Save All</a>
			<a href="javascript:cancelAll();">Close</a>
		</span>
	</c:when>
	<c:otherwise>
		<span style="float:right">
			<a href="javascript:editAll();">Edit All</a>
		</span>
	</c:otherwise>
</c:choose>
<div class="row">
  <div class="left-content">
    <div id="groupProfile" class="section">
      <t:insert page="/group/${action}/groupProfile.do"/>
    </div>
    <div id="address" class="section">
      <t:insert page="/group/${action}/address.do"/>
    </div>
	<div id="contact" class="section">
	  <t:insert page="/group/${action}/contact.do"/>
	</div>
    <div id="groupCorporation" class="section">
	  <t:insert page="/group/${action}/groupCorporation.do"/>
	</div>
  </div>
  <div class="right-content">

    <div id="preferences" class="section">
      <t:insert page="/group/${action}/preferences.do"/>
    </div>
  </div>
</div>