<%@include file="/include/tag-import.jsp"%>

<h2>Group Members</h2>
<div class="content">

<table id="member_table" class="datagrid">
	<thead>
		<tr>
			<th mochi:format="str">Name</th>
			<th mochi:format="str">Login</th>
			<th mochi:format="str">Type</th>
			<th/>
		</tr>
	</thead>
	<tfoot class="invisible">
		<tr>
			<th mochi:format="str">Name</th>
			<th mochi:format="str">Login</th>
			<th mochi:format="str">Type</th>
			<th/>
		</tr>
	</tfoot>
	<tbody id="member_table_body">
		<c:forEach items="${group.members}" var="member">
			<tr>
				<td>
					<c:url var="memberProfile" value="/member/dynamic/profile.do">
					  <c:param name="id" value="${member.id}"/>
					</c:url>
					<a href="${memberProfile}">
					  ${member.lastName}, ${member.firstName}
					</a>
				</td>
				<td><str:truncateNicely lower="13" upper="17">${member.login}</str:truncateNicely></td>
				<td>${member.memberType}</td>
				<td><c:url
					var="removeUrl" value="/group/custom/removeMember.do">
					<c:param name="id" value="${group.id}" />
					<c:param name="memberId" value="${member.id}" />
				</c:url>
				<c:url var="delete" value="/resources/images/icon_reject_up.gif"/>
				 <proto:linkRemote update="members" href="${removeUrl}"
					confirm="Are you sure you want to remove ${member.firstName} ${member.lastName} from this group">
              <img src="${delete}" border="0"/>
            </proto:linkRemote></td>
			</tr>
		</c:forEach>
		<c:if test="${empty group.members}">
			<tr>
				<td colspan="4">No Matching Results</td>
			</tr>
		</c:if>
	</tbody>
</table>


</div>
	
<c:url var="addMembers" value="/group/custom/addMembers.do"/>
<script type="text/javascript">
 function addMember(element) {
    Element.remove(element);
    new Ajax.Request('${addMembers}', {parameters: 'id=${group.id}&memberId=' + element.id});
  	new Insertion.Top('member_table_body', '<tr id="' + element.id + '"><td>' + element.innerHTML + '</td><td colspan="3">Member Added, please refresh...</td></tr>');
 }
</script>

<%-- used for returning from MemberCreate Ajax.Updater, evalScripts:true --%>
<script type="text/javascript">
	var text = '<p>' + '${createMemberLogin}' + '</p>';
	if (document.getElementById('memberCreateMessage')) {
		Element.update('memberCreateMessage', text );
	}
</script>
