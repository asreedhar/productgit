<%@include file="/include/tag-import.jsp" %>

	<script language="JavaScript">
var keybNumeric = new keybEdit('01234567890','Numeric input only.');
function keybEdit(strValid, strMsg) {
	//	Variables
	var reWork = new RegExp('[a-z]','gi');		//	Regular expression\

	//	Properties
	if(reWork.test(strValid))
		this.valid = strValid.toLowerCase() + strValid.toUpperCase();
	else
		this.valid = strValid;

	if((strMsg == null) || (typeof(strMsg) == 'undefined'))
		this.message = '';
	else
		this.message = strMsg;

	//	Methods
	this.getValid = keybEditGetValid;
	this.getMessage = keybEditGetMessage;
	
	function keybEditGetValid() {
		return this.valid.toString();
	}
	
	function keybEditGetMessage() {
		return this.message;
	}
}

void function validateInput(objForm) {
	objKeyb = keybNumeric
	strWork = objKeyb.getValid();
	strMsg = '';							// Error message
	blnValidChar = false;					// Valid character flag

	// Part 1: Validate input
	if(!blnValidChar)
		for(i=0;i < strWork.length;i++)
			if(window.event.keyCode == strWork.charCodeAt(i)) {
				blnValidChar = true;
				break;
			}

	// Part 2: Build error message
	if(!blnValidChar) {
		if(objKeyb.getMessage().toString().length != 0)
			alert('Error: ' + objKeyb.getMessage());
		window.event.returnValue = false;		// Clear invalid character
		objForm.focus();						// Set focus
	}
}
	</script>
    
    <div id="dealerCreateButton">
      <a href="#" onclick="Element.toggle('dealerCreateButton'); Element.toggle('dealerCreateForm');"/>add new dealer</a>
    </div>
    <div id="dealerCreateForm" class="wide-section" style="display:none;">
      <div id="dealerCreateMessage"></div>
      <h3>Add New Dealer</h3>
      <c:url var="createDealer" value="/group/custom/dealerCreate.do"/>
      <proto:formRemote href="${createDealer}" update="dealers" id="dealerCreate">
        <div class="content">
          <div class="row">
            <br/>
            <input type="hidden" name="id" value="${group.id}"/>
            <table>
              <tr>
                <td>Short Name:</td>
                <td><input type="text" size="15" name="shortName"/></td>	
              </tr>
              <tr>
                <td>Name:</td>
                <td><input type="text" size="15" name="name"/></td>
              </tr>
              <tr>
                <td>Zip Code:</td>
                <td><input onkeypress="javascript:validateInput(this);" type="text" size="15" name="zip" maxlength="5"/></td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                  <input type="submit" value="Add Dealer"/>       
                </td>
              </tr>
            </table>
          </div>
        </div>
      </proto:formRemote>
    </div>
    
    <c:url var="addDealers" value="/group/custom/addDealers.do"/>
	<script>
 function addDealer(element) {
    Element.remove(element);
    new Ajax.Request('${addDealers}', {parameters: 'id=${group.id}&dealerId=' + element.id});
  	new Insertion.Top('dealer_table_body', '<tr id="' + element.id + '"><td>' + element.innerHTML + '</td><td colspan="3">Member Added, please refresh...</td></tr>');
 }
 

   <%-- used for returning from DealerCreate Ajax.Updater, evalScripts:true --%>
	var text = '<p>' + '${createDealer}' + '</p>';
	Element.update('dealerCreateMessage', text );
</script>
