<%@include file="/include/tag-import.jsp" %>


<table id="group_table" class="datagrid">
  <thead>
    <tr>
      <th mochi:format="str">Group Name</th>
      <th mochi:format="str">Nickname</th>
      <th mochi:format="str">Account #</th>
      <th mochi:format="city">City</th>
      <th mochi:format="state">State</th>
    </tr>
  </thead>
  <tfoot class="invisible">
    <tr>
      <th mochi:format="str">Group Name</th>
      <th mochi:format="str">Nickname</th>
      <th mochi:format="str">Account #</th>
      <th mochi:format="city">City</th>
      <th mochi:format="state">State</th>
    </tr>
  </tfoot>
  <tbody>
    <c:forEach items="${results}" var="group">
      <tr>
        <td>
          <c:url var="profileUrl" value="/group/dynamic/profile.do">
		    <c:param name="id" value="${group.id}"/>
          </c:url>
          <a href="${profileUrl}">
            ${group.name}
          </a>
        </td>
        <td>${group.shortName}</td>
        <td>${group.code}</td>
        <td>${group.city}</td>
        <td>${group.state}</td>
      </tr>
    </c:forEach>
    <c:if test="${empty results}">
      <tr>
        <td colspan="5">No Matching Results</td>
      </tr>
    </c:if>
  </tbody>
</table>      

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('group_table'));
</script>
  