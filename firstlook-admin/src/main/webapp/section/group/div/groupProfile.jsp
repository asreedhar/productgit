<%@include file="/include/tag-import.jsp" %>

<tag:element title="Dealer Group Profile" id="${group.id}" 
             section="group" element="groupProfile" 
             formBean="${group_groupProfile}">

  <tag:text title="Name:" property="name" value="${group.name}"/><br/>
  <tag:text title="Short Name:" property="shortName" value="${group.shortName}"/><br/>
  <b>Code:</b> ${group.code}

</tag:element>