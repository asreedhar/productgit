<%@include file="/include/tag-import.jsp" %>

<c:url var="add" value="/resources/images/add.gif"/>

<p>Click <img src="${add}" border="0"/> to add a dealer</p>


<ul id="associateDealers">
  <c:forEach items="${results}" var="dealer">
    <li id="${dealer.id}" login="${dealer.name}">
      <a href="#" 
         onclick="addDealer($('${dealer.id}'));">
        <img src="${add}" border="0"/>
      </a>&nbsp;&nbsp;${dealer.name}</li>
  </c:forEach>
  <c:if test="${empty results}">
    <li>No Matching Dealer</li>
  </c:if>
</ul>

