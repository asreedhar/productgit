<%@include file="/include/tag-import.jsp" %>

<tag:element title="Contact Information" id="${group.id}" 
             section="group" element="contact" 
             formBean="${group_contact}">
      
  <tag:text title="Office Phone:" property="phone" value="${group.phone}"/><br/>
  <tag:text title="Office Fax:" property="fax" value="${group.fax}"/><br/>

</tag:element>