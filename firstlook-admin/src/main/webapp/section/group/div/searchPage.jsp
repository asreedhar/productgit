<%@include file="/include/tag-import.jsp"%>

<script type="text/javascript">
function searchGroup() {
	var searchStr = $F('dealerGroup');
	
	new Ajax.Updater(	'groupSearchResults', 
						'/fl-admin/group/div/search.do?' + searchStr, 
						{asynchronous:true, evalScripts:true, method:'post'} ); 
}
</script>

<h2>Create New Group</h2>

<div id="createButton"><a href="#"
	onclick="Element.toggle('createButton'); Element.toggle('createForm');" />create
group</a></div>
<div id="createForm" style="display:none;">
<c:url var="create"
	value="/group/custom/groupCreate.do" /> 
	<proto:formRemote id="searchPage"
	href="${create}" update="search">
	<center>
	<table>
		<tr>
			<td>Name:</td>
			<td><input type="text" size="15" name="name" /></td>
		</tr>
		<tr>
			<td>Short Name:</td>
			<td><input type="text" size="15" name="shortName" /></td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td><input type="text" size="15" name="phone" /></td>
		</tr>
		<tr>
			<td>Fax:</td>
			<td><input type="text" size="15" name="fax" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Add Group" /></td>
		</tr>
	</table>
	</center>
</proto:formRemote></div>

<h2>Search for a Dealer Group</h2>

Search by name or code: <input type="text" id="dealerGroup" size="30" />
<input type="submit"  onclick="searchGroup();" value="Search" />
<script language="JavaScript" type="text/javascript">
 $('dealerGroup').focus();
</script>

<c:url var="allGroupsURL" value="/group/div/searchAll.do" />
<h3><proto:linkRemote href="${allGroupsURL}"
	update="groupSearchResults">list all groups</proto:linkRemote></h3>
<br />

<div id="groupSearchResults" style="border: 1px solid black;">
<center>
<p>No search results</p>
</center>
</div>
