<%@include file="/include/tag-import.jsp" %>

<h3>Add Existing Member</h3>
<c:url var="addMembers" value="/group/custom/addMembers.do" /> 
  <input type="hidden" name="groupId" value="${group.id}" />
  <p>Search by Name (Last or First): <input type="text" id="memberName" size="30" /></p>
  <SCRIPT language="JavaScript">
    $('memberName').focus(); 
  </SCRIPT>
  <c:url var="memberSearchURL"
	     value="/group/div/associateMemberSearch.do" />
  <proto:observeField href="${memberSearchURL}"
	                  update="memberSearchResults" field="memberName" frequency="1" />
  <div id="memberSearchResults" />
