<%@include file="/include/tag-import.jsp" %>

<tag:element title="Address" id="${group.id}" 
             section="group" element="address" 
             formBean="${group_address}">

<div style="clear: both; padding-left: 15px;">
  <tag:text title="Address 1:" property="street1" value="${group.street1}" maxlength="35"/><br/>
  <tag:text title="Address 2:" property="street2" omitEmpty="${true}" value="${group.street2}" maxlength="35"/><br/>
  <tag:text title="City:" property="city" value="${group.city}" maxlength="20"/>
  <tag:text title="State:" property="state" value="${group.state}" maxlength="2"/>
  <tag:text title="Zip:" property="zip" value="${group.zip}" maxlength="10"/><br/>
</div>

</tag:element>