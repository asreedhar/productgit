<%@include file="/include/tag-import.jsp" %>

<tag:element title="Group Preferences" id="${group.id}"
             section="group" element="preferences"
             formBean="${group_preferences}">
	<tag:text title="Aging Policy:" property="agingPolicy" value="${group.preference.agingPolicy}" size="5"/><br/>
	<tag:text title="Aging Policy Adj:" property="agingPolicyAdjustment" value="${group.preference.agingPolicyAdjustment}" size="5"/><br/>
	<tag:text title="Inventory Exch. Days:" property="inventoryExchangeAgeLimit" value="${group.preference.inventoryExchangeAgeLimit}" size="5"/><br/>
	<tag:text title="Price Point Deals Theshold:" property="pricePointDealsThreshold" value="${group.preference.pricePointDealsThreshold}" size="5"/><br/>
	<tag:checkbox title="Include Round Table?" property="includeRoundTable" value="${group.preference.includeRoundTable}"/><br/>
	<tag:checkbox title="Include CIA:" property="includeCIA" value="${group.preference.includeCIA}"/><br/>
	<tag:checkbox title="Include Red Light Aging Plan:" property="includeRedLightAgingPlan" value="${group.preference.includeRedLightAgingPlan}"/><br/>
	<tag:checkbox title="Include Redistribution?" property="includeRedistribution" value="${group.preference.includeRedistribution}"/><br/>
	<tag:checkbox title="Is Lithia" property="lithiaStore" value="${group.preference.lithiaStore}"/><br/>
	<tag:checkbox title="Enable Command Center" property="includePerformanceManagementCenter" value="${group.preference.includePerformanceManagementCenter}"/><br/>
	
	<h3>Group Appraisals</h3>
	<tag:checkbox title="Show Customer Form Offer To Group?" property="showAppraisalFormOffer" value="${group.preference.showAppraisalFormOffer}"/><br/>
	<tag:checkbox title="Show Appraisal Value To Group?" property="showAppraisalValue" value="${group.preference.showAppraisalValue}"/><br/>
	<h6>Note group appraisals settings will affect ALL stores in the dealer group.</h6>

	<tag:checkbox title="Exclude Wholesale From Days Supply Calculation?" property="excludeWholesaleFromDaysSupply" value="${group.preference.excludeWholesaleFromDaysSupply}"/>
  
  	<tag:checkbox title="Show Group Car Search:" property="showCarSearch" value="${group.preference.showCarSearch}"/><br/>
	<tag:text title="Group Car Search Title:" property="carSearchTitle" value="${group.preference.carSearchTitle}"/><br/>
	<tag:checkbox title="Group has Transfer Pricing?" property="includeTransferPricing" value="${group.preference.includeTransferPricing}"/><br/>
	<tag:checkbox title="Enable In-Group Inventory Bookout Transfer:" property="inventoryBookoutTransfer" value="${group.preference.inventoryBookoutTransfer}"/><br/>
	<h6>Refresh the page after save to see the "transfer price" tab.</h6>
	<h6>Unchecking this will delete the rules for each dealer in group.</h6>
	<h6>Max users need to logout to see this updated properly.</h6>
	<br/>
	
</tag:element>