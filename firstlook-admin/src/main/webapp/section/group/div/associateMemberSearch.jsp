<%@include file="/include/tag-import.jsp" %>

<c:url var="add" value="/resources/images/add.gif"/>

<p>Click <img src="${add}" border="0"/> to add a Member</p>




<ul id="associateMembers">
  <c:forEach items="${results}" var="member">
    <li id="${member.id}" login="${member.login}">
      <a href="#" onclick="addMember($('${member.id}'));"><img src="${add}" border="0"/></a>
      <b>${member.firstName} ${member.lastName}, ${member.login}</b> : ${member.defaultDealerGroupName}  </li>
  </c:forEach>
  <c:if test="${empty results}">
    <li>No Matching Members</li>
  </c:if>
</ul>
