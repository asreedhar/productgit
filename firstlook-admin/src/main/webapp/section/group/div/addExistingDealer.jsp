<%@include file="/include/tag-import.jsp" %>

<h3>Add Existing Dealer</h3>
<c:url var="addDealers" value="/group/custom/addDealers.do" /> 
  <input type="hidden" name="groupId" value="${group.id}" />
  <p>Search by Name: <input type="text" id="dealerName" size="30" /></p>
  <SCRIPT language="JavaScript">
    $('dealerName').focus(); 
  </SCRIPT>
  <c:url var="dealerSearchURL"
	     value="/group/div/associateDealerSearch.do" />
  <proto:observeField href="${dealerSearchURL}"
	                  update="dealerSearchResults" field="dealerName" frequency="1" />
  <div id="dealerSearchResults"></div>
  