<%@include file="/include/tag-import.jsp" %>

<h2>Dealers In Group</h2>

  
<div class="content">
  <table id="dealer_table" class="datagrid">
    <thead>
      <tr>
        <th mochi:format="str">Dealer Name</th>
        <th mochi:format="str">Actions</th>
      </tr>
    </thead>
    <tfoot class="invisible">
      <tr>
        <th mochi:format="str">Dealer Name</th>
        <th mochi:format="str">Actions</th>
      </tr>
    </tfoot>
    <tbody id="dealer_table_body">
      <c:forEach items="${group.dealers}" var="dealer">
        <tr>
          <td>
            <c:url var="profileUrl" value="/dealer/dynamic/profile.do">
		      <c:param name="id" value="${dealer.id}"/>
            </c:url>
            <a href="${profileUrl}">
              ${dealer.name}
            </a>
          </td>
          <td>
            <c:url var="removeUrl" value="/group/custom/removeDealer.do">
			  <c:param name="id" value="${group.id}"/>
		      <c:param name="dealerId" value="${dealer.id}"/>
            </c:url>
			<c:url var="delete" value="/resources/images/icon_reject_up.gif"/>
            <proto:linkRemote update="dealers" href="${removeUrl}" 
                              confirm="Are you sure you want to remove ${dealer.name} from this group">
              <img src="${delete}" border="0"/>
            </proto:linkRemote>
          </td>
        </tr>
      </c:forEach>
      <c:if test="${empty group.dealers}">
        <tr>
          <td colspan="5">No Matching Results</td>
        </tr>
      </c:if>
    </tbody>
  </table> 
</div>
