<%@include file="/include/tag-import.jsp" %>

<tag:element title="Contact Information" id="${corporation.id}" 
             section="corporation" element="contact" 
             formBean="${corporation_contact}">

  <tag:text title="Office Phone:" property="phone" value="${corporation.phone}"/><br/>
  <tag:text title="Office Fax:" property="fax" value="${corporation.fax}"/><br/>

</tag:element>