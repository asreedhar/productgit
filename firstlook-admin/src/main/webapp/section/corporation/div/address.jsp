<%@include file="/include/tag-import.jsp" %>

<tag:element title="Address" id="${corporation.id}" 
             section="corporation" element="address" 
             formBean="${corporation_address}">

<div style="clear: both; padding-left: 15px;">
  <tag:text title="Address 1 :" property="street1" value="${corporation.street1}" maxlength="35" /><br/>
  <tag:text title="Address 2 :" property="street2" omitEmpty="${true}" value="${corporation.street2}" maxlength="35"/><br/>
  <tag:text title="City :" property="city" value="${corporation.city}" maxlength="20"/> 
  <tag:text title="State :" property="state" value="${corporation.state}" maxlength="2"/>
  <tag:text title="Zip :" property="zip" value="${corporation.zip}" maxlength="10"/><br/>
</div>
  
</tag:element>
