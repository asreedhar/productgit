<%@include file="/include/tag-import.jsp" %>

<tag:element title="Corporation Profile" id="${corporation.id}" 
             section="corporation" element="profile" 
             formBean="${corporation_profile}">

  <tag:text title="Name:" property="name" value="${corporation.name}"/><br/>
  <tag:text title="Short Name:" property="shortName" value="${corporation.shortName}"/><br/>
  <tag:text title="Code:" property="code" value="${corporation.code}"  readOnly="true"/><br/>

</tag:element>