<%@include file="/include/tag-import.jsp" %>


<table id="corporation_table" class="datagrid">
  <thead>
    <tr>
      <th mochi:format="str">corporation Name</th>
      <th mochi:format="str">Nickname</th>
      <th mochi:format="str">Account #</th>
      <th mochi:format="city">City</th>
      <th mochi:format="state">State</th>
    </tr>
  </thead>
  <tfoot class="invisible">
    <tr>
      <th mochi:format="str">corporation Name</th>
      <th mochi:format="str">Nickname</th>
      <th mochi:format="str">Account #</th>
      <th mochi:format="city">City</th>
      <th mochi:format="state">State</th>
    </tr>
  </tfoot>
  <tbody>
    <c:forEach items="${results}" var="corporation">
      <tr>
        <td>
          <c:url var="profileUrl" value="/corporation/dynamic/profile.do">
		    <c:param name="id" value="${corporation.id}"/>
          </c:url>
          <a href="${profileUrl}">
            ${corporation.name}
          </a>
        </td>
        <td>${corporation.shortName}</td>
        <td>${corporation.code}</td>
        <td>${corporation.city}</td>
        <td>${corporation.state}</td>
      </tr>
    </c:forEach>
    <c:if test="${empty results}">
      <tr>
        <td colspan="5">No Matching Results</td>
      </tr>
    </c:if>
  </tbody>
</table>

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('corporation_table'));
</script>
        