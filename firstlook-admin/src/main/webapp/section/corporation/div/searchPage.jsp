<%@include file="/include/tag-import.jsp" %>

<script type="text/javascript">
function searchCorp() {
	var searchStr = $F('dealercorporation');
	
	new Ajax.Updater(	'corporationSearchResults', 
						'/fl-admin/corporation/div/search.do?' + searchStr, 
						{asynchronous:true, evalScripts:true, method:'post'} ); 
}
</script>

<h2>Create New Corporation</h2>

<div id="createButton"><a href="#"
	onclick="Element.toggle('createButton'); Element.toggle('createForm');" />create
corporation</a></div>
<div id="createForm" style="display:none;">
<c:url var="create"
	value="/corporation/custom/corporationCreate.do" /> 
	<proto:formRemote id="searchPage"
	href="${create}" update="search">
	<center>
	<table>
		<tr>
			<td>Name:</td>
			<td><input type="text" size="15" name="name" /></td>
		</tr>
		<tr>
			<td>Short Name:</td>
			<td><input type="text" size="15" name="shortName" /></td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td><input type="text" size="15" name="phone" /></td>
		</tr>
		<tr>
			<td>Fax:</td>
			<td><input type="text" size="15" name="fax" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Add Corporation" /></td>
		</tr>
	</table>
	</center>
</proto:formRemote></div>

<h2>Search for a Corporation</h2>

Search by name or code: <input type="text" id="dealercorporation" size="30"/>
<input type="submit"  onclick="searchCorp();" value="Search" />

<SCRIPT language="JavaScript">
 $('dealercorporation').focus();
</SCRIPT>

<c:url var="allCorporationsURL" value="/corporation/div/searchAll.do"/>
<h3><proto:linkRemote href="${allCorporationsURL}" update="corporationSearchResults">list all corporations</proto:linkRemote></h3>
<br/>

<div id="corporationSearchResults" style="border: 1px solid black;">
 <center>
   <p>No search results</p>
 </center>
</div>

