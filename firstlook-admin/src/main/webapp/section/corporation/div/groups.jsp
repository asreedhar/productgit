<%@include file="/include/tag-import.jsp" %>

<h2>Groups In Corporation</h2>

<div class="content">
  <div class="row">
    <div id="groupCreateButton">
      <a href="#" onclick="Element.toggle('groupCreateButton'); Element.toggle('groupCreateForm');"/>add new group</a>
    </div>
    <div id="groupCreateForm" class="wide-section" style="display:none;">
      <h3>Add New Group</h3>
      <c:url var="createGroup" value="/corporation/custom/groupCreate.do"/>
      <proto:formRemote href="${createGroup}" update="groups" name="groupsForm" id="groupsForm">
        <div class="content">
          <div class="row">
            <br/>
            <input type="hidden" name="id" value="${corporation.id}"/>
            code: <input type="text" size="15" name="code"/>&nbsp;&nbsp;
            short name: <input type="text" size="15" name="shortName"/>&nbsp;&nbsp;
            name: <input type="text" size="15" name="name"/>&nbsp;&nbsp;
            <input type="submit" value="Add Group"/>       
          </div>
        </div>
      </proto:formRemote>
    </div>
  </div>
<br/>
    <div class="row">
    <div id="addGroupButton">
      <a href="#" onclick="Element.toggle('addGroupButton'); Element.toggle('addGroupForm');"/>add existing groups to corporation</a>
    </div>
    <div id="addGroupForm" class="wide-section" style="display:none;">
      <h3>Add Existing Groups to Corporation</h3>
          <c:url var="addGroups" value="/corporation/custom/addGroups.do"/>
          <proto:formRemote href="${addGroups}" update="groups" id="addGroups">
            <div class="nested-link">
              <input type="submit" value="ADD SELECTED GROUPS"/>
            </div>
            <input type="hidden" name="id" value="${corporation.id}"/>
            <ref:ref beanName="imt-persist-dealerGroupDao" scope="request"/>
            <select name="dealerGroupIds" multiple size="5" width="40" style="font-family: courier">
              <c:forEach items="${values}" var="group">
                <option value="${group.id}">
                  ${group.name}
                </option>
              </c:forEach>
            </select>            
          </proto:formRemote>
    </div>
  </div>
  <br/>
  
<div class="content">
  <table id="group_table" class="datagrid">
    <thead>
      <tr>
        <th mochi:format="str">Group Name</th>
        <th mochi:format="str">Nickname</th>
        <th mochi:format="str">Account #</th>
        <th mochi:format="str">Actions</th>
      </tr>
    </thead>
    <tfoot class="invisible">
      <tr>
        <th mochi:format="str">Group Name</th>
        <th mochi:format="str">Nickname</th>
        <th mochi:format="str">Account #</th>
        <th mochi:format="str">Actions</th>
      </tr>
    </tfoot>
    <tbody>
      <c:forEach items="${corporation.groups}" var="group">
        <tr>
          <td>
            <c:url var="profileUrl" value="/group/dynamic/profile.do">
		      <c:param name="id" value="${group.id}"/>
            </c:url>
            <a href="${profileUrl}">
              ${group.name}
            </a>
          </td>
          <td>${group.shortName}</td>
          <td>${group.code}</td>
          <td>
            <c:url var="removeUrl" value="/corporation/custom/removeGroup.do">
			  <c:param name="id" value="${corporation.id}"/>
		      <c:param name="dealerGroupId" value="${group.id}"/>
            </c:url>
            <proto:linkRemote update="groups" href="${removeUrl}" 
                              confirm="Are you sure you want to remove ${group.name} from this corporation">
              remove from corporation
            </proto:linkRemote>
          </td>
        </tr>
      </c:forEach>
      <c:if test="${empty corporation.groups}">
        <tr>
          <td colspan="5">No Matching Results</td>
        </tr>
      </c:if>
    </tbody>
  </table> 
</div>

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('group_table'));
</script>
