<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>

<ul id="tablist">
  <c:choose>
    <c:when test="${not empty corporation}">
      <c:url var="profileURL" value="/corporation/dynamic/profile.do">
        <c:param name="id" value="${corporation.id}"/>
      </c:url>
      <c:choose>
        <c:when test="${subsectionName == 'profile'}">
          <li id="active">
            <a href="${profileURL}" id="current" accesskey="1">profile</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${profileURL}" accesskey="1">profile</a> 
          </li>
        </c:otherwise>
      </c:choose>
            
      <c:url var="groupsURL" value="/corporation/dynamic/groups.do">
        <c:param name="id" value="${corporation.id}"/>
      </c:url>
      <c:choose>
        <c:when test="${subsectionName == 'groups'}">
          <li id="active">
            <a href="${groupsURL}" id="current" accesskey="2">groups</a> 
          </li> 	
        </c:when>
        <c:otherwise>
          <li>
            <a href="${groupsURL}" accesskey="2">groups</a> 
          </li>
        </c:otherwise>
      </c:choose>

    </c:when>
    <c:otherwise>
      <c:url var="corporationSearch" value="/corporation/page/search.do"/>
      <li id="active">
        <a href="${corporationSearch}" id="current">search corporations</a> 
      </li> 	
    </c:otherwise>
  </c:choose>	
</ul>
