<%@include file="/include/tag-import.jsp" %>

<ul id="crumblist">
  <c:choose>
    <c:when test="${not empty corporation}">
       <c:choose>
         <c:when test="${corporation.id == 100150}">
          <li>CRP: ${corporation.name}</li>
         </c:when>
         <c:otherwise>
           <c:url var="profileURL" value="/corporation/dynamic/profile.do">
             <c:param name="id" value="${corporation.id}"/>
           </c:url>
           <li>CRP: <a href="${profileURL}">${corporation.name}</a></li>
         </c:otherwise>
       </c:choose>
    </c:when>
    <c:otherwise>
      <li>Corporation Search Page</li>
    </c:otherwise>
  </c:choose>
</ul>

