<c:url var="profileURL" value="/corporation/dynamic/profile.do">
  <c:param name="id" value="${corporation.id}"/>
</c:url>

<table cellspacing="0" align="center" class="NavBar">
	<tr valign="top">
		<td>
			<p class="topnavi">
				CORPORATION: ${corporation.name}: 
				<b>
				<a href="${profileURL}">profile</a> <span style="color: #ccc;">&bull;</span> 				

				</b>
			</p>	
				
		</td>
		<td align="right">
		</td>	
	</tr>
</table>
