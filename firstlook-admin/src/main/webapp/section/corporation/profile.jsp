<%@include file="/include/tag-import.jsp" %>

<div class="row">
  <div class="left-content">
    <div id="profile" class="section">
      <t:insert page="/corporation/display/profile.do"/>
    </div>
    <br/>
    <div id="address" class="section">
      <t:insert page="/corporation/display/address.do"/>
    </div>
  </div>
  <div class="right-content">
    <div id="contact" class="section">
      <t:insert page="/corporation/display/contact.do"/>
    </div>
  </div>
</div>