<%@include file="/include/tag-import.jsp" %>

<h2>Export Settings</h2>

<div class="row">
	<ul id="tablist">
		<li><a href="exportSettings.do?exportType=eBiz">eBiz</a></li>
		<li><a href="exportSettings.do?exportType=SIS">SIS</a></li>
		<li><a href="exportSettings.do?exportType=ReyDirect">Reynolds Direct</a></li>
	</ul>
	<t:insert page="/system/display/loadExportSettings.do?type=${param.exportType}"/>
</div>