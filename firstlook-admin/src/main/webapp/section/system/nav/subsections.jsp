<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>

<c:url var="regionsURL" value="/system/page/regions.do"/>
<c:url var="marketSupressionURL" value="/system/page/marketSupression.do"/>
<c:url var="adminURL" value="/system/page/admin.do"/>
<c:url var="exportSettingsURL" value="/system/page/exportSettings.do">
	<c:param name="exportType" value="eBiz"/>
</c:url>
<c:url var="pmrURL" value="/system/display/pmrSettings.do"/>
<c:url var="PingIIURL" value="/system/page/pingIISettings.do">
	 <c:param name="dealerId" value="100150"/>
</c:url>
<c:url var="cpoURL" value="../../../CertifiedProgram/Admin/Pages/ManageCertifiedPrograms.aspx" />
<c:url var="pgwURL" value="/../promotions/action/list">
	 <c:param name="method" value="index"/>
</c:url>

<ul id="tablist">
  <c:choose>
    <c:when test="${subsectionName == 'regions'}">
       <li id="active">
         <a href="${regionsURL}" id="current" accesskey="1">regions</a> 
       </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${regionsURL}" accesskey="1">regions</a> 
      </li>
    </c:otherwise>
  </c:choose>

  <c:choose>
    <c:when test="${subsectionName == 'marketSupression'}">
       <li id="active">
         <a href="${marketSupressionURL}" id="current" accesskey="2">marketSupression</a> 
       </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${marketSupressionURL}" accesskey="2">marketSupression</a> 
      </li>
    </c:otherwise>
  </c:choose>

  <c:choose>
    <c:when test="${subsectionName == 'admin'}">
       <li id="active">
         <a href="${adminURL}" id="current" accesskey="3">admin</a> 
       </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${adminURL}" accesskey="3">admin</a> 
      </li>
    </c:otherwise>
  </c:choose>
  
  
  <c:choose>
    <c:when test="${subsectionName == 'exportSettings'}">
       <li id="active">
         <a href="${exportSettingsURL}" id="current" accesskey="4">Export Settings</a> 
       </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${exportSettingsURL}" accesskey="4">Export Settings</a> 
      </li>
    </c:otherwise>
  </c:choose>

      <li>
        <a target="ReportCenter" href="${pmrURL}" accesskey="4">Reporting Center</a> 
      </li>
  
  <c:choose>
    <c:when test="${subsectionName == 'PingII'}">
       <li id="active">
         <a href="${PingIIURL}" id="current" accesskey="5">Ping II System Settings</a> 
       </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${PingIIURL}" accesskey="5">Ping II System Settings</a> 
      </li>
    </c:otherwise>
  </c:choose>
  <li>
  	<a target="CertifiedProgram" href="${cpoURL}" accesskey="6">CPO</a>
  </li>
  <li>
  	<a target="_blank" href="${pgwURL}" accesskey="7">Promotions Gateway</a>
  </li>
</ul>
			
<div id="subsections">
    <ul>
    </ul> 				
</div>
