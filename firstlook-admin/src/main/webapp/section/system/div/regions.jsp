<%@include file="/include/tag-import.jsp" %>

<c:url var="dojoPath" value="/resources/dojo"/>
<script src="${dojoPath}/dojo.js"></script>
<script src="${dojoPath}/src/widget/__package__.js"></script>
<script src="${dojoPath}/src/widget/FloatingPane.js"></script>
<script src="${dojoPath}/src/widget/ResizeHandle.js"></script>

<c:url var="addZipCodeUrl" value="/system/custom/addZipCode.do"/>
<c:url var="removeZipCodeUrl" value="/system/custom/removeZipCode.do"/>


<script type="text/javascript">
  function showFloat(node,title) {
  	
  
	  var floatingPane = dojo.widget.fromScript("FloatingPane", 
	      {
	      	style:"width: 300px; height: 500px;",
	      	resizable:true,
	      	title:title,
	      	constrainToContainer:false,
	      	displayCloseAction:true
	      },  node);
	  floatingPane.show();
  }
  
  function addZipCode(regionId, zipcode) {
      new Ajax.Request('${addZipCodeUrl}', {parameters: 'regionId=' + regionId + '&zipCode=' + zipcode});
  	  markup = '<span id="region' + regionId + '-' + zipcode + '">' + zipcode + '<a href="#" onclick="Element.remove(\'region' + regionId + '-' + zipcode + '\'\);">delete</a></span>';
      new Insertion.Bottom('region' + regionId + '-zipcodes', markup );
  }
	  
</script>

<h2>First Look Regions</h2>

<div class="content">

  <div class="row">
    <div id="regionCreateButton">
      <a href="#" onclick="Element.toggle('regionCreateButton'); Element.toggle('regionCreateForm');"/>add new region</a>
    </div>
    <div id="regionCreateForm" class="wide-section" style="display:none;">
      <div class="nested-title">
        Add New Region
      </div>
      <c:url var="createregion" value="/system/custom/regionCreate.do"/>
      <proto:formRemote href="${createregion}" update="regions" id="sysRegions">
        <div class="nested-link">
          <input type="submit" value="ADD"/>
        </div>
        <div class="content">
          <div class="row">
            <br/>
            Name: <input type="text" size="15" name="name"/>
          </div>
        </div>
      </proto:formRemote>
    </div>

  </div>
  <br/>
  <table id="region_table" class="datagrid">
    <thead>
      <tr>
        <th mochi:format="str">Name</th>
        <th mochi:format="str">Actions</th>
      </tr>
    </thead>
    <tfoot class="invisible">
      <tr>
        <th mochi:format="str">Name</th>
        <th mochi:format="str">Actions</th>
      </tr>
    </tfoot>

    <tbody>
      <c:forEach items="${regions}" var="region">
        <tr>
          <td>
            ${region.name}
          </td>
          <td>
            <a href="#" onclick="showFloat($('region${region.id}'),'${region.name} Detail')">edit zipcodes</a>
            &nbsp;&nbsp;
            &nbsp;&nbsp;
            &nbsp;&nbsp;
            <c:url var="removeUrl" value="/system/custom/removeRegion.do">
			  <c:param name="id" value="${region.id}"/>
            </c:url>
            <proto:linkRemote update="regions" href="${removeUrl}" 
                              confirm="Are you sure you want to remove region ${region.name}?">
              remove region
            </proto:linkRemote>          
          </td>
        </tr>

        
      </c:forEach>
    </tbody>

</table> 

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('region_table'));
</script>


      <c:forEach items="${regions}" var="region">
        <div id="region${region.id}" style="display:none; width: 200px; height: 500px;">
          <p>Zip Codes:</p>
          <input id="zipcode-${region.id}" type="text" size="15"><br/>
          <input type="submit" value="  ADD ZIP CODE  " 
                 onclick="addZipCode( '${region.id}', $F('zipcode-${region.id}') );"/><br/>
          <p id="region${region.id}-zipcodes">
            <c:forEach items="${region.zipCodes}" var="zipCode">
              <span id="region${region.id}-${zipCode}">
                <nobr>${zipCode} 
                <a href="#" onclick="new Ajax.Request('${removeZipCodeUrl}', {parameters: 'regionId=${region.id}&zipCode=${zipCode}'}); Element.remove('region${region.id}-${zipCode}');">delete</a></nobr>
              </span>
            </c:forEach>
          </p>	
        </div>
      </c:forEach>

</div>


