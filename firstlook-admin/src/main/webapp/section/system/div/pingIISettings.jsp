<%@include file="/include/tag-import.jsp" %>

<c:url var="validation" value="/resources/js/bucketRanges.js"/>
<script src="${validation}" type="text/javascript"></script>

<script type="text/javascript">
function submitForm(){
	if( parseInt($F('defaultSearchRadius')) > 100 || parseInt($F('defaultSearchRadius')) == -1 || parseInt($F('maxSearchRadius')) > 100 || parseInt($F('maxSearchRadius') == -1 )) {
		if(!confirm('Increasing the search to this radius may negatively impact system performance for this dealer.  You should understand the dealership, their market, their general geographic area, and top selling brands before making this change.'))
			return false;
	}
	
	if( parseInt($F('defaultSearchRadius')) > parseInt($F('maxSearchRadius')) ) {
		alert('The max search radius must be greater than the default radius. Please modify and resubmit');
		return false;
	}
	
	
	var pars = Form.serialize(document.forms['pingIIForm']); 
	new Ajax.Updater(	'pingIISystemPrefs', 
						'/fl-admin/system/save/pingIISettings.do?dealerId=100150', 
						{asynchronous:true, evalScripts:true, method:'post', parameters:pars} ); 
	
	return false;
}
</script>

<div id="pingIISystemPrefs">

<h1><a href="/pricing/Pages/Internet/ChooseSeller.aspx" target="_blank"> Go To PingII Sales Tool </a></h1>
<br/><br/>

<c:set var="age_range1"><b:write name="age_ranges" property="range1"/></c:set> 
<c:set var="age_range1MinMarket"><b:write name="age_ranges" property="range1Value1"/></c:set> 
<c:set var="age_range1MaxMarket"><b:write name="age_ranges" property="range1Value2"/></c:set> 
<c:set var="age_range1MinGross"><b:write name="age_ranges" property="range1Value3"/></c:set> 
<c:set var="age_range1High"><b:write name="age_ranges" property="range1High"/></c:set> 
<c:set var="age_range2"><b:write name="age_ranges" property="range2"/></c:set>
<c:set var="age_range2MinMarket"><b:write name="age_ranges" property="range2Value1"/></c:set>
<c:set var="age_range2MaxMarket"><b:write name="age_ranges" property="range2Value2"/></c:set>
<c:set var="age_range2MinGross"><b:write name="age_ranges" property="range2Value3"/></c:set>
<c:set var="age_range2Low"><b:write name="age_ranges" property="range2Low"/></c:set> 
<c:set var="age_range2High"><b:write name="age_ranges" property="range2High"/></c:set> 
<c:set var="age_range3"><b:write name="age_ranges" property="range3"/></c:set>
<c:set var="age_range3MinMarket"><b:write name="age_ranges" property="range3Value1"/></c:set>
<c:set var="age_range3MaxMarket"><b:write name="age_ranges" property="range3Value2"/></c:set>
<c:set var="age_range3MinGross"><b:write name="age_ranges" property="range3Value3"/></c:set>
<c:set var="age_range3Low"><b:write name="age_ranges" property="range3Low"/></c:set> 
<c:set var="age_range3High"><b:write name="age_ranges" property="range3High"/></c:set> 
<c:set var="age_range4"><b:write name="age_ranges" property="range4"/></c:set>
<c:set var="age_range4MinMarket"><b:write name="age_ranges" property="range4Value1"/></c:set>
<c:set var="age_range4MaxMarket"><b:write name="age_ranges" property="range4Value2"/></c:set>
<c:set var="age_range4MinGross"><b:write name="age_ranges" property="range4Value3"/></c:set>
<c:set var="age_range4Low"><b:write name="age_ranges" property="range4Low"/></c:set> 
<c:set var="age_range4High"><b:write name="age_ranges" property="range4High"/></c:set> 
<c:set var="age_range5"><b:write name="age_ranges" property="range5"/></c:set>
<c:set var="age_range5MinMarket"><b:write name="age_ranges" property="range5Value1"/></c:set>
<c:set var="age_range5MaxMarket"><b:write name="age_ranges" property="range5Value2"/></c:set>
<c:set var="age_range5MinGross"><b:write name="age_ranges" property="range5Value3"/></c:set>
<c:set var="age_range5Low"><b:write name="age_ranges" property="range5Low"/></c:set> 
<c:set var="age_range5High"><b:write name="age_ranges" property="range5High"/></c:set>
<c:set var="age_range6"><b:write name="age_ranges" property="range6"/></c:set>
<c:set var="age_range6MinMarket"><b:write name="age_ranges" property="range6Value1"/></c:set>
<c:set var="age_range6MaxMarket"><b:write name="age_ranges" property="range6Value2"/></c:set>
<c:set var="age_range6MinGross"><b:write name="age_ranges" property="range6Value3"/></c:set>
<c:set var="age_range6Low"><b:write name="age_ranges" property="range6Low"/></c:set> 
<c:set var="age_range6High"><b:write name="age_ranges" property="range6High"/></c:set>
<c:set var="mileage_range1"><b:write name="age_ranges" property="range1"/></c:set> 
<c:set var="mileage_range1LowAdj"><b:write name="mileage_ranges" property="range1Value1"/></c:set> 
<c:set var="mileage_range1HighAdj"><b:write name="mileage_ranges" property="range1Value2"/></c:set> 
<c:set var="mileage_range1High"><b:write name="mileage_ranges" property="range1High"/></c:set> 
<c:set var="mileage_range2"><b:write name="mileage_ranges" property="range2"/></c:set>
<c:set var="mileage_range2LowAdj"><b:write name="mileage_ranges" property="range2Value1"/></c:set>
<c:set var="mileage_range2HighAdj"><b:write name="mileage_ranges" property="range2Value2"/></c:set>
<c:set var="mileage_range2Low"><b:write name="mileage_ranges" property="range2Low"/></c:set> 
<c:set var="mileage_range2High"><b:write name="mileage_ranges" property="range2High"/></c:set> 
<c:set var="mileage_range3"><b:write name="mileage_ranges" property="range3"/></c:set>
<c:set var="mileage_range3LowAdj"><b:write name="mileage_ranges" property="range3Value1"/></c:set>
<c:set var="mileage_range3HighAdj"><b:write name="mileage_ranges" property="range3Value2"/></c:set>
<c:set var="mileage_range3Low"><b:write name="mileage_ranges" property="range3Low"/></c:set> 
<c:set var="mileage_range3High"><b:write name="mileage_ranges" property="range3High"/></c:set> 
<c:set var="mileage_range4"><b:write name="mileage_ranges" property="range4"/></c:set>
<c:set var="mileage_range4LowAdj"><b:write name="mileage_ranges" property="range4Value1"/></c:set>
<c:set var="mileage_range4HighAdj"><b:write name="mileage_ranges" property="range4Value2"/></c:set>
<c:set var="mileage_range4Low"><b:write name="mileage_ranges" property="range4Low"/></c:set> 
<c:set var="mileage_range4High"><b:write name="mileage_ranges" property="range4High"/></c:set> 
<c:set var="mileage_range5"><b:write name="mileage_ranges" property="range5"/></c:set>
<c:set var="mileage_range5LowAdj"><b:write name="mileage_ranges" property="range5Value1"/></c:set>
<c:set var="mileage_range5HighAdj"><b:write name="mileage_ranges" property="range5Value2"/></c:set>
<c:set var="mileage_range5Low"><b:write name="mileage_ranges" property="range5Low"/></c:set> 
<c:set var="mileage_range5High"><b:write name="mileage_ranges" property="range5High"/></c:set>
<c:set var="mileage_range6"><b:write name="mileage_ranges" property="range6"/></c:set>
<c:set var="mileage_range6LowAdj"><b:write name="mileage_ranges" property="range6Value1"/></c:set>
<c:set var="mileage_range6HighAdj"><b:write name="mileage_ranges" property="range6Value2"/></c:set>
<c:set var="mileage_range6Low"><b:write name="mileage_ranges" property="range6Low"/></c:set> 
<c:set var="mileage_range6High"><b:write name="mileage_ranges" property="range6High"/></c:set>
<c:set var="mileage_range7"><b:write name="mileage_ranges" property="range7"/></c:set>
<c:set var="mileage_range7LowAdj"><b:write name="mileage_ranges" property="range7Value1"/></c:set>
<c:set var="mileage_range7HighAdj"><b:write name="mileage_ranges" property="range7Value2"/></c:set>
<c:set var="mileage_range7Low"><b:write name="mileage_ranges" property="range7Low"/></c:set> 
<c:set var="mileage_range7High"><b:write name="mileage_ranges" property="range7High"/></c:set>

<!--///////////////// NON-EDITABLE MODE //////////////-->
<c:if test="${!isEdit}">
	<h1>Ping II System Default Settings</h1>
	<h3>
		<a onclick="new Ajax.Updater('pingIISystemPrefs', '/fl-admin/system/edit/pingIISettings.do?state=edit&dealerId=100150', 
			{asynchronous:true, evalScripts:true } ); return false;" href="#" >edit</a>
	</h3><br/>
	Default Search Radius: ${preference.pingII_DefaultSearchRadius} mi<br/>
	Max Search Radius: ${preference.pingII_MaxSearchRadius} mi<br/>
	Supress Sellers Name: ${preference.pingII_SupressSellerName}<br/>
	Suppress Trim Match Data: ${preference.pingII_SuppressTrimMatchStatus}<br/>
	Exclude No Price &amp; Zero Price from Calculations: ${preference.pingII_ExcludeNoPriceFromCalc}<br/>
	<h3>Remove External Listing Price Outliers</h3>
	Low Price Multiplier: ${preference.pingII_ExcludeLowPriceOutliersMultiplier}<br/>
	High Price Multiplier: ${preference.pingII_ExcludeHighPriceOutliersMultiplier}<br/>
	Match Certified By Default: ${preference.pingII_MatchCertifiedByDefault}<br/>
	<br/>
	
	<h2>Default Ping Age Buckets</h2>
	<table border="1">
		<tr><td>Age Bucket</td><td>Min Market %</td><td>Max Market %</td><td>Min Gross</td><tr>
		<tr>
			<td><span id="age_range1LowDisp">0</span> - <span id="age_range1HighDisp">${age_range1High}</span> Days
			<td>${age_range1MinMarket}</td><td>${age_range1MaxMarket}</td><td>${age_range1MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range2LowDisp">${age_range2Low}</span> <c:if test="${age_range2High > 0}"> - <span id="age_range2HighDisp">${age_range2High}</span></c:if><c:if test="${age_range2High eq 0}">+</c:if> Days
			<td>${age_range2MinMarket}</td><td>${age_range2MaxMarket}</td><td>${age_range2MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range3LowDisp">${age_range3Low}</span> <c:if test="${age_range3High > 0}"> - <span id="age_range3HighDisp">${age_range3High}</span></c:if><c:if test="${age_range3High eq 0}">+</c:if> Days
			<td>${age_range3MinMarket}</td><td>${age_range3MaxMarket}</td><td>${age_range3MinGross}</td>
		</tr>	
		<tr>
			<td><span id="age_range4LowDisp">${age_range4Low}</span> <c:if test="${age_range4High > 0}"> - <span id="age_range4HighDisp">${age_range4High}</span></c:if><c:if test="${age_range4High eq 0}">+</c:if> Days
			<td>${age_range4MinMarket}</td><td>${age_range4MaxMarket}</td><td>${age_range4MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range5LowDisp">${age_range5Low}</span> <c:if test="${age_range5High > 0}"> - <span id="age_range5HighDisp">${age_range5High}</span></c:if><c:if test="${age_range5High eq 0}">+</c:if> Days
			<td>${age_range5MinMarket}</td><td>${age_range5MaxMarket}</td><td>${age_range5MinGross}</td>
		</tr>		
		<tr>
			<td><span id="age_range6LowDisp">${age_range6Low}</span> <c:if test="${age_range6High > 0}"> - <span id="age_range6HighDisp">${age_range6High}</span></c:if><c:if test="${age_range6High eq 0}">+</c:if> Days
			<td>${age_range6MinMarket}</td><td>${age_range6MaxMarket}</td><td>${age_range6MinGross}</td>
		</tr>		
    </table>

    <br/>

    <h2>Ping II Mileage Adjustments</h2>
    <table border="1">
        <tr><td>Vehicle Mileage / (Current Year - Vehicle Year + 1)</td><td>Low Adjustment</td><td>High Adjustment</td></tr>
        <tr>
            <td><span>0</span> - <span>${mileage_range1High}</span></td>
            <td>${mileage_range1LowAdj}%</td><td>${mileage_range1HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range2Low}</span> - <span>${mileage_range2High}</span></td>
            <td>${mileage_range2LowAdj}%</td><td>${mileage_range2HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range3Low}</span> - <span>${mileage_range3High}</span></td>
            <td>${mileage_range3LowAdj}%</td><td>${mileage_range3HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range4Low}</span> - <span>${mileage_range4High}</span></td>
            <td>${mileage_range4LowAdj}%</td><td>${mileage_range4HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range5Low}</span> - <span>${mileage_range5High}</span></td>
            <td>${mileage_range5LowAdj}%</td><td>${mileage_range5HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range6Low}</span> - <span>${mileage_range6High}</span></td>
            <td>${mileage_range6LowAdj}%</td><td>${mileage_range6HighAdj}%</td>
        </tr>
        <tr>
            <td><span>${mileage_range7Low}</span> +</td>
            <td>${mileage_range7LowAdj}%</td><td>${mileage_range7HighAdj}%</td>
        </tr>
    </table>
			
</c:if>

<!--///////////////// EDITABLE MODE //////////////-->
<c:if test="${isEdit}">
	<h1>Ping II System Default Settings</h1>
	<h3>
		<a onclick=" new Ajax.Updater('pingIISystemPrefs', '/fl-admin/system/display/pingIISettings.do?state=display&dealerId=100150', 
			{asynchronous:true, evalScripts:true } ); return false;" href="#" >cancel</a>
	</h3><br/>
	
	<form name="pingIIForm" action="#">
		Default Search Radius: 
			<select name="defaultSearchRadius" >
			<option value="10" ${preference.pingII_DefaultSearchRadius == 10 ? 'selected=""':'' }>10</option>
			<option value="25" ${preference.pingII_DefaultSearchRadius == 25 ? 'selected=""':'' }>25</option>
			<option value="50" ${preference.pingII_DefaultSearchRadius == 50 ? 'selected=""':'' }>50</option>
			<option value="75" ${preference.pingII_DefaultSearchRadius == 75 ? 'selected=""':'' }>75</option>
			<option value="100" ${preference.pingII_DefaultSearchRadius == 100 ? 'selected=""':'' }>100</option>
			<option value="150" ${preference.pingII_DefaultSearchRadius == 150 ? 'selected=""':'' }>150</option>
	    	<option value="250" ${preference.pingII_DefaultSearchRadius == 250 ? 'selected=""':'' }>250</option>
			<option value="500" ${preference.pingII_DefaultSearchRadius == 500 ? 'selected=""':'' }>500</option>
			<option value="750" ${preference.pingII_DefaultSearchRadius == 750 ? 'selected=""':'' }>750</option>
			<option value="1000" ${preference.pingII_DefaultSearchRadius == 1000 ? 'selected=""':'' }>1000</option>
			
		</select><br/>
		Max Search Radius: 
		<select name="maxSearchRadius" >
	    	<option value="10" ${preference.pingII_MaxSearchRadius == 10 ? 'selected=""':'' }>10</option>
	    	<option value="25" ${preference.pingII_MaxSearchRadius == 25 ? 'selected=""':'' }>25</option>
	    	<option value="50" ${preference.pingII_MaxSearchRadius == 50 ? 'selected=""':'' }>50</option>
	    	<option value="75" ${preference.pingII_MaxSearchRadius == 75 ? 'selected=""':'' }>75</option>
	    	<option value="100" ${preference.pingII_MaxSearchRadius == 100 ? 'selected=""':'' }>100</option>
	    	<option value="150" ${preference.pingII_MaxSearchRadius == 150 ? 'selected=""':'' }>150</option>
	    	<option value="250" ${preference.pingII_MaxSearchRadius == 250 ? 'selected=""':'' }>250</option>
	    	<option value="500" ${preference.pingII_MaxSearchRadius == 500 ? 'selected=""':'' }>500</option>
	    	<option value="750" ${preference.pingII_MaxSearchRadius == 750 ? 'selected=""':'' }>750</option>
	    	<option value="1000" ${preference.pingII_MaxSearchRadius == 1000 ? 'selected=""':'' }>1000</option>
	    	
		</select><br/>
		Supress Sellers Name: 
		<input type="checkbox" name="supressSellerName" ${preference.pingII_SupressSellerName?'checked="checked"':'' }/><br/>
		Suppress Trim Match Data:
		<input type="checkbox" name="suppressTrimMatchStatus" ${preference.pingII_SuppressTrimMatchStatus?'checked="checked"':'' }/><br/>
		Exclude No Price & Zero Price from Calculations:
		<input type="checkbox" name="excludeNoPriceFromCalc" ${preference.pingII_ExcludeNoPriceFromCalc?'checked="checked"':'' }/><br/>
		<h3>Remove External Listing Price Outliers</h3>
		Low Price Multiplier:
		<input type="text" name="excludeLowPriceOutliersMultiplier" value="${preference.pingII_ExcludeLowPriceOutliersMultiplier}" size="4"/><br/>
		High Price Multiplier:
		<input type="text" name="excludeHighPriceOutliersMultiplier" value="${preference.pingII_ExcludeHighPriceOutliersMultiplier}" size="4" /><br/>
		Match Certified By Default:
		<input type="checkbox" name="matchCertifiedByDefault" ${preference.pingII_MatchCertifiedByDefault?'checked="checked"':'' }/><br/>
		

		<h2>Ping Age Buckets</h2>

	<span id="message"></span>	

    <table width="540px" align="center">
    <tr>
        <td>Bucket Size</td>
        <td>Bucket Range</td>
        <td>Min Market %</td>
        <td>Max Market %</td>
        <td>Min Gross</td>
    </tr>
    <tr>
	    <td>
	    Bucket #1*:
	    <input type="text" name="age_range1" value="${age_range1}" size="3" onchange="recalculateDateRanges(1, false, 'age_');"/>
	    </td>
	    <td>
	    	<span id="age_range1LowDisp">0</span> - <span id="age_range1HighDisp">${age_range1High}</span> Days
	    </td>
	    <td><input type="text" name="age_range1Value1" value="${age_range1MinMarket}" size="4"/></td>
	    <td><input type="text" name="age_range1Value2" value="${age_range1MaxMarket}" size="4"/></td>
	    <td><input type="text" name="age_range1Value3" value="${age_range1MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #2*:
	    <input type="text" name="age_range2"value="${age_range2}" size="3" onchange="recalculateDateRanges(2, false, 'age_');"/>
    	</td><td>
	    	<span id="age_range2LowDisp">${age_range2Low}</span> - <span id="age_range2HighDisp">${age_range2High}</span> Days
   		</td>
       	<td><input type="text" name="age_range2Value1" value="${age_range2MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range2Value2" value="${age_range2MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range2Value3" value="${age_range2MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #3*:
	    	<input type="text" name="age_range3" value="${age_range3}" size="3" onchange="recalculateDateRanges(3, false, 'age_');"/>
	    </td><td>
	    	<span id="age_range3LowDisp">${age_range3Low}</span> - <span id="age_range3HighDisp">${age_range3High}</span> Days
    	</td>
    	<td><input type="text" name="age_range3Value1" value="${age_range3MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range3Value2" value="${age_range3MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range3Value3" value="${age_range3MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #4:
	    	<input type="text" name="age_range4" value="${age_range4}" size="3" onchange="recalculateDateRanges(4, false, 'age_');"/>
	    </td><td>
		    <c:choose>
		        <c:when test="${age_range4Low == 0 && age_range4High == 0}"><span id="age_range4LowDisp"></span> - <span id="age_range4HighDisp"></span> Days</c:when>
		        <c:when test="${age_range4Low != 0 && age_range4High == 0}"><span id="age_range4LowDisp">${age_range4Low}</span> - <span id="age_range4HighDisp">+</span> Days</c:when>
		        <c:otherwise><span id="age_range4LowDisp">${age_range4Low}</span> - <span id="age_range4HighDisp">${age_range4High}</span> Days</c:otherwise>
		    </c:choose>  
    	</td>
    	<td><input type="text" name="age_range4Value1" value="${age_range4MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range4Value2" value="${age_range4MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range4Value3" value="${age_range4MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #5:        
	    	<input type="text" name="age_range5" value="${age_range5}" size="3" onchange="recalculateDateRanges(5, false, 'age_');"/>
	    </td><td>  
		    <c:choose>
		        <c:when test="${age_range5Low == 0 && age_range5High == 0}"><span id="age_range5LowDisp"></span> - <span id="age_range5HighDisp"></span> Days</c:when>
		        <c:when test="${age_range5Low != 0 && age_range5High == 0}"><span id="age_range5LowDisp">${age_range5Low}</span> - <span id="age_range5HighDisp">+</span> Days</c:when>
		        <c:otherwise><span id="age_range5LowDisp">${age_range5Low}</span> - <span id="age_range5HighDisp">${age_range5High}</span> Days</c:otherwise>
		    </c:choose>  
    	</td>
       	<td><input type="text" name="age_range5Value1" value="${age_range5MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range5Value2" value="${age_range5MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range5Value3" value="${age_range5MinGross}" size="4"/></td>
    </tr>
    <tr>
    	<td>Bucket #6:        
	    	<input type="text" name="age_range6" value="${age_range6}" size="3" onchange="recalculateDateRanges(6, false, 'age_');"/>
	    </td><td>  
		    <c:choose>
		        <c:when test="${age_range6Low == 0 && age_range6High == 0}"><span id="age_range6LowDisp"></span> - <span id="age_range6HighDisp"></span> Days</c:when>
		        <c:when test="${age_range6Low != 0 && age_range6High == 0}"><span id="age_range6LowDisp">${age_range6Low}</span> - <span id="age_range6HighDisp">+</span> Days</c:when>
		        <c:otherwise><span id="age_range6LowDisp">${age_range6Low}</span> - <span id="age_range6HighDisp">${age_range6High}</span> Days</c:otherwise>
		    </c:choose>  
    	</td>
       	<td><input type="text" name="age_range6Value1" value="${age_range6MinMarket}" size="4"/></td>
    	<td><input type="text" name="age_range6Value2" value="${age_range6MaxMarket}" size="4"/></td>
    	<td><input type="text" name="age_range6Value3" value="${age_range6MinGross}" size="4"/></td>    	
    </tr>
    </table> 
    
    <h:hidden property="useWatchList" styleId="useWatchList" value="false"/>
    <h:hidden property="age_range1Low" styleId="age_range1Low" value="${age_range1Low}"/>
    <h:hidden property="age_range1High" styleId="age_range1High" value="${age_range1High}"/>
    <h:hidden property="age_range2Low" styleId="age_range2Low" value="${age_range2Low}"/>
    <h:hidden property="age_range2High" styleId="age_range2High" value="${age_range2High}"/>
    <h:hidden property="age_range3Low" styleId="age_range3Low" value="${age_range3Low}"/>
    <h:hidden property="age_range3High" styleId="age_range3High" value="${age_range3High}"/>
    <h:hidden property="age_range4Low" styleId="age_range4Low" value="${age_range4Low}"/>
    <h:hidden property="age_range4High" styleId="age_range4High" value="${age_range4High}"/>
    <h:hidden property="age_range5Low" styleId="age_range5Low" value="${age_range5Low}"/>
    <h:hidden property="age_range5High" styleId="age_range5High" value="${age_range5High}"/>
    <h:hidden property="age_range6Low" styleId="age_range6Low" value="${age_range6Low}"/>
    <h:hidden property="age_range6High" styleId="age_range6High" value="${age_range6High}"/>

    <br/>

    <h2>Ping II Mileage Adjustments</h2>
    <table border="1">
        <tr><td>Vehicle Mileage / (Current Year - Vehicle - Year + 1)</td><td>Low Adjustment</td><td>High Adjustment</td></tr>
        <tr>
            <td>Bucket #1:<input type="text" name="mileage_range1" value="${mileage_range1}" size="5" onchange="recalculateDateRanges(1, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range1Value1" value="${mileage_range1LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range1Value2" value="${mileage_range1HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #2:<input type="text" name="mileage_range2" value="${mileage_range2}" size="5" onchange="recalculateDateRanges(2, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range2Value1" value="${mileage_range2LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range2Value2" value="${mileage_range2HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #3:<input type="text" name="mileage_range3" value="${mileage_range3}" size="5" onchange="recalculateDateRanges(3, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range3Value1" value="${mileage_range3LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range3Value2" value="${mileage_range3HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #4:<input type="text" name="mileage_range4" value="${mileage_range4}" size="5" onchange="recalculateDateRanges(4, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range4Value1" value="${mileage_range4LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range4Value2" value="${mileage_range4HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #5:<input type="text" name="mileage_range5" value="${mileage_range5}" size="5" onchange="recalculateDateRanges(5, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range5Value1" value="${mileage_range5LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range5Value2" value="${mileage_range5HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #6:<input type="text" name="mileage_range6" value="${mileage_range6}" size="5" onchange="recalculateDateRanges(6, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range6Value1" value="${mileage_range6LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range6Value2" value="${mileage_range6HighAdj}" size="2" /></td>
        </tr>
        <tr>
            <td>Bucket #7:<input type="text" name="mileage_range7" value="${mileage_range7}" size="5" onchange="recalculateDateRanges(7, false, 'mileage_');"/></td>
            <td><input type="text" name="mileage_range7Value1" value="${mileage_range7LowAdj}" size="2" /></td>
            <td><input type="text" name="mileage_range7Value2" value="${mileage_range7HighAdj}" size="2" /></td>
        </tr>
    </table>

    <h:hidden property="mileage_range1Low" styleId="mileage_range1Low" value="${mileage_range1Low}"/>
    <h:hidden property="mileage_range1High" styleId="mileage_range1High" value="${mileage_range1High}"/>
    <h:hidden property="mileage_range2Low" styleId="mileage_range2Low" value="${mileage_range2Low}"/>
    <h:hidden property="mileage_range2High" styleId="mileage_range2High" value="${mileage_range2High}"/>
    <h:hidden property="mileage_range3Low" styleId="mileage_range3Low" value="${mileage_range3Low}"/>
    <h:hidden property="mileage_range3High" styleId="mileage_range3High" value="${mileage_range3High}"/>
    <h:hidden property="mileage_range4Low" styleId="mileage_range4Low" value="${mileage_range4Low}"/>
    <h:hidden property="mileage_range4High" styleId="mileage_range4High" value="${mileage_range4High}"/>
    <h:hidden property="mileage_range5Low" styleId="mileage_range5Low" value="${mileage_range5Low}"/>
    <h:hidden property="mileage_range5High" styleId="mileage_range5High" value="${mileage_range5High}"/>
    <h:hidden property="mileage_range6Low" styleId="mileage_range6Low" value="${mileage_range6Low}"/>
    <h:hidden property="mileage_range6High" styleId="mileage_range6High" value="${mileage_range6High}"/>
    <h:hidden property="mileage_range7Low" styleId="mileage_range7Low" value="${mileage_range7Low}"/>
    <h:hidden property="mileage_range7High" styleId="mileage_range7High" value="${mileage_range7High}"/>
	
		
	<input type="button" onclick="submitForm();" value="Save All" />
	</form>
	
</c:if>

</div>
