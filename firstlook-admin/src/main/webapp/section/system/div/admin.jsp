<%@include file="/include/tag-import.jsp" %>

<h2>First Look Administrators</h2>

<div class="content">

  <div class="row">
  	<div id="memberCreateMessage"></div>
    <div id="adminCreateButton">
      <a href="#" onclick="Element.toggle('adminCreateButton'); Element.toggle('adminCreateForm');"/>add new admin</a>
    </div>
    <div id="adminCreateForm" class="wide-section" style="display:none;">
      <div class="nested-title">
        Add New Administrator
      </div>
      <c:url var="createAdmin" value="/system/custom/adminCreate.do?memberType=ADMIN"/>
      <proto:formRemote href="${createAdmin}" update="admin" id="sysAdmin">
        <div class="nested-link">
          <input type="submit" value="ADD"/>
        </div>
        <div class="content">
          <div class="row">
            <br/>
            First: <input type="text" size="15" name="firstName"/>&nbsp;&nbsp;Last: <input type="text" size="15" name="lastName"/>
            Login: <input type="text" size="15" name="login"/>&nbsp;&nbsp;Password: <input type="password" size="15" name="password"/><br/>
          </div>
        </div>
      </proto:formRemote>
    </div>

  </div>
  <br/>
  <table id="member_table" class="datagrid">
    <thead>
      <tr>
        <th mochi:format="str">First</th>
        <th mochi:format="str">Last</th>
        <th mochi:format="str">Login</th>

        <th mochi:format="str">Type</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tfoot class="invisible">
      <tr><th mochi:format="str">Name</th></tr>
    </tfoot>

    <tbody>
      <c:forEach items="${members}" var="member">
        <tr>
          <td>${member.firstName}</td>
          <td>${member.lastName}</td>
          <td><str:truncateNicely lower="13" upper="17">${member.login}</str:truncateNicely></td>
          <td>${member.memberType}</td>
          <td>
            <c:url var="profileUrl" value="/member/dynamic/profile.do">
		      <c:param name="id" value="${member.id}"/>
            </c:url>
            <a href="${profileUrl}">view</a>
        </td>
      </tr>
    </c:forEach>
    <c:if test="${empty dealer.members}">
      <tr><td colspan="4">No Matching Results</td></tr>
    </c:if>
  </tbody>

</table> 

</div>

<script type="text/javascript">
  sortableManager = new SortableManager();
  sortableManager.initWithTable($('member_table'));
  <%-- used for returning from MemberCreate Ajax.Updater, evalScripts:true --%>
	var text = '<p>' + '${createMemberLogin}' + '</p>';
	Element.update('memberCreateMessage', text );
</script>

