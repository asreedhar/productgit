<%@include file="/include/tag-import.jsp" %>

<c:url var="suppressUrl" value="/system/custom/suppressMarket.do"/>
<c:url var="unsuppressUrl" value="/system/custom/unsuppressMarket.do"/>


<h2>Suppressed Market Performers</h2>

<div class="content">
  <table>
    <tr>
      <td valign="top" id="suppressTarget">
    <p>Suppressed Groupings</p>
    
    <ul id="suppressed" style="width: 300px;">
    </ul>
    <script type="text/javascript" language="javascript">
 	  Droppables.add('suppressTarget',
 	  	{onDrop: function(element) {
		   Element.remove(element);
           new Ajax.Request('${suppressUrl}', {parameters: 'id=' + element.id});
  	       new Insertion.Bottom('suppressed', '<li id="' + element.id + '">' + element.innerHTML + '</li>');
   		   new Draggable(element.id,{revert:true});
   		   new Effect.Highlight(element.id)
 	  	}});
	</script>
     </td>
     <td valign="top" id="allTarget">  
  
  <div class="right-content">
  	<p>Groupings</p>
  	
   	<ul id="all" style="width: 300px;">
      <c:forEach items="${groupings}" var="grouping">
   	    <li id="${grouping.id}">${grouping.name}</li>
      </c:forEach>
  	</ul>
    <script type="text/javascript" language="javascript">
    
      <c:forEach items="${groupings}" var="grouping">
        this.createDraggableListener = function() {
          this.draggable = new Draggable('${grouping.id}', {revert:true});

          //remove this listener once the draggable has been 
          // created (no longer needed)
          Event.stopObserving($("${grouping.id}"), "mouseover",
                              this.createDraggableListener);
          this.createDraggableListener = null;
        };
        Event.observe($("${grouping.id}"), "mouseover", this.createDraggableListener);

          <c:if test="${grouping.suppress}">
 		    Element.remove('${grouping.id}');
 	  	    new Insertion.Bottom('suppressed', '<li id="${grouping.id}">${grouping.name}</li>');
   		    new Draggable('${grouping.id}',{revert:true});
   		    new Effect.Highlight('${grouping.id}')
          </c:if>
	  </c:forEach>
  	
    
 	  Droppables.add('allTarget',
 	  	{onDrop: function(element) {
		   Element.remove(element);
           new Ajax.Request('${unsuppressUrl}', {parameters: 'id=' + element.id});
 	  	   new Insertion.Bottom('all', '<li id="' + element.id + '">' + element.innerHTML + '</li>');
   		   new Draggable(element.id,{revert:true});
   		   new Effect.Highlight(element.id)
 	  	}});
	</script>
  	</td>
  </tr>
</table>

</div>