<%@include file="/include/tag-import.jsp"%>

<script type="text/javascript">

function submitForm( pars ) {
	new Ajax.Updater(	'message', 
						'/fl-admin/system/display/editExportSettings.do?exportType=' + $('exportType').innerHTML, 
						{asynchronous:true, evalScripts:true, method:'post', parameters:pars} ); 
	return false;
}

</script>

<div id="exportType" style="display:none;">${param.exportType}</div>

<div id="exportSettings" class="wide-section">
	
	<c:url var="editExportSettings" value="/system/display/editExportSettings.do">
		<c:param name="exportType" value="${param.exportType}"/>	
	</c:url>
	
	<form action="#" onsubmit="submitSuperUserForm('exportSettingsForm', submitForm); return false;" id="exportSettingsForm" method="post">
	
		<div class="content">
			<div class="row">
				<br/>
				<c:choose>
				<c:when test="${param.exportType == 'eBiz'}">
					eBiz ID: <input type="text" size="8" name="eBizId" value="${eBizId}" }/>
					<br></br>
					eBiz Auth: <input type="text" size="50" name="eBizAuth" value="${eBizAuth}"/>
				</c:when>
				<c:when test="${param.exportType == 'SIS' }">
					SIS Web Service URL: <input type="text" size="50" name="SIS_URL" value="${SIS_URL}"/>
				</c:when>
				<c:when test="${param.exportType == 'ReyDirect' }">
					Reynolds Direct Web Service URL: <input type="text" size="50" name="Reynolds_Direct_URL" value="${Reynolds_Direct_URL}"/>
				</c:when>
				</c:choose>
			</div>
		</div><br/>
		Super Admin Password: <input type="password" name="suPassword" size="10"/>
		
		<div class="nested-link">
			<input type="submit" value="Save"/>
		</div>
	</form>
	<br/>	
	<div id="message">
	</div>
</div>


