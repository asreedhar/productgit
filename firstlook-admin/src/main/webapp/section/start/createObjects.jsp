<%@include file="/include/tag-import.jsp" %>

<c:url var="admin" value="/system/page/admin.do"/>
<c:url var="groupSearch" value="/group/page/search.do"/>
<c:url var="dealerSearch" value="/dealer/page/search.do"/>
<c:url var="corporationSearch" value="/corporation/page/search.do"/>

<div class="row">
  <div class="wide-content">
    <div class="wide-section">
      <h2>Creating New Members</h2>
      <p>
        In the First Look system, there are three classes of users:
        Administrators, Account Representatives, and Users.
      </p>
      <ul>
        <li>
          <b>To create an Administrator:</b>
          <a href="${admin}">Click Here</a>
        </li>
        <li>
          <b>To create an Account Representative:</b>
          <a href="${groupSearch}">Find a Group</a>,
          then click on the "Members" tab.  You will then
          see a link which will allow you to add a Member to
          one or more Dealers in that particular group.
        </li>
        <li>
          <b>To create a Member User:</b> 
          <a href="${dealerSearch}">Find a Dealer</a>, 
          then click on the "Members" tab.  You will then
          see a link which will allow you to add a Member to
          that Dealer and other Dealers in the Same Group.
        </li>
      </ul>
      <h2>Creating New Dealers</h2>
      <p>
        Dealers are added to specific Dealer Groups.
      </p>
      <ul>
        <li>
          <b>To create an Dealer:</b>
          <a href="${groupSearch}">Find a Group</a>,
          then click on the "Dealers" tab.  You will then see
          a link which will allow you to add a Dealer to that 
          Dealer Group.
        </li>
      </ul>
      <h2>Creating New Dealer Groups</h2>
      <ul>
        <li>
          <b>To create a new Dealer Group:</b>
          Dealer Groups can be created from the <a href="${groupSearch}">
          Group Search Page</a>.  On this page you will find a link to
          a form which allow you to add new Dealer Groups.
        </li>
      </ul>
      <h2>Creating New Corporations</h2>
      <ul>
        <li>
          <b>To create a new Corporation:</b>
          Corporations can be created from the <a href="${corporationSearch}">
          Corporation Search Page</a>.  On this page you will find a link to
          a form which allow you to add new Corporations.
        </li>
      </ul>
      <h2>Managing System Objects (Regions, Cost Factors, Markets)</h2>
      <ul>
        <li>
          <b>To manage system objects:</b> 
          Click on the System in the top navigation menu.
        </li>
      </ul>
    </div>
  </div>
</div>