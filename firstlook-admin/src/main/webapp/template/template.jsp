<%@include file="/include/tag-import.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<t:importAttribute/>

<html>
	<head>
		<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"/>
		<title>FLADMIN - <t:getAsString name="title"/></title>
		<c:url var="styles" value="/resources/css/styles.css"/>
		<link rel="stylesheet" href="${styles}" type="text/css"/>

		<c:url var="sections_nav_list" value="/resources/css/sections_nav_list.css"/>
		<link rel="stylesheet" href="${sections_nav_list}" type="text/css"/>

		<c:url var="breadcrumb" value="/resources/css/breadcrumb.css"/>
		<link rel="stylesheet" href="${breadcrumb}" type="text/css"/>

		<c:url var="subsections" value="/resources/css/subsections.css"/>
		<link rel="stylesheet" href="${subsections}" type="text/css"/>

		<c:url var="cal_css" value="/resources/css/calendar.css"/>
		<link rel="stylesheet" href="${cal_css}" type="text/css"/>

		<c:url var="basecamp" value="/resources/css/basecamp.css"/>
		<link rel="stylesheet" href="${basecamp}" type="text/css"/>

		<c:url var="basecamp_color" value="/resources/css/basecamp-color.css"/>
		<link rel="stylesheet" href="${basecamp_color}" type="text/css"/>

		<c:url var="sortable_tables" value="/resources/css/Mochikit/sortable_tables.css"/>
        <link rel="stylesheet" href="${sortable_tables}" type="text/css" />
	    <c:if test="${not empty extraCSS}">
	      <c:url var="extraCSSUrl" value="${extraCSS}"/>
	      <link rel="stylesheet" type="text/css" href="${extraCSSUrl}"/>
	    </c:if>

   	  <c:url var="validation" value="/resources/js/validation.js"/>
	  <script src="${validation}" type="text/javascript"></script>

   	  <c:url var="prototype" value="/resources/js/ajax/prototype.js"/>
	  <script src="${prototype}" type="text/javascript"></script>

   	  <c:url var="effects" value="/resources/js/ajax/effects.js"/>
	  <script src="${effects}" type="text/javascript"></script>

   	  <c:url var="dragdrop" value="/resources/js/ajax/dragdrop.js"/>
	  <script src="${dragdrop}" type="text/javascript"></script>

   	  <c:url var="controls" value="/resources/js/ajax/controls.js"/>
	  <script src="${controls}" type="text/javascript"></script>

   	  <c:url var="flpopup" value="/resources/js/fl/fl-util.js"/>
	  <script src="${flpopup}" type="text/javascript"></script>

   	  <c:url var="mochikit" value="/resources/js/MochiKit/MochiKit.js"/>
      <script src="${mochikit}" type="text/javascript"></script>

   	  <c:url var="sortable_tables" value="/resources/js/MochiKit/sortable_tables.js"/>
      <script src="${sortable_tables}" type="text/javascript"></script>

   	  <c:url var="fckEditor" value="/resources/FCKeditor/fckeditor.js"/>
      <script type="text/javascript" src="${fckEditor}"></script>

   	  <c:url var="calendar" value="/resources/js/calendar.js"/>
      <script type="text/javascript" src="${calendar}"></script>

   	  <c:url var="calendar_en" value="/resources/js/calendar-en.js"/>
      <script type="text/javascript" src="${calendar_en}"></script>

   	  <c:url var="local_site" value="/resources/js/local_site.js"/>
      <script type="text/javascript" src="${local_site}"></script>

      <c:url var="multiEdit" value="/resources/js/multiEdit.js"/>
      <script type="text/javascript" src="${multiEdit}"></script>

	</head>
	<body>

	<div id="Header">
      <h3 class="global">

        <a href="/IMT/LogoutAction.go" class="">Exit</a>

	  </h3>

	  <h1>First Look Administration</h1>

	  <div id="Tabs">
 	      <t:insert name="topNav">
		    <t:put name="sectionName" value="${sectionName}"/>
   	      </t:insert>
      </div>


      </div>


      <div class="row">
		<div id="breadcrumb">
 		  <t:get name="breadcrumb"/>
		</div>
		<div id="subsections">
 		  <t:insert name="subsections">
		    <t:put name="subsectionName" value="${subsectionName}"/>
 		  </t:insert>
		</div>
	  </div>
   	  <div id="content">
		<t:get name="body"/>
	  </div>



	</body>
</html>
