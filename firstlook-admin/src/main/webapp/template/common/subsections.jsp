<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>

<c:url var="memberSearch" value="/member/page/search.do"/>
<c:url var="dealerSearch" value="/dealer/page/search.do"/>
<c:url var="groupSearch" value="/group/page/search.do"/>
<c:url var="corporationSearch" value="/corporation/page/search.do"/>
<c:url var="createObjects" value="/start/page/createObjects.do"/>

<ul id="tablist">
  <c:choose>
    <c:when test="${subsectionName == 'memberSearch'}">
      <li id="active">
        <a href="${memberSearch}" id="current">search members</a> 
      </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${memberSearch}">search members</a> 
      </li> 	
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${subsectionName == 'dealerSearch'}">
      <li id="active">
        <a href="${dealerSearch}" id="current">search dealers</a> 
      </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${dealerSearch}">search dealers</a> 
      </li> 	
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${subsectionName == 'groupSearch'}">
      <li id="active">
        <a href="${groupSearch}" id="current">search groups</a> 
      </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${groupSearch}">search groups</a> 
      </li> 	
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${subsectionName == 'corporationSearch'}">
      <li id="active">
        <a href="${corporationSearch}" id="current">search corporations</a> 
      </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${corporationSearch}">search corporations</a> 
      </li> 	
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${subsectionName == 'createObjects'}">
      <li id="active">
        <a href="${createObjects}" id="current">create</a> 
      </li> 	
    </c:when>
    <c:otherwise>
      <li>
        <a href="${createObjects}">create</a> 
      </li> 	
    </c:otherwise>
  </c:choose>
</ul>
