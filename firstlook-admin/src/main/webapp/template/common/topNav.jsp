<%@include file="/include/tag-import.jsp" %>

<t:importAttribute/>

<c:url var="dealerSearch" value="/dealer/page/search.do"/>
<c:url var="groupSearch" value="/group/page/search.do"/>
<c:url var="corporationSearch" value="/corporation/page/search.do"/>
<c:url var="memberSearch" value="/member/page/search.do"/>
<c:url var="system" value="/system/page/system.do"/>

<ul id="MainTabs">
    <c:choose>
      <c:when test="${sectionName == 'Member'}">
      	<li><a href="${memberSearch}" class="current" id="current" accesskey="m">Member</a></li>
      </c:when>
      <c:otherwise>
      	<li><a href="${memberSearch}" accesskey="m">Member</a></li>
      </c:otherwise>
    </c:choose>
    <c:choose>
      <c:when test="${sectionName == 'Dealer'}">
      	<li><a href="${dealerSearch}" class="current" id="current" accesskey="d">Dealer</a></li>
      </c:when>
      <c:otherwise>
      	<li><a href="${dealerSearch}" accesskey="d">Dealer</a></li>
      </c:otherwise>
    </c:choose>
    <c:choose>
      <c:when test="${sectionName == 'Group'}">
      	<li><a href="${groupSearch}" class="current" id="current" accesskey="g">Group</a></li>
      </c:when>
      <c:otherwise>
      	<li><a href="${groupSearch}" accesskey="g">Group</a></li>
      </c:otherwise>
    </c:choose>
    <c:choose>
      <c:when test="${sectionName == 'Corporation'}">
      	<li><a href="${corporationSearch}" class="current" id="current" accesskey="c">Corporation</a></li>
      </c:when>
      <c:otherwise>
      	<li><a href="${corporationSearch}" accesskey="c">Corporation</a></li>
      </c:otherwise>
    </c:choose>
    <c:choose>
      <c:when test="${sectionName == 'System'}">
      	<li><a href="${system}" class="current" style="float: right; margin-right: 30px;" id="current" accesskey="s">System</a></li>
      </c:when>
      <c:otherwise>
      	<li><a href="${system}" style="float: right; margin-right: 30px;" accesskey="s">System</a></li>
      </c:otherwise>
    </c:choose>
  </ul>

