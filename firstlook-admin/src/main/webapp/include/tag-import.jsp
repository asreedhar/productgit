<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="func" %>
<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@taglib uri="http://web.discursive.com/tags-prototype" prefix="proto" %>
<%@taglib uri="http://web.discursive.com/tags-reference" prefix="ref" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/string-1.1" prefix="str" %>
<%@taglib uri="http://web.discursive.com/tags-collection" prefix="collection" %>

<%@ taglib tagdir="/WEB-INF/tags" prefix="tag"%>

