<%@ attribute name="title" required="false"%>
<%@ attribute name="property" required="false"%>
<%@ attribute name="maxlength" required="false"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="size" required="false" type="java.lang.Integer"%>
<%@ attribute name="prefix" required="false"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="omitEmpty" required="false" type="java.lang.Boolean"%>
<%@ attribute name="emptyVal" required="false"%>
<%@ attribute name="onchange" required="false"%>
<%@ attribute name="id" required="false"%>
<%@ attribute name="readOnly" required="false" %>
<%@include file="/include/tag-import.jsp" %>

<c:if test="${empty size}">
  <c:set var="size" value="${35}"/>
</c:if>

<c:if test="${empty maxlength}">
  <c:set var="maxlength" value="${300}"/>
</c:if>

<c:if test="${empty emptyVal}">
  <c:set var="emptyVal" value="-"/>
</c:if>

<c:if test="${empty readOnly}">
  <c:set var="readOnly" value="${false}"/>
</c:if>

<c:if test="${not empty title}">
  <div>
  <span class="errorMessage"><h:errors property="${property}" /></span>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<c:choose>
  <c:when test="${isEdit && not empty property && !readOnly}">${prefix}<h:text property="${property}" maxlength="${maxlength}" size="${size}" onchange="${onchange}" disabled="${disabled}" styleId="${id}"/></c:when>
  <c:otherwise>
    <span id="${property}">
      <c:choose>
        <c:when test="${not empty value}">
          ${ empty prefix ? "" : prefix }
          ${value}
        </c:when>
        <c:when test="${omitEmpty}">
        </c:when>
        <c:otherwise>
          ${emptyVal}
        </c:otherwise>
      </c:choose>
    </span>
  </c:otherwise>
</c:choose>
<c:if test="${not empty title}">
  </div>
</c:if>