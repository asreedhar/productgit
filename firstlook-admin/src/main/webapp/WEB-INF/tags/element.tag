<%@ attribute name="title" required="true"%>
<%@ attribute name="section" required="true"%>
<%@ attribute name="element" required="true"%>
<%@ attribute name="targetExtra" required="false"%>
<%@ attribute name="formBean" required="true" type="java.lang.Object"%>
<%@ attribute name="id" required="true"%>
<%@ attribute name="onSubmit" required="false"%>
<%@ attribute name="params" required="false"%>
<%@ attribute name="validate" required="false" type="java.lang.Boolean"%>

<%@include file="/include/tag-import.jsp" %>

<h2>${title}</h2>

<c:url var="saveURL" value="/${section}/save/${element}.do?${params}"/>

<proto:formRemote href="${saveURL}" update="${element}${targetExtra}" name="${section}_${element}${targetExtra}" id="${section}_${element}${targetExtra}">
  <c:set var="org.apache.struts.taglib.html.BEAN"
         value="${formBean}" scope="request"/>
  <c:if test="${validate}">
    <h:javascript method="validate_${section}_${element}" 
                  formName="${section}_${element}"
                  staticJavascript="false"/>
  </c:if>

  <c:if test="${isEdit}"><h:hidden property="id"/></c:if>
  <c:choose>
    <c:when test="${isEdit}">
      <span class="formButtons">
		<c:url var="indicator" value="/resources/images/rotating-line-load.gif"/>
		<img src="${indicator}" id="indicator" style="display: none;"/>
        <c:choose>
          <c:when test="${not empty onSubmit}">
            <h:submit value="Save" onclick="${onSubmit}"/>
          </c:when>
          <c:when test="${validate}">
            <h:submit value="Save" onclick="return validate_${section}_${element}($('${section}_${element}'));"/>
          </c:when>
          <c:otherwise>
            <h:submit value="Save"/>
          </c:otherwise>
        </c:choose>
       	<c:if test="${empty id}">
       		<c:set var="id" value="${formBean.id}" />
       	</c:if>
        <c:url var="cancelURL" value="/${section}/display/${element}.do?id=${id}&${params}"/>
		<proto:linkRemote href="${cancelURL}" update="${element}${targetExtra}">Cancel</proto:linkRemote>
      </span>
    </c:when>
    <c:otherwise>
      <c:url var="editURL" value="/${section}/edit/${element}.do?id=${id}&${params}"/>
      <span class="editLink"><proto:linkRemote href="${editURL}" update="${element}${targetExtra}">Edit</proto:linkRemote></span>
    </c:otherwise>
  </c:choose>	

  <jsp:doBody/>

</proto:formRemote>

<script type="text/javascript">
  var formVar${section}_${element}2 = $('${section}_${element}')

  formVar${section}_${element}2.addEventListener( 'submit', function() {
      Element.toggle('indicator');
      return true;
  }, false );
</script>
