<%@ attribute name="title" required="false"%>
<%@ attribute name="property" required="false"%>
<%@ attribute name="cols" required="true"%>
<%@ attribute name="rows" required="true"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="withLabel" required="false" type="java.lang.Boolean"%>
<%@include file="/include/tag-import.jsp" %>

<c:if test="${empty withLabel}">
  <c:set var="withLabel" value="${true}"/>
</c:if>

<c:if test="${empty size}">
  <c:set var="size" value="${10}"/>
</c:if>

<c:if test="${withLabel}">
  <div>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<c:choose>
  <c:when test="${isEdit && not empty property}">
    <h:textarea property="${property}" cols="${cols}" rows="${rows}" disabled="${disabled}"/>
  </c:when>
  <c:otherwise>
    <span id="${property}">
      <jsp:doBody/>
    </span>
  </c:otherwise>
</c:choose>
<c:if test="${withLabel}">
  </div>
</c:if>
