<%@ attribute name="title" required="false"%>
<%@ attribute name="property" required="false"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="selectedValue" required="false"%>
<%@ attribute name="options" required="false"%>
<%@ attribute name="nameProperty" required="false"%>
<%@ attribute name="valueProperty" required="false"%>
<%@ attribute name="size" required="false" type="java.lang.Integer"%>
<%@ attribute name="prefix" required="false"%>
<%@ attribute name="omitEmpty" required="false" type="java.lang.Boolean"%>
<%@ attribute name="omitValue" required="false"%>
<%@ attribute name="emptyOption" required="false" type="java.lang.Boolean"%>
<%@ attribute name="emptyContent" required="false"%>
<%@ attribute name="showOptions" required="false" type="java.lang.Boolean"%>
<%@ attribute name="onchange" required="false" %>
<%@ attribute name="isEnum" required="false" type="java.lang.Boolean"%>
<%@ attribute name="enumValues" required="false" type="java.lang.String[]"%>
<%@ attribute name="id" required="false"%>

<%@include file="/include/tag-import.jsp" %>

<c:if test="${empty size}">
  <c:set var="size" value="${1}"/>
</c:if>

<c:if test="${empty nameProperty}">
  <c:set var="nameProperty" value="name"/>
</c:if>

<c:if test="${empty valueProperty}">
  <c:set var="valueProperty" value="id"/>
</c:if>

<c:if test="${empty isEnum}">
  <c:set var="isEnum" value="${false}"/>
</c:if>

<c:if test="${empty omitValue}">
  <c:set var="omitValue" value=""/>
</c:if>

<c:if test="${empty id}">
  <c:set var="id" value=""/>
</c:if>

<c:if test="${empty emptyOption}">
  <c:set var="emptyOption" value="${false}"/>
</c:if>

<c:if test="${empty emptyContext}">
  <c:set var="emptyContent" value="(empty)"/>
</c:if>

<c:if test="${omitEmpty}">
  <c:set var="emptyContent" value=""/>
</c:if>

<c:if test="${empty showOptions}">
  <c:set var="showOptions" value="${false}"/>
</c:if>

<c:if test="${not empty title}">
  <div>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<c:choose>
  <c:when test="${isEdit && not empty property}">
    ${prefix}
    <h:select styleId="${id}" property="${property}" size="${size}" value="${selectedValue}" onchange="${onchange}"> 
      <c:if test="${emptyOption}">
        <h:option value=""></h:option>
      </c:if>
      <c:choose>
        <c:when test="${isEnum}">
          <c:forEach items="${enumValues}" var="enumValue">
			<c:if test="${enumValue != omitValue}">
              <h:option value="${enumValue}">${enumValue}</h:option>
            </c:if>
          </c:forEach>
        </c:when>
        <c:when test="${not empty options}">
          <h:optionsCollection name="${options}" label="${nameProperty}" value="${valueProperty}"/>
        </c:when>
        <c:otherwise>
          <jsp:doBody/>
        </c:otherwise>
      </c:choose>
    </h:select>
  </c:when>
  <c:otherwise>
    <span id="${property}">
      <c:set var="isEmpty" value="${ empty value }"/>
      <c:choose>
        <c:when test="${showOptions}">
            <jsp:doBody/>
        </c:when>
        <c:when test="${isEmpty}">
		  ${emptyContent}
        </c:when>
        <c:otherwise>
          ${ empty prefix ? "" : prefix }
          ${value}
        </c:otherwise>
      </c:choose>
    </span>
  </c:otherwise>
</c:choose>
<c:if test="${not empty title}">
  </div>
</c:if>