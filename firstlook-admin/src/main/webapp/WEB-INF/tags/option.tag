<%@ attribute name="value" required="true"%>
<%@ attribute name="selected" required="false"%>

<%@include file="/include/tag-import.jsp" %>

<c:choose>
  <c:when test="${not isEdit && selected == value}">
    <jsp:doBody/>
  </c:when>
  <c:when test="${isEdit}">  
    <c:choose>
      <c:when test="${selected == value}">
        <option value="${value}" selected><jsp:doBody/></option>
      </c:when>
      <c:otherwise>
        <option value="${value}"><jsp:doBody/></option>
      </c:otherwise>
    </c:choose>
  </c:when>
</c:choose>

     