<%@ attribute name="title" required="false"%>
<%@ attribute name="property" required="false"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="size" required="false" type="java.lang.Integer"%>
<%@ attribute name="prefix" required="false"%>
<%@ attribute name="omitEmpty" required="false" type="java.lang.Boolean"%>
<%@ attribute name="emptyVal" required="false"%>
<%@ attribute name="onchange" required="false"%>
<%@ attribute name="id" required="true"%>
<%@ attribute name="readOnly" required="false" %>
<%@include file="/include/tag-import.jsp" %>

<c:if test="${empty size}">
  <c:set var="size" value="${10}"/>
</c:if>

<c:if test="${empty emptyVal}">
  <c:set var="emptyVal" value="-"/>
</c:if>

<c:if test="${empty readOnly}">
  <c:set var="readOnly" value="${false}"/>
</c:if>

<c:if test="${not empty title}">
  <div>
  <span class="errorMessage"><h:errors property="${property}" /></span>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<c:choose>
  <c:when test="${isEdit && not empty property && !readOnly}">
     ${prefix}<h:text property="${property}" size="${size}" onchange="${onchange}" styleId="${id}"/>
     <input type="reset" value=" ... " onclick="return showCalendar('${id}', '%m/%d/%Y', '12', true);"/>
  </c:when>
  <c:otherwise>
    <span id="${property}">
      <c:choose>
        <c:when test="${not empty value}">
          ${ empty prefix ? "" : prefix }
          ${value}
        </c:when>
        <c:when test="${omitEmpty}">
        </c:when>
        <c:otherwise>
          ${emptyVal}
        </c:otherwise>
      </c:choose>
    </span>
  </c:otherwise>
</c:choose>
<c:if test="${not empty title}">
  </div>
</c:if>