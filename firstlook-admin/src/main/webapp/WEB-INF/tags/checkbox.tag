<%@ attribute name="title" required="true"%>
<%@ attribute name="property" required="true"%>
<%@ attribute name="value" required="true" type="java.lang.Boolean"%>
<%@ attribute name="onclick" required="false"%>
<%@ attribute name="id" required="false"%>

<%@include file="/include/tag-import.jsp" %>

<c:if test="${not empty title}">
  <div>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<c:set var="checked" value="${ value ? 'checked' : '' }"/>
<c:choose>
  <c:when test="${isEdit}"><span class="errorMessage"><h:errors property="${property}" /></span><h:checkbox property="${property}" styleId="${id}" onclick="${onclick}"/></c:when>
  <c:otherwise><input type="checkbox" disabled ${checked} id="${id}"/></c:otherwise>
</c:choose>
<c:if test="${not empty title}">
  </div>
</c:if>