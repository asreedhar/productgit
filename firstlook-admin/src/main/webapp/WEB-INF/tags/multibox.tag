<%@ attribute name="title" required="false"%>
<%@ attribute name="property" required="false"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="selectedValue" required="false"%>
<%@ attribute name="options" required="false" type="java.lang.Object"%>
<%@ attribute name="nameProperty" required="false"%>
<%@ attribute name="valueProperty" required="false"%>
<%@ attribute name="size" required="false" type="java.lang.Integer"%>
<%@ attribute name="prefix" required="false"%>
<%@ attribute name="omitEmpty" required="false" type="java.lang.Boolean"%>

<%@ attribute name="isEnum" required="false" type="java.lang.Boolean"%>
<%@ attribute name="enumValues" required="false" type="java.lang.String[]"%>

<%@include file="/include/tag-import.jsp" %>

<c:if test="${empty size}">
  <c:set var="size" value="${1}"/>
</c:if>

<c:if test="${empty nameProperty}">
  <c:set var="nameProperty" value="name"/>
</c:if>

<c:if test="${empty valueProperty}">
  <c:set var="valueProperty" value="id"/>
</c:if>

<c:if test="${empty isEnum}">
  <c:set var="isEnum" value="${false}"/>
</c:if>

<c:if test="${not empty title}">
  <div>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<div style="padding-left: 130px; margin-top: -15px;">
<c:choose>
  <c:when test="${isEdit && not empty property}">
    ${prefix}
      <c:choose>
        <c:when test="${isEnum}">
          <c:forEach items="${enumValues}" var="enumValue">
            <h:multibox property="${property}" value="${selectedValue}"> 
              ${enumValue}
            </h:multibox>
            ${enumValue}<br/>
          </c:forEach>
        </c:when>
        <c:otherwise>
          <c:forEach items="${options}" var="option">
            <h:multibox property="${property}"> 
              ${option[valueProperty]}
            </h:multibox>
              ${option[nameProperty]}<br/>
          </c:forEach>
       </c:otherwise>
      </c:choose>
  </c:when>
  <c:otherwise>
    <span id="${property}">
      <c:choose>
        <c:when test="${not empty value}">
          ${ empty prefix ? "" : prefix }
          ${value}
        </c:when>
        <c:when test="${omitEmpty}">
        </c:when>
        <c:otherwise>
          (empty)
        </c:otherwise>
      </c:choose>
    </span>
  </c:otherwise>
</c:choose>
</div>
<c:if test="${not empty title}">
  </div>
</c:if>