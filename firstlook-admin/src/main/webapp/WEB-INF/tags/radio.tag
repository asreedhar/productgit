<%@ attribute name="title" required="true"%>
<%@ attribute name="property" required="true"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="beanPropertyValue" required="true"%>

<%@include file="/include/tag-import.jsp" %>

<c:set var="checked" value="${(beanPropertyValue == value) ? 'checked=\"checked\"' : ''}" />
<c:choose>
  <c:when test="${isEdit}"><input type="radio" value="${value}" ${checked} name="${property}" /> ${title}</c:when>
  <c:otherwise>
      <input type="radio" disabled ${checked} name="${property}" /> ${title}
  </c:otherwise>
</c:choose>
<c:if test="${not empty title}">
</c:if>