<%@ attribute name="title" required="false"%>
<%@ attribute name="property" required="false"%>
<%@ attribute name="value" required="true"%>
<%@ attribute name="withLabel" required="false" type="java.lang.Boolean"%>
<%@ attribute name="size" required="false" type="java.lang.Integer"%>
<%@include file="/include/tag-import.jsp" %>

<c:if test="${empty withLabel}">
  <c:set var="withLabel" value="${true}"/>
</c:if>

<c:if test="${empty size}">
  <c:set var="size" value="${10}"/>
</c:if>

<c:if test="${withLabel}">
  <div>
  <label for="${property}" class="fieldLabel">${title}</label>
</c:if>
<c:choose>
  <c:when test="${isEdit && not empty property}"><h:password property="${property}" size="${size}"/></c:when>
  <c:otherwise><span id="${property}">******</span></c:otherwise>
</c:choose>
<c:if test="${withLabel}">
  </div>
</c:if>