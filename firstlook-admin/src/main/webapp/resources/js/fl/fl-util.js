var Popup = Class.create();
Popup.prototype = 
{
  initialize: function(options)
  {
    this.options = {
      url: '#',
      width: 300,
      height: 300
    }
    Object.extend(this.options, options || {});
    window.name = "main";
    window.open(this.options.url, '', 'width='+this.options.width+',height='+this.options.height);
  }
}

function submitSuperUserForm( formName, submitFunction ) {
	// verify password - if correct - submit form	
	var pars = Form.serialize($(formName));
	new Ajax.Request( '/fl-admin/system/custom/ValidateSUPassword.do', 
						{ method:'get', parameters:pars,
							onComplete : function(xhr) {
  								isSuperUser = xhr.getResponseHeader('isSuperUser');
  								if( isSuperUser ) {
  									submitFunction(pars);
  								} else {
  									alert('Incorrect Password!');
  								}
  								;}
						}); 
} 