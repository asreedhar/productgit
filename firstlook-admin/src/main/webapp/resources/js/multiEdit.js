function submitAll() {
	for (i=0; i<document.forms.length; i++) {
		var path = createPath(document.forms[i].name);
		var pars = Form.serialize(document.forms[i]);
		doSubmit(path, pars);
	}
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}

function doSubmit(path, pars) {
	new Ajax.Request(path,
		{asynchronous:false, method: 'get',parameters:pars}
	);
}

function cancelAll() {
	document.location.href = document.location.href.substring(0, document.location.href.length-12);
}

function editAll() {
	document.location.href = document.location.href + '&isEdit=true';
}

function createPath(formName) {
	var formAction = '/fl-admin/';
	var indexOfBreak = formName.indexOf("_");
	var section = formName.substring(0, indexOfBreak) + '/save/';
	var target = formName.substring(indexOfBreak+1, formName.length)+'.do?';
 	return formAction + section + target;
}