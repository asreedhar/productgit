// logic to calculate and maintain bucket ranges

function recalculateDateRanges(startRange, hasWatchListBucket, prefix)
{
	if (hasWatchListBucket == null) {
		hasWatchListBucket = true;
	}
    if (prefix == null) {
        prefix = '';
    }
    var maxRanges = 7; //hard coded to 7!? why?
    var newRange = document.getElementById(prefix + 'range' + startRange).value;
    var originalHighValue = document.getElementById( prefix + 'range' + startRange + 'High' ).value;
    var originalRange = ( originalHighValue - document.getElementById( prefix + 'range' + startRange + 'Low' ).value) + 1;      
    
    document.getElementById("message").innerHTML = "";

    if ( isNaN(newRange) )
    {
        //alert("Bucket values must be numeric.");
        document.getElementById("message").innerHTML = "Bucket values must be numeric.";
        document.getElementById(prefix + 'range' + startRange).value = originalRange;
        return;    
    }

    if ( startRange < 4 && newRange == 0 )
    {
        //alert("You must specify a number greater than zero for this bucket.");
        document.getElementById("message").innerHTML = "You must specify a number greater than zero for this bucket.";
        document.getElementById(prefix + 'range' + startRange).value = originalRange;
        return;
    }
    
    var rangeDifference = newRange - originalRange;
    var foundLastBucket = false;
    // increment/decrement the high range and all subsequent ranges
    for ( var i = parseInt(startRange,10); i <= maxRanges; i++ )
    {
        // update the high range
        var highVal = 0;
        var highValDisp = 0;
        var element = document.getElementById(prefix + 'range' + i);
        if(element)
        {
	        if (document.getElementById(prefix + 'range' + i).value != 0) 
	        {  
	            highVal = parseInt(document.getElementById(prefix + 'range' + i + 'High').value, 10) + rangeDifference;
	            highValDisp = highVal;
	        }
	        else
	        {
	            highVal = 0;
	            highValDisp = '+';
	        }
	
	        document.getElementById(prefix + 'range' + i + 'High').value = highVal;
	        document.getElementById(prefix + 'range' + i + 'HighDisp').innerHTML = highValDisp;
	
	        // update the low range	
	        if ( i != parseInt(startRange, 10) && foundLastBucket )
	        {
	            document.getElementById(prefix + 'range' + i + 'Low').value = 0;
	            document.getElementById(prefix + 'range' + i + 'LowDisp').innerHTML = '';        
	            document.getElementById(prefix + 'range' + i + 'High').value = 0;
	            document.getElementById(prefix + 'range' + i + 'HighDisp').innerHTML = '';        
	        }
	        else if ( i != parseInt(startRange, 10) )
	        {
	            var currentLow = parseInt(document.getElementById(prefix + 'range' + i + 'Low').value, 10);
	            var newLow = 0;
	            if ( currentLow == 0 )
	            {
	                newLow = parseInt(document.getElementById(prefix + 'range' + startRange + 'High').value, 10) + 1;
	            }
	            else
	            {
	                newLow = currentLow + rangeDifference;  
	            }
	                       
	            document.getElementById( prefix + 'range' + i + 'Low' ).value = newLow;
	            document.getElementById( prefix + 'range' + i + 'LowDisp' ).innerHTML = newLow;                 
	        }  
	        
	        if (document.getElementById(prefix + 'range' + i).value == 0) 
	        {
	            foundLastBucket = true;
	        }
		}
    }
    if(hasWatchListBucket) {
    	// always set watchList (RangeId=1) to be identical to range2
    	document.getElementById(prefix + 'range1').value = document.getElementById(prefix + 'range2').value;
    	document.getElementById(prefix + 'range1Low').value = document.getElementById(prefix + 'range2Low').value;
    	document.getElementById(prefix + 'range1High').value = document.getElementById(prefix + 'range2High').value;
    }
}

