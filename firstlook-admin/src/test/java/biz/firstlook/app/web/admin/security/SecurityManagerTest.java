package biz.firstlook.app.web.admin.security;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import junit.framework.TestCase;
import biz.firstlook.app.web.admin.AdminSession;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.ref.MemberType;
import biz.firstlook.persist.imt.MemberDao;

public class SecurityManagerTest extends TestCase {

	public SecurityManagerTest(String name) {
		super(name);
	}
	
	public void testCreateAdminSession() throws Exception {
		SecurityManager securityManager = new SecurityManager();
		securityManager.setMemberDao( mockMemberDao() );
		AdminSession adminSession = securityManager.createAdminSession( "admin" );
		
		assertEquals( "admin", adminSession.getLogin() );
		assertEquals( "Dude", adminSession.getFirstName() );
		assertEquals( "Master", adminSession.getLastName() );
		assertEquals( MemberType.ADMIN, adminSession.getMemberType() );
		assertEquals( new Integer(1), adminSession.getMemberId() );
	}
	
	private MemberDao mockMemberDao() {
		MemberDao memberDao = createMock(MemberDao.class);
		
		Member member = new Member();
		member.setFirstName( "Dude" );
		member.setLastName( "Master" );
		member.setLogin( "admin" );
		member.setMemberType(MemberType.ADMIN);
		member.setId( 1 );
		
		expect(memberDao.byLogin("admin")).andReturn(member);
		replay(memberDao);
		return memberDao;
	}
	
	public void testIsAdminSession() throws Exception {
		AdminSession s1 = new AdminSession();
		s1.setMemberType( MemberType.USER );
		assertFalse( SecurityManager.isAdmin( s1 ) );

		s1.setMemberType( MemberType.ADMIN );
		assertTrue( SecurityManager.isAdmin( s1 ) );
}

}
