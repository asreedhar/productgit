package biz.firstlook.app.web.admin.filter;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import javax.servlet.http.HttpSession;

import junit.framework.TestCase;
import biz.firstlook.app.web.admin.AdminSession;

public class AdminSecurityFilterTest extends TestCase {

	public AdminSecurityFilterTest(String arg0) {
		super(arg0);
	}
	
	public void testSessionPopulated() throws Exception {
		AdminSecurityFilter filter = new AdminSecurityFilter();
		
		HttpSession session = createMock( HttpSession.class );
		expect( session.getAttribute(AdminSession.ADMIN_SESSION))
			.andReturn( new AdminSession() );
		replay( session );
		
		assertTrue( filter.sessionPopulated( session ) );
	}

}
