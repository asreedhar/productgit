require File.dirname(__FILE__) + '/../test_helper'
require 'management/planning_act_spiff_controller'

class PlanningActSPIFFControllerTest < ManagementBaseControllerTest
  
  def setup
    @controller = Management::PlanningActSPIFFController
    super()
  end
  
  def active_record_class()
    ::PlanningActSPIFF
  end
  
  def controller()
    @controller
  end
  
end