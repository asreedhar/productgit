require File.dirname(__FILE__) + '/../test_helper'
require 'management/base_controller'

class Management::BaseController; def rescue_action(e) raise e end; end

class ManagementBaseControllerTest < Test::Unit::TestCase
  
  def setup
    # child class will create the controller to test
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end
  
  def test_list
    get :list
    assert_response :success
    assert_template list_template()
    id = dom_id(active_record_class().new)
    assert_tag :tag => "ul", :attributes => {
      "id"          => id,
      "hal:refresh" => "new Ajax.Updater('#{id}', '/#{controller().controller_path()}/list', {asynchronous:true, evalScripts:true})"
    }
  end
  
  def test_start
    get :start, { 'mode' => 'tile' }
    assert_response :success
    assert_template manager_template()
    mgr_assert_open_form(0)
    mgr_assert_save_form(true)
    mgr_assert_link_to_remote("add",true)
    mgr_assert_link_to_remote("edit",false)
    mgr_assert_link_to_remote("delete",false)
    mgr_assert_link_to_remote("cancel",false)
  end
  
  def test_add
    test_start
    get :add, { 'mode' => 'tile' }
    assert_response :success
    assert_template manager_template()
    mgr_assert_open_form(0)
    mgr_assert_save_form(false)
    mgr_assert_link_to_remote("add",false)
    mgr_assert_link_to_remote("edit",false)
    mgr_assert_link_to_remote("delete",false)
    mgr_assert_link_to_remote("cancel",true)
  end
  
  def test_open
    test_start
    option = css_select("select[name=#{primary_key()}] option").detect{|o| o.attributes['value'].to_i > 0}
    unless option.nil?
      get :open, { 'mode' => 'tile'}, { primary_key() => option.attributes['value'] }
      assert_response :success
      assert_template manager_template()
      mgr_assert_open_form(option.attributes['value'])
      mgr_assert_save_form(true)
      mgr_assert_link_to_remote("add",true)
      mgr_assert_link_to_remote("edit",true)
      mgr_assert_link_to_remote("delete",true)
      mgr_assert_link_to_remote("cancel",false)
    end
    return option
  end
  
  def test_edit
    option = test_open
    unless option.nil?
      get :edit, { 'mode' => 'tile' }, { primary_key() => option.attributes['value'] }
      assert_response :success
      assert_template manager_template()
      mgr_assert_open_form
      mgr_assert_save_form(false)
      mgr_assert_link_to_remote("add",false)
      mgr_assert_link_to_remote("edit",false)
      mgr_assert_link_to_remote("delete",false)
      mgr_assert_link_to_remote("cancel",true)
    end
  end
  
  def test_delete
    assert true
  end
  
  def test_save
    assert true
  end
  
  def test_cancel
    assert true
  end
  
  protected
  
  def mgr_assert_open_form(selected_value)
    open_action = "/#{controller().controller_path()}/open"
    assert_tag :tag => "form", :attributes => {
      "action" => open_action
    }
    option = css_select("select[name=#{primary_key()}] option[value=#{selected_value}]")
    assert_not_nil option
    assert_not_nil option.attributes['selected']
  end
  
  def mgr_assert_save_form(disabled)
    save_action = "/#{controller().controller_path()}/save"
    assert_tag :tag => "form", :attributes => {
      "action" => save_action
    }
    css_select("form[action=#{save_action}] input").each do |input|
      if disabled
        assert_not_nil input.attributes['disabled']
      else
        assert_nil input.attributes['disabled']
      end
    end
  end
  
  def mgr_assert_link_to_remote(action,no_tag)
    if no_tag
      assert_no_tag :tag => "a", :attributes => {
          "onclick" => Regexp.new("new Ajax.Request('/#{controller().controller_path()}/#{action}")
      }
    else
      assert_tag :tag => "a", :attributes => {
          "onclick" => Regexp.new("new Ajax.Request('/#{controller().controller_path()}#{action}")
      }
    end
  end
  
  private
  
  def list_template()
    "#{manager_template()}s"
  end
  
  def manager_template()
    c = singular_class_name(active_record_class())
    "management/#{c}/#{c}"
  end
  
  def primary_key()
    "#{singular_class_name(active_record_class())}_id"
  end
  
  def dom_id(record, prefix=nil)
    prefix ||= 'new' unless record.id
    [ prefix, singular_class_name(record), record.id ].compact * '_'
  end
  
  def singular_class_name(record_or_class)
    class_from_record_or_class(record_or_class).name.underscore.tr('/', '_')
  end
  
  def class_from_record_or_class(record_or_class)
    record_or_class.is_a?(Class) ? record_or_class : record_or_class.class
  end
  
end
