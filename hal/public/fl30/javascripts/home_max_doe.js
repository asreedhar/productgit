var MaxDOE = {
    // Variables
    $dueForPrice: null,
    $notOnline: null,
    $noEquipment: null,
    $noPrice: null,
    $lowActivity: null,
    $noPhotos: null,
    $lowAdQuality: null,

    // Methods
    ShowValues: function (dueForPrice, notOnline, noEquipment, noPrice, lowActivity, noPhotos, lowAdQuality) {
        MaxDOE.$dueForPrice.html('<a href="' + filters["DueForRepricing"].Url + '">' + filters["DueForRepricing"].Count + '</a>' );
        MaxDOE.$notOnline.html('<a href="' + filters["NotOnline"].Url + '">' + filters["NotOnline"].Count + '</a>' );
        MaxDOE.$noEquipment.html('<a href="' + filters["NoEquipment"].Url + '">' + filters["NoEquipment"].Count + '</a>' );
        MaxDOE.$noPrice.html('<a href="' + filters["NoPrice"].Url + '">' + filters["NoPrice"].Count + '</a>' );
        MaxDOE.$lowActivity.html('<a href="' + filters["LowActivity"].Url + '">' + filters["LowActivity"].Count + '</a>' );
        MaxDOE.$noPhotos.html('<a href="' + filters["NoPhotos"].Url + '">' + filters["NoPhotos"].Count + '</a>' );
        MaxDOE.$lowAdQuality.html('<a href="' + filters["LowAdQuality"].Url + '">' + filters["LowAdQuality"].Count + '</a>' );
    }
};
