﻿// Prevent collision between jQuery and prototype
jQuery.noConflict();

// Wrapping $ so that jQuery will avoid conflicts with prototype.js just for this function
(function($) {
    /********************************************
    /       DOM Ready Event
    /********************************************/
    $(document).ready(function () {
        setBrowserCheckVariables();
        setGeneralChartingVariables();
        setMaxDOEVariables();
        setAppraiseGraphVariables();
        setCoreInventoryGraphGraphVariables();
        setCurrentInventoryVariables();
    });

    var setBrowserCheckVariables = function() {
        BrowserCheck.IsIE = navigator.appName == 'Microsoft Internet Explorer';
        BrowserCheck.IsIE7 = BrowserCheck.IsIE && ((document.documentMode && document.documentMode <= 7) || (!document.documentMode && parseInt($.browser.version, 10) == 7));
        BrowserCheck.IsIE8 = BrowserCheck.IsIE && document.documentMode && document.documentMode == 8;
        BrowserCheck.IsIE9 = BrowserCheck.IsIE && document.documentMode && document.documentMode == 9;
        BrowserCheck.IsFirefox = $.browser.mozilla;
    };

    var setGeneralChartingVariables = function() {
        Charting.$body = $('body');
    };

    var setMaxDOEVariables = function() {
        MaxDOE.$dueForPrice = $('#maxdoe-dueforprice');
        MaxDOE.$notOnline = $('#maxdoe-notonline');
        MaxDOE.$noEquipment = $('#maxdoe-noequipment');
        MaxDOE.$noPrice = $('#maxdoe-noprice');
        MaxDOE.$lowActivity = $('#maxdoe-lowactivity');
        MaxDOE.$noPhotos = $('#maxdoe-nophotos');
        MaxDOE.$lowAdQuality = $('#maxdoe-lowadquality');
    };

    var setAppraiseGraphVariables = function() {
        AppraiseGraph.$barOverlay = $('.appraise-graph-baroverlay');
        AppraiseGraph.$body = $('#appraise-graph-body');
        AppraiseGraph.$bodyImage = $('#appraise-graph-body-image');
        AppraiseGraph.$toolTipContainer = $('#appraise-graph-tooltip-container');
        AppraiseGraph.$toolTip = $('#appraise-graph-tooltip');
        AppraiseGraph.$mainDiv = $('#appraise-graph-div');
        AppraiseGraph.$barOverlay0 = $('#appraise-graph-baroverlay0');
        AppraiseGraph.$barOverlay1 = $('#appraise-graph-baroverlay1');
        AppraiseGraph.$barOverlay2 = $('#appraise-graph-baroverlay2');
        AppraiseGraph.$barOverlays = $('.appraise-graph-baroverlay');
    };

    var setCoreInventoryGraphGraphVariables = function() {
        CoreInventoryGraph.$barOverlay = $('.core-inventory-graph-baroverlay');
        CoreInventoryGraph.$body = $('#core-inventory-graph-body');
        CoreInventoryGraph.$bodyImage = $('#core-inventory-graph-body-image');
        CoreInventoryGraph.$toolTipContainer = $('#core-inventory-graph-tooltip-container');
        CoreInventoryGraph.$toolTip = $('#core-inventory-graph-tooltip');
        CoreInventoryGraph.$mainDiv = $('#core-inventory-graph-div');
        CoreInventoryGraph.$barOverlay0 = $('#core-inventory-graph-baroverlay0');
        CoreInventoryGraph.$barOverlay1 = $('#core-inventory-graph-baroverlay1');
        CoreInventoryGraph.$barOverlay2 = $('#core-inventory-graph-baroverlay2');
        CoreInventoryGraph.$barOverlays = $('.core-inventory-graph-baroverlay');
    };

    var setCurrentInventoryVariables = function(){
        CurrentInventoryGraph.$cigBuckets = $('.CIG-bucket');
        CurrentInventoryGraph.$cigValueBarOverlays = $('.CurrentInventoryGraph-ValueBarOverlay');
        CurrentInventoryGraph.$IEBarOverlays = $('.IEBarOverlay');
    };
    
})(jQuery);
