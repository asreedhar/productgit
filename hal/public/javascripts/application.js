// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
Ajax.Responders.register({
  onCreate: function() {
    if($('busy') && Ajax.activeRequestCount>0)
      Effect.Appear('busy',{duration:0.5,queue:'end'});
  },
  onComplete: function() {
    if($('busy') && Ajax.activeRequestCount==0)
      Effect.Fade('busy',{duration:0.5,queue:'end'});
  }
});
function AppraisalPreferenceRulesDHTML(request, x, y, postition_id) {
	var div = '<div id="appraisal_preference_float" class="management_float" style="top:0px; left:0px;"></div>';
	var params = '';
	var p = new Popup();
	if ($('appraisal_preference_float')) {
		Element.remove('appraisal_preference_float');
	}
	new Insertion.After($('header'),div);
    $('appraisal_preference_float').innerHTML =  request.responseText;
	p.popup_show('appraisal_preference_float','appraisal_preference_set_header', ['appraisal_review_preference_submit', 'appraisal_review_preference_cancel'], 'element-bottom', x, y, postition_id);
}
function AppraisalReviewBookletFormDHTML(request, x, y, postition_id) {
    /* show the floating div */
	var div = '<div id="appraisal_review_booklet_float" class="management_float" style="top:0px; left:0px;"></div>';
	var params = '';
	var p = new Popup();
	if ($('appraisal_review_booklet_float')) {
		Element.remove('appraisal_review_booklet_float');
	}
	new Insertion.After($('header'),div);
    $('appraisal_review_booklet_float').innerHTML =  request.responseText;
	p.popup_show('appraisal_review_booklet_float','appraisal_review_booklet_preference_header', ['appraisal_review_booklet_preference_submit', 'appraisal_review_booklet_preference_cancel'], 'element-bottom', x, y, postition_id);    
	/* attach behaviour */
	var actors = [
        'include_appraisal_review_summary',
        'include_appraisal_review_follow_up_summary',
        'include_appraisal_review_detail',
        'include_appraisal_review_follow_up_detail',
        'email_notification',
        'email_address'
    ]
    for (var i = 0; i < actors.length; i++) {
        Event.observe($(actors[i]), 'click', AppraisalReviewBookletFormValidation)
    }
    Event.observe($('email_address'), 'change', AppraisalReviewBookletFormValidation)
    registerKeyEventHandlers(AppraisalReviewBookletEmailAddressHandler)
    /* attach slider */
    var slider_value = parseInt($F('appraisal_review_booklet_summary_days_back'));
    var slider_days_back = parseIntArray($F('slider_days_back').split(','));
    var slider_number_of_appraisal_reviews = parseIntArray($F('slider_number_of_appraisal_reviews').split(','));
    var slider_number_of_appraisals = parseIntArray($F('slider_number_of_appraisals').split(','));
    /* slider update function */
    var slider_update_function = function(v) {
        var num_p = slider_number_of_appraisal_reviews;
        var num_a = slider_number_of_appraisals;
        $('appraisal_review_booklet_summary_days_back').value = v;
        $('days-back').innerHTML = v;
        $('number-of-potential-deals').innerHTML = num_p[v-1];
        $('number-of-appraisals').innerHTML = num_a[v-1];
    };
    new Control.Slider('handle', 'track', {
        range:       $R(1,slider_days_back.length,false),
        values:      slider_days_back,
        sliderValue: slider_value,
        onSlide:     slider_update_function,
        onChange:    slider_update_function
    });
}
function parseIntArray(objs) {
    var ints = [];
    for (var i =0; i < objs.length; i++) {
        ints[ints.length] = parseInt(objs[i]);
    }
    return ints;
}
function AppraisalReviewBookletFormValidation(event) {
    /* 0 = ERROR and 1 = OK */
    var checkboxes = [
        'include_appraisal_review_summary',
        'include_appraisal_review_follow_up_summary',
        'include_appraisal_review_detail',
        'include_appraisal_review_follow_up_detail'
    ]
    var j = 0
    for (var i = 0; i < checkboxes.length; i++) {
        if ($(checkboxes[i]).checked) {
            j += 1
        }
    }
    if (j == 0) {
        $('print_item_notes').setStyle({color: '#F00'})
    }
    else {
        $('print_item_notes').setStyle({color: '#000'})
    }
    var k = 1
    if ($('email_notification').checked) {
        email_address = $F($('email_address'))
        /* this was not due to a key press */
        if (!event) {
            if (email_address.length == 0)
                k = 0
        }
        /* if the key press was DEL or BS then make sure we have length > 1 */
        else {
            if ((keyCode(event) == 8 || keyCode(event) == 46) && email_address.length == 1) {
                k = 0
            }
        }
    }
    if (k == 0) {
        $('notification_notes').setStyle({color: '#F00'})
    }
    else {
        $('notification_notes').setStyle({color: '#000'})
    }
    if (j == 0 || k == 0) {
        $('appraisal_review_booklet_preference_submit').disabled = 'true';
        $('booklet_preferences_submit_enabled').hide();
        $('booklet_preferences_submit_disabled').show();
    }
    else {
        $('appraisal_review_booklet_preference_submit').disabled = '';
        $('booklet_preferences_submit_enabled').show();
        $('booklet_preferences_submit_disabled').hide();
    }
}
function ResetSegmentDaysSupply() {
    var value = 45; var threshold = 10;
    $$('.current_inventory_day_supply .store .value').each(function(item) {
        value = item.value;
    })
    $$('.current_inventory_day_supply .store .threshold').each(function(item) {
        threshold = item.value;
    })
    $$('.current_inventory_day_supply .segment .value').each(function(item) {
        item.value = value;
    })
    $$('.current_inventory_day_supply .segment .threshold').each(function(item) {
        item.value = threshold;
    })
}
function ResetAppraisalPreferences(vehicle_category_id) {
    var book_category_value = $F('book_category_' + vehicle_category_id)
    var elements = $('appraisal_bookout_preferences_' + vehicle_category_id).getElementsByTagName("select");
    for (var i = 0; i < elements.length; i++) {
        for (var j = 0; j < elements[i].options.length; j++) {
            if (elements[i].options[j].value == book_category_value) {
                elements[i].selectedIndex = j;
                break;
            }
        }
    }
}
function MapToQueryString(map) {
    var queryString = ''
    for (var property in map) {
        if (queryString.length > 0)
            queryString = queryString + '&'
        queryString = queryString + property + '=' + map[property];
    }
    return queryString;
}
function AppraisalFormPopup(url) {
    window.open(
        url,
        'appraisalForm',
        'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=950,height=650'
    );
}
function PrintAppraisalForm(url) {
    window.open(
        url,
        'appraisalForm',
        'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=950,height=650'
    );
}
function SelectedOptionText(element) {
    if (element.type.indexOf('select') == -1)
        return '';
    if (element.selectedIndex == -1)
        return '';
    return element.options[element.selectedIndex].text;
}
function NAAAAuctionReport(map) {
    // naaa ids
    map.naaa_area_id   = $F('naaa_area_id');
    map.naaa_period_id = $F('naaa_period_id');
    map.vic            = $F('naaa_series_body_style_id');
    // naaa names
    naaa_series_body_style = $('naaa_series_body_style_id');
    if (naaa_series_body_style.type.indexOf('select') >= 0) {
        map.naaa_series_body_style_name = SelectedOptionText(naaa_series_body_style);
    }
    else {
        if($('naaa_series_body_style_name')){
            map.naaa_series_body_style_name = $('naaa_series_body_style_name').innerHTML;
        }
    }
    map.naaa_period_name            = SelectedOptionText($('naaa_period_id'));
    map.naaa_area_name              = SelectedOptionText($('naaa_area_id'));
    // popup
    window.open(
        // '/reporting/naaa_auction_report?' + MapToQueryString(map),
		'/support/Reports/AuctionTransactions.aspx?VIN=' + map.vin + "&VIC=" + map.vic,
        'auctionReport',
        'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=950,height=650'
    );
}
function getScreenDimensions(){
    var width  = 0;
    var height = 0;
    var dims = new Array();
    if (self.innerWidth)
    {
    	width = self.innerWidth;
    	height = self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientWidth)
    {
    	width = document.documentElement.clientWidth;
    	height = document.documentElement.clientHeight;
    }
    else if (document.body)
    {
    	width = document.body.clientWidth;
    	height = document.body.clientHeight;
    }
    dims.push(width);
    dims.push(height);
    return dims;
}
function staticPopup(url, name, width, height){

    var dims = getScreenDimensions();
    var xLoc = 0;
    var yLoc = 0;
    var scrWidth = parseInt(dims[0]);
    var scrHeight = parseInt(dims[1]);    
    xLoc = (scrWidth / 2) - (width /2);
    yLoc = (scrHeight / 2) - (height /2);
    
    return window.open(
        url,
        name,
        'location=no,status=no,menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left='+xLoc+',top='+yLoc+',width='+width+',height='+height
    );    

}
function ChangePlanningCategoryStrategy(value) {
    var active = 'false';
    var is_an_alias = 'false';
    var alias   = '';
    $('aliases').disabled = (value != 'alias')
    if (value == 'alias') {
        active = 'true';
        is_an_alias = 'true';
        alias = $F('aliases');
    }
    else if (value == 'seperate') {
        active = 'true';
    }
    else if (value == 'nothing') {
        // defaults are fine
    }
    $("business_unit_planning_category_active").value = active;
    $("business_unit_planning_category_is_an_alias").value = is_an_alias;
    $("business_unit_planning_category_alias_planning_category_id").value = alias;
}
function AliasPlanningCategory(element) {
    $("business_unit_planning_category_alias_planning_category_id").value = $F(element);
}
function ValidatePlanningCategoryStrategy() {
    var choices = ['alias','seperate','nothing'];
    for (var i = 0; i < choices.length; i++) {
        var element = $('choice_' + choices[i]);
        if (element.checked) {
            return true;
        }
    }
    return false;
}
function CarfaxReportPopup(vin, reportType, hotListings) {
  var cccFlagFromTile = hotListings || false;
  var url = "/IMT/CarfaxPopupAction.go?vin=" + vin + "&reportType=" + reportType + "&cccFlagFromTile=" + cccFlagFromTile;
  var w = 924;
  var h = 700;
  staticPopup(url, 'carfax', w, h);
}
function AutoCheckReportPop (dealerId,vin) {
	var url = "/support/Reports/AutoCheckReport.aspx?DealerId="+dealerId+"&Vin="+vin;
	var w = 924;
    var h = 700;
	staticPopup(url, 'autocheck', w, h);
}
function toggleArrow(el) {
    var twistie = el.className;
    if (twistie == 'closedArrow') {
        el.className = 'openArrow';
    } else {
        el.className = 'closedArrow';
    }
}
// Partials

// resets a single panel in the search module, a section of the overall form
function resetSearchPanel(id){
    var formEles = $(id).getElementsByTagName('input');
    for (var i = 0; i < formEles.length; i++) {
        if (formEles[i].type == 'checkbox') {
            formEles[i].checked = false;
        }
        else if (formEles[i].type == 'text') {
            formEles[i].value = '';
        }
        else if (formEles[i].type == 'hidden') {
            if (formEles[i].id.indexOf('lower_bound') != -1 ||
                formEles[i].id.indexOf('upper_bound') != -1) {
                    formEles[i].value='';
            }
        }
    }
    var slider = window['slider_'+id];
    if (slider) {
        slider.reset();
    }
}

function setSearchName(name) {
    if ($('search_name'))
        $('search_name').value = name;
}

function resetSearchForm(form) {
    var inequality_types = ['total_mileage','yearly_mileage', 'list_price', 'unit_cost', 'book_value', 'book_vs_cost','inventory_age'];
    $(form).reset(); // reset the search form
    var slider;
    for (var i=0; i < inequality_types.length; i++) {
       if (window['slider_'+inequality_types[i]]) {
            slider = window['slider_'+inequality_types[i]];
            slider.reset();
       }
    }
}

function FlashLocateFormManager(){
    
    this.showForm = _showForm;
    this.hideForm = _hideForm;
    
    function _showForm(){
        Element.show('flash_locate');
        $('dummy_locate').style.visibility="hidden";
    }
    
    function _hideForm(){
        Element.hide('flash_locate');
        $('dummy_locate').style.visibility="visible";
    }
    
    //this validation should be backed by a model, ideally.
    this.validate = function() {
    	var valid = false;
    	if($('makeText')) {
    		var makeText = $('makeText').value;
    		if(makeText) {
	    		if(makeText.length > 0) {
	    			valid = true;
	    		}
    		}
    	}
    	
    	if(!valid) {
    		alert('Please select a Make.');
    		return false;
    	}
    	
    	if($('modelText')) {
	    	var modelText = $('modelText').value;
	    	if(modelText) {
	    		$('modelText').value = modelText;
	    		if(modelText.length > 0) {
	    			return true;
	    		}
	    	}
    	}
    	
    	alert('Please enter at least the first character of the Model.');
		return false;
    }
}
var flashLocateFormManager = new FlashLocateFormManager();
function RepriceManager(){

    var lastActivator = null;
    var lastWidgit = null;
    var lastInput = null;
    this.showReprice = _showReprice;
    this.cancelReprice = _cancelReprice;
    
    function _showReprice(activator, widgit){    
        activator = $(activator);
        widgit = $(widgit);       
        _cancelReprice();
        _toggle(activator, widgit);  
         
        lastActivator = activator;
        lastWidgit = widgit;
        lastInput = widgit.getElementsByTagName('input')[0];
    }
    function _cancelReprice(){ 
    
        if(lastActivator && lastWidgit && lastInput){ 
            _toggle(lastActivator, lastWidgit);           
            lastInput.value = _formatNumber(lastActivator.innerHTML);          
            lastActivator = null;
            lastWidgit = null;
            lastInput = null;
         }
    }
    function _formatNumber(number){ 
       
        number = number.replace(/\$/g,'');      
        number = number.replace(/,/g,'');        
        return number;
    }
    function _toggle(a,w){
       if(a && w){
            if( Element.visible(a)){
                Element.hide(a);
                Element.show(w);
            }else{
                Element.show(a);
                Element.hide(w);
            }
       }
    }
    function _submitReprice(inventoryId){
    
      
        
       
    }
}
var repriceManager = new RepriceManager();


function PlanningContent(){

   var lastEdited = null;
   
   this.editContent = _editContent;
   
   function _editContent(link, content_id){       
     var loc = Position.positionedOffset(link);      
   
     $(content_id).style.top = (loc[1] + 2)+'px';
     $(content_id).style.left = (loc[0] - 265 )+'px';
      if($(lastEdited)){
        Element.hide(lastEdited);        
      }
      Element.show(content_id);      
      lastEdited = content_id;
       
   }

}
var planningEdit = new PlanningContent();
function TabController(){
    var lastSelected = null;//last selected list node
    var lastContent = null; //last content area
    this.swapContent = _swapContent;
    this.expandOptions = _expandOptions;
    this.swapTabs_nonTree = _swapContentTabs;
    
    function _swapContent(ele,content) {
        
         content = $(content);
         if(lastSelected){
            Element.removeClassName(lastSelected,'selected_node');
         }
         if(ele){                                       
            Element.addClassName(ele,'selected_node');
            lastSelected = ele;
         }
         if(content){ 
            if(!lastContent)
                lastContent = $('default_message') ? $('default_message'): null;
            if(lastContent){
            
                Element.hide(lastContent);
            }
            Element.show(content);
            lastContent = content;
         }
    }
    
    function _swapContentTabs(ele,content){
         content = $(content);
         if(lastSelected){
            Element.removeClassName(lastSelected,'active');
            
         }
         if(ele){                                       
            Element.addClassName(ele.parentNode,'active');           
            lastSelected = ele.parentNode;
         }
         if(content){            
            if(lastContent){            
                Element.hide(lastContent);
            }
            Element.show(content);
            lastContent = content;
         }    
    }
    
    function _expandOptions(parent, childBlock){
      
        var firstItem = $(childBlock).getElementsByTagName('div')[0];
        
        new Effect.toggle(childBlock,'slide',{duration:0.1}); 
          
        if(Element.hasClassName(parent,'expanded')){
            Element.removeClassName(parent,'expanded');
            Element.addClassName(parent,'contracted')
        }else{
            Element.removeClassName(parent,'contracted');
            Element.addClassName(parent,'expanded')
        }
     }
}
var tabController = new TabController();

function validateRequired(ele,defaultValue,errorDiv){
    
    ele = $(ele);
    errorDiv = $(errorDiv);
    
    if(ele.value == defaultValue){
        Element.show(errorDiv);
        return false;
    }
    return true;

}

// Calendar Code
// Calendar code
var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
    var i, a, main;
    for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
        if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
            a.disabled = true;
            if(a.getAttribute("title") == title) a.disabled = false;
        }
    }
    if (oldLink) oldLink.style.fontWeight = 'normal';
    oldLink = link;
    link.style.fontWeight = 'bold';
    return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el);        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}

function ruleValues(flagObj, val1, val2) {
	
	var rule_num = flagObj.id.split("_");
	rule_num = parseInt(rule_num[3]);
	
	if(rule_num == 2) {
		var naaaDiv = document.getElementById("auctionBox");
		var openOrClosed = naaaDiv.style.display;
		if(openOrClosed == "none") {
			val1 = "auctionTwist";
		} else {
			val1 = "rule_value_rule_2_value";
		}
	}
	
	var val1RestoreColor = "#fff949";
	var val2RestoreColor = "#fff949";

	if(rule_num == 4 || rule_num == 5) {
		val1RestoreColor = "#eeeeee";
		val2RestoreColor = "#eeeeee";
	}
	
	if(rule_num == 2 && val1 == "auctionTwist") {
		val1RestoreColor = "#eeeeee";
	}
	
    if(document.getElementById(val1)) {
        new Effect.Highlight (val1, {startcolor: '#fff949', endcolor: '#666666', restorecolor: val1RestoreColor});
    }
    if(document.getElementById(val2)) {
        new Effect.Highlight (val2, {startcolor: '#fff949', endcolor: '#666666', restorecolor: val2RestoreColor});
    }
}

function ruleFlagChanger(objFlag,whichWay) {
    if(whichWay == "over") {
        objFlag.style.textDecoration='underline';
    } else {
        objFlag.style.textDecoration='none';
    }
}

/* Navigation */

var g_menu_bar_registry = [
    "preference_menu_bar",
    "planning_mode_menu_bar"
];

function fixNavForIE() {
	if (document.all && document.getElementById) {
        for (var m = 0; m < g_menu_bar_registry.length; m++) {
    		navRoot = document.getElementById(g_menu_bar_registry[m]);
    		if (navRoot) {
    			var nodelist = navRoot.getElementsByTagName("LI");
        		for (i=0; i<nodelist.length; i++) {
        			node = nodelist[i];
        			if (node.nodeName == "LI") {
        				node.onmouseover = function() {
        					this.className += " over";
        				}
        				node.onmouseout=function() {
        					this.className = this.className.replace(" over", "");
        				}
        			}
        		}
    		}
    	}
	}
}

if (window.addEventListener) {
    window.addEventListener('load', fixNavForIE, false);
}
else if (window.attachEvent) {
    window.attachEvent('onload', fixNavForIE);
}

/* Hack in Key Event Handlers */

function registerKeyEventHandlers(callback) {
	var w = window;
	if (w.captureEvents) {
		// FF: keys
		w.captureEvents(Event.KEYDOWN | Event.KEYUP | Event.KEYPRESS);
		w.onkeyup	  = callback;
		w.onkeydown   = callback;
		w.onkeypress  = callback;
	}
	else {
		// IE: keys
		w.document.onkeyup   = callback;
		w.document.onkeydown   = callback;
		w.document.onkeypress  = callback;
	}
}

function releaseKeyEventHandlers() {
	var w = window;
	if (w.captureEvents) {
		// FF: keys
		w.releaseEvents(Event.KEYDOWN | Event.KEYUP | Event.KEYPRESS);
		w.onkeyup   = null;
		w.onkeydown   = null;
		w.onkeypress  = null;
	}
	else {
		// IE: keys
		w.document.onkeyup   = null;
		w.document.onkeydown   = null;
		w.document.onkeypress  = null;
	}
}

function eventTargetElement(event) {
	var target;
	if (event.target)
		target = event.target;
	else if (event.srcElement)
		target = event.srcElement;
	if (target.nodeType == 3) // defeat Safari bug
		target = target.parentNode;
	return target;
}

function keyCode(event) {
	var code;
	if (event.keyCode)
		code = event.keyCode;
	else if (event.which)
		code = event.which;
	return code;
}
	
function AppraisalReviewBookletEmailAddressHandler(event) {
	// get the event
	if (!event)
		event = window.event;
	// handle the event
	if (event.type == 'keydown' || event.type == 'keyup'  || event.type == 'keypress') {
        // note: do not get non-whitespace characters (like backspace)
        if (eventTargetElement(event).id == 'email_address') {
            AppraisalReviewBookletFormValidation(event);
        }
	}
	event.returnValue = true;
	return event.returnValue;
}

/* Declare DOM constants for IE */

if (!document.ELEMENT_NODE) {
    document.ELEMENT_NODE = 1;
    document.ATTRIBUTE_NODE = 2;
    document.TEXT_NODE = 3;
    document.CDATA_SECTION_NODE = 4;
    document.ENTITY_REFERENCE_NODE = 5;
    document.ENTITY_NODE = 6;
    document.PROCESSING_INSTRUCTION_NODE = 7;
    document.COMMENT_NODE = 8;
    document.DOCUMENT_NODE = 9;
    document.DOCUMENT_TYPE_NODE = 10;
    document.DOCUMENT_FRAGMENT_NODE = 11;
    document.NOTATION_NODE = 12;
}

/* enumerate intrinsic events */

document.INTRINSIC_EVENTS = [
    "onload",
    "onunload",
    "onclick",
    "ondblclick",
    "onmousedown",
    "onmouseup",
    "onmouseover",
    "onmousemove",
    "onmouseout",
    "onfocus",
    "onblur",
    "onkeypress",
    "onkeydown",
    "onkeyup",
    "onsubmit",
    "onreset",
    "onselect",
    "onchange"
];

/* Create a "good-enough" importNode method */

document._importNode = function(node, allChildren) {
    /* find the node type to import */
    switch (node.nodeType) {
        case document.ELEMENT_NODE:
            /* create a new element */
            var newNode = document.createElement(node.nodeName);
            /* does the node have any attributes to add? */
            if (node.attributes && node.attributes.length > 0)
                /* add all of the attributes */
                for (var i = 0, il = node.attributes.length; i < il; i++) {
                    var nodeName = node.attributes[i].nodeName;
                    var nodeValue = node.getAttribute(node.attributes[i].nodeName);
                    if (nodeName == "class") {
                        newNode.className = nodeValue;
                    }
                    else {
                        if (nodeName == "colspan") {
                            nodeName = "colSpan"; // FIX IE
                        }
                        newNode.setAttribute(nodeName, nodeValue);
                    }
                }
            /* are we going after children too, and does the node have any? */
            if (allChildren && node.childNodes && node.childNodes.length > 0)
                /* recursively get all of the child nodes */
                for (var i = 0, il = node.childNodes.length; i < il;)
                    newNode.appendChild(document._importNode(node.childNodes[i++], allChildren));
            return newNode;
            break;
        case document.TEXT_NODE:
        case document.CDATA_SECTION_NODE:
        case document.COMMENT_NODE:
            return document.createTextNode(node.nodeValue);
            break;
    }
}

/* Create a method to register events for the element which is now-in-the-document */

document._processEvents = function(node, allChildren) {
    /* find the node type to import */
    switch (node.nodeType) {
        case document.ELEMENT_NODE:
            /* does the node have any attributes to add? */
            if (node.attributes && node.attributes.length > 0) {
                /* add all of the attributes */
                for (var i = 0, il = node.attributes.length; i < il; i++) {
                    var nodeName = node.attributes[i].nodeName;
                    var nodeValue = node.getAttribute(node.attributes[i].nodeName);
                    if (nodeValue == null) {
                        continue;
                    }
                    for (var j = 0; j < document.INTRINSIC_EVENTS.length; j++) {
                        if (nodeName == document.INTRINSIC_EVENTS[j]) {
                            try {
                                var scriptText = nodeValue;
                                Event.observe(node, nodeName.substring(2,nodeName.length), function(e) {
                                    eval(scriptText);
                                });
                            } catch (e) {
                                alert(e.description);
                            }
                            break;
                        }
                    }
                }
            }
            /* are we going after children too, and does the node have any? */
            if (allChildren && node.childNodes && node.childNodes.length > 0)
                /* recursively get all of the child nodes */
                for (var i = 0, il = node.childNodes.length; i < il;)
                    document._processEvents(node.childNodes[i++], allChildren);
            break;
        case document.TEXT_NODE:
        case document.CDATA_SECTION_NODE:
        case document.COMMENT_NODE:
            break;
    }
}

/* Method to add tbody elements after the given element */

function tBodyReplace(elementId, responseText) {
    /* get a handle on the tbody with the calling script */
    var script = document.getElementById(elementId);
    /* loop over every tbody in the response */
    var jdx = 0;
    var idx = responseText.indexOf("</tbody>", jdx);
    var tbody;
    while (idx != -1) {
        /* extract tbody xhtml */
        html = responseText.substring(jdx, idx+8);
        /* new tbody element */
        var oDomDoc = (new DOMParser()).parseFromString(html, "text/xml");
        if (Sarissa.getParseErrorText(oDomDoc) == Sarissa.PARSED_OK) {
            tbody = document._importNode(oDomDoc.documentElement, true);
            script.parentNode.insertBefore(tbody, script.nextSibling);
            document._processEvents(tbody);
        }
        else {
            alert(Sarissa.getParseErrorText(oDomDoc));
        }
        /* next element */
        jdx = idx+8;
        idx = responseText.indexOf("</tbody>", jdx);
    }
    /* get rid of the script element */
    script.parent.removeChild(script);
}
