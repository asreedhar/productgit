// <script>

// Copyright (C) 2005 Ilya S. Lyubinskiy. All rights reserved.
// Technical support: http://www.php-development.ru/
//
// YOU MAY NOT
// (1) Remove or modify this copyright notice.
// (2) Distribute this code, any part or any modified version of it.
//     Instead, you can link to the homepage of this code:
//     http://www.php-development.ru/javascripts/dropdown.php.
//
// YOU MAY
// (1) Use this code on your website.
// (2) Use this code as a part of another product provided that
//     this code will be a small part of this product.
//
// NO WARRANTY
// This code is provided "as is" without warranty of any kind, either
// expressed or implied, including, but not limited to, the implied warranties
// of merchantability and fitness for a particular purpose. You expressly
// acknowledge and agree that use of this code is at your own risk.


// USAGE
//
// function popup_show(id, drag_id, exit_id, position, x, y, position_id)
//
// id          - id of a popup window;
// drag_id     - id of an element within popup window intended for dragging it
// exit_id     - id of an element within popup window intended for hiding it
// position    - positioning type:
//               "screen-corner", "screen-center"
//               "mouse-corner" , "mouse-center"
//               "element-right", "element-bottom"
// x, y        - offset
// position_id - for the last two types of positioning popup window will be
//               positioned relative to this element

function Popup() { // Namespace added 11/15/2006 SBW

// ----- Variables -------------------------------------------------------------

var popup_dragging = false;
var popup_target;
var popup_mouseX;
var popup_mouseY;
var popup_mouseposX;
var popup_mouseposY;
var popup_oldfunction;
var element;
var ie = navigator.appName == "Microsoft Internet Explorer";

// ----- Functions -------------------------------------------------------------
// Namespace added 11/15/2006 SBW

this.popup_display = popup_display;
this.popup_mousedown = popup_mousedown;
this.popup_mousemove = popup_mousemove;
this.popup_mouseup =popup_mouseup;
this.popup_exit = popup_exit;
this.popup_show = popup_show;
this.popup_mousepos = popup_mousepos;

function popup_display(x)
{
  var win = window.open();
  for (var i in x) win.document.write(i+' = '+x[i]+'<br>');
}

// ----- popup_mousedown -------------------------------------------------------

function popup_mousedown(e)
{

  if ( ie && window.event.button != 1) return;
  if (!ie && e.button            != 0) return;

  popup_dragging = true;
  popup_target   = this['target'];
  popup_mouseX   = ie ? window.event.clientX : e.clientX;
  popup_mouseY   = ie ? window.event.clientY : e.clientY;

  if (ie)
       popup_oldfunction      = document.onselectstart;
  else popup_oldfunction      = document.onmousedown;

  if (ie)
       document.onselectstart = new Function("return false;");
  else document.onmousedown   = new Function("return false;");
}

// ----- popup_mousemove -------------------------------------------------------

function popup_mousemove(e)
{
  if (!popup_dragging) return;

  var mouseX = ie ? window.event.clientX : e.clientX;
  var mouseY = ie ? window.event.clientY : e.clientY;

  // Stop Window Exiting Screen added 04/20/2007 SBW
  var maxHeight = document.documentElement.clientHeight + document.documentElement.scrollTop;
  var maxWidth = document.documentElement.clientWidth;
  var newX =  (element.offsetLeft+mouseX-popup_mouseX);
  var newY = (element.offsetTop +mouseY-popup_mouseY);
  if (newX + element.clientWidth > maxWidth-1) {
    newX = (maxWidth-1) - element.clientWidth;
  }
  else if (newX < 1) {
    newX = 1;
  }
  if (newY + element.clientHeight > maxHeight -1) {
    newY =(maxHeight-1) - element.clientHeight;
  }
  else if (newY < document.documentElement.scrollTop + 1) {
    newY = document.documentElement.scrollTop + 1;
  }
  element.style.left = newX+'px';
  element.style.top  = newY+'px';
  element.left = newX+'px';
  element.top = newY+'px';
  // element.style.left = (element.offsetLeft+mouseX-popup_mouseX)+'px';
  // element.style.top  = (element.offsetTop +mouseY-popup_mouseY)+'px';

  popup_mouseX = ie ? window.event.clientX : e.clientX;
  popup_mouseY = ie ? window.event.clientY : e.clientY;
}

// ----- popup_mouseup ---------------------------------------------------------

function popup_mouseup(e)
{
  if (!popup_dragging) return;
  popup_dragging = false;

  
  if (ie)
       document.onselectstart = popup_oldfunction;
  else document.onmousedown   = popup_oldfunction;
}

// ----- popup_exit ------------------------------------------------------------

function popup_exit(e)
{
  
  popup_mouseup(e);
  element.style.visibility = 'hidden';
  element.style.display    = 'none';
}


// ----- popup_show ------------------------------------------------------------

function popup_show(id, drag_id, exit_id, position, x, y, position_id)
{
  
  element      = document.getElementById(id);

  drag_element = document.getElementById(drag_id);

  // 11/15/2006 SBW
  // exit_element = document.getElementById(exit_id);
  if (this.isArray(exit_id)) {
    exit_element = new Array();
    for (var i = 0; i < exit_id.length; i++) {
      exit_element[i] = document.getElementById(exit_id[i]);
    }
  }
  else {
    exit_element = document.getElementById(exit_id);
  }

  element.style.position   = "absolute";
  element.style.visibility = "visible";
  element.style.display    = "block";

  if (position == "screen-corner")
  {
    element.style.left = (document.documentElement.scrollLeft+x)+'px';
    element.style.top  = (document.documentElement.scrollTop +y)+'px';
  }

  if (position == "screen-center")
  {
    element.style.left = (document.documentElement.scrollLeft+(document.body.clientWidth -element.clientWidth )/2+x)+'px';
    element.style.top  = (document.documentElement.scrollTop +(document.body.clientHeight-element.clientHeight)/2+y)+'px';
  }

  if (position == "mouse-corner")
  {
    element.style.left = (document.documentElement.scrollLeft+popup_mouseposX+x)+'px';
    element.style.top  = (document.documentElement.scrollTop +popup_mouseposY+y)+'px';
  }

  if (position == "mouse-center")
  {
    element.style.left = (document.documentElement.scrollLeft+popup_mouseposX-element.clientWidth /2+x)+'px';
    element.style.top  = (document.documentElement.scrollTop +popup_mouseposY-element.clientHeight/2+y)+'px';
  }

  if (position == "element-right" || position == "element-bottom")
  {
    var position_element = document.getElementById(position_id);

    for (var p = position_element; p; p = p.offsetParent)
      if (p.style.position != 'absolute')
      {
        x += p.offsetLeft;
        y += p.offsetTop ;
      }

    if (position == "element-right" ) x += position_element.clientWidth;
    if (position == "element-bottom") y += position_element.clientHeight;

    element.style.left = x+'px';
    element.style.top  = y+'px';
  }

  drag_element['target']   = id;
  drag_element.onmousedown = popup_mousedown;

  // 11/15/2006 SBW
  // exit_element.onclick  = popup_exit;
  if (exit_element) {
    if (this.isArray(exit_element)) {
   
      for (var i = 0; i < exit_element.length; i++) {
        exit_element[i].onclick = this.popup_exit;	
       
      }
    }
    else {
      exit_element.onclick = this.popup_exit;
    }
  }

}

// ----- popup_mousepos --------------------------------------------------------

function popup_mousepos(e)
{
 

  popup_mouseposX = ie ? window.event.clientX : e.clientX;
  popup_mouseposY = ie ? window.event.clientY : e.clientY;
}

// ----- Attach Events ---------------------------------------------------------

if (ie)
     document.attachEvent('onmousedown', popup_mousepos);
else document.addEventListener('mousedown', popup_mousepos, false);

if (ie)
     document.attachEvent('onmousemove', popup_mousemove);
else document.addEventListener('mousemove', popup_mousemove, false);

if (ie)
     document.attachEvent('onmouseup', popup_mouseup);
else document.addEventListener('mouseup', popup_mouseup, false);

// ----- Utility Methods -------------------------------------------------------
// 15/11/2005 SBW

this.isArray    = isArray;
this.isObject   = isObject;
this.isFunction = isFunction;

function isArray(obj)
{
    return this.isObject(obj) && obj.constructor == Array;
}

function isObject(a)
{
    return (a && typeof a == 'object') || this.isFunction(a);
}

function isFunction(a)
{
    return typeof a == 'function';
}

} // Namespace added 11/15/2006 SBW
