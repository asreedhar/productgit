// Window Scroll Positioning - for the planning pane
    Position.Window = {
        //extended prototypes position to return the scrolled window deltas
        getDeltas: function() {
            var deltaX =  window.pageXOffset
                || document.documentElement.scrollLeft
                || document.body.scrollLeft
                || 0;
            var deltaY =  window.pageYOffset
                || document.documentElement.scrollTop
                || document.body.scrollTop
                || 0;
            return [deltaX, deltaY];
        },
        size: function() {
            var winWidth, winHeight, d=document;
            if (typeof window.innerWidth!='undefined') {
                winWidth = window.innerWidth;
                winHeight = window.innerHeight;
            } else {
                if (d.documentElement && typeof d.documentElement.clientWidth!='undefined' && d.documentElement.clientWidth!=0) {
                    winWidth = d.documentElement.clientWidth
                    winHeight = d.documentElement.clientHeight
                } else {
                    if (d.body && typeof d.body.clientWidth!='undefined') {
                        winWidth = d.body.clientWidth
                        winHeight = d.body.clientHeight
                    }
                }
            }
            return [winWidth, winHeight];
        }
    }
    //custom effect that calls the Effect.Move Scriptaculous effect with the correct window offsets
    Effect.KeepFixed = function(element, offsetx, offsety) {
        var _scroll = Position.Window.getDeltas();
        var _window = Position.Window.size();
        var elementDimensions = Element.getDimensions(element);
        var eWidth = elementDimensions.width;
        var eHeight = elementDimensions.height;
            if (_scroll[1]==0 || _window[1]<eHeight) {
                var moveY = 0;
            }
            else {
                var moveY = _scroll[1] + offsety;
            }
        return new Effect.Move(element, { y: moveY, mode: 'absolute' });        
    }

    //the function that calls the KeepFixed effect.  It accepts the id, and offsets
    function feedback() {
        new Effect.KeepFixed('InventoryForm', 0, -50);
    }

    //and endless loop that continually positions the fixed element - call onscroll instead
    new PeriodicalExecuter(feedback, 1);