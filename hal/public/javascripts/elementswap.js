
// getElementById 
function findObj(n, d) { 
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

// swap element properties
function changeProp(objName,x,theProp,theValue) { 
  var obj = findObj(objName);
  if (obj && (theProp.indexOf("style.")==-1 || obj.style)){
    if (theValue == true || theValue == false)
      eval("obj."+theProp+"="+theValue);
    else eval("obj."+theProp+"='"+theValue+"'");
  }
}


// getElementByClassName and returns an array
var classname = 'inventory'; 
var classname1 = 'inventory';// off
var classname2 = 'inventory_select';// on

function getElementsByClassName(classname) {
    var my_array = document.getElementById('Inventory').getElementsByTagName('*');
	//var my_array = document.getElementsByTagName("*");
	var retvalue = new Array();
	var i;
	var j;

	for (i=0,j=0;i<my_array.length;i++) {
		var c = " " + my_array[i].className + " ";
		if (c.indexOf(" " + classname + " ") != -1) retvalue[j++] = my_array[i];
	}
	return retvalue;
}

// swap class names
function toggleClass(srcElement) {
	var divs = getElementsByClassName(classname2);
	for(i=0; i <divs.length;i++) {
		divs[i].className = classname1;
	}
	srcElement.className = classname2;
	classname = classname1;
}