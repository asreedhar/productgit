
USE IMT
GO

--
-- New Inventory Bucket
--

INSERT INTO dbo.InventoryBuckets ([Description], [InventoryType]) VALUES ('Aging Inventory - Short Leash', 2)
GO

-- Lights == 1 (red)
-- 1 & 2^(1-1) = 1 & 1 = 001 & 001 = 1
-- 1 & 2^(2-1) = 1 & 2 = 001 & 010 = 0
-- 1 & 2^(3-1) = 1 & 4 = 001 & 100 = 0

INSERT INTO [IMT].[dbo].[InventoryBucketRanges](
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
)
VALUES (5, 1, 'Red Light Vehicles', 21, 30, 1, 100150, 'Days in inventory thresholds for red light vehicles for warning- and error-operational insights', 1)
GO

-- Lights == 2 (yellow)
-- 2 & 2^(1-1) = 2 & 1 = 010 & 001 = 0
-- 2 & 2^(2-1) = 2 & 2 = 010 & 010 = 1
-- 2 & 2^(3-1) = 2 & 4 = 010 & 100 = 0

INSERT INTO [IMT].[dbo].[InventoryBucketRanges](
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
)
VALUES (5, 2, 'Yellow Light Vehicles', 30, 45, 2, 100150, 'Days in inventory thresholds for yellow light vehicles for warning- and error-operational insights', 2)
GO

-- Lights == 4 (green)
-- 4 & 2^(1-1) = 4 & 1 = 100 & 001 = 0
-- 4 & 2^(2-1) = 4 & 2 = 100 & 010 = 0
-- 4 & 2^(3-1) = 4 & 4 = 100 & 100 = 1

INSERT INTO [IMT].[dbo].[InventoryBucketRanges](
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
)
VALUES (5, 3, 'Green Light Vehicles', 45, 60, 4, 100150, 'Days in inventory thresholds for green light vehicles for warning- and error-operational insights', 3)
GO

--
-- Rollup of the segment days supply function.
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetDaysSupplyRollup]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetDaysSupplyRollup]
GO

CREATE FUNCTION dbo.GetDaysSupplyRollup(@BusinessUnitID INT, @BaseDate DATETIME)
RETURNS TABLE
AS
RETURN
(
	select 	cast(count(*) / SR.SalesRate as decimal(9,5)) DaysSupply
	from 	[IMT].dbo.Inventory I
		join [IMT].dbo.DealerPreference PR on I.BusinessUnitID = PR.BusinessUnitID
		cross join (
			select	count(*) / cast(DP.DaysInPeriod as decimal(9,5)) SalesRate
			from	[IMT].dbo.Inventory I
				join [IMT].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
				join [IMT].dbo.GetCompositeBasisPeriod(@BusinessUnitID,@BaseDate,'StoreTargetInventory') CBP on VS.DealDate between CBP.BeginDate and CBP.EndDate
				join [IMT].dbo.DealerPreference PR on I.BusinessUnitID = PR.BusinessUnitID
				cross join (	select	sum(datediff(dd,BeginDate,EndDate) + 1) DaysInPeriod
						from	[IMT].dbo.GetCompositeBasisPeriod(@BusinessUnitID,@BaseDate,'StoreTargetInventory')	
						) DP		
			where	I.BusinessUnitID = @BusinessUnitID
				and I.InventoryType = 2 	
				and VS.SaleDescription = 'R'
				and I.UnitCost between PR.UnitCostThresholdLower and PR.UnitCostThresholdUpper
			group
			by	DP.DaysInPeriod
		) SR
	where	I.BusinessUnitID = @BusinessUnitID
		and I.UnitCost between PR.UnitCostThresholdLower and PR.UnitCostThresholdUpper
		and dbo.ToDate(@BaseDate) between InventoryReceivedDate and isnull(DeleteDt, '6/6/2079')	-- max small date time
		and I.InventoryType = 2 	-- Used
		AND NOT EXISTS ( 
			SELECT 1 
			FROM [IMT].dbo.AIP_Event E 
			JOIN [IMT].dbo.AIP_EventType T ON T.AIP_EventTypeID = E.AIP_EventTypeID 
			WHERE E.InventoryID = I.InventoryID 
			AND T.AIP_EventTypeID = E.AIP_EventTypeID 
			AND T.AIP_EventCategoryID IN (2,3) 
		)
	group
	by	SR.SalesRate
)
GO

--
-- DROP TABLE COMMANDS
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberInsightTypePreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MemberInsightTypePreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerInsightTargetPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.DealerInsightTargetPreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightTarget]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightTarget
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightTargetValueType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightTargetValueType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightTargetType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightTargetType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightReflection]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightReflection
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightAdjectival]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightAdjectival
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightCategory
GO

--
-- Reference Table: Insight Category
--

CREATE TABLE dbo.InsightCategory (
	[InsightCategoryID] INT NOT NULL,
	[Name]              VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightCategory PRIMARY KEY (
		InsightCategoryID
	)
)
GO

INSERT INTO dbo.InsightCategory ([InsightCategoryID], [Name]) VALUES (1, 'Appraisal Insights')
INSERT INTO dbo.InsightCategory ([InsightCategoryID], [Name]) VALUES (2, 'Operation Insights')
GO

--
-- Reference Table: Insight Type
--

CREATE TABLE dbo.InsightType (
	[InsightTypeID]     INT NOT NULL,
	[InsightCategoryID] INT NOT NULL,
	[Name]              VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightType PRIMARY KEY (
		InsightTypeID
	),
	CONSTRAINT FK_InsightType_InsightCategory FOREIGN KEY (
		InsightCategoryID
	)
	REFERENCES dbo.InsightCategory (
		InsightCategoryID
	)
)
GO

INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (1, 1, 'Insufficient Sales History')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (2, 1, 'Average Gross Profit')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (3, 1, 'Frequent No Sales')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (4, 1, 'Average Margin')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (5, 1, 'Contribution Rank')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (6, 1, 'Average Days to Sale')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (7, 1, 'Traffic Light')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (8, 1, 'Stocking Levels')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (9, 1, 'High Mileage')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (10, 1, 'Older Vehicle')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (11, 1, 'Annualized ROI')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (12, 1, 'Make/Model Market Share Percentage (in Segment)')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (13, 1, 'Make/Model Market Share Rank (in Segment)')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (14, 1, 'Model Year Market Share Percentage')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (15, 1, 'Model Year Market Share Rank')
GO

INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (16, 2, 'Appraisal Closing Rate')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (17, 2, 'Make a Deal')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (18, 2, 'Trade-In Inventory Analyzed')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (19, 2, 'Average Immediate Wholesale Profit')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (20, 2, 'Current Inventory - Short Leash')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (21, 2, 'Current Inventory - Unplanned Vehicles')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (22, 2, 'Current Inventory - Days Supply')
GO

--
-- Reference Table: Insight Adjectival
--

CREATE TABLE dbo.InsightAdjectival (
	[InsightAdjectivalID] INT NOT NULL,
	[Name]                VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightAdjectival PRIMARY KEY (
		InsightAdjectivalID
	)
)
GO

INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (1, 'None')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (2, 'Insufficent')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (3, 'Faster')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (4, 'Slower')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (5, 'More')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (6, 'Less')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (7, 'Higher')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (8, 'Lower')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (9, 'Under')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (10, 'Over')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (11, 'High')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (12, 'Older')
GO

INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (13, 'Profit')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (14, 'Loss')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (15, 'Below')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (16, 'Above')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (17, 'On')
GO

--
-- Reference Table: Insight Reflection (Suggestions and Warnings)
--

CREATE TABLE dbo.InsightReflection (
	[InsightReflectionID] INT NOT NULL,
	[Name]                VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightReflection PRIMARY KEY (
		InsightReflectionID
	)
)
GO

INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (1, 'Low Trade Analyzer Usage')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (2, 'Inadequate Trade Analyzer Usage')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (3, 'Wholesale Profit Hurting Closing Rate?')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (4, 'Losing Retail Deals?')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (5, 'Over-paying for trade-ins but it is not helping your closing rate')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (6, 'Consider for wholesale?')
GO

CREATE TABLE dbo.InsightTargetType (
	[InsightTargetTypeID] INT         NOT NULL,
	[Name]                VARCHAR(50) NOT NULL,
	CONSTRAINT PK_InsightTargetType PRIMARY KEY (
		InsightTargetTypeID
	)
)
GO

INSERT INTO dbo.InsightTargetType ([InsightTargetTypeID], [Name]) VALUES (1, 'N/A')
INSERT INTO dbo.InsightTargetType ([InsightTargetTypeID], [Name]) VALUES (2, 'Range')
INSERT INTO dbo.InsightTargetType ([InsightTargetTypeID], [Name]) VALUES (3, 'Value')
GO

CREATE TABLE dbo.InsightTargetValueType (
	[InsightTargetValueTypeID] INT         NOT NULL,
	[Name]                     VARCHAR(50) NOT NULL,
	CONSTRAINT PK_InsightTargetValueType PRIMARY KEY (
		InsightTargetValueTypeID
	)
)
GO

INSERT INTO dbo.InsightTargetValueType ([InsightTargetValueTypeID], [Name]) VALUES (1, 'Currency')
INSERT INTO dbo.InsightTargetValueType ([InsightTargetValueTypeID], [Name]) VALUES (2, 'Percentage')
INSERT INTO dbo.InsightTargetValueType ([InsightTargetValueTypeID], [Name]) VALUES (3, 'Number')
GO

CREATE TABLE dbo.InsightTarget (
	[InsightTargetID]          INT IDENTITY(1,1) NOT NULL,
	[InsightTargetTypeID]      INT NOT NULL,
	[InsightTargetValueTypeID] INT NOT NULL,
	[InsightTypeID]            INT NOT NULL,
	[InsightContext]           INT NULL,
	[Name]                     VARCHAR(255) NOT NULL,
	[Value]                    INT NULL,
	[Min]                      INT NULL,
	[Max]                      INT NULL,
	[Threshold]                INT NULL,
	CONSTRAINT PK_InsightTarget PRIMARY KEY (
		InsightTargetID
	),
	CONSTRAINT FK_InsightTarget_InsightTargetType FOREIGN KEY (
		InsightTargetTypeID
	)
	REFERENCES dbo.InsightTargetType (
		InsightTargetTypeID
	),
	CONSTRAINT FK_InsightTarget_InsightTargetValueType FOREIGN KEY (
		InsightTargetValueTypeID
	)
	REFERENCES dbo.InsightTargetValueType (
		InsightTargetValueTypeID
	),
	CONSTRAINT FK_InsightTarget_InsightType FOREIGN KEY (
		InsightTypeID
	)
	REFERENCES dbo.InsightType (
		InsightTypeID
	)
)
GO

INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 16, NULL, 'Appraisal Closing Rate', 60, NULL, NULL, 20)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 17, NULL, 'Make a Deal', NULL, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 2, 18, NULL, 'Trade-In Inventory Analyzed', 100, NULL, NULL, 20)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (2, 1, 19, NULL, 'Average Immediate Wholesale Profit', NULL, -250, 250, 500)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 20, 1, 'Current Inventory - Short Leash - Red', NULL, NULL, NULL, 2)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 20, 2, 'Current Inventory - Short Leash - Yellow', NULL, NULL, NULL, 2)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 20, 3, 'Current Inventory - Short Leash - Green', NULL, NULL, NULL, 2)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 21, NULL, 'Current Inventory - Unplanned Vehicles', NULL, NULL, NULL, 5)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, NULL, 'Current Inventory - Days Supply', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 2, 'Current Inventory - Days Supply - Truck', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 3, 'Current Inventory - Days Supply - Sedan', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 4, 'Current Inventory - Days Supply - Coupe', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 5, 'Current Inventory - Days Supply - Van', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 6, 'Current Inventory - Days Supply - SUV', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 7, 'Current Inventory - Days Supply - Convertible', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 22, 8, 'Current Inventory - Days Supply - Wagon', 45, NULL, NULL, 10)
GO

--
-- Instance Table: Dealer (BusinessUnit type 4) override of the Insight Target defaults.
--

CREATE TABLE dbo.DealerInsightTargetPreference (
	[DealerInsightTargetPreferenceID] INT IDENTITY(1,1) NOT NULL,
	[BusinessUnitID]                  INT NOT NULL,
	[InsightTargetID]                 INT NOT NULL,
	[Value]                           INT NULL,
	[Min]                             INT NULL,
	[Max]                             INT NULL,
	[Threshold]                       INT NULL,
	CONSTRAINT PK_DealerInsightTargetPreference PRIMARY KEY (
		DealerInsightTargetPreferenceID
	),
	CONSTRAINT FK_DealerInsightTargetPreference_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES dbo.BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_DealerInsightTargetPreference_InsightTarget FOREIGN KEY (
		InsightTargetID
	)
	REFERENCES dbo.InsightTarget (
		InsightTargetID
	)
)
GO

--
-- View: Targets and Thresholds for the BusinessUnit falling back to the defaults.
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerInsightTarget]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[DealerInsightTarget]
GO

CREATE VIEW dbo.DealerInsightTarget (
	[BusinessUnitID],
	[InsightTargetID],
	[InsightTargetTypeID],
	[InsightTargetValueTypeID],
	[InsightTypeID],
	[InsightContext],
	[Name],
	[Value],
	[Min],
	[Max],
	[Threshold]
)
AS
SELECT
	B.[BusinessUnitID],
	T.[InsightTargetID],
	T.[InsightTargetTypeID],
	T.[InsightTargetValueTypeID],
	T.[InsightTypeID],
	T.[InsightContext],
	T.[Name],
	COALESCE(P.[Value], T.[Value]) Value,
	COALESCE(P.[Min], T.[Min]) [Min],
	COALESCE(P.[Max], T.[Max]) [Max],
	COALESCE(P.[Threshold],T.[Threshold]) [Threshold]
FROM
                dbo.InsightTarget T
LEFT JOIN       dbo.DealerInsightTargetPreference P ON P.InsightTargetID = T.InsightTargetID
CROSS JOIN      dbo.BusinessUnit B
GO

--
-- Instance Table: Insight Permission / Visibility
--

CREATE TABLE dbo.MemberInsightTypePreference (
	[MemberInsightTypePreference] INT IDENTITY(1,1) NOT NULL,
	[BusinessUnitID]              INT NOT NULL,
	[MemberID]                    INT NOT NULL,
	[InsightTypeID]               INT NOT NULL,
	[Active]                      BIT NOT NULL,
	CONSTRAINT PK_MemberInsightType PRIMARY KEY (
		MemberInsightTypePreference
	),
	CONSTRAINT FK_MemberInsightType_InsightType FOREIGN KEY (
		InsightTypeID
	)
	REFERENCES dbo.InsightType (
		InsightTypeID
	),
	CONSTRAINT FK_MemberInsightType_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES dbo.BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_MemberInsightType_Member FOREIGN KEY (
		MemberID
	)
	REFERENCES dbo.Member (
		MemberID
	),
	CONSTRAINT UK_MemberInsightType UNIQUE (
		BusinessUnitID,
		MemberID,
		InsightTypeID
	)
)
GO

--
-- Instance View: Members Insight Visibility
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberInsightType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[MemberInsightType]
GO

CREATE VIEW dbo.MemberInsightType (
	[BusinessUnitID],
	[MemberID],
	[InsightTypeID]
)
AS
(
	-- standard users: 118K rows
	SELECT	MA.BusinessUnitID,
		M.MemberID,
		T.InsightTypeID
	FROM	Member M
	JOIN	MemberAccess MA ON MA.MemberID = M.MemberID
	CROSS JOIN dbo.InsightType T
	LEFT  JOIN dbo.MemberInsightTypePreference P ON (
			P.InsightTypeID   = T.InsightTypeID
		AND	MA.BusinessUnitID = P.BusinessUnitID
		AND	MA.MemberID       = P.MemberID
		)
	WHERE	M.MemberType = 2
	AND	COALESCE(P.Active,1) = 1
UNION ALL
	-- admin users: 1.3M rows
	SELECT	B.BusinessUnitID,
		M.MemberID,
		T.InsightTypeID
	FROM	Member M
	CROSS JOIN dbo.BusinessUnit B
	CROSS JOIN dbo.InsightType T
	LEFT  JOIN dbo.MemberInsightTypePreference P ON (
			T.InsightTypeID  = P.InsightTypeID
		AND	B.BusinessUnitID = P.BusinessUnitID
		AND	M.MemberID       = P.MemberID
		)
	WHERE	M.MemberType = 1
	AND	B.BusinessUnitTypeID = 4
	AND	B.Active = 1
	AND	COALESCE(P.Active,1) = 1
)
GO

--
-- Move to HAL database as the Operational Insights reference HAL tables (MAD and Planning)
--

USE HAL

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetOperationalInsights]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetOperationalInsights]
GO

CREATE PROCEDURE dbo.GetOperationalInsights(
	@BusinessUnitID INT,
	@MemberID       INT
)
AS
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF  -- REMOVE MESSAGE OCCURRING IF A NULL IS IN AN AGGREGATE
BEGIN

	DECLARE @Debug BIT
	SET @Debug = 0

	/*
	 * Today as variable for function calls.
	 */

	DECLARE @BaseDate DATETIME
	SET @BaseDate = GETDATE()

	/*
	 * Insight table.
	 */

	DECLARE @Insight TABLE (
		InsightTypeID       INT, -- operational metric
		InsightAdjectivalID INT, -- profit/loss
		InsightValue        INT, -- by so much ($)
		InsightTargetValue  INT, -- where we said wanted this value
		InsightContext      INT, -- vehicle segment etc
		InsightReflectionID INT  -- by the way ... "blah blah blah"
	)

	/*
	 * Targets, Thresholds and Supporting Data
	 */

	DECLARE @FlipsProfitTargetMin INT, @FlipsProfitTargetMax INT, @FlipsProfitThreshold INT
	DECLARE @ClosingRateTarget INT, @ClosingRateThreshold INT
	DECLARE @TradeInAnalyzedTarget INT, @TradeInAnalyzedThreshold INT
	DECLARE @MakeDealTarget INT, @MakeDealThreshold INT
	DECLARE @UnplannedTarget INT, @UnplannedThreshold INT

	SELECT	@FlipsProfitTargetMin = [Min],
		@FlipsProfitTargetMax = [Max],
		@FlipsProfitThreshold = Threshold
	FROM	[IMT].dbo.DealerInsightTarget
	WHERE	BusinessUnitId = @BusinessUnitID
	AND	InsightTypeID = 19

	IF @Debug = 1
	SELECT	@FlipsProfitTargetMin FlipsProfitTargetMin,
		@FlipsProfitTargetMax FlipsProfitTargetMax

	SELECT	@ClosingRateTarget = Value,
		@ClosingRateThreshold = Threshold
	FROM	[IMT].dbo.DealerInsightTarget
	WHERE	BusinessUnitId = @BusinessUnitID
	AND	InsightTypeID = 16

	IF @Debug = 1
	SELECT	@ClosingRateTarget ClosingRateTarget,
		@ClosingRateThreshold ClosingRateThreshold 

	SELECT	@TradeInAnalyzedTarget = Value,
		@TradeInAnalyzedThreshold = Threshold
	FROM	[IMT].dbo.DealerInsightTarget
	WHERE	BusinessUnitId = @BusinessUnitID
	AND	InsightTypeID = 20

	IF @Debug = 1
	SELECT	@TradeInAnalyzedTarget TradeInAnalyzedTarget,
		@TradeInAnalyzedThreshold TradeInAnalyzedThreshold

	SELECT	@MakeDealTarget = Value,
		@MakeDealThreshold = Threshold
	FROM	[IMT].dbo.DealerInsightTarget
	WHERE	BusinessUnitId = @BusinessUnitID
	AND	InsightTypeID = 17

	IF @Debug = 1
	SELECT	@MakeDealTarget MakeDealTarget,
		@MakeDealThreshold MakeDealThreshold

	SELECT	@UnplannedTarget = Value,
		@UnplannedThreshold = Threshold
	FROM	[IMT].dbo.DealerInsightTarget
	WHERE	BusinessUnitId = @BusinessUnitID
	AND	InsightTypeID = 21

	IF @Debug = 1
	SELECT	@UnplannedTarget UnplannedTarget,
		@UnplannedThreshold UnplannedThreshold

	DECLARE @DaysSupplyTarget INT, @DaysSupplyThreshold INT
	DECLARE @SegmentDaysSupplyTarget TABLE (
		DisplayBodyTypeID   INT,
		DaysSupplyTarget    INT,
		DaysSupplyThreshold INT
	)
	
	SELECT	@DaysSupplyTarget = COALESCE(NULLIF(P.CIATargetDaysSupply,0),45),
		@DaysSupplyThreshold = T.Threshold
	FROM	[IMT].dbo.DealerPreference P
	JOIN	[IMT].dbo.DealerInsightTarget T ON T.BusinessUnitID = P.BusinessUnitID
	WHERE	P.BusinessUnitID = @BusinessUnitID
	AND	T.InsightTypeID = 22
	AND	T.InsightContext IS NULL

	IF @Debug = 1
	SELECT	@DaysSupplyTarget DaysSupplyTarget,
		@DaysSupplyThreshold DaysSupplyThreshold

	INSERT INTO @SegmentDaysSupplyTarget (
		DisplayBodyTypeID,
		DaysSupplyTarget,
		DaysSupplyThreshold
	)
	SELECT	InsightContext,
		Value,
		Threshold
	FROM	[IMT].dbo.DealerInsightTarget
	WHERE	BusinessUnitID = @BusinessUnitID
	AND	InsightTypeID = 22
	AND	InsightContext IS NOT NULL
	
	DECLARE @InventoryCount INT, @InventoryAnalyzedCount INT, @InventoryAnalyzedPct REAL

	SELECT	@InventoryCount = COUNT(*)
	FROM	[IMT].dbo.Inventory I
	WHERE	I.BusinessUnitID = @BusinessUnitID
	AND	I.InventoryType = 2
	AND	I.InventoryActive = 1
	AND	I.TradeOrPurchase = 2

	SELECT	@InventoryAnalyzedCount = COUNT(*)
	FROM	[IMT].dbo.Inventory I CROSS JOIN [EdgeScorecard].dbo.GetBusinessUnitPreferences(@BusinessUnitID) P
	JOIN	[IMT].dbo.Appraisals A ON (
			A.BusinessUnitID = I.BusinessUnitID
		AND	A.VehicleID = I.VehicleID
		AND	DATEDIFF(DD, A.DateCreated, I.InventoryReceivedDate) BETWEEN 0 AND P.AppraisalLifeThreshold
		)
	WHERE	I.BusinessUnitID = @BusinessUnitID
	AND	I.InventoryType = 2
	AND	I.InventoryActive = 1
	AND	I.TradeOrPurchase = 2

	SET @InventoryAnalyzedPct = CAST(@InventoryAnalyzedCount AS REAL)/CAST(@InventoryCount AS REAL)

	IF @Debug = 1
	SELECT	@InventoryAnalyzedPct InventoryAnalyzedPct

	/*
	 * Appraisal Closing Rate:
	 *
	 *	"Appraisal closing rate is <x%> <above/below/at> target"
	 *
	 * if 50% < % inventory analyzed < 75% then
	 * 	"Low Trade Analyzer usage is impacting appraisal closing rate accuracy"
	 *
	 * if % inventory analyzed < 50% then
	 * 	"Trade Analyzer usage too low to determine an accurate appraisal closing rate"
	 */
	
	DECLARE @ClosingRate DECIMAL(12,2)
	
	SELECT	@ClosingRate = Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	BusinessUnitID = @BusinessUnitID
	AND	PeriodID = 2
	AND	MetricID = 1

	IF @Debug = 1
	SELECT @ClosingRate ClosingRate

	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightReflectionID
	)
	SELECT	16 InsightTypeID,
		CASE	WHEN @ClosingRate > @ClosingRateTarget THEN 16
			WHEN @ClosingRate < @ClosingRateTarget THEN 15
			ELSE 17
		END InsightAdjectivalID,
		ABS(@ClosingRate-@ClosingRateTarget) InsightValue,
		@ClosingRateTarget InsightTargetValue,
		CASE	WHEN @InventoryAnalyzedPct BETWEEN 0.5 AND 0.75 THEN 1
			WHEN @InventoryAnalyzedPct < 0.5 THEN 2
			ELSE NULL
		END InsightReflectionID
	WHERE
		@ClosingRateTarget - @ClosingRate > @ClosingRateThreshold
	
	/*
	 * Make a Deal
	 *
	 * if number of opportunities > 10 then
	 * 	"x possible deals on deck"
	 */

	DECLARE @MakeDealOpportunities INT
	
	SELECT	@MakeDealOpportunities = number_of_appraisal_reviews
	FROM	[HAL].dbo.appraisal_reviews_days_back_report r
	JOIN	[HAL].dbo.appraisal_review_preferences p on p.days_back = r.days_back
	WHERE	p.business_unit_id = @BusinessUnitID
	AND	r.business_unit_id = @BusinessUnitID

	IF @Debug = 1
	SELECT @MakeDealOpportunities MakeDealOpportunities
	
	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightReflectionID
	)
	SELECT	17 InsightTypeID,
		NULL InsightAdjectivalID,
		@MakeDealOpportunities InsightValue,
		NULL InsightTargetValue,
		NULL InsightReflectionID
	WHERE
		@MakeDealOpportunities > @MakeDealThreshold

	/*
	 * Trade-In Inventory Analyzed
	 * 
	 * "Percentage of trade-in inventory analyzed is <x%> <below/above/at> target"
	 * 
	 * "Trades taken into inventory: <y>"
	 * 
	 * "Number of trades analyzed: <y>"
	 */

	DECLARE @TradeInAnalyzed DECIMAL(12,2)
	
	SELECT	@TradeInAnalyzed = Measure*100
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	BusinessUnitID = @BusinessUnitID
	AND	PeriodID = 2
	AND	MetricID = 2

	IF @Debug = 1
	SELECT @TradeInAnalyzed TradeInAnalyzed
	
	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightReflectionID
	)
	SELECT	18 InsightTypeID,
		CASE	WHEN @TradeInAnalyzed > @TradeInAnalyzedTarget THEN 16
			WHEN @TradeInAnalyzed < @TradeInAnalyzedTarget THEN 15
			ELSE 17
		END InsightAdjectivalID,
		ABS(@TradeInAnalyzed-@TradeInAnalyzedTarget) InsightValue,
		@TradeInAnalyzedTarget InsightTargetValue,
		NULL InsightReflectionID
	WHERE
		@TradeInAnalyzedTarget - @TradeInAnalyzed > @TradeInAnalyzedThreshold
	
	/*
	 * Average Immediate Wholesale Profit
	 * 
	 * "Immediate wholesale <profit/loss> outside target range"
	 * 
	 * if flip profit > 0 then
	 *   if flip profit > positive flip profit target + flip profit threshold then
	 *     if closing-rate < closing-rate target then
	 *       "Is wholesale profit hurting your closing rate?"
	 *     else
	 *       "Are you losing retail deals?"
	 *     end
	 *   end
	 * else
	 *   if flip profit < negative flip profit target - flip profit threshold then
	 *     if closing-rate < closing-rate target then
	 *       if % trades analyzed > 75% then
	 *         "You're paying too much for trade-ins but it's not helping your closing rate. Whats wrong?"
	 *       end
	 *     else
	 *   end
	 * end
	 */

	DECLARE @FlipsProfit DECIMAL(12,2)
	
	SELECT	@FlipsProfit = Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	BusinessUnitID = @BusinessUnitID
	AND	PeriodID = 2
	AND	MetricID = 18

	IF @Debug = 1
	SELECT @FlipsProfit FlipsProfit
	
	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightReflectionID
	)
	SELECT	19 InsightTypeID,
		CASE WHEN @FlipsProfit > 0 THEN 13 ELSE 14 END InsightAdjectivalID,
		@FlipsProfit InsightValue,
		CASE WHEN @FlipsProfit > @FlipsProfitTargetMax THEN @FlipsProfitTargetMax ELSE @FlipsProfitTargetMin END InsightTargetValue,
		CASE WHEN @FlipsProfit > @FlipsProfitTargetMax THEN
			CASE WHEN @ClosingRate < @ClosingRateTarget THEN 3 ELSE 4 END
		ELSE
			CASE WHEN @ClosingRate < @ClosingRateTarget AND @TradeInAnalyzed > 75 THEN 5 ELSE NULL END
		END InsightReflectionID
	WHERE
		@FlipsProfit < @FlipsProfitTargetMin-@FlipsProfitThreshold
	OR	@FlipsProfit > @FlipsProfitTargetMax+@FlipsProfitThreshold

	/*
	 * Current Inventory - Short Leash
	 *
	 * "<x> <red/yellow/green> vehicles older than <y> days"
	 */

	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightContext,
		InsightReflectionID
	)
	SELECT	20 InsightTypeID,
		12 InsightAdjectivalID,
		COUNT(*) InsightValue,
		CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, @BaseDate) > IBR.High THEN IBR.High ELSE IBR.Low END InsightTargetValue,
		IBR.RangeID InsightContext,
		CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, @BaseDate) > IBR.High THEN 6 ELSE NULL END InsightReflectionID
	FROM	[IMT].dbo.Inventory I CROSS JOIN [IMT].dbo.GetInventoryBucketRanges(@BusinessUnitID, 5) IBR
	WHERE	DATEDIFF(DD, I.InventoryReceivedDate, @BaseDate) > IBR.Low
	AND	IBR.Lights & POWER(2,I.CurrentVehicleLight-1) = POWER(2,I.CurrentVehicleLight-1)
	AND	I.CurrentVehicleLight = InitialVehicleLight
	AND	I.BusinessUnitID = @BusinessUnitID
	AND	I.InventoryType = 2
	AND	I.InventoryActive = 1
	GROUP
	BY	IBR.RangeID,
		CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, @BaseDate) > IBR.High THEN IBR.High ELSE IBR.Low END,
		CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, @BaseDate) > IBR.High THEN 6 ELSE NULL END

	/*
	 * Current Inventory - Unplanned Vehicles
	 *
	 * "<x> unplanned vehicles"
	 */

	DECLARE @UnplannedVehicles INT

	SELECT	@UnplannedVehicles = ReplanUnits
	FROM	[IMT].dbo.GetAgingInventoryPlanSummary(@BusinessUnitID, null, 2, @BaseDate, 1)
	WHERE	RangeID = 0

	IF @Debug = 1
	SELECT @UnplannedVehicles UnplannedVehicles
	
	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightReflectionID
	)
	SELECT	21 InsightTypeID,
		NULL InsightAdjectivalID,
		@UnplannedVehicles InsightValue,
		NULL InsightTargetValue,
		NULL InsightReflectionID
	WHERE
		@UnplannedVehicles > @UnplannedThreshold
	
	/*
	 * Current Inventory - Days Supply
	 *
	 * "<x> days supply with target <y> days supply" (store level)
	 * 
	 * "<x> days <segment> with target <y> days supply" (segment level)
	 */

	DECLARE @DaysSupply INT
	DECLARE @SegmentDaysSupply TABLE (
		DisplayBodyTypeID INT,
		DaysSupply        INT
	)

	SELECT	@DaysSupply = DaysSupply
	FROM	[IMT].dbo.GetDaysSupplyRollup(@BusinessUnitID, @BaseDate)

	IF @Debug = 1
	SELECT @DaysSupply DaysSupply
	
	INSERT INTO @SegmentDaysSupply (
		DisplayBodyTypeID,
		DaysSupply
	)
	SELECT	DisplayBodyTypeID,
		DaysSupply
	FROM	[IMT].dbo.GetDaysSupply(@BusinessUnitID, @BaseDate)
	
	IF @Debug = 1
	SELECT * FROM @SegmentDaysSupply
	
	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightContext,
		InsightReflectionID
	)
	SELECT	22 InsightTypeID,
		CASE WHEN @DaysSupply < @DaysSupplyTarget THEN 15 WHEN @DaysSupply < @DaysSupplyTarget THEN 16 ELSE 17 END InsightAdjectivalID,
		@DaysSupply InsightValue,
		@DaysSupplyTarget InsightTargetValue,
		NULL InsightContext,
		NULL InsightReflectionID
	WHERE
		@DaysSupplyTarget - @DaysSupply > @DaysSupplyThreshold

	INSERT INTO @Insight(
		InsightTypeID,
		InsightAdjectivalID,
		InsightValue,
		InsightTargetValue,
		InsightContext,
		InsightReflectionID
	)
	SELECT	22 InsightTypeID,
		CASE WHEN V.DaysSupply < T.DaysSupplyTarget THEN 15 WHEN V.DaysSupply < T.DaysSupplyTarget THEN 16 ELSE 17 END InsightAdjectivalID,
		V.DaysSupply InsightValue,
		T.DaysSupplyTarget InsightTargetValue,
		V.DisplayBodyTypeID InsightContext,
		NULL InsightReflectionID
	FROM
		@SegmentDaysSupply V
	JOIN	@SegmentDaysSupplyTarget T ON V.DisplayBodyTypeID = T.DisplayBodyTypeID
	WHERE
		T.DaysSupplyTarget - V.DaysSupply > T.DaysSupplyThreshold

	/*
	 * Finished!
	 */
	
	IF @Debug = 1
	SELECT * FROM @Insight

	SELECT
		I.[InsightTypeID],
		T.[Name] InsightTypeDescription,
		I.[InsightAdjectivalID],
		A.[Name] InsightAdjectivalDescription,
		I.[InsightValue],
		I.[InsightTargetValue],
		I.[InsightContext],
		I.[InsightReflectionID],
		R.[Name] InsightReflectionDescription
	FROM
		@Insight I
	JOIN	[IMT].dbo.InsightType T ON I.InsightTypeID = T.InsightTypeID
	JOIN	[IMT].dbo.MemberInsightType M ON I.InsightTypeID = M.InsightTypeID
	LEFT JOIN [IMT].dbo.InsightAdjectival A ON I.InsightAdjectivalID = A.InsightAdjectivalID
	LEFT JOIN [IMT].dbo.InsightReflection R ON I.InsightReflectionID = R.InsightReflectionID
	WHERE
		M.MemberID       = @MemberID
	AND	M.BusinessUnitID = @BusinessUnitID
	ORDER BY
		I.InsightTypeID
	
	RETURN

END
GO

-- exec GetOperationalInsights @BusinessUnitID=100147, @MemberId=100000

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_unit_insight_target_preferences]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_unit_insight_target_preferences]
GO

CREATE VIEW dbo.business_unit_insight_target_preferences (
	[id],
	[business_unit_id],
	[insight_target_id],
	[value],
	[min],
	[max],
	[threshold]
)
AS
SELECT
	[DealerInsightTargetPreferenceID],
	[BusinessUnitID],
	[InsightTargetID],
	[Value],
	[Min],
	[Max],
	[Threshold]
FROM
	[IMT].dbo.DealerInsightTargetPreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_unit_insight_targets]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_unit_insight_targets]
GO

CREATE VIEW dbo.business_unit_insight_targets (
	[id],
	[business_unit_id],
	[insight_target_id],
	[insight_target_type_id],
	[insight_target_value_type_id],
	[insight_type_id],
	[insight_context],
	[name],
	[value],
	[min],
	[max],
	[threshold]
)
AS
SELECT
	CAST([BusinessUnitID] AS VARCHAR) + '_' + CAST([InsightTargetID] AS VARCHAR) + '_' + CAST([InsightContext] AS VARCHAR),
	[BusinessUnitID],
	[InsightTargetID],
	[InsightTargetTypeID],
	[InsightTargetValueTypeID],
	[InsightTypeID],
	[InsightContext],
	[Name],
	[Value],
	[Min],
	[Max],
	[Threshold]
FROM
	[IMT].dbo.DealerInsightTarget
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[insight_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[insight_categories]
GO

CREATE VIEW dbo.insight_categories (
	[id],
	[name]
)
AS
SELECT
	[InsightCategoryID],
	[Name]
FROM
	[IMT].dbo.InsightCategory
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[insight_targets]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[insight_targets]
GO

CREATE VIEW dbo.insight_targets (
	[id],
	[insight_target_type_id],
	[insight_target_value_type_id],
	[insight_type_id],
	[insight_context],
	[name],
	[value],
	[min],
	[max],
	[threshold]
)
AS
SELECT
	[InsightTargetID],
	[InsightTargetTypeID],
	[InsightTargetValueTypeID],
	[InsightTypeID],
	[InsightContext],
	[Name],
	[Value],
	[Min],
	[Max],
	[Threshold]
FROM
	[IMT].dbo.InsightTarget
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[insight_target_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[insight_target_types]
GO

CREATE VIEW dbo.insight_target_types (
	[id],
	[name]
)
AS
SELECT
	[InsightTargetTypeID],
	[Name]
FROM
	[IMT].dbo.InsightTargetType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[insight_target_value_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[insight_target_value_types]
GO

CREATE VIEW dbo.insight_target_value_types (
	[id],
	[name]
)
AS
SELECT
	[InsightTargetValueTypeID],
	[Name]
FROM
	[IMT].dbo.InsightTargetValueType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[insight_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[insight_types]
GO

CREATE VIEW dbo.insight_types (
	[id],
	[insight_category_id],
	[name]
)
AS
SELECT
	[InsightTypeID],
	[InsightCategoryID],
	[Name]
FROM
	[IMT].dbo.InsightType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[member_insight_type_preferences]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[member_insight_type_preferences]
GO

CREATE VIEW dbo.member_insight_type_preferences (
	[id],
	[business_unit_id],
	[member_id],
	[insight_type_id],
	[active]
)
AS
SELECT
	[MemberInsightTypePreference],
	[BusinessUnitID],
	[MemberID],
	[InsightTypeID],
	[Active]
FROM
	[IMT].dbo.MemberInsightTypePreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[member_insight_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[member_insight_types]
GO

CREATE VIEW dbo.member_insight_types (
	[id],
	[business_unit_id],
	[member_id],
	[insight_type_id]
)
AS
SELECT
	CAST([BusinessUnitID] AS VARCHAR) + '_' + CAST([MemberID] AS VARCHAR) + '_' + CAST([InsightTypeID] AS VARCHAR),
	[BusinessUnitID],
	[MemberID],
	[InsightTypeID]
FROM
	[IMT].dbo.MemberInsightType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_buckets]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[inventory_buckets]
GO

CREATE VIEW dbo.inventory_buckets (
	[id],
	[name],
	[inventory_type_id]
)
AS
SELECT
	[InventoryBucketID],
	[Description],
	[InventoryType]
FROM
	[IMT].dbo.InventoryBuckets
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_bucket_items]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[inventory_bucket_items]
GO

CREATE VIEW dbo.inventory_bucket_items (
	[id],
	[inventory_bucket_id],
	[range_id],
	[name],
	[low],
	[high],
	[lights],
	[business_unit_id],
	[description],
	[position]
)
AS
SELECT
	[InventoryBucketRangeID],
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
FROM
	[IMT].dbo.InventoryBucketRanges
GO
