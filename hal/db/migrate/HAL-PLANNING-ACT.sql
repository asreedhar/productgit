
USE HAL

--
-- DROP PLANNING ACTION DEFAULT JOIN TABLES
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultReprices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultSmallPrintAdvertisements]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultInternetAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultLotPromotions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultSPIFFs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultOthers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultServices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultLiveAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultsPlanningActionDefaultWholesalers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultsPlanningActionDefaultWholesalers
GO

--
-- DROP PLANNING ACTION DEFAULT TABLES
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultReprices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultRepriceTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultRepriceTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultSmallPrintAdvertisements]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultInternetAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultLotPromotions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultSPIFFs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultOthers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultServices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultLiveAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaultWholesalers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaultWholesalers
GO

--
-- DROP PLANNING ACT JOIN TABLES
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActReprices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActSmallPrintAdvertisements]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActInternetAdvertisements]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActInternetAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActInternetAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActLotPromotions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActSPIFFs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActOthers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActServices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActLiveAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActWholesalers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActWholesalers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActsPlanningActSolds]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActsPlanningActSolds
GO

--
-- DROP PLANNING ACT TABLES
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_Planning]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_Planning
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActReprices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActSmallPrintAdvertisements]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActPriceAttributes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActPriceAttributes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActInternetAdvertisements]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActInternetAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActInternetAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActLotPromotions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActSPIFFs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActOthers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActServices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActLiveAuctions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActWholesalers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActWholesalers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActSolds]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActSolds
GO

--
-- TABLES
--

CREATE TABLE dbo.Inventory_Planning (
	InventoryID               INT NOT NULL,
	PlanningActionTypeID      INT NOT NULL,
	CandidateForAdvertisement BIT NOT NULL,
	CandidateForWholesale     BIT NOT NULL,
	CONSTRAINT PK_Inventory_Planning PRIMARY KEY (
		InventoryID
	),
	CONSTRAINT FK_Inventory_Planning_PlanningActionType FOREIGN KEY (
		PlanningActionTypeID
	)
	REFERENCES PlanningActionTypes (
		PlanningActionTypeID
	)
)
GO

CREATE TABLE dbo.PlanningActPriceAttributes (
	PlanningActPriceAttributeID INT         NOT NULL,
	[Name]                      VARCHAR(50) NOT NULL
	CONSTRAINT PK_PlanningActPriceAttribute PRIMARY KEY (
		PlanningActPriceAttributeID
	)
)
GO

INSERT INTO dbo.PlanningActPriceAttributes ([PlanningActPriceAttributeID], [Name]) VALUES (1, 'List Price')
INSERT INTO dbo.PlanningActPriceAttributes ([PlanningActPriceAttributeID], [Name]) VALUES (2, 'Ad Price')
GO

CREATE TABLE dbo.PlanningActReprices (
	PlanningActRepriceID        INT IDENTITY(1,1) NOT NULL,
	PlanningActPriceAttributeID INT NOT NULL,
	BusinessUnitID              INT NOT NULL,
	OriginalPrice               INT NOT NULL,
	NewPrice                    INT NOT NULL,
	CONSTRAINT PK_PlanningActReprice PRIMARY KEY (
		PlanningActRepriceID
	),
	CONSTRAINT FK_PlanningActReprice_PlanningActPriceAttribute FOREIGN KEY (
		PlanningActPriceAttributeID
	)
	REFERENCES PlanningActPriceAttributes (
		PlanningActPriceAttributeID
	)
)
GO

CREATE TABLE dbo.PlanningActSmallPrintAdvertisements (
	PlanningActSmallPrintAdvertisementID INT IDENTITY(1,1) NOT NULL,
	PlanningActPriceAttributeID          INT NOT NULL,
	BusinessUnitID                       INT NOT NULL,
	CampaignRunID                        INT NOT NULL,
	[Description]                        VARCHAR(500) NOT NULL,
	ShowPriceOnPlan                      BIT NOT NULL,
	CONSTRAINT PK_PlanningActSmallPrintAdvertisement PRIMARY KEY (
		PlanningActSmallPrintAdvertisementID
	),
	CONSTRAINT FK_PlanningActSmallPrintAdvertisement_PlanningActPriceAttribute FOREIGN KEY (
		PlanningActPriceAttributeID
	)
	REFERENCES PlanningActPriceAttributes (
		PlanningActPriceAttributeID
	)
)
GO

CREATE TABLE dbo.PlanningActInternetAdvertisements (
	PlanningActInternetAdvertisementID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                     INT               NOT NULL,
	InternetAdvertiserID               INT               NOT NULL,
	[Description]                      VARCHAR(500)      NOT NULL,
	Highlights                         VARCHAR(500)      NOT NULL,
	CONSTRAINT PK_PlanningActInternetAdvertisement PRIMARY KEY (
		PlanningActInternetAdvertisementID
	)
)
GO

CREATE TABLE dbo.PlanningActInternetAuctions (
	PlanningActInternetAuctionID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID               INT               NOT NULL,
	MarketplaceID                INT               NOT NULL,
	SendPictures                 BIT               NOT NULL,
	SendAnnouncements            BIT               NOT NULL,
	Announcements                VARCHAR(500)      NULL,
	CONSTRAINT PK_PlanningActInternetAuction PRIMARY KEY (
		PlanningActInternetAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActLotPromotions (
	PlanningActLotPromotionID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID            INT               NOT NULL,
	Active                    BIT               NOT NULL,
	[Name]                    VARCHAR(255)      NOT NULL,
	CONSTRAINT PK_PlanningActLotPromotion PRIMARY KEY (
		PlanningActLotPromotionID
	)
)
GO

CREATE TABLE dbo.PlanningActSPIFFs (
	PlanningActSPIFFID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID     INT               NOT NULL,
	Active             BIT               NOT NULL,
	[Name]             VARCHAR(255)      NOT NULL,
	Amount             INT               NOT NULL,
	CONSTRAINT PK_PlanningActSPIFF PRIMARY KEY (
		PlanningActSPIFFID
	)
)
GO

CREATE TABLE dbo.PlanningActOthers (
	PlanningActOtherID INT IDENTITY(1,1) NOT NULL,
	PlanningActionID   INT               NOT NULL,
	BusinessUnitID     INT               NOT NULL,
	Active             BIT               NOT NULL,
	[Name]             VARCHAR(255)      NOT NULL,
	CONSTRAINT PK_PlanningActOther PRIMARY KEY (
		PlanningActOtherID
	),
	CONSTRAINT FK_PlanningActOther_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	)
)
GO

CREATE TABLE dbo.PlanningActServices (
	PlanningActServiceID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID       INT               NOT NULL,
	EnterDate            SMALLDATETIME     NULL,
	ExitDate             SMALLDATETIME     NULL,
	Detail               BIT               NOT NULL,
	CONSTRAINT PK_PlanningActService PRIMARY KEY (
		PlanningActServiceID
	)
)
GO

CREATE TABLE dbo.PlanningActLiveAuctions (
	PlanningActLiveAuctionID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID           INT               NOT NULL,
	LiveAuctionID            INT               NOT NULL,
	[Date]                   SMALLDATETIME     NULL,
	CONSTRAINT PK_PlanningActLiveAuction PRIMARY KEY (
		PlanningActLiveAuctionID
	),
	CONSTRAINT FK_PlanningActLiveAuction_LiveAuction FOREIGN KEY (
		LiveAuctionID
	)
	REFERENCES LiveAuction_ThirdPartyEntity (
		LiveAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActWholesalers (
	PlanningActWholesalerID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID          INT               NOT NULL,
	WholesalerID            INT               NOT NULL,
	[Date]                  SMALLDATETIME     NULL,
	CONSTRAINT PK_PlanningActWholesaler PRIMARY KEY (
		PlanningActWholesalerID
	),
	CONSTRAINT FK_PlanningActWholesaler_Wholesaler FOREIGN KEY (
		WholesalerID
	)
	REFERENCES Wholesaler_ThirdPartyEntity (
		WholesalerID
	)
)
GO

CREATE TABLE dbo.PlanningActSolds (
	PlanningActSoldID    INT IDENTITY(1,1) NOT NULL,
	PlanningActionTypeID INT               NOT NULL,
	BusinessUnitID       INT               NOT NULL,
	Buyer                VARCHAR(255)      NULL,
	[Date]               SMALLDATETIME     NULL,
	CONSTRAINT PK_PlanningActSold PRIMARY KEY (
		PlanningActSoldID
	),
	CONSTRAINT FK_PlanningActSold_PlanningActionType FOREIGN KEY (
		PlanningActionTypeID
	)
	REFERENCES PlanningActionTypes (
		PlanningActionTypeID
	)
)
GO

--
-- JOIN TABLES
-- 

CREATE TABLE dbo.PlanningActsPlanningActReprices (
	PlanningActID        INT NOT NULL,
	PlanningActRepriceID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActReprice PRIMARY KEY (
		PlanningActID,
		PlanningActRepriceID
	),
	CONSTRAINT FK_PlanningActsPlanningActReprice_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActReprice_PlanningActReprice FOREIGN KEY (
		PlanningActRepriceID
	)
	REFERENCES PlanningActReprices (
		PlanningActRepriceID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActSmallPrintAdvertisements (
	PlanningActID                        INT NOT NULL,
	PlanningActSmallPrintAdvertisementID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActSmallPrintAdvertisement PRIMARY KEY (
		PlanningActID,
		PlanningActSmallPrintAdvertisementID
	),
	CONSTRAINT FK_PlanningActsPlanningActSmallPrintAdvertisement_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActSmallPrintAdvertisement_PlanningActSmallPrintAdvertisement FOREIGN KEY (
		PlanningActSmallPrintAdvertisementID
	)
	REFERENCES PlanningActSmallPrintAdvertisements (
		PlanningActSmallPrintAdvertisementID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActInternetAdvertisements (
	PlanningActID                      INT NOT NULL,
	PlanningActInternetAdvertisementID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActInternetAdvertisement PRIMARY KEY (
		PlanningActID,
		PlanningActInternetAdvertisementID
	),
	CONSTRAINT FK_PlanningActsPlanningActInternetAdvertisement_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActInternetAdvertisement_PlanningActInternetAdvertisement FOREIGN KEY (
		PlanningActInternetAdvertisementID
	)
	REFERENCES PlanningActInternetAdvertisements (
		PlanningActInternetAdvertisementID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActInternetAuctions (
	PlanningActID                INT NOT NULL,
	PlanningActInternetAuctionID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActInternetAuction PRIMARY KEY (
		PlanningActID,
		PlanningActInternetAuctionID
	),
	CONSTRAINT FK_PlanningActsPlanningActInternetAuction_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActInternetAuction_PlanningActInternetAuction FOREIGN KEY (
		PlanningActInternetAuctionID
	)
	REFERENCES PlanningActInternetAuctions (
		PlanningActInternetAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActLotPromotions (
	PlanningActID             INT NOT NULL,
	PlanningActLotPromotionID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActLotPromotion PRIMARY KEY (
		PlanningActID,
		PlanningActLotPromotionID
	),
	CONSTRAINT FK_PlanningActsPlanningActLotPromotion_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActLotPromotion_PlanningActLotPromotion FOREIGN KEY (
		PlanningActLotPromotionID
	)
	REFERENCES PlanningActLotPromotions (
		PlanningActLotPromotionID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActSPIFFs (
	PlanningActID      INT NOT NULL,
	PlanningActSPIFFID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActSPIFF PRIMARY KEY (
		PlanningActID,
		PlanningActSPIFFID
	),
	CONSTRAINT FK_PlanningActsPlanningActSPIFF_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActSPIFF_PlanningActSPIFF FOREIGN KEY (
		PlanningActSPIFFID
	)
	REFERENCES PlanningActSPIFFs (
		PlanningActSPIFFID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActOthers (
	PlanningActID      INT NOT NULL,
	PlanningActOtherID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActOther PRIMARY KEY (
		PlanningActID,
		PlanningActOtherID
	),
	CONSTRAINT FK_PlanningActsPlanningActOther_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActOther_PlanningActOther FOREIGN KEY (
		PlanningActOtherID
	)
	REFERENCES PlanningActOthers (
		PlanningActOtherID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActServices (
	PlanningActID        INT NOT NULL,
	PlanningActServiceID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActService PRIMARY KEY (
		PlanningActID,
		PlanningActServiceID
	),
	CONSTRAINT FK_PlanningActsPlanningActService_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActService_PlanningActService FOREIGN KEY (
		PlanningActServiceID
	)
	REFERENCES PlanningActServices (
		PlanningActServiceID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActLiveAuctions (
	PlanningActID            INT NOT NULL,
	PlanningActLiveAuctionID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActLiveAuction PRIMARY KEY (
		PlanningActID,
		PlanningActLiveAuctionID
	),
	CONSTRAINT FK_PlanningActsPlanningActLiveAuction_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActLiveAuction_PlanningActLiveAuction FOREIGN KEY (
		PlanningActLiveAuctionID
	)
	REFERENCES PlanningActLiveAuctions (
		PlanningActLiveAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActWholesalers (
	PlanningActID           INT NOT NULL,
	PlanningActWholesalerID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActWholesaler PRIMARY KEY (
		PlanningActID,
		PlanningActWholesalerID
	),
	CONSTRAINT FK_PlanningActsPlanningActWholesaler_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActWholesaler_PlanningActWholesaler FOREIGN KEY (
		PlanningActWholesalerID
	)
	REFERENCES PlanningActWholesalers (
		PlanningActWholesalerID
	)
)
GO

CREATE TABLE dbo.PlanningActsPlanningActSolds (
	PlanningActID     INT NOT NULL,
	PlanningActSoldID INT NOT NULL,
	CONSTRAINT PK_PlanningActPlanningActSold PRIMARY KEY (
		PlanningActID,
		PlanningActSoldID
	),
	CONSTRAINT FK_PlanningActsPlanningActSold_PlanningAct FOREIGN KEY (
		PlanningActID
	)
	REFERENCES PlanningActs (
		PlanningActID
	),
	CONSTRAINT FK_PlanningActsPlanningActSold_PlanningActSold FOREIGN KEY (
		PlanningActSoldID
	)
	REFERENCES PlanningActSolds (
		PlanningActSoldID
	)
)
GO

--
-- DEFAULT TABLES
--

CREATE TABLE dbo.PlanningActionDefaultRepriceTypes (
	PlanningActionDefaultRepriceTypeID INT         NOT NULL,
	[Name]                             VARCHAR(50) NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultRepriceType PRIMARY KEY (
		PlanningActionDefaultRepriceTypeID
	)
)
GO

INSERT INTO dbo.PlanningActionDefaultRepriceTypes (PlanningActionDefaultRepriceTypeID,[Name]) VALUES (1, 'Reduce price by $X')
INSERT INTO dbo.PlanningActionDefaultRepriceTypes (PlanningActionDefaultRepriceTypeID,[Name]) VALUES (2, 'Compare List Price to PING Average Price')
INSERT INTO dbo.PlanningActionDefaultRepriceTypes (PlanningActionDefaultRepriceTypeID,[Name]) VALUES (3, 'Set List Price to Unit Cost + $X')
GO

CREATE TABLE dbo.PlanningActionDefaultReprices (
	PlanningActionDefaultRepriceID     INT IDENTITY(1,1) NOT NULL,
	PlanningActionDefaultRepriceTypeID INT               NOT NULL,
	BusinessUnitID                     INT               NOT NULL,
	Active                             BIT               NOT NULL,
	Amount                             INT               NULL,
	CONSTRAINT PK_PlanningActionDefaultReprice PRIMARY KEY (
		PlanningActionDefaultRepriceID
	),
	CONSTRAINT FK_PlanningActionDefaultReprice_PlanningActionDefaultRepriceType FOREIGN KEY (
		PlanningActionDefaultRepriceTypeID
	)
	REFERENCES PlanningActionDefaultRepriceTypes (
		PlanningActionDefaultRepriceTypeID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultSmallPrintAdvertisements (
	PlanningActionDefaultSmallPrintAdvertisementID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID INT NOT NULL,
	Active         BIT NOT NULL,
	CampaignID     INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultSmallPrintAdvertisement PRIMARY KEY (
		PlanningActionDefaultSmallPrintAdvertisementID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultInternetAuctions (
	PlanningActionDefaultInternetAuctionID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                         INT               NOT NULL,
	Active                                 BIT               NOT NULL,
	MarketplaceID                          INT               NOT NULL
	CONSTRAINT PK_PlanningActionDefaultInternetAuction PRIMARY KEY (
		PlanningActionDefaultInternetAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultLotPromotions (
	PlanningActionDefaultLotPromotionID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                      INT               NOT NULL,
	Active                              BIT               NOT NULL,
	PlanningActLotPromotionID           INT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultLotPromotion PRIMARY KEY (
		PlanningActionDefaultLotPromotionID
	),
	CONSTRAINT FK_PlanningActionDefaultLotPromotion_PlanningActLotPromotion FOREIGN KEY (
		PlanningActLotPromotionID
	)
	REFERENCES PlanningActLotPromotions (
		PlanningActLotPromotionID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultSPIFFs (
	PlanningActionDefaultSPIFFID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID               INT               NOT NULL,
	Active                       BIT               NOT NULL,
	PlanningActSPIFFID           INT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultSPIFF PRIMARY KEY (
		PlanningActionDefaultSPIFFID
	),
	CONSTRAINT FK_PlanningActionDefaultSPIFF_PlanningActSPIFF FOREIGN KEY (
		PlanningActSPIFFID
	)
	REFERENCES PlanningActSPIFFs (
		PlanningActSPIFFID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultOthers (
	PlanningActionDefaultOtherID INT IDENTITY(1,1) NOT NULL,
	PlanningActionID             INT               NOT NULL,
	BusinessUnitID               INT               NOT NULL,
	Active                       BIT               NOT NULL,
	PlanningActOtherID           INT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultOther PRIMARY KEY (
		PlanningActionDefaultOtherID
	),
	CONSTRAINT FK_PlanningActionDefaultOther_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	),
	CONSTRAINT FK_PlanningActionDefaultOther_PlanningActOther FOREIGN KEY (
		PlanningActOtherID
	)
	REFERENCES PlanningActOthers (
		PlanningActOtherID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultServices (
	PlanningActionDefaultServiceID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                 INT               NOT NULL,
	Active                         BIT               NOT NULL,
	TimeInDays                     INT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultService PRIMARY KEY (
		PlanningActionDefaultServiceID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultLiveAuctions (
	PlanningActionDefaultLiveAuctionID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                     INT               NOT NULL,
	Active                             BIT               NOT NULL,
	LiveAuctionID                      INT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultLiveAuction PRIMARY KEY (
		PlanningActionDefaultLiveAuctionID
	),
	CONSTRAINT FK_PlanningActionDefaultLiveAuction_LiveAuction FOREIGN KEY (
		LiveAuctionID
	)
	REFERENCES LiveAuction_ThirdPartyEntity (
		LiveAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultWholesalers (
	PlanningActionDefaultWholesalerID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                    INT               NOT NULL,
	Active                            BIT               NOT NULL,
	WholesalerID                      INT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultWholesaler PRIMARY KEY (
		PlanningActionDefaultWholesalerID
	),
	CONSTRAINT FK_PlanningActionDefaultWholesaler_Wholesaler FOREIGN KEY (
		WholesalerID
	)
	REFERENCES Wholesaler_ThirdPartyEntity (
		WholesalerID
	)
)
GO

--
-- DEFAULT JOIN TABLES
-- 

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultReprices (
	PlanningActionDefaultID        INT NOT NULL,
	PlanningActionDefaultRepriceID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultReprice PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultRepriceID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultReprice_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultReprice_PlanningActionDefaultReprice FOREIGN KEY (
		PlanningActionDefaultRepriceID
	)
	REFERENCES PlanningActionDefaultReprices (
		PlanningActionDefaultRepriceID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultSmallPrintAdvertisements (
	PlanningActionDefaultID                        INT NOT NULL,
	PlanningActionDefaultSmallPrintAdvertisementID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultSmallPrintAdvertisement PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultSmallPrintAdvertisementID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultSmallPrintAdvertisement_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultsPlanningActionDefaultSmallPrintAdvertisement_PlanningActionDefaultSmallPrintAdvertisement FOREIGN KEY (
		PlanningActionDefaultSmallPrintAdvertisementID
	)
	REFERENCES PlanningActionDefaultSmallPrintAdvertisements (
		PlanningActionDefaultSmallPrintAdvertisementID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultInternetAuctions (
	PlanningActionDefaultID                INT NOT NULL,
	PlanningActionDefaultInternetAuctionID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultInternetAuction PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultInternetAuctionID
	),
	CONSTRAINT FK_PlanningActsPlanningActInternetAuction_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultInternetAuction_PlanningActionDefaultInternetAuction FOREIGN KEY (
		PlanningActionDefaultInternetAuctionID
	)
	REFERENCES PlanningActionDefaultInternetAuctions (
		PlanningActionDefaultInternetAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultLotPromotions (
	PlanningActionDefaultID             INT NOT NULL,
	PlanningActionDefaultLotPromotionID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultLotPromotion PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultLotPromotionID
	),
	CONSTRAINT FK_PlanningActionDefaultsPlanningActionDefaultLotPromotion_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultLotPromotion_PlanningActionDefaultLotPromotion FOREIGN KEY (
		PlanningActionDefaultLotPromotionID
	)
	REFERENCES PlanningActionDefaultLotPromotions (
		PlanningActionDefaultLotPromotionID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultSPIFFs (
	PlanningActionDefaultID      INT NOT NULL,
	PlanningActionDefaultSPIFFID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultSPIFF PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultSPIFFID
	),
	CONSTRAINT FK_PlanningActionDefaultsPlanningActionDefaultSPIFF_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultSPIFF_PlanningActionDefaultSPIFF FOREIGN KEY (
		PlanningActionDefaultSPIFFID
	)
	REFERENCES PlanningActionDefaultSPIFFs (
		PlanningActionDefaultSPIFFID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultOthers (
	PlanningActionDefaultID      INT NOT NULL,
	PlanningActionDefaultOtherID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultOther PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultOtherID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultOther_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultOther_PlanningActionDefaultOther FOREIGN KEY (
		PlanningActionDefaultOtherID
	)
	REFERENCES PlanningActionDefaultOthers (
		PlanningActionDefaultOtherID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultServices (
	PlanningActionDefaultID        INT NOT NULL,
	PlanningActionDefaultServiceID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultService PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultServiceID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultService_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultService_PlanningActionDefaultService FOREIGN KEY (
		PlanningActionDefaultServiceID
	)
	REFERENCES PlanningActionDefaultServices (
		PlanningActionDefaultServiceID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultLiveAuctions (
	PlanningActionDefaultID            INT NOT NULL,
	PlanningActionDefaultLiveAuctionID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultLiveAuction PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultLiveAuctionID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultLiveAuction_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultLiveAuction_PlanningActionDefaultLiveAuction FOREIGN KEY (
		PlanningActionDefaultLiveAuctionID
	)
	REFERENCES PlanningActionDefaultLiveAuctions (
		PlanningActionDefaultLiveAuctionID
	)
)
GO

CREATE TABLE dbo.PlanningActionDefaultsPlanningActionDefaultWholesalers (
	PlanningActionDefaultID           INT NOT NULL,
	PlanningActionDefaultWholesalerID INT NOT NULL,
	CONSTRAINT PK_PlanningActionDefaultPlanningActionDefaultWholesaler PRIMARY KEY (
		PlanningActionDefaultID,
		PlanningActionDefaultWholesalerID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultWholesaler_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaultPlanningActionDefaultWholesaler_PlanningActionDefaultWholesaler FOREIGN KEY (
		PlanningActionDefaultWholesalerID
	)
	REFERENCES PlanningActionDefaultWholesalers (
		PlanningActionDefaultWholesalerID
	)
)
GO

--
-- Ruby Views
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_planning_attributes]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[inventory_planning_attributes]
GO

CREATE VIEW dbo.inventory_planning_attributes (
	[id],
	[inventory_id],
	[planning_action_type_id],
	[candidate_for_advertisement],
	[candidate_for_wholesale]
)
AS
SELECT
	InventoryID,
	InventoryID,
	PlanningActionTypeID,
	CandidateForAdvertisement,
	CandidateForWholesale
FROM
	dbo.Inventory_Planning
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_price_attributes]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_price_attributes]
GO

CREATE VIEW dbo.planning_act_price_attributes (
	[id],
	[name]
)
AS
SELECT
	PlanningActPriceAttributeID,
	[Name]
FROM
	dbo.PlanningActPriceAttributes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_reprices]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_reprices]
GO

CREATE VIEW dbo.planning_act_reprices (
	[id],
	[planning_act_price_attribute_id],
	[business_unit_id],
	[original_price],
	[new_price]
)
AS
SELECT
	PlanningActRepriceID,
	PlanningActPriceAttributeID,
	BusinessUnitID,
	OriginalPrice,
	NewPrice
FROM
	dbo.PlanningActReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_small_print_advertisements]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_small_print_advertisements]
GO

CREATE VIEW dbo.planning_act_small_print_advertisements (
	[id],
	[planning_act_price_attribute_id],
	[business_unit_id],
	[campaign_run_id],
	[description],
	[show_price_on_plan]
)
AS
SELECT
	PlanningActSmallPrintAdvertisementID,
	PlanningActPriceAttributeID,
	BusinessUnitID,
	CampaignRunID,
	[Description],
	ShowPriceOnPlan
FROM
	dbo.PlanningActSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_internet_advertisements]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_internet_advertisements]
GO

CREATE VIEW dbo.planning_act_internet_advertisements (
	[id],
	[business_unit_id],
	[internet_publisher_id],
	[description],
	[highlights]
)
AS
SELECT
	PlanningActInternetAdvertisementID,
	BusinessUnitID,
	InternetAdvertiserID,
	[Description],
	Highlights
FROM
	dbo.PlanningActInternetAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_internet_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_internet_auctions]
GO

CREATE VIEW dbo.planning_act_internet_auctions (
	[id],
	[business_unit_id],
	[third_party_id],
	[send_pictures],
	[send_announcements],
	[announcements]
)
AS
SELECT
	PlanningActInternetAuctionID,
	BusinessUnitID,
	MarketplaceID,
	SendPictures,
	SendAnnouncements,
	Announcements
FROM
	dbo.PlanningActInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_lot_promotions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_lot_promotions]
GO

CREATE VIEW dbo.planning_act_lot_promotions (
	[id],
	[business_unit_id],
	[active],
	[name]
)
AS
SELECT
	PlanningActLotPromotionID,
	BusinessUnitID,
	Active,
	[Name]
FROM
	dbo.PlanningActLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_spiffs]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_spiffs]
GO

CREATE VIEW dbo.planning_act_spiffs (
	[id],
	[business_unit_id],
	[active],
	[name],
	[amount]
)
AS
SELECT
	PlanningActSPIFFID,
	BusinessUnitID,
	Active,
	[Name],
	Amount
FROM
	dbo.PlanningActSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_others]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_others]
GO

CREATE VIEW dbo.planning_act_others (
	[id],
	[planning_action_id],
	[business_unit_id],
	[active],
	[name]
)
AS
SELECT
	PlanningActOtherID,
	PlanningActionID,
	BusinessUnitID,
	Active,
	[Name]
FROM
	dbo.PlanningActOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_services]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_services]
GO

CREATE VIEW dbo.planning_act_services (
	[id],
	[business_unit_id],
	[enter_date],
	[exit_date],
	[detail]
)
AS
SELECT
	PlanningActServiceID,
	BusinessUnitID,
	EnterDate,
	ExitDate,
	Detail
FROM
	dbo.PlanningActServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_live_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_live_auctions]
GO

CREATE VIEW dbo.planning_act_live_auctions (
	[id],
	[business_unit_id],
	[live_auction_id],
	[date]
)
AS
SELECT
	PlanningActLiveAuctionID,
	BusinessUnitID,
	LiveAuctionID,
	[Date]
FROM
	dbo.PlanningActLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_wholesalers]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_wholesalers]
GO

CREATE VIEW dbo.planning_act_wholesalers (
	[id],
	[business_unit_id],
	[wholesaler_id],
	[date]
)
AS
SELECT
	PlanningActWholesalerID,
	BusinessUnitID,
	WholesalerID,
	[Date]
FROM
	dbo.PlanningActWholesalers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_act_solds]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_act_solds]
GO

CREATE VIEW dbo.planning_act_solds (
	[id],
	[planning_action_type_id],
	[business_unit_id],
	[buyer],
	[date]
)
AS
SELECT
	PlanningActSoldID,
	PlanningActionTypeID,
	BusinessUnitID,
	Buyer,
	[Date]
FROM
	dbo.PlanningActSolds
GO

-- join tables

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_reprices]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_reprices]
GO

CREATE VIEW dbo.planning_acts_planning_act_reprices (
	[planning_act_id],
	[planning_act_reprice_id]
)
AS
SELECT
	PlanningActID,
	PlanningActRepriceID
FROM
	dbo.PlanningActsPlanningActReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_small_print_advertisements]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_small_print_advertisements]
GO

CREATE VIEW dbo.planning_acts_planning_act_small_print_advertisements (
	[planning_act_id],
	[planning_act_small_print_advertisement_id]
)
AS
SELECT
	PlanningActID,
	PlanningActSmallPrintAdvertisementID
FROM
	dbo.PlanningActsPlanningActSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_internet_advertisements]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_internet_advertisements]
GO

CREATE VIEW dbo.planning_acts_planning_act_internet_advertisements (
	[planning_act_id],
	[planning_act_internet_advertisement_id]
)
AS
SELECT
	PlanningActID,
	PlanningActInternetAdvertisementID
FROM
	dbo.PlanningActsPlanningActInternetAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_internet_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_internet_auctions]
GO

CREATE VIEW dbo.planning_acts_planning_act_internet_auctions (
	[planning_act_id],
	[planning_act_internet_auction_id]
)
AS
SELECT
	PlanningActID,
	PlanningActInternetAuctionID
FROM
	dbo.PlanningActsPlanningActInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_lot_promotions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_lot_promotions]
GO

CREATE VIEW dbo.planning_acts_planning_act_lot_promotions (
	[planning_act_id],
	[planning_act_lot_promotion_id]
)
AS
SELECT
	PlanningActID,
	PlanningActLotPromotionID
FROM
	dbo.PlanningActsPlanningActLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_spiffs]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_spiffs]
GO

CREATE VIEW dbo.planning_acts_planning_act_spiffs (
	[planning_act_id],
	[planning_act_spiff_id]
)
AS
SELECT
	PlanningActID,
	PlanningActSPIFFID
FROM
	dbo.PlanningActsPlanningActSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_others]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_others]
GO

CREATE VIEW dbo.planning_acts_planning_act_others (
	[planning_act_id],
	[planning_act_other_id]
)
AS
SELECT
	PlanningActID,
	PlanningActOtherID
FROM
	dbo.PlanningActsPlanningActOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_services]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_services]
GO

CREATE VIEW dbo.planning_acts_planning_act_services (
	[planning_act_id],
	[planning_act_service_id]
)
AS
SELECT
	PlanningActID,
	PlanningActServiceID
FROM
	dbo.PlanningActsPlanningActServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_live_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_live_auctions]
GO

CREATE VIEW dbo.planning_acts_planning_act_live_auctions (
	[planning_act_id],
	[planning_act_live_auction_id]
)
AS
SELECT
	PlanningActID,
	PlanningActLiveAuctionID
FROM
	dbo.PlanningActsPlanningActLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_wholesalers]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_wholesalers]
GO

CREATE VIEW dbo.planning_acts_planning_act_wholesalers (
	[planning_act_id],
	[planning_act_wholesaler_id]
)
AS
SELECT
	PlanningActID,
	PlanningActWholesalerID
FROM
	dbo.PlanningActsPlanningActWholesalers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts_planning_act_solds]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts_planning_act_solds]
GO

CREATE VIEW dbo.planning_acts_planning_act_solds (
	[planning_act_id],
	[planning_act_sold_id]
)
AS
SELECT
	PlanningActID,
	PlanningActSoldID
FROM
	dbo.PlanningActsPlanningActSolds
GO

-- defaults

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_reprice_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_reprice_types]
GO

CREATE VIEW dbo.planning_action_default_reprice_types (
	[id],
	[name]
)
AS
SELECT
	PlanningActionDefaultRepriceTypeID,
	[Name]
FROM
	dbo.PlanningActionDefaultRepriceTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_reprices]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_reprices]
GO

CREATE VIEW dbo.planning_action_default_reprices (
	[id],
	[planning_action_default_reprice_type_id],
	[business_unit_id],
	[active],
	[amount]
)
AS
SELECT
	PlanningActionDefaultRepriceID,
	PlanningActionDefaultRepriceTypeID,
	BusinessUnitID,
	Active,
	Amount
FROM
	dbo.PlanningActionDefaultReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_small_print_advertisements]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_small_print_advertisements]
GO

CREATE VIEW dbo.planning_action_default_small_print_advertisements (
	[id],
	[business_unit_id],
	[active],
	[campaign_id]
)
AS
SELECT
	PlanningActionDefaultSmallPrintAdvertisementID,
	BusinessUnitID,
	Active,
	CampaignID
FROM
	dbo.PlanningActionDefaultSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_internet_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_internet_auctions]
GO

CREATE VIEW dbo.planning_action_default_internet_auctions (
	[id],
	[business_unit_id],
	[active],
	[third_party_id]
)
AS
SELECT
	PlanningActionDefaultInternetAuctionID,
	BusinessUnitID,
	Active,
	MarketplaceID
FROM
	dbo.PlanningActionDefaultInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_lot_promotions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_lot_promotions]
GO

CREATE VIEW dbo.planning_action_default_lot_promotions (
	[id],
	[business_unit_id],
	[active],
	[planning_act_lot_promotion_id]
)
AS
SELECT
	PlanningActionDefaultLotPromotionID,
	BusinessUnitID,
	Active,
	PlanningActLotPromotionID
FROM
	dbo.PlanningActionDefaultLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_spiffs]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_spiffs]
GO

CREATE VIEW dbo.planning_action_default_spiffs (
	[id],
	[business_unit_id],
	[active],
	[planning_act_spiff_id]
)
AS
SELECT
	PlanningActionDefaultSPIFFID,
	BusinessUnitID,
	Active,
	PlanningActSPIFFID
FROM
	dbo.PlanningActionDefaultSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_others]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_others]
GO

CREATE VIEW dbo.planning_action_default_others (
	[id],
	[planning_action_id],
	[business_unit_id],
	[active],
	[planning_act_other_id]
)
AS
SELECT
	PlanningActionDefaultOtherID,
	PlanningActionID,
	BusinessUnitID,
	Active,
	PlanningActOtherID
FROM
	dbo.PlanningActionDefaultOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_services]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_services]
GO

CREATE VIEW dbo.planning_action_default_services (
	[id],
	[business_unit_id],
	[active],
	[time_in_days]
)
AS
SELECT
	PlanningActionDefaultServiceID,
	BusinessUnitID,
	Active,
	TimeInDays
FROM
	dbo.PlanningActionDefaultServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_live_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_live_auctions]
GO

CREATE VIEW dbo.planning_action_default_live_auctions (
	[id],
	[business_unit_id],
	[active],
	[live_auction_id]
)
AS
SELECT
	PlanningActionDefaultLiveAuctionID,
	BusinessUnitID,
	Active,
	LiveAuctionID
FROM
	dbo.PlanningActionDefaultLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_default_wholesalers]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_default_wholesalers]
GO

CREATE VIEW dbo.planning_action_default_wholesalers (
	[id],
	[business_unit_id],
	[active],
	[wholesaler_id]
)
AS
SELECT
	PlanningActionDefaultWholesalerID,
	BusinessUnitID,
	Active,
	WholesalerID
FROM
	dbo.PlanningActionDefaultWholesalers
GO

-- default joins

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_reprices]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_reprices]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_reprices (
	[planning_action_default_id],
	[planning_action_default_reprice_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultRepriceID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultReprices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_small_print_advertisements]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_small_print_advertisements]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_small_print_advertisements (
	[planning_action_default_id],
	[planning_action_default_small_print_advertisement_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultSmallPrintAdvertisementID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultSmallPrintAdvertisements
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_internet_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_internet_auctions]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_internet_auctions (
	[planning_action_default_id],
	[planning_action_default_internet_auction_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultInternetAuctionID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultInternetAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_lot_promotions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_lot_promotions]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_lot_promotions (
	[planning_action_default_id],
	[planning_action_default_lot_promotion_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultLotPromotionID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultLotPromotions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_spiffs]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_spiffs]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_spiffs (
	[planning_action_default_id],
	[planning_action_default_spiff_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultSPIFFID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultSPIFFs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_others]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_others]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_others (
	[planning_action_default_id],
	[planning_action_default_other_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultOtherID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultOthers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_services]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_services]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_services (
	[planning_action_default_id],
	[planning_action_default_service_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultServiceID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultServices
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_live_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_live_auctions]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_live_auctions (
	[planning_action_default_id],
	[planning_action_default_live_auction_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultLiveAuctionID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultLiveAuctions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults_planning_action_default_wholesalers]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_action_defaults_planning_action_default_wholesalers]
GO

CREATE VIEW dbo.planning_action_defaults_planning_action_default_wholesalers (
	[planning_action_default_id],
	[planning_action_default_wholesaler_id]
)
AS
SELECT
	PlanningActionDefaultID,
	PlanningActionDefaultWholesalerID
FROM
	dbo.PlanningActionDefaultsPlanningActionDefaultWholesalers
GO
