class CreateSessionTable < ActiveRecord::Migration
  def self.up
    create_table :sessions do |t|
      t.column :sessid, :string, :limit => 32
      t.column :data, :text
      t.column :created_at, :timestamp
      t.column :updated_at, :timestamp
    end
  end

  def self.down
    drop_table :sessions
  end
end
