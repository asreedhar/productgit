
USE HAL

-- ALTER DATABASE HAL set recovery simple
-- DBCC SHRINKFILE(2)
-- DBCC SHRINKDATABASE('HAL')

-- EXEC dbo.LoadHAL
-- EXEC dbo.LoadInventory_A6
-- EXEC dbo.LoadInventory_A7
-- EXEC dbo.LoadInventory_A8
-- EXEC dbo.InsertNewZipCodes
-- EXEC dbo.LoadZipCode_A1
-- EXEC dbo.ProcessPlanningCategoryAgeBoundaries

--
-- User Context
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UserContext]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.UserContext
GO

CREATE TABLE dbo.UserContext (
	UserContextID  INT IDENTITY(1,1) NOT NULL,
	RunDate        SMALLDATETIME NOT NULL
	CONSTRAINT PK_UserContext PRIMARY KEY NONCLUSTERED (
		UserContextID
	)
)
GO

INSERT INTO dbo.UserContext (RunDate) VALUES ([IMT].dbo.ToDate(GETDATE()))
GO

INSERT INTO dbo.UserContext (RunDate) VALUES (DATEADD(WW,1,[IMT].dbo.ToDate(GETDATE())))
GO

UPDATE dbo.UserContext SET RunDate = [IMT].dbo.ToDate(GETDATE()) WHERE UserContextID = 1
GO

UPDATE dbo.UserContext SET RunDate = DATEADD(WW,1,[IMT].dbo.ToDate(GETDATE())) WHERE UserContextID = 2
GO

--
-- Aggregate Tables
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AggregateTables]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AggregateTables
GO

CREATE TABLE dbo.AggregateTables (
	[AggregateTableID] [tinyint]       NOT NULL,
	[TableName]        [varchar] (128) NOT NULL,
	[Description]      [varchar] (255) NULL,
	[IsBuffered]       [bit]           NOT NULL DEFAULT (1),
	[HasPrimaryKey]    [bit]           NOT NULL DEFAULT (0),
	[Loader]           [varchar] (128) NULL,
	[LoaderTypeID]     [tinyint]       NULL,
	CONSTRAINT [PK_AggregateTables] PRIMARY KEY CLUSTERED (
		[AggregateTableID]
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ViewStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ViewStatus
GO

CREATE TABLE dbo.ViewStatus (
	ViewName      VARCHAR(255) NOT NULL,
	TableIndex    INT NOT NULL,
	CONSTRAINT PK_ViewStatus PRIMARY KEY NONCLUSTERED (
		ViewName,
		TableIndex
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadTable]
GO

CREATE PROCEDURE dbo.LoadTable
	@Prefix VARCHAR(4000),
	@Keep   VARCHAR(4000),
	@SQL    VARCHAR(4000)
AS
	SET NOCOUNT ON

	DECLARE @err INT, @msg VARCHAR(255), @Table VARCHAR(4000), @Index INT, @NewIndex INT
	SELECT @Index = TableIndex FROM dbo.ViewStatus WHERE ViewName = @Prefix
	SELECT @NewIndex = CASE WHEN @Index = 0 THEN 1 ELSE 0 END
	SET @Table = @Prefix + '#' + CAST(@NewIndex AS VARCHAR)
	
	SET @msg = 'TRUNCATE TABLE ' + @Table
	EXECUTE (@msg)

	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed

	IF LEN(@Keep) > 0
	BEGIN
		SET @msg = 'INSERT INTO ' + @Table + ' SELECT * FROM ' + @Prefix + ' WHERE ' + @Keep
		EXECUTE (@msg)
		IF @err <> 0 GOTO Failed
	END

	SET @msg = 'INSERT INTO ' + @Table + ' ' + @SQL
	EXECUTE (@msg)

	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed

	BEGIN TRANSACTION T0

	if exists (select * from dbo.sysobjects where id = object_id(N'' + @Prefix) and OBJECTPROPERTY(id, N'IsView') = 1)
	BEGIN
		SET @msg = 'DROP VIEW ' + @Prefix
		EXECUTE (@msg)
		SET @err = @@ERROR
		IF @err <> 0 GOTO Failed_In_Transaction
	END

	SET @msg = 'CREATE VIEW dbo.' + @Prefix + ' AS SELECT * FROM ' + @Table
	EXECUTE (@msg)
	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed_In_Transaction

	SET @msg = 'UPDATE dbo.ViewStatus'
	UPDATE dbo.ViewStatus SET TableIndex = @NewIndex WHERE ViewName = @Prefix
	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed_In_Transaction

	COMMIT TRANSACTION T0

	SET @msg = 'TRUNCATE TABLE ' + @Prefix + '#' + CAST(@Index AS VARCHAR)
	EXECUTE (@msg)
	SET @err = @@ERROR
	IF @err <> 0 GOTO Failed

	RETURN 0
	
Failed_In_Transaction:

	ROLLBACK TRANSACTION T0

Failed:
	SELECT cast(@err AS VARCHAR), @msg

	RETURN @err
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals_A1#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Appraisals_A1#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals_A1#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Appraisals_A1#1
GO

CREATE TABLE dbo.Appraisals_A1#0 (
	BusinessUnitID       INT NOT NULL,
	AppraisalID          INT NOT NULL,
	ThirdPartyID         INT NOT NULL,
	ThirdPartyCategoryID INT NOT NULL,
	VehicleID            INT NOT NULL,
	MakeModelGroupingID  INT NOT NULL,
	ModelYear            INT NOT NULL,
	Mileage              INT NOT NULL,
	DateCreated          DATETIME NOT NULL,
	AppraisalValue       DECIMAL(12,2) NULL,
	BookoutValue         INT NOT NULL,
	CustomerOffer        INT NULL
	CONSTRAINT PK_Appraisal_A1#0 PRIMARY KEY NONCLUSTERED (
		AppraisalID,
		ThirdPartyID,
		ThirdPartyCategoryID
	)
)
GO

CREATE TABLE dbo.Appraisals_A1#1 (
	BusinessUnitID       INT NOT NULL,
	AppraisalID          INT NOT NULL,
	ThirdPartyID         INT NOT NULL,
	ThirdPartyCategoryID INT NOT NULL,
	VehicleID            INT NOT NULL,
	MakeModelGroupingID  INT NOT NULL,
	ModelYear            INT NOT NULL,
	Mileage              INT NOT NULL,
	DateCreated          DATETIME NOT NULL,
	AppraisalValue       DECIMAL(12,2) NULL,
	BookoutValue         INT NOT NULL,
	CustomerOffer        INT NULL,
	CONSTRAINT PK_Appraisal_A1#1 PRIMARY KEY NONCLUSTERED (
		AppraisalID,
		ThirdPartyID,
		ThirdPartyCategoryID
	)
)
GO

CREATE INDEX IDX_Appraisals_A1#0 ON dbo.Appraisals_A1#0 (
	BusinessUnitID,
	ThirdPartyID,
	ThirdPartyCategoryID
)
GO

CREATE INDEX IDX_Appraisals_A1#1 ON dbo.Appraisals_A1#1 (
	BusinessUnitID,
	ThirdPartyID,
	ThirdPartyCategoryID
)
GO

CREATE INDEX IDX_Appraisals_A1#0_BusinessUnit ON dbo.Appraisals_A1#0 (
	BusinessUnitID
)
GO

CREATE INDEX IDX_Appraisals_A1#1_BusinessUnit ON dbo.Appraisals_A1#1 (
	BusinessUnitID
)
GO

CREATE INDEX IDX_Appraisals_A1#0_Appraisal ON dbo.Appraisals_A1#0 (
	AppraisalID
)
GO

CREATE INDEX IDX_Appraisals_A1#1_Appraisal ON dbo.Appraisals_A1#1 (
	AppraisalID
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals_A1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Appraisals_A1]
GO

CREATE VIEW dbo.Appraisals_A1 AS SELECT * FROM Appraisals_A1#0
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Appraisals_A1', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	1,
	'Appraisals_A1',
	'Aggregated snapshot of last four weeks of appraisals',
	1,
	1,
	'GetAppraisals_V1',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MaxAppraisalFormOptions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[MaxAppraisalFormOptions]
GO

CREATE VIEW dbo.MaxAppraisalFormOptions (
	AppraisalFormID,
	AppraisalID,
	AppraisalFormOffer,
	AppraisalFormHistory,
	PrimaryBooksToDisplay,
	SecondaryBooksToDisplay,
	ShowEquipmentOptions,
	ShowPhotos
)
AS
SELECT
	AFO.AppraisalFormID,
	AFO.AppraisalID,
	AFO.AppraisalFormOffer,
	AFO.AppraisalFormHistory,
	AFO.PrimaryBooksToDisplay,
	AFO.SecondaryBooksToDisplay,
	AFO.ShowEquipmentOptions,
	AFO.ShowPhotos
FROM
	[IMT].dbo.AppraisalFormOptions AFO
JOIN	(
		SELECT
			AppraisalID,
			MAX(AppraisalFormID) AppraisalFormID
		FROM
			[IMT].dbo.AppraisalFormOptions
		GROUP BY
			AppraisalID
	) AFOM ON AFO.AppraisalFormID = AFOM.AppraisalFormID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MaxAppraisalAction]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[MaxAppraisalAction]
GO

CREATE VIEW dbo.MaxAppraisalAction (
	AppraisalActionID,
	AppraisalID,
	AppraisalActionTypeID,
	DateCreated,
	WholesalePrice,
	EstimatedReconditioningCost,
	ConditionDescription
)
AS
SELECT
	AA.AppraisalActionID,
	AA.AppraisalID,
	AA.AppraisalActionTypeID,
	AA.DateCreated,
	AA.WholesalePrice,
	AA.EstimatedReconditioningCost,
	AA.ConditionDescription
FROM
	[IMT].dbo.AppraisalActions AA
JOIN	(
		SELECT
			AppraisalID,
			MAX(AppraisalActionID) AppraisalActionID
		FROM
			[IMT].dbo.AppraisalActions
		GROUP BY
			AppraisalID
	) AAMAX ON (AAMAX.AppraisalID = AA.AppraisalID AND AA.AppraisalActionID = AAMAX.AppraisalActionID)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisals_V1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisals_V1]
GO

CREATE VIEW dbo.GetAppraisals_V1
(
	BusinessUnitID,
	AppraisalID,
	ThirdPartyID,
	ThirdPartyCategoryID,
	VehicleID,
	MakeModelGroupingID,
	ModelYear,
	Mileage,
	DateCreated,
	AppraisalValue,
	BookoutValue,
	CustomerOffer
)
AS
SELECT
	A.BusinessUnitID,
	A.AppraisalID,
	T.ThirdPartyID,
	B.ThirdPartyCategoryID,
	A.VehicleID,
	V.MakeModelGroupingID,
	V.VehicleYear,
	A.Mileage,
	A.DateCreated,
	AZ.Value,
	B.Value,
	AFO.AppraisalFormOffer
FROM
	[IMT].dbo.Appraisals A
JOIN	[FLDW].dbo.Appraisal_F AZ ON AZ.AppraisalID = A.AppraisalID AND AZ.BusinessUnitID = A.BusinessUnitID
JOIN	[FLDW].dbo.AppraisalBookout_F B ON B.AppraisalID = A.AppraisalID AND B.BusinessUnitID = A.BusinessUnitID
JOIN	[IMT].dbo.ThirdPartyCategories T ON T.ThirdPartyCategoryID = B.ThirdPartyCategoryID
JOIN	[IMT].dbo.tbl_Vehicle V ON A.VehicleID = V.VehicleID
LEFT JOIN dbo.MaxAppraisalFormOptions AFO ON AFO.AppraisalID = A.AppraisalID
LEFT JOIN dbo.MaxAppraisalAction AA ON A.AppraisalID = AA.AppraisalID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	A.DateCreated BETWEEN DATEADD(WW,-5,UC.RunDate) AND UC.RunDate
AND	COALESCE(AA.AppraisalActionTypeID,5) = 5
AND	((T.ThirdPartyID <> 3) OR (T.ThirdPartyID = 3 AND B.BookoutValueTypeID = 2)) -- kbb retail only
AND	NOT EXISTS (
		SELECT
			1
		FROM
			[IMT].dbo.Inventory I
		WHERE
			A.VehicleID = I.VehicleID
		AND	I.BusinessUnitID = A.BusinessUnitID
		AND	DATEDIFF(DD, [IMT].dbo.ToDate(A.DateCreated), I.InventoryReceivedDate) >= 0
	)
GO

-- select * from dbo.GetAppraisals_V1 WHERE BusinessUnitID = 100147

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadAppraisals_A1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadAppraisals_A1]
GO

CREATE PROCEDURE dbo.LoadAppraisals_A1
	-- no params
AS
	EXEC LoadTable 'Appraisals_A1', '', 'SELECT * FROM dbo.GetAppraisals_V1 WHERE BookoutValue IS NOT NULL'
GO

EXEC LoadAppraisals_A1 -- 2m 47s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A2#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A2#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A2#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A2#1
GO

CREATE TABLE dbo.Inventory_A2#0(
	BusinessUnitID             INT NOT NULL,
	GroupingDescriptionID      INT NOT NULL,
	ModelYear                  INT NOT NULL,
	Units                      INT NOT NULL,
	UnitsOverUnitCostThreshold INT NOT NULL,
	CONSTRAINT PK_Inventory_A2#0 PRIMARY KEY NONCLUSTERED (
		BusinessUnitID,
		GroupingDescriptionID,
		ModelYear
	)
)
GO

CREATE TABLE dbo.Inventory_A2#1(
	BusinessUnitID             INT NOT NULL,
	GroupingDescriptionID      INT NOT NULL,
	ModelYear                  INT NOT NULL,
	Units                      INT NOT NULL,
	UnitsOverUnitCostThreshold INT NOT NULL,
	CONSTRAINT PK_Inventory_A2#1 PRIMARY KEY NONCLUSTERED (
		BusinessUnitID,
		GroupingDescriptionID,
		ModelYear
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A2]
GO

CREATE VIEW dbo.Inventory_A2 AS SELECT * FROM Inventory_A2#0
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A2', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	2,
	'Inventory_A2',
	'Aggregated snapshot of active used car inventory',
	1,
	1,
	'GetInventory_V2',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V2]
GO

CREATE VIEW dbo.GetInventory_V2(
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	Units,
	UnitsOverUnitCostThreshold
)
AS
SELECT
	IX.BusinessUnitID,
	MMG.GroupingDescriptionID,
	VX.VehicleYear ModelYear,
	COUNT(*) Units,
	SUM(CASE WHEN IX.UnitCost < DP.UnitCostThresholdLower THEN 0 ELSE 1 END) UnitsOverUnitCostThreshold
FROM
	[IMT].dbo.Inventory IX
JOIN	[IMT].dbo.Vehicle VX ON IX.VehicleID = VX.VehicleID
JOIN	[IMT].dbo.tbl_MakeModelGrouping MMG ON MMG.MakeModelGroupingID = VX.MakeModelGroupingID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = IX.BusinessUnitID
WHERE
	IX.InventoryActive = 1
AND	IX.InventoryType   = 2
AND	IX.DeleteDT IS NULL
AND	NOT EXISTS (
		SELECT
			1
		FROM
			[IMT].dbo.AIP_Event E
		JOIN	[IMT].dbo.AIP_EventType T ON T.AIP_EventTypeID = E.AIP_EventTypeID
		WHERE
			E.InventoryID = IX.InventoryID
		AND	T.AIP_EventTypeID = E.AIP_EventTypeID
		AND	T.AIP_EventCategoryID IN (2,3)
	)
GROUP BY
	IX.BusinessUnitID,
	MMG.GroupingDescriptionID,
	VX.VehicleYear
GO

-- SELECT * FROM GetInventory_V2

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A2]
GO

CREATE PROCEDURE dbo.LoadInventory_A2
	-- no params
AS
	EXEC LoadTable 'Inventory_A2', '', 'SELECT * FROM dbo.GetInventory_V2 ORDER BY BusinessUnitID, GroupingDescriptionID, ModelYear'
GO

EXEC dbo.LoadInventory_A2 -- 37s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A3#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A3#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A3#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A3#1
GO

CREATE TABLE dbo.Inventory_A3#0(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	ModelYear             INT NULL,
	Units                 INT NOT NULL,
	CIACategoryID         INT NOT NULL
)
GO

CREATE INDEX IDX_Inventory_A3#0 ON Inventory_A3#0 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear
)
GO

CREATE INDEX IDX_Inventory_A3#0_BusinessUnit ON Inventory_A3#0 (
	BusinessUnitID
)
GO

CREATE TABLE dbo.Inventory_A3#1(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	ModelYear             INT NULL,
	Units                 INT NOT NULL,
	CIACategoryID         INT NOT NULL
)
GO

CREATE INDEX IDX_Inventory_A3#1 ON Inventory_A3#1 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear
)
GO

CREATE INDEX IDX_Inventory_A3#1_BusinessUnit ON Inventory_A3#1 (
	BusinessUnitID
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A3]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A3]
GO

CREATE VIEW dbo.Inventory_A3 AS SELECT * FROM Inventory_A3#0
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A3', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	3,
	'Inventory_A3',
	'Aggregated snapshot of optimal used car inventory',
	1,
	0,
	'GetInventory_V3',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetCIA_V1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetCIA_V1]
GO

CREATE VIEW dbo.GetCIA_V1 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	Units,
	CIAGroupingItemDetailTypeID,
	CIACategoryID
)
AS
SELECT
	S.BusinessUnitID,
	I.GroupingDescriptionID,
	CASE WHEN D.CIAGroupingItemDetailLevelID <> 2 THEN NULL ELSE CAST(D.Value AS INT) END,
	SUM(D.Units),
	D.CIAGroupingItemDetailTypeID,
	C.CIACategoryID
FROM
	[IMT].dbo.CIAGroupingItems I
JOIN	[IMT].dbo.CIACategories C ON I.CIACategoryID = C.CIACategoryID
JOIN	[IMT].dbo.CIAGroupingItemDetails D ON I.CIAGroupingItemID = D.CIAGroupingItemID
JOIN	[IMT].dbo.CIABodyTypeDetails B ON B.CIABodyTypeDetailID = I.CIABodyTypeDetailID
JOIN	[IMT].dbo.CIASummary S ON B.CIASummaryID = S.CIASummaryID
WHERE
	S.CIASummaryStatusID = 2
GROUP BY
	S.BusinessUnitID,
	I.GroupingDescriptionID,
	CASE WHEN D.CIAGroupingItemDetailLevelID <> 2 THEN NULL ELSE CAST(D.Value AS INT) END,
	D.CIAGroupingItemDetailTypeID,
	C.CIACategoryID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V3]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V3]
GO

CREATE VIEW dbo.GetInventory_V3	(
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	Units,
	CIACategoryID
)
AS
SELECT
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	Units,
	CIACategoryID
FROM
	GetCIA_V1
WHERE
	CIAGroupingItemDetailTypeID = 4
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A3]
GO

CREATE PROCEDURE dbo.LoadInventory_A3
	-- no params
AS
	EXEC LoadTable 'Inventory_A3', '', 'SELECT * FROM dbo.GetInventory_V3 ORDER BY BusinessUnitID, GroupingDescriptionID, ModelYear'
GO

EXEC LoadInventory_A3 -- 3s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A4#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A4#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A4#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A4#1
GO

CREATE TABLE dbo.Inventory_A4#0(
	BusinessUnitID                             INT NOT NULL,
	GroupingDescriptionID                      INT NOT NULL,
	ModelYear                                  INT NULL,
	StockedUnitsModelYear                      INT NOT NULL,
	StockedUnitsModel                          INT NOT NULL,
	StockedUnitsModelYearOverUnitCostThreshold INT NOT NULL,
	StockedUnitsModelOverUnitCostThreshold     INT NOT NULL,
	OptimalUnitsModelYear                      INT NULL,
	OptimalUnitsModel                          INT NULL,
	CIACategoryID                              INT NULL
)
GO

CREATE INDEX IDX_Inventory_A4#0 ON Inventory_A4#0 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear
)
GO

CREATE TABLE dbo.Inventory_A4#1(
	BusinessUnitID                             INT NOT NULL,
	GroupingDescriptionID                      INT NOT NULL,
	ModelYear                                  INT NULL,
	StockedUnitsModelYear                      INT NOT NULL,
	StockedUnitsModel                          INT NOT NULL,
	StockedUnitsModelYearOverUnitCostThreshold INT NOT NULL,
	StockedUnitsModelOverUnitCostThreshold     INT NOT NULL,
	OptimalUnitsModelYear                      INT NULL,
	OptimalUnitsModel                          INT NULL,
	CIACategoryID                              INT NULL
)
GO

CREATE INDEX IDX_Inventory_A4#1 ON Inventory_A4#1 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A4]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A4]
GO

CREATE VIEW dbo.Inventory_A4 AS SELECT * FROM Inventory_A4#1
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A4', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	4,
	'Inventory_A4',
	'Aggregated snapshot of stocked-, optimal- and optimal-stocked-inventory',
	1,
	0,
	'GetInventory_V4',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V4_Stocked]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V4_Stocked]
GO

CREATE VIEW dbo.GetInventory_V4_Stocked (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	StockedUnitsModelYear,
	StockedUnitsModel,
	StockedUnitsModelYearOverUnitCostThreshold,
	StockedUnitsModelOverUnitCostThreshold
)
AS
SELECT
	A2.BusinessUnitID,
	A2.GroupingDescriptionID,
	A2.ModelYear,
	COALESCE(A2.Units,0) StockedUnitsModelYear,
	COALESCE(A2S.Units,0) StockedUnitsModel,
	COALESCE(A2.UnitsOverUnitCostThreshold,0) StockedUnitsModelYearOverUnitCostThreshold,
	COALESCE(A2S.UnitsOverUnitCostThreshold,0) StockedUnitsModelOverUnitCostThreshold
FROM
	dbo.Inventory_A2 A2
JOIN	(
		SELECT
			BusinessUnitID,
			GroupingDescriptionID,
			SUM(Units) Units,
			SUM(UnitsOverUnitCostThreshold) UnitsOverUnitCostThreshold
		FROM
			dbo.Inventory_A2
		GROUP BY
			BusinessUnitID,
			GroupingDescriptionID
	) A2S ON
	(
			A2S.BusinessUnitID        = A2.BusinessUnitID
		AND	A2S.GroupingDescriptionID = A2.GroupingDescriptionID
	)
GROUP BY
	A2.BusinessUnitID,
	A2.GroupingDescriptionID,
	A2.ModelYear,
	A2.Units,
	A2.UnitsOverUnitCostThreshold,
	A2S.Units,
	A2S.UnitsOverUnitCostThreshold
GO

-- select * from GetInventory_V4_Stocked WHERE BusinessUnitID = 100215 and GroupingDescriptionID = 101717
-- select * from GetInventory_V4_Optimal WHERE BusinessUnitID = 100215 and GroupingDescriptionID = 101717

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V4_Optimal]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V4_Optimal]
GO

CREATE VIEW dbo.GetInventory_V4_Optimal (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	CIACategoryID
)
AS
SELECT
	A3.BusinessUnitID,
	A3.GroupingDescriptionID,
	A3.ModelYear,
	CASE WHEN A3.ModelYear IS NULL THEN NULL ELSE A3.Units END,
	A3S.Units,
	A3.CIACategoryID
FROM
	dbo.Inventory_A3 A3
JOIN	(
		SELECT
			BusinessUnitID,
			GroupingDescriptionID,
			SUM(Units) Units
		FROM
			dbo.Inventory_A3
		GROUP BY
			BusinessUnitID,
			GroupingDescriptionID
	) A3S ON
	(
			A3S.BusinessUnitID        = A3.BusinessUnitID
		AND	A3S.GroupingDescriptionID = A3.GroupingDescriptionID
	)
GROUP BY
	A3.BusinessUnitID,
	A3.GroupingDescriptionID,
	A3.ModelYear,
	A3.Units,
	A3S.Units,
	A3.CIACategoryID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V4]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V4]
GO

CREATE VIEW dbo.GetInventory_V4 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	StockedUnitsModelYear,
	StockedUnitsModel,
	StockedUnitsModelYearOverUnitCostThreshold,
	StockedUnitsModelOverUnitCostThreshold,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	CIACategoryID
)
AS
SELECT
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	StockedUnitsModelYear,
	StockedUnitsModel,
	StockedUnitsModelYearOverUnitCostThreshold,
	StockedUnitsModelOverUnitCostThreshold,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	CIACategoryID
FROM
(
	-- in stock "not-optimal in any way what-so-ever"
	SELECT
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		NULL OptimalUnitsModelYear,
		NULL OptimalUnitsModel,
		NULL CIACategoryID
	FROM
		dbo.GetInventory_V4_Stocked A2
	WHERE
		NOT EXISTS (
			SELECT
				1
			FROM
				dbo.Inventory_A3 A3
			WHERE
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
		)
	GROUP BY
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold
UNION ALL
	-- in stock "optimal-model not optimal-model-year"
	SELECT
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
	FROM
		dbo.GetInventory_V4_Stocked A2
	JOIN	dbo.GetInventory_V4_Optimal A3 ON
		(
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
		)
	WHERE
		A3.ModelYear IS NULL
	GROUP BY
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
UNION ALL
	-- out-of-stock "optimal-model not optimal-model-year"
	SELECT
		A3.BusinessUnitID,
		A3.GroupingDescriptionID,
		A3.ModelYear,
		0 StockedUnitsModelYear,
		0 StockedUnitsModel,
		0 StockedUnitsModelYearOverUnitCostThreshold,
		0 StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
	FROM
		dbo.GetInventory_V4_Optimal A3
	WHERE
		A3.ModelYear IS NULL
	AND	NOT EXISTS (
			SELECT
				1
			FROM
				dbo.Inventory_A2 A2
			WHERE
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
		)
	GROUP BY
		A3.BusinessUnitID,
		A3.GroupingDescriptionID,
		A3.ModelYear,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
UNION ALL
	-- in-stock "non-optimal-model-year where model has optimal-model-year"
	SELECT
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		NULL OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
	FROM
		dbo.GetInventory_V4_Stocked A2
	JOIN	dbo.GetInventory_V4_Optimal A3 ON
		(
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
		)
	WHERE
		A3.ModelYear IS NOT NULL
	AND	A2.ModelYear NOT IN (
			SELECT
				XX.ModelYear
			FROM
				dbo.GetInventory_V4_Optimal XX
			WHERE
				XX.BusinessUnitID         = A2.BusinessUnitID
			AND	XX.GroupingDescriptionID = A2.GroupingDescriptionID
		)
	GROUP BY
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
UNION ALL
	-- in-stock "optimal-model-year"
	SELECT
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
	FROM
		dbo.GetInventory_V4_Stocked A2
	JOIN	dbo.GetInventory_V4_Optimal A3 ON
		(
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
			AND	A2.ModelYear             = A3.ModelYear
		)
	WHERE
		A3.ModelYear IS NOT NULL
	GROUP BY
		A2.BusinessUnitID,
		A2.GroupingDescriptionID,
		A2.ModelYear,
		A2.StockedUnitsModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelYearOverUnitCostThreshold,
		A2.StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
UNION ALL
	-- out-of-stock "optimal-model-year"
	SELECT
		A3.BusinessUnitID,
		A3.GroupingDescriptionID,
		A3.ModelYear,
		0 StockedUnitsModelYear,
		COALESCE(A2.StockedUnitsModel,0) StockedUnitsModel,
		0 StockedUnitsModelYearOverUnitCostThreshold,
		COALESCE(A2.StockedUnitsModelOverUnitCostThreshold,0) StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
	FROM
		dbo.GetInventory_V4_Optimal A3
	LEFT JOIN
		(
			SELECT
				BusinessUnitID,
				GroupingDescriptionID,
				StockedUnitsModel,
				StockedUnitsModelOverUnitCostThreshold
			FROM
				dbo.GetInventory_V4_Stocked
			GROUP BY
				BusinessUnitID,
				GroupingDescriptionID,
				StockedUnitsModel,
				StockedUnitsModelOverUnitCostThreshold
		) A2 ON
		(
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
		)
	WHERE
		A3.ModelYear IS NOT NULL
	AND	NOT EXISTS (
			SELECT
				1
			FROM
				dbo.Inventory_A2 A2
			WHERE
				A2.BusinessUnitID        = A3.BusinessUnitID
			AND	A2.GroupingDescriptionID = A3.GroupingDescriptionID
			AND	A2.ModelYear             = A3.ModelYear
		)
	GROUP BY
		A3.BusinessUnitID,
		A3.GroupingDescriptionID,
		A3.ModelYear,
		A2.StockedUnitsModel,
		A2.StockedUnitsModelOverUnitCostThreshold,
		A3.OptimalUnitsModelYear,
		A3.OptimalUnitsModel,
		A3.CIACategoryID
) T
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A4]
GO

CREATE PROCEDURE dbo.LoadInventory_A4
	-- no params
AS
	EXEC LoadTable 'Inventory_A4', '', 'SELECT * FROM dbo.GetInventory_V4 ORDER BY BusinessUnitID, GroupingDescriptionID, ModelYear'
GO

EXEC LoadInventory_A4 -- 8s

-- select * from Inventory_A4 order by businessunitid, groupingdescriptionid, modelyear

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A5#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A5#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A5#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A5#1
GO

CREATE TABLE dbo.Inventory_A5#0(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	ModelYear             INT NULL,
	Units                 INT NOT NULL,
	CIACategoryID         INT NOT NULL
)
GO

CREATE INDEX IDX_Inventory_A5#0 ON Inventory_A5#0 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear
)
GO

CREATE TABLE dbo.Inventory_A5#1(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	ModelYear             INT NULL,
	Units                 INT NOT NULL,
	CIACategoryID         INT NOT NULL
)
GO

CREATE INDEX IDX_Inventory_A5#1 ON Inventory_A5#1 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A5]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A5]
GO

CREATE VIEW dbo.Inventory_A5 AS SELECT * FROM Inventory_A5#0
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A5', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	5,
	'Inventory_A5',
	'Default Search and Aquisition Buying Plan',
	1,
	0,
	'GetInventory_V5',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V5]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V5]
GO

CREATE VIEW dbo.GetInventory_V5 (
	BusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	Units,
	CIACategoryID
)
AS
SELECT
	COALESCE(O.BusinessUnitID,B.BusinessUnitID) BusinessUnitID,
	COALESCE(O.GroupingDescriptionID,B.GroupingDescriptionID) GroupingDescriptionID,
	COALESCE(O.ModelYear,B.ModelYear) ModelYear,
	COALESCE(O.Units,B.Units) Units,
	COALESCE(O.CIACategoryID,B.CIACategoryID) CIACategoryID
FROM
(
	-- optimal understocked targets
	SELECT
		O.BusinessUnitID,
		O.GroupingDescriptionID,
		CASE WHEN O.OptimalUnitsModelYear IS NULL THEN NULL ELSE O.ModelYear END ModelYear,
		CASE WHEN O.OptimalUnitsModelYear IS NULL THEN O.OptimalUnitsModel ELSE O.OptimalUnitsModelYear END Units,
		O.CIACategoryID
	FROM
		dbo.Inventory_A4 O
	WHERE
		OptimalUnitsModel IS NOT NULL
	AND	StockedUnitsModel < OptimalUnitsModel
) O
FULL OUTER JOIN
(
	-- buy targets with year level preferences
	SELECT
		B.BusinessUnitID,
		B.GroupingDescriptionID,
		P.ModelYear,
		B.Units,
		B.CIACategoryID
	FROM
		dbo.GetCIA_V1 B
		LEFT JOIN dbo.GetCIA_V1 P ON
			B.GroupingDescriptionID = P.GroupingDescriptionID
		AND	B.BusinessUnitID        = P.BusinessUnitID
	WHERE
		B.CIAGroupingItemDetailTypeID = 3
	AND	P.CIAGroupingItemDetailTypeID = 1
	AND	P.ModelYear IS NOT NULL
) B ON
	B.GroupingDescriptionID = O.GroupingDescriptionID
AND	B.BusinessUnitID = O.BusinessUnitID
FULL OUTER JOIN
(
	-- avoid targets
	SELECT
		A.BusinessUnitID,
		A.GroupingDescriptionID,
		A.ModelYear,
		NULL Units,
		NULL CIACategoryID
	FROM
		dbo.GetCIA_V1 A
	WHERE
		A.CIAGroupingItemDetailTypeID = 2
) A ON
	A.GroupingDescriptionID = O.GroupingDescriptionID
AND	A.BusinessUnitID = O.BusinessUnitID
WHERE
	A.GroupingDescriptionID IS NULL                       -- skip avoids
AND	COALESCE(O.CIACategoryID,B.CIACategoryID) <> 4        -- not market performers
GO

-- select * from GetInventory_V5 -- 4s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A5]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A5]
GO

CREATE PROCEDURE dbo.LoadInventory_A5
	-- no params
AS
	EXEC LoadTable 'Inventory_A5', '', 'SELECT * FROM dbo.GetInventory_V5 ORDER BY BusinessUnitID, GroupingDescriptionID, ModelYear'
GO

EXEC dbo.LoadInventory_A5 -- 4s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A6#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A6#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A6#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A6#1
GO

CREATE TABLE dbo.Inventory_A6#0(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	InventoryID           INT NOT NULL,
	ModelYear             INT NOT NULL,
	Mileage               INT NOT NULL,
	Active                TINYINT NOT NULL,
	DaysToSale            TINYINT NULL,
	TotalGross            INT NULL,
	CONSTRAINT PK_Inventory_A6#0 PRIMARY KEY NONCLUSTERED (
		InventoryID
	),
	CONSTRAINT CK_Inventory_A6#0_Active CHECK (
		Active = 1
	)
)
GO

CREATE TABLE dbo.Inventory_A6#1(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	InventoryID           INT NOT NULL,
	ModelYear             INT NOT NULL,
	Mileage               INT NOT NULL,
	Active                TINYINT NOT NULL,
	DaysToSale            TINYINT NULL,
	TotalGross            INT NULL,
	CONSTRAINT PK_Inventory_A6#1 PRIMARY KEY NONCLUSTERED (
		InventoryID
	),
	CONSTRAINT CK_Inventory_A6#1_Active CHECK (
		Active = 1
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A6]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A6]
GO

CREATE VIEW dbo.Inventory_A6 AS SELECT * FROM Inventory_A6#1
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A6', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	12,
	'Inventory_A6',
	'Active Inventory',
	1,
	0,
	'GetInventory_V6',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V6]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V6]
GO

-- active inventory
CREATE VIEW dbo.GetInventory_V6 (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross
)
AS
SELECT
	I.BusinessUnitID,
	V.GroupingDescriptionID,
	I.InventoryID,
	V.VehicleYear,
	I.MileageReceived,
	I.InventoryActive,
	NULL,
	NULL
FROM
	[FLDW].dbo.InventoryActive I
JOIN	[FLDW].dbo.Vehicle V ON I.VehicleID = V.VehicleID
WHERE
	I.InventoryActive = 1
AND	I.InventoryType = 2
AND	I.DeleteDT IS null
AND	NOT EXISTS (
		SELECT
			1
		FROM
			[IMT].dbo.AIP_Event E
		JOIN	[IMT].dbo.AIP_EventType T ON T.AIP_EventTypeID = E.AIP_EventTypeID
		WHERE
			E.InventoryID = I.InventoryID
		AND	T.AIP_EventTypeID = E.AIP_EventTypeID
		AND	T.AIP_EventCategoryID IN (2,3)
	)
GROUP BY
	I.BusinessUnitID,
	V.GroupingDescriptionID,
	I.InventoryID,
	V.VehicleYear,
	I.MileageReceived,
	I.InventoryActive
GO

-- select * from GetInventory_V6

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A6]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A6]
GO

CREATE PROCEDURE dbo.LoadInventory_A6
	-- no params
AS
	EXEC LoadTable 'Inventory_A6', '', 'SELECT * FROM dbo.GetInventory_V6 ORDER BY BusinessUnitID, GroupingDescriptionID, InventoryID'
GO

EXEC dbo.LoadInventory_A6 -- 16s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A7#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A7#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A7#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A7#1
GO

CREATE TABLE dbo.Inventory_A7#0(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	InventoryID           INT NOT NULL,
	ModelYear             INT NOT NULL,
	Mileage               INT NOT NULL,
	Active                TINYINT NOT NULL,
	DaysToSale            TINYINT NULL,
	TotalGross            INT NULL,
	CONSTRAINT PK_Inventory_A7#0 PRIMARY KEY NONCLUSTERED (
		InventoryID
	),
	CONSTRAINT CK_Inventory_A7#0_Active CHECK (
		Active = 0
	)
)
GO

CREATE TABLE dbo.Inventory_A7#1(
	BusinessUnitID        INT NOT NULL,
	GroupingDescriptionID INT NOT NULL,
	InventoryID           INT NOT NULL,
	ModelYear             INT NOT NULL,
	Mileage               INT NOT NULL,
	Active                TINYINT NOT NULL,
	DaysToSale            TINYINT NULL,
	TotalGross            INT NULL,
	CONSTRAINT PK_Inventory_A7#1 PRIMARY KEY NONCLUSTERED (
		InventoryID
	),
	CONSTRAINT CK_Inventory_A7#1_Active CHECK (
		Active = 0
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A7]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A7]
GO

CREATE VIEW dbo.Inventory_A7 AS SELECT * FROM Inventory_A7#1
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A7', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	13,
	'Inventory_A7',
	'Inactive inventory sold within the last 100 days',
	1,
	0,
	'GetInventory_V7',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V7]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V7]
GO

-- sold inventory
CREATE VIEW dbo.GetInventory_V7 (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross
)
AS
SELECT
	I.BusinessUnitID,
	M.GroupingDescriptionID,
	I.InventoryID,
	V.VehicleYear,
	I.MileageReceived,
	CASE WHEN I.InventoryActive = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END Active,
	CASE WHEN DATEDIFF(DD,I.InventoryReceivedDate,VS.DealDate) <= 255 THEN DATEDIFF(DD,I.InventoryReceivedDate,VS.DealDate) ELSE 255 END DaysToSale,
	TotalGross
FROM
	[IMT].dbo.tbl_VehicleSale VS
JOIN	[IMT].dbo.Inventory I ON I.InventoryID = VS.InventoryID
JOIN	[IMT].dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN	[IMT].dbo.tbl_MakeModelGrouping M ON M.MakeModelGroupingID = V.MakeModelGroupingID
WHERE
	VS.DealDate >= DATEADD(DD,-100,GETDATE())
AND	I.InventoryType = 2
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A7]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A7]
GO

CREATE PROCEDURE dbo.LoadInventory_A7
	-- no params
AS
	EXEC LoadTable 'Inventory_A7', '', 'SELECT * FROM dbo.GetInventory_V7 ORDER BY BusinessUnitID, GroupingDescriptionID, ModelYear, DaysToSale'
GO

EXEC dbo.LoadInventory_A7 -- 26s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CreateView_GetInventory]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CreateView_GetInventory]
GO

CREATE PROCEDURE dbo.CreateView_GetInventory
	-- no params
AS
	DECLARE @A6 CHAR(1), @A7 CHAR(1), @MSG VARCHAR(255), @err INT

	SELECT @A6 = CAST(TableIndex AS CHAR(1)) FROM dbo.ViewStatus WHERE ViewName = 'Inventory_A6'
	SELECT @A7 = CAST(TableIndex AS CHAR(1)) FROM dbo.ViewStatus WHERE ViewName = 'Inventory_A7'

	if exists (select * from dbo.sysobjects where id = object_id(N'GetInventory') and OBJECTPROPERTY(id, N'IsView') = 1)
	BEGIN
		SET @MSG = 'DROP VIEW dbo.GetInventory'
		EXECUTE (@MSG)
		IF @err <> 0 GOTO Failed
	END

	SET @MSG = 'CREATE VIEW dbo.GetInventory AS SELECT * FROM Inventory_A6#' + @A6 + ' UNION ALL SELECT * FROM Inventory_A7#' + @A7
	EXECUTE (@MSG)
	IF @err <> 0 GOTO Failed

	RETURN 0

Failed:

	SELECT cast(@err AS VARCHAR), @MSG

	RETURN 1
GO

-- EXEC dbo.CreateView_GetInventory

-- SELECT COUNT(*) FROM GetInventory

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ZipCode
GO

CREATE TABLE dbo.ZipCode (
	ZipCodeID INT IDENTITY(1,1) NOT NULL,
	ZipCode   CHAR(5)  NULL,
	Lat       FLOAT(8) NULL,
	Long      FLOAT(8) NULL,
	CONSTRAINT PK_ZipCode PRIMARY KEY NONCLUSTERED (
		ZipCodeID
	),
	CONSTRAINT UK_ZipCode UNIQUE NONCLUSTERED (
		ZipCode
	)
)
GO

CREATE CLUSTERED INDEX IDX_ZipCode ON ZipCode (
	ZipCode
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsertNewZipCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[InsertNewZipCodes]
GO

CREATE PROCEDURE dbo.InsertNewZipCodes
	-- no params
AS
	SET NOCOUNT ON

	INSERT INTO ZipCode (
		ZipCode, Lat, Long
	)
	SELECT DISTINCT
		T.ZipCode, LL.Lat, LL.Long
	FROM	(
		SELECT DISTINCT
			ZipCode
		FROM
			[IMT].dbo.BusinessUnit
		WHERE
			ZipCode LIKE '[0-9][0-9][0-9][0-9][0-9]'
	UNION ALL
		SELECT DISTINCT
			ZipCode
		FROM
			[Auction]..Location
		WHERE
			ZipCode LIKE '[0-9][0-9][0-9][0-9][0-9]'
	UNION ALL
		SELECT DISTINCT
			ZIP_CODE ZipCode
		FROM
			[ATC]..Vehicles
		WHERE
			ZIP_CODE LIKE '[0-9][0-9][0-9][0-9][0-9]'
	) T

	JOIN (	SELECT	ZipCode	= ZIP,
			MIN(Lat) Lat,
			MIN(Long) Long
		FROM	[MARKET].dbo.Market_Ref_ZipX
		GROUP
		BY	ZIP
		) LL on T.ZipCode = LL.ZipCode

	WHERE
		NOT EXISTS (
			SELECT
				1
			FROM
				dbo.ZipCode Z
			WHERE
				Z.ZipCode = T.ZipCode
		)
	ORDER BY
		T.ZipCode
GO

EXEC dbo.InsertNewZipCodes

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode_A1#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ZipCode_A1#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode_A1#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ZipCode_A1#1
GO

CREATE TABLE dbo.ZipCode_A1#0 (
	ZipCodeID1 INT NOT NULL,
	ZipCodeID2 INT NOT NULL,
	Distance   INT NOT NULL
)
GO

CREATE INDEX IDX_ZipCode_A1#0_Full ON ZipCode_A1#0 (
	ZipCodeID1,
	ZipCodeID2
)
GO

CREATE INDEX IDX_ZipCode_A1#0_Left ON ZipCode_A1#0 (
	ZipCodeID1
)
GO

CREATE INDEX IDX_ZipCode_A1#0_Right ON ZipCode_A1#0 (
	ZipCodeID2
)
GO

CREATE TABLE dbo.ZipCode_A1#1 (
	ZipCodeID1 INT NOT NULL,
	ZipCodeID2 INT NOT NULL,
	Distance   INT NOT NULL
)
GO

CREATE INDEX IDX_ZipCode_A1#1_Full ON ZipCode_A1#1 (
	ZipCodeID1,
	ZipCodeID2
)
GO

CREATE INDEX IDX_ZipCode_A1#1_Left ON ZipCode_A1#1 (
	ZipCodeID1
)
GO

CREATE INDEX IDX_ZipCode_A1#1_Right ON ZipCode_A1#1 (
	ZipCodeID2
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode_A1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ZipCode_A1]
GO

CREATE VIEW dbo.ZipCode_A1 AS SELECT * FROM ZipCode_A1#0
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('ZipCode_A1', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	6,
	'ZipCode_A1',
	'Pre-Computed ZipCode Distances',
	1,
	0,
	'GetZipCode_V1',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetZipCode_V1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetZipCode_V1]
GO

CREATE VIEW dbo.GetZipCode_V1 (
	ZipCodeID1,
	ZipCodeID2,
	Distance
)
AS
SELECT
	ZC1.ZipCodeID,
	ZC2.ZipCodeID,
	degrees(acos(
		(sin(radians(ZC1.Lat)) *
			sin(radians(ZC2.Lat)) +
			cos(radians(ZC1.Lat)) *
			cos(radians(ZC2.Lat)) *
			cos(radians(ZC1.Long - ZC2.Long))
		))) * 69.09
FROM
	dbo.ZipCode ZC1
JOIN	dbo.ZipCode ZC2 ON ZC2.ZipCodeID > ZC1.ZipCodeID
WHERE
	ZC1.Long IS NOT NULL AND ZC1.Lat IS NOT NULL
AND	ZC2.Long IS NOT NULL AND ZC2.Lat IS NOT NULL
GO

CREATE FUNCTION dbo.ZipCodeDistance(
	@ZipCode1 CHAR(5),
	@ZipCode2 CHAR(5)
)
RETURNS FLOAT AS
BEGIN
	DECLARE @M1_Lat FLOAT, @M1_Long FLOAT, @M2_Lat FLOAT, @M2_Long FLOAT

	SELECT
		@M1_Lat  = MIN(Lat),
		@M1_Long = MIN(Long)
	FROM
		[MARKET].dbo.Market_Ref_ZipX
	WHERE
		ZIP = @ZipCode1

	SELECT
		@M2_Lat  = MIN(Lat),
		@M2_Long = MIN(Long)
	FROM
		[MARKET].dbo.Market_Ref_ZipX
	WHERE
		ZIP = @ZipCode2
	
	RETURN (degrees(acos(
		(sin(radians(@M1_Lat)) *
	              sin(radians(@M2_Lat)) +
	              cos(radians(@M1_Lat)) *
	              cos(radians(@M2_Lat)) *
	              cos(radians(@M1_Long - @M2_Long)))
	))) * 69.09
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadZipCode_A1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadZipCode_A1]
GO

CREATE PROCEDURE dbo.LoadZipCode_A1
	-- no params
AS
	EXEC LoadTable 'ZipCode_A1', '', 'SELECT * FROM dbo.GetZipCode_V1 ORDER BY ZipCodeID1, ZipCodeID2'
GO

EXEC dbo.LoadZipCode_A1 -- 7m 10s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MileageRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MileageRange
GO

CREATE TABLE dbo.MileageRange(
	MileageRangeID INT IDENTITY(1,1) NOT NULL,
	LowMileageRange INT NOT NULL,
	HighMileageRange INT NOT NULL,
	CONSTRAINT PK_MileageRange PRIMARY KEY NONCLUSTERED (
		MileageRangeID
	)
)
GO

CREATE INDEX IDX_MileageRange ON dbo.MileageRange (
	LowMileageRange,
	HighMileageRange
)
GO

INSERT INTO dbo.MileageRange (LowMileageRange, HighMileageRange)
SELECT DISTINCT
	LowMileageRange,
	HighMileageRange
FROM
	[AuctionNet].dbo.AuctionTransaction_AV2
ORDER BY
	LowMileageRange,
	HighMileageRange
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Auction_A1#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Auction_A1#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Auction_A1#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Auction_A1#1
GO

CREATE TABLE dbo.Auction_A1#0 (
	MakeModelGroupingID INT NOT NULL,
	ModelYear INT NOT NULL,
	AreaID INT NOT NULL,
	PeriodID INT NOT NULL,
	MileageRangeID INT NOT NULL,
	AverageSalePrice DECIMAL(8,2) NOT NULL
)
GO

CREATE INDEX IDX_Auction_A1#0 ON Auction_A1#0 (
	MakeModelGroupingID,
	ModelYear,
	AreaID,
	PeriodID,
	MileageRangeID
)
GO

CREATE INDEX IDX_Auction_A1#0_Period ON Auction_A1#0 (
	PeriodID
)
GO

CREATE TABLE dbo.Auction_A1#1 (
	MakeModelGroupingID INT NOT NULL,
	ModelYear INT NOT NULL,
	AreaID INT NOT NULL,
	PeriodID INT NOT NULL,
	MileageRangeID INT NOT NULL,
	AverageSalePrice DECIMAL(8,2) NOT NULL
)
GO

CREATE INDEX IDX_Auction_A1#0 ON Auction_A1#1 (
	MakeModelGroupingID,
	ModelYear,
	AreaID,
	PeriodID,
	MileageRangeID
)
GO

CREATE INDEX IDX_Auction_A1#1_Period ON Auction_A1#1 (
	PeriodID
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Auction_A1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Auction_A1]
GO

CREATE VIEW dbo.Auction_A1 AS SELECT * FROM Auction_A1#0
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Auction_A1', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	7,
	'Auction_A1',
	'NAAA Auction Data for vehicles with a single series (trim)',
	1,
	0,
	'GetAuction_V1',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAuction_V1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAuction_V1]
GO

CREATE VIEW dbo.GetAuction_V1(
	MakeModelGroupingID,
	ModelYear,
	AreaID,
	PeriodID,
	MileageRangeID,
	AverageSalePrice
)
AS
SELECT
	A.MakeModelGroupingID,
	A.ModelYear,
	A.AreaID,
	A.PeriodID,
	R.MileageRangeID,
	A.AveSalePrice
FROM
	[AuctionNet].dbo.AuctionTransaction_A4 A
JOIN	dbo.MileageRange R ON
	(
			R.LowMileageRange = A.LowMileageRange
		AND	R.HighMileageRange = A.HighMileageRange
	)
WHERE
	MakeModelGroupingID <> 0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadAuction_A1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadAuction_A1]
GO

CREATE PROCEDURE dbo.LoadAuction_A1
	-- no params
AS
	EXEC LoadTable 'Auction_A1', '', 'SELECT * FROM dbo.GetAuction_V1 ORDER BY MakeModelGroupingID, ModelYear, AreaID, PeriodID, MileageRangeID'
GO

EXEC LoadAuction_A1 -- 20s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BusinessUnitAccessGroups]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[BusinessUnitAccessGroups]
GO

CREATE VIEW dbo.BusinessUnitAccessGroups(
	BusinessUnitID,
	AccessGroupID
)
AS
SELECT DISTINCT
	BusinessUnitID,
	AccessGroupID
FROM
(
	SELECT
		BU.BusinessUnitID,
		AG.AccessGroupID
	FROM
		[ATC].dbo.AccessGroup AG
		CROSS JOIN [IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	WHERE
		AG.AccessGroupTypeID = 3
	AND	AG.Public_Group = 1
	AND	DP.ATCEnabled = 1
	AND	DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
UNION ALL
	SELECT
		BU.BusinessUnitID,
		AG.AccessGroupID
	FROM
		[ATC].dbo.AccessGroup AG
		CROSS JOIN [IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	WHERE
		AG.AccessGroupTypeID = 4
	AND	AG.Public_Group = 1
	AND	DP.GMACEnabled = 1
	AND	DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
UNION ALL
	SELECT
		BU.BusinessUnitID,
		AG.AccessGroupID
	FROM
		[ATC].dbo.AccessGroup AG
		CROSS JOIN [IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	WHERE
		AG.AccessGroupTypeID = 5
	AND	AG.Public_Group = 1
	AND	DP.TFSEnabled = 1
	AND	DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
UNION ALL
	SELECT
		BU.BusinessUnitID,
		AG.AccessGroupID
	FROM
		[ATC].dbo.AccessGroup AG
		CROSS JOIN [IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	WHERE
		AG.AccessGroupTypeID = 6
	AND	AG.Public_Group = 0
	AND	DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
UNION ALL
	SELECT
		BU.BusinessUnitID,
		AG.AccessGroupID
	FROM
		[ATC].dbo.AccessGroup AG
		CROSS JOIN [IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	WHERE
		AG.AccessGroupTypeID = 7
	AND	DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
UNION ALL
	SELECT
		BU.BusinessUnitID,
		AG.AccessGroupID
	FROM
		[ATC].dbo.AccessGroup AG
		CROSS JOIN [IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	WHERE
		AG.AccessGroupTypeID = 8
	AND	AG.Public_Group = 1
	AND	DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
UNION ALL
	SELECT
		BU.BusinessUnitID,
		DAG.AccessGroupID
	FROM
		[IMT].dbo.BusinessUnit BU
		JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
		JOIN [IMT].dbo.tbl_DealerATCAccessGroups DAG ON DAG.BusinessUnitID = BU.BusinessUnitID
	WHERE
		DP.GoLiveDate IS NOT NULL
	AND	BU.Active = 1
) AG
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Auction_A2#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Auction_A2#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Auction_A2#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Auction_A2#1
GO

CREATE TABLE dbo.Auction_A2#0 (
	BusinessUnitID   INT NULL,
	AccessGroupID    INT NULL,
	SaleID           INT NULL,
	PrecisionMatches INT NULL,
	TotalMatches     INT NULL
)
GO

CREATE TABLE dbo.Auction_A2#1 (
	BusinessUnitID   INT NULL,
	AccessGroupID    INT NULL,
	SaleID           INT NULL,
	PrecisionMatches INT NULL,
	TotalMatches     INT NULL
)
GO

CREATE VIEW dbo.Auction_A2 AS SELECT * FROM Auction_A2#1
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Auction_A2', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	8,
	'Auction_A2',
	'Online and In-Group "Auction" Data Summary',
	1,
	0,
	'GetAuction_V2',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAuction_V2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAuction_V2]
GO

CREATE VIEW dbo.GetAuction_V2 (
	BusinessUnitID,
	AccessGroupID,
	SaleID,
	PrecisionMatches,
	TotalMatches
)
AS
SELECT DISTINCT
	BusinessUnitID,
	AccessGroupID,
	SaleID,
	PrecisionMatches,
	TotalMatches
FROM
(
	SELECT
		T1.BusinessUnitID,
		T1.AccessGroupID,
		T1.SaleID,
		COUNT(*) PrecisionMatches,
		SUM(T1.Matches) AS TotalMatches
	FROM
	(
		SELECT
			BusinessUnitID,
			AccessGroupID,
			SaleID,
			GroupingDescriptionID,
			ModelYear,
			Matches
		FROM
		(
			SELECT
				BU.BusinessUnitID,
				VAG.AccessGroupID,
				NULL SaleID,
				I5.GroupingDescriptionID,
				I5.ModelYear,
				COUNT(*) Matches
			FROM
				[IMT].dbo.BusinessUnit BU
			JOIN	[IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
			JOIN	dbo.BusinessUnitAccessGroups BUAG ON BU.BusinessUnitID = BUAG.BusinessUnitID
			JOIN	[ATC].dbo.AccessGroup AG ON BUAG.AccessGroupID = AG.AccessGroupID
			JOIN	[ATC].dbo.VehicleAccessGroups VAG ON VAG.AccessGroupID = AG.AccessGroupID
			JOIN	[ATC].dbo.Vehicles V ON V.VehicleID = VAG.VehicleID
			JOIN	[ATC].dbo.VehicleProperties VP ON VP.VehicleID = V.VehicleID
			JOIN	[IMT].dbo.tbl_MakeModelGrouping MMG ON MMG.MakeModelGroupingID = VP.MakeModelGroupingID
			JOIN	dbo.Inventory_A5 I5 ON
				(
						I5.BusinessUnitID = BU.BusinessUnitID
					AND	I5.GroupingDescriptionID = MMG.GroupingDescriptionID
					AND	VP.VehicleYear = COALESCE(I5.ModelYear,VP.VehicleYear)
				)
			JOIN	dbo.UserContext UC ON UC.UserContextID = 1
			JOIN	dbo.ZipCode ZC1 ON BU.ZipCode = ZC1.ZipCode
			JOIN	dbo.ZipCode ZC2 ON V.ZIP_CODE = ZC2.ZipCode
			LEFT JOIN dbo.ZipCode_A1 ZCD1 ON (ZC1.ZipCodeID = ZCD1.ZipCodeID1 AND ZC2.ZipCodeID = ZCD1.ZipCodeID2)
			LEFT JOIN dbo.ZipCode_A1 ZCD2 ON (ZC1.ZipCodeID = ZCD2.ZipCodeID2 AND ZC2.ZipCodeID = ZCD2.ZipCodeID1)
			where
				DATEADD(HH, +2,CAST(V.AUCTION_END AS DATETIME)) > UC.RunDate
			AND	COALESCE(ZCD1.Distance,ZCD2.Distance,-1) BETWEEN 0 AND DP.PurchasingDistanceFromDealer
			GROUP BY
				BU.BusinessUnitID,
				VAG.AccessGroupID,
				I5.GroupingDescriptionID,
				I5.ModelYear
		UNION ALL
			SELECT
				BU.BusinessUnitID,
				141 AccessGroupID,
				NULL SaleID,
				BUI5.GroupingDescriptionID,
				BUI5.ModelYear,
				BU2I2.Units Matches
			FROM
				[IMT].dbo.BusinessUnit BU
			JOIN	[IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
			JOIN	[IMT].dbo.BusinessUnitRelationship BR1 on BU.BusinessUnitID = BR1.BusinessUnitID
			JOIN	[IMT].dbo.BusinessUnitRelationship BR2 on BR1.ParentID = BR2.ParentID
			JOIN	[IMT].dbo.BusinessUnit BU2 on BR2.BusinessUnitID = BU2.BusinessUnitID
			JOIN	dbo.Inventory_A5 BUI5 ON BUI5.BusinessUnitID = BU.BusinessUnitID
			JOIN	dbo.Inventory_A2 BU2I2 ON
				(
						BU2I2.BusinessUnitID = BU2.BusinessUnitID
					AND	BU2I2.GroupingDescriptionID = BUI5.GroupingDescriptionID
					AND	BU2I2.ModelYear = COALESCE(BUI5.ModelYear,BU2I2.ModelYear)
				)
			JOIN	dbo.ZipCode ZC ON BU.ZipCode = ZC.ZipCode
			JOIN	dbo.ZipCode ZC2 ON BU2.ZipCode = ZC2.ZipCode
			LEFT JOIN dbo.ZipCode_A1 ZCD1 ON (ZC.ZipCodeID = ZCD1.ZipCodeID1 AND ZC2.ZipCodeID = ZCD1.ZipCodeID2)
			LEFT JOIN dbo.ZipCode_A1 ZCD2 ON (ZC.ZipCodeID = ZCD2.ZipCodeID2 AND ZC2.ZipCodeID = ZCD2.ZipCodeID1)
			WHERE
				COALESCE(ZCD1.Distance,ZCD2.Distance,-1) BETWEEN 0 AND DP.PurchasingDistanceFromDealer
			GROUP BY
				BU.BusinessUnitID,
				BUI5.GroupingDescriptionID,
				BUI5.ModelYear,
				BU2I2.Units
		UNION ALL
			SELECT
				BU.BusinessUnitID,
				142 AccessGroupID,
				S.SaleID,
				YBP.GroupingDescriptionID,
				YBP.ModelYear,
				COUNT(*) Matches
			FROM
				[IMT].dbo.BusinessUnit BU
			JOIN	[IMT].dbo.DealerPreference DP ON BU.BusinessUnitID = DP.BusinessUnitID
			JOIN	dbo.Inventory_A5 YBP ON YBP.BusinessUnitID = BU.BusinessUnitID
			JOIN	[Auction].dbo.SaleVehicle SV ON YBP.GroupingDescriptionID = SV.GroupingDescriptionID AND COALESCE(YBP.ModelYear,SV.Year) = SV.Year
			JOIN	[Auction].dbo.Sale S ON SV.SaleID = S.SaleID
			JOIN	[Auction].dbo.Location L ON L.LocationID = S.LocationID
			JOIN	dbo.ZipCode ZCL ON ZCL.ZipCode = L.ZipCode
			JOIN	dbo.ZipCode ZCB ON ZCB.ZipCode = BU.ZipCode
			LEFT JOIN dbo.ZipCode_A1 ZCD1 ON (ZCL.ZipCodeID = ZCD1.ZipCodeID1 AND ZCB.ZipCodeID = ZCD1.ZipCodeID2)
			LEFT JOIN dbo.ZipCode_A1 ZCD2 ON (ZCL.ZipCodeID = ZCD2.ZipCodeID2 AND ZCB.ZipCodeID = ZCD2.ZipCodeID1)
			JOIN	dbo.UserContext UC1 ON UC1.UserContextID = 1
			JOIN	dbo.UserContext UC2 ON UC2.UserContextID = 2
			WHERE
				S.DateHeld BETWEEN UC1.RunDate AND UC2.RunDate
			AND	BU.Active = 1
			AND	COALESCE(ZCD1.Distance,ZCD2.Distance,-1) BETWEEN 0 AND DP.PurchasingDistanceFromDealer
			GROUP BY
				BU.BusinessUnitID,
				S.SaleID,
				YBP.GroupingDescriptionID,
				YBP.ModelYear
		) T0
	) T1
	GROUP BY
		T1.BusinessUnitID,
		T1.AccessGroupID,
		T1.SaleID
		WITH ROLLUP
) T2
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadAuction_A2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadAuction_A2]
GO

CREATE PROCEDURE dbo.LoadAuction_A2
	-- no params
AS
	EXEC LoadTable 'Auction_A2', '', 'SELECT * FROM dbo.GetAuction_V2'
GO

EXEC LoadAuction_A2 -- 45s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals_A3#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Appraisals_A3#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals_A3#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Appraisals_A3#1
GO

CREATE TABLE dbo.Appraisals_A3#0 (
	BusinessUnitID             INT      NOT NULL,
	AppraisalID                INT      NOT NULL,
	VehicleID                  INT      NOT NULL,
	DateCreated                DATETIME NOT NULL,
	WasInventoryAfterAppraisal BIT      NOT NULL,
	CONSTRAINT PK_Appraisal_A3#0 PRIMARY KEY NONCLUSTERED (
		BusinessUnitID,
		AppraisalID
	)
)
GO

CREATE TABLE dbo.Appraisals_A3#1 (
	BusinessUnitID             INT      NOT NULL,
	AppraisalID                INT      NOT NULL,
	VehicleID                  INT      NOT NULL,
	DateCreated                DATETIME NOT NULL,
	WasInventoryAfterAppraisal BIT      NOT NULL,
	CONSTRAINT PK_Appraisal_A3#1 PRIMARY KEY NONCLUSTERED (
		BusinessUnitID,
		AppraisalID
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisals_V3]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisals_V3]
GO

CREATE VIEW dbo.GetAppraisals_V3
(
	BusinessUnitID,
	AppraisalID,
	VehicleID,
	DateCreated,
	WasInventoryAfterAppraisal
)
AS
SELECT
	A.BusinessUnitID,
	A.AppraisalID,
	A.VehicleID,
	A.DateCreated,
	CASE WHEN EXISTS (
		SELECT
			1
		FROM
			[IMT].dbo.Inventory I
		WHERE
			A.VehicleID = I.VehicleID
		AND	I.BusinessUnitID = A.BusinessUnitID
		AND	DATEDIFF(DD, [IMT].dbo.ToDate(A.DateCreated), I.InventoryReceivedDate) >= 0
	) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END
FROM
	[IMT].dbo.Appraisals A CROSS JOIN dbo.UserContext UC
WHERE
	A.DateCreated BETWEEN DATEADD(WW, -5, UC.RunDate) AND UC.RunDate
AND	A.AppraisalID > 0
AND	UC.UserContextID = 1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals_A3]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Appraisals_A3]
GO

CREATE VIEW dbo.Appraisals_A3 AS SELECT * FROM Appraisals_A3#1
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Appraisals_A3', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	14,
	'Appraisals_A3',
	'Aggregated snapshot of last four weeks of appraisals',
	1,
	1,
	'GetAppraisals_V3',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadAppraisals_A3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadAppraisals_A3]
GO

CREATE PROCEDURE dbo.LoadAppraisals_A3
	-- no params
AS
	EXEC LoadTable 'Appraisals_A3', '', 'SELECT * FROM dbo.GetAppraisals_V3'
GO

EXEC LoadAppraisals_A3 -- 3s

--
-- Helper Function
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[initcap]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[initcap]
GO

CREATE FUNCTION dbo.initcap (@text varchar(4000))
RETURNS VARCHAR(4000)
AS
BEGIN
	DECLARE @counter int, 
		@length int,
		@numprefix int,
		@char char(1),
		@textnew varchar(4000)

	SET @text	= RTRIM(@text)
	SET @text	= LOWER(@text)
	SET @length 	= LEN(@text)
	SET @counter 	= 1

	SET @text = UPPER(LEFT(@text, 1) ) + RIGHT(@text, @length - 1)

	WHILE @counter <> @length
	BEGIN
		SELECT @char = SUBSTRING(@text, @counter, 1)

		IF @char = SPACE(1)
		or @char = '-'
		or @char = '_'
		or @char = '-'
		or @char = ','
		or @char = '.'
		or @char = '\'
		or @char = '/'
		or @char = '('
		or @char = ')'
		BEGIN
			SET @textnew = LEFT(@text, @counter) + UPPER(SUBSTRING(@text, @counter+1, 1)) + RIGHT(@text, (@length - @counter) - 1)
			SET @text    = @textnew
		END

		SET @counter = @counter + 1
	END

	RETURN @text
END
GO

--
-- Tables
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DaysBack]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.DaysBack
GO

CREATE TABLE dbo.DaysBack (
	Days INT NOT NULL,
	CONSTRAINT PK_DaysBack PRIMARY KEY (
		Days
	)
)
GO

INSERT INTO dbo.DaysBack (Days) VALUES (1)
INSERT INTO dbo.DaysBack (Days) VALUES (2)
INSERT INTO dbo.DaysBack (Days) VALUES (3)
INSERT INTO dbo.DaysBack (Days) VALUES (4)
INSERT INTO dbo.DaysBack (Days) VALUES (5)
INSERT INTO dbo.DaysBack (Days) VALUES (6)
INSERT INTO dbo.DaysBack (Days) VALUES (7)
INSERT INTO dbo.DaysBack (Days) VALUES (8)
INSERT INTO dbo.DaysBack (Days) VALUES (9)
INSERT INTO dbo.DaysBack (Days) VALUES (10)
INSERT INTO dbo.DaysBack (Days) VALUES (11)
INSERT INTO dbo.DaysBack (Days) VALUES (12)
INSERT INTO dbo.DaysBack (Days) VALUES (13)
INSERT INTO dbo.DaysBack (Days) VALUES (14)
INSERT INTO dbo.DaysBack (Days) VALUES (15)
INSERT INTO dbo.DaysBack (Days) VALUES (16)
INSERT INTO dbo.DaysBack (Days) VALUES (17)
INSERT INTO dbo.DaysBack (Days) VALUES (18)
INSERT INTO dbo.DaysBack (Days) VALUES (19)
INSERT INTO dbo.DaysBack (Days) VALUES (20)
INSERT INTO dbo.DaysBack (Days) VALUES (21)
INSERT INTO dbo.DaysBack (Days) VALUES (22)
INSERT INTO dbo.DaysBack (Days) VALUES (23)
INSERT INTO dbo.DaysBack (Days) VALUES (24)
INSERT INTO dbo.DaysBack (Days) VALUES (25)
INSERT INTO dbo.DaysBack (Days) VALUES (26)
INSERT INTO dbo.DaysBack (Days) VALUES (27)
INSERT INTO dbo.DaysBack (Days) VALUES (28)
INSERT INTO dbo.DaysBack (Days) VALUES (29)
INSERT INTO dbo.DaysBack (Days) VALUES (30)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleLine]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleLine
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleMake]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleMake
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleCategory
GO

CREATE TABLE dbo.VehicleCategory (
	VehicleCategoryID INT IDENTITY (1,1) NOT NULL,
	[Name] VARCHAR(20),
	CONSTRAINT PK_VehicleCategory  PRIMARY KEY NONCLUSTERED (
		VehicleCategoryID
	)
)
GO

INSERT INTO dbo.VehicleCategory ([Name]) VALUES ('Domestic')
INSERT INTO dbo.VehicleCategory ([Name]) VALUES ('Import')
INSERT INTO dbo.VehicleCategory ([Name]) VALUES ('Hi-Line')
GO

CREATE TABLE dbo.VehicleMake (
	VehicleMakeID     INT IDENTITY (1,1) NOT NULL,
	VehicleCategoryID INT NOT NULL,
	[Name] VARCHAR(20),
	CONSTRAINT PK_Make  PRIMARY KEY NONCLUSTERED (
		VehicleMakeID
	),
	CONSTRAINT FK_VehicleMake_VehicleCategory FOREIGN KEY (
		VehicleCategoryID
	)
	REFERENCES VehicleCategory (
		VehicleCategoryID
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicleMake]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetVehicleMake]
GO

CREATE VIEW dbo.GetVehicleMake(
	[Name],
	[VehicleCategoryID]
)
AS
SELECT	DISTINCT
	CASE WHEN Make IN ('BMW','GMC') THEN Make ELSE dbo.initcap(Make) END,
	1
FROM
	[IMT].dbo.tbl_MakeModelGrouping
WHERE
	LOWER(Make) <> 'unknown'
GO

INSERT INTO dbo.VehicleMake (
	[Name],
	[VehicleCategoryID]
)
SELECT
	[Name],
	[VehicleCategoryID]
FROM
	dbo.GetVehicleMake
GO

-- imports
UPDATE dbo.VehicleMake SET VehicleCategoryID = 2 WHERE [Name] IN (
'Alfa Romeo',
'Aston Martin',
'Audi',
'Daewoo',
'Honda',
'Hyundai',
'Isuzu',
'Jaguar',
'Kia',
'Mazda',
'Mini',
'Mitsubishi',
'Nissan',
'Saab',
'Scion',
'Subaru',
'Suzuki',
'Toyota',
'Volvo',
'Volkswagen'
)
GO

-- hi-line
UPDATE dbo.VehicleMake SET VehicleCategoryID = 3 WHERE [Name] IN (
'Acura',
'Aston Martin',
'Bentley',
'BMW',
'Bugatti',
'Cadillac',
'Ducati',
'Ferrari',
'Hummer',
'Infiniti',
'Jaguar',
'Lamborghini',
'Land Rover',
'Lexus',
'Lincoln',
'Lotus',
'Maserati',
'Maybach',
'Mercedes-Benz',
'Panoz',
'Porshe',
'Rolls-Royce',
'Sterling'
)
GO

CREATE TABLE dbo.VehicleLine (
	VehicleLineID INT IDENTITY (1,1) NOT NULL,
	VehicleMakeID  INT NOT NULL,
	MakeModelGroupingID INT NOT NULL,
	[Name]  VARCHAR(50),
	QualifiedName VARCHAR(50),
	CONSTRAINT PK_VehicleLine PRIMARY KEY NONCLUSTERED (
		VehicleLineID
	),
	CONSTRAINT FK_VehicleLine_VehicleMake FOREIGN KEY (
		VehicleMakeID
	)
	REFERENCES VehicleMake (
		VehicleMakeID
	)
)
GO

CREATE INDEX IDX_VehicleLine_MakeModelGrouping ON dbo.VehicleLine (
	MakeModelGroupingID
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicleLine]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetVehicleLine]
GO

CREATE VIEW dbo.GetVehicleLine(
	VehicleMakeID,
	MakeModelGroupingID,
	[Name],
	QualifiedName
)
AS
SELECT
	M2.VehicleMakeID,
	M0.MakeModelGroupingID,
	M0.Model,
	M1.Model
FROM
	[IMT].dbo.tbl_MakeModelGrouping M0
JOIN	[IMT].dbo.MakeModelGrouping M1 ON M0.MakeModelGroupingID = M1.MakeModelGroupingID
JOIN	dbo.VehicleMake M2 ON LOWER(M2.[Name]) = LOWER(M0.Make)
GO

INSERT INTO dbo.VehicleLine (VehicleMakeID,MakeModelGroupingID,[Name],QualifiedName)
SELECT
	VehicleMakeID,
	MakeModelGroupingID,
	[Name],
	QualifiedName
FROM
	dbo.GetVehicleLine
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadHAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadHAL]
GO

CREATE PROCEDURE dbo.LoadHAL
	-- no params
AS
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	-- make sure we are working with the correct dates
	UPDATE dbo.UserContext SET RunDate = [IMT].dbo.ToDate(GETDATE()) WHERE UserContextID = 1
	UPDATE dbo.UserContext SET RunDate = DATEADD(WW,1,[IMT].dbo.ToDate(GETDATE())) WHERE UserContextID = 2
	-- this is for make a deal
	EXEC dbo.LoadAppraisals_A1
	EXEC dbo.LoadInventory_A2
	EXEC dbo.LoadInventory_A3
	EXEC dbo.LoadInventory_A4
	EXEC dbo.LoadInventory_A5
	EXEC dbo.LoadAuction_A1
--	EXEC dbo.LoadAuction_A2
	EXEC dbo.LoadAppraisals_A3
	-- this is for planning
	EXEC dbo.LoadInventory_A6
	EXEC dbo.LoadInventory_A7
	EXEC dbo.CreateView_GetInventory
--	EXEC dbo.LoadInventory_A8
--	EXEC dbo.LoadAppraisalReviews
GO

-- EXEC LoadHAL -- 2m 45s

--
-- VIEWS for Ruby and ActiveRecord
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_units]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_units]
GO

CREATE VIEW dbo.business_units (
	[id],
	[business_unit_type_id],
	[parent_id],
	[name],
	[short_name],
	[telephone_number]
)
AS
SELECT
	BU.BusinessUnitID,
	BU.BusinessUnitTypeID,
	BUR.ParentID,
	BU.BusinessUnit,
	BU.BusinessUnitShortName,
	BU.OfficePhone
FROM
	[IMT].dbo.BusinessUnit BU
JOIN	[IMT].dbo.BusinessUnitRelationship BUR ON BU.BusinessUnitID = BUR.BusinessUnitID
WHERE
	Active = 1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_unit_preferences]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_unit_preferences]
GO

CREATE VIEW dbo.business_unit_preferences (
	[id],
	[business_unit_id],
	[high_mileage_overall],
	[primary_book_id],
	[primary_book_category_id],
	[secondary_book_id],
	[secondary_book_category_id],
	[ad_price_enabled],
	[naaa_area_id]
)
AS
SELECT
	P.DealerPreferenceID,
	P.BusinessUnitID,
	R.HighMileageOverall,
	P.GuideBookID,
	P.BookOutPreferenceId,
	P.GuideBook2Id,
	P.GuideBook2BookOutPreferenceId,
	P.InternetAdvertiser_AdPriceEnabled,
	P.AuctionAreaID
FROM
	[IMT].dbo.DealerPreference P
JOIN	[IMT].dbo.DealerRisk R ON P.BusinessUnitID = R.BusinessUnitID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[upgrades]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[upgrades]
GO

CREATE VIEW dbo.upgrades (
	[id],
	[name]
)
AS
SELECT
	DealerUpgradeCD,
	DealerUpgradeDESC
FROM
	[IMT].dbo.lu_DealerUpgrade
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_units_upgrades]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_units_upgrades]
GO

CREATE VIEW dbo.business_units_upgrades (
	[business_unit_id],
	[upgrade_id]
)
AS
SELECT
	BusinessUnitID,
	DealerUpgradeCD
FROM
	[IMT].dbo.DealerUpgrade
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_units_books]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_units_books]
GO

CREATE VIEW dbo.business_units_books (
	business_unit_id,
	book_id
)
AS
SELECT	DISTINCT
	BusinessUnitID,
	BookID
FROM
(
	SELECT BusinessUnitID, GuideBookID AS BookID FROM [IMT].dbo.DealerPreference WHERE GuideBookID IS NOT NULL
	UNION ALL
	SELECT BusinessUnitID, GuideBook2ID AS BookID FROM [IMT].dbo.DealerPreference WHERE GuideBook2ID IS NOT NULL
) T
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[members]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[members]
GO

CREATE VIEW dbo.members (
	[id],
	[login],
	[first_name],
	[middle_initial],
	[last_name],
	[member_type_id]
)
AS
SELECT
	MemberID,
	Login,
	FirstName,
	MiddleInitial,
	LastName,
	MemberType
FROM
	[IMT].dbo.Member
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[members_business_units]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[members_business_units]
GO

CREATE VIEW dbo.members_business_units (
	[member_id],
	[business_unit_id]
)
AS
SELECT
	MemberID,
	BusinessUnitID
FROM
	[IMT].dbo.MemberAccess
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[books]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[books]
GO

CREATE VIEW dbo.books (
	[id],
	[name],
	[short_name],
	[footer_text]
)
AS
SELECT
	[ThirdPartyID],
	[Description],
	[Name],
	[DefaultFooter]
FROM
	[IMT].dbo.ThirdParties
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_categories]
GO

CREATE VIEW dbo.book_categories (
	[id],
	[book_id],
	[name]
)
AS
SELECT
	[ThirdPartyCategoryID],
	[ThirdPartyID],
	[Category]
FROM
	[IMT].dbo.ThirdPartyCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_valuations]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_valuations]
GO

CREATE VIEW dbo.book_valuations (
	[id],
	[book_id],
	[date_published],
	[is_accurate]
)
AS
SELECT
	BookoutID,
	ThirdPartyID,
	DatePublished,
	IsAccurate
FROM
	[IMT].dbo.Bookouts
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_category_valuations]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_category_valuations]
GO

CREATE VIEW dbo.book_category_valuations (
	[id],
	[book_valuation_id],
	[book_category_id]
)
AS
SELECT
	BookoutThirdPartyCategoryID,
	BookoutID,
	ThirdPartyCategoryID
FROM
	[IMT].dbo.BookoutThirdPartyCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_value_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_value_types]
GO

CREATE VIEW dbo.book_value_types (
	[id],
	[name]
)
AS
SELECT
	[BookoutValueTypeID],
	[Description]
FROM
	[IMT].dbo.BookoutValueTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_values]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_values]
GO

CREATE VIEW dbo.book_values (
	[id],
	[book_category_valuation_id],
	[book_value_type_id],
	[value]
)
AS
SELECT
	BookoutValueID,
	BookoutThirdPartyCategoryID,
	BookoutValueTypeID,
	Value
FROM
	[IMT].dbo.BookoutValues
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_categories]
GO

CREATE VIEW dbo.vehicle_categories (
	[id],
	[name]
)
AS
SELECT
	VehicleCategoryID,
	[Name]
FROM
	dbo.VehicleCategory
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_makes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_makes]
GO

CREATE VIEW dbo.vehicle_makes (
	[id],
	[name],
	[vehicle_category_id]
)
AS
SELECT
	VehicleMakeID,
	[Name],
	VehicleCategoryID
FROM
	dbo.VehicleMake
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_lines]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_lines]
GO

CREATE VIEW dbo.vehicle_lines (
	[id],
	[legacy_id],
	[vehicle_make_id],
	[name],
	[qualified_name]
)
AS
SELECT
	VehicleLineID,
	MakeModelGroupingID,
	VehicleMakeID,
	[Name],
	QualifiedName
FROM
	dbo.VehicleLine
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_segments]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_segments]
GO

CREATE VIEW dbo.vehicle_segments (
	[id],
	[name]
)
AS
SELECT
	GroupingDescriptionID,
	GroupingDescription
FROM
	[IMT].dbo.tbl_GroupingDescription
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicles]
GO

CREATE VIEW dbo.vehicles (
	[id],
	[vehicle_make_id],
	[vehicle_line_id],
	[vehicle_segment_id],
	[series],
	[vin],
	[vic],
	[model_year],
	[color],
	[segment],
	[description]
)
AS
SELECT
	VehicleID,
	VM.VehicleMakeID,
	VL.VehicleLineID,
	G.GroupingDescriptionID,
	V.VehicleTrim,
	V.VIN,
	V.VIC,
	V.VehicleYear,
	V.BaseColor,
	M.DisplayBodyTypeID,
	CAST(V.VehicleYear AS VARCHAR) + ' ' + vm.[name] +  ' ' + vl.[name] + CASE WHEN COALESCE(V.VehicleTrim,'N/A') = 'N/A' THEN '' ELSE ' ' + V.VehicleTrim END
FROM
	[IMT].dbo.tbl_Vehicle V WITH (NOLOCK)
JOIN	[IMT].dbo.tbl_MakeModelGrouping M ON V.MakeModelGroupingID = M.MakeModelGroupingID
JOIN	[IMT].dbo.tbl_GroupingDescription G ON G.GroupingDescriptionID = M.GroupingDescriptionID
JOIN	dbo.VehicleLine VL ON VL.MakeModelGroupingID = M.MakeModelGroupingID
JOIN	dbo.VehicleMake VM ON VM.VehicleMakeID = VL.VehicleMakeID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_inventory_lights]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_inventory_lights]
GO

CREATE VIEW dbo.vehicle_inventory_lights (
	[id],
	[name]
)
AS
SELECT
	InventoryVehicleLightCD,
	InventoryVehicleLightDesc
FROM
	[IMT].dbo.lu_InventoryVehicleLight
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisals]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisals]
GO

CREATE VIEW dbo.appraisals (
	[id],
	[business_unit_id],
	[vehicle_id],
	[date_created],
	[deal_track_stock_number],
	[deal_track_sales_person_id],
	[deal_track_new_or_used],
	[appraisal_source_id]
)
AS
SELECT
	A.AppraisalID,
	A.BusinessUnitID,
	A.VehicleID,
	A.DateCreated,
	DealTrackStockNumber,
	DealTrackSalespersonID,
	DealTrackNewOrUsed,
	AppraisalSourceID
FROM
	[IMT].dbo.Appraisals A
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_offers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_offers]
GO

CREATE VIEW dbo.appraisal_offers (
	[id],
	[appraisal_id],
	[customer_offer]
)
AS
SELECT
	AppraisalFormID,
	AppraisalID,
	AppraisalFormOffer
FROM
	[IMT].dbo.AppraisalFormOptions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_valuations]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_valuations]
GO

CREATE VIEW dbo.appraisal_valuations(
	[id],
	[appraisal_id],
	[sequence_number],
	[date_created],
	[value],
	[appraiser_name]
)
AS
SELECT
	AppraisalValueID,
	AppraisalID,
	SequenceNumber,
	DateCreated,
	Value,
	AppraiserName
FROM
	[IMT].dbo.AppraisalValues
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_actions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_actions]
GO

CREATE VIEW dbo.appraisal_actions(
	[id],
	[appraisal_id],
	[appraisal_action_type_id],
	[date_created],
	[wholesale_price],
	[estimated_reconditioning_cost],
	[conditioning_description]
)
AS
SELECT
	AppraisalActionID,
	AppraisalID,
	AppraisalActionTypeID,
	DateCreated,
	WholesalePrice,
	EstimatedReconditioningCost,
	ConditionDescription
FROM
	[IMT].dbo.AppraisalActions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[customers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[customers]
GO

CREATE VIEW dbo.customers (
	[id],
	[appraisal_id],
	[email],
	[first_name],
	[last_name],
	[gender],
	[telephone_number]
)
AS
SELECT
	AppraisalID,
	AppraisalID,
	Email,
	FirstName,
	LastName,
	Gender,
	PhoneNumber
FROM
	[IMT].dbo.AppraisalCustomer
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[measures]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[measures]
GO

CREATE VIEW dbo.measures (
	[id],
	[business_unit_id],
	[period_id],
	[set_id],
	[metric_id],
	[measure],
	[comments],
	[date_created]
)
AS
SELECT
	CAST(BusinessUnitID AS VARCHAR) + '_' + CAST(PeriodID AS VARCHAR) + '_' + CAST(SetID AS VARCHAR) + '_' + CAST(MetricID AS VARCHAR),
	BusinessUnitID,
	PeriodID,
	SetID,
	MetricID,
	Measure,
	Comments,
	DateCreated
FROM
	[EdgeScorecard].dbo.Measures
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_summary]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_summary]
GO

CREATE VIEW dbo.search_summary(
	[id],
	[business_unit_id],
	[access_group_id],
	[sale_id],
	[precision_matches],
	[total_matches]
)
AS
SELECT
	CAST(BusinessUnitID AS VARCHAR) + '_' + CAST(AccessGroupID AS VARCHAR) + '_' + CAST(SaleID AS VARCHAR),
	BusinessUnitID,
	AccessGroupID,
	SaleID,
	PrecisionMatches,
	TotalMatches
FROM
	dbo.Auction_A2
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisals_book_vehicles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisals_book_vehicles]
GO

CREATE VIEW dbo.appraisals_book_vehicles (
	[appraisal_id],
	[book_vehicle_id]
)
AS
SELECT
	AppraisalID,
	ThirdPartyVehicleID
FROM
(
	SELECT
		A.AppraisalID,
		V.ThirdPartyID,
		MAX(V.ThirdPartyVehicleID) ThirdPartyVehicleID
	FROM
		[IMT].dbo.AppraisalThirdPartyVehicles A
	JOIN	[IMT].dbo.ThirdPartyVehicles V ON A.ThirdPartyVehicleID = V.ThirdPartyVehicleID
	GROUP BY
		A.AppraisalID,
		V.ThirdPartyID
) T
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_vehicles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_vehicles]
GO

CREATE VIEW dbo.book_vehicles (
	[id],
	[book_id],
	[description],
	[make],
	[model],
	[series],
	[is_selected],
	[created]
)
AS
SELECT
	[ThirdPartyVehicleID],
	[ThirdPartyID],
	[Description],
	[Make],
	[Model],
	[Body],
	[Status],
	[DateCreated]
FROM
	[IMT].dbo.ThirdPartyVehicles
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_option_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_option_types]
GO

CREATE VIEW dbo.book_option_types (
	[id],
	[name]
)
AS
SELECT
	ThirdPartyOptionTypeID,
	OptionTypeName
FROM
	[IMT].dbo.ThirdPartyOptionTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_options]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_options]
GO

CREATE VIEW dbo.book_options (
	[id],
	[book_option_type_id],
	[name]
)
AS
SELECT
	ThirdPartyOptionID,
	ThirdPartyOptionTypeID,
	OptionName

FROM
	[IMT].dbo.ThirdPartyOptions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_vehicle_options]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_vehicle_options]
GO

CREATE VIEW dbo.book_vehicle_options (
	[id],
	[book_vehicle_id],
	[book_option_id],
	[is_standard_option],
	[is_selected],
	[sort_order]
)
AS
SELECT
	ThirdPartyVehicleOptionID,
	ThirdPartyVehicleID,
	ThirdPartyOptionID,
	IsStandardOption,
	Status,
	SortOrder
FROM
	[IMT].dbo.ThirdPartyVehicleOptions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_vehicle_option_value_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_vehicle_option_value_types]
GO

CREATE VIEW dbo.book_vehicle_option_value_types (
	[id],
	[name]
)
AS
SELECT
	ThirdPartyOptionValueTypeID,
	OptionValueTypeName
FROM
	[IMT].dbo.ThirdPartyVehicleOptionValueTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[book_vehicle_option_values]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[book_vehicle_option_values]
GO

CREATE VIEW dbo.book_vehicle_option_values (
	[book_vehicle_option_id],
	[book_vehicle_option_value_type_id],
	[value]
)
AS
SELECT
	ThirdPartyVehicleOptionID,
	ThirdPartyOptionValueTypeID,
	Value
FROM
	[IMT].dbo.ThirdPartyVehicleOptionValues
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_areas]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_areas]
GO

CREATE VIEW dbo.naaa_areas (
	[id],
	[name]
)
AS
SELECT
	AreaID,
	AreaName
FROM
	[AuctionNet].dbo.Area_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_periods]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_periods]
GO

CREATE VIEW dbo.naaa_periods (
	[id],
	[name],
	[rank]
)
AS
SELECT
	PeriodID,
	[Description],
	Rank
FROM
	[AuctionNet].dbo.Period_D
WHERE
	TimeTypeCD = 6
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_series]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_series]
GO

CREATE VIEW dbo.naaa_series (
	[id],
	[name]
)
AS
SELECT
	SeriesID,
	Series
FROM
	[AuctionNet].dbo.Series_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_body_styles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_body_styles]
GO

CREATE VIEW dbo.naaa_body_styles (
	[id],
	[name]
)
AS
SELECT
	BodyStyleID,
	BodyStyle
FROM
	[AuctionNet].dbo.BodyStyle_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_series_body_styles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_series_body_styles]
GO

CREATE VIEW dbo.naaa_series_body_styles (
	[id],
	[naaa_series_id],
	[naaa_body_style_id]
)
AS
SELECT
	SeriesBodyStyleID,
	SeriesID,
	BodyStyleID
FROM
	[AuctionNet].dbo.SeriesBodyStyle_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_lines_naaa_series_body_styles]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_lines_naaa_series_body_styles]
GO

CREATE VIEW dbo.vehicle_lines_naaa_series_body_styles (
	[vehicle_line_id],
	[naaa_series_body_style_id]
)
AS
SELECT DISTINCT
	L.VehicleLineID,
	S.SeriesBodyStyleID
FROM
	[AuctionNet].dbo.MakeModelGroupingSeriesBodyStyle S
JOIN	dbo.VehicleLine L ON L.MakeModelGroupingID = S.MakeModelGroupingID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_statistics]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_statistics]
GO

CREATE VIEW dbo.naaa_statistics (
	[id],
	[naaa_area_id],
	[naaa_period_id],
	[vic],
	[model_year],
	[high_sales_price],
	[average_sales_price],
	[low_sales_price],
	[high_mileage],
	[average_mileage],
	[low_mileage],
	[vehicle_count]
)
AS
SELECT
	CAST(A.AreaID AS VARCHAR) + '_' + CAST(A.PeriodID AS VARCHAR) + '_' + CAST(A.VIC AS VARCHAR),
	A.AreaID,
	A.PeriodID,
	A.VIC,
	V.ModelYear,
	A.HighSalePrice,
	A.AveSalePrice,
	A.LowSalePrice,
	A.HighMileage,
	A.AveMileage,
	A.LowMileage,
	A.TransactionCount
FROM
	[AuctionNet].dbo.AuctionTransaction_A1 A
JOIN	[VehicleUC].dbo.VIC_Catalog V ON V.VIC = A.VIC
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_fallback_statistics]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_fallback_statistics]
GO

CREATE VIEW dbo.naaa_fallback_statistics (
	[id],
	[naaa_area_id],
	[naaa_period_id],
	[mmg],
	[model_year],
	[high_sales_price],
	[average_sales_price],
	[low_sales_price],
	[high_mileage],
	[average_mileage],
	[low_mileage],
	[vehicle_count]
)
AS
SELECT
	CAST(A.AreaID AS VARCHAR) + '_' + CAST(A.PeriodID AS VARCHAR) + '_' + CAST(A.MakeModelGroupingID AS VARCHAR) + '_' + CAST(A.ModelYear AS VARCHAR),
	A.AreaID,
	A.PeriodID,
	A.MakeModelGroupingID,
	A.ModelYear,
	A.HighSalePrice,
	A.AveSalePrice,
	A.LowSalePrice,
	A.HighMileage,
	A.AveMileage,
	A.LowMileage,
	A.TransactionCount
FROM
	[AuctionNet].dbo.AuctionTransaction_A3 A
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_samples]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_samples]
GO

CREATE VIEW dbo.naaa_samples (
	[id],
	[naaa_area_id],
	[naaa_period_id],
	[vic],
	[model_year],
	[low_mileage_range],
	[high_mileage_range],
	[average_sales_price]
)
AS
SELECT
	CAST(A.AreaID AS VARCHAR) + '_' + CAST(A.PeriodID AS VARCHAR) + '_' + CAST(A.VIC AS VARCHAR),
	A.AreaID,
	A.PeriodID,
	A.VIC,
	V.ModelYear,
	A.LowMileageRange,
	A.HighMileageRange,
	A.AveSalePrice
FROM
	[AuctionNet].dbo.AuctionTransaction_A2 A
JOIN	[VehicleUC].dbo.VIC_Catalog V ON V.VIC = A.VIC
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[naaa_fallback_samples]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[naaa_fallback_samples]
GO

CREATE VIEW dbo.naaa_fallback_samples (
	[id],
	[naaa_area_id],
	[naaa_period_id],
	[mmg],
	[model_year],
	[low_mileage_range],
	[high_mileage_range],
	[average_sales_price]
)
AS
SELECT
	CAST(A.AreaID AS VARCHAR) + '_' + CAST(A.PeriodID AS VARCHAR) + '_' + CAST(A.MakeModelGroupingID AS VARCHAR) + '_' + CAST(A.ModelYear AS VARCHAR),
	A.AreaID,
	A.PeriodID,
	A.MakeModelGroupingID,
	A.ModelYear,
	A.LowMileageRange,
	A.HighMileageRange,
	A.AveSalePrice
FROM
	[AuctionNet].dbo.AuctionTransaction_A4 A
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_identification_codes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_identification_codes]
GO

CREATE VIEW dbo.vehicle_identification_codes (
	[id],
	[model_year],
	[make],
	[series],
	[body_style]
)
AS
SELECT
	VIC,
	ModelYear,
	Make,
	Series,
	Body
FROM
	[VehicleUC].dbo.VIC_Catalog
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_identification_number_prefixes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_identification_number_prefixes]
GO

CREATE VIEW dbo.vehicle_identification_number_prefixes (
	[id],
	[vehicle_identification_numer_prefix],
	[vehicle_identification_code_id]
)
AS
SELECT
	VINPrefix + '_' + VIC,
	VINPrefix,
	VIC
FROM
	[VehicleUC].dbo.VINPrefix_VIC
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory]
GO

CREATE VIEW dbo.inventory (
	[id],
	[business_unit_id],
	[vehicle_id],
	[stock_number],
	[inventory_received_date],
	[unit_cost],
	[aquisition_price],
	[list_price],
	[trade_or_purchase],
	[mileage_received],
	[plan_reminder_date],
	[current_vehicle_light_id],
	[certified],
	[special_finance],
	[risk_analysis],
	[active],
	[inventory_type_id]
)
AS
SELECT
	InventoryID,
	BusinessUnitID,
	VehicleID,
	StockNumber,
	InventoryReceivedDate,
	UnitCost,
	AcquisitionPrice,
	ListPrice,
	TradeOrPurchase,
	MileageReceived,
	PlanReminderDate,
	CurrentVehicleLight,
	Certified,
	SpecialFinance,
	Level4Analysis,
	InventoryActive,
	InventoryType
FROM
	[IMT].dbo.Inventory
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_book_valuation_summaries]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_book_valuation_summaries]
GO

CREATE VIEW dbo.inventory_book_valuation_summaries (
	[id],
	[business_unit_id],
	[inventory_id],
	[inventory_type_id],
	[book_valuation_id],
	[book_category_id],
	[book_value_type_id],
	[book_value],
	[is_valid],
	[is_accurate],
	[is_primary_book_value],
	[is_secondary_book_value]
)
AS
SELECT
	CAST(F.BookoutID AS VARCHAR) + '_' + CAST(F.ThirdPartyCategoryID AS VARCHAR) + '_' + CAST(F.BookoutValueTypeID AS VARCHAR),
	F.BusinessUnitID,
	F.InventoryID,
	F.InventoryTypeID,
	F.BookoutID,
	F.ThirdPartyCategoryID,
	F.BookoutValueTypeID,
	F.BookValue,
	F.IsValid,
	F.IsAccurate,
	CAST(CASE WHEN F.ThirdPartyCategoryID = P.primary_book_category_id THEN 1 ELSE 0 END AS BIT) IsPrimaryBookValue,
	CAST(CASE WHEN F.ThirdPartyCategoryID = P.secondary_book_category_id THEN 1 ELSE 0 END AS BIT) IsSecondaryBookValue
FROM
	[FLDW].dbo.InventoryBookout_F F
JOIN	dbo.business_unit_preferences P ON P.business_unit_id = F.BusinessUnitID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_book_valuation_summaries]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_book_valuation_summaries]
GO

CREATE VIEW dbo.appraisal_book_valuation_summaries (
	[id],
	[business_unit_id],
	[appraisal_id],
	[book_valuation_id],
	[book_category_id],
	[book_value_type_id],
	[book_value],
	[is_valid],
	[is_accurate]
)
AS
SELECT
	CAST(F.BookoutID AS VARCHAR) + '_' + CAST(F.ThirdPartyCategoryID AS VARCHAR) + '_' + CAST(F.BookoutValueTypeID AS VARCHAR),
	F.BusinessUnitID,
	F.AppraisalID,
	F.BookoutID,
	F.ThirdPartyCategoryID,
	F.BookoutValueTypeID,
	F.Value,
	F.IsValid,
	F.IsAccurate
FROM
	[FLDW].dbo.AppraisalBookout_F F
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_advertising_attributes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_advertising_attributes]
GO

CREATE VIEW dbo.inventory_advertising_attributes (
	[id],
	[inventory_id],
	[description],
	[price]
)
AS
SELECT
	InventoryID,
	InventoryID,
	[Description],
	AdPrice
FROM
	[IMT].dbo.Inventory_Advertising
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[credential_authorities]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[credential_authorities]
GO

CREATE VIEW dbo.credential_authorities (
	[id],
	[name]
)
AS
SELECT
	CredentialTypeID,
	Name
FROM
	[IMT].dbo.CredentialType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[credentials]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[credentials]
GO

CREATE VIEW dbo.credentials (
	[id],
	[member_id],
	[credential_authority_id],
	[username],
	[password]
)
AS
SELECT
	[MemberCredentialID],
	[MemberID],
	[CredentialTypeID],
	[Username],
	[Password]
FROM
	[IMT].dbo.MemberCredential
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[marketplace_submissions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[marketplace_submissions]
GO

CREATE VIEW dbo.marketplace_submissions (
	[id],
	[third_party_id],
	[inventory_id],
	[member_id],
	[marketplace_submission_status_id],
	[url],
	[begin_effective_date],
	[end_effective_date],
	[date_created]
)
AS
SELECT
	MarketplaceSubmissionID,
	MarketplaceID,
	InventoryID,
	MemberID,
	MarketplaceSubmissionStatusCD,
	URL,
	BeginEffectiveDate,
	EndEffectiveDate,
	DateCreated
FROM
	[IMT].dbo.MarketplaceSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_items]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_items]
GO

CREATE VIEW dbo.inventory_items (
	[id],
	[business_unit_id],
	[inventory_type_id],
	[inventory_received_date],
	[active],
	[vehicle_id],
	[vehicle_age],
	[vehicle_description],
	[vehicle_color],
	[vehicle_mileage],
	[vehicle_stock_number],
	[vehicle_list_price],
	[vehicle_ad_price],
	[vehicle_unit_cost],
	[vehicle_certified],
	[vehicle_special_finance],
	[vehicle_risk],
	[vehicle_trade_or_purchase],
	[vehicle_light_id],
	[primary_book_name],
	[primary_book_category_name],
	[primary_book_value],
	[primary_book_vs_cost]
)
AS
SELECT
	i.[id],
	i.business_unit_id,
	i.inventory_type_id,
	i.inventory_received_date,
	i.active,
	v.id,
	DATEDIFF(dd,[IMT].dbo.ToDate(i.inventory_received_date),[IMT].dbo.ToDate(GETDATE())),
	CAST(v.model_year AS VARCHAR) + ' ' + vm.[name] +  ' ' + vl.[name] + CASE WHEN v.series = 'N/A' THEN '' ELSE ' ' + v.series END,
	v.color,
	i.mileage_received,
	i.stock_number,
	i.list_price,
	coalesce(a.price,i.list_price),
	i.unit_cost,
	i.certified,
	i.special_finance,
	i.risk_analysis,
	i.trade_or_purchase,
	i.current_vehicle_light_id,
	b.name,
	bc.name,
	ib.bookvalue,
	i.unit_cost-ib.bookvalue
FROM
	inventory i
JOIN	vehicles v on v.id = i.vehicle_id
JOIN	vehicle_makes vm on vm.id = v.vehicle_make_id
JOIN	vehicle_lines vl on vl.id = v.vehicle_line_id
JOIN	business_unit_preferences bup on bup.business_unit_id = i.business_unit_id
LEFT JOIN	inventory_advertising_attributes a on i.id = a.id
LEFT JOIN	[FLDW].dbo.InventoryBookout_F ib on (
			ib.inventoryid          = i.id
		and	ib.thirdpartycategoryid = bup.primary_book_category_id
		)
LEFT JOIN	book_valuations bv on bv.id = ib.bookoutid and bv.book_id = bup.primary_book_id
LEFT JOIN	books b on b.id = bv.book_id
LEFT JOIN	book_categories bc on bc.id = ib.thirdpartycategoryid
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[people]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[people]
GO

CREATE VIEW dbo.people (
	[id],
	[business_unit_id],
	[last_name],
	[middle_initial],
	[first_name]
)
AS
SELECT
	PersonID,
	BusinessUnitID,
	LastName,
	MiddleInitial,
	FirstName
FROM
	[IMT].dbo.Person
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[positions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[positions]
GO

CREATE VIEW dbo.positions (
	[id],
	[name]
)
AS
SELECT
	[PositionID],
	[Description]
FROM
	[IMT].dbo.Position
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[people_positions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[people_positions]
GO

CREATE VIEW dbo.people_positions (
	[person_id],
	[position_id]
)
AS
SELECT
	PersonID,
	PositionID
FROM
	[IMT].dbo.PersonPosition
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerGridPreferenceTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.DealerGridPreferenceTypes
GO

CREATE TABLE dbo.DealerGridPreferenceTypes (
	[DealerGridPreferenceTypeID] INT NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_DealerGridPreferenceType PRIMARY KEY (
		DealerGridPreferenceTypeID
	)
)
GO

INSERT INTO dbo.DealerGridPreferenceTypes ([DealerGridPreferenceTypeID],[Name]) VALUES (1, 'Powerzone')
INSERT INTO dbo.DealerGridPreferenceTypes ([DealerGridPreferenceTypeID],[Name]) VALUES (2, 'Winners')
INSERT INTO dbo.DealerGridPreferenceTypes ([DealerGridPreferenceTypeID],[Name]) VALUES (3, 'Good Bets')
INSERT INTO dbo.DealerGridPreferenceTypes ([DealerGridPreferenceTypeID],[Name]) VALUES (4, 'Market Performer or Other Green')
INSERT INTO dbo.DealerGridPreferenceTypes ([DealerGridPreferenceTypeID],[Name]) VALUES (5, 'Market Performer or Managers Choice')
INSERT INTO dbo.DealerGridPreferenceTypes ([DealerGridPreferenceTypeID],[Name]) VALUES (6, 'Managers Choice')
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InGroupRedistribution]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[InGroupRedistribution]
GO

CREATE VIEW dbo.InGroupRedistribution (
	BusinessUnitID,
	TargetBusinessUnitID,
	GroupingDescriptionID,
	ModelYear,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	StockedUnitsModelYear,
	StockedUnitsModel,
	DistanceInMiles
)
AS
SELECT
	BU1.BusinessUnitID,
	BU2.BusinessUnitID,
	I.GroupingDescriptionID,
	I.ModelYear,
	I.OptimalUnitsModelYear,
	I.OptimalUnitsModel,
	I.StockedUnitsModelYear,
	I.StockedUnitsModel,
	COALESCE(ZCD1.Distance,ZCD2.Distance,0) DistanceInMiles
FROM
	[IMT].dbo.BusinessUnit BU1
JOIN	[IMT].dbo.BusinessUnitRelationship BR1 on BU1.BusinessUnitID = BR1.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnitRelationship BR2 on BR1.ParentID = BR2.ParentID
JOIN	[IMT].dbo.BusinessUnit BU2 on BR2.BusinessUnitID = BU2.BusinessUnitID
JOIN	dbo.Inventory_A4 I ON BU2.BusinessUnitID = I.BusinessUnitID
JOIN	dbo.ZipCode ZC1 ON BU1.ZipCode = ZC1.ZipCode
JOIN	dbo.ZipCode ZC2 ON BU2.ZipCode = ZC2.ZipCode
LEFT JOIN dbo.ZipCode_A1 ZCD1 ON (ZC1.ZipCodeID = ZCD1.ZipCodeID1 AND ZC2.ZipCodeID = ZCD1.ZipCodeID2)
LEFT JOIN dbo.ZipCode_A1 ZCD2 ON (ZC1.ZipCodeID = ZCD2.ZipCodeID2 AND ZC2.ZipCodeID = ZCD2.ZipCodeID1)
WHERE
	BU1.BusinessUnitID <> BU2.BusinessUnitID
GROUP BY
	BU1.BusinessUnitID,
	BU2.BusinessUnitID,
	I.GroupingDescriptionID,
	I.ModelYear,
	I.OptimalUnitsModelYear,
	I.OptimalUnitsModel,
	I.StockedUnitsModelYear,
	I.StockedUnitsModel,
	ZCD1.Distance,
	ZCD2.Distance
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OptimalInventoryReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[OptimalInventoryReport]
GO

CREATE VIEW dbo.OptimalInventoryReport (
	BusinessUnitID,
	TotalStockedUnitsModel,
	TotalOptimalUnitsModel,
	Percentage
)
AS
SELECT
	B.BusinessUnitID,
	COALESCE(sum(case when StockedUnits > OptimalUnits then OptimalUnits else StockedUnits end),0) as 'TotalStockedUnitsModel',
	COALESCE(sum(OptimalUnits),0) as 'TotalOptimalUnitsModel',
	COALESCE(cast(sum(case when StockedUnits > OptimalUnits then OptimalUnits else StockedUnits end) as real),0)
	/
	COALESCE(NULLIF(sum(OptimalUnits),0),1) as 'Percentage'
FROM
	BusinessUnit B LEFT JOIN
(
	SELECT * FROM
	(
		SELECT 
		X.BusinessUnitID,
		X.GroupingDescriptionID,
		X.ModelYear,
		X.OptimalUnits,
		Y.StockedUnits
		FROM
		(
			SELECT
				BusinessUnitID,
				GroupingDescriptionID,
				ModelYear,
				OptimalUnitsModelYear AS 'OptimalUnits'
			FROM
				dbo.Inventory_A4
			WHERE
				OptimalUnitsModel IS NOT NULL
			AND	CIACategoryID < 4
			AND	OptimalUnitsModelYear IS NOT NULL
			AND	OptimalUnitsModelYear > 0
			GROUP BY
				BusinessUnitID,
				GroupingDescriptionID,
				ModelYear,
				OptimalUnitsModelYear
		) X
		LEFT JOIN
		(
			SELECT
				BusinessUnitID,
				GroupingDescriptionID,
				ModelYear,
				StockedUnitsModelYearOverUnitCostThreshold StockedUnits
			FROM
				dbo.Inventory_A4
			WHERE
				OptimalUnitsModel IS NOT NULL
			AND	CIACategoryID < 4
			GROUP BY
				BusinessUnitID,
				GroupingDescriptionID,
				ModelYear,
				StockedUnitsModelYearOverUnitCostThreshold
		) Y ON X.Businessunitid = y.businessunitid AND X.GroupingDescriptionID = Y.GroupingDescriptionID and X.ModelYear = Y.ModelYear
	) BIGYear  
	
	UNION all
	
	SELECT * FROM
	(
		SELECT 
		Z.BusinessUnitID,
		Z.GroupingDescriptionID,
		Z.ModelYear,
		Z.OptimalUnits,
		K.StockedUnits
		FROM
		(
			SELECT
				BusinessUnitID,
				GroupingDescriptionID,
				NULL AS 'ModelYear',
				OptimalUnitsModel AS 'OptimalUnits'
			FROM
				dbo.Inventory_A4
			WHERE
				OptimalUnitsModel IS NOT NULL
				AND OptimalUnitsModelYear IS NULL
				AND	CIACategoryID < 4
			GROUP BY
				BusinessUnitID,
				GroupingDescriptionID,
				OptimalUnitsModel
		) Z
		LEFT JOIN
		(
			SELECT
				BusinessUnitID,
				GroupingDescriptionID,
				NULL AS 'ModelYear',
				StockedUnitsModelOverUnitCostThreshold StockedUnits
			FROM
				dbo.Inventory_A4
			WHERE
				OptimalUnitsModel IS NOT NULL
				AND OptimalUnitsModelYear IS NULL
				AND	CIACategoryID < 4
			GROUP BY
				BusinessUnitID,
				GroupingDescriptionID,
				StockedUnitsModelOverUnitCostThreshold
		) K ON Z.Businessunitid = K.businessunitid AND Z.GroupingDescriptionID = K.GroupingDescriptionID	
	) BigModel
) ZZZ ON ZZZ.BusinessUnitID = B.BusinessUnitID
GROUP BY B.BusinessUnitID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ViewDealsReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ViewDealsReport]
GO

CREATE VIEW dbo.ViewDealsReport AS SELECT * FROM [IMT].dbo.InventorySales
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NAAAAuctionReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[NAAAAuctionReport]
GO

CREATE VIEW dbo.NAAAAuctionReport (
	AuctionTransactionID,
	PeriodID,
	VIC,
	AreaID,
	RegionID,
	RegionName,
	ModelYear,
	SaleDate,
	SaleTypeName,
	SalePrice,
	Mileage,
	Engine,
	Transmission,
	Series,
	VIN
)
AS
SELECT
	A.AuctionTransactionID,
	A.PeriodID,
	A.VIC,
	A.AreaID,
	A.RegionID,
	R.RegionName,
	A.ModelYear,
	A.SaleDate,
	A.SaleTypeName,
	A.SalePrice,
	A.Mileage,
	A.Engine,
	A.Transmission,
	A.Series,
	A.VIN
FROM
	[AuctionNet].dbo.AuctionDetailReport A
JOIN	[AuctionNet].dbo.Region_D R ON A.RegionID = R.RegionID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TradeClosingRateReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[TradeClosingRateReport]
GO

CREATE VIEW dbo.TradeClosingRateReport (
	BusinessUnitID,
	AppraisalID,
	VehicleID,
	AppraisalDate,
	InventoryReceivedDate,
	InventoryVehicleLight,
	VehicleMake,
	VehicleLine,
	VehicleSeries,
	ModelYear,
	VIN,
	AppraisalValue,
	CustomerOffer,
	BookName,
	BookCategoryName,
	BookValue
)
AS
SELECT
	A.BusinessUnitID,
	A.AppraisalID,
	A.VehicleID,
	A.DateCreated AppraisalDate,
	I.InventoryReceivedDate,
	I.CurrentVehicleLight,
	M.Make,
	M.Model,
	V.VehicleTrim,
	V.VehicleYear,
	V.VIN,
	COALESCE(AV.Value,0),
	AF.AppraisalFormOffer,
	TP.[Description],
	TPC.Category,
	AB.Value
FROM
	[IMT].dbo.Appraisals A
LEFT JOIN
	(
		SELECT
			BusinessUnitID,
			VehicleID,
			CurrentVehicleLight,
			TradeOrPurchase,
			max(I.InventoryReceivedDate) InventoryReceivedDate
		FROM
			[IMT].dbo.Inventory I
		GROUP BY    
			BusinessUnitID,
			VehicleID,
			CurrentVehicleLight,
			TradeOrPurchase
	) I ON
	(
			A.BusinessUnitID = I.BusinessUnitID
		and	A.VehicleID = I.VehicleID
		and	I.InventoryReceivedDate BETWEEN [IMT].dbo.ToDate(A.DateCreated) AND DATEADD(WK,2,[IMT].dbo.ToDate(A.DateCreated))
	)
JOIN	[IMT].dbo.DealerPreference DP ON A.BusinessUnitID = DP.BusinessUnitID
JOIN	[FLDW].dbo.AppraisalBookout_F AB ON AB.AppraisalID = A.AppraisalID AND AB.BusinessUnitID = A.BusinessUnitID AND AB.ThirdPartyCategoryID = DP.BookOutPreferenceID
JOIN	[FLDW].dbo.Appraisal_F AV ON AV.AppraisalID = A.AppraisalID AND AV.BusinessUnitID = A.BusinessUnitID
JOIN	[IMT].dbo.ThirdParties TP ON TP.ThirdPartyID = DP.GuideBookID
JOIN	[IMT].dbo.ThirdPartyCategories TPC ON TPC.ThirdPartyCategoryID = DP.BookOutPreferenceID
JOIN	[IMT].dbo.tbl_Vehicle V ON A.VehicleID = V.VehicleID
JOIN	[IMT].dbo.tbl_MakeModelGrouping M ON V.MakeModelGroupingID = M.MakeModelGroupingID
LEFT JOIN dbo.MaxAppraisalFormOptions AF ON AF.AppraisalID = A.AppraisalID
WHERE
	COALESCE(I.TradeOrPurchase,2) = 2
AND	((TP.ThirdPartyID <> 3) OR (TP.ThirdPartyID = 3 AND AB.BookoutValueTypeID = 2))
GO

-- SELECT * FROM TradeClosingRateReport WHERE BusinessUnitID = 100147 AND AppraisalDate BETWEEN DATEADD(WW,-1,GETDATE()) AND GETDATE()

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TradeReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[TradeReport]
GO

CREATE VIEW dbo.TradeReport (
	BusinessUnitID,
	InventoryReceivedDate,
	TradeAnalyzerDate,
	VIN,
	VehicleYear,
	VehicleDescription,
	UnitCost,
	MileageReceived,
	RiskLevel,
	Valid
)
AS
SELECT
	I.BusinessUnitID,
	I.InventoryReceivedDate,
	A.TradeAnalyzerDate,
	UPPER(V.VIN) VIN,
	V.VehicleYear,
	LEFT(V.Make + ' '+  V.Model + isnull(' ' + nullif(V.VehicleTrim,'N/A'),'') + ' ' + case when VBS.VehicleBodyStyleID = 1 then upper(isnull(V.OriginalBodyStyle,'')) else VBS.Description end,80) VehicleDescription,
	I.UnitCost,
	I.MileageReceived,
	I.CurrentVehicleLight RiskLevel,
	case when DATEDIFF(WW,imt.dbo.todate(A.TradeAnalyzerDate), I.InventoryReceivedDate) BETWEEN 0 AND 2 then 1 else 0 end as 'Valid'
FROM
	[IMT].dbo.Inventory I
JOIN	[IMT].dbo.Vehicle V on I.VehicleID = V.VehicleID
JOIN	[IMT].dbo.VehicleBodyStyle VBS on V.VehicleBodyStyleID = VBS.VehicleBodyStyleID
LEFT JOIN (
		SELECT
			A.BusinessUnitID,
			V.VIN,
			MAX(A.DateCreated) TradeAnalyzerDate
 		FROM
			[IMT].dbo.Appraisals A
		JOIN	[IMT].dbo.Vehicle V on A.VehicleID = V.VehicleID
		GROUP BY
			A.BusinessUnitID,
			V.VIN
	) A on I.BusinessUnitID = A.BusinessUnitID and V.VIN = A.VIN
WHERE
	I.TradeOrPurchase = 2 -- TRADE
AND	I.InventoryType = 2   -- USED
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TradeFlipsReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[TradeFlipsReport]
GO

CREATE VIEW dbo.TradeFlipsReport (
	BusinessUnitID,
	InventoryReceivedDate,
	SaleDate,
	DaysInInventory,
	UnitCost,
	FrontEndGross,
	VehicleYear,
	VehicleDescription,
	MileageReceived,
	RiskLevel
)
AS
SELECT
	I.BusinessUnitID,
	I.InventoryReceivedDate,
	VS.DealDate,
	datediff(dd, I.InventoryReceivedDate,VS.DealDate) as 'DaysInInventory',
	I.UnitCost,
	VS.FrontEndGross,
	V.VehicleYear,
	LEFT(V.Make + ' '+  V.Model + isnull(' ' + nullif(V.VehicleTrim,'N/A'),'') + ' ' + case when VBS.VehicleBodyStyleID = 1 then upper(isnull(V.OriginalBodyStyle,'')) else VBS.Description end,80) VehicleDescription,
	COALESCE(I.MileageReceived,VS.VehicleMileage),
	I.InitialVehicleLight RiskLevel
FROM
	[IMT].dbo.tbl_vehiclesale VS
JOIN	[IMT].dbo.Inventory I on VS.inventoryid = I.inventoryid
JOIN	[IMT].dbo.Vehicle V on I.VehicleID = V.VehicleID
JOIN	[IMT].dbo.VehicleBodyStyle VBS on V.VehicleBodyStyleID = VBS.VehicleBodyStyleID
WHERE
	I.TradeOrPurchase = 2 -- TRADE
AND	I.InventoryType = 2   -- USED
AND	VS.SaleDescription = 'W'
AND 	datediff(dd, I.InventoryReceivedDate,VS.DealDate) < 30
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ping_providers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ping_providers]
GO

CREATE VIEW dbo.ping_providers (
	id,
	name,
	code,
	available,
	sortOrder
)
AS
SELECT
	id,
	name,
	code,
	available,
	sortOrder
FROM
	[IMT].dbo.PING_Provider
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryBucketRanges]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetInventoryBucketRanges]
GO

CREATE  FUNCTION dbo.GetInventoryBucketRanges
(
@BusinessUnitID INT, 		-- Optional; if null, return for all 
				-- business units
@InventoryBucketID INT
)

RETURNS @Results TABLE (BusinessUnitID		INT,
			InventoryBucketRangeID	INT, 
			InventoryBucketID 	INT, 
			RangeID 		TINYINT, 
			Description 		VARCHAR(100), 
			Low 			INT,
			High 			INT,
			Lights 			TINYINT,
			LongDescription 	VARCHAR(100) null,
			SortOrder		TINYINT
			)
AS

BEGIN

	INSERT
	INTO	@Results
	
	SELECT	BU.BusinessUnitID, 
		COALESCE(IBR.InventoryBucketRangeID, IBR1.InventoryBucketRangeID, IBR2.InventoryBucketRangeID, IBR3.InventoryBucketRangeID, IBR4.InventoryBucketRangeID),
		COALESCE(IBR.InventoryBucketID, IBR1.InventoryBucketID, IBR2.InventoryBucketID, IBR3.InventoryBucketID, IBR4.InventoryBucketID),
		COALESCE(IBR.RangeID, IBR1.RangeID, IBR2.RangeID, IBR3.RangeID, IBR4.RangeID),
		COALESCE(IBR.Description, IBR1.Description, IBR2.Description, IBR3.Description, IBR4.Description),
		COALESCE(IBR.Low, IBR1.Low, IBR2.Low, IBR3.Low, IBR4.Low),
		COALESCE(IBR.High, IBR1.High, IBR2.High, IBR3.High, IBR4.High),
		COALESCE(IBR.Lights, IBR1.Lights, IBR2.Lights, IBR3.Lights, IBR4.Lights),
		COALESCE(IBR.LongDescription, IBR1.LongDescription, IBR2.LongDescription, IBR3.LongDescription, IBR4.LongDescription),
		COALESCE(IBR.SortOrder, IBR1.SortOrder, IBR2.SortOrder, IBR3.SortOrder, IBR4.SortOrder)
	
	
	FROM	[IMT]..BusinessUnit BU
		LEFT JOIN [IMT]..InventoryBucketRanges IBR ON IBR.InventoryBucketID = @InventoryBucketID AND BU.BusinessUnitID = IBR.BusinessUnitID
		LEFT JOIN (	[IMT]..BusinessUnitRelationship BUR1
				LEFT JOIN [IMT]..InventoryBucketRanges IBR1 ON IBR1.InventoryBucketID = @InventoryBucketID AND BUR1.ParentID = IBR1.BusinessUnitID
				) ON BU.BusinessUnitID = BUR1.BusinessUnitID AND IBR.InventoryBucketRangeID IS NULL
	
		LEFT JOIN (	[IMT]..BusinessUnitRelationship BUR2
				LEFT JOIN [IMT]..InventoryBucketRanges IBR2 ON IBR2.InventoryBucketID = @InventoryBucketID AND BUR2.ParentID = IBR2.BusinessUnitID
				) ON BUR1.ParentID = BUR2.BusinessUnitID AND IBR1.InventoryBucketRangeID IS NULL
	
		LEFT JOIN (	[IMT]..BusinessUnitRelationship BUR3
				LEFT JOIN [IMT]..InventoryBucketRanges IBR3 ON IBR3.InventoryBucketID = @InventoryBucketID AND BUR3.ParentID = IBR3.BusinessUnitID
				) ON BUR2.ParentID = BUR3.BusinessUnitID AND IBR2.InventoryBucketRangeID IS NULL
	
		LEFT JOIN (	[IMT]..BusinessUnitRelationship BUR4
				LEFT JOIN [IMT]..InventoryBucketRanges IBR4 ON IBR4.InventoryBucketID = @InventoryBucketID AND BUR4.ParentID = IBR4.BusinessUnitID
				) ON BUR3.ParentID = BUR4.BusinessUnitID AND IBR3.InventoryBucketRangeID IS NULL
				
	WHERE	BusinessUnitTypeID = 4
		AND (BU.BusinessUnitID = @BusinessUnitID OR @BusinessUnitID IS NULL)
		AND COALESCE(IBR.InventoryBucketRangeID, IBR1.InventoryBucketRangeID, IBR2.InventoryBucketRangeID, IBR3.InventoryBucketRangeID, IBR4.InventoryBucketRangeID) IS NOT NULL
		
	RETURN
END


GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryBookoutValueAll]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[InventoryBookoutValueAll]
GO

CREATE view dbo.InventoryBookoutValueAll
as
select	I.InventoryID, StockNumber, I.BusinessUnitID, VehicleID, TradeOrPurchase, InventoryReceivedDate, ReconditionCost, InventoryType, DeleteDT, InitialVehicleLight, UnitCost, CurrentVehicleLight, InventoryActive, 		
	isnull(IBCV.Value,0.0) GuideBookValue
from	[IMT]..Inventory I
	join [IMT]..BusinessUnit BU on I.BusinessUnitID = BU.BusinessUnitID 
	join [IMT]..BusinessUnitRelationship BUR on BU.BusinessUnitID = BUR.BusinessUnitID
	join [IMT]..DealerPreference DP on BU.BusinessUnitID = DP.BusinessUnitID 
	left join [IMT]..InventoryBookoutCurrentValue IBCV on I.InventoryID = IBCV.InventoryID and DP.BookoutPreferenceId = IBCV.ThirdPartyCategoryID


GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryGraphData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetInventoryGraphData]
GO

CREATE  PROCEDURE dbo.GetInventoryGraphData
	@BusinessUnitID	int
AS

SET NOCOUNT ON

DECLARE @today SMALLDATETIME
SET @today = [IMT].dbo.ToDate(GETDATE())

DECLARE @IBR TABLE (	BusinessUnitID		INT,
			InventoryBucketRangeID	INT, 
			InventoryBucketID 	INT, 
			RangeID 		TINYINT, 
			Description 		VARCHAR(100), 
			Low 			INT,
			High 			INT,
			Lights 			TINYINT,
			LongDescription 	VARCHAR(100) null,
			SortOrder		TINYINT
			)

INSERT INTO @IBR
SELECT * FROM [IMT].dbo.GetInventoryBucketRanges(@BusinessUnitID, 4)

DECLARE @AGG TABLE (	Low 			INT,
			High 			INT,
			RangeID 		TINYINT, 
			Units			INT,
			SumBook			INT,
			SumUnitCost		INT,
			BookMinusCost		INT,
			TotalInventoryDollars	INT,
			RedLights		INT,
			YellowLights		INT,
			GreenLights		INT,
			UnknownLights		INT
			)

INSERT INTO @AGG
SELECT 
	B.[Low],
	B.[High],
	B.RangeID,
	COUNT(*) Units,
	SUM(COALESCE(IBVA.GuideBookValue,0)) SumBook,
	SUM(CASE WHEN COALESCE(IBVA.GuideBookValue,0) > 0 THEN IX.UnitCost ELSE 0 END) SumUnitCost,
	SUM(COALESCE(IBVA.GuideBookValue,0)) - SUM(CASE WHEN COALESCE(IBVA.GuideBookValue,0) > 0 THEN IX.UnitCost ELSE 0 END) BookMinusCost,
	SUM(IX.UnitCost) TotalInventoryDollars,
	SUM(CASE WHEN IX.CurrentVehicleLight = 1 THEN 1 ELSE 0 END) RedLights,
	SUM(CASE WHEN IX.CurrentVehicleLight = 2 THEN 1 ELSE 0 END) YellowLights,
	SUM(CASE WHEN IX.CurrentVehicleLight = 3 THEN 1 ELSE 0 END) GreenLights,
	SUM(CASE WHEN IX.CurrentVehicleLight = 0 THEN 1 ELSE 0 END) UnknownLights
FROM
	[IMT].dbo.Inventory IX
JOIN	dbo.InventoryBookoutValueAll IBVA ON (
			IX.InventoryID    = IBVA.InventoryID
		AND	IX.BusinessUnitID = IBVA.BusinessUnitID
	)
JOIN	@IBR B ON (
			DATEDIFF(DD, IX.InventoryReceivedDate, @today) BETWEEN B.Low AND ISNULL(B.High, 9999)
		AND	(
				B.Lights = 7 
			OR	(B.Lights = 4 and IX.CurrentVehicleLight = 3)
			OR	(B.Lights = 3 and IX.CurrentVehicleLight < 3)
		)
	)
WHERE
	IX.BusinessUnitID = @BusinessUnitID
AND	IX.InventoryActive = 1
AND	IX.InventoryType = 2
GROUP
BY	B.RangeID, B.[Low], B.[High]
ORDER
BY	B.RangeID

SELECT
	F.Low,
	F.High,
	F.RangeID,
	COALESCE(A.Units,0) Units,
	COALESCE(A.SumBook,0) SumBook,
	COALESCE(A.SumUnitCost,0) SumUnitCost,
	COALESCE(A.BookMinusCost,0) BookMinusCost,
	COALESCE(A.TotalInventoryDollars,0) TotalInvDollars,
	COALESCE(A.RedLights,0) RedLights,
	COALESCE(A.YellowLights,0) YellowLights,
	COALESCE(A.GreenLights,0) GreenLights,
	COALESCE(A.UnknownLights,0) UnknownLights
FROM
	@IBR F LEFT JOIN @AGG A ON F.RangeID = A.RangeID

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OverstockedModelsThresholds]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.OverstockedModelsThresholds
GO

CREATE TABLE dbo.OverstockedModelsThresholds (
	OptUnits int NOT NULL ,
	Threshold int NOT NULL 
)
GO



Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(4,7)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(5,8)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(6,9)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(7,11)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(8,12)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(9,13)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(10,14)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(11,15)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(12,16)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(13,18)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(14,19)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(15,20)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(16,21)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(17,22)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(18,23)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(19,24)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(20,25)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(21,27)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(22,28)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(23,29)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(24,30)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(25,31)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(26,32)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(27,33)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(28,34)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(29,35)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(30,36)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(31,38)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(32,39)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(33,40)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(34,41)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(35,42)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(36,43)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(37,44)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(38,45)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(39,46)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(40,47)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(41,48)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(42,49)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(43,51)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(44,52)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(45,53)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(46,54)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(47,55)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(48,56)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(49,57)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(50,58)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(51,59)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(52,60)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(53,61)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(54,62)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(55,63)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(56,64)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(57,66)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(58,67)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(59,68)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(60,69)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(61,70)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(62,71)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(63,72)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(64,73)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(65,74)
Insert Into OverstockedModelsThresholds (OptUnits,Threshold) values(66,75)
go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetDaysSupply]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetDaysSupply]
GO

CREATE PROCEDURE dbo.GetDaysSupply
---Parameters-------------------------------------------------------------------
--
@buid int
--
AS
SET NOCOUNT ON

DECLARE @currentDateTime smalldatetime, @today smalldatetime, @yesterday smalldatetime, @26WeeksAgo smalldatetime
SET @currentDateTime = getDate()
set @today = [IMT].dbo.ToDate(@currentDateTime)
set @yesterday = dateadd(dd,-1,@today)
set @26WeeksAgo = dateadd(dd,-181,@yesterday)
--SET @buid = 100147

DECLARE @dates table(buid int, forecast int, BeginDate smalldatetime, EndDate smalldatetime)
INSERT INTO @dates
SELECT businessunitid, forecast, begindate, enddate
FROM [IMT].dbo.GetCompositeBasisPeriod(@buid,@today,'StoreTargetInventory')

DECLARE @f1 smalldatetime, @f2 smalldatetime, @r1 smalldatetime, @r2 smalldatetime
SET @f1 = (SELECT begindate FROM @dates WHERE forecast = 1)
SET @f2 = (SELECT enddate FROM @dates WHERE forecast = 1)
SET @r1 = (SELECT begindate FROM @dates WHERE forecast = 0)
SET @r2 = (SELECT enddate FROM @dates WHERE forecast = 0)
DECLARE @dsTarget int
SET @dsTarget = 45 --(select CIATargetDaysSupply FROM DealerPreference WHERE BusinessUnitID = @buid)

declare @DSR table (Sales int, DSR real, OptUnits int)
Insert into @DSR
SELECT 
count(*) AS 'Sales',
cast(count(*) AS real) / 182 AS 'DSR',
cast(Round((cast(count(*) AS real) / 182) * @dsTarget / .9,0) AS int) AS 'OptimalUnits'
FROM [IMT].dbo.tbl_VehicleSale vs
JOIN [IMT].dbo.Inventory ix ON vs.InventoryID = ix.InventoryID
WHERE 
ix.businessunitid = @buid
AND ix.inventorytype = 2
and ix.unitcost >= 4000
AND 
	(
	vs.dealdate between @r1 and @r2
	or
	vs.dealdate between @f1 and @f2
	)

--and vs.dealdate between @26WeeksAgo and @yesterday
AND vs.saledescription = 'R'

declare @InvUnits int
set @InvUnits = 
(
	SELECT  count(*)
	FROM [IMT].dbo.inventory ix
	WHERE ix.inventoryactive = 1
	AND ix.deletedt IS null
	AND ix.businessunitid = @buid
	AND ix.inventorytype = 2
	and ix.unitcost >= 4000
	AND NOT EXISTS ( 
		SELECT 1 
		FROM [IMT].dbo.AIP_Event E 
		JOIN [IMT].dbo.AIP_EventType T ON T.AIP_EventTypeID = E.AIP_EventTypeID 
		WHERE E.InventoryID = IX.InventoryID 
		AND T.AIP_EventTypeID = E.AIP_EventTypeID 
		AND T.AIP_EventCategoryID IN (2,3) 
	)
)

select 
cast(round(CASE WHEN @InvUnits = 0 THEN 0 ELSE @InvUnits / DSR END,0) as int) as 'DS'
from @DSR

GO

--
-- Search And Aquisition Home Page Stored Procedure (Thanks Bill!)
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetPurchasingCenterVehicleSummary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetPurchasingCenterVehicleSummary]
GO

CREATE PROC dbo.GetPurchasingCenterVehicleSummary
----------------------------------------------------------------------------------------------------
--
--	Returns the number of vehicles available for the various auction channels
--
---Parmeters----------------------------------------------------------------------------------------
--
@BusinessUnitID	INT,
@MemberID	INT = NULL,		-- Optional MemberID.  If not passed, will default to CIA
@RunDateTime	SMALLDATETIME = NULL,	-- Optional run date, defaults to GETDATE()
@LiveBeginDate	SMALLDATETIME = NULL,	-- Begin date of the search period for live auctions
@LiveEndDate	SMALLDATETIME = NULL,	-- End date of the live auction search period.  The period
					-- will default to two weeks (from RunDate) if not passed
@Mode		TINYINT = 1,		-- 0 = DETAIL, 1 = SUMMARY
@IMP_Filter	BIT = 0,		-- Flags whether to filter out vehicles that have an IMP
					-- Event where the category is "Wholesale" or "Sold"
@Debug		BIT = 0			-- "Debug" mode, defaults to off
--
---History------------------------------------------------------------------------------------------
--	
--	WGH	01/12/2007	Initial design/development
--
----------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON

SET @RunDateTime 	= ISNULL(@RunDateTime, GETDATE())
SET @LiveBeginDate 	= [IMT].dbo.ToDate(ISNULL(@LiveBeginDate,@RunDateTime))
SET @LiveEndDate   	= [IMT].dbo.ToDate(ISNULL(@LiveEndDate,DATEADD(DD, 13, @LiveEndDate)))	-- MAKE SURE IT'S ONLY A 14-DAY PERIOD, NOT 15-DAYS!!!

DECLARE @Candidates TABLE (BusinessUnitID INT NOT NULL, GroupingDescriptionID INT NOT NULL, ModelYear INT)
DECLARE @AccessGroupMileageDistance TABLE (AccessGroupID INT, MinVehicleMileage INT, MaxVehicleMileage INT, MaxDistanceFromDealer INT)

------------------------------------------------------------------------------------------------------------------------
--	GET VEHICLE GROUPING & YEAR SEARCH CANDIDATES
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	@Candidates (BusinessUnitID, GroupingDescriptionID, ModelYear)

SELECT	DISTINCT @BusinessUnitID, MMG.GroupingDescriptionID,		-- NEEDS TO BE DISTINCT DUE TO MMG->GD "DESIGN"
	ModelYear
FROM	[IMT]..SearchCandidateList SCL
	JOIN [IMT]..SearchCandidate SC ON SCL.SearchCandidateListID = SC.SearchCandidateListID
	JOIN [IMT]..SearchCandidateOption SCO ON SC.SearchCandidateID = SCO.SearchCandidateID
	JOIN [IMT]..SearchCandidateOptionAnnotation A ON SCO.SearchCandidateOptionID = A.SearchCandidateOptionID
	JOIN [IMT]..SearchCandidateOptionAnnotationType T ON A.SearchCandidateOptionAnnotationTypeID = T.SearchCandidateOptionAnnotationTypeID
	LEFT JOIN [IMT]..tbl_MakeModelGrouping MMG ON SC.Make = MMG.Make AND SC.Line = MMG.Model AND SC.SegmentID = MMG.DisplayBodyTypeID
WHERE	A.SearchCandidateOptionAnnotationTypeID IN (1,3)	-- Optimal Plan, Hot List
	AND SCL.BusinessUnitID = @BusinessUnitID
	AND SCL.MemberID = @MemberID

INSERT
INTO	@Candidates (BusinessUnitID, GroupingDescriptionID, ModelYear)
SELECT	BusinessUnitID, GroupingDescriptionID, ModelYear
FROM	Inventory_A5 I
WHERE	I.BusinessUnitID = @BusinessUnitID
	AND I.CIACategoryID <> 4 -- SBW: FIX 28-Jan-2007
	AND (SELECT COUNT(*) FROM @Candidates) = 0

------------------------------------------------------------------------------------------------------------------------
--	GRAB THE ACCESS GROUPS AND MILEAGE/DISTANCE SETTINGS AS SELECTED ON THE S&A PAGE.
--	VERIFY THAT WE SHOULD USE THE "Permanet Market List" IF THE CURRENT DOES NOT EXIST.
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	@AccessGroupMileageDistance (AccessGroupID, MinVehicleMileage, MaxVehicleMileage, MaxDistanceFromDealer)
SELECT	AccessGroupID, ISNULL(AG.MinVehicleMileage,0), ISNULL(AG.MaxVehicleMileage,999999), AG.MaxDistanceFromDealer
FROM	[IMT]..tbl_MemberATCAccessGroupList AGL
	JOIN [IMT]..tbl_MemberATCAccessGroup AG ON AGL.MemberATCAccessGroupListID = AG.MemberATCAccessGroupListID

WHERE	AGL.MemberATCAccessGroupListTypeID = (CASE WHEN EXISTS (SELECT 	1 
								FROM 	[IMT]..tbl_MemberATCAccessGroupList		
								WHERE	BusinessUnitID = @BusinessUnitID		
									AND MemberID = @MemberID
						  			AND MemberATCAccessGroupListTypeID = 2	-- Current Market List
								) THEN 2 ELSE 1 END)				-- Else Permanent Market List
	AND Suppress = 0				
	AND BusinessUnitID = @BusinessUnitID
	AND MemberID = @MemberID

INSERT
INTO	@AccessGroupMileageDistance (AccessGroupID,MinVehicleMileage,MaxVehicleMileage,MaxDistanceFromDealer)
SELECT	NULL, 0, 999999, DP.PurchasingDistanceFromDealer
FROM	[IMT]..DealerPreference DP
WHERE	DP.BusinessUnitID = @BusinessUnitID
	AND (SELECT COUNT(*) FROM @AccessGroupMileageDistance) = 0

------------------------------------------------------------------------------------------------------------------------
--	DEBUG
------------------------------------------------------------------------------------------------------------------------
IF @Debug = 1 BEGIN

	SELECT 	DISTINCT GD.GroupingDescriptionID, GD.GroupingDescription, C.ModelYear
	FROM 	@Candidates C 
		JOIN IMT..GroupingDescription GD ON C.GroupingDescriptionID = GD.GroupingDescriptionID
	ORDER
	BY	GD.GroupingDescriptionID, GD.GroupingDescription, C.ModelYear
	SELECT	*
	FROM	@AccessGroupMileageDistance
END

DECLARE	@Results TABLE (ChannelID TINYINT, Description VARCHAR(100), AccessGroupID INT, Units INT, Locations INT)

------------------------------------------------------------------------------------------------------------------------
--	ONLINE AUCTION
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	@Results (ChannelID, Description, AccessGroupID, Units)

SELECT	1, GD.GroupingDescription, AG.AccessGroupID, COUNT(*) [On-Line Auctions]

FROM	[IMT].dbo.BusinessUnit BU
	JOIN [IMT].dbo.DealerPreference DP on DP.BusinessUnitID = BU.BusinessUnitID
	JOIN dbo.BusinessUnitAccessGroups BUAG ON BU.BusinessUnitID = BUAG.BusinessUnitID
	JOIN [ATC].dbo.AccessGroup AG ON BUAG.AccessGroupID = AG.AccessGroupID
	JOIN [ATC].dbo.VehicleAccessGroups VAG ON VAG.AccessGroupID = AG.AccessGroupID
	JOIN [ATC].dbo.Vehicles V ON V.VehicleID = VAG.VehicleID
	JOIN [ATC].dbo.VehicleProperties VP ON VP.VehicleID = V.VehicleID
	JOIN [IMT].dbo.tbl_MakeModelGrouping MMG ON MMG.MakeModelGroupingID = VP.MakeModelGroupingID
	JOIN [IMT].dbo.GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID

 	JOIN dbo.ZipCode ZC1 ON BU.ZipCode = ZC1.ZipCode
 	JOIN dbo.ZipCode ZC2 ON V.ZIP_CODE = ZC2.ZipCode
 
	JOIN @Candidates C ON BU.BusinessUnitID = C.BusinessUnitID AND MMG.GroupingDescriptionID = C.GroupingDescriptionID AND VP.VehicleYear = ISNULL(C.ModelYear, VP.VehicleYear)

	JOIN @AccessGroupMileageDistance GMD ON VAG.AccessGroupID = ISNULL(GMD.AccessGroupID, VAG.AccessGroupID) 
						AND V.MILEAGE BETWEEN GMD.MinVehicleMileage AND GMD.MaxVehicleMileage 
						AND degrees(acos((sin(radians(ZC1.Lat)) * sin(radians(ZC2.Lat)) + cos(radians(ZC1.Lat)) * cos(radians(ZC2.Lat)) * cos(radians(ZC1.Long - ZC2.Long))))) * 69.09 BETWEEN 0 AND GMD.MaxDistanceFromDealer

WHERE	DATEADD(HH, +2,CAST(V.AUCTION_END AS DATETIME)) >= @RunDateTime

GROUP 
BY	GD.GroupingDescription, AG.AccessGroupID with ROLLUP
HAVING	(@Mode = 1 AND GROUPING(GD.GroupingDescription) = @Mode) OR (@Mode = 0 AND GROUPING(AG.AccessGroupID) = 0)

------------------------------------------------------------------------------------------------------------------------
--	IN-GROUP
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	@Results (ChannelID, Description, AccessGroupID, Units)

SELECT	2, GD.GroupingDescription, 141, COUNT(*) [In-Group]

FROM	[IMT]..BusinessUnit BU 
	JOIN [IMT]..BusinessUnitRelationship BR1 ON BU.BusinessUnitID = BR1.BusinessUnitID
	JOIN [IMT]..BusinessUnitRelationship BR2 ON BR1.ParentID = BR2.ParentID
	JOIN [IMT]..BusinessUnit BU2 ON BR2.BusinessUnitID = BU2.BusinessUnitID AND BU2.BusinessUnitID <> BU.BusinessUnitID 
	JOIN [FLDW]..InventoryActive I ON BU2.BusinessUnitID = I.BusinessUnitID
	JOIN [FLDW]..Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
 	JOIN [IMT]..DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID AND DP.GoLiveDate IS NOT NULL
 	JOIN [IMT]..tbl_MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
	JOIN [IMT].dbo.GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID
 
  	JOIN @Candidates C ON BU.BusinessUnitID = C.BusinessUnitID AND MMG.GroupingDescriptionID = C.GroupingDescriptionID AND V.VehicleYear = ISNULL(C.ModelYear, V.VehicleYear)

	JOIN dbo.ZipCode ZC1 ON BU.ZipCode = ZC1.ZipCode
	JOIN dbo.ZipCode ZC2 ON BU2.ZipCode = ZC2.ZipCode

	JOIN @AccessGroupMileageDistance GMD ON 141 = ISNULL(GMD.AccessGroupID, 141) 
						AND I.MileageReceived BETWEEN GMD.MinVehicleMileage AND GMD.MaxVehicleMileage 
						AND degrees(acos((sin(radians(ZC1.Lat)) * sin(radians(ZC2.Lat)) + cos(radians(ZC1.Lat)) * cos(radians(ZC2.Lat)) * cos(radians(ZC1.Long - ZC2.Long))))) * 69.09 BETWEEN 0 AND GMD.MaxDistanceFromDealer
WHERE	BU.BusinessUnitID = @BusinessUnitID
	AND I.InventoryType = 2
	AND I.DeleteDT IS NULL
	AND NOT EXISTS (SELECT	1
			FROM	[IMT].dbo.AIP_Event E
				JOIN [IMT].dbo.AIP_EventType T ON T.AIP_EventTypeID = E.AIP_EventTypeID
			WHERE 	E.InventoryID = I.InventoryID
				AND T.AIP_EventTypeID = E.AIP_EventTypeID
				AND T.AIP_EventCategoryID IN (2,3)
				AND @IMP_Filter = 1
			)
GROUP
BY	GD.GroupingDescription with ROLLUP
HAVING	GROUPING(GD.GroupingDescription) = @Mode

------------------------------------------------------------------------------------------------------------------------
--	LIVE AUCTIONS
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	@Results (ChannelID, Description, AccessGroupID, Units, Locations)

SELECT	3, CASE WHEN @Mode = 1 THEN NULL ELSE GD.GroupingDescription END, 142, COUNT(*) [Live], COUNT(DISTINCT S.SaleID) [Locations]
FROM	[Auction].dbo.SaleVehicle SV 
	JOIN [Auction].dbo.Sale S ON SV.SaleID = S.SaleID
	JOIN [Auction].dbo.Location L ON L.LocationID = S.LocationID
	JOIN [Auction]..SaleVehicleCompanyData_Adesa SVC ON SV.SaleVehicleID = SVC.SaleVehicleID
	JOIN [IMT].dbo.BusinessUnit BU ON BU.BusinessUnitID = @BusinessUnitID
	JOIN [IMT].dbo.GroupingDescription GD ON SV.GroupingDescriptionID = GD.GroupingDescriptionID

 	JOIN dbo.ZipCode ZC1 ON BU.ZipCode = ZC1.ZipCode
 	JOIN dbo.ZipCode ZC2 ON L.ZipCode = ZC2.ZipCode

 	JOIN @Candidates C ON BU.BusinessUnitID = C.BusinessUnitID AND SV.GroupingDescriptionID = C.GroupingDescriptionID AND SV.Year = ISNULL(C.ModelYear, SV.Year)

	JOIN @AccessGroupMileageDistance GMD ON 142 = ISNULL(GMD.AccessGroupID, 142) 
						AND CAST(SVC.Odometer AS INT) BETWEEN GMD.MinVehicleMileage AND GMD.MaxVehicleMileage 
						AND degrees(acos((sin(radians(ZC1.Lat)) * sin(radians(ZC2.Lat)) + cos(radians(ZC1.Lat)) * cos(radians(ZC2.Lat)) * cos(radians(ZC1.Long - ZC2.Long))))) * 69.09 BETWEEN 0 AND GMD.MaxDistanceFromDealer

WHERE	S.DateHeld BETWEEN @LiveBeginDate  AND @LiveEndDate 

GROUP
BY	CASE WHEN @Mode = 1 THEN NULL ELSE GD.GroupingDescription END

------------------------------------------------------------------------------------------------------------------------
--	RETURN RESULTS
------------------------------------------------------------------------------------------------------------------------

SELECT	R.ChannelID, ISNULL(R.Description, PCC.ChannelName) Description, AccessGroupID, Units, Locations
FROM	@Results R
	LEFT JOIN [IMT]..PurchasingCenterChannels PCC ON R.ChannelID = PCC.ChannelID
	
GO

-- EXEC dbo.GetPurchasingCenterVehicleSummary @BusinessUnitID = 100148, @MemberID = 100000, @Mode = 1, @IMP_Filter = 1, @LiveBeginDate = '12/1/2006', @LiveEndDate = '1/31/2007'
-- EXEC dbo.GetPurchasingCenterVehicleSummary @BusinessUnitID = 100148, @MemberID = 100000, @Mode = 0, @IMP_Filter = 0, @LiveBeginDate = '12/1/2006', @LiveEndDate = '1/31/2007'

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetMetricPeriodDates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetMetricPeriodDates]
GO

CREATE PROCEDURE dbo.GetMetricPeriodDates(@PeriodID INT)
AS
	DECLARE @RunDate DATETIME
	SET @RunDate = GETDATE()
	SELECT	BeginDate, EndDate
	FROM	[EdgeScorecard].dbo.GetPeriodDates(@RunDate)
	WHERE	PeriodID = @PeriodID
GO
