
--
-- Per vehicle make book preference for each business unit
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewBookoutPreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookoutPreferences
GO

CREATE TABLE dbo.AppraisalReviewBookoutPreferences (
	AppraisalReviewBookoutPreferenceID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID INT NOT NULL,
	VehicleMakeID  INT NOT NULL,
	ThirdPartyID TINYINT NOT NULL,
	ThirdPartyCategoryID INT NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookoutPreference PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookoutPreferenceID
	)
)
GO

CREATE CLUSTERED INDEX IDX_AppraisalReviewBookoutPreferences ON AppraisalReviewBookoutPreferences (
	BusinessUnitID,
	VehicleMakeID
)

INSERT INTO dbo.AppraisalReviewBookoutPreferences (
	BusinessUnitID,
	VehicleMakeID,
	ThirdPartyID,
	ThirdPartyCategoryID
)
SELECT
	P.business_unit_id,
	M.id,
	P.primary_book_id,
	P.primary_book_category_id
FROM
	dbo.business_unit_preferences P
	CROSS JOIN dbo.vehicle_makes M
GO

--
-- Appraisal review preferences
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewPreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewPreferences
GO

CREATE TABLE dbo.AppraisalReviewPreferences (
	AppraisalReviewPreferenceID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID INT NOT NULL,
	AppraisalCreatedDaysThreshold INT NOT NULL,
	MaxDistanceFromDealer INT NOT NULL,
	ValuationDiscrepancyThresholdRule1 INT NOT NULL,
	ValuationDiscrepancyThresholdRule2 INT NOT NULL,
	ValuationDiscrepancyThresholdRule3 INT NOT NULL,
	ValuationDiscrepancyThresholdRule6 INT NOT NULL,
	EnabledRule1 BIT NOT NULL,
	EnabledRule2 BIT NOT NULL,
	EnabledRule3 BIT NOT NULL,
	EnabledRule4 BIT NOT NULL,
	EnabledRule5 BIT NOT NULL,
	EnabledRule6 BIT NOT NULL,
	ClosingRateTarget INT NOT NULL,
	CONSTRAINT PK_AppraisalReviewPreference PRIMARY KEY NONCLUSTERED (
		AppraisalReviewPreferenceID
	)
)
GO

CREATE INDEX IDX_AppraisalReviewPreferences ON dbo.AppraisalReviewPreferences (
	BusinessUnitID
)
GO

CREATE VIEW dbo.DefaultAppraisalReviewPreferences (
	BusinessUnitID,
	MaxDistanceFromDealer,
	AppraisalCreatedDaysThreshold,
	ValuationDiscrepancyThresholdRule1,
	ValuationDiscrepancyThresholdRule2,
	ValuationDiscrepancyThresholdRule3,
	ValuationDiscrepancyThresholdRule6,
	EnabledRule1,
	EnabledRule2,
	EnabledRule3,
	EnabledRule4,
	EnabledRule5,
	EnabledRule6,
	ClosingRateTarget
)
AS
SELECT
	B.BusinessUnitID,
	100,  -- miles
	7,    -- display appraisals for last week
	1500, -- rule 1; appraisal vs book
	1500, -- rule 2; appraisal vs naaa
	1500, -- rule 3; appraisal vs nada
	500,  -- rule 6; appraisal vs offer
	1, 1, 1, 1, 1, 1,60
FROM
	[IMT].dbo.BusinessUnit B
GO

INSERT INTO dbo.AppraisalReviewPreferences (
	BusinessUnitID,
	MaxDistanceFromDealer,
	AppraisalCreatedDaysThreshold,
	ValuationDiscrepancyThresholdRule1,
	ValuationDiscrepancyThresholdRule2,
	ValuationDiscrepancyThresholdRule3,
	ValuationDiscrepancyThresholdRule6,
	EnabledRule1,
	EnabledRule2,
	EnabledRule3,
	EnabledRule4,
	EnabledRule5,
	EnabledRule6,
	ClosingRateTarget
)
SELECT
	BusinessUnitID,
	MaxDistanceFromDealer,
	AppraisalCreatedDaysThreshold,
	ValuationDiscrepancyThresholdRule1,
	ValuationDiscrepancyThresholdRule2,
	ValuationDiscrepancyThresholdRule3,
	ValuationDiscrepancyThresholdRule6,
	EnabledRule1,
	EnabledRule2,
	EnabledRule3,
	EnabledRule4,
	EnabledRule5,
	EnabledRule6,
	ClosingRateTarget
FROM
	dbo.DefaultAppraisalReviewPreferences
GO

--
-- The views which build the entries for appraisal review. One rule, one view is the rule folks. SBW
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_AppraisalToBookValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_AppraisalToBookValue]
GO

CREATE VIEW dbo.GetAppraisalReviews_AppraisalToBookValue(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT
	S.BusinessUnitID,
	S.AppraisalID,
	1, 0, 0, 0, 0, 0,
	S.AppraisalValue,
	S.CustomerOffer,
	S.BookoutValue,
	S.ThirdPartyID,
	S.ThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	S.DateCreated
FROM
	dbo.AppraisalReviewBookoutPreferences PM
JOIN	dbo.AppraisalReviewPreferences P ON PM.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.Appraisals_A1 S ON (PM.BusinessUnitID = S.BusinessUnitID AND PM.ThirdPartyID = S.ThirdPartyID and PM.ThirdPartyCategoryID = S.ThirdPartyCategoryID)
JOIN	dbo.VehicleLine VL ON S.MakeModelGroupingID = VL.MakeModelGroupingID AND VL.VehicleMakeID = PM.VehicleMakeID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	S.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	(S.BookoutValue - S.AppraisalValue) >= P.ValuationDiscrepancyThresholdRule1
GO

-- SELECT * FROM GetAppraisalReviews_AppraisalToBookValue WHERE BusinessUnitID = 100215 -- < 1s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_AppraisalToNAAAValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_AppraisalToNAAAValue]
GO

CREATE VIEW dbo.GetAppraisalReviews_AppraisalToNAAAValue(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT DISTINCT
	P.BusinessUnitID,
	S.AppraisalID,
	0 Rule1, 1 Rule2, 0 Rule3, 0 Rule4, 0 Rule5, 0 Rule6,
	S.AppraisalValue,
	S.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	A.AverageSalePrice NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	S.DateCreated
FROM
	dbo.Appraisals_A1 S
JOIN	dbo.AppraisalReviewPreferences P ON S.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.MileageRange MR ON
	(
			MR.LowMileageRange = ROUND(S.Mileage,-4)
		AND	MR.HighMileageRange = ROUND(S.Mileage,-4)+9999
	)
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.Auction_A1 A ON
	(
			A.MakeModelGroupingID = S.MakeModelGroupingID
		AND	A.ModelYear           = S.ModelYear
		AND	A.AreaID              = DP.AuctionAreaID
		AND	A.PeriodID            = 12
		AND	A.MileageRangeID      = MR.MileageRangeID
	)
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	(A.AverageSalePrice - S.AppraisalValue) >= 1500
AND	S.DateCreated >= DATEADD(DD,-31,UC.RunDate)
GO

-- SELECT * FROM dbo.GetAppraisalReviews_AppraisalToNAAAValue WHERE BusinessUnitID = 100215 -- < 1s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_BookToNADALoanValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_BookToNADALoanValue]
GO

CREATE VIEW dbo.GetAppraisalReviews_BookToNADALoanValue(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT
	P.BusinessUnitID,
	Book.AppraisalID,
	0, 0, 1, 0, 0, 0,
	Book.AppraisalValue,
	Book.CustomerOffer,
	Book.BookoutValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NADA.BookoutValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	NADA.DateCreated
FROM
	dbo.Appraisals_A1 NADA
JOIN	dbo.Appraisals_A1 Book ON NADA.AppraisalID = Book.AppraisalID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = NADA.BusinessUnitID
JOIN	dbo.AppraisalReviewPreferences P ON NADA.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	NADA.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	NADA.ThirdPartyID = 2
AND	NADA.ThirdPartyCategoryID = 3
AND	Book.ThirdPartyID <> 2
AND	Book.ThirdPartyCategoryID = CASE WHEN DP.GuideBookID = Book.ThirdPartyID THEN DP.BookOutPreferenceId ELSE DP.GuideBook2BookOutPreferenceId END
AND	(NADA.BookoutValue - Book.BookoutValue) >= P.ValuationDiscrepancyThresholdRule3
GO

-- SELECT * FROM GetAppraisalReviews_BookToNADALoanValue WHERE BusinessUnitID = 100215 -- < 1s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_Understocked]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_Understocked]
GO

CREATE VIEW dbo.GetAppraisalReviews_Understocked (
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT DISTINCT
	BU.BusinessUnitID,
	A.AppraisalID,
	0, 0, 0,
	CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN 1 ELSE 0 END,
	CASE WHEN BU.BusinessUnitID <> BU2.BusinessUnitID THEN 1 ELSE 0 END,
	0,
	A.AppraisalValue,
	A.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	I4.StockedUnitsModelYear,
	I4.StockedUnitsModel,
	I4.OptimalUnitsModelYear,
	I4.OptimalUnitsModel,
	CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN NULL ELSE BU2.BusinessUnitID END,
	A.DateCreated
FROM
	dbo.Appraisals_A1 A
JOIN	dbo.AppraisalReviewPreferences P ON A.BusinessUnitID = P.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit BU on A.BusinessUnitID = BU.BusinessUnitID
JOIN	dbo.ZipCode ZC ON BU.ZipCode = ZC.ZipCode
JOIN	[IMT].dbo.BusinessUnitRelationship BR1 on BU.BusinessUnitID = BR1.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnitRelationship BR2 on BR1.ParentID = BR2.ParentID
JOIN	[IMT].dbo.BusinessUnit BU2 on BR2.BusinessUnitID = BU2.BusinessUnitID
JOIN	dbo.ZipCode ZC2 ON BU2.ZipCode = ZC2.ZipCode
JOIN	[IMT].dbo.tbl_MakeModelGrouping MMG ON A.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN	[IMT].dbo.tbl_GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID
JOIN	dbo.Inventory_A4 I4 ON I4.BusinessUnitID = BU2.BusinessUnitID AND I4.GroupingDescriptionID = GD.GroupingDescriptionID AND COALESCE(I4.ModelYear,A.ModelYear) = A.ModelYear
LEFT JOIN dbo.ZipCode_A1 ZCD1 ON (ZC.ZipCodeID = ZCD1.ZipCodeID1 AND ZC2.ZipCodeID = ZCD1.ZipCodeID2)
LEFT JOIN dbo.ZipCode_A1 ZCD2 ON (ZC.ZipCodeID = ZCD2.ZipCodeID2 AND ZC2.ZipCodeID = ZCD2.ZipCodeID1)
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
JOIN	[IMT].dbo.DealerRisk R1 ON BU.BusinessUnitID = R1.BusinessUnitID
JOIN	[IMT].dbo.DealerRisk R2 ON BU2.BusinessUnitID = R2.BusinessUnitID
WHERE
	A.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	I4.CIACategoryID IN (1,2,3)
AND	COALESCE(ZCD1.Distance,ZCD2.Distance,0) < P.MaxDistanceFromDealer
AND	A.Mileage < CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN R1.HighMileageOverall ELSE R2.HighMileageOverall END
AND	CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN 1 ELSE COALESCE((OptimalUnitsModelYear - StockedUnitsModelYear), (OptimalUnitsModel - StockedUnitsModel), 0) END > 0
GO

-- SELECT * FROM dbo.GetAppraisalReviews_Understocked WHERE BusinessUnitID = 100215 -- 1s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_LowOffer]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_LowOffer]
GO

CREATE VIEW dbo.GetAppraisalReviews_LowOffer(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT DISTINCT
	A.BusinessUnitID,
	A.AppraisalID,
	0, 0, 0, 0, 0, 1,
	A.AppraisalValue,
	A.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	A.DateCreated
FROM
	dbo.Appraisals_A1 A
JOIN	dbo.AppraisalReviewPreferences P ON A.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	A.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	(A.AppraisalValue - COALESCE(NULLIF(A.CustomerOffer,0),A.AppraisalValue)) >= P.ValuationDiscrepancyThresholdRule6
GO

-- SELECT * FROM dbo.GetAppraisalReviews_LowOffer WHERE BusinessUnitID = 100215 -- < 1s

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews]
GO

CREATE VIEW dbo.GetAppraisalReviews (
	BusinessUnitID,
	AppraisalID,
	Rule1,
	Rule2,
	Rule3,
	Rule4,
	Rule5,
	Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	AppraisalDate
)
AS
SELECT
	BusinessUnitID,
	AppraisalID,
	CASE WHEN SUM(CAST(Rule1 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule2 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule3 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule4 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule5 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule6 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	MAX(COALESCE(AppraisalValue,0)),
	MAX(COALESCE(CustomerOffer,0)),
	MAX(COALESCE(BookValue,0)),
	MAX(BookThirdPartyID),
	MAX(BookThirdPartyCategoryID),
	MAX(COALESCE(NAAAValue,0)),
	MAX(COALESCE(NADAValue,0)),
	MAX(COALESCE(StockedUnitsModelYear,0)),
	MAX(COALESCE(StockedUnitsModel,0)),
	MAX(COALESCE(OptimalUnitsModelYear,0)),
	MAX(COALESCE(OptimalUnitsModel,0)),
	MAX(COALESCE(AppraisalDate,getDate()))
FROM
(
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_AppraisalToBookValue
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_AppraisalToNAAAValue
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_BookToNADALoanValue
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_Understocked
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_LowOffer
) TMP
GROUP BY
	BusinessUnitID,
	AppraisalID
GO

-- SELECT * FROM dbo.GetAppraisalReviews WHERE BusinessUnitID = 100147

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviewBusinessUnits]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviewBusinessUnits]
GO

CREATE VIEW dbo.GetAppraisalReviewBusinessUnits (
	AppraisalID,
	BusinessUnitID,
	TargetBusinessUnitID
)
AS
SELECT
	AppraisalID,
	BusinessUnitID,
	TargetBusinessUnitID
FROM
	dbo.GetAppraisalReviews_Understocked
WHERE
	TargetBusinessUnitID IS NOT NULL
GO

-- SELECT * FROM dbo.GetAppraisalReviewBusinessUnits

--
-- Textual Description of the six Appraisal Review Rules
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewRules]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewRules
GO

CREATE TABLE dbo.AppraisalReviewRules (
	AppraisalReviewRuleID INT NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_AppraisalReviewRule PRIMARY KEY (
		AppraisalReviewRuleID
	)
)
GO

INSERT INTO AppraisalReviewRules (AppraisalReviewRuleID, [Name]) VALUES (1, 'Below Primary Book Value')
INSERT INTO AppraisalReviewRules (AppraisalReviewRuleID, [Name]) VALUES (2, 'Below NAAA Value')
INSERT INTO AppraisalReviewRules (AppraisalReviewRuleID, [Name]) VALUES (3, 'Below NADA Value')
INSERT INTO AppraisalReviewRules (AppraisalReviewRuleID, [Name]) VALUES (4, 'Understocked')
INSERT INTO AppraisalReviewRules (AppraisalReviewRuleID, [Name]) VALUES (5, 'Understocked at Other Dealer')
INSERT INTO AppraisalReviewRules (AppraisalReviewRuleID, [Name]) VALUES (6, 'Low Customer Offer')
GO

--
-- This table will need to be partitioned by month.  Every day about 12k rows will be added to
-- the table and we will need at most two months of data. I have tried to cluster this table but
-- SQL Server 2000 has major issues with bit columns. Let us hope this is fixed in 2005. SBW
--

--
-- Table design notes.  The primary key does not include the partition key (MonthID) as I am not
-- doing the partitioning.  However, the table does include the DayID as part of the primary key
-- so when we look for review items on any given day we can index scan. When partitioning is
-- implemented ther month will also be part of the primary key (albeit redundantly). SBW
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviews]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviews
GO

CREATE TABLE dbo.AppraisalReviews (
	[AppraisalID]              [int] NOT NULL ,
	[BusinessUnitID]           [int] NOT NULL ,
	[VehicleID]                [int] NOT NULL ,
	[MonthID]                  [int] NOT NULL ,
	[DayID]                    [int] NOT NULL ,
	[Rule1]                    [bit] NOT NULL ,
	[Rule2]                    [bit] NOT NULL ,
	[Rule3]                    [bit] NOT NULL ,
	[Rule4]                    [bit] NOT NULL ,
	[Rule5]                    [bit] NOT NULL ,
	[Rule6]                    [bit] NOT NULL ,
	[AppraisalValue]           [int] NULL ,
	[CustomerOffer]            [int] NULL ,
	[BookValue]                [int] NULL ,
	[BookThirdPartyID]         [int] NULL ,
	[BookThirdPartyCategoryID] [int] NULL ,
	[NAAAValue]                [int] NULL ,
	[NADAValue]                [int] NULL ,
	[StockedUnitsModelYear]    [int] NOT NULL ,
	[StockedUnitsModel]        [int] NOT NULL ,
	[OptimalUnitsModelYear]    [int] NULL ,
	[OptimalUnitsModel]        [int] NULL ,
	[InventoryVehicleLightID]  [int] NOT NULL,
	[Mileage]                  [int] NOT NULL,
	[Display]                  [bit] NOT NULL,
	[FollowUp]                 [bit] NOT NULL,
	[AppraisalDate]            [datetime] NOT NULL ,
	CONSTRAINT PK_AppraisalReview PRIMARY KEY CLUSTERED (
		DayID,
		BusinessUnitID,
		AppraisalID
	)
)
GO

--
-- Join from the an appraisal on a given day to business units who may be interested in taking the
-- vehicle into inventory. Again, this table needs to be partitioned much like AppraisalReviews. SBW
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewBusinessUnits]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBusinessUnits
GO

CREATE TABLE dbo.AppraisalReviewBusinessUnits (
	[AppraisalID]              [int] NOT NULL ,
	[BusinessUnitID]           [int] NOT NULL ,
	[TargetBusinessUnitID]     [int] NOT NULL ,
	[MonthID]                  [int] NOT NULL ,
	[DayID]                    [int] NOT NULL ,	
	CONSTRAINT PK_AppraisalReviewBusinessUnit PRIMARY KEY CLUSTERED (
		DayID,
		BusinessUnitID,
		AppraisalID,
		TargetBusinessUnitID
	)
)
GO

--
-- Insert or update the AppraisalReviews table with the most recent data.
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadAppraisalReviews]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadAppraisalReviews]
GO

CREATE PROCEDURE dbo.LoadAppraisalReviews
	@BusinessUnitID INT = NULL
AS
	-- collect the partition data
	DECLARE @MonthID INT, @DayID INT, @YesterdayID INT
	SET SELECT @MonthID     = CAST(CONVERT(CHAR(6), GETDATE(), 112) AS INT)
	SET SELECT @DayID       = CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT)
	SET SELECT @YesterdayID = CAST(CONVERT(CHAR(8), DATEADD(DD, -1, GETDATE()), 112) AS INT)
	
	-- collect the reviews
	CREATE TABLE #T_AppraisalReviews (
		AppraisalID              INT NOT NULL,
		BusinessUnitID           INT NOT NULL,
		VehicleID                INT NULL,
		Rule1                    BIT NOT NULL,
		Rule2                    BIT NOT NULL,
		Rule3                    BIT NOT NULL,
		Rule4                    BIT NOT NULL,
		Rule5                    BIT NOT NULL,
		Rule6                    BIT NOT NULL,
		AppraisalValue           INT NULL,
		CustomerOffer            INT NULL,
		BookValue                INT NULL,
		BookThirdPartyID         INT NULL,
		BookThirdPartyCategoryID INT NULL,
		NAAAValue                INT NULL,
		NADAValue                INT NULL,
		StockedUnitsModelYear    INT NOT NULL,
		StockedUnitsModel        INT NOT NULL,
		OptimalUnitsModelYear    INT NULL,
		OptimalUnitsModel        INT NULL,
		AppraisalDate            DATETIME NOT NULL,
		InventoryVehicleLightID  INT NULL,
		Mileage                  INT NULL,
		Display                  BIT NOT NULL,
		FollowUp                 BIT NOT NULL,
		IsInsert                 BIT NULL
	)
	
	CREATE TABLE #T_AppraisalReviewBusinessUnits (
		AppraisalID              INT NOT NULL,
		BusinessUnitID           INT NOT NULL,
		TargetBusinessUnitID     INT NOT NULL
	)
	
	INSERT INTO #T_AppraisalReviews (
		AppraisalID,
		BusinessUnitID,
		Rule1,
		Rule2,
		Rule3,
		Rule4,
		Rule5,
		Rule6,
		AppraisalValue,
		CustomerOffer,
		BookValue,
		BookThirdPartyID,
		BookThirdPartyCategoryID,
		NAAAValue,
		NADAValue,
		StockedUnitsModelYear,
		StockedUnitsModel,
		OptimalUnitsModelYear,
		OptimalUnitsModel,
		AppraisalDate,
		Display,
		FollowUp,
		IsInsert
	)
	SELECT
		AppraisalID,
		BusinessUnitID,
		Rule1,
		Rule2,
		Rule3,
		Rule4,
		Rule5,
		Rule6,
		AppraisalValue,
		CustomerOffer,
		BookValue,
		BookThirdPartyID,
		BookThirdPartyCategoryID,
		NAAAValue,
		NADAValue,
		StockedUnitsModelYear,
		StockedUnitsModel,
		OptimalUnitsModelYear,
		OptimalUnitsModel,
		AppraisalDate,
		1,
		0,
		1
	FROM
		GetAppraisalReviews
	WHERE
		COALESCE(@BusinessUnitID,BusinessUnitID) = BusinessUnitID
	
	IF @@ERROR <> 0 GOTO Failed
	
	INSERT INTO #T_AppraisalReviewBusinessUnits (
		AppraisalID,
		BusinessUnitID,
		TargetBusinessUnitID
	)
	SELECT
		AppraisalID,
		BusinessUnitID,
		TargetBusinessUnitID
	FROM
		GetAppraisalReviewBusinessUnits
	WHERE
		COALESCE(@BusinessUnitID,BusinessUnitID) = BusinessUnitID
	
	IF @@ERROR <> 0 GOTO Failed

	-- get the vehicle id
	UPDATE	T
	SET	T.VehicleID = A.VehicleID, T.Mileage = A.Mileage
	FROM	#T_AppraisalReviews T
	JOIN	[IMT].dbo.Appraisals A ON A.AppraisalID = T.AppraisalID

	IF @@ERROR <> 0 GOTO Failed

	-- delete all appraisals without a vehicle
	DELETE	T
	FROM	#T_AppraisalReviews T
	WHERE	T.VehicleID IS NULL

	IF @@ERROR <> 0 GOTO Failed

	-- get the inventory vehicle light id
	UPDATE	T
	SET	T.InventoryVehicleLightID = GL.InventoryVehicleLightID
	FROM	#T_AppraisalReviews T
	JOIN	[IMT].dbo.tbl_Vehicle V ON V.VehicleID = T.VehicleID
	JOIN	[IMT].dbo.tbl_MakeModelGrouping M ON V.MakeModelGroupingID = M.MakeModelGroupingID
	JOIN	[IMT].dbo.tbl_GroupingDescription G ON G.GroupingDescriptionID = M.GroupingDescriptionID
	JOIN	[IMT].dbo.GDLight GL ON GL.GroupingDescriptionID = G.GroupingDescriptionID AND T.BusinessUnitID = GL.BusinessUnitID

	IF @@ERROR <> 0 GOTO Failed

	SELECT @@ROWCOUNT AS NumberOfInventoryLightsApplied
	SELECT COUNT(*) AS NumberOfInventoryLightsNull FROM #T_AppraisalReviews WHERE InventoryVehicleLightID IS NULL

	-- mark the updates
	UPDATE	T
	SET	T.IsInsert = 0
	FROM	#T_AppraisalReviews T
	JOIN	dbo.AppraisalReviews A ON A.AppraisalID = T.AppraisalID
	WHERE	A.MonthID = @MonthID
	AND	A.DayID   = @DayID

	IF @@ERROR <> 0 GOTO Failed

	-- carry display and follow up from yesterday
	CREATE TABLE #LastState (
		AppraisalID INT NOT NULL,
		Display     BIT NOT NULL,
		FollowUp    BIT NOT NULL
	)
	
	INSERT INTO #LastState (
		AppraisalID,
		Display,
		FollowUp
	)
	SELECT
		A.AppraisalID,
		A.Display,
		A.FollowUp
	FROM
		#T_AppraisalReviews T
	JOIN	dbo.AppraisalReviews A ON A.AppraisalID = T.AppraisalID
	JOIN	(
			SELECT
				AppraisalID,
				MAX(DayID) DayID
			FROM
				dbo.AppraisalReviews
			WHERE
				DayID < @DayID
			GROUP BY
				AppraisalID
		) L ON A.AppraisalID = L.AppraisalID
	WHERE
		T.IsInsert = 1

	UPDATE	T
	SET	T.Display = L.Display,
		T.FollowUp = L.FollowUp
	FROM	#T_AppraisalReviews T
	JOIN	#LastState L ON L.AppraisalID = T.AppraisalID

	BEGIN TRANSACTION T0

	-- insert new review items
	DECLARE @RowsInserted INT

	INSERT INTO AppraisalReviews (
		AppraisalID,
		BusinessUnitID,
		VehicleID,
		MonthID,
		DayID,
		Rule1,
		Rule2,
		Rule3,
		Rule4,
		Rule5,
		Rule6,
		AppraisalValue,
		CustomerOffer,
		BookValue,
		BookThirdPartyID,
		BookThirdPartyCategoryID,
		NAAAValue,
		NADAValue,
		StockedUnitsModelYear,
		StockedUnitsModel,
		OptimalUnitsModelYear,
		OptimalUnitsModel,
		InventoryVehicleLightID,
		Mileage,
		Display,
		FollowUp,
		AppraisalDate
	)
	SELECT
		AppraisalID,
		BusinessUnitID,
		VehicleID,
		@MonthID,
		@DayID,
		Rule1,
		Rule2,
		Rule3,
		Rule4,
		Rule5,
		Rule6,
		AppraisalValue,
		CustomerOffer,
		BookValue,
		BookThirdPartyID,
		BookThirdPartyCategoryID,
		NAAAValue,
		NADAValue,
		StockedUnitsModelYear,
		StockedUnitsModel,
		OptimalUnitsModelYear,
		OptimalUnitsModel,
		COALESCE(InventoryVehicleLightID,0),
		COALESCE(Mileage,0),
		Display,
		FollowUp,
		AppraisalDate
	FROM
		#T_AppraisalReviews A
	WHERE
		A.IsInsert = 1
	
	IF @@ERROR <> 0 GOTO Failed
	
	SET @RowsInserted = @@ROWCOUNT
	
	-- update existing reviews
	DECLARE @RowsUpdated INT

	UPDATE	AR
	SET	AR.Rule1                     = T.Rule1,
		AR.Rule2                     = T.Rule2,
		AR.Rule3                     = T.Rule3,
		AR.Rule4                     = T.Rule4,
		AR.Rule5                     = T.Rule5,
		AR.Rule6                     = T.Rule6,
		AR.AppraisalValue            = T.AppraisalValue,
		AR.CustomerOffer             = T.CustomerOffer,
		AR.BookValue                 = T.BookValue,
		AR.BookThirdPartyID          = T.BookThirdPartyID,
		AR.BookThirdPartyCategoryID  = T.BookThirdPartyCategoryID,
		AR.NAAAValue                 = T.NAAAValue,
		AR.NADAValue                 = T.NADAValue,
		AR.StockedUnitsModelYear     = T.StockedUnitsModelYear,
		AR.StockedUnitsModel         = T.StockedUnitsModel,
		AR.OptimalUnitsModelYear     = T.OptimalUnitsModelYear,
		AR.OptimalUnitsModel         = T.OptimalUnitsModel,
		AR.InventoryVehicleLightID   = COALESCE(T.InventoryVehicleLightID,0),
		AR.Mileage                   = COALESCE(T.Mileage,0)
	FROM	AppraisalReviews AR
	JOIN	#T_AppraisalReviews T ON T.AppraisalID = AR.AppraisalID AND T.BusinessUnitID = AR.BusinessUnitID
	WHERE	AR.MonthID = @MonthID
	AND	AR.DayID = @DayID
	AND	T.IsInsert = 0

	IF @@ERROR <> 0 GOTO Failed
	
	SET @RowsUpdated = @@ROWCOUNT
	
	-- update join table
	DECLARE @RowsDeleted INT
	
	DELETE	ARBU
	FROM	AppraisalReviewBusinessUnits ARBU LEFT JOIN #T_AppraisalReviewBusinessUnits T ON (
				ARBU.AppraisalID          = T.AppraisalID
			AND	ARBU.BusinessUnitID       = T.BusinessUnitID
			AND	ARBU.TargetBusinessUnitID = T.TargetBusinessUnitID
		)
	WHERE	ARBU.DayID = @DayID
	AND	ARBU.BusinessUnitID = COALESCE(@BusinessUnitID, ARBU.BusinessUnitID)
	AND	T.AppraisalID IS NULL

	IF @@ERROR <> 0 GOTO Failed

	SET @RowsDeleted = @@ROWCOUNT
	
	DECLARE @RowsJoined INT
	
	INSERT INTO AppraisalReviewBusinessUnits (
		AppraisalID,
		BusinessUnitID,
		TargetBusinessUnitID,
		MonthID,
		DayID
	)
	SELECT
		J.AppraisalID,
		J.BusinessUnitID,
		J.TargetBusinessUnitID,
		@MonthID,
		@DayID
	FROM
		#T_AppraisalReviewBusinessUnits J
	WHERE
		NOT EXISTS (
			SELECT	1
			FROM	dbo.AppraisalReviewBusinessUnits X
			WHERE	X.AppraisalID          = J.AppraisalID
			AND	X.BusinessUnitID       = J.BusinessUnitID
			AND	X.TargetBusinessUnitID = J.TargetBusinessUnitID
			AND	X.DayID                = @DayID
		)

	IF @@ERROR <> 0 GOTO Failed

	SET @RowsJoined = @@ROWCOUNT

	COMMIT TRANSACTION T0

	SELECT COUNT(*) NumberOrAppraisalReviews FROM #T_AppraisalReviews
	SELECT COUNT(*) NumberOrAppraisalReviewBusinessUnits FROM #T_AppraisalReviewBusinessUnits
	SELECT @RowsInserted RowsInserted, @RowsUpdated RowsUpdated, @RowsDeleted RowsDeleted, @RowsJoined RowsJoined

	DROP TABLE #T_AppraisalReviews
	DROP TABLE #T_AppraisalReviewBusinessUnits
	DROP TABLE #LastState

	RETURN 0

Failed:

	SELECT cast(@@ERROR AS VARCHAR)

	ROLLBACK TRANSACTION T0

	RETURN(666)

GO

-- delete from appraisalreviews
-- delete from appraisalreviewbusinessunits

EXEC dbo.LoadAppraisalReviews -- 1m 52s
-- EXECUTE dbo.LoadAppraisalReviews 100215 -- 7s

--
-- An enumeration of the terminal states of the current appraisal review follow up workflow.
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewFollowUpActions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewFollowUpActions
GO

CREATE TABLE dbo.AppraisalReviewFollowUpActions (
	AppraisalReviewFollowUpActionID INT NOT NULL,
	[Name] VARCHAR(50) NOT NULL,
	CONSTRAINT PK_AppraisalReviewFollowUpAction PRIMARY KEY CLUSTERED (
		AppraisalReviewFollowUpActionID
	)
)
GO

INSERT INTO dbo.AppraisalReviewFollowUpActions (AppraisalReviewFollowUpActionID, [Name]) VALUES (1, 'Traded-In')
INSERT INTO dbo.AppraisalReviewFollowUpActions (AppraisalReviewFollowUpActionID, [Name]) VALUES (2, 'No Deal')
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewFollowUps]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewFollowUps
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalReviewFollowUpLists]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewFollowUpLists
GO

--
-- Grouping entity for a collection of appraisal review follow up items.
--

CREATE TABLE dbo.AppraisalReviewFollowUpLists (
	AppraisalReviewFollowUpListID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                INT      NOT NULL,
	DateCreated                   DATETIME NOT NULL,
	LastModified                  DATETIME NOT NULL,
	Active                        BIT      NOT NULL,
	CONSTRAINT PK_AppraisalReviewFollowUpList PRIMARY KEY (
		AppraisalReviewFollowUpListID
	),
	CONSTRAINT UK_AppraisalReviewFollowUpList UNIQUE (
		BusinessUnitID,
		Active
	)
)
GO

--
-- Notes for an appraisal review marked for customer follow up.
--

CREATE TABLE dbo.AppraisalReviewFollowUps (
	AppraisalReviewFollowUpID       INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                  INT          NOT NULL,
	AppraisalID                     INT          NOT NULL,
	DayID                           INT          NOT NULL,
	Notes                           VARCHAR(500) NULL,
	AppraisalReviewFollowUpListID   INT          NOT NULL,
	AppraisalReviewFollowUpActionID INT          NULL,
	CONSTRAINT PK_AppraisalReviewFollowUp PRIMARY KEY (
		AppraisalReviewFollowUpID
	),
	CONSTRAINT FK_AppraisalReviewFollowUp_AppraisalReviewFollowUpAction FOREIGN KEY (
		AppraisalReviewFollowUpActionID
	)
	REFERENCES AppraisalReviewFollowUpActions (
		AppraisalReviewFollowUpActionID
	),
	CONSTRAINT FK_AppraisalReviewFollowUp_AppraisalReviewFollowUpList FOREIGN KEY (
		AppraisalReviewFollowUpListID
	)
	REFERENCES AppraisalReviewFollowUpLists (
		AppraisalReviewFollowUpListID
	),
	CONSTRAINT UK_AppraisalReviewFollowUp UNIQUE (
		AppraisalID,
		AppraisalReviewFollowUpListID
	)
)
GO

--
-- Ruby Style Views
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_review_preferences]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_review_preferences]
GO

CREATE VIEW dbo.appraisal_review_preferences (
	[id],
	[business_unit_id],
	[days_back],
	[max_distance],
	[rule_1_threshold],
	[rule_2_threshold],
	[rule_3_threshold],
	[rule_6_threshold],
	[rule_1_enabled],
	[rule_2_enabled],
	[rule_3_enabled],
	[rule_4_enabled],
	[rule_5_enabled],
	[rule_6_enabled],
	[closing_rate_target]
)
AS
SELECT
	AppraisalReviewPreferenceID,
	BusinessUnitID,
	AppraisalCreatedDaysThreshold,
	MaxDistanceFromDealer,
	ValuationDiscrepancyThresholdRule1,
	ValuationDiscrepancyThresholdRule2,
	ValuationDiscrepancyThresholdRule3,
	ValuationDiscrepancyThresholdRule6,
	EnabledRule1,
	EnabledRule2,
	EnabledRule3,
	EnabledRule4,
	EnabledRule5,
	EnabledRule6,
	ClosingRateTarget
FROM
	dbo.AppraisalReviewPreferences
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_review_bookout_preferences]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_review_bookout_preferences]
GO

CREATE VIEW dbo.appraisal_review_bookout_preferences (
	[id],
	[business_unit_id],
	[vehicle_make_id],
	[book_id],
	[book_category_id]
)
AS
SELECT
	AppraisalReviewBookoutPreferenceID,
	BusinessUnitID,
	VehicleMakeID,
	ThirdPartyID,
	ThirdPartyCategoryID
FROM
	dbo.AppraisalReviewBookoutPreferences
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_reviews]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_reviews]
GO

CREATE VIEW dbo.appraisal_reviews (
	[id],
	[appraisal_id],
	[business_unit_id],
	[vehicle_id],
	[month_id],
	[day_id],
	[appraisal_date],
	[rule_1],
	[rule_2],
	[rule_3],
	[rule_4],
	[rule_5],
	[rule_6],
	[appraisal_value],
	[customer_offer],
	[book_id],
	[book_category_id],
	[book_value],
	[naaa_value],
	[nada_loan_value],
	[stocked_units_model_year],
	[stocked_units_model],
	[optimal_units_model_year],
	[optimal_units_model],
	[vehicle_inventory_light_id],
	[vehicle_mileage],
	[display],
	[follow_up],
	[has_enabled_rule_matches]
)
AS
SELECT 
	CAST(AR.AppraisalID AS VARCHAR) + '_' + CAST(AR.DayID AS VARCHAR),
	AR.AppraisalID,
	AR.BusinessUnitID,
	AR.VehicleID,
	AR.MonthID,
	AR.DayID,
	AR.AppraisalDate,
	CASE WHEN P.EnabledRule1 = 1 THEN AR.Rule1 ELSE 0 END,
	CASE WHEN P.EnabledRule2 = 1 THEN AR.Rule2 ELSE 0 END,
	CASE WHEN P.EnabledRule3 = 1 THEN AR.Rule3 ELSE 0 END,
	CASE WHEN P.EnabledRule4 = 1 THEN AR.Rule4 ELSE 0 END,
	CASE WHEN P.EnabledRule5 = 1 THEN AR.Rule5 ELSE 0 END,
	CASE WHEN P.EnabledRule6 = 1 THEN AR.Rule6 ELSE 0 END,
	AR.AppraisalValue,
	AR.CustomerOffer,
	AR.BookThirdPartyID,
	AR.BookThirdPartyCategoryID,
	AR.BookValue,
	AR.NAAAValue,
	AR.NADAValue,
	AR.StockedUnitsModelYear,
	AR.StockedUnitsModel,
	AR.OptimalUnitsModelYear,
	AR.OptimalUnitsModel,
	AR.InventoryVehicleLightID,
	AR.Mileage,
	AR.Display,
	AR.FollowUp,
	CAST(COALESCE(
		CASE WHEN P.EnabledRule1 = 1 THEN NULLIF(AR.Rule1,0) ELSE NULL END,
		CASE WHEN P.EnabledRule2 = 1 THEN NULLIF(AR.Rule2,0) ELSE NULL END,
		CASE WHEN P.EnabledRule3 = 1 THEN NULLIF(AR.Rule3,0) ELSE NULL END,
		CASE WHEN P.EnabledRule4 = 1 THEN NULLIF(AR.Rule4,0) ELSE NULL END,
		CASE WHEN P.EnabledRule5 = 1 THEN NULLIF(AR.Rule5,0) ELSE NULL END,
		CASE WHEN P.EnabledRule6 = 1 THEN NULLIF(AR.Rule6,0) ELSE NULL END,
		0
	) AS BIT)
FROM
	dbo.AppraisalReviews AR
JOIN	dbo.AppraisalReviewPreferences P ON AR.BusinessUnitID = P.BusinessUnitID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_review_business_units]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_review_business_units]
GO

CREATE VIEW dbo.appraisal_review_business_units (
	[id],
	[appraisal_id],
	[business_unit_id],
	[target_business_unit_id],
	[month_id],
	[day_id]
)
AS
SELECT
	CAST(AppraisalID AS VARCHAR) + '_' + CAST(DayID AS VARCHAR) + '_' + CAST(TargetBusinessUnitID AS VARCHAR),
	AppraisalID,
	BusinessUnitID,
	TargetBusinessUnitID,
	MonthID,
	DayID
FROM
	dbo.AppraisalReviewBusinessUnits
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_review_follow_up_actions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_review_follow_up_actions]
GO

CREATE VIEW dbo.appraisal_review_follow_up_actions (
	[id],
	[name]
)
AS
SELECT 
	[AppraisalReviewFollowUpActionID],
	[Name]
FROM
	dbo.AppraisalReviewFollowUpActions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_review_follow_up_lists]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_review_follow_up_lists]
GO

CREATE VIEW dbo.appraisal_review_follow_up_lists (
	[id],
	[business_unit_id],
	[date_created],
	[last_modified],
	[active]
)
AS
SELECT 
	[AppraisalReviewFollowUpListID],
	[BusinessUnitID],
	[DateCreated],
	[LastModified],
	[Active]
FROM
	dbo.AppraisalReviewFollowUpLists
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_review_follow_ups]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_review_follow_ups]
GO

CREATE VIEW dbo.appraisal_review_follow_ups (
	[id],
	[business_unit_id],
	[appraisal_id],
	[day_id],
	[notes],
	[appraisal_review_follow_up_list_id],
	[appraisal_review_follow_up_action_id]
)
AS
SELECT 
	AppraisalReviewFollowUpID,
	BusinessUnitID,
	AppraisalID,
	DayID,
	Notes,
	AppraisalReviewFollowUpListID,
	AppraisalReviewFollowUpActionID
FROM
	dbo.AppraisalReviewFollowUps
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisal_reviews_days_back_report]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisal_reviews_days_back_report]
GO

CREATE VIEW dbo.appraisal_reviews_days_back_report (
	business_unit_id,
	days_back,
	number_of_appraisal_reviews,
	number_of_appraisals
)
AS
SELECT
	BusinessUnitID,
	DaysBack,
	SUM(NumberOfPotentialDeals) NumberOfAppraisalReviews,
	SUM(NumberOfAppraisals) NumberOfAppraisals
FROM
(
	SELECT
		B.id BusinessUnitID,
		DB.Days DaysBack,
		SUM(CASE WHEN DATEDIFF(DD, [IMT].dbo.ToDate(AR.appraisal_date), [IMT].dbo.ToDate(GETDATE())) <= DB.Days THEN 1 ELSE 0 END) NumberOfPotentialDeals,
		0 NumberOfAppraisals
	FROM
		dbo.DaysBack DB CROSS JOIN dbo.business_units B LEFT JOIN dbo.appraisal_reviews AR ON B.id = AR.business_unit_id
	WHERE
		AR.day_id = convert(CHAR(8), GETDATE(), 112)
	AND	AR.display = 1 
	AND	AR.has_enabled_rule_matches = 1
	GROUP BY
		B.id, DB.days
UNION ALL
	SELECT
		B.BusinessUnitID,
		DB.Days DaysBack,
		0 NumberOfPotentialDeals,
		SUM(CASE WHEN DATEDIFF(DD, [IMT].dbo.ToDate(A3.DateCreated), [IMT].dbo.ToDate(GETDATE())) <= DB.Days THEN 1 ELSE 0 END) NumberOfAppraisals
	FROM
		dbo.DaysBack DB CROSS JOIN BusinessUnit B LEFT JOIN dbo.Appraisals_A3 A3 ON B.BusinessUnitID = A3.BusinessUnitID
	GROUP BY
		B.BusinessUnitID, DB.days
) T
GROUP BY
	BusinessUnitID,
	DaysBack
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[appraisals_waiting_report]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[appraisals_waiting_report]
GO

CREATE VIEW dbo.appraisals_waiting_report (
	business_unit_id,
	number_of_appraisals_waiting
)
AS
SELECT
	B.BusinessUnitID,
	COALESCE(T.NumberOfAppraisalsWaiting,0) NumberOfAppraisalsWaiting
FROM
	[IMT].dbo.BusinessUnit B LEFT JOIN (
		SELECT
			A.BusinessUnitID,
			COUNT(*) NumberOfAppraisalsWaiting
		FROM
			[IMT].dbo.Appraisals A
		JOIN	[IMT].dbo.DealerPreference P ON A.BusinessUnitID = P.BusinessUnitID
		JOIN	[IMT].dbo.AppraisalActions AA ON A.AppraisalID = AA.AppraisalID
		JOIN	(
				SELECT	AppraisalID, MAX(AppraisalActionID) AppraisalActionID
				FROM	[IMT].dbo.AppraisalActions
				GROUP
				BY	AppraisalID
			) AX ON AX.AppraisalID = AA.AppraisalID AND AX.AppraisalActionID = AA.AppraisalActionID
		WHERE
			AA.AppraisalActionTypeID = 6
		AND	DATEDIFF(DD, dbo.ToDate(A.DateCreated), dbo.ToDate(GETDATE())) < P.TradeManagerDaysFilter
		GROUP BY
			A.BusinessUnitID
	) T ON T.BusinessUnitID = B.BusinessUnitID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[potential_trade_in_report]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[potential_trade_in_report]
GO

CREATE VIEW dbo.potential_trade_in_report (
	business_unit_id,
	number_of_potential_trade_ins
)
AS
SELECT
	B.BusinessUnitID,
	COALESCE(T.NumberOfPotentialTradeIns,0) NumberOfPotentialTradeIns
FROM
	[IMT].dbo.BusinessUnit B LEFT JOIN (
		SELECT
			AGD.BusinessUnitID,
			COUNT(*) NumberOfPotentialTradeIns
		FROM
			[IMT].dbo.Appraisals ax
		JOIN	[IMT].dbo.AppraisalBusinessUnitGroupDemand agd ON agd.AppraisalID = ax.AppraisalID
		JOIN	[IMT].dbo.dealerpreference dp on agd.businessunitid = dp.businessunitid
		JOIN	[IMT].dbo.AppraisalActions aa on ax.appraisalid = aa.appraisalid
		WHERE 
			DATEDIFF(DD, dbo.ToDate(aa.datecreated), dbo.ToDate(GETDATE())) <= dp.ShowroomDaysFilter
		GROUP BY
			AGD.BusinessUnitID
	) T ON T.BusinessUnitID = B.BusinessUnitID
GO

-- SELECT * FROM PotentialDealsDaysBackReport WHERE BusinessUnitID = 100147 ORDER BY DaysBack
-- SELECT * FROM appraisal_reviews_days_back_report WHERE business_unit_id = 100147 ORDER BY days_back

--
-- This is the code to partition the AppraisalReview table. It does not work. But it is a starting
-- point to see if it can work before we tidy the obvious flaws. SBW
--

-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TablePartitions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
-- DROP TABLE dbo.TablePartitions
-- GO
-- 
-- CREATE TABLE dbo.TablePartitions (
-- 	[TableName] [varchar] (100) NOT NULL ,
-- 	[Partition] [varchar] (100) NOT NULL ,
-- 	CONSTRAINT [PK_Partition] PRIMARY KEY CLUSTERED (
-- 		[TableName],
-- 		[Partition]
-- 	)
-- )
-- GO
-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CreateTablePartitionView]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
-- drop procedure [dbo].[CreateTablePartitionView]
-- GO
-- 
-- CREATE PROCEDURE dbo.CreateTablePartitionView
-- 	@TableName VARCHAR(100)
-- AS
-- 	DECLARE @Partition VARCHAR(100), @CMD VARCHAR(4000), @MSG VARCHAR(4000)
-- 	SET @CMD = ''
-- 
-- 	DECLARE table_partition_cursor CURSOR FOR
-- 	SELECT	Partition
-- 	FROM	TablePartitions
-- 	WHERE	TableName = @TableName
-- 
-- 	OPEN table_partition_cursor
-- 
-- 	FETCH NEXT FROM table_partition_cursor INTO @Partition
-- 
-- 	WHILE @@FETCH_STATUS = 0
-- 	BEGIN
-- 		IF LEN(@CMD) <> 0
-- 			SET @CMD = @CMD + ' UNION ALL '
-- 		SET @CMD = @CMD + 'SELECT * FROM dbo.' + @TableName + '_' + @Partition
-- 		FETCH NEXT FROM table_partition_cursor INTO @Partition
-- 	END
-- 	CLOSE table_partition_cursor
-- 	DEALLOCATE table_partition_cursor
-- 	
-- 	if exists (select * from dbo.sysobjects where id = object_id(N'' + @TableName) and OBJECTPROPERTY(id, N'IsView') = 1)
-- 	BEGIN
-- 		SET @MSG = 'DROP VIEW dbo.' + @TableName
-- 		EXECUTE (@MSG)
-- 	END
-- 
-- 	SET @MSG = 'CREATE VIEW dbo.' + @TableName + ' AS ' + @CMD
-- 	EXECUTE (@MSG)
-- GO
-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CreateAppraisalReviews]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
-- drop procedure [dbo].[CreateAppraisalReviews]
-- GO
-- 
-- CREATE PROCEDURE dbo.CreateAppraisalReviews
-- 	@MonthID CHAR(6)
-- AS
-- 	DECLARE @Prefix VARCHAR(100), @TableName VARCHAR(100)
-- 	SET @Prefix = 'AppraisalReviews'
-- 	SET @TableName = @Prefix + '_' + @MonthID
-- 
-- 	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].' + @TableName) and OBJECTPROPERTY(id, N'IsUserTable') = 1)
-- 	return
-- 	
-- 	DECLARE @CMD VARCHAR(6000)
-- 	
-- 	SET @CMD =
-- 'CREATE TABLE dbo.' + @TableName + ' (
-- 	AppraisalID              INT NOT NULL,
-- 	BusinessUnitID           INT NOT NULL,
-- 	VehicleID                INT NOT NULL,
-- 	MonthID                  INT NOT NULL,
-- 	DayID                    INT NOT NULL,
-- 	Rule1                    BIT NOT NULL,
-- 	Rule2                    BIT NOT NULL,
-- 	Rule3                    BIT NOT NULL,
-- 	Rule4                    BIT NOT NULL,
-- 	Rule5                    BIT NOT NULL,
-- 	Rule6                    BIT NOT NULL,
-- 	AppraisalValue           INT NULL,
-- 	CustomerOffer            INT NULL,
-- 	BookValue                INT NULL,
-- 	BookThirdPartyID         INT NULL,
-- 	BookThirdPartyCategoryID INT NULL,
-- 	NAAAValue                INT NULL,
-- 	NADAValue                INT NULL,
-- 	StockedUnitsModelYear    INT NOT NULL,
-- 	StockedUnitsModel        INT NOT NULL,
-- 	OptimalUnitsModelYear    INT NULL,
-- 	OptimalUnitsModel        INT NULL,
-- 	CONSTRAINT PK_' + @TableName + ' PRIMARY KEY NONCLUSTERED (
-- 		MonthID,
-- 		DayID,
-- 		AppraisalID
-- 	),
-- 	CONSTRAINT CK_' + @TableName + '_MonthID CHECK (
-- 		MonthID = 200701
-- 	)
-- )'
-- 	EXECUTE (@CMD)
-- 	
-- 	SET @CMD = 'CREATE CLUSTERED INDEX IDX_' + @TableName + ' ON ' + @TableName + ' (BusinessUnitID, DayID)'
-- 	EXECUTE (@CMD)
-- 	
-- 	INSERT INTO dbo.TablePartitions (TableName, Partition) VALUES (@Prefix, @MonthID)
-- 	
-- 	EXECUTE dbo.CreateTablePartitionView @Prefix
-- GO
