/*

Restore IMT from tape folder (complete and differential) to Dev_Hal folder
use imt
exec sp_dropuser  'firstlook'
*/

--  Template for quick restore
DECLARE @ReturnVal  int

EXECUTE @ReturnVal = sp_ComprehensiveRestore_SLS
  @SourceDatabase           = 'IMT'
 ,@TargetDatabase           = 'IMT'
 ,@FinalState               = 3
 ,@CompleteBackupFolder     = '\\ord1file01\production.backups$\proddb01sql'
 ,@DifferentialBackupFolder = '\\ord1file01\production.backups$\proddb01sql'
 ,@FileLocations            = '
   move "Data1" to "D:\Data_HAL\SQL_Datafiles\IMT.mdf"
  ,move "Data2" to "D:\Data_HAL\SQL_Datafiles\IMT_Data2.ndf"
  ,move "IDX1"  to "D:\Data_HAL\SQL_Datafiles\IMT_IDX.ndf"
  ,move "Log"   to "D:\Data_HAL\SQL_Datafiles\IMT_Log.ldf"
'  --   "move" functionality not amenable to quick templates...
 ,@Replace                  = 1
 ,@Live                     = 1

PRINT case @ReturnVal
       when  0 then 'Restore completed'
       when  1 then 'Not set to overwrite existing database'
       when  2 then 'Database does not exist for T or D restore'
       when  3 then 'Database recovered, cannot append T or D restores'
       when  4 then 'Standby file must be specified'
       when  5 then 'Unable to locate complete backup file'
       when 11 then 'Complete restore failed'
       when 12 then 'Differential restore failed'
       when 13 then 'Transaction restore failed'
       else 'Unanticipated error generated: ' + cast(@ReturnVal as varchar(20))
      end

/*

ALTER DATABASE IMT
 set recovery simple

USE IMT

DBCC SHRINKFILE(2)
-- DBCC SHRINKDATABASE('IMT')

EXECUTE sp_grantDBAccess 'Firstlook\swenmouth'
EXECUTE sp_addRoleMember 'db_owner', 'Firstlook\swenmouth'

EXECUTE sp_grantDBAccess 'Firstlook\tbergerson'
EXECUTE sp_addRoleMember 'db_owner', 'Firstlook\tbergerson'

EXECUTE sp_grantDBAccess 'Firstlook\tobrien'
EXECUTE sp_addRoleMember 'db_owner', 'Firstlook\tobrien'

EXECUTE sp_grantDBAccess 'RubyBuilder'
EXECUTE sp_addRoleMember 'db_datareader', 'RubyBuilder'

EXECUTE sp_change_users_login 'auto_fix', 'firstlook'

UPDATE Member
 set
   Password          = '7288edd0fc3ffcbe93a0cf06e3568e28521687bc'  --  "test123"
      ,OfficePhoneNumber = '3122791234'
  ,OfficeFaxNumber   = '3124927086'
  ,EmailAddress      = 'admin@firstlook.biz'

*/