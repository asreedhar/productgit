
USE FLDW

EXEC BuildTransactionViews						-- Probably not necessary
EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 3			-- Market_A1, necessary for DealerMarket

EXEC BuildWarehouseTableGroup @WarehouseTableGroupID = 5		-- Rebuild post-dataload group of tables

EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 4			-- Market_A2
EXEC LoadWarehouseTables @TableTypeID = 3, @TableID = 3			-- InventoryBookout_F
EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 7			-- InventoryBookout_A1
EXEC LoadWarehouseTables @TableTypeID = 2, @TableID = 12		-- Market_A3
