/*

RESTORE FILELISTONLY
 from disk = '<BackupFile>'

EXECUTE master.dbo.xp_restore_filelistonly
 @filename = '<path + file>'

*/

--  Template for quick restore
DECLARE @ReturnVal  int

EXECUTE @ReturnVal = sp_ComprehensiveRestore_SLS
  @SourceDatabase           = 'ATC'
 ,@TargetDatabase           = 'ATC'
 ,@FinalState               = 3
 ,@CompleteBackupFolder     = '\\ord1file01\production.backups$\proddb01sql'
 ,@FileLocations            = '
   move "DATA1" to "D:\Data_HAL\SQL_Datafiles\ATC.mdf"
  ,move "DATA2" to "D:\Data_HAL\SQL_Datafiles\ATC_1.ndf"
  ,move "IDX"   to "D:\Data_HAL\SQL_Datafiles\ATC_Data2.ndf"
  ,move "LOG"   to "D:\Data_HAL\SQL_Datafiles\ATC_Log.ldf"
'  --   "move" functionality not amenable to quick templates...
 ,@Replace                  = 1
 ,@Live                     = 1

PRINT case @ReturnVal
       when  0 then 'Restore completed'
       when  1 then 'Not set to overwrite existing database'
       when  2 then 'Database does not exist for T or D restore'
       when  3 then 'Database recovered, cannot append T or D restores'
       when  4 then 'Standby file must be specified'
       when  5 then 'Unable to locate complete backup file'
       when 11 then 'Complete restore failed'
       when 12 then 'Differential restore failed'
       when 13 then 'Transaction restore failed'
       else 'Unanticipated error generated: ' + cast(@ReturnVal as varchar(20))
      end

/*

USE ATC
EXECUTE sp_change_users_login 'auto_fix', 'firstlook'

EXECUTE sp_grantDBAccess  'RubyBuilder'
EXECUTE sp_addRoleMember 'db_dataReader', 'RubyBuilder'

*/
