
--
-- Look at pulling PlanningCategoryConfigurations back into a view.
--

USE HAL

-- --------------------------------------------------------------------
--                      Third Party Entity Tables                    --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LiveAuction_ThirdPartyEntity]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.LiveAuction_ThirdPartyEntity
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Wholesaler_ThirdPartyEntity]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Wholesaler_ThirdPartyEntity
GO

-- --------------------------------------------------------------------
--                          Inventory Tables                         --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryPlanningTasks]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InventoryPlanningTasks
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryPlanningCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InventoryPlanningCategories
GO

-- --------------------------------------------------------------------
--                          Planning Tables                          --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryTableCells]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategoryTableCells
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryTableCols]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategoryTableCols
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryTableRows]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategoryTableRows
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryTables]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategoryTables
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BusinessUnitPlanningCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.BusinessUnitPlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionDefaults]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionDefaults
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryLights]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategoryLights
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionsThirdPartyEntityTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionsThirdPartyEntityTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningActionTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningActionTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryConfigurations]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.PlanningCategoryConfigurations
GO

--
-- Types of Planning Category
--

CREATE TABLE dbo.PlanningCategoryLights (
	[PlanningCategoryLightID] INT NOT NULL,
	[Name]                    VARCHAR(255) NOT NULL,
	CONSTRAINT PK_PlanningCategoryLight PRIMARY KEY (
		PlanningCategoryLightID
	)
)
GO

INSERT INTO dbo.PlanningCategoryLights ([PlanningCategoryLightID],[Name]) VALUES (1,'Red')
INSERT INTO dbo.PlanningCategoryLights ([PlanningCategoryLightID],[Name]) VALUES (2,'Yellow')
INSERT INTO dbo.PlanningCategoryLights ([PlanningCategoryLightID],[Name]) VALUES (3,'Green')
GO

--
-- List of Planning Categories
--

CREATE TABLE dbo.PlanningCategories (
	[PlanningCategoryID]      INT NOT NULL,
	[PlanningCategoryLightID] INT NOT NULL,
	[Rank]                    INT NOT NULL,
	[CanBeAliased]            BIT NOT NULL,
	[Name]                    VARCHAR(255) NOT NULL,
	CONSTRAINT PK_PlanningCategory PRIMARY KEY (
		PlanningCategoryID
	),
	CONSTRAINT FK_PlanningCategory_PlanningCategoryLight FOREIGN KEY (
		PlanningCategoryLightID
	)
	REFERENCES PlanningCategoryLights (
		PlanningCategoryLightID
	)
)
GO

INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (1, 3, 100, 1, 'Optimal')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (2, 3, 200, 1, 'Non-Optimal')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (3, 2, 300, 1, 'Low Sales History')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (4, 2, 400, 1, 'Poor Performance')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (5, 1, 500, 1, 'Poor Performance')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (6, 1, 600, 0, 'High Mileage')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (7, 1, 700, 0, 'Excessive Mileage')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (8, 1, 800, 0, 'High Age')
INSERT INTO dbo.PlanningCategories ([PlanningCategoryID],[PlanningCategoryLightID],[Rank],[CanBeAliased],[Name]) VALUES (9, 1, 900, 0, 'Excessive Age')
GO

--
-- Types of Planning Action a dealer can schedule to take.
--

CREATE TABLE dbo.PlanningActionTypes (
	[PlanningActionTypeID] INT NOT NULL,
	[Rank]                 INT NOT NULL,
	[CanBeScheduled]       BIT NOT NULL,
	[Name]                 VARCHAR(255) NOT NULL,
	[CamelCaseName]        VARCHAR(255) NOT NULL,
	[UnderscoreName]       VARCHAR(255) NOT NULL,
	CONSTRAINT PK_PlanningActionType PRIMARY KEY (
		PlanningActionTypeID
	)
)
GO

INSERT INTO dbo.PlanningActionTypes ([PlanningActionTypeID],[Rank],[CanBeScheduled],[Name],[CamelCaseName],[UnderscoreName]) VALUES (1, 100, 1, 'Retail', 'Retail', 'retail')
INSERT INTO dbo.PlanningActionTypes ([PlanningActionTypeID],[Rank],[CanBeScheduled],[Name],[CamelCaseName],[UnderscoreName]) VALUES (2, 200, 1, 'Wholesale', 'Wholesale', 'wholesale')
INSERT INTO dbo.PlanningActionTypes ([PlanningActionTypeID],[Rank],[CanBeScheduled],[Name],[CamelCaseName],[UnderscoreName]) VALUES (3, 300, 0, 'Sold', 'Sold', 'sold')
INSERT INTO dbo.PlanningActionTypes ([PlanningActionTypeID],[Rank],[CanBeScheduled],[Name],[CamelCaseName],[UnderscoreName]) VALUES (4, 400, 0, 'Other', 'Other', 'other')
GO

--
-- List of Planning Actions. This is a sub-set of all possible actions for the same reason
-- we use a sub-set of all action types. There is a sparsely distributed Rank column for
-- when the business decides to re-order or add more actions.
--

CREATE TABLE dbo.PlanningActions (
	[PlanningActionID]     INT NOT NULL,
	[PlanningActionTypeID] INT NOT NULL,
	[Name]                 VARCHAR(255) NOT NULL,
	[CamelCaseName]        VARCHAR(255) NOT NULL,
	[UnderscoreName]       VARCHAR(255) NOT NULL,
	[Rank]                 INT NOT NULL,
	[CanBeScheduled]       BIT NOT NULL,
	CONSTRAINT PK_PlanningAction PRIMARY KEY (
		PlanningActionID
	),
	CONSTRAINT FK_PlanningCategory_PlanningActionType FOREIGN KEY (
		PlanningActionTypeID
	)
	REFERENCES PlanningActionTypes (
		PlanningActionTypeID
	)
)
GO

-- retail actions

INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (01,1,100,1,'Reprice','Reprice', 'reprice')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (02,1,200,1,'Small Print Advertisement','SmallPrintAdvertisement', 'small_print_advertisement')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (11,1,250,0,'Internet Advertisement','InternetAdvertisement', 'internet_advertisement')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (03,1,300,0,'Internet Auction','InternetAuction', 'internet_auction')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (04,1,400,1,'Lot Promotion','LotPromotion', 'lot_promotion')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (05,1,500,1,'SPIFF','SPIFF', 'spiff')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (06,1,600,1,'Other','Other', 'other')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (15,1,700,1,'Service','Service', 'service')
GO

-- wholesale actions

INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (07,2,700,1,'Live Auction','LiveAuction', 'live_auction')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (08,2,800,1,'Internet Auction','InternetAuction', 'internet_auction')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (12,2,850,1,'In Group','InGroup', 'in_group')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (09,2,900,1,'Wholesaler','Wholesaler', 'wholesaler')
INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (10,2,1000,1,'Other','Other', 'other')
GO

-- sold actions

INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (13,3,1100,0,'Sold','Sold', 'sold')
GO

-- other actions

INSERT INTO dbo.PlanningActions (PlanningActionID,PlanningActionTypeID,Rank,CanBeScheduled,[Name],[CamelCaseName],[UnderscoreName]) VALUES (14,4,1200,0,'Other','Other', 'other')
GO

CREATE TABLE dbo.PlanningActionsThirdPartyEntityTypes (
	[PlanningActionID]       INT NOT NULL,
	[ThirdPartyEntityTypeID] INT NOT NULL,
	CONSTRAINT PK_PlanningActionThirdPartyEntity PRIMARY KEY (
		PlanningActionID,
		ThirdPartyEntityTypeID
	),
	CONSTRAINT FK_PlanningActionThirdPartyEntity_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	)
)
GO

INSERT INTO dbo.PlanningActionsThirdPartyEntityTypes (PlanningActionID,ThirdPartyEntityTypeID) VALUES (02,1)
INSERT INTO dbo.PlanningActionsThirdPartyEntityTypes (PlanningActionID,ThirdPartyEntityTypeID) VALUES (07,2)
INSERT INTO dbo.PlanningActionsThirdPartyEntityTypes (PlanningActionID,ThirdPartyEntityTypeID) VALUES (09,3)
INSERT INTO dbo.PlanningActionsThirdPartyEntityTypes (PlanningActionID,ThirdPartyEntityTypeID) VALUES (11,4)
INSERT INTO dbo.PlanningActionsThirdPartyEntityTypes (PlanningActionID,ThirdPartyEntityTypeID) VALUES (03,5)
INSERT INTO dbo.PlanningActionsThirdPartyEntityTypes (PlanningActionID,ThirdPartyEntityTypeID) VALUES (08,5)
GO

--
-- This is the base table for the planning action defaults. There are three types of default:
-- 1) Specification of the planning act to use (SPIFF, Lot Promote, Other, ...)
-- 2) Specification of a third party entity (Internet Advertising, Live/Internet Auction, Wholesaler)
-- 3) Specification of arguments into a function (Print Advertising, Reprice, Service)
-- Each planning action default has its own table; each table duplicates the business unit and active
-- columns as they can be re-used between defaults.
--

CREATE TABLE dbo.PlanningActionDefaults (
	PlanningActionDefaultID           INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                    INT               NOT NULL,
	PlanningActionID                  INT               NOT NULL,
	Rank                              INT               NOT NULL,
	EmptyValue                        BIT               NOT NULL,
	Active                            BIT               NOT NULL,
	CONSTRAINT PK_PlanningActionDefault PRIMARY KEY (
		PlanningActionDefaultID
	),
	CONSTRAINT FK_PlanningActionDefaults_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	)
)
GO

INSERT INTO dbo.PlanningActionDefaults (BusinessUnitID,PlanningActionID,Rank,EmptyValue,Active)
SELECT
	B.BusinessUnitID,
	P.PlanningActionID,
	0 AS Rank,
	1 AS EmptyValue,
	1 AS Active
FROM
	[IMT].dbo.BusinessUnit B
	CROSS JOIN dbo.PlanningActions P
GO

--
-- Configuration of PlanningCategories for a BusinessUnit.
--

CREATE TABLE dbo.BusinessUnitPlanningCategories (
	BusinessUnitPlanningCategoryID  INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                  INT NOT NULL,
	PlanningCategoryID              INT NOT NULL,
	Active                          BIT NOT NULL,
	IsAnAlias                       BIT NULL,
	AliasPlanningCategoryID         INT NULL,
	LowerThreshold                  INT NULL,
	UpperThreshold                  INT NULL,
	IsAdvertisementCandidate        BIT NOT NULL,
	AdvertisementCandidateFromDay   INT NULL,
	IsWholesaleCandidate            BIT NOT NULL,
	WholesaleCandidateFromDay       INT NULL,
	IsComparisonConfiguration       BIT NOT NULL,
	CONSTRAINT PK_PlanningCategoryConfiguration PRIMARY KEY (
		BusinessUnitPlanningCategoryID
	),
	CONSTRAINT FK_PlanningCategoryConfiguration_PlanningCategory FOREIGN KEY (
		PlanningCategoryID
	)
	REFERENCES PlanningCategories (
		PlanningCategoryID
	),
	CONSTRAINT UK_PlanningCategoryConfiguration UNIQUE (
		BusinessUnitID,
		PlanningCategoryID,
		IsComparisonConfiguration
	)
)
GO

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	C.PlanningCategoryID,
	1 Active,
	0 IsAnAlias,
	NULL AliasPlanningCategoryID,
	NULL LowerThreshold,
	NULL UpperThreshold,
	CASE WHEN C.PlanningCategoryID BETWEEN 1 AND 4 THEN 1 ELSE 0 END IsAdvertisementCandidate,
	CASE WHEN C.PlanningCategoryID IN (1,2) THEN 30 WHEN C.PlanningCategoryID IN (3,4) THEN 21 ELSE NULL END AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	CASE WHEN C.PlanningCategoryID IN (1,2) THEN 60 WHEN C.PlanningCategoryID = 3 THEN 45 WHEN C.PlanningCategoryID = 4 THEN 30 ELSE 21 END WholesaleCandidateFromDay,
	0 IsComparisonConfiguration
FROM
	[IMT].dbo.BusinessUnit B
CROSS JOIN
	dbo.PlanningCategories C
WHERE
	B.Active = 1
AND	C.PlanningCategoryID BETWEEN 1 AND 5
ORDER BY
	B.BusinessUnitID,
	C.PlanningCategoryID
GO

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	C.PlanningCategoryID,
	1 Active,
	0 IsAnAlias,
	NULL AliasPlanningCategoryID,
	NULL LowerThreshold,
	NULL UpperThreshold,
	CASE WHEN C.PlanningCategoryID BETWEEN 1 AND 4 THEN 1 ELSE 0 END IsAdvertisementCandidate,
	CASE WHEN C.PlanningCategoryID IN (1,2) THEN 30 WHEN C.PlanningCategoryID IN (3,4) THEN 21 ELSE NULL END AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	CASE WHEN C.PlanningCategoryID IN (1,2) THEN 60 WHEN C.PlanningCategoryID = 3 THEN 45 WHEN C.PlanningCategoryID = 4 THEN 30 ELSE 21 END WholesaleCandidateFromDay,
	1 IsComparisonConfiguration
FROM
	[IMT].dbo.BusinessUnit B
CROSS JOIN
	dbo.PlanningCategories C
WHERE
	B.Active = 1
AND	C.PlanningCategoryID BETWEEN 1 AND 5
ORDER BY
	B.BusinessUnitID,
	C.PlanningCategoryID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerRisk]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[DealerRisk]
GO

CREATE VIEW dbo.DealerRisk (
	BusinessUnitID,
	HighMileageOverall,
	HighMileageRedLight,
	RiskLevelYearInitialTimePeriod,
	RiskLevelYearSecondaryTimePeriod,
	RiskLevelYearRollOverMonth
)
AS
SELECT
	B.BusinessUnitID,
	COALESCE(R.HighMileageOverall,80000)           AS HighMileageOverall,
	COALESCE(R.HighMileageRedLight,100000)         AS HighMileageRedLight,
	COALESCE(R.RiskLevelYearInitialTimePeriod,6)   AS RiskLevelYearInitialTimePeriod,
	COALESCE(R.RiskLevelYearSecondaryTimePeriod,5) AS RiskLevelYearSecondaryTimePeriod,
	COALESCE(R.RiskLevelYearRollOverMonth,9)       AS RiskLevelYearRollOverMonth
FROM
	[IMT].dbo.BusinessUnit B
	LEFT JOIN [IMT].dbo.DealerRisk R ON R.BusinessUnitID = B.BusinessUnitID
GO

-- high mileage

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	6,
	0,
	NULL,
	NULL,
	R.HighMileageOverall,
	R.HighMileageRedLight,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	0
FROM
	[IMT].dbo.BusinessUnit B
JOIN	dbo.DealerRisk R ON R.BusinessUnitID = B.BusinessUnitID	
WHERE
	B.Active = 1
GO

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	6,
	1,
	0,
	NULL,
	R.HighMileageOverall,
	R.HighMileageRedLight,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	1
FROM
	[IMT].dbo.BusinessUnit B
JOIN	dbo.DealerRisk R ON R.BusinessUnitID = B.BusinessUnitID	
WHERE
	B.Active = 1
GO

-- excessive mileage

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	7,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	0
FROM
	[IMT].dbo.BusinessUnit B
WHERE
	B.Active = 1
GO

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	7,
	1,
	0,
	NULL,
	NULL,
	NULL,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	1
FROM
	[IMT].dbo.BusinessUnit B
WHERE
	B.Active = 1
GO

-- high age

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	8,
	0,
	NULL,
	NULL,
	R.RiskLevelYearInitialTimePeriod-1,
	R.RiskLevelYearInitialTimePeriod,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	0
FROM
	[IMT].dbo.BusinessUnit B
JOIN	dbo.DealerRisk R ON R.BusinessUnitID = B.BusinessUnitID	
WHERE
	B.Active = 1
GO

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	8,
	1,
	0,
	NULL,
	R.RiskLevelYearInitialTimePeriod-1,
	R.RiskLevelYearInitialTimePeriod,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	1
FROM
	[IMT].dbo.BusinessUnit B
JOIN	dbo.DealerRisk R ON R.BusinessUnitID = B.BusinessUnitID	
WHERE
	B.Active = 1
GO

-- excessive age

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	9,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	0
FROM
	[IMT].dbo.BusinessUnit B
WHERE
	B.Active = 1
GO

INSERT INTO dbo.BusinessUnitPlanningCategories (
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
)
SELECT
	B.BusinessUnitID,
	9,
	1,
	0,
	NULL,
	NULL,
	NULL,
	0 IsAdvertisementCandidate,
	NULL AdvertisementCandidateFromDay,
	1 IsWholesaleCandidate,
	21 WholesaleCandidateFromDay,
	1
FROM
	[IMT].dbo.BusinessUnit B
WHERE
	B.Active = 1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlanningCategoryConfigurations]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[PlanningCategoryConfigurations]
GO

CREATE VIEW dbo.PlanningCategoryConfigurations (
	BusinessUnitID,
	HasExcessiveMileageCategory,
	HasHighMileageCategory,
	HighMileageLowerThreshold,
	HighMileageUpperThreshold,
	IsHighMileageCategoryAnAlias,
	HighMileageCategoryAlias,
	IsExcessiveMileageCategoryAnAlias,
	ExcessiveMileageCategoryAlias,
	HasExcessiveAgeCategory,
	HasHighAgeCategory,
	HighAgeLowerThreshold,
	HighAgeUpperThreshold,
	IsHighAgeCategoryAnAlias,
	HighAgeCategoryAlias,
	IsExcessiveAgeCategoryAnAlias,
	ExcessiveAgeCategoryAlias,
	IsComparisonConfiguration
)
AS
SELECT
	BusinessUnitID,
	-- mileage
	CAST(SUM(CASE WHEN PlanningCategoryID = 7 THEN CAST(Active AS TINYINT) ELSE 0 END) AS BIT) HasExcessiveMileageCategory,
	CAST(SUM(CASE WHEN PlanningCategoryID = 6 THEN CAST(Active AS TINYINT) ELSE 0 END) AS BIT) HasHighMileageCategory,
	SUM(CASE WHEN PlanningCategoryID = 6 THEN LowerThreshold ELSE NULL END) HighMileageLowerThreshold,
	SUM(CASE WHEN PlanningCategoryID = 6 THEN UpperThreshold ELSE NULL END) HighMileageUpperThreshold,
	CAST(SUM(CASE WHEN PlanningCategoryID = 6 THEN CAST(IsAnAlias AS TINYINT) ELSE 0 END) AS BIT) IsHighMileageCategoryAnAlias,
	SUM(CASE WHEN PlanningCategoryID = 6 THEN AliasPlanningCategoryID ELSE NULL END) HighMileageCategoryAlias,
	CAST(SUM(CASE WHEN PlanningCategoryID = 7 THEN CAST(IsAnAlias AS TINYINT) ELSE 0 END) AS BIT) IsExcessiveMileageCategoryAnAlias,
	SUM(CASE WHEN PlanningCategoryID = 7 THEN AliasPlanningCategoryID ELSE NULL END) ExcessiveMileageCategoryAlias,
	-- age
	CAST(SUM(CASE WHEN PlanningCategoryID = 9 THEN CAST(Active AS TINYINT) ELSE 0 END) AS BIT) HasExcessiveAgeCategory,
	CAST(SUM(CASE WHEN PlanningCategoryID = 8 THEN CAST(Active AS TINYINT) ELSE 0 END) AS BIT) HasHighAgeCategory,
	SUM(CASE WHEN PlanningCategoryID = 8 THEN LowerThreshold ELSE NULL END) HighAgeLowerThreshold,
	SUM(CASE WHEN PlanningCategoryID = 8 THEN UpperThreshold ELSE NULL END) HighAgeUperThreshold,
	CAST(SUM(CASE WHEN PlanningCategoryID = 8 THEN CAST(IsAnAlias AS TINYINT) ELSE 0 END) AS BIT) IsHighAgeCategoryAnAlias,
	SUM(CASE WHEN PlanningCategoryID = 8 THEN AliasPlanningCategoryID ELSE NULL END) HighAgeCategoryAlias,
	CAST(SUM(CASE WHEN PlanningCategoryID = 9 THEN CAST(IsAnAlias AS TINYINT) ELSE 0 END) AS BIT) IsExcessiveAgeCategoryAnAlias,
	SUM(CASE WHEN PlanningCategoryID = 9 THEN AliasPlanningCategoryID ELSE NULL END) ExcessiveAgeCategoryAlias,
	IsComparisonConfiguration
FROM
	dbo.BusinessUnitPlanningCategories
GROUP BY
	BusinessUnitID,
	IsComparisonConfiguration
GO

--
-- Planning Category Age Boundaries
--
-- This is a highly redundant de-normalized data structure which makes user input trivial.
-- If there was time I'd have used an additional normalized data structure and worked out all
-- the interactions. As it stands I have very little time at all and this will have to wait.
--
-- The "upside" of using this data-model is that its redundancy means cascade deletes are no
-- longer needed to handle when age boundaries are removed. Age boundaries are now disabled
-- and "forever kept" so the foreign keys can be left intact. Hooray!
--

CREATE TABLE dbo.PlanningCategoryTables (
	PlanningCategoryTableID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID          INT NOT NULL,
	PlanningCategoryID      INT NOT NULL,
	CONSTRAINT PK_PlanningCategoryTable PRIMARY KEY (
		PlanningCategoryTableID
	),
	CONSTRAINT FK_PlanningCategoryTable_PlanningCategory FOREIGN KEY (
		PlanningCategoryID
	)
	REFERENCES PlanningCategories (
		PlanningCategoryID
	),
	CONSTRAINT UK_PlanningCategoryTable UNIQUE (
		BusinessUnitID,
		PlanningCategoryID
	)
)
GO

CREATE TABLE dbo.PlanningCategoryTableRows (
	PlanningCategoryTableRowID INT IDENTITY(1,1) NOT NULL,
	PlanningCategoryTableID    INT NOT NULL,
	PlanningActionID           INT NOT NULL,
	RIndex                     INT NOT NULL,
	CONSTRAINT PK_PlanningCategoryTableRow PRIMARY KEY (
		PlanningCategoryTableRowID
	),
	CONSTRAINT FK_PlanningCategoryTableRow_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	),
	CONSTRAINT FK_PlanningCategoryTableRow_PlanningCategoryTable FOREIGN KEY (
		PlanningCategoryTableID
	)
	REFERENCES PlanningCategoryTables (
		PlanningCategoryTableID
	)
--	SQL Server does not have deferred unique constraints
-- 	, CONSTRAINT UK_PlanningCategoryTableRow UNIQUE (
-- 		PlanningCategoryTableID,
-- 		RIndex
-- 	)
)
GO

CREATE TABLE dbo.PlanningCategoryTableCols (
	PlanningCategoryTableColID INT IDENTITY(1,1) NOT NULL,
	PlanningCategoryTableID    INT NOT NULL,
	Age                        INT NOT NULL,
	CIndex                     INT NOT NULL,
	CONSTRAINT PK_PlanningCategoryTableCol PRIMARY KEY (
		PlanningCategoryTableColID
	),
	CONSTRAINT FK_PlanningCategoryTableCol_PlanningCategoryTable FOREIGN KEY (
		PlanningCategoryTableID
	)
	REFERENCES PlanningCategoryTables (
		PlanningCategoryTableID
	)
--	SQL Server does not have deferred unique constraints
-- 	, CONSTRAINT UK_PlanningCategoryTableCol UNIQUE (
-- 		PlanningCategoryTableID,
-- 		CIndex
-- 	)
)
GO

CREATE TABLE dbo.PlanningCategoryTableCells (
	PlanningCategoryTableCellID INT IDENTITY(1,1) NOT NULL,
	PlanningCategoryTableRowID  INT NOT NULL,
	PlanningCategoryTableColID  INT NOT NULL,
	PlanningActionDefaultID     INT NOT NULL,
	Active                      BIT NOT NULL,
	CONSTRAINT PK_PlanningCategoryTableCell PRIMARY KEY (
		PlanningCategoryTableCellID
	),
	CONSTRAINT FK_PlanningCategoryTableCol_PlanningCategoryTableRow FOREIGN KEY (
		PlanningCategoryTableRowID
	)
	REFERENCES PlanningCategoryTableRows (
		PlanningCategoryTableRowID
	),
	CONSTRAINT FK_PlanningCategoryTableCol_PlanningCategoryTableCol FOREIGN KEY (
		PlanningCategoryTableColID
	)
	REFERENCES PlanningCategoryTableCols (
		PlanningCategoryTableColID
	),
	CONSTRAINT FK_PlanningCategoryTableCol_PlanningActionDefault FOREIGN KEY (
		PlanningActionDefaultID
	)
	REFERENCES PlanningActionDefaults (
		PlanningActionDefaultID
	),
	CONSTRAINT UK_PlanningCategoryTableCell UNIQUE (
		PlanningCategoryTableRowID,
		PlanningCategoryTableColID
	)
)
GO

CREATE INDEX IDX_PlanningCategoryTableCells_ColActive ON PlanningCategoryTableCells (
	PlanningCategoryTableColID,
	Active
)
GO

INSERT INTO dbo.PlanningCategoryTables (
	BusinessUnitID,
	PlanningCategoryID
)
SELECT
	B.BusinessUnitID,
	C.PlanningCategoryID
FROM
	[IMT].dbo.BusinessUnit B
CROSS JOIN
	dbo.PlanningCategories C
WHERE
	B.Active = 1
ORDER BY
	B.BusinessUnitID,
	C.PlanningCategoryID
GO

INSERT INTO dbo.PlanningCategoryTableRows (
	PlanningCategoryTableID,
	PlanningActionID,
	RIndex
)
SELECT
	T.PlanningCategoryTableID,
	A.PlanningActionID,
	A.PlanningActionID
FROM
	dbo.PlanningCategoryTables T
CROSS JOIN
	dbo.PlanningActions A
ORDER BY
	T.PlanningCategoryTableID,
	A.PlanningActionID
GO

INSERT INTO dbo.PlanningCategoryTableCols (
	PlanningCategoryTableID,
	Age,
	CIndex
)
SELECT
	T.PlanningCategoryTableID,
	A.Age,
	A.Age/7 - CASE WHEN (B.WholesaleCandidateFromDay-7*6)/7 < 0 THEN 0 ELSE (B.WholesaleCandidateFromDay-7*6)/7 END CIndex
FROM
	dbo.PlanningCategoryTables T
JOIN	dbo.BusinessUnitPlanningCategories B ON B.PlanningCategoryID = T.PlanningCategoryID AND B.BusinessUnitID = T.BusinessUnitID
CROSS JOIN
	(
			SELECT 07 AS Age
	UNION ALL	SELECT 14 AS Age
	UNION ALL	SELECT 21 AS Age
	UNION ALL	SELECT 28 AS Age
	UNION ALL	SELECT 35 AS Age
	UNION ALL	SELECT 42 AS Age
	UNION ALL	SELECT 49 AS Age
	UNION ALL	SELECT 56 AS Age
	UNION ALL	SELECT 63 AS Age
	UNION ALL	SELECT 70 AS Age
	UNION ALL	SELECT 77 AS Age
	UNION ALL	SELECT 84 AS Age
	UNION ALL	SELECT 91 AS Age
	UNION ALL	SELECT 98 AS Age
	) A
WHERE
	B.IsComparisonConfiguration = 0
AND	((A.Age < B.WholesaleCandidateFromDay AND A.Age > B.WholesaleCandidateFromDay-7*6)
OR	 (A.Age >= B.WholesaleCandidateFromDay AND A.Age < B.WholesaleCandidateFromDay+7*6))
ORDER BY
	T.PlanningCategoryTableID,
	A.Age
GO

INSERT INTO dbo.PlanningCategoryTableCells(
	PlanningCategoryTableRowID,
	PlanningCategoryTableColID,
	Active,
	PlanningActionDefaultID
)
SELECT
	R.PlanningCategoryTableRowID,
	C.PlanningCategoryTableColID,
	0,
	D.PlanningActionDefaultID
FROM
	dbo.PlanningCategoryTables T
JOIN	dbo.PlanningCategoryTableRows R ON R.PlanningCategoryTableID = T.PlanningCategoryTableID
JOIN	dbo.PlanningCategoryTableCols C ON C.PlanningCategoryTableID = T.PlanningCategoryTableID
JOIN	dbo.PlanningActionDefaults D ON D.PlanningActionID = R.PlanningActionID AND D.BusinessUnitID = T.BusinessUnitID
WHERE
	D.EmptyValue = 1
ORDER BY
	R.PlanningCategoryTableRowID,
	C.PlanningCategoryTableColID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BusinessUnitPlanningCategoryPlanningAgeBoundaries]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[BusinessUnitPlanningCategoryPlanningAgeBoundaries]
GO

CREATE VIEW dbo.BusinessUnitPlanningCategoryPlanningAgeBoundaries (
	BusinessUnitPlanningCategoryID,
	BusinessUnitID,
	PlanningCategoryID,
	PlanningCategoryTableColID0,
	Age0,
	PlanningCategoryTableColID1,
	Age1
)
AS
SELECT
	BPC.BusinessUnitPlanningCategoryID,
	BPC.BusinessUnitID,
	BPC.PlanningCategoryID,
	COL1.PlanningCategoryTableColID PlanningCategoryTableColID0,
	COL1.Age Age0,
	COL2.PlanningCategoryTableColID PlanningCategoryTableColID1,
	COL2.Age Age1
FROM
	dbo.BusinessUnitPlanningCategories BPC
JOIN	dbo.PlanningCategoryTables TBL ON TBL.BusinessUnitID = BPC.BusinessUnitID AND TBL.PlanningCategoryID = BPC.PlanningCategoryID
JOIN	dbo.PlanningCategoryTableCols COL1 ON TBL.PlanningCategoryTableID = COL1.PlanningCategoryTableID
JOIN	dbo.PlanningCategoryTableCells CEL ON COL1.PlanningCategoryTableColID = CEL.PlanningCategoryTableColID
LEFT JOIN (
		SELECT
			COL1.PlanningCategoryTableColID      AS PlanningCategoryTableColID1,
			MIN(COL2.PlanningCategoryTableColID) AS PlanningCategoryTableColID2
		FROM
			dbo.PlanningCategoryTableCells CEL1
		JOIN	dbo.PlanningCategoryTableCols  COL1 ON CEL1.PlanningCategoryTableColID = COL1.PlanningCategoryTableColID
		JOIN	dbo.PlanningCategoryTableCols  COL2 ON COL1.PlanningCategoryTableID = COL2.PlanningCategoryTableID
		JOIN	dbo.PlanningCategoryTableCells CEL2 ON COL2.PlanningCategoryTableColID = CEL2.PlanningCategoryTableColID
		WHERE
			COL2.CIndex > COL1.CIndex
		AND	CEL1.Active = 1
		AND	CEL2.Active = 1
		GROUP BY
			COL1.PlanningCategoryTableColID
	) NXT ON COL1.PlanningCategoryTableColID = NXT.PlanningCategoryTableColID1
LEFT JOIN dbo.PlanningCategoryTableCols COL2 ON COL2.PlanningCategoryTableColID = NXT.PlanningCategoryTableColID2
WHERE
	BPC.IsComparisonConfiguration = 0
AND	BPC.Active = 1
AND	BPC.IsAnAlias = 0
AND	CEL.Active = 1
GROUP BY
	BPC.BusinessUnitPlanningCategoryID,
	BPC.BusinessUnitID,
	BPC.PlanningCategoryID,
	COL1.PlanningCategoryTableColID,
	COL1.Age,
	COL2.PlanningCategoryTableColID,
	COL2.Age
GO

-- SELECT * FROM BusinessUnitPlanningCategoryPlanningAgeBoundaries

--
-- Instance of a planning action, possibly pre-populated with a planning action default.
-- Planning action specific data is stored in a related table.
--

CREATE TABLE dbo.PlanningActs (
	PlanningActID                     INT IDENTITY(1,1) NOT NULL,
	PlanningActionID                  INT               NOT NULL,
	BusinessUnitID                    INT               NOT NULL,
	InventoryID                       INT               NOT NULL,
	InventoryPlanningTaskID           INT               NULL,
	Notes                             VARCHAR(255)      NULL,
	IncludeInPlanUntil                SMALLDATETIME     NULL,
	Created                           SMALLDATETIME     NOT NULL,
	CONSTRAINT PK_PlanningAct PRIMARY KEY (
		PlanningActID
	),
	CONSTRAINT FK_PlanningAct_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	)
)
GO

-- --------------------------------------------------------------------
--                          Inventory Views                          --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories_HighMileage]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories_HighMileage]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories_HighMileage (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsAnAlias,
	AliasPlanningCategoryID,
	IsComparisonConfiguration
)
AS
SELECT
	I.BusinessUnitID,
	I.GroupingDescriptionID,
	I.InventoryID,
	I.ModelYear,
	I.Mileage,
	I.Active,
	I.DaysToSale,
	I.TotalGross,
	COALESCE(C.IsHighMileageCategoryAnAlias,0),
	C.HighMileageCategoryAlias,
	C.IsComparisonConfiguration
FROM
	dbo.GetInventory I
JOIN	dbo.PlanningCategoryConfigurations C ON I.BusinessUnitID = C.BusinessUnitID
WHERE
	(C.HasHighMileageCategory = 1 AND I.Mileage BETWEEN C.HighMileageLowerThreshold AND C.HighMileageUpperThreshold)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories_ExcessiveMileage]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories_ExcessiveMileage]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories_ExcessiveMileage (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsAnAlias,
	AliasPlanningCategoryID,
	IsComparisonConfiguration
)
AS
SELECT
	I.BusinessUnitID,
	I.GroupingDescriptionID,
	I.InventoryID,
	I.ModelYear,
	I.Mileage,
	I.Active,
	I.DaysToSale,
	I.TotalGross,
	COALESCE(C.IsExcessiveMileageCategoryAnAlias,0),
	C.ExcessiveMileageCategoryAlias,
	C.IsComparisonConfiguration
FROM
	dbo.GetInventory I
JOIN	dbo.PlanningCategoryConfigurations C ON I.BusinessUnitID = C.BusinessUnitID
JOIN	dbo.DealerRisk R ON I.BusinessUnitID = R.BusinessUnitID
WHERE
	C.HasExcessiveMileageCategory = 1
AND	(
	(C.HasHighMileageCategory = 1 AND I.Mileage > C.HighMileageUpperThreshold)
OR	(C.HasHighMileageCategory = 0 AND I.Mileage > R.HighMileageOverall)
	)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories_HighAge]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories_HighAge]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories_HighAge (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsAnAlias,
	AliasPlanningCategoryID,
	IsComparisonConfiguration
)
AS
SELECT
	I.BusinessUnitID,
	I.GroupingDescriptionID,
	I.InventoryID,
	I.ModelYear,
	I.Mileage,
	I.Active,
	I.DaysToSale,
	I.TotalGross,
	COALESCE(C.IsHighAgeCategoryAnAlias,0),
	C.HighAgeCategoryAlias,
	C.IsComparisonConfiguration
FROM
	dbo.GetInventory I
JOIN	dbo.PlanningCategoryConfigurations C ON I.BusinessUnitID = C.BusinessUnitID
JOIN	dbo.DealerRisk R ON I.BusinessUnitID = R.BusinessUnitID
WHERE
	C.HasHighAgeCategory = 1
AND	I.ModelYear BETWEEN
		(DATEPART(YY,GETDATE()) -
			CASE
				WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
					C.HighAgeUpperThreshold
				ELSE
					C.HighAgeUpperThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
			END)
	AND	(DATEPART(YY,GETDATE()) -
			CASE
				WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
					C.HighAgeLowerThreshold
				ELSE
					C.HighAgeLowerThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
			END)
AND NOT	(
	(C.HasHighMileageCategory = 1 AND I.Mileage BETWEEN C.HighMileageLowerThreshold AND C.HighMileageUpperThreshold)
OR	(C.HasExcessiveMileageCategory = 1 AND C.HasHighMileageCategory = 1 AND I.Mileage > C.HighMileageUpperThreshold)
OR	(C.HasExcessiveMileageCategory = 1 AND C.HasHighMileageCategory = 0 AND I.Mileage > R.HighMileageOverall))
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories_ExcessiveAge]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories_ExcessiveAge]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories_ExcessiveAge (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsAnAlias,
	AliasPlanningCategoryID,
	IsComparisonConfiguration
)
AS
SELECT
	I.BusinessUnitID,
	I.GroupingDescriptionID,
	I.InventoryID,
	I.ModelYear,
	I.Mileage,
	I.Active,
	I.DaysToSale,
	I.TotalGross,
	COALESCE(C.IsExcessiveAgeCategoryAnAlias,0),
	C.ExcessiveAgeCategoryAlias,
	C.IsComparisonConfiguration
FROM
	dbo.GetInventory I
JOIN	dbo.PlanningCategoryConfigurations C ON I.BusinessUnitID = C.BusinessUnitID
JOIN	dbo.DealerRisk R ON I.BusinessUnitID = R.BusinessUnitID
WHERE
	C.HasExcessiveAgeCategory = 1
AND	(
	(C.HasHighAgeCategory = 1 AND I.ModelYear < (
		DATEPART(YY,GETDATE())
	-	CASE
			WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
				C.HighAgeUpperThreshold
			ELSE
				C.HighAgeUpperThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
		END
	))
OR	(C.HasHighAgeCategory = 0 AND I.ModelYear < (
		DATEPART(YY,GETDATE()) -
		CASE
			WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
				R.RiskLevelYearInitialTimePeriod
			ELSE
				R.RiskLevelYearSecondaryTimePeriod
		END
	))
	)
AND NOT	(
	(C.HasHighMileageCategory = 1 AND I.Mileage BETWEEN C.HighMileageLowerThreshold AND C.HighMileageUpperThreshold)
OR	(C.HasExcessiveMileageCategory = 1 AND C.HasHighMileageCategory = 1 AND I.Mileage > C.HighMileageUpperThreshold)
OR	(C.HasExcessiveMileageCategory = 1 AND C.HasHighMileageCategory = 0 AND I.Mileage > R.HighMileageOverall)
OR	(C.HasHighAgeCategory = 1 AND I.ModelYear BETWEEN
		(DATEPART(YY,GETDATE()) -
			CASE
				WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
					C.HighAgeUpperThreshold
				ELSE
					C.HighAgeUpperThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
			END)
	AND	(DATEPART(YY,GETDATE()) -
			CASE
				WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
					C.HighAgeLowerThreshold
				ELSE
					C.HighAgeLowerThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
			END))
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories_NeitherHighMileageNorHighAge]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories_NeitherHighMileageNorHighAge]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories_NeitherHighMileageNorHighAge (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsComparisonConfiguration
)
AS
SELECT
	I.BusinessUnitID,
	I.GroupingDescriptionID,
	I.InventoryID,
	I.ModelYear,
	I.Mileage,
	I.Active,
	I.DaysToSale,
	I.TotalGross,
	C.IsComparisonConfiguration
FROM
	dbo.GetInventory I
JOIN	dbo.PlanningCategoryConfigurations C ON I.BusinessUnitID = C.BusinessUnitID
JOIN	dbo.DealerRisk R ON I.BusinessUnitID = R.BusinessUnitID
WHERE
NOT	(
	(C.HasHighMileageCategory = 1 AND COALESCE(C.IsHighMileageCategoryAnAlias,0) = 0 AND I.Mileage BETWEEN C.HighMileageLowerThreshold AND C.HighMileageUpperThreshold)
OR	(C.HasExcessiveMileageCategory = 1 AND COALESCE(C.IsExcessiveMileageCategoryAnAlias,0) = 0 AND C.HasHighMileageCategory = 1 AND I.Mileage > C.HighMileageUpperThreshold)
OR	(C.HasExcessiveMileageCategory = 1 AND COALESCE(C.IsExcessiveMileageCategoryAnAlias,0) = 0 AND C.HasHighMileageCategory = 0 AND I.Mileage > R.HighMileageOverall)
OR	(C.HasHighAgeCategory = 1 AND COALESCE(C.IsHighAgeCategoryAnAlias,0) = 0 AND I.ModelYear BETWEEN
		(DATEPART(YY,GETDATE()) -
			CASE
				WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
					C.HighAgeUpperThreshold
				ELSE
					C.HighAgeUpperThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
			END)
	AND	(DATEPART(YY,GETDATE()) -
			CASE
				WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
					C.HighAgeLowerThreshold
				ELSE
					C.HighAgeLowerThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
			END))
OR	(C.HasExcessiveAgeCategory = 1 AND COALESCE(C.IsExcessiveAgeCategoryAnAlias,0) = 0 AND C.HasHighAgeCategory = 1 AND I.ModelYear < (
		DATEPART(YY,GETDATE())
	-	CASE
			WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
				C.HighAgeUpperThreshold
			ELSE
				C.HighAgeUpperThreshold+(R.RiskLevelYearInitialTimePeriod-R.RiskLevelYearSecondaryTimePeriod)
		END
	))
OR	(C.HasExcessiveAgeCategory = 1 AND COALESCE(C.IsExcessiveAgeCategoryAnAlias,0) = 0 AND C.HasHighAgeCategory = 0 AND I.ModelYear < (
		DATEPART(YY,GETDATE())
	-	CASE
			WHEN DATEPART(MM,GETDATE()) < R.RiskLevelYearRollOverMonth THEN
				R.RiskLevelYearInitialTimePeriod
			ELSE
				R.RiskLevelYearSecondaryTimePeriod
		END
	))
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories_Standard]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories_Standard]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories_Standard (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsAnAlias,
	AliasPlanningCategoryID,
	IsComparisonConfiguration
)
AS
SELECT
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsAnAlias,
	AliasPlanningCategoryID,
	IsComparisonConfiguration
FROM
(
	SELECT
		BusinessUnitID, GroupingDescriptionID, InventoryID, ModelYear, Mileage, Active, DaysToSale, TotalGross, IsAnAlias, AliasPlanningCategoryID, IsComparisonConfiguration
	FROM
		dbo.GetInventoryPlanningCategories_HighMileage HM
	WHERE
		IsAnAlias = 1
UNION ALL
	SELECT
		BusinessUnitID, GroupingDescriptionID, InventoryID, ModelYear, Mileage, Active, DaysToSale, TotalGross, IsAnAlias, AliasPlanningCategoryID, IsComparisonConfiguration
	FROM
		dbo.GetInventoryPlanningCategories_ExcessiveMileage EM
	WHERE
		IsAnAlias = 1
UNION ALL
	SELECT
		BusinessUnitID, GroupingDescriptionID, InventoryID, ModelYear, Mileage, Active, DaysToSale, TotalGross, IsAnAlias, AliasPlanningCategoryID, IsComparisonConfiguration
	FROM
		dbo.GetInventoryPlanningCategories_HighAge HA
	WHERE
		IsAnAlias = 1
UNION ALL
	SELECT
		BusinessUnitID, GroupingDescriptionID, InventoryID, ModelYear, Mileage, Active, DaysToSale, TotalGross, IsAnAlias, AliasPlanningCategoryID, IsComparisonConfiguration
	FROM
		dbo.GetInventoryPlanningCategories_ExcessiveAge EA
	WHERE
		IsAnAlias = 1
UNION ALL
	SELECT
		BusinessUnitID, GroupingDescriptionID, InventoryID, ModelYear, Mileage, Active, DaysToSale, TotalGross, 0, NULL, IsComparisonConfiguration
	FROM
		dbo.GetInventoryPlanningCategories_NeitherHighMileageNorHighAge NN
) T
GO

-- rows = profit
-- cols = units

-- SELECT DISTINCT IndexKey FROM [IMT].dbo.DealerGridValues WHERE LightValue = 3 ORDER BY IndexKey
--   Green
-- . . . . .
-- . . x x x
-- . x x x x
-- . x x x x
-- . x x x x

-- SELECT DISTINCT IndexKey FROM [IMT].dbo.DealerGridValues WHERE LightValue = 2 ORDER BY IndexKey
--  Yellow
-- x x x x x
-- . x . . .
-- . x . . .
-- . x . . .
-- . . . . .

-- SELECT DISTINCT IndexKey FROM [IMT].dbo.DealerGridValues WHERE LightValue = 1 ORDER BY IndexKey
--    Red
-- . . . . .
-- x . . . .
-- x . . . .
-- x . . . .
-- x . . . .

--
-- This query runs using just under the maximum possible number of tables. Be very careful about editing it.
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPlanningCategories]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[GetInventoryPlanningCategories]
GO

CREATE VIEW dbo.GetInventoryPlanningCategories (
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsComparisonConfiguration,
	PlanningCategoryID
)
AS
SELECT
	BusinessUnitID,
	GroupingDescriptionID,
	InventoryID,
	ModelYear,
	Mileage,
	Active,
	DaysToSale,
	TotalGross,
	IsComparisonConfiguration,
	PlanningCategoryID
FROM
(
	-- excessive age
	SELECT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		9 AS PlanningCategoryID
	FROM
		dbo.GetInventoryPlanningCategories_ExcessiveAge I
	WHERE
		IsAnAlias = 0
UNION ALL
	-- high age market
	SELECT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		8 AS PlanningCategoryID
	FROM
		dbo.GetInventoryPlanningCategories_HighAge I
	WHERE
		IsAnAlias = 0
UNION ALL
	-- excessive mileage
	SELECT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		7 AS PlanningCategoryID
	FROM
		dbo.GetInventoryPlanningCategories_ExcessiveMileage I
	WHERE
		IsAnAlias = 0
UNION ALL
	-- high mileage
	SELECT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		6 AS PlanningCategoryID
	FROM
		dbo.GetInventoryPlanningCategories_HighMileage I
	WHERE
		IsAnAlias = 0
UNION ALL
	-- red: poor performance
	SELECT	DISTINCT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		5 AS PlanningCategoryID
	FROM
		[IMT].dbo.GDLight L
	JOIN	dbo.GetInventoryPlanningCategories_Standard I ON
		(
				L.BusinessUnitID = I.BusinessUnitID
			AND	L.GroupingDescriptionID = I.GroupingDescriptionID
		)
	WHERE
		L.InventoryVehicleLightID = 1
	AND	(I.IsAnAlias = 0 OR I.AliasPlanningCategoryID = 5)
UNION ALL
	-- yellow: poor performance
	SELECT	DISTINCT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		4 AS PlanningCategoryID
	FROM
		[IMT].dbo.GDLight L
	JOIN	dbo.GetInventoryPlanningCategories_Standard I ON
		(
				L.BusinessUnitID = I.BusinessUnitID
			AND	L.GroupingDescriptionID = I.GroupingDescriptionID
		)
	JOIN	[IMT].dbo.DealerGridValues G ON
		(
				L.GridCell = G.IndexKey
			AND	L.BusinessUnitID = G.BusinessUnitID
		)
	WHERE
		L.InventoryVehicleLightID = 2
	AND	G.IndexKey IN (7,12,17)
	AND	(I.IsAnAlias = 0 OR I.AliasPlanningCategoryID = 4)
UNION ALL
	-- yellow: low to no sales history
	SELECT	DISTINCT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		3 AS PlanningCategoryID
	FROM
		[IMT].dbo.GDLight L
	JOIN	dbo.GetInventoryPlanningCategories_Standard I ON
		(
				L.BusinessUnitID = I.BusinessUnitID
			AND	L.GroupingDescriptionID = I.GroupingDescriptionID
		)
	JOIN	[IMT].dbo.DealerGridValues G ON
		(
				L.GridCell = G.IndexKey
			AND	L.BusinessUnitID = G.BusinessUnitID
		)
	WHERE
		L.InventoryVehicleLightID = 2
	AND	G.IndexKey BETWEEN 1 AND 5
	AND	(I.IsAnAlias = 0 OR I.AliasPlanningCategoryID = 3)
UNION ALL
	-- green: not optimal
	-- green: optimal with units of zero
	-- green: non-optimal year where model has optimal years
	SELECT	DISTINCT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		2 AS PlanningCategoryID
	FROM
		[IMT].dbo.GDLight L
	JOIN	dbo.GetInventoryPlanningCategories_Standard I ON
		(
				L.BusinessUnitID = I.BusinessUnitID
			AND	L.GroupingDescriptionID = I.GroupingDescriptionID
		)
	LEFT JOIN dbo.Inventory_A3 O ON
		(
				L.BusinessUnitID = O.BusinessUnitID
			AND	L.GroupingDescriptionID = O.GroupingDescriptionID
		)
	WHERE
		L.InventoryVehicleLightID = 3
	AND	(I.IsAnAlias = 0 OR I.AliasPlanningCategoryID = 2)
	AND 	(	(O.Units = 0 AND I.ModelYear = COALESCE(O.ModelYear,I.ModelYear))
		OR	(O.GroupingDescriptionID IS NULL)
		OR	(O.ModelYear IS NOT NULL AND I.ModelYear NOT IN (
			SELECT DISTINCT
				X.ModelYear
			FROM
				dbo.Inventory_A3 X
			WHERE
				I.BusinessUnitID = X.BusinessUnitID
			AND	I.GroupingDescriptionID = X.GroupingDescriptionID
		)))
UNION ALL
	-- green: optimal year or optimal model with no optimal years
	SELECT	DISTINCT
		I.BusinessUnitID, I.GroupingDescriptionID, I.InventoryID, I.ModelYear, I.Mileage, I.Active, I.DaysToSale, I.TotalGross, I.IsComparisonConfiguration,
		1 AS PlanningCategoryID
	FROM
		[IMT].dbo.GDLight L
	JOIN	dbo.GetInventoryPlanningCategories_Standard I ON
		(
				L.BusinessUnitID = I.BusinessUnitID
			AND	L.GroupingDescriptionID = I.GroupingDescriptionID
		)
	JOIN	dbo.Inventory_A3 O ON
		(
				L.BusinessUnitID = O.BusinessUnitID
			AND	L.GroupingDescriptionID = O.GroupingDescriptionID
		)
	WHERE
		L.InventoryVehicleLightID = 3
	AND	O.Units > 0
	AND	I.ModelYear = COALESCE(O.ModelYear,I.ModelYear)
	AND	(I.IsAnAlias = 0 OR I.AliasPlanningCategoryID = 1)
) T
GO

-- select count(*) from dbo.GetInventoryPlanningCategories

-- select count(*) from dbo.GetInventoryPlanningCategories where active = 1 and IsComparisonConfiguration = 0 --  57831, 17s
-- select count(*) from dbo.GetInventoryPlanningCategories where active = 0 and IsComparisonConfiguration = 0 -- 172743, 39s
-- select count(*) from dbo.GetInventoryPlanningCategories where active = 1 and IsComparisonConfiguration = 1 --  57913, 17s
-- select count(*) from dbo.GetInventoryPlanningCategories where active = 0 and IsComparisonConfiguration = 1 -- 172809, 34s
-- 
-- begin
-- 	declare @i table (InventoryID INT)
-- 	insert into @i (InventoryID)
-- 	select InventoryID from dbo.GetInventoryPlanningCategories where active = 1 and IsComparisonConfiguration = 0
-- 	select c.* from dbo.GetInventoryPlanningCategories c left join @i i on i.inventoryid = c.inventoryid where c.Active = 1 and c.IsComparisonConfiguration = 1 and i.inventoryid is null order by c.businessunitid, c.inventoryid
-- end

-- BusinessUnitID GroupingDescriptionID InventoryID ModelYear   Mileage     Active DaysToSale TotalGross  IsComparisonConfiguration PlanningCategoryID 
-- -------------- --------------------- ----------- ----------- ----------- ------ ---------- ----------- ------------------------- ------------------ 
-- 101546         101925                4635203     1997        121663      1      NULL       NULL        1                         7
-- 101548         101922                4635448     2000        58087       1      NULL       NULL        1                         8

-- select * from PlanningCategoryConfigurations where BusinessUnitID = 101546
-- select * from getinventoryplanningcategories where inventoryid = 4635203
-- select * from dbo.GetInventoryPlanningCategories_NeitherHighMileageNorHighAge where inventoryid = 4635203
-- select * from GetInventoryPlanningCategories_Standard where inventoryid = 4635203
-- select * from imt..gdlight where groupingdescriptionid = 101925 and BusinessUnitID = 101546
-- select * from imt..gdlight where BusinessUnitID = 101546
-- select * from IMT..BusinessUnit where BusinessUnitID in (101546,101548)
-- select * from getinventory where  BusinessUnitID in (101546,101548) and active = 1 order by businessunitid, inventoryid

-- --------------------------------------------------------------------
--                       Inventory Aggregate                         --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A8#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A8#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A8#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inventory_A8#1
GO

CREATE TABLE dbo.Inventory_A8#0(
	BusinessUnitID            INT NOT NULL,
	PlanningCategoryID        INT NOT NULL,
	IsComparisonConfiguration BIT NOT NULL,
	DaysToSale                INT NOT NULL,
	AverageProfit             INT NOT NULL,
	StdevProfit               INT NULL,
	NumberOfVehicles          INT NOT NULL,
	CONSTRAINT PK_Inventory_A8#0 PRIMARY KEY CLUSTERED (
		BusinessUnitID,
		PlanningCategoryID,
		IsComparisonConfiguration,
		DaysToSale
	)
)
GO

CREATE TABLE dbo.Inventory_A8#1(
	BusinessUnitID            INT NOT NULL,
	PlanningCategoryID        INT NOT NULL,
	IsComparisonConfiguration BIT NOT NULL,
	DaysToSale                INT NOT NULL,
	AverageProfit             INT NOT NULL,
	StdevProfit               INT NULL,
	NumberOfVehicles          INT NOT NULL,
	CONSTRAINT PK_Inventory_A8#1 PRIMARY KEY CLUSTERED (
		BusinessUnitID,
		PlanningCategoryID,
		IsComparisonConfiguration,
		DaysToSale
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inventory_A8]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Inventory_A8]
GO

CREATE VIEW dbo.Inventory_A8 AS SELECT * FROM Inventory_A8#1
GO

INSERT INTO dbo.ViewStatus (ViewName,TableIndex) VALUES ('Inventory_A8', 1)
GO

INSERT INTO [dbo].AggregateTables(
	[AggregateTableID],
	[TableName],
	[Description],
	[IsBuffered],
	[HasPrimaryKey],
	[Loader],
	[LoaderTypeID]
)
VALUES(
	11,
	'Inventory_A8',
	'Aggregated snapshot of used car inventory daya-to-sale versus average-profit pivoted on planning-category',
	1,
	1,
	'GetInventory_V8',
	3
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventory_V8]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetInventory_V8]
GO

-- days to sale versus profit per planning category
CREATE VIEW dbo.GetInventory_V8 (
	BusinessUnitID,
	PlanningCategoryID,
	IsComparisonConfiguration,
	DaysToSale,
	AverageProfit,
	StdevProfit,
	NumberOfVehicles
)
AS
SELECT
	I.BusinessUnitID,
	I.PlanningCategoryID,
	I.IsComparisonConfiguration,
	I.DaysToSale,
	AVG(I.TotalGross) AverageProfit,
	STDEV(I.TotalGross) StdevProfit,
	COUNT(*) NumberOfVehicles
FROM
	dbo.GetInventoryPlanningCategories I
WHERE
	I.Active = 0
GROUP BY
	I.BusinessUnitID,
	I.PlanningCategoryID,
	I.IsComparisonConfiguration,
	I.DaysToSale
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LoadInventory_A8]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LoadInventory_A8]
GO

CREATE PROCEDURE dbo.LoadInventory_A8
	-- no params
AS
	EXEC LoadTable 'Inventory_A8', '', 'SELECT * FROM dbo.GetInventory_V8'
GO

EXEC dbo.LoadInventory_A8 -- 28s exec loadhal

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UpdateInventory_A8]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[UpdateInventory_A8]
GO

CREATE PROCEDURE dbo.UpdateInventory_A8
	@BusinessUnitID INT
AS
	DECLARE @Keep VARCHAR(4000), @SQL VARCHAR(4000)
	SET @Keep = 'BusinessUnitID <> ' + CAST(@BusinessUnitID AS VARCHAR)
	SET @SQL = 'SELECT * FROM dbo.GetInventory_V8 WHERE BusinessUnitID = ' + CAST(@BusinessUnitID AS VARCHAR)
	EXEC LoadTable 'Inventory_A8', @Keep, @SQL
GO

-- EXEC UpdateInventory_A8 100216 -- 2s

-- --------------------------------------------------------------------
--                          Inventory Tables                         --
-- --------------------------------------------------------------------

--
-- An Inventory item belongs to a number of categories during its life span: it may start in category
-- 1 (Green Optimal) but later move into 8 (High Age) and then 9 (Excessive Age).  These age boundaries
-- are calculated upon import -- this saves the need for an offline nightly task to manage the state
-- of inventory items as they age.
--
-- If a planning category is removed from a business unit then rows from the this table cascade delete
-- as it is no longer valid. If a category is added to a business unit we need a trigger to re-evaluate
-- the rows in this table.
--

CREATE TABLE dbo.InventoryPlanningCategories (
	InventoryPlanningCategoryID    INT      IDENTITY(1,1) NOT NULL,
	InventoryID                    INT      NOT NULL,
	BusinessUnitPlanningCategoryID INT      NOT NULL,
	ValidFrom                      DATETIME NOT NULL,
	ValidUntil                     DATETIME NOT NULL,
	Invalidated                    DATETIME NULL,
	CONSTRAINT PK_InventoryPlanningCategory PRIMARY KEY NONCLUSTERED (
		InventoryPlanningCategoryID
	),
	CONSTRAINT FK_InventoryPlanningCategory_BusinessUnitPlanningCategory FOREIGN KEY (
		BusinessUnitPlanningCategoryID
	)
	REFERENCES dbo.BusinessUnitPlanningCategories (
		BusinessUnitPlanningCategoryID
	),
	CONSTRAINT UK_InventoryPlanningCategory UNIQUE (
		InventoryID,
		BusinessUnitPlanningCategoryID
	)
)
GO

--
-- Planning actions for an inventory item (within a planning category) for a planning age boundaries.
--

CREATE TABLE dbo.InventoryPlanningTasks (
	InventoryPlanningTaskID     INT IDENTITY(1,1) NOT NULL,
	InventoryPlanningCategoryID INT NOT NULL,
	PlanningCategoryTableCellID INT NOT NULL,
	PlanningActionID            INT NOT NULL,
	ValidFrom                   DATETIME NOT NULL,
	ValidUntil                  DATETIME NOT NULL,
	Satisfied                   DATETIME NULL,
	Invalidated                 DATETIME NULL,
	CONSTRAINT PK_InventoryPlanningTask PRIMARY KEY (
		InventoryPlanningTaskID
	),
	CONSTRAINT FK_InventoryPlanningTask_InventoryPlanningCategory FOREIGN KEY (
		InventoryPlanningCategoryID
	)
	REFERENCES InventoryPlanningCategories (
		InventoryPlanningCategoryID
	),
	CONSTRAINT FK_InventoryPlanningTask_PlanningCategoryTableCell FOREIGN KEY (
		PlanningCategoryTableCellID
	)
	REFERENCES PlanningCategoryTableCells (
		PlanningCategoryTableCellID
	),
	CONSTRAINT UK_InventoryPlanningTask UNIQUE (
		InventoryPlanningCategoryID,
		PlanningCategoryTableCellID,
		PlanningActionID
	)
)
GO

CREATE INDEX IDX_InventoryPlanningTasks_PlanningAction ON dbo.InventoryPlanningTasks (
	PlanningActionID
)
GO

-- --------------------------------------------------------------------
--        Stored Procedure to Populate the above Tables              --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ProcessPlanningCategoryAgeBoundaries]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[ProcessPlanningCategoryAgeBoundaries]
GO

CREATE PROCEDURE dbo.ProcessPlanningCategoryAgeBoundaries
	@BusinessUnitID int = NULL 
AS
	SET NOCOUNT ON

	DECLARE @RunDate DATETIME

	SET @RunDate = [IMT].dbo.ToDate(GETDATE())

	-- what the inventory planning categories should look like
	-- 12B * 58472 = 685KB
	DECLARE @IPC TABLE (
		BusinessUnitID     INT,
		InventoryID        INT,
		PlanningCategoryID INT
	)
	
	INSERT INTO @IPC (BusinessUnitID,InventoryID,PlanningCategoryID)
	SELECT
		BusinessUnitID,
		InventoryID,
		PlanningCategoryID
	FROM
		dbo.GetInventoryPlanningCategories
	WHERE
		Active = 1
	AND	IsComparisonConfiguration = 0
	AND	COALESCE(@BusinessUnitID, BusinessUnitID) = BusinessUnitID

	-- the inventory planning categories we need to turn off
	DECLARE @OFF TABLE (
		BusinessUnitID              INT,
		InventoryID                 INT,
		InventoryPlanningCategoryID INT
	)
	
	INSERT INTO @OFF (BusinessUnitID, InventoryID, InventoryPlanningCategoryID)
	SELECT
		B.BusinessUnitID,
		C.InventoryID,
		C.InventoryPlanningCategoryID
	FROM
		dbo.InventoryPlanningCategories C
	JOIN	dbo.BusinessUnitPlanningCategories B ON B.BusinessUnitPlanningCategoryID = C.BusinessUnitPlanningCategoryID
	LEFT JOIN @IPC I ON (
			C.InventoryID        = I.InventoryID
		AND	B.PlanningCategoryID = B.PlanningCategoryID
		AND	B.BusinessUnitID     = B.BusinessUnitID
		)
	WHERE
		B.IsComparisonConfiguration = 0
	AND	I.InventoryID IS NULL
	
	-- turn off the inventory planning categories
	DECLARE @CategoriesDisabled INT

	UPDATE	dbo.InventoryPlanningCategories
	SET	ValidUntil = @RunDate
	WHERE	InventoryPlanningCategoryID IN (SELECT InventoryPlanningCategoryID FROM @OFF)
	
	SET @CategoriesDisabled = @@ROWCOUNT
	
	-- turn off inventory planning tasks
	DECLARE @TasksDisabled INT
	
	UPDATE	dbo.InventoryPlanningTasks
	SET	ValidUntil = @RunDate
	WHERE	InventoryPlanningCategoryID IN (SELECT InventoryPlanningCategoryID FROM @OFF)
	
	SET @TasksDisabled = @@ROWCOUNT
	
	-- add new inventory planning categories
	DECLARE @CategoriesAdded INT

	INSERT INTO dbo.InventoryPlanningCategories (
		InventoryID,
		BusinessUnitPlanningCategoryID,
		ValidFrom,
		ValidUntil
	)
	SELECT
		C.InventoryID,
		B.BusinessUnitPlanningCategoryID,
		@RunDate,
		'2038-01-18'
	FROM
		@IPC C
	JOIN	dbo.BusinessUnitPlanningCategories B ON C.BusinessUnitID = B.BusinessUnitID AND B.PlanningCategoryID = C.PlanningCategoryID
	WHERE
		B.IsComparisonConfiguration = 0
	AND	NOT EXISTS (
			SELECT	1
			FROM	dbo.InventoryPlanningCategories X
			WHERE	X.BusinessUnitPlanningCategoryID = B.BusinessUnitPlanningCategoryID
			AND	X.InventoryID = C.InventoryID
		)
	
	SET @CategoriesAdded = @@ROWCOUNT
	
	-- add inventory planning tasks
	DECLARE @TasksAdded INT

	INSERT INTO dbo.InventoryPlanningTasks (
		InventoryPlanningCategoryID,
		PlanningCategoryTableCellID,
		PlanningActionID,
		ValidFrom,
		ValidUntil,
		Satisfied,
		Invalidated
	)
	SELECT
		IPC.InventoryPlanningCategoryID,
		CEL.PlanningCategoryTableCellID,
		ROW.PlanningActionID,
		@RunDate ValidFrom,
		CASE WHEN BPC.Age1 IS NULL THEN '2038-01-18' ELSE DATEADD(DD,BPC.Age1-BPC.Age0,@RunDate) END ValidUntil,
		NULL Satisfied,
		NULL Invalidated
	FROM
		dbo.BusinessUnitPlanningCategoryPlanningAgeBoundaries BPC
	JOIN	dbo.InventoryPlanningCategories IPC ON BPC.BusinessUnitPlanningCategoryID = IPC.BusinessUnitPlanningCategoryID
	JOIN	[IMT].dbo.Inventory I ON IPC.InventoryID = I.InventoryID
	JOIN	dbo.PlanningCategoryTableCols COL ON BPC.PlanningCategoryTableColID0 = COL.PlanningCategoryTableColID
	JOIN	dbo.PlanningCategoryTableCells CEL ON COL.PlanningCategoryTableColID = CEL.PlanningCategoryTableColID
	JOIN	dbo.PlanningCategoryTableRows ROW ON CEL.PlanningCategoryTableRowID = ROW.PlanningCategoryTableRowID
	WHERE
		DATEDIFF(DD,[IMT].dbo.ToDate(I.InventoryReceivedDate),@RunDate) BETWEEN BPC.Age0 AND COALESCE(BPC.Age1,1000)
	AND	CEL.Active = 1
	AND	NOT EXISTS (
			SELECT
				1
			FROM
				dbo.InventoryPlanningTasks T
			WHERE
				T.InventoryPlanningCategoryID = IPC.InventoryPlanningCategoryID
			AND	T.PlanningActionID            = ROW.PlanningActionID
			AND	T.PlanningCategoryTableCellID = CEL.PlanningCategoryTableCellID
		)
	AND	COALESCE(@BusinessUnitID, BPC.BusinessUnitID) = BPC.BusinessUnitID

	SET @TasksAdded = @@ROWCOUNT
	
	DECLARE @DEBUG INT
	SET @DEBUG = 1

	IF @DEBUG = 1
		SELECT	@CategoriesDisabled CategoriesDisabled,
			@CategoriesAdded    CategoriesAdded,
			@TasksDisabled      TasksDisabled,
			@TasksAdded         TasksAdded

GO

-- EXEC dbo.ProcessPlanningCategoryAgeBoundaries

-- to insert/update values in the inventory_planning table

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsertOrUpdateInventoryPlanningActionTypes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[InsertOrUpdateInventoryPlanningActionTypes]
GO

CREATE PROCEDURE dbo.InsertOrUpdateInventoryPlanningActionTypes
	-- no params
AS
	SET NOCOUNT ON
	
	DECLARE @RunDate DATETIME
	
	SET @RunDate = [IMT].dbo.ToDate(GETDATE())

	-- calculate candidate values for all active used inventory
	
	DECLARE @ActionCandidates TABLE (
		InventoryID               INT,
		CandidateForAdvertisement INT,
		CandidateForWholesale     INT
	)
	
	INSERT INTO @ActionCandidates (
		InventoryID,
		CandidateForAdvertisement,
		CandidateForWholesale
	)
	SELECT
		InventoryID,
		CASE WHEN DaysOnLot BETWEEN AdvertisementStart AND WholesaleStart-1 THEN 1 ELSE 0 END CandidateForAdvertisement,
		CASE WHEN DaysOnLot BETWEEN WholesaleStart AND 1000 THEN 1 ELSE 0 END CandidateForWholesale
	FROM (
		SELECT
			I.InventoryID,
			DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) DaysOnLot,
			CASE WHEN B.IsAdvertisementCandidate = 0 THEN 50000 ELSE B.AdvertisementCandidateFromDay END AdvertisementStart,
			CASE WHEN B.IsWholesaleCandidate = 0 THEN 50000 ELSE B.WholesaleCandidateFromDay END WholesaleStart
		FROM
			[IMT].dbo.Inventory I
		JOIN	[HAL].dbo.InventoryPlanningCategories C ON C.InventoryID = I.InventoryID
		JOIN	[HAL].dbo.BusinessUnitPlanningCategories B ON B.BusinessUnitPlanningCategoryID = C.BusinessUnitPlanningCategoryID
		WHERE
			I.InventoryActive = 1
		AND	I.InventoryType = 2
		AND	C.Invalidated IS NULL
		AND	B.IsComparisonConfiguration = 0
	) T
	
	-- update existing candidate values
	
	DECLARE @RowsUpdated INT
	
	UPDATE	Inventory_Planning
	SET	CandidateForAdvertisement = C.CandidateForAdvertisement,
		CandidateForWholesale = C.CandidateForWholesale
	FROM	Inventory_Planning
	JOIN	@ActionCandidates C ON Inventory_Planning.InventoryID = C.InventoryID
	WHERE	Inventory_Planning.CandidateForAdvertisement <> C.CandidateForAdvertisement
	OR	Inventory_Planning.CandidateForWholesale <> C.CandidateForWholesale
	
	SET @RowsUpdated = @@ROWCOUNT

	-- insert new candidate values

	DECLARE @RowsInserted INT
		
	INSERT INTO dbo.Inventory_Planning (
		InventoryID,
		CandidateForAdvertisement,
		CandidateForWholesale,
		PlanningActionTypeID
	)
	SELECT
		InventoryID,
		CandidateForAdvertisement,
		CandidateForWholesale,
		CASE WHEN CandidateForWholesale = 1 THEN 2 ELSE 1 END
	FROM
		@ActionCandidates C
	WHERE
		NOT EXISTS (
			SELECT 1 FROM dbo.Inventory_Planning I WHERE I.InventoryID = C.InventoryID
		)

	SET @RowsInserted = @@ROWCOUNT
	
	DECLARE @DEBUG INT
	
	SET @DEBUG = 1
	
	IF @DEBUG = 1
		SELECT @RowsInserted RowsInserted, @RowsUpdated RowsUpdated

GO

-- EXECUTE dbo.InsertOrUpdateInventoryPlanningActionTypes

-- to insert/update values in the inventory_planning table with data from nextgen

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UpdateInventoryPlanningActionTypesWithNextGenData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UpdateInventoryPlanningActionTypesWithNextGenData]
GO

CREATE PROCEDURE dbo.UpdateInventoryPlanningActionTypesWithNextGenData
	@BusinessUnitID INT = NULL
AS
	SET NOCOUNT ON
	
	DECLARE @RunDate DATETIME
	
	SET @RunDate = [IMT].dbo.ToDate(GETDATE())

	DECLARE @NextGenPlanningActionType TABLE (
		InventoryID INT,
		PlanningActionTypeID INT
	)
	
	INSERT INTO @NextGenPlanningActionType (InventoryID, PlanningActionTypeID)
	SELECT
		E.InventoryID,
		T.AIP_EventCategoryID
	FROM
		[IMT].dbo.AIP_UserEvent E
	JOIN	[IMT].dbo.AIP_EventType T ON E.AIP_EventTypeID = T.AIP_EventTypeID
	JOIN	(
			SELECT	I.InventoryID, I.PlanReminderDate, MAX(E.BeginDate) PlanReminderDate1
			FROM	[FLDW].dbo.InventoryActive I
				LEFT OUTER JOIN [IMT].dbo.AIP_Event E on (
						E.InventoryID = I.InventoryID
					AND	E.AIP_EventTypeID = 14
					AND	E.BeginDate < I.PlanReminderDate
				)
			GROUP
			BY	I.InventoryID, I.PlanReminderDate
		) PRD ON E.InventoryID = PRD.InventoryID
	WHERE
		T.AIP_EventCategoryID BETWEEN 1 AND 4
	AND	(
			(GETDATE() BETWEEN E.StartOn AND ISNULL(E.EndDate, '6/6/2079'))
		OR	(
				(PRD.PlanReminderDate1 BETWEEN E.StartOn AND GETDATE())
			OR
				(E.StartOn BETWEEN PRD.PlanReminderDate1 AND GETDATE())
			)
		)
	AND	COALESCE(@BusinessUnitID, E.BusinessUnitID) = E.BusinessUnitID
	GROUP BY
		E.InventoryID,
		T.AIP_EventCategoryID

	
	DECLARE @RowsUpdated INT
	
	UPDATE	Inventory_Planning
	SET	PlanningActionTypeID = N.PlanningActionTypeID
	FROM	Inventory_Planning
	JOIN	@NextGenPlanningActionType N ON Inventory_Planning.InventoryID = N.InventoryID
	
	SET @RowsUpdated = @@ROWCOUNT

	DECLARE @DEBUG INT
	
	SET @DEBUG = 1
	
	IF @DEBUG = 1
		SELECT @RowsUpdated RowsUpdated

GO

-- EXECUTE UpdateInventoryPlanningActionTypesWithNextGenData 100147

-- --------------------------------------------------------------------
--                      Third Party Entity Tables                    --
-- --------------------------------------------------------------------

CREATE TABLE dbo.LiveAuction_ThirdPartyEntity (
	LiveAuctionID INT         NOT NULL,
	City          VARCHAR(50) NULL,
	State         VARCHAR(50) NULL,
	CONSTRAINT PK_LiveAuction_ThirdPartyEntity PRIMARY KEY (
		LiveAuctionID
	)
)
GO

INSERT INTO dbo.LiveAuction_ThirdPartyEntity (LiveAuctionID)
SELECT ThirdPartyEntityID FROM [IMT].dbo.ThirdPartyEntity WHERE ThirdPartyEntityTypeID = 2
GO

CREATE TABLE dbo.Wholesaler_ThirdPartyEntity (
	WholesalerID    INT         NOT NULL,
	TelephoneNumber VARCHAR(50) NULL,
	CONSTRAINT PK_Wholesaler_ThirdPartyEntity PRIMARY KEY (
		WholesalerID
	)
)
GO

INSERT INTO dbo.Wholesaler_ThirdPartyEntity (WholesalerID)
SELECT ThirdPartyEntityID FROM [IMT].dbo.ThirdPartyEntity WHERE ThirdPartyEntityTypeID = 3
GO

-- --------------------------------------------------------------------
--                        Offline Command Table                      --
-- --------------------------------------------------------------------

-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OfflineCommandRequests]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
-- DROP TABLE dbo.OfflineCommandRequests
-- GO
-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OfflineCommands]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
-- DROP TABLE dbo.OfflineCommands
-- GO
-- 
-- CREATE TABLE dbo.OfflineCommands (
-- 	OfflineCommandID INT IDENTITY(1,1) NOT NULL,
-- 	Command          VARCHAR(255) NOT NULL,
-- 	CONSTRAINT PK_OfflineCommand PRIMARY KEY (
-- 		OfflineCommandID
-- 	)
-- )
-- GO
-- 
-- INSERT INTO dbo.OfflineCommands (Command) VALUES ('EXECUTE UpdateAppraisals_A2 business_unit_id')
-- INSERT INTO dbo.OfflineCommands (Command) VALUES ('EXECUTE UpdateInventory_A8 business_unit_id')
-- 
-- CREATE TABLE dbo.OfflineCommandRequests (
-- 	OfflineCommandRequestID INT IDENTITY(1,1) NOT NULL,
-- 	OfflineCommandID        INT NOT NULL,
-- 	BusinessUnitID          VARCHAR(255) NOT NULL,
-- 	CONSTRAINT PK_OfflineCommandRequest PRIMARY KEY (
-- 		OfflineCommandRequestID
-- 	),
-- 	CONSTRAINT FK_OfflineCommandRequest FOREIGN KEY (
-- 		OfflineCommandID
-- 	)
-- 	REFERENCES OfflineCommands (
-- 		OfflineCommandID
-- 	),
-- 	CONSTRAINT UK_OfflineCommandRequest UNIQUE (
-- 		BusinessUnitID,
-- 		OfflineCommandID
-- 	)
-- )
-- GO
-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[offline_commands]') and OBJECTPROPERTY(id, N'IsView') = 1)
-- drop view [dbo].[offline_commands]
-- GO
-- 
-- CREATE VIEW dbo.offline_commands (
-- 	[id],
-- 	[command]
-- )
-- AS
-- SELECT
-- 	OfflineCommandID,
-- 	Command
-- FROM
-- 	dbo.OfflineCommands
-- GO
-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[offline_command_requests]') and OBJECTPROPERTY(id, N'IsView') = 1)
-- drop view [dbo].[offline_command_requests]
-- GO
-- 
-- CREATE VIEW dbo.offline_command_requests (
-- 	[id],
-- 	[offline_command_id],
-- 	[business_unit_id]
-- )
-- AS
-- SELECT
-- 	OfflineCommandRequestID,
-- 	OfflineCommandID,
-- 	BusinessUnitID
-- FROM
-- 	dbo.OfflineCommandRequests
-- GO

-- --------------------------------------------------------------------
--                        ActiveRecord Views                         --
-- --------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_category_lights]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_category_lights]
GO

CREATE VIEW dbo.planning_category_lights (
	[id],
	[name]
)
AS
SELECT
	[PlanningCategoryLightID],
	[Name]
FROM
	dbo.PlanningCategoryLights
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_categories]
GO

CREATE VIEW dbo.planning_categories (
	[id],
	[planning_category_light_id],
	[name],
	[rank],
	[can_be_aliased]
)
AS
SELECT
	[PlanningCategoryID],
	[PlanningCategoryLightID],
	[Name],
	[Rank],
	[CanBeAliased]
FROM
	dbo.PlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_action_types]
GO

CREATE VIEW dbo.planning_action_types (
	[id],
	[name],
	[camel_case_name],
	[underscore_name],
	[rank]
)
AS
SELECT
	PlanningActionTypeID,
	[Name],
	CamelCaseName,
	UnderscoreName,
	Rank
FROM
	dbo.PlanningActionTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_actions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_actions]
GO

CREATE VIEW dbo.planning_actions (
	[id],
	[planning_action_type_id],
	[name],
	[camel_case_name],
	[underscore_name],
	[rank]
)
AS
SELECT
	PlanningActionID,
	PlanningActionTypeID,
	[Name],
	CamelCaseName,
	UnderscoreName,
	Rank
FROM
	dbo.PlanningActions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_actions_third_party_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_actions_third_party_types]
GO

CREATE VIEW dbo.planning_actions_third_party_types (
	[planning_action_id],
	[third_party_type_id]
)
AS
SELECT
	PlanningActionID,
	ThirdPartyEntityTypeID
FROM
	dbo.PlanningActionsThirdPartyEntityTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_action_defaults]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_action_defaults]
GO

CREATE VIEW dbo.planning_action_defaults (
	[id],
	[business_unit_id],
	[planning_action_id],
	[rank],
	[empty_value],
	[active]
)
AS
SELECT
	PlanningActionDefaultID,
	BusinessUnitID,
	PlanningActionID,
	Rank,
	EmptyValue,
	Active
FROM
	dbo.PlanningActionDefaults
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_acts]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_acts]
GO

CREATE VIEW dbo.planning_acts (
	[id],
	[planning_action_id],
	[business_unit_id],
	[inventory_id],
	[inventory_planning_task_id],
	[notes],
	[include_in_plan_until],
	[created]
)
AS
SELECT
	PlanningActID,
	PlanningActionID,
	BusinessUnitID,
	InventoryID,
	InventoryPlanningTaskID,
	Notes,
	IncludeInPlanUntil,
	Created
FROM
	dbo.PlanningActs
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_age_buckets]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_age_buckets]
GO

CREATE VIEW dbo.planning_age_buckets(
	[id],
	[business_unit_id],
	[range_id],
	[name],
	[low],
	[high],
	[lights],
	[description],
	[position]
)
AS
SELECT
	COALESCE(IBR.InventoryBucketRangeID, IBR1.InventoryBucketRangeID, IBR2.InventoryBucketRangeID, IBR3.InventoryBucketRangeID, IBR4.InventoryBucketRangeID) InventoryBucketRangeID,
	BU.BusinessUnitID,
	COALESCE(IBR.RangeID, IBR1.RangeID, IBR2.RangeID, IBR3.RangeID, IBR4.RangeID) RangeID,
	COALESCE(IBR.Description, IBR1.Description, IBR2.Description, IBR3.Description, IBR4.Description) Description,
	COALESCE(IBR.Low, IBR1.Low, IBR2.Low, IBR3.Low, IBR4.Low) Low,
	COALESCE(IBR.High, IBR1.High, IBR2.High, IBR3.High, IBR4.High) High,
	COALESCE(IBR.Lights, IBR1.Lights, IBR2.Lights, IBR3.Lights, IBR4.Lights) Lights,
	COALESCE(IBR.LongDescription, IBR1.LongDescription, IBR2.LongDescription, IBR3.LongDescription, IBR4.LongDescription) LongDescription,
	COALESCE(IBR.SortOrder, IBR1.SortOrder, IBR2.SortOrder, IBR3.SortOrder, IBR4.SortOrder) SortOrder
FROM
	[IMT].dbo.BusinessUnit BU
	LEFT JOIN [IMT].dbo.InventoryBucketRanges IBR ON IBR.InventoryBucketID = 4 AND BU.BusinessUnitID = IBR.BusinessUnitID
	LEFT JOIN (	[IMT].dbo.BusinessUnitRelationship BUR1
			LEFT JOIN [IMT].dbo.InventoryBucketRanges IBR1 ON IBR1.InventoryBucketID = 4 AND BUR1.ParentID = IBR1.BusinessUnitID
			) ON BU.BusinessUnitID = BUR1.BusinessUnitID AND IBR.InventoryBucketRangeID IS NULL

	LEFT JOIN (	[IMT].dbo.BusinessUnitRelationship BUR2
			LEFT JOIN [IMT].dbo.InventoryBucketRanges IBR2 ON IBR2.InventoryBucketID = 4 AND BUR2.ParentID = IBR2.BusinessUnitID
			) ON BUR1.ParentID = BUR2.BusinessUnitID AND IBR1.InventoryBucketRangeID IS NULL

	LEFT JOIN (	[IMT].dbo.BusinessUnitRelationship BUR3
			LEFT JOIN [IMT].dbo.InventoryBucketRanges IBR3 ON IBR3.InventoryBucketID = 4 AND BUR3.ParentID = IBR3.BusinessUnitID
			) ON BUR2.ParentID = BUR3.BusinessUnitID AND IBR2.InventoryBucketRangeID IS NULL

	LEFT JOIN (	[IMT].dbo.BusinessUnitRelationship BUR4
			LEFT JOIN [IMT].dbo.InventoryBucketRanges IBR4 ON IBR4.InventoryBucketID = 4 AND BUR4.ParentID = IBR4.BusinessUnitID
			) ON BUR3.ParentID = BUR4.BusinessUnitID AND IBR3.InventoryBucketRangeID IS NULL
WHERE
	BusinessUnitTypeID = 4
AND	COALESCE(IBR.InventoryBucketRangeID, IBR1.InventoryBucketRangeID, IBR2.InventoryBucketRangeID, IBR3.InventoryBucketRangeID, IBR4.InventoryBucketRangeID) IS NOT NULL
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[third_parties]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[third_parties]
GO

CREATE VIEW dbo.third_parties (
	[id],
	[business_unit_id],
	[third_party_type_id],
	[name]
)
AS
SELECT
	ThirdPartyEntityID,
	BusinessUnitID,
	ThirdPartyEntityTypeID,
	Name
FROM
	[IMT].dbo.ThirdPartyEntity
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[third_party_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[third_party_types]
GO

CREATE VIEW dbo.third_party_types (
	[id],
	[name]
)
AS
SELECT
	ThirdPartyEntityTypeID,
	Description
FROM
	[IMT].dbo.ThirdPartyEntityType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[live_auctions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[live_auctions]
GO

CREATE VIEW dbo.live_auctions (
	[id],
	[city],
	[state]
)
AS
SELECT
	LiveAuctionID,
	City,
	State
FROM
	dbo.LiveAuction_ThirdPartyEntity
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[wholesalers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[wholesalers]
GO

CREATE VIEW dbo.wholesalers (
	[id],
	[telephone_number]
)
AS
SELECT
	WholesalerID,
	TelephoneNumber
FROM
	dbo.Wholesaler_ThirdPartyEntity
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[internet_publishers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[internet_publishers]
GO

CREATE VIEW dbo.internet_publishers (
	[id],
	[data_feed_code]
)
AS
SELECT
	InternetAdvertiserID,
	DatafeedCode
FROM
	[IMT].dbo.InternetAdvertiser_ThirdPartyEntity
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[internet_publishers_business_units]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[internet_publishers_business_units]
GO

CREATE VIEW dbo.internet_publishers_business_units (
	[internet_publisher_id],
	[business_unit_id]
)
AS
SELECT
	InternetAdvertiserID,
	BusinessUnitID
FROM
	[IMT].dbo.InternetAdvertiserDealership
WHERE
	IsLive = 1
AND	Active = 1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[print_publishers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[print_publishers]
GO

CREATE VIEW dbo.print_publishers (
	[id],
	[email],
	[fax],
	[include_vehicle_price],
	[book_id]
)
AS
SELECT
	PrintAdvertiserID,
	ContactEmail,
	FaxNumber,
	DisplayPrice,
	VehicleOptionThirdPartyID
FROM
	[IMT].dbo.PrintAdvertiser_ThirdPartyEntity P
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[print_publisher_print_component_list_items]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[print_publisher_print_component_list_items]
GO

CREATE VIEW dbo.print_publisher_print_component_list_items (
	[id],
	[print_publisher_id],
	[print_component_id],
	[position],
	[active]
)
AS
SELECT
	PrintAdvertiserTextOptionDefaultID,
	PrintAdvertiserID,
	PrintAdvertiserTextOptionID,
	DisplayOrder,
	Status
FROM
	[IMT].dbo.PrintAdvertiserTextOptionDefault
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[print_components]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[print_components]
GO

CREATE VIEW dbo.print_components (
	[id],
	[name]
)
AS
SELECT
	[PrintAdvertiserTextOptionID],
	[Description]
FROM
	[IMT].dbo.PrintAdvertiserTextOption
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_unit_planning_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[business_unit_planning_categories]
GO

CREATE VIEW dbo.business_unit_planning_categories (
	[id],
	[business_unit_id],
	[planning_category_id],
	[active],
	[is_an_alias],
	[alias_planning_category_id],
	[lower_threshold],
	[upper_threshold],
	[is_advertisement_candidate],
	[advertisement_candidate_from_day],
	[is_wholesale_candidate],
	[wholesale_candidate_from_day],
	[is_comparison_configuration]
)
AS
SELECT
	BusinessUnitPlanningCategoryID,
	BusinessUnitID,
	PlanningCategoryID,
	Active,
	IsAnAlias,
	AliasPlanningCategoryID,
	LowerThreshold,
	UpperThreshold,
	IsAdvertisementCandidate,
	AdvertisementCandidateFromDay,
	IsWholesaleCandidate,
	WholesaleCandidateFromDay,
	IsComparisonConfiguration
FROM
	dbo.BusinessUnitPlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_category_configurations]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[planning_category_configurations]
GO

CREATE VIEW dbo.planning_category_configurations (
	[id],
	[business_unit_id],
	[has_excessive_mileage_category],
	[has_high_mileage_category],
	[high_mileage_lower_threshold],
	[high_mileage_upper_threshold],
	[is_high_mileage_category_an_alias],
	[high_mileage_category_alias],
	[is_excessive_mileage_category_an_alias],
	[excessive_mileage_category_alias],
	[has_excessive_age_category],
	[has_high_age_category],
	[high_age_lower_threshold],
	[high_age_upper_threshold],
	[is_high_age_category_an_alias],
	[high_age_category_alias],
	[is_excessive_age_category_an_alias],
	[excessive_age_category_alias],
	[is_comparison_configuration]
)
AS
SELECT
	CAST(BusinessUnitID AS VARCHAR) + '_' + CAST(IsComparisonConfiguration AS VARCHAR),
	BusinessUnitID,
	HasExcessiveMileageCategory,
	HasHighMileageCategory,
	HighMileageLowerThreshold,
	HighMileageUpperThreshold,
	IsHighMileageCategoryAnAlias,
	HighMileageCategoryAlias,
	IsExcessiveMileageCategoryAnAlias,
	ExcessiveMileageCategoryAlias,
	HasExcessiveAgeCategory,
	HasHighAgeCategory,
	HighAgeLowerThreshold,
	HighAgeUpperThreshold,
	IsHighAgeCategoryAnAlias,
	HighAgeCategoryAlias,
	IsExcessiveAgeCategoryAnAlias,
	ExcessiveAgeCategoryAlias,
	IsComparisonConfiguration
FROM
	dbo.PlanningCategoryConfigurations
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_category_tables]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_category_tables]
GO

CREATE VIEW dbo.planning_category_tables (
	[id],
	[business_unit_id],
	[planning_category_id]
)
AS
SELECT
	PlanningCategoryTableID,
	BusinessUnitID,
	PlanningCategoryID
FROM
	dbo.PlanningCategoryTables
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_category_table_rows]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_category_table_rows]
GO

CREATE VIEW dbo.planning_category_table_rows (
	[id],
	[planning_category_table_id],
	[planning_action_id],
	[rindex]
)
AS
SELECT
	PlanningCategoryTableRowID,
	PlanningCategoryTableID,
	PlanningActionID,
	RIndex
FROM
	dbo.PlanningCategoryTableRows
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_category_table_cols]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_category_table_cols]
GO

CREATE VIEW dbo.planning_category_table_cols (
	[id],
	[planning_category_table_id],
	[age],
	[cindex]
)
AS
SELECT
	PlanningCategoryTableColID,
	PlanningCategoryTableID,
	Age,
	CIndex
FROM
	dbo.PlanningCategoryTableCols
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[planning_category_table_cells]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[planning_category_table_cells]
GO

CREATE VIEW dbo.planning_category_table_cells (
	[id],
	[planning_category_table_row_id],
	[planning_category_table_col_id],
	[planning_action_default_id],
	[active],
	[rindex],
	[cindex]
)
AS
SELECT
	E.PlanningCategoryTableCellID,
	E.PlanningCategoryTableRowID,
	E.PlanningCategoryTableColID,
	E.PlanningActionDefaultID,
	E.Active,
	R.RIndex,
	C.CIndex
FROM
	dbo.PlanningCategoryTableCells E
JOIN	dbo.PlanningCategoryTableRows R ON E.PlanningCategoryTableRowID = R.PlanningCategoryTableRowID
JOIN	dbo.PlanningCategoryTableCols C ON E.PlanningCategoryTableColID = C.PlanningCategoryTableColID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[business_unit_planning_category_planning_age_boundaries]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[business_unit_planning_category_planning_age_boundaries]
GO

CREATE VIEW dbo.business_unit_planning_category_planning_age_boundaries (
	[id],
	[business_unit_planning_category_id],
	[business_unit_id],
	[planning_category_id],
	[planning_category_table_col_id_0],
	[planning_category_table_col_id_1],
	[age_0],
	[age_1]
)
AS
SELECT
	CAST(BusinessUnitPlanningCategoryID AS VARCHAR) + '_' + CAST(BusinessUnitID AS VARCHAR) + '_' + CAST(PlanningCategoryID AS VARCHAR) + '_' + CAST(PlanningCategoryTableColID0 AS VARCHAR) + '_' + CAST(PlanningCategoryTableColID1 AS VARCHAR),
	BusinessUnitPlanningCategoryID,
	BusinessUnitID,
	PlanningCategoryID,
	PlanningCategoryTableColID0,
	PlanningCategoryTableColID1,
	Age0,
	Age1
FROM
	dbo.BusinessUnitPlanningCategoryPlanningAgeBoundaries
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_sales_data]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_sales_data]
GO

CREATE VIEW dbo.inventory_sales_data (
	[id],
	business_unit_id,
	planning_category_id,
	is_comparison_configuration,
	days_to_sale,
	average_profit,
	stdev_profit,
	number_of_vehicles
)
AS
SELECT
	CAST(BusinessUnitID AS VARCHAR) + '_' + CAST(PlanningCategoryID AS VARCHAR) + '_' + CAST(DaysToSale AS VARCHAR),
	BusinessUnitID,
	PlanningCategoryID,
	IsComparisonConfiguration,
	DaysToSale,
	AverageProfit,
	StdevProfit,
	NumberOfVehicles
FROM
	dbo.Inventory_A8
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_planning_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_planning_categories]
GO

CREATE VIEW dbo.inventory_planning_categories(
	[id],
	[inventory_id],
	[business_unit_planning_category_id],
	[valid_from],
	[valid_until],
	[invalidated]
)
AS
SELECT
	InventoryPlanningCategoryID,
	InventoryID,
	BusinessUnitPlanningCategoryID,
	ValidFrom,
	ValidUntil,
	Invalidated
FROM
	dbo.InventoryPlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_planning_tasks]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_planning_tasks]
GO

CREATE VIEW dbo.inventory_planning_tasks(
	[id],
	[inventory_planning_category_id],
	[planning_category_table_cell_id],
	[planning_action_id],
	[valid_from],
	[valid_until],
	[satisfied],
	[invalidated]
)
AS
SELECT
	InventoryPlanningTaskID,
	InventoryPlanningCategoryID,
	PlanningCategoryTableCellID,
	PlanningActionID,
	ValidFrom,
	ValidUntil,
	Satisfied,
	Invalidated
FROM
	dbo.InventoryPlanningTasks
GO

--
-- try and move away from these views
--

-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_planning_actions]') and OBJECTPROPERTY(id, N'IsView') = 1)
-- drop view [dbo].[inventory_planning_actions]
-- GO
-- 
-- CREATE VIEW dbo.inventory_planning_actions (
-- 	inventory_item_id,
-- 	planning_action_id
-- )
-- AS
-- SELECT
-- 	c.inventory_id,
-- 	t.planning_action_id
-- FROM
-- 	inventory_planning_tasks t
-- JOIN	inventory_planning_categories c on c.id = t.inventory_planning_category_id
-- WHERE
-- 	t.valid_until > GETDATE()
-- AND	t.satisfied IS NULL
-- AND	t.invalidated IS NULL
-- GO
-- 
-- if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_items_planning_actions]') and OBJECTPROPERTY(id, N'IsView') = 1)
-- drop view [dbo].[inventory_items_planning_actions]
-- GO
-- 
-- CREATE VIEW dbo.inventory_items_planning_actions (
-- 	inventory_item_id,
-- 	planning_action_id
-- )
-- AS
-- SELECT
-- 	c.inventory_id,
-- 	t.planning_action_id
-- FROM
-- 	inventory_planning_tasks t
-- JOIN	inventory_planning_categories c on c.id = t.inventory_planning_category_id
-- WHERE
-- 	t.valid_until > GETDATE()
-- AND	t.satisfied IS NULL
-- AND	t.invalidated IS NULL
-- GO

-- USE IMT
-- INSERT INTO [IMT].[dbo].[ThirdPartyEntityType]([ThirdPartyEntityTypeID], [Description])
-- VALUES(5, 'Internet Auction')
-- GO
