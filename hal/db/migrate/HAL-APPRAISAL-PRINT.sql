
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletScheduleInterval') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletScheduleInterval
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletSchedule') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletSchedule
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletPreference') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletPreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletSummary') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletSummary
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBooklet') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBooklet
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].FutureTask') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.FutureTask
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].FutureTaskStatus') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.FutureTaskStatus
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].FutureTaskType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.FutureTaskType
GO

CREATE TABLE dbo.AppraisalReviewBookletSchedule (
	AppraisalReviewBookletScheduleID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                   INT               NOT NULL,
	HourOfDay                        INT               NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookletSchedule PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletScheduleID
	),
	CONSTRAINT UK_AppraisalReviewBookletSchedule_BusinessUnit UNIQUE CLUSTERED (
		BusinessUnitID
	),
	CONSTRAINT CK_AppraisalReviewBookletSchedule_HourOfDay CHECK (
		HourOfDay BETWEEN 0 AND 23
	)
)
GO

CREATE TABLE dbo.AppraisalReviewBookletScheduleInterval (
	AppraisalReviewBookletScheduleID INT NOT NULL,
	IntervalID                       INT NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookletScheduleInterval PRIMARY KEY CLUSTERED (
		AppraisalReviewBookletScheduleID,
		IntervalID
	),
	CONSTRAINT FK_AppraisalReviewBookletScheduleInterval_AppraisalReviewBookletSchedule FOREIGN KEY (
		AppraisalReviewBookletScheduleID
	)
	REFERENCES AppraisalReviewBookletSchedule (
		AppraisalReviewBookletScheduleID
	),
	CONSTRAINT FK_AppraisalReviewBookletScheduleInterval_IntervalID FOREIGN KEY (
		IntervalID
	)
	REFERENCES Interval (
		IntervalID
	)
)
GO

CREATE TABLE dbo.AppraisalReviewBookletPreference (
	AppraisalReviewBookletPreferenceID    INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                        INT               NOT NULL,
	IncludeAppraisalReviewSummary         BIT               NOT NULL,
	IncludeAppraisalReviewDetail          BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpSummary BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpDetail  BIT               NOT NULL,
	EmailNotification                     BIT               NOT NULL,
	EmailAddress                          VARCHAR(255)      NULL,
	Created                               DATETIME          NOT NULL,
	LastModified                          DATETIME          NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookletPreference PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletPreferenceID
	),
	CONSTRAINT UK_AppraisalReviewBookletPreference_BusinessUnit UNIQUE CLUSTERED (
		BusinessUnitID
	),
	CONSTRAINT CK_AppraisalReviewBookletPreference_LastModified CHECK (
		LastModified >= Created
	),
	CONSTRAINT CK_AppraisalReviewBookletPreference_EmailAddress CHECK (
		(EmailNotification = 0) OR (EmailNotification = 1 AND EmailAddress IS NOT NULL)
	)
)
GO

CREATE TABLE dbo.FutureTaskStatus (
	[FutureTaskStatusID] INT          NOT NULL,
	[Name]               VARCHAR(255) NOT NULL,
	CONSTRAINT PK_FutureTaskStatus PRIMARY KEY CLUSTERED (
		[FutureTaskStatusID]
	),
	CONSTRAINT FK_FutureTaskStatus_Name UNIQUE NONCLUSTERED (
		[Name]
	)
)
GO

INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (1, 'Submitted')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (2, 'Accepted')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (3, 'Processing')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (4, 'Error')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (5, 'Completed')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (6, 'Canceled')
GO

CREATE TABLE dbo.FutureTaskType (
	[FutureTaskTypeID] INT          NOT NULL,
	[Name]             VARCHAR(255) NOT NULL,
	CONSTRAINT PK_FutureTaskType PRIMARY KEY CLUSTERED (
		[FutureTaskTypeID]
	),
	CONSTRAINT FK_FutureTaskType_Name UNIQUE NONCLUSTERED (
		[Name]
	)
)
GO

INSERT INTO dbo.FutureTaskType ([FutureTaskTypeID], [Name]) VALUES (1, 'Appraisal Review Booklet')
GO

CREATE TABLE dbo.FutureTask (
	FutureTaskID         INT IDENTITY(1,1) NOT NULL,
	FutureTaskTypeID     INT NOT NULL,
	FutureTaskStatusID   INT NOT NULL,
	BusinessUnitID       INT NOT NULL,
	MemberID             INT NULL,
	Title                VARCHAR(255) NOT NULL,
	UnitsOfWork          INT NOT NULL,
	UnitsOfWorkCompleted INT NOT NULL,
	Created              DATETIME NOT NULL,
	Commenced            DATETIME NULL,
	Completed            DATETIME NULL,
	Acknowledged         DATETIME NULL,
	CONSTRAINT PK_FutureTask PRIMARY KEY CLUSTERED (
		FutureTaskID
	),
	CONSTRAINT FK_FutureTask_FutureTaskType FOREIGN KEY (
		FutureTaskTypeID
	)
	REFERENCES FutureTaskType (
		FutureTaskTypeID
	),
	CONSTRAINT FK_FutureTask_FutureTaskStatus FOREIGN KEY (
		FutureTaskStatusID
	)
	REFERENCES FutureTaskStatus (
		FutureTaskStatusID
	),
	CONSTRAINT CK_FutureTask_UnitsOfWorkCompleted CHECK (
		UnitsOfWorkCompleted >= UnitsOfWork
	),
	CONSTRAINT CK_FutureTask_Commenced CHECK (
		Commenced IS NULL OR Commenced >= Created
	),
	CONSTRAINT CK_FutureTask_Completed CHECK (
		Completed IS NULL OR Completed >= Commenced
	),
	CONSTRAINT CK_FutureTask_Acknowledged CHECK (
		Acknowledged IS NULL OR Acknowledged >= Completed
	)
)
GO

CREATE TABLE dbo.AppraisalReviewBooklet (
	AppraisalReviewBookletID              INT IDENTITY(1,1) NOT NULL,
	FutureTaskID                          INT               NOT NULL,
	Booklet                               IMAGE             NULL,
	CONSTRAINT PK_AppraisalReviewBooklet PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletID
	),
	CONSTRAINT FK_AppraisalReviewBooklet_FutureTask FOREIGN KEY (
		FutureTaskID
	)
	REFERENCES FutureTask (
		FutureTaskID
	),
	CONSTRAINT UK_AppraisalReviewBooklet_FutureTask UNIQUE CLUSTERED (
		FutureTaskID
	)
)
GO

CREATE TABLE dbo.AppraisalReviewBookletSummary (
	AppraisalReviewBookletSummaryID       INT IDENTITY(1,1) NOT NULL,
	AppraisalReviewBookletID              INT               NOT NULL,
	NumberOfAppraisalReviews              INT               NULL,
	NumberOfAppraisalReviewFollowUps      INT               NULL,
	IncludeAppraisalReviewSummary         BIT               NOT NULL,
	IncludeAppraisalReviewDetail          BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpSummary BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpDetail  BIT               NOT NULL,
	EmailNotification                     BIT               NOT NULL,
	EmailAddress                          VARCHAR(255)      NULL,
	NumberOfPages                         INT               NULL,
	SizeOfBooklet                         INT               NULL,
	CONSTRAINT PK_AppraisalReviewBookletSummary PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletSummaryID
	),
	CONSTRAINT FK_AppraisalReviewBookletSummary_AppraisalReviewBooklet FOREIGN KEY (
		AppraisalReviewBookletID
	)
	REFERENCES AppraisalReviewBooklet (
		AppraisalReviewBookletID
	),
	CONSTRAINT CK_AppraisalReviewBookletSummary_EmailAddress CHECK (
		(EmailNotification = 0) OR (EmailNotification = 1 AND EmailAddress IS NOT NULL)
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].appraisal_review_booklet_schedules') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].appraisal_review_booklet_schedules
GO

CREATE VIEW dbo.appraisal_review_booklet_schedules (
	[id],
	[business_unit_id],
	[hour_of_day]
)
AS
SELECT
	AppraisalReviewBookletScheduleID,
	BusinessUnitID,
	HourOfDay
FROM
	dbo.AppraisalReviewBookletSchedule
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].appraisal_review_booklet_schedules_intervals') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].appraisal_review_booklet_schedules_intervals
GO

CREATE VIEW dbo.appraisal_review_booklet_schedules_intervals (
	[appraisal_review_booklet_schedule_id],
	[interval_id]
)
AS
SELECT
	AppraisalReviewBookletScheduleID,
	IntervalID
FROM
	dbo.AppraisalReviewBookletScheduleInterval
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].appraisal_review_booklet_preferences') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].appraisal_review_booklet_preferences
GO

CREATE VIEW dbo.appraisal_review_booklet_preferences (
	[id],
	[business_unit_id],
	[include_appraisal_review_summary],
	[include_appraisal_review_detail],
	[include_appraisal_review_follow_up_summary],
	[include_appraisal_review_follow_up_detail],
	[email_notification],
	[email_address],
	[created],
	[last_modified]
)
AS
SELECT
	AppraisalReviewBookletPreferenceID,
	BusinessUnitID,
	IncludeAppraisalReviewSummary,
	IncludeAppraisalReviewDetail,
	IncludeAppraisalReviewFollowUpSummary,
	IncludeAppraisalReviewFollowUpDetail,
	EmailNotification,
	EmailAddress,
	Created,
	LastModified
FROM
	dbo.AppraisalReviewBookletPreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].future_task_statuses') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].future_task_statuses
GO

CREATE VIEW dbo.future_task_statuses (
	[id],
	[name]
)
AS
SELECT
	[FutureTaskStatusID],
	[Name]
FROM
	dbo.FutureTaskStatus
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].future_task_types') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].future_task_types
GO

CREATE VIEW dbo.future_task_types (
	[id],
	[name]
)
AS
SELECT
	[FutureTaskTypeID],
	[Name]
FROM
	dbo.FutureTaskType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].future_tasks') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].future_tasks
GO

CREATE VIEW dbo.future_tasks (
	[id],
	[future_task_type_id],
	[future_task_status_id],
	[business_unit_id],
	[member_id],
	[title],
	[units_of_work],
	[units_of_work_completed],
	[created],
	[commenced],
	[completed],
	[acknowledged]
)
AS
SELECT
	[FutureTaskTypeID],
	[FutureTaskTypeID],
	[FutureTaskStatusID],
	[BusinessUnitID],
	[MemberID],
	[Title],
	[UnitsOfWork],
	[UnitsOfWorkCompleted],
	[Created],
	[Commenced],
	[Completed],
	[Acknowledged]
FROM
	dbo.FutureTask
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].appraisal_review_booklets') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].appraisal_review_booklets
GO

CREATE VIEW dbo.appraisal_review_booklets (
	[id],
	[future_task_id],
	[booklet]
)
AS
SELECT
	[AppraisalReviewBookletID],
	[FutureTaskID],
	[Booklet]
FROM
	dbo.AppraisalReviewBooklet
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].appraisal_review_booklet_summaries') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].appraisal_review_booklet_summaries
GO

CREATE VIEW dbo.appraisal_review_booklet_summaries (
	[id],
	[appraisal_review_booklet_id],
	[number_of_appraisal_reviews],
	[number_of_appraisal_review_follow_ups],
	[include_appraisal_review_summary],
	[include_appraisal_review_detail],
	[include_appraisal_review_follow_up_summary],
	[include_appraisal_review_follow_up_detail],
	[email_notification],
	[email_address],
	[number_of_pages],
	[size_of_booklet]
)
AS
SELECT
	[AppraisalReviewBookletSummaryID],
	[AppraisalReviewBookletID],
	[NumberOfAppraisalReviews],
	[NumberOfAppraisalReviewFollowUps],
	[IncludeAppraisalReviewSummary],
	[IncludeAppraisalReviewDetail],
	[IncludeAppraisalReviewFollowUpSummary],
	[IncludeAppraisalReviewFollowUpDetail],
	[EmailNotification],
	[EmailAddress],
	[NumberOfPages],
	[SizeOfBooklet]
FROM
	dbo.AppraisalReviewBookletSummary
GO
