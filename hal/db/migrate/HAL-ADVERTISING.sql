
USE HAL

--
-- Note to all who use these tables: I use the plural for the table names where possible to keep the naming
-- similar to ActiveRecord in RoR.  Where the name is not compound (e.g. Color) I have used the singular for
-- the DB and the plural for RoR. This is a bit annoying to me -- I hope this does not annoy you too much.
-- SBW
--

--
-- DROP TABLES
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CampaignRuns]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.CampaignRuns
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Campaign]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Campaign
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Interval]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Interval
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[IntervalTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.IntervalTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CampaignTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.CampaignTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchResultsSearchCriteria]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchResultsSearchCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchResults]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchResults
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleListItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleListItems
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleLists]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleLists
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleListTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleListTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaSearchCriteriaInequalities]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaSearchCriteriaInequalities
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaInequalities]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaInequalities
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaPlanningActionTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaPlanningActionTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaPlanningActions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaPlanningActions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaPlanningCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaPlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaInventoryVehicleLights]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaInventoryVehicleLights
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaVehicleLines]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaVehicleLines
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaVehicleMakes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaVehicleMakes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaModelYears]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaModelYears
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaColors]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaColors
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteria]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchCriteriaInequalityTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SearchCriteriaInequalityTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MileageUnits]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MileageUnits
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Inequality]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Inequality
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ModelYears]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ModelYears
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Color]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Color
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MonthDays]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MonthDays
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WeekDays]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.WeekDays
GO

--
-- TABLE DEFINITIONS
--

--
-- Reference Table: Days of the week
--

CREATE TABLE dbo.WeekDays (
	[WeekDay]   INT         NOT NULL,
	[Name]      VARCHAR(50) NOT NULL,
	[ShortName] CHAR(3)     NOT NULL,
	CONSTRAINT PK_WeekDay PRIMARY KEY (
		[WeekDay]
	)
)
GO

INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (1, 'Sunday', 'Sun')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (2, 'Monday', 'Mon')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (3, 'Tuesday', 'Tue')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (4, 'Wednesday', 'Wed')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (5, 'Thursday', 'Thu')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (6, 'Friday', 'Fri')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (7, 'Saturday', 'Sat')
GO

--
-- Reference Table: Days of a month
--

CREATE TABLE dbo.MonthDays (
	[MonthDay]  INT         NOT NULL,
	[Name]      VARCHAR(50) NOT NULL,
	CONSTRAINT PK_MonthDay PRIMARY KEY (
		[MonthDay]
	)
)
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (1, '1st')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (2, '2nd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (3, '3rd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (4, '4th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (5, '5th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (6, '6th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (7, '7th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (8, '8th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (9, '9th')
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (10, '10th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (11, '11th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (12, '12th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (13, '13th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (14, '14th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (15, '15th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (16, '16th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (17, '17th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (18, '18th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (19, '19th')
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (20, '20th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (21, '21st')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (22, '22nd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (23, '23rd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (24, '24th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (25, '25th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (26, '26th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (27, '27th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (28, '28th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (29, '29th')
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (30, '30th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (31, '31st')
GO

--
-- Reference Table: Colors
--

CREATE TABLE dbo.Color (
	ColorID INT         NOT NULL,
	[Name]  VARCHAR(50) NOT NULL,
	CONSTRAINT PK_Color PRIMARY KEY (
		ColorID
	),
	CONSTRAINT UK_Color_Name UNIQUE (
		[Name]
	)
)
GO

INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (1,  'UNKNOWN')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (2,  'BEIGE')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (3,  'BLACK')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (4,  'BLUE')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (5,  'BROWN')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (6,  'BURGUNDY')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (7,  'GOLD')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (8,  'GRAY')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (9,  'GREEN')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (10, 'MAROON')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (11, 'ORANGE')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (12, 'PEWTER')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (13, 'PURPLE')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (14, 'RED')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (15, 'SILVER')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (16, 'TAN')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (17, 'TEAL')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (18, 'WHITE')
INSERT INTO dbo.Color ([ColorID], [Name]) VALUES (19, 'YELLOW')
GO

--
-- Reference Table: ModelYears and their VIN character code
--

CREATE TABLE dbo.ModelYears (
	ModelYear     INT     NOT NULL,
	CharacterCode CHAR(1) NOT NULL,
	CONSTRAINT PK_ModelYear PRIMARY KEY (
		ModelYear
	)
)
GO

INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1980, 'A')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1981, 'B')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1982, 'C')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1983, 'D')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1984, 'E')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1985, 'F')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1986, 'G')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1987, 'H')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1988, 'J')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1989, 'K')
GO

INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1990, 'L')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1991, 'M')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1992, 'N')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1993, 'P')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1994, 'R')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1995, 'S')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1996, 'T')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1997, 'V')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1998, 'W')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (1999, 'X')
GO

INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2000, 'Y')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2001, '1')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2002, '2')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2003, '3')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2004, '4')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2005, '5')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2006, '6')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2007, '7')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2008, '8')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2009, '9')
GO

INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2010, 'A')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2011, 'B')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2012, 'C')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2013, 'D')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2014, 'E')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2015, 'F')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2016, 'G')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2017, 'H')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2018, 'J')
INSERT INTO dbo.ModelYears (ModelYear, CharacterCode) VALUES (2019, 'K')
GO

--
-- Reference table: mathematical inequalities
--

CREATE TABLE dbo.Inequality (
	InequalityID INT         NOT NULL,
	[Name]       VARCHAR(50) NOT NULL,
	[Symbol]     VARCHAR(50) NOT NULL,
	CONSTRAINT PK_Inequality PRIMARY KEY (
		InequalityID
	),
	CONSTRAINT UK_Inequality_Name UNIQUE (
		NAme
	),
	CONSTRAINT UK_Inequality_Symbol UNIQUE (
		Symbol
	)
)
GO

INSERT INTO dbo.Inequality ([InequalityID], [Name],[Symbol]) VALUES (1, 'Less Than', '<')
INSERT INTO dbo.Inequality ([InequalityID], [Name],[Symbol]) VALUES (2, 'Greater Than', '>')
INSERT INTO dbo.Inequality ([InequalityID], [Name],[Symbol]) VALUES (3, 'Between', 'BETWEEN')
GO

--
-- Reference table: ways to measure the milage of a car (absolute and yearly)
--

CREATE TABLE dbo.MileageUnits (
	MileageUnitID INT         NOT NULL,
	[Name]        VARCHAR(50) NOT NULL,
	CONSTRAINT PK_MileageUnit PRIMARY KEY (
		MileageUnitID
	)
)
GO

INSERT INTO dbo.MileageUnits (MileageUnitID, [Name]) VALUES (1, 'Absolute Mileage')
INSERT INTO dbo.MileageUnits (MileageUnitID, [Name]) VALUES (2, 'Mileage Per Year')
GO

--
-- Reference table: The different types of inequality we use to restrict a selection of cars.
--

CREATE TABLE dbo.SearchCriteriaInequalityTypes (
	SearchCriterionInequalityTypeID INT         NOT NULL,
	[Name]                          VARCHAR(50) NOT NULL,
	CONSTRAINT PK_SearchCriterionInequalityType PRIMARY KEY (
		SearchCriterionInequalityTypeID
	)
)
GO

INSERT INTO dbo.SearchCriteriaInequalityTypes (SearchCriterionInequalityTypeID,[Name]) VALUES (1, 'Mileage')
INSERT INTO dbo.SearchCriteriaInequalityTypes (SearchCriterionInequalityTypeID,[Name]) VALUES (2, 'List Price')
INSERT INTO dbo.SearchCriteriaInequalityTypes (SearchCriterionInequalityTypeID,[Name]) VALUES (3, 'Unit Cost')
INSERT INTO dbo.SearchCriteriaInequalityTypes (SearchCriterionInequalityTypeID,[Name]) VALUES (4, 'Book Value')
INSERT INTO dbo.SearchCriteriaInequalityTypes (SearchCriterionInequalityTypeID,[Name]) VALUES (5, 'Book Versus Unit Cost')
INSERT INTO dbo.SearchCriteriaInequalityTypes (SearchCriterionInequalityTypeID,[Name]) VALUES (6, 'Inventory Age')
GO

--
-- Instance table: Parent table which contains the PK for search criterion.
--

CREATE TABLE dbo.SearchCriteria (
	SearchCriterionID   INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID      INT         NOT NULL,
	[Name]              VARCHAR(50) NULL,
	OverstockModels     BIT         NULL,
	OverstockVehicles   BIT         NULL,
	Active              BIT         NOT NULL,
	CONSTRAINT PK_SearchCriterion PRIMARY KEY (
		SearchCriterionID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and colors.
--

CREATE TABLE dbo.SearchCriteriaColors (
	SearchCriterionID INT NOT NULL,
	ColorID           INT NOT NULL,
	CONSTRAINT PK_SearchCriterionColor PRIMARY KEY (
		SearchCriterionID,
		ColorID
	),
	CONSTRAINT FK_SearchCriterionColor_Color FOREIGN KEY (
		ColorID
	)
	REFERENCES Color (
		ColorID
	),
	CONSTRAINT FK_SearchCriterionColor_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and model years.
--

CREATE TABLE dbo.SearchCriteriaModelYears (
	SearchCriterionID INT NOT NULL,
	ModelYear         INT NOT NULL,
	CONSTRAINT PK_SearchCriterionModelYear PRIMARY KEY (
		SearchCriterionID,
		ModelYear
	),
	CONSTRAINT FK_SearchCriterionModelYear_ModelYear FOREIGN KEY (
		ModelYear
	)
	REFERENCES ModelYears (
		ModelYear
	),
	CONSTRAINT FK_SearchCriterionModelYear_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and vehicle makes
--

CREATE TABLE dbo.SearchCriteriaVehicleMakes (
	SearchCriterionID            INT NOT NULL,
	VehicleMakeID                INT NOT NULL,
	CONSTRAINT PK_SearchCriterionVehicleMake PRIMARY KEY (
		SearchCriterionID,
		VehicleMakeID
	),
	CONSTRAINT FK_SearchCriterionVehicleMake_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchCriterionVehicleMake_VehicleMake FOREIGN KEY (
		VehicleMakeID
	)
	REFERENCES VehicleMake (
		VehicleMakeID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and vehicle lines
--

CREATE TABLE dbo.SearchCriteriaVehicleLines (
	SearchCriterionID INT NOT NULL,
	VehicleLineID     INT NOT NULL,
	CONSTRAINT PK_SearchCriterionVehicleLine PRIMARY KEY (
		SearchCriterionID,
		VehicleLineID
	),
	CONSTRAINT FK_SearchCriterionVehicleLine_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchCriterionVehicleLine_VehicleLine FOREIGN KEY (
		VehicleLineID
	)
	REFERENCES VehicleLine (
		VehicleLineID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and inventory vehicle lights (red/yellow/green)
--

CREATE TABLE dbo.SearchCriteriaInventoryVehicleLights (
	SearchCriterionID       INT NOT NULL,
	InventoryVehicleLightID INT NOT NULL,
	CONSTRAINT PK_SearchCriterionInventoryVehicleLight PRIMARY KEY (
		SearchCriterionID,
		InventoryVehicleLightID
	),
	CONSTRAINT FK_SearchCriterionInventoryVehicleLight_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and (risk) planning categories
--

CREATE TABLE dbo.SearchCriteriaPlanningCategories (
	SearchCriterionID  INT NOT NULL,
	PlanningCategoryID INT NOT NULL,
	CONSTRAINT PK_SearchCriterionPlanningCategory PRIMARY KEY (
		SearchCriterionID,
		PlanningCategoryID
	),
	CONSTRAINT FK_SearchCriterionPlanningCategory_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchCriterionPlanningCategory_PlanningCategory FOREIGN KEY (
		PlanningCategoryID
	)
	REFERENCES PlanningCategories (
		PlanningCategoryID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and planning action
--

CREATE TABLE dbo.SearchCriteriaPlanningActions (
	SearchCriterionID INT NOT NULL,
	PlanningActionID  INT NOT NULL,
	CONSTRAINT PK_SearchCriteriaPlanningAction PRIMARY KEY (
		SearchCriterionID,
		PlanningActionID
	),
	CONSTRAINT FK_SearchCriteriaPlanningAction_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchCriteriaPlanningAction_PlanningAction FOREIGN KEY (
		PlanningActionID
	)
	REFERENCES PlanningActions (
		PlanningActionID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and planning action type
--

CREATE TABLE dbo.SearchCriteriaPlanningActionTypes (
	SearchCriterionID    INT NOT NULL,
	PlanningActionTypeID INT NOT NULL,
	CONSTRAINT PK_SearchCriteriaPlanningActionType PRIMARY KEY (
		SearchCriterionID,
		PlanningActionTypeID
	),
	CONSTRAINT FK_SearchCriteriaPlanningActionType_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchCriteriaPlanningActionType_PlanningAction FOREIGN KEY (
		PlanningActionTypeID
	)
	REFERENCES PlanningActionTypes (
		PlanningActionTypeID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and inventory bucket ranges
--

CREATE TABLE dbo.SearchCriteriaInventoryBucketRanges (
	SearchCriterionID      INT NOT NULL,
	InventoryBucketRangeID INT NOT NULL,
	CONSTRAINT PK_SearchCriteriaInventoryBucketRange PRIMARY KEY (
		SearchCriterionID,
		InventoryBucketRangeID
	),
	CONSTRAINT FK_SearchCriteriaInventoryBucketRange_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	)
)
GO

--
-- Instance table: Instance of search criteria inequality (e.g. list price less than $10,000)
--

CREATE TABLE dbo.SearchCriteriaInequalities (
	SearchCriterionInequalityID     INT IDENTITY(1,1) NOT NULL,
	SearchCriterionInequalityTypeID INT NOT NULL,
	InequalityID                    INT NOT NULL,
	UpperBound                      INT NULL,
	LowerBound                      INT NULL,
	MileageUnitID                   INT NULL,
	CONSTRAINT PK_SearchCriterionInequality PRIMARY KEY (
		SearchCriterionInequalityID
	),
	CONSTRAINT FK_SearchCriterionInequality_SearchCriterionInequalityType FOREIGN KEY (
		SearchCriterionInequalityID
	)
	REFERENCES SearchCriteriaInequalities (
		SearchCriterionInequalityID
	),
	CONSTRAINT FK_SearchCriterionInequality_Inequality FOREIGN KEY (
		InequalityID
	)
	REFERENCES Inequality (
		InequalityID
	),
	CONSTRAINT FK_SearchCriterionInequality_MileageUnit FOREIGN KEY (
		MileageUnitID
	)
	REFERENCES MileageUnits (
		MileageUnitID
	)
)
GO

--
-- Instance table: Many to Many join table between search criteria and its inequalities
--

CREATE TABLE dbo.SearchCriteriaSearchCriteriaInequalities (
	SearchCriterionID           INT NOT NULL,
	SearchCriterionInequalityID INT NOT NULL,
	CONSTRAINT PK_SearchCriterionSearchCriterionInequality PRIMARY KEY (
		SearchCriterionID,
		SearchCriterionInequalityID
	),
	CONSTRAINT FK_SearchCriterionSearchCriterionInequality_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchCriterionSearchCriterionInequality_SearchCriterionInequality FOREIGN KEY (
		SearchCriterionInequalityID
	)
	REFERENCES SearchCriteriaInequalities (
		SearchCriterionInequalityID
	)
)
GO

--
-- Reference table: Types of vehicle list
--

CREATE TABLE dbo.VehicleListTypes (
	VehicleListTypeID INT         NOT NULL,
	[Name]            VARCHAR(50) NOT NULL,
	CONSTRAINT PK_VehicleListType PRIMARY KEY (
		VehicleListTypeID
	)
)
GO

INSERT INTO dbo.VehicleListTypes (VehicleListTypeID,[Name]) VALUES (1, 'Search Results')
INSERT INTO dbo.VehicleListTypes (VehicleListTypeID,[Name]) VALUES (2, 'Campaign')
GO

--
-- Instance table: Parent table for a list of vehicles
--

CREATE TABLE dbo.VehicleLists (
	VehicleListID     INT IDENTITY(1,1) NOT NULL,
	VehicleListTypeID INT               NOT NULL,
	Created           DATETIME          NOT NULL,
	LastModified      DATETIME          NOT NULL,
	CONSTRAINT PK_VehicleList PRIMARY KEY (
		VehicleListID
	),
	CONSTRAINT FK_VehicleList_VehicleListType FOREIGN KEY (
		VehicleListTypeID
	)
	REFERENCES VehicleListTypes (
		VehicleListTypeID
	)
)
GO

--
-- Instance table: Items in a vehicle list. Acts as a many-to-many join between vehicle-lists and vehicles.
--

CREATE TABLE dbo.VehicleListItems (
	VehicleListItemID INT IDENTITY(1,1) NOT NULL,
	VehicleListID     INT NOT NULL,
	VehicleID         INT NOT NULL,
	Position          INT NOT NULL,
	CONSTRAINT PK_VehicleListItem PRIMARY KEY (
		VehicleListItemID
	),
	CONSTRAINT FK_VehicleListItem_VehicleList FOREIGN KEY (
		VehicleListID
	)
	REFERENCES VehicleLists (
		VehicleListID
	)
)
GO

--
-- Instance table: A list of vehicles owned by a business unit that appeared in a search results pane.
--

CREATE TABLE dbo.SearchResults (
	SearchResultID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID INT NOT NULL,
	VehicleListID  INT NOT NULL,
	CONSTRAINT PK_SearchResult PRIMARY KEY (
		SearchResultID
	),
	CONSTRAINT FK_SearchResult_VehicleList FOREIGN KEY (
		VehicleListID
	)
	REFERENCES VehicleLists (
		VehicleListID
	)
)
GO

--
-- Instance table: Many to Many join table between search results and the criteria used to gather the list of
-- vehicles.
--

CREATE TABLE dbo.SearchResultsSearchCriteria (
	SearchResultID    INT NOT NULL,
	SearchCriterionID INT NOT NULL,
	CONSTRAINT PK_SearchResultSearchCriterion PRIMARY KEY (
		SearchResultID,
		SearchCriterionID
	),
	CONSTRAINT FK_SearchResultSearchCriterion_SearchCriterion FOREIGN KEY (
		SearchCriterionID
	)
	REFERENCES SearchCriteria (
		SearchCriterionID
	),
	CONSTRAINT FK_SearchResultSearchCriterion_SearchResult FOREIGN KEY (
		SearchResultID
	)
	REFERENCES SearchResults (
		SearchResultID
	)
)
GO

--
-- Reference table: Types of campaign undertstood by HAL.
--

CREATE TABLE dbo.CampaignTypes (
	CampaignTypeID INT         NOT NULL,
	[Name]         VARCHAR(50) NOT NULL,
	CONSTRAINT PK_CampaignType PRIMARY KEY (
		CampaignTypeID
	)
)
GO

INSERT INTO dbo.CampaignTypes (CampaignTypeID,[Name]) VALUES (1, 'Small Print Advertisement')
INSERT INTO dbo.CampaignTypes (CampaignTypeID,[Name]) VALUES (2, 'Lot Promotion')
GO

--
-- Reference table: Types of interval (weekly, monthly etc)
--

CREATE TABLE dbo.IntervalTypes (
	IntervalTypeID INT         NOT NULL,
	[Name]         VARCHAR(50) NOT NULL,
	CONSTRAINT PK_IntervalType PRIMARY KEY (
		IntervalTypeID
	)
)
GO

INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (1, 'One Time')
INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (2, 'Weekly')
INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (3, 'Monthly')
INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (4, 'Yearly')
GO

--
-- Instance table: Instance of an interval (e.g. every tuesday)
--

CREATE TABLE dbo.Interval (
	IntervalID     INT IDENTITY(1,1) NOT NULL,
	IntervalTypeID INT NOT NULL,
	DayOfWeek      INT NULL,
	DayOfMonth     INT NULL,
	DayOfYear      SMALLDATETIME NULL,
	CONSTRAINT PK_Interval PRIMARY KEY (
		IntervalID
	),
	CONSTRAINT FK_Interval_IntervalType FOREIGN KEY (
		IntervalTypeID
	)
	REFERENCES IntervalTypes (
		IntervalTypeID
	),
	CONSTRAINT FK_Interval_DayOfWeek FOREIGN KEY (
		DayOfWeek
	)
	REFERENCES WeekDays (
		[WeekDay]
	),
	CONSTRAINT FK_Interval_DayOfMonth FOREIGN KEY (
		DayOfMonth
	)
	REFERENCES MonthDays (
		[MonthDay]
	)
)
GO

--
-- Instance table: Instance of campaign (e.g. Sun Times Sunday Car Supplement)
--

CREATE TABLE dbo.Campaign (
	CampaignID          INT IDENTITY(1,1) NOT NULL,
	CampaignTypeID      INT NOT NULL,
	RunDateIntervalID   INT NOT NULL,
	DeadlineIntervalID  INT NOT NULL,
	BusinessUnitID      INT NOT NULL,
	ThirdPartyEntityID  INT NULL,
	[Name]              VARCHAR(50) NOT NULL,
	Active              BIT NOT NULL,
	CONSTRAINT PK_Campaign PRIMARY KEY (
		CampaignID
	),
	CONSTRAINT FK_Campaign_CampaignType FOREIGN KEY (
		CampaignTypeID
	)
	REFERENCES CampaignTypes (
		CampaignTypeID
	),
	CONSTRAINT FK_Campaign_Interval_RunDate FOREIGN KEY (
		RunDateIntervalID
	)
	REFERENCES Interval (
		IntervalID
	),
	CONSTRAINT FK_Campaign_Interval_Deadline FOREIGN KEY (
		DeadlineIntervalID
	)
	REFERENCES Interval (
		IntervalID
	)
)
GO

--
-- Instance table: Instance of campaign (e.g. Sun Times Sunday Car Supplement)
--

CREATE TABLE dbo.CampaignRuns (
	CampaignRunID INT IDENTITY(1,1) NOT NULL,
	CampaignID    INT               NOT NULL,
	Deadline      SMALLDATETIME     NOT NULL,
	RunDate       SMALLDATETIME     NOT NULL,
	VehicleListID INT               NOT NULL,
	CONSTRAINT PK_CampaignRun PRIMARY KEY (
		CampaignRunID
	),
	CONSTRAINT FK_CampaignRun_Campaign FOREIGN KEY (
		CampaignID
	)
	REFERENCES Campaign (
		CampaignID
	),
	CONSTRAINT FK_CampaignRun_VehicleList FOREIGN KEY (
		VehicleListID
	)
	REFERENCES VehicleLists (
		VehicleListID
	),
	CONSTRAINT CK_CampaignRun_Dates CHECK (
		Deadline < RunDate
	),
	CONSTRAINT UK_CampaignRun UNIQUE (
		CampaignID,
		RunDate
	)
)
GO

--
-- Ruby Views
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[campaign_runs]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[campaign_runs]
GO

CREATE VIEW dbo.campaign_runs (
	[id],
	[campaign_id],
	[deadline],
	[run_date],
	[vehicle_list_id]
)
AS
SELECT
	CampaignRunID,
	CampaignID,
	Deadline,
	RunDate,
	VehicleListID
FROM
	dbo.CampaignRuns
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[campaigns]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[campaigns]
GO

CREATE VIEW dbo.campaigns (
	[id],
	[campaign_type_id],
	[run_date_interval_id],
	[deadline_interval_id],
	[business_unit_id],
	[third_party_id],
	[name],
	[active]
)
AS
SELECT
	[CampaignID],
	[CampaignTypeID],
	[RunDateIntervalID],
	[DeadlineIntervalID],
	[BusinessUnitID],
	[ThirdPartyEntityID],
	[Name],
	[Active]
FROM
	dbo.Campaign
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[intervals]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[intervals]
GO

CREATE VIEW dbo.intervals (
	[id],
	[interval_type_id],
	[day_of_week],
	[day_of_month],
	[day_of_year]
)
AS
SELECT
	IntervalID,
	IntervalTypeID,
	DayOfWeek,
	DayOfMonth,
	DayOfYear
FROM
	dbo.Interval
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[interval_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[interval_types]
GO

CREATE VIEW dbo.interval_types (
	[id],
	[name]
)
AS
SELECT
	[IntervalTypeID],
	[Name]
FROM
	dbo.IntervalTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[campaign_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[campaign_types]
GO

CREATE VIEW dbo.campaign_types (
	[id],
	[name]
)
AS
SELECT
	[CampaignTypeID],
	[Name]
FROM
	dbo.CampaignTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_results_search_criteria]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_results_search_criteria]
GO

CREATE VIEW dbo.search_results_search_criteria (
	[search_result_id],
	[search_criterion_id]
)
AS
SELECT
	SearchResultID,
	SearchCriterionID
FROM
	dbo.SearchResultsSearchCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_results]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_results]
GO

CREATE VIEW dbo.search_results (
	[id],
	[business_unit_id],
	[vehicle_list_id]
)
AS
SELECT
	SearchResultID,
	BusinessUnitID,
	VehicleListID
FROM
	dbo.SearchResults
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_list_items]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_list_items]
GO

CREATE VIEW dbo.vehicle_list_items (
	[id],
	[vehicle_list_id],
	[vehicle_id],
	[position]
)
AS
SELECT
	VehicleListItemID,
	VehicleListID,
	VehicleID,
	Position
FROM
	dbo.VehicleListItems
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_lists]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_lists]
GO

CREATE VIEW dbo.vehicle_lists (
	[id],
	[vehicle_list_type_id],
	[created],
	[last_modified]
)
AS
SELECT
	VehicleListID,
	VehicleListTypeID,
	Created,
	LastModified
FROM
	dbo.VehicleLists
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vehicle_list_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vehicle_list_types]
GO

CREATE VIEW dbo.vehicle_list_types (
	[id],
	[name]
)
AS
SELECT
	[VehicleListTypeID],
	[Name]
FROM
	dbo.VehicleListTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_search_criteria_inequalities]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_search_criteria_inequalities]
GO

CREATE VIEW dbo.search_criteria_search_criteria_inequalities (
	[search_criterion_id],
	[search_criteria_inequality_id]
)
AS
SELECT
	[SearchCriterionID],
	[SearchCriterionInequalityID]
FROM
	dbo.SearchCriteriaSearchCriteriaInequalities
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_inequalities]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_inequalities]
GO

CREATE VIEW dbo.search_criteria_inequalities (
	[id],
	[search_criteria_inequality_type_id],
	[inequality_id],
	[upper_bound],
	[lower_bound],
	[mileage_unit_id]
)
AS
SELECT
	SearchCriterionInequalityID,
	SearchCriterionInequalityTypeID,
	InequalityID,
	UpperBound,
	LowerBound,
	MileageUnitID
FROM
	dbo.SearchCriteriaInequalities
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_planning_categories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_planning_categories]
GO

CREATE VIEW dbo.search_criteria_planning_categories (
	[search_criterion_id],
	[planning_category_id]
)
AS
SELECT
	SearchCriterionID,
	PlanningCategoryID
FROM
	dbo.SearchCriteriaPlanningCategories
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_planning_actions]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_planning_actions]
GO

CREATE VIEW dbo.search_criteria_planning_actions (
	[search_criterion_id],
	[planning_action_id]
)
AS
SELECT
	SearchCriterionID,
	PlanningActionID
FROM
	dbo.SearchCriteriaPlanningActions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_planning_action_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_planning_action_types]
GO

CREATE VIEW dbo.search_criteria_planning_action_types (
	[search_criterion_id],
	[planning_action_type_id]
)
AS
SELECT
	SearchCriterionID,
	PlanningActionTypeID
FROM
	dbo.SearchCriteriaPlanningActionTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_planning_age_buckets]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_planning_age_buckets]
GO

CREATE VIEW dbo.search_criteria_planning_age_buckets (
	[search_criterion_id],
	[planning_age_bucket_id]
)
AS
SELECT
	SearchCriterionID,
	InventoryBucketRangeID
FROM
	dbo.SearchCriteriaInventoryBucketRanges
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_inventory_vehicle_lights]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_inventory_vehicle_lights]
GO

CREATE VIEW dbo.search_criteria_inventory_vehicle_lights (
	[search_criterion_id],
	[inventory_vehicle_light_id]
)
AS
SELECT
	SearchCriterionID,
	InventoryVehicleLightID
FROM
	dbo.SearchCriteriaInventoryVehicleLights
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_vehicle_lines]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_vehicle_lines]
GO

CREATE VIEW dbo.search_criteria_vehicle_lines (
	[search_criterion_id],
	[vehicle_line_id]
)
AS
SELECT
	SearchCriterionID,
	VehicleLineID
FROM
	dbo.SearchCriteriaVehicleLines
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_vehicle_makes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_vehicle_makes]
GO

CREATE VIEW dbo.search_criteria_vehicle_makes (
	[search_criterion_id],
	[vehicle_make_id]
)
AS
SELECT
	SearchCriterionID,
	VehicleMakeID
FROM
	dbo.SearchCriteriaVehicleMakes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_model_years]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_model_years]
GO

CREATE VIEW dbo.search_criteria_model_years (
	[search_criterion_id],
	[model_year_id]
)
AS
SELECT
	SearchCriterionID,
	ModelYear
FROM
	dbo.SearchCriteriaModelYears
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_colors]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_colors]
GO

CREATE VIEW dbo.search_criteria_colors (
	[search_criterion_id],
	[color_id]
)
AS
SELECT
	SearchCriterionID,
	ColorID
FROM
	dbo.SearchCriteriaColors
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria]
GO

CREATE VIEW dbo.search_criteria (
	[id],
	[business_unit_id],
	[name],
	[overstock_models],
	[overstock_vehicles],
	[active]
)
AS
SELECT
	[SearchCriterionID],
	[BusinessUnitID],
	[Name],
	[OverstockModels],
	[OverstockVehicles],
	[Active]
FROM
	dbo.SearchCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[search_criteria_inequality_types]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[search_criteria_inequality_types]
GO

CREATE VIEW dbo.search_criteria_inequality_types (
	[id],
	[name]
)
AS
SELECT
	[SearchCriterionInequalityTypeID],
	[Name]
FROM
	dbo.SearchCriteriaInequalityTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mileage_units]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[mileage_units]
GO

CREATE VIEW dbo.mileage_units (
	[id],
	[name]
)
AS
SELECT
	[MileageUnitID],
	[Name]
FROM
	dbo.MileageUnits
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inequalities]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inequalities]
GO

CREATE VIEW dbo.inequalities (
	[id],
	[name],
	[symbol]
)
AS
SELECT
	[InequalityID],
	[Name],
	[Symbol]
FROM
	dbo.Inequality
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[model_years]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[model_years]
GO

CREATE VIEW dbo.model_years (
	[id],
	[character_code]
)
AS
SELECT
	[ModelYear],
	[CharacterCode]
FROM
	dbo.ModelYears
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[colors]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[colors]
GO

CREATE VIEW dbo.colors (
	[id],
	[name]
)
AS
SELECT
	[ColorID],
	[Name]
FROM
	dbo.Color
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[month_days]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[month_days]
GO

CREATE VIEW dbo.month_days (
	[id],
	[name]
)
AS
SELECT
	[MonthDay],
	[Name]
FROM
	dbo.MonthDays
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[week_days]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[week_days]
GO

CREATE VIEW dbo.week_days (
	[id],
	[name]
)
AS
SELECT
	[WeekDay],
	[Name]
FROM
	dbo.WeekDays
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[inventory_vehicle_lights]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[inventory_vehicle_lights]
GO

CREATE VIEW dbo.inventory_vehicle_lights (
	[id],
	[name]
)
AS
SELECT
	[InventoryVehicleLightCD],
	[InventoryVehicleLightDesc]
FROM
	[IMT].dbo.lu_InventoryVehicleLight
GO

--
-- reporting functions and views
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NextIntervalDate]') and xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[NextIntervalDate]
GO

CREATE FUNCTION dbo.NextIntervalDate(@interval_type_id int, @day_of_week int, @day_of_month int, @day_of_year datetime, @now datetime)
RETURNS DATETIME
AS
BEGIN
	DECLARE @next DATETIME
	
	SET @next = CASE
		WHEN @interval_type_id = 2 THEN
			DATEADD(DD,ABS(DATEPART(DW,@now)-@day_of_week),@now)
		WHEN @interval_type_id = 3 THEN
			CASE
				WHEN DATEPART(DD,@now) < @day_of_month THEN
					CAST(CAST(YEAR(@now) AS VARCHAR) + '-' + CAST(MONTH(@now) AS VARCHAR) + '-' + CAST(@day_of_month AS VARCHAR) AS DATETIME)
				ELSE
					CAST(CAST(YEAR(DATEADD(MM,1,@now)) AS VARCHAR) + '-' + CAST(MONTH(DATEADD(MM,1,@now)) AS VARCHAR) + '-' + CAST(@day_of_month AS VARCHAR) AS DATETIME)
			END
		ELSE
			CASE WHEN @day_of_year < @now THEN NULL ELSE @day_of_year END
	END
	
	RETURN @next
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[campaign_deadlines]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[campaign_deadlines]
GO

CREATE VIEW dbo.campaign_deadlines (
	[id],
	[business_unit_id],
	[third_party_id],
	[third_party_name],
	[campaign_id],
	[campaign_name],
	[campaign_type_name],
	[deadline],
	[run_date],
	[days_until_deadline],
	[campaign_run_id]
)
AS
SELECT
	CAST(c.campaign_id AS VARCHAR) + '_' + COALESCE(CAST(r.id AS VARCHAR), 'nil'),
	c.business_unit_id,
	c.third_party_id,
	c.third_party_name,
	c.campaign_id,
	c.campaign_name,
	c.campaign_type_name,
	c.next_deadline,
	c.next_run_date,
	DATEDIFF(DY,GETDATE(),c.next_deadline) days_until_next_deadline,
	r.id campaign_run_id
FROM
(
	SELECT
		tp.id third_party_id,
		tp.name third_party_name,
		t.name campaign_type_name,
		c.name campaign_name,
		c.id campaign_id,
		c.business_unit_id,
		dbo.NextIntervalDate(d.interval_type_id, d.day_of_week, d.day_of_month, d.day_of_year, GETDATE()) next_deadline,
		dbo.NextIntervalDate(r.interval_type_id, r.day_of_week, r.day_of_month, r.day_of_year, GETDATE()) next_run_date
	FROM
		campaigns c
	JOIN	campaign_types t ON c.campaign_type_id = t.id
	JOIN	intervals r ON c.run_date_interval_id = r.id
	JOIN	intervals d ON c.deadline_interval_id = d.id
	JOIN	third_parties tp ON c.third_party_id = tp.id
	WHERE
		c.active = 1
) c
LEFT JOIN campaign_runs r ON c.campaign_id = r.campaign_id
GO

CREATE VIEW dbo.search_criteria_vehicle_makes_and_lines (
	search_criterion_id,
	vehicle_line_id,
	vehicle_line_source_id
)
AS
SELECT
	T.SearchCriterionID,
	T.VehicleLineID,
	T.VehicleLineSourceID
FROM
(
	SELECT
		C.SearchCriterionID,
		L.VehicleLineID,
		1 VehicleLineSourceID
	FROM
		dbo.SearchCriteria C
	JOIN	dbo.SearchCriteriaVehicleLines L ON L.SearchCriterionID = C.SearchCriterionID
UNION
	SELECT
		C.SearchCriterionID,
		L.VehicleLineID,
		2 VehicleLineSourceID
	FROM
		dbo.SearchCriteria C
	JOIN	dbo.SearchCriteriaVehicleMakes M ON M.SearchCriterionID = C.SearchCriterionID
	JOIN	dbo.VehicleLine L ON L.VehicleMakeID  = M.VehicleMakeID
) T
GO

CREATE VIEW dbo.overstocked_vehicle_lines (
	business_unit_id,
	vehicle_line_id,
	overstocked_units
)
AS
SELECT	DISTINCT
	I4.BusinessUnitID,
	MG.MakeModelGroupingID,
	I4.StockedUnitsModel - OS.Threshold
FROM
	Inventory_A4 I4
JOIN	[IMT].dbo.MakeModelGrouping MG ON MG.GroupingDescriptionID = I4.GroupingDescriptionID
JOIN	dbo.OverstockedModelsThresholds OS ON I4.OptimalUnitsModel = OS.OptUnits
WHERE
	I4.StockedUnitsModel > OS.Threshold
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SatisfiesSearchCriteriaInequality]') and xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[SatisfiesSearchCriteriaInequality]
GO

CREATE FUNCTION dbo.SatisfiesSearchCriteriaInequality(@inequality_id int, @lower_bound int, @upper_bound int, @value int)
RETURNS BIT
AS
BEGIN
	DECLARE @satisfied BIT
	
	SET @satisfied = CASE
		WHEN @inequality_id = 1 THEN
			CASE WHEN @value BETWEEN 0 AND @lower_bound THEN 1 ELSE 0 END
		WHEN @inequality_id = 2 THEN
			CASE WHEN @value BETWEEN @lower_bound AND 1000000 THEN 1 ELSE 0 END
		ELSE
			CASE WHEN @value BETWEEN @lower_bound AND @upper_bound THEN 1 ELSE 0 END
	END
	
	RETURN @satisfied
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SearchInventory]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[SearchInventory]
GO

CREATE FUNCTION dbo.SearchInventory(
	@SearchCriterionID INT,
	@BaseDate          DATETIME
)
RETURNS @Results TABLE (
	InventoryID INT
)
BEGIN

	DECLARE @BusinessUnitID INT, @OverstockModels BIT, @ERR INT
	
	SELECT	@BusinessUnitID = BusinessUnitID,
		@OverstockModels = OverstockModels
	FROM	SearchCriteria
	WHERE	SearchCriterionID = @SearchCriterionID
		
	DECLARE @Inventory TABLE (
		business_unit_id         INT,
		inventory_id             INT,
		mileage_received         INT,
		list_price               INT,
		unit_cost                INT,
		inventory_received_date  DATETIME,
		current_vehicle_light_id INT,
		vehicle_line_id          INT,
		color_id                 INT,
		model_year               INT,
		years_old                INT,
		planning_age_bucket_id   INT
	)
	
	INSERT INTO @Inventory (
		business_unit_id,
		inventory_id,
		mileage_received,
		list_price,
		unit_cost,
		inventory_received_date,
		current_vehicle_light_id,
		vehicle_line_id,
		color_id,
		model_year,
		years_old,
		planning_age_bucket_id
	)
	SELECT
		i.business_unit_id,
		i.id inventory_id,
		i.mileage_received,
		i.list_price,
		i.unit_cost,
		i.inventory_received_date,
		i.current_vehicle_light_id,
		v.vehicle_line_id,
		c.id,
		v.model_year,
		COALESCE(NULLIF((
			CASE WHEN DATEPART(YY, @BaseDate) <= v.model_year THEN
				0
			ELSE
				DATEPART(YY, @BaseDate) - v.model_year
			END
			+
			CASE WHEN DATEPART(MM, @BaseDate) < 9 THEN
				0
			ELSE
				1
			END
		),0),1) years_old,
		b.id
	FROM
		inventory i
	JOIN	vehicles v on v.id = i.vehicle_id
	JOIN	colors c on v.color = c.name
	JOIN	dbo.planning_age_buckets b on b.business_unit_id = i.business_unit_id
	WHERE
		i.business_unit_id = @BusinessUnitID
	AND	i.active = 1
	AND	i.inventory_type_id = 2
	AND	CASE
		WHEN DATEDIFF(DD, i.inventory_received_date, [IMT].dbo.ToDate(@BaseDate)) < 1 THEN
			1
		ELSE
			DATEDIFF(DD, i.inventory_received_date, [IMT].dbo.ToDate(@BaseDate))
		END
		BETWEEN COALESCE(b.[low],0) AND COALESCE(b.[high], 9999)
	AND	COALESCE(b.[lights],7) & POWER(2, i.current_vehicle_light_id-1) = POWER(2, i.current_vehicle_light_id-1)
	
	DECLARE @Planning TABLE (
		inventory_id            INT,
		planning_category_id    INT,
		planning_action_id      INT,
		planning_action_type_id INT
	)
	
	INSERT INTO @Planning (
		inventory_id,
		planning_category_id,
		planning_action_id,
		planning_action_type_id
	)
	SELECT		i.inventory_id,
			planb.planning_category_id,
			plant.planning_action_id,
			plana.planning_action_type_id
	FROM		@Inventory i
	JOIN		dbo.inventory_planning_categories planc on planc.inventory_id = i.inventory_id
	LEFT JOIN	dbo.inventory_planning_tasks plant on plant.inventory_planning_category_id = planc.id
	LEFT JOIN	dbo.planning_actions plana on plana.id = plant.planning_action_id
	JOIN		dbo.business_unit_planning_categories planb on planb.id = planc.business_unit_planning_category_id
	WHERE		planc.invalidated IS NULL AND COALESCE(planc.valid_until,'2038-01-18') = '2038-01-18'
	AND		plant.invalidated IS NULL AND plant.satisfied IS NULL AND COALESCE(plant.valid_until,'2038-01-18') >= @BaseDate
	
	-- SELECT COUNT(*) FROM @Inventory
	
	-- SELECT * FROM @Inventory
	
	DECLARE @Colors TABLE (
		color_id INT
	)
	
	INSERT INTO @Colors (
		color_id
	)
	SELECT	color_id
	FROM	dbo.search_criteria_colors
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE
	FROM	@Inventory
	WHERE	color_id NOT IN (SELECT color_id FROM @Colors)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @InventoryVehicleLights TABLE (
		inventory_vehicle_light_id INT
	)
	
	INSERT INTO @InventoryVehicleLights (
		inventory_vehicle_light_id
	)
	SELECT	inventory_vehicle_light_id
	FROM	dbo.search_criteria_inventory_vehicle_lights
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE
	FROM	@Inventory
	WHERE	current_vehicle_light_id NOT IN (SELECT inventory_vehicle_light_id FROM @InventoryVehicleLights)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @ModelYears TABLE (
		model_year_id INT
	)
	
	INSERT INTO @ModelYears (
		model_year_id
	)
	SELECT	model_year_id
	FROM	dbo.search_criteria_model_years
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE
	FROM	@Inventory
	WHERE	model_year NOT IN (SELECT model_year_id FROM @ModelYears)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @VehicleLines TABLE (
		vehicle_line_id INT
	)
	
	INSERT INTO @VehicleLines (
		vehicle_line_id
	)
	SELECT	vehicle_line_id
	FROM	dbo.search_criteria_vehicle_makes_and_lines
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE
	FROM	@Inventory
	WHERE	vehicle_line_id NOT IN (SELECT vehicle_line_id FROM @VehicleLines)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @PlanningCategories TABLE (
		planning_category_id INT
	)
	
	INSERT INTO @PlanningCategories (
		planning_category_id
	)
	SELECT	planning_category_id
	FROM	dbo.search_criteria_planning_categories
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE	I
	FROM	@Inventory I
	JOIN	@Planning P ON I.inventory_id = P.inventory_id
	WHERE	P.planning_category_id NOT IN (SELECT planning_category_id FROM @PlanningCategories)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @PlanningActions TABLE (
		planning_action_id INT
	)
	
	INSERT INTO @PlanningActions (
		planning_action_id
	)
	SELECT	planning_action_id
	FROM	dbo.search_criteria_planning_actions
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE	I
	FROM	@Inventory I
	JOIN	@Planning P ON I.inventory_id = P.inventory_id
	WHERE	P.planning_action_id NOT IN (SELECT planning_action_id FROM @PlanningActions)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @PlanningActionTypes TABLE (
		planning_action_type_id INT
	)
	
	INSERT INTO @PlanningActionTypes (
		planning_action_type_id
	)
	SELECT	planning_action_type_id
	FROM	dbo.search_criteria_planning_action_types
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE	I
	FROM	@Inventory I
	JOIN	@Planning P ON I.inventory_id = P.inventory_id
	WHERE	P.planning_action_type_id NOT IN (SELECT planning_action_type_id FROM @PlanningActionTypes)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @PlanningAgeBuckets TABLE (
		planning_age_bucket_id INT
	)
	
	INSERT INTO @PlanningAgeBuckets (
		planning_age_bucket_id
	)
	SELECT	planning_age_bucket_id
	FROM	dbo.search_criteria_planning_age_buckets
	WHERE	search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE	I
	FROM	@Inventory I
	WHERE	I.planning_age_bucket_id NOT IN (SELECT planning_age_bucket_id FROM @PlanningAgeBuckets)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	IF @OverstockModels = 1
	DELETE	I
	FROM	@Inventory I
	WHERE	I.vehicle_line_id NOT IN (SELECT vehicle_line_id FROM dbo.overstocked_vehicle_lines WHERE business_unit_id = @BusinessUnitID AND overstocked_units > 0)
	
	-- SELECT COUNT(*) FROM @Inventory
	
	DECLARE @SearchCriteriaInequalities TABLE (
		[id]                                 INT,
		[search_criteria_inequality_type_id] INT,
		[inequality_id]                      INT,
		[upper_bound]                        INT,
		[lower_bound]                        INT,
		[mileage_unit_id]                    INT
	)
	
	INSERT INTO @SearchCriteriaInequalities (
		[id],
		[search_criteria_inequality_type_id],
		[inequality_id],
		[upper_bound],
		[lower_bound],
		[mileage_unit_id]
	)
	SELECT	I.[id],
		I.[search_criteria_inequality_type_id],
		I.[inequality_id],
		I.[upper_bound],
		I.[lower_bound],
		I.[mileage_unit_id]
	FROM	search_criteria_search_criteria_inequalities C
	JOIN	search_criteria_inequalities I ON C.search_criteria_inequality_id = I.id
	WHERE	C.search_criterion_id = @SearchCriterionID
	
	IF @@ROWCOUNT <> 0
	DELETE	I
	FROM	@Inventory I CROSS JOIN @SearchCriteriaInequalities X
	JOIN	[FLDW].dbo.InventoryBookout_F B ON I.inventory_id = B.InventoryID
	JOIN	dbo.business_unit_preferences P ON P.business_unit_id = I.business_unit_id
	WHERE	COALESCE(B.ThirdPartyCategoryID,P.primary_book_category_id) = P.primary_book_category_id
	AND	dbo.SatisfiesSearchCriteriaInequality(X.inequality_id, X.lower_bound, X.upper_bound, CASE
			WHEN X.search_criteria_inequality_type_id = 1 AND X.mileage_unit_id = 1 THEN
				I.mileage_received
			WHEN X.search_criteria_inequality_type_id = 1 AND X.mileage_unit_id = 2 THEN
				I.mileage_received / I.years_old
			WHEN X.search_criteria_inequality_type_id = 2 THEN
				I.list_price
			WHEN X.search_criteria_inequality_type_id = 3 THEN
				I.unit_cost
			WHEN X.search_criteria_inequality_type_id = 4 THEN
				COALESCE(B.BookValue,0)
			WHEN X.search_criteria_inequality_type_id = 5 THEN
				I.unit_cost-COALESCE(B.BookValue,I.unit_cost)
			WHEN X.search_criteria_inequality_type_id = 6 THEN
				DATEDIFF(DD,I.inventory_received_date,@BaseDate)
			END
		) = 1

	INSERT INTO @Results (InventoryID) SELECT inventory_id FROM @Inventory

	RETURN

END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NewSearchResults]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[NewSearchResults]
GO

CREATE PROCEDURE dbo.NewSearchResults
	@SearchCriterionID INT
AS
	SET NOCOUNT ON

	DECLARE @BusinessUnitID INT, @BaseDate DATETIME, @ERR INT
	
	SELECT	@BusinessUnitID = BusinessUnitID
	FROM	SearchCriteria
	WHERE	SearchCriterionID = @SearchCriterionID
	
	SET @BaseDate = GETDATE()

	BEGIN TRANSACTION T0

	DECLARE @VehicleListID INT
	
	INSERT INTO VehicleLists (VehicleListTypeID, Created, LastModified) VALUES (
		1, @BaseDate, @BaseDate
	)
	
	SET @VehicleListID = SCOPE_IDENTITY() 

	DECLARE @SearchResultID INT
	
	INSERT INTO SearchResults (BusinessUnitID,VehicleListID) VALUES (
		@BusinessUnitID, @VehicleListID
	)

	IF @ERR <> 0 GOTO Failed

	SET @SearchResultID = SCOPE_IDENTITY()
	
	INSERT INTO SearchResultsSearchCriteria (SearchResultID,SearchCriterionID) VALUES (
		@SearchResultID,
		@SearchCriterionID
	)

	IF @ERR <> 0 GOTO Failed

 	DECLARE @VehicleListItems_T TABLE (
 		VehicleID INT,
 		Position  INT
 	)

	INSERT INTO @VehicleListItems_T (VehicleID, Position)
	SELECT
		v.id AS VehicleID,
		0
	FROM
		dbo.SearchInventory(@SearchCriterionID, @BaseDate) s
	JOIN	dbo.inventory i on i.id = s.InventoryID
	JOIN	dbo.vehicles v on v.id = i.vehicle_id
	
	IF @ERR <> 0 GOTO Failed
	
	DECLARE @VehicleID INT, @Position INT
	SET @Position = 0

	DECLARE position_cursor CURSOR FOR
	SELECT	VehicleID
	FROM	@VehicleListItems_T

	OPEN position_cursor

	FETCH NEXT FROM position_cursor INTO @VehicleID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE @VehicleListItems_T SET Position = @Position WHERE CURRENT OF position_cursor
		SET @Position = @Position+1
		FETCH NEXT FROM position_cursor INTO @VehicleID
	END
	CLOSE position_cursor
	DEALLOCATE position_cursor

	IF @ERR <> 0 GOTO Failed
	
	INSERT INTO VehicleListItems (VehicleListID,VehicleID,Position)
	SELECT
		@VehicleListID,
		VehicleID,
		Position
	FROM
		@VehicleListItems_T

	IF @ERR <> 0 GOTO Failed
		
	COMMIT TRANSACTION T0
	
	SELECT @SearchResultID AS SearchResultID, @Position NumberOfVehicles

	RETURN

Failed:
	
	SELECT @ERR AS ErrorCode

	ROLLBACK TRANSACTION T0
	
	RETURN
GO
