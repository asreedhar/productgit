class CreatePingPrices < ActiveRecord::Migration
  def self.up
    create_table :ping_prices do |t|
      t.column :mmg_id, :integer
      t.column :ping_provider_id, :integer
      t.column :highest, :float
      t.column :lowest, :float
      t.column :average, :float
      t.column :zip, :integer
      t.column :distance, :integer
      t.column :year, :integer
      t.column :matches, :integer
      t.column :url, :string
      t.column :created, :datetime
    end
  end

  def self.down
    drop_table :ping_prices
  end
end
