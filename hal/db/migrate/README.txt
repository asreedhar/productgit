
How To (Re-) Build the HAL Instance
===================================

Start in the master database.

1) Run IMT.sql.  This restores production IMT from backup. (30 mins)
2) Once that has completed successfully (it may fail if there are connections using the IMT schema)
   run the commented out code at the bottom of that script.
3) USE IMT
4) Run the scripts:
   - IMT/database/management/scripts/AlterSQL/
        
        None as of 19 Jan 2007

Go back to the master database

5) Follow steps (1) and (2) for the EdgeScorecard.sql and run EXEC PopulateMeasures in EdgeScorecard db
	or just run EXEC PopulateMeasures in EdgeScorecard db if there have been no schema changes as step 9.
	(1.75 or 1.5 Hours depending)

6)  Follow steps (1) and (2) for the Auction.sql and ATC.sql (3 mins)

Go to the FLDW database

7) Run FLDW.sql (2 hours)

Go to the HAL database

8) Run the commands (15 mins)
		EXEC dbo.LoadAppraisalBookout_F
        EXEC dbo.LoadHAL
        --EXEC dbo.LoadInventory_A6
        --EXEC dbo.LoadInventory_A7
        --EXEC dbo.LoadInventory_A8
        EXEC dbo.LoadZipCode_A1
        EXEC dbo.ProcessPlanningCategoryAgeBoundaries
