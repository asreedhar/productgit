
class HasDealerFilter
  
  def self.filter(controller)
    if controller.request.session[:business_unit].nil?
      controller.session[:after_login] = controller.params
      controller.redirect_to(:controller => 'login', :only_path => true) 
    end
    return !controller.request.session[:business_unit].nil?
  end
  
end
