#!/usr/bin/env ruby

require 'base64'

require 'rubygems'

gem 'soap4r'

require 'ReliableMessageQueueDriver.rb'

module ReliableMessageQueue

endpoint_url = ARGV.shift || MESSAGE_QUEUE_ENDPOINT

obj = ReliableMessageQueueServiceSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR # if $DEBUG

# SYNOPSIS
#   add(parameters)
#
# ARGS
#   parameters      QueueRequest - {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueRequest
#
# RETURNS
#   parameters      QueueResponse - {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueResponse
#
parameters = QueueRequest.new('TestClientRuby', QueueItem.new('TestClientRuby', nil, ::Base64.encode64("Hello Ruby World")), nil)
response = obj.add(parameters)
puts "status code: ", response.status.code
puts "status message: ", response.status.message
puts "queue receipt: ", response.queueReceipt

# SYNOPSIS
#   poll(parameters)
#
# ARGS
#   parameters      QueueRequest - {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueRequest
#
# RETURNS
#   parameters      QueueResponse - {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueResponse
#
parameters = QueueRequest.new('TestClientRuby')
response = obj.poll(parameters)
puts "status code: ", response.status.code
puts "status message: ", response.status.message
puts response.queueItem.message
puts ::Base64.decode64(response.queueItem.message)
puts ::Base64.decode64(::Base64.decode64(response.queueItem.message))
puts "queue receipt: ", response.queueItem.queueReceipt

# SYNOPSIS
#   commit(parameters)
#
# ARGS
#   parameters      QueueRequest - {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueRequest
#
# RETURNS
#   parameters      QueueResponse - {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueResponse
#
parameters = QueueRequest.new('TestClientRuby', nil, response.queueItem.queueReceipt)
response = obj.commit(parameters)
puts "status code: ", response.status.code
puts "status message: ", response.status.message

end
