require 'xsd/qname'

module ReliableMessageQueue


# {http://www.firstlook.biz/service/ReliableMessageQueue/v1}Status
class Status
  attr_accessor :code
  attr_accessor :message

  def initialize(code = nil, message = nil)
    @code = code
    @message = message
  end
end

# {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueItem
class QueueItem
  attr_accessor :queueName
  attr_accessor :queueReceipt
  attr_accessor :message

  def initialize(queueName = nil, queueReceipt = nil, message = nil)
    @queueName = queueName
    @queueReceipt = queueReceipt
    @message = message
  end
end

# {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueRequest
class QueueRequest
  attr_accessor :queueName
  attr_accessor :queueItem
  attr_accessor :queueReceipt

  def initialize(queueName = nil, queueItem = nil, queueReceipt = nil)
    @queueName = queueName
    @queueItem = queueItem
    @queueReceipt = queueReceipt
  end
end

# {http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueResponse
class QueueResponse
  attr_accessor :status
  attr_accessor :queueReceipt
  attr_accessor :queueItem

  def initialize(status = nil, queueReceipt = nil, queueItem = nil)
    @status = status
    @queueReceipt = queueReceipt
    @queueItem = queueItem
  end
end

# {http://www.firstlook.biz/service/ReliableMessageQueue/v1}StatusCode
class StatusCode < ::String
  Failure = StatusCode.new("Failure")
  Success = StatusCode.new("Success")
end


end
