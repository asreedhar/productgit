require 'Carfax.rb'
require 'CarfaxMappingRegistry.rb'

module Carfax
require 'soap/rpc/driver'

class CarfaxWebServiceSoap < ::SOAP::RPC::Driver
  DefaultEndpointUrl = "http://localhost:9090/VehicleHistoryReport/Services/CarfaxWebService.asmx"

  Methods = [
    [ "http://services.firstlook.biz/VehicleHistoryReport/Carfax/HasAccount",
      "hasAccount",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "HasAccount"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "HasAccountResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/Carfax/GetReportPreference",
      "getReportPreference",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "GetReportPreference"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "GetReportPreferenceResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/Carfax/CanPurchaseReport",
      "canPurchaseReport",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CanPurchaseReport"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CanPurchaseReportResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/Carfax/GetReport",
      "getReport",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "GetReport"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "GetReportResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/Carfax/PurchaseReport",
      "purchaseReport",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReport"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReportResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/Carfax/PurchaseReports",
      "purchaseReports",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReports"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReportsResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]

  def initialize(endpoint_url = nil)
    endpoint_url ||= DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = DefaultMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = DefaultMappingRegistry::LiteralRegistry
    init_methods
  end

private

  def init_methods
    Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name and qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end


end
