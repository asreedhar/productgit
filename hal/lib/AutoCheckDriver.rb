require 'AutoCheck.rb'
require 'AutoCheckMappingRegistry.rb'

module AutoCheck
require 'soap/rpc/driver'

class AutoCheckWebServiceSoap < ::SOAP::RPC::Driver
  DefaultEndpointUrl = "http://localhost:9090/VehicleHistoryReport/Services/AutoCheckWebService.asmx"

  Methods = [
    [ "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/HasAccount",
      "hasAccount",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "HasAccount"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "HasAccountResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/CanPurchaseReport",
      "canPurchaseReport",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CanPurchaseReport"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CanPurchaseReportResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/GetReport",
      "getReport",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "GetReport"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "GetReportResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/GetReportHtml",
      "getReportHtml",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "GetReportHtml"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "GetReportHtmlResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/PurchaseReport",
      "purchaseReport",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "PurchaseReport"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "PurchaseReportResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/PurchaseReports",
      "purchaseReports",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "PurchaseReports"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "PurchaseReportsResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]

  def initialize(endpoint_url = nil)
    endpoint_url ||= DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = DefaultMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = DefaultMappingRegistry::LiteralRegistry
    init_methods
  end

private

  def init_methods
    Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name and qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end


end
