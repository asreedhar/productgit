#!/usr/bin/env ruby
require 'AutoCheckDriver.rb'


module AutoCheck

endpoint_url = ARGV.shift
obj = AutoCheckWebServiceSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   HasAccount(parameters)
#
# ARGS
#   parameters      HasAccount - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}HasAccount
#
# RETURNS
#   parameters      HasAccountResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}HasAccountResponse
#
parameters = nil
puts obj.hasAccount(parameters)

# SYNOPSIS
#   CanPurchaseReport(parameters)
#
# ARGS
#   parameters      CanPurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}CanPurchaseReport
#
# RETURNS
#   parameters      CanPurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}CanPurchaseReportResponse
#
parameters = nil
puts obj.canPurchaseReport(parameters)

# SYNOPSIS
#   GetReport(parameters)
#
# ARGS
#   parameters      GetReport - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReport
#
# RETURNS
#   parameters      GetReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportResponse
#
parameters = nil
puts obj.getReport(parameters)

# SYNOPSIS
#   GetReportHtml(parameters)
#
# ARGS
#   parameters      GetReportHtml - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportHtml
#
# RETURNS
#   parameters      GetReportHtmlResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportHtmlResponse
#
parameters = nil
puts obj.getReportHtml(parameters)

# SYNOPSIS
#   PurchaseReport(parameters)
#
# ARGS
#   parameters      PurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReport
#
# RETURNS
#   parameters      PurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReportResponse
#
parameters = nil
puts obj.purchaseReport(parameters)

# SYNOPSIS
#   PurchaseReports(parameters)
#
# ARGS
#   parameters      PurchaseReports - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReports
#
# RETURNS
#   parameters      PurchaseReportsResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReportsResponse
#
parameters = nil
puts obj.purchaseReports(parameters)


endpoint_url = ARGV.shift
obj = AutoCheckWebServiceSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   HasAccount(parameters)
#
# ARGS
#   parameters      HasAccount - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}HasAccount
#
# RETURNS
#   parameters      HasAccountResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}HasAccountResponse
#
parameters = nil
puts obj.hasAccount(parameters)

# SYNOPSIS
#   CanPurchaseReport(parameters)
#
# ARGS
#   parameters      CanPurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}CanPurchaseReport
#
# RETURNS
#   parameters      CanPurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}CanPurchaseReportResponse
#
parameters = nil
puts obj.canPurchaseReport(parameters)

# SYNOPSIS
#   GetReport(parameters)
#
# ARGS
#   parameters      GetReport - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReport
#
# RETURNS
#   parameters      GetReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportResponse
#
parameters = nil
puts obj.getReport(parameters)

# SYNOPSIS
#   GetReportHtml(parameters)
#
# ARGS
#   parameters      GetReportHtml - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportHtml
#
# RETURNS
#   parameters      GetReportHtmlResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportHtmlResponse
#
parameters = nil
puts obj.getReportHtml(parameters)

# SYNOPSIS
#   PurchaseReport(parameters)
#
# ARGS
#   parameters      PurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReport
#
# RETURNS
#   parameters      PurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReportResponse
#
parameters = nil
puts obj.purchaseReport(parameters)

# SYNOPSIS
#   PurchaseReports(parameters)
#
# ARGS
#   parameters      PurchaseReports - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReports
#
# RETURNS
#   parameters      PurchaseReportsResponse - {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReportsResponse
#
parameters = nil
puts obj.purchaseReports(parameters)




end
