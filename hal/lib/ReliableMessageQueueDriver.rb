require 'ReliableMessageQueue.rb'
require 'ReliableMessageQueueMappingRegistry.rb'

module ReliableMessageQueue
require 'soap/rpc/driver'

class ReliableMessageQueueServiceSoap < ::SOAP::RPC::Driver
  DefaultEndpointUrl = "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue"

  Methods = [
    [ "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/add",
      "add",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueRequest"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/poll",
      "poll",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueRequest"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/rollback",
      "rollback",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueRequest"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/commit",
      "commit",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueRequest"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]

  def initialize(endpoint_url = nil)
    endpoint_url ||= DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = ReliableMessageQueueMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = ReliableMessageQueueMappingRegistry::LiteralRegistry
    init_methods
  end

private

  def init_methods
    Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name and qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end


end
