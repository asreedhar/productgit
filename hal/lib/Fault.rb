require 'xsd/qname'

module Fault

# {http://services.firstlook.biz/Fault/}SaveRemoteArgumentsDto
class SaveRemoteArgumentsDto
  attr_accessor :faultEvent

  def initialize(faultEvent = nil)
    @faultEvent = faultEvent
  end
end

# {http://services.firstlook.biz/Fault/}FaultEventDto
class FaultEventDto
  attr_accessor :faultTime
  attr_accessor :fault
  attr_accessor :faultData
  attr_accessor :requestInformation
  attr_accessor :application
  attr_accessor :machine
  attr_accessor :platform
  attr_accessor :isNew
  attr_accessor :id

  def initialize(faultTime = nil, fault = nil, faultData = nil, requestInformation = nil, application = nil, machine = nil, platform = nil, isNew = nil, id = nil)
    @faultTime = faultTime
    @fault = fault
    @faultData = faultData
    @requestInformation = requestInformation
    @application = application
    @machine = machine
    @platform = platform
    @isNew = isNew
    @id = id
  end
end

# {http://services.firstlook.biz/Fault/}FaultDto
class FaultDto
  attr_accessor :cause
  attr_accessor :exceptionType
  attr_accessor :message
  attr_accessor :stack

  def initialize(cause = nil, exceptionType = nil, message = nil, stack = nil)
    @cause = cause
    @exceptionType = exceptionType
    @message = message
    @stack = stack
  end
end

# {http://services.firstlook.biz/Fault/}StackDto
class StackDto
  attr_accessor :stackFrames

  def initialize(stackFrames = nil)
    @stackFrames = stackFrames
  end
end

# {http://services.firstlook.biz/Fault/}ArrayOfStackFrameDto
class ArrayOfStackFrameDto < ::Array
end

# {http://services.firstlook.biz/Fault/}StackFrameDto
class StackFrameDto
  attr_accessor :className
  attr_accessor :fileName
  attr_accessor :methodName
  attr_accessor :isNativeMethod
  attr_accessor :isUnknownSource
  attr_accessor :lineNumber

  def initialize(className = nil, fileName = nil, methodName = nil, isNativeMethod = nil, isUnknownSource = nil, lineNumber = nil)
    @className = className
    @fileName = fileName
    @methodName = methodName
    @isNativeMethod = isNativeMethod
    @isUnknownSource = isUnknownSource
    @lineNumber = lineNumber
  end
end

# {http://services.firstlook.biz/Fault/}ArrayOfFaultDataDto
class ArrayOfFaultDataDto < ::Array
end

# {http://services.firstlook.biz/Fault/}FaultDataDto
class FaultDataDto
  attr_accessor :key
  attr_accessor :value

  def initialize(key = nil, value = nil)
    @key = key
    @value = value
  end
end

# {http://services.firstlook.biz/Fault/}RequestInformationDto
class RequestInformationDto
  attr_accessor :path
  attr_accessor :url
  attr_accessor :user
  attr_accessor :userHostAddress

  def initialize(path = nil, url = nil, user = nil, userHostAddress = nil)
    @path = path
    @url = url
    @user = user
    @userHostAddress = userHostAddress
  end
end

# {http://services.firstlook.biz/Fault/}ApplicationDto
class ApplicationDto
  attr_accessor :name

  def initialize(name = nil)
    @name = name
  end
end

# {http://services.firstlook.biz/Fault/}MachineDto
class MachineDto
  attr_accessor :name

  def initialize(name = nil)
    @name = name
  end
end

# {http://services.firstlook.biz/Fault/}PlatformDto
class PlatformDto
  attr_accessor :name

  def initialize(name = nil)
    @name = name
  end
end

# {http://services.firstlook.biz/Fault/}SaveRemoteResultsDto
class SaveRemoteResultsDto
  attr_accessor :faultEvent

  def initialize(faultEvent = nil)
    @faultEvent = faultEvent
  end
end

# {http://services.firstlook.biz/Fault/}SaveFault
class SaveFault
  attr_accessor :arguments

  def initialize(arguments = nil)
    @arguments = arguments
  end
end

# {http://services.firstlook.biz/Fault/}SaveFaultResponse
class SaveFaultResponse
  attr_accessor :saveFaultResult

  def initialize(saveFaultResult = nil)
    @saveFaultResult = saveFaultResult
  end
end

end