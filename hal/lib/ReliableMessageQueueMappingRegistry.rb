require 'ReliableMessageQueue.rb'
require 'soap/mapping'

module ReliableMessageQueue

module ReliableMessageQueueMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new

  EncodedRegistry.register(
    :class => ReliableMessageQueue::Status,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "Status",
    :schema_element => [
      ["code", ["ReliableMessageQueue::StatusCode", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Code")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Message")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ReliableMessageQueue::QueueItem,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "QueueItem",
    :schema_element => [
      ["queueName", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName")]],
      ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")], [0, 1]],
      ["message", ["SOAP::SOAPBase64", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Message")]]
    ]
  )

  EncodedRegistry.register(
    :class => ReliableMessageQueue::QueueRequest,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "QueueRequest",
    :schema_element => [
      ["queueName", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName")]],
      [ :choice,
        ["queueItem", ["ReliableMessageQueue::QueueItem", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem")]],
        ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")]]
      ]
    ]
  )

  EncodedRegistry.register(
    :class => ReliableMessageQueue::QueueResponse,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "QueueResponse",
    :schema_element => [
      ["status", ["ReliableMessageQueue::Status", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Status")]],
      [ :choice,
        ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")]],
        ["queueItem", ["ReliableMessageQueue::QueueItem", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem")]]
      ]
    ]
  )

  EncodedRegistry.register(
    :class => ReliableMessageQueue::StatusCode,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "StatusCode"
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::Status,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "Status",
    :schema_qualified => false,
    :schema_element => [
      ["code", ["ReliableMessageQueue::StatusCode", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Code")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Message")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::QueueItem,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "QueueItem",
    :schema_qualified => false,
    :schema_element => [
      ["queueName", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName")]],
      ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")], [0, 1]],
      ["message", ["SOAP::SOAPBase64", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Message")]]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::QueueRequest,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "QueueRequest",
    :schema_qualified => false,
    :schema_element => [
      ["queueName", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName")]],
      [ :choice,
        ["queueItem", ["ReliableMessageQueue::QueueItem", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem")]],
        ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::QueueResponse,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "QueueResponse",
    :schema_qualified => false,
    :schema_element => [
      ["status", ["ReliableMessageQueue::Status", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Status")]],
      [ :choice,
        ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")]],
        ["queueItem", ["ReliableMessageQueue::QueueItem", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::StatusCode,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_type => "StatusCode"
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::Status,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_name => "Status",
    :schema_qualified => true,
    :schema_element => [
      ["code", ["ReliableMessageQueue::StatusCode", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Code")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Message")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::QueueItem,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_name => "QueueItem",
    :schema_qualified => true,
    :schema_element => [
      ["queueName", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName")]],
      ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")], [0, 1]],
      ["message", ["SOAP::SOAPBase64", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Message")]]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::QueueRequest,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_name => "QueueRequest",
    :schema_qualified => true,
    :schema_element => [
      ["queueName", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName")]],
      [ :choice,
        ["queueItem", ["ReliableMessageQueue::QueueItem", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem")]],
        ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => ReliableMessageQueue::QueueResponse,
    :schema_ns => "http://www.firstlook.biz/service/ReliableMessageQueue/v1",
    :schema_name => "QueueResponse",
    :schema_qualified => true,
    :schema_element => [
      ["status", ["ReliableMessageQueue::Status", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Status")]],
      [ :choice,
        ["queueReceipt", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt")]],
        ["queueItem", ["ReliableMessageQueue::QueueItem", XSD::QName.new("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem")]]
      ]
    ]
  )
end

end
