#!/usr/bin/env ruby

require 'rubygems'

gem 'soap4r'

require 'PrinceXmlDriver.rb'

module PrinceXml

endpoint_url = ARGV.shift || PRINCE_XML_ENDPOINT

obj = PrinceXmlWebService.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR

# SYNOPSIS
#   generate(parameters)
#
# ARGS
#   parameters      PrinceXmlRequest - {http://www.firstlook.biz/service/PrinceXml/v1}PrinceXmlRequest
#
# RETURNS
#   parameters      PrinceXmlResponse - {http://www.firstlook.biz/service/PrinceXml/v1}PrinceXmlResponse
#
parameters = PrinceXmlRequest.new(
  Input.new(ContentType::Html, "<html><body>Ruby Rules</body></html>"),
  OutputType.new(OutputTypeCode::SharedFileSystem, SharedFileSystem.new("test-", ".pdf", "incisent"))
)
response = obj.generate(parameters)
puts "status code: ", response.status.code
puts "status message: ", response.status.message
puts "output type code: ", response.output.outputTypeCode
puts "output path: ", response.output.filePathRelativeToSharedFileSystemRoot

end
