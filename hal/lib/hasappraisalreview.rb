
class HasAppraisalReviewFilter
  
  def self.filter(controller)
    return !controller.request.session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 21}.nil?
  end
  
end
