require 'xsd/qname'

module PrinceXml


# {http://www.firstlook.biz/service/PrinceXml/v1}Status
class Status
  attr_accessor :code
  attr_accessor :message

  def initialize(code = nil, message = nil)
    @code = code
    @message = message
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}SharedFileSystem
class SharedFileSystem
  attr_accessor :filePrefix
  attr_accessor :fileSuffix
  attr_accessor :directoryPathRelativeToSharedFileSystemRoot

  def initialize(filePrefix = nil, fileSuffix = nil, directoryPathRelativeToSharedFileSystemRoot = nil)
    @filePrefix = filePrefix
    @fileSuffix = fileSuffix
    @directoryPathRelativeToSharedFileSystemRoot = directoryPathRelativeToSharedFileSystemRoot
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}OutputType
class OutputType
  attr_accessor :outputTypeCode
  attr_accessor :sharedFileSystem

  def initialize(outputTypeCode = nil, sharedFileSystem = nil)
    @outputTypeCode = outputTypeCode
    @sharedFileSystem = sharedFileSystem
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}Input
class Input
  attr_accessor :contentType
  attr_accessor :content
  attr_accessor :styleSheetPaths

  def initialize(contentType = nil, content = nil, styleSheetPaths = [])
    @contentType = contentType
    @content = content
    @styleSheetPaths = styleSheetPaths
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}PrinceXmlRequest
class PrinceXmlRequest
  attr_accessor :input
  attr_accessor :outputType

  def initialize(input = nil, outputType = nil)
    @input = input
    @outputType = outputType
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}Output
class Output
  attr_accessor :outputTypeCode
  attr_accessor :byteStream
  attr_accessor :filePathRelativeToSharedFileSystemRoot

  def initialize(outputTypeCode = nil, byteStream = nil, filePathRelativeToSharedFileSystemRoot = nil)
    @outputTypeCode = outputTypeCode
    @byteStream = byteStream
    @filePathRelativeToSharedFileSystemRoot = filePathRelativeToSharedFileSystemRoot
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}PrinceXmlResponse
class PrinceXmlResponse
  attr_accessor :status
  attr_accessor :output

  def initialize(status = nil, output = nil)
    @status = status
    @output = output
  end
end

# {http://www.firstlook.biz/service/PrinceXml/v1}StatusCode
class StatusCode < ::String
  ClientError = StatusCode.new("ClientError")
  ServerError = StatusCode.new("ServerError")
  Success = StatusCode.new("Success")
end

# {http://www.firstlook.biz/service/PrinceXml/v1}OutputTypeCode
class OutputTypeCode < ::String
  ByteStream = OutputTypeCode.new("ByteStream")
  SharedFileSystem = OutputTypeCode.new("SharedFileSystem")
end

# {http://www.firstlook.biz/service/PrinceXml/v1}ContentType
class ContentType < ::String
  Html = ContentType.new("Html")
  Xml = ContentType.new("Xml")
end


end
