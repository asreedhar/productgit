#!/usr/bin/env ruby
require 'CarfaxDriver.rb'


module Carfax

endpoint_url = ARGV.shift
obj = CarfaxWebServiceSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   HasAccount(parameters)
#
# ARGS
#   parameters      HasAccount - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}HasAccount
#
# RETURNS
#   parameters      HasAccountResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}HasAccountResponse
#
parameters = nil
puts obj.hasAccount(parameters)

# SYNOPSIS
#   GetReportPreference(parameters)
#
# ARGS
#   parameters      GetReportPreference - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportPreference
#
# RETURNS
#   parameters      GetReportPreferenceResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportPreferenceResponse
#
parameters = nil
puts obj.getReportPreference(parameters)

# SYNOPSIS
#   CanPurchaseReport(parameters)
#
# ARGS
#   parameters      CanPurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CanPurchaseReport
#
# RETURNS
#   parameters      CanPurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CanPurchaseReportResponse
#
parameters = nil
puts obj.canPurchaseReport(parameters)

# SYNOPSIS
#   GetReport(parameters)
#
# ARGS
#   parameters      GetReport - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReport
#
# RETURNS
#   parameters      GetReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportResponse
#
parameters = nil
puts obj.getReport(parameters)

# SYNOPSIS
#   PurchaseReport(parameters)
#
# ARGS
#   parameters      PurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReport
#
# RETURNS
#   parameters      PurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReportResponse
#
parameters = nil
puts obj.purchaseReport(parameters)

# SYNOPSIS
#   PurchaseReports(parameters)
#
# ARGS
#   parameters      PurchaseReports - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReports
#
# RETURNS
#   parameters      PurchaseReportsResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReportsResponse
#
parameters = nil
puts obj.purchaseReports(parameters)


endpoint_url = ARGV.shift
obj = CarfaxWebServiceSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   HasAccount(parameters)
#
# ARGS
#   parameters      HasAccount - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}HasAccount
#
# RETURNS
#   parameters      HasAccountResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}HasAccountResponse
#
parameters = nil
puts obj.hasAccount(parameters)

# SYNOPSIS
#   GetReportPreference(parameters)
#
# ARGS
#   parameters      GetReportPreference - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportPreference
#
# RETURNS
#   parameters      GetReportPreferenceResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportPreferenceResponse
#
parameters = nil
puts obj.getReportPreference(parameters)

# SYNOPSIS
#   CanPurchaseReport(parameters)
#
# ARGS
#   parameters      CanPurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CanPurchaseReport
#
# RETURNS
#   parameters      CanPurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CanPurchaseReportResponse
#
parameters = nil
puts obj.canPurchaseReport(parameters)

# SYNOPSIS
#   GetReport(parameters)
#
# ARGS
#   parameters      GetReport - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReport
#
# RETURNS
#   parameters      GetReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportResponse
#
parameters = nil
puts obj.getReport(parameters)

# SYNOPSIS
#   PurchaseReport(parameters)
#
# ARGS
#   parameters      PurchaseReport - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReport
#
# RETURNS
#   parameters      PurchaseReportResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReportResponse
#
parameters = nil
puts obj.purchaseReport(parameters)

# SYNOPSIS
#   PurchaseReports(parameters)
#
# ARGS
#   parameters      PurchaseReports - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReports
#
# RETURNS
#   parameters      PurchaseReportsResponse - {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReportsResponse
#
parameters = nil
puts obj.purchaseReports(parameters)




end
