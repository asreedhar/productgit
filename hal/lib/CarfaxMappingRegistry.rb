require 'Carfax.rb'
require 'soap/mapping'

module Carfax

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new

  EncodedRegistry.register(
    :class => Carfax::UserIdentity,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "UserIdentity",
    :schema_element => [
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "UserName")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Carfax::CarfaxReportPreferenceTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportPreferenceTO",
    :schema_element => [
      ["displayInHotListings", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "DisplayInHotListings")]],
      ["purchaseReport", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReport")]],
      ["reportType", ["Carfax::CarfaxReportType", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "ReportType")]]
    ]
  )

  EncodedRegistry.register(
    :class => Carfax::CarfaxReportTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportTO",
    :schema_element => [
      ["expirationDate", ["SOAP::SOAPDateTime", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "ExpirationDate")]],
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "UserName")], [0, 1]],
      ["vin", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Vin")], [0, 1]],
      ["reportType", ["Carfax::CarfaxReportType", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "ReportType")]],
      ["displayInHotList", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "DisplayInHotList")]],
      ["hasProblem", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "HasProblem")]],
      ["ownerCount", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "OwnerCount")]],
      ["inspections", ["Carfax::ArrayOfCarfaxReportInspectionTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Inspections")], [0, 1]]
    ]
  )

  EncodedRegistry.set(
    Carfax::ArrayOfCarfaxReportInspectionTO,
    ::SOAP::SOAPArray,
    ::SOAP::Mapping::EncodedRegistry::TypedArrayFactory,
    { :type => XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxReportInspectionTO") }
  )

  EncodedRegistry.register(
    :class => Carfax::CarfaxReportInspectionTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportInspectionTO",
    :schema_element => [
      ["id", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Id")]],
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Name")], [0, 1]],
      ["selected", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Selected")]]
    ]
  )

  EncodedRegistry.register(
    :class => Carfax::VehicleEntityType,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "VehicleEntityType"
  )

  EncodedRegistry.register(
    :class => Carfax::CarfaxReportType,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportType"
  )

  EncodedRegistry.register(
    :class => Carfax::CarfaxTrackingCode,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxTrackingCode"
  )

  LiteralRegistry.register(
    :class => Carfax::UserIdentity,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "UserIdentity",
    :schema_qualified => false,
    :schema_element => [
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "UserName")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::CarfaxReportPreferenceTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportPreferenceTO",
    :schema_qualified => false,
    :schema_element => [
      ["displayInHotListings", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "DisplayInHotListings")]],
      ["purchaseReport", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReport")]],
      ["reportType", ["Carfax::CarfaxReportType", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "ReportType")]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::CarfaxReportTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportTO",
    :schema_qualified => false,
    :schema_element => [
      ["expirationDate", ["SOAP::SOAPDateTime", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "ExpirationDate")]],
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "UserName")], [0, 1]],
      ["vin", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Vin")], [0, 1]],
      ["reportType", ["Carfax::CarfaxReportType", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "ReportType")]],
      ["displayInHotList", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "DisplayInHotList")]],
      ["hasProblem", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "HasProblem")]],
      ["ownerCount", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "OwnerCount")]],
      ["inspections", ["Carfax::ArrayOfCarfaxReportInspectionTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Inspections")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::ArrayOfCarfaxReportInspectionTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "ArrayOfCarfaxReportInspectionTO",
    :schema_element => [
      ["CarfaxReportInspectionTO", ["Carfax::CarfaxReportInspectionTO[]", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxReportInspectionTO")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::CarfaxReportInspectionTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportInspectionTO",
    :schema_qualified => false,
    :schema_element => [
      ["id", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Id")]],
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Name")], [0, 1]],
      ["selected", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "Selected")]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::VehicleEntityType,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "VehicleEntityType"
  )

  LiteralRegistry.register(
    :class => Carfax::CarfaxReportType,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxReportType"
  )

  LiteralRegistry.register(
    :class => Carfax::CarfaxTrackingCode,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_type => "CarfaxTrackingCode"
  )

  LiteralRegistry.register(
    :class => Carfax::HasAccount,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "HasAccount",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::HasAccountResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "HasAccountResponse",
    :schema_qualified => true,
    :schema_element => [
      ["hasAccountResult", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "HasAccountResult")]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::UserIdentity,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "UserIdentity",
    :schema_qualified => true,
    :schema_element => [
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "UserName")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::GetReportPreference,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "GetReportPreference",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vehicleEntityType", "Carfax::VehicleEntityType"]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::GetReportPreferenceResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "GetReportPreferenceResponse",
    :schema_qualified => true,
    :schema_element => [
      ["getReportPreferenceResult", ["Carfax::CarfaxReportPreferenceTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "GetReportPreferenceResult")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::CanPurchaseReport,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "CanPurchaseReport",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::CanPurchaseReportResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "CanPurchaseReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["canPurchaseReportResult", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CanPurchaseReportResult")]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::GetReport,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "GetReport",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vin", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::GetReportResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "GetReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["getReportResult", ["Carfax::CarfaxReportTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "GetReportResult")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::PurchaseReport,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "PurchaseReport",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vin", "SOAP::SOAPString", [0, 1]],
      ["reportType", "Carfax::CarfaxReportType"],
      ["displayInHotList", "SOAP::SOAPBoolean"],
      ["trackingCode", "Carfax::CarfaxTrackingCode"]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::PurchaseReportResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "PurchaseReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["purchaseReportResult", ["Carfax::CarfaxReportTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "PurchaseReportResult")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::PurchaseReports,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "PurchaseReports",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vehicleEntityType", "Carfax::VehicleEntityType"],
      ["reportType", "Carfax::CarfaxReportType"],
      ["displayInHotList", "SOAP::SOAPBoolean"],
      ["trackingCode", "Carfax::CarfaxTrackingCode"]
    ]
  )

  LiteralRegistry.register(
    :class => Carfax::PurchaseReportsResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/Carfax/",
    :schema_name => "PurchaseReportsResponse",
    :schema_qualified => true,
    :schema_element => []
  )
end

end
