
class AssetHostingStrategy
  attr_accessor :asset_path, :asset_host
  
  def initialize(asset_path, asset_host)
    self.asset_path, self.asset_host = asset_path, asset_host
  end
  
  def call(source, request)
    asset_host(source, request)
  end
  
  private
  
  def asset_host(source, request)
    if request.env["HTTP_X_FORWARDED_HOST"]
      url = request.protocol + request.env["HTTP_X_FORWARDED_HOST"] + @asset_path
    elsif request.env["HTTP_HOST"]
      url = request.protocol + request.env["HTTP_HOST"] + @asset_path
    else
      url = request.protocol + @asset_host + @asset_path
    end
    url + source
  end
  
end

module ActionView
  module Helpers
    module AssetTagHelper
      def compute_public_path_dynamic(source, dir, ext)
        source  = "/#{dir}/#{source}" unless source.first == "/" || source.include?(":")
        source << ".#{ext}" unless source.split("/").last.include?(".")
        source << '?' + rails_asset_id(source) if defined?(RAILS_ROOT) && %r{^[-a-z]+://} !~ source
        source  = "#{@controller.request.relative_url_root}#{source}" unless %r{^[-a-z]+://} =~ source
        source = ActionController::Base.asset_host.call(source, request) unless source.include?(":")
        source
      end
      alias compute_public_path_static compute_public_path
      alias compute_public_path compute_public_path_dynamic
    end
  end
end
