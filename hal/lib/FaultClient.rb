#!/usr/bin/env ruby
require 'FaultDriver.rb'

module Fault

endpoint_url = ARGV.shift
obj = FaultSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   SaveFault(parameters)
#
# ARGS
#   parameters      SaveFault - {http://services.firstlook.biz/Fault/}SaveFault
#
# RETURNS
#   parameters      SaveFaultResponse - {http://services.firstlook.biz/Fault/}SaveFaultResponse
#
parameters = nil
puts obj.saveFault(parameters)


endpoint_url = ARGV.shift
obj = FaultSoap.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   SaveFault(parameters)
#
# ARGS
#   parameters      SaveFault - {http://services.firstlook.biz/Fault/}SaveFault
#
# RETURNS
#   parameters      SaveFaultResponse - {http://services.firstlook.biz/Fault/}SaveFaultResponse
#
parameters = nil
puts obj.saveFault(parameters)


end