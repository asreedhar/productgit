require 'xsd/qname'

module GMAC


# {http://www.gmacinspections.com}PostInspectionReport
class PostInspectionReport
  attr_accessor :inspectionReportFile
  attr_accessor :vcDesc
  attr_accessor :stockNo
  attr_accessor :userId
  attr_accessor :password

  def initialize(inspectionReportFile = nil, vcDesc = nil, stockNo = nil, userId = nil, password = nil)
    @inspectionReportFile = inspectionReportFile
    @vcDesc = vcDesc
    @stockNo = stockNo
    @userId = userId
    @password = password
  end
end

# {http://www.gmacinspections.com}PostInspectionReportResponse
class PostInspectionReportResponse
  attr_accessor :directInspectResponse

  def initialize(directInspectResponse = nil)
    @directInspectResponse = directInspectResponse
  end
end

# {http://www.gmacinspections.com}QueryString
class QueryString
  attr_reader :__xmlele_any

  def set_any(elements)
    @__xmlele_any = elements
  end

  def initialize
    @__xmlele_any = nil
  end
end

# {http://www.gmacinspections.com}InspectionReportFile
class InspectionReportFile
  attr_accessor :inspectionReport

  def xmlattr_SendDate
    (@__xmlattr ||= {})[XSD::QName.new(nil, "SendDate")]
  end

  def xmlattr_SendDate=(value)
    (@__xmlattr ||= {})[XSD::QName.new(nil, "SendDate")] = value
  end

  def xmlattr_TotalReports
    (@__xmlattr ||= {})[XSD::QName.new(nil, "TotalReports")]
  end

  def xmlattr_TotalReports=(value)
    (@__xmlattr ||= {})[XSD::QName.new(nil, "TotalReports")] = value
  end

  def initialize(inspectionReport = [])
    @inspectionReport = inspectionReport
    @__xmlattr = {}
  end
end

# {http://www.gmacinspections.com}InspectionReport
class InspectionReport
  attr_accessor :vehicle
  attr_accessor :image
  attr_accessor :exception
  attr_accessor :gMACAccount
  attr_accessor :customerName
  attr_accessor :cobuyerName
  attr_accessor :conditionGrade
  attr_accessor :dateReturned
  attr_accessor :dealerName
  attr_accessor :dealerNumber
  attr_accessor :dealerPhone
  attr_accessor :dealerContact
  attr_accessor :dealerAddress
  attr_accessor :dealerAddress2
  attr_accessor :dealerCity
  attr_accessor :dealerState
  attr_accessor :dealerZip
  attr_accessor :inspectorID
  attr_accessor :inspectionDate
  attr_accessor :comments
  attr_accessor :inspectionRequestDate
  attr_accessor :chargeableTotal
  attr_accessor :nonChargeableTotal
  attr_accessor :total
  attr_accessor :extendedAttributes

  def initialize(vehicle = nil, image = [], exception = [], gMACAccount = nil, customerName = nil, cobuyerName = nil, conditionGrade = nil, dateReturned = nil, dealerName = nil, dealerNumber = nil, dealerPhone = nil, dealerContact = nil, dealerAddress = nil, dealerAddress2 = nil, dealerCity = nil, dealerState = nil, dealerZip = nil, inspectorID = nil, inspectionDate = nil, comments = nil, inspectionRequestDate = nil, chargeableTotal = nil, nonChargeableTotal = nil, total = nil, extendedAttributes = nil)
    @vehicle = vehicle
    @image = image
    @exception = exception
    @gMACAccount = gMACAccount
    @customerName = customerName
    @cobuyerName = cobuyerName
    @conditionGrade = conditionGrade
    @dateReturned = dateReturned
    @dealerName = dealerName
    @dealerNumber = dealerNumber
    @dealerPhone = dealerPhone
    @dealerContact = dealerContact
    @dealerAddress = dealerAddress
    @dealerAddress2 = dealerAddress2
    @dealerCity = dealerCity
    @dealerState = dealerState
    @dealerZip = dealerZip
    @inspectorID = inspectorID
    @inspectionDate = inspectionDate
    @comments = comments
    @inspectionRequestDate = inspectionRequestDate
    @chargeableTotal = chargeableTotal
    @nonChargeableTotal = nonChargeableTotal
    @total = total
    @extendedAttributes = extendedAttributes
  end
end

# {http://www.gmacinspections.com}Vehicle
class Vehicle
  attr_accessor :accessory
  attr_accessor :tire
  attr_accessor :vIN
  attr_accessor :year
  attr_accessor :make
  attr_accessor :model
  attr_accessor :series
  attr_accessor :exteriorColor
  attr_accessor :interiorColor
  attr_accessor :licensePlate
  attr_accessor :licenseState
  attr_accessor :mileage
  attr_accessor :interiorMaterial
  attr_accessor :engineType
  attr_accessor :engineLiters
  attr_accessor :engineCylinder
  attr_accessor :transmissionType
  attr_accessor :turboSuperCharged
  attr_accessor :smokerFlag
  attr_accessor :comments

  def initialize(accessory = [], tire = [], vIN = nil, year = nil, make = nil, model = nil, series = nil, exteriorColor = nil, interiorColor = nil, licensePlate = nil, licenseState = nil, mileage = nil, interiorMaterial = nil, engineType = nil, engineLiters = nil, engineCylinder = nil, transmissionType = nil, turboSuperCharged = nil, smokerFlag = nil, comments = nil)
    @accessory = accessory
    @tire = tire
    @vIN = vIN
    @year = year
    @make = make
    @model = model
    @series = series
    @exteriorColor = exteriorColor
    @interiorColor = interiorColor
    @licensePlate = licensePlate
    @licenseState = licenseState
    @mileage = mileage
    @interiorMaterial = interiorMaterial
    @engineType = engineType
    @engineLiters = engineLiters
    @engineCylinder = engineCylinder
    @transmissionType = transmissionType
    @turboSuperCharged = turboSuperCharged
    @smokerFlag = smokerFlag
    @comments = comments
  end
end

# {http://www.gmacinspections.com}Accessory
class Accessory
  attr_accessor :accessoryID
  attr_accessor :description

  def initialize(accessoryID = nil, description = nil)
    @accessoryID = accessoryID
    @description = description
  end
end

# {http://www.gmacinspections.com}Tire
class Tire
  attr_accessor :location
  attr_accessor :manufacturer
  attr_accessor :size
  attr_accessor :tread
  attr_accessor :comments
  attr_accessor :damageCost

  def initialize(location = nil, manufacturer = nil, size = nil, tread = nil, comments = nil, damageCost = nil)
    @location = location
    @manufacturer = manufacturer
    @size = size
    @tread = tread
    @comments = comments
    @damageCost = damageCost
  end
end

# {http://www.gmacinspections.com}InspectionReportImage
class InspectionReportImage
  attr_accessor :fileName

  def initialize(fileName = nil)
    @fileName = fileName
  end
end

# {http://www.gmacinspections.com}Exception
class Exception
  attr_accessor :type
  attr_accessor :description
  attr_accessor :chargeableFlag
  attr_accessor :frameHours
  attr_accessor :paintHours
  attr_accessor :partCost
  attr_accessor :metalHours
  attr_accessor :repairHours
  attr_accessor :total
  attr_accessor :location

  def initialize(type = nil, description = nil, chargeableFlag = nil, frameHours = nil, paintHours = nil, partCost = nil, metalHours = nil, repairHours = nil, total = nil, location = nil)
    @type = type
    @description = description
    @chargeableFlag = chargeableFlag
    @frameHours = frameHours
    @paintHours = paintHours
    @partCost = partCost
    @metalHours = metalHours
    @repairHours = repairHours
    @total = total
    @location = location
  end
end

# {http://www.gmacinspections.com}InspectionReportExtendedAttributes
class InspectionReportExtendedAttributes
  attr_accessor :owner
  attr_accessor :newPosting
  attr_accessor :postingPrice
  attr_accessor :titleState
  attr_accessor :subOwner

  def initialize(owner = nil, newPosting = nil, postingPrice = nil, titleState = nil, subOwner = nil)
    @owner = owner
    @newPosting = newPosting
    @postingPrice = postingPrice
    @titleState = titleState
    @subOwner = subOwner
  end
end

# {http://www.gmacinspections.com}DirectInspectResponse
class DirectInspectResponse
  attr_accessor :userID
  attr_accessor :vIN
  attr_accessor :valid
  attr_accessor :message
  attr_accessor :timeStamp
  attr_accessor :queryString

  def initialize(userID = nil, vIN = nil, valid = nil, message = nil, timeStamp = nil, queryString = nil)
    @userID = userID
    @vIN = vIN
    @valid = valid
    @message = message
    @timeStamp = timeStamp
    @queryString = queryString
  end
end

# {http://www.gmacinspections.com}TireLocation
class TireLocation < ::String
  FL = TireLocation.new("FL")
  FR = TireLocation.new("FR")
  OT = TireLocation.new("OT")
  RL = TireLocation.new("RL")
  RR = TireLocation.new("RR")
  SP = TireLocation.new("SP")
end

# {http://www.gmacinspections.com}InspectionReportExtendedAttributesOwner
class InspectionReportExtendedAttributesOwner < ::String
  AAEXCHG = InspectionReportExtendedAttributesOwner.new("AAEXCHG")
  ADP = InspectionReportExtendedAttributesOwner.new("ADP")
  AUCT123 = InspectionReportExtendedAttributesOwner.new("AUCT123")
  AUTOUP = InspectionReportExtendedAttributesOwner.new("AUTOUP")
  CDMData = InspectionReportExtendedAttributesOwner.new("CDMData")
  DLRSPEC = InspectionReportExtendedAttributesOwner.new("DLRSPEC")
  DLRWIRE = InspectionReportExtendedAttributesOwner.new("DLRWIRE")
  DMNDLOT = InspectionReportExtendedAttributesOwner.new("DMNDLOT")
  ECUSTOM = InspectionReportExtendedAttributesOwner.new("ECUSTOM")
  FIRSTLOOK = InspectionReportExtendedAttributesOwner.new("FIRSTLOOK")
  MPOWER = InspectionReportExtendedAttributesOwner.new("MPOWER")
  POINT = InspectionReportExtendedAttributesOwner.new("POINT")
  REYNOLDS = InspectionReportExtendedAttributesOwner.new("REYNOLDS")
  Sfsc = InspectionReportExtendedAttributesOwner.new("sfsc")
  TESTONE = InspectionReportExtendedAttributesOwner.new("TESTONE")
  TESTTWO = InspectionReportExtendedAttributesOwner.new("TESTTWO")
  VINSTKR = InspectionReportExtendedAttributesOwner.new("VINSTKR")
end


end
