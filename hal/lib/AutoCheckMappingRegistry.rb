require 'AutoCheck.rb'
require 'soap/mapping'

module AutoCheck

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new

  EncodedRegistry.register(
    :class => AutoCheck::UserIdentity,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "UserIdentity",
    :schema_element => [
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "UserName")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AutoCheck::AutoCheckReportTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "AutoCheckReportTO",
    :schema_element => [
      ["expirationDate", ["SOAP::SOAPDateTime", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "ExpirationDate")]],
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "UserName")], [0, 1]],
      ["vin", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Vin")], [0, 1]],
      ["score", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Score")]],
      ["compareScoreRangeLow", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CompareScoreRangeLow")]],
      ["compareScoreRangeHigh", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CompareScoreRangeHigh")]],
      ["inspections", ["AutoCheck::ArrayOfAutoCheckReportInspectionTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Inspections")], [0, 1]]
    ]
  )

  EncodedRegistry.set(
    AutoCheck::ArrayOfAutoCheckReportInspectionTO,
    ::SOAP::SOAPArray,
    ::SOAP::Mapping::EncodedRegistry::TypedArrayFactory,
    { :type => XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckReportInspectionTO") }
  )

  EncodedRegistry.register(
    :class => AutoCheck::AutoCheckReportInspectionTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "AutoCheckReportInspectionTO",
    :schema_element => [
      ["id", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Id")]],
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Name")], [0, 1]],
      ["selected", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Selected")]]
    ]
  )

  EncodedRegistry.register(
    :class => AutoCheck::VehicleEntityType,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "VehicleEntityType"
  )

  LiteralRegistry.register(
    :class => AutoCheck::UserIdentity,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "UserIdentity",
    :schema_qualified => false,
    :schema_element => [
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "UserName")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::AutoCheckReportTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "AutoCheckReportTO",
    :schema_qualified => false,
    :schema_element => [
      ["expirationDate", ["SOAP::SOAPDateTime", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "ExpirationDate")]],
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "UserName")], [0, 1]],
      ["vin", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Vin")], [0, 1]],
      ["score", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Score")]],
      ["compareScoreRangeLow", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CompareScoreRangeLow")]],
      ["compareScoreRangeHigh", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CompareScoreRangeHigh")]],
      ["inspections", ["AutoCheck::ArrayOfAutoCheckReportInspectionTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Inspections")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::ArrayOfAutoCheckReportInspectionTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "ArrayOfAutoCheckReportInspectionTO",
    :schema_element => [
      ["AutoCheckReportInspectionTO", ["AutoCheck::AutoCheckReportInspectionTO[]", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckReportInspectionTO")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::AutoCheckReportInspectionTO,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "AutoCheckReportInspectionTO",
    :schema_qualified => false,
    :schema_element => [
      ["id", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Id")]],
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Name")], [0, 1]],
      ["selected", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "Selected")]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::VehicleEntityType,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_type => "VehicleEntityType"
  )

  LiteralRegistry.register(
    :class => AutoCheck::HasAccount,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "HasAccount",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::HasAccountResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "HasAccountResponse",
    :schema_qualified => true,
    :schema_element => [
      ["hasAccountResult", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "HasAccountResult")]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::UserIdentity,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "UserIdentity",
    :schema_qualified => true,
    :schema_element => [
      ["userName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "UserName")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::CanPurchaseReport,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "CanPurchaseReport",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::CanPurchaseReportResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "CanPurchaseReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["canPurchaseReportResult", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "CanPurchaseReportResult")]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::GetReport,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "GetReport",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vin", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::GetReportResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "GetReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["getReportResult", ["AutoCheck::AutoCheckReportTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "GetReportResult")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::GetReportHtml,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "GetReportHtml",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vin", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::GetReportHtmlResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "GetReportHtmlResponse",
    :schema_qualified => true,
    :schema_element => [
      ["getReportHtmlResult", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "GetReportHtmlResult")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::PurchaseReport,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "PurchaseReport",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vin", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::PurchaseReportResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "PurchaseReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["purchaseReportResult", ["AutoCheck::AutoCheckReportTO", XSD::QName.new("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "PurchaseReportResult")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::PurchaseReports,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "PurchaseReports",
    :schema_qualified => true,
    :schema_element => [
      ["dealerId", "SOAP::SOAPInt"],
      ["vehicleEntityType", "AutoCheck::VehicleEntityType"]
    ]
  )

  LiteralRegistry.register(
    :class => AutoCheck::PurchaseReportsResponse,
    :schema_ns => "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/",
    :schema_name => "PurchaseReportsResponse",
    :schema_qualified => true,
    :schema_element => []
  )
end

end
