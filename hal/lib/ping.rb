#!/usr/local/bin/ruby

require 'net/https'
require 'open-uri'
require 'cgi'
require 'yaml'
require 'rexml/document'
require 'logger'

module Ping
  
  class PingAdapter
  
    @@ssl = true
    @@host = "devhal.firstlook.biz"
    @@port = 443
    @@path = "/ping/price.xml"
    
    cattr_accessor :host, :port, :path, :ssl
    
    def self.version
    "1.0.0"
    end
    
    def initialize
    end
    
    def initialize(options={})
    end
    
    def parse(options)
      options = { :mmg => 102097, :zip => 60202, :distance => 50, :year => 2004, :provider_code => "autotrader" }.merge(options)
      
      @req = Net::HTTP::new(@@host, @@port )
      @req.use_ssl = @@ssl
      @req.verify_mode = OpenSSL::SSL::VERIFY_NONE if @@ssl
      
      print "HOST: " + @@host.to_s
      print "PORT: " + @@port.to_s
      
      headers = { 'Content-Type' => 'application/x-www-form-urlencoded' }
      
      res = @req.post(@@path, "provider-code=#{options[:provider_code]}&mmg=#{options[:mmg]}&zip=#{options[:zip]}&distance=#{options[:distance]}&year=#{options[:year]}", headers)
      
      # parse our response headers for the room number.. magic!
      doc = REXML::Document.new res.body 
      
      prices = doc.elements["/prices"]
      distance = prices.attribute("distance").value
      year = prices.attribute("year").value
      zip = prices.attribute("zip").value
      matches = prices.attribute("matches").value
      highest = doc.elements["/prices/highest"].text
      average = doc.elements["/prices/average"].text
      lowest = doc.elements["/prices/lowest"].text
      url = doc.elements["/prices/url"].text
      
      { "mmg_id" => options[:mmg], "distance" => distance, "year" => year, 
      "zip" => zip, "matches" => matches, "highest" => highest, 
      "average" => average, "lowest" => lowest, "url" => url }
    end
    
    
    
  end
  
end