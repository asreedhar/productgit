require 'PrinceXml.rb'
require 'PrinceXmlMappingRegistry.rb'

module PrinceXml
require 'soap/rpc/driver'

class PrinceXmlWebService < ::SOAP::RPC::Driver
  DefaultEndpointUrl = "https://www.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService"

  Methods = [
    [ "https://www.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService/generate",
      "generate",
      [ ["in", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlRequest"]],
        ["out", "parameters", ["::SOAP::SOAPElement", "http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]

  def initialize(endpoint_url = nil)
    endpoint_url ||= DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = PrinceXmlMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = PrinceXmlMappingRegistry::LiteralRegistry
    init_methods
  end

private

  def init_methods
    Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name and qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end


end
