require 'PrinceXml.rb'
require 'soap/mapping'

module PrinceXml

module PrinceXmlMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new

  EncodedRegistry.register(
    :class => PrinceXml::Status,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "Status",
    :schema_element => [
      ["code", ["PrinceXml::StatusCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Code")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Message")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::SharedFileSystem,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "SharedFileSystem",
    :schema_element => [
      ["filePrefix", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FilePrefix")]],
      ["fileSuffix", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FileSuffix")]],
      ["directoryPathRelativeToSharedFileSystemRoot", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "DirectoryPathRelativeToSharedFileSystemRoot")]]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::OutputType,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "OutputType",
    :schema_element => [
      ["outputTypeCode", ["PrinceXml::OutputTypeCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputTypeCode")]],
      [ :choice,
        ["sharedFileSystem", ["PrinceXml::SharedFileSystem", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "SharedFileSystem")]]
      ]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::Input,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "Input",
    :schema_element => [
      ["contentType", ["PrinceXml::ContentType", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "ContentType")]],
      ["content", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Content")]],
      ["styleSheetPaths", ["SOAP::SOAPString[]", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "StyleSheetPaths")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::PrinceXmlRequest,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "PrinceXmlRequest",
    :schema_element => [
      ["input", ["PrinceXml::Input", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Input")]],
      ["outputType", ["PrinceXml::OutputType", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputType")]]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::Output,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "Output",
    :schema_element => [
      ["outputTypeCode", ["PrinceXml::OutputTypeCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputTypeCode")]],
      [ :choice,
        ["byteStream", ["SOAP::SOAPHexBinary", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "ByteStream")]],
        ["filePathRelativeToSharedFileSystemRoot", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FilePathRelativeToSharedFileSystemRoot")]]
      ]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::PrinceXmlResponse,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "PrinceXmlResponse",
    :schema_element => [
      ["status", ["PrinceXml::Status", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Status")]],
      ["output", ["PrinceXml::Output", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Output")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => PrinceXml::StatusCode,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "StatusCode"
  )

  EncodedRegistry.register(
    :class => PrinceXml::OutputTypeCode,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "OutputTypeCode"
  )

  EncodedRegistry.register(
    :class => PrinceXml::ContentType,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "ContentType"
  )

  LiteralRegistry.register(
    :class => PrinceXml::Status,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "Status",
    :schema_qualified => false,
    :schema_element => [
      ["code", ["PrinceXml::StatusCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Code")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Message")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::SharedFileSystem,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "SharedFileSystem",
    :schema_qualified => false,
    :schema_element => [
      ["filePrefix", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FilePrefix")]],
      ["fileSuffix", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FileSuffix")]],
      ["directoryPathRelativeToSharedFileSystemRoot", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "DirectoryPathRelativeToSharedFileSystemRoot")]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::OutputType,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "OutputType",
    :schema_qualified => false,
    :schema_element => [
      ["outputTypeCode", ["PrinceXml::OutputTypeCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputTypeCode")]],
      [ :choice,
        ["sharedFileSystem", ["PrinceXml::SharedFileSystem", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "SharedFileSystem")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::Input,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "Input",
    :schema_qualified => false,
    :schema_element => [
      ["contentType", ["PrinceXml::ContentType", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "ContentType")]],
      ["content", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Content")]],
      ["styleSheetPaths", ["SOAP::SOAPString[]", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "StyleSheetPaths")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::PrinceXmlRequest,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "PrinceXmlRequest",
    :schema_qualified => false,
    :schema_element => [
      ["input", ["PrinceXml::Input", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Input")]],
      ["outputType", ["PrinceXml::OutputType", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputType")]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::Output,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "Output",
    :schema_qualified => false,
    :schema_element => [
      ["outputTypeCode", ["PrinceXml::OutputTypeCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputTypeCode")]],
      [ :choice,
        ["byteStream", ["SOAP::SOAPHexBinary", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "ByteStream")]],
        ["filePathRelativeToSharedFileSystemRoot", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FilePathRelativeToSharedFileSystemRoot")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::PrinceXmlResponse,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "PrinceXmlResponse",
    :schema_qualified => false,
    :schema_element => [
      ["status", ["PrinceXml::Status", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Status")]],
      ["output", ["PrinceXml::Output", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Output")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::StatusCode,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "StatusCode"
  )

  LiteralRegistry.register(
    :class => PrinceXml::OutputTypeCode,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "OutputTypeCode"
  )

  LiteralRegistry.register(
    :class => PrinceXml::ContentType,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_type => "ContentType"
  )

  LiteralRegistry.register(
    :class => PrinceXml::Status,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "Status",
    :schema_qualified => true,
    :schema_element => [
      ["code", ["PrinceXml::StatusCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Code")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Message")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::SharedFileSystem,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "SharedFileSystem",
    :schema_qualified => true,
    :schema_element => [
      ["filePrefix", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FilePrefix")]],
      ["fileSuffix", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FileSuffix")]],
      ["directoryPathRelativeToSharedFileSystemRoot", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "DirectoryPathRelativeToSharedFileSystemRoot")]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::OutputType,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "OutputType",
    :schema_qualified => true,
    :schema_element => [
      ["outputTypeCode", ["PrinceXml::OutputTypeCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputTypeCode")]],
      [ :choice,
        ["sharedFileSystem", ["PrinceXml::SharedFileSystem", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "SharedFileSystem")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::Input,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "Input",
    :schema_qualified => true,
    :schema_element => [
      ["contentType", ["PrinceXml::ContentType", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "ContentType")]],
      ["content", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Content")]],
      ["styleSheetPaths", ["SOAP::SOAPString[]", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "StyleSheetPaths")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::PrinceXmlRequest,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "PrinceXmlRequest",
    :schema_qualified => true,
    :schema_element => [
      ["input", ["PrinceXml::Input", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Input")]],
      ["outputType", ["PrinceXml::OutputType", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputType")]]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::Output,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "Output",
    :schema_qualified => true,
    :schema_element => [
      ["outputTypeCode", ["PrinceXml::OutputTypeCode", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "OutputTypeCode")]],
      [ :choice,
        ["byteStream", ["SOAP::SOAPHexBinary", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "ByteStream")]],
        ["filePathRelativeToSharedFileSystemRoot", ["SOAP::SOAPString", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "FilePathRelativeToSharedFileSystemRoot")]]
      ]
    ]
  )

  LiteralRegistry.register(
    :class => PrinceXml::PrinceXmlResponse,
    :schema_ns => "http://www.firstlook.biz/service/PrinceXml/v1",
    :schema_name => "PrinceXmlResponse",
    :schema_qualified => true,
    :schema_element => [
      ["status", ["PrinceXml::Status", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Status")]],
      ["output", ["PrinceXml::Output", XSD::QName.new("http://www.firstlook.biz/service/PrinceXml/v1", "Output")], [0, 1]]
    ]
  )
end

end
