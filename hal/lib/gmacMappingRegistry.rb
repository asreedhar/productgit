require 'gmac.rb'
require 'soap/mapping'

module GMAC

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new

  EncodedRegistry.register(
    :class => GMAC::InspectionReportFile,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportFile",
    :schema_element => [
      ["inspectionReport", ["GMAC::InspectionReport[]", XSD::QName.new("http://www.gmacinspections.com", "InspectionReport")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::InspectionReport,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReport",
    :schema_element => [
      ["vehicle", ["GMAC::Vehicle", XSD::QName.new("http://www.gmacinspections.com", "Vehicle")]],
      ["image", ["GMAC::InspectionReportImage[]", XSD::QName.new("http://www.gmacinspections.com", "Image")]],
      ["exception", ["GMAC::Exception[]", XSD::QName.new("http://www.gmacinspections.com", "Exception")]],
      ["gMACAccount", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "GMACAccount")]],
      ["customerName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "CustomerName")]],
      ["cobuyerName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "CobuyerName")]],
      ["conditionGrade", ["SOAP::SOAPInteger", XSD::QName.new("http://www.gmacinspections.com", "ConditionGrade")]],
      ["dateReturned", ["SOAP::SOAPDate", XSD::QName.new("http://www.gmacinspections.com", "DateReturned")]],
      ["dealerName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerName")]],
      ["dealerNumber", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerNumber")]],
      ["dealerPhone", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerPhone")]],
      ["dealerContact", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerContact")]],
      ["dealerAddress", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerAddress")]],
      ["dealerAddress2", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerAddress2")]],
      ["dealerCity", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerCity")]],
      ["dealerState", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerState")]],
      ["dealerZip", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerZip")]],
      ["inspectorID", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "InspectorID")]],
      ["inspectionDate", ["SOAP::SOAPDate", XSD::QName.new("http://www.gmacinspections.com", "InspectionDate")]],
      ["comments", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Comments")]],
      ["inspectionRequestDate", ["SOAP::SOAPDate", XSD::QName.new("http://www.gmacinspections.com", "InspectionRequestDate")]],
      ["chargeableTotal", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "ChargeableTotal")]],
      ["nonChargeableTotal", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "NonChargeableTotal")]],
      ["total", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "Total")]],
      ["extendedAttributes", ["GMAC::InspectionReportExtendedAttributes", XSD::QName.new("http://www.gmacinspections.com", "ExtendedAttributes")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::Vehicle,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Vehicle",
    :schema_element => [
      ["accessory", ["GMAC::Accessory[]", XSD::QName.new("http://www.gmacinspections.com", "Accessory")]],
      ["tire", ["GMAC::Tire[]", XSD::QName.new("http://www.gmacinspections.com", "Tire")]],
      ["vIN", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "VIN")]],
      ["year", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Year")]],
      ["make", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Make")]],
      ["model", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Model")]],
      ["series", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Series")]],
      ["exteriorColor", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "ExteriorColor")]],
      ["interiorColor", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "InteriorColor")]],
      ["licensePlate", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "LicensePlate")]],
      ["licenseState", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "LicenseState")]],
      ["mileage", ["SOAP::SOAPLong", XSD::QName.new("http://www.gmacinspections.com", "Mileage")]],
      ["interiorMaterial", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "InteriorMaterial")]],
      ["engineType", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "EngineType")]],
      ["engineLiters", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "EngineLiters")]],
      ["engineCylinder", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "EngineCylinder")]],
      ["transmissionType", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TransmissionType")]],
      ["turboSuperCharged", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TurboSuperCharged")]],
      ["smokerFlag", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "SmokerFlag")]],
      ["comments", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Comments")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::Accessory,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Accessory",
    :schema_element => [
      ["accessoryID", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "AccessoryID")]],
      ["description", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Description")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::Tire,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Tire",
    :schema_element => [
      ["location", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Location")]],
      ["manufacturer", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Manufacturer")]],
      ["size", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Size")]],
      ["tread", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Tread")]],
      ["comments", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Comments")]],
      ["damageCost", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "DamageCost")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::InspectionReportImage,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportImage",
    :schema_element => [
      ["fileName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "FileName")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::Exception,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Exception",
    :schema_element => [
      ["type", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Type")]],
      ["description", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Description")]],
      ["chargeableFlag", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "ChargeableFlag")]],
      ["frameHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "FrameHours")]],
      ["paintHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "PaintHours")]],
      ["partCost", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "PartCost")]],
      ["metalHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "MetalHours")]],
      ["repairHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "RepairHours")]],
      ["total", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "Total")]],
      ["location", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Location")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::InspectionReportExtendedAttributes,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportExtendedAttributes",
    :schema_element => [
      ["owner", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Owner")]],
      ["newPosting", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "NewPosting")]],
      ["postingPrice", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "PostingPrice")]],
      ["titleState", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TitleState")]],
      ["subOwner", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "SubOwner")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::DirectInspectResponse,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "DirectInspectResponse",
    :schema_element => [
      ["userID", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "UserID")]],
      ["vIN", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "VIN")]],
      ["valid", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "Valid")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Message")]],
      ["timeStamp", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TimeStamp")]],
      ["queryString", ["GMAC::QueryString", XSD::QName.new(nil, "QueryString")]]
    ]
  )

  EncodedRegistry.register(
    :class => GMAC::TireLocation,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "TireLocation"
  )

  EncodedRegistry.register(
    :class => GMAC::InspectionReportExtendedAttributesOwner,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportExtendedAttributesOwner"
  )

  LiteralRegistry.register(
    :class => GMAC::PostInspectionReport,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_name => "PostInspectionReport",
    :schema_qualified => false,
    :schema_element => [
      ["inspectionReportFile", ["GMAC::InspectionReportFile", XSD::QName.new("http://www.gmacinspections.com", "InspectionReportFile")]],
      ["vcDesc", "SOAP::SOAPString"],
      ["stockNo", "SOAP::SOAPString"],
      ["userId", "SOAP::SOAPString"],
      ["password", "SOAP::SOAPString"]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::PostInspectionReportResponse,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_name => "PostInspectionReportResponse",
    :schema_qualified => true,
    :schema_element => [
      ["directInspectResponse", ["GMAC::DirectInspectResponse", XSD::QName.new("http://www.gmacinspections.com", "DirectInspectResponse")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::QueryString,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_name => "QueryString",
    :schema_qualified => true,
    :schema_element => [
      ["any", [nil, XSD::QName.new("http://www.w3.org/2001/XMLSchema", "anyType")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::InspectionReportFile,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportFile",
    :schema_qualified => false,
    :schema_element => [
      ["inspectionReport", ["GMAC::InspectionReport[]", XSD::QName.new("http://www.gmacinspections.com", "InspectionReport")]]
    ],
    :schema_attribute => {
      XSD::QName.new(nil, "SendDate") => "SOAP::SOAPDateTime",
      XSD::QName.new(nil, "TotalReports") => "SOAP::SOAPInteger"
    }
  )

  LiteralRegistry.register(
    :class => GMAC::InspectionReport,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReport",
    :schema_qualified => false,
    :schema_element => [
      ["vehicle", ["GMAC::Vehicle", XSD::QName.new("http://www.gmacinspections.com", "Vehicle")]],
      ["image", ["GMAC::InspectionReportImage[]", XSD::QName.new("http://www.gmacinspections.com", "Image")]],
      ["exception", ["GMAC::Exception[]", XSD::QName.new("http://www.gmacinspections.com", "Exception")]],
      ["gMACAccount", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "GMACAccount")]],
      ["customerName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "CustomerName")]],
      ["cobuyerName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "CobuyerName")]],
      ["conditionGrade", ["SOAP::SOAPInteger", XSD::QName.new("http://www.gmacinspections.com", "ConditionGrade")]],
      ["dateReturned", ["SOAP::SOAPDate", XSD::QName.new("http://www.gmacinspections.com", "DateReturned")]],
      ["dealerName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerName")]],
      ["dealerNumber", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerNumber")]],
      ["dealerPhone", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerPhone")]],
      ["dealerContact", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerContact")]],
      ["dealerAddress", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerAddress")]],
      ["dealerAddress2", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerAddress2")]],
      ["dealerCity", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerCity")]],
      ["dealerState", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerState")]],
      ["dealerZip", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "DealerZip")]],
      ["inspectorID", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "InspectorID")]],
      ["inspectionDate", ["SOAP::SOAPDate", XSD::QName.new("http://www.gmacinspections.com", "InspectionDate")]],
      ["comments", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Comments")]],
      ["inspectionRequestDate", ["SOAP::SOAPDate", XSD::QName.new("http://www.gmacinspections.com", "InspectionRequestDate")]],
      ["chargeableTotal", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "ChargeableTotal")]],
      ["nonChargeableTotal", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "NonChargeableTotal")]],
      ["total", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "Total")]],
      ["extendedAttributes", ["GMAC::InspectionReportExtendedAttributes", XSD::QName.new("http://www.gmacinspections.com", "ExtendedAttributes")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::Vehicle,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Vehicle",
    :schema_qualified => false,
    :schema_element => [
      ["accessory", ["GMAC::Accessory[]", XSD::QName.new("http://www.gmacinspections.com", "Accessory")]],
      ["tire", ["GMAC::Tire[]", XSD::QName.new("http://www.gmacinspections.com", "Tire")]],
      ["vIN", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "VIN")]],
      ["year", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Year")]],
      ["make", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Make")]],
      ["model", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Model")]],
      ["series", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Series")]],
      ["exteriorColor", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "ExteriorColor")]],
      ["interiorColor", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "InteriorColor")]],
      ["licensePlate", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "LicensePlate")]],
      ["licenseState", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "LicenseState")]],
      ["mileage", ["SOAP::SOAPLong", XSD::QName.new("http://www.gmacinspections.com", "Mileage")]],
      ["interiorMaterial", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "InteriorMaterial")]],
      ["engineType", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "EngineType")]],
      ["engineLiters", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "EngineLiters")]],
      ["engineCylinder", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "EngineCylinder")]],
      ["transmissionType", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TransmissionType")]],
      ["turboSuperCharged", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TurboSuperCharged")]],
      ["smokerFlag", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "SmokerFlag")]],
      ["comments", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Comments")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::Accessory,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Accessory",
    :schema_qualified => false,
    :schema_element => [
      ["accessoryID", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "AccessoryID")]],
      ["description", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Description")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::Tire,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Tire",
    :schema_qualified => false,
    :schema_element => [
      ["location", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Location")]],
      ["manufacturer", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Manufacturer")]],
      ["size", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Size")]],
      ["tread", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Tread")]],
      ["comments", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Comments")]],
      ["damageCost", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "DamageCost")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::InspectionReportImage,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportImage",
    :schema_qualified => false,
    :schema_element => [
      ["fileName", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "FileName")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::Exception,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "Exception",
    :schema_qualified => false,
    :schema_element => [
      ["type", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Type")]],
      ["description", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Description")]],
      ["chargeableFlag", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "ChargeableFlag")]],
      ["frameHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "FrameHours")]],
      ["paintHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "PaintHours")]],
      ["partCost", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "PartCost")]],
      ["metalHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "MetalHours")]],
      ["repairHours", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "RepairHours")]],
      ["total", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "Total")]],
      ["location", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Location")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::InspectionReportExtendedAttributes,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportExtendedAttributes",
    :schema_qualified => true,
    :schema_element => [
      ["owner", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Owner")]],
      ["newPosting", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "NewPosting")]],
      ["postingPrice", ["SOAP::SOAPDecimal", XSD::QName.new("http://www.gmacinspections.com", "PostingPrice")]],
      ["titleState", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TitleState")]],
      ["subOwner", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "SubOwner")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::DirectInspectResponse,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "DirectInspectResponse",
    :schema_qualified => false,
    :schema_element => [
      ["userID", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "UserID")]],
      ["vIN", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "VIN")]],
      ["valid", ["SOAP::SOAPBoolean", XSD::QName.new("http://www.gmacinspections.com", "Valid")]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "Message")]],
      ["timeStamp", ["SOAP::SOAPString", XSD::QName.new("http://www.gmacinspections.com", "TimeStamp")]],
      ["queryString", ["GMAC::QueryString", XSD::QName.new(nil, "QueryString")]]
    ]
  )

  LiteralRegistry.register(
    :class => GMAC::TireLocation,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "TireLocation"
  )

  LiteralRegistry.register(
    :class => GMAC::InspectionReportExtendedAttributesOwner,
    :schema_ns => "http://www.gmacinspections.com",
    :schema_type => "InspectionReportExtendedAttributesOwner"
  )
end

end
