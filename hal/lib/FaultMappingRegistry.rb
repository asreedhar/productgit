require 'Fault.rb'
require 'soap/mapping'

module Fault

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new

  EncodedRegistry.register(
    :class => Fault::SaveRemoteArgumentsDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "SaveRemoteArgumentsDto",
    :schema_element => [
      ["faultEvent", ["Fault::FaultEventDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultEvent")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::FaultEventDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "FaultEventDto",
    :schema_element => [
      ["faultTime", ["SOAP::SOAPDateTime", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultTime")]],
      ["fault", ["Fault::FaultDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Fault")], [0, 1]],
      ["faultData", ["Fault::ArrayOfFaultDataDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultData")], [0, 1]],
      ["requestInformation", ["Fault::RequestInformationDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "RequestInformation")], [0, 1]],
      ["application", ["Fault::ApplicationDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Application")], [0, 1]],
      ["machine", ["Fault::MachineDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Machine")], [0, 1]],
      ["platform", ["Fault::PlatformDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Platform")], [0, 1]],
      ["isNew", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/Fault/", "IsNew")]],
      ["id", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/Fault/", "Id")]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::FaultDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "FaultDto",
    :schema_element => [
      ["cause", ["Fault::FaultDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Cause")], [0, 1]],
      ["exceptionType", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "ExceptionType")], [0, 1]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Message")], [0, 1]],
      ["stack", ["Fault::StackDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Stack")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::StackDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "StackDto",
    :schema_element => [
      ["stackFrames", ["Fault::ArrayOfStackFrameDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "StackFrames")], [0, 1]]
    ]
  )

  EncodedRegistry.set(
    Fault::ArrayOfStackFrameDto,
    ::SOAP::SOAPArray,
    ::SOAP::Mapping::EncodedRegistry::TypedArrayFactory,
    { :type => XSD::QName.new("http://services.firstlook.biz/Fault/", "StackFrameDto") }
  )

  EncodedRegistry.register(
    :class => Fault::StackFrameDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "StackFrameDto",
    :schema_element => [
      ["className", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "ClassName")], [0, 1]],
      ["fileName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "FileName")], [0, 1]],
      ["methodName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "MethodName")], [0, 1]],
      ["isNativeMethod", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/Fault/", "IsNativeMethod")]],
      ["isUnknownSource", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/Fault/", "IsUnknownSource")]],
      ["lineNumber", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/Fault/", "LineNumber")]]
    ]
  )

  EncodedRegistry.set(
    Fault::ArrayOfFaultDataDto,
    ::SOAP::SOAPArray,
    ::SOAP::Mapping::EncodedRegistry::TypedArrayFactory,
    { :type => XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultDataDto") }
  )

  EncodedRegistry.register(
    :class => Fault::FaultDataDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "FaultDataDto",
    :schema_element => [
      ["key", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Key")], [0, 1]],
      ["value", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Value")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::RequestInformationDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "RequestInformationDto",
    :schema_element => [
      ["path", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Path")], [0, 1]],
      ["url", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Url")], [0, 1]],
      ["user", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "User")], [0, 1]],
      ["userHostAddress", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "UserHostAddress")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::ApplicationDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "ApplicationDto",
    :schema_element => [
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Name")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::MachineDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "MachineDto",
    :schema_element => [
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Name")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::PlatformDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "PlatformDto",
    :schema_element => [
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Name")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Fault::SaveRemoteResultsDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "SaveRemoteResultsDto",
    :schema_element => [
      ["faultEvent", ["Fault::FaultEventDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultEvent")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::SaveRemoteArgumentsDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "SaveRemoteArgumentsDto",
    :schema_qualified => false,
    :schema_element => [
      ["faultEvent", ["Fault::FaultEventDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultEvent")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::FaultEventDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "FaultEventDto",
    :schema_qualified => false,
    :schema_element => [
      ["faultTime", ["SOAP::SOAPDateTime", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultTime")]],
      ["fault", ["Fault::FaultDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Fault")], [0, 1]],
      ["faultData", ["Fault::ArrayOfFaultDataDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultData")], [0, 1]],
      ["requestInformation", ["Fault::RequestInformationDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "RequestInformation")], [0, 1]],
      ["application", ["Fault::ApplicationDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Application")], [0, 1]],
      ["machine", ["Fault::MachineDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Machine")], [0, 1]],
      ["platform", ["Fault::PlatformDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Platform")], [0, 1]],
      ["isNew", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/Fault/", "IsNew")]],
      ["id", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/Fault/", "Id")]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::FaultDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "FaultDto",
    :schema_qualified => false,
    :schema_element => [
      ["cause", ["Fault::FaultDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Cause")], [0, 1]],
      ["exceptionType", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "ExceptionType")], [0, 1]],
      ["message", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Message")], [0, 1]],
      ["stack", ["Fault::StackDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "Stack")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::StackDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "StackDto",
    :schema_qualified => false,
    :schema_element => [
      ["stackFrames", ["Fault::ArrayOfStackFrameDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "StackFrames")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::ArrayOfStackFrameDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "ArrayOfStackFrameDto",
    :schema_element => [
      ["StackFrameDto", ["Fault::StackFrameDto[]", XSD::QName.new("http://services.firstlook.biz/Fault/", "StackFrameDto")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::StackFrameDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "StackFrameDto",
    :schema_qualified => false,
    :schema_element => [
      ["className", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "ClassName")], [0, 1]],
      ["fileName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "FileName")], [0, 1]],
      ["methodName", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "MethodName")], [0, 1]],
      ["isNativeMethod", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/Fault/", "IsNativeMethod")]],
      ["isUnknownSource", ["SOAP::SOAPBoolean", XSD::QName.new("http://services.firstlook.biz/Fault/", "IsUnknownSource")]],
      ["lineNumber", ["SOAP::SOAPInt", XSD::QName.new("http://services.firstlook.biz/Fault/", "LineNumber")]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::ArrayOfFaultDataDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "ArrayOfFaultDataDto",
    :schema_element => [
      ["FaultDataDto", ["Fault::FaultDataDto[]", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultDataDto")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::FaultDataDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "FaultDataDto",
    :schema_qualified => false,
    :schema_element => [
      ["key", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Key")], [0, 1]],
      ["value", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Value")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::RequestInformationDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "RequestInformationDto",
    :schema_qualified => false,
    :schema_element => [
      ["path", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Path")], [0, 1]],
      ["url", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Url")], [0, 1]],
      ["user", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "User")], [0, 1]],
      ["userHostAddress", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "UserHostAddress")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::ApplicationDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "ApplicationDto",
    :schema_qualified => false,
    :schema_element => [
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Name")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::MachineDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "MachineDto",
    :schema_qualified => false,
    :schema_element => [
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Name")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::PlatformDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "PlatformDto",
    :schema_qualified => false,
    :schema_element => [
      ["name", ["SOAP::SOAPString", XSD::QName.new("http://services.firstlook.biz/Fault/", "Name")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::SaveRemoteResultsDto,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_type => "SaveRemoteResultsDto",
    :schema_qualified => false,
    :schema_element => [
      ["faultEvent", ["Fault::FaultEventDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "FaultEvent")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::SaveFault,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_name => "SaveFault",
    :schema_qualified => true,
    :schema_element => [
      ["arguments", "Fault::SaveRemoteArgumentsDto", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Fault::SaveFaultResponse,
    :schema_ns => "http://services.firstlook.biz/Fault/",
    :schema_name => "SaveFaultResponse",
    :schema_qualified => true,
    :schema_element => [
      ["saveFaultResult", ["Fault::SaveRemoteResultsDto", XSD::QName.new("http://services.firstlook.biz/Fault/", "SaveFaultResult")], [0, 1]]
    ]
  )
end

end