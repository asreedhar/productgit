
class IdentFilter
  
  @@hostname = 'localhost'
  
  cattr_accessor :hostname
  
  def self.filter(controller)
    controller.response.headers["Box"] = @@hostname
    return true
  end
  
end
