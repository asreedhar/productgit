require 'xsd/qname'

module AutoCheck


# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}UserIdentity
class UserIdentity
  attr_accessor :userName

  def initialize(userName = nil)
    @userName = userName
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}AutoCheckReportTO
class AutoCheckReportTO
  attr_accessor :expirationDate
  attr_accessor :userName
  attr_accessor :vin
  attr_accessor :score
  attr_accessor :compareScoreRangeLow
  attr_accessor :compareScoreRangeHigh
  attr_accessor :inspections

  def initialize(expirationDate = nil, userName = nil, vin = nil, score = nil, compareScoreRangeLow = nil, compareScoreRangeHigh = nil, inspections = nil)
    @expirationDate = expirationDate
    @userName = userName
    @vin = vin
    @score = score
    @compareScoreRangeLow = compareScoreRangeLow
    @compareScoreRangeHigh = compareScoreRangeHigh
    @inspections = inspections
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}ArrayOfAutoCheckReportInspectionTO
class ArrayOfAutoCheckReportInspectionTO < ::Array
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}AutoCheckReportInspectionTO
class AutoCheckReportInspectionTO
  attr_accessor :id
  attr_accessor :name
  attr_accessor :selected

  def initialize(id = nil, name = nil, selected = nil)
    @id = id
    @name = name
    @selected = selected
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}VehicleEntityType
class VehicleEntityType < ::String
  Appraisal = VehicleEntityType.new("Appraisal")
  Inventory = VehicleEntityType.new("Inventory")
  Undefined = VehicleEntityType.new("Undefined")
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}HasAccount
class HasAccount
  attr_accessor :dealerId

  def initialize(dealerId = nil)
    @dealerId = dealerId
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}HasAccountResponse
class HasAccountResponse
  attr_accessor :hasAccountResult

  def initialize(hasAccountResult = nil)
    @hasAccountResult = hasAccountResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}CanPurchaseReport
class CanPurchaseReport
  attr_accessor :dealerId

  def initialize(dealerId = nil)
    @dealerId = dealerId
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}CanPurchaseReportResponse
class CanPurchaseReportResponse
  attr_accessor :canPurchaseReportResult

  def initialize(canPurchaseReportResult = nil)
    @canPurchaseReportResult = canPurchaseReportResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReport
class GetReport
  attr_accessor :dealerId
  attr_accessor :vin

  def initialize(dealerId = nil, vin = nil)
    @dealerId = dealerId
    @vin = vin
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportResponse
class GetReportResponse
  attr_accessor :getReportResult

  def initialize(getReportResult = nil)
    @getReportResult = getReportResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportHtml
class GetReportHtml
  attr_accessor :dealerId
  attr_accessor :vin

  def initialize(dealerId = nil, vin = nil)
    @dealerId = dealerId
    @vin = vin
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}GetReportHtmlResponse
class GetReportHtmlResponse
  attr_accessor :getReportHtmlResult

  def initialize(getReportHtmlResult = nil)
    @getReportHtmlResult = getReportHtmlResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReport
class PurchaseReport
  attr_accessor :dealerId
  attr_accessor :vin

  def initialize(dealerId = nil, vin = nil)
    @dealerId = dealerId
    @vin = vin
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReportResponse
class PurchaseReportResponse
  attr_accessor :purchaseReportResult

  def initialize(purchaseReportResult = nil)
    @purchaseReportResult = purchaseReportResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReports
class PurchaseReports
  attr_accessor :dealerId
  attr_accessor :vehicleEntityType

  def initialize(dealerId = nil, vehicleEntityType = nil)
    @dealerId = dealerId
    @vehicleEntityType = vehicleEntityType
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}PurchaseReportsResponse
class PurchaseReportsResponse
  def initialize
  end
end


end
