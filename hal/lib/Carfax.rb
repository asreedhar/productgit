require 'xsd/qname'

module Carfax


# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}UserIdentity
class UserIdentity
  attr_accessor :userName

  def initialize(userName = nil)
    @userName = userName
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportPreferenceTO
class CarfaxReportPreferenceTO
  attr_accessor :displayInHotListings
  attr_accessor :purchaseReport
  attr_accessor :reportType

  def initialize(displayInHotListings = nil, purchaseReport = nil, reportType = nil)
    @displayInHotListings = displayInHotListings
    @purchaseReport = purchaseReport
    @reportType = reportType
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportTO
class CarfaxReportTO
  attr_accessor :expirationDate
  attr_accessor :userName
  attr_accessor :vin
  attr_accessor :reportType
  attr_accessor :displayInHotList
  attr_accessor :hasProblem
  attr_accessor :ownerCount
  attr_accessor :inspections

  def initialize(expirationDate = nil, userName = nil, vin = nil, reportType = nil, displayInHotList = nil, hasProblem = nil, ownerCount = nil, inspections = nil)
    @expirationDate = expirationDate
    @userName = userName
    @vin = vin
    @reportType = reportType
    @displayInHotList = displayInHotList
    @hasProblem = hasProblem
    @ownerCount = ownerCount
    @inspections = inspections
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}ArrayOfCarfaxReportInspectionTO
class ArrayOfCarfaxReportInspectionTO < ::Array
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportInspectionTO
class CarfaxReportInspectionTO
  attr_accessor :id
  attr_accessor :name
  attr_accessor :selected

  def initialize(id = nil, name = nil, selected = nil)
    @id = id
    @name = name
    @selected = selected
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}VehicleEntityType
class VehicleEntityType < ::String
  Appraisal = VehicleEntityType.new("Appraisal")
  Inventory = VehicleEntityType.new("Inventory")
  Undefined = VehicleEntityType.new("Undefined")
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportType
class CarfaxReportType < ::String
  BTC = CarfaxReportType.new("BTC")
  CIP = CarfaxReportType.new("CIP")
  Undefined = CarfaxReportType.new("Undefined")
  VHR = CarfaxReportType.new("VHR")
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxTrackingCode
class CarfaxTrackingCode < ::String
  FLA = CarfaxTrackingCode.new("FLA")
  FLN = CarfaxTrackingCode.new("FLN")
  Undefined = CarfaxTrackingCode.new("Undefined")
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}HasAccount
class HasAccount
  attr_accessor :dealerId

  def initialize(dealerId = nil)
    @dealerId = dealerId
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}HasAccountResponse
class HasAccountResponse
  attr_accessor :hasAccountResult

  def initialize(hasAccountResult = nil)
    @hasAccountResult = hasAccountResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportPreference
class GetReportPreference
  attr_accessor :dealerId
  attr_accessor :vehicleEntityType

  def initialize(dealerId = nil, vehicleEntityType = nil)
    @dealerId = dealerId
    @vehicleEntityType = vehicleEntityType
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportPreferenceResponse
class GetReportPreferenceResponse
  attr_accessor :getReportPreferenceResult

  def initialize(getReportPreferenceResult = nil)
    @getReportPreferenceResult = getReportPreferenceResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CanPurchaseReport
class CanPurchaseReport
  attr_accessor :dealerId

  def initialize(dealerId = nil)
    @dealerId = dealerId
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CanPurchaseReportResponse
class CanPurchaseReportResponse
  attr_accessor :canPurchaseReportResult

  def initialize(canPurchaseReportResult = nil)
    @canPurchaseReportResult = canPurchaseReportResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReport
class GetReport
  attr_accessor :dealerId
  attr_accessor :vin

  def initialize(dealerId = nil, vin = nil)
    @dealerId = dealerId
    @vin = vin
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}GetReportResponse
class GetReportResponse
  attr_accessor :getReportResult

  def initialize(getReportResult = nil)
    @getReportResult = getReportResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReport
class PurchaseReport
  attr_accessor :dealerId
  attr_accessor :vin
  attr_accessor :reportType
  attr_accessor :displayInHotList
  attr_accessor :trackingCode

  def initialize(dealerId = nil, vin = nil, reportType = nil, displayInHotList = nil, trackingCode = nil)
    @dealerId = dealerId
    @vin = vin
    @reportType = reportType
    @displayInHotList = displayInHotList
    @trackingCode = trackingCode
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReportResponse
class PurchaseReportResponse
  attr_accessor :purchaseReportResult

  def initialize(purchaseReportResult = nil)
    @purchaseReportResult = purchaseReportResult
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReports
class PurchaseReports
  attr_accessor :dealerId
  attr_accessor :vehicleEntityType
  attr_accessor :reportType
  attr_accessor :displayInHotList
  attr_accessor :trackingCode

  def initialize(dealerId = nil, vehicleEntityType = nil, reportType = nil, displayInHotList = nil, trackingCode = nil)
    @dealerId = dealerId
    @vehicleEntityType = vehicleEntityType
    @reportType = reportType
    @displayInHotList = displayInHotList
    @trackingCode = trackingCode
  end
end

# {http://services.firstlook.biz/VehicleHistoryReport/Carfax/}PurchaseReportsResponse
class PurchaseReportsResponse
  def initialize
  end
end


end
