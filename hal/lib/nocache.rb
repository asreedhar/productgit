require 'stringio'
require 'zlib'

class NoCacheFilter

  # http://support.microsoft.com/kb/234067
  # http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
  def self.filter(controller)
    controller.response.headers["Cache-Control"] = "no-cache"
    controller.response.headers["Pragma"] = "no-cache"
    controller.response.headers["Expires"] = "-1"
  end

end

class RemoveCacheHeadersFilter

  # http://kb.adobe.com/selfservice/viewContent.do?externalId=fdc7b5c&sliceId=1
  def self.filter(controller)
    controller.response.headers.delete("Cache-Control")
    controller.response.headers.delete("Pragma")
    controller.response.headers["Expires"] = "-1"
  end

end

if defined? Mongrel::DirHandler
  module Mongrel
    class DirHandler
      def send_file_with_expires(req_path, request, response, header_only=false)
        response.header['Cache-Control'] = 'max-age=21600'
        response.header['Expires'] = (Time.now + 21600).rfc2822
        send_file_without_expires(req_path, request, response, header_only)
      end
      alias send_file_without_expires send_file
      alias send_file send_file_with_expires
    end
  end
end

#if defined? WEBrick::HTTPServlet::DefaultFileHandler
#  module WEBrick
#    module HTTPServlet
#      class DefaultFileHandler
#        def do_GET_with_expires(req, res)
#          st = File::stat(@local_path)
#          mtime = st.mtime
#          res['etag'] = sprintf("%x-%x-%x", st.ino, st.size, st.mtime.to_i)
#          if not_modified?(req, res, mtime, res['etag'])
#            res.body = ''
#            raise HTTPStatus::NotModified
#          elsif req['range'] 
#            make_partial_content(req, res, @local_path, st.size)
#            raise HTTPStatus::PartialContent
#          else
#            mtype = HTTPUtils::mime_type(@local_path, @config[:MimeTypes])
#            res['content-type'] = mtype
#            res['cache-control'] = "max-age=1800"
#            res['content-length'] = st.size
#            res['last-modified'] = mtime.httpdate
#            res.body = open(@local_path, "rb")
#          end
#        end
#        alias do_GET_without_expires do_GET
#        alias do_GET do_GET_with_expires
#      end
#    end
#  end
#end
