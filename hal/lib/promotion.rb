require 'uri'
require 'logger'

class PromotionFilter
  
  def self.filter(controller)
    cookies = ActionController::CookieJar.new(controller)
    if cookies[:promotion].nil?
      cookies[:promotion] = "true"

      if controller.params
        parms = controller.params.dup
      else
        parms = {}
      end
        
      parms.delete("ticket")
        
      query = (parms.collect {|key, val| "#{key}=#{val}"}).join("&")
      query = "?" + query unless query.empty?
      
      req = controller.request

      if req.env['HTTP_X_FORWARDED_HOST']
        serv_name = req.env['HTTP_X_FORWARDED_HOST']
      elsif req.env['HTTP_X-Forwarded-Host']
        serv_name = req.env['HTTP_X-Forwarded-Host']
      elsif req.env['HTTP_HOST']
        serv_name = req.env['HTTP_HOST']
      end
        
      service_uri = "#{req.protocol}#{serv_name}#{req.request_uri.split(/\?/)[0]}#{query}"
      
      RAILS_DEFAULT_LOGGER.error( service_uri )
      
      service_uri = URI.escape(service_uri, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]", false, 'U').freeze)

      RAILS_DEFAULT_LOGGER.error( "escaped: #{service_uri}" )
     
      controller.redirect_to "#{PROMOTION_REDIRECT}&service=#{service_uri}"
    end
    return cookies[:promotion] != nil
  end
  
end
