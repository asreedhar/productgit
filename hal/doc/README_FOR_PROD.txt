============================================
===== Sessions table
============================================
1. Need to setup a job to clean this sucker out.
2. Needs indexing on sessid field.

============================================
===== Internal redirects
============================================
In our network architecture, SSL terminates away from the application servers.  
This causes the Internal Redirects problem where on a redirect, the application will redirect to
the port/protocol they are currently listening/using on.

So our rails app is sitting on HTTP, but outside world needs to see HTTPS redirects.  The solution is
to add a HEADER to each request HTTP_X_FORWARDED_PROTO=https, and rails will pick it up.

"
Thankfully, after writing the original version of this essay, the Rails Core team added a check that makes things much easier. This check looks like:

def ssl?
  @env['HTTPS'] == 'on' || @env['HTTP_X_FORWARDED_PROTO'] == 'https'
end

So, all you need to do is make sure that the X\_FORWARDED\_PROTO header is set by your proxy. Here's an example of how to do this with Apache using the RequestHeader directive:

RequestHeader set X_FORWARDED_PROTO "https"

You would put this inside the Virtual Host block for your SSL requests where they get passed off to Rails.
"
http://blog.duncandavidson.com/2006/01/reverse_proxyin.html

Other Resources:
http://blog.innerewut.de/articles/2006/06/21/mongrel-and-rails-behind-apache-2-2-and-ssl

Additional headers that our application uses are

1. What is my service url to reply to CAS?
	check req.env['HTTP_X_FORWARDED_HOST'] and use if exists
	check req.env['HTTP_X-Forwarded-Host'] and use if exists
	check req.env['HTTP_HOST'] and use if exists
	fallback to configured value in CAS::Filter.server_name in environments/*.rb file.