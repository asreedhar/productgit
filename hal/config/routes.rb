ActionController::Routing::Routes.draw do |map|
  # The priority is based upon order of creation: first created -> highest priority.
  
  # Sample of regular route:
  # map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  # map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # You can have the root of your site routed by hooking up '' 
  # -- just remember to delete public/index.html.
  map.connect '', :controller => "login"

  # Allow downloading Web Service WSDL as a file with an extension
  # instead of a file named 'wsdl'
  map.connect ':controller/service.wsdl', :action => 'wsdl'

  # Pretty Preference URLs
  map.connect 'preferences/make_a_deal/insight/:id', :controller => 'insight_preferences', :action => 'make_a_deal'
  map.connect 'preferences/targets/:action/:id', :controller => 'insight_preferences'
  map.connect 'preferences/make_a_deal/bookout_preferences/domestic', :controller => 'appraisal_review_preferences', :action => 'bookout_preferences', :vehicle_category_id => '1'
  map.connect 'preferences/make_a_deal/bookout_preferences/import', :controller => 'appraisal_review_preferences', :action => 'bookout_preferences', :vehicle_category_id => '2'
  map.connect 'preferences/make_a_deal/bookout_preferences/hiline', :controller => 'appraisal_review_preferences', :action => 'bookout_preferences', :vehicle_category_id => '3'
  map.connect 'preferences/make_a_deal/:action/:id', :controller => 'appraisal_review_preferences'
  map.connect 'preferences/my_configuration/:action/:id', :controller => 'member_preferences'
  map.connect 'preferences/planning/schedule/:action/:id', :controller => 'planning_category_table'
  map.connect 'preferences/planning/configuration/:action/:id', :controller => 'planning_category_configuration'
  
  # Management - Third Parties
  map.connect 'management/live_auction/:action/:id', :controller => 'management/live_auction'
  map.connect 'management/internet_auction/:action/:id', :controller => 'management/internet_auction'
  map.connect 'management/wholesaler/:action/:id', :controller => 'management/wholesaler'
  map.connect 'management/print_publisher/:action/:id', :controller => 'management/print_publisher'
  
  # Management - Planning Act
  map.connect 'management/lot_promotion/:action/:id', :controller => 'management/planning_act_lot_promotion'
  map.connect 'management/other/:action/:id', :controller => 'management/planning_act_other'
  map.connect 'management/spiff/:action/:id', :controller => 'management/planning_act_spiff'
  
  # Management - Campaign
  map.connect 'management/campaign/:action/:id', :controller => 'management/campaign'
  
  # Install the default route as the lowest priority.
  map.connect ':controller/:action/:id'
end
