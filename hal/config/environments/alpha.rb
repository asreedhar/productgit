# Settings specified here will take precedence over those in config/environment.rb

# The production environment is meant for finished, "live" apps.
# Code is not reloaded between requests
config.cache_classes = true

# Use a different logger for distributed setups
# config.logger = SyslogLogger.new

# Full error reports are disabled and caching is turned on
config.action_controller.consider_all_requests_local = false 
config.action_controller.perform_caching             = true

# Configure Asset Hosting
if ENV.member?('PRINCEXMLCLIENT')
  ASSET_HOST_PATH = "/max" 
  ASSET_HOST_SERVER = "images-b.int.firstlook.biz"  
else
  ASSET_HOST_PATH = "/content/max" 
  ASSET_HOST_SERVER = "max-b.int.firstlook.biz"
end

# Disable delivery errors if you bad email addresses should just be ignored
# config.action_mailer.raise_delivery_errors = false

##
## MAX Settings
##

# Custom Logger
Log = Log4r::Logger.new("MAX")
Log.add Log4r::Outputter.stderr
# Log.add Log4r::SyslogOutputter.new("MAX", { :facility => "local6" })

# SOAP SETTINGS
CARFAX_ENDPOINT = "http://services-b.int.firstlook.biz/VehicleHistoryReport/Services/CarfaxWebService.asmx"
AUTOCHECK_ENDPOINT = "http://services-b.int.firstlook.biz/VehicleHistoryReport/Services/AutoCheckWebService.asmx"
FAULT_ENDPOINT = "http://services-b.int.firstlook.biz/Fault/Services/Fault.asmx"
PRINCE_XML_ENDPOINT = "http://services-b.int.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService"
MESSAGE_QUEUE_ENDPOINT = "http://services-b.int.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueueService"
MESSAGE_QUEUE_NAME = "offline_pdf_queue"
PDF_HOST = 'https://max-b.int.firstlook.biz'
DATAWEBSERVICES_BASE_URL = "http://2k3bapp-svcs01x.int.firstlook.biz/"
CLIENT_ENDPOINT = "http://services-b.int.firstlook.biz/Client/Services/Client.asmx"
LICENSE_ENDPOINT = "http://services-b.int.firstlook.biz/Client/Services/License.asmx"

# CAS Settings
CAS_FILTER_CAS_BASE_URL = "https://auth-b.int.firstlook.biz/cas/"
CAS_FILTER_VALIDATE_URL = "https://auth-b.int.firstlook.biz/cas/serviceValidate"
CAS_FILTER_LOGOUT_URL = "https://auth-b.int.firstlook.biz/cas/logout"
CAS_FILTER_SERVER_NAME = "max-b.int.firstlook.biz"

# PING Settings
PING_ADAPTER_HOST = "max-b.int.firstlook.biz"
PING_ADAPTER_PORT = 443
PING_ADAPTER_SSL = true

# Memcached
MEMCACHED_HOST		= 'services-b.int.firstlook.biz:11211'
MEMCACHED_NAMESPACE	= 'hal'

# IdentFilter
IDENT_FILTER_HOSTNAME = ENV['HOSTNAME'] || 'ub9bweb-ruby01x.int.firstlook.biz'

# SAN Location
SAN_ROOT = '/var/www/html'

# Performance Management Center URL
PMC_URL = 'https://max-b.int.firstlook.biz/command_center/Default.aspx?token=DEALER_GROUP_SYSTEM_COMPONENT'

# For Appraisal hal/script/appraisal_review_booklet_generator originally - not set in prod but prod is hard-coded
CRITICAL_ERROR_EMAIL_LIST = [
    'jdomonkos@firstlookmax.com',
    'jtucker@firstlookmax.com',
    'ereppen@firstlookmax.com',
    'lcollins@incisent.com',
    'tcallaghan@incisent.com',
    'dpatton@firstlooksystems.com',
    'bhartman@incisent.com'
]
# Email Server
ActionMailer::Base.server_settings = {
  :address   => "ord4mail01.firstlook.biz",
  :domain    => "firstlook.biz"
}
ActionMailer::Base.raise_delivery_errors = true

##Promotion Gateway
PROMOTION_REDIRECT = "/promotions/action/view?method=next"


#Google Analytics Account details
GOOGLE_ACCOUNT = "UA-47838651-3"

