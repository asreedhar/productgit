# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
require 'uri'

class ApplicationController < ActionController::Base

  after_filter IdentFilter
  
  # These entries are for Marhsal.load as used by the memcached sessioning
  model :member
  model :business_unit
  model :business_unit_preference
  model :appraisal_review_preference
  model :appraisal_review_bookout_preference
  model :planning_age_bucket
  model :credential
  
  # The definition in the parent class ignores the :only_path part of the url_for hash
  # as part of its "clever" recursive implementation ... which ends up calling the method
  # with a string and thus prepending protocol and host name negating its existance. I hate
  # whoever took away two hours of my life for this utterly useless and broken code re-use.
  # Mine is not the most elegant fix but it works as the url_for prepends the protocol and
  # host so we do not lose this data in the case when the caller "wanted it".
  def redirect_to(options = {}, *parameters_for_method_reference)
    case options
      when %r{^\w+://.*}
        raise DoubleRenderError if performed?
          logger.info("Redirected to #{options}") if logger
          response.redirect(options)
          response.redirected_to = options
          @performed_redirect = true

      when String
        redirect_to(request.protocol + request.host_with_port + options)
      
      when :back
        request.env["HTTP_REFERER"] ? redirect_to(request.env["HTTP_REFERER"]) : raise(RedirectBackError)

      else
        the_url_requested = nil
        if parameters_for_method_reference.empty?
          the_url_requested = url_for(options)
          response.redirected_to = options
        else
          the_url_requested = url_for(options, *parameters_for_method_reference)
          response.redirected_to, response.redirected_to_method_params = options, parameters_for_method_reference
        end
        logger.info("Redirected to #{the_url_requested}") if logger
        response.redirect(the_url_requested)
        @performed_redirect = true
    end
  end
  
  def redirect_to_external_url
    external_url = params['external_url']
    params['p.productMode'] = 'edge'
    params['p.inventoryType'] = 'used'
    params['HALSESSIONID'] = session.session_id
    logger.info "external_url is " + external_url.to_s
    first = true
    params.each do |key,value|
      next if ['external_url', 'action', 'controller'].include?(key)
        logger.debug "key is " + key.to_s
        if not value.nil? 
          logger.debug "value is " + value.to_s
        else
          logger.debug "value of key was nil"
        end
        if first && !external_url.match(/\?/)
          external_url << '?'
          first = false
        else
          external_url << '&'
        end
        if not value.nil?
          external_url << URI.escape(key, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
          external_url << '='
          external_url << URI.escape(value, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
        end
    end
    session[:return_controller] = controller_name()
    response.redirect(external_url)
    response.redirected_to = external_url
    @performed_redirect = true
    logger.info("Redirected to #{external_url}") if logger
    return
  end
  
  def update_record(record, attributes, &block)
    if record.update_attributes(attributes)
      yield if block_given?
    else
      record_errors(record)
    end
  end
  
  def update_record!(record, attributes, &block)
    if record.update_attributes(attributes)
      yield if block_given?
    else
      record_errors(record)
      raise ActiveRecord::RecordNotSaved, flash.now[:notice].join(",")
    end
  end
  
  def save_record(record, &block)
    if record.save
      yield if block_given?
    else
      record_errors(record)
    end
  end
  
  def save_record!(record, &block)
    if record.save
      yield if block_given?
    else
      record_errors(record)
      raise ActiveRecord::RecordNotSaved, flash.now[:notice].join(",")
    end
  end
  
  def record_errors(record)
    if flash.now[:notice].nil?
      flash.now[:notice] = Array.new
    end
    if (record.is_a?(Array))
      record.each do |r|
        r.errors.each_full { |msg| flash.now[:notice] << msg }
      end
    else
      record_errors([record])
    end
  end
  
  def has_no_errors()
    return (flash.now[:notice].nil? || flash.now[:notice].empty?)
  end
  
  def exit_store
    HalSession.delete_by_session_id(session.session_id)
    session[:business_unit] = nil
    session[:search_and_aquisition_report] = nil
    if session[:remote_entry]
      redirect_to :controller => 'login', :action => 'exit', :only_path => true
    else
      redirect_to :controller => 'login', :action => 'index', :only_path => true
    end
  end
  
  def logout
    HalSession.delete_by_session_id(session.session_id)
    if session[:remote_entry]
      session[:business_unit] = nil
      session[:search_and_aquisition_report] = nil
      redirect_to :controller => 'login', :action => 'exit', :only_path => true
    else
      session.delete
      # create a service url
      parms = params.dup
      parms.delete("ticket")
      parms.delete("action")
      parms.delete("controller")
      path  = request.request_uri
      path  = path.sub("/logout","")
      query = (parms.collect {|key, val| "#{key}=#{val}"}).join("&")
      query = "?" + query unless query.empty?
      # use our own service url
      params[:service] = "#{request.protocol}#{configured_host(request)}#{path}#{query}"
      # get a logout url from cas
      redirect_to CAS::Filter.logout_url(self)
    end
  end
  
  def log_error(exception)
    super
    FaultHelper.log_fault(exception, request)   
  end

  def rescue_action_in_public(exception)
    render :file => "#{RAILS_ROOT}/public/500.html", :status => 500
  end
  
  def rescue_action_locally(exception)
    render :file => "#{RAILS_ROOT}/public/500.html", :status => 500
  end
  
  def neither_nil_nor_empty(map, key)
    if map.member?(key)
      obj = map[key]
      return (!obj.nil? && !obj.empty?)
    else
      return false
    end
  end
  
  def neither_nil_nor_empty_n(map, keys)
    raise StandardError, "No keys supplied!" if keys.empty?
    m = map
    keys.each_with_index do |key,index|
      if m.member?(key)
        m = m[key]
        return false if m.nil?
        return false if index+1 < keys.length && !(m.kind_of?(Hash) || m.kind_of?(HashWithIndifferentAccess))
      else
        return false
      end
    end
    return !m.nil? && (!m.kind_of?(Enumerable) || !m.empty?)
  end
  
  def not_nil(map,key)
    if map.member?(key)
      obj = map[key]
      return !obj.nil?
    else
      return false
    end
  end
  
  def delete_non_values(map,objects,attrs,non_values=[])
    objects.each do |object|
      if neither_nil_nor_empty(map,object)
        attrs.each do |attr|
          if not_nil(map[object],attr)
            if ['Please Select',''].concat(non_values).include?(map[object][attr])
              map[object].delete(attr)
            end
          end
        end
      end
    end
  end
  
  def configured_host(request)
    if request.env['HTTP_X_FORWARDED_HOST']
      return request.env['HTTP_X_FORWARDED_HOST']
    elsif request.env['HTTP_X-Forwarded-Host']
      return request.env['HTTP_X-Forwarded-Host']
    elsif request.env['HTTP_HOST']
      return request.env['HTTP_HOST']
    else
      return CAS::Filter.server_name
    end
  end
  
end
