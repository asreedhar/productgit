class InventorySalesGraphController < DynamicController
  skip_after_filter NoCacheFilter
  after_filter RemoveCacheHeadersFilter
  def index
    business_unit_id = session[:business_unit].id
    planning_category_id = params[:planning_category_id]
    alt_configuration = params[:alt_configuration] || 0
    @data = InventorySalesData.find_by_sql(<<-SQL
        SELECT
            *
        FROM
            inventory_sales_data
        WHERE
            business_unit_id            = #{business_unit_id}
        AND planning_category_id        = #{planning_category_id}
        AND is_comparison_configuration = #{alt_configuration}
        AND days_to_sale <= 100
        ORDER BY
            days_to_sale
      SQL
    )
    @min = 0
    @max = 0
    for sample in @data do
      @min = (@min > sample.average_profit) ? sample.average_profit : @min
      @max = (@max < sample.average_profit) ? sample.average_profit : @max
    end
    if @min != 0
      @min = (@min/100)*100 + (@min%100>=0 ? 100 : 0)
      @min = (@min/500)*500 + (@min%500>=0 ? 500 : 0)
    end
    if @max != 0
      @max = (@max/100)*100 + (@max%100>=0 ? 100 : 0)
      @max = (@max/500)*500 + (@max%500>=0 ? 500 : 0)
    end
    @steps = (@max-@min)/1000
    # scale diagram size
    width  = params['width']  || 400
    height = params['height'] || 300
    @chart_rect = Hash.new
    @chart_rect[:x] = width.to_i  * 0.15
    @chart_rect[:y] = width.to_i  * 0.20
    @chart_rect[:w] = width.to_i  * 0.75
    @chart_rect[:h] = height.to_i * 0.50
    @text_x = Hash.new
    @text_x[:x] = width.to_i  *  0.41250
    @text_x[:y] = height.to_i *  0.80000
    @text_y = Hash.new
    @text_y[:x] = width.to_i  * -0.06250
    @text_y[:y] = height.to_i *  0.57000
    
  end
  
  def object
    render :partial => 'inventory_sales_graph/object', :locals => {
      :width                => params['width'] || 400,
      :height               => params['height'] || 300,
      :object_id            => params['object_id'] || 'chart',
      :planning_category_id => params['planning_category_id'] || 1,
      :alt_configuration    => params['alt_configuration'] || 0
    }
  end
  
  def invage
    business_unit_id = @session[:business_unit].id
    @graph_data = InventorySalesGraphData.for_business_unit(session[:business_unit].id)
    total_units = 0
    @graph_data.each{|d| total_units += d.units}
    @graph_data.each{|d| d.percentages(total_units)}
  end

  def invage_2
    business_unit_id = @session[:business_unit].id
    @graph_data = InventorySalesGraphData.for_business_unit(session[:business_unit].id)
    @total_units = 0
    @graph_data.each{|d| @total_units += d.units}
    @graph_data.each{|d| d.percentages(@total_units)}
  end
  
end
