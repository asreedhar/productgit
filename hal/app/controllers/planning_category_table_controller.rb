class PlanningCategoryTableController < DynamicController
  
  layout "preference"
  
  def planning_category
    page_vars
    @business_unit_planning_category = BusinessUnitPlanningCategory.find(
      :first,
      :conditions => [
        "business_unit_id = ? and is_comparison_configuration = 0 and planning_category_id = ?",
        session[:business_unit].id,
        params[:planning_category_id]
      ]
    )
    @planning_category_table = PlanningCategoryTable.find(
      :first,
      :conditions => [
        "business_unit_id = ? and planning_category_id = ?",
        session[:business_unit].id,
        params[:planning_category_id]
      ]
    )
  end
  
  def select_table_cell
    cell_id = params[:planning_category_table_cell_id]
    @planning_category_table_cell = PlanningCategoryTableCell.find(cell_id)
    @planning_category_table_cell.update_attribute(:active, true)
  end
  
  def deselect_table_cell
    cell_id = params[:planning_category_table_cell_id]
    @planning_category_table_cell = PlanningCategoryTableCell.find(cell_id)
    @planning_category_table_cell.update_attribute(:active, false)
    default_id = PlanningActionDefault.find(
      :first,
      :conditions => [
        "business_unit_id = ? and planning_action_id = ? and empty_value = 1",
        session[:business_unit].id,
        @planning_category_table_cell.planning_category_table_row.planning_action_id
      ]
    ).id
    @planning_category_table_cell.update_attribute(:planning_action_default_id, default_id)
  end
  
  # new table manipulation methods
  
  def table_command_vars(is_delete=false)
    business_unit_planning_category_id = params[:business_unit_planning_category_id].to_i
    planning_category_table_id = params[:planning_category_table_id].to_i
    planning_category_table_col_id = params[:planning_category_table_col_id].to_i
    @business_unit_planning_category = BusinessUnitPlanningCategory.find(business_unit_planning_category_id)
    @planning_category_table = PlanningCategoryTable.find(planning_category_table_id)
    @planning_category_table_col = PlanningCategoryTableCol.find(planning_category_table_col_id) unless is_delete
  end
  
  def sibling_table_cols
    p, c, n = nil, nil, nil
    @planning_category_table.planning_category_table_cols(:order => "cindex").each do |col|
      p, c, n = c, n, col
      break if (!c.nil? && c.id == @planning_category_table_col.id)
    end
    p, c, n = c, n, nil unless (!c.nil? && c.id == @planning_category_table_col.id)
    return [p,n]
  end
  
  def mid_point(n_1, n_2, inc)
    raise "no data supplied" if n_1.nil? && n_2.nil?
    raise "non ascending data supplied (#{n_1}, #{n_2})" if !n_1.nil? && !n_2.nil? && n_1 >= n_2
    raise "no int mid point (gap too small: #{n_1}, #{n_2})" if !n_1.nil? && !n_2.nil? && (n_2 - n_1) < 2
    raise "no int mid point (negative result: #{n_1}, #{n_2}, #{inc})" if !n_1.nil? && n_2.nil? && n_1 == 0
    m = nil
    if n_1.nil?
      m = n_2 + inc
    else
      if n_2.nil?
        m = [n_1 + inc, 0].max
      else
        m = n_1 + ((n_2 - n_1) / 2)
      end
    end
    raise "no result" if m.nil?
    raise "bad result #{n_1} < #{m} < #{n_2}" if !n_1.nil? && !n_2.nil? && !(m > n_1 && m < n_2)
    Log.error("#{n_1} < #{m} < #{n_2}")
    return m
  end
  
  def insert_column(fn_age,fn_push)
    # verify we got passed a block
    raise "you must pass a block so i know upon what side of myself i need to insert the new column" if fn_age.nil? || !fn_age.kind_of?(Proc)
    # load vars
    table_command_vars()
    # get columns either side of me
    prev_col, next_col = * sibling_table_cols()
    # get the column i am pushing to the right
    push_column = fn_push.call(prev_col, @planning_category_table_col, next_col)
    # so the new cindex is
    cindex = (push_column.nil? ? @planning_category_table.planning_category_table_cols.collect{|c| c.cindex}.max+1 : push_column.cindex)
    # make new column
    @new_planning_category_table_col = PlanningCategoryTableCol.new do |c|
      c.planning_category_table_id = @planning_category_table.id
      c.age = fn_age.call(prev_col, @planning_category_table_col, next_col)
      c.cindex = cindex
    end
    # add cells
    @planning_category_table.planning_category_table_rows.each do |r|
      @new_planning_category_table_col.planning_category_table_cells << PlanningCategoryTableCell.new do |c|
        c.planning_category_table_col = @new_planning_category_table_col
        c.planning_category_table_row = r
        c.planning_action_default = PlanningActionDefault.find(:first, :conditions => ["business_unit_id = ? and planning_action_id = ?", session[:business_unit].id, r.planning_action_id])
        c.active = false
      end
    end
    # big transaction: update later cindex's and save new column (uk constraint on cindex)
    PlanningCategoryTableCol.transaction() do
      # update cindex of myself and all that follow me
      @planning_category_table.planning_category_table_cols.each do |c|
        if c.cindex >= cindex
          unless c.update_attribute(:cindex, c.cindex+1)
            record_errors(c)
            break
          end
        end
      end
      # save the new column
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        unless @new_planning_category_table_col.save
          record_errors(@new_planning_category_table_col)
        end
      end
    end
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        # reload the table
        @planning_category_table.reload
        # refresh the tables
        page.replace_html 'planning_category_table_retail', :partial => 'planning_category_table', :object => @planning_category_table, :locals => { :planning_action_type_id => 1, :business_unit_planning_category => @business_unit_planning_category }
        page.replace_html 'planning_category_table_wholesale', :partial => 'planning_category_table', :object => @planning_category_table, :locals => { :planning_action_type_id => 2, :business_unit_planning_category => @business_unit_planning_category }
        # highlight the new column:
        page.select("planning_category_table_col_#{@new_planning_category_table_col.id}").each do |element|
          page << "new Effect.Highlight(value);"
        end
        page.select(".col_#{@new_planning_category_table_col.id}").each do |element|
          page << "new Effect.Highlight(value);"
        end
      else
        # show an error
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def insert_column_to_the_left
    insert_column(
      Proc.new {|prev_col, this_col, next_col| mid_point(prev_col.nil? ? nil : prev_col.age, this_col.age, -7) },
      Proc.new {|prev_col, this_col, next_col| this_col }
    )
  end
  
  def insert_column_to_the_right
    insert_column(
      Proc.new {|prev_col, this_col, next_col| mid_point(this_col.age, next_col.nil? ? nil : next_col.age, +7) },
      Proc.new {|prev_col, this_col, next_col| next_col }
    )
  end
  
  def delete_column
    PlanningCategoryTable.transaction() do
      PlanningCategoryTableCell.connection().delete(
        "DELETE FROM PlanningCategoryTableCells WHERE PlanningCategoryTableColID = #{params[:planning_category_table_col_id]}",
        "Delete Column Cells"
      )
      PlanningCategoryTableCol.connection().delete(
        "DELETE FROM PlanningCategoryTableCols WHERE PlanningCategoryTableColID = #{params[:planning_category_table_col_id]}",
        "Delete Column Cells"
      )
    end
    table_command_vars(true)
    render :update do |page|
      page.replace_html 'planning_category_table_retail', :partial => 'planning_category_table', :object => @planning_category_table, :locals => { :planning_action_type_id => 1, :business_unit_planning_category => @business_unit_planning_category }
      page.replace_html 'planning_category_table_wholesale', :partial => 'planning_category_table', :object => @planning_category_table, :locals => { :planning_action_type_id => 2, :business_unit_planning_category => @business_unit_planning_category }
    end
  rescue ActiveRecord::RecordNotFound => exception
    flash.now[:notice] = 'An exception occurred that could not be handled: ' + exception
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
  def edit_column
    table_command_vars
    # get cols either side of the one supplied
    render :update do |page|
    end
  end
  
  def planning_action_default_spiff
    planning_act_spiff_id = params[:planning_act_spiff][:id].first
    planning_action_default = PlanningActionDefault.find_by_sql(<<-SQL
        SELECT
              d.*
        FROM
              planning_action_defaults d
        JOIN  planning_action_defaults_planning_action_default_spiffs ds ON ds.planning_action_default_id = d.id
        JOIN  planning_action_default_spiffs s ON s.id = ds.planning_action_default_spiff_id
        WHERE
              d.active = 1
        AND   d.business_unit_id = #{session[:business_unit].id}
        AND   s.active = 1
        AND   s.business_unit_id = #{session[:business_unit].id}
        AND   s.planning_act_spiff_id = #{planning_act_spiff_id}
    SQL
    ).first
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_action_default_spiff = PlanningActionDefaultSPIFF.new do |s|
        s.business_unit_id = session[:business_unit].id
        s.active = true
        s.planning_act_spiff_id = planning_act_spiff_id
      end
      planning_action_default.planning_action_default_spiffs << planning_action_default_spiff
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_action_default_lot_promote
    planning_act_lot_promotion_ids = params[:planning_act_lot_promotion][:id]
    planning_action_default = get_planning_action_defaults(planning_act_lot_promotion_ids,"lot_promotion")
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_act_lot_promotion_ids.each do|id|
        planning_action_default_lot_promotion = PlanningActionDefaultLotPromotion.new do |s|
          s.business_unit_id = session[:business_unit].id
          s.active = true
          s.planning_act_lot_promotion_id = id
        end
        planning_action_default.planning_action_default_lot_promotions << planning_action_default_lot_promotion
       end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_action_default_other
    planning_act_other_ids = params[:planning_act_other][:id]
    planning_action_default = get_planning_action_defaults(planning_act_other_ids,"other")
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_act_other_ids.each do|id|
        planning_action_default_other = PlanningActionDefaultOther.new do |s|
          s.business_unit_id = session[:business_unit].id
          s.active = true
          s.planning_action_id = params[:planning_action_id]
          s.planning_act_other_id = id
        end
        planning_action_default.planning_action_default_others << planning_action_default_other
       end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_action_default_live_auction
    planning_act_live_auction_ids = params[:live_auction][:id]
    
    planning_action_default = get_planning_action_defaults(planning_act_live_auction_ids,"live_auction","live_auction_id")
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_act_live_auction_ids.each do|id|
        planning_action_default_live_auction = PlanningActionDefaultLiveAuction.new do |s|
          s.business_unit_id = session[:business_unit].id
          s.active = true
          s.live_auction_id = id
        end
        planning_action_default.planning_action_default_live_auctions << planning_action_default_live_auction
       end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  
  
  def planning_action_default_wholesaler
    planning_act_wholesaler_ids = params[:wholesaler][:id]
    planning_action_default = get_planning_action_defaults(planning_act_wholesaler_ids,"wholesaler","wholesaler_id")
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_act_wholesaler_ids.each do|id|
        planning_action_default_wholesaler = PlanningActionDefaultWholesaler.new do |s|
          s.business_unit_id = session[:business_unit].id
          s.active = true
          s.wholesaler_id = id
        end
        planning_action_default.planning_action_default_wholesalers << planning_action_default_wholesaler
       end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_action_default_small_print_advertisement
    planning_act_small_print_advertisement_ids = params[:campaign][:id]
    
    planning_action_default = get_planning_action_defaults(planning_act_small_print_advertisement_ids,"small_print_advertisement","campaign_id")
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_act_small_print_advertisement_ids.each do|id|
        planning_action_default_small_print_advertisement = PlanningActionDefaultSmallPrintAdvertisement.new do |s|
          s.business_unit_id = session[:business_unit].id
          s.active = true
          s.campaign_id = id
        end
        planning_action_default.planning_action_default_small_print_advertisements << planning_action_default_small_print_advertisement
       end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_action_default_internet_auction
    planning_act_internet_auction_ids = params[:third_party][:id]
    planning_action_default = get_planning_action_defaults(planning_act_internet_auction_ids,"internet_auction","third_party_id")
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_act_internet_auction_ids.each do |id|
        planning_action_default_internet_auction = PlanningActionDefaultInternetAuction.new do |s|
          s.business_unit_id = session[:business_unit].id
          s.active = true
          s.third_party_id = id
        end
        planning_action_default.planning_action_default_internet_auctions << planning_action_default_internet_auction
      end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_action_default_reprice
    planning_action_default = PlanningActionDefault.find_by_sql(<<-SQL
      SELECT
            d.*
      FROM
            planning_action_defaults d
      JOIN  planning_action_defaults_planning_action_default_reprices j on d.id = j.planning_action_default_id
      JOIN  planning_action_default_reprices r on r.id = j.planning_action_default_reprice_id
      WHERE
            d.business_unit_id = #{session[:business_unit].id}
      AND   d.active = 1
      AND   d.empty_value = 0
      AND   r.business_unit_id = #{session[:business_unit].id}
      AND   r.active = 1
      AND   r.planning_action_default_reprice_type_id = #{params[:planning_action_default_reprice][:planning_action_default_reprice_type_id]}
      AND   r.amount = #{params[:planning_action_default_reprice][:amount]}
    SQL
    ).first
    planning_category_table_cell = PlanningCategoryTableCell.find(params[:planning_category_table_cell_id])
    if planning_action_default.nil?
      planning_action_default = new_planning_action_default(params[:planning_action_id])
      planning_action_default_reprice = PlanningActionDefaultReprice.new do |r|
        r.business_unit_id = session[:business_unit].id
        r.active = true
        r.planning_action_default_reprice_type_id = params[:planning_action_default_reprice][:planning_action_default_reprice_type_id]
        r.amount = params[:planning_action_default_reprice][:amount]
      end
      # planning_action_default.planning_action_default_reprices << planning_action_default_reprice
      logger.debug "number of reprices=#{planning_action_default.planning_action_default_reprices.length}"
      PlanningActionDefaultReprice.transaction() do
        save(planning_action_default) do
          save(planning_action_default_reprice) do
            planning_action_default.planning_action_default_reprices << planning_action_default_reprice
          end
        end
      end
      unless planning_action_default.save
        record_errors(planning_action_default)
      end
    end
    if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      unless planning_category_table_cell.update_attribute(:planning_action_default_id, planning_action_default.id)
        record_errors(planning_category_table_cell)
      end
    end
    render :update do |page|
      if (flash.now[:notice].nil? || flash.now[:notice].empty?)
        page.replace 'planning_category_table_cell', :partial => 'planning_category_table_cell', :object => planning_category_table_cell
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  private
  
  def get_planning_action_defaults(ids,type,alternate = nil)
    alternate = "planning_act_#{type}_id" if alternate.nil?
    sums = ids.collect {|id| "sum(case when p.#{alternate} = #{id} then 1 else 0 end) has_#{id}"}.join(",")
    has =  ids.collect {|id| " t.has_#{id} = 1"}.join(" and ")
    commas = ids.join(",")
    planning_action_default = PlanningActionDefault.find_by_sql(<<-SQL
       SELECT d.*
       FROM   planning_action_defaults d
       JOIN (
              SELECT 
                d.id,
                #{sums},
                count(*) number_of_rows
              FROM        
                planning_action_defaults d
              JOIN       planning_action_defaults_planning_action_default_#{type}s j on j.planning_action_default_id = d.id
              JOIN       planning_action_default_#{type}s p on j.planning_action_default_#{type}_id = p.id
              WHERE
                         d.business_unit_id = #{session[:business_unit].id}
              and        d.active = 1
              and        d.empty_value = 0
              and        p.business_unit_id = #{session[:business_unit].id}
              and        p.active = 1
              and        p.#{alternate} IN (#{commas})
              GROUP BY
                         d.id
       ) t on d.id = t.id
       WHERE 
            #{has}
       AND
            t.number_of_rows = #{ids.length}
    SQL
    ).first
    return planning_action_default
  end
  
  def new_planning_action_default(planning_action_id)
    planning_action_default = PlanningActionDefault.new do |d|
      d.business_unit_id = session[:business_unit].id
      d.active = true
      d.planning_action_id = planning_action_id
      d.empty_value = false
      d.rank = PlanningActionDefault.count_by_sql("SELECT COUNT(*) FROM planning_action_defaults WHERE business_unit_id = #{session[:business_unit].id} and active = 1 and planning_action_id = #{planning_action_id}")
    end
    return planning_action_default
  end
  
  private
  
  def page_vars
    @page_title = "Preferences - Planning - Schedule"
    @page_style = "preference"
  end
  
end
