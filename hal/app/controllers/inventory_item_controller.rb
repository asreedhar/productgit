class InventoryItemController < DynamicController
 include ApplicationHelper
 
  def edit_ad_text
    id = params[:inventory_id]
    render :update do |page|
      page << "Element.show('editAdTextArea#{id}');"
    end
  end
  
   def cancel_ad_text
    id = params[:inventory_id]
    render :update do |page|
      page << "Element.hide('editAdTextArea#{id}');"
    end
   end

end