class InsightPreferencesController < DynamicController

  layout "preference"
  
  class PairwiseValidationError < StandardError
  end
  
  def appraisal_closing_rate
    page_vars
    load_insight_target(16)
  end
  
  def make_a_deal
    page_vars
    load_insight_target(17)
  end
  
  def trade_in_inventory_analyzed
    page_vars
    load_insight_target(18)
  end
  
  def average_immediate_wholesale_profit
    page_vars
    load_insight_target(19)
  end
  
  def current_inventory_short_leash
    page_vars
    load_inventory_bucket_items
  end
  
  def current_inventory_unplanned_vehicles
    page_vars
    load_insight_target(21)
  end
  
  def current_inventory_days_supply
    page_vars
    load_insight_target(22)
  end
  
  def save_insight_targets
    BusinessUnitInsightTargetPreference.transaction() do
      record_errors(BusinessUnitInsightTargetPreference.update(
        params[:business_unit_insight_target_preference].keys,
        params[:business_unit_insight_target_preference].values
      ))
    end
    flash.now[:notice] = "Update Successful" if flash.now[:notice].empty?
    success
  rescue StandardError => exception
    error(exception)
  end
  
  def save_inventory_bucket_items
    # load the buckets
    inventory_bucket_items = []
    params[:inventory_bucket_item].keys.each do |inventory_bucket_item_id|
      inventory_bucket_item = InventoryBucketItem.find(inventory_bucket_item_id)
      inventory_bucket_item.attributes = params[:inventory_bucket_item][inventory_bucket_item_id]
      inventory_bucket_items << inventory_bucket_item
    end
    # sort the items so we can pairwise validate then later
    inventory_bucket_items.sort!{|a,b| a.range_id <=> b.range_id}
    # save (and therefore validate) the records
    InventoryBucketItem.transaction() do
      # save and validate
      inventory_bucket_items.each do |inventory_bucket_item|
        save_record!(inventory_bucket_item)
      end
      # force rollback on pairwise validation errors
      inventory_bucket_items.each_with_index do |inventory_bucket_item, index|
        next if index == 0
        a, b = inventory_bucket_items[index-1], a = inventory_bucket_items[index]
        raise PairwiseValidationError, "#{a.name} Warning value greater than #{b.name} Warning value" if a.low > b.low
        raise PairwiseValidationError, "#{a.name} Wholesale value greater than #{b.name} Wholesale value" if a.high > b.high
      end
    end
    flash.now[:notice] = "Update Successful" if flash.now[:notice].empty?
    success
  rescue StandardError => exception
    error(exception)
  end
  
  private
  
  def page_vars
    @page_title = "Preferences - Strategy and Targets"
    @page_style = "preference"
  end
  
  def load_inventory_bucket_items
    @inventory_bucket_items = InventoryBucketItem.find(
      :all,
      :conditions => ["business_unit_id = ? and inventory_bucket_id = 5", session[:business_unit].id],
      :order => 'range_id'
    )
    if @inventory_bucket_items.nil? || @inventory_bucket_items.empty?
      items = InventoryBucketItem.find_by_sql(<<-SQL
        SELECT
        	BusinessUnitID         [business_unit_id],
        	InventoryBucketRangeID [id],
        	InventoryBucketID      [inventory_bucket_id],
        	RangeID                [range_id],
        	Description            [name],
        	Low                    [low],
        	High                   [high],
        	Lights                 [lights],
        	LongDescription        [description],
        	SortOrder              [position]
        FROM
        	dbo.GetInventoryBucketRanges(#{session[:business_unit].id}, 5)
        SQL
      )
      items.each do |item|
        @inventory_bucket_items << InventoryBucketItem.new do |p|
          p.inventory_bucket_id = 5
          p.business_unit_id    = session[:business_unit].id
          p.range_id            = item.range_id
          p.name                = item.name
          p.low                 = item.low
          p.high                = item.high
          p.lights              = item.lights
          p.description         = item.description
          p.position            = item.position
        end
      end
      # save as it allows us to submit back an array indexed by pk
      InventoryBucketItem.transaction() do
        @inventory_bucket_items.each do |p|
          unless p.save
            record_errors(p)
          end
        end
      end
    end
  rescue StandardError => exception
    if flash.now[:notice].nil?
      flash.now[:notice] = 'An error occurred that could not be handled: ' + exception
    end
  end
  
  def load_insight_target(insight_type_id)
    @business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.load_insight_target(
      insight_type_id,
      session[:business_unit].id
    )
  end
  
  def success
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
  def error(exception)
    if flash.now[:notice].nil?
      if exception.instance_of?(PairwiseValidationError)
        flash.now[:notice] = exception.message
      else
        flash.now[:notice] = 'An error occurred that could not be handled: ' + exception.message
      end
    end
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
end
