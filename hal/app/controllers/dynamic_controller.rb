
require 'nocache'
require 'hasdealer'
require 'promotion'

class DynamicController < ApplicationController
  
  before_filter CAS::Filter
  before_filter HasDealerFilter
  before_filter PromotionFilter
  after_filter NoCacheFilter
  
end
