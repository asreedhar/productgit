
require 'hasappraisalreview'

class AppraisalReviewBookletController < DynamicController

  before_filter HasAppraisalReviewFilter
  
  skip_after_filter NoCacheFilter, :only => [:appraisal_review_booklet]
  
  layout "home"
  
  def index
    # page title
    page_vars("Make-A-Deal Workbooks")
    # summary items
    appraisal_review_booklet_summaries = AppraisalReviewBookletSummary.find_by_sql(<<-SQL
        SELECT
              s.*
        FROM
              dbo.appraisal_review_booklet_summaries s
        JOIN  dbo.appraisal_review_booklets b on s.appraisal_review_booklet_id = b.id
        JOIN  dbo.future_tasks f on b.future_task_id = f.id
        WHERE
              f.created BETWEEN DATEADD(DD, 1, DATEADD(MM, -3, GETDATE())) AND GETDATE()
        AND   f.business_unit_id = #{session[:business_unit].id}
        ORDER
        BY    f.created DESC
      SQL
    )
    # proxy components
    @proxy_component_collections = ProxyComponent::Collection.groups(
      :proxy_component_collection => AppraisalReviewBookletSummaryProxyComponentCollection,
      :number_of_groups           => 3,
      :number_of_initial_records  => 6,
      :records                    => appraisal_review_booklet_summaries,
      :group_by_proc              => Proc.new { |record|
        # determine the last three months hack-wise
        today = DateTime.now
        month_x, year_x = today.mon, today.year
        month_y, year_y = (month_x == 1 ? 12 : month_x-1), (month_x == 1 ? year_x-1 : year_x)
        month_z, year_z = (month_y == 1 ? 12 : month_y-1), (month_y == 1 ? year_y-1 : year_y)
        # build an array so we can order them
        arr = [[month_x, year_x], [month_y, year_y], [month_z, year_z]]
        index = 2
        arr.each_with_index do |date, idx|
          if record.future_task.created.mon == date[0] && record.future_task.created.year == date[1]
            index = idx
            break
          end
        end
        index
      }
    )
    # get counts
    @number_of_appraisal_review_booklet_summaries = appraisal_review_booklet_summaries.length
    # render the page
    render :action => 'index', :layout => (params[:layout].nil? || params[:layout] != 'false')
  end
  
  def appraisal_review_booklet_summary
    appraisal_review_booklet_summaries = AppraisalReviewBookletSummary.find_by_sql(<<-SQL
        SELECT
              s.*
        FROM
              dbo.appraisal_review_booklet_summaries s
        JOIN  dbo.appraisal_review_booklets b on s.appraisal_review_booklet_id = b.id
        JOIN  dbo.future_tasks f on b.future_task_id = f.id
        WHERE
              s.id IN (#{params[:appraisal_review_booklet_summary_id].collect{|id| id.to_i}.join(',')})
        AND   f.business_unit_id = #{session[:business_unit].id}
        ORDER
        BY    f.created DESC
      SQL
    )
    render :partial => 'appraisal_review_booklet_summary', :collection => appraisal_review_booklet_summaries
  end
  
  def appraisal_review_booklet
    appraisal_review_booklets = AppraisalReviewBooklet.find_by_sql(<<-SQL
        SELECT
              b.id, b.future_task_id, b.booklet
        FROM
              dbo.appraisal_review_booklets b
        JOIN  dbo.future_tasks f on b.future_task_id = f.id
        WHERE
              f.business_unit_id = #{session[:business_unit].id}
        AND   b.id = #{params[:id]}
      SQL
    )
    if appraisal_review_booklets.empty?
      render :inline => "<p>No such workbook.</p>"
    else
      appraisal_review_booklet = appraisal_review_booklets.first
      if appraisal_review_booklet.future_task.future_task_status_id == 5
        pdf = "#{SAN_ROOT}/#{appraisal_review_booklet.booklet}"
        if FileTest.exists?(pdf)
          send_file pdf, :type => 'application/pdf', :disposition => 'inline', :filename => "make_a_deal_workbook_#{appraisal_review_booklet.future_task.completed.strftime('%Y%m%dT%H%M%S')}.pdf"
        else
          render :inline => "<p>Workbook is not available at this time.</p>"
        end
      else
        render :inline => "<p>Workbook is not generated.</p>"
      end
    end
  end
  
  def appraisal_review_booklet_form
    # load or create preferences
    appraisal_review_booklet_preference = AppraisalReviewBookletPreference.find(
      :first,
      :conditions => ["business_unit_id = ?", session[:business_unit].id]
    )
    appraisal_review_booklet_preference = AppraisalReviewBookletPreference.new do |p|
      p.business_unit_id = session[:business_unit].id
      p.include_appraisal_review_summary = true
      p.include_appraisal_review_detail = true
      p.include_appraisal_review_high_mileage_summary = true
      p.include_appraisal_review_high_mileage_detail = true
      p.include_appraisal_review_follow_up_summary = true
      p.include_appraisal_review_follow_up_detail = true
      p.include_appraisal_summary = true
      p.include_appraisal_summary_high_mileage = true
      p.email_notification = false
      p.email_address = nil
      p.created = DateTime.now.strftime("%Y-%m-%dT%H:%M:%S")
      p.last_modified = DateTime.now.strftime("%Y-%m-%dT%H:%M:%S")
    end if appraisal_review_booklet_preference.nil?
    appraisal_review_booklet_preference.save! if appraisal_review_booklet_preference.new_record?
    # load or create schedule
    appraisal_review_booklet_schedule = AppraisalReviewBookletSchedule.find(
      :first,
      :conditions => ["business_unit_id = ?", session[:business_unit].id]
    )
    appraisal_review_booklet_schedule = AppraisalReviewBookletSchedule.new do |s|
      s.business_unit_id = session[:business_unit].id
      s.hour_of_day = 8
    end if appraisal_review_booklet_schedule.nil?
    appraisal_review_booklet_schedule.save! if appraisal_review_booklet_schedule.new_record?
    # make a deal slider values
    appraisal_reviews_slider_values = ActiveRecord::Base.connection().select_all(<<-SQL
        SELECT
            days_back,
            number_of_appraisal_reviews,
            number_of_appraisals
        FROM
            dbo.appraisal_reviews_days_back_report
        WHERE
            business_unit_id = #{session[:business_unit].id}
        ORDER BY
            days_back
      SQL
    )
    appraisal_review_preference = session[:business_unit].appraisal_review_preference(true)
    appraisal_reviews_slider_value = appraisal_reviews_slider_values.detect{|r| r['days_back'].to_i == appraisal_review_preference.days_back}
    appraisal_reviews_count = appraisal_reviews_slider_value['number_of_appraisal_reviews'].to_i
    appraisals_count = appraisal_reviews_slider_value['number_of_appraisals'].to_i
    # render the page
    render :partial => 'appraisal_review_booklet_form', :object => nil, :locals => {
      :appraisal_review_booklet_preference => appraisal_review_booklet_preference,
      :appraisal_review_booklet_schedule => appraisal_review_booklet_schedule,
      :appraisal_reviews_slider_values => appraisal_reviews_slider_values,
      :appraisal_review_preference => appraisal_review_preference,
      :appraisal_reviews_count => appraisal_reviews_count,
      :appraisals_count => appraisals_count
    }
  end
  
  def appraisal_review_booklet_request
    # create future task
    future_task = FutureTask.new do |f|
      f.future_task_type_id = 1
      f.future_task_status_id = 1
      f.business_unit_id = session[:business_unit].id
      f.member_id = (session[:member].nil? ? nil : session[:member].id)
      f.title = "Generate Make-A-Deal Meeting Booklet"
      f.units_of_work = 0
      f.units_of_work_completed = 0
      f.created = DateTime.now.strftime("%Y-%m-%dT%H:%M:%S")
      f.commenced = nil
      f.completed = nil
      f.acknowledged = nil
    end
    # create booklet placeholder
    appraisal_review_booklet = AppraisalReviewBooklet.new()
    # create summary placeholder
    appraisal_review_booklet_summary = AppraisalReviewBookletSummary.new(params[:appraisal_review_booklet_summary])
    appraisal_review_booklet_summary.email_address = nil if appraisal_review_booklet_summary.email_address.nil? || appraisal_review_booklet_summary.email_address.empty?
    # save and queue work
    AppraisalReviewBookletSummary.transaction() do
      save_record!(future_task) do
        appraisal_review_booklet.future_task = future_task
        save_record!(appraisal_review_booklet) do
          appraisal_review_booklet_summary.appraisal_review_booklet = appraisal_review_booklet
          save_record!(appraisal_review_booklet_summary) do
            # message
            request_message = ReliableMessageQueue::QueueRequest.new(
              MESSAGE_QUEUE_NAME,
              ReliableMessageQueue::QueueItem.new(MESSAGE_QUEUE_NAME, nil, Base64::encode64(Marshal::dump(future_task))),
              nil
            )
            # send
            soap = ReliableMessageQueue::ReliableMessageQueueServiceSoap.new(MESSAGE_QUEUE_ENDPOINT)
            soap.generate_explicit_type = false
            soap.wiredump_dev = STDERR
	    soap.options['protocol.http.ssl_config.verify_mode'] = OpenSSL::SSL::VERIFY_NONE
            response_message = soap.add(request_message)
            # response
            unless response_message.status.code == ReliableMessageQueue::StatusCode::Success
              raise StandardError, response_message.status.message
            end
          end
        end
      end
    end
    # done!
    render :update do |page|
      page.hide 'notice'
      page.insert_html :top, '0_months_ago', :partial => 'appraisal_review_booklet_summary', :object => appraisal_review_booklet_summary
    end
  rescue StandardError => exception
    Log.error(exception)
    flash.now[:notice] = ["An error occurred that could not be handled", exception.message] if (flash.now[:notice].nil? || flash.now[:notice].empty?)
    render :update do |page|
      page.replace_html 'notice', "<p>We are unable to process your request at the moment. Please try again later.</p>"
      # page.replace_html 'notice', "<ul><li>#{flash.now[:notice].join('</li><li>')}</li></ul>"
      page.show 'notice'
    end
  end
  
  def appraisal_review_booklet_preference
    delete_non_values(params, [:appraisal_review_booklet_summary], [:email_address])
    params[:appraisal_review_booklet_summary].delete(:days_back)
    appraisal_review_booklet_preference = AppraisalReviewBookletPreference.find(
      :first,
      :conditions => ["business_unit_id = ?", session[:business_unit].id]
    )
    success = false
    update_record(appraisal_review_booklet_preference, params[:appraisal_review_booklet_summary]) do
      success = true
    end
    render :inline => (success ? "<%= image_tag('btn_submit_sm.gif') %>" : "<%= image_tag('btn_cancel_sm.gif') %>")
  end
  
  def appraisal_review_booklet_schedule
    appraisal_review_booklet_schedule = AppraisalReviewBookletSchedule.find(
      :first,
      :conditions => ["business_unit_id = ?", session[:business_unit].id]
    )
    params[:appraisal_review_booklet_schedule].each do |attribute, value|
      appraisal_review_booklet_schedule.send attribute.intern, value if appraisal_review_booklet_schedule.respond_to?(attribute.intern) && attribute != 'hour_of_day' 
    end
    appraisal_review_booklet_schedule.hour_of_day = params[:appraisal_review_booklet_schedule][:hour_of_day].to_i
    success = false
    save_record(appraisal_review_booklet_schedule) do
      success = true
    end
    flash.now[:notice].each {|msg| Log.error(msg) } unless (flash.now[:notice].nil? || flash.now[:notice].empty?)
    render :inline => (success ? "<%= image_tag('btn_submit_sm.gif') %>" : "<%= image_tag('btn_cancel_sm.gif') %>")
  end
  
  private
  
  def page_vars(title = "")
    @page_title = "Make-a-Deal: #{title}"
    @page_style = "potential_deal"
  end
  
end
