class HomeController < DynamicController

  skip_before_filter CAS::Filter, :except => [:index]
  skip_before_filter PromotionFilter, :except => [:index]

  layout :get_layout

private
  def get_layout
    return "#{@view_prefix}home"
  end

  def get_view_parameters
          return {
        :has_fl30 => true,
        :prefix => "fl30_",
        :page_style => "/fl30/stylesheets/home.css"
      }
	end

public
  def index
    
    @view_parameters = get_view_parameters
    @page_style = @view_parameters[:page_style]
    @view_prefix = @view_parameters[:prefix]
    @has_fl30 = @view_parameters[:has_fl30]

    # FIXME: This code needs to be removed and exists during CAS integration.
    if params[:id]
      @session[:business_unit] = BusinessUnit.find(params[:id])
    end
    # ajax the s&a query so we don't hang
    @search_and_aquisition_report = search_and_aquisition_report_cache
    # html data for under big graph in center of page
    @inventory_sales_graph_data = InventorySalesGraphData.for_business_unit(session[:business_unit].id)
    days_supply = ActiveRecord::Base.connection().select_all("exec dbo.GetDaysSupply #{session[:business_unit].id}")
    @total_units, @total_water, @total_dollars, @days_supply = 0, 0, 0, 0
    @inventory_sales_graph_data.each{|d| @total_units += d.units}
    @inventory_sales_graph_data.each{|d| @total_water += d.water}
    @inventory_sales_graph_data.each{|d| @total_dollars += d.dollars}
    days_supply.each{|d| @days_supply += d["DS"].to_i}
    # edge replanning count
    upgrades = session[:business_unit].business_units_upgrades(true)
    if upgrades.detect{|u| u.upgrade_id == 2}
      @number_of_vehicles_due_for_replanning = 0
      planning_data = ActiveRecord::Base.connection().select_all("SELECT * FROM GetAgingInventoryPlanSummary(#{session[:business_unit].id}, null, 4, '#{Date.today.strftime('%Y-%m-%d')}', 1) WHERE RangeID = 0")
      planning_data.each{|d| @number_of_vehicles_due_for_replanning += d["ReplanUnits"].to_i}
      @has_planning = true
    else
      @has_planning = false
    end
    # make a deal upgrade
    @show_make_a_deal = upgrades.detect{|u| u.upgrade_id == 21}
    # has purchasing center upgrade
    @show_purchasing = upgrades.detect{|u| u.upgrade_id == 3}
    # has equity analyzer upgrade
    @show_equityAnalyzer = upgrades.detect{|u| u.upgrade_id == 11}
    #transfer pricing group 'preference'
    @show_in_group_search = session[:business_unit].parent.business_unit_group_preference.show_car_search
    #pingII pricing tool
    @has_pricing = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 19}
    # Market Stocking
    @has_market_stocking = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 22}
    #sales accelerator or consumer value accelerator or max marketing
    @has_marketing = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 23 }
    # Max Ad
    @has_merchandising = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 24}

    render :action => "#{@view_prefix}index"
  end

  # The Partials Follow
  def appraise_gauge
    @view_prefix = get_view_parameters[:prefix]

    # get the target
    business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.load_insight_target(
      16,
      session[:business_unit].id
    )
    closing_rate_target = business_unit_insight_target_preferences.first.value.to_s
    # get the values
    appraisal_closing_rate_measure = AppraisalClosingRateMeasure.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
    if appraisal_closing_rate_measure.nil? || appraisal_closing_rate_measure.measure_from.nil? || appraisal_closing_rate_measure.measure_upto.nil?
      date0 = ''
      date1 = ''
    else
      date0 = appraisal_closing_rate_measure.measure_from.strftime('%m/%d/%Y')
      date1 = appraisal_closing_rate_measure.measure_upto.strftime('%m/%d/%Y')
    end
    # render
    render :partial => "#{@view_prefix}appraise_gauge", :locals => { :closing_rate_target => closing_rate_target, :date0 => date0, :date1 => date1 }
  end

  def appraise_graph
    @view_prefix = get_view_parameters[:prefix]

    # get the target
    business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.load_insight_target(
      16,
      session[:business_unit].id
    )
    closing_rate_target = business_unit_insight_target_preferences.first.value.to_s
    # get the values
	#appraisal_closing_rate_measure2 = ActiveRecord::Base.connection().select_all("exec dbo.CountAppraisalsClosed 1, '#{session[:business_unit].id}'")
	
	appraisal_closing_rate_measure_list = AppraisalClosingRateMeasure.find_by_sql("exec dbo.CountAppraisalsClosed 1, '#{session[:business_unit].id}'")
    #appraisal_closing_rate_measure = AppraisalClosingRateMeasure.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
	appraisal_closing_rate_measure = AppraisalClosingRateMeasure.new
	
	if !appraisal_closing_rate_measure.nil? && !appraisal_closing_rate_measure_list.nil?
	
		appraisal_closing_rate_measure_list.each do |appraisal_closing_rate_measure2|
			appraisal_closing_rate_measure.measure=appraisal_closing_rate_measure2.measure
			appraisal_closing_rate_measure.measure_from=appraisal_closing_rate_measure2.measure_from
			appraisal_closing_rate_measure.measure_upto=appraisal_closing_rate_measure2.measure_upto
			appraisal_closing_rate_measure.measure_denominator=appraisal_closing_rate_measure2.measure_denominator
			appraisal_closing_rate_measure.measure_numerator=appraisal_closing_rate_measure2.measure_numerator
		end
	
		
	end
	
	
	
    if appraisal_closing_rate_measure.nil? || appraisal_closing_rate_measure.measure_from.nil? || appraisal_closing_rate_measure.measure_upto.nil?
      date0 = ''
      date1 = ''
    else
	
		appraisal_closing_rate_measure_list.each do |appraisal_closing_rate_measure2|
			appraisal_closing_rate_measure.measure=appraisal_closing_rate_measure2.measure
			appraisal_closing_rate_measure.measure_from=appraisal_closing_rate_measure2.measure_from
			appraisal_closing_rate_measure.measure_upto=appraisal_closing_rate_measure2.measure_upto
			appraisal_closing_rate_measure.measure_denominator=appraisal_closing_rate_measure2.measure_denominator
			appraisal_closing_rate_measure.measure_numerator=appraisal_closing_rate_measure2.measure_numerator
		end
      date0 = appraisal_closing_rate_measure.measure_from.strftime('%m/%d/%Y')
      date1 = appraisal_closing_rate_measure.measure_upto.strftime('%m/%d/%Y')
	
	  
    end
    if appraisal_closing_rate_measure.nil?
      appraisals = 0
      closed = 0
      not_closed = 0
    else
      appraisals = appraisal_closing_rate_measure.measure_denominator.nil? ? 0 : appraisal_closing_rate_measure.measure_denominator
      closed = appraisal_closing_rate_measure.measure_numerator.nil? ? 0 : appraisal_closing_rate_measure.measure_numerator
      not_closed = appraisals - closed
    end

    # render
    render :partial => "#{@view_prefix}appraise_graph", 
      :locals => { 
        :date0 => date0, :date1 => date1, 
        :appraisals => appraisals, :closed => closed, :not_closed => not_closed,
        :percentage => appraisal_closing_rate_measure.nil? ? "" : appraisal_closing_rate_measure.measure.to_s
      }
  end

  def appraise_facts
    @view_prefix = get_view_parameters[:prefix]
  	# get the trade_in_inventory_analyzed values
  	#trade_in_inventory_analyzed_measure = TradeInInventoryAnalyzedMeasure.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
  	business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.load_insight_target(
  	  18,
  	  session[:business_unit].id
  	)
  	trade_in_inventory_analyzed_target = business_unit_insight_target_preferences.first.value
  	# get the average immediate wholesale profit values
  	#average_immediate_wholesale_profit_measure = AverageImmediateWholesaleProfitMeasure.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
  	
	business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.load_insight_target(
  	  19,
  	  session[:business_unit].id
  	)
  	target = business_unit_insight_target_preferences.first
  	average_immediate_wholesale_profit_target = ""
  	unless target.nil? || target.min.nil? || target.max.nil?
  		if target.min.abs == target.max.abs
  			if target.min == target.max
  			average_immediate_wholesale_profit_target = "$#{target.min.abs}"
  			else
  			average_immediate_wholesale_profit_target = "&#177; $#{target.min.abs}"
  			end
  		else
  			min = (target.min >= 0 ? "$#{target.min}" : "-$#{target.min.abs}")
  			max = (target.max >= 0 ? "$#{target.max}" : "-$#{target.max.abs}")
  			average_immediate_wholesale_profit_target = "#{min} to #{max}"
  		end
  	end
	
	
	#####################################
	
	
	average_immediate_wholesale_profit_measure = AverageImmediateWholesaleProfitMeasure.new
	average_immediate_wholesale_profit_measure_list = AverageImmediateWholesaleProfitMeasure.find_by_sql("exec dbo.GetAvgImmediateWholeSalePLReport '#{session[:business_unit].id}'")
    average_immediate_wholesale_profit_measure.measure=0
	if !average_immediate_wholesale_profit_measure.nil? && !average_immediate_wholesale_profit_measure_list.nil?
	
		average_immediate_wholesale_profit_measure_list.each do |average_immediate_wholesale_profit_measure2|
			average_immediate_wholesale_profit_measure.measure=average_immediate_wholesale_profit_measure2.measure
			
		end
	end
	
  	trade_in_inventory_analyzed_measure = TradeInInventoryAnalyzedMeasure.new
	trade_in_inventory_analyzed_measure_list= TradeInInventoryAnalyzedMeasure.find_by_sql("exec dbo.GetTradeAnalyzerUsageReport '#{session[:business_unit].id}'")
    
	trade_in_inventory_analyzed_measure.measure=0
	if !trade_in_inventory_analyzed_measure.nil? && !trade_in_inventory_analyzed_measure_list.nil?
	
		trade_in_inventory_analyzed_measure_list.each do |trade_in_inventory_analyzed_measure2|
			trade_in_inventory_analyzed_measure.measure=trade_in_inventory_analyzed_measure2.measure
			
		end
	
		
	end
	
	
	
	#####################################
  	# render the component
  	render :partial => "#{@view_prefix}appraise_facts", :locals => {
  	  :trade_in_inventory_analyzed_measure        => trade_in_inventory_analyzed_measure,
  	  :trade_in_inventory_analyzed_target         => trade_in_inventory_analyzed_target,
  	  :average_immediate_wholesale_profit_measure => average_immediate_wholesale_profit_measure,
  	  :average_immediate_wholesale_profit_target  => average_immediate_wholesale_profit_target
  	}
  end

  def make_deal
    @view_prefix = get_view_parameters[:prefix]

    business_unit_id = session[:business_unit].id
    appraisal_reviews_slider_values = ActiveRecord::Base.connection().select_all(<<-SQL
        SELECT
            days_back,
            number_of_appraisal_reviews,
            number_of_appraisals
        FROM
            dbo.appraisal_reviews_days_back_report
        WHERE
            business_unit_id = #{business_unit_id}
        ORDER BY
            days_back
      SQL
    )
    appraisal_review_preference = AppraisalReviewPreference.find(
      :first,
      :conditions => ["business_unit_id = ?", business_unit_id]
    )
    appraisal_reviews_slider_value = appraisal_reviews_slider_values.detect{|r| r['days_back'].to_i == appraisal_review_preference.days_back}
    appraisals_count = appraisal_reviews_slider_value['number_of_appraisals'].to_i
    appraisal_reviews_count = appraisal_reviews_slider_value['number_of_appraisal_reviews'].to_i
    follow_up_counts = AppraisalReviewFollowUp.count_by_sql(<<-SQL
        SELECT  count(*)
		FROM    appraisal_review_follow_ups F
		JOIN    appraisal_review_follow_up_lists L ON L.id = F.appraisal_review_follow_up_list_id
		WHERE   F.business_unit_id = #{business_unit_id}
		AND     F.appraisal_review_follow_up_action_id IS NULL
		AND     L.active = 1
      SQL
    )
    render :partial => "#{@view_prefix}make_deal", :object => nil, :locals => {
      :follow_up_counts => follow_up_counts,
      :appraisals_count => appraisals_count,
      :appraisal_review_preference => appraisal_review_preference,
      :appraisal_reviews_count => appraisal_reviews_count,
      :appraisal_reviews_slider_values => appraisal_reviews_slider_values
    }
  end

  def trade_analyzer
    @view_prefix = get_view_parameters[:prefix]
    
    @appraisals = Appraisal.count_by_sql(<<-SQL
        SELECT  number_of_appraisals_waiting
        FROM    appraisals_waiting_report
        WHERE   business_unit_id = #{session[:business_unit].id}
      SQL
    )
    # has purchasing center upgrade
    @show_tradeAnalyzer = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 1}
    render :partial => "#{@view_prefix}trade_analyzer"
  end
  
  def inventory_gauge
    @view_prefix = get_view_parameters[:prefix]

    optimal_inventory_report = OptimalInventoryReport.new(session[:business_unit].id)
    @stocked = if !optimal_inventory_report.total_stocked_units_model.nil? 
                  optimal_inventory_report.total_stocked_units_model.to_s
               else
                  0.to_s
               end
    @optimal = if !optimal_inventory_report.total_optimal_units_model.nil?
                  optimal_inventory_report.total_optimal_units_model.to_s
               else
                  0.to_s
               end
   
    render :partial => "#{@view_prefix}inventory_gauge"
  end

  def core_inventory_graph
    @view_prefix = get_view_parameters[:prefix]

    optimal_inventory_report = OptimalInventoryReport.new(session[:business_unit].id)
    @stocked = if !optimal_inventory_report.total_stocked_units_model.nil? 
                  optimal_inventory_report.total_stocked_units_model.to_s
               else
                  0.to_s
               end
    @optimal = if !optimal_inventory_report.total_optimal_units_model.nil?
                  optimal_inventory_report.total_optimal_units_model.to_s
               else
                  0.to_s
               end
   
    render :partial => "#{@view_prefix}core_inventory_graph"
  end
    
  def potential_tradeins
    @view_prefix = get_view_parameters[:prefix]

    business_unit_id = @session[:business_unit].id
    @groupTrades = Appraisal.count_by_sql(<<-SQL
      SELECT  number_of_potential_trade_ins
      FROM    potential_trade_in_report
      WHERE   business_unit_id = #{business_unit_id}
    SQL
    )
    # has purchasing center upgrade
    @show_purchasing = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 3}
    render :partial => "#{@view_prefix}potential_tradeins"
  end
  
  def in_group_search
    @view_prefix = get_view_parameters[:prefix]

  	render :partial => "#{@view_prefix}in_group_search", :locals => {:halsessionid => @session.session_id}
  end
    
  def in_group_search_locate
  	vin = ActiveRecord::Base.connection().select_one("exec dbo.GetInGroupInventorySearch @BusinessUnitID=#{session[:business_unit].id}, @StockOrVin='#{params[:stockNumberOrVin]}'")
  	if vin
  		headers['X-Instruction'] = "ok"
  		logger.debug(vin) if logger
  		render :text => vin['Vin']
  	else
		headers['X-Instruction'] = "notfound"
  		render :text => "Vehicle not found."
  	end
  end
  
  def flash_locate
    @view_prefix = get_view_parameters[:prefix]

    # has purchasing center upgrade
    @show_purchasing = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 3}
    @vehicle_makes = VehicleMake.all_options
    render :partial => "#{@view_prefix}flash_locate"
  end
  
  def search_and_aquisition
    @view_prefix = get_view_parameters[:prefix]

    @search_and_aquisition_report = search_and_aquisition_report_cache
    if @search_and_aquisition_report.nil?
      @search_and_aquisition_report = PurchasingCenterVehicleSummaryReport.new(
        session[:business_unit].id,
        session[:member].id
      )
      session[:search_and_aquisition_report] = @search_and_aquisition_report
    end
    render :update do |page|
      ActionView::Base.debug_rjs = false
      page.replace 'online-buying', :partial => "#{@view_prefix}online_buying", :object => nil, :locals => { :search_and_aquisition_report => @search_and_aquisition_report }
      page.replace 'live-auctions', :partial => "#{@view_prefix}live_auctions", :object => nil, :locals => { :search_and_aquisition_report => @search_and_aquisition_report }
      page.replace 'in-group-container', :partial => "#{@view_prefix}in_group", :object => nil, :locals => { :search_and_aquisition_report => @search_and_aquisition_report }
    end
  end
  
  def search_and_aquisition_report_cache
    @view_prefix = get_view_parameters[:prefix]

    search_and_aquisition_report = session[:search_and_aquisition_report]
    unless search_and_aquisition_report.nil?
      query_age = ((DateTime.now - search_and_aquisition_report.created)*86400).to_i
      thirty_minutes = 60*30
      if query_age > thirty_minutes
        search_and_aquisition_report = nil
      end
    end
    search_and_aquisition_report
  end

  def due_for_updating_price_in_ad
      url = "#{DATAWEBSERVICES_BASE_URL}datawebservices/api.aspx/ActionNeeded/#{session[:business_unit].id}/#{session[:member].login}"
      encoded_url = URI.encode(url)
      response = Net::HTTP.get_response(URI.parse(encoded_url))
      headers['Content-Type'] = "application/json"
      render :text => response.body
  end 

end
