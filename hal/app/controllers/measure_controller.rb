class MeasureController < DynamicController
  skip_after_filter NoCacheFilter
  after_filter RemoveCacheHeadersFilter
  def index
    # load the closing rate target
    business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.load_insight_target(
      16,
      session[:business_unit].id
    )
    @closing_rate_target = (business_unit_insight_target_preferences.first.value*2.08).to_s
    # load the measure
    metric_id = params[:metric_id].to_i
    period_id = params[:period_id].to_i
    @span, @fraction, @date = 0, "", ""
    if period_id == 2
      klass, title, fmt, date = nil, nil, nil, nil
      case metric_id
        when 1
          klass = AppraisalClosingRateMeasure
          title = "Appraisal \rClosing Rate: %d%"
          fmt   = Proc.new{|v| v * 100}
          date  = "4 weeks ending %s"
        when 2
          klass = TradeInInventoryAnalyzedMeasure
          title = "Trade In \rInventory Analyzed: %d%"
          fmt   = Proc.new{|v| v * 100}
          date  = "ending %s"
        when 18
          klass = AverageImmediateWholesaleProfitMeasure
          title = "Avg. Imm. \rWholesale Profit: $%d"
          fmt   = Proc.new{|v| v}
          date  = "ending %s"
      end
      unless klass.nil?
        record = klass.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
        @span     = fmt.call(record.measure)*2
        @fraction = sprintf(title, fmt.call(record.measure))
        @date     = sprintf(date, record.measure_upto.strftime('%m/%d/%Y')) unless record.measure_upto.nil?
      end
    else
      measures = Measure.find(
        :first,
        :conditions => [
          "business_unit_id = ? and metric_id = ? and period_id = ?",
          session[:business_unit].id,
          metric_id,
          period_id
        ]
      )
      if measures.nil?
        @span = 0
      else
        @span = measures.first.measure*200
      end
    end
  end
  
  def optimal_inventory_report
    optimal_inventory_report = OptimalInventoryReport.new(session[:business_unit].id)
    @title   = 'Optimal Inventory'
    @subtitle = 'in stock'
    @stocked = if !optimal_inventory_report.total_stocked_units_model.nil? 
                  optimal_inventory_report.total_stocked_units_model.to_s
               else
                  0.to_s
               end
    @optimal = if !optimal_inventory_report.total_optimal_units_model.nil?
                  optimal_inventory_report.total_optimal_units_model.to_s
               else
                  0.to_s
               end
    
    @p1 = if !optimal_inventory_report.percentage.nil?
            sprintf("%.0f",(optimal_inventory_report.percentage.to_f * 100))
          else
            0
          end
    @p2 = @p1.to_s + '%'
    @span    = 0
    if (optimal_inventory_report.percentage*228).to_i > 238
      @span = 238
    else
      @span = (optimal_inventory_report.percentage*228).to_i
    end
  end
  
end
