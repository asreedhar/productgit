class PlanningCategoryConfigurationController < DynamicController
  
  layout "preference"
  
  def planning_category
    page_vars
    @business_unit_planning_category = BusinessUnitPlanningCategory.find(
      :first,
      :conditions => [
        "business_unit_id = ? and is_comparison_configuration = 0 and planning_category_id = ?",
        session[:business_unit].id,
        params[:planning_category_id]
      ]
    )
  end
  
  def update_business_unit_planning_category
    BusinessUnitPlanningCategory.transaction() do
      BusinessUnitPlanningCategory.update(
        params[:business_unit_planning_category].keys,
        params[:business_unit_planning_category].values
      )
    end
    refresh
    success
  rescue StandardError => exception
    error(exception)
  end
  
  private
  
  def page_vars
    @page_title = "Preferences - Planning - Configuration"
    @page_style = "preference"
  end
  
  def success
    render :update do |page|
      page.replace 'compare_categories', :partial => 'compare_categories', :locals => { :planning_category_id => params[:planning_category_id] }
    end
  end
  
  def error(exception)
    Log.error(exception)
    if flash.now[:notice].nil?
      flash.now[:notice] = 'An error occurred that could not be handled: ' + exception
    end
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
  def refresh
    ActiveRecord::Base.connection.execute("EXECUTE dbo.UpdateInventory_A8 #{session[:business_unit].id}", "Rebuild Graph Data for #{session[:business_unit].id}");
  end
  
end
