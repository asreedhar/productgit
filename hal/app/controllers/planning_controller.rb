require 'ping'

class PlanningController < DynamicController

  include ApplicationHelper
  include PlanningHelper
  include CampaignRunHelper
  
  def index
    # setup layout and css
    @page_title = "Planning"
    @page_style = "planning"
    # load menu items
    @planning_age_buckets = session[:business_unit].planning_age_buckets()
    @planning_categories  = PlanningCategory.business_unit_options(session[:business_unit].id)
    @planning_actions     = PlanningAction.all_options
    @vehicle_makes        = VehicleMake.all_options
    # if there is no search defined start with the first bucket
    if !SearchCriterion.has_search_criterion_params?(params)
      params[:planning_age_bucket] = { :id => [@planning_age_buckets.first.id] }
    end
    # run the search and pass the search result id back in to inventory item list later
    @search_criterion, @vehicle_list_id, @number_of_vehicles = * find_inventory(session[:business_unit].id, params)
    @planning_action_id = nil
    if neither_nil_nor_empty(params,:planning_action)
      if neither_nil_nor_empty(params[:planning_action],:id)
        obj = params[:planning_action][:id]
        case obj
          when String
            @planning_action_id = obj.to_i if obj =~ /^\d+$/
          when Integer
            @planning_action_id = obj
        end
      end
    end
  end
  
  def inventory_item_list
    # extract nil safe vars from request
    search_result_id = params[:search_result][:id] if neither_nil_nor_empty(params,:search_result)
    vehicle_list_id = params[:vehicle_list][:id] if neither_nil_nor_empty(params,:vehicle_list)
    planning_action_id = params[:planning_action][:id] if neither_nil_nor_empty(params,:planning_action)
    # load list of vehicles
    inventory = load_inventory(search_result_id, vehicle_list_id)
    # update page
    render :update do |page|
      # clear out whatever is there at the moment
      page.replace_html 'Inventory', ''
      page.replace_html 'InventoryForm', ''
      # choose the page layout
      if planning_action_id.nil?
        # (a) by inventory item
        page.insert_html :bottom, 'Inventory', :partial => 'inventory', :collection => inventory, :locals => { :side_of_page => 'left', :first_inventory_item => inventory.first.id }
        unless inventory.empty?
          page.replace_html 'InventoryForm', :partial => 'inventory_form', :locals => { :inventory => inventory.first, :planning_action_id => nil }
        end
      else
        # (b) by single planning action
        page.insert_html :bottom, 'Inventory', :partial => 'inventory_list', :object => inventory, :locals => { :planning_action_id => planning_action_id }
      end
    end
  end
  
  def inventory_form
    @inventory = Inventory.find(params[:id])
    render :update do |page|
      page.replace_html 'InventoryForm', :partial => 'inventory_form', :locals => { :inventory => @inventory, :planning_action_id => nil }
    end
  end
  
  def ping_price
    begin
      @inventory = Inventory.find( params[:id] )
    
      mmg_id = @inventory.vehicle.vehicle_line.legacy_id
    
      @ping_prices = PingPrices.find( :first, :conditions => [ "mmg_id = ? AND distance = 50 AND year = 2004 and ping_provider_id = 1", mmg_id] )
      if @ping_prices.nil?
        pa = Ping::PingAdapter::new
        values = pa.parse({:mmg => mmg_id, :distance => 50, :year => 2004, :provider_code => "autotrader"})
        @ping_prices = PingPrices::new( values )
        @ping_prices.created = Time.now
        @ping_prices.ping_provider_id = 1
        @ping_prices.save
      end

      render :partial => 'ping_price'
    
    rescue 
      logger.error $!
      logger.error $@.join( "\n\t" )
      render :partial => 'ping_price_error'
    end    
  end

  def refresh_planning_act
    # get my vars
    @planning_action = PlanningAction.find(params[:planning_action_id])
    @inventory = Inventory.find(params[:inventory_id], :include => [:vehicle,:inventory_planning_tasks,:inventory_book_valuation_summaries])
    # render my partial
    if params[:menu_bar_component]
      @inventory_id = @inventory.id
      @planning_action_type_id = @planning_action.planning_action_type_id
      @planning_action_id = @planning_action.id
      render :inline => "<%= render_menu_bar_component(:planning_action_menu_bar) %>"
    else
      render :partial => "planning_act_#{@planning_action.underscore_name}", :locals => { :inventory => @inventory, :planning_action => @planning_action }
    end
  end
  
  #
  # Service Actions
  #
  
  def service_open
    render :update do |page|
      page.replace_html 'Service', :partial => 'service_panel'
    end
  end

  def service_close
    render :update do |page|
      page.replace_html 'Service', ''
    end
  end
  
  #
  # Save New Planning Acts
  # 
  
  def planning_act_reprice
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # new reprice act
    planning_act_reprice = PlanningActReprice.new(params[:planning_act_reprice])
    planning_act_reprice.business_unit_id = session[:business_unit].id
    planning_act.planning_act_reprices << planning_act_reprice
    # inventory record for reprice
    inventory = Inventory.find(params[:planning_act][:inventory_id], :include => :inventory_advertising_attribute)
    # new price
    new_price = params[:planning_act_reprice][:new_price]
    # save the data
    PlanningAct.transaction() do
      if planning_act.save
        if planning_act_reprice.planning_act_price_attribute_id == 1
          # update list price
          unless inventory.update_attribute(:list_price, new_price)
            record_errors(inventory)
          end
        else
          # create/update ad price
          inventory_advertising_attribute = inventory.inventory_advertising_attribute
          if inventory_advertising_attribute.nil?
            inventory_advertising_attribute = InventoryAdvertisingAttribute.new do |a|
              a.id           = inventory.id
              a.inventory_id = inventory.id
              a.price        = new_price
            end
          else
            inventory_advertising_attribute.price = new_price
          end
          unless inventory_advertising_attribute.save
            record_errors(inventory_advertising_attribute)
          end
        end
      else
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_lot_promotion
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to selected lot promotions
    planning_act_lot_promotions = PlanningActLotPromotion.find(params[:planning_act_lot_promotion][:id])
    planning_act.planning_act_lot_promotions << planning_act_lot_promotions
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_spiff
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to selected spiff or create a new spiff
    if params[:planning_act_spiff][:id].detect{|id| id == ""}
      params[:planning_act_spiff].delete(:id)
      planning_act_spiff = PlanningActSPIFF.new(params[:planning_act_spiff])
      planning_act_spiff.business_unit_id = session[:business_unit].id
      planning_act_spiff.active = true
      planning_act_spiffs = [planning_act_spiff]
    else
      planning_act_spiffs = PlanningActSPIFF.find(params[:planning_act_spiff][:id])
    end
    planning_act.planning_act_spiffs << planning_act_spiffs
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_other
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to selected lot promotions
    planning_act_others = PlanningActOther.find(params[:planning_act_other][:id])
    planning_act.planning_act_others << planning_act_others
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_live_auction
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to live auction
    planning_act_live_auction = PlanningActLiveAuction.new(params[:planning_act_live_auction])
    planning_act_live_auction.live_auction = LiveAuction.find(params[:live_auction][:id].first)
    planning_act_live_auction.business_unit_id = session[:business_unit].id
    planning_act.planning_act_live_auctions << planning_act_live_auction
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_wholesaler
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to live auction
    planning_act_wholesaler = PlanningActWholesaler.new(params[:planning_act_wholesaler])
    planning_act_wholesaler.wholesaler = Wholesaler.find(params[:wholesaler][:id].first)
    planning_act_wholesaler.business_unit_id = session[:business_unit].id
    planning_act.planning_act_wholesalers << planning_act_wholesaler
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_sold
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to live auction
    planning_act_sold = PlanningActSold.new(params[:planning_act_sold])
    planning_act_sold.business_unit_id = session[:business_unit].id
    planning_act.planning_act_solds << planning_act_sold
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  def planning_act_small_print_advertisement_generate_text
    @inventory = Inventory.find(params[:inventoryId])
    @print_publisher = PrintPublisher.find(23) 
    render :text => build_ad(@inventory, @print_publisher).to_s
  end
  
  def planning_act_small_print_advertisement
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    delete_non_values(params,[:inventory_advertising_attribute],[:description])
    delete_non_values(params,[:planning_act_small_print_advertisement],[:description])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # load inventory item
    inventory = Inventory.find(params[:planning_act][:inventory_id])
    # update inventory advertising attribute
    inventory_advertising_attribute = InventoryAdvertisingAttribute.find(:first, :conditions => ["inventory_id = ?", inventory.id])
    description = (neither_nil_nor_empty(params[:inventory_advertising_attribute],:description) ? params[:inventory_advertising_attribute][:description] : nil);
    if inventory_advertising_attribute.nil?
      inventory_advertising_attribute = InventoryAdvertisingAttribute.new do |a|
        a.id           = inventory.id
        a.inventory_id = inventory.id
        a.description  = description
      end
    else
      inventory_advertising_attribute.description = description
    end
    # new small print planning acts
    planning_act_small_print_advertisements = []
    vehicle_lists = []
    params[:campaign][:id].each do |campaign_id|
      # TODO: if the vehicle is already in the list update the small print planning act?
      campaign_run, vehicle_list = * create_campaign_run_for_campaign(campaign_id)
      next if vehicle_list.vehicle_list_items.detect{|item| item.vehicle_id == inventory.vehicle_id}
      planning_act_small_print_advertisement = PlanningActSmallPrintAdvertisement.new(params[:planning_act_small_print_advertisement])
      planning_act_small_print_advertisement.business_unit_id = session[:business_unit].id
      planning_act_small_print_advertisement.campaign_run_id = campaign_run.id
      planning_act_small_print_advertisements << planning_act_small_print_advertisement
      vehicle_lists << vehicle_list
    end
    planning_act.planning_act_small_print_advertisements << planning_act_small_print_advertisements
    # save the data
    PlanningAct.transaction() do
      save_record(planning_act) do
        save_record(inventory_advertising_attribute) do
          vehicle_lists.each do |vehicle_list|
            vehicle_list.vehicle_list_items << VehicleListItem.new do |item|
              item.vehicle_id = inventory.vehicle_id
            end
          end
        end
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], inventory)
  end
  
  def planning_act_internet_auction
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    delete_non_values(params,[:planning_act_internet_auction],[:announcements])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # load third party
    third_party = ThirdParty.find(params[:planning_act_internet_auction][:third_party_id])
    # join to live auction
    planning_act_internet_auction = PlanningActInternetAuction.new(params[:planning_act_internet_auction])
    planning_act_internet_auction.business_unit_id = session[:business_unit].id
    planning_act.planning_act_internet_auctions << planning_act_internet_auction
    # prepare GMAC posting
    if third_party.name == 'GMAC Smart Auction'
      if has_credential?(session[:member], 3)
        # load objects
        inventory = Inventory.find(params[:planning_act][:inventory_id], :include => :vehicle)
        # marshall SOAP objects
        vehicle = GMAC::Vehicle.new([], [], inventory.vehicle.vin, inventory.vehicle.model_year, inventory.vehicle.vehicle_make.name, inventory.vehicle.vehicle_line.name, inventory.vehicle.series, inventory.vehicle.color, nil, nil, nil, inventory.mileage_received, nil, nil, nil, nil, nil, nil, nil, nil)
        images = []
        if planning_act_internet_auction.send_pictures == true
          Photos.find(:all, :conditions => ["photo_type_id = ? and parent_entity_id = ?", 2, inventory.id]).each do |photo|
            images << GMAC::InspectionReportImage.new("http://localhost:9090/photos/" + photo.url)
          end
        end
        announcements = ""
        if planning_act_internet_auction.send_announcements == true
          announcements = planning_act_internet_auction.announcements
        end
        extended_attributes = GMAC::InspectionReportExtendedAttributes.new("FIRSTLOOK")
        inspection_report = GMAC::InspectionReport.new(vehicle, images, [], nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, planning_act_internet_auction.announcements, nil, nil, nil, nil, extended_attributes)
        inspection_report_file = GMAC::InspectionReportFile.new([inspection_report])
        inspection_report_file.xmlattr_SendDate = DateTime.now.strftime("%Y-%m-%dT%H:%M:%S")
        inspection_report_file.xmlattr_TotalReports = 1
        post_inspection_report = GMAC::PostInspectionReport.new(inspection_report_file, "#{inventory.vehicle.vehicle_make.name} #{inventory.vehicle.vehicle_line.name} #{inventory.vehicle.series}", inventory.stock_number, "firstlook", "webservice7!")
      end
    end
    # save the data
    PlanningAct.transaction() do
      if planning_act.save
        if third_party.name == 'GMAC Smart Auction'
          # make the soap request
          soap = GMAC::DIPostServiceSoap.new("http://test.inspections.ally.com/GMACSAPosterService/DiPostService.asmx")
          soap.generate_explicit_type = false
          soap.wiredump_dev = STDERR
          post_inspection_report_response = soap.postInspectionReport(post_inspection_report)
          url = post_inspection_report_response.directInspectResponse.queryString
          logger.debug url.to_s
          # save the response
          marketplace_submission = MarketplaceSubmission.new do |s|
            s.third_party_id = third_party.id
            s.inventory_id = inventory.id
            s.member_id = session[:member].id
            s.marketplace_submission_status_id = 1
            s.url = url
            s.begin_effective_date = Time.now.strftime("%m/%d/%Y")
            s.end_effective_date = (Date.today + 1).strftime("%m/%d/%Y")
            s.date_created = Time.now.strftime("%m/%d/%Y")
          end
          unless marketplace_submission.save
            record_errors(marketplace_submission)
          end
        end
      else
        record_errors(planning_act)
        record_errors(planning_act_internet_auction)
      end
    end
    # re-render the form
    if flash.now[:notice].nil? || flash.now[:notice].empty?
      render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
    else
      render :update do |page|
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def planning_act_internet_advertisement
    delete_non_values(params,[:planning_act],[:notes,:include_in_plan_until])
    # new planning act
    planning_act = PlanningAct.new(params[:planning_act])
    planning_act.business_unit_id = session[:business_unit].id
    planning_act.created = Time.now
    # join to live auction
    planning_act_internet_advertisement = PlanningActInternetAdvertisement.new(params[:planning_act_internet_advertisement])
    planning_act_internet_advertisement.business_unit_id = session[:business_unit].id
    planning_act.planning_act_internet_advertisements << planning_act_internet_advertisement
    # save the data
    PlanningAct.transaction() do
      unless planning_act.save
        record_errors(planning_act)
      end
    end
    # re-render the form
    render_planning_act(params[:planning_act][:planning_action_id], Inventory.find(params[:planning_act][:inventory_id]))
  end
  
  private
  
  def delete_non_values(map,objects,attrs)
    objects.each do |object|
      if neither_nil_nor_empty(map,object)
        attrs.each do |attr|
          if not_nil(map[object],attr)
            if ['Please Select',''].include?(map[object][attr])
              map[object].delete(attr)
            end
          end
        end
      end
    end
  end
  
end
