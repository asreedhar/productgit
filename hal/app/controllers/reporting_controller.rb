class ReportingController < DynamicController

  include ReportingHelper
  
  def view_deals_report
    @page_style = "drill_down"
    vehicle_mileage = nil
    if params.key?("high_mileage_filter") && !params["high_mileage_filter"].nil?
      vehicle_mileage = Array.new
      vehicle_mileage << 0
      vehicle_mileage << session[:business_unit].business_unit_preference.high_mileage_overall
    end
    @retail = run_view_deals_report(
        session[:business_unit].id,
        params["vehicle_segment_id"],
        params["vehicle_series"],
        vehicle_mileage,
        params["inventory_type"],
        'R',
        params["weeks"],
        params["forecast"],
        params["sort"]
    )
    @no_sales = run_view_deals_report(
        session[:business_unit].id,
        params["vehicle_segment_id"],
        params["vehicle_series"],
        vehicle_mileage,
        params["inventory_type"],
        'N',
        params["weeks"],
        params["forecast"],
        params["sort"]
    )
  end
  
  def naaa_auction_report
    @page_style = "drill_down"
    @results = run_naaa_auction_report(
        params["naaa_area_id"],
        params["naaa_period_id"],
        params["vic"],
        params["sort"]
    )
  end
  
  def trade_closing_rate_report


    @page_style = "drill_down"
    @results = run_trade_closing_rate_report(
        session[:business_unit].id,
        params["date_0"],
        params["date_1"],
        params["sort"]
    )
    business_unit_id = @session[:business_unit].id
    @business_unit = @session[:business_unit].name
    measures = Measure.find_by_sql(<<-SQL
        SELECT
          *
        FROM
          measures
        WHERE
            business_unit_id = #{business_unit_id}
        AND metric_id = 1
        AND period_id = 2
      SQL
    )
    measure   = measures.first
    comments  = measure.comments
    @date     = nil
     if !comments.nil?
      comments = comments.strip
        match = Regexp.new('^((\d+)/(\d+)) (... .. .... ..:....) to (... .. .... ..:....)').match(comments)
        if !match.nil?
           @date = sprintf(
              "%s to %s",
              DateTime.parse(match[4]).strftime('%m/%d/%Y'),
              DateTime.parse(match[5]).strftime('%m/%d/%Y')
          )
          @date0 = sprintf(
              "%s",
              DateTime.parse(match[4]).strftime('%m/%d/%Y')
          )
          
          @date1 = sprintf(
              "%s",
              DateTime.parse(match[5]).strftime('%m/%d/%Y')
          )
          @newDate = sprintf(
              "4 weeks ending %s",
              DateTime.parse(match[5]).strftime('%m/%d/%Y')
          )
          @numerator = sprintf(
              "%s",
              match[2]
          )
          @denominator = sprintf(
              "%s",
              match[3]
          )
          @fraction = sprintf(
              "%s out of %s",
              match[2],
              match[3]
          )
          @newFraction = sprintf(
              "Appraisal \rClosing Rate: %d%",
              measure.measure*100
          )
          @span = measure.measure*180
        end
      end
  end
  
  
  
  def trade_in_analyzed_report
    @page_style = "drill_down"

    has_agg_data = true;

    if (params["date_0"] == Date.today.strftime('%m/%d/%Y') && params["date_1"] == Date.today.strftime('%m/%d/%Y'))
        has_agg_data = false;
    end

    @results = run_trade_in_analyzed_report(
        session[:business_unit].id,
        params["date_0"],
        params["date_1"],
        params["sort"]
    )
  
    business_unit_id = @session[:business_unit].id
    @business_unit = @session[:business_unit].name
    measures = Measure.find_by_sql(<<-SQL
      SELECT CAST(#{business_unit_id} AS VARCHAR(2000)) + '_2_2_2' AS [id]
        ,#{business_unit_id} AS [business_unit_id]
        ,2 AS [period_id]
        ,2 AS [set_id]
        ,2 AS [metric_id]
        ,I.PercentAnalyzed AS [measure]
        ,(CAST(I.VehiclesAnalyzed AS VARCHAR(2000)) + '/' + CAST(I.TotalVehicles AS VARCHAR(2000)) + ', ' + CONVERT(VARCHAR(20), P.BeginDate, 100) + ' to ' + CONVERT(VARCHAR(20), P.EndDate, 100)) AS [comments]
        ,W.DateLoaded AS [date_created]
         FROM FLDW.dbo.InventoryTradeIns_A1 I
         JOIN FLDW.dbo.Period_D P ON I.PeriodID = P.PeriodID
         CROSS JOIN FLDW.dbo.WarehouseTables W
         WHERE I.BusinessUnitID = #{business_unit_id} AND
			   W.TableName = 'InventoryTradeIns_A1'
      SQL
    )
    if has_agg_data
      measure   = measures.first;
      comments  = measure.comments;
      @date_created = measure.date_created;
      @period     = nil
       if !comments.nil?
        comments = comments.strip
          match = Regexp.new('^((\d+)/(\d+),) (... .. .... ..:....) to (... .. .... ..:....)').match(comments)
          if !match.nil?
             @period = sprintf(
                "%s to %s",
                DateTime.parse(match[4]).strftime('%m/%d/%Y'),
                DateTime.parse(match[5]).strftime('%m/%d/%Y')
            )
            @date0 = sprintf(
                "%s",
                DateTime.parse(match[4]).strftime('%m/%d/%Y')
            )

            @date1 = sprintf(
                "%s",
                DateTime.parse(match[5]).strftime('%m/%d/%Y')
            )
            @newDate = sprintf(
                "4 weeks ending %s",
                DateTime.parse(match[5]).strftime('%m/%d/%Y')
            )
            @numerator = sprintf(
                "%s",
                match[2]
            )
            @denominator = sprintf(
                "%s",
                match[3]
            )
            @fraction = sprintf(
                "%s out of %s",
                match[2],
                match[3]
            )
            @newFraction = sprintf(
                "%d%",
                measure.measure*100
            )
          end
        end
    else
      @date_created = Date.today;
      @period = Date.today.strftime('%m/%d/%Y') + " to " + Date.today.strftime('%m/%d/%Y');
      @date0 = Date.today.strftime('%m/%d/%Y');
      @date1 = Date.today.strftime('%m/%d/%Y');
      @newDate = "4 weeks ending " + Date.today.strftime('%m/%d/%Y');
      @numerator = "0";
      @denominator = "0";
      @fraction = "0 out of 0";
      @newFraction = "0";
    end
  end   


  def trade_in_flips_report
    @page_style = "drill_down"
    @results = run_trade_in_flips_report(
        session[:business_unit].id,
        params["date_0"],
        params["date_1"],
        params["sort"]
    )
  
    business_unit_id = @session[:business_unit].id
    @business_unit = @session[:business_unit].name
    measures = Measure.find_by_sql(<<-SQL
        SELECT
          *
        FROM
          measures
        WHERE
            business_unit_id = #{business_unit_id}
        AND metric_id = 18
        AND period_id = 2
      SQL
    )
    measure     = measures.first
    @flipProfit = sprintf("%d", measure.measure)
    @flipTarget = 250
    comments    = measure.comments
    @flipPeriod     = nil
     if !comments.nil?
      comments = comments.strip
        match = Regexp.new('^((\d+) (.....),) (... .. .... ..:....) to (... .. .... ..:....)').match(comments)
        if !match.nil?
           @flipPeriod = sprintf(
              "%s to %s",
              DateTime.parse(match[4]).strftime('%m/%d/%Y'),
              DateTime.parse(match[5]).strftime('%m/%d/%Y')
          )
          @flipDate0 = sprintf(
              "%s",
              DateTime.parse(match[4]).strftime('%m/%d/%Y')
          )
          
          @flipDate1 = sprintf(
              "%s",
              DateTime.parse(match[5]).strftime('%m/%d/%Y')
          )
          @flipEndingDateString = sprintf(
              "4 weeks ending %s",
              DateTime.parse(match[5]).strftime('%m/%d/%Y')
          )
          @flipUnits = sprintf(
              "%s",
              match[2]
          )
          @flipMeasure = sprintf(
              "%d%",
              measure.measure
          )
        end
      end
    @flipTotal = (@flipProfit.to_i * @flipUnits.to_i)
  end   


end
