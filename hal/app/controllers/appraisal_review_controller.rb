
require 'hasappraisalreview'

class AppraisalReviewController < DynamicController
  
  before_filter HasAppraisalReviewFilter
  
  include ApplicationHelper
  
  layout "home"
  
  skip_before_filter CAS::Filter, :only => [:appraisal_review]
  
  #
  # AppraisalReview
  #
  
  def index
    # setup title and page css
    page_vars("Summary")
    # load appraisal reviews
    appraisal_reviews = load_appraisal_reviews(params[:is_high_mileage] == 'true', params[:days_back])
    # create proxy groups
    @proxy_component_collections = ProxyComponent::Collection.groups(
      :proxy_component_collection => AppraisalReviewProxyComponentCollection,
      :number_of_groups           => 8,
      :number_of_initial_records  => 8,
      :records                    => appraisal_reviews,
      :group_by_proc              => Proc.new { |record|
        index = nil
        [:rule_7, :rule_8,:rule_6, :rule_4, :rule_1, :rule_5, :rule_2, :rule_3 ].each_with_index do |method, idx|
          if record.attributes[method.id2name] == 1
            index = idx
            break
          end
        end
        index
      }
    )
    # get counts
    @number_of_appraisal_reviews = appraisal_reviews.length
    # has JDPower upgrade
    @show_jdpower = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 18}
    # render the page
    render :action => 'index', :layout => (params[:layout].nil? || params[:layout] != 'false')
  end
  
  def appraisal_review
    logger.warn("appraisal_review(): calling AppraisalReview.find()" )
    appraisal_reviews = AppraisalReview.find(
      :all,
      :conditions => ["appraisal_reviews.business_unit_id = ? AND appraisal_reviews.day_id = ? AND appraisal_reviews.appraisal_id IN (#{params[:appraisal_id].collect{|id| id.to_i}.join(',')})",
        session[:business_unit].id,
        params[:day_id].to_i
      ],
      :include => [:vehicle, :appraisal]
    )

    logger.warn("appraisal_review(): length:" + appraisal_reviews.length.to_s  );

    if (appraisal_reviews.length == 0)
      appraisal_reviews = []
    end

    logger.info("appraisal_review(): calling render: collection="  +  appraisal_reviews.to_s)
    render :partial => 'appraisal_review', :collection => appraisal_reviews, :locals => { :rule => params[:rule] }


  rescue StandardError => exception
    error(exception)
  end
  
  def detail
    page_vars("Detail")
    find_appraisal_review
    @appraisal = @appraisal_review.appraisal
    book_valuations = {}
    @appraisal_review.appraisal.appraisal_book_valuation_summaries.each do |summary|
      book_valuations[summary.book_category.book_id] = summary.book_valuation
    end
    @book_vehicle_summaries = Array::new
    @appraisal_review.appraisal.book_vehicles.each do |book_vehicle|
      @book_vehicle_summaries << BookVehicleSummary.new(@appraisal_review.appraisal.vehicle, book_vehicle, book_valuations[book_vehicle.book_id], session[:business_unit].id) if session[:business_unit].business_unit_preference.book_ids.include?(book_vehicle.book_id)
    end
    @appraisal_action = nil
    unless @appraisal_review.appraisal.appraisal_actions.empty?
      @appraisal_action = @appraisal_review.appraisal.appraisal_actions.last
    end
    @colors = %w{
      UNKNOWN BEIGE BLACK BLUE BROWN BURGUNDY GOLD GRAY GREEN
      MAROON ORANGE PEWTER PURPLE RED SILVER TAN TEAL WHITE YELLOW
    }
    # carfax  
    dealer_id = session[:business_unit].id
    user_name = ""
    user_name = session[:member].login unless session[:member].nil?
    @has_carfax_account = CarfaxHelper.has_account(dealer_id, user_name)
    if @has_carfax_account
      @can_purchase_carfax_report = CarfaxHelper.can_purchase_report(dealer_id, user_name)
      @carfax_report = CarfaxHelper.get_report(dealer_id, user_name, @appraisal.vehicle.vin)
      if !@carfax_report.nil?
        @has_carfax_report = DateTime.new < @carfax_report.expirationDate
        @carfax_report_type = @carfax_report.reportType
      else
        carfax_preference = CarfaxHelper.get_report_preference(dealer_id, user_name, Carfax::VehicleEntityType::Appraisal)
        if carfax_preference.nil?
          @carfax_report_type = 'BTC'
        else
          @carfax_report_type = carfax_preference.reportType
        end
      end
    else
      @can_purchase_carfax_report = false
      @has_carfax_report = false
    end
    # autocheck
    @has_autocheck_account = AutoCheckHelper.has_account(dealer_id, user_name)
    if @has_autocheck_account
      @can_purchase_autocheck_report = AutoCheckHelper.can_purchase_report(dealer_id, user_name)
      @autocheck_report = AutoCheckHelper.get_report(dealer_id, user_name, @appraisal.vehicle.vin)
    else
      @can_purchase_autocheck_report = false
    end
    # other
    @has_pingII = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 19}
    @has_tmv = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 20}
    @has_kbb = @book_vehicle_summaries.detect{|b| b.book.name == 'KBB' }
    render :action => 'detail', :layout => (params[:layout].nil? || params[:layout] != 'false')
  end
  
  def appraisal_summary_list
    # page title and css file
    page_vars("All Appraisals")
    # load the data
    appraisals = load_appraisals(params[:is_high_mileage] == 'true', params[:days_back])
    # create the proxy collection
    @proxy_component_collection = AppraisalSummaryProxyComponentCollection.new(1)
    # add the records to the proxy collection
    appraisals.each{|appraisal| @proxy_component_collection << appraisal}
    # allocate the initial records for immediate display
    @proxy_component_collection.allocate_initial_records(20)
    # proxy batch sizing
    @proxy_component_collection.allocate_proxy_records(20)
    # get counts
    @number_of_appraisals = appraisals.length
    # render the page
    render :action => 'appraisal_summary_list', :layout => (params[:layout].nil? || params[:layout] != 'false')
  end
  
  def appraisal_summary
    appraisals = Appraisal.find(
      :all,
      :conditions => ["appraisals.business_unit_id = ? AND appraisals.id IN (#{params[:appraisal_id].collect{|id| id.to_i}.join(',')})",
        session[:business_unit].id
      ],
      :include => [:vehicle,:appraisal_insight,:customer,:person]
    )
    render :partial => 'appraisal_summary', :collection => appraisals
  rescue StandardError => exception
    error(exception)
  end
  
  def already_traded_in
    find_appraisal_review
    AppraisalAction.transaction() do
      appraisal_action, success = * insert_or_update_appraisal_action_type(@appraisal_review.appraisal_id, 1)
      if success
        if @appraisal_review.update_attribute(:display, false)
          number_of_appraisal_reviews
          number_of_appraisal_reviews_for_rule(params[:rule])
        else
          record_errors(@appraisal_review)
        end
      else
        record_errors(appraisal_action)
      end
    end
    render :update do |page|
      remove_appraisal_review_from_rule_rjs(page, params[:rule])
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def delete_from_list
    find_appraisal_review
    unless @appraisal_review.nil?
      if @appraisal_review.update_attribute(:display, false)
       unless params[:rule].nil?
          number_of_appraisal_reviews       
          number_of_appraisal_reviews_for_rule(params[:rule])
        end
      else
        record_errors(@appraisal_review)
      end
    end
    if params[:return_to_summary].nil?
      render :update do |page|
        remove_appraisal_review_from_rule_rjs(page, params[:rule])
      end
    else
      render :update do |page|
        page << "document.location.href='/appraisal_review'"
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def update_appraisal_review_and_add_to_follow_up_list
    # set page title and css
    page_vars()
    # find the appraisal review
    find_appraisal_review
    # find the follow up list
    appraisal_review_follow_up_list = AppraisalReviewFollowUpList.find(
      :first,
      :conditions => ["business_unit_id = ? AND active = 1", session[:business_unit].id]
    )
    # create it if it was not there
    if appraisal_review_follow_up_list.nil?
      now = Time.new
      appraisal_review_follow_up_list = AppraisalReviewFollowUpList.new do |l|
        l.business_unit_id = session[:business_unit].id
        l.active = 1
        l.date_created = now
        l.last_modified = now
      end
      save_record!(appraisal_review_follow_up_list)
    end
    # get rid of non values from the request
    delete_non_values(params, [:appraisal_review_follow_up], [:notes], ['NOTES'])
    delete_non_values(params, [:appraisal_valuation], [:appraiser_name])
    # if a user entered customer offer is included in params - save it
    appraisal_valuations = @appraisal_review.appraisal.appraisal_valuations
    current_appraisal_value = (appraisal_valuations.empty? ? nil : appraisal_valuations.first.value)
    customer_offer_non_values = [current_appraisal_value]
    # create a new appraisal valuation (if needed)
    current_appraisal_value = _new_appraisal_valuation(current_appraisal_value)
    customer_offer_non_values << current_appraisal_value
    # create a new customer offer (if needed)
    _new_appraisal_offer(customer_offer_non_values)
    # handle the two modes of operation: add and update
    if params[:follow_up_mode] == 'true'
      # update: follow up notes
      if neither_nil_nor_empty_n(params, [:appraisal_review_follow_up, :notes])
        appraisal_review_follow_up = AppraisalReviewFollowUp.find(
          :first,
          :conditions => ["appraisal_id = ?", @appraisal_review.appraisal.id]
        )
        update_record!(appraisal_review_follow_up, {:notes => params[:appraisal_review_follow_up][:notes]})
      end
      # re-render tile
      render :update do |page|
        page.replace 'update_appraisal_review_and_add_to_follow_up_list', :partial => 'update_appraisal_review_and_add_to_follow_up_list', :object => nil, :locals => { :appraisal => @appraisal_review.appraisal, :appraisal_review => @appraisal_review }
      end
    else
      # add: new follow up list item
      appraisal_review_follow_up = AppraisalReviewFollowUp.new(params[:appraisal_review_follow_up])
      appraisal_review_follow_up.business_unit_id = session[:business_unit].id
      # save the objects to the db
      AppraisalReviewFollowUpList.transaction() do
        appraisal_review_follow_up.appraisal_review_follow_up_list_id = appraisal_review_follow_up_list.id
        save_record!(appraisal_review_follow_up) do
          @appraisal_review.display = false
          @appraisal_review.follow_up = true
          save_record!(@appraisal_review)
        end
      end
      if params[:page] == 'detail'
        render :update do |page|
          page << "document.location.href='/appraisal_review'"
        end
      else
        # summary page
        page_vars()
        number_of_appraisal_reviews
        number_of_appraisal_reviews_for_rule(params[:rule])
        render :update do |page|
          page.replace 'appraisal_review_follow_up_list_summary', :partial => 'appraisal_review_follow_up_list_summary', :object => nil, :locals => { :number_of_appraisal_review_follow_ups => @number_of_appraisal_review_follow_ups }
          remove_appraisal_review_from_rule_rjs(page, params[:rule])
        end
      end
    end
  rescue StandardError => e
    Log.error(e)
    render :update do |page|
      flash.now[:notice] = ['An error occurred that could not be handled', e].compact if (flash.now[:notice].nil? || flash.now[:notice].empty?)
      flash_notice_rjs(page,flash)
    end
  end
  
  def delete_appraisal_review_from_follow_up_list
    # get the appraisal review
    find_appraisal_review
    # save the objects to the db
    success = false
    if neither_nil_nor_empty_n(params, [:appraisal_review_follow_up, :id])
      AppraisalReviewFollowUp.transaction() do
        AppraisalReviewFollowUp.delete(params[:appraisal_review_follow_up][:id])
        @appraisal_review.display = true
        @appraisal_review.follow_up = false
        save_record(@appraisal_review) do
          success = true
        end
      end
    end
    # load the appraisal review follow up count
    page_vars()
    # redirect or refresh
    render :update do |page|
      if success
        if params[:return_to_summary].nil?
          remove_appraisal_review_from_follow_up_list_rjs(page)
        else
          if @number_of_appraisal_review_follow_ups == 0
            page << "document.location.href='/appraisal_review/'"
          else
            page << "document.location.href='/appraisal_review/appraisal_review_follow_up_list'"
          end
        end
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def start_a_new_appraisal_review_follow_up_list
    # find the list
    old_appraisal_review_follow_up_list = AppraisalReviewFollowUpList.find(
      :first,
      :conditions => ["business_unit_id = ? AND active = 1", session[:business_unit].id]
    )
    # create a new list
    now = Time.new
    appraisal_review_follow_up_list = AppraisalReviewFollowUpList.new do |l|
      l.business_unit_id = session[:business_unit].id
      l.active = 1
      l.date_created = now
      l.last_modified = now
    end
    AppraisalReviewFollowUpList.transaction() do
      # deactivate the old list
      unless old_appraisal_review_follow_up_list.nil?
        old_appraisal_review_follow_up_list.active = false
        old_appraisal_review_follow_up_list.last_modified = now
        unless old_appraisal_review_follow_up_list.save
          record_error(old_appraisal_review_follow_up_list)
        end
      end
      if appraisal_review_follow_up_list.save
        page_vars()
      else
        record_error(appraisal_review_follow_up_list)
      end
    end
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        page.replace 'appraisal_review_follow_up_list_summary', :partial => 'appraisal_review_follow_up_list_summary', :object => nil, :locals => { :number_of_appraisal_review_follow_ups => @number_of_appraisal_review_follow_ups }
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def appraisal_review_follow_up_list
    page_vars("Follow Up List")
    @appraisal_reviews = load_appraisal_reviews_in_follow_up_list()
    @number_of_appraisal_review_follow_ups = @appraisal_reviews.length
    render :action => 'appraisal_review_follow_up_list', :layout => (params[:layout].nil? || params[:layout] != 'false')
  end
  
  def appraisal_review_deal_lost
    find_appraisal_review
    AppraisalReview.transaction() do
      unless @appraisal_review.nil?
        if @appraisal_review.appraisal_review_follow_up.update_attribute(:appraisal_review_follow_up_action_id, 2)
          if @appraisal_review.update_attribute(:display, false)
            appraisal_action, success = * insert_or_update_appraisal_action_type(@appraisal_review.appraisal_id, 4)
            if success
              number_of_appraisal_review_follow_ups()
            else
              record_errors(appraisal_action)
            end
          else
            record_errors(@appraisal_review)
          end
        end
      end
    end
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        remove_appraisal_review_from_follow_up_list_rjs(page)
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def appraisal_review_deal_complete
    find_appraisal_review
    AppraisalReview.transaction() do
      unless @appraisal_review.nil?
        if @appraisal_review.appraisal_review_follow_up.update_attribute(:appraisal_review_follow_up_action_id, 1)
          if @appraisal_review.update_attribute(:display, false)
            appraisal_action, success = * insert_or_update_appraisal_action_type(@appraisal_review.appraisal_id, 1)
            if success
              number_of_appraisal_review_follow_ups()
            else
              record_errors(appraisal_action)
            end
          else
            record_errors(@appraisal_review)
          end
        end
      end
    end
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        remove_appraisal_review_from_follow_up_list_rjs(page)
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def appraisal_review_update
    # update the various objects
    Appraisal.transaction() do
      _update_appraisal_review_follow_up
      _update_appraisal
      _update_customer
      _new_appraisal_valuation
      _new_appraisal_offer
    end
    id = params[:appraisal_review][:id]
    update_id = params[:update_element]
    @appraisal_review = AppraisalReview.find(id)
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        page.replace_html update_id,:partial=>"appraisal_review_follow_up", :object=>nil, :locals=>{:appraisal_review=>@appraisal_review,:root=>update_id}
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  #
  # Appraisal
  #
  
  def _new_appraisal_offer(non_values=nil)
    # check they submitted a value for the customer offer
    if neither_nil_nor_empty_n(params, [:appraisal_offer, :customer_offer])
      customer_offer = params[:appraisal_offer][:customer_offer]
      unless customer_offer.empty?
        customer_offer = customer_offer.to_i
        # load or create an appraisal_offer
        appraisal_offer = nil
        appraisal_offer = AppraisalOffer.find(params[:appraisal_offer][:id]) if neither_nil_nor_empty_n(params,[:appraisal_offer,:id])
        if appraisal_offer.nil?
          appraisal_offer = AppraisalOffer.new do |a|
            a.appraisal_id = params[:appraisal_offer][:appraisal_id]
          end
        end
        # save the record if it passes 'validation' criteria
        if non_values.nil? || !non_values.include?(customer_offer)
          if appraisal_offer.customer_offer.nil? || appraisal_offer.customer_offer != customer_offer
            appraisal_offer.old_customer_offer = appraisal_offer.customer_offer
            appraisal_offer.customer_offer = customer_offer
            save_record!(appraisal_offer)
          end
        end
      end
    end
  end
  
  def appraisal_review_preference
    appraisal_review_preference = AppraisalReviewPreference.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
    @show_jdpower = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 18}
    render :partial => "appraisal_review_preference", :object => appraisal_review_preference
  end
  
  def update_appraisal_review_preference
    @appraisal_review_preference = AppraisalReviewPreference.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id])
    if !@appraisal_review_preference.nil?
      unless @appraisal_review_preference.update_attributes(params[:appraisal_review_preference][@appraisal_review_preference.id.to_s])
        record_errors(@appraisal_review_preference)
      end
    else
      flash.now[:notice] = 'Cannot find your appraisal preferences!'
    end
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        unless params[:reload].nil?
          page << "window.location.reload(false);"
        end
      else
        flash_notice_rjs(page,flash)
      end
    end
  end
  
  def update_appraisal
    _update_appraisal
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        page.replace_html 'appraisal', :partial => 'appraisal', :object => @appraisal
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def update_appraisal_action
    insert_or_update_appraisal_action_type(
      params["appraisal_action"]["appraisal_id"],
      params["appraisal_action"]["appraisal_action_type_id"],
      params["appraisal_action"]
    )
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  rescue StandardError => exception
    error(exception)
  end
  
  def update_customer
    _update_customer
    render :update do |page|
      if flash.now[:notice].nil? || flash.now[:notice].empty?
        page.replace_html 'customer', :partial => 'customer', :object => @customer
      else
        flash_notice_rjs(page,flash)
      end
    end
  rescue StandardError => exception
    error(exception)
  end
  
  #
  # Reports
  #
  
  def trade_analyzer_report
    proxy_component = TradeAnalyzerReportProxyComponent.new(params.update(:business_unit_id => session[:business_unit].id))
    proxy_component.render_component(self)
  end
  
  def insights
    proxy_component = InsightsProxyComponent.new(params.update(:business_unit_id => session[:business_unit].id))
    proxy_component.render_component(self)
  end
  
  def in_group_redistribution_report
    proxy_component = InGroupRedistributionReportProxyComponent.new(params.update(:business_unit_id => session[:business_unit].id))
    proxy_component.render_component(self)
  end
  
  def naaa_report
    Vehicle.find(params[:vehicle_id]).update_attribute(:vic, params[:vic]) if neither_nil_nor_empty(params,:vehicle_id) && neither_nil_nor_empty(params,:vic)
    proxy_component = NAAAReportProxyComponent.new(params.update(:business_unit_id => session[:business_unit].id))
    proxy_component.render_component(self)
  end
  
  def purchase_carfax_report
  	@appraisal = Appraisal.find(params[:appraisal][:id])
  	@carfax_report = CarfaxHelper.purchase_report(session[:business_unit].id, session[:member].login, @appraisal.vehicle.vin, params[:reportType], params[:cccFlagFromTile].nil?)  	
    if !@carfax_report.nil?
      @carfax_report_type = @carfax_report.reportType
    end
    render :partial => "carfax_form", :locals => { 
	    :has_carfax_account => true, 
	    :can_purchase_carfax_report => true,
	    :has_carfax_report => true, 
	    :carfax_report => @carfax_report,
	    :carfax_report_type => @carfax_report_type,
	    :vin => @appraisal.vehicle.vin
    }
  rescue StandardError => exception
  	error(exception)
  end
  
  private
  
  def page_vars(title = "")
    @page_title = "Make-a-Deal: #{title}"
    @page_style = "potential_deal"
    number_of_appraisal_review_follow_ups()
  end
  
  def number_of_appraisal_review_follow_ups
    @number_of_appraisal_review_follow_ups = AppraisalReviewFollowUp.count_by_sql(<<-SQL
        SELECT  count(*)
		FROM    appraisal_review_follow_ups F
		JOIN    appraisal_review_follow_up_lists L ON L.id = F.appraisal_review_follow_up_list_id
    JOIN    appraisals A on F.appraisal_id = A.id AND F.business_unit_id = A.business_unit_id
		WHERE   F.business_unit_id = #{session[:business_unit].id}
		AND     L.active = 1
		AND     F.appraisal_review_follow_up_action_id IS NULL
      SQL
    )
  end
  
  def number_of_appraisal_reviews
    @number_of_appraisal_reviews = AppraisalReview.count_by_sql(<<-SQL
        SELECT  count(*)
		FROM    appraisal_reviews a
		JOIN    appraisal_review_preferences p ON a.business_unit_id = p.business_unit_id
		WHERE   a.business_unit_id = #{session[:business_unit].id}
		AND     a.display = 1
		AND     a.has_enabled_rule_matches = 1
		AND     a.day_id = #{Date.today.strftime('%Y%m%d').to_i}
		AND     datediff(dd,imt.dbo.todate(a.appraisal_date),imt.dbo.todate(getDate())) <= p.days_back
      SQL
    )
  end
  
  def number_of_alternate_appraisal_reviews(is_high_mileage, high_mileage_boundary)
    @number_of_alternate_appraisal_reviews = AppraisalReview.count_by_sql(<<-SQL
        SELECT  count(*)
		FROM    appraisal_reviews a
		JOIN    appraisal_review_preferences p ON a.business_unit_id = p.business_unit_id
		WHERE   a.business_unit_id = #{session[:business_unit].id}
		AND     a.display = 1
		AND     a.has_enabled_rule_matches = 1
		AND     a.day_id = #{Date.today.strftime('%Y%m%d').to_i}
		AND     datediff(dd,imt.dbo.todate(a.appraisal_date),imt.dbo.todate(getDate())) <= p.days_back
		AND     a.vehicle_mileage #{is_high_mileage ? '<' : '>='} #{high_mileage_boundary}
      SQL
    )
  end
  
  def number_of_alternate_appraisals(is_high_mileage, high_mileage_boundary)
    @number_of_alternate_appraisals = Appraisal.count_by_sql(<<-SQL
      SELECT  COUNT(*)
      FROM    appraisals a
      JOIN    appraisal_review_preferences p ON a.business_unit_id = p.business_unit_id
      WHERE   a.business_unit_id = #{session[:business_unit].id}
      AND     DATEDIFF(DD, a.date_created, GETDATE()) <= p.days_back
      AND     a.vehicle_mileage #{is_high_mileage ? '<' : '>='} #{high_mileage_boundary}
      SQL
    )
  end
  
  def number_of_appraisal_reviews_for_rule(rule_number)
    @number_of_appraisal_reviews_for_rule = AppraisalReview.count_by_sql(<<-SQL
        SELECT  count(*)
		FROM    appraisal_reviews a
		JOIN    appraisal_review_preferences p ON a.business_unit_id = p.business_unit_id
		WHERE   a.business_unit_id = #{session[:business_unit].id}
		AND     a.display = 1
		AND     a.has_enabled_rule_matches = 1
		AND     a.day_id = #{Date.today.strftime('%Y%m%d').to_i}
		AND     a.rule_#{rule_number} = 1
		AND     datediff(dd,imt.dbo.todate(a.appraisal_date),imt.dbo.todate(getDate())) <= p.days_back
      SQL
    )
  end
  
  def find_appraisal_review
    @appraisal_review = AppraisalReview.find(
      :first,
      :conditions => ["appraisal_reviews.business_unit_id = ? AND appraisal_reviews.appraisal_id = ? AND appraisal_reviews.day_id = ?",
        session[:business_unit].id,
        params[:appraisal_id],
        params[:day_id]
      ],
      :include => [:appraisal, :book, :book_category, :vehicle, :vehicle_inventory_light]
    )
  end
  
  # returns the appraisaed_value
  def _new_appraisal_valuation(current_appraised_value=nil)
    # delete non-values
    delete_non_values(params, [:appraisal_valuation], [:appraiser_name])
    # default return value
    r = current_appraised_value
    # perform lots of sanity checks
    if neither_nil_nor_empty_n(params, [:appraisal_valuation, :value])
      appraised_value = params[:appraisal_valuation][:value].to_s.gsub(/[$,]/, "")
      unless appraised_value.empty?
        appraised_value = appraised_value.to_i
        # if we have a different appraised value then save it
        if current_appraised_value.nil? || current_appraised_value != appraised_value
          params[:appraisal_valuation][:value] = appraised_value
          sequence_number = 1
          appraisal_id = params[:appraisal_valuation][:appraisal_id]
          max_sequence = ActiveRecord::Base.connection.select_all("SELECT MAX(sequence_number) sequence_number FROM appraisal_valuations WHERE appraisal_id = #{appraisal_id}").first
          if !max_sequence.nil?
            sequence_number = max_sequence['sequence_number'].to_i+1
          end
          now = Time.new
          AppraisalValuation.transaction() do
            appraisal_valuation = AppraisalValuation.new do |v|
              v.appraisal_id    = appraisal_id
              v.sequence_number = sequence_number
              v.date_created    = now.strftime('%Y-%m-%d %H:%M:%S')
              v.value           = params["appraisal_valuation"]["value"]
              v.appraiser_name  = params["appraisal_valuation"]["appraiser_name"]
            end
            save_record!(appraisal_valuation) do
              r = appraised_value
            end
          end
        end
      end
    end
    r
  end
  
  def _update_appraisal 
    if neither_nil_nor_empty_n(params, [:appraisal, :id])
      @appraisal = Appraisal.find(params[:appraisal][:id])
      update_record!(@appraisal, params[:appraisal])
    end
  end
  
  def _update_appraisal_review_follow_up
    delete_non_values(params, [:appraisal_review_follow_up], [:notes], ['NOTES'])
    if neither_nil_nor_empty_n(params, [:appraisal_review_follow_up, :id])
      @appraisal_review_follow_up = AppraisalReviewFollowUp.find(params[:appraisal_review_follow_up][:id])
      update_record!(@appraisal_review_follow_up, params[:appraisal_review_follow_up])
    end
  end
  
  def _update_customer
    delete_non_values(params, [:customer], [:email,:first_name,:last_name,:telephone_number,:gender])
    if neither_nil_nor_empty_n(params, [:customer, :id])
      @customer = Customer.find(params[:customer][:id])
      @customer.first_name = nil if @customer.first_name == ''
      @customer.last_name = nil if @customer.last_name == ''
      @customer.email = nil if @customer.email == ''
      @customer.gender = nil if @customer.gender == ''
      @customer.telephone_number = nil if @customer.telephone_number == ''
      update_record!(@customer, params[:customer])
    else
      @customer = Customer.new(params[:customer])
      save_record!(@customer)
    end
  end
  
  def insert_or_update_appraisal_action_type(appraisal_id, appraisal_action_type_id, extras = nil)
    appraisal_action = AppraisalAction.find(:first, :conditions => ["appraisal_id = ?", appraisal_id])
    if appraisal_action.nil?
      appraisal_action = AppraisalAction.new do |a|
        a.appraisal_id = appraisal_id
        a.appraisal_action_type_id = appraisal_action_type_id
        a.date_created = Time.now
      end
    else
      appraisal_action.appraisal_action_type_id = appraisal_action_type_id
    end
    unless extras.nil?
      appraisal_action.wholesale_price = extras["wholesale_price"] unless extras["wholesale_price"].nil?
      appraisal_action.estimated_reconditioning_cost = extras["estimated_reconditioning_cost"] unless extras["estimated_reconditioning_cost"].nil?
      appraisal_action.conditioning_description = extras["conditioning_description"] unless extras["conditioning_description"].nil?
    end
    return [appraisal_action, appraisal_action.save]
  end
  
  private
  
  # these methods are used by the PDF generator script too - do not refactor them
  
  def load_appraisal_reviews(is_high_mileage, days_back=nil)
    days_back = session[:business_unit].appraisal_review_preference(true).days_back if days_back.nil?
    high_mileage_enabled = session[:business_unit].appraisal_review_preference.high_mileage_enabled
    high_mileage_red_light = session[:business_unit].business_unit_preference(true).high_mileage_red_light
    number_of_alternate_appraisal_reviews(is_high_mileage, high_mileage_red_light) if high_mileage_enabled
    AppraisalReview.find(
      :all,
      :conditions => ["appraisal_reviews.business_unit_id = ? AND appraisal_reviews.day_id = ? AND appraisal_reviews.display = 1 AND appraisal_reviews.has_enabled_rule_matches = 1 AND DATEDIFF(DD, appraisal_reviews.appraisal_date, GETDATE()) <= ? AND appraisal_reviews.vehicle_mileage #{high_mileage_enabled ? (is_high_mileage ? '>=' : '<') : '<'} ?",
        session[:business_unit].id,
        Date.today.strftime('%Y%m%d').to_i,
        days_back,
        high_mileage_enabled ? high_mileage_red_light : 1000000
      ],
      :order => "rule_7 DESC, rule_8 DESC, rule_6 DESC, rule_4 DESC, rule_1 DESC, rule_5 DESC, rule_2 DESC, rule_3 DESC"
    )
  end
  
  def load_appraisal_reviews_in_follow_up_list()
    # appraisal reviews may not be on the summary page but on the follow up list
    AppraisalReview.find_by_sql(<<-SQL
        SELECT
        		ar.*
        FROM
        		dbo.appraisal_reviews ar
        JOIN	dbo.appraisal_review_follow_ups f ON f.appraisal_id = ar.appraisal_id AND f.day_id = ar.day_id AND f.business_unit_id = ar.business_unit_id
        JOIN	dbo.appraisal_review_follow_up_lists l ON f.appraisal_review_follow_up_list_id = l.id
        JOIN  dbo.appraisals a on f.appraisal_id = a.id AND f.business_unit_id = a.business_unit_id
        WHERE
        		ar.business_unit_id = #{session[:business_unit].id}
        AND		ar.follow_up = 1
        AND		l.active = 1
        AND		f.appraisal_review_follow_up_action_id IS NULL
        ORDER
        BY	  rule_7 DESC, rule_8 DESC, rule_6 DESC, rule_4 DESC, rule_5 DESC, rule_1 DESC, rule_2 DESC, rule_3 DESC
      SQL
    )
  end
  
  def load_appraisals(is_high_mileage, days_back=nil)
    days_back = session[:business_unit].appraisal_review_preference(true).days_back if days_back.nil?
    high_mileage_enabled = session[:business_unit].appraisal_review_preference.high_mileage_enabled
    high_mileage_red_light = session[:business_unit].business_unit_preference(true).high_mileage_red_light
    number_of_alternate_appraisals(is_high_mileage, high_mileage_red_light) if high_mileage_enabled
    Appraisal.find_by_sql(<<-SQL
      SELECT  a.*
      FROM    appraisals a
      JOIN    (SELECT MAX(id) id, appraisal_id FROM appraisal_actions GROUP BY appraisal_id) max_aa ON max_aa.appraisal_id = a.id
      JOIN    appraisal_actions aa on aa.id = max_aa.id and aa.appraisal_id = a.id
      JOIN    appraisal_insights ai on ai.appraisal_id = a.id
      LEFT JOIN (SELECT appraisal_id FROM appraisal_reviews WHERE day_id = #{Date.today.strftime('%Y%m%d')} AND display = 1 AND has_enabled_rule_matches = 1) ar on ar.appraisal_id = a.id
      WHERE   a.business_unit_id = #{session[:business_unit].id}
      AND     aa.appraisal_action_type_id IN (4,5)
      AND     DATEDIFF(DD, a.date_created, GETDATE()) <= #{days_back}
      AND     a.vehicle_mileage #{high_mileage_enabled ? (is_high_mileage ? '>=' : '<') : '<'} #{high_mileage_enabled ? high_mileage_red_light : 1000000}
      ORDER
      BY      CASE WHEN ar.appraisal_id IS NOT NULL THEN 1 ELSE 0 END DESC,
              ai.vehicle_light_id DESC,
              DATEDIFF(DD, a.date_created, GETDATE()) ASC
    SQL
    )
  end
  
  def error(exception)
    if flash.now[:notice].nil?
      flash.now[:notice] = [exception.message]
    end
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
end
