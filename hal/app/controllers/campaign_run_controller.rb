class CampaignRunController < DynamicController
  
  include ApplicationHelper
  include CampaignRunHelper
  
  layout "home"
  
  # refresh the campaign run listing
  def campaign_runs
    page_vars
    render :partial => 'campaign_runs', :locals => { :inventory => @inventory, :planning_action => @planning_action, :input_type => params[:input_type] || 'radio', :input_object => params[:input_object], :input_method => params[:input_method], :html_options => params[:html_options] || {} }
  end
  
  # list all the running campaigns
  def index
    page_vars
    load_campaigns
  end
  
  # open a campaign run; if the campaign run does not exist then create it.
  def open
    page_vars
    # take the command from the request hash
    campaign_id = params[:campaign_id]
    campaign_run_id = params[:campaign_run_id]
    # figure out what we're doing
    if campaign_id.nil? or campaign_id == "0"
      @campaign_run, @vehicle_list = nil,nil
    else
      @campaign_run, @vehicle_list = * create_campaign_run_for_campaign(campaign_id)
    end
    unless @vehicle_list.nil?
      @inventory = load_inventory(@vehicle_list.id)
    end
  end
  
  # campaign run search result. this is a standard callback method used by pages which
  # utilise the search controller component. this is seperate from the search controller
  # as each page will want to display its search results differently.
  def search_result
    campaign_run = CampaignRun.find(params[:campaign_run][:id])
    search_result = SearchResult.find(params[:search_result][:id])
    search_criterion = SearchCriterion.find(params[:search_criterion][:id])
    inventory = load_inventory(search_result.vehicle_list_id, campaign_run.vehicle_list_id)
    render :update do |page|
      page.replace_html 'SearchResultContainer', :partial => 'search_result', :object => search_result, :locals => { :campaign_run => campaign_run, :search_criterion => search_criterion, :inventory => inventory }
    end
  end
  
  # add a vehicle from the search results into the campaign run. a vehicle cannot appear
  # in both the campaign run and the search results so we remove the vehicle from the search
  # results.
  def add_to_campaign_run
    campaign_run = CampaignRun.find(params[:campaign_run_id], :include => :vehicle_list)
    search_result = SearchResult.find(params[:search_result_id], :include => :vehicle_list)
    vehicle_list_item = search_result.vehicle_list.vehicle_list_items.detect{|item| item.vehicle_id == params[:vehicle_id].to_i}
    unless vehicle_list_item.nil?
      vehicle_list_item.destroy
      campaign_run.vehicle_list.vehicle_list_items << VehicleListItem.new do |item|
        item.vehicle_id = vehicle_list_item.vehicle_id
      end
    end
    inventory = Inventory.find(params[:inventory_id], :include => :inventory_planning_tasks)
    render :update do |page|
      page.remove "inventory_#{inventory.id}"
      page.replace 'vehicle_list_summary', :partial => 'vehicle_list_summary', :object => nil, :locals => { :campaign_run => campaign_run, :number_of_vehicles => campaign_run.vehicle_list.vehicle_list_items.length }
      page.insert_html :after, 'vehicle_list_summary', :partial => 'inventory', :object => inventory, :locals => { :search_result => nil, :campaign_run => campaign_run }
    end
  end

  # remove a vehicle from the campaign run. since we do not store the search result set which
  # we took the vehicle from it does not get added back to the search result set.
  def remove_from_campaign_run
    campaign_run = CampaignRun.find(params[:campaign_run_id], :include => :vehicle_list)
    vehicle_list_item = campaign_run.vehicle_list.vehicle_list_items.detect{|item| item.vehicle_id == params[:vehicle_id].to_i}
    vehicle_list_item.destroy
    render :update do |page|
      page.remove "inventory_#{params[:inventory_id]}"
      page.replace 'vehicle_list_summary', :partial => 'vehicle_list_summary', :object => nil, :locals => { :campaign_run => campaign_run, :number_of_vehicles => campaign_run.vehicle_list.vehicle_list_items.length }
    end
  end
  
  def show_planning_space
    # see show_planning_space RJS
  end
  
  def hide_planning_space
    # see hide_planning_space RJS
  end
  
  def campaign_list
    load_campaigns
    render :partial => 'campaign_list',  :locals => { :campaigns => @campaigns, :target_campaign_id => params[:campaign_id] }
  end
  
  def campaign_run_list
    load_campaign_runs(params[:campaign_id])
     render :update do |page|
       page.insert_html :after, "select_campaign", :partial => 'campaign_run_select', :locals => { :campaign_runs => @campaign_runs }
     end
  end
  
  def load_vehicle_list
    if neither_nil_nor_empty(params,:campaign_id) && neither_nil_nor_empty(params,:target_campaign_id)
      # destination campaign_run
      dst_campaign_run = CampaignRun.find(
        :first,
        :conditions => ["campaign_id = ?", params[:target_campaign_id]],
        :include => :vehicle_list
      )
      # source campaign_run
      src_campaign_run = CampaignRun.find(
        :first,
        :conditions => ["campaign_id = ?", params[:campaign_id]],
        :include => :vehicle_list
      )
      inventory = load_inventory(src_campaign_run.vehicle_list_id, dst_campaign_run.vehicle_list_id)
      # copy from src to dst
      inventory.each do |vehicle_item|
        dst_campaign_run.vehicle_list.vehicle_list_items << VehicleListItem.new do |item|
          item.vehicle_id = vehicle_item.vehicle_id
        end
      end
      # get the updated list in the correct format for display
      inventory = load_inventory(dst_campaign_run.vehicle_list_id)
      # update the page with some RJS
      render :update do |page|
        page.replace_html 'CampaignRunVehicleListContainer', :partial => 'vehicle_list', :object => nil, :locals => { :inventory => inventory, :campaign_run => dst_campaign_run }
      end
    end
  end
  
  private
  
  # layout variables
  def page_vars
    @page_title = "Campaign Runs"
    @page_style = "campaign"
  end
  
  # load the running campaigns for the business unit
  def load_campaigns
    @campaigns = Campaign.find(
      :all,
      :conditions => ["business_unit_id = ?", session[:business_unit].id]
    )
  end
  
  def load_inventory(vehicle_list_id, not_in_vehicle_list_id=nil)
    if not_in_vehicle_list_id.nil?
      ids = ActiveRecord::Base.connection.select_all(<<-SQL
          SELECT    i.vehicle_id
          FROM      vehicle_list_items i
          WHERE     i.vehicle_list_id = #{vehicle_list_id}
          ORDER BY  i.position
        SQL
      ).collect{|row| row['vehicle_id']}.join(',')
    else
      ids = ActiveRecord::Base.connection.select_all(<<-SQL
          SELECT    i.vehicle_id
          FROM      vehicle_list_items i
          WHERE     i.vehicle_list_id = #{vehicle_list_id}
          AND       i.vehicle_id NOT IN (SELECT x.vehicle_id FROM vehicle_list_items x WHERE x.vehicle_list_id = #{not_in_vehicle_list_id})
          ORDER BY  i.position
        SQL
      ).collect{|row| row['vehicle_id']}.join(',')
    end
    if ids.length < 1
      inventory = []
    else
       inventory = Inventory.find(
         :all,
         :conditions => "inventory.active = 1 AND inventory.inventory_type_id = 2 AND inventory.vehicle_id IN (#{ids})",
         :order      => "inventory.inventory_received_date",
         :include    => [:vehicle,:inventory_planning_tasks,:inventory_book_valuation_summaries]
        )
    end
    return inventory
  end
  
end
