class CredentialController < DynamicController
  
  def open
    credential = Credential.find(:first, :conditions => ["member_id = ? and credential_authority_id = ?", session[:member].id, params[:credential_authority_id]])
    render :update do |page|
      page.replace_html 'CredentialManagement', :partial => 'credential', :object => credential, :locals => { :credential_authority_id => params[:credential_authority_id] }
    end
  end
  
  def save
    if params[:credential][:id].nil?
      # its all completely new
      Credential.transaction() do
        credential = Credential.new(params[:credential])
        credential.member_id = session[:member].id
        unless credential.save
          record_errors(credential)
        end
      end
    else
      # updating something already saved
      Credential.transaction() do
        credential = Credential.find(params[:credential][:id])
        credential.username = params[:credential][:username]
        credential.password = params[:credential][:password]
        unless credential.save
          record_errors(credential)
        end
      end
    end
    if flash.now[:notice].nil? || flash.now[:notice].empty?
      close
    else
      render :update do |page|
        page.replace 'CredentialManagerErrors', "<div id='CredentialManagerErrors'>#{flash[:notice]}</div>"
        page.show 'CredentialManagerErrors'
        page.visual_effect :highlight, 'CredentialManagerErrors'
      end
    end
  end
  
  def close
    render :update do |page|
      page << "RedBox.close();"
      page.select("*[class=\"credential_authority_#{params[:credential_authority_id]}\"]").each do |value,index|
        page << "eval($(value).getAttribute('hal:refresh'));"
      end
    end
  end
  
end
