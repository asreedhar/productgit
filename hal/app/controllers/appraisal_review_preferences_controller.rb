
require 'hasappraisalreview'

class AppraisalReviewPreferencesController < DynamicController
  
  before_filter HasAppraisalReviewFilter
  
  layout "preference"
  
  def preferences
    page_vars
    @appraisal_review_preference = AppraisalReviewPreference.find(
      :first,
      :conditions => ["business_unit_id = ?", session[:business_unit].id]
    )
    # JDPower upgrade
    @show_jdpower = session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 18}
  end
  
  def bookout_preferences
    page_vars
    @vehicle_category = VehicleCategory.find(params[:vehicle_category_id])
    @appraisal_review_bookout_preferences = AppraisalReviewBookoutPreference.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              appraisal_review_bookout_preferences p
        JOIN  vehicle_makes m ON m.id = p.vehicle_make_id
        WHERE
              m.vehicle_category_id = #{params[:vehicle_category_id]}
        AND   p.business_unit_id = #{session[:business_unit].id}
        ORDER BY
              m.name
      SQL
    )
  end
  
  def update_preferences
    AppraisalReviewPreference.transaction() do
      record_errors(AppraisalReviewPreference.update(
        params[:appraisal_review_preference].keys,
        params[:appraisal_review_preference].values
      ))
    end
    refresh
    success
  rescue StandardError => exception
    error(exception)
  end
  
  def update_bookout_preferences
    AppraisalReviewPreference.transaction() do
      record_errors(AppraisalReviewBookoutPreference.update(
        params[:appraisal_review_bookout_preference].keys,
        params[:appraisal_review_bookout_preference].values
      ))
    end
    refresh
    success
  rescue StandardError => exception
    error(exception)
  end
  
  private
  
  def refresh
    if flash.now[:notice].nil? || flash.now[:notice].empty?
      AppraisalReviewPreference.transaction() do
        AppraisalReviewPreference.connection().execute(
          "EXECUTE LoadAppraisalReviews #{session[:business_unit].id}",
          "LoadAppraisalReviews"
        )
      end
      flash.now[:notice] = 'Update Successful'
    end
  end
  
  def success
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
  def error(exception)
    if flash.now[:notice].nil?
      flash.now[:notice] = 'An error occurred that could not be handled: ' + exception
    end
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
  def page_vars
    @page_title = "Preferences - Make a Deal"
    @page_style = "preference"
  end
  
end
