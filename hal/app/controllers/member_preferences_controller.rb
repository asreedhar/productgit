class MemberPreferencesController < DynamicController

  layout "preference"
  
  def insights
    page_vars
    load_member_insight_type_preferences
  end
  
  def save_insight_preferences
    MemberInsightTypePreference.transaction() do
      record_errors(MemberInsightTypePreference.update(
        params[:member_insight_type_preference].keys,
        params[:member_insight_type_preference].values
      ))
    end
    flash.now[:notice] = "Update Successful" if flash.now[:notice].nil?
    success
  rescue StandardError => exception
    error(exception)
  end
  
  private
  
  def page_vars
    @page_title = "Preferences - My Configuration"
    @page_style = "preference"
  end
  
  def load_member_insight_type_preferences
    @member_insight_type_preferences = MemberInsightTypePreference.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              member_insight_type_preferences p
        JOIN  insight_types t on t.id = p.insight_type_id
        WHERE
              p.business_unit_id = #{session[:business_unit].id}
        AND   p.member_id = #{session[:member].id}
        ORDER BY
              t.id
      SQL
    )
    if @member_insight_type_preferences.nil? || @member_insight_type_preferences.empty?
      insight_types = InsightType.find(
        :all,
        :conditions => ["insight_category_id = 2"],
        :order => "id"
      )
      insight_types.each do |insight_type|
        @member_insight_type_preferences << MemberInsightTypePreference.new do |p|
          p.business_unit_id = session[:business_unit].id
          p.member_id = session[:member].id
          p.insight_type = insight_type
          p.active = true
        end
      end
      # save as it allows us to submit back an array indexed by pk
      MemberInsightTypePreference.transaction() do
        @member_insight_type_preferences.each do |p|
          unless p.save
            record_errors(p)
          end
        end
      end
    end
  rescue StandardError => exception
    if flash.now[:notice].nil?
      flash.now[:notice] = 'An error occurred that could not be handled: ' + exception
    end
  end
  
  def success
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
  
  def error(exception)
    if flash.now[:notice].nil?
      flash.now[:notice] = 'An error occurred that could not be handled: ' + exception
    end
    render :update do |page|
      flash_notice_rjs(page,flash)
    end
  end
    
end
