class ReturnController < DynamicController
  def index
    redirect_to :controller => session[:return_controller]
  end
end
