
require 'hasdealer'

class LoginController < DynamicController

  layout "login"
  
  skip_before_filter HasDealerFilter
  
  skip_before_filter CAS::Filter, :only => [:exit]
  
  @@domain = "www.firstlook.biz"
  
  @@development = false
  
  # FIXME: Remove the index from the login exceptions to stop username/password fishing
  def index
    @page_title = "Login"
    @page_style = "login"
    login = session[:casfilteruser]
    #login = "admin"
    if login.nil?
      login_error(0)
      return
    else
      member = Member.find_by_login(login);
      if member.nil?
        login_error(1)
        return
      elsif member.login_status == 1
          redirect_to :action => :redirect_to_external_url, :external_url => '/IMT/LoginAction.go'
          return
      else       
        session[:member] = member
        cookies['ts_username'] = member.login.gsub /\s|\[|\]|\{|\}|\(|\)|\=|\,|\"|\?|\\|\/|@|:|;/,''
        @business_units = business_units_with_upgrade(member.id)
        if @business_units.length == 1
          session[:business_unit] = @business_units[0]
          session[:can_exit_store] = false
          add_dealer_cookies session[:business_unit]
          new_hal_session
          set_system_software_component_state(session[:business_unit])
          if session[:after_login].nil?
            redirect_to :controller => 'home', :action => 'index', :only_path => true
          else
            after_login = session[:after_login]
            session[:after_login] = nil
            redirect_to after_login.update(:only_path => true)
          end
          return
        else
          session[:can_exit_store] = true
          if member.member_type_id == 1
            @business_units = all_business_units_with_upgrade
            if (@business_units.nil? || @business_units.empty?)
              login_error(2)
              return
            end
          else
            if (@business_units.nil? || @business_units.empty?)
              domain = configured_host(request)
              redirect_to "https://#{domain}/IMT/"
              return
            else
              if member.member_type_id == 2 && member.group_home_page_id == 2
                preference = @business_units.first.parent.business_unit_group_preference
                if !preference.nil? && preference.include_performance_management_center
                  pmc_uri = URI.parse(PMC_URL)
                  pmc_uri.host = configured_host(request)
                  redirect_to PMC_URL
                  return
                end
              end
            end
          end
          render
          return
        end
      end
    end
    login_error(3)
  end
  
  def select_business_unit
    member = session[:member]
    unless member.nil?
      business_unit = BusinessUnit.find(params[:id])
      if !business_unit.nil?
        if (member.member_type_id == 1 || member.business_units.include?(business_unit))
          session[:business_unit] = business_unit
          session[:search_and_aquisition_report] = nil
          add_dealer_cookies session[:business_unit]
          new_hal_session
          set_system_software_component_state(session[:business_unit])
          if session[:after_login].nil?
            redirect_to :controller => 'home', :action => 'index', :only_path => true
          else
            after_login = session[:after_login]
            session[:after_login] = nil
            redirect_to after_login.update(:only_path => true)
          end
          return
        end
      end
    end
    login_error(4)
  end
  
  def redirect_to_group_pmr
    business_units = business_units_with_upgrade(session[:member].id)
    add_dealer_cookies business_units.first
    redirect_to :action => :redirect_to_external_url, :external_url => '/IMT/ReportCenterRedirectionAction.go', :dealerGroupId => business_units.first.parent.id
  end
  
  def remote_entry
    session[:remote_entry] = true
    login = session[:casfilteruser]
    state = SoftwareSystemComponentState.find_by_sql(<<-SQL
        SELECT
              S.*
        FROM
              software_system_component_states S
        JOIN  software_system_components C ON S.software_system_component_id = C.id
        JOIN  members M ON S.authorized_member_id = M.id
        WHERE
              M.login = '#{login}'
        AND   C.token = 'DEALER_SYSTEM_COMPONENT'
      SQL
    ).first
    controller_name = nil
    if state.nil? || state.dealer.nil?
      controller_name = 'login'
    else
      session[:member] = state.authorized_member
      session[:business_unit] = state.dealer
      add_dealer_cookies state.dealer
      new_hal_session
      controller_name = 'home'
    end
    redirect_to :controller => controller_name, :action => 'index', :only_path => true
  end
  
  def set_system_software_component_state(dealer)
    login = session[:casfilteruser]
    state = SoftwareSystemComponentState.find_by_sql(<<-SQL
        SELECT
              S.*
        FROM
              software_system_component_states S
        JOIN  software_system_components C ON S.software_system_component_id = C.id
        JOIN  members M ON S.authorized_member_id = M.id
        WHERE
              M.login = '#{login}'
        AND   C.token = 'DEALER_SYSTEM_COMPONENT'
      SQL
    ).first
    if state.nil?
      state = SoftwareSystemComponentState.new do |s|
        s.software_system_component = SoftwareSystemComponent.find_by_token('DEALER_SYSTEM_COMPONENT');
        s.authorized_member = Member.find_by_login(login);
      end
	  end
    state.dealer = dealer;
    state.dealer_group = dealer.parent;
    state.member = nil;
    unless state.save
      record_errors(state)
    end
  end
  
  def business_units_with_upgrade(member_id)
    business_units = BusinessUnit.find_by_sql(<<-SQL
        SELECT
              B.*
        FROM
              business_units B
        JOIN  members_business_units MB ON MB.business_unit_id = B.id
        JOIN  business_units_upgrades BU ON BU.business_unit_id = B.id
        WHERE
              BU.upgrade_id = 15
        AND   MB.member_id = #{member_id}
        ORDER BY
              B.name
      SQL
    )
  end
  
  def all_business_units_with_upgrade
    if @@development == true
      return BusinessUnit.find_by_sql(<<-SQL
          SELECT
                B.*
          FROM
                business_units B
          ORDER BY
                B.name
        SQL
      )
    else
      return BusinessUnit.find_by_sql(<<-SQL
          SELECT
                B.*
          FROM
                business_units B
          JOIN  business_units_upgrades BU ON BU.business_unit_id = B.id
          WHERE
                BU.upgrade_id IN (15,16)
          ORDER BY
                B.name
        SQL
      )
    end
  end
  
  def login_error(cause)
    Log.error "Login Error! Cause: #{cause}, Business Unit: #{session[:business_unit]}, Member: #{session[:member]}}"
    render :file => "#{RAILS_ROOT}/public/403.html", :status => 403
  end
  
  def new_hal_session
    @hal_session = HalSession.find_by_session_id(session.session_id)
    bu = session[:business_unit]
    if @hal_session.nil?
      @hal_session = HalSession.new
      #@hal_session.session = Session.find_by_session_id(session.session_id)
      @hal_session.sessid = session.session_id
      @hal_session.business_unit = bu
      HalSession.transaction() do
        unless @hal_session.save
          record_errors(@hal_session)
        end
      end
    else
      HalSession.update(@hal_session.id, {"business_unit" => bu})
    end
  end
   
  def add_dealer_cookies(dealer)
  	cookies[:dealerId] = dealer.id.to_s
  	cookies['ts_dealer'] = dealer.name.gsub /\s|\[|\]|\{|\}|\(|\)|\=|\,|\"|\?|\\|\/|@|:|;/,''
  	dealer_group = BusinessUnit.find(dealer.parent_id)
  	cookies['ts_dealergroup'] = dealer_group.name.gsub /\s|\[|\]|\{|\}|\(|\)|\=|\,|\"|\?|\\|\/|@|:|;/,''
  end

end
