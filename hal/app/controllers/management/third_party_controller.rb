module Management

  class ThirdPartyController < BaseController
  
    protected
    
    def default_container()
      'ThirdPartyManagement'
    end
    
    def collection()
      active_record_class().find_by_sql(<<-SQL
          SELECT
                p.*
          FROM
                #{active_record_class().table_name()} p
          JOIN  third_parties t ON t.id = p.id
          WHERE
                t.business_unit_id = #{session[:business_unit].id}
        SQL
      )
    end
    
  end

end