module Management
  
  class PlanningActLotPromotionController < PlanningActController
    
    attr_reader :active_record
    
    protected
    
    def active_record_class()
      ::PlanningActLotPromotion
    end
    
    def insert_or_update
      if params[:planning_act_lot_promotion][:id].nil?
        # its all completely new
        PlanningActLotPromotion.transaction() do
          @active_record = PlanningActLotPromotion.new(params[:planning_act_lot_promotion])
          @active_record.business_unit_id = session[:business_unit].id
          @active_record.active = true
          save_record(@active_record)
        end
      else
        # updating something already saved
        PlanningActLotPromotion.transaction() do
          @active_record = PlanningActLotPromotion.find(params[:planning_act_lot_promotion][:id])
          unless @active_record.update_attributes(params[:planning_act_lot_promotion])
            record_errors(@active_record)
          end
        end
      end
    end
    
  end
  
end
