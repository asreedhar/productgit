module Management
  
  class BaseController < DynamicController
    
    layout "preference"
    
    def list
      render :partial => "#{partial()}s", :locals => { :inventory => inventory(), :planning_action => planning_action(), collection_var() => collection() }.update(collections_map(params))
    end
    
    def start
      perform_render nil, locals_for_partial().update(:editable => false)
    end
    
    def finish
      ActionView::Base.debug_rjs = false
      css = css_selector()
      container = container()
      render :update do |page|
        page.replace_html container, ''
        page.select("*[class=\"#{css}\"]").each do |value,index|
          page << "try { eval($(value).getAttribute('hal:refresh')); } catch (e) {alert(e);}"
        end
        page.select('#CampaignManagement').each do |management,index|
          page << " if (!Element.empty(value)) {RedBox.showOverlay(); RedBox.addHiddenContent(value); RedBox.showSelectBoxes();} "
        end
      end
    end
    
    def add
      perform_render new_instance(), locals_for_partial().update(:editable => true)
    end
    
    def open
      perform_render load(), locals_for_partial().update(:editable => false)
    end
    
    def edit
      perform_render load(), locals_for_partial().update(:editable => true)
    end
    
    def delete
      record = load()
      unless record.update_attribute(:active, false)
        record_errors(record)
      end
      if mode() == 'page' && (flash.now[:notice].nil? || flash.now[:notice].empty?)
        redirect_to :action => 'start', :mode => mode(), :container => container()
      else
        perform_render nil, locals_for_partial().update(:editable => false)
      end
    end
    
    def save
      insert_or_update
      if mode() == 'page' && (flash.now[:notice].nil? || flash.now[:notice].empty?)
        redirect_to :action => 'open', primary_key() => active_record(), :mode => mode(), :container => container()
      else
        perform_render active_record(), locals_for_partial().update(:editable => ((flash.now[:notice].nil? || flash.now[:notice].empty?) ? false : true))
      end
    end
    
    def cancel
      perform_render load(true), locals_for_partial().update(:editable => false)
    end
    
    protected
    
    def perform_render(object=nil, locals={})
      partial = partial()
      params[:mode] = locals[:mode]
      params[:container] = locals[:container]
      case locals[:mode]
        when 'page'
          page_vars
          render :partial => partial, :object => object, :locals => locals, :layout => true
        when 'tile'
          render :partial => partial, :object => object, :locals => locals
        when 'float'
          render :update do |page|
            page.replace_html locals[:container], :partial => partial, :object => object, :locals => locals
          end
      end
    end
    
    def collections_map(map={},input_type='radio')
      defaults = {
        :input_type   => input_type,
        :html_options => {},
        :selections   => []
      }
      results = {}
      [:input_type, :input_object, :input_method, :selections, :html_options].each do |key|
        results[key] = map[key] || defaults[key]
      end
      results
    end
    
    def mode()
      return params[:mode] || 'float'
    end
    
    def container()
      return params[:container] || default_container()
    end
    
    def inventory_id()
      params[:inventory_id]
    end
    
    def inventory()
      @inventory = Inventory.find(inventory_id()) if @inventory.nil? && !inventory_id().nil?
      @inventory
    end
    
    def planning_action_id()
      params[:planning_action_id]
    end
    
    def planning_action()
      @planning_action = PlanningAction.find(planning_action_id()) if @planning_action.nil? && !planning_action_id().nil?
      @planning_action
    end
    
    def load(allow_nil=false)
      load_record(primary_key(), active_record_class(), allow_nil)
    end
    
    def load_record(pk, klass, allow_nil)
      obj = nil
      if params[pk].nil?
        raise "Nil primary key value (pk=#{pk.to_s})" unless allow_nil
      else
        obj = klass.find(params[pk])
      end
      raise "Record not found for pk=#{pk.to_s} val=#{params[pk]}" if obj.nil? && !allow_nil
      obj
    end
    
    def locals_for_partial()
      { primary_key() => primary_key_value(), :inventory_id => inventory_id(), :planning_action_id => planning_action_id(), collection_var() => collection(), :mode => mode(), :container => container() }
    end
    
    def primary_key_value()
      params[primary_key()]
    end
    
    def new_instance()
      active_record_class().new
    end
    
    def partial()
      active_record_class().to_s().underscore()
    end
    
    def collection_var()
      active_record_class().to_s().underscore().pluralize()
    end
    
    def primary_key()
      "#{active_record_class().to_s().underscore()}_id"
    end
    
    def page_vars()
      @page_title = "Preferences - Third Party"
      @page_style = "preference"
    end
    
    def css_selector
      "#{planning_action().planning_action_type.underscore_name}_#{planning_action().underscore_name}_values"
    end
  
  end
  
end