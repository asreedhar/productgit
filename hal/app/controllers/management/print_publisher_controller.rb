module Management
  
  class PrintPublisherController < ThirdPartyController
    
    attr_reader :active_record
    
    def print_publisher_print_component_list_items_sort
      @print_components, @print_publisher = nil, nil
      if params[:print_publisher_id].nil?
        @print_components = Array.new
        params[:print_component_list].each do |print_component_id|
          @print_components << PrintComponent.find(print_component_id)
        end
      else
        @print_publisher = PrintPublisher.find(params[:print_publisher_id])
        params[:print_component_list].each_with_index do |item_id,idx|
          @print_publisher.print_publisher_print_component_list_items.each do |item|
            if item.id == item_id.to_i
              item.position = idx+1
              break
            end
          end
        end
      end
      render :partial => 'print_publisher_print_component_list_items', :locals => {
        :print_publisher  => @print_publisher,
        :print_components => @print_components,
        :editable         => true
      }
    end
    
    protected
    
    def active_record_class()
      ::PrintPublisher
    end
    
    def new_instance()
      PrintPublisher.new do |p|
        p.include_vehicle_price = true
        p.book_id = BusinessUnitPreference.find(:first, :conditions => ["business_unit_id = ?", session[:business_unit].id]).primary_book_id
      end
    end
    
    def insert_or_update
      if params[:print_publisher][:id].nil?
        # its all completely new
        PrintPublisher.transaction() do
          third_party = ThirdParty.new do |t|
            t.business_unit_id = session[:business_unit].id
            t.third_party_type_id = 1
            t.name = params[:print_publisher][:name]
          end
          save_record(third_party) do
            @active_record = PrintPublisher.new do |p|
              p.id      = third_party.id
              p.email   = params[:print_publisher][:email]
              p.fax     = params[:print_publisher][:fax]
              p.book_id = params[:print_publisher][:book_id]
              p.include_vehicle_price = params[:print_publisher][:include_vehicle_price]
            end
            save_record(@active_record) do
              params[:print_publisher_print_component_list_item].each_pair do |key,value|
                print_publisher_print_component_list_item = PrintPublisherPrintComponentListItem.new(value)
                print_publisher_print_component_list_item.print_publisher_id = @print_publisher.id
                unless print_publisher_print_component_list_item.save
                  record_errors(print_publisher_print_component_list_item)
                end
              end
            end
          end
        end
      else
        # updating something already saved
        PrintPublisher.transaction() do
          third_party = ThirdParty.find(params[:print_publisher][:id])
          if third_party.update_attribute(:name, params[:print_publisher][:name])
            @active_record = PrintPublisher.find(params[:print_publisher][:id])
            @active_record.email   = params[:print_publisher][:email]
            @active_record.fax     = params[:print_publisher][:fax]
            @active_record.book_id = params[:print_publisher][:book_id]
            @active_record.include_vehicle_price = params[:print_publisher][:include_vehicle_price]
            save_record(@active_record) do
              record_errors(PrintPublisherPrintComponentListItem.update(
                params[:print_publisher_print_component_list_item].keys,
                params[:print_publisher_print_component_list_item].values
              ))
            end
          else
            record_errors(third_party)
          end
        end
      end
    end
    
    def locals_for_partial()
      super().update(:print_components => print_components(), :books => books())
    end
    
    def print_components()
      PrintComponent.find(:all)
    end
    
    def books()
      Book.find_by_sql(<<-SQL
          SELECT
                b.*
          FROM
                books b
          JOIN  business_units_books bb ON b.id = bb.book_id
          WHERE
                bb.business_unit_id = #{session[:business_unit].id}
        SQL
      )
    end
    
  end
  
end
