module Management
  
  class WholesalerController < ThirdPartyController
    
    attr_reader :active_record
    
    protected
    
    def active_record_class()
      ::Wholesaler
    end
    
    def insert_or_update
      if params[:wholesaler][:id].nil?
        # its all completely new
        Wholesaler.transaction() do
          third_party = ThirdParty.new do |t|
            t.business_unit_id    = session[:business_unit].id
            t.third_party_type_id = 3
            t.name                = params[:wholesaler][:name]
          end
          save_record(third_party) do
            @active_record = Wholesaler.new do |p|
              p.id = third_party.id
              p.telephone_number = params[:wholesaler][:telephone_number]
            end
            save_record(@active_record)
          end
        end
      else
        # updating something already saved
        Wholesaler.transaction() do
          third_party = ThirdParty.find(params[:wholesaler][:id])
          if third_party.update_attribute(:name, params[:wholesaler][:name])
            @active_record = Wholesaler.find(params[:wholesaler][:id])
            @active_record.telephone_number = params[:wholesaler][:telephone_number]
            save_record(@active_record)
          else
            record_errors(third_party)
          end
        end
      end
    end
    
  end
  
end
