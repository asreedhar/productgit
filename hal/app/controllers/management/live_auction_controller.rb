module Management
  
  class LiveAuctionController < ThirdPartyController
    
    attr_reader :active_record
    
    protected
    
    def active_record_class()
      ::LiveAuction
    end
    
    def insert_or_update
      if params[:live_auction][:id].nil?
        # its all completely new
        ::LiveAuction.transaction() do
          third_party = ThirdParty.new do |t|
            t.business_unit_id    = session[:business_unit].id
            t.third_party_type_id = 2
            t.name                = params[:live_auction][:name]
          end
          save_record(third_party) do
            @active_record = ::LiveAuction.new do |p|
              p.id    = third_party.id
              p.city  = params[:live_auction][:city]
              p.state = params[:live_auction][:state]
            end
            save_record(@active_record)
          end
        end
      else
        # updating something already saved
        ::LiveAuction.transaction() do
          third_party = ::ThirdParty.find(params[:live_auction][:id])
          if third_party.update_attribute(:name, params[:live_auction][:name])
            @active_record = ::LiveAuction.find(params[:live_auction][:id])
            @active_record.city  = params[:live_auction][:city]
            @active_record.state = params[:live_auction][:state]
            save_record(@active_record)
          else
            record_errors(third_party)
          end
        end
      end
    end
    
  end
  
end
