module Management

  class CampaignController < BaseController
    
    attr_reader :active_record
    
    def finish
      render :update do |page|
        page.replace_html 'CampaignManagement', ''
        page.select("*[class=\"campaign_values\"]").each do |value,index|
          page << "eval($(value).getAttribute('hal:refresh'));"
        end
        page.select("*[class=\"campaign_run_values\"]").each do |value,index|
          page << "eval($(value).getAttribute('hal:refresh'));"
        end
      end
    end
    
    def change_campaign_type
      @third_parties, @third_party_type = nil, nil
      case params[:campaign_type_id].to_i
        when 1
          @third_parties = ThirdParty.find(:all, :conditions => ["business_unit_id = ? and third_party_type_id = 1", session[:business_unit].id])
          @third_party_type = ThirdPartyType.find(1)
      end
      render :update do |page|
        if @third_parties.nil?
          page.replace_html 'third_party_label_container', '--'
          page.replace_html 'third_party_input_container', '--'
        else
          page.replace 'third_party_container', :partial => 'third_parties', :object => @third_parties, :locals => { :third_party_type => @third_party_type, :html_options => Hash.new, :planning_action_id => @planning_action_id, :inventory_item_id => @inventory_item_id }
        end
      end
    end
    
    def change_interval_type
      render :update do |page|
        object_name = params[:object_name]
        interval_type_id = params[:interval_type_id].to_i
        updates = {}
        [0,1,2,3,4].each do |id|
          container = "##{object_name}_interval_type_#{id}_container"
          on = (interval_type_id == id)
          if on
            page.select(container).each {|value,index| page << "Element.classNames(value).remove('off')" }
          else
            page.select(container).each {|value,index| page << "Element.classNames(value).add('off')" }
          end
          case id
            when 2
              updates["##{object_name}_day_of_week"] = (on == false)
            when 3
              updates["##{object_name}_day_of_month"] = (on == false)
            when 1,4
              updates["##{object_name}_day_of_year_#{id}"] = (on == false)
          end
        end
        updates.each do |input,disabled|
          if disabled
            page.select(input).each {|value,index| page << "value.blur(); value.value = ''; value.disabled = 'true';" }
          else
            page.select(input).each {|value,index| page << "value.disabled = '';" }
          end
        end
        page.select("##{object_name}_interval_type_id").each do |value,index|
          page << "value.disabled = ''; value.value = '#{interval_type_id}';"
        end
      end
    end
    
    protected
    
    def active_record_class()
      ::Campaign
    end
    
    def default_container()
      'CampaignManagement'
    end
    
    def collection()
      Campaign.find(
        :all,
        :conditions => ["business_unit_id = ? and active = 1", session[:business_unit].id],
        :order      => "name"
      )
    end
    
    def insert_or_update()
      # tidy the junk from the request
      delete_non_values(params, [:run_date,:deadline], [:day_of_week,:day_of_month,:day_of_year])
      delete_non_values(params, [:campaign], [:campaign_type_id,:third_party_id])
      # try and save the rest
      if neither_nil_nor_empty(params,:campaign)
        if neither_nil_nor_empty(params[:campaign],:id)
          # save the changes
          Campaign.transaction() do
            record_errors(Campaign.update(params[:campaign][:id], params[:campaign]))
            record_errors(Interval.update(params[:run_date][:id], params[:run_date]))
            record_errors(Interval.update(params[:deadline][:id], params[:deadline]))
            @active_record = Campaign.find(params[:campaign][:id])
          end
        else
          # marshall some objects
          @active_record = Campaign.new(params[:campaign])
          @active_record.business_unit_id = session[:business_unit].id
          @active_record.active = true
          run_date = Interval.new(params[:run_date])
          deadline = Interval.new(params[:deadline])
          # save them checking for errors along the way
          Campaign.transaction() do
            save_record(run_date) do
              save_record(deadline) do
                @active_record.run_date_interval_id = run_date.id
                @active_record.deadline_interval_id = deadline.id
                save_record(@active_record)
              end
            end
          end
        end
      end
    end
    
    def delete_non_values(map,objects,attrs)
      objects.each do |object|
        if neither_nil_nor_empty(map,object)
          attrs.each do |attr|
            if not_nil(map[object],attr)
              if ['Please Select',''].include?(map[object][attr])
                map[object].delete(attr)
              end
            end
          end
        end
      end
    end
    
  end

end