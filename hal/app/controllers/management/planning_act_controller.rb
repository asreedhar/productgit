module Management

  class PlanningActController < BaseController
    
    protected
    
    def default_container()
      'PlanningActManagement'
    end
    
    def collection()
      case active_record_class().table_name()
        when 'planning_act_others'
          active_record_class().find(
            :all,
            :conditions => [
              "business_unit_id = ? and planning_action_id = ? and active = 1",
              session[:business_unit].id,
              planning_action_id()
            ]
          )
        else
          active_record_class().find(
            :all,
            :conditions => [
              "business_unit_id = ? and active = 1",
              session[:business_unit].id
            ]
          )
      end
    end
    
  end

end