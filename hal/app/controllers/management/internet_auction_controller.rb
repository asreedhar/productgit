module Management
  
  class InternetAuctionController < ThirdPartyController
    
    attr_reader :active_record
    
    protected
    
    def active_record_class()
      ::ThirdParty
    end
    
    def collection()
      ThirdParty.find_by_sql(<<-SQL
          SELECT
                t.*
          FROM
                third_parties t
          WHERE
                t.third_party_type_id = 5
          AND   (t.business_unit_id = #{session[:business_unit].id}#{params[:action] == 'list' ? " OR t.business_unit_id = 100150" : ""})
        SQL
      )
    end
    
    def insert_or_update
      if params[:internet_auction][:id].nil?
        # its all completely new
        ThirdParty.transaction() do
          @active_record = ThirdParty.new do |t|
            t.business_unit_id    = session[:business_unit].id
            t.third_party_type_id = 5
            t.name                = params[:internet_auction][:name]
          end
          save_record(@active_record)
        end
      else
        # updating something already saved
        ThirdParty.transaction() do
          @active_record = ThirdParty.find(params[:internet_auction][:id])
          unless @active_record.update_attribute(:name, params[:internet_auction][:name])
            record_errors(@active_record)
          end
        end
      end
    end
    
    def partial()
      'internet_auction'
    end
    
    def collection_var()
      'internet_auctions'
    end
    
    def primary_key()
      "internet_auction_id"
    end
    
  end
  
end
