module Management
  
  class PlanningActSpiffController < PlanningActController
    
    attr_reader :active_record
    
    protected
    
    def active_record_class()
      ::PlanningActSPIFF
    end
    
    def insert_or_update
      if params[:planning_act_spiff][:id].nil?
        # its all completely new
        PlanningActSPIFF.transaction() do
          @active_record = PlanningActSPIFF.new(params[:planning_act_spiff])
          @active_record.business_unit_id = session[:business_unit].id
          @active_record.active = true
          save_record(@active_record)
        end
      else
        # updating something already saved
        PlanningActSPIFF.transaction() do
          @active_record = PlanningActSPIFF.find(params[:planning_act_spiff][:id])
          unless @active_record.update_attributes(params[:planning_act_spiff])
            record_errors(@active_record)
          end
        end
      end
    end
    
  end
  
end
