module Management
  
  class PlanningActOtherController < PlanningActController
    
    attr_reader :active_record
    
    protected
    
    def active_record_class()
      ::PlanningActOther
    end
    
    def insert_or_update
      if params[:planning_act_other][:id].nil?
        # its all completely new
        PlanningActOther.transaction() do
          @active_record = PlanningActOther.new(params[:planning_act_other])
          @active_record.business_unit_id = session[:business_unit].id
          @active_record.active = true
          save_record(@active_record)
        end
      else
        # updating something already saved
        PlanningActOther.transaction() do
          @active_record = PlanningActOther.find(params[:planning_act_other][:id])
          unless @active_record.update_attributes(params[:planning_act_other])
            record_errors(@active_record)
          end
        end
      end
    end
    
  end
  
end
