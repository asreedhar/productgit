class SearchController < DynamicController
  
  include ApplicationHelper
  
  layout "home"
 
  def index
    @page_title = "Search"
    @page_style = "search"
  end
  def search_criteria_list
    bu_id = session[:business_unit].id
    saved_searches = SearchCriterion.find(
                                          :all,
                                          :conditions => ["business_unit_id = ? and name is not null", session[:business_unit].id]
                                        )
    render :partial => 'search_criterion_load',  :locals => { :saved_searches => saved_searches }
  end
  def search_criteria_load
   search_criterion = nil
    unless params[:id].nil?
     search_criterion = SearchCriterion.find(params[:id])
    end
    render :update do |page|
      page.replace 'search_criterion_container', :partial => 'search_criterion', :object => search_criterion
    end
  end
 
  def search
    @search_criterion = SearchCriterion.search_criterion_builder(session[:business_unit].id, params)
    SearchCriterion.transaction() do
      if @search_criterion.save
        search_result_rs = ActiveRecord::Base.connection.select_all(<<-SQL
           EXECUTE dbo.NewSearchResults #{@search_criterion.id}
          SQL
        ).first
        @search_result = SearchResult.find(search_result_rs['SearchResultID'])
      else
        record_errors(@search_criterion)
      end
    end
  end

  def name_search_criterion_open
    search_criterion = SearchCriterion.find(params[:search_criterion_id])
    
      render :partial => 'search_criterion_name', :object => nil, :locals => { :search_criterion => search_criterion }
    
  end
  
  def name_search_criterion_save
    @search_criterion = SearchCriterion.find(params[:search_criterion][:id])
     @search_criterion.update_attribute(:name, params[:search_criterion][:name])
     
    
    render :update do |page|
      page.replace_html 'name_search_criterion_container', ''
    end
  end
  
  def search_result
    @search_result = SearchResult.find(params[:search_result][:id])
    @search_criterion = SearchCriterion.find(params[:search_criterion][:id])
    ids = ActiveRecord::Base.connection.select_all(<<-SQL
        SELECT    v.vehicle_id
        FROM      vehicle_list_items v
        WHERE     vehicle_list_id = #{@search_result.vehicle_list_id}
        ORDER BY  position
      SQL
    ).collect{|row| row['vehicle_id']}.join(',')
    inventory = Inventory.find(
      :all,
      :conditions => "inventory_type_id = 2 and active = 1 and vehicle_id IN (#{ids})",
      :include    => :vehicle
    )
    render :update do |page|
      page.replace_html 'SearchResultContainer', :partial => 'search_result', :object => @search_result, :locals => { :search_criterion => @search_criterion, :inventory => inventory }
     # page.update_html 'search_summary',@search_criterion.name
    end
  end
  
end
