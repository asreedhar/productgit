class StaticPageController < ApplicationController
 layout "popup"
 def about
  @page_title="About First Look"
  @page_style="popup"
 end
 def contact
  @page_title="Contact First Look"
  @page_style="popup"
 end
end