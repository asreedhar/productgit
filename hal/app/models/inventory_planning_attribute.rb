class InventoryPlanningAttribute < ActiveRecord::Base
  belongs_to :inventory
  belongs_to :planning_action_type
  validates_presence_of :inventory_id, :planning_action_type_id, :candidate_for_advertisement, :candidate_for_wholesale
end
