class InventoryPlanningCategory < ActiveRecord::Base
  belongs_to :inventory_id
  belongs_to :business_unit_planning_category
  has_many   :inventory_planning_tasks
end
