class AppraisalReviewBookoutPreference < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :book
  belongs_to :book_category
  belongs_to :vehicle_make
  validates_uniqueness_of :book_category_id, :scope => [:business_unit_id, :vehicle_make_id, :book_id]
end
