class VehicleLine < CachedModel
  belongs_to :vehicle_make
  has_and_belongs_to_many :naaa_series_body_styles, :class_name => "NAAASeriesBodyStyle", :join_table => 'vehicle_lines_naaa_series_body_styles'
end
