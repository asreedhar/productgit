class Customer < ActiveRecord::Base
  
  belongs_to :appraisal
  
  validates_inclusion_of  :gender, :in => ['M', 'F'], :allow_nil => true
  validates_length_of     :email, :within => 1..50, :allow_nil => true
  validates_as_email      :email, :message => 'is invalid'
  validates_length_of     :first_name, :in => 1..40, :allow_nil => true
  validates_length_of     :last_name, :in => 1..40, :allow_nil => true
  validates_uniqueness_of :id, :scope => :appraisal_id
  
  def empty?
    return (
      nil_or_empty(first_name) &&
      nil_or_empty(last_name) &&
      nil_or_empty(email) &&
      nil_or_empty(gender) &&
      nil_or_empty(telephone_number))
  end
  
  def full_name
    name = "#{read_attribute(:first_name)} #{read_attribute(:last_name)}"
    name = "" if name.length == 1
    name
  end
  
  def contact_summary
    txt = ""
    unless telephone_number.nil? || telephone_number.empty?
      txt << telephone_number
    end
    unless email.nil? || email.empty?
      txt << " / " if txt.length > 0
      txt << email
    end
    txt
  end
  
  protected :attributes_with_quotes
  
  def attributes_with_quotes(include_primary_key = true)
    attributes.inject({}) do |quoted, (name, value)|
      if column = column_for_attribute(name)
        quoted[name] = quote(value, column) unless !include_primary_key && column.primary || name == 'id'
      end
      quoted
    end
  end
  
  def nil_or_empty(obj)
    return obj.nil? || obj.empty? || obj.blank?
  end
  
end
