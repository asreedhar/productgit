class AppraisalReviewProxyComponentCollection < ProxyComponent::Collection
  
  def initialize(i)
    super(i)
  end
  
  def controller
    'appraisal_review'
  end
  
  def proxy_url(records)
    { :controller => controller(), :action => action(), :appraisal_id => records.collect{|r| r.appraisal_id}, :day_id => records.first.day_id, :rule => rule_id }
  end
  
  def cycle_name()
    'bucket_alignment'
  end
  
  def rule_id()
    [7,8,6, 4, 1, 5, 2, 3][id-1]
  end
  
  def locals()
    { :rule => rule_id(), :number_of_appraisal_reviews => records().length() }
  end
  
end