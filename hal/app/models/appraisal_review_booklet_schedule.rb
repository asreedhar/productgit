class AppraisalReviewBookletSchedule < ActiveRecord::Base
  
  belongs_to :business_unit
  has_and_belongs_to_many :intervals
  
  validates_numericality_of :hour_of_day, :only_integer => true, :allow_nil => false
  validates_inclusion_of :hour_of_day, :in => 0..23, :allow_nil => false
  
  class << self
    def week_day(id, name)
      module_eval <<-"end;"
        def has_#{name}_enabled(*args)
          interval = intervals.detect{|i| i.interval_type_id == 2 && i.day_of_week == #{id}}
          if args.nil? || args.empty?
            return !interval.nil?
          else
            Interval.transaction() do
              if [1, '1', true, 'true'].include?(args[0])
                intervals << Interval.new({ :interval_type_id => 2, :day_of_week => #{id}}) if interval.nil?
              else
                unless interval.nil?
                  intervals.delete(interval)
                  interval.destroy()
                end
              end
            end
          end
        end
      end;
    end
  end
  
  week_day 1, 'sunday'
  week_day 2, 'monday'
  week_day 3, 'tuesday'
  week_day 4, 'wednesday'
  week_day 5, 'thursday'
  week_day 6, 'friday'
  week_day 7, 'saturday'
  
end