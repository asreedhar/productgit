class BusinessUnitPlanningCategory < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :planning_category
  belongs_to :alias_planning_category, :class_name => 'PlanningCategory'
  has_and_belongs_to_many :planning_age_boundaries, :class_name => "PlanningAgeBoundary", :join_table => 'business_unit_planning_categories_planning_age_boundaries'
  acts_as_tree
  validates_each :lower_threshold, :upper_threshold do |record, attr, value|
    if !value.nil?
      record.errors.add attr, 'less than 0' if value < 0
      record.errors.add attr, 'greater than 2,147,483,647' if value > 2147483647
    end
  end
  validates_uniqueness_of :planning_category_id, :scope => [:business_unit_id, :is_comparison_configuration]
end
