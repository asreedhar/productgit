class NAAASeriesBodyStyle < CachedModel
  belongs_to :naaa_series, :class_name => "NAAASeries"
  belongs_to :naaa_body_style, :class_name => "NAAABodyStyle"
end
