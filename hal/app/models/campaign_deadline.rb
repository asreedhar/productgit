class CampaignDeadline < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :campaign
  belongs_to :campaign_run
end
