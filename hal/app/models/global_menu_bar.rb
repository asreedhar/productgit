class GlobalMenuBar < MenuBar::Base

  Development = false
  
  add_action do
    with_text 'HOME'
    links_to :controller => '/home', :action => 'index'
  end
  
  add_menu do
    with_text 'TOOLS'
    with_icon proc{" <img src='#{ActionController::Base.asset_host.call('/images/navarrow.gif', request)}' alt='' /> <br />"}
    with_default_action false
      add_action do
        with_text 'Custom Inventory Analysis'
        links_to :action => :redirect_to_external_url, :external_url => '/IMT/CIASummaryDisplayAction.go'
        show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 3}}
      end
      add_action do
        with_text 'First Look Search &amp; Acquisition'
        links_to :action => :redirect_to_external_url, :external_url =>'/NextGen/SearchHomePage.go'
        show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 3}}
      end
      add_action do
        with_text 'Flash Locator'
        links_to :action => :redirect_to_external_url, :external_url =>'/NextGen/FlashLocateSummary.go'
        show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 3}}
      end
      add_action do
        with_text 'Forecaster'
        links_to :action => :redirect_to_external_url, :external_url =>'/IMT/DealerOldHomeDisplayAction.go', :forecast => '1'
        show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 3}}
      end
    add_menu do
      with_text 'Inventory Management Plan'
      with_default_action false
      add_action do
        with_text 'Buckets'
        links_to :controller => '/planning', :action => 'index'
      end
      add_action do
        with_text 'Category'
        links_to :controller => '/planning', :action => 'index', 'planning_category[id]' => '5'
      end
      add_action do
        with_text 'Action'
        links_to :controller => '/planning', :action => 'index', 'planning_action[id]' => '1'
      end
    end if Development
    add_action do
      with_text 'Inventory Management Plan'
      links_to :action => :redirect_to_external_url, :external_url => '/NextGen/InventoryPlan.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end unless Development
    add_action do
      with_text 'Loan Value - Book Value Calculator'
      links_to :action => :redirect_to_external_url, :external_url => '/NextGen/EquityAnalyzer.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 11}}
    end
    add_action do
      with_text 'Make A Deal'
      links_to :controller => '/appraisal_review'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 21}}
    end
    add_action do
      with_text 'Market Velocity Stocking Guide'
      links_to :action => :redirect_to_external_url, :external_url => '/pricing/', :token => "DEALER_SYSTEM_COMPONENT", :pageName => "Pages/Market/"
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 22}}
    end
    add_action do
      with_text 'Management Summary'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/ucbp/TileManagementCenter.go'
      link_html_options :popup => ['popup','width=470,height=615']
    end
    add_menu do
      with_text 'Manual Bookout'
      show_if lambda{session[:business_unit].business_unit_preference.book_ids.detect {|id| [1,2,3].include?(id)}}
      add_action do
        with_text 'KBB'
        links_to :action => :redirect_to_external_url, :external_url => '/IMT/KBBManualBookoutAction.go', :selected => 'display'
        show_if lambda{session[:business_unit].business_unit_preference.book_ids.include?(3)}
      end
      add_action do
        with_text 'NADA'
        links_to :action => :redirect_to_external_url, :external_url => '/IMT/NADAManualBookoutAction.go', :selected => 'display'
        show_if lambda{session[:business_unit].business_unit_preference.book_ids.include?(2)}
      end
      add_action do
        with_text 'BlackBook'
        links_to :action => :redirect_to_external_url, :external_url => '/IMT/BlackBookManualBookoutAction.go', :selected => 'display'
        show_if lambda{session[:business_unit].business_unit_preference.book_ids.include?(1)}
      end
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 1}}
    end
    add_action do
      with_text 'Performance Analyzer'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/PerformanceAnalyzerDisplayAction.go'
    end
    add_action do
      with_text 'Performance Mgmt Dashboard'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/EdgeScoreCardDisplayAction.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 7}}
    end
    add_action do
      with_text 'Trade Analyzer'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/QuickAppraise.go' 
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 1}}
    end
    add_action do
      with_text 'Appraisal Manager'
      links_to proc{
        number_of_appraisals_waiting = Appraisal.count_by_sql(<<-SQL
            SELECT  number_of_appraisals_waiting
            FROM    appraisals_waiting_report
            WHERE   business_unit_id = #{session[:business_unit].id}
          SQL
        )
        link = { :action => :redirect_to_external_url, :external_url => '/IMT/AppraisalManagerAction.go' }
        link.update(:selectedTab => "all") if number_of_appraisals_waiting == 0
        link
      }
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 1}}
    end
    add_action do
      with_text 'Vehicle Analyzer'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/VehicleAnalyzerDisplayAction.go'
    end
    add_action do
      with_text 'Create New Inventory'
      links_to :action => :redirect_to_external_url, :external_url => '/support/', :pageName => "Pages/Inventory/"      
      show_if lambda{session[:business_unit].business_unit_preference.show_in_transit_inventory_form == true}
    end

  end
  
  add_menu do
    with_text 'REPORTS'
    with_icon proc{" <img src='#{ActionController::Base.asset_host.call('/images/navarrow.gif', request)}' alt=' ' /> <br />"}
    with_default_action false
    add_action do
      with_text 'Action Plans'
      links_to :action => :redirect_to_external_url, :external_url =>'/NextGen/ActionPlansReports.go'
    end
    add_action do
      with_text 'Deal Log'
      links_to :action => :redirect_to_external_url, :external_url =>'/IMT/DealLogDisplayAction.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Fastest Sellers'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/FullReportDisplayAction.go', :ReportType => 'FASTESTSELLER', :weeks => 26
    end
    add_action do
      with_text 'Inventory Manager'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/DashboardDisplayAction.go', :module => 'U'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Inventory Overview'
      links_to :action => :redirect_to_external_url, :external_url => '/NextGen/InventoryOverview.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Most Profitable Vehicles'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/FullReportDisplayAction.go', :ReportType => 'MOSTPROFITABLE', :weeks => 26
    end
    add_action do
      with_text 'Performance Management Reports'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/ReportCenterRedirectionAction.go'
      link_html_options :popup => ['popup','width=1000,height=615,scrollbars=yes']
    end
    add_action do
      with_text 'Pricing Change Failure Report'
      links_to :action => :redirect_to_external_url, :external_url => '/NextGen/PriceChangeFailureReportAction.go'
      show_if lambda{session[:business_unit].business_units_internet_advertisers.size > 0}
    end
    add_action do
      with_text 'Pricing List'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/PricingListDisplayAction.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Stocking Reports'
      links_to :action => :redirect_to_external_url, :external_url => '/NextGen/OptimalInventoryStockingReports.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Top Sellers'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/FullReportDisplayAction.go', :ReportType => 'TOPSELLER', :weeks => 26
    end
    add_action do
      with_text 'Total Inventory Report'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/TotalInventoryReportDisplayAction.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Trades &amp; Purchases Report'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/DealerTradesDisplayAction.go'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 2}}
    end
    add_action do
      with_text 'Aged Inventory Exchange'
      links_to :action => :redirect_to_external_url, :external_url => '/IMT/InventoryExchangeDisplayAction.go'
    end
  end
  
  add_action do
    with_text 'PRINT'
    html_options(:style => proc{parent.children.last.text.equal?(self.text) ? "border-right:0;" : ""})
    links_to {}
    link_html_options(:onclick => 'window.print(); return false;')
  end
  
  add_action do
    with_text 'EXIT STORE'
    html_options(:style => proc{parent.children.last.text.equal?(self.text) ? "border-right:0;" : ""})
    links_to :action => :exit_store
    show_if lambda{session[:can_exit_store] == true}
  end
  
end
