class PlanningActionDefault < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :planning_action
  has_and_belongs_to_many :planning_action_default_reprices, :class_name => "PlanningActionDefaultReprice", :join_table => 'planning_action_defaults_planning_action_default_reprices'
  has_and_belongs_to_many :planning_action_default_lot_promotions, :class_name => "PlanningActionDefaultLotPromotion", :join_table => 'planning_action_defaults_planning_action_default_lot_promotions'
  has_and_belongs_to_many :planning_action_default_spiffs, :class_name => "PlanningActionDefaultSPIFF", :join_table => 'planning_action_defaults_planning_action_default_spiffs'
  has_and_belongs_to_many :planning_action_default_others, :class_name => "PlanningActionDefaultOther", :join_table => 'planning_action_defaults_planning_action_default_others'
  has_and_belongs_to_many :planning_action_default_live_auctions, :class_name => "PlanningActionDefaultLiveAuction", :join_table => 'planning_action_defaults_planning_action_default_live_auctions'
  has_and_belongs_to_many :planning_action_default_wholesalers, :class_name => "PlanningActionDefaultWholesaler", :join_table => 'planning_action_defaults_planning_action_default_wholesalers'
  has_and_belongs_to_many :planning_action_default_solds, :class_name => "PlanningActionDefaultSold", :join_table => 'planning_action_defaults_planning_action_default_solds'
  has_and_belongs_to_many :planning_action_default_small_print_advertisements, :class_name => "PlanningActionDefaultSmallPrintAdvertisement", :join_table => 'planning_action_defaults_planning_action_default_small_print_advertisements'
  has_and_belongs_to_many :planning_action_default_internet_auctions, :class_name => "PlanningActionDefaultInternetAuction", :join_table => 'planning_action_defaults_planning_action_default_internet_auctions'
  has_and_belongs_to_many :planning_action_default_reprices, :class_name => "PlanningActionDefaultReprice", :join_table => 'planning_action_defaults_planning_action_default_reprices'
  has_and_belongs_to_many :planning_age_default_boundaries, :class_name => "PlanningAgeBoundary", :join_table => 'planning_age_boundaries_planning_action_defaults'
  validates_presence_of :business_unit_id, :planning_action_id, :active
  def self.for_inventory_id(inventory_id)
    PlanningActionDefault.find_by_sql(<<-SQL
        SELECT
              d.*
        FROM
              planning_action_defaults d
        JOIN  planning_category_table_cells c on d.id = c.planning_action_default_id
        JOIN  inventory_planning_tasks t on c.id = t.planning_category_table_cell_id
        JOIN  inventory_planning_categories i on i.id = t.inventory_planning_category_id
        WHERE
              i.invalidated IS NULL
        AND   t.invalidated IS NULL
        AND   t.satisfied IS NULL
        AND   t.valid_until >= GETDATE()
        AND   i.inventory_id = #{inventory_id}
      SQL
    )
  end
end
