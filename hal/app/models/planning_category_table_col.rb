class PlanningCategoryTableCol < ActiveRecord::Base
  belongs_to :planning_category_table
  has_many   :planning_category_table_cells, :order => 'rindex'
end
