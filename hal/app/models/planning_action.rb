class PlanningAction < CachedModel
  belongs_to :planning_action_type
  has_and_belongs_to_many :third_party_types, :class_name => "ThirdPartyType", :join_table => 'planning_actions_third_party_types'
  def self.all_options
    return PlanningAction.find(:all, :order => "rank")
  end
  def self.retail_options
    return PlanningAction.find(:all, :conditions => ["planning_action_type_id = ?", 1], :order => "rank")
  end
  def self.wholesale_options
    return PlanningAction.find(:all, :conditions => ["planning_action_type_id = ?", 2], :order => "rank")
  end
end
