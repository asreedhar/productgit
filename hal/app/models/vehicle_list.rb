class VehicleList < ActiveRecord::Base
  belongs_to :vehicle_list_type
  has_many   :vehicle_list_items, :order => 'position'
end
