class BookValuation < CachedModel
  belongs_to :book
  has_many   :book_category_valuations, :order => "book_category_id"
end
