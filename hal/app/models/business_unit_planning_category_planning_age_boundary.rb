class BusinessUnitPlanningCategoryPlanningAgeBoundary < ActiveRecord::Base
  belongs_to :business_unit_planning_category
  belongs_to :business_unit
  belongs_to :planning_category
  belongs_to :planning_category_table_col_0, :class_name => 'PlanningCategoryTableCol'
  belongs_to :planning_category_table_col_1, :class_name => 'PlanningCategoryTableCol'
end