class InventoryAdvertisingAttribute < ActiveRecord::Base

  belongs_to :inventory
  
  protected :attributes_with_quotes
  
  def attributes_with_quotes(include_primary_key = true)
    attributes.inject({}) do |quoted, (name, value)|
      if column = column_for_attribute(name)
        quoted[name] = quote(value, column) unless !include_primary_key && column.primary || name == 'inventory_id'
      end
      quoted
    end
  end
  
end
