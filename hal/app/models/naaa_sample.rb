class NAAASample < CachedModel
  belongs_to :naaa_area, :class_name => "NAAAArea"
  belongs_to :naaa_period, :class_name => "NAAAPeriod"
end
