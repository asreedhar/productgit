class PlanningActionDefaultSmallPrintAdvertisement < ActiveRecord::Base
  belongs_to :campaign
  validates_presence_of :business_unit_id, :active, :campaign_id
end
