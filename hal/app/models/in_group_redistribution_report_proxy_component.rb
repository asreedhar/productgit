class InGroupRedistributionReportProxyComponent < ProxyComponent::Base

  def initialize(options)
    @business_unit_id   = options[:business_unit_id]
    @vehicle_segment_id = options[:vehicle_segment_id]
    @model_year         = options[:model_year]
  end
  
  def render_component(context)
    in_group_redistribution_report = component_data()
    the_component_partial = component_partial()
    cmd = proc{render(:partial => the_component_partial, :object => in_group_redistribution_report)}
    context.instance_eval(&cmd)
  end
  
  def component_data()
    vehicle_segment_clause = nil
    if @vehicle_segment_id.is_a?(Array)
      vehicle_segment_clause = ' IN (' + @vehicle_segment_id.collect{ |i| i.to_s }.join(',') + ')'
    else
      vehicle_segment_clause = ' = ' + @vehicle_segment_id.to_s
    end
    return InGroupRedistributionReport.new(
      ActiveRecord::Base.connection.select_all(<<-SQL
          SELECT
              B.id business_unit_id,
              B.name business_unit_name,
              B.telephone_number,
              GroupingDescriptionID vehicle_segment_id,
              ModelYear model_year,
              OptimalUnitsModelYear optimal_units_model_year,
              OptimalUnitsModel optimal_units_model,
              StockedUnitsModelYear stocked_units_model_year,
              StockedUnitsModel stocked_units_model,
              DistanceInMiles distance_in_miles
          FROM
                dbo.InGroupRedistribution I
          JOIN  dbo.business_units B ON B.id = I.TargetBusinessUnitID
          WHERE
              I.BusinessUnitID = #{@business_unit_id}
          AND	I.GroupingDescriptionID #{vehicle_segment_clause}
          AND	(I.ModelYear = #{@model_year} or ModelYear is null)
        SQL
      ),
      self
    )
  end
  
  def controller
    "appraisal_review"
  end
  
  def action_params
    {
      :business_unit_id   => @business_unit_id,
      :vehicle_segment_id => @vehicle_segment_id,
      :model_year         => @model_year
    }
  end
  
  class InGroupRedistributionReport < ProxyComponent::Data
    attr_reader  :results
    proxy_reader :business_unit_id, :vehicle_segment_id, :model_year
    def initialize(results,proxy)
      @results = results
      @proxy = proxy
    end
    def at(business_unit_id)
      @results.each do |result|
        if result["business_unit_id"] == business_unit_id
          return result
        end
      end
      return nil
    end
  end
  
end