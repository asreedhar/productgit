class BookValue < CachedModel
  belongs_to :book_category_valuation
  belongs_to :book_value_type
  belongs_to :book_subcategory
end
