class Member < ActiveRecord::Base
  has_and_belongs_to_many :business_units, :class_name => "BusinessUnit", :join_table => 'members_business_units'
  has_many :credentials
end
