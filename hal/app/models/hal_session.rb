class HalSession < ActiveRecord::Base
  belongs_to :business_unit
  
  def self.find_by_session_id(sessid)
    #sessid *SHOULD* be unique, but there's still chance that it isn't.
    #use the latest session in case of session ID clash
    find(:first, :conditions => ["sessid = ?",  sessid], :order => "updated_at DESC")
  end

  def self.delete_by_session_id(sessid)
    delete_all( "sessid = '" + sessid + "'")
  end
  
end
