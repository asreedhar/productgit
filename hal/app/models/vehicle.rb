class Vehicle < CachedModel
  belongs_to :vehicle_make
  belongs_to :vehicle_line
  belongs_to :vehicle_segment
  
  def name
    "#{model_year} #{vehicle_line.vehicle_make.name} #{vehicle_line.qualified_name}"
  end
  
  protected :attributes_with_quotes
  
  def attributes_with_quotes(include_primary_key = true)
    ignore_cols = ['vehicle_make_id', 'vehicle_line_id', 'vehicle_segment_id', 'segment', 'description']
    attributes.inject({}) do |quoted, (name, value)|
      if column = column_for_attribute(name)
        quoted[name] = quote(value, column) unless !include_primary_key && column.primary || ignore_cols.include?(name)
      end
      quoted
    end
  end
  
end
