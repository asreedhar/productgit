class PlanningActionType < CachedModel
  def self.all_options
    PlanningActionType.find_by_sql(<<-SQL
        SELECT
            *
        FROM
            planning_action_types
        ORDER BY
            rank
      SQL
    )
  end
end
