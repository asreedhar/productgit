#
# This class is a pre-aggregated inventory item for use in displaying the
# inventory tiles. The query is slightly faster than the independent calls
# from the inventory class. SW
#
class InventoryItem < ActiveRecord::Base
  has_and_belongs_to_many :planning_actions, :class_name => "PlanningAction", :join_table => 'inventory_items_planning_actions'
  def trade_or_purchase
    if self.vehicle_trade_or_purchase == 2
      "Trade"
    else 
      "Purchase"
    end
  end
end
