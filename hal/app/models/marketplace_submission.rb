class MarketplaceSubmission < ActiveRecord::Base
  validates_presence_of :third_party_id, :member_id, :marketplace_submission_status_id, :url, :begin_effective_date, :end_effective_date, :date_created
  validates_length_of :url, :within => 1..1000, :allow_nil => false
end
