class AppraisalReviewBookletNotifier < ActionMailer::Base
  def booklet_notification(email_address, appraisal_review_booklet_id, size_in_kb, completed, filename, summary_html, business_unit_name, empty_booklet)
    recipients email_address
    from "noreply@firstlook.biz"
    subject "Your Make-A-Deal Workbook Is Ready"
    headers "Reply-To" => "support@firstlook.biz"
    content_type "text/html"
    if size_in_kb.to_i < 512 && FileTest.exist?(filename)
      pdf = nil
      File.open(filename,"rb") do |f|
        pdf = f.read
      end
      attachment :content_type => "application/pdf", :filename => "make_a_deal_workbook_#{completed.strftime('%Y%m%dT%H%M%S')}.pdf", :body => pdf
    end
    body :url_for_booklet => "#{PDF_HOST}/appraisal_review_booklet/appraisal_review_booklet/#{appraisal_review_booklet_id}",
         :size_in_kb => size_in_kb,
         :completed => completed,
         :summary_html => summary_html,
         :business_unit_name => business_unit_name,
         :empty_booklet => empty_booklet
  end
end
