class OfflineCommandRequestObserver < ActiveRecord::Observer

  def after_create(offline_command_request)
    command = offline_command_request.offline_command.command.gsub(
        /business_unit_id/,
        offline_command_request.business_unit_id.to_s
    )
    ActiveRecord::Base.connection.execute(command, "Offline Command Observer")
    OfflineCommandRequest.delete(offline_command_request.id)
  end
  
end
