class InsightsProxyComponent < ProxyComponent::Base
  
  def initialize(options)
      @business_unit_id   = options[:business_unit_id]
      @number_of_weeks    = options[:number_of_weeks]
      @vehicle_segment_id = options[:vehicle_segment_id]
      @model_year         = options[:model_year]
      @series             = options[:series]
      @inventory_type     = options[:inventory_type]
      @vehicle_mileage    = options[:vehicle_mileage]
      # private data
      @vehicle_segment    = VehicleSegment.find(@vehicle_segment_id).name
  end
  
  def render_component(context)
    insights = ActiveRecord::Base.connection.select_all(
      sprintf(
          "EXECUTE GetInsights %d, %d, %s, %d, %d, %d, %d",
          @business_unit_id,
          @vehicle_segment_id,
          (@series.nil? || @series.empty?) ? "null" : "'#{series}'",
          @model_year,
          @vehicle_mileage,
          @inventory_type == 'New' ? 1 : 2,
          @number_of_weeks
      )
    ).collect{|row| Insight.new(row, @vehicle_segment, @model_year)}
    the_component_partial = component_partial()
    cmd = proc{render(:partial => the_component_partial, :object => insights)}
    context.instance_eval(&cmd)
  end
  
  def controller
    "appraisal_review"
  end
  
  def action_params
    {
      :number_of_weeks    => @number_of_weeks,
      :vehicle_segment_id => @vehicle_segment_id,
      :model_year         => @model_year,
      :series             => @series,
      :inventory_type     => @inventory_type,
      :vehicle_mileage    => @vehicle_mileage
    }
  end
  
  class Insight
    def initialize(row,segment,model_year)
      @text = ""
      case row['InsightTypeID']
        when 1
          @text = sprintf("%s sales history over last %s weeks", adjective(row['InsightAdjectivalID']), row['InsightValue'])
        when 2
          @text = sprintf("$%s %s profitable than average", row['InsightValue'], adjective(row['InsightAdjectivalID']))
        when 3
          @text = sprintf("No sales %s of the time", row['InsightValue'])  
        when 4
          @text = sprintf("%s %s margin than average", row['InsightValue'], adjective(row['InsightAdjectivalID']))
        when 5
          @text = sprintf("#%s profit generator", row['InsightValue'])
        when 6
          @text = sprintf("Sells %s %s %s than average", row['InsightValue'], (row['InsightValue'].to_i == 1 ? 'day' : 'days'), adjective(row['InsightAdjectivalID']))
        when 7
          @text = "" # traffic light
        when 8
          @text = "#{adjective(row['InsightAdjectivalID'])}stocked"
        when 9
          @text = sprintf("High mileage")
        when 10
          @text = sprintf("Older vehicle")
        when 11
          @text = sprintf("Annual ROI %s%%", row['InsightValue'])
        when 12
          @text = sprintf("%s%% of sales in core market", row['InsightValue'].to_i == 0 ? "less than 1" : row['InsightValue'])
        when 13
          @text = sprintf("#%s in %s sales", row['InsightValue'], segment)
        when 14
          @text = sprintf("%s%% of sales for make/model", row['InsightValue'].to_i == 0 ? "less than 1" : row['InsightValue'])
        when 15
          @text = sprintf("%s #%s selling year", model_year, row['InsightValue'])
      end
    end
    def adjective(id)
      adj = ""
      case id
        when 1
          adj = "No"
        when 2
          adj = "Insufficient"
        when 3
          adj = "Faster"
        when 4
          adj = "Slower"
        when 5
          adj = "More"
        when 6
          adj = "Less"
        when 7
          adj = "Higher"
        when 8
          adj = "Lower"
        when 9
          adj = "Under"
        when 10
          adj = "Over"
        when 11
          adj = "High"
        when 12
          adj = "Older"
      end
      adj
    end
    def to_s
      @text
    end
  end
  
end