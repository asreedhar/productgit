class PrintPublisherPrintComponentListItem < ActiveRecord::Base
  belongs_to :print_publisher
  belongs_to :print_component
  acts_as_list :scope => :print_publisher, :column => :position
  validates_each :print_publisher_id, :print_component_id, :position, :active do |record, attr, value|
    record.errors.add attr, 'is not set' if value.nil?
  end
  validates_uniqueness_of :print_component_id, :scope => :print_publisher_id
end
