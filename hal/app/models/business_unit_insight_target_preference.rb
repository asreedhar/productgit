class BusinessUnitInsightTargetPreference < ActiveRecord::Base
  
  belongs_to :business_unit
  belongs_to :insight_target
  
  validates_numericality_of :min,       :only_integer => true, :allow_nil => true
  validates_numericality_of :max,       :only_integer => true, :allow_nil => true
  validates_numericality_of :value,     :only_integer => true, :allow_nil => true
  validates_numericality_of :threshold, :only_integer => true, :allow_nil => true
  
  validate do |record|
    # setup some flags on how we validate the values
    is_range = record.insight_target.insight_target_type_id == 2
    t_value, t_min, t_max, t_threshold = nil, nil, nil, nil
    m_value, m_min, m_max, m_threshold = "", "", "", ""
    case record.insight_target.insight_type_id
      when 16 # appraisal closing rate
        t_value = Proc.new{|x| !x.nil? && x >= 0 && x <= 100}
        m_value = "should be an integer between 0 and 100"
        t_threshold = Proc.new{|t,x| !t.nil? && !x.nil? && t >= 0 && t <= 100 && t <= x}
        m_threshold = "should be an integer between 0 and 100 and less than the target value"
      when 17 # make a deal
        t_value = Proc.new{|x| x.nil?}
        m_value = "should be nil"
        t_threshold = Proc.new{|t,x| !t.nil? && t >= 0 && t < 1000}
        m_threshold = "should be an integer between 0 and 1000"
      when 18 # trade-in inventory analyzed
        t_value = Proc.new{|x| !x.nil? || x >= 0 && x <= 100}
        m_value = "should be an integer between 0 and 100"
        t_threshold = Proc.new{|t,x| !x.nil? && !t.nil? && t >= 0 && t <= 100 && t <= x}
        m_threshold = "should be an integer between 0 and 100 and less than the target value"
      when 19 # average immediate wholesale profit
        t_min = Proc.new{|mn| !mn.nil? && mn.abs <= 1000}
        m_min = "should be an integer between &#177; 1000"
        t_max = Proc.new{|mn,mx| !mn.nil? && !mx.nil? && mx.abs <= 1000 && mx >= mn}
        m_max = "should be an integer between &#177; 1000 and greater than (or equal to) the min value"
        t_threshold = Proc.new{|t,x| !t.nil? && t >= 0 && t <= 1000}
        m_threshold = "should be an integer between 0 and 1000"
      when 20 # current inventory - aggressive retail
        raise StandardError, "Use InventoryBucketRange TABLE"
      when 21 # current inventory - unplanned vehicles
        t_value = Proc.new{|x| x.nil?}
        m_value = "should be nil"
        t_threshold = Proc.new{|t,x| !t.nil? && t >= 0 && t < 1000}
        m_threshold = "should be an integer between 0 and 1000"
      when 22 # current inventory - days supply
        t_value = Proc.new{|x| !x.nil? && x >= 0 && x < 1000}
        m_value = "should be an integer between 0 and 1000"
        t_threshold = Proc.new{|t,x| !t.nil? && !x.nil? && t >= 0 && t < 1000 && t <= x}
        m_threshold = "should be an integer between 0 and 100 and less than the target"
    end
    # test the record
    if is_range
      record.errors.add("min", m_min) unless t_min.call(record.min)
      record.errors.add("max", m_max) unless t_max.call(record.min, record.max)
      record.errors.add("threshold", m_threshold) unless t_threshold.call(record.threshold, nil)
      record.errors.add("value", "should be nil") unless record.value.nil?
    else
      record.errors.add("value", m_value) unless t_value.call(record.value)
      record.errors.add("threshold", m_threshold) unless t_threshold.call(record.threshold, record.value)
      record.errors.add("min", "should be nil") unless record.min.nil?
      record.errors.add("max", "should be nil") unless record.max.nil?
    end
  end
  
  def self.load_insight_target(insight_type_id, business_unit_id)
    business_unit_insight_target_preferences = BusinessUnitInsightTargetPreference.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              business_unit_insight_target_preferences p
        JOIN  insight_targets t on t.id = p.insight_target_id
        WHERE
              p.business_unit_id = #{business_unit_id}
        AND   t.insight_type_id = #{insight_type_id}
        ORDER BY
              t.id
      SQL
    )
    if business_unit_insight_target_preferences.nil? || business_unit_insight_target_preferences.empty?
      insight_targets = InsightTarget.find(
        :all,
        :conditions => ["insight_type_id = ?", insight_type_id],
        :order => "id"
      )
      insight_targets.each do |insight_target|
        business_unit_insight_target_preferences << BusinessUnitInsightTargetPreference.new do |p|
          p.business_unit_id = business_unit_id
          p.insight_target_id = insight_target.id
          p.value = insight_target.value
          p.min = insight_target.min
          p.max = insight_target.max
          p.threshold = insight_target.threshold
        end
      end
      # save as it allows us to submit back an array indexed by pk
      BusinessUnitInsightTargetPreference.transaction() do
        business_unit_insight_target_preferences.each do |p|
          p.save!
        end
      end
    end
    business_unit_insight_target_preferences
  end
  
end
