class Color < CachedModel
  
  def self.all_colors
    Color.find(:all, :order => "name")
  end
  
end
