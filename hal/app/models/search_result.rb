class SearchResult < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :vehicle_list, :include => :vehicle_list_items
  has_and_belongs_to_many :search_criteria, :class_name => "SearchCriterion", :join_table => 'search_results_search_criteria'
end
