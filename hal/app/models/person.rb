class Person < ActiveRecord::Base
  belongs_to :business_unit
  has_and_belongs_to_many :positions, :class_name => "Position", :join_table => 'people_positions'
  
  def full_name
    name = "#{read_attribute(:first_name)} #{read_attribute(:last_name)}"
    name = "" if name.length == 1
    name
  end
end
