class VehicleListItem < ActiveRecord::Base
  belongs_to :vehicle_list
  belongs_to :vehicle
  acts_as_list :scope => :vehicle_list
end
