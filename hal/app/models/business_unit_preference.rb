class BusinessUnitPreference < ActiveRecord::Base
  belongs_to :business_unit
  def book_ids
    [read_attribute(:primary_book_id), read_attribute(:secondary_book_id)]
  end
end
