class PlanningActSPIFF < ActiveRecord::Base
  validates_presence_of :business_unit_id, :active, :name, :amount
  validates_length_of :name, :minimum => 1, :allow_nil => false
  validates_numericality_of :amount, :allow_nil => false
end
