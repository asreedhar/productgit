class AppraisalBookValuationSummary < ActiveRecord::Base
  belongs_to :appraisal
  belongs_to :book_valuation
  belongs_to :book_category
  belongs_to :book_value_type
end

