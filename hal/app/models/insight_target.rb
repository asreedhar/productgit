class InsightTarget < ActiveRecord::Base
  belongs_to :insight_target_type
  belongs_to :insight_target_value_type
  belongs_to :insight_type
end
