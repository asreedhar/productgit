class PlanningActReprice < ActiveRecord::Base
  validates_presence_of :business_unit_id, :original_price, :new_price
  validates_numericality_of :original_price, :allow_nil => false
  validates_numericality_of :new_price, :allow_nil => false
end
