class AppraisalReviewBooklet < ActiveRecord::Base
  belongs_to :future_task
  validates_presence_of :future_task
end