class AppraisalValuation < ActiveRecord::Base
  belongs_to :appraisal
  validates_length_of :appraiser_name, :in => 1..255, :allow_nil => true
  validates_numericality_of :value, :only_integer => true, :allow_nil => false
end
