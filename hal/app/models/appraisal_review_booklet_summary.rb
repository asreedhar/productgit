class AppraisalReviewBookletSummary < ActiveRecord::Base

  belongs_to :appraisal_review_booklet
  
  validates_numericality_of :number_of_appraisal_reviews, :only_integer => true, :allow_nil => true
  validates_numericality_of :number_of_appraisal_review_follow_ups, :only_integer => true, :allow_nil => true
  validates_numericality_of :number_of_pages, :only_integer => true, :allow_nil => true
  validates_numericality_of :size_of_booklet, :only_integer => true, :allow_nil => true
  validates_inclusion_of :include_appraisal_review_summary, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_detail, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_high_mileage_summary, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_high_mileage_detail, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_follow_up_summary, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_follow_up_detail, :in => [true,false], :allow_nil => false
  validates_inclusion_of :email_notification, :in => [true,false], :allow_nil => false
  validates_length_of    :email_address, :within => 1..255, :allow_nil => true
  validates_as_email     :email_address, :message => 'is invalid'
  validates_each :email_address do |record, attr, value|
    record.errors.add :email_address, 'not specified but email notification is enabled' if record.email_address.nil? && record.email_notification == true
  end
  
  attr_reader :future_task
  
  def future_task()
    return nil if read_attribute(:appraisal_review_booklet_id).nil?
    @future_tasks = FutureTask.find_by_sql(<<-SQL
        SELECT
              f.*
        FROM
              dbo.future_tasks f
        JOIN  dbo.appraisal_review_booklets b on b.future_task_id = f.id
        WHERE
              b.id = #{read_attribute(:appraisal_review_booklet_id)}
      SQL
    )
    return (@future_tasks.empty? ? nil : @future_tasks.first)
  end
  
end