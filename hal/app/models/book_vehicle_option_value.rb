class BookVehicleOptionValue < CachedModel
  belongs_to :book_option
  belongs_to :book_option_value_type
end
