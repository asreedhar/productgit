class BookCategoryValuation < CachedModel
  belongs_to :book_valuation
  belongs_to :book_category
  has_many   :book_values
end
