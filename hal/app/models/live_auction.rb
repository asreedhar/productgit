class LiveAuction < ActiveRecord::Base
  def third_party
    if read_attribute(:id).nil?
      return nil
    elsif @third_party.nil?
      @third_party = ThirdParty.find(read_attribute(:id))
    end
    return @third_party
  end
  def name
    if third_party().nil?
      return nil
    else
      return third_party().name
    end
  end
end
