class SearchCriteriaInequality < ActiveRecord::Base
  belongs_to :search_criteria_inequality_type
  belongs_to :inequality
  belongs_to :mileage_unit
  def name()
    t = nil
    case read_attribute(:search_criteria_inequality_type_id)
      when 1
        t = read_attribute(:mileage_unit_id) ? 'Total Mileage' : 'Yearly Mileage'
      when 2
        t = 'List Price'
      when 3
        t = 'Unit Cost'
      when 4
        t = 'Book Value'
      when 5
        t = 'Book vs Unit Cost'
      when 6
        t = 'Inventory Age'
    end
    case read_attribute(:inequality_id)
      when 1
        t << " less than #{lower_bound}"
      when 2
        t << " greater than #{lower_bound}"
      when 3
        t << " between #{lower_bound} and #{upper_bound}"
    end
    t
  end
end
