class PlanningCategoryTableCell < ActiveRecord::Base
  belongs_to :planning_category_table_row
  belongs_to :planning_category_table_col
  belongs_to :planning_action_default
  
  protected :attributes_with_quotes
  
  def attributes_with_quotes(include_primary_key = true)
    attributes.inject({}) do |quoted, (name, value)|
      if column = column_for_attribute(name)
        quoted[name] = quote(value, column) unless !include_primary_key && column.primary || name == 'rindex' || name == 'cindex'
      end
      quoted
    end
  end
  
end
