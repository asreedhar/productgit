class MemberInsightTypePreference < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :member
  belongs_to :insight_type
end
