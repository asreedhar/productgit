class PlanningActSmallPrintAdvertisement < ActiveRecord::Base
  validates_presence_of :business_unit_id, :campaign_run_id
end
