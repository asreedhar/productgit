class PlanningModeMenuBar < MenuBar::Base
  
  add_menu do
    with_text 'Age Bucket'
    with_icon " <img src='#{ActionController::Base.asset_host}/images/navarrow.gif' alt='' /> <br />"
    html_options(:id => 'age_bucket_planning_menu')
    # set_tear_off
    add_actions do
      actions = []
      session[:business_unit].planning_age_buckets.each do |planning_age_bucket|
        actions << MenuBar::Action.new(nil) do
          with_text planning_age_bucket.name
          links_to :controller => 'planning', :action => 'index', 'planning_age_bucket[id]' => "#{planning_age_bucket.id}"
        end
      end
      actions
    end
  end
  
  add_menu do
    with_text 'Risk Category'
    with_icon " <img src='#{ActionController::Base.asset_host}/images/navarrow.gif' alt='' /> <br />"
    html_options(:id => 'risk_category_planning_menu')
    # set_tear_off
    add_actions do
      actions = []
      PlanningCategory.business_unit_options(session[:business_unit].id).each do |planning_category|
        actions << MenuBar::Action.new(nil) do
          with_text "#{planning_category.name}"
          links_to :controller => 'planning', :action => 'index', 'planning_category[id]' => "#{planning_category.id}"
        end
      end
      actions
    end
  end
  
  add_menu do
    with_text 'Outstanding'
    with_icon " <img src='#{ActionController::Base.asset_host}/images/navarrow.gif' alt='' /> <br />"
    html_options(:id => 'outstanding_tasks_planning_menu')
    add_menu do
      with_text 'Retail'
      html_options(:id => 'outstanding_retail_tasks_planning_menu')
      # set_tear_off
      add_actions do
        actions = []
        PlanningAction.retail_options.each do |planning_action|
          actions << MenuBar::Action.new(nil) do
            with_text planning_action.name
            links_to :controller => 'planning', :action => 'index', 'planning_action[id]' => "#{planning_action.id}"
          end
        end
        actions
      end
    end
    add_menu do
      with_text 'Wholesale'
      html_options(:id => 'outstanding_wholesale_tasks_planning_menu')
      # set_tear_off
      add_actions do
        actions = []
        PlanningAction.wholesale_options.each do |planning_action|
          actions << MenuBar::Action.new(nil) do
            with_text planning_action.name
            links_to :controller => 'planning', :action => 'index', 'planning_action[id]' => "#{planning_action.id}"
          end
        end
        actions
      end
    end
  end
  
  add_action do
    with_text 'Search'
    links_to :controller => 'planning', :action => 'index', :search => 'true'
  end

end
