class FutureTask < ActiveRecord::Base
  
  belongs_to :future_task_type
  belongs_to :future_task_status
  belongs_to :business_unit
  belongs_to :member
  has_one    :appraisal_review_booklet
  
  validates_presence_of :future_task_type_id
  validates_presence_of :future_task_status_id
  validates_presence_of :business_unit_id
  validates_length_of :title, :within => 1..255, :allow_nil => false
  validates_numericality_of :units_of_work, :only_integer => true, :allow_nil => false
  validates_numericality_of :units_of_work_completed, :only_integer => true, :allow_nil => false
  
  def appraisal_review_booklet_summary(*args)
    return nil unless read_attribute(:future_task_type_id) == 1 && !read_attribute(:id).nil?
    @appraisal_review_booklet_summaries = AppraisalReviewBookletSummary.find_by_sql(<<-SQL
        SELECT
              s.*
        FROM
              dbo.appraisal_review_booklet_summaries s
        JOIN  dbo.appraisal_review_booklets b on b.id = s.appraisal_review_booklet_id
        WHERE
              b.future_task_id = #{read_attribute(:id)}
      SQL
    ) if @appraisal_review_booklet_summaries.nil? || args.length > 0
    return (@appraisal_review_booklet_summaries.empty? ? nil : @appraisal_review_booklet_summaries.first)
  end
  
  def notify_completed_unit_of_work
    update_attribute(:units_of_work_completed, units_of_work_completed+1)
  rescue StandardError => exception
    Log.error("Cannot register work completed! [id=#{id},units_of_work=#{units_of_work}, units_of_work_completed=#{units_of_work_completed}]")
  end
  
end
