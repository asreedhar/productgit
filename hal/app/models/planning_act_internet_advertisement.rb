class PlanningActInternetAdvertisement < ActiveRecord::Base
  validates_presence_of :business_unit_id, :internet_publisher_id
  validates_length_of :description, :minimum => 1, :maximum => 500, :allow_nil => true
  validates_length_of :highlights, :minimum => 1, :maximum => 500,  :allow_nil => true
end
