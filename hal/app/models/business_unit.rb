class BusinessUnit < CachedModel
  has_one :business_unit_preference
  has_one :business_unit_group_preference
  has_one :appraisal_review_preference
  has_many :appraisal_review_bookout_preferences
  has_many :planning_age_buckets, :order => 'position'
  has_many :business_units_upgrades
  has_many :business_units_internet_advertisers
  acts_as_tree
end
