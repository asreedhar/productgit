class AppraisalReviewBookletPreference < ActiveRecord::Base
  belongs_to :business_unit
  validates_inclusion_of :include_appraisal_review_summary, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_detail, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_high_mileage_summary, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_high_mileage_detail, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_follow_up_summary, :in => [true,false], :allow_nil => false
  validates_inclusion_of :include_appraisal_review_follow_up_detail, :in => [true,false], :allow_nil => false
  validates_inclusion_of :email_notification, :in => [true,false], :allow_nil => false
  validates_length_of    :email_address, :within => 1..255, :allow_nil => true
  validates_as_email     :email_address, :message => 'is invalid'
  validates_each :email_address do |record, attr, value|
    record.errors.add :email_address, 'not specified but email notification is enabled' if record.email_address.nil? && record.email_notification == true
  end
end