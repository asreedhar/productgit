class PlanningActionDefaultSPIFF < ActiveRecord::Base
  belongs_to :planning_act_spiff
  validates_presence_of :business_unit_id, :active, :planning_act_spiff_id
end
