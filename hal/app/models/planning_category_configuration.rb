class PlanningCategoryConfiguration < ActiveRecord::Base
  belongs_to :business_unit
  validates_each :high_mileage_lower_threshold, :high_mileage_upper_threshold do |record, attr, value|
    if !value.nil?
      record.errors.add attr, 'less than 0' if value < 0
      record.errors.add attr, 'greater than 2,147,483,647' if value > 2147483647
    end
  end
  validates_each :high_age_lower_threshold, :high_age_upper_threshold do |record, attr, value|
    if !value.nil?
      record.errors.add attr, 'less than 0' if value < 0
      record.errors.add attr, 'greater than 2,147,483,647' if value > 2147483647
    end
  end
  validates_uniqueness_of :is_comparison_configuration, :scope => [:business_unit_id]
end
