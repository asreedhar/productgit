class AppraisalReviewPreference < ActiveRecord::Base
  belongs_to :business_unit
  validates_numericality_of :days_back,        :only_integer => true, :allow_nil => false
  validates_numericality_of :max_distance,     :only_integer => true, :allow_nil => false
  validates_numericality_of :rule_1_threshold, :only_integer => true, :allow_nil => false
  validates_numericality_of :rule_2_threshold, :only_integer => true, :allow_nil => false
  validates_numericality_of :rule_3_threshold, :only_integer => true, :allow_nil => false
  validates_numericality_of :rule_6_threshold, :only_integer => true, :allow_nil => false
  validates_each :days_back, :max_distance, :rule_1_threshold, :rule_2_threshold, :rule_3_threshold, :rule_6_threshold do |record, attr, value|
    record.errors.add attr, 'less than 0' if value < 0
    record.errors.add attr, 'greater than 2,147,483,647' if value > 2147483647
  end
  validates_each :days_back do |record, attr, value|
    record.errors.add attr, 'greater than 30' if value > 30
  end
  validates_each :max_distance do |record, attr, value|
    record.errors.add attr, 'greater than 1000' if value > 1000
  end
end
