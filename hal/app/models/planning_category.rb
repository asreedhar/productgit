class PlanningCategory < CachedModel
  belongs_to :planning_category_light
  def name
    t = ""
    if read_attribute(:id) < 6
      t << planning_category_light.name
      t << " - "
    end
    t << read_attribute(:name)
    t
  end
  def self.all_options
    PlanningCategory.find(:all, :order => "rank")
  end
  def self.alias_options
    PlanningCategory.find(:all, :conditions => "can_be_aliased  = 1", :order => "rank")
  end
  def self.business_unit_options(business_unit_id)
    PlanningCategory.find_by_sql(<<-SQL
        SELECT
            c.*
        FROM
              planning_categories c
        JOIN  business_unit_planning_categories b on b.planning_category_id = c.id
        WHERE
              b.business_unit_id = #{business_unit_id}
        AND   b.active = 1
        AND   b.is_an_alias = 0
        AND   b.is_comparison_configuration = 0
        ORDER BY
              c.rank
      SQL
    )
  end
end
