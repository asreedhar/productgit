class PlanningAgeBoundary < ActiveRecord::Base
  has_and_belongs_to_many :business_unit_planning_categories, :class_name => "BusinessUnitPlanningCategory", :join_table => 'business_unit_planning_categories_planning_age_boundaries'
  has_and_belongs_to_many :planning_action_defaults, :class_name => "PlanningActionDefault", :join_table => 'planning_age_boundaries_planning_action_defaults'
end
