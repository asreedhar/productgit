class PlanningActionDefaultWholesaler < ActiveRecord::Base
  belongs_to :wholesaler
  validates_presence_of :business_unit_id, :active, :wholesaler_id
end
