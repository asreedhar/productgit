class InventorySalesData < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :planning_category
end
