class ModelYear < ActiveRecord::Base

  @@all_years = nil
  
  def self.all_years
    return @@all_years unless @@all_years.nil?
    @@all_years = ModelYear.find_by_sql(<<-SQL
        SELECT
        	m.*
        FROM
        	model_years m
        WHERE
        	m.id <=	CASE WHEN MONTH(GETDATE()) < 9 THEN YEAR(GETDATE()) ELSE YEAR(GETDATE())+1 END
        ORDER BY
        	m.id
      SQL
    )
    return @@all_years
  end
  
end
