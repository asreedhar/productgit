class PlanningActLotPromotion < ActiveRecord::Base
  validates_presence_of :business_unit_id, :active, :name
  validates_length_of :name, :minimum => 1, :allow_nil => false
end
