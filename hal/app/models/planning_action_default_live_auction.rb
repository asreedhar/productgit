class PlanningActionDefaultLiveAuction < ActiveRecord::Base
  belongs_to :live_auction
  validates_presence_of :business_unit_id, :active, :live_auction_id
end
