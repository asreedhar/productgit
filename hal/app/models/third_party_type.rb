class ThirdPartyType < CachedModel
  has_and_belongs_to_many :planning_actions, :class_name => "PlanningAction", :join_table => 'planning_actions_third_party_types'
end
