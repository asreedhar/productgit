class NAAAReportProxyComponent < ProxyComponent::Base

  attr_reader :vin
  attr_reader :vic
  attr_reader :vics
  attr_reader :mmg
  attr_reader :model_year
  attr_reader :vehicle
  attr_reader :vehicle_id
  attr_reader :vehicle_line_id
  attr_reader :vehicle_mileage
  attr_reader :naaa_areas
  attr_reader :naaa_area_id
  attr_reader :naaa_periods
  attr_reader :naaa_period_id
  attr_reader :rule_2
  attr_reader :mileage_0
  attr_reader :mileage_1
  
  def initialize(options)
    # reference variables
    @naaa_areas      = NAAAArea.find(:all)
    @naaa_periods    = NAAAPeriod.find(:all, :order => "rank asc")
    # request variables
    @vin             = options[:vin]
    @vic             = options[:vic]
    @vics            = VehicleIdentificationCode.for_vin(@vin)
    @mmg             = options[:mmg]
    @model_year      = options[:model_year]
    @vehicle_id      = options[:vehicle_id]
    @vehicle_line_id = options[:vehicle_line_id]
    @vehicle_mileage = options[:vehicle_mileage]
    @naaa_area_id    = int_or_default(options, :naaa_area_id, @naaa_areas.first.id)
    @naaa_period_id  = int_or_default(options, :naaa_period_id, @naaa_periods.first.id)
    @rule_2          = options[:rule_2]
    # derived variables
    @vehicle         = @vehicle_id.nil? ? nil : Vehicle.find(@vehicle_id)
    @mileage_0       = @vehicle_mileage.nil? ? nil : ((@vehicle_mileage.to_i/10000)*10000)
    @mileage_1       = @vehicle_mileage.nil? ? nil : (mileage_0+9999)
  end
  
  def int_or_default(map,key,value)
    return ((map[key].nil? || map[key].empty?) ? value : map[key].to_i)
  end
  
  def has_custom_form?
    true
  end
  
  def render_component(context)
    if (@naaa_area_id >= 0 && @naaa_period_id >= 0 && (!@vic.nil? && !@vic.empty?))
      statistic = NAAAStatistic.find_by_sql(<<-SQL
          SELECT
            *
          FROM
            naaa_statistics
          WHERE
              vic = '#{@vic}'
          and naaa_area_id = #{@naaa_area_id}
          and naaa_period_id = #{@naaa_period_id}
        SQL
      ).first
      sample = NAAASample.find_by_sql(<<-SQL
          SELECT
            *
          FROM
            naaa_samples
          WHERE
              vic = '#{vic}'
          and naaa_area_id = #{@naaa_area_id}
          and naaa_period_id = #{@naaa_period_id}
          and low_mileage_range = #{@mileage_0}
          and high_mileage_range = #{@mileage_1}
        SQL
      ).first
    elsif (@naaa_area_id >= 0 && @naaa_period_id >= 0 && (!@mmg.nil? && !@mmg.empty?))
      statistic = NAAAFallbackStatistic.find_by_sql(<<-SQL
          SELECT
            *
          FROM
            naaa_fallback_statistics
          WHERE
              mmg = #{@mmg}
          and model_year = #{@model_year}
          and naaa_area_id = #{@naaa_area_id}
          and naaa_period_id = #{@naaa_period_id}
        SQL
      ).first
      sample = NAAAFallbackSample.find_by_sql(<<-SQL
          SELECT
            *
          FROM
            naaa_fallback_samples
          WHERE
              mmg = #{@mmg}
          and model_year = #{@model_year}
          and naaa_area_id = #{@naaa_area_id}
          and naaa_period_id = #{@naaa_period_id}
          and low_mileage_range = #{@mileage_0}
          and high_mileage_range = #{@mileage_1}
        SQL
      ).first
    end
    if statistic.nil? && sample.nil?
      naaa_report = nil
    else
      naaa_report = NAAAReport.new(statistic, sample, self)
    end
    cmd = proc{render(:partial => 'naaa_report', :object => naaa_report)}
    context.instance_eval(&cmd)
  end
  
  def controller
    "appraisal_review"
  end
  
  def form_name()
    "#{form_partial()}_#{@vin}"
  end
  
  class NAAAReport < ProxyComponent::Data
    attr_reader  :statistic
    attr_reader  :sample
    proxy_reader :vic, :mmg, :model_year, :vehicle_line_id, :area_id, :period_id, :vehicle_mileage, :rule_2
    def initialize(statistic, sample, proxy)
      @statistic = statistic
      @sample    = sample
      @proxy     = proxy
    end
  end
  
end