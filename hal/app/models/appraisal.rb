class Appraisal < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :vehicle, :include => [:vehicle_make, :vehicle_line, :vehicle_segment]
  has_many   :appraisal_valuations, :order => 'sequence_number DESC'
  has_many   :appraisal_actions, :order => 'date_created ASC'
  has_one    :appraisal_insight
  has_one    :customer
  has_one    :appraisal_offer
  belongs_to :person, :foreign_key => "deal_track_sales_person_id"
  has_many   :appraisal_book_valuation_summaries, :class_name => "AppraisalBookValuationSummary"
  has_and_belongs_to_many :book_vehicles, :include => :book_vehicle_options
  def is_appraisal_review(business_unit_id)
    number_of_appraisal_reviews = AppraisalReview.count_by_sql(<<-SQL
      SELECT  COUNT(*)
      FROM    appraisal_reviews a
      JOIN    appraisal_review_preferences p ON a.business_unit_id = p.business_unit_id
      WHERE   a.appraisal_id = #{id}
      AND     a.business_unit_id = #{business_unit_id}
      AND     a.day_id = #{Date.today.strftime('%Y%m%d')}
      AND     a.has_enabled_rule_matches = 1
      AND     a.display = 1
      AND     DATEDIFF(DD, a.appraisal_date, GETDATE()) <= 30 --p.days_back
    SQL
    )
    return (number_of_appraisal_reviews == 1)
  end
  def deal_summary
    txt = ""
    unless deal_track_new_or_used.nil?
      case
        when 0
          txt << "Unknown"
        when 1
          txt << "New"
        else
          txt << "Used"
      end
    end
    unless deal_track_stock_number.nil? || deal_track_stock_number.empty?
      txt << " (#{deal_track_stock_number})" 
    end
    txt
  end
end
