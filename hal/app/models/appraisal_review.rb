class AppraisalReview < ActiveRecord::Base
  belongs_to :appraisal, :include => :person
  belongs_to :business_unit
  belongs_to :book
  belongs_to :book_category
  belongs_to :vehicle, :include => [:vehicle_make, :vehicle_line]
  belongs_to :vehicle_inventory_light
  def appraisal_review_follow_up
    if @appraisal_review_follow_up.nil? && follow_up == true
      @appraisal_review_follow_up = AppraisalReviewFollowUp.find_by_sql(<<-SQL
          SELECT
                f.*
          FROM
                dbo.appraisal_review_follow_ups f
          JOIN  dbo.appraisal_review_follow_up_lists l ON f.appraisal_review_follow_up_list_id = l.id
          JOIN  dbo.appraisals a on f.appraisal_id = a.id AND f.business_unit_id = a.business_unit_id
          WHERE
                f.business_unit_id = #{business_unit_id}
          AND   f.appraisal_id = #{appraisal_id}
          AND   f.day_id = #{day_id}
          AND   f.appraisal_review_follow_up_action_id IS NULL
          AND   l.active = 1
        SQL
      ).first
    end
    @appraisal_review_follow_up
  end
  def interested_business_units
    BusinessUnit.find_by_sql(<<-SQL
        SELECT
              b.*
        FROM
              business_units b
        JOIN  appraisal_review_business_units a ON b.id = a.target_business_unit_id
        WHERE
              a.appraisal_id = #{read_attribute(:appraisal_id)}
        AND   a.day_id = #{read_attribute(:day_id)}
      SQL
    )
  end
  def in_group_distribution_report
    if @report.nil?
      vehicle = Vehicle.find(read_attribute(:vehicle_id))
      @report = InGroupRedistributionReportProxyComponent.new({
        :business_unit_id   => read_attribute(:business_unit_id),
        :vehicle_segment_id => vehicle.vehicle_segment_id,
        :model_year         => vehicle.model_year
      }).component_data()
    end
    return @report
  end
  
  protected :attributes_with_quotes
  
  def attributes_with_quotes(include_primary_key = true)
    ignore_cols = ['rule_1', 'rule_2', 'rule_3', 'rule_4', 'rule_5', 'rule_6', 'rule_7', 'rule_8', 'rule_9', 'has_enabled_rule_matches']
    attributes.inject({}) do |quoted, (name, value)|
      if column = column_for_attribute(name)
        quoted[name] = quote(value, column) unless !include_primary_key && column.primary || ignore_cols.include?(name)
      end
      quoted
    end
  end
  
end
