class CampaignRun < ActiveRecord::Base
  belongs_to :campaign
  belongs_to :vehicle_list, :include => :vehicle_list_items
  validates_presence_of :campaign_id, :deadline, :run_date, :vehicle_list_id
  validates_each :deadline, :run_date do |record,attr,value|
    return if value.nil?
    now = DateTime.parse(Time.new().strftime('%m/%d/%Y'))
    ref = DateTime.parse(value.strftime('%m/%d/%Y'))
    record.errors.add attr, 'is in the past' if ref < now
  end
  validate :validate_dates
  def validate_dates
    errors.add :run_date, "is before deadline" if run_date < deadline
  end
end
