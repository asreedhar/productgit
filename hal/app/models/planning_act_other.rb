class PlanningActOther < ActiveRecord::Base
  validates_presence_of :business_unit_id, :planning_action_id, :active, :name
  validates_length_of :name, :minimum => 1, :allow_nil => false
end
