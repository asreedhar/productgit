class InventoryPlanningTask < ActiveRecord::Base
  belongs_to :inventory_planning_category
  belongs_to :planning_category_table_cell, :include => :planning_action_default
  belongs_to :planning_action
end