class AppraisalReviewFollowUp < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :appraisal
  belongs_to :appraisal_review_follow_up_action
  belongs_to :appraisal_review_follow_up_list
  validates_presence_of :business_unit_id, :appraisal_id, :appraisal_review_follow_up_list, :day_id
  validates_length_of :notes, :within => 1..500, :allow_nil => true
  validates_uniqueness_of :id, :scope=>:appraisal_id
end
