class AppraisalOffer < ActiveRecord::Base
  belongs_to :appraisal
  validates_presence_of :appraisal_id
  validates_numericality_of :customer_offer, :allow_nil => true, :only_integer => true
  validates_numericality_of :old_customer_offer, :allow_nil => true, :only_integer => true
end
