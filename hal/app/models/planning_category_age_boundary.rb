class PlanningCategoryAgeBoundary < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :planning_category
  has_and_belongs_to_many :planning_actions, :class_name => "PlanningAction", :join_table => 'planning_category_age_boundaries_planning_actions'
  validates_each :age_in_days do |record, attr, value|
    record.errors.add attr, 'greater than 100' if value > 100
  end
  validates_uniqueness_of :age_in_days, :scope => [:business_unit_id, :planning_category_id]
end
