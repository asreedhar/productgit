class PlanningActionDefaultLotPromotion < ActiveRecord::Base
  belongs_to :planning_act_lot_promotion
  validates_presence_of :business_unit_id, :active, :planning_act_lot_promotion_id
end
