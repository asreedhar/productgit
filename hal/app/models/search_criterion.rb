class SearchCriterion < ActiveRecord::Base
  belongs_to :business_unit
  has_and_belongs_to_many :colors, :class_name => "Color", :join_table => 'search_criteria_colors'
  has_and_belongs_to_many :search_criteria_inequalities, :class_name => "SearchCriteriaInequality", :join_table => 'search_criteria_search_criteria_inequalities'
  has_and_belongs_to_many :inventory_vehicle_lights, :class_name => "InventoryVehicleLight", :join_table => 'search_criteria_inventory_vehicle_lights'
  has_and_belongs_to_many :model_years, :class_name => "ModelYear", :join_table => 'search_criteria_model_years'
  has_and_belongs_to_many :planning_categories, :class_name => "PlanningCategory", :join_table => 'search_criteria_planning_categories'
  has_and_belongs_to_many :planning_actions, :class_name => "PlanningAction", :join_table => 'search_criteria_planning_actions'
  has_and_belongs_to_many :planning_action_types, :class_name => "PlanningActionType", :join_table => 'search_criteria_planning_action_types'
  has_and_belongs_to_many :planning_age_buckets, :class_name => "PlanningAgeBucket", :join_table => 'search_criteria_planning_age_buckets'
  has_and_belongs_to_many :vehicle_lines, :class_name => "VehicleLine", :join_table => 'search_criteria_vehicle_lines'
  has_and_belongs_to_many :vehicle_makes, :class_name => "VehicleMake", :join_table => 'search_criteria_vehicle_makes'
  has_and_belongs_to_many :search_results, :class_name => "SearchResult", :join_table => 'search_result_search_criteria', :order => 'last_modified'

#  cannot do this as it runs asynchronously on the single available db connection
#  after_save :trg_new_search_results
#  
#  private
#    def trg_new_search_results
#      ActiveRecord::Base.connection.execute("EXECUTE dbo.NewSearchResults #{read_attribute(:id)}", "Create Search Results")
#    end
  
  def search_type_id
    sizes = [
      colors.size,
      search_criteria_inequalities.size,
      inventory_vehicle_lights.size,
      model_years.size,
      planning_categories.size,
      planning_actions.size,
      planning_action_types.size,
      planning_age_buckets.size,
      vehicle_lines.size,
      vehicle_makes.size
    ]
    return  1 if sizes == [1,0,0,0,0,0,0,0,0,0]
    return  2 if sizes == [0,1,0,0,0,0,0,0,0,0]
    return  3 if sizes == [0,0,1,0,0,0,0,0,0,0]
    return  4 if sizes == [0,0,0,1,0,0,0,0,0,0]
    return  5 if sizes == [0,0,0,0,1,0,0,0,0,0]
    return  6 if sizes == [0,0,0,0,0,1,0,0,0,0]
    return  7 if sizes == [0,0,0,0,0,0,1,0,0,0]
    return  8 if sizes == [0,0,0,0,0,0,0,1,0,0]
    return  9 if sizes == [0,0,0,0,0,0,0,0,1,0]
    return 10 if sizes == [0,0,0,0,0,0,0,0,0,1]
    return 11
  end
  
  def search_type()
    t = nil
    case search_type_id()
      when 1
        t = "#{colors.first.name} Color"
      when 2
        t = search_criteria_inequalities.first.name
      when 3
        t = "#{inventory_vehicle_lights.first.name} Light"
      when 4
        t = "{model_years.first.id} Model Year"
      when 5
        t = planning_category = planning_categories.first.name
      when 6
        planning_action = planning_actions.first
        t = "#{planning_action.planning_action_type.name} - #{planning_action.name}"
      when 7
        t = planning_action_types.first.name
      when 8
        t = planning_age_buckets.first.name
      when 9
        vehicle_line = vehicle_lines.first
        t = "#{vehicle_line.vehicle_make.name} #{vehicle_line.name}"
      when 10
        t = vehicle_make.name
      else
        t = 'Custom Search Criteria'
    end
    t
  end
  
  def self.has_search_criterion_params?(params)
    [:search_criterion, :model_year, :color, :vehicle_make, :vehicle_line,
     :planning_category, :planning_action, :planning_action_type, :planning_age_bucket,
     :inventory_vehicle_light, :total_mileage, :yearly_mileage, :list_price, :unit_cost,
     :book_value, :book_vs_cost, :inventory_age].each do |key|
      return true if neither_nil_nor_empty(params,key)
    end
    return false
  end
  
  def self.search_criterion_builder(business_unit_id, params)
    search_criterion = SearchCriterion.new() do |c|
      c.business_unit_id = business_unit_id
      c.active = true
    end
    if neither_nil_nor_empty(params,:search_criterion)
      if neither_nil_nor_empty(params[:search_criterion],:overstock_models)
        search_criterion.overstock_models = params[:search_criterion][:overstock_models]
      end
    end
    # model_year[id]
    if neither_nil_nor_empty(params,:model_year)
      search_criterion.model_years << ModelYear.find(params[:model_year][:id])
    end
    # color[id]
    if neither_nil_nor_empty(params,:color)
      search_criterion.colors << Color.find(params[:color][:id])
    end
    # vehicle_make[id]
    if neither_nil_nor_empty(params,:vehicle_make)
      search_criterion.vehicle_makes << VehicleMake.find(params[:vehicle_make][:id])
    end
    # vehicle_line[id]
    if neither_nil_nor_empty(params,:vehicle_line)
      search_criterion.vehicle_lines << VehicleLine.find(params[:vehicle_line][:id])
    end
    # planning_category[id]
    if neither_nil_nor_empty(params,:planning_category)
      search_criterion.planning_categories << PlanningCategory.find(params[:planning_category][:id])
    end
    # planning_action[id]
    if neither_nil_nor_empty(params,:planning_action)
      search_criterion.planning_actions << PlanningAction.find(params[:planning_action][:id])
    end
    # planning_action_type[id]
    if neither_nil_nor_empty(params,:planning_action_type)
      search_criterion.planning_action_types << PlanningActionType.find(params[:planning_action_type][:id])
    end
    # planning_age_bucket[id]
    if neither_nil_nor_empty(params,:planning_age_bucket)
      search_criterion.planning_age_buckets << PlanningAgeBucket.find(params[:planning_age_bucket][:id])
    end
    # inventory_vehicle_light[id]
    if neither_nil_nor_empty(params,:inventory_vehicle_light)
      search_criterion.inventory_vehicle_lights << InventoryVehicleLight.find(params[:inventory_vehicle_light][:id])
    end
    # total_mileage
    # yearly_mileage
    # list_price
    # unit_cost
    # book_value
    # book_vs_cost
    [:total_mileage, :yearly_mileage, :list_price, :unit_cost, :book_value, :book_vs_cost, :inventory_age].each do |inequality|
      if neither_nil_nor_empty(params,inequality)
        if neither_nil_nor_empty(params[inequality],:lower_bound) || neither_nil_nor_empty(params[inequality],:upper_bound)
          search_criterion.search_criteria_inequalities << SearchCriteriaInequality.new(params[inequality])
        end
      end      
    end
    return search_criterion
  end
  
  def self.neither_nil_nor_empty(map,key)
    if map.member?(key)
      obj = map[key]
      return !obj.nil? && !obj.empty?
    else
      return false
    end
  end
  
end
