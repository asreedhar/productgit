class BookVehicleOption < CachedModel
  belongs_to :book_vehicle
  belongs_to :book_option
  has_many   :book_vehicle_option_values
end
