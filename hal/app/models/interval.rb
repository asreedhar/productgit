class Interval < ActiveRecord::Base
  
  include Comparable
  
  belongs_to :interval_type
  validates_presence_of :interval_type_id
  validates_numericality_of :day_of_week,  :only_integer => true, :allow_nil => true
  validates_numericality_of :day_of_month, :only_integer => true, :allow_nil => true
  validate :validate_day_of_x
  
  def validate_day_of_x
    check_nil, check_not_nil = [], []
    check_date_is_in_future = false
    case read_attribute(:interval_type_id)
      when 1,4
        check_nil = [:day_of_week,:day_of_month]
        check_not_nil = [:day_of_year]
        check_date_is_in_future = true
      when 2
        check_nil = [:day_of_month,:day_of_year]
        check_not_nil = [:day_of_week]
      when 3
        check_nil = [:day_of_week,:day_of_year]
        check_not_nil = [:day_of_month]
    end
    check_nil.each do |attr|
      errors.add attr, 'should be nil' unless read_attribute(attr).nil?
    end
    check_not_nil.each do |attr|
      errors.add attr, 'is not specified' if read_attribute(attr).nil?
    end
    if check_date_is_in_future && !read_attribute(:day_of_year).nil?
      errors.add :day_of_year, 'is in the past' if read_attribute(:day_of_year) < Time.new()
    end
  end
  
  def <=>(other)
    # quick arg validation
    raise StandardError, "Comparing Interval with non-Interval" unless other.instance_of?(Interval)
    raise StandardError, "Comparing different Interval types" unless interval_type_id == other.interval_type_id
    # compare
    result = 0
    case interval_type_id
      when 1,4
        result = (day_of_year <=> other.day_of_year)
      when 2
        result = (day_of_week <=> other.day_of_week)
      when 3
        result = (day_of_month <=> other.day_of_month)
    end
    return result
  end
  
end
