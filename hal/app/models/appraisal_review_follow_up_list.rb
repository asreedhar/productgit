class AppraisalReviewFollowUpList < ActiveRecord::Base
  has_many :appraisal_review_follow_ups
end
