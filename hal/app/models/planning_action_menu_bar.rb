class PlanningActionMenuBar < MenuBar::Base
  
  add_action do
    with_text 'Strategies: '
    links_to {}
  end
  
  add_menu do
    with_text 'Retail'
    html_options(:id => 'retail_planning_action_menu')
    set_tear_off
    add_action do
      with_text 'Print Ads'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '2', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 1 && (@planning_action_id.nil? || @planning_action_id == 2) ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'SPIFF'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '5', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 1 && @planning_action_id == 5 ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'Lot Promote'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '4', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 1 && @planning_action_id == 4 ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'Internet Ads'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '11', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 1 && @planning_action_id == 11 ? {} : { :active => 'false' } }
      show_if proc{has_internet_publishers?(session[:business_unit].id)}
    end
    add_action do
      with_text 'Internet Auctions'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '3', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 1 && @planning_action_id == 3 ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'Other'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '6', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 1 && @planning_action_id == 6 ? {} : { :active => 'false' } }
    end
  end
  
  add_menu do
    with_text 'Wholesale'
    html_options(:id => 'wholesale_planning_action_menu')
    set_tear_off
    add_action do
      with_text 'Internet Auction'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '8', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 2 && (@planning_action_id.nil? || @planning_action_id == 8) ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'In Group'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '12', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 2 && @planning_action_id == 12 ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'Live Auction'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '7', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 2 && @planning_action_id == 7 ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'Wholesaler'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '9', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 2 && @planning_action_id == 9 ? {} : { :active => 'false' } }
    end
    add_action do
      with_text 'Other'
      links_to_remote(proc{ { :controller => 'planning', :action => 'refresh_planning_act', :planning_action_id => '10', :inventory_id => @inventory_id.to_s } }, { })
      highlights_on proc{ @planning_action_type_id == 2 && @planning_action_id == 10 ? {} : { :active => 'false' } }
    end
  end
  
end
