class PlanningActionDefaultInternetAuction < ActiveRecord::Base
  belongs_to :third_party
  validates_presence_of :business_unit_id, :active, :third_party_id
end
