class PlanningAct < ActiveRecord::Base
  belongs_to :planning_act_type
  has_and_belongs_to_many :planning_act_reprices, :class_name => "PlanningActReprice", :join_table => 'planning_acts_planning_act_reprices'
  has_and_belongs_to_many :planning_act_lot_promotions, :class_name => "PlanningActLotPromotion", :join_table => 'planning_acts_planning_act_lot_promotions'
  has_and_belongs_to_many :planning_act_spiffs, :class_name => "PlanningActSPIFF", :join_table => 'planning_acts_planning_act_spiffs'
  has_and_belongs_to_many :planning_act_others, :class_name => "PlanningActOther", :join_table => 'planning_acts_planning_act_others'
  has_and_belongs_to_many :planning_act_live_auctions, :class_name => "PlanningActLiveAuction", :join_table => 'planning_acts_planning_act_live_auctions'
  has_and_belongs_to_many :planning_act_wholesalers, :class_name => "PlanningActWholesaler", :join_table => 'planning_acts_planning_act_wholesalers'
  has_and_belongs_to_many :planning_act_solds, :class_name => "PlanningActSold", :join_table => 'planning_acts_planning_act_solds'
  has_and_belongs_to_many :planning_act_small_print_advertisements, :class_name => "PlanningActSmallPrintAdvertisement", :join_table => 'planning_acts_planning_act_small_print_advertisements'
  has_and_belongs_to_many :planning_act_internet_auctions, :class_name => "PlanningActInternetAuction", :join_table => 'planning_acts_planning_act_internet_auctions'
  validates_presence_of :planning_action_id, :inventory_id, :created
  validates_length_of :notes, :minimum => 1, :allow_nil => true
end
