class PlanningCategoryTable < ActiveRecord::Base
  belongs_to :business_unit
  belongs_to :planning_category
  has_many   :planning_category_table_rows, :order => "rindex"
  has_many   :planning_category_table_cols, :order => "cindex"
end
