class PlanningActInternetAuction < ActiveRecord::Base
  validates_presence_of :business_unit_id, :third_party_id
  validates_length_of :announcements, :within => 1..500, :allow_nil => true
  validates_each :send_pictures, :send_announcements do |record, attr, value|
    if value.nil?
      record.errors.add attr, 'is not set'
    else
      unless value.instance_of?(TrueClass) || value.instance_of?(FalseClass)
        record.errors.add attr, 'not boolean'
      end
    end
  end
end
