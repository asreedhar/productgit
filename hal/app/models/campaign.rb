class Campaign < ActiveRecord::Base
  belongs_to :campaign_type
  belongs_to :business_unit
  belongs_to :third_party
  belongs_to :run_date_interval, :class_name => "Interval", :foreign_key => "run_date_interval_id"
  belongs_to :deadline_interval, :class_name => "Interval", :foreign_key => "deadline_interval_id"
  has_one    :campaign_deadline
  validates_length_of   :name, :minumum => 1, :maximum => 50
  validates_presence_of :name, :campaign_type_id, :business_unit_id, :run_date_interval_id, :deadline_interval_id
  validate :validate_third_party, :validate_intervals
  def validate_third_party
    if read_attribute(:campaign_type_id) == 1
      errors.add :third_party_id, 'is not specified' if read_attribute(:third_party_id).nil?
    end
  end
  def validate_intervals
    return if read_attribute(:run_date_interval_id).nil? || read_attribute(:deadline_interval_id).nil?
    if run_date_interval.interval_type_id != deadline_interval.interval_type_id
      errors.add :run_date_interval_id, 'has different interval type to deadline'
    else
      errors.add :run_date_interval_id, 'is before deadline' if run_date_interval < deadline_interval
    end
  end
end

