class AppraisalReviewBookletSummaryProxyComponentCollection < ProxyComponent::Collection
  
  def initialize(i)
    super(i)
  end
  
  def controller
    'appraisal_review_booklet'
  end
  
  def proxy_url(records)
    { :controller => controller(), :action => action(), :appraisal_review_booklet_summary_id => records.collect{|r| r.id} }
  end
  
  def locals()
    { :number_of_appraisal_review_booklet_summaries => records().length(), :months_ago => id()-1 }
  end
  
end