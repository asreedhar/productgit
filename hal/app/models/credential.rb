class Credential < ActiveRecord::Base
  belongs_to :credential_authority
  belongs_to :member
  validates_presence_of :credential_authority_id, :member_id
  validates_length_of :username, :minimum => 1, :maxcimum => 25, :allow_nil => true
  validates_length_of :password, :minimum => 1, :maxcimum => 25, :allow_nil => true
end
