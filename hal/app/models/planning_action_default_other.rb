class PlanningActionDefaultOther < ActiveRecord::Base
  belongs_to :planning_act_other
  validates_presence_of :business_unit_id, :active, :planning_act_other_id
end
