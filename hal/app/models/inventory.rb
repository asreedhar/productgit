class Inventory < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :business_unit
  has_one    :inventory_advertising_attribute
  has_one    :inventory_planning_attribute
  has_many :inventory_planning_categories, :order => "inventory_planning_categories.valid_from", :conditions => "inventory_planning_categories.invalidated IS NULL AND inventory_planning_categories.valid_until > GETDATE()"
  has_many :inventory_planning_tasks, :through => :inventory_planning_categories, :conditions => "inventory_planning_tasks.invalidated IS NULL AND inventory_planning_tasks.satisfied IS NULL AND inventory_planning_tasks.valid_until > GETDATE()", :order => "inventory_planning_tasks.planning_action_id", :include => :planning_action
  has_many :inventory_book_valuation_summaries
  
  # TODO: This is, most-likely, very wrong and needs to be addressed
  def acquire_type
    if self.trade_or_purchase == 2
      "Trade"
    else 
      "Purchase"
    end
  end
  
  def age
    Date.today() - Date.parse(inventory_received_date.strftime('%Y-%m-%d'))
  end
  
  def planning_category_name
    planning_category = PlanningCategory.find_by_sql(<<-SQL
        SELECT
              c.*
        FROM
              planning_categories c
        JOIN  business_unit_planning_categories b on c.id = b.planning_category_id
        JOIN  inventory_planning_categories i on i.business_unit_planning_category_id = b.id
        WHERE
              i.inventory_id = #{id}
      SQL
    ).first
    name = ""
    case planning_category.planning_category_light_id
      when 1
        name << 'Green '
      when 2
        name << 'Yellow '
      when 3
        name << 'Red '
    end
    name << planning_category.name
    return name
  end
  
  def primary_book_summary
    return nil if inventory_book_valuation_summaries.nil?
    return inventory_book_valuation_summaries.detect{|c| c.is_primary_book_value == true}
  end
  
  def primary_book_name
    p = primary_book_summary
    return (p.nil? ? "" : p.book_category.book.name)
  end
  
  def primary_book_category_name
    p = primary_book_summary
    return (p.nil? ? "" : p.book_category.name)
  end
  
  def primary_book_value
    p = primary_book_summary
    return (p.nil? ? "" : p.book_value)
  end
  
  def primary_book_value_vs_unit_cost
    p = primary_book_summary
    return ((p.nil? || p.book_value.nil?) ? "" : p.book_value - unit_cost)
  end
  
  def active_tasks
    return inventory_planning_tasks
  end
  
  def active_task_by_action( name )
    return inventory_planning_tasks.select{|t| t.planning_action.name == name}
  end

end
