class PlanningCategoryTableRow < ActiveRecord::Base
  belongs_to :planning_category_table
  belongs_to :planning_action
  has_many   :planning_category_table_cells, :order => 'cindex'
end
