class AppraisalSummaryProxyComponentCollection < ProxyComponent::Collection
  
  def initialize(i)
    super(i)
  end
  
  def controller
    'appraisal_review'
  end
  
  def proxy_url(records)
    { :controller => controller(), :action => action(), :appraisal_id => records.collect{|r| r.id} }
  end
  
  def locals()
    { :number_of_appraisals => records().length() }
  end
  
  def is_table?
    true
  end
  
  def colspan
    13
  end
  
end
