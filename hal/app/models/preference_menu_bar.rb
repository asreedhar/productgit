class PreferenceMenuBar < MenuBar::Base
  
  Development = false
  
  add_action do
    with_text 'Campaign'
    html_options(:id => 'campaign_menu')
    links_to :controller => "/management/campaign", :action => 'start', :mode => 'page', :container => 'preference_menu_bar_container'
    ['add', 'open', 'edit', 'delete', 'save', 'cancel'].each do |action|
      highlights_on :controller => "management/campaign", :action => action, :mode => 'page', :container => 'preference_menu_bar_container'
    end
  end if Development
  
  add_menu do
    with_text 'Third Parties'
    html_options(:id => 'third_party_menu')
    set_tear_off
    [['Media Outlets', 'print_publisher'], ['Wholesalers', 'wholesaler'], ['Live Auctions', 'live_auction'], ['Internet Auctions', 'internet_auction']].each do |pair|
      add_action do
        with_text pair[0]
        html_options(:id => "#{pair[1]}_third_party_menu")
        links_to :controller => "/management/#{pair[1]}", :action => 'start', :mode => 'page'
        ['add', 'open', 'edit', 'delete', 'save', 'cancel'].each do |action|
          highlights_on :controller => "management/#{pair[1]}", :action => action, :mode => 'page'
        end
      end
    end
  end if Development
  
  add_menu do
    with_text 'Planning'
    html_options(:id => 'planning_menu')
    set_tear_off
    add_menu do
      with_text 'Reference Data'
      html_options(:id => 'planning_act_management')
      set_tear_off
      [['Lot Promotions', 'planning_act_lot_promotion', nil], ['SPIFF', 'planning_act_spiff', nil], ['Other - Retail', 'planning_act_other', '6'], ['Other - Wholesale', 'planning_act_other', '10']].each do |triple|
        add_action do
          with_text triple[0]
          html_options(:id => "#{triple[1]}_planning_act_menu")
          links_to({ :controller => "/management/#{triple[1]}", :action => 'start', :mode => 'page', :planning_action_id => triple[2] }.reject{|key, value| value.nil?})
          ['add', 'open', 'edit', 'delete', 'save', 'cancel'].each do |action|
            highlights_on({ :controller => "management/#{triple[1]}", :action => action, :mode => 'page', :planning_action_id => triple[2] }.reject{|key, value| value.nil?})
          end
        end
      end
    end
    add_action do
      with_text 'Green - Optimal'
      html_options(:id => 'planning_category_1_schedule')
      links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '1'
    end
    add_action do
      with_text 'Green - Non-Optimal'
      html_options(:id => 'planning_category_2_schedule')
      links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '2'
    end
    add_action do
      with_text 'Yellow - Low Sales History'
      html_options(:id => 'planning_category_3_schedule')
      links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '3'
    end
    add_action do
      with_text 'Yellow - Poor Performance'
      html_options(:id => 'planning_category_4_schedule')
      links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '4'
    end
    add_action do
      with_text 'Red - Poor Performance'
      html_options(:id => 'planning_category_5_schedule')
      links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '5'
    end
    add_menu do
      with_text 'High Mileage'
      html_options(:id => 'planning_category_6_menu')
      set_tear_off
      add_action do
        with_text 'Configuration'
        html_options(:id => 'planning_category_6_configuration')
        links_to :controller => '/planning_category_configuration', :action => 'planning_category', :planning_category_id => '6'
      end
      add_action do
        with_text 'Schedule'
        html_options(:id => 'planning_category_6_schedule')
        links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '6'
      end
    end
    add_menu do
      with_text 'Excessive Mileage'
      html_options(:id => 'planning_category_7_menu')
      set_tear_off
      add_action do
        with_text 'Configuration'
        html_options(:id => 'planning_category_7_configuration')
        links_to :controller => '/planning_category_configuration', :action => 'planning_category', :planning_category_id => '7'
      end
      add_action do
        with_text 'Schedule'
        html_options(:id => 'planning_category_7_schedule')
        links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '7'
      end
    end
    add_menu do
      with_text 'High Age'
      html_options(:id => 'planning_category_8_menu')
      set_tear_off
      add_action do
        with_text 'Configuration'
        html_options(:id => 'planning_category_8_configuration')
        links_to :controller => '/planning_category_configuration', :action => 'planning_category', :planning_category_id => '8'
      end
      add_action do
        with_text 'Schedule'
        html_options(:id => 'planning_category_8_schedule')
        links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '8'
      end
    end
    add_menu do
      with_text 'Excessive Age'
      html_options(:id => 'planning_category_9_menu')
      set_tear_off
      add_action do
        with_text 'Configuration'
        html_options(:id => 'planning_category_9_configuration')
        links_to :controller => '/planning_category_configuration', :action => 'planning_category', :planning_category_id => '9'
      end
      add_action do
        with_text 'Schedule'
        html_options(:id => 'planning_category_9_menu')
        html_options(:id => 'planning_category_9_schedule')
        links_to :controller => '/planning_category_table', :action => 'planning_category', :planning_category_id => '9'
      end
    end
  end if Development
  
  add_menu do
    with_text 'Targets'
    html_options(:id => 'targets_menu')
    set_tear_off
    add_action do
      with_text 'Appraisal Closing Rate'
      html_options(:id => 'appraisal_closing_rate')
      links_to :controller => '/insight_preferences', :action => 'appraisal_closing_rate'
    end
    add_action do
      with_text 'Trade-In Inventory Analyzed'
      html_options(:id => 'trade_in_inventory_analyzed')
      links_to :controller => '/insight_preferences', :action => 'trade_in_inventory_analyzed'
    end
    add_action do
      with_text 'Average Immediate Wholesale Profit'
      html_options(:id => 'average_immediate_wholesale_profit')
      links_to :controller => '/insight_preferences', :action => 'average_immediate_wholesale_profit'
    end
    add_action do
      with_text 'Make-A-Deal'
      html_options(:id => 'make_a_deal_insight_threshold')
      links_to :controller => '/insight_preferences', :action => 'make_a_deal'
      show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 21}}
    end
  end
  
  add_menu do
    with_text 'Make a Deal'
    html_options(:id => 'make_a_deal_menu')
    set_tear_off
    show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 21}}
    add_action do
      with_text 'Rules'
      html_options(:id => 'make_a_deal_preferences')
      links_to :controller => '/appraisal_review_preferences', :action => 'preferences'
    end
    add_menu do
      with_text 'Book Settings'
      html_options(:id => 'make_a_deal_bookout_preferences')
      set_tear_off
      add_action do
        with_text 'Domestic'
        html_options(:id => 'make_a_deal_bookout_preferences_domestic')
        links_to :controller => '/appraisal_review_preferences', :action => 'bookout_preferences', :vehicle_category_id => '1'
      end
      add_action do
        with_text 'Import'
        html_options(:id => 'make_a_deal_bookout_preferences_import')
        links_to :controller => '/appraisal_review_preferences', :action => 'bookout_preferences', :vehicle_category_id => '2'
      end
      add_action do
        with_text 'HiLine'
        html_options(:id => 'make_a_deal_bookout_preferences_hiline')
        links_to :controller => '/appraisal_review_preferences', :action => 'bookout_preferences', :vehicle_category_id => '3'
      end
    end
  end
  
  add_menu do
  	with_text 'Internet Advertising Accelerator'
  	html_options(:id => "internet_advertising_menu")
  	set_tear_off
  	show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 19} && session[:business_unit].business_unit_preference(true).pricing_package_id == 6}
  	add_action do
  		with_text "Pricing Preferences"
  		html_options(:id => "pricing_preferences")
  		links_to :action => :redirect_to_external_url, :external_url => '/pricing/', :token => "DEALER_SYSTEM_COMPONENT", :pageName => "Pages/Preferences/"
  	end
  end
  
  add_menu do
  	with_text 'MAX Merchandising'
	html_options(:id => "max_marketing_menu")
  	set_tear_off
  	show_if lambda{session[:business_unit].business_units_upgrades.detect{|u| u.upgrade_id == 23}}
  	add_action do
  		with_text "MAX Merchandising"
  		html_options(:id => "max_marketing_preferences")
  		links_to :action => :redirect_to_external_url, :external_url => '/pricing/', :token => "DEALER_SYSTEM_COMPONENT", :pageName => "Pages/MaxMarketing/Preferences/Dealer/Default.aspx"
  	end
  end

  add_menu do
  	with_text 'Certified Program'
  	html_options(:id => "certified_program_menu")
  	set_tear_off
  	add_action do
  		with_text "Certified Preferences"
  		html_options(:id => "cpo_preferences")
  		links_to :action => :redirect_to_external_url, :external_url => '/CertifiedProgram/Dealer/Pages/', :token => 'DEALER_SYSTEM_COMPONENT'
  	end
  end
      
end
