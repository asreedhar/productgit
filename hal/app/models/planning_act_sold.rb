class PlanningActSold < ActiveRecord::Base
  validates_presence_of :business_unit_id, :buyer, :date
  validates_length_of :buyer, :minimum => 1, :allow_nil => false
end
