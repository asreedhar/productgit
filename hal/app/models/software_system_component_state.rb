class SoftwareSystemComponentState < ActiveRecord::Base
  belongs_to :software_system_component
  belongs_to :authorized_member, :class_name => 'Member', :foreign_key => 'authorized_member_id'
  belongs_to :dealer_group, :class_name => 'BusinessUnit', :foreign_key => 'dealer_group_id'
  belongs_to :dealer, :class_name => 'BusinessUnit', :foreign_key => 'dealer_id'
  belongs_to :member
end
