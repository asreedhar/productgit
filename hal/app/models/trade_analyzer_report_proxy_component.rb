class TradeAnalyzerReportProxyComponent < ProxyComponent::Base

  attr_reader :business_unit_id
  attr_reader :number_of_weeks
  attr_reader :make
  attr_reader :line
  attr_reader :series
  attr_reader :include_dealer_group
  attr_reader :inventory_type
  
  def initialize(options)
      @business_unit_id     = options[:business_unit_id]
      @number_of_weeks      = options[:number_of_weeks]
      @make                 = options[:make]
      @line                 = options[:line]
      @series               = options[:series]
      @include_dealer_group = options[:include_dealer_group]
      @inventory_type       = options[:inventory_type]
  end
  
  def proxy_component_object()
    TradeAnalyzerReport.new(nil, nil, self)
  end
  
  def render_component(context)
    trade_analyzer_report = TradeAnalyzerReport.new(
      ActiveRecord::Base.connection.select_all(
        sprintf(
          "EXECUTE GetTradeAnalyzerReport %d, %d, '%s', '%s', %s, %s, %s",
          @business_unit_id,
          @number_of_weeks,
          @make,
          @line,
          @include_dealer_group == "true" ? 1 : 0,
          @inventory_type == 'New' ? 1 : 2,
          (@series.nil? || @series.empty?) ? "null" : "'#{@series}'"
        )
      ).first,
      ActiveRecord::Base.count_by_sql(<<-SQL
          SELECT  COUNT(*) UnitsInStock
          FROM    dbo.Inventory_A6 I
          JOIN    [IMT].dbo.MakeModelGrouping M ON M.GroupingDescriptionID = I.GroupingDescriptionID
          WHERE   BusinessUnitID = #{@business_unit_id}
          AND     M.Make = '#{@make}'
          AND     M.Model = '#{@line}'
        SQL
      ),
      self
    )
    the_component_partial = component_partial()
    cmd = proc{render(:partial => the_component_partial, :object => trade_analyzer_report)}
    context.instance_eval(&cmd)
  end
  
  def controller
    "appraisal_review"
  end
  
  def action_params
    {
      :number_of_weeks      => @number_of_weeks,
      :make                 => @make,
      :line                 => @line,
      :series               => @series,
      :include_dealer_group => @include_dealer_group,
      :inventory_type       => @inventory_type
    }
  end
  
  class TradeAnalyzerReport < ProxyComponent::Data
  
    attr_reader :business_unit_id
    attr_reader :number_of_weeks
    attr_reader :make
    attr_reader :line
    attr_reader :series
    attr_reader :include_dealer_group
    attr_reader :inventory_type
    attr_reader :results
    
    proxy_reader :business_unit_id, :number_of_weeks, :make, :line, :series, :include_dealer_group, :inventory_type
    
    row_reader :units_sold, :average_gross_profit, :average_back_end_gross_profit, :average_days, :average_mileage, :total_sales_price, :total_gross_margin, :no_sales, :total_back_end_gross_profit
    
    def initialize(row,units_in_stock,proxy)
      @row = row
      @units_in_stock = units_in_stock
      @proxy = proxy
    end
    def results
      @row
    end
    def vehicle_segment_name
      @row['VehicleGroupingDescription']
    end
    def vehicle_segment_id
      @row['GroupingID']
    end
    def units_in_stock
      @units_in_stock
    end
    
  end
  
end