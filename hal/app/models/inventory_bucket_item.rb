class InventoryBucketItem < ActiveRecord::Base

  belongs_to :inventory_bucket
  belongs_to :business_unit
  
  acts_as_list :scope => :inventory_bucket
  
  validates_numericality_of :inventory_bucket_id, :only_integer => true, :allow_nil => false
  validates_numericality_of :range_id,            :only_integer => true, :allow_nil => false
  validates_numericality_of :low,                 :only_integer => true, :allow_nil => false
  validates_numericality_of :high,                :only_integer => true, :allow_nil => true
  validates_numericality_of :lights,              :only_integer => true, :allow_nil => false
  validates_numericality_of :business_unit_id,    :only_integer => true, :allow_nil => false
  validates_numericality_of :position,            :only_integer => true, :allow_nil => false
  
  validates_uniqueness_of :range_id, :scope => [:business_unit_id, :inventory_bucket_id]
  validates_uniqueness_of :position, :scope => [:business_unit_id, :inventory_bucket_id]
  
  # low <= high unless high.nil?
  validate do |record|
    record.errors.add("low", "is greater than high value") if !record.high.nil? && record.low > record.high
  end
  
end
