class BookVehicle < CachedModel
  belongs_to :book
  has_many   :book_vehicle_options
end
