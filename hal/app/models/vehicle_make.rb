class VehicleMake < CachedModel
  belongs_to :vehicle_category
  def self.all_options
    VehicleMake.find_by_sql(<<-SQL
        SELECT
            *
        FROM
            vehicle_makes
        ORDER BY
            name
      SQL
    )
  end
end
