class PlanningActionDefaultReprice < ActiveRecord::Base
  belongs_to :planning_action_default_reprice_type
  validates_presence_of :business_unit_id, :active, :planning_action_default_reprice_type_id
  validates_numericality_of :amount, :allow_nil => false
end
