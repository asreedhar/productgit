class VehicleIdentificationCode < CachedModel
  has_many :vehicle_identification_number_prefixes
  def self.for_vin(vin)
    find_by_sql(<<-SQL
      SELECT
            VIC.*
      FROM
            vehicle_identification_codes VIC
      JOIN  vehicle_identification_number_prefixes PFX ON VIC.id = PFX.vehicle_identification_code_id
      WHERE
            PFX.vehicle_identification_number_prefix = '#{vin[0,8]}0#{vin[9,1]}'
    SQL
    )
  end
end
