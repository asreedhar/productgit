module PlanningHelper
  
  def find_inventory(business_unit_id, params)
    # short cut for list in request
    if params.member?(:search_result_id) && !params[:search_result_id].nil?
      return [
        SearchCriterion.new,
        SearchResult.find(params[:search_result_id]).vehicle_list_id,
        VehicleListItem.count_by_sql(<<-SQL
          SELECT  COUNT(*)
          FROM    vehicle_list_items i
          JOIN    vehicle_lists l ON l.id = i.vehicle_list_id
          JOIN    search_results s ON s.vehicle_list_id = l.id
          WHERE   s.id = #{params[:search_result_id]}
        SQL
        )
      ]
    end
    # short cut for list in request
    if params.member?(:vehicle_list_id) && !params[:vehicle_list_id].nil?
      return [
        SearchCriterion.new,
        params[:vehicle_list_id],
        VehicleListItem.count_by_sql(<<-SQL
          SELECT  COUNT(*)
          FROM    vehicle_list_items i
          JOIN    vehicle_lists l ON l.id = i.vehicle_list_id
          WHERE   l.id = #{params[:vehicle_list_id]}
        SQL
        )
      ]
    end
    # perform a search
    search_criterion = SearchCriterion.search_criterion_builder(session[:business_unit].id, params)
    SearchCriterion.transaction() do
      if search_criterion.save
        search_result_rs = ActiveRecord::Base.connection.select_all(<<-SQL
           EXECUTE dbo.NewSearchResults #{search_criterion.id}
          SQL
        ).first
        return [
          search_criterion,
          SearchResult.find(search_result_rs['SearchResultID']).vehicle_list_id,
          search_result_rs['NumberOfVehicles']
        ]
      else
        record_errors(search_criterion)
      end
    end
    # error case
    return [nil,nil,0]
  end
  
  def load_inventory(search_result_id, vehicle_list_id)
    unless search_result_id.nil?
      vehicle_list_id = SearchResult.find(search_result_id).vehicle_list_id
    end
    ids = ActiveRecord::Base.connection.select_all(<<-SQL
        SELECT    i.vehicle_id
        FROM      vehicle_list_items i
        WHERE     i.vehicle_list_id = #{vehicle_list_id}
        ORDER BY  i.position
      SQL
    ).collect{|row| row['vehicle_id']}.join(',')
    if ids.length < 1
      inventory = []
    else
       inventory = Inventory.find(
         :all,
         :conditions => "inventory.active = 1 AND inventory.inventory_type_id = 2 AND inventory.vehicle_id IN (#{ids})",
         :order      => "inventory.inventory_received_date",
         :include    => [:vehicle,:inventory_planning_tasks,:inventory_book_valuation_summaries]
        )
    end
    return inventory
  end
  
  def planning_action_defaults(inventory, planning_action_id)
    inventory_planning_task = inventory.inventory_planning_tasks.detect{|t| t.planning_action_id == planning_action_id}
    return [] if inventory_planning_task.nil?
    default = inventory_planning_task.planning_category_table_cell.planning_action_default
    return [] if default.empty_value
    case planning_action_id
      when 1
        defaults = default.planning_action_default_reprices
      when 2
        defaults = default.planning_action_default_small_print_advertisements
      when 3
        defaults = default.planning_action_default_internet_auctions
      when 4
        defaults = default.planning_action_default_lot_promotions
      when 5
        defaults = default.planning_action_default_spiffs
      when 6,10,14
        defaults = default.planning_action_default_others
      when 7
        defaults = default.planning_action_default_live_auctions
      when 8
        defaults = default.planning_action_default_internet_auctions
      when 9
        defaults = default.planning_action_default_wholesalers
      when 11
        defaults = default.planning_action_default_internet_advertisements
      when 12
        defaults = [] # in group
      when 13
        defaults = [] # sold
      when 15
        defaults = [] # service
    end
    return defaults
  end
  
  def ping_conditional_display(inventory_item)
    inventory = Inventory.find( inventory_item.id )
    mmg_id = inventory.vehicle.vehicle_line.legacy_id
    ping_prices = PingPrices.find( :first, :conditions => [ "mmg_id = ? AND distance = 50 AND year = 2004 and ping_provider_id = 1", mmg_id] )
    if ping_prices.nil?
      javascript_tag( remote_function( :url => { :action => :ping_price, :id => inventory_item.id }, 
        :update => 'ping_price' ) )
                                
    else
      ping_prices.average
    end
  end
  
  # the RJS
  #   ActionView::Base.debug_rjs = false
  # this is a hack to stop:
  #   actionpack-1.12.5\lib\action_view\helpers\prototype_helper.rb
  # line 438 nesting try-catch blocks. known bug:
  #   http://dev.rubyonrails.org/ticket/6318
  # this fix, however, stops all try-catch blocks as its setting a static var
  
  def inventory_item_rjs(callback = nil)
    ActionView::Base.debug_rjs = false
    update_page do |page|
      page.replace_html 'InventoryItemForm', ''
      page.visual_effect :appear, "InventoryItemForm", :duration => 1.0, :queue => { :position => 'end', :scope => 'rjs'}
      page.replace_html 'InventoryItemForm', :partial => 'inventory_item_form', :locals => { :inventory_item => @inventory_item, :planning_action_id => nil }
      unless callback.nil?
        page << "#{callback}()"
      end
    end
  end
  
  def nil_safe_to_i(record)
    if record.nil?
      return nil
    end
    return record.to_i
  end
  
  #
  # These are helper methods used in the RHTML to generate tags with the long complicated IDs used
  # so we can have many instances of a single form on a page and identify one precisely. SW
  #
  
  def menu_bar_variables(inventory)
    @inventory = inventory
    @inventory_id = inventory.id
    @planning_action_type_id = inventory.inventory_planning_attribute.nil? ? 1 : inventory.inventory_planning_attribute.planning_action_type_id
    planning_action_ids = @planning_action_type_id == 1 ? [2,3,4,5,6,11,15] : [7,8,9,10,12]
    inventory_planning_task = inventory.inventory_planning_tasks.collect{|t| t if planning_action_ids.include?(t.planning_action_id)}.compact.first
    @planning_action_id = inventory_planning_task.nil? ? nil : inventory_planning_task.planning_action_id
    Log.info("menu_bar_vars.inventory_id=#{@inventory_id}")
    Log.info("menu_bar_vars.planning_action_type_id=#{@planning_action_type_id}")
    Log.info("menu_bar_vars.planning_action_id=#{@planning_action_id}")
  end
  
  def render_planning_act(planning_action_id, inventory)
    planning_action = PlanningAction.find(planning_action_id)
    render :partial => "planning_act_#{planning_action.underscore_name}", :locals => { :inventory => inventory, :planning_action => planning_action }
  end
  
  def refresh_planning_act(planning_action, inventory)
    id = planning_act_camel_prefix(planning_action,inventory) + "Container"
    remote_function( :url => { :action => 'refresh_planning_act', :planning_action_id => planning_action.id, :inventory_id => inventory.id }, :update => id)
  end
  
  def planning_act_camel_prefix(planning_action,inventory)
    "#{planning_action.planning_action_type.camel_case_name}Objective#{planning_action.camel_case_name}I#{inventory.id}"
  end
  
  def planning_act_form_tag(planning_action,inventory,action=:planning_act_add)
    camel_prefix = planning_act_camel_prefix(planning_action,inventory)
    html = form_remote_tag(:url  => { :action => action }, :html => { :name => "#{camel_prefix}Form", :id   => "#{camel_prefix}Form" }, :update => "#{camel_prefix}Container")
    html << hidden_field_tag('dirty', 'false')
    html << planning_act_hidden_field_tag('inventory_id', inventory.id, planning_action, inventory)
    html << planning_act_hidden_field_tag('planning_action_id', planning_action.id, planning_action, inventory)
    return html
  end
  
  def planning_act_tag_id(property,planning_action,inventory,suffix=nil)
    "#{planning_action.planning_action_type.underscore_name}_planning_act_#{planning_action.underscore_name}_#{property}_#{inventory.id}" + (suffix.nil? ? '' : '_' + suffix.to_s)
  end
  
  def planning_act_hidden_field_tag(property,value,planning_action,inventory,suffix=nil)
    id = planning_act_tag_id(property,planning_action,inventory,suffix)
    hidden_field_tag("planning_act[#{property}]", value, { :id => id })
  end
  
  def build_ad(inventory, print_publisher)
    adItems = []
    adDescription = ""
    @print_publisher.print_publisher_print_component_list_items.each do |list_item|
      name = list_item.print_component.name
      if name == "Ad Description"
        adDescription = ""# skip it
      elsif name == "Vehicle Description"
        adItems << inventory.vehicle.model_year
        adItems << inventory.vehicle.vehicle_line.vehicle_make.name
        # Java code has a small amount of extra complexity, didn't know if we needed to recreate
        adItems << inventory.vehicle.vehicle_line.name
      elsif name == "VIN"
        adItems << inventory.vehicle.vin
      elsif name == "Stock #"
        adItems << inventory.stock_number 
      elsif name == "Color"
        adItems << inventory.vehicle.color
      elsif name == "Mileage"
        adItems << inventory.mileage_received
      elsif name == "Options"
        inventory.inventory_book_valuation_summaries[0].book_valuation.book.book_vehicle.book_vehicle_options.each do |option|
          # TODO: Do we need the check if it is selected?
          adItems << option.book_option.name
        end
      end
    end   
    adItems.join(" ")
  end
  
end
