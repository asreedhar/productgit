class InventorySalesGraphData
  
  def initialize(range_id = 1, age_lower = 0, age_upper = nil, units = 0, water = 0, dollars = 0, red_light_units = 0, yellow_light_units = 0, green_light_units = 0)
    @range_id = range_id
    @age_lower = age_lower
    @age_upper = age_upper
    @units = units
    @water = water
    @dollars = dollars
    @red_light_units = red_light_units
    @yellow_light_units = yellow_light_units
    @green_light_units = green_light_units
    @bucket_red_light_percentage = (red_light_units == 0 ? 0 : red_light_units / units)
    @bucket_yellow_light_percentage = (yellow_light_units == 0 ? 0 : yellow_light_units / units)
    @bucket_green_light_percentage = (green_light_units == 0 ? 0 : green_light_units / units)
  end
  
  def percentages(total_units)
    @percentage = @units / total_units.to_f * 100
    @red_light_percentage = @red_light_units / total_units.to_f * 100
    @yellow_light_percentage = @yellow_light_units / total_units.to_f * 100
    @green_light_percentage = @green_light_units / total_units.to_f * 100
  end
  
  attr_reader :range_id
  attr_reader :age_lower
  attr_reader :age_upper
  attr_reader :units
  attr_reader :water
  attr_reader :dollars
  attr_reader :red_light_units
  attr_reader :yellow_light_units
  attr_reader :green_light_units
  
  # these are overall buckets
  attr_reader :percentage
  attr_reader :red_light_percentage
  attr_reader :yellow_light_percentage
  attr_reader :green_light_percentage
  
  # these are per bucket buckets
  attr_reader :bucket_red_light_percentage
  attr_reader :bucket_yellow_light_percentage
  attr_reader :bucket_green_light_percentage
  
  def self.for_business_unit(business_unit_id)
    results = ActiveRecord::Base.connection().select_all("exec dbo.GetInventoryGraphData #{business_unit_id}")
    items = []
    results.each do |result|
      items << InventorySalesGraphData.new(
        result['RangeID'].to_i,
        result['Low'].to_i,
        result['High'].nil? ? nil : result['High'].to_i,
        result["Units"].to_i,
        result["BookMinusCost"].to_f,
        result["TotalInvDollars"].to_f,
        result["RedLights"].to_f,
        result["YellowLights"].to_f,
        result["GreenLights"].to_f
      )
    end
    return items
  end
  
end
