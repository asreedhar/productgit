module ReportingHelper

  def run_view_deals_report(business_unit_id, vehicle_segment_id, vehicle_series, vehicle_mileage, inventory_type, retail_or_wholesale_or_no_sales, weeks, forecast, sort)
    if forecast.nil? || forecast.empty?
      date_0 = Date::today - (7*weeks.to_i)
      date_1 = Date::today
    else
      date_0 = Date::today - (52*7)
      date_1 = date_1 + (13*7)
    end
    time_0 = Time.mktime(date_0.year, date_0.month, date_0.day).strftime('%Y-%m-%d')
    time_1 = Time.mktime(date_1.year, date_1.month, date_1.day).strftime('%Y-%m-%d')
    sale_description = retail_or_wholesale_or_no_sales
    if retail_or_wholesale_or_no_sales == "N"
      sale_description = "W"
    end
    report = <<-SQL
        SELECT
            FrontEndGross,
            DealDate,
            FinanceInsuranceDealNumber,
            VehicleMileage,
            BaseColor,
            VehicleYear,
            VehicleTrim,
            Model,
            Make,
            Descriptions,
            UnitCost,
            TradeOrPurchase,
            DaysToSale
        FROM
            dbo.ViewDealsReport
        WHERE
            DealDate BETWEEN '#{time_0}' AND '#{time_1}'
        AND	SaleDescription = '#{sale_description}'
        AND	InventoryType = #{inventory_type}
        AND	GroupingDescriptionId = #{vehicle_segment_id}
        AND	BusinessUnitId = #{business_unit_id}
    SQL
    if !vehicle_series.nil?
      report << " AND VehicleTrim = '#{vehicle_series}' "
    end
    if !vehicle_mileage.nil?
      report << " AND VehicleMileage BETWEEN #{vehicle_mileage[0]} AND #{vehicle_mileage[1]} "
    end
    if retail_or_wholesale_or_no_sales == 'N'
      report << " AND DaysToSale > 30 "
    end
    if !sort.nil?
      report << " ORDER BY #{sort} "
    end
    return ActiveRecord::Base.connection.select_all(report)
  end
  
  def sort_view_deals_report(name,sort)
    link_to_remote name, :url => {
        :action              => "view_deals_report",
        :vehicle_segment_id  => params["vehicle_segment_id"],
        :vehicle_series      => params["vehicle_series"],
        :vehicle_mileage     => params["vehicle_mileage"],
        :inventory_type      => params["inventory_type"],
        :retail_or_wholesale => params["retail_or_wholesale"],
        :weeks               => params["weeks"],
        :forecast            => params["forecast"],
        :high_mileage_filter => params["high_mileage_filter"],
        :sort                => sort
    }
  end
  
  def toggle_view_deals_report(name,state)
    link_to_remote name, :url => {
        :action              => "view_deals_report",
        :vehicle_segment_id  => params["vehicle_segment_id"],
        :vehicle_series      => params["vehicle_series"],
        :vehicle_mileage     => params["vehicle_mileage"],
        :inventory_type      => params["inventory_type"],
        :retail_or_wholesale => params["retail_or_wholesale"],
        :weeks               => params["weeks"],
        :forecast            => params["forecast"],
        :high_mileage_filter => state
    }
  end
  
  def run_naaa_auction_report(naaa_area_id, naaa_period_id, vic, sort)
    report = <<-SQL
        SELECT
          SaleDate,
          RegionName,
          SaleTypeName,
          SalePrice,
          Mileage,
          Engine,
          Transmission
        FROM
            dbo.NAAAAuctionReport
        WHERE
            AreaID = #{naaa_area_id}
        AND	PeriodID = #{naaa_period_id}
        AND	VIC = '#{vic}'
    SQL
    if !sort.nil?
      report << " ORDER BY #{sort} "
    end
    return ActiveRecord::Base.connection.select_all(report)
  end
  
  def sort_naaa_auction_report(name,sort)
    link_to_remote name, :url => {
        :action                    => "naaa_auction_report",
        :naaa_area_id              => params["naaa_area_id"],
        :naaa_period_id            => params["naaa_period_id"],
        :naaa_series_body_style_id => params["naaa_series_body_style_id"],
        :model_year                => params["model_year"],
        :sort                      => params["sort"]
    }
  end
  
  def run_trade_closing_rate_report(business_unit_id, date_0, date_1, sort)
    report = <<-SQL
        SELECT
            AppraisalID,
            VehicleID,
            AppraisalDate,
            InventoryReceivedDate,
            InventoryVehicleLight,
            VehicleMake,
            VehicleLine,
            VehicleSeries,
            ModelYear,
            VIN,
            AppraisalValue,
            CustomerOffer,
            BookName,
            BookCategoryName,
            BookValue
        FROM
            dbo.TradeClosingRateReport
        WHERE
            IMT.dbo.ToDate(AppraisalDate) BETWEEN '#{date_0}' AND '#{date_1}'
        AND BusinessUnitID = #{business_unit_id}
    SQL
    if !sort.nil?
      report << " ORDER BY #{sort} "
    end
    return ActiveRecord::Base.connection.select_all(report)
  end
  
  def run_trade_in_analyzed_report(business_unit_id, date_0, date_1, sort)
    report = <<-SQL
        SELECT
            InventoryReceivedDate,
            TradeAnalyzerDate,
            VIN,
            VehicleYear,
            VehicleDescription,
            UnitCost,
            MileageReceived,
            RiskLevel,
            Valid
        FROM
            dbo.TradeReport
        WHERE
            InventoryReceivedDate between '#{date_0}' AND '#{date_1}'
        AND BusinessUnitID = #{business_unit_id}
    SQL
    if !sort.nil?
      report << " ORDER BY #{sort} "
    end
    return ActiveRecord::Base.connection.select_all(report)
  end
  
  def run_trade_in_flips_report(business_unit_id, date_0, date_1, sort)
    report = <<-SQL
        SELECT
            InventoryReceivedDate,
        	SaleDate,
        	DaysInInventory,
        	UnitCost,
        	FrontEndGross,
        	VehicleYear,
        	VehicleDescription,
        	MileageReceived,
        	RiskLevel
        FROM
            dbo.TradeFlipsReport
        WHERE
            SaleDate between '#{date_0}' AND '#{date_1}'
        AND BusinessUnitID = #{business_unit_id}
    SQL
    if !sort.nil?
      report << " ORDER BY #{sort} "
    end
    return ActiveRecord::Base.connection.select_all(report)
  end
end
