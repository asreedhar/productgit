class PurchasingCenterVehicleSummaryReport
  def initialize(business_unit_id, member_id)
    @business_unit_id = business_unit_id
    @member_id = member_id
    @results = ActiveRecord::Base.connection.select_all("EXECUTE GetPurchasingCenterVehicleSummary #{business_unit_id}, #{member_id}")
    @created = DateTime.now
  end
  attr_reader :results, :created
  def in_group
    @results.detect{|result| result['ChannelID'] == 2}
  end
  def online_auction
    @results.detect{|result| result['ChannelID'] == 1}
  end
  def live_auction
    @results.detect{|result| result['ChannelID'] == 3}
  end
end