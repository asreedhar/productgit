require 'extensions/numeric'

# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  def is_print_page
    (params[:layout] == 'false')
  end
  
  def get_theme
  	env = request.env if request
  	return env["HTTP_X_THEME"]
  end
  
  def take_id(record)
    if record.nil?
      return nil
    else
      record.id
    end
  end
  
  def take_name(record)
    if record.nil?
      return nil
    else
      record.name
    end
  end
  
  def value_or_default(value,default_value)
    if value.nil? || value == ""
      default_value
    else
      value
    end
  end
  
  def light_id_to_image(id,size=10)
    case id
      when 3
        image_tag("light-#{size}x#{size}-green.gif")
      when 2
        image_tag("light-#{size}x#{size}-red.gif")
      when 1
        image_tag("light-#{size}x#{size}-yellow.gif")
      else
       ''
    end
  end
  
  #just pass in the light color name
  def light_name_to_image(name,size=10)
    case name
      when 'GREEN'
        image_tag("light-#{size}x#{size}-green.gif")
      when 'RED'
        image_tag("light-#{size}x#{size}-red.gif")
      when 'YELLOW'
        image_tag("light-#{size}x#{size}-yellow.gif")
      else
       ''
    end
  end
  
  def money(input)
    value = input.to_f.round
    value.format_s(:usd, :acct => true)    
  end
  
  def days_old(inventory)
    Date.today - Date.new(inventory.inventory_received_date.year, inventory.inventory_received_date.month, inventory.inventory_received_date.day)
  end
  
  def inventory_description(inventory)
    "#{inventory.vehicle.model_year} #{inventory.vehicle.vehicle_make.name} #{inventory.vehicle.vehicle_line.name} #{inventory.vehicle.series}"
  end
  
  def interval_as_text(interval)
    txt = ""
    case interval.interval_type_id
      when 2
        txt << "Every " + WeekDay.find(interval.day_of_week).name
      when 3
        txt << "The " + MonthDay.find(interval.day_of_month).name + " of every month"
      when 1,4
        txt << "On " + interval.day_of_year.strftime('%m/%d/%Y')
    end
    return txt
  end
  
  def interval_as_bare_text(interval)
    txt = ""
    case interval.interval_type_id
      when 2
        txt << WeekDay.find(interval.day_of_week).name
      when 3
        txt << MonthDay.find(interval.day_of_month).name + " of the month"
      when 1,4
        txt << interval.day_of_year.strftime('%m/%d/%Y')
    end
    return txt
  end
  
  def vehicle_light_img(inventory_item)
    light_id = 0
    if inventory_item.respond_to?(:vehicle_light_id)
      light_id = inventory_item.vehicle_light_id
    elsif inventory_item.respond_to?(:current_vehicle_light_id)
      light_id = inventory_item.current_vehicle_light_id
    end
    light_id_to_image(light_id,9)
  end
  
  def print_publishers_by_business_unit(business_unit_id)
    @results = PrintPublisher.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              print_publishers p
        JOIN  third_parties t ON t.id = p.id
        WHERE
              t.business_unit_id = #{business_unit_id}
      SQL
    )
  end
  
  def live_auctions_by_business_unit(business_unit_id)
    @results = LiveAuction.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              live_auctions p
        JOIN  third_parties t ON t.id = p.id
        WHERE
              t.business_unit_id = #{business_unit_id}
      SQL
    )
  end
  
  def wholesalers_by_business_unit(business_unit_id)
    @results = Wholesaler.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              wholesalers p
        JOIN  third_parties t ON t.id = p.id
        WHERE
              t.business_unit_id = #{business_unit_id}
      SQL
    )
  end
  
  def internet_auctions_by_business_unit(business_unit_id, include_firstlook_auctions=true)
    @results = ThirdParty.find_by_sql(<<-SQL
        SELECT
              t.*
        FROM
              third_parties t
        WHERE
              t.third_party_type_id = 5
        AND   (t.business_unit_id = #{business_unit_id}#{include_firstlook_auctions ? " OR t.business_unit_id = 100150" : ""})
      SQL
    )
  end
  
  def internet_publisher_by_business_unit(business_unit_id)
    @results = InternetPublisher.find_by_sql(<<-SQL
        SELECT
              p.*
        FROM
              internet_publishers p
        JOIN  internet_publishers_business_units j on j.internet_publisher_id = p.id
        WHERE
              j.business_unit_id = #{business_unit_id}
      SQL
    )
  end

  def has_internet_publishers?(business_unit_id)
    count = InternetPublisher.count_by_sql(<<-SQL
        SELECT
              COUNT(*)
        FROM
              internet_publishers p
        JOIN  internet_publishers_business_units j on j.internet_publisher_id = p.id
        WHERE
              j.business_unit_id = #{business_unit_id}
      SQL
    )
    return (count > 0)
  end
  
  def books_for_business_unit(business_unit_id)
    @results = Book.find_by_sql(<<-SQL
        SELECT
              b.*
        FROM
              books b
        JOIN  business_units_books bb ON b.id = bb.book_id
        WHERE
              bb.business_unit_id = #{business_unit_id}
      SQL
    )
  end
  
  def has_credential?(member,credential_authority_id)
    if member.nil?
      return false
    else
      return member.credentials(true).detect{|c| c.credential_authority_id == credential_authority_id && !c.username.nil? && !c.password.nil?}
    end
  end
  
  def planning_category_indicator(planning_category)
    if planning_category.id == 1 || planning_category.id == 2 || planning_category.id == 6
      image_tag "light-9x9-green.gif"
    elsif planning_category.id == 3 || planning_category.id == 4
      image_tag "light-9x9-red.gif"
    elsif planning_category.id == 5
      image_tag "light-9x9-yellow.gif"
    end
  end
  
  def toggle(link_text, id)
    link_to_function link_text, "Element.toggle('#{id}');"
  end

  def switch(link_text, first_id, second_id)
    link_to_function link_text, "Element.toggle('#{first_id}'); Element.toggle('#{second_id}');"
  end
  
  def format_mileage(mileage) 
    return number_with_delimiter(mileage, delimiter=",")
  end
  
  def third_party_name(planning_action_default)
    if planning_action_default.third_party_id.nil?
      return planning_action_default.third_party_name
    else
      return planning_action_default.third_party.name
    end
  end
  
  def neither_nil_nor_empty(map,key)
    if map.member?(key)
      obj = map[key]
      return !obj.nil? && !obj.empty?
    else
      return false
    end
  end
  
  def not_nil(map,key)
    if map.member?(key)
      obj = map[key]
      return !obj.nil?
    else
      return false
    end
  end
  
  def error_list(errors)
    html = "<ul>"
    errors.each {|e| html << "<li>#{e}</li>"}
    html << "</ul>"
  end
  
  def vehicles(business_unit_id)
    vehicles = ActiveRecord::Base.connection.select_all(<<-SQL
        SELECT
        	ii.vehicle_make_id,
        	vm.name vehicle_make,
        	ii.vehicle_line_id,
        	vl.name vehicle_line,
        	ii.units_in_stock
        FROM
        (
        	SELECT
        		vm.id vehicle_make_id,
        		vl.id vehicle_line_id,
        		COUNT(*) units_in_stock
        	FROM
        		inventory i
        	JOIN	vehicles v on v.id = i.vehicle_id
        	JOIN	vehicle_makes vm on vm.id = v.vehicle_make_id
        	JOIN	vehicle_lines vl on vl.id = v.vehicle_line_id
        	WHERE
        		i.business_unit_id = #{business_unit_id}
        	AND	i.active = 1
        	AND	i.inventory_type_id = 2
        	GROUP BY
        		vm.id,
        		vl.id
        	WITH ROLLUP
        	HAVING
        		vm.id IS NOT NULL
        ) ii
        LEFT JOIN vehicle_makes vm on vm.id = ii.vehicle_make_id
        LEFT JOIN vehicle_lines vl on vl.id = ii.vehicle_line_id
        ORDER BY
        	vm.name,
        	vl.name
      SQL
    )
    results = {}
    vehicles.each do |vehicle|
      next if vehicle['vehicle_line_id'].nil?
      make = { :id => vehicle['vehicle_make_id'], :name => vehicle['vehicle_make'] }
      line = { :id => vehicle['vehicle_line_id'], :name => vehicle['vehicle_line'], :units_in_stock => vehicle['units_in_stock'] }
      if results.member?(vehicle['vehicle_make_id'])
        results[vehicle['vehicle_make_id']][:lines] << line
      else
        results[vehicle['vehicle_make_id']] = { :make => make, :lines => [line]}
      end
    end
    return results
  end
  
  def third_parties_for_planning_action_id(planning_action_id)
    return ThirdParty.find_by_sql(<<-SQL
        SELECT
              tp.*
        FROM
              third_parties tp
        JOIN  third_party_types tt on tt.id = tp.third_party_type_id
        JOIN  planning_actions_third_party_types jt on tt.id = jt.third_party_type_id
        WHERE
              jt.planning_action_id = #{planning_action_id}
        AND   tp.business_unit_id = #{session[:business_unit].id}
      SQL
    )
  end
  
  def flash_notice_rjs(page,flash)
    unless flash.now[:notice].nil? || flash.now[:notice].empty?
      if flash.now[:notice].kind_of?(Array)
        page.replace 'notice', "<div id=\"notice\"><ul><li>#{flash[:notice].join('</li><li>')}</li></ul></div>"
      else
        page.replace 'notice', "<div id=\"notice\">#{flash[:notice]}</div>"
      end
      page.visual_effect :highlight, 'notice'
    end
  end
  
  def form_mode_tag(options={},&block)
    html = ""
    case params[:mode]
      when 'page'
        html = form_tag(options[:url], options[:html] || {})
      when 'tile'
        html = form_remote_tag(options, &block)
      when 'float'
        if options[:float]
          options[:loading]  = "RedBox.loading(); " + options[:loading].to_s if options[:float][:loading]
          options[:complete] = "RedBox.addHiddenContent('#{params[:container]}');" + options[:complete].to_s if options[:float][:complete]
        end
        html = form_remote_tag(options, &block)
      else
        raise "There is no mode specified!"
    end
    html << hidden_field_tag("mode", params[:mode], { :id => nil })
    html << hidden_field_tag("container", params[:container], { :id => nil })
  end
  
  def link_to_mode(text,options={}, html_options={}, float_options={})
    options[:url][:mode] = params[:mode]
    options[:url][:container] = params[:container]
    case params[:mode]
      when 'page'
        link_to(text, options[:url], html_options)
      when 'tile'
        link_to_remote(text, options, html_options)
      when 'float'
        options[:loading]  = "RedBox.loading(); " + options[:loading].to_s if float_options[:loading]
        options[:complete] = "RedBox.addHiddenContent('#{params[:container]}');" + options[:complete].to_s if float_options[:complete]
        link_to_remote(text, options, html_options)
    end
  end
  
  def gen_id(records,suffix=nil)
    id = nil
    records.each {|record| id = dom_id(record,id)}
    return [id, suffix].compact * '_'
  end
  
  def dom_id(record, prefix = nil)
    prefix ||= 'new' unless record.id
    [ prefix, singular_class_name(record), record.id ].compact * '_'
  end

  def singular_class_name(record_or_class)
    class_from_record_or_class(record_or_class).name.underscore.tr('/', '_')
  end

  private
  
  def class_from_record_or_class(record_or_class)
    record_or_class.is_a?(Class) ? record_or_class : record_or_class.class
  end
  
end
