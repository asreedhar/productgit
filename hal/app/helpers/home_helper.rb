module HomeHelper
	def pingII_branding(has_fl30 = false)
		package_id = session[:business_unit].business_unit_preference(true).pricing_package_id

		if has_fl30
			case package_id

				when 1..6
					return content_tag(:h2, "PING III" + content_tag(:h3,"Market Pricing Analyzer", :class => 'branding'), :class => 'branding')
				when 7,8
					return content_tag(:h2, "PING III " + content_tag(:small, "with") + content_tag(:h3, "MARKET PRICING POWER", :class => 'branding', :style => "font-style: italic;"), :class => 'branding')
				end
		else
			case package_id
				when 1..6
					return content_tag(:h2, "PING III" + content_tag(:sub,"Market Pricing Analyzer"), :class => 'branding')
				when 7,8
					return content_tag(:h2, "PING III " + content_tag(:small, "with") + content_tag(:sub,"MARKET PRICING POWER", :style => "font-style: italic;"), :class => 'branding')
				end
		end		
	end
	
	def pingII_branding_title
		package_id = session[:business_unit].business_unit_preference(true).pricing_package_id

		case package_id
		when 1..6
			return "Ping III Market Pricing Analyzer"
		when 7,8
			return "Ping III Market Pricing Power"
		end
	end
	
	def default_value_input_helper(options = {})
		default_options = { 
			:type => 'text',
			:class => "text",
			:onblur => "if( this.value == '' )this.value=this.defaultValue;",
			:onfocus => "if( this.value == this.defaultValue ) this.value=''; this.select();"
		}
		default_options.merge!(options)
		tag(:input, default_options)
	end
	
	def header_branding_helper
		if get_theme == "gmac" && (@has_merchandising or @has_marketing)
			return image_tag("/images/branding/qt_max.gif", { :alt => "QuickTurn MAX Powered by FirstLook", :class => "max_logo" })
		elsif get_theme == "gmac"
			return image_tag("/images/branding/qt.gif", { :alt => "QuickTurn Powered by FirstLook", :class => "max_logo" })
		elsif (@has_merchandising or @has_marketing)
			return image_tag("/images/branding/fl_max_retail.gif", { :alt => "FirstLook MAX Retail Performance System", :class => "max_logo" })
		else
			return image_tag("/images/branding/fl_inventory.gif", { :alt => "FirstLook Inventory Managment System", :class => "max_logo" })
		end
	end
end
