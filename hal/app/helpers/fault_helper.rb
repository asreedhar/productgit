require 'soap/header/simplehandler'

class FaultHelper

  include Singleton

=begin
This method will make a SOAP message call to the URL defined by
FAULT_ENDPOINT, saving the exception that was thrown, using request
information as found in the HTTP request.

parameters:
exception     - The exception thrown
request_info  - The HTTP request, used to obtain session information
=end
  def self.log_fault(exception, request_info)
    # generate client
    client = Fault::FaultSoap.new(FAULT_ENDPOINT)
    client.generate_explicit_type = false
    
    # Get some environment related information
    application = Fault::ApplicationDto.new('HAL')
    platform = Fault::PlatformDto.new('Ruby')
    machine = Fault::MachineDto.new(request_info.host)
  
    # build the request information
    request_information = Fault::RequestInformationDto.new;
    request_information.path = request_info.env['PATH_INFO']
    request_information.url = request_info.env['REQUEST_URI']
    request_information.user = request_info.session[:casfilteruser]

    # construct a FaultEvent object
    fault_event = Fault::FaultEventDto.new
    fault_event.application = application
    fault_event.machine = machine
    fault_event.platform = platform
    fault_event.requestInformation = request_information
    fault_event.faultTime = Time.new
    fault_event.isNew = true
    fault_event.id = 0

    # construct a Fault object
    fault = Fault::FaultDto.new
    fault.stack = build_stack(exception.backtrace)
    fault.exceptionType = exception.class
    fault.message = exception.message

    # update FaultEvent object with Fault reference
    fault_event.fault = fault

    # make soap request
    save_remote_arguments = Fault::SaveRemoteArgumentsDto.new
    save_remote_arguments.faultEvent = fault_event

    response = client.saveFault(Fault::SaveFault.new(save_remote_arguments))
    response.saveFaultResult

  end

=begin
Using the CALLER_RE regular expression, this method will parse a trace entry
from the exception backtrace, getting the file, line, and method
in question for the stack frame.

parameters:
exception_backtrace - The exception backtrace

return:
- returns an array of file,line,method sets
=end
  CALLER_RE = /^(.+?):(\d+)(|:in `(.+)')$/
	def self.parse_caller(exception_backtrace)
	  exception_backtrace.collect do |c|
	    captures = CALLER_RE.match(c)
	    [captures[1], captures[2], captures[4]]
	  end
	end

=begin
Build a stack given the exception backtrace.

parameters:
backtrace   - The exception backtrace

return:
stack       - The stack that was built
=end
  def self.build_stack(backtrace)
    
    stack = Fault::StackDto.new
    stack.stackFrames = Fault::ArrayOfStackFrameDto.new

      # On return from the parse_caller there will be an array
      # of file,line,method sets that will be used to construct
      # stack frames.

      parse_caller(backtrace).each do |file, line, method|
        stack_frame = Fault::StackFrameDto.new
        stack_frame.fileName = file
        stack_frame.lineNumber = line
        stack_frame.methodName = method
        stack_frame.isNativeMethod = false
        stack_frame.isUnknownSource = false
        stack.stackFrames.push(stack_frame)
    end      
    return stack
  end

end
