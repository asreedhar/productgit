
require 'soap/header/simplehandler'

class AutoCheckHelper
  
  class AutoCheckUserIdentityHandler < SOAP::Header::SimpleHandler
    NAMESPACE = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/"
    attr_accessor :userName
    def initialize()
      super(XSD::QName.new(NAMESPACE, 'UserIdentity'))
      @userName = nil
    end
    def on_simple_outbound
      return { "UserName" => userName }
    end
    def on_simple_inbound(my_header, mustunderstand) 
      @userName = my_header["UserName"] 
    end 
  end
  
  def self.has_account(dealer_id, user_name)
    # generate client
    client = AutoCheck::AutoCheckWebServiceSoap.new(AUTOCHECK_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = AutoCheckUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler << user
    # make request
    response = client.hasAccount(AutoCheck::HasAccount.new(dealer_id))
    response.hasAccountResult
  end
  
  def self.can_purchase_report(dealer_id, user_name)
    # generate client
    client = AutoCheck::AutoCheckWebServiceSoap.new(AUTOCHECK_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = AutoCheckUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler << user
    # make request
    response = client.canPurchaseReport(AutoCheck::CanPurchaseReport.new(dealer_id))
    response.canPurchaseReportResult
  end
  
  def self.get_report(dealer_id, user_name, vin)
    # generate client
    client = AutoCheck::AutoCheckWebServiceSoap.new(AUTOCHECK_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = AutoCheckUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler <<  user
    # make request
    response = client.getReport(AutoCheck::GetReport.new(dealer_id, vin))
    response.getReportResult
  end
  
end