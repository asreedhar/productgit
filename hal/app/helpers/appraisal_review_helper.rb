module AppraisalReviewHelper
  
  def find_people(business_unit_id, position_id)
    return Person.find_by_sql(<<-SQL
          SELECT
                people.*
          FROM
                people
          JOIN  people_positions on people.id = people_positions.person_id
          WHERE
                people_positions.position_id = #{position_id}
          AND   people.business_unit_id = #{business_unit_id}
      SQL
    )
  end
  
  def appraisal_num_to_str(num)
    if num == 1
      return "1"
    elsif num == 2
      return "2"
    end    
    return num    
  end

  def nil_output_protect(item, show_holder)    
     if item.nil? || item.empty?      
        if show_holder || show_holder.nil?
          return "?"
        else
          return ""
        end
     else
      return item;
     end  
  end
  
  def book_value(obj, book_category_id)
    appraisal = nil
    appraisal = obj.appraisal if obj.instance_of?(AppraisalReview)
    appraisal = obj if obj.instance_of?(Appraisal)
    return nil if appraisal.nil?
    appraisal.appraisal_book_valuation_summaries.find(:all, :conditions => ["business_unit_id = ? AND book_category_id = ?", session[:business_unit].id, book_category_id]).each do |summary|
      if summary.book_value_type_id.nil? || summary.book_value_type_id == 2
        return "#{summary.book_category.book.name} #{summary.book_category.name}: <strong>#{number_to_currency(summary.book_value, :precision => 0)}</strong>"
      end
    end
    return nil
  end
  
  def remove_appraisal_review_from_rule_rjs(page, rule)
    if flash.now[:notice].nil? || flash.now[:notice].empty?
      page.visual_effect :fade,  "appraisal_review_#{@appraisal_review.id}", :duration => 0.5
      page.replace_html 'mad_summary_title_count', "#{@number_of_appraisal_reviews}"
      if @number_of_appraisal_reviews_for_rule == 0
        page.visual_effect :fade, "rule_#{rule}"
      else
        page.replace_html "rule_#{rule}_count", "#{@number_of_appraisal_reviews_for_rule}"
      end
    else
      flash_notice_rjs(page,flash)
    end
  end
  
  def remove_appraisal_review_from_follow_up_list_rjs(page)
    if @number_of_appraisal_review_follow_ups == 0
      page << "document.location.href='/appraisal_review/'"
    else
      page.visual_effect :fade,  "appraisal_review_#{@appraisal_review.id}", :duration => 0.5
      page.replace_html 'follow_up_summary_title_count', "#{@number_of_appraisal_review_follow_ups}"
    end
  end
  
  def customer_offer(appraisal_offer, appraisal_valuation)
    if appraisal_offer.nil? || appraisal_offer.customer_offer.nil? || appraisal_offer.customer_offer.zero?
      if appraisal_valuation.nil?
        nil
      else
        appraisal_valuation.value
      end
    else
      appraisal_offer.customer_offer
    end
  end
  
end
