module CampaignRunHelper
  
  def create_campaign_run_for_campaign(campaign_id)
    campaign_deadline = CampaignDeadline.find(:first, :conditions => ["campaign_id = ?", campaign_id], :include => :campaign)
    campaign = campaign_deadline.campaign
    campaign_run_id = campaign_deadline.campaign_run_id if campaign_run_id.nil?
    if campaign_run_id.nil?
      # create a vehicle list for the campaign run
      vehicle_list = VehicleList.new do |l|
        l.vehicle_list_type_id = 2
        l.created              = Date.today()
        l.last_modified        = Date.today()
      end
      # create a new campaign run
      campaign_run = CampaignRun.new do |c|
        c.campaign_id = campaign_id
        c.deadline = campaign_deadline.deadline
        c.run_date = campaign_deadline.run_date
      end
      # commit these objects to the db
      CampaignRun.transaction() do
        if vehicle_list.save
          campaign_run.vehicle_list_id = vehicle_list.id
          unless campaign_run.save
            record_errors(campaign_run)
          end
        else
          record_errors(vehicle_list)
        end
      end
    else
      # load the campaign run
      campaign_run = CampaignRun.find(campaign_run_id)
      vehicle_list = campaign_run.vehicle_list
    end
    return [campaign_run, vehicle_list]
  end
  
  def title_list(values)
    retstring = ""
     values.each do |value|
        if retstring != ""
         retstring = retstring+", "+value.name
        else
            retstring = value.name
        end
    end 
    return retstring 
  end
  
end