module Management::CampaignHelper

  def interval_class(interval,interval_type_id)
    if interval.nil? || interval.interval_type_id != interval_type_id
      return "class=\"off\""
    else
      return ""
    end
  end
  
end
