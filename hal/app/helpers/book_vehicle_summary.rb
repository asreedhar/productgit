# BookID BookName           BookVehicleOptionValueType
# ------ -----------------  --------------------------
# 1      BlackBook          N/A
# 4      GALVES             N/A
# 3      Kelley Blue Book   Retail
# 3      Kelley Blue Book   Wholesale
# 3      Kelley Blue Book   Trade-In
# 3      Kelley Blue Book   Private party - never displayed
# 2      NADA	            Loan
# 2      NADA	            N/A
# 2      NADA	            Retail
# 2      NADA	            Trade-In
class BookVehicleSummary
  def initialize(vehicle, book_vehicle, book_valuation, business_unit_id)
    @vehicle              = vehicle
    @book_vehicle         = book_vehicle
    @book                 = book_vehicle.book
    @book_valuation       = book_valuation
    @last_booked_out      = book_valuation.date_published unless book_valuation.nil?
    @general_options      = Array::new
    @equipment_options    = Array::new
    @engine_options       = Array::new
    @transmission_options = Array::new
    @drive_chain_options  = Array::new
    book_vehicle.book_vehicle_options.each do |book_vehicle_option|
      option              = Hash::new
      option[:option]     = book_vehicle_option
      option[:values]     = Hash::new
      option[:value_type] = nil
      case @book.id
        when 1 then option[:value_type] = :not_applicable
        when 2 then option[:value_type] = :retail
        when 3 then option[:value_type] = :retail
        when 4 then option[:value_type] = :not_applicable
        when 5 then option[:value_type] = :not_applicable
      end
      book_vehicle_option.book_vehicle_option_values.each do |book_vehicle_option_value|
        case book_vehicle_option_value.book_vehicle_option_value_type_id
          when 0
            option[:values][:not_applicable] = book_vehicle_option_value
          when 1
            option[:values][:retail] = book_vehicle_option_value
          when 2
            option[:values][:wholesale] = book_vehicle_option_value
          when 3
            option[:values][:loan] = book_vehicle_option_value
          else
            option[:values][:trade_in] = book_vehicle_option_value
        end
      end
      case book_vehicle_option.book_option.book_option_type_id
        when 0
          @general_options << option
        when 1
          @equipment_options << option
        when 2
          @engine_options << option
        when 3
          @transmission_options << option
        when 4
          @drive_chain_options << option
      end
    end
    # special logic to aid KBB display
    if book_vehicle.book.id == 3
      book_valuation.book_category_valuations.each do |book_category_valuation|
        for book_value in book_category_valuation.book_values
          if book_value.book_value_type.id == 3 # mileage indep, we'll call base
            case book_category_valuation.book_category.id
              when 8 #wholesale
                @kbb_wholesale_base = book_value.value
              when 9 #retail
                @kbb_retail_base = book_value.value
              when 11 #trade in
                @kbb_trade_in_base = book_value.value
            end
          elsif book_value.book_value_type.id == 2 # final
            case book_category_valuation.book_category.id
              when 8 #wholesale
                @kbb_wholesale_final = book_value.value
              when 9 #retail
                @kbb_retail_final = book_value.value
              when 11 #trade in
                @kbb_trade_in_final = book_value.value
            end
          elsif book_value.book_value_type.id == 4 # options value
            if book_category_valuation.book_category.id == 11
              @kbb_trade_in_options = book_value.value
            end
          end
        end  
        @kbb_mileage_adjustment = book_valuation.mileage_adjustment
      end
    end
  end
  attr_reader :vehicle
  attr_reader :book
  attr_reader :book_vehicle
  attr_reader :book_valuation
  attr_reader :last_booked_out
  attr_reader :general_options
  attr_reader :equipment_options
  attr_reader :engine_options
  attr_reader :transmission_options
  attr_reader :drive_chain_options
  attr_reader :kbb_wholesale_base
  attr_reader :kbb_wholesale_final
  attr_reader :kbb_retail_base
  attr_reader :kbb_retail_final
  attr_reader :kbb_mileage_adjustment
  attr_reader :kbb_trade_in_base
  attr_reader :kbb_trade_in_options
  attr_reader :kbb_trade_in_final
end
