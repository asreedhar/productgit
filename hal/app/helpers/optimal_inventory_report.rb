class OptimalInventoryReport
  def initialize(business_unit_id)
	current_date = Time.now.strftime("%m/%d/%Y")
	@results = ActiveRecord::Base.connection.select_all(<<-SQL
		 SELECT 
			BusinessUnitId,
			TargetUnits,
			UnitsInStock,
			Percentage
		 FROM
			HAL.dbo.GetStockCountsforOptimalandInStock(#{business_unit_id},#{current_date}) 
	SQL
    ).first
  end
  def business_unit_id
    @results['BusinessUnitId']
  end
  def total_optimal_units_model
    @results['TargetUnits']
  end
  def total_stocked_units_model
    @results['UnitsInStock']
  end
  def percentage
    @results['Percentage']
  end
end
