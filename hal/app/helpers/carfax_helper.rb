
require 'soap/header/simplehandler'

class CarfaxHelper
  
  include Singleton
  
  class CarfaxUserIdentityHandler < SOAP::Header::SimpleHandler
    NAMESPACE = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/"
    attr_accessor :userName
    def initialize()
      super(XSD::QName.new(NAMESPACE, 'UserIdentity'))
      @userName = nil
    end
    def on_simple_outbound
      return { "UserName" => userName }
    end
    def on_simple_inbound(my_header, mustunderstand) 
      @userName = my_header["UserName"] 
    end 
  end
  
  def self.has_account(dealer_id, user_name)
    # generate client
    client = Carfax::CarfaxWebServiceSoap.new(CARFAX_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = CarfaxUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler << user
    # make request
    response = client.hasAccount(Carfax::HasAccount.new(dealer_id))
    response.hasAccountResult
  end
  
  def self.can_purchase_report(dealer_id, user_name)
    # generate client
    client = Carfax::CarfaxWebServiceSoap.new(CARFAX_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = CarfaxUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler << user
    # make request
    response = client.canPurchaseReport(Carfax::CanPurchaseReport.new(dealer_id))
    response.canPurchaseReportResult
  end
  
  def self.get_report(dealer_id, user_name, vin)
    # generate client
    client = Carfax::CarfaxWebServiceSoap.new(CARFAX_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = CarfaxUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler <<  user
    # make request
    response = client.getReport(Carfax::GetReport.new(dealer_id, vin))
    response.getReportResult
  end
  
  def self.get_report_preference(dealer_id, user_name, vehicle_entity_type)
    # generate client
    client = Carfax::CarfaxWebServiceSoap.new(CARFAX_ENDPOINT)
    client.generate_explicit_type = false
    client.wiredump_dev = STDERR
    # add header
    user = CarfaxUserIdentityHandler.new
    user.userName = user_name;
    client.headerhandler <<  user
    # make request
    response = client.getReportPreference(Carfax::GetReportPreference.new(dealer_id, vehicle_entity_type))
    response.getReportPreferenceResult
  end
  
  def self.purchase_report(dealer_id, user_name, vin, carfax_report_type, display_in_hot_list)
  	# generate client
  	client = Carfax::CarfaxWebServiceSoap.new(CARFAX_ENDPOINT)
  	client.generate_explicit_type = false
  	client.wiredump_dev = STDERR
  	# add header
  	user = CarfaxUserIdentityHandler.new
  	user.userName = user_name
  	client.headerhandler << user
  	# make request
  	response = client.purchaseReport(Carfax::PurchaseReport.new(dealer_id, vin, carfax_report_type, display_in_hot_list, "FLN"))
  	response.purchaseReportResult
  end
  
end