module ProxyComponent

  module ProxyComponentHelper
    
    protected
    
    def render_proxy_component(proxy_component_options = {})
      # extract local variables
      proxy_component_symbol = proxy_component_options[:proxy_component_symbol]
      proxy_component_params = proxy_component_options[:proxy_component_params]
      # load class
      require proxy_component_symbol.to_s
      # new class
      proxy = eval(proxy_component_symbol.id2name.camelcase.gsub(/Naaa/,"NAAA")).new(proxy_component_params)
      # generate output
      html = ""
      if proxy.has_custom_form?
        html << proxy.render_custom_form(self)
        html << "\n"
      end
      html << tag('div', { :id => proxy.container_id, :class => proxy.container_class }, true)
      if proxy_component_options[:proxy] == true
        unless proxy.has_custom_form?
          html << "\n"
          html << form_remote_tag(:url => { :controller => proxy.controller, :action => proxy.action }, :html => { :name => proxy.form_name }, :update => proxy.container_id)
          html << "\n"
          html << tag('fieldset', {}, true)
          proxy.action_params.each do |key, value|
            html << "\n"
            html << tag('input', { :name => key, :type => "hidden", :value => value })
          end
          html << "\n</fieldset>"
          html << "\n</form>"
          html << "\n"
        end
        html << tag('script', { :type => 'text/javascript', :language => 'javascript' }, true)
        html << "\nfunction #{proxy.container_id}_onload(evt) { document.#{proxy.form_name}.onsubmit(); }"
        html << "\nEvent.observe(window, 'load', #{proxy.container_id}_onload, false);"
        html << "\n</script>\n"
        html << proxy.render_proxy_component(self)
      else
        html << proxy.render_component(self)
      end
      html << "\n"
      html << "</div>"
      html
    end
    
    def render_proxy_component_collection(options = {})
      proxy_component_collection = options[:proxy_component_collection]
      return "" if proxy_component_collection.records.empty? && !options.member?(:include_empty)
      html = ""
      if options[:proxy] == true
        html << do_render_proxy_component_collection(proxy_component_collection, proxy_component_collection.initial_records)
        proxy_component_collection.proxy_records.each_with_index do |records,index|
          proxy_container_id = "#{proxy_component_collection.proxy_container_prefix}_#{index}"
          if proxy_component_collection.is_table?
            html << tag('tbody', { :id => proxy_container_id }, true)
            html << "<tr><td colspan=\"#{proxy_component_collection.colspan}\">\n"
            html << tag('script', { :type => 'text/javascript', :language => 'javascript' }, true)
            html << remote_function( :url => proxy_component_collection.proxy_url(records), :success => "tBodyReplace('#{proxy_container_id}', request.responseText);" ) # $('#{proxy_container_id}').replace(request.responseText);
            html << "\n</script>\n</td></tr>\n</tbody>\n"
            html << ""
          else
            html << tag('div', { :id => proxy_container_id }, true)
            html << "\n"
            html << tag('script', { :type => 'text/javascript', :language => 'javascript' }, true)
            html << remote_function( :url => proxy_component_collection.proxy_url(records), :success => "$('#{proxy_container_id}').innerHTML = request.responseText" )
            html << "\n</script>\n</div>\n"
          end
        end
      else
        html << do_render_proxy_component_collection(proxy_component_collection, proxy_component_collection.records)
      end
      return render(:partial => proxy_component_collection.collection_partial, :object => html, :locals => proxy_component_collection.locals)
    end
    
    private
    
    def do_render_proxy_component_collection(proxy, records)
      html = ""
      unless records.empty?
        html << render(:partial => proxy.component_partial, :collection => records, :locals => proxy.locals)
        reset_cycle(proxy.cycle_name) if proxy.respond_to?(:cycle_name)
      end
      html
    end
    
  end

end
