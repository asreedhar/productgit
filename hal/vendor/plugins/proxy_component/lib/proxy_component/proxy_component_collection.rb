module ProxyComponent
  
  class Collection
    
    attr_reader :id
    attr_reader :initial_records
    attr_reader :proxy_records
    
    def initialize(id)
      @id = id
      @records = []
      @initial_records = []
      @proxy_records = []
    end
    
    def <<(record)
      @records << record
    end
    
    def allocate_initial_records(i)
      @initial_records = @records.slice!(0, (@records.length >= i) ? i : @records.length)
      return @initial_records.length
    end
    
    def allocate_proxy_records(i)
      while @records.length >= i
        @proxy_records << @records.slice!(0,i)
      end
      @proxy_records << @records.slice!(0, @records.length) unless @records.empty?
    end
    
    def records()
      (@initial_records + @proxy_records.flatten())
    end
    
    def component_partial()
      "#{self.class.to_s.underscore.gsub(/_proxy_component_collection/,'')}"
    end
    
    def collection_partial()
      "#{self.class.to_s.underscore.gsub(/_proxy_component_collection/,'').pluralize()}"
    end
    
    def action()
      "#{self.class.to_s.underscore.gsub(/_proxy_component_collection/,'')}"
    end
    
    def proxy_container_prefix
      "#{self.class.to_s.underscore}_#{self.__id__}"
    end
    
    def is_table?
      false
    end
    
    def self.groups(options = {})
      # extract sensible defaults
      number_of_groups          = options[:number_of_groups] || 1
      number_of_initial_records = options[:number_of_initial_records]|| 6
      records                   = options[:records] || []
      group_by_proc             = options[:group_by_proc] || proc{|record| return 0}
      # instantiate groups
      groups = []
      1.upto(number_of_groups) { |i| groups << options[:proxy_component_collection].new(i) }
      # group records
      records.each do |record|
        groups[group_by_proc.call(record)] << record
      end
      # set the initial items on each group
      items_left = number_of_initial_records
      groups.each do |group|
        items_left = items_left - group.allocate_initial_records(items_left)
      end
      # minimize the number of requests using a maximum block size of 20
      groups.each do |group|
        group.allocate_proxy_records(20)
      end
      return groups
    end
    
  end
  
end
