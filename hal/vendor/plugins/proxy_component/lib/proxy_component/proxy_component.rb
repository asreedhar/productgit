module ProxyComponent
  
  class Base
    
    def container_id()
      "#{component_partial()}_#{self.__id__}".gsub(/[-]/,'')
    end
    
    def container_class()
      "#{component_partial()}".gsub(/[-]/,'')
    end
    
    def form_name()
      "#{form_partial()}_#{self.__id__}".gsub(/[-]/,'')
    end
    
    def has_custom_form?
      false
    end
      
    def action
      component_partial()
    end
    
    def action_params
      {}
    end
    
    def component_partial
      "#{self.class.to_s.underscore.gsub(/_proxy_component/,'')}"
    end
    
    def form_partial
      "#{self.class.to_s.underscore.gsub(/proxy_component/,'form')}"
    end
    
    def render_custom_form(context)
      the_form         = self
      the_form_name    = form_name()
      the_form_partial = form_partial()
      the_container_id = container_id()
      cmd = proc{render(:partial => the_form_partial, :object => the_form, :locals => { :container_id => the_container_id, :form_name => the_form_name })}
      context.instance_eval(&cmd)
    end
    
    def render_proxy_component(context)
      the_component_partial = component_partial()
      the_proxy_component_object = proxy_component_object()
      cmd = proc{render(:partial => the_component_partial, :object => the_proxy_component_object)}
      context.instance_eval(&cmd)
    end
    
    def proxy_component_object()
      nil
    end
    
  end
  
end