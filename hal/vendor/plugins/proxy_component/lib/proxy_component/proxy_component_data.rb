module ProxyComponent

  class Data
    
    # helper to read attribute from proxy
    class << self
      def proxy_reader(*ids)
        for id in ids
          module_eval <<-"end;"
            def #{id.id2name}(*args, &block)
              @proxy.#{id.id2name}
            end
          end;
        end
      end
    end
    
    # helper to read attribute from result set row
    class << self
      def row_reader(*cols)
        for col in cols
          module_eval <<-"end;"
            def #{col.id2name}(*args, &block)
              @row['#{col.id2name.camelcase}']
            end
          end;
        end
      end
    end
    
  end
  
end