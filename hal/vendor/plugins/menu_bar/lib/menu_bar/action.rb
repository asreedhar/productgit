module MenuBar

  class Action < Node
    include MenuBar::Display
    include MenuBar::Validation
    
    attr_reader :link, :remote_options, :highlights, :link_html_options, :html_options
    attr_accessor :page
    
    def initialize parent, &block
      super(parent)
      @shortcut, @tooltip, @text, @icon = nil, nil, nil, nil
      @link, @remote_options, @highlights = nil, nil, []
      @html_options, @link_html_options = {}, {}
      @html_options[:class] = "#{class_name()} menu_bar_level_#{depth()}"
      instance_eval(&block);
    end
    
    def link
      result = @link.kind_of?(Proc) ? page.instance_eval(&@link) : @link
      check_hash result, 'link'
    end
    
    def highlighted? options={}
      option = clean_unwanted_keys(options)
      result = false
      @highlights.each do |highlight|
        break if result
        highlighted = true
        raise "You are an infernal idiot as there is no page!" if page.nil? && highlight.kind_of?(Proc)
        h = highlight.kind_of?(Proc) ? page.instance_eval(&highlight) : highlight
        check_hash h, 'highlight'
        h = clean_unwanted_keys(h)
        h.each_key do |key|
          val = options
          key.to_s.split(%r{[\[\]]}).each do |k|
            break unless val.kind_of?(Hash) && val.key?(k)
            val = val[k]
          end
          ref = (key.to_s == 'controller') ? h[key].gsub(%r{^/}, "") : h[key] # FUDGE-TASTIC
          highlighted &= (ref.to_s == val.to_s)
        end
        result |= highlighted
      end
      return result
    end
    
    def html_options(options=nil)
      @html_options.merge!(options) if options
      @html_options
    end
    
    def link_html_options(options=nil)
      @link_html_options.merge!(options) if options
      @link_html_options
    end
    
    private
    
    def links_to(link={})
      @link = check_hash_or_proc link, 'links_to'
      unless @link.kind_of?(Proc)
        highlights_on @link unless @link.empty?
      end
    end
    
    def links_to_remote(link={},options={})
      @link = check_hash_or_proc link, 'links_to_remote link'
      @remote_options = check_hash_or_proc options, 'links_to_remote options'
    end
    
    def highlights_on(options={})
      @highlights << check_hash_or_proc(options, 'highlight_on')
    end
    
  end
  
end