module MenuBar

  module MenuBarHelper
  
    protected
    
    def clone_options(options,context)
      clone_options = {}
      options.each do |k,v|
        clone_options[k] = v.kind_of?(Proc) ? context.instance_eval(&v) : (v.respond_to?(:clone) ? v.clone : v)
      end
      clone_options
    end
    
    def delete_empty_values(options)
      options.each do |k,v|
        options.delete(k) if v.nil? || v.empty?
      end
      options
    end
    
    def link_tag(component, html_options)
      link, remote_options, tag = nil, nil, nil
      if component.kind_of?(Menu)
        link = component.default_action.link
        remote_options = component.default_action.remote_options
      else
        link = component.link
        remote_options = component.remote_options
      end
      if remote_options
        if remote_options.empty?
          # pretty big assumption: leaving the remote hash alone you're doing a menu_bar_component
          link = clone_options(link, component).update(:menu_bar_component => true)
          remote_options = { :complete => "Element.replace($('#{component.root_node.class.to_s.underscore}_component'), request.responseText);", :url => link }
          tag = link_to_remote(component.text, remote_options, html_options)
        else
          tag = link_to_remote(component.text, clone_options(remote_options, component).update(:url => link), html_options)
        end
      else
        tag = link_to(component.text, link, html_options)
      end
      tag
    end
    
    def content(menu)
      result = ""
      menu.content.each do |content_item|
        content_item.page=self
        result << "\n"
        result << tag('div', content_item.html_options, true)
        result << "\n"
        result << render(:partial => content_item.partial, :locals => content_item.locals)
        result << "\n</div>\n"
      end
      result
    end
    
    def menu(m)
      # make sure we have context for the dynamic actions
      m.page = self if m.kind_of?(Menu)
      # ul
      action = nil
      tear_offs = []
      result = ""
      result << tag('div', {:class => "menu_bar_level_#{m.depth()}"}, true) if m.respond_to?(:is_tear_off) && m.is_tear_off == true
      result << tag('ul', m.html_options , true)
      result << "\n"
      # take a ref to the components and loop
      components = m.components
      components.each do |c|
        # safe copy of component
        component = c.clone
        component.page=self
        # skip if it's not visible
        next unless component.visible?
        # cache whether is highlighted
        highlighted = component.highlighted?(params)
        # especially if it's an action
        action = component if highlighted && component.kind_of?(Action)
        # css for <li>
        li_html_options = clone_options(component.html_options, component)
        li_html_options[:id] << '_li' if li_html_options[:id]
        li_html_options[:class] = '' unless li_html_options[:class]
        li_html_options[:class] << ' active ' if highlighted
        li_html_options[:class].strip!
        delete_empty_values(li_html_options)
        # css for <a>
        link_html_options = component.respond_to?(:link_html_options) ? clone_options(component.link_html_options, component) : {}
        link_html_options[:class] = '' unless link_html_options[:class]
        link_html_options[:class] << ' active ' if highlighted
        link_html_options[:class].strip!
        link_html_options[:title] = component.tooltip if component.tooltip
        link_html_options[:accesskey] = component.shortcut if component.shortcut
        link_html_options['hh:interaction'] = "click:#{component.path}" if component.respond_to?(:path)
        delete_empty_values(link_html_options)
        # li
        result << tag('li', li_html_options, true)
        if component.instance_of?(Menu)
          if component.default_action
            result << link_tag(component, link_html_options)
          else
            result << content_tag('span', component.text, link_html_options)
          end
          result << component.icon if component.icon
          if component.is_tear_off
            tear_offs << component if highlighted
          else
            html, ignore_tear_offs = * menu(component)
            result << content(component)
            result << "\n"
            result << html
          end
        elsif component.link.empty?
          result << content_tag('span', component.text, link_html_options)
        else
          result << link_tag(component, link_html_options)
        end
        result << "</li>\n"
      end
      result << "</ul>"
      result << content(m) if m.respond_to?(:content) && (!m.respond_to?(:is_tear_off) || m.respond_to?(:is_tear_off) && m.is_tear_off == true)
      result << '</div>' if m.respond_to?(:is_tear_off) && m.is_tear_off == true
      [result, tear_offs, action]
    end
    
    def menu_bar(menu_bar_sym)
      menu(eval(menu_bar_sym.id2name.camelcase).instance)
    end
    
    def menu_tag(m)
      result, tear_offs, action = * menu(m)
      result << "\n"
      result << tag('div', {:id => m.html_options[:id].gsub('/', '_') + '_container', :class => 'container'}, true)
      result << "\n"
      [result, tear_offs, action]
    end
    
    def menu_bar_tag(menu_bar_sym)
      mb = eval(menu_bar_sym.id2name.camelcase).instance
      open, tear_offs, action = * menu_tag(mb)
      close = '</div>'
      while !tear_offs.empty?
        html, new_tear_offs, action = menu_tag(tear_offs.shift)
        tear_offs.concat(new_tear_offs)
        open << html
        close << '</div>'
      end
      [open, close, action]
    end
    
    def render_menu_bar_component(menu_bar_sym)
      require menu_bar_sym.to_s
      open_menu_tag, close_menu_tag, action = * menu_bar_tag(menu_bar_sym)
      link = action.link
      html = "<div id=\"#{menu_bar_sym.to_s}_component\">"
      html << open_menu_tag
      html << render_component(:controller => link[:controller], :action => link[:action], :id => link[:id], :params => link.reject{|k,v| [:controller,:action,:id].include?(k)})
      html << close_menu_tag
      html << "</div>"
      html
    end
    
  end
end