module MenuBar
  class Node
    attr_reader :parent, :children
    def initialize parent=nil
      @parent = parent
      @children = []
      parent.append_child(self) unless parent.nil?
    end
    def append_child node=nil
      raise "No child supplied" if node.nil?
      @children << node
    end
    def depth
      n = self
      d = -1
      while !n.nil? && n.kind_of?(Node)
        d += 1
        n = n.parent
      end
      d
    end
    def root_node
      n = self
      while n.kind_of?(Node) && !n.parent.nil?
        n = n.parent
      end
      n
    end
    def path
      txt = ""
      n = self
      while !n.nil? && n.kind_of?(Node)
        if n.respond_to?(:text)
          if txt.empty?
            txt = "#{n.text}"
          else
            txt = "#{n.text}/#{txt}"
          end
        end
        txt = "#{n.class}/#{txt}" if n.kind_of?(MenuBar::Base)
        n = n.parent
      end
      txt = txt.gsub(/&amp;/,'&')
      txt
    end
  end
end