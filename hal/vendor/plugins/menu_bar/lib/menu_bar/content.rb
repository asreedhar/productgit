module MenuBar

  class Content < Node
    include MenuBar::Validation
    
    attr_reader :partial, :locals, :html_options
    attr_accessor :page
    
    def initialize parent, &block
      super(parent)
      @partial, @locals, @html_options = nil, {}, {}
      instance_eval(&block);
    end
    
    def partial
      result = @partial.kind_of?(Proc) ? page.instance_eval(&@partial) : @partial
      check_string result, 'partial'
    end
    
    def locals
      result = @locals.kind_of?(Proc) ? page.instance_eval(&@locals) : @locals
      check_hash result, 'locals'
    end
    
    def html_options(options=nil)
      @html_options.merge!(options) if options
      @html_options
    end
    
    private
    
    def with_partial(partial)
      @partial = check_string_or_proc partial, 'with_partial'
    end
    
    def with_locals(locals={})
      @locals = check_hash_or_proc locals, 'with_locals'
    end
    
  end
  
end