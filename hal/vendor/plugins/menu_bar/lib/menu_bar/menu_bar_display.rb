module MenuBar

  module Display
    
    def class_name
      self.class.to_s.underscore.gsub('/', '_')
    end
    
    # getters: called from helper
 
    def shortcut
      result = @shortcut.kind_of?(Proc) ? page.instance_eval(&@shortcut) : @shortcut
      check_string result, 'shortcut'
    end
    
    def text
      result = @text.kind_of?(Proc) ? page.instance_eval(&@text) : @text
      check_string result, 'name'
    end
    
    def tooltip
      result = @tooltip.kind_of?(Proc) ? page.instance_eval(&@tooltip) : @tooltip
      check_string result, 'tooltip'
    end
    
    def icon
      result = @icon.kind_of?(Proc) ? page.instance_eval(&@icon) : @icon
      check_string result, 'icon'
    end
    
    # setters: called from initialize block
    
    def with_text(text)
      @text = check_string_or_proc(text, 'name')
    end
    
    def with_shortcut(shortcut)
      @shortcut = check_string_or_proc(shortcut, 'shortcut')
    end
    
    def with_tooltip(title)
      @title = check_string_or_proc(title, 'title')
    end
    
    def with_icon(icon)
      @icon = check_string_or_proc(icon, 'icon')
    end
    
    # conditionals
    
    def show_if(condition)
      @condition = check_string_or_proc(condition, 'condition')
    end
    
    def visible?
      return true if @condition.nil?
      if @condition.kind_of?(String)
        result = page.instance_eval(@condition)
      elsif @condition.kind_of?(Proc)
        result = page.instance_eval(&@condition)
      end
      return result 
    end
    
  end
  
end