module MenuBar
  class Menu < Node
    include MenuBar::Display
    include MenuBar::Validation
    
    attr_reader :components, :content, :html_options, :default_action, :is_tear_off
    attr_accessor :page
    
    def initialize parent, &block
      super(parent)
      @shortcut, @tooltip, @text, @icon = nil, nil, nil, nil
      @default_action = nil
      @is_tear_off = false
      @components = []
      @content = []
      @html_options = {}
      @html_options[:class] = "#{class_name()} menu_bar_level_#{depth()}"
      instance_eval(&block);
    end
    
    def add_action &block
      raise "you should provide a block" if !block_given? 
      action = MenuBar::Action.new(self, &block)
      @components << action
      return action
    end
    
    def add_actions &block
      raise "you should provide a block" if !block_given? 
      @components << block
      return block
    end
    
    def add_menu &block
      raise "you should provide a block" if !block_given? 
      menu = MenuBar::Menu.new(self, &block)
      @components << menu
      return menu
    end
    
    def add_content &block
      raise "you should provide a block" if !block_given? 
      content = MenuBar::Content.new(self, &block)
      @content << content
      return content
    end
    
    def components
      dyna_components = []
      @components.each do |component|
        if component.kind_of?(Proc)
          new_components = page.instance_eval(&component)
          new_components.each{|c| raise "is not an action: #{c.class} from #{component}" unless c.kind_of?(Action) }
          dyna_components.concat(new_components)
        else
          dyna_components << component
        end
      end
      dyna_components
    end
    
    def html_options(options=nil)
      @html_options.merge!(options) if options
      @html_options
    end
    
    def default_action
      return @default_action unless @default_action.nil?
      c = components.detect{|c| c.kind_of?(Menu) ? true : (c.link.empty? ? false : c.visible?) }
      return c.instance_of?(Menu) ? c.default_action : c
    end
    
    def highlighted? options={}
      result = false
      components.each do |component|
        component.page = page
        result |= component.highlighted?(options)
      end
      return result
    end
    
    private
    
    def set_tear_off
      @is_tear_off = true
    end
    
    def with_default_action(action)
      @default_action = action
    end
    
  end
end