module MenuBar
  class Base < Node
    include Singleton
    attr_accessor :components, :content, :html_options
    
    def self.add_action(&block)
      raise "you should provide a block" if !block_given? 
      action = MenuBar::Action.new(instance, &block)
      instance.components ||= []
      instance.components << action
      action
    end
    
    def self.add_menu(&block)
      raise "you should provide a block" if !block_given? 
      menu = MenuBar::Menu.new(instance, &block)
      instance.components ||= []
      instance.components << menu
      menu
    end
    
    def self.add_content &block
      raise "you should provide a block" if !block_given? 
      content = MenuBar::Content.new(instance, &block)
      instance.content << content
      return content
    end
    
    def self.html_options(options)
      instance.html_options.merge!(options)
    end
    
    private
    
    def initialize
      super(nil)
      @components = []
      @content = []
      @html_options = {}
      @html_options[:id] = self.class.to_s.underscore.gsub('/', '_')
      @html_options[:class] = "#{@html_options[:id]} menu_bar_level_0"
    end
    
  end
end