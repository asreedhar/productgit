module MenuBar

  module Validation
    
    def check_string_or_proc(param, param_name)
      unless param.kind_of?(String) or param.kind_of?(Proc) 
        raise "param #{param_name} should be a String or a Proc but is a #{param.class}"
      end 
      param 
    end
    
    def check_hash_or_proc(param, param_name)
      unless param.kind_of?(Hash) or param.kind_of?(Proc) 
        raise "param #{param_name} should be a Hash or a Proc but is a #{param.class}"
      end
      param 
    end
    
    def check_string(param, param_name)
      return if param.nil?
      raise "param #{param_name} should be a String but is a #{param.class}" unless param.kind_of?(String)  
      param
    end
    
    def check_hash(param, param_name)
      raise "param '#{param_name}' should be a Hash but is #{param.inspect}" unless param.kind_of?(Hash)
      param
    end
    
    def clean_unwanted_keys(hash)
      ignored_keys = [:only_path, :use_route]
      hash.dup.delete_if{|key,value| ignored_keys.include?(key)}
    end
    
  end
  
end