$LOAD_PATH << File.expand_path( File.dirname(__FILE__) + '/../lib' )

require 'log4r'
require 'cas_auth'

require 'test/unit'
require 'test/unit/ui/console/testrunner'

class TC_CAS_Filter < Test::Unit::TestCase

  def setup  
  	@request = Class.new do
 	  class << self
	    def env
	  	  @props
	  	end
	  	def env=(props)
	  	  @props = props
	  	end
	  	def protocol
	  	end
	  	def request_uri
	  	  ""
	  	end
  	  end
  	end
  	@request.env = {"HTTP_X_AUTH_HOST" => "http://auth.foo.com/cas"}
  	
  	@controller = Class.new do
 	  class << self
 	  	def params
 	  	end
 	    def request
 	      @req
  		end
  		def request=(req)
  		  @req = req
  		end
  	  end
  	end
  	@controller.request = @request
  	
  	CAS::Filter.cas_base_url = "http://www.bar.com/cas"
  	CAS::Filter.validate_url = "http://www.bar.com/cas/serviceValidate"
  	CAS::Filter.logout_url = "http://www.bar.com/cas/logout"
  	CAS::Filter.server_name = "www.bar.com"
  end

  def test_get_auth_host_url
  	url = CAS::Filter.get_auth_host_url(@request, "www.bar.com", 'login')
  	assert_equal(url, "http://auth.foo.com/cas/login")
  	
  	@request.env = {"HTTP_X_AUTH_HOST" => "http://auth.foo.com/cas/"}
  	url = CAS::Filter.get_auth_host_url(@request, "www.bar.com", 'blargle')
  	assert_equal(url, "http://auth.foo.com/cas/blargle")
  	
  	@request.env = {}
  	url = CAS::Filter.get_auth_host_url(@request, "www.bar.com", 'blargle')
  	assert_equal(url, "www.bar.com")
  	
  	url = CAS::Filter.get_auth_host_url(@request, "www.bar.com/login", 'blargle')
  	assert_equal(url, "www.bar.com/login")
  end

  def test_login_url
  	url = CAS::Filter.login_url(@controller)
  	assert_equal(url, "http://auth.foo.com/cas/login")
  end
  
  def test_validate_url
    url = CAS::Filter.validate_url(@controller)
  	assert_equal(url, "http://auth.foo.com/cas/serviceValidate")
  end
  
  def test_logout_url
    url = CAS::Filter.logout_url(@controller)
  	assert_equal(url, "http://auth.foo.com/cas/logout?service=www.bar.com")
  end
  
  def test_configured_host
  	host = CAS::Filter.configured_host(@request)
  	assert_equal(host, CAS::Filter.server_name)
  end
end

Test::Unit::UI::Console::TestRunner.run(TC_CAS_Filter)