class TestEmailValidation
  EmailAddress = begin
    qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]'
    dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]'
    atom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-' +
      '\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+'
    quoted_pair = '\\x5c[\\x00-\\x7f]'
    domain_literal = "\\x5b(?:#{dtext}|#{quoted_pair})*\\x5d"
    quoted_string = "\\x22(?:#{qtext}|#{quoted_pair})*\\x22"
    domain_ref = atom
    sub_domain = "(?:#{domain_ref}|#{domain_literal})"
    word = "(?:#{atom}|#{quoted_string})"
    domain = "#{sub_domain}(?:\\x2e#{sub_domain})*"
    local_part = "#{word}(?:\\x2e#{word})*"
    addr_spec = "#{local_part}\\x40#{domain}"
    group = "#{addr_spec}(?: *\\x2C *#{addr_spec})*"
    #phrase = "(?: *#{word}? *)+"
    #route = "\\x40#{domain}\\x3A"
    #route_addr = "\\x3C(?:#{route})?#{addr_spec}\\x3E"
    #mailbox = "(?:#{addr_spec}|#{phrase}#{route_addr})"
    #address = "(?:#{mailbox}|#{group})"
    pattern = /\A#{group}\z/
  end
  def TestEmailValidation.assert(address,expected)
    success = ((address =~ EmailAddress) != nil)
    print 'err: ' + address + "\n" if ((success && !expected) || (!success && expected))
  end
end

print "NON ADDRESSES\n----------\n"

[
  'abc',
  '@b.c',
  '',
  ' '
].each do |k,v|
  TestEmailValidation::assert(k,false);
end

print "COMMENTED ADDRESSES\n----------\n"

[
  'jerry@eagle.uucp (Jerry Schwarz)', 
  'jerry@eagle.ATT.COM (Jerry Schwarz)',
  'Pete(A wonderful \) chap) <pete(his account)@silly.test(his host)>',
  'John Doe <jdoe@machine(comment).  example>',
  '1234   @   local(blah)  .machine .example'
].each do |k,v|
  TestEmailValidation::assert(k,false);
end

print "ADDRESSES\n-----------\n"

[
  'root@localhost',
  'Jones@Registry.Org',
  'Smith@Registry.Org',
  '"Al Neuman"@Mad-Host',
  'Sam.Irving@Other-Host',
  'Ken Davis <KDavis@This-Host.This-net>',
  'George Jones <Group@Some-Reg.An-Org>',
  '<642@eagle.UUCP>',
  'Mailer-Daemon <daemon@somewhere.com>',
  'Nathainel Borenstein <nsb@bellcore.com>',
  '199407072116.RAA14128@CS.UTK.EDU',
  'John Doe <jdoe@machine.example>',
  '"Joe Q. Public" <john.q.public@example.com>',
  '"Giant; \"Big\" Box" <sysservices@example.net>',
  '"Mary Smith: Personal Account" <smith@home.example>',
  '1234@local.machine.example',
  'jdoe@test   . example',
  'root @   127.0.0.1',
  'George Jones<Group@Host>',
  'George Jones<@foo:Group@Host>',
  'Alfred Neuman <Neuman@BBN-TENEXA>',
  'Neuman@BBN-TENEXA',
  '"George, Ted" <Shared@Group.Arpanet>',
  'swenmouth@firstlook.biz, vmease@incisent.com   , noah.keen@gmail.com,"al green"@cemetery.dead'
].each do |k,v|
  TestEmailValidation::assert(k,true);
end
