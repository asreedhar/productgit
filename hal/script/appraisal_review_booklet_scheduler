#!/usr/bin/env ruby

# sample arguments for application
# ARGV[0] = 'development'
# ARGV[1] = '2007-08-27T09:08:00'
# ARGV[1] = 'CDT'

unless ARGV.length == 3
	puts "Usage:"
	puts "\tappraisal_review_booklet_scheduler <environment> <localtime> <timezone>"
	exit
end

unless ['development', 'alpha', 'beta', 'production'].include?(ARGV[0])
	puts "Error: Environment not recognized"
	puts "Usage:"
	puts "\tappraisal_review_booklet_scheduler <environment> <localtime> <timezone>"
	exit
end

unless /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/ === ARGV[1]
	puts "Error: Local Time not in format YYYY-MM-DD'T'HH:MM:SS"
	puts "Usage:"
	puts "\tappraisal_review_booklet_scheduler <environment> <localtime> <timezone>"
	exit
end

if ARGV[2].empty?
	puts "Error: TimeZone not specified"
	puts "Usage:"
	puts "\tappraisal_review_booklet_scheduler <environment> <localtime> <timezone>"
	exit
end

ENV['RAILS_ENV'] = ARGV[0]

require File.expand_path(File.dirname(__FILE__) + "/../config/environment")

require 'action_controller'
require 'action_controller/test_process'
require 'action_controller/integration'

require 'application'
require 'dynamic_controller'
require 'appraisal_review_booklet_controller'

require 'appraisal_review_booklet'
require 'appraisal_review_booklet_preference'
require 'appraisal_review_booklet_schedule'
require 'appraisal_review_booklet_summary'
require 'business_unit'
require 'future_task'
require 'future_task_status'
require 'future_task_type'
require 'member'

require 'ReliableMessageQueueDriver'
require 'PrinceXmlDriver'

class AppraisalReviewBookletController
  skip_before_filter CAS::Filter, :only => [:appraisal_review_booklet_request]
  skip_before_filter PromotionFilter, :only => [:appraisal_review_booklet_request]
end

class AppraisalReviewBookletClient
  include ActionController::TestProcess
  attr_reader :controller, :request, :response
  def initialize()
    @controller = AppraisalReviewBookletController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end
  def queue_request(business_unit, preference, days_back)
    params = {
      "appraisal_review_booklet_summary" => {
        "include_appraisal_review_detail"               => (preference.include_appraisal_review_detail ? "1" : "0"),
        "include_appraisal_review_summary"              => (preference.include_appraisal_review_summary ? "1" : "0"),
        "include_appraisal_review_high_mileage_detail"  => (preference.include_appraisal_review_high_mileage_detail ? "1" : "0"),
        "include_appraisal_review_high_mileage_summary" => (preference.include_appraisal_review_high_mileage_summary ? "1" : "0"),
        "include_appraisal_review_follow_up_detail"     => (preference.include_appraisal_review_follow_up_detail ? "1" : "0"),
        "include_appraisal_review_follow_up_summary"    => (preference.include_appraisal_review_follow_up_summary ? "1" : "0"),
        "include_appraisal_summary"                     => (preference.include_appraisal_summary ? "1" : "0"),
        "include_appraisal_summary_high_mileage"        => (preference.include_appraisal_summary_high_mileage ? "1" : "0"),
        "email_notification"                            => (preference.email_notification ? "1" : "0"),
        "email_address"                                 => (preference.email_address),
        "days_back"                                     => days_back
      }
    }
    session = {
      :business_unit => business_unit
    }
    get :appraisal_review_booklet_request, params, session
    unless (flash[:notice].nil? || flash[:notice].empty?)
      flash[:notice].each{|msg| STDERR.puts msg}
    end
  end
end

schedules = ActiveRecord::Base.connection().select_all(<<-SQL
	WITH schedule (
		[id],
		[business_unit_id],
		[interval_id],
		[day_of_week],
		[hour_of_day]
	)
	AS (
		SELECT	s.id 'appraisal_review_booklet_schedule_id', s.business_unit_id, i.id 'interval_id', i.day_of_week, s.hour_of_day
		FROM	dbo.appraisal_review_booklet_schedules s
		JOIN	dbo.appraisal_review_booklet_schedules_intervals si on si.appraisal_review_booklet_schedule_id = s.id
		JOIN	dbo.intervals i on si.interval_id = i.id
		JOIN	dbo.business_units bu on s.business_unit_id = bu.id	
		WHERE	i.interval_type_id = 2
	)
	SELECT
			s.business_unit_id, arp.days_back
	FROM
			schedule s
	JOIN	dbo.business_units_time_zones butz ON s.business_unit_id = butz.business_unit_id
	JOIN dbo.appraisal_review_preferences AS arp ON s.business_unit_id = arp.business_unit_id
	CROSS JOIN dbo.time_zones tz
	LEFT JOIN
			(
				SELECT	s.id, s.interval_id, MAX(x.day_of_week) day_of_week
				FROM	schedule s
				JOIN	schedule x on x.id = s.id
				WHERE	s.day_of_week > x.day_of_week
				GROUP
				BY		s.id, s.interval_id
			) x on x.id = s.id AND x.interval_id = s.interval_id
	LEFT JOIN
			(
				SELECT	s.id, s.interval_id, MAX(x.day_of_week) day_of_week
				FROM	schedule s
				JOIN	schedule x on x.id = s.id
				WHERE	s.day_of_week < x.day_of_week
				GROUP
				BY		s.id, s.interval_id
			) y on y.id = s.id AND y.interval_id = s.interval_id
	WHERE	s.day_of_week = DATEPART(DW, dbo.GETTZDATE(
				butz.time_zone_id,
				DATEADD(
					HH,
					-tz.utc,
					CONVERT(DATETIME, '#{ARGV[1]}', 126))))
	AND		s.hour_of_day = DATEPART(HH, dbo.GETTZDATE(
				butz.time_zone_id,
				DATEADD(
					HH,
					-tz.utc,
					CONVERT(DATETIME, '#{ARGV[1]}', 126))))
	AND		tz.code = '#{ARGV[2]}'
  SQL
)

schedules.each do |schedule|
  preference = AppraisalReviewBookletPreference.find(
    :first,
    :conditions => ["business_unit_id = ?", schedule['business_unit_id']]
  )
  client = AppraisalReviewBookletClient.new
  client.queue_request(BusinessUnit.find(schedule['business_unit_id']), preference, schedule['days_back'] )
end
