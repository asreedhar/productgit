#!/bin/sh

RAILS_ENV=

case `hostname -s` in
  ub9bweb-ruby01x) RAILS_ENV=alpha;;
  betaweb01 | betaweb02) RAILS_ENV=beta;;
  prodweb13 | prodweb14) RAILS_ENV=production;;
  *) RAILS_ENV=development;;
esac

date +'%FT%T %Z' | xargs ruby /var/www/apps/hal/script/appraisal_review_booklet_scheduler $RAILS_ENV
