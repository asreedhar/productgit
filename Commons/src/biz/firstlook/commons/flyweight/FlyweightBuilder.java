package biz.firstlook.commons.flyweight;

/**
 * <p>Builder pattern interface for the FlyweightFactory.</p>
 * @author swenmouth
 * @param <F> flyweight class
 * @param <K> key which defines an equivalence to the flyweight 
 */
public interface FlyweightBuilder<F,K> {
	/**
	 * Instantiate and return a new instance of a flyweight class.
	 * @param key the data from which the flyweight is to be constructed
	 * @return a new flyweight populated with data from the key 
	 */
	public F newFlyweight(K key);
}
