package biz.firstlook.commons.flyweight;

import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * <p>Implementatation of the FlyweightPool interface backed by a PriorityQueue whose
 * ordering is defined by last-access.</p>
 * @author swenmouth
 * @param <F> flyweight
 * @param <K> flyweight key
 */
public class PriorityQueueFlyweightPool<F,K> implements FlyweightPool<F,K> {
	Queue<Entry<F>> queue;
	int capacity;
	public PriorityQueueFlyweightPool(int capacity) {
		this.capacity = capacity; 
		this.queue    = new PriorityBlockingQueue<Entry<F>>(capacity);
	}
	public F get(K key) {
		for (Entry<F> e : queue) {
			if (key.equals(e.flyweight)) {
				e.lastAccessed = System.currentTimeMillis();
				return e.flyweight;
			}
		}
		return null;
	}
	public void put(K key, F flyweight) {
		if (queue.size() == capacity) {
			queue.poll();
		}
		queue.add(new Entry<F>(flyweight));
	}
	static class Entry<F> implements Comparable<Entry<F>> {
		long lastAccessed;
		F flyweight;
		public Entry(F flyweight) {
			this.lastAccessed = System.currentTimeMillis();
			this.flyweight = flyweight;
		}
		public int compareTo(Entry<F> o) {
			if (o instanceof Entry) {
				final long other = ((Entry<F>) o).lastAccessed;
				return (lastAccessed > other) ? 1 : (lastAccessed < other ? -1 : 0);
			}
			return 0;
		}
	}
}
