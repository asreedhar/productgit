package biz.firstlook.commons.flyweight;

/**
 * <p>A collection of flyweights.</p>
 * @author swenmouth
 * @param <F> flyweight
 * @param <K> flyweight key
 */
public interface FlyweightPool<F,K> {
	/**
	 * Returns the flyweight to which this pool maps the specified key. 
	 * @param  key key whose associated flyweight is to be returned
	 * @return the flyweight to which the pool maps the key, or null if the pool
	 *         contains no mapping for this key.
	 */
	public F get(K key);
	/**
	 * Associates the specfied flyweight with the specified key in this pool
	 * @param key the key with which the specified flyweight is to be associated
	 * @param flyweight flyweight to be associated with the specified key
	 */
	public void put(K key, F flyweight);
}
