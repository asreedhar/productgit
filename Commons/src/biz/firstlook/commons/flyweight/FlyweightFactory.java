package biz.firstlook.commons.flyweight;

/**
 * <p>Creates and manages flyweight objects. This class implements the
 * FlyweightFactory pattern.</p>
 * @author swenmouth
 * @param <F> Flyweight class
 * @param <K> Flyweight key
 * @param <B> FlyweightBuilder
 */
public class FlyweightFactory<F,K,B extends FlyweightBuilder<F,K>> {
	FlyweightPool<F,K> pool;
	B builder;
	/**
	 * Create a new flyweight factory instance.
	 * @param pool the caching/pooling mechanism for the flyweights
	 * @param builder the builder class to get new instances of flyweights
	 */
	public FlyweightFactory(FlyweightPool<F,K> pool, B builder) {
		this.pool = pool;
		this.builder = builder;
	}
	/**
	 * <p>When a client requests a flyweight, this method supplies an existing
	 * instance or creates one, if none exists.</p>
	 * @param key object which defines a unique equivalence with a flyweight
	 * @return an instance of the flyweight for the given key
	 */
	public F getFlyweight(K key) {
		F flyweight = pool.get(key);
		if (flyweight == null) {
			flyweight = builder.newFlyweight(key);
			pool.put(key, flyweight);
		}
		return flyweight;
	}
}
