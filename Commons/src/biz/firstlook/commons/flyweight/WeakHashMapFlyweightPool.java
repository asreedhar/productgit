package biz.firstlook.commons.flyweight;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * <p>Implementation of the FlyweightPool interface backed by a WeakHashMap.</p>
 * <p>You need to wrap the flyweights in a <code>WeakReference</code> to account
 * for flyweights that refer to the key under which they are stored. An example of
 * this would be a "type-safe string" class that wraps the string and the string
 * is used as the key under which the flyweight is "put".</p> 
 * @author swenmouth
 * @param <F> flyweight
 * @param <K> flyweight key
 */
public class WeakHashMapFlyweightPool<F,K> implements FlyweightPool<F,K> {
	Map<K,WeakReference<F>> pool = Collections.synchronizedMap(new WeakHashMap<K,WeakReference<F>>());
	public F get(K key) {
		WeakReference<F> r = pool.get(key);
		if (r != null) {
			return r.get();
		}
		return null;
	}
	public void put(K key, F flyweight) {
		pool.put(key, new WeakReference<F>(flyweight));
	}
}
