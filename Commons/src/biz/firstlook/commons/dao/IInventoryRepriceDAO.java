package biz.firstlook.commons.dao;

import java.util.Date;

import biz.firstlook.commons.enumeration.RepriceSouce;


public interface IInventoryRepriceDAO
{

	/**
	 * Inserting a reprice event has the following effects:
	 * 	1) Updates the Inventory's ListPrice with the new value
	 * 	2) Creates a new RepriceEvent
	 *  3) Updates the Inventory Item's Next Plan Date
	 *  4) Creates all RepriceExport rows necessary for the Reprice Processor to 
	 *  	update third parties (Internet Advertisers, DMS)
	 * 
	 * @param inventoryId
	 * @param value
	 * @param originalListPrice
	 * @param beginDate
	 * @param endDate
	 * @param createdBy
	 * @param lastModified
	 * @param repriceConfirmed
	 */
	public void insertRepriceEvent( Integer businessUnitId, Integer inventoryId, Integer value, Double originalListPrice, 
			Date beginDate, Date endDate, String createdBy, Date lastModified,  
			Boolean repriceConfirmed, RepriceSouce repriceSouce );

}