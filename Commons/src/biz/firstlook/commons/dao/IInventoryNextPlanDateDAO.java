package biz.firstlook.commons.dao;

import java.util.Date;

public interface IInventoryNextPlanDateDAO {

	public Date getNextPlanDate( Integer inventoryId, Integer ageInflation );
}
