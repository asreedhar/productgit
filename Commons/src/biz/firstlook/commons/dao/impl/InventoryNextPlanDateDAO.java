package biz.firstlook.commons.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.commons.dao.IInventoryNextPlanDateDAO;

public class InventoryNextPlanDateDAO extends JdbcDaoSupport implements IInventoryNextPlanDateDAO {

	public Date getNextPlanDate(Integer inventoryId, Integer ageInflation) {
		String sql = "select dbo.GetNextPlanDate( " + inventoryId + ", " + ageInflation + " )";
		return (Date)getJdbcTemplate().query( sql, new NextPlanDateRowMapper() ).get(0);
	}

	class NextPlanDateRowMapper implements RowMapper
	{
		public Object mapRow( ResultSet row, int arg1 ) throws SQLException
		{
			return row.getDate(1);
		}
	}	
	
}
