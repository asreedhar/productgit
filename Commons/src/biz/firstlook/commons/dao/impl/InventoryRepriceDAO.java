package biz.firstlook.commons.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.commons.dao.IInventoryRepriceDAO;
import biz.firstlook.commons.enumeration.RepriceSouce;

/**
 * This class is used to synchronize the reprice "events" between IMT and serviceFramework codebase. IMT does not have the new AIP code context;
 * this is used to get around that.
 */
public class InventoryRepriceDAO extends StoredProcedure implements IInventoryRepriceDAO
{

private static final String STORED_PROC_NAME = "InsertRepriceEvent";

private static final String PARAM_RESULT_SET = "ResultSet";
private static final String PARAM_RC = "RC";

private static final String PARAM_INVENTORY_ID = "inventoryID";
private static final String PARAM_BEGIN_DATE = "beginDate";
private static final String PARAM_END_DATE = "endDate";
private static final String PARAM_VALUE = "value";
private static final String PARAM_CREATED_BY = "createdBy";
private static final String PARAM_ORIGINAL_LIST_PRICE = "originalListPrice";
private static final String PARAM_LAST_MODIFIED = "lastModified";
private static final String PARAM_CONFIRMED = "confirmed";
private static final String PARAM_SOURCE = "source";

public InventoryRepriceDAO( DataSource dataSource )
{
	super( dataSource, STORED_PROC_NAME );
	setFunction( false );
	declareParameter( new SqlReturnResultSet( PARAM_RESULT_SET, new InsertRepriceEventResultSetExtractor() ) );
	//declareParameter( new SqlOutParameter( PARAM_RC, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_INVENTORY_ID, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_BEGIN_DATE, Types.DATE ) );
	declareParameter( new SqlParameter( PARAM_END_DATE, Types.DATE ) );
	declareParameter( new SqlParameter( PARAM_VALUE, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_CREATED_BY, Types.VARCHAR ) );
	declareParameter( new SqlParameter( PARAM_ORIGINAL_LIST_PRICE, Types.DECIMAL ) );
	declareParameter( new SqlParameter( PARAM_LAST_MODIFIED, Types.TIMESTAMP ) );
	declareParameter( new SqlParameter( PARAM_CONFIRMED, Types.BOOLEAN) );
	declareParameter( new SqlParameter( PARAM_SOURCE, Types.INTEGER) );
	compile();
}

/* (non-Javadoc)
 * @see biz.firstlook.commons.services.inventory.impl.IInventoryRepriceDAO#insertRepriceEvent(java.lang.Integer, java.lang.Integer, java.sql.Timestamp, java.sql.Timestamp, java.lang.Integer, java.lang.String, java.lang.Double, java.sql.Timestamp, java.lang.Integer)
 */
public void insertRepriceEvent( Integer businessUnitId, Integer inventoryId, Integer value, Double originalListPrice, 
								Date beginDate, Date endDate, String createdBy, Date lastModified,  
								Boolean repriceConfirmed, RepriceSouce repriceSouce )
{
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put( PARAM_INVENTORY_ID, inventoryId );
	parameters.put( PARAM_BEGIN_DATE, beginDate );
	parameters.put( PARAM_END_DATE, endDate );
	parameters.put( PARAM_VALUE, value );
	parameters.put( PARAM_CREATED_BY, createdBy );
	parameters.put( PARAM_ORIGINAL_LIST_PRICE, originalListPrice );
	parameters.put( PARAM_LAST_MODIFIED, lastModified );
	parameters.put( PARAM_CONFIRMED, repriceConfirmed);
	parameters.put( PARAM_SOURCE, repriceSouce.getId());
	
	logger.debug( parameters );
	Map results = execute( parameters );
	logger.debug( results.get( PARAM_RC ) );
	
	//return (List)results.get( PARAM_RESULT_SET );
}

private class InsertRepriceEventResultSetExtractor implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	//do nothing, this is just an update.
	return null;
}

}

}
