package biz.firstlook.commons.services.fault;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.xfire.XFireRuntimeException;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.services.fault.*;

public class FaultService {
	
	private static final String END_POINT_PROPERTY = "fault.webservice.endpoint";
	
	private static final String END_POINT_PROPERTY_URI;
	
	static {
		END_POINT_PROPERTY_URI = PropertyLoader.getProperty(END_POINT_PROPERTY);
	}
	
	private static final FaultService instance = new FaultService();
	
	/**
	 * Returns the instance of this object.
	 * @return Returns the instance of this object.
	 */
	public static FaultService getInstance() {
		return instance;
	}
	
	private FaultSoap service;
	
	/**
	 * Calls createService to create the proxy to the web service being invoked, and sets the service private variable
	 * with the returned proxy.
	 */
	private FaultService() {
		service = createService();
	}
	
	/**
	 * Creates the proxy that will be used to communicate to the web service.
	 * @return	Returns the proxy to the web service being invoked.
	 */
	private FaultSoap createService() {
		return new FaultClient().getFaultSoap(END_POINT_PROPERTY_URI);

	}
	
	/**
	 * Returns the proxy to the web service being invoked.
	 * @return	Returns the proxy to the web service being invoked.
	 */
	private FaultSoap getService() {
		return service;
	}

	/**
	 * This method will constuct the SaveRemoteArgumentsDto which consists of the FaultEvent to be saved.
	 * After the dto is built, a web service call is made to the Fault web service, to store the event in the Fault database.
	 *  
	 * @param e					The exception that was thrown.
	 * @param application		The application that threw the exception.
	 * @param machine			The machine the exception was generated on.
	 * @param platform			This should be hardcoded as "Java"
	 * @param user				The user signed on when the exception occurred.
	 * @param path				The path associated with the exception.
	 * @param url				The url associated with the exception.
	 * @param userHostAddress	The user host address associated with the exception
	 * @return					Returns the saved fault event (including an ID assigned when it is saved successfully).
	 * @throws DatatypeConfigurationException	Throws this exception.
	 */
	public SaveRemoteResultsDto saveException(Throwable e, String application, String machine, String platform, String user, String path, String url, String userHostAddress)
		throws DatatypeConfigurationException{
		try {
						
			SaveRemoteArgumentsDto arguments = new SaveRemoteArgumentsDto();
			FaultEventDto faultEvent = new FaultEventDto();
			
			//Get the current date in the appropriate form for the web service call
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
	        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
	        XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);	 
			
			faultEvent.setFaultTime(now);
			
			ApplicationDto applicationDto = new ApplicationDto();
			applicationDto.setName(application);
			
			MachineDto machineDto = new MachineDto();
			machineDto.setName(machine);

			PlatformDto platformDto = new PlatformDto();
			platformDto.setName(platform);
			
			//Build the request information object
			RequestInformationDto requestInformation = new RequestInformationDto();
			requestInformation.setPath(path);
			requestInformation.setUser(user);
			requestInformation.setUserHostAddress(userHostAddress);
			requestInformation.setUrl(url);
			
			faultEvent.setApplication(applicationDto);
			faultEvent.setMachine(machineDto);
			faultEvent.setPlatform(platformDto);
			faultEvent.setRequestInformation(requestInformation);
			
			//Start building the fault dto object
			FaultDto fault =  new FaultDto();
			//Put message on fault, not fault event anymore
			fault.setMessage(e.getMessage());			

			//If the exception has a causing exception, process causing FaultDtos
			if(e.getCause()!=null){
				fault.setCause(ProcessCauseFault(e.getCause()));
			}
			
			fault.setStack(buildStack(e));
			fault.setExceptionType(e.getClass().toString());			
			faultEvent.setFault(fault);
			
			arguments.setFaultEvent(faultEvent);
			
			//Make the web service call to save the fault, and return the saved fault event, encapsulated in the SaveRemoteResultsDto object.
			return getService().saveFault(arguments);
		}
		catch (XFireRuntimeException xfr) {
			throw xfr;
		}
		catch(DatatypeConfigurationException err){
			throw err;
		}
		
	}
	
	/**
	 * This method will recursively process causing exceptions if they exist, building the appropriate chain.
	 * 
	 * @param e		The exception to process
	 * @return		Returns a FaultDto, which is comprised of a chain of causing exceptions if they exist.
	 */
	public FaultDto ProcessCauseFault(Throwable e){
	
		FaultDto faultChain = new FaultDto();	
		faultChain.setExceptionType(e.getClass().toString());
		//Save off inner exception message
		faultChain.setMessage(e.getMessage());
		
		//If the exception has not cause, then process the stack, set the cause to null, and return the FaultDto
		if(e.getCause() == null){
			faultChain.setStack(buildStack(e));
			faultChain.setCause(null);
			return faultChain;
		}

		//If the exception has a causing exception, recursively call this method until all causing exceptions have been processed.
		faultChain.setCause(ProcessCauseFault(e.getCause()));
		faultChain.setStack(buildStack(e));
		return faultChain;
		
	}
	
	/**
	 * This method will build a StackDto composed of multiple stackframes (as stack frame dtos) associated with the passed in exception.
	 * 
	 * @param e		The exception to process
	 * @return		Returns a stackdto which is part of a FaultDto
	 */
	public StackDto buildStack(Throwable e){
		StackDto stack = new StackDto();
		
		//Process the stack trace if it exists
		if(e.getStackTrace()!=null){	

			//Get the stack frames from the stack trace
			StackTraceElement[] stackFrames = e.getStackTrace();
			ArrayOfStackFrameDto stackList = new ArrayOfStackFrameDto();
			
			//Loop through all of the frames, grabbing the pertinent information
			for(int i=0;i<=stackFrames.length-1; i++){

				StackFrameDto frame = new StackFrameDto();
				frame.setClassName(stackFrames[i].getClassName());
				frame.setFileName(stackFrames[i].getFileName());
				frame.setLineNumber(stackFrames[i].getLineNumber());
				frame.setIsNativeMethod(stackFrames[i].isNativeMethod());
				frame.setMethodName(stackFrames[i].getMethodName());		
				stackList.getStackFrameDto().add(frame);
			}

			//Assign the array of stack frames to the stack
			stack.setStackFrames(stackList);
		}		
		return stack;
		
	}

}
