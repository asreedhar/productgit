package biz.firstlook.commons.services.vehicleCatalog;

import java.util.ArrayList;


/**
 * This is a special bean used to describe a vehicle. It is *highly* coupled to
 * the VehicleContainerCall back and the stored procedure GetVehicleAttributes.
 * It should not be used outside of this Commons pacakge.  It's only use is to 
 * map GetVehicleAttributes results to VehicleCatalogEntries through
 * getVehicleCatalogEntry and getVehicleCatalogMultiEntry 
 *  
 * @author nkeen
 *
 */
class VehicleContainer {	
	
	// Known unique values for this vehicles attributes. 
	// Always only one Make, Model, ModelYear, Segment, vin, makeModelGrouping
	private String _make;
	private String _model; // This is the MODEL from MMG, not the Edmunds Model, which is stored in Model. Eg) Model = CIVIC, LINE = CIVIC LS. Important bc Model is what is used to querry for all Performance/Analytics info
	private Integer _modelYear;
	private String _vin;
	private Integer _makeModelGroupingId;
	private Integer _modelId; //This is model id from vehicle catalog
	private Integer _groupingDescriptionId;
	private String _groupingDescription;
	
	// Collections of possible attributes which can potential describe this vehicle
	private ArrayList<VehicleCatalogKey> catalogKeys = new ArrayList<VehicleCatalogKey>();
	private ArrayList<String> lines = new ArrayList<String>();
	private ArrayList<String> bodyTypes = new ArrayList<String>();
	private ArrayList<Integer> bodyTypeIds = new ArrayList<Integer>();
	private ArrayList<String> series = new ArrayList<String>();
	private ArrayList<String> doors = new ArrayList<String>();
	private ArrayList<String> fuelTypes = new ArrayList<String>();
	private ArrayList<String> engines = new ArrayList<String>();
	private ArrayList<String> driveTypeCodes = new ArrayList<String>();
	private ArrayList<String> transmissions = new ArrayList<String>();
	private ArrayList<String> cylinderQuantities = new ArrayList<String>();
	private ArrayList<Integer> segmentIds = new ArrayList<Integer>();
	private ArrayList<String> segments = new ArrayList<String>();

	// Known unique values for this vehicles attributes. 
	// Those with null have multiple possible values contained in the corresponding collection above   
	private VehicleCatalogKey _catalogKey; // not an attribute on vehicle
	private String _line; // This is the MODEL the catalog (Edmunds Model), which is stored in OriginalModel. This is persisted but not really used expect for reference.
	private String _bodyType; // not an attribute on vehicle
	private Integer _bodyTypeId;
	private String _series; // aka: trim. 
	private String _doors;
	private String _fuelType;
	private String _engine;
	private String _driveTypeCode; 
	private String _transmission;
	private String _cylinderQuantity;
	private Integer _segmentId; // not an attribute on vehicle
	private String _segment;
	
	/**
	 * Returns a new VehicleCatalogEntry as represented by the Container
	 * If an attribute does not have a distinct value (either
	 * directly set or a collection of size 1) then the attribute
	 * value on the vehicle is set to null.
	 * This mimics the results of the storedProcedure used to populate
	 * this container.
	 * 
	 */
	public VehicleCatalogEntry getVehicleCatalogEntry() {
		VehicleCatalogEntry vehicle = new VehicleCatalogEntry();
		vehicle.setVin( getVin() );
		vehicle.setCatalogKey(getCatalogKey());
		vehicle.setModelYear( getModelYear() );
		vehicle.setMake(getMake());
		vehicle.setLine( getLine() );
		vehicle.setModel(getModel()); 
		vehicle.setBodyType( getBodyType() );
		vehicle.setSeries( getSeriess() );
		vehicle.setMakeModelGroupingId( getMakeModelGroupingId() );
		vehicle.setModelId( getModelId() );
		vehicle.setDoors( getDoorss() );
		vehicle.setFuelType(getFuelType());
		vehicle.setEngine(getEngine());
		vehicle.setDriveTrain(getDriveTypeCode());
		vehicle.setTransmission(getTransmission());
		vehicle.setCylinderCount(getCylinderQuantity());
		vehicle.setSegmentId(getSegmentId());
		vehicle.setSegment(getSegment());
		vehicle.setBodyTypeId( getBodyTypeId() );
		vehicle.setBodyType( getBodyType() );		
		vehicle.setGroupingDescriptionId( getGroupingDescriptionId());
		vehicle.setGroupingDescription( getGroupingDescription());
		return vehicle;
	}

	/**
	 * Returns a new VehicleCatalogMultiEntry as represented by the Container
	 * This populates all the gaurenteed to be unique results fields 
	 * and populates the variable fiels (catalogKey, trim) with lists of all
	 * possible values
	 */
	public VehicleCatalogMultiEntry getVehicleCatalogMultiEntry() {
		VehicleCatalogMultiEntry vehicle = new VehicleCatalogMultiEntry();
		vehicle.setVin( getVin() );
		vehicle.setCatalogKeys(getCatalogKeys());
		vehicle.setModelYear( getModelYear() );
		vehicle.setMake(getMake());
		vehicle.setLine( getLine() );
		vehicle.setModel(getModel()); 
		vehicle.setBodyType( getBodyType() );
		vehicle.setTrims( getSeries() );
		vehicle.setMakeModelGroupingId( getMakeModelGroupingId() );
		vehicle.setModelId( getModelId() );
		vehicle.setDoors( getDoorss() );
		vehicle.setFuelType(getFuelType());
		vehicle.setEngine(getEngine());
		vehicle.setDriveTrain(getDriveTypeCode());
		vehicle.setTransmission(getTransmission());
		vehicle.setCylinderCount(getCylinderQuantity());
		vehicle.setSegmentId(getSegmentId());
		vehicle.setSegment(getSegment());
		vehicle.setGroupingDescriptionId( getGroupingDescriptionId());
		vehicle.setGroupingDescription( getGroupingDescription());
		return vehicle;
	}
	
	public String getBodyType() {
		String result = null;
		if (_bodyType != null) {
			result = _bodyType;
		}
		else if (bodyTypes.size() == 1) {
			result = bodyTypes.get(0);
		}
		return result;	
	}
	public void setBodyType(String type) {
		_bodyType = type;
	}	
	
	public String getLine() {
		String result = null;
		if (_line != null) {
			result = _line;
		}
		else if (lines.size() == 1) {
			result = lines.get(0);
		}
		return result;	
	}
	public void setLine(String line) {
		_line = line;
	}		
	
	public Integer getCylinderQuantity() {
		Integer result = null;
		if (_cylinderQuantity != null) {
			result = new Integer(_cylinderQuantity);
		}
		else if ((cylinderQuantities.size()) == 1 && (cylinderQuantities.get(0) != null)) {
			result = new Integer(cylinderQuantities.get(0));
		}
		return result;			
	}
	public void setCylinderQuantity(String quantity) {
		_cylinderQuantity = quantity;
	}
	
	public VehicleCatalogKey getCatalogKey() {
		VehicleCatalogKey result = null;
		if (_catalogKey != null) {
			result = _catalogKey;
		}
		else if (catalogKeys.size() == 1) {
			result = catalogKeys.get(0);
		}
		return result;	
	}
	public void setCatalogKey(VehicleCatalogKey catalogKey) {
		this._catalogKey = catalogKey;
	}

	
	/**
	 * Return the unqiue value or if the collection only
	 * has one element, return that. Otherwise null.
	 * 
	 * @return
	 */
	public Integer getSegmentId() {
		Integer result = null;
		if (_segmentId != null) {
			result = new Integer(_segmentId);
		}
		else if ((segmentIds.size() == 1) && (segmentIds.get(0) != null)) {
			result = new Integer(segmentIds.get(0));
		}
		return result;		
	}
	public void setSegmentId(Integer bodyTypeId) {
		_segmentId = bodyTypeId;
	}
	
	public Integer getDoorss() {
		Integer result = null;
		if (_doors != null) {
			result = new Integer(_doors);
		}
		else if ((doors.size() == 1) && (doors.get(0) != null)) {
			result = new Integer(doors.get(0));
		} 
		return result;			
	}
	public void setDoors(String _doors) {
		this._doors = _doors;
	}
	
	public String getDriveTypeCode() {
		String result = null;
		if (_driveTypeCode != null) {
			result = _driveTypeCode;
		}
		else if (driveTypeCodes.size() == 1) {
			result = driveTypeCodes.get(0);
		}
		return result;	
	}
	public void setDriveTypeCode(String typeCode) {
		_driveTypeCode = typeCode;
	}
	
	public String getEngine() {
		String result = null;
		if (_engine != null) {
			result = _engine;
		}
		else if (engines.size() == 1) {
			result = engines.get(0);
		}
		return result;	
	}
	public void setEngine(String _engine) {
		this._engine = _engine;
	}
	
	public String getFuelType() {
		String result = null;
		if (_fuelType != null) {
			result = _fuelType;
		}
		else if (fuelTypes.size() == 1) {
			result = fuelTypes.get(0);
		}
		return result;	
	}
	public void setFuelType(String type) {
		_fuelType = type;
	}
	
	public Integer getBodyTypeId() {
		Integer result = null;
		if (_bodyTypeId != null) {
			result = _bodyTypeId;
		}
		else if (bodyTypeIds.size() == 1) {
			result = bodyTypeIds.get(0);
		}
		return result;	
	}
	public void setBodyTypeId(Integer _bodyStyleId) {
		this._bodyTypeId= _bodyStyleId;
	}
	
	public String getSeriess() {
		String result = null;
		if (_series != null) {
			result = _series;
		}
		else if (series.size() == 1) {
			result = series.get(0);
		}
		return result;	
	}
	public void setSeriess(String _series) {
		this._series = _series;
	}
	
	public String getTransmission() {
		String result = null;
		if (_transmission != null) {
			result = _transmission;
		}
		else if (transmissions.size() == 1) {
			result = transmissions.get(0);
		}
		return result;	
	}
	public void setTransmission(String _transmission) {
		this._transmission = _transmission;
	}
	
	public String getSegment() {
		String result = null;
		if (_segment != null) {
			result = _segment;
		}
		else if (segments.size() == 1) {
			result = segments.get(0);
		}
		return result;	
	}
	public void setSegment(String _segment) {
		this._segment = _segment;
	}
	
	/**
	 * the following classes have 1 and only 1 values per VIN pattern
	 */
	public String getMake() {
		return _make;
	}
	public void setMake(String _make) {
		this._make = _make;
	}
	public Integer getGroupingDescriptionId() {
		return _groupingDescriptionId;	
	}
	public void setGroupingDescriptionId(Integer descriptionId) {
		_groupingDescriptionId = descriptionId;
	}
	public Integer getMakeModelGroupingId() {
		return _makeModelGroupingId;	
	}
	public void setMakeModelGroupingId(Integer modelGroupingId) {
		_makeModelGroupingId = modelGroupingId;
	}
	public Integer getModelId() {
		return _modelId;
	}
	public void setModelId(Integer modelId) {
		_modelId = modelId;
	}	
	public String getModel() {
		return _model;		
	}
	public void setModel(String _model) {
		this._model = _model;
	}
	public Integer getModelYear() {
		return _modelYear;	
	}
	public void setModelYear(Integer year) {
		_modelYear = year;
	}
	public String getVin() {
		return _vin;
	}
	public void setVin(String type) {
		_vin = type;
	}	
	public void setGroupingDescription( String groupingDescription )
	{
		_groupingDescription = groupingDescription;
	}
	public String getGroupingDescription()
	{
		return _groupingDescription;
	}
	
	public ArrayList<String> getBodyTypes() {
		return bodyTypes;
	}
	public ArrayList<Integer> getBodyTypeIds() {
		return bodyTypeIds;
	}

	public ArrayList<String> getCylinderQuantities() {
		return cylinderQuantities;
	}

	public ArrayList<VehicleCatalogKey> getCatalogKeys() {
		return catalogKeys;
	}

	public ArrayList<Integer> getSegmentIds() {
		return segmentIds;
	}

	public ArrayList<String> getDoors() {
		return doors;
	}

	public ArrayList<String> getDriveTypeCodes() {
		return driveTypeCodes;
	}

	public ArrayList<String> getEngines() {
		return engines;
	}
	public ArrayList<String> getFuelTypes() {
		return fuelTypes;
	}
	public ArrayList<String> getSeries() {
		return series;
	}
	public ArrayList<String> getTransmissions() {
		return transmissions;
	}
	
	// Add Methods. Adds an element to the corresponding collection
	public void addCatalogKey(VehicleCatalogKey key) {
		this.catalogKeys.add(key);
	}
	public void addBodyType(String bodyType) {
		this.bodyTypes.add(bodyType);
	}
	public void addBodyTypeId(Integer bodyTypeId) {
		this.bodyTypeIds.add(bodyTypeId);
	}
	public void addLine(String line) {
		this.lines.add(line);
	}
	public void addSeries(String series) {
		this.series.add(series);
	}
	public void addDoors(String doors) {
		this.doors.add(doors);
	}
	public void addFuelType(String fuelType) {
		this.fuelTypes.add(fuelType);
	}
	public void addEngine(String engine) {
		this.engines.add(engine);
	}
	public void addDriveTypeCode(String driveTypeCode) {
		this.driveTypeCodes.add(driveTypeCode);
	}
	public void addSegment(String segment) {
		this.segments.add(segment);
	}
	public void addTransmission(String transmission) {
		this.transmissions.add(transmission);
	}
	public void addCylinderQuantity(String cylinderQuantity) {
		this.cylinderQuantities.add(cylinderQuantity);
	}
	public void addSegmentId(Integer segmentId) {
		this.segmentIds.add(segmentId);
	}
	
}
