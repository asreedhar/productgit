package biz.firstlook.commons.services.vehicleCatalog;

/**
 * Package local exception thrown by the VehicleContainerCallback to indicate that we expected
 * data from the stored procedure but none was found.
 * @author swenmouth
 */
class MissingDataException extends RuntimeException {

	private static final long serialVersionUID = 331401765136705005L;

	public MissingDataException(String message) {
		super(message);
	}

}
