package biz.firstlook.commons.services.vehicleCatalog;

import java.io.Serializable;
import java.util.List;

public class VehicleCatalogMultiEntry extends VehicleCatalogBaseEntry implements Serializable
{

private static final long serialVersionUID = 8785461579244197793L;

private List<String> trims; //series
private List<VehicleCatalogKey> catalogKeys;

public List< VehicleCatalogKey > getCatalogKeys()
{
	return catalogKeys;
}
public void setCatalogKeys( List< VehicleCatalogKey > catalogKeys )
{
	this.catalogKeys = catalogKeys;
}
public List< String > getTrims()
{
	return trims;
}
public void setTrims( List< String > trims )
{
	this.trims = trims;
}


}
