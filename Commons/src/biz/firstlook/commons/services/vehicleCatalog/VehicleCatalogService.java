package biz.firstlook.commons.services.vehicleCatalog;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.services.vehicleCatalog.VehicleContainerCallBack.VehicleContainerCallBackMode;
import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

// package scoped to force apps to use interface
class VehicleCatalogService extends StoredProcedureTemplate implements IVehicleCatalogService, InitializingBean
{
	
private static final Logger logger = Logger.getLogger( VehicleCatalogService.class );
//nk - currently, by default everything is US. In the future this will be a dealer pref
private static final Integer US_COUNTRY_CODE = Integer.valueOf(1); 

//cache of all makes, since new makes only appear every year.
private final List<String> makes = new ArrayList<String>();

//cache cleaner
private final Timer makesCacheTimer = new Timer("VehicleCatalog-Makes-Timer", true);

@Override
public void afterPropertiesSet() {
	super.afterPropertiesSet();
	
	//clear the cache every 12 hours
	long delay = 0; //run once right after spring is done loading.
	long period = 1000 * 60 * 60 * 12; //1000 millisecond/sec * 60 sec/min * 60 min/hour * 12 hours = X milliseconds
	makesCacheTimer.schedule(new TimerTask() {
		@Override
		public void run() {
			synchronized (makes) {
				makes.clear();
			}
		}
	}, 
	delay, period);
}

/**
 * Returns a VehicleCatalogMultiEntry based on VIN
 * Used during the second stage of the tradeAnalyzer and in the Vehicle Info Frame
 * @param vin
 * @throws VehicleCatalogServiceException 
 */
public VehicleCatalogMultiEntry retrieveVehicleCatalogMultiEntry( String vin ) throws VehicleCatalogServiceException 
{
	VehicleContainer vc = null;
	try {
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add( new SqlParameterWithValue( "@vin", Types.VARCHAR, vin) );

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetVehicleAttributes @VIN=?}");
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		vc = (VehicleContainer)this.callWithCallBack( sprocRetrieverParams, new VehicleContainerCallBack(VehicleContainerCallBackMode.RETRIEVE_ALL));
	} catch (MissingDataException e) {
		throw new NoSuchVehicleException(vin, e);
	} catch (DataAccessException e) {
		logger.error("DB Exception while trying to access the VehicleCatalog", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to access the VehicleCatalog", e);
	} catch (Exception e) {
		logger.error("Exception while trying to access the VehicleCatalog for VIN : " + vin, e);
		throw new VehicleCatalogServiceException("System Exception while trying to access the VehicleCatalog for VIN : " + vin, e);
	}
	
	return vc.getVehicleCatalogMultiEntry();
}

/**
 * Returns a VehicleCatalogMultiEntry based on VIN
 * Intended to be used to populate the CatalogKey drop downs during the
 * VIN-less appraisal process.
 * @param make
 * @param model
 * @param modelYear
 * @throws VehicleCatalogServiceException 
 */
public VehicleCatalogMultiEntry retrieveVehicleCatalogMultiEntry(String make, String model, int modelYear) throws VehicleCatalogServiceException {
	VehicleContainer vc = null;
	try {
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add(new SqlParameterWithValue( "@MAKE", Types.VARCHAR, make) );
		parameters.add( new SqlParameterWithValue( "@MODEL", Types.VARCHAR, model) );
		parameters.add( new SqlParameterWithValue( "@MODELYEAR", Types.INTEGER, modelYear) );
		
		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetVehicleAttributes @MAKE=?, @MODEL=?, @MODELYEAR=? }");
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		// retreive a VehicleContainer with all Extended 
		vc = (VehicleContainer)this.callWithCallBack( sprocRetrieverParams, new VehicleContainerCallBack(VehicleContainerCallBackMode.RETRIEVE_ALL));
	} catch (MissingDataException e) {
		throw new NoSuchVehicleException(make, model, modelYear, e);
	}catch (DataAccessException e) {
		logger.error("DB Exception while trying to access the VehicleCatalog", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to access the VehicleCatalog");
	}
	return vc.getVehicleCatalogMultiEntry();
}

/**
 * Calls the same stored procedure: GetVehicleAttributes 
 * but with a make, model, modelYear and CatalogKey.
 * This is used for Vinless appraisal when a VIN can not be found in
 * the system and the user specifies make, mdoel, year and catalogkey,
 * aka the Vin-less appraisal process
 * @param vin The full VIN of a vehicle
 * @param catalogKey This is one of the values in the 'CatalogKey' 
 * 							  Column returned as the second result set from 
 * 						      GetVehicleAttributes
 * @throws VehicleCatalogServiceException 
 */
public VehicleCatalogEntry retrieveVehicleCatalogEntry(String make, String model, int modelYear, Integer catalogKeyId) throws VehicleCatalogServiceException {
	VehicleContainer vc = null;
	try {
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add(new SqlParameterWithValue( "@MAKE", Types.VARCHAR, make) );
		parameters.add( new SqlParameterWithValue( "@MODEL", Types.VARCHAR, model) );
		parameters.add( new SqlParameterWithValue( "@MODELYEAR", Types.INTEGER, modelYear) );
		parameters.add( new SqlParameterWithValue( "@CATALOGKEYID", Types.INTEGER, catalogKeyId) );

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetVehicleAttributes @MAKE=?, @MODEL=?, @MODELYEAR=?, @VEHICLECATALOGID=? }");
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		vc = (VehicleContainer)this.callWithCallBack( sprocRetrieverParams, new VehicleContainerCallBack(VehicleContainerCallBackMode.RETRIEVE_UNIQUE_VEHICLE));
	} catch (MissingDataException e) {
		throw new NoSuchVehicleException(make, model, modelYear, catalogKeyId, e);
	} catch (DataAccessException e) {
		logger.error("DB Exception while trying to access the VehicleCatalog", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to access the VehicleCatalog");
	}
	return vc.getVehicleCatalogEntry();
}

/**
 * Calls the same stored procedure: GetVehicleAttributes 
 * but with a Unique catalogKey. This and a Vin are 
 * a combination which uniquely identifies a "complete" Vehicle 
 * "description" in our catalog (currently edmunds).
 * 
 * @param vin The full VIN of a vehicle
 * @param catalogKey This is one of the values in the 'CatalogKey' 
 * 							  Column returned as the second result set from 
 * 						      GetVehicleAttributes
 * @throws VehicleCatalogServiceException 
 */
public VehicleCatalogEntry retrieveVehicleCatalogEntry(String vin, Integer catalogKeyId) throws VehicleCatalogServiceException {
	VehicleContainer vc = null;
	try {
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add( new SqlParameterWithValue( "@VIN", Types.VARCHAR, vin) );
		parameters.add( new SqlParameterWithValue( "@CATALOGKEYID", Types.INTEGER, catalogKeyId) );

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetVehicleAttributes @VIN=?, @VEHICLECATALOGID=? }");
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		vc = (VehicleContainer)this.callWithCallBack( sprocRetrieverParams, new VehicleContainerCallBack(VehicleContainerCallBackMode.RETRIEVE_UNIQUE_VEHICLE));
	} catch (MissingDataException e) {
		throw new NoSuchVehicleException(vin, catalogKeyId, e);
	} catch (DataAccessException e) {
		logger.error("DB Exception while trying to access the VehicleCatalog", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to access the VehicleCatalog");
	}
	return vc.getVehicleCatalogEntry();
	
}

/**
 *  Calls GetVehicleAttributes - retruns a single vehicleCatalogEntry
 *  this is the basic interaction with the vehicleCatalog 
 * @param vin
 * @throws VehicleCatalogServiceException 
 */
public VehicleCatalogEntry retrieveVehicleCatalogEntry( String vin ) throws VehicleCatalogServiceException
{
	VehicleContainer vc = null;
	try {
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add( new SqlParameterWithValue( "@VIN", Types.VARCHAR, vin) );

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetVehicleAttributes @VIN=? }");
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		vc = (VehicleContainer)this.callWithCallBack( sprocRetrieverParams, new VehicleContainerCallBack(VehicleContainerCallBackMode.RETRIEVE_UNIQUE_VEHICLE));
	} catch (MissingDataException e) {
		throw new NoSuchVehicleException(vin, e);
	} catch (DataAccessException e) {
		logger.error("DB Exception while trying to access the VehicleCatalog", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to access the VehicleCatalog");
	}
	return vc.getVehicleCatalogEntry();
}

/**
 * Uses the function GetMakes in IMT to retrieve all unique make values.
 *
 * @return Returns a Collection of Strings. Each is a Make value.
 * @throws VehicleCatalogServiceException 
 *  
 */
@SuppressWarnings("unchecked")
public List<String> retrieveAllMakes() throws VehicleCatalogServiceException {
	synchronized (makes) {
		if(makes.isEmpty()) {
			try {
				List<String> allMakes = (List<String>)query("SELECT Make FROM dbo.GetMakes() WHERE Make <> 'UNKNOWN' ORDER BY Make", new VehicleValueRowMapper());
				makes.addAll(allMakes);
			} catch ( Exception e ) {
				logger.error("DB Exception while trying to exec GetMakes()", e);
				throw new VehicleCatalogServiceException("DB Exception while trying to exec dbo.GetMakes()");
			}
		}
		return makes;
	}
}

/**
 * Uses the function GetModels in IMT to retrieve all unique model values
 * for the given Make.
 * 
 * Note: 	What is returned is Model: (LINE + TRIM). 
 * 		 	E.g. : For Make=HONDA, model results = CIVIC LS, CIVIC LT
 *
 * @return Returns a Collection of Strings. Each is a possible Model value for the given make.
 * @throws VehicleCatalogServiceException 
 */
@SuppressWarnings("unchecked")
public List<String> retrieveAllModels(String selectedMake, String selectedYear) throws VehicleCatalogServiceException {
	List<String> allModels = new ArrayList<String>();
	try
	{
		StringBuilder queryString = new StringBuilder( "select * from GetModels(?, ?, ? )" );
		Object[] parameters = { selectedMake, ("null".equals( selectedYear )) ? null : selectedYear, US_COUNTRY_CODE };
		allModels = (List<String>)query(queryString.toString(), parameters, new VehicleValueRowMapper());
	}
	catch ( Exception e )
	{
		logger.error("DB Exception while trying to exec GetModels(?, ? )", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to exec GetModels(?, ? )");
	}
	return allModels;
}

/**
 * Uses the function GetModelTrims in IMT to retrieve all unique Trims for the
 * given Make and Model. Make and Model should both be values from the retrieveAllMakes()
 * and retrieveAllModels() - iwo, Makes and Models that are used in Analytics - not raw 
 * Catalog (Edmunds) data. For example - Mode is (LINE + TRIM), e.g. CIVIC LS not just CIVIC.
 * 
 * 
 * @param make Make to find trims for.
 * @param model Model to find trims for. (LINE + TRIM). 
 * 				E.g. : For Make=HONDA, model results = CIVIC LS, CIVIC LT
 * @return A collection of Trims as Strings
 * @throws VehicleCatalogServiceException 
 */
@SuppressWarnings("unchecked")
public List<String> retrieveAllModelTrims(String make, String model) throws VehicleCatalogServiceException {
	List<String> allTrims = new ArrayList<String>();
	Object[] parameters = { make, model, US_COUNTRY_CODE };
	try
	{
		allTrims = (List<String>)query("select * from GetModelTrims( ? , ?, ?)", parameters, new VehicleValueRowMapper());
	}
	catch ( Exception e )
	{
		logger.error("DB Exception while trying to exec GetModelTrims( ? , ?)", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to exec GetModelTrims( ? , ?)");
	}
	return allTrims;
}

@SuppressWarnings("unchecked")
public List<String> retrieveModelYears( Integer groupingDescriptionId, boolean asc ) throws VehicleCatalogServiceException
{
	List<String> allYears = new ArrayList<String>();
	
	StringBuilder query = new StringBuilder( "SELECT	DISTINCT ModelYear ");
	query.append(" FROM	VehicleCatalog.Firstlook.ModelYear MY");
	query.append(" JOIN MakeModelGrouping MMG ON MY.ModelID = MMG.ModelID");
	query.append(" WHERE	MMG.GroupingDescriptionID = ?");
	query.append(" ORDER BY ModelYear");
	query.append( ( asc ) ?  " asc " : " desc" );
	
	Object[] parameters = { groupingDescriptionId };
	try
	{
		allYears = (List<String>)query(query.toString(), parameters, new VehicleValueRowMapper());
	}
	catch ( Exception e )
	{
		logger.error("DB Exception while trying to retrieveModelYears", e);
		throw new VehicleCatalogServiceException("DB Exception while trying to retrieveModelYears");
	}
	return allYears;
}

private class VehicleValueRowMapper implements ResultSetExtractor {
	public ArrayList<String> extractData(ResultSet rs) throws SQLException,
		DataAccessException {
		ArrayList<String> result = new ArrayList<String>();
		while (rs.next()) 
		{
			result.add(rs.getString(1)); 
		} 
		return result;
	}
}



}
	
