package biz.firstlook.commons.services.vehicleCatalog;

import java.io.Serializable;

public class VehicleCatalogEntry extends VehicleCatalogBaseEntry implements Serializable
{

private static final long serialVersionUID = 6930549300426549503L;

private String series = ""; //series - Nk - is set to be defaulted on the DB. 
private VehicleCatalogKey catalogKey;

public VehicleCatalogKey getCatalogKey()
{
	return catalogKey;
}
public void setCatalogKey( VehicleCatalogKey catalogKey )
{
	this.catalogKey = catalogKey;
}
public String getSeries()
{
	return series;
}
public void setSeries( String series )
{
	this.series = series;
}

}
