package biz.firstlook.commons.services.vehicleCatalog;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;


class VehicleContainerCallBack implements CallableStatementCallback {

	private static final Logger logger = Logger.getLogger( VehicleContainerCallBack.class );	
	enum VehicleContainerCallBackMode{
		RETRIEVE_ALL, // Process all result sets and returns a VehicleContainer with each field populated with ALL options for that field 
		RETRIEVE_UNIQUE_VEHICLE};  // Process Only the first result set entirely. This provides a complete Vehicle description with NULL values in columns which can not be unqiuely identified. Returns a VehicleContainer. The Vehicle can be accessed with the getVehicle method.
	
	private VehicleContainerCallBackMode mode = VehicleContainerCallBackMode.RETRIEVE_ALL; // default
	
	public VehicleContainerCallBack(VehicleContainerCallBackMode mode) {
		switch (mode) {
		case RETRIEVE_UNIQUE_VEHICLE:
			this.mode = VehicleContainerCallBackMode.RETRIEVE_UNIQUE_VEHICLE;
			break;
		default:
			this.mode = VehicleContainerCallBackMode.RETRIEVE_ALL;
			logger.info("VehicleBuilderCallBack initialized without mode. Using default.");
		}
	}
	
	/**
	 * 
	 * Returns a VehicleContainer populated based on the MODE set in the constructor.
	 * If no result is retrieved, then a MissingDataException is raised. 
	 * 
	 */
	public Object doInCallableStatement(CallableStatement statement) throws SQLException, DataAccessException {
		
		VehicleContainer vehicle = null;
		
		// EXEC GetVehicleAttributes
		statement.execute();

		// Result Set 1 : A column for each Vehicle Attribute with NULL values in Columns with mutliple possibilities.
		ResultSet rs = statement.getResultSet();
		if (rs != null) {
			// only instantiate if we have results. Otherwise return null.
			if (rs.next()) {
				vehicle = new VehicleContainer();
				vehicle.setVin( rs.getString("VIN") );
				vehicle.setModelYear(rs.getInt("ModelYear"));
				vehicle.setMake(rs.getString("Make"));
				vehicle.setModel(rs.getString("Model"));
				vehicle.setMakeModelGroupingId(rs.getInt("MakeModelGroupingId"));
				vehicle.setGroupingDescriptionId(rs.getInt("GroupingDescriptionId"));
				vehicle.setGroupingDescription( rs.getString("GroupingDescription") );
				vehicle.setModelId( rs.getInt( "ModelID" ) );
				
				if (mode == VehicleContainerCallBackMode.RETRIEVE_UNIQUE_VEHICLE) {
					vehicle.addCatalogKey(new VehicleCatalogKey(rs.getInt("VehicleCatalogID"), rs.getString("CatalogKey"))); 
					vehicle.addLine(rs.getString("Line"));
					vehicle.addBodyType(rs.getString("BodyType"));
					vehicle.addBodyTypeId(rs.getInt("BodyTypeID"));
					String series = rs.getString("Series");
					if (series != null) {
						vehicle.addSeries(series);
					}
					vehicle.addDoors(rs.getString("Doors"));
					vehicle.addFuelType(rs.getString("FuelType"));
					vehicle.addEngine(rs.getString("Engine"));
					String driveType = rs.getString("DriveTypeCode");
					if ((driveType == null) || (driveType.trim().length() == 0)) {
						driveType = "N/A";
					}
					vehicle.addDriveTypeCode(driveType);
					String trans = rs.getString("Transmission");
					if ((trans == null) || (trans.trim().length() == 0)) {
						trans = "N/A";
					}
					vehicle.addTransmission(trans);
					vehicle.addCylinderQuantity(rs.getString("CylinderQty"));
					vehicle.addSegmentId(rs.getInt("SegmentId"));
					vehicle.addSegment( rs.getString( "Segment" ));
					
					return vehicle;
				}
			}
			else {
				throw new MissingDataException("Expected vehicle catalog data");
				}
			}
		else {
			throw new MissingDataException("Expected vehicle catalog data");
		} 
		  
		// Result Set 2 : VehicleCatalogId and CatalogKey
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {	
				while (rs.next()) {
					vehicle.addCatalogKey(new VehicleCatalogKey(rs.getInt("VehicleCatalogId"), rs.getString("CatalogKey")));
				}
			}
		}
		
		// Result Set 3 : BodyType
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addBodyType(rs.getString("BodyType"));
				}
			}	
		}
		
		// Result Set 4 : BodyTypeId
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addBodyTypeId(rs.getInt("BodyTypeID"));
				}
			}	
		}

		// Result Set 5 : Series
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addSeries(rs.getString("Series"));
				}
			}	
		}

		// Result Set 6 : Doors
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addDoors(rs.getString("Doors"));
				}
			}	
		}
		
		// Result Set 7 : FuelType
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addFuelType(rs.getString("FuelType"));
				}
			}	
		}		
		
		// Result Set 8 : Engine
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addEngine(rs.getString("Engine"));
				}
			}	
		}
		
		// Result Set 9 : DriveTypeCode
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addDriveTypeCode(rs.getString("DriveTypeCode"));
				}
			}	
		}		

		// Result Set 10 : Transmission
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addTransmission(rs.getString("Transmission"));
				}
			}	
		}		
		
		// Result Set 11 : CylinderQuantity
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addCylinderQuantity(rs.getString("CylinderQty"));
				}
			}	
		}				

		// Result Set 13 : SegmentId
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addSegmentId(rs.getInt("SegmentId"));
				}
			}	
		}				
		// Result Set 14 : Segment
		if (statement.getMoreResults()) {
			rs = statement.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					vehicle.addSegment(rs.getString("Segment"));
				}
			}	
		}	
		
		return vehicle;
	}

}
