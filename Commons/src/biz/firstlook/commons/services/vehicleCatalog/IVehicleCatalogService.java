package biz.firstlook.commons.services.vehicleCatalog;

import java.util.List;

public interface IVehicleCatalogService {

/**
 * Returns a VehicleCatalogMultiEntry based on VIN
 * Used during the second stage of the tradeAnalyzer and in the Vehicle Info Frame
 * @param vin
 */
public VehicleCatalogMultiEntry retrieveVehicleCatalogMultiEntry( String vin ) throws VehicleCatalogServiceException;

/**
 * Uses the stored procedure GetVehicleAttributes in FLDW to retrieve all unique StyleKeys 
 * in the catalog for the given Make and Model. Make and Model should both be values from the 
 * calls: retrieveAllMakes() and retrieveAllModels() - iwo, Makes and Models that are used in 
 * Analytics - not raw Catalog (Edmunds) data. 
 * For example:  Mode is (LINE + TRIM), e.g. CIVIC LS not just CIVIC.
 * 
 * Note: Yes, this seems weird that you call it with the Analytics Make/Model arguments but get
 * back catalog trims, but its not if you consider that the analytics stuff is derrived from the 
 * catalog.
 * 
 * @param make Make to find trims for.
 * @param model Model to find trims for. (LINE + TRIM). 
 * 				E.g. : For Make=HONDA, model results = CIVIC LS, CIVIC LT
 * @return A collection of StyleKeys as Strings
 */
public VehicleCatalogMultiEntry retrieveVehicleCatalogMultiEntry(String make, String model, int modelYear) throws VehicleCatalogServiceException;

/**
 * Calls the same stored procedure: GetVehicleAttributes 
 * but with a make, model, modelYear and CatalogKey.
 * This is used for Vinless appraisal when a VIN can not be found in
 * the system and the user specifies make, mdoel, year and catalogkey,
 * aka the Vin-less appraisal process
 * @param vin The full VIN of a vehicle
 * @param catalogKey This is one of the values in the 'CatalogKey' 
 * 							  Column returned as the second result set from 
 * 						      GetVehicleAttributes
 */
public VehicleCatalogEntry retrieveVehicleCatalogEntry(String make, String model, int modelYear, Integer catalogKeyId) throws VehicleCatalogServiceException; 

/**
 * Calls the same stored procedure: GetVehicleAttributes 
 * but with a Unique catalogKey. This and a Vin are 
 * a combination which uniquely identifies a "complete" Vehicle 
 * "description" in our catalog (currently edmunds).
 * 
 * @param vin The full VIN of a vehicle
 * @param catalogKey This is one of the values in the 'CatalogKey' 
 * 							  Column returned as the second result set from 
 * 						      GetVehicleAttributes
 */
public VehicleCatalogEntry retrieveVehicleCatalogEntry(String vin, Integer catalogKeyId) throws VehicleCatalogServiceException;


/**
 *  Calls GetVehicleAttributes - retruns a single vehicleCatalogEntry
 *  this is the basic interaction with the vehicleCatalog 
 * @param vin
 */
public VehicleCatalogEntry retrieveVehicleCatalogEntry( String vin ) throws VehicleCatalogServiceException;

/**
 * Uses the function GetMakes in IMT to retrieve all unique make values.
 * @return Returns a Collection of Strings. Each is a Make value.
 */
public List<String> retrieveAllMakes() throws VehicleCatalogServiceException;

/**
 * Uses the function GetModels in IMT to retrieve all unique model values for the given Make.
 * Note: What is returned is Model: (LINE + TRIM). E.g. : For Make=HONDA, model results = CIVIC LS, CIVIC LT
 * @return Returns a Collection of Strings. Each is a possible Model value for the given make.
 */
public List<String> retrieveAllModels(String selectedMake, String selectedYear) throws VehicleCatalogServiceException;

/**
 * Uses the function GetModelTrims in IMT to retrieve all unique Trims for the
 * given Make and Model. Make and Model should both be values from the retrieveAllMakes()
 * and retrieveAllModels() - iwo, Makes and Models that are used in Analytics - not raw 
 * Catalog (Edmunds) data. For example - Mode is (LINE + TRIM), e.g. CIVIC LS not just CIVIC.
 * 
 * These trims can not be used to identify a Vehicle, but can be used for alaytic querries. 
 * 
 * @param make Make to find trims for.
 * @param model Model to find trims for. (LINE + TRIM). 
 * 				E.g. : For Make=HONDA, model results = CIVIC LS, CIVIC LT
 * @return A collection of Trims as Strings
 */
public List<String> retrieveAllModelTrims(String make, String model) throws VehicleCatalogServiceException;

/**
 * Retrieves all the years a model was made based off grouping description.
 * this is used in CIA - but probably not that usefully otherwise
 * @param groupingDescriptionId
 * @return List of years 
 */
public List<String> retrieveModelYears( Integer groupingDescriptionId, boolean ascending ) throws VehicleCatalogServiceException;
}