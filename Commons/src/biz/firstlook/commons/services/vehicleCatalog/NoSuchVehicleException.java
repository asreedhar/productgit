package biz.firstlook.commons.services.vehicleCatalog;

/**
 * Thrown to indicate we could not locate information for the vehicle(s) for the 
 * supplied information.
 * 
 * @author swenmouth
 */
public class NoSuchVehicleException extends VehicleCatalogServiceException {

	private static final long serialVersionUID = -1421316604234961843L;
	
	private String vin;
	private String make;
	private String model;
	private String modelYear;
	private Integer catalogKeyId;
	
	public NoSuchVehicleException(String vin, Throwable cause) {
		super(cause);
		this.vin = vin;
	}

	public NoSuchVehicleException(String vin, Integer catalogKeyId, Throwable cause) {
		super(cause);
		this.vin = vin;
		this.catalogKeyId = catalogKeyId;
	}

	public NoSuchVehicleException(String make, String model, int modelYear, Throwable cause) {
		super(cause);
		this.make = make;
		this.model = model;
		this.modelYear = Integer.toString(modelYear);
	}

	public NoSuchVehicleException(String make, String model, int modelYear, Integer catalogKeyId, Throwable cause) {
		super(cause);
		this.make = make;
		this.model = model;
		this.modelYear = Integer.toString(modelYear);
		this.catalogKeyId = catalogKeyId;
	}

	public String getMessage() {
		StringBuffer message = new StringBuffer("Unable to identify vehicle(s) with parameters: [");
		int fieldCount = 0;
		if (neitherNullNorEmpty(vin))
			message.append(fieldCount++ == 0 ? "" : ",").append("vin=").append(vin);
		if (neitherNullNorEmpty(make))
			message.append(fieldCount++ == 0 ? "" : ",").append("make=").append(make);
		if (neitherNullNorEmpty(model))
			message.append(fieldCount++ == 0 ? "" : ",").append("model=").append(model);
		if (neitherNullNorEmpty(modelYear))
			message.append(fieldCount++ == 0 ? "" : ",").append("modelYear=").append(modelYear);
		if (neitherNullNorEmpty(catalogKeyId.toString()))
			message.append(fieldCount++ == 0 ? "" : ",").append("catalogKey=").append(catalogKeyId.toString());
		return message.toString();
	}
	
	private boolean neitherNullNorEmpty(String str) {
		return (str != null && str.trim().length() > 0);
	}
}
