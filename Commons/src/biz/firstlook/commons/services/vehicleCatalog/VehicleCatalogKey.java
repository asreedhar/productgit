package biz.firstlook.commons.services.vehicleCatalog;

import java.io.Serializable;

public class VehicleCatalogKey implements Serializable {

	private static final long serialVersionUID = -3573368011826482760L;

	private Integer vehicleCatalogId;
	private String vehicleCatalogDescription;
	
	public VehicleCatalogKey(Integer id, String description) {
		this.vehicleCatalogDescription = description;
		this.vehicleCatalogId = id;
	}
	
	public Integer getVehicleCatalogId() {
		return vehicleCatalogId;
	}
	public void setVehicleCatalogId(Integer vehicleCatalogId) {
		this.vehicleCatalogId = vehicleCatalogId;
	}
	public String getVehicleCatalogDescription() {
		return vehicleCatalogDescription;
	}
	public void setVehicleCatalogDescription(String vehicleCatalogDescription) {
		this.vehicleCatalogDescription = vehicleCatalogDescription;
	}
	
	
}
