package biz.firstlook.commons.services.vehicleCatalog;

import java.io.Serializable;

abstract class VehicleCatalogBaseEntry implements Serializable
{

private static final long serialVersionUID = -2492336506142523559L;

private String vin;
private Integer modelYear;
private String make;
private String line;
private String model;
private String bodyType;
private Integer bodyTypeId;
private Integer makeModelGroupingId;
private Integer modelId;
private Integer groupingDescriptionId; 
private String groupingDescription;
private Integer doors;
private String fuelType;
private String engine;
private String driveTrain;
private String transmission;
private Integer cylinderCount;
private Integer segmentId;
private String segment;

public String getBodyType()
{
	return bodyType;
}
public void setBodyType( String bodyType )
{
	this.bodyType = bodyType;
}
public Integer getBodyTypeId()
{
	return bodyTypeId;
}
public void setBodyTypeId( Integer bodyTypeId )
{
	this.bodyTypeId = bodyTypeId;
}

public Integer getCylinderCount()
{
	return cylinderCount;
}
public void setCylinderCount( Integer cylinderCount )
{
	this.cylinderCount = cylinderCount;
}
public String getSegment()
{
	return segment;
}
public void setSegment( String segment )
{
	this.segment = segment;
}
public Integer getDoors()
{
	return doors;
}
public void setDoors( Integer doors )
{
	this.doors = doors;
}
public String getDriveTrain()
{
	return driveTrain;
}
public void setDriveTrain( String driveTrain )
{
	this.driveTrain = driveTrain;
}
public String getFuelType()
{
	return fuelType;
}
public void setFuelType( String fuelType )
{
	this.fuelType = fuelType;
}
public String getGroupingDescription()
{
	return groupingDescription;
}
public void setGroupingDescription( String groupingDescription )
{
	this.groupingDescription = groupingDescription;
}
public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}
public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}
public String getLine()
{
	return line;
}
public void setLine( String line )
{
	this.line = line;
}
public String getMake()
{
	return make;
}
public void setMake( String make )
{
	this.make = make;
}
public Integer getMakeModelGroupingId()
{
	return makeModelGroupingId;
}
public void setMakeModelGroupingId( Integer makeModelGroupingId )
{
	this.makeModelGroupingId = makeModelGroupingId;
}
public String getModel()
{
	return model;
}
public void setModel( String model )
{
	this.model = model;
}
public Integer getModelYear()
{
	return modelYear;
}
public void setModelYear( Integer modelYear )
{
	this.modelYear = modelYear;
}
public String getTransmission()
{
	return transmission;
}
public void setTransmission( String transmission )
{
	this.transmission = transmission;
}

public String getVin()
{
	return vin;
}
public void setVin( String vin )
{
	this.vin = vin;
}
public Integer getSegmentId()
{
	return segmentId;
}
public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}
public String getEngine()
{
	return engine;
}
public void setEngine( String engine )
{
	this.engine = engine;
}
public Integer getModelId()
{
	return modelId;
}
public void setModelId( Integer modelId )
{
	this.modelId = modelId;
}

}
