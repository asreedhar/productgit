package biz.firstlook.commons.services.vehicleCatalog;

import biz.firstlook.commons.services.ServiceException;


public class VehicleCatalogServiceException extends ServiceException {

	private static final long serialVersionUID = -4570893346519632640L;

	public VehicleCatalogServiceException() {
		super();
	}

	public VehicleCatalogServiceException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public VehicleCatalogServiceException(String msg) {
		super(msg);
	}

	public VehicleCatalogServiceException(Throwable cause) {
		super(cause);
	}
	
}
