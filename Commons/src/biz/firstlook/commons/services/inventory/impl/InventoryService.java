package biz.firstlook.commons.services.inventory.impl;

import java.util.Date;

import biz.firstlook.commons.dao.IInventoryNextPlanDateDAO;
import biz.firstlook.commons.dao.IInventoryRepriceDAO;
import biz.firstlook.commons.enumeration.RepriceSouce;
import biz.firstlook.commons.services.inventory.IInventoryService;
import biz.firstlook.services.reprice.distribution.DistributorClientAdapter;

public class InventoryService implements IInventoryService
{

private IInventoryRepriceDAO inventoryRepriceDAO;
private IInventoryNextPlanDateDAO inventoryNextPlanDateDAO;

public void repriceInventory( Integer inventoryId, Date beginDate, Date endDate, Integer originalPrice, Integer newPrice,
								Boolean repriceConfirmed, String createdBy, Date lastModified, Integer businessUnitId,RepriceSouce repriceSource )
{

	Double origPrice = null;
	
	if (originalPrice != null){
		origPrice = originalPrice.doubleValue();
	}
	
	inventoryRepriceDAO.insertRepriceEvent( businessUnitId, inventoryId, newPrice, origPrice,
											beginDate, endDate, createdBy, lastModified, 
											repriceConfirmed, repriceSource );
	try{
	DistributorClientAdapter.sendRepriceEventToWebService(businessUnitId, inventoryId, newPrice, createdBy);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}

public Date getNextPlanDate( Integer inventoryId, Integer ageInflation ) {
	return inventoryNextPlanDateDAO.getNextPlanDate(inventoryId, ageInflation);
}




public void setInventoryRepriceDAO( IInventoryRepriceDAO inventoryRepriceDAO )
{
	this.inventoryRepriceDAO = inventoryRepriceDAO;
}

public void setInventoryNextPlanDateDAO(
		IInventoryNextPlanDateDAO inventoryNextPlanDateDAO) {
	this.inventoryNextPlanDateDAO = inventoryNextPlanDateDAO;
}



}