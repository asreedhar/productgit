package biz.firstlook.commons.services.inventory;

import java.util.Date;

import biz.firstlook.commons.enumeration.RepriceSouce;

public interface IInventoryService
{

public void repriceInventory( Integer inventoryId, Date beginDate, Date endDate, Integer originalPrice, Integer newPrice, Boolean repriceConfirmed,
							  String createdBy, Date lastModified, Integer businessUnitId, RepriceSouce repriceSource );

public Date getNextPlanDate( Integer inventoryId, Integer ageInflation);

}
