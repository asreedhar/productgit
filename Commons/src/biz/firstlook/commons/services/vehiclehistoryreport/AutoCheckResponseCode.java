package biz.firstlook.commons.services.vehiclehistoryreport;

public enum AutoCheckResponseCode {
	UNKNOWN(0, "First Look encountered an unexpected error.  Please contact the helpdesk."),
	FORBIDDEN(403, "Your AutoCheck request has failed due to an invalid USERNAME.  Please contact AutoCheck to fix this problem."),
	SERVER_ERROR(500, "Your AutoCheck request has failed due to an internal error code not being supplied.  Please contact Firstlook if this problem perists.");
	private final int code;
	private final String message;
	AutoCheckResponseCode(int code, String message) {
		this.code = code;
		this.message = message;
	}
	public int getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	public static AutoCheckResponseCode fromValue(int v) {
        for (AutoCheckResponseCode c: AutoCheckResponseCode.values()) {
            if (c.code == v) {
                return c;
            }
        }
        throw new IllegalArgumentException(Integer.toString(v));
    }
}
