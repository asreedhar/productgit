package biz.firstlook.commons.services.vehiclehistoryreport;

public enum CarfaxResponseCode {
	UNKNOWN(0, "First Look encountered an unexpected error.  Please contact the helpdesk."),
	NO_DATA_AVAILABLE(600, "CARFAX does not have any records for this vehicle."),
	INVALID_VIN(800, "Your CARFAX request has failed due to an invalid vin."),
	SERVICE_UNAVAILABLE(900, "The CARFAX Vehicle History Service is currently not available, please try again later."),
	TRANSACTION_ERROR(901, "Your CARFAX request has failed due to a transmission error. Please try again later."),
	ACCOUNT_STATUS(903, "Your CARFAX request has failed due to an invalid account.  Please contact CARFAX to fix this problem."),
	SECURITY_VIOLATION_USERNAME(905, "Your CARFAX request has failed due to an invalid USER NAME.  Please contact CARFAX to fix this problem."),
	SECURITY_VIOLATION_PASSWORD(904, "Your CARFAX request has failed due to an invalid PASSWORD.  Please contact CARFAX to fix this problem."),
	NO_CODE_SUPPLIED(906, "Your CARFAX request has failed due to an internal error code not being supplied.  Please contact Firstlook if this problem perists.");
	private final int code;
	private final String message;
	CarfaxResponseCode(int code, String message) {
		this.code = code;
		this.message = message;
	}
	public int getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	public static CarfaxResponseCode fromValue(int v) {
        for (CarfaxResponseCode c: CarfaxResponseCode.values()) {
            if (c.code == v) {
                return c;
            }
        }
        throw new IllegalArgumentException(Integer.toString(v));
    }
}
