package biz.firstlook.commons.services.vehiclehistoryreport;

public class AutoCheckException extends RuntimeException {
	
	private static final long serialVersionUID = 1243281444942971465L;
	
	AutoCheckResponseCode responseCode;

	public AutoCheckException(AutoCheckResponseCode responseCode) {
		super(responseCode.getMessage());
		this.responseCode = responseCode;
	}

	public AutoCheckResponseCode getResponseCode() {
		return responseCode;
	}
}
