package biz.firstlook.commons.services.vehiclehistoryreport;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.fault.XFireFault;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.services.vehiclehistoryreport.autocheck.ArrayOfAutoCheckReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportTO;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckWebServiceClient;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckWebServiceSoap;
import biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity;
import biz.firstlook.services.vehiclehistoryreport.carfax.ArrayOfCarfaxReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;

public class AutoCheckService {

private static final String END_POINT_PROPERTY = "autocheck.webservice.endpoint";
	
	private static final String END_POINT_PROPERTY_URI;
	
	static {
		END_POINT_PROPERTY_URI = PropertyLoader.getProperty(END_POINT_PROPERTY);
	}
	
	private static final AutoCheckService instance = new AutoCheckService();
	
	public static AutoCheckService getInstance() {
		return instance;
	}
	
	private AutoCheckWebServiceSoap service;
	
	private AutoCheckService() {
		service = createService();
	}
	
	private AutoCheckWebServiceSoap createService() {
		return new AutoCheckWebServiceClient().getAutoCheckWebServiceSoap(END_POINT_PROPERTY_URI);
	}
	
	private UserIdentity createUserIdentity(String userName) {
		UserIdentity userIdentity = new UserIdentity();
		userIdentity.setUserName(userName);
		return userIdentity;
	}

	private AutoCheckWebServiceSoap getService() {
		return service;
	}
	
	private AutoCheckException convertServiceException(XFireRuntimeException xfr) {
		AutoCheckException handle = new AutoCheckException(AutoCheckResponseCode.UNKNOWN);
		Throwable xfc = xfr.getCause();
		if (xfc != null && xfc instanceof XFireFault) {
			XFireFault xff = (XFireFault) xfc;
			if (xff.getDetail() != null) {
				org.jdom.Element el = xff.getDetail();
				org.jdom.Element rc = el.getChild("ResponseCode", el.getNamespace("http://services.firstlook.biz/VehicleHistoryReport/Carfax/"));
				if (rc != null) {
					String responseCodeText = rc.getTextTrim();
					if (!StringUtils.isBlank(responseCodeText)) {
						AutoCheckResponseCode responseCode = AutoCheckResponseCode.fromValue(
								Integer.parseInt(responseCodeText));
						handle = new AutoCheckException(responseCode);
					}
				}
			}
			handle.initCause(xff);
		}
		return handle;
	}
	
	public boolean hasAccount(int dealerId, String userName) {
		try {
			return getService().hasAccount(dealerId, createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public Map<String,String> getAutoCheckReportProperties(int dealerId, String userName) {
		try {
			UserIdentity userIdentity = createUserIdentity(userName);
			AutoCheckWebServiceSoap service = getService();
			Map<String,String> map = new HashMap<String,String>();
			map.put("viewAutocheckReportOnly", Boolean.toString(!service.canPurchaseReport(dealerId, userIdentity)));
			map.put("hasAutocheck", Boolean.toString(service.hasAccount(dealerId, userIdentity)));
			return map;
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public AutoCheckReportTO getAutoCheckReport(int dealerId, String userName, String vin) {
		try {
			return getService().getReport(
					dealerId,
					vin,
					createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public Map<String,String> getAutoCheckReportProperties(int dealerId, String userName, String vin) {
		try {
			UserIdentity userIdentity = createUserIdentity(userName);
			AutoCheckWebServiceSoap service = getService();
			Map<String,String> map = new HashMap<String,String>();
			map.put("viewAutocheckReportOnly", Boolean.toString(!service.canPurchaseReport(dealerId, userIdentity)));
			if (service.hasAccount(dealerId, userIdentity)) {
				map.put("hasAutocheck", Boolean.TRUE.toString());
				AutoCheckReportTO report = service.getReport(dealerId, vin, userIdentity);
				if (report != null) {
					GregorianCalendar expirationDate = report.getExpirationDate().toGregorianCalendar();
					GregorianCalendar now = new GregorianCalendar();
					if (expirationDate.compareTo(now) >= 0) {
						map.put("autoCheckReportAvailable", Boolean.TRUE.toString());
						map.put("autoCheckReportOK", Boolean.toString(isOkay(report)));
						map.put("autoCheckReportUrl", reportUrl(dealerId, vin));
						map.put("autoCheckReportScore", Integer.toString(report.getScore()));
						map.put("autoCheckReportScoreCompareLow", Integer.toString(report.getCompareScoreRangeLow()));
						map.put("autoCheckReportScoreCompareHigh", Integer.toString(report.getCompareScoreRangeHigh()));
					}
					else {
						map.put("autoCheckReportAvailable", Boolean.FALSE.toString());
					}
				}
				else {
					map.put("autoCheckReportAvailable", Boolean.FALSE.toString());
				}
			}
			else {
				map.put("hasAutocheck", Boolean.FALSE.toString());
			}
			return map;
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}

	public List<AutoCheckReportInspectionTO> getAutoCheckReportInspectionTO(AutoCheckReportTO report){

		if (report == null){
			return Collections.emptyList();
		}
		
		ArrayOfAutoCheckReportInspectionTO inspections = report.getInspections();
		
		if (inspections == null){
			return Collections.emptyList();
		}
		
		return inspections.getAutoCheckReportInspectionTO();
	}
	
	public boolean isOkay(AutoCheckReportTO report) {
		boolean okay = true;
		for (AutoCheckReportInspectionTO inspection : report.getInspections().getAutoCheckReportInspectionTO()) {
			if (inspection.getId() == 1) {
				continue; // skip 1-owner
			}
			okay &= inspection.isSelected();
		}
		return okay;
	}
	
	public String reportUrl(int dealerId, String vin) {
		StringBuffer sb = new StringBuffer("/support/Reports/AutoCheckReport.aspx?DealerId=");
		sb.append(dealerId);
		sb.append("&Vin=");
		sb.append(vin);
		return sb.toString();
	}
}
