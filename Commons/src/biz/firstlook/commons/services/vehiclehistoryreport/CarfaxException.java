package biz.firstlook.commons.services.vehiclehistoryreport;

public class CarfaxException extends RuntimeException {

	private static final long serialVersionUID = -6319101418655475269L;
	
	CarfaxResponseCode responseCode;

	public CarfaxException(CarfaxResponseCode responseCode) {
		super(responseCode.getMessage());
		this.responseCode = responseCode;
	}

	public CarfaxResponseCode getResponseCode() {
		return responseCode;
	}
	
}
