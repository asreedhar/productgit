package biz.firstlook.commons.services.vehiclehistoryreport;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.fault.XFireFault;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.services.vehiclehistoryreport.carfax.ArrayOfCarfaxReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.ArrayOfCarfaxReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportPreferenceTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportType;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxTrackingCode;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxWebServiceClient;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxWebServiceSoap;
import biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;

public class CarfaxService {
	
	private static final String END_POINT_PROPERTY = "carfax.webservice.endpoint";
	
	private static final String END_POINT_PROPERTY_URI;
	
	static {
		END_POINT_PROPERTY_URI = PropertyLoader.getProperty(END_POINT_PROPERTY);
	}
	
	private static final CarfaxService instance = new CarfaxService();
	
	public static CarfaxService getInstance() {
		return instance;
	}
	
	private CarfaxWebServiceSoap service;
	
	private CarfaxService() {
		service = createService();
	}
	
	private CarfaxWebServiceSoap createService() {
		return new CarfaxWebServiceClient().getCarfaxWebServiceSoap(END_POINT_PROPERTY_URI);
	}
	
	private UserIdentity createUserIdentity(String userName) {
		UserIdentity userIdentity = new UserIdentity();
		userIdentity.setUserName(userName);
		return userIdentity;
	}

	private CarfaxWebServiceSoap getService() {
		return service;
	}
	
	private CarfaxException convertServiceException(XFireRuntimeException xfr) {
		CarfaxException handle = new CarfaxException(CarfaxResponseCode.UNKNOWN);
		Throwable xfc = xfr.getCause();
		if (xfc != null && xfc instanceof XFireFault) {
			XFireFault xff = (XFireFault) xfc;
			if (xff.getDetail() != null) {
				org.jdom.Element el = xff.getDetail();
				org.jdom.Element rc = el.getChild("ResponseCode", el.getNamespace("http://services.firstlook.biz/VehicleHistoryReport/Carfax/"));
				if (rc != null) {
					String responseCodeText = rc.getTextTrim();
					if (!StringUtils.isBlank(responseCodeText)) {
						CarfaxResponseCode responseCode = CarfaxResponseCode.fromValue(
								Integer.parseInt(responseCodeText));
						handle = new CarfaxException(responseCode);
					}
				}
				else if (xff.getFaultCode() == XFireFault.RECEIVER) {
					handle = new CarfaxException(CarfaxResponseCode.UNKNOWN);					
				}
			}
			handle.initCause(xff);
		}
		return handle;
	}

	public boolean hasAccount(int dealerId, String userName) {
		try {
			return getService().hasAccount(dealerId, createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public boolean canPurchaseReport(int dealerId, String userName) {
		try {
			return getService().canPurchaseReport(dealerId, createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public void purchaseCarfaxReports(
			int dealerId,
			String userName,
			VehicleEntityType vehicleEntityType,
			CarfaxReportType reportType,
			boolean displayInHotList) {
		try {
			getService().purchaseReports(
					dealerId,
					vehicleEntityType,
					reportType,
					displayInHotList,
					CarfaxTrackingCode.FLN,
					createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public CarfaxReportTO purchaseCarfaxReport(
			int dealerId,
			String userName,
			String vin,
			CarfaxReportType reportType,
			boolean displayInHotList) {
		try {
			return getService().purchaseReport(
					dealerId,
					vin,
					reportType,
					displayInHotList,
					CarfaxTrackingCode.FLN,
					createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}

	public CarfaxReportTO getCarfaxReport(int dealerId, String userName, String vin) {
		try {
			return getService().getReport(
					dealerId,
					vin,
					createUserIdentity(userName));
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public List<CarfaxReportInspectionTO> getCarfaxReportInspections(CarfaxReportTO report){

		if (report == null){
			return Collections.emptyList();
		}
		
		ArrayOfCarfaxReportInspectionTO inspections = report.getInspections();
		
		if (inspections == null){
			return Collections.emptyList();
		}
		
		return inspections.getCarfaxReportInspectionTO();
	}
	
	public List<CarfaxReportTO> getCarfaxReports(int dealerId, String userName, VehicleEntityType vehicleEntityType) {
		try {
			ArrayOfCarfaxReportTO reports = getService().getReports(
					dealerId,
					vehicleEntityType,
					createUserIdentity(userName));
			if (reports == null) {
				return Collections.emptyList();
			}
			return reports.getCarfaxReportTO();
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public CarfaxReportPreferenceTO getCarfaxReportPreferences(int dealerId, String userName, VehicleEntityType vehicleEntityType) {
		try {
			
			CarfaxReportPreferenceTO carfaxReportPreferenceTO = getService().getReportPreference(
					dealerId,
					vehicleEntityType,
					createUserIdentity(userName));
			
			return carfaxReportPreferenceTO;
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public Map<String,String> getCarfaxReportProperties(int dealerId, String userName) {
		try {
			UserIdentity userIdentity = createUserIdentity(userName);
			CarfaxWebServiceSoap service = getService();
			Map<String,String> map = new HashMap<String,String>();
			map.put("viewCarfaxReportOnly", Boolean.toString(!service.canPurchaseReport(dealerId, userIdentity)));
			map.put("hasCarfax", Boolean.toString(service.hasAccount(dealerId, userIdentity)));
			return map;
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}

	public Map<String,String> getCarfaxReportProperties(
			int dealerId,
			String userName,
			String vin,
			VehicleEntityType vehicleEntityType) {
		try {
			UserIdentity userIdentity = createUserIdentity(userName);
			CarfaxWebServiceSoap service = getService();
			Map<String,String> map = new HashMap<String,String>();
			// dealer preferences
			CarfaxReportPreferenceTO preference = service.getReportPreference(dealerId, vehicleEntityType, userIdentity);
			if (preference != null) {
				map.put("autoRun", Boolean.toString(preference.isPurchaseReport()));
				map.put("CCCFlag", Boolean.toString(preference.isDisplayInHotListings()));
				map.put("reportType", preference.getReportType().value());
			}
			else {
				map.put("autoRun", Boolean.FALSE.toString());
				map.put("CCCFlag", Boolean.FALSE.toString());
				map.put("reportType", CarfaxReportType.BTC.value());
			}
			
			// member preferences
			map.put("viewCarfaxReportOnly", Boolean.toString(!service.canPurchaseReport(dealerId, userIdentity)));
			// report preferences
			if (service.hasAccount(dealerId, userIdentity)) {
				map.put("hasCarfax", Boolean.TRUE.toString());
				CarfaxReportTO report = service.getReport(dealerId, vin, userIdentity);
				if (report != null) {
					GregorianCalendar expirationDate = report.getExpirationDate().toGregorianCalendar();
					GregorianCalendar now = new GregorianCalendar();
					if (expirationDate.compareTo(now) >= 0) {
						map.put("reportAvailable", Boolean.TRUE.toString());
						map.put("reportType", report.getReportType().value());
						map.put("reportUrl", reportUrl(report));
						map.put("carfaxReportOK", Boolean.toString(isOkay(report)));
						map.put("hasOneOwner", Boolean.toString(report.getOwnerCount() == 1));
						map.put("CCCFlag", Boolean.toString(report.isDisplayInHotList()));
					}
					else {
						map.put("reportAvailable", Boolean.FALSE.toString());
					}
					map.put("carfaxReportExpiration", expirationDate.getTime().toGMTString());
				}
				else {
					map.put("reportAvailable", Boolean.FALSE.toString());
				}
			}
			else {
				map.put("hasCarfax", Boolean.FALSE.toString());
			}
			return map;
		}
		catch (XFireRuntimeException xfr) {
			throw convertServiceException(xfr);
		}
	}
	
	public boolean isOkay(CarfaxReportTO report) {
		boolean okay = true;
		for (CarfaxReportInspectionTO inspection : report.getInspections().getCarfaxReportInspectionTO()) {
			if (inspection.getId() == 1) {
				continue; // skip 1-owner
			}
			okay &= inspection.isSelected();
		}
		return okay;
	}
	
	public String reportUrl(CarfaxReportTO report) {
		StringBuffer sb = new StringBuffer("http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID=");
		sb.append(report.getUserName());
		sb.append("&vin=");
		sb.append(report.getVin());
		return sb.toString();
	}
}
