package biz.firstlook.commons.services.insights;

import biz.firstlook.commons.services.ServiceException;

@SuppressWarnings("serial")
public class InsightServiceException extends ServiceException {


	private int businessUnitId;
	private String trim;
	private int groupingDescriptionId;
	private int year;
	
	@Override
	public String getMessage() {
		return "Error generating Insights for businessUnitId: " + businessUnitId + ", GroupingDescription:" + groupingDescriptionId + ", Year:" + year +", trim:" + trim + "\n" + super.getMessage();
	}
	
	@Override
	public String getLocalizedMessage() {
		return "Error generating Insights for businessUnitId: " + businessUnitId + ", GroupingDescription:" + groupingDescriptionId + ", Year:" + year +", trim:" + trim + "\n" + super.getLocalizedMessage();
	}

	public InsightServiceException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public InsightServiceException(String msg) {
		super(msg);
	}

	public InsightServiceException(Throwable cause, int businessUnitId, int groupingDescriptionId, int year, String trim) {
		super(cause);
		this.businessUnitId = businessUnitId;
		this.trim = trim;
		this.year = year;
		this.groupingDescriptionId = groupingDescriptionId;
	}

}
