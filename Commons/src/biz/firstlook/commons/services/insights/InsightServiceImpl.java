package biz.firstlook.commons.services.insights;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import biz.firstlook.commons.sql.DataSourceUtils;
import biz.firstlook.commons.sql.PreparedStatementCreator;
import biz.firstlook.commons.sql.RowMapper;

public class InsightServiceImpl implements InsightService {

	public List<Insight> getInsights(final InsightParameters parameters) throws InsightServiceException {
		
		final Integer businessUnitId = parameters.getBusinessUnitID();
		final Integer year = parameters.getVehicleYear();
		final Integer groupingDescriptionId =  parameters.getGroupingDescriptionID();
		final String trim = parameters.getTrim();
		
		try {
			return DataSourceUtils.executeQuery(
					DataSourceUtils.getDataSource("jdbc/FLDW"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							Logger.getLogger(InsightServiceImpl.class).debug("GetInsights(" + parameters + ")");
							CallableStatement stmt = conn.prepareCall("{call GetInsights(?,?,?,?,?,?,?)}");
							stmt.setInt(1, businessUnitId);
							stmt.setInt(2, groupingDescriptionId);
							if (trim == null || trim.trim().length() == 0) {
								stmt.setNull(3, java.sql.Types.VARCHAR);
							}
							else {
								stmt.setString(3, trim);
							}
							if (year == null) {
								stmt.setNull(4, java.sql.Types.INTEGER);
							}
							else {
								stmt.setInt(4, year);
							}
							if (parameters.getVehicleMileage() == null) {
								stmt.setNull(5, java.sql.Types.INTEGER);
							}
							else {
								stmt.setInt(5, parameters.getVehicleMileage());
							}
							stmt.setInt(6, parameters.getInventoryTypeID());
							stmt.setInt(7, parameters.getWeeks());
							return stmt;
						}
					},
					new RowMapper<Insight>() {
						public Insight mapRow(ResultSet rs, int rowNum) throws SQLException {
							InsightTypeEnum insightType = InsightTypeEnum.decode(rs.getInt("InsightTypeID"));
							InsightAdjectivalEnum insightAdjectival = null;
							int insightAdjectivalID = rs.getInt("InsightAdjectivalID");
							if (!rs.wasNull()) {
								insightAdjectival = InsightAdjectivalEnum.decode(insightAdjectivalID);
							}
							Integer insightValue = rs.getInt("InsightValue");
							if (rs.wasNull()) {
								insightValue = null;
							}
							return new Insight(parameters, insightType, insightAdjectival, insightValue);
						}
					});
		}
		catch (NamingException ne) {
			throw new InsightServiceException(ne, businessUnitId, groupingDescriptionId, year, trim );
		}
		catch (SQLException se) {
			throw new InsightServiceException(se, businessUnitId, groupingDescriptionId, year, trim );
		}
	}

}
