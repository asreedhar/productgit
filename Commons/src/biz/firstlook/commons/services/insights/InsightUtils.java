package biz.firstlook.commons.services.insights;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class InsightUtils {

	protected static Logger logger = Logger.getLogger(InsightUtils.class);	

	public static final List<Insight> getInsights(InsightParameters parameters) {
		List<Insight> insights = Collections.emptyList();
		try {
			insights = InsightServiceFactory.getFactory().getInsightService().getInsights(parameters);
		}
		catch (InsightServiceException ise) {
			logger.warn(ise);
		}
		return insights;
	}
	
	public static final Map<InsightTypeEnum,Insight> mapOnInsightType(List<Insight> insights) {
		Map<InsightTypeEnum,Insight> insightMap = new HashMap<InsightTypeEnum,Insight>();
		for (Insight insight : insights) {
			insightMap.put(insight.getInsightType(), insight);
		}
		return insightMap;
	}
	
	public static final boolean containsTrafficLight(List<Insight> insights) {
		return mapOnInsightType(insights).containsKey(InsightTypeEnum.TRAFFIC_LIGHT);
	}

	public static final Insight getTrafficLight(List<Insight> insights) {
		return mapOnInsightType(insights).get(InsightTypeEnum.TRAFFIC_LIGHT);
	}

	public static final boolean containsAnnualROI(List<Insight> insights) {
		return mapOnInsightType(insights).containsKey(InsightTypeEnum.ANNUAL_ROI);
	}

	public static final Insight getAnnualROI(List<Insight> insights) {
		return mapOnInsightType(insights).get(InsightTypeEnum.ANNUAL_ROI);
	}
	
	public static final Integer getInsightValue(Insight insight, Integer defaultValue) {
		if (insight != null) {
			if (insight.getInsightValue() != null) {
				return insight.getInsightValue();
			}
		}
		return defaultValue;
	}
}
