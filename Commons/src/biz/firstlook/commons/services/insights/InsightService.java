package biz.firstlook.commons.services.insights;

import java.util.List;

public interface InsightService {

	public List<Insight> getInsights(InsightParameters parameters) throws InsightServiceException;
	
}
