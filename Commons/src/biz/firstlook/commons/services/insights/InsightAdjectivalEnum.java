package biz.firstlook.commons.services.insights;

public enum InsightAdjectivalEnum {
	
	NONE ( 1, "None" ),
	INSUFFICIENT ( 2, "Insufficient" ),
	FASTER ( 3, "Faster" ),
	SLOWER ( 4, "Slower" ),
	MORE ( 5, "More" ),
	LESS ( 6, "Less" ),
	HIGHER ( 7, "Higher" ),
	LOWER ( 8, "Lower" ),
	UNDER ( 9, "Under" ),
	OVER ( 10, "Over" ),
	HIGH ( 11, "High" ),
	OLDER ( 12, "Older" );
	
	private int id;
	private String description;
	
	InsightAdjectivalEnum(int id, String description) {
		this.id = id;
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
	public int getId() {
		return id;
	}
	
	public static final InsightAdjectivalEnum decode(int id) {
		switch (id) {
		case 1:
			return NONE;
		case 2:
			return INSUFFICIENT;
		case 3:
			return FASTER;
		case 4:
			return SLOWER;
		case 5:
			return MORE;
		case 6:
			return LESS;
		case 7:
			return HIGHER;
		case 8:
			return LOWER;
		case 9:
			return UNDER;
		case 10:
			return OVER;
		case 11:
			return HIGH;
		case 12:
			return OLDER;
		default:
			throw new IllegalArgumentException("Not a recognized InsightAdjectivalEnum ID: " + id);
		}
	}
}
