package biz.firstlook.commons.services.insights;

import java.io.Serializable;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.commons.util.PropertyLoader;

@SuppressWarnings("serial")
public class Insight implements Serializable {

	private InsightParameters parameters;
	private InsightTypeEnum insightType;
	private InsightAdjectivalEnum insightAdjectival;
	private Integer insightValue;
	
	public Insight(InsightParameters parameters, InsightTypeEnum insightType, InsightAdjectivalEnum insightAdjectival, Integer insightValue) {
		super();
		this.parameters = parameters;
		this.insightType = insightType;
		this.insightAdjectival = insightAdjectival;
		this.insightValue = insightValue;
	}
	
	public InsightAdjectivalEnum getInsightAdjectival() {
		return insightAdjectival;
	}
	
	public InsightTypeEnum getInsightType() {
		return insightType;
	}
	
	public Integer getInsightValue() {
		return insightValue;
	}
	
	public String toString() {
		String arg0 = "";
		String arg1 = "";
		String arg2 = "";
		String id = Functions.nullSafeToString(getInsightType().getId());
		String key = "";
		switch (getInsightType()) {
			case INSUFFICIENT_OR_NO_SALES_HISTORY:
				key = "insight.adj.insufficient";
				if (getInsightAdjectival() != InsightAdjectivalEnum.INSUFFICIENT) {
					key += ".opp";
				}
				arg0 = PropertyLoader.getProperty(key);
				arg1 = Functions.nullSafeToString(getInsightValue());
				break;
			case AVERAGE_GROSS_PROFIT:
				arg0 = Functions.nullSafeToDollarString(Functions.nullSafeAbs(getInsightValue()));
				key = "insight.adj.more";
				if (getInsightAdjectival() != InsightAdjectivalEnum.MORE) {
					key += ".opp";
				}
				arg1 = PropertyLoader.getProperty(key);
				break;
			case FREQUENT_NO_SALES:
				arg0 = Functions.nullSafeToString(getInsightValue());
				break;
			case AVERAGE_MARGIN:
				arg0 = Functions.nullSafeToString(getInsightValue());
				key = "insight.adj.higher";
				if (getInsightAdjectival() != InsightAdjectivalEnum.HIGHER) {
					key += ".opp";
				}
				arg1 = PropertyLoader.getProperty(key);
				break;
			case CONTRIBUTION_RANK:
				arg0 = Functions.nullSafeToString(getInsightValue());
				break;
			case AVERAGE_DAYS_TO_SALE:
				arg0 = Functions.nullSafeToString(getInsightValue());
				key = "l.days";
				if (getInsightValue() == 1) {
					key += ".singular";
				}
				arg1 = PropertyLoader.getProperty(key);
				key = "insight.adj.faster";
				if (getInsightAdjectival() != InsightAdjectivalEnum.FASTER) {
					key += ".opp";
				}
				arg2 = PropertyLoader.getProperty(key);						
				break;
			case STOCKING_LEVELS:
				id += "." + getInsightAdjectival().getDescription().toLowerCase();
				break;
			case ANNUAL_ROI:
				arg0 = Functions.nullSafeToString(getInsightValue());
				break;
			case OLDER_VEHICLE:
				arg0 = Functions.nullSafeToString(getInsightValue());
				break;
			case HIGH_MILEAGE:
				arg0 = Functions.nullSafeToString(getInsightValue());
				break;
			case MAKE_MODEL_MARKET_SHARE_PERCENTAGE:
			case MODEL_YEAR_MARKET_SHARE_PERCENTAGE:
				final int nullSafeIntValue = Functions.nullSafeIntValue(getInsightValue());
				if (nullSafeIntValue == 0) {
					arg0 = "less than 1";
				}
				else {
					arg0 = Integer.toString(nullSafeIntValue);
				}
				break;
			case MAKE_MODEL_MARKET_SHARE_RANK:
				arg0 = Integer.toString(Functions.nullSafeIntValue(getInsightValue()));
				arg1 = parameters.getSegment();
				break;
			case MODEL_YEAR_MARKET_SHARE_RANK:
				arg0 = Functions.nullSafeToString(parameters.getVehicleYear());
				arg1 = Functions.nullSafeToString(getInsightValue());
				break;
			default:
				return "";
		}
		// escape any $ values or replace all regex is screwed up
		arg0 = escapeDollarSign(arg0);
		arg1 = escapeDollarSign(arg1);
		arg2 = escapeDollarSign(arg2);
		
		String result = PropertyLoader.getProperty("insight.id" + id);
		result = result.replaceAll("\\{0\\}", arg0);
		result = result.replaceAll("\\{1\\}", arg1);
		result = result.replaceAll("\\{2\\}", arg2);
		return result;
	}

	private String escapeDollarSign(String arg) {
		if (arg != null && arg.indexOf('$') != -1 ) {
			arg = arg.replace("$", "\\$");
		}
		return arg;
	}
	
}

