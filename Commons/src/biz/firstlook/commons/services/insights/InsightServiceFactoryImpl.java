package biz.firstlook.commons.services.insights;

public class InsightServiceFactoryImpl extends InsightServiceFactory {

	private InsightService insightService;
	
	public InsightServiceFactoryImpl() {
		super();
		setFactory(this);
	}
	
	public InsightService getInsightService() {
		return insightService;
	}

	/** This is called as a result of evaulating the spring configuration. */
	public void setInsightService(InsightService insightService) {
		this.insightService = insightService;
	}

}
