package biz.firstlook.commons.services.insights;

/**
 * <p>Enumeration for the Insight report type column.</p>
 * @author swenmouth
 */
public enum InsightTypeEnum {

	INSUFFICIENT_OR_NO_SALES_HISTORY ( 1, "Insufficient or No Sales-History" ),
	AVERAGE_GROSS_PROFIT ( 2, "Average Gross Profit" ),
	FREQUENT_NO_SALES ( 3, "Frequent No Sales" ),
	AVERAGE_MARGIN ( 4, "Average Margin" ),
	CONTRIBUTION_RANK ( 5, "Contribution Rank" ),
	AVERAGE_DAYS_TO_SALE ( 6, "Average Days to Sale" ),
	TRAFFIC_LIGHT ( 7, "Traffic Light" ),
	STOCKING_LEVELS ( 8, "Stocking Levels" ),
	HIGH_MILEAGE ( 9, "High Mileage Vehicle" ),
	OLDER_VEHICLE( 10, "Older Vehicle" ),
	ANNUAL_ROI( 11, "Annual ROI" ),
	MAKE_MODEL_MARKET_SHARE_PERCENTAGE( 12, "Make/Model Market Share Percentage" ),
	MAKE_MODEL_MARKET_SHARE_RANK( 13, "Make/Model Market Share Rank" ),
	MODEL_YEAR_MARKET_SHARE_PERCENTAGE( 14, "Model Year Market Share Percentage" ),
	MODEL_YEAR_MARKET_SHARE_RANK( 15, "Model Year Market Share Rank" );
	
	private int id;
	private String description;
	
	InsightTypeEnum(int id, String description) {
		this.id = id;
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
	public int getId() {
		return id;
	}
	
	public static final InsightTypeEnum decode(int id) {
		switch (id) {
		case 1:
			return INSUFFICIENT_OR_NO_SALES_HISTORY;
		case 2:
			return AVERAGE_GROSS_PROFIT;
		case 3:
			return FREQUENT_NO_SALES;
		case 4:
			return AVERAGE_MARGIN;
		case 5:
			return CONTRIBUTION_RANK;
		case 6:
			return AVERAGE_DAYS_TO_SALE;
		case 7:
			return TRAFFIC_LIGHT;
		case 8:
			return STOCKING_LEVELS;
		case 9:
			return HIGH_MILEAGE;
		case 10:
			return OLDER_VEHICLE;
		case 11:
			return ANNUAL_ROI;
		case 12:
			return MAKE_MODEL_MARKET_SHARE_PERCENTAGE;
		case 13:
			return MAKE_MODEL_MARKET_SHARE_RANK;
		case 14:
			return MODEL_YEAR_MARKET_SHARE_PERCENTAGE;
		case 15:
			return MODEL_YEAR_MARKET_SHARE_RANK;
		default:
			throw new IllegalArgumentException("Not a recognized InsightType ID: " + id);
		}
	}
}
