package biz.firstlook.commons.services.insights;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import biz.firstlook.commons.sql.DataSourceUtils;
import biz.firstlook.commons.sql.PreparedStatementCreator;
import biz.firstlook.commons.sql.RowMapper;

public class InsightParameters {
	
	// mandatory parameters
	private int businessUnitID;
	private int groupingDescriptionID;
	private int inventoryTypeID;
	private int weeks;
	
	// optional parameters
	private String trim;
	private Integer vehicleYear;
	private Integer vehicleMileage;
	
	// full contructor
	public InsightParameters(int businessUnitID, int groupingDescriptionID, int inventoryTypeID, int weeks, String trim, Integer vehicleYear, Integer vehicleMileage) {
		this.businessUnitID = businessUnitID;
		this.groupingDescriptionID = groupingDescriptionID;
		this.inventoryTypeID = inventoryTypeID;
		this.weeks = weeks;
		this.trim = trim;
		this.vehicleYear = vehicleYear;
		this.vehicleMileage = vehicleMileage;
	}
	
	// partial contructor
	public InsightParameters(int businessUnitID, int groupingDescriptionID, int inventoryTypeID, int weeks) {
		this(businessUnitID, groupingDescriptionID, inventoryTypeID, weeks, null, null, null);
	}

	public int getBusinessUnitID() {
		return businessUnitID;
	}

	public int getGroupingDescriptionID() {
		return groupingDescriptionID;
	}

	public int getInventoryTypeID() {
		return inventoryTypeID;
	}

	public String getTrim() {
		return trim;
	}

	public Integer getVehicleMileage() {
		return vehicleMileage;
	}

	public Integer getVehicleYear() {
		return vehicleYear;
	}

	public int getWeeks() {
		return weeks;
	}
	
	public String getSegment() {
		String segment = "";
		try {
			List<String> segments = DataSourceUtils.executeQuery(
					DataSourceUtils.getDataSource("jdbc/IMT"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							PreparedStatement stmt = conn.prepareStatement("SELECT DISTINCT(DBT.Segment) Segment FROM MakeModelGrouping MMG JOIN Segment DBT ON DBT.SegmentID = MMG.SegmentID WHERE MMG.GroupingDescriptionID = ?");
							stmt.setInt(1, getGroupingDescriptionID());
							return stmt;
						}
					},
					new RowMapper<String>() {
						public String mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getString(1);
						}
					});
			if (!segments.isEmpty()) {
				segment = segments.get(0);
			}
		}
		catch (NamingException ne) {
			// ignore
		}
		catch (SQLException se) {
			// ignore
		}
		return segment;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{businessUnitID=").append(businessUnitID).append(", ");
		sb.append("groupingDescriptionID=").append(groupingDescriptionID).append(", ");
		sb.append("inventoryTypeID=").append(inventoryTypeID).append(", ");
		sb.append("weeks=").append(weeks).append(", ");
		sb.append("trim=").append(trim).append(", ");
		sb.append("vehicleYear=").append(vehicleYear).append(", ");
		sb.append("vehicleMileage=").append(vehicleMileage).append("}");
		return sb.toString();
	}
	
}
