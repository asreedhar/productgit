package biz.firstlook.commons.services.insights;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.commons.util.PropertyLoader;

public class InsightPresentationAdapter {

	private List<Insight> insights;

	public InsightPresentationAdapter(List<Insight> insights) {
		this.insights = insights;
	}
	
	public int getTrafficLight() {
		return InsightUtils.getInsightValue(InsightUtils.getTrafficLight(insights), 2);
	}
	
	public boolean isRedLight() {
		return (getTrafficLight() == 1);
	}

	public void setRedLight(boolean redLight) {
		// ignore: for bean intropectors
	}
	
	public boolean isYellowLight() {
		return (getTrafficLight() == 2);
	}

	public void setYellowLight(boolean yellowLight) {
		// ignore: for bean intropectors
	}
	
	public boolean isGreenLight() {
		return (getTrafficLight() == 3);
	}

	public void setGreenLight(boolean greenLight) {
		// ignore: for bean intropectors
	}
	
	public String getDangerMessage() {
		return PropertyLoader.getProperty( "firstlook.risklevel.red.danger.message" );
	}

	public void setDangerMessage(String dangerMessage) {
		// ignore: for bean introspectors
	}

	public String getWarningMessage() {
		return PropertyLoader.getProperty( "firstlook.risklevel.yellow.caution.message" ) ;
	}

	public void setWarningMessage(String warningMessage) {
		// ignore: for bean introspectors
	}

	public int getAnnualROI() {
		return InsightUtils.getInsightValue(InsightUtils.getAnnualROI(insights), 0);
	}

	public void setAnnualROI(int annualROI) {
		// ignore: for bean introspectors
	}

	public List<Insight> getInsights() {
		return insights;
	}

	public void setInsights(List<Insight> insights) {
		// ignore: for bean introspectors
	}
	
	public List<Insight> getVehicleInsights() {
		return getInsights(new InsightTypeEnum[] {
				InsightTypeEnum.HIGH_MILEAGE,
				InsightTypeEnum.OLDER_VEHICLE
		}, insights);
	}

	public void setVehicleInsights(List<Insight> vehicleInsights) {
		// ignore: for bean introspectors
	}

	public List<Insight> getStoreInsights() {
		return getInsights(new InsightTypeEnum[] {
				InsightTypeEnum.INSUFFICIENT_OR_NO_SALES_HISTORY,
				InsightTypeEnum.AVERAGE_GROSS_PROFIT,
				InsightTypeEnum.FREQUENT_NO_SALES,
				InsightTypeEnum.AVERAGE_MARGIN,
				InsightTypeEnum.CONTRIBUTION_RANK,
				InsightTypeEnum.AVERAGE_DAYS_TO_SALE,
				InsightTypeEnum.STOCKING_LEVELS
		}, insights);
	}
	
	public List<Insight> getFirstStoreInsightsColumn() {
		return getInsights(new InsightTypeEnum[] {
				InsightTypeEnum.INSUFFICIENT_OR_NO_SALES_HISTORY,
				InsightTypeEnum.AVERAGE_GROSS_PROFIT,
				InsightTypeEnum.AVERAGE_DAYS_TO_SALE,
				InsightTypeEnum.AVERAGE_MARGIN
		}, insights);
	}
	
	public List<Insight> getSecondStoreInsightColumn() {
		return getInsights(new InsightTypeEnum[] {
				InsightTypeEnum.CONTRIBUTION_RANK,
				InsightTypeEnum.FREQUENT_NO_SALES,
				InsightTypeEnum.STOCKING_LEVELS
		}, insights);
	}

	public void setStoreInsights(List<Insight> storeInsights) {
		// ignore: for bean introspectors
	}

	public List<Insight> getMarketInsights() {
		return getInsights(new InsightTypeEnum[] {
				InsightTypeEnum.MAKE_MODEL_MARKET_SHARE_PERCENTAGE,
				InsightTypeEnum.MAKE_MODEL_MARKET_SHARE_RANK,
				InsightTypeEnum.MODEL_YEAR_MARKET_SHARE_PERCENTAGE,
				InsightTypeEnum.MODEL_YEAR_MARKET_SHARE_RANK
		}, insights);
	}

	public void setMarketInsights(List<Insight> marketInsights) {
		// ignore: for bean introspectors
	}
	
	public boolean isMakeModelMarketInsightAvailable() {
		return (getInsight(InsightTypeEnum.MAKE_MODEL_MARKET_SHARE_RANK, insights) != null);
	}
	
	public void setMakeModelMarketInsightAvailable(boolean makeModelMarketInsightAvailable) {
		// ignore: for bean introspectors
	}
	
	public String getMakeModelMarketInsight() {
		Insight percentage = getInsight(InsightTypeEnum.MAKE_MODEL_MARKET_SHARE_PERCENTAGE, insights);
		Insight rank = getInsight(InsightTypeEnum.MAKE_MODEL_MARKET_SHARE_RANK, insights);
		StringBuffer sb = new StringBuffer();
		if (rank != null) {
			sb.append(rank.toString()).append(" (").append(percentage.toString()).append(")");
		}
		return sb.toString();
	}
	
	public void setMakeModelMarketInsight(String makeModelMarketInsight) {
		// ignore: for bean introspectors
	}

	public boolean isModelYearMarketInsightAvailable() {
		return (getInsight(InsightTypeEnum.MODEL_YEAR_MARKET_SHARE_RANK, insights) != null);
	}
	
	public void setModelYearMarketInsightAvailable(boolean modelYearMarketInsightAvailable) {
		// ignore: for bean introspectors
	}
	
	public String getModelYearMarketInsight() {
		Insight percentage = getInsight(InsightTypeEnum.MODEL_YEAR_MARKET_SHARE_PERCENTAGE, insights);
		Insight rank = getInsight(InsightTypeEnum.MODEL_YEAR_MARKET_SHARE_RANK, insights);
		StringBuffer sb = new StringBuffer();
		if (rank != null) {
			sb.append(rank.toString()).append(" (").append(percentage.toString()).append(")");
		}
		return sb.toString();
	}
	
	public void setModelYearMarketInsight(String modelYearMarketInsight) {
		// ignore: for bean introspectors
	}
		
	private Insight getInsight(InsightTypeEnum insightType, List<Insight> insights) {
		 List<Insight> tgt = getInsights(new InsightTypeEnum[] { insightType }, insights);
		 return tgt.isEmpty() ? null : tgt.get(0);
	}
	
	private List<Insight> getInsights(InsightTypeEnum[] insightTypes, List<Insight> insights) {
		List<Insight> dst = new ArrayList<Insight>();
		for (Insight insight : insights) {
			for (InsightTypeEnum insightType : insightTypes) {
				if (insight.getInsightType().equals(insightType)) {
					dst.add(insight);
					break;
				}
			}
		}
		return dst;
	}
}
