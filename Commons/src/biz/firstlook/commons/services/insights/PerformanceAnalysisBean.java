package biz.firstlook.commons.services.insights;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import biz.firstlook.commons.util.PropertyLoader;

/**
 * Adapter around a list of Insights providing the same interface as the legacy IMT code.
 * 
 * @author swenmouth
 * 
 * @deprecated since 1.0 as it used by IMT (and should not be used anywhere else)
 */
public class PerformanceAnalysisBean {
	
	List<Insight> insights;
	
	public PerformanceAnalysisBean(List<Insight> insights) {
		this.insights = insights;
	}

	public int getLight() {
		return InsightUtils.getInsightValue(InsightUtils.getTrafficLight(insights), 2);
	}
	
	public String getCautionMessage() {
		String message = "";
		if (getLight() == 2) {
			message = PropertyLoader.getProperty( "firstlook.risklevel.yellow.caution.message");
		}
		return message;
	}
	
	public String getDangerMessage() {
		String message = "";
		if (getLight() == 1) {
			message = PropertyLoader.getProperty("firstlook.risklevel.red.danger.message");
		}
		return message;
	}
	
	public List<String> getDescriptors() {
		List<String> descriptors = new ArrayList<String>(insights.size());
		for (Insight insight : insights) {
			if (INSIGHT_TYPES.contains(insight.getInsightType())) {
				descriptors.add(insight.toString());
			}
		}
		return descriptors;
	}
	
	public ColumnedFormIterator<String> getDescriptorsIterator() {
		return new ColumnedFormIterator<String>(2, getDescriptors());
	}
	
	private static final Set<InsightTypeEnum> INSIGHT_TYPES = new HashSet<InsightTypeEnum>();
	
	static {
		INSIGHT_TYPES.add(InsightTypeEnum.INSUFFICIENT_OR_NO_SALES_HISTORY);
		INSIGHT_TYPES.add(InsightTypeEnum.AVERAGE_GROSS_PROFIT);
		INSIGHT_TYPES.add(InsightTypeEnum.FREQUENT_NO_SALES);
		INSIGHT_TYPES.add(InsightTypeEnum.AVERAGE_MARGIN);
		INSIGHT_TYPES.add(InsightTypeEnum.CONTRIBUTION_RANK);
		INSIGHT_TYPES.add(InsightTypeEnum.AVERAGE_DAYS_TO_SALE);
		INSIGHT_TYPES.add(InsightTypeEnum.STOCKING_LEVELS);
		INSIGHT_TYPES.add(InsightTypeEnum.HIGH_MILEAGE);
		INSIGHT_TYPES.add(InsightTypeEnum.OLDER_VEHICLE);
	}
	
	/**
	 * Fixed column-size row-wise iterator over a dataset.  Is used in table based layouts.
	 *  
	 * @author swenmouth
	 *
	 * @param <T> the data type on which we prepare for display (by calling its toString)
	 */
	public class ColumnedFormIterator<T> implements Iterator<List<DisplayValueForm<T>>> {
		
		private int columns;
		private int pos;
		private int nec;
		private List<T> values;
		
		public ColumnedFormIterator(int columns, List<T> values) {
			this.columns = columns;
			this.values  = values;
			this.pos     = 0;
		}

		/** True if there is another row left to iterate over. */
		public boolean hasNext() {
			return (pos < values.size());
		}

		/** The next row to iterate over. */
		public List<DisplayValueForm<T>> next() {
			ArrayList<DisplayValueForm<T>> row = new ArrayList<DisplayValueForm<T>>();
			nec = 0;
			for (int i = 0; i < columns; i++) {
				if (pos < values.size()) {
					row.add(new DisplayValueForm<T>(values.get(pos)));
					pos++;
				}
				else {
					nec++;
				}
			}
			return row;
		}

		/** Unsupported Operation. */
		public void remove() {
			throw new UnsupportedOperationException("ColumnedFormIterator is read-only");
		}
		
		/** Size of the underlying dataset. Used for presentation purposes. */
		public int getSize() {
			return values.size();
		}
		
		public void setSize(int size) {
			// ignore: for bean introspectors
		}
		
		/** Number of empty columns in the current row. */
		public int getMissingColumns() {
			return nec;
		}
		
		/** Why not just test (hasNext == false)? 'Cos the engineer who wrote the IMT version was dumb. */
		public boolean isCurrentRowLastRow() {
			return (hasNext() == false);
		}
		
	}
	
	/** IMT class which allows a client to safely get the string value of an object. */
	public class DisplayValueForm<T> {
		private T t;
		public DisplayValueForm(T t) {
			this.t = t;
		}
		public String getValue() {
			return (t == null) ? "" : t.toString();
		}
		public void setValue(T t) {
			this.t = t;
		}
	}
	
}
