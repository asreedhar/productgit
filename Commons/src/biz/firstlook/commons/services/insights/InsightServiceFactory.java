package biz.firstlook.commons.services.insights;

public abstract class InsightServiceFactory {
	
	private static InsightServiceFactory factory;

	public static InsightServiceFactory getFactory() {
		return factory;
	}

	/** This is called as a result of evaulating the spring configuration. */
	public static void setFactory(InsightServiceFactory factory) {
		InsightServiceFactory.factory = factory;
	}
	
	protected InsightServiceFactory() {
		super();
	}

	public abstract InsightService getInsightService();

}
