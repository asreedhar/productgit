package biz.firstlook.commons.servlet.http;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

/**
 * A utility class used to support our reverse proxying and white labeling (virutal hosting). 
 * @author bfung
 *
 */
public class HttpServletRequestHelper {
	
	private static final String HTTPS = "https";

	private HttpServletRequestHelper() {
		//static utility class.
	}
	
	public static String getScheme(HttpServletRequest request) {
		if (request.isSecure() 
				|| HTTPS.equals(request.getHeader("X-Forwarded-Proto"))
				|| HTTPS.equals(request.getHeader("X_FORWARDED_PROTO"))) {
			return HTTPS;
		} else {
			return "http";
		}
	}
	
	public static String getHost(HttpServletRequest request) {
		String host;
		if(StringUtils.isNotBlank(request.getHeader("X-Forwarded-Host"))) {
			host = request.getHeader("X-Forwarded-Host");
		} else if(StringUtils.isNotBlank(request.getHeader("X_FORWARDED_HOST"))) {
			host = request.getHeader("X_FORWARDED_HOST");
		} else if(StringUtils.isNotBlank(request.getHeader("Host"))) {
			host = request.getHeader("Host");
		} else {
			//find the host based on request url...
			final String requestUrl = request.getRequestURL().toString();
			int begin = requestUrl.indexOf("://") + 3;
			int end = requestUrl.indexOf("/", begin);
			if(end < begin) {
				end = requestUrl.length();
			}
			host = requestUrl.substring(begin, end);
		}
		return host;
	}
	
	/**
	 * Returns a url associated with the request with:
	 * <p>
	 * protocol overrides using request headers X-Forwarded-Proto or X_FORWARDED_PROTO, precendence in order described.
	 * If the value is not set, the protocol is default to http.
	 * <p>
	 * host name overrides using X-Forwarded-Host, X_FORWARDED_HOST, and finally Host.
	 * If the value is not set, the servername will be guessed at from the request.
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getServiceUrl(HttpServletRequest request) {
		final StringBuilder url = new StringBuilder();
		final String scheme = getScheme(request);
		url.append(scheme);
		url.append("://");
		url.append(getHost(request));
		
		if(StringUtils.isNotBlank(request.getContextPath())) {
			url.append(request.getContextPath());
		}
		
		if(!url.toString().endsWith("/")) {
			url.append("/");
		}
		
		if(StringUtils.isNotBlank(request.getQueryString())) {
			StringBuilder queryParameters = new StringBuilder();
			Map<String, String[]> paramMap = request.getParameterMap();
			Set<String> keySet = paramMap.keySet(); 
			for(String key : keySet) {
				if("ticket".equals(key))
					continue; //break early
				
				String[] values = request.getParameterValues(key);
				if(values != null) {
					for(String value : values) {
						if(queryParameters.length() > 0)
							queryParameters.append("&");
						
						queryParameters.append(key).append("=").append(value);
					}
				} else {
					if(queryParameters.length() > 0)
						queryParameters.append("&");
					
					queryParameters.append(key).append("=");
				}
			}
			if(queryParameters.length() > 0) {
				url.append("?");
			}
			url.append(queryParameters.toString());
		}
		
		return url.toString();
	}
}
