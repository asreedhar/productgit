package biz.firstlook.commons.enumeration;

public enum RepriceSouce {

	IMP(1, "IMP"),
	ESTOCK(2, "eSTOCK");
	
	private Integer id;
	private String name;
	
	private RepriceSouce(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
