package biz.firstlook.commons.enumeration;

public class LightAndCIATypeDescriptor
{

public final static int POWERZONE = 1;
public final static int WINNERS = 2;
public final static int GOOD_BETS = 3;
public final static int MARKET_PERFORMER_OR_OTHER_GREEN = 4;
public final static int MARKET_PERFORMER_OR_MANAGERS_CHOICE = 5;
public final static int MANAGERS_CHOICE = 6;

public final static int RED_LIGHT = 1;
public final static int YELLOW_LIGHT = 2;
public final static int GREEN_LIGHT = 3;

public static LightAndCIATypeDescriptor RED_LIGHT_MC = new LightAndCIATypeDescriptor(
        1, RED_LIGHT, MANAGERS_CHOICE, "RED-MANAGER'S-CHOICE");
public static LightAndCIATypeDescriptor YELLOW_LIGHT_MC_MP = new LightAndCIATypeDescriptor(
        2, YELLOW_LIGHT, MARKET_PERFORMER_OR_MANAGERS_CHOICE,
        "YELLOW-MANAGER'S-CHOICE");
public static LightAndCIATypeDescriptor YELLOW_LIGHT_POWERZONE = new LightAndCIATypeDescriptor(
        3, YELLOW_LIGHT, POWERZONE, "YELLOW-POWERZONE");
public static LightAndCIATypeDescriptor GREEN_LIGHT_MP_OG = new LightAndCIATypeDescriptor(
        4, GREEN_LIGHT, MARKET_PERFORMER_OR_OTHER_GREEN,
        "GREEN-MANAGER'S-CHOICE-OTHER-GREEN");
public static LightAndCIATypeDescriptor GREEN_LIGHT_GOOD_BETS = new LightAndCIATypeDescriptor(
        5, GREEN_LIGHT, GOOD_BETS, "GREEN-GOOD-BETS");
public static LightAndCIATypeDescriptor GREEN_LIGHT_WINNER = new LightAndCIATypeDescriptor(
        6, GREEN_LIGHT, WINNERS, "GREEN-WINNER");
public static LightAndCIATypeDescriptor GREEN_LIGHT_POWERZONE = new LightAndCIATypeDescriptor(
        7, GREEN_LIGHT, POWERZONE, "GREEN-POWERZONE");

private int id;
private int ciaTypeValue;
private int lightValue;
private String description;

public LightAndCIATypeDescriptor()
{
}

public LightAndCIATypeDescriptor( int id, String description )
{
    setId(id);
    setDescription(description);
}

public LightAndCIATypeDescriptor( int id, int lightValue, int ciaType,
        String description )
{
    setId(id);
    setLightValue(lightValue);
    setCiaTypeValue(ciaType);
    setDescription(description);
}

public String getDescription()
{
    return description;
}

public int getLightValue()
{
    return lightValue;
}

public void setDescription( String string )
{
    description = string;
}

public int getCiaTypeValue()
{
    return ciaTypeValue;
}

public int getId()
{
    return id;
}

public void setId( int i )
{
    id = i;
}

public void setCiaTypeValue( int i )
{
    ciaTypeValue = i;
}

public void setLightValue( int i )
{
    lightValue = i;
}

}
