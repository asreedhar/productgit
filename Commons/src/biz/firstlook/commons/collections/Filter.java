package biz.firstlook.commons.collections;

/**
 * A boolean function which is used to determine whether an item is filtered or not.
 * 
 * @author swenmouth
 *
 * @param <I> the type of the argument for which an implementation determines acceptance 
 */
public interface Filter<I> {
	public boolean accept(I element);
}
