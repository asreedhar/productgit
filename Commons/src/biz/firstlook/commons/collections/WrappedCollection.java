package biz.firstlook.commons.collections;

import java.util.Collection;

/**
 * Indicates that the implementing class is a wrapper around a collection.
 * 
 * @author swenmouth
 *
 * @param <A> the type of the collection
 */
public interface WrappedCollection<A> {

	public Collection<A> getCollection();
	
}
