package biz.firstlook.commons.collections;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.ref.Disposable;
import biz.firstlook.commons.ref.GhostReference;

/**
 * <p>Contains an optional hard reference to a list and the file to which it is serialized. When
 * the hard reference is null the list is loaded from disk.</p>
 * <p>Before Oct 18 2007 we used a {@link GhostReference} to a <code>List</code> that after
 * garbage collection was serialized to disk. The strategy needed a latch on the write to make
 * sure the file was written to disk and as such polled and called the garbage collector to
 * force finalization. This caused GC thrashing so the ghost reference was removed.</p>
 * <p>Implements the Proxy pattern.</p>
 * 
 * @author swenmouth
 * 
 * @param <I> the type of the elements of the list
 */
public class MemorySensitiveList<I> extends ListWrapper<I> implements List<I> {
	
	private transient MemorySensitiveListWrapper<I> hard;
	
	private volatile transient MemorySensitiveListPool<I> pool;

	private volatile File file;
	
	private volatile long lastAccessed;
	
	MemorySensitiveList(List<I> list, MemorySensitiveListPool<I> pool) throws IOException {
		this(null, list, pool);
	}
	
	/**
	 * Create a new <code>MemorySensitiveList</code> wrapping the supplied list, persisting the
	 * serialized version into the supplied directory.
	 * 
	 * @param directory the directory where the serialized list is to be stored
	 * @param list the large list which will be stored on disk
	 * @throws IOException if we failed to create a temp file there
	 */
	MemorySensitiveList(File directory, List<I> list, MemorySensitiveListPool<I> pool) throws IOException {
		super(null);
		this.file = File.createTempFile("MemorySensitiveList-", ".ser", directory);
		this.file.deleteOnExit();
		this.pool = pool;
		this.lastAccessed = System.currentTimeMillis();
		init(list, false);
	}
	
	private void init(List<I> list, boolean aquire) {
		this.hard = new MemorySensitiveListWrapper<I>(this.file, list);
		if (aquire) {
			this.pool.aquire(this);
		}
	}
	
	/**
	 * Delete the backing file when the object is finalized.
	 */
	protected void finalize() throws Throwable {
		pool.release(this);
		release();
		if (file != null) {
			file.delete();
			file = null;
		}
	}
	
	/**
	 * Checks to see if the hard referent is present and if not loads it back from disk.
	 * 
	 * @return the real subject [wrapped] list
	 */
	public List<I> getList() {
		lastAccessed = System.currentTimeMillis();
		List<I> obj = hard;
		if (obj == null) {
			obj = MemorySensitiveList.load(file);
			init(obj, true);
		}
		return obj;
	}
	
	/**
	 * The pool is telling us to let go of our hard reference.
	 */
	void release() {
		try {
			if (hard != null)
				hard.dispose();
		}
		catch (Throwable t) {
			Logger.getLogger(MemorySensitiveList.class).error("Error disposing MemorySensitiveList", t);
		}
		hard = null;
	}
	
	/**
	 * Property used by the pool comparator to deterime which memory sensitive list will be the
	 * next to lose its hard reference.
	 * 
	 * @return the last time the list was accessed
	 */
	long lastAccessed() {
		return lastAccessed;
	}
	
	/**
	 * A list wrapper that is the aggregate of a list and a file location to which the list
	 * can be serialized. Implements the Adapter pattern.
	 * 
	 * @author swenmouth
	 *
	 * @param <T> the list element type
	 */
	static class MemorySensitiveListWrapper<T> extends ListWrapper<T> implements List<T>, Disposable {
		
		private File file;
		
		public MemorySensitiveListWrapper(File file, List<T> list) {
			super(list);
			this.file = file;
		}
		
		public void dispose() throws Throwable {
			FileOutputStream fos = null;
			ObjectOutputStream oos = null;
			long t0 = System.currentTimeMillis();
			try {
				fos = new FileOutputStream(file);
				oos = new ObjectOutputStream(fos);
				oos.writeObject(getList());
				oos.flush();
			}
			catch (IOException e) {
				throw e;
			}
			finally {
				close(oos);
				close(fos);
				Logger.getLogger(MemorySensitiveList.class).info(String.format("Took %d ms to serialize %d items to disk (%d bytes)", System.currentTimeMillis()-t0, getList().size(), file.length()));
			}
		}
	}
	
	/**
	 * Load a serialized list from disk.
	 * 
	 * @param <X> the type of the list 
	 * @param file the location of the serialized list
	 * @return the de-serialized list
	 * @throws NullPointerException if the list could not be loaded
	 */
	@SuppressWarnings("unchecked")
	private static final <X> List<X> load(File file) {
		List<X> list = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		long t0 = System.currentTimeMillis();
		try {
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);
			list = (List<X>) ois.readObject();
			return list;
		}
		catch (IOException e) {
			NullPointerException n = new NullPointerException("Failed to load list from file '" + file.toString() + "'");
			n.initCause(e);
			throw n;
		}
		catch (ClassNotFoundException e) {
			NullPointerException n = new NullPointerException("Failed to load list from file '" + file.toString() + "'");
			n.initCause(e);
			throw n;
		}
		finally {
			close(ois);
			close(fis);
			Logger.getLogger(MemorySensitiveList.class).info(String.format("Took %d ms to load %d items from disk (%d bytes)", System.currentTimeMillis()-t0, (list == null ? -1 : list.size()), file.length()));
		}
	}
	
	/**
	 * Null safe method to close <code>Closeable</code>s.
	 * 
	 * @param c the closable object on which <code>close</code> will be run.
	 */
	private static final void close(Closeable c) {
		try {
			if (c != null) {
				c.close();
			}
		}
		catch (IOException e) {
			Logger.getLogger(MemorySensitiveList.class).error(e);
		}
	}
}
