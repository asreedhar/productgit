package biz.firstlook.commons.collections;

import java.util.ListIterator;

/**
 * <p>Provides a convenient implementation of the ListIterator interface that can be subclassed by
 * developers wishing to adapt the iterator providing more functionality. This class implements
 * the Wrapper or Decorator pattern. Methods default to calling through to the wrapped iterator
 * object.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Interface of the collected element's
 */
public class ListIteratorWrapper<I> implements ListIterator<I> {

	protected ListIterator<I> it;

	public ListIteratorWrapper(ListIterator<I> it) {
		this.it = it;
	}

	public void add(I element) {
		it.add(element);
	}

	public boolean hasNext() {
		return it.hasNext();
	}

	public boolean hasPrevious() {
		return it.hasPrevious();
	}

	public I next() {
		return it.next();
	}

	public int nextIndex() {
		return it.nextIndex();
	}

	public I previous() {
		return it.previous();
	}

	public int previousIndex() {
		return it.previousIndex();
	}

	public void remove() {
		it.remove();
	}

	public void set(I element) {
		it.set(element);
	}

}
