package biz.firstlook.commons.collections;

import java.util.Collection;
import java.util.Iterator;

/**
 * <p>Provides a convenient implementation of the Collection interface that can be subclassed by
 * developers wishing to adapt the collection providing more functionality. This class implements
 * the Wrapper or Decorator pattern. Methods default to calling through to the wrapped collection
 * object.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Interface of the collected element's
 */
public class CollectionWrapper<I> implements Collection<I>, WrappedCollection<I> {

	protected Collection<I> collection;
	
	public CollectionWrapper(Collection<I> collection) {
		this.collection = collection;
	}

	public boolean add(I element) {
		return collection.add(element);
	}

	public boolean addAll(Collection<? extends I> c) {
		return collection.addAll(c);
	}

	public void clear() {
		collection.clear();
	}

	public boolean contains(Object o) {
		return collection.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return collection.containsAll(c);
	}

	public boolean isEmpty() {
		return collection.isEmpty();
	}

	public Iterator<I> iterator() {
		return collection.iterator();
	}

	public boolean remove(Object o) {
		return collection.remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		return collection.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return collection.retainAll(c);
	}

	public int size() {
		return collection.size();
	}

	public Object[] toArray() {
		return collection.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return collection.toArray(a);
	}

	public Collection<I> getCollection() {
		return collection;
	}
	
}
