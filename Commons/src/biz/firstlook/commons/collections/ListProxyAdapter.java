package biz.firstlook.commons.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * Extends the <code>ListAdapter</code> class providing a consistent set of
 * adapters such that changes can be saved between invocations without
 * commiting them to the adapted instance.
 * @author swenmouth
 *
 * @param <I> objects in list adapted to this interface
 * @param <A> class being adapted
 */
public abstract class ListProxyAdapter<I,A> extends ListAdapter<I,A> implements List<I>, WrappedList<A> {

	List<I> proxies;
	
	public ListProxyAdapter(List<A> list) {
		super(list);
		proxies = new ArrayList<I>(list.size());
	}

	public I get(int index) {
		if (proxies.size() > index) {
			return proxies.get(index);
		}
		for (int i = proxies.size(); i <= index; i++) {
			proxies.add(i, super.get(i));
		}
		return proxies.get(index);
	}
	
}
