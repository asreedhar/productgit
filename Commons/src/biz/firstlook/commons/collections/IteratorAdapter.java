package biz.firstlook.commons.collections;

import java.util.Iterator;

/**
 * <p>Provides a convenient implementation of the Iterator interface that can be subclassed by
 * developers wishing to adapt the return type of an existing iterator to another type. This class
 * implements the Adapter pattern.</p>
 * @author swenmouth
 *
 * @param <I>
 * @param <A>
 */
public abstract class IteratorAdapter<I,A> implements Iterator<I> {

	protected Iterator<A> it;
	
	public IteratorAdapter(Iterator<A> it) {
		this.it = it;
	}

	public boolean hasNext() {
		return it.hasNext();
	}

	public I next() {
		return adapt(it.next());
	}

	public void remove() {
		it.remove();
	}

	protected abstract I adapt(A a);
	
}
