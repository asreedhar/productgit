package biz.firstlook.commons.collections;

import java.util.AbstractList;
import java.util.List;

/**
 * <p>Provides a convenient implementation of the List interface that can be subclassed by
 * developers wishing to adapt the return type of an existing list to another type. This class
 * implements the Adapter pattern.</p>
 * @author swenmouth
 *
 * @param <I> the outward facing interface/class
 * @param <A> the interface/class we are adapting
 */
public abstract class ListAdapter<I,A> extends AbstractList<I> implements List<I>, WrappedList<A> {

	protected List<A> list;
	
	public ListAdapter(List<A> list) {
		super();
		this.list = list;
	}

	public I get(int index) {
		return adaptItem(list.get(index));
	}

	public int size() {
		return list.size();
	}

	abstract public I adaptItem(A a);

	public List<A> getList() {
		return list;
	}
	
}
