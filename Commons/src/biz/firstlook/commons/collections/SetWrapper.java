package biz.firstlook.commons.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class SetWrapper<I> implements Set<I> {

	private Set<I> set;
	
	public SetWrapper(Set<I> set) {
		this.set = set;
	}

	public boolean add(I element) {
		return set.add(element);
	}

	public boolean addAll(Collection<? extends I> c) {
		return set.addAll(c);
	}

	public void clear() {
		set.clear();
	}

	public boolean contains(Object element) {
		return set.contains(element);
	}

	public boolean containsAll(Collection<?> c) {
		return set.containsAll(c);
	}

	public boolean isEmpty() {
		return set.isEmpty();
	}

	public Iterator<I> iterator() {
		return set.iterator();
	}

	public boolean remove(Object element) {
		return set.remove(element);
	}

	public boolean removeAll(Collection<?> c) {
		return set.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return set.retainAll(c);
	}

	public int size() {
		return set.size();
	}

	public Object[] toArray() {
		return set.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return set.toArray(a);
	}

}
