package biz.firstlook.commons.collections;

import java.util.List;

/**
 * Indicates that the implementing class is a wrapper around a list.
 * 
 * @author swenmouth
 *
 * @param <A> the type of the list
 */
public interface WrappedList<A> {

	public List<A> getList();
	
}
