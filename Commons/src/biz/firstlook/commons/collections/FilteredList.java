package biz.firstlook.commons.collections;

import java.util.AbstractList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * <p>Implementation of the <code>List</code> interface whose elements are a subset of
 * anothers as determined by a <code>Filter</code> function.</p>
 * <p>There is commented out code which proxies the objects so we can keep track of
 * concurrent modifications. This causes a lot of problems if you need to cast or
 * check against an actual implementation class. If we were to enable the dynamic proxy
 * code then in each of those aforementioned cases the concrete class would have to
 * implement an interface which exposed the implementation class (which would be a
 * method which performs <code>return this;</code>).  I am not taking this approach
 * just for this class. Yet. Simon</p> 
 * @author swenmouth
 *
 * @param <I> The type of the list
 */
public class FilteredList<I> extends AbstractList<I> implements List<I>, WrappedList<I> {

	List<I>   list;
	Filter<I> filter;

	/**
	 * <p>Create a new filtered list from an argument list and filter function.</p>
	 * @param listOf the type of the elements in the list
	 * @param list the list we are filtering
	 * @param filter defines the accept function for membership of the filter list
	 */
	public FilteredList(List<I> list, Filter<I> filter) {
		super();
		this.list = list;
		this.filter = filter;
	}

	/**
	 * <p>Inserts the specified element at the equivalent position to that specified
	 * in the filtered list.</p>
	 */
	public void add(int index, I element) {
		if (index == size()) {
			list.add(element);
		}
		else { 
			int idx = -1;
			for (Iterator<I> it = list.iterator(); it.hasNext() && idx < index;) {
				if (filter.accept(it.next())) {
					idx++;
				}
			}
			if (idx < 0 || idx > list.size()) {
				throw new IndexOutOfBoundsException("{idx=" + idx + " | idx < 0 || idx > " + list.size() + "}");
			}
			list.add(idx, element);
		}
	}
	
	/**
	 * Returns the element at the position in the filtered list.
	 */
	@SuppressWarnings("unchecked")
	public I get(int index) {
		Iterator<I> it = list.iterator();
		int idx = -1;
		I element = null;
		while (it.hasNext() && idx < index) {
			element = it.next();
			if (filter.accept(element)) {
				idx++;
			}
		}
		if (idx != index) {
			throw new IndexOutOfBoundsException(index + " >= " + idx);
		}
		return element;
//		return (I) Proxy.newProxyInstance(
//				getClass().getClassLoader(),
//				element.getClass().getInterfaces(),
//				new Mediator(element));
	}

	/**
	 * Removes the element at the specified position in the filtered list.
	 */
	public I remove(int index) {
		I element = get(index);
		int innerIndex = list.indexOf(element);
		if (innerIndex < 0 || innerIndex >= list.size()) {
			throw new IndexOutOfBoundsException();
		}
		return list.remove(innerIndex);
	}

	public int size() {
		int size = 0;
		for (I element : list) {
			if (filter.accept(element)) {
				size++;
			}
		}
		return size;
	}
	
	@SuppressWarnings("unchecked")
	public List<I> getList() {
		return list;
//		return (List<I>) Proxy.newProxyInstance(
//				getClass().getClassLoader(),
//				list.getClass().getInterfaces(),
//				new ListMediator(list));
	}

	/**
	 * <p>Proxy invocation handler which updates the modCount of the AbstractList
	 * to indicate the list has changed.</p>
	 * @author swenmouth
	 */
//	class Mediator implements InvocationHandler {
//		I element;
//		public Mediator(I element) {
//			this.element = element;
//		}
//		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//			boolean accept = filter.accept(element);
//			Object result = method.invoke(element, args);
//			if (accept != filter.accept(element)) {
//				modCount++;
//			}
//			return result;
//		}
//	}
	
	/**
	 * <p>Proxy invocation handler which updates the modCount of the AbstractList
	 * to indicate the inner list has been accessed.</p>
	 * @author swenmouth
	 */
//	class ListMediator implements InvocationHandler {
//		List<I> list;
//		public ListMediator(List<I> list) {
//			this.list = list;
//		}
//		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//			modCount++; 
//			return method.invoke(list, args);
//		}
//	}
	
	public int indexOf(Object o) {
		Iterator<I> it = list.iterator();
		int idx = -1;
		I element = null;
		while (it.hasNext()) {
			element = it.next();
			if (filter.accept(element)) {
				++idx;
				if (o == null) {
					if (element == null)
						return idx;
				}
				else {
					if (o.equals(element))
						return idx;
				}
			}
		}
		return -1;
	}
	
	public Iterator<I> iterator() {
		return new OptimizedItr();
	}
	
	public ListIterator<I> listIterator(final int index) {
		if (index < 0 || index > size())
			throw new IndexOutOfBoundsException("Index: " + index);
		return new OptimizedListItr(index);
	}
	
	private class OptimizedItr implements Iterator<I> {
		/**
		 * Index of element to be returned by subsequent call to next.
		 */
		int cursor = 0;

		/**
		 * Index of element returned by most recent call to next or
		 * previous.  Reset to -1 if this element is deleted by a call
		 * to remove.
		 */
		int lastRet = -1;

		/**
		 * The modCount value that the iterator believes that the backing
		 * List should have.  If this expectation is violated, the iterator
		 * has detected concurrent modification.
		 */
		int expectedModCount = modCount;

		public boolean hasNext() {
			return (theNextIndex() != -1);
		}

		protected int theNextIndex() {
			int idx = cursor;
			int end = list.size();
			while (idx < end) {
				if (filter.accept(list.get(idx)))
					return idx;
				idx++;
			}
			return -1;
		}
		
		public I next() {
	        checkForComodification();
		    try {
		    	I next = null;
		    	do {
		    		next = list.get(cursor);
		    		lastRet = cursor++;
		    	}
		    	while (!filter.accept(next));
				return next;
		    }
		    catch (IndexOutOfBoundsException e) {
				checkForComodification();
				throw new NoSuchElementException();
		    }
		}

		public void remove() {
		    if (lastRet == -1)
				throw new IllegalStateException();
            checkForComodification();
		    try {
		    	list.remove(lastRet);
				if (lastRet < cursor)
				    cursor--;
				lastRet = -1;
				expectedModCount = modCount;
		    }
		    catch (IndexOutOfBoundsException e) {
		    	throw new ConcurrentModificationException();
		    }
		}

		final void checkForComodification() {
		    if (modCount != expectedModCount)
		    	throw new ConcurrentModificationException();
		}
    }
	
	private class OptimizedListItr extends OptimizedItr implements ListIterator<I> {
		
		OptimizedListItr(int index) {
			Iterator<I> it = list.iterator();
			int realCursor = 0;
			int idx = -1;
			I element = null;
			while (it.hasNext() && idx < index) {
				element = it.next();
				if (filter.accept(element)) {
					idx++;
				}
				realCursor++;
			}
		    cursor = realCursor;
		}

		public boolean hasPrevious() {
			return (thePreviousIndex() != -1);
		}

		protected int thePreviousIndex() {
			int idx = cursor;
			while (idx >= 0) {
				if (filter.accept(list.get(idx)))
					return idx;
				idx--;
			}
			return -1;
		}
		
        public I previous() {
            checkForComodification();
            try {
            	I previous = null;
            	do {
	                int i = cursor - 1;
	                previous = list.get(i);
	                lastRet = cursor = i;
            	}
            	while(!filter.accept(previous));
                return previous;
            }
            catch (IndexOutOfBoundsException e) {
                checkForComodification();
                throw new NoSuchElementException();
            }
        }

		public int nextIndex() {
			throw new UnsupportedOperationException();
		}

		public int previousIndex() {
			throw new UnsupportedOperationException();
		}

		public void set(I o) {
		    if (lastRet == -1)
				throw new IllegalStateException();
            checkForComodification();
		    try {
				list.set(lastRet, o);
				expectedModCount = modCount;
		    }
		    catch(IndexOutOfBoundsException e) {
		    	throw new ConcurrentModificationException();
		    }
		}

		public void add(I o) {
            checkForComodification();
		    try {
				list.add(cursor++, o);
				lastRet = -1;
				expectedModCount = modCount;
		    }
		    catch(IndexOutOfBoundsException e) {
		    	throw new ConcurrentModificationException();
		    }
		}
    }
}
