package biz.firstlook.commons.collections;

import java.util.ArrayList;
import java.util.List;

public class Adapters 
{

public static <T, S> List<T> adapt( List<S> list, Adapter<T, S> adapter )
{
	List<T> adaptedList = new ArrayList<T>();
	if( list == null )
		return null;
	
	for( S item:list)
	{
		adaptedList.add( adapter.adaptItem(item) );
	}
	return adaptedList;
}

public static <T, S> List<S> unadapt( List<T> list, Adapter<T, S> adapter )
{
	List<S> adaptedList = new ArrayList<S>();
	if( list == null )
		return null;
	
	for( T item:list)
	{
		adaptedList.add( adapter.unadaptItem(item) );
	}
	return adaptedList;
}
	
}
