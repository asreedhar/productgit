package biz.firstlook.commons.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class maintains a boolean flag to keep the state Collection it is wrapping.  Every modification to the Collection
 * (add/remove/etc..) sets the validState flag to false.  After the user validates the state of the collection, they call the
 * setAsValid method to flip the validState flag to true. The isValidState method returns the current value of the validState field.</p>
 * <p> This implmentation assumes if a validatable list is instantiated with a collection passed in, it is in an invalid state. If
 * the default  (empty) construtor is used it it in a valid state.</p>  
 * <p> Calling clear on the collection also sets the validState flag to true. </p>  
 * @author dweintrop
 *
 */
public class ValidatableList<I> extends CollectionWrapper<I> {

	private boolean validState;
	
	public ValidatableList() {
		super(new ArrayList<I>());
		this.validState = true;
	}

	public ValidatableList(List<I> collection) {
		super(collection);
		this.validState = false;
	}

	public boolean add(I element) {
		validState = false;
		return collection.add(element);
	}

	public boolean addAll(Collection<? extends I> c) {
		validState = false;
		return collection.addAll(c);
	}

	public void clear() {
		validState = true;
		collection.clear();
	}

	public boolean remove(Object o) {
		validState = false;
		return collection.remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		validState = false;
		return collection.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		validState = false;
		return collection.retainAll(c);
	}

	public List<I> getCollection() {
		// this is safe since the constructor only accepts lists
		// don't let users modify list outside this interface
		return Collections.unmodifiableList( (List<I>)collection );
	}

	public List<I> getList() {
		// this is safe since the constructor only accepts lists
		// don't let users modify list outside this interface
		return Collections.unmodifiableList( (List<I>)collection );
	}

	public void setAsValid() {
		validState = true;
	}
	
	public void setAsInvalid() {
		validState = false;
	}
	
	public boolean isValidState() {
		return validState;
	}
}
