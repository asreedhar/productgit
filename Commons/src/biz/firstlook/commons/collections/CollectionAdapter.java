package biz.firstlook.commons.collections;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

import biz.firstlook.commons.util.Functions;

/**
 * <p>Provides a convenient implementation of the Collection interface that can be subclassed by
 * developers wishing to adapt the return type of an existing collection to another type. This class
 * implements the Adapter pattern.</p>
 * @author swenmouth
 *
 * @param <I> the outward facing interface/class
 * @param <A> the interface/class we are adapting
 */
public abstract class CollectionAdapter<I,A> extends AbstractCollection<I> implements Collection<I>, WrappedCollection<A> {

	protected Collection<A> collection;
	
	public CollectionAdapter(Collection<A> a) {
		super();
		this.collection = a;
	}

	public Iterator<I> iterator() {
		return new Itr(collection.iterator());
	}

	public int size() {
		return collection.size();
	}

	public int hashCode() {
		return collection.hashCode();
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final CollectionAdapter other = (CollectionAdapter) obj;
		return Functions.nullSafeEquals(collection, other.collection);
	}
	
	protected class Itr extends IteratorAdapter<I,A> implements Iterator<I> {
		public Itr(Iterator<A> it) {
			super(it);
		}
		protected I adapt(A a) {
			return adaptItem(a);
		}
	}

	abstract public I adaptItem(A a);

	public Collection<A> getCollection() {
		return collection;
	}
	
}
