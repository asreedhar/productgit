package biz.firstlook.commons.collections;

public interface Adapter<T, S> {
	public T adaptItem(S oldItem);
	public S unadaptItem(T newItem);
}
