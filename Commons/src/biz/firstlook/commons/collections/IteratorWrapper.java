package biz.firstlook.commons.collections;

import java.util.Iterator;

/**
 * <p>Provides a convenient implementation of the Iterator interface that can be subclassed by
 * developers wishing to adapt the iterator providing more functionality. This class implements
 * the Wrapper or Decorator pattern. Methods default to calling through to the wrapped iterator
 * object.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Interface of the collected element's
 */
public class IteratorWrapper<I> implements Iterator<I> {

	protected Iterator<I> it;
	
	public IteratorWrapper(Iterator<I> it) {
		this.it = it;
	}

	public boolean hasNext() {
		return it.hasNext();
	}

	public I next() {
		return it.next();
	}

	public void remove() {
		it.remove();
	}

}
