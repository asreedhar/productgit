package biz.firstlook.commons.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * <p>Provides a convenient implementation of the List interface that can be subclassed by
 * developers wishing to adapt the list providing more functionality. This class implements
 * the Wrapper or Decorator pattern. Methods default to calling through to the wrapped list
 * object.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Interface of the collected element's
 */
public class ListWrapper<I> implements List<I>, WrappedList<I> {

	private List<I> list;

	public ListWrapper(List<I> list) {
		this.list = list;
	}

	public boolean add(I element) {
		return getList().add(element);
	}

	public void add(int index, I element) {
		getList().add(index, element);
	}

	public boolean addAll(Collection<? extends I> c) {
		return getList().addAll(c);
	}

	public boolean addAll(int index, Collection<? extends I> c) {
		return getList().addAll(index, c);
	}

	public void clear() {
		getList().clear();
	}

	public boolean contains(Object o) {
		return getList().contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return getList().containsAll(c);
	}

	public I get(int index) {
		return getList().get(index);
	}

	public int indexOf(Object o) {
		return getList().indexOf(o);
	}

	public boolean isEmpty() {
		return getList().isEmpty();
	}

	public Iterator<I> iterator() {
		return getList().iterator();
	}

	public int lastIndexOf(Object o) {
		return getList().lastIndexOf(o);
	}

	public ListIterator<I> listIterator() {
		return getList().listIterator();
	}

	public ListIterator<I> listIterator(int index) {
		return getList().listIterator(index);
	}

	public I remove(int index) {
		return getList().remove(index);
	}

	public boolean remove(Object o) {
		return getList().remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		return getList().removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return getList().retainAll(c);
	}

	public I set(int index, I element) {
		return getList().set(index, element);
	}

	public int size() {
		return getList().size();
	}

	public List<I> subList(int fromIndex, int toIndex) {
		return getList().subList(fromIndex, toIndex);
	}

	public Object[] toArray() {
		return getList().toArray();
	}

	public <T> T[] toArray(T[] a) {
		return getList().toArray(a);
	}

	public List<I> getList() {
		return list;
	}
}
