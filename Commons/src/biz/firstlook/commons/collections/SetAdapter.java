package biz.firstlook.commons.collections;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;

/**
 * <p>Provides a convenient implementation of the Set interface that can be subclassed by
 * developers wishing to adapt the return type of an existing set to another type. This class
 * implements the Adapter pattern.</p>
 * @author swenmouth
 *
 * @param <I> the outward facing interface/class
 * @param <A> the interface/class we are adapting
 */
public abstract class SetAdapter<I,A> extends AbstractSet<I> implements Set<I> {

	protected Set<A> set;
	
	public SetAdapter(Set<A> a) {
		super();
		this.set = a;
	}

	public Iterator<I> iterator() {
		return new Itr(set.iterator());
	}

	public int size() {
		return set.size();
	}

	protected class Itr extends IteratorAdapter<I,A> implements Iterator<I> {
		public Itr(Iterator<A> it) {
			super(it);
		}
		protected I adapt(A a) {
			return adaptItem(a);
		}
	}

	abstract public I adaptItem(A a);

}

