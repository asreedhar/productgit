package biz.firstlook.commons.collections;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import biz.firstlook.commons.util.Functions;

public class MemorySensitiveListPool<I> {
	
	private static final MemorySensitiveListPoolReaper REAPER;
	static {
		REAPER = new MemorySensitiveListPoolReaper();
	}
	
	private String name;
	private int capactity;
	private PriorityQueue<MemorySensitiveList<I>> hard;
	
	public MemorySensitiveListPool(String name, int capactity) {
		this.name = name;
		this.capactity = capactity;
		this.hard = new PriorityQueue<MemorySensitiveList<I>>(capactity, new MemorySensitiveListComparator<I>());
		REAPER.register(this);
	}
	
	protected void finalize() throws Throwable {
		REAPER.remove(this);
	}

	public MemorySensitiveList<I> newInstance(List<I> list) throws IOException {
		MemorySensitiveList<I> item = new MemorySensitiveList<I>(list, this);
		insert(item);
		return item;
	}

	private void insert(MemorySensitiveList<I> item) {
		synchronized (this) {
			if (hard.contains(item)) {
				return;
			}
			if (hard.size() >= capactity) {
				final MemorySensitiveList<I> evicted = hard.poll();
				evicted.release();
			}
			hard.add(item);
		}
	}
	
	public void aquire(MemorySensitiveList<I> list) {
		synchronized (this) {
			insert(list);
		}
	}
	
	public void release(MemorySensitiveList<I> list) {
		synchronized (this) {
			hard.remove(list);
		}
	}
	
	public void processOldEntries() {
		final int tenMinutes = 1000*60*10;
		synchronized (this) {
			MemorySensitiveList<I> item = null;
			while ((item = hard.peek()) != null) {
				if (System.currentTimeMillis() - item.lastAccessed() > tenMinutes) {
					// let go of the hard reference
					item.release();
					// remove item from the list of hard reference holders
					hard.remove(item);
				}
				else {
					break;
				}
			}
		}
	}
	
	public String toString() {
		return String.format("MemorySensitiveListPool[name=%s,capacity=%d,hard=%d]", name, capactity, hard.size());
	}
	
	class MemorySensitiveListComparator<X> implements Comparator<MemorySensitiveList<X>> {
		public int compare(MemorySensitiveList<X> o1, MemorySensitiveList<X> o2) {
			return Functions.nullSafeCompareTo(o1.lastAccessed(), o2.lastAccessed());
		}
	}
	
	static class MemorySensitiveListPoolReaper extends Thread {
		List<MemorySensitiveListPool> pools = new CopyOnWriteArrayList<MemorySensitiveListPool>();
		public MemorySensitiveListPoolReaper() {
			setDaemon(true);
			start();
		}
		public void run() {
			while (true) {
				try {
					for (MemorySensitiveListPool pool : pools) {
						pool.processOldEntries();
					}
					Thread.sleep(60000);
				}
				catch (Throwable t) {
					Logger.getLogger(MemorySensitiveListPoolReaper.class).error(t);
				}
			}
		}
		public void register(MemorySensitiveListPool pool) {
			pools.add(pool);
		}
		public void remove(MemorySensitiveListPool pool) {
			pools.remove(pool);
		}
	}
}
