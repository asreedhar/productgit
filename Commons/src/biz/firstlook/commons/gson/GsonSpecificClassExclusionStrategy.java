package biz.firstlook.commons.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;


public class GsonSpecificClassExclusionStrategy implements ExclusionStrategy {
   private final Class<?> excludedThisClass;

   public GsonSpecificClassExclusionStrategy(Class<?> excludedThisClass) {
     this.excludedThisClass = excludedThisClass;
   }

   public boolean shouldSkipClass(Class<?> clazz) {
     return excludedThisClass.equals(clazz);
   }

   public boolean shouldSkipField(FieldAttributes f) {
     return excludedThisClass.equals(f.getDeclaredClass());
   }
 }