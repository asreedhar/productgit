package biz.firstlook.commons.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

//Excludes any field (or class) that is tagged with an "@GsonExclude"
public class GsonExcludeAnnotationExclusionStrategy implements ExclusionStrategy {
  
	public boolean shouldSkipClass(Class<?> clazz) {
	    return clazz.getAnnotation(GsonExclude.class) != null;
	  }
	
	public boolean shouldSkipField(FieldAttributes f) {
	    return f.getAnnotation(GsonExclude.class) != null;
	  }
}

