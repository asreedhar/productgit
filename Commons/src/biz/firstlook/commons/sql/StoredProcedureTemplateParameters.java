package biz.firstlook.commons.sql;

import java.util.List;

import org.springframework.jdbc.core.SqlParameter;

public class StoredProcedureTemplateParameters
{

private String storedProcedureCallString;
private List<SqlParameter> sqlParametersWithValues;

public StoredProcedureTemplateParameters()
{
	super();
}

public String getStoredProcedureCallString()
{
	return storedProcedureCallString;
}

/**
 * This should be in the form of { ? = call <i>sprocName</i> ( ?, ?, ... ? ) }.
 * See CallableStatement API for details on the call format.
 * 
 * @param storedProcedureCall
 */
public void setStoredProcedureCallString( String storedProcedureCall )
{
	this.storedProcedureCallString = storedProcedureCall;
}

/**
 * Order must be preserved.
 * @return
 */
public List<SqlParameter> getSqlParametersWithValues()
{
	return sqlParametersWithValues;
}

/**
 * Order must be preserved.
 * @param sqlParametersWithValues
 */
public void setSqlParametersWithValues( List<SqlParameter> sqlParametersWithValues )
{
	this.sqlParametersWithValues = sqlParametersWithValues;
}

}
