package biz.firstlook.commons.sql;

import org.springframework.jdbc.core.SqlParameter;

/**
 * the ordering of parameters is 1's based
 * 
 * @author bfung
 * 
 */
public class SqlParameterWithValue extends SqlParameter
{

private Object parameterValue;

public SqlParameterWithValue( String name, int sqlType, Object parameterValue )
{
	super( name, sqlType );
	this.parameterValue = parameterValue;
}

public Object getParameterValue()
{
	return parameterValue;
}

public void setParameterValue( Object parameterValue )
{
	this.parameterValue = parameterValue;
}

}
