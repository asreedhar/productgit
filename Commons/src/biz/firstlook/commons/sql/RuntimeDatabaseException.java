package biz.firstlook.commons.sql;

public class RuntimeDatabaseException extends RuntimeException {
	
	private static final long serialVersionUID = -5770837328619747082L;

	public RuntimeDatabaseException() {
		super();
	}

	public RuntimeDatabaseException(String message) {
		super(message);
	}

	public RuntimeDatabaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public RuntimeDatabaseException(Throwable cause) {
		super(cause);
	}

}
