package biz.firstlook.commons.sql;

import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

/**
 * A Template for calling a stored procedure using the Spring Framework
 * 
 * use org.springframework.jdbc.object.StoredProcedure.
 * @see org.springframework.jdbc.object.StoredProcedure
 * @see biz.firstlook.cia.persistence.CoreMarketPenetrationRetriever
 */
public class StoredProcedureTemplate extends JdbcTemplate
{

private static final Logger logger = Logger.getLogger( StoredProcedureTemplate.class );

protected static final int SUCCESS_RETURN_CODE = 0;
protected static final String RESULT_SET = "ResultSet";
protected static final String RETURN_CODE = "@RC";

/**
 * If you are getting some error, check to see that your SQLParameterWithValues are put into the list in the order specified by the stored
 * procedure.
 * 
 * @param storedProcParameters
 * @param rowMapper
 * @return
 * @throws DataAccessException
 */
@SuppressWarnings("unchecked")
public Map<String, Object> call( StoredProcedureTemplateParameters storedProcParameters, RowMapper rowMapper ) throws DataAccessException
{
	// The values to pass into the stored proc
	Map<String, Object> inputParameters = new HashMap<String, Object>();
	List<SqlParameter> sqlParametersWithValues = storedProcParameters.getSqlParametersWithValues();
	Iterator inputParameterNameIter = sqlParametersWithValues.iterator();
	while ( inputParameterNameIter.hasNext() )
	{
		SqlParameterWithValue sqlParameterWithValue = (SqlParameterWithValue)inputParameterNameIter.next();
		inputParameters.put( sqlParameterWithValue.getName(), sqlParameterWithValue.getParameterValue() );
	}

	sqlParametersWithValues.add( 0, new SqlReturnResultSet( StoredProcedureTemplate.RESULT_SET, rowMapper ) );
	sqlParametersWithValues.add( 1, new SqlOutParameter( StoredProcedureTemplate.RETURN_CODE, Types.INTEGER ) );

	if ( logger.isDebugEnabled() )
	{
		for ( int i = 0; i < sqlParametersWithValues.size(); i++ )
		{
			SqlParameter sqlParameter = (SqlParameter)sqlParametersWithValues.get( i );
			logger.debug( "sqlParameter parameter: " + sqlParameter.getName() + " at position " + i );
		}
	}

	// the statement to execute
	CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( storedProcParameters.getStoredProcedureCallString(),
																				sqlParametersWithValues );
	CallableStatementCreator csc = cscf.newCallableStatementCreator( inputParameters );
	return call( csc, sqlParametersWithValues );
}

/**
 * Calls the stored procedure and process the result with the CallBack.
 * This is used for special stored procedure calls, for example, those 
 * that return multiple result sets.
 * 
 * The RowMapper can be redundant depending on what role the callBack plays.
 * It is, however, necessary to call a stored procedure with spring. 
 * 
 * @param storedProcParameters
 * @param callback
 * @return
 * @throws DataAccessException
 */
public Object callWithCallBack( StoredProcedureTemplateParameters storedProcParameters, CallableStatementCallback callback ) throws DataAccessException
{
	// The values to pass into the stored proc
	Map<String, Object> inputParameters = new HashMap<String, Object>();
	List<SqlParameter> sqlParametersWithValues = storedProcParameters.getSqlParametersWithValues();
	Iterator inputParameterNameIter = sqlParametersWithValues.iterator();
	while ( inputParameterNameIter.hasNext() )
	{
		SqlParameterWithValue sqlParameterWithValue = (SqlParameterWithValue)inputParameterNameIter.next();
		inputParameters.put( sqlParameterWithValue.getName(), sqlParameterWithValue.getParameterValue() );
	}

	sqlParametersWithValues.add( 0, new SqlOutParameter( StoredProcedureTemplate.RETURN_CODE, Types.INTEGER ) );

	if ( logger.isDebugEnabled() )
	{
		for ( int i = 0; i < sqlParametersWithValues.size(); i++ )
		{
			SqlParameter sqlParameter = (SqlParameter)sqlParametersWithValues.get( i );
			logger.debug( "sqlParameter parameter: " + sqlParameter.getName() + " at position " + i );
		}
	}

	// the statement to execute
	CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( storedProcParameters.getStoredProcedureCallString(),
																				sqlParametersWithValues );

	CallableStatementCreator csc = cscf.newCallableStatementCreator( inputParameters );
	Object result = execute(csc, callback);
	return result;
}

}
