package biz.firstlook.commons.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PreparedStatementCreator {
	public PreparedStatement createPrepareStatement(Connection conn) throws SQLException;
}
