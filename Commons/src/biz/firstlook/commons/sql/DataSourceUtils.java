package biz.firstlook.commons.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * <p>Utility methods for SQL DataSource's.</p>
 * @author swenmouth
 */
public class DataSourceUtils {
	public static final DataSource getDataSource(String jndiName) throws NamingException {
		return (DataSource) ((Context) new InitialContext().lookup("java:comp/env")).lookup(jndiName);
	}
	public static final <T> List<T> executeQuery(DataSource dataSource, PreparedStatementCreator preparedStatementCreator, RowMapper<T> rowMapper) throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<T> objects = new ArrayList<T>();
		try {
			connection = dataSource.getConnection();
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			rs = stmt.executeQuery();
			int rowNum = 0;
			while (rs.next()) {
				objects.add(rowMapper.mapRow(rs, rowNum++));
			}
			return objects;
		}
		finally {
			close(rs);
			close(stmt);
			close(connection);
		}
	}
	public static final <T> List<T> executeQueryForUpdate(DataSource dataSource, PreparedStatementCreator preparedStatementCreator, RowMapper<T> rowMapper) throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<T> objects = new ArrayList<T>();
		try {
			connection = dataSource.getConnection();
			connection.setReadOnly(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			connection.setAutoCommit(false);
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			rs = stmt.executeQuery();
			int rowNum = 0;
			while (rs.next()) {
				objects.add(rowMapper.mapRow(rs, rowNum++));
			}
			connection.commit();
			return objects;
		}
		catch (SQLException e) {
			connection.rollback();
			throw e;
		}
		catch (RuntimeException e) {
			connection.rollback();
			throw e;
		}
		finally {
			close(rs);
			close(stmt);
			close(connection);
		}
	}
	public static final int execute(DataSource dataSource, PreparedStatementCreator preparedStatementCreator) throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = dataSource.getConnection();
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			return stmt.executeUpdate();
		}
		finally {
			close(stmt);
			close(connection);
		}
	}
	public static final int executeInsert(DataSource dataSource, PreparedStatementCreator preparedStatementCreator) throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		PreparedStatement read = null;
		ResultSet rs = null;
		try {
			connection = dataSource.getConnection();
			connection.setReadOnly(true);
			connection.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
			connection.setAutoCommit(false);
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			read = connection.prepareStatement("SELECT @@IDENTITY Id");
			int rowsUpdated = stmt.executeUpdate();
			int identity = -1;
			if (rowsUpdated == 1) {
				rs = read.executeQuery();
				while (rs.next()) {
					identity = rs.getInt("Id");
					if (rs.wasNull()) {
						identity = -1;
					}
				}
				connection.commit();
			}
			else {
				throw new SQLException(String.format("%d rows updated; expected 1", rowsUpdated));
			}
			return identity;
		}
		catch (SQLException e) {
			connection.rollback();
			throw e;
		}
		catch (RuntimeException e) {
			connection.rollback();
			throw e;
		}
		finally {
			close(rs);
			close(read);
			close(stmt);
			close(connection);
		}
	}
	public static final void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			}
			catch (SQLException se) {
				// ignore
			}
		}
	}
	public static final void close(PreparedStatement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			}
			catch (SQLException se) {
				// ignore
			}
		}
	}
	public static final void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			}
			catch (SQLException se) {
				// ignore
			}
		}
	}
}
