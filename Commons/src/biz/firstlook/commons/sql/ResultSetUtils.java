package biz.firstlook.commons.sql;

import java.lang.ref.WeakReference;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>Utility methods to do null safe column accesses from a ResultSet.</p>
 * @author swenmouth
 */
public class ResultSetUtils {
	public static final Date getDate(ResultSet rs, String columnName, boolean nullable) throws SQLException {
		if (nullable && !getColumnNames(rs).contains(columnName)) {
			return null;
		}
		Date columnValue = (Date) rs.getObject(columnName);
		if (rs.wasNull() && !nullable) {
			throw new IllegalStateException("Column '" + columnName + "' was null.");
		}
		return columnValue;
	}
	public static final String getString(ResultSet rs, String columnName, boolean nullable) throws SQLException {
		if (nullable && !getColumnNames(rs).contains(columnName)) {
			return null;
		}
		String columnValue = rs.getString(columnName);
		if (rs.wasNull() && !nullable) {
			throw new IllegalStateException("Column '" + columnName + "' was null.");
		}
		return columnValue;
	}
	public static final Integer getInt(ResultSet rs, String columnName, boolean nullable) throws SQLException {
		if (nullable && !getColumnNames(rs).contains(columnName)) {
			return null;
		}
		int columnValue = rs.getInt(columnName);
		if (rs.wasNull()) {
			if (nullable) {
				return null;
			}
			else {
				throw new IllegalStateException("Column '" + columnName + "' was null.");
			}
		}
		return columnValue;
	}
	public static final Double getDouble(ResultSet rs, String columnName, boolean nullable) throws SQLException {
		if (nullable && !getColumnNames(rs).contains(columnName)) {
			return null;
		}
		double columnValue = rs.getDouble(columnName);
		if (rs.wasNull()) {
			if (nullable) {
				return null;
			}
			else {
				throw new IllegalStateException("Column '" + columnName + "' was null.");
			}
		}
		return columnValue;
	}
	public static final Float getFloat(ResultSet rs, String columnName, boolean nullable) throws SQLException {
		if (nullable && !getColumnNames(rs).contains(columnName)) {
			return null;
		}
		float columnValue = rs.getFloat(columnName);
		if (rs.wasNull()) {
			if (nullable) {
				return null;
			}
			else {
				throw new IllegalStateException("Column '" + columnName + "' was null.");
			}
		}
		return columnValue;
	}
	public static final Boolean getBoolean(ResultSet rs, String columnName, boolean nullable) throws SQLException {
		if (nullable && !getColumnNames(rs).contains(columnName)) {
			return null;
		}
		int columnValue = rs.getInt(columnName);
		if (rs.wasNull()) {
			if (nullable) {
				return null;
			}
			else {
				throw new IllegalStateException("Column '" + columnName + "' was null.");
			}
		}
		return ((columnValue == 1) ? Boolean.TRUE : Boolean.FALSE); // true is a 1 false is a zero
	}
	public static final Set<String> getColumnNames(ResultSet rs) throws SQLException {
		return getMetaStruct(rs).columnNames;
	}
	private static MetaStruct getMetaStruct(ResultSet rs) throws SQLException {
		WeakReference<MetaStruct> metaRef = metaCache.get();
		MetaStruct meta = null;
		if (metaRef == null || metaRef.get() == null) {
			meta = setMetaStruct(rs);
		}
		else {
			meta = metaRef.get();
			if (meta.rs != rs) {
				metaRef.clear();
				meta = setMetaStruct(rs);
			}
		}
		return meta;
	}
	private static MetaStruct setMetaStruct(ResultSet rs) throws SQLException {
		MetaStruct meta = new MetaStruct().init(rs, rs.getMetaData());
		WeakReference<MetaStruct> metaRef = new WeakReference<MetaStruct>(meta);
		metaCache.set(metaRef);
		return meta;
	}
	private static class MetaStruct {
		public ResultSet rs;
		public ResultSetMetaData meta;
		public Set<String> columnNames;
		public MetaStruct init(ResultSet rs, ResultSetMetaData meta) throws SQLException {
			this.rs = rs;
			this.meta = meta;
			if (this.columnNames == null) {
				columnNames = new HashSet<String>();
			}
			else {
				columnNames.clear();
			}
			for (int i = 1; i <= meta.getColumnCount(); i++) {
				columnNames.add(meta.getColumnName(i));
			}
			return this;
		}
	}
	private static ThreadLocal<WeakReference<MetaStruct>> metaCache = new ThreadLocal<WeakReference<MetaStruct>>() {};
}
