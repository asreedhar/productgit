package biz.firstlook.commons.date;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

public class BasisPeriod
{

public static final BasisPeriod THIRTEEN_WEEKS = new BasisPeriod( 91, false );
public static final BasisPeriod TWELVE_WEEKS = new BasisPeriod( 84, false );
public static final BasisPeriod TWENTY_SIX_WEEKS = new BasisPeriod( 182, false );
public static final BasisPeriod FIFTY_TWO_WEEKS = new BasisPeriod( 364, false );
public static final BasisPeriod EIGHTEEN_MONTHS = new BasisPeriod( 549, false );
public static final int BUCKET_WEEKS = 52;

private int days;
private boolean forecast;

public BasisPeriod()
{
}

public BasisPeriod( int days, boolean forecast )
{
    setDays( days );
    setForecast( forecast );
}

public int getDays()
{
    return days;
}

public int getWeeks()
{
    return days / 7;
}

public boolean isForecast()
{
    return forecast;
}

public void setDays( int i )
{
    days = i;
}

public void setForecast( boolean b )
{
    forecast = b;
}

public Date getStartDate()
{
    Calendar cal = getCalendar();

    if ( isForecast() )
    {
        cal.add( Calendar.WEEK_OF_YEAR, -52 );
    }
    else
    {
        cal.add( Calendar.DAY_OF_YEAR, getDays() * -1 );
    }
    
    // MH, 12/29/2005
    // The date used for the basis period should have its time set to midnight (00:00:00)
    Date startDate = new Date( cal.getTimeInMillis() + cal.get( Calendar.DST_OFFSET ) );
	startDate = DateUtils.truncate( startDate, Calendar.DATE );
	return startDate;
}

public Date getEndDate()
{
    Date startDate = getStartDate();
    Calendar cal = getCalendar();
    cal.setTime( startDate );

    int offset = cal.get( Calendar.DST_OFFSET );

    cal.add( Calendar.DAY_OF_YEAR, getDays() );

    offset = ( offset > 0 ) ? 0 : cal.get( Calendar.DST_OFFSET );

    // MH, 12/29/2005
    // The date used for the basis period should have its time set to midnight (00:00:00)
    Date endDate = new Date( cal.getTimeInMillis() + offset );
	endDate = DateUtils.truncate( endDate, Calendar.DATE );
	return endDate;
}

public Calendar getCalendar()
{
    Calendar cal = Calendar.getInstance();
    cal = DateUtils.truncate( cal, Calendar.DATE );

    return cal;
}

}
