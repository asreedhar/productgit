/**
 * 
 */
package biz.firstlook.commons.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class RequestWithCookie extends HttpServletRequestWrapper {
	Cookie cookie;
	public RequestWithCookie(HttpServletRequest request, Cookie cookie) {
		super(request);
		this.cookie = cookie;
	}
	public Cookie[] getCookies() {
		List<Cookie> cookies = new ArrayList<Cookie>(Arrays.asList(super.getCookies()));
		cookies.add(cookie);
		return cookies.toArray(new Cookie[cookies.size()]);
	}
}