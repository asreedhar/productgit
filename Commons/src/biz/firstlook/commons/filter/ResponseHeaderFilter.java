package biz.firstlook.commons.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import biz.firstlook.commons.util.PropertyLoader;

public class ResponseHeaderFilter implements Filter
{
FilterConfig fc;

@SuppressWarnings("unchecked")
public void doFilter( ServletRequest req, ServletResponse res, FilterChain chain ) throws IOException, ServletException
{
	boolean cacheGifImages = PropertyLoader.getBooleanProperty( "firstlook.cache.image.gif", true );
	HttpServletResponse response = (HttpServletResponse)res;
	
	if ( cacheGifImages )
	{
		// set the provided HTTP response parameters
		for ( Enumeration<String> e = fc.getInitParameterNames(); e.hasMoreElements(); )
		{
			String headerName = e.nextElement();
			response.addHeader( headerName, fc.getInitParameter( headerName ) );
		}
	}
	
	// pass the request/response on
	chain.doFilter( req, response );
}

public void init( FilterConfig filterConfig )
{
	this.fc = filterConfig;
}

public void destroy()
{
	this.fc = null;
}
}