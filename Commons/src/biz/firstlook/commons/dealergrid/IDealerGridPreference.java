package biz.firstlook.commons.dealergrid;

public interface IDealerGridPreference
{

public int getFirstDealThreshold();

public int getFirstValuationThreshold();

public int getGridCIATypeValue( int grid );

public int getGridLightValue( int grid );

public int getSecondDealThreshold();

public int getSecondValuationThreshold();

public int getThirdDealThreshold();

public int getThirdValuationThreshold();

public int getFourthDealThreshold();

public int getFourthValuationThreshold();

}
