package biz.firstlook.commons.dealergrid;

public class DealerGridCell
{

private DealerGridCellRange dealRange;
private DealerGridCellRange valuationRange;
private int lightValue;
private int ciaTypeValue;

public DealerGridCell()
{
    super();
}

public int getCiaTypeValue()
{
    return ciaTypeValue;
}

public DealerGridCellRange getDealRange()
{
    return dealRange;
}

public int getLightValue()
{
    return lightValue;
}

public DealerGridCellRange getValuationRange()
{
    return valuationRange;
}

public void setCiaTypeValue( int i )
{
    ciaTypeValue = i;
}

public void setDealRange( DealerGridCellRange range )
{
    dealRange = range;
}

public void setLightValue( int i )
{
    lightValue = i;
}

public void setValuationRange( DealerGridCellRange range )
{
    valuationRange = range;
}

}
