package biz.firstlook.commons.dealergrid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DealerGridPreferenceService
{

private IDealerGridPreference preference;

public DealerGridPreferenceService( IDealerGridPreference preference )
{
    super();
    this.preference = preference;
}

public Map createDealerGridMap()
{
    Map<Integer, Map<Integer, DealerGridCell>> dealerGridMap = new TreeMap<Integer, Map<Integer, DealerGridCell>>();
    int gridValueCounter = 0;

    List dealThresholds = createDealThresholdList();
    List valuationThresholds = createValuationThresholdList();

    Iterator dealThresholdIterator = dealThresholds.iterator();
    DealerGridCellRange dealThreshold = null;
    DealerGridCellRange valuationThreshold = null;

    while (dealThresholdIterator.hasNext())
    {
        dealThreshold = (DealerGridCellRange) dealThresholdIterator.next();
        Map<Integer, DealerGridCell> rowMap = new TreeMap<Integer, DealerGridCell>();
        Iterator valuationThresholdIter = valuationThresholds.iterator();
        while (valuationThresholdIter.hasNext())
        {
            valuationThreshold = (DealerGridCellRange) valuationThresholdIter
                    .next();
            DealerGridCell cell = new DealerGridCell();
            cell.setValuationRange(valuationThreshold);
            cell.setDealRange(dealThreshold);
            cell.setCiaTypeValue(preference
                    .getGridCIATypeValue(gridValueCounter));
            cell.setLightValue(preference.getGridLightValue(gridValueCounter));
            rowMap
                    .put(new Integer(valuationThreshold.getHighThreshold()),
                            cell);
            gridValueCounter++;
        }
        dealerGridMap
                .put(new Integer(dealThreshold.getHighThreshold()), rowMap);
    }

    return dealerGridMap;
}

private List createDealThresholdList()
{
    List<DealerGridCellRange> thresholds = new ArrayList<DealerGridCellRange>();

    DealerGridCellRange firstDealRange = new DealerGridCellRange(0, preference
            .getFirstDealThreshold());
    DealerGridCellRange secondDealRange = new DealerGridCellRange(preference
            .getFirstDealThreshold() + 1, preference.getSecondDealThreshold());
    DealerGridCellRange thirdDealRange = new DealerGridCellRange(preference
            .getSecondDealThreshold() + 1, preference.getThirdDealThreshold());
    DealerGridCellRange fourthDealRange = new DealerGridCellRange(preference
            .getThirdDealThreshold() + 1, preference.getFourthDealThreshold());
    DealerGridCellRange fifthDealRange = new DealerGridCellRange(preference
            .getFourthDealThreshold() + 1, Integer.MAX_VALUE);

    thresholds.add(firstDealRange);
    thresholds.add(secondDealRange);
    thresholds.add(thirdDealRange);
    thresholds.add(fourthDealRange);
    thresholds.add(fifthDealRange);

    return thresholds;
}

private List createValuationThresholdList()
{
    List<DealerGridCellRange> thresholds = new ArrayList<DealerGridCellRange>();

    DealerGridCellRange firstValuationRange = new DealerGridCellRange(
            Integer.MIN_VALUE, preference.getFirstValuationThreshold());
    DealerGridCellRange secondValuationRange = new DealerGridCellRange(
            preference.getFirstValuationThreshold() + 1, preference
                    .getSecondValuationThreshold());
    DealerGridCellRange thirdValuationRange = new DealerGridCellRange(
            preference.getSecondValuationThreshold() + 1, preference
                    .getThirdValuationThreshold());
    DealerGridCellRange fourthValuationRange = new DealerGridCellRange(
            preference.getThirdValuationThreshold() + 1, preference
                    .getFourthValuationThreshold());
    DealerGridCellRange fifthValuationRange = new DealerGridCellRange(
            preference.getFourthValuationThreshold() + 1, Integer.MAX_VALUE);

    thresholds.add(firstValuationRange);
    thresholds.add(secondValuationRange);
    thresholds.add(thirdValuationRange);
    thresholds.add(fourthValuationRange);
    thresholds.add(fifthValuationRange);

    return thresholds;
}

}
