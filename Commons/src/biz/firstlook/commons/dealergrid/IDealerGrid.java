package biz.firstlook.commons.dealergrid;

public interface IDealerGrid
{
public IDealerGridPreference getDealerGridPreference();

public DealerGridCell determineDealerGridCell( double valuation, int unitsSold );

}
