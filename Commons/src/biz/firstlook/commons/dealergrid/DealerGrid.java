package biz.firstlook.commons.dealergrid;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class DealerGrid implements IDealerGrid
{

private IDealerGridPreference dealerGridPreference;

public DealerGrid()
{
    super();
}

public DealerGrid( IDealerGridPreference dealerGridPreference )
{
    this.dealerGridPreference = dealerGridPreference;
}

public DealerGridCell determineDealerGridCell( double valuation, int unitsSold )
{
    DealerGridPreferenceService service = new DealerGridPreferenceService(
            dealerGridPreference);
    Map dealerGridMap = service.createDealerGridMap();

    Set dealThresholdKeys = dealerGridMap.keySet();
    Iterator dealThresholdKeysIter = dealThresholdKeys.iterator();
    Integer dealKey = null;
    Integer valuationKey = null;

    while (dealThresholdKeysIter.hasNext())
    {
        dealKey = (Integer) dealThresholdKeysIter.next();
        if ( unitsSold <= dealKey.intValue() )
        {
            Map valuationMap = (TreeMap) dealerGridMap.get(dealKey);
            Set valuationThresholdKeys = valuationMap.keySet();
            Iterator valuationThresholdKeysIter = valuationThresholdKeys
                    .iterator();
            while (valuationThresholdKeysIter.hasNext())
            {
                valuationKey = (Integer) valuationThresholdKeysIter.next();
                if ( valuation <= valuationKey.intValue() )
                {
                    return (DealerGridCell) valuationMap.get(valuationKey);
                }
            }
        }
    }
    return null;

}

public IDealerGridPreference getDealerGridPreference()
{
    return dealerGridPreference;
}
}
