package biz.firstlook.commons.dealergrid;

public class DealerGridCellRange
{

private int lowThreshold;
private int highThreshold;

public DealerGridCellRange( int low, int high )
{
    super();
    lowThreshold = low;
    highThreshold = high;
}

public int getHighThreshold()
{
    return highThreshold;
}

public int getLowThreshold()
{
    return lowThreshold;
}

public void setHighThreshold( int i )
{
    highThreshold = i;
}

public void setLowThreshold( int i )
{
    lowThreshold = i;
}

}
