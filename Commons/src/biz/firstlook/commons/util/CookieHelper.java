package biz.firstlook.commons.util;

import javax.servlet.http.Cookie;

public class CookieHelper {
	private final static String INVALID_COOKIE_CHARS = "\\s|\\[|\\]|\\{|\\}|\\(|\\)|\\=|\\,|\\\"|\\?|\\\\|/|@|:|;";
	public final static String TRUESIGHT_USERNAME_COOKIE_NAME = "ts_username";
	public final static String TRUESIGHT_DEALER_COOKIE_NAME = "ts_dealer";
	public final static String TRUESIGHT_DEALERGROUP_COOKIE_NAME = "ts_dealergroup";
	
	/**
	 * Helps creating a cookie, removing invalid characters in the value as to be compatible with Version 0 (by Netscape)
	 * @param name - a name conforming to RFC 2109.
	 * @param value
	 * @return the constructed cookie
	 * @throws java.lang.IllegalArgumentException - if the cookie name contains illegal characters (for example, a comma, space, or semicolon) or it is one of the tokens reserved for use by the cookie protocol
	 * @see http://java.sun.com/javaee/5/docs/api/javax/servlet/http/Cookie.html#setValue(java.lang.String)
	 */
	public static Cookie createCookie(String name, String value) {
		return new Cookie(name, value.replaceAll(INVALID_COOKIE_CHARS, ""));
	}
	
	private static Cookie createSessionCookie(String name, String value) {
		Cookie cookie = createCookie(name, value);
		cookie.setPath("/");
		cookie.setMaxAge(-1);
		return cookie;
	}
	
	public static Cookie createTrueSightMemberCookie(String memberName) {
		return createSessionCookie(TRUESIGHT_USERNAME_COOKIE_NAME, memberName);
	}
	
	public static Cookie createTrueSightDealerCookie(String dealerName) {
		return createSessionCookie(TRUESIGHT_DEALER_COOKIE_NAME, dealerName);
	}
	
	public static Cookie createTrueSightDealerGroupCookie(String dealerGroupName) {
		return createSessionCookie(TRUESIGHT_DEALERGROUP_COOKIE_NAME, dealerGroupName);
	}
}
