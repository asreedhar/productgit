package biz.firstlook.commons.util;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class VinUtility 
{

/**
 * As per Vin standard, a vin is only unique for 30 years.
 */
private static final Map<String, Integer> modelYearHash;
private static final Map<String, Integer> vinValueHash = new HashMap<String, Integer>();
private static final int[] vinCharacterWeightFactor = new int[] { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };

static
{
	HashMap<String, Integer> modelYearDigit = new HashMap<String, Integer>(30);
	modelYearDigit.put( "9", 2009 );
	modelYearDigit.put( "8", 2008 );
	modelYearDigit.put( "7", 2007 );
	modelYearDigit.put( "6", 2006 );
	modelYearDigit.put( "5", 2005 );
	modelYearDigit.put( "4", 2004 );
	modelYearDigit.put( "3", 2003 );
	modelYearDigit.put( "2", 2002 );
	modelYearDigit.put( "1", 2001 );
	modelYearDigit.put( "Y", 2000 );
	modelYearDigit.put( "X", 1999 );
	modelYearDigit.put( "W", 1998 );
	modelYearDigit.put( "V", 1997 );
	modelYearDigit.put( "T", 1996 );
	modelYearDigit.put( "S", 1995 );
	modelYearDigit.put( "R", 1994 );
	modelYearDigit.put( "P", 1993 );
	modelYearDigit.put( "N", 1992 );
	modelYearDigit.put( "M", 1991 );
	modelYearDigit.put( "L", 1990 );
	modelYearDigit.put( "K", 1989 );
	modelYearDigit.put( "J", 1988 );
	modelYearDigit.put( "H", 1987 );
	modelYearDigit.put( "G", 1986 );
	modelYearDigit.put( "F", 1985 );
	modelYearDigit.put( "E", 1984 );
	modelYearDigit.put( "D", 1983 );
	modelYearDigit.put( "C", 1982 );
	modelYearDigit.put( "B", 1981 );
	modelYearDigit.put( "A", 1980 );
	modelYearHash = Collections.unmodifiableMap(modelYearDigit);

	vinValueHash.put( "A", Integer.valueOf( 1 ) );
	vinValueHash.put( "B", Integer.valueOf( 2 ) );
	vinValueHash.put( "C", Integer.valueOf( 3 ) );
	vinValueHash.put( "D", Integer.valueOf( 4 ) );
	vinValueHash.put( "E", Integer.valueOf( 5 ) );
	vinValueHash.put( "F", Integer.valueOf( 6 ) );
	vinValueHash.put( "G", Integer.valueOf( 7 ) );
	vinValueHash.put( "H", Integer.valueOf( 8 ) );
	vinValueHash.put( "J", Integer.valueOf( 1 ) );
	vinValueHash.put( "K", Integer.valueOf( 2 ) );
	vinValueHash.put( "L", Integer.valueOf( 3 ) );
	vinValueHash.put( "M", Integer.valueOf( 4 ) );
	vinValueHash.put( "N", Integer.valueOf( 5 ) );
	vinValueHash.put( "P", Integer.valueOf( 7 ) );
	vinValueHash.put( "R", Integer.valueOf( 9 ) );
	vinValueHash.put( "S", Integer.valueOf( 2 ) );
	vinValueHash.put( "T", Integer.valueOf( 3 ) );
	vinValueHash.put( "U", Integer.valueOf( 4 ) );
	vinValueHash.put( "V", Integer.valueOf( 5 ) );
	vinValueHash.put( "W", Integer.valueOf( 6 ) );
	vinValueHash.put( "X", Integer.valueOf( 7 ) );
	vinValueHash.put( "Y", Integer.valueOf( 8 ) );
	vinValueHash.put( "Z", Integer.valueOf( 9 ) );
	vinValueHash.put( "0", Integer.valueOf( 0 ) );
	vinValueHash.put( "1", Integer.valueOf( 1 ) );
	vinValueHash.put( "2", Integer.valueOf( 2 ) );
	vinValueHash.put( "3", Integer.valueOf( 3 ) );
	vinValueHash.put( "4", Integer.valueOf( 4 ) );
	vinValueHash.put( "5", Integer.valueOf( 5 ) );
	vinValueHash.put( "6", Integer.valueOf( 6 ) );
	vinValueHash.put( "7", Integer.valueOf( 7 ) );
	vinValueHash.put( "8", Integer.valueOf( 8 ) );
	vinValueHash.put( "9", Integer.valueOf( 9 ) );
}

//Static class
private VinUtility() {
	super();
}

public static boolean isValidFirstLookVin( final String vin )
{
	try {
		validate(vin);
	} catch (InvalidVinException e) {
		return false;
	}
	return true;
}

public static void validate( final String vin ) throws InvalidVinException
{
	if ( vin == null || vin.length() != 17 ) { 
		throw new InvalidVinException( "Vin is not 17 digits.");
	}
	
	int total = 0;
	final String vinSequence = vin.toUpperCase().trim();
	getLatestModelYear( vinSequence );
	for ( int i = 0; i < vinSequence.length(); i++ )
	{
		Integer assignedValue = (Integer)VinUtility.vinValueHash.get( vinSequence.substring( i, i + 1 ) );
		if ( assignedValue == null )
		{
			throw new InvalidVinException( "Not a valid vin, failed checksum, digit #" + (i + 1) + " is bad.");
		}
		total += vinCharacterWeightFactor[i] * assignedValue.intValue();
	}

	int remainder = total % 11;

	final String tenthDigit = vinSequence.substring( 8, 9 );
	if ( remainder == 10 )
	{
		if(!tenthDigit.equalsIgnoreCase( "X" )) {
			throw new InvalidVinException("The calculated remainder was 10, but the check digit was not 'X'");
		}
	}
	else
	{
		int checkDigit = 0;
		try
		{
			checkDigit = Integer.parseInt( tenthDigit );
		}
		catch ( NumberFormatException nfe )
		{
			throw new InvalidVinException("VIN digit 10 was expected to be a number");
		}
		
		if( remainder != checkDigit ) {
			throw new InvalidVinException( "VIN did not pass 'remainder == checkDigit' test");
		}
	}
}

public static Integer getLatestModelYear( final String vin ) throws InvalidVinException
{
	String tenthDigit = vin.substring( 9, 10 );
	
	Integer baseYear = VinUtility.modelYearHash.get( tenthDigit.toUpperCase() );
	
	if (baseYear == null) {
		throw new InvalidVinException( "Invalid ModelYear digit in the vin: " + tenthDigit);
	}
	
	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	
	int modelYearRot = modelYearHash.size();
	
	int year = baseYear;
	
	if ((currentYear - baseYear) >= modelYearRot) {
		year += modelYearRot;
	}
	
	return year;
}

}
