package biz.firstlook.commons.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Common functions used through-out the code.
 * @author swenmouth
 */
public class Functions {

	/**
	 * Compare the arguments for equality.
	 * @param <T> generic type of arguments
	 * @param t0 reference object for equality
	 * @param t1 target object compared to for equality
	 * @return true if the two objects are equivalent
	 */
	public static final boolean nullSafeEqualsIgnoreCase(String t0, String t1) {
		boolean lEquals = true;
		if (t0 == null) {
			lEquals &= (t1 == null);
		}
		else {
			if (t1 == null) {
				lEquals &= false;
			}
			else {
				lEquals &= t0.equalsIgnoreCase(t1);
			}
		}
		return lEquals;
	}
	/**
	 * Compare the arguments for equality.
	 * @param <T> generic type of arguments
	 * @param t0 reference object for equality
	 * @param t1 target object compared to for equality
	 * @return true if the two objects are equivalent
	 */
	public static final <T extends Object> boolean nullSafeEquals(T t0, T t1) {
		boolean lEquals = true;
		if (t0 == null) {
			lEquals &= (t1 == null);
		}
		else {
			if (t1 == null) {
				lEquals &= false;
			}
			else {
				lEquals &= t0.equals(t1);
			}
		}
		return lEquals;
	}
	
	/**
	 * Take a null safe hash code of the argument.
	 * @param <T> the type of the object
	 * @param t0 the object whose hash code we desire
	 * @return the hashCode of the object, 0 if null
	 */
	public static final <T extends Object> int nullSafeHashCode(T t0) {
		if (t0 == null) {
			return 0;
		}
		return t0.hashCode();
	}
	
	/**
	 * Take a null safe compareTo of the arguments.
	 * @param <T> type we are comparing
	 * @param t0 the reference object
	 * @param t1 the object we are comparing against
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
	 */
	public static final <T> int nullSafeCompareTo(Comparable<T> t0, T t1) {
		if (t0 == null) {
			if (t1 == null) {
				return 0;
			}
			return -1;
		}
		else {
			if (t1 == null) {
				return 1;
			}
			return t0.compareTo(t1);
		}
	}
	
	/**
	 * Take a null safe compareTo of the arguments unless the value of c is not zero.
	 * @param <T> type we are comparing
	 * @param t0 the reference object
	 * @param t1 the object we are comparing against
	 * @param c the clients compareTo state 
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
	 */
	public static final <T> int nullSafeCompareTo(Comparable<T> t0, T t1, int c) {
		if (c == 0) {
			return nullSafeCompareTo(t0, t1);
		}
		return c;
	}

	public static final <T extends Comparable<T>> T nullSafeMin(T t0, T t1) {
		if (t0 == null) {
			if (t1 == null) {
				return null;
			}
			return t1;
		}
		else {
			if (t1 == null) {
				return t0;
			}
			return (t0.compareTo(t1) <= 0) ? t0 : t1;  
		}
	}
	
	public static final <T extends Comparable<T>> T nullSafeMax(T t0, T t1) {
		if (t0 == null) {
			if (t1 == null) {
				return null;
			}
			return t1;
		}
		else {
			if (t1 == null) {
				return t0;
			}
			return (t0.compareTo(t1) >= 0) ? t0 : t1;  
		}
	}
	
	public static final <T extends Object> Boolean nullSafeBoolean(T t) {
		Boolean val = null;
		if(t != null) {
			if(t instanceof Boolean) {
				val = (Boolean)t;
			} else if(t instanceof Number) {
				Number res = (Number)t;
				if (res.longValue() >= 1) {
					val = Boolean.TRUE;
				} else {
					if(res.doubleValue() >= 1.0D ) {
						val = Boolean.TRUE;
					} else {
						val = Boolean.FALSE;
					}
				}
			} else if(t instanceof String) {
				String res = ((String)t).trim();
				if (res.equalsIgnoreCase(Boolean.TRUE.toString())) {
					val = Boolean.TRUE;
				} else {
					Long resLong = null;
					try {
						resLong = Long.valueOf(res);
						val = nullSafeBoolean(resLong);
					} catch (NumberFormatException nfe ) {
						//drop through, resLong should be null
					}
					
					if(resLong == null) {
						try {
							Double resDouble = Double.valueOf(res);
							val = nullSafeBoolean(resDouble);
						} catch (NumberFormatException nfe ) {
							val = Boolean.FALSE;
						}
					}
				}
			}
			else {
				val = Boolean.valueOf(t.toString());
			}
		}
		return val;
	}
	
	public static final <T extends Object> Float nullSafeFloat(T t) {
		Float f = null;
		if (t != null) {
			if (t instanceof Number) {
				f = ((Number)t).floatValue();
			}
			else {
				try {
					f = Float.valueOf(t.toString());
				}
				catch (NumberFormatException nfe) {
					// ignore safely;
				}
			}
		}
		return f;
	}

	public static final <T extends Object> int nullSafeIntValue(T t) {
		int intValue = 0;
		if (t != null) {
			if (t instanceof Number) {
				intValue = ((Number)t).intValue();
			}
			else {
				try {
					intValue = Integer.parseInt(t.toString());
				}
				catch (NumberFormatException nfe) {
					// ignore safely;
				}
			}
		}
		return intValue;
	}
	
	public static final <T extends Object> Integer nullSafeInteger(T t) {
		Integer integer = null;
		if (t != null) {
			if (t instanceof Number) {
				integer = Integer.valueOf(((Number)t).intValue());
			}
			else {
				try {
					integer = Integer.valueOf(t.toString());
				}
				catch (NumberFormatException nfe) {
					// ignore safely;
				}
			}
		}
		return integer;
	}
	
	public static final <T extends Object> String nullSafeToString(T t) {
		return (t == null) ? "null" : t.toString();
	}

	public static final <T extends Number> String nullSafeToDollarString(T t) {
		return (t == null) ? "null" : new DecimalFormat("$###,###,###").format(t.longValue());
	}
	
	public static final Integer nullSafeAbs(Integer i) {
		return (i == null) ? null : Math.abs(i);
	}
	
	public static final String trimWithEmptyToNull(String str) {
		return (isNullOrEmpty(str)) ? null : str.trim();
	}
	
	public static final boolean isNullOrEmpty(String str) {
		return (str == null || str.trim().length() == 0);
	}
	
	public static final List<Integer> parseInt(List<String> vals) {
		List<Integer> ints = new ArrayList<Integer>(vals.size());
		for (String str : vals) {
			try {
				ints.add(Integer.parseInt(str));
			}
			catch (NumberFormatException nfe) {
				// ignore
			}
		}
		return ints;
	}
	
	public static final <T> List<T> subList(List<T> source, List<Integer> indices) {
		List<T> target = new ArrayList<T>(indices.size());
		for (Integer index : indices) {
			if (index >= 0 && index < source.size()) {
				target.add(source.get(index));
			}
		}
		return target;
	}
	
	public static final <K,V> void put(Map<K,List<V>> map, K key, V value) {
		List<V> list = map.get(key);
		if (list == null) {
			list = new ArrayList<V>();
			map.put(key, list);
		}
		list.add(value);
	}
	
	public static final Date midnight(Date date) {
		Date midnight = null;
		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			midnight = cal.getTime();
		}
		return midnight;
	}
	
	public static final Date midnight(int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, days);
		return Functions.midnight(cal.getTime());
	}
	
	public static final <T> Set<T> intersection(Set<T> a, Set<T> b) {
		Set<T> union = new HashSet<T>(a);
		union.retainAll(b);
		return union;
	}
	
}
