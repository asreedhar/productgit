package biz.firstlook.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.time.DateUtils;

public class DateUtilFL
{

public final static long MILLISECONDS_PER_DAY = 86400000;
public final static long MILLISECONDS_PER_HOUR = 3600000;
public final static long MILLISECONDS_PER_MINUTE = 60000;

/**
 * Calculates the difference between two calendar days.
 * 
 * @return a positive number of days between the startDate and endDate regardless of chronology.
 */
public static int calculateNumberOfCalendarDays( Date dateA, Date dateB )
{
	int differenceInDays = 0;
	if ( dateA != null && dateB != null ) {
		Calendar calA = normalizeDST(dateA);
		Calendar calB = normalizeDST(dateB);
		long diff = Math.abs(calA.getTimeInMillis() - calB.getTimeInMillis());
		if (diff >= MILLISECONDS_PER_DAY) { 
		  differenceInDays = (int) (diff/MILLISECONDS_PER_DAY);
		}
		return differenceInDays;
	}
	return differenceInDays;
}

static int calculateNumberOfLeapYears( int earlierYear, int laterYear )
{
	int numberOfYears = Math.abs( laterYear - earlierYear );

	if ( numberOfYears >= 4 )
	{
		return numberOfYears / 4;
	}
	else if ( numberOfYears != 0 )
	{
		boolean hasLeapYear = false;
		for ( int year = earlierYear; year <= laterYear; year++ )
		{
			GregorianCalendar calendar = new GregorianCalendar();
			hasLeapYear = hasLeapYear | calendar.isLeapYear( year );
		}

		if ( hasLeapYear )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	return 0;
}

/**
 * <p>
 * This method sets the time field on a Calendar object to take into account the Daylights Savings Time. This is especially helpful when doing
 * date diff.
 * </p>
 * For example, an inventory record came into the system on: <br>
 * [hh:mm::ss] 00:00:00 Feb. 11, 2005 (CST). <br>
 * Today is [hh:mm::ss] 00:00:00 Apr. 12, 2005 (CDT). <br>
 * <p>
 * If you do a straight date2.getTime() - date1.getTime() / millisecondsPerDay, you get 59 days, while the correct answer is 60 days. This is
 * because [hh:mm::ss] 00:00:00 Apr. 12, 2005 (CDT) is really [hh:mm::ss] 23:00:00 Apr. 11, 2005 (CST). This method will make the correction
 * automatically.
 * </p>
 */
public static Calendar normalizeDST( Date date )
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( date );
	calendar.add( Calendar.HOUR_OF_DAY, (int)( calendar.get( Calendar.DST_OFFSET ) / DateUtilFL.MILLISECONDS_PER_HOUR ) );
	return calendar;
}

/**
 * @deprecated As of Iteration 170, replaced by {@link #normalizeDST(Date) normalizeDST}. Use the <code>getTimeInMillis()</code> from
 *             java.util.Calendar to get a long from the new method.
 */
public static long dateInMillisWithDST( Date date )
{
	// return normalizeDST( date ).getTimeInMillis();
	return date.getTime() + Calendar.getInstance().get( Calendar.DST_OFFSET );
}

public static Integer getWeekOfYear( Date date )
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( date );
	return new Integer( calendar.get( Calendar.WEEK_OF_YEAR ) );
}

public static Date getWeekStartDate( Date date )
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( date );
	calendar.add( Calendar.DAY_OF_WEEK, Calendar.SUNDAY - calendar.get( Calendar.DAY_OF_WEEK ) );
	return calendar.getTime();

}

public static Date getWeekEndDate( Date date )
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( date );
	calendar.add( Calendar.DAY_OF_WEEK, Calendar.SATURDAY - calendar.get( Calendar.DAY_OF_WEEK ) );
	return calendar.getTime();

}

public static Date addDaysToDate( Date date, int days )
{
	Calendar cal = Calendar.getInstance();
	cal.setTime( date );
	cal.add( Calendar.DATE, days );
	return cal.getTime();
}

public static Date addDaysToDateNoTime( Date date, int days )
{
	date = addDaysToDate( date, days );
	Calendar cal = Calendar.getInstance();
	cal.setTime( date );
	cal = DateUtils.truncate( cal, Calendar.DATE );
	return cal.getTime();
}

public static String getFormattedDatePeriodBasedOnDays( int daysOut )
{
	String returnStr = "";
	switch ( daysOut )
	{
		case 7:
			returnStr = "One Week";
			break;
		case 14:
			returnStr = "Two Weeks";
			break;
		case 30:
			returnStr = "One Month";
			break;
		case 60:
			returnStr = "Two Months";
			break;
		case 183:
			returnStr = "Six Months";
			break;
		case 365:
			returnStr = "One Year";
			break;
		default:
			returnStr = "No Preference";
			break;

	}
	return returnStr;
}

/**
 * Returns today's date at 23:59
 * 
 * @return Date
 */
public static Date setToEndOfDay( Date date )
{
	Calendar cal = Calendar.getInstance();
	cal.setTime( date );

	cal.set( Calendar.HOUR_OF_DAY, 23 );
	cal.set( Calendar.MINUTE, 59 );

	return cal.getTime();
}

/**
 * Parses a SQL Server date string into a java.util.Date
 * @param sqlServerDate
 * @return Date
 */
public static Date getDateFromSQLServerDate( String sqlServerDate )
{
    SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
    try
    {
    	return dateFormat.parse( sqlServerDate );
    }
    catch (ParseException pe )
    {
    	return null;	
    }		
}

/**
 * Parses a SQL Server date string into a short formatted
 * date string (MM-dd-yyyy).
 * @param sqlServerDate
 * @return String
 */
public static String formatSQLServerDate( String sqlServerDate )
{
    Date reminderDate = getDateFromSQLServerDate( sqlServerDate );
    return new SimpleDateFormat( "MM-dd-yyyy" ).format( reminderDate );
}

/**
 * Parses a date string of the format "MM/dd/yyyy" into
 * a Date object
 * @param dateString
 * @return Date
 */
public static Date parseShortDateString( String dateString )
{
    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
    
    try
    {
        return dateFormat.parse( dateString );	
    }
    catch ( ParseException pe )
    {
        return null;
    }
}

/*
 * Helper functions
 */
public static Date getDateInPast( Integer numberOfDaysBack )
{
	return DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -numberOfDaysBack.intValue() );
}


}
