package biz.firstlook.commons.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class ColorUtility
{

public enum Color
{
	//They all need to be upper case! -bf.
	UNKNOWN, BEIGE, BLACK, BLUE, BROWN, GOLD, GRAY, GREEN, MAROON, 
	ORANGE, PEWTER, RED, SILVER, TAN, TEAL, WHITE, YELLOW;
}

private static final List< String > colors;

static
{
	List< String > tmpColors = new ArrayList< String >();
	for( Color color : Color.values() )
		tmpColors.add( color.toString() );
	colors = Collections.unmodifiableList( tmpColors );
}

/**
 * @return an unmodifiable list of standard colors.
 */
public static List< String > retrieveStandardColors()
{
	return colors;
}

public static String formatColor( String color )
{
	if( StringUtils.isEmpty( color ) )
		return Color.UNKNOWN.toString();
	else if( StringUtils.isWhitespace( color ) )
		return color;
	else
		return color.trim().toUpperCase();
}

}
