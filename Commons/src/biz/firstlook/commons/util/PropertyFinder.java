package biz.firstlook.commons.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class PropertyFinder implements IPropertyFinder
{
private ResourceBundle bundle;
private String bundleName;

public PropertyFinder( String bundleName )
{
    this.bundleName = bundleName;
    loadBundle(bundleName);
}

public String getProperty( String key )
{
    try
    {
        return bundle.getObject(key).toString();
    } catch (Exception ex)
    {
        return null;
    }
}

private synchronized void loadBundle( String bundleName )
{
    try
    {
        bundle = ResourceBundle.getBundle(bundleName);
    } catch (MissingResourceException e)
    {
        System.out.println(" Error Loading Resource : " + e);
    }
}

public synchronized void reset()
{
    loadBundle(bundleName);
}

}