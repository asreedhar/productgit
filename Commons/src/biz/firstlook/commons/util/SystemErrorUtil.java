package biz.firstlook.commons.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import biz.firstlook.commons.sql.DataSourceUtils;
import biz.firstlook.commons.sql.PreparedStatementCreator;
import biz.firstlook.commons.sql.RowMapper;

import biz.firstlook.commons.services.fault.*;
import biz.firstlook.services.fault.SaveRemoteResultsDto;

/**
 * This is the common util to used to log to our SystemErrors table.
 * It returns the index of the SystemError in the DB.
 * Returns null when an exception is thrown writting to the SystemErrors table
 * 
 * @author nkeen
 * 
 */
public class SystemErrorUtil {
	private static final Logger logger = Logger.getLogger(SystemErrorUtil.class);
	private static final String platform = "Java";
	
	public static List<Integer> log(final SystemErrorBean errorBean) {
		try {
			return DataSourceUtils.executeQuery(
					DataSourceUtils.getDataSource("jdbc/IMT"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							CallableStatement stmt = conn.prepareCall("{call LogSystemError(?,?,?,?,?,?,?)}");
							setString(stmt, 1, errorBean.getStackTrace());
							setInt(stmt, 2, errorBean.getBusinessUnitId());
							setInt(stmt, 3, errorBean.getMemberId());
							setString(stmt, 4, errorBean.getHttpSessionId());
							setString(stmt, 5, errorBean.getProduct());
							setString(stmt, 6, errorBean.getSource());
							setString(stmt, 7, errorBean.getHalSessionID());
							return stmt;
						}
						private void setInt(CallableStatement stmt, final int index, final Integer i) throws SQLException {
							if (i == null) {
								stmt.setNull(index, java.sql.Types.INTEGER);
							}
							else {
								stmt.setInt(index, i);
							}
						}
						private void setString(CallableStatement stmt, final int index, final String s) throws SQLException {
							if (s == null) {
								stmt.setNull(index, java.sql.Types.VARCHAR);
							}
							else {
								// truncate trace to 8000 char... if a trace is longer than this
								// the extra detail will be in the sys logs on the file system
								if (s.length() > 7999) {
									stmt.setString(index, s.substring(0, 7999));
								}
								else {
									stmt.setString(index, s);
								}
							}
						}
					},
					new RowMapper<Integer>() {
						public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
							Integer result = -615;
							if (!rs.wasNull()) {
								result = rs.getInt(1);
							}
							return result;
						}
					});
		}
		catch (Exception e) {
			logger.error("Error logging system exception to dbo.SystemErrors.",e);
		}
		return null;
	}
	
	public static Integer save(final SystemErrorBean errorBean, HttpServletRequest request) {
		try {
			
			FaultService service = FaultService.getInstance();
			
			SaveRemoteResultsDto result = service.saveException(errorBean.getException(), errorBean.getProduct(), request.getServerName(), platform, request.getRemoteUser(), request.getContextPath(), request.getRequestURL().toString(), request.getRemoteHost());
			return new Integer(result.getFaultEvent().getId());
		}
		catch (Exception e) {
			logger.error("Error saving system exception to the Fault database.",e);
		}
		return null;
	}

}
