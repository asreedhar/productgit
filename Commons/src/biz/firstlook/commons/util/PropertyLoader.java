package biz.firstlook.commons.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


public class PropertyLoader
{

private static Collection<IPropertyFinder> propertyFinders = new ArrayList<IPropertyFinder>();

private static Collection<IPropertyFinder> getPropertyFinders()
{
    if ( propertyFinders == null )
    {
        propertyFinders = new ArrayList<IPropertyFinder>();
    }
    if ( propertyFinders.isEmpty() )
    {
        propertyFinders.add(new SystemPropertyFinder());
        // global - in $TOMCAT_HOME/common/classes
        propertyFinders.add(new PropertyFinder("firstlookEnvironment"));
        // local to web app - E.g. IMT, NextGen have individual instances
        propertyFinders.add(new PropertyFinder("firstlook"));
        propertyFinders.add(new PropertyFinder("firstlookMessages"));
        // Globally available as a single instance stored in Commons 
        // individual web apps need to include path in app classpath if they need to use this
        propertyFinders.add(new PropertyFinder("insight"));
    }
    return propertyFinders;
}

public static boolean getBooleanProperty( String key, boolean defaultValue )
{
    String value = getProperty(key);
    if ( value == null )
    {
        return defaultValue;
    } else
    {
        return Boolean.valueOf(value).booleanValue();
    }
}

public static Class<?> getClassProperty( String key, Class<?> defaultValue )
{
    String value = getProperty(key);
    if ( value == null ) {
        return defaultValue;
    } else
    {
        try
        {
            return Class.forName(value);
        } catch (ClassNotFoundException e)
        {
            return defaultValue;
        }
    }
}

public static double getDoubleProperty( String key, double defaultValue )
{
    String value = getProperty(key);
    if ( value == null )
    {
        return defaultValue;
    } else
    {
        try
        {
            return Double.valueOf(value).doubleValue();
        } catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }
}

public static float getFloatProperty( String key, float defaultValue )
{
    String value = getProperty(key);
    if ( value == null )
    {
        return defaultValue;
    } else
    {
        try
        {
            return Float.valueOf(value).floatValue();
        } catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }
}

public static int getIntProperty( String key, int defaultValue )
{
    String value = getProperty(key);
    if ( value == null )
    {
        return defaultValue;
    } else
    {
        try
        {
            return Integer.parseInt(value);
        } catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }
}

public static long getLongProperty( String key, long defaultValue )
{
    String value = getProperty(key);
    if ( value == null )
    {
        return defaultValue;
    } else
    {
        try
        {
            return Long.parseLong(value);
        } catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }
}

public static String getProperty( String key, String defaultValue )
{
    String value = getProperty(key);
    if ( value == null )
    {
        return defaultValue;
    } else
    {
        return value;
    }
}

public static String getProperty( String key )
{
    Iterator<IPropertyFinder> propFinders = getPropertyFinders().iterator();
    while (propFinders.hasNext())
    {
        IPropertyFinder finder = propFinders.next();
        String property = finder.getProperty(key);
        if ( property != null ) {
            return property.toString();
        }
    }
    return null;
}

public synchronized static void reset()
{
    Iterator<IPropertyFinder> propFinders = getPropertyFinders().iterator();
    while (propFinders.hasNext())
    {
        IPropertyFinder finder = propFinders.next();
        finder.reset();
    }
}

public static void setPropertyFinders( Collection<IPropertyFinder> propFinders )
{
    propertyFinders = propFinders;
}

}