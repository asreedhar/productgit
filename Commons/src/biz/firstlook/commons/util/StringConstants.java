package biz.firstlook.commons.util;

public final class StringConstants {
	
	/**
	 * The system new line character. 
	 */
	public final static String NEW_LINE = System.getProperty("line.separator");
	
	/**
	 * Private constructor to enforce static class usage.
	 */
	private StringConstants() {}
}
