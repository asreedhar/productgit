package biz.firstlook.commons.util;

public interface IPropertyFinder
{
public abstract String getProperty( String key );

public abstract void reset();
}