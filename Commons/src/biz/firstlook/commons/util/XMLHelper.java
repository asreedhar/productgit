package biz.firstlook.commons.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.NodeSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLHelper
{

public XMLHelper()
{
	super();
}

/**
 * Returns an ArrayList of Nodes in the parentNode which match the nodeName
 * 
 * @param parentNode
 * @param nodeName
 * @return
 */
public static ArrayList<Node> getChildren( Node parentNode, String nodeName )
{
	ArrayList<Node> result = new ArrayList<Node>();
    Node child;
    NodeList children = parentNode.getChildNodes();
    for (int count = 0; count < children.getLength(); count++)
    {
        child = children.item(count);
        if (child.getNodeName().equals(nodeName))
        {
        	result.add(child);
        }
    }
    return result; 
} 

public static Node getChild( Node parentNode, String nodeName )
{
    Node child;
    Node target = null;

    NodeList children = parentNode.getChildNodes();
    for (int count = 0; count < children.getLength(); count++)
    {
        child = children.item(count);
        if (child.getNodeName().equals(nodeName))
        {
            target = child;
            break;
        }
    }

    return target;
}

public static Element createTag( Node parent, String tagName, String value, Document doc )
{
   Element element = doc.createElement(tagName);
   element.appendChild(doc.createTextNode(value));
   parent.appendChild(element);
   return element;
}

public static String getAttribute( Node node, String attName, Document doc )
{
    NamedNodeMap nodeMap = node.getAttributes();
    if (nodeMap != null)
    {
        Node attribute = nodeMap.getNamedItem(attName);
        if (attribute != null)
            return attribute.getNodeValue();
        else
            return null;
    }
    else
        return null;
}

public static String serializeXml( Document doc ) throws IOException
{
    StringWriter sw = new StringWriter();

    OutputFormat of = new OutputFormat();
    of.setIndent(2);
    of.setPreserveSpace(false);
    XMLSerializer serxml = new XMLSerializer(of);
    serxml.setOutputCharStream(sw);
    serxml.serialize(doc);
    
    return sw.toString();
}

public static NodeList getChildren( Node parentNode, String tagName, Document doc )
{
  NodeList children = parentNode.getChildNodes();
  NodeSet filtered = new NodeSet();
  Node node;
  Element child;

  for (int count = 0; count < children.getLength(); count++)
  {
      node = children.item(count);
      if (node instanceof Element)
      {
          child = (Element)node;
          if (child.getTagName().equals(tagName))
            filtered.addElement(child);
      }
  }

  return filtered;
}

}
