package biz.firstlook.commons.util;


public class SystemPropertyFinder implements IPropertyFinder
{

public String getProperty( String key )
{
    return System.getProperty(key);
}

public void reset()
{
    // do nothing
}

}
