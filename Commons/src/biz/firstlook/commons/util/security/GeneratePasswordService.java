package biz.firstlook.commons.util.security;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import biz.firstlook.commons.util.IOUtils;

public class GeneratePasswordService {
    
    Logger log = Logger.getLogger( GeneratePasswordService.class );
    
    public GeneratePasswordService() {
    }
    
    public String generate() {
        List<String> tokens = gatherTokens();
        
        String password = "";
        
        Collections.shuffle( tokens );
        
        password = password + tokens.get(0);
        password = password + tokens.get(1);
        
        password = password.replace( 'o', '0' );
        password = password.replace( 'i', '1' );
        password = password.replace( 'a', '@' );
        password = password.replace( 'e', '3' );
        password = password.replace( 'u', '0' );
            
        return password;
    }

    private List<String> gatherTokens() {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy( GeneratePasswordService.class.getResourceAsStream("wordlist.txt"),
                          writer );
        } catch (IOException e) {

        }
        String content = writer.toString();
        String[] array = StringUtils.split( content, " \t\n\r" );
        List<String> tokens = Arrays.asList( array );
        return tokens;
    }
    
}
