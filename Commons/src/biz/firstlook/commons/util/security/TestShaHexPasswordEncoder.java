package biz.firstlook.commons.util.security;

import junit.framework.TestCase;

public class TestShaHexPasswordEncoder extends TestCase
{

private PasswordEncoder encoder;

public TestShaHexPasswordEncoder( String name )
{
    super(name);
}

public void setUp()
{   
    encoder = new ShaHexPasswordEncoder();
}

public void testDigest() throws Exception
{
    String toBeDigested = "test123";
    String expectedDigest = "7288edd0fc3ffcbe93a0cf06e3568e28521687bc";

    String digested = encoder.encode(toBeDigested);
    assertEquals(expectedDigest, digested);
    
    toBeDigested = "abc123";
    expectedDigest = "6367c48dd193d56ea7b0baad25b19455e529f5ee";
    
    digested = encoder.encode(toBeDigested);
    assertEquals(expectedDigest, digested);
}

}
