package biz.firstlook.commons.util.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;


/**
 * Encodes a password by using the SHA algorithm and then Hex encoding it.
 * @author bfung
 */
public class ShaHexPasswordEncoder implements PasswordEncoder
{
private static Logger logger = Logger.getLogger( ShaHexPasswordEncoder.class );

private static final String MISSING_HASH = "Missing Cryptographic Hash Algorithm: ";
private static final String HASH_ALGORITHM = "SHA";

public String encode( String plainTextPassword )
{
	if ( plainTextPassword != null )
	{
		try
		{
			MessageDigest digester = MessageDigest.getInstance( HASH_ALGORITHM );
			return new String( Hex.encodeHex( digester.digest( plainTextPassword.getBytes() ) ) );
		}
		catch ( NoSuchAlgorithmException e )
		{
			logger.fatal( MISSING_HASH + HASH_ALGORITHM );
		}
	}
	return null;
}

}
