package biz.firstlook.commons.util.security;


public interface PasswordEncoder
{

public String encode( String plainTextPassword );

}