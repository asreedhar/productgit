package biz.firstlook.commons.util;

public class InvalidVinException extends Exception
{

private static final long serialVersionUID = 9196209356718790725L;

public InvalidVinException()
{
	super();
}

public InvalidVinException( String arg0 )
{
	super( arg0 );
}

public InvalidVinException( Throwable arg0 )
{
	super( arg0 );
}

public InvalidVinException( String arg0, Throwable arg1 )
{
	super( arg0, arg1 );
}

}
