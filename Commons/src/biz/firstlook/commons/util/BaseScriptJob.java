package biz.firstlook.commons.util;

import jargs.gnu.CmdLineParser;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class BaseScriptJob
{
private long startTime;
private long endTime;

private OptionsParser options = null;

protected static class OptionsParser extends CmdLineParser
{
public static final Option OPT_REPORT_TYPE = new CmdLineParser.Option.StringOption( 'r', "reporttype" );
public static final Option OPT_REPORTSERVER = new CmdLineParser.Option.StringOption( 's', "reportserver" );
public static final Option OPT_PASSWORD = new CmdLineParser.Option.StringOption( 'p', "password" );
public static final Option OPT_FILENAME = new CmdLineParser.Option.StringOption( 'f', "filename" );
public static final Option OPT_FULLBBMODE = new CmdLineParser.Option.BooleanOption( 'b', "fullbook" );
public static final Option OPT_DAILYMODE = new CmdLineParser.Option.BooleanOption( 'x', "daily" );
public static final Option OPT_DEALERGROUPID = new CmdLineParser.Option.StringOption( 'd', "dealerGroupId" );
public static final Option OPT_TYPE = new CmdLineParser.Option.BooleanOption( 't', "type" );
public static final Option OPT_NUMBER_OF_DAYS = new CmdLineParser.Option.StringOption( 'n', "numberOfDays" );
public static final Option OPT_USERNAME = new CmdLineParser.Option.StringOption( 'u', "username" );
public static final Option OPT_CLEAR_TABLE = new CmdLineParser.Option.BooleanOption( 'c', "clear" );
public static final Option OPT_REPORT_PASSWORD = new CmdLineParser.Option.StringOption( 'q', "reportpassword" );
public static final Option OPT_WEEKLYMODE = new CmdLineParser.Option.BooleanOption( 'w', "weekly" );
public static final Option OPT_LOGGINGMODE = new CmdLineParser.Option.StringOption( 'o', "level" );
public static final Option OPT_OVERWRITECURRENTSUMMARYMODE = new CmdLineParser.Option.BooleanOption( 'v', "overwritecurrentsummary" );
public static final Option OPT_ALL = new CmdLineParser.Option.BooleanOption( 'a', "all" );

public OptionsParser()
{
	super();
	addOption( OPT_REPORT_TYPE );
	addOption( OPT_REPORTSERVER );
	addOption( OPT_PASSWORD );
	addOption( OPT_FILENAME );
	addOption( OPT_FULLBBMODE );
	addOption( OPT_DAILYMODE );
	addOption( OPT_DEALERGROUPID );
	addOption( OPT_TYPE );
	addOption( OPT_NUMBER_OF_DAYS );
	addOption( OPT_USERNAME );
	addOption( OPT_CLEAR_TABLE );
	addOption( OPT_REPORT_PASSWORD );
	addOption( OPT_WEEKLYMODE );
	addOption( OPT_LOGGINGMODE );
	addOption( OPT_OVERWRITECURRENTSUMMARYMODE );
	addOption( OPT_ALL );
}

public String getStringOption( Option o )
{
	return getStringOption( o, true, null );
}

public String getStringOption( Option o, String defaultValue )
{
	return getStringOption( o, false, defaultValue );
}

public String getStringOption( Option o, boolean isRequired, String defaultValue )
{
	Object object = getOptionValue( o );
	if ( object != null )
	{
		return (String)object;
	}
	else
	{
		if ( isRequired )
		{
			throw new IllegalArgumentException( "argument -" + o.shortForm() + "/--" + o.longForm() + " expected and not found" );
		}
		else
		{
			return defaultValue;
		}
	}
}

public boolean getBooleanOption( Option o )
{
	Object object = getOptionValue( o );
	if ( object != null )
	{
		return true;
	}
	else
	{
		return false;
	}
}
}

protected OptionsParser createOptionsParser( String args[] )
{
	try
	{
		options = new OptionsParser();
		options.parse( args );
		handleBaseOptions();
		return options;
	}
	catch ( CmdLineParser.UnknownOptionException e )
	{
		throw new IllegalArgumentException( e.getMessage() );
	}
	catch ( CmdLineParser.IllegalOptionValueException e )
	{
		throw new IllegalArgumentException( e.getMessage() );
	}
}

public void digestArguments()
{

}

public void executeJob( String[] args )
{
	try
	{
		createOptionsParser( args );
		markStartOfJob();
		System.out.println( "batch job started at " + formatTime( getStartTime() ) );
		digestArguments();
		start();
	}
	catch ( IllegalArgumentException ae )
	{
		System.out.println( "Illegal Argument: " + ae.getMessage() );
		printUsage();
		System.exit( 1 );
	}
	catch ( Throwable th )
	{
		th.printStackTrace();
		System.out.println( "Fatal Error: " + th.getMessage() );
		System.exit( 1 );
	}
	finally
	{
		markEndOfJob();
		System.out.println( getJobStatus() );
		System.out.println( "batch job finshed at: " + formatTime( getEndTime() ) );
		System.out.println( "total running time: " + getFormattedRuntime() );
	}
	
	System.exit( 0 );
}

protected String formatRuntime( long runTime )
{
	return ( runTime / 1000 / 60 ) + "m" + ( runTime / 1000 % 60 ) + "s";
}

protected String formatTime( long time )
{
	SimpleDateFormat format = new SimpleDateFormat( "hh:mm:ssa" );
	return format.format( new Date( time ) );
}

public long getEndTime()
{
	return endTime;
}

protected String getFormattedRuntime()
{
	return formatRuntime( endTime - startTime );
}

protected abstract String getJobStatus();

protected OptionsParser getOptions()
{
	return options;
}

public long getStartTime()
{
	return startTime;
}

protected abstract String getUsage();

private void handleBaseOptions()
{
	String overideDatabasePassword = options.getStringOption( OptionsParser.OPT_PASSWORD, null );
	if ( overideDatabasePassword != null )
	{
		System.setProperty( "firstlook.database.password", overideDatabasePassword );
	}
}

protected void markEndOfJob()
{
	endTime = System.currentTimeMillis();
}

protected void markStartOfJob()
{
	startTime = System.currentTimeMillis();
}

public String parseContext( String url )
{
	if ( url != null )
	{
		int startOfContext = url.lastIndexOf( "/" );
		if ( startOfContext == -1 )
		{
			return "";
		}
		return url.substring( startOfContext + 1, url.length() );
	}
	return null;
}

public String parseHost( String url )
{
	if ( url != null )
	{
		int endOfHost = url.indexOf( ":" );
		return url.substring( 0, endOfHost );
	}
	return null;
}

public String parsePort( String url )
{
	if ( url != null )
	{
		int endOfHost = url.indexOf( ":" );
		int startOfContext = url.indexOf( "/" );
		if ( startOfContext == -1 )
		{
			startOfContext = url.length();
		}
		return url.substring( endOfHost + 1, startOfContext );
	}
	return null;
}

private void printUsage()
{
	System.out.println( "format command <mandatory argument> [optional argument]" );
	System.out.println( getUsage() );
}

public abstract void start() throws Throwable;

}