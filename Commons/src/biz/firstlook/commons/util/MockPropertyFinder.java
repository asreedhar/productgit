package biz.firstlook.commons.util;

import java.util.HashMap;
import java.util.Map;


public class MockPropertyFinder implements IPropertyFinder
{

private Map<String, String> stringProps = new HashMap<String, String>();

public MockPropertyFinder()
{
    super();
}

public String getProperty( String key )
{
    String value = (String) stringProps.get(key);
    if ( value == null )
    {
        return "";
    } else
    {
        return value;
    }
}

public void setStringProperty( String key, String property )
{
    stringProps.put(key, property);
}

public void reset() {
	stringProps.clear();
}

}
