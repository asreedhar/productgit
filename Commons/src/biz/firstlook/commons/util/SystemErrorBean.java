package biz.firstlook.commons.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *	This is a bean which stores all information about a logged system error 
 * 
 * @author dweintrop
 */
public class SystemErrorBean
{
private Throwable error;
private Integer businessUnitId;
private Integer memberId;
private String product;
private String httpSessionId;
private String halSessionId;
private String source;

public SystemErrorBean( Throwable error, Integer businessUnitId, Integer memberId, String product, String httpSessionId, String halSessionId, String source )
{
	super();
	this.error = error;
	this.businessUnitId = businessUnitId;
	this.memberId = memberId;
	this.product = product;
	this.httpSessionId = httpSessionId;
	this.halSessionId = halSessionId;
	this.source = source;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}
public String getHttpSessionId()
{
	return httpSessionId;
}
public Integer getMemberId()
{
	return memberId;
}
public String getProduct()
{
	return product;
}
public String getSource()
{
	return source;
}
public String getHalSessionID()
{
	return halSessionId;
}
public Throwable getException(){
	return error;
}
public String getStackTrace()
{
	StringWriter writer = new StringWriter();
	error.printStackTrace(new PrintWriter(writer));
	String stackTrace = writer.toString().replace("\n", "||");
	if (stackTrace.length() > 8000) {
		stackTrace = stackTrace.substring(0, 7999);
	}
	return stackTrace;
}

public String toString()
{
	return getContext() + "|| \n TRACE: ||" + getStackTrace();
}

public String getContext()
{
	StringBuilder msg = new StringBuilder();
	msg.append( source );
	msg.append( ", memberId: " ).append( memberId );
	msg.append( ", businessUnitId: " ).append( businessUnitId );
	msg.append( ", product: " ).append( product );
	msg.append( ", halSessionId: " ).append( halSessionId);
	msg.append( ", HttpSessionID: " ).append( httpSessionId );
	return msg.toString();
}

}
