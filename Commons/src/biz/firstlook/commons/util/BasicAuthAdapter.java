package biz.firstlook.commons.util;

import java.io.Serializable;

import jodd.util.Base64;
import org.apache.commons.lang.StringUtils;
public class BasicAuthAdapter implements Serializable {
	private static final long serialVersionUID = -3610313499698397147L;
	public static final String NAME_IN_SESSION = ".basicAuthString.";
	String basicAuthString = "";
	public BasicAuthAdapter(String user, String sha1Pswd){
		if (StringUtils.isEmpty(user))
			throw new IllegalArgumentException("User may not be null.");
		if (StringUtils.isEmpty(sha1Pswd))
			throw new IllegalArgumentException("Password may not be null.");
		basicAuthString = Base64.encodeToString(user + ":" + sha1Pswd);
	}
	
	public String basicAuthString(){
		return basicAuthString;
	}
};
