package biz.firstlook.commons.ref;

import java.lang.ref.ReferenceQueue;

import org.apache.log4j.Logger;

public class GhostReferenceReaper<T extends Disposable> extends Thread {
	private final ReferenceQueue<T> queue;
	public GhostReferenceReaper(ReferenceQueue<T> queue) {
		this.queue = queue;
		setDaemon(true);
		start();
	}
	@SuppressWarnings("unchecked")
	public void run() {
		while (true) {
			GhostReference<T> ref = null;
			try {
				ref = (GhostReference<T>) queue.remove();
				T item = ref.getReferent();
				if (item != null) {
					item.dispose();
				}
				else {
					Logger.getLogger(GhostReferenceReaper.class).error("Referent list was null!");
				}
			}
			catch (Throwable e) {
				Logger.getLogger(GhostReferenceReaper.class).error("Error whilst disposing GhostReference", e);
			}
			finally {
				if (ref != null) {
					ref.clear();
				}
			}
		}
	}
}
