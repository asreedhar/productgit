package biz.firstlook.commons.ref;

/**
 * A <code>Disposable</code> is an object that has a public finalizer.
 *  
 * @author swenmouth
 */
public interface Disposable {
	public void dispose() throws Throwable;
}
