package biz.firstlook.commons.ref;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Phantom references are enqueued in the reference queue <em>before</em> their referent is cleared.
 * Extending the Phantom reference and using reflection to expose the referent we get a last chance
 * to clean-up the referent.
 * 
 * As is the case with all PhantomReference objects, you have to call {@link #clear} on the reference
 * once you have completed working with it.
 * 
 * @author swenmouth
 *
 * @param <X> the referent type
 */
public class GhostReference<X> extends PhantomReference<X> {

	private static final Collection<GhostReference<?>> currentRefs = Collections.synchronizedSet(new HashSet<GhostReference<?>>());
	private static final Field referent;

	static {
		try {
			referent = Reference.class.getDeclaredField("referent");
			referent.setAccessible(true);
		}
		catch (NoSuchFieldException e) {
			throw new RuntimeException("Field \"referent\" not found");
		}
	}

	public GhostReference(X referent, ReferenceQueue<? super X> queue) {
		super(referent, queue);
		currentRefs.add(this);
	}

	public void clear() {
		currentRefs.remove(this);
		super.clear();
	}

	@SuppressWarnings("unchecked")
	public X getReferent() {
		try {
			return (X) referent.get(this);
		}
		catch (IllegalAccessException e) {
			throw new IllegalStateException("referent should be accessible!");
		}
	}
}
