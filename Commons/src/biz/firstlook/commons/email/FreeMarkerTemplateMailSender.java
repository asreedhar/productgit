package biz.firstlook.commons.email;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import biz.firstlook.commons.email.EmailService.EmailFormat;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreeMarkerTemplateMailSender implements TemplateMailSender
{

private static final Logger logger = Logger.getLogger( FreeMarkerTemplateMailSender.class );

private JavaMailSender javaMailSender;
private FreeMarkerConfigurer freeMarkerConfigurer;

private String from;

public void mail( final String emailAddress, final List<String> ccEmailAddresses, final Map context, final String templateName, 
                  final String subject, final String replyTo, final Map< String, Resource > resources, 
                  final Map< String, String > resourceContentTypes, final EmailFormat emailFormat )
		throws IOException, MessagingException
{
	MimeMessagePreparator preparator = new MimeMessagePreparator()
	{
		public void prepare( MimeMessage mimeMessage ) throws MessagingException, IOException
		{
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper( mimeMessage, true, "UTF-8" );
			mimeMessageHelper.setTo( emailAddress );
			mimeMessageHelper.setCc( ccEmailAddresses.toArray( new String[ccEmailAddresses.size()] ) );
			mimeMessageHelper.setFrom( from );
			mimeMessageHelper.setSubject( subject );
			if ( replyTo != null)
				mimeMessageHelper.setReplyTo( replyTo );

			Template textTemplate = freeMarkerConfigurer.getConfiguration().getTemplate( templateName + "-text.ftl" );
			final StringWriter textWriter = new StringWriter();
			try
			{
				textTemplate.process( context, textWriter );
			}
			catch ( TemplateException e )
			{
				throw new MailPreparationException( "Can't generate text subscription mail", e );
			}

			if ( EmailFormat.HTML_EXTERNAL_LINKS.equals( emailFormat ) || EmailFormat.HTML_MULTIPART.equals( emailFormat ))
			{
				Template htmlTemplate = freeMarkerConfigurer.getConfiguration().getTemplate( templateName + "-html.ftl" );
				final StringWriter htmlWriter = new StringWriter();
				try
				{
					htmlTemplate.process( context, htmlWriter );
				}
				catch ( TemplateException e )
				{
					throw new MailPreparationException( "Can't generate HTML subscription mail", e );
				}

				mimeMessageHelper.setText( textWriter.toString(), htmlWriter.toString() );
				
				//Must set resources after the message.
				if( EmailFormat.HTML_MULTIPART.equals( emailFormat ) )
				{
					if ( resources != null )
					{
						for ( String resourceKey : resources.keySet() )
						{
                            if (resourceKey.equalsIgnoreCase("pdf")) { //If it is a pdf we want to attach it not inline.
                                FileSystemResource resource = (FileSystemResource) resources.get(resourceKey);
                                mimeMessageHelper.addAttachment("ActionPlan.pdf", resource.getFile());
                            }
                            if (resourceKey.equalsIgnoreCase("CustOfferPdf")) { //If it is a pdf we want to attach it not inline.
                                Resource resource = resources.get(resourceKey);
                                try{
                                	mimeMessageHelper.addAttachment("Offer.pdf",resource);
                                }
                                catch(Exception e){
                                	e.printStackTrace();
                                }
                            }
                            else {
                                mimeMessageHelper.addInline( resourceKey, resources.get( resourceKey ), resourceContentTypes.get( resourceKey ) ); 
                            }
						}
					}
				}
			}
			else
			{
				mimeMessageHelper.setText( textWriter.toString(), false );
			}
		}
	};

	if ( logger.isInfoEnabled() )
		logger.info( "Attempting to send " + templateName + " to " + emailAddress );

	javaMailSender.send( preparator );
}

public void setJavaMailSender( JavaMailSender mailSender )
{
	this.javaMailSender = mailSender;
}

public void setFreeMarkerConfigurer( FreeMarkerConfigurer freeMarkerConfigurer )
{
	this.freeMarkerConfigurer = freeMarkerConfigurer;
}

public String getFrom()
{
	return from;
}

public void setFrom( String from )
{
	this.from = from;
}

}
