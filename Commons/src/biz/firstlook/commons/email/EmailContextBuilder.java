package biz.firstlook.commons.email;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailService.EmailFormat;

/**
 * This class is used in EmailService as a value object.  Probably should be an interface?
 * @author bfung
 *
 */
public interface EmailContextBuilder
{
public abstract Map<String, Map< String, Object > > getRecipientsAndContext();
public abstract String getSubject();
public abstract String getReplyTo();
public abstract String getTemplateName();
public abstract Map<String, EmailFormat> getEmailFormats();
public abstract Map<String, Resource> getEmbeddedResources();
public abstract Map<String, String> getEmbeddedResourcesContentTypes();
public abstract List< String > getCcEmailAddresses();
}
