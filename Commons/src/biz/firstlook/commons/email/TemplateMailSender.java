package biz.firstlook.commons.email;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailService.EmailFormat;

public interface TemplateMailSender
{

/**
 * Send a mail with both a text and a HTML version.
 * 
 * @param emailAddress
 *            the email address where to send the email
 * @param context
 *            a {@link Map} of objects to expose to the template engine
 * @param replyTo
 *            an optional string that defines what address reply's to that e-mail get sent to
 * @param templateName
 *            the template name to use. The files templateName-text.ftl and templateName-html.ftl must exist.
 * @param emailMimeType TODO
 */
void mail( String emailAddress, List<String> ccEmailAddresses, Map context, String templateName, String subject, final String replyTo, Map<String, Resource> resources, Map<String, String> resourceContentType, EmailFormat emailMimeType ) throws IOException, MessagingException;

}
