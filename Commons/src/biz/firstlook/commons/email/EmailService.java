package biz.firstlook.commons.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;

/**
 * A framework for sending emails based on FreeMarker Templates.
 * 
 * An improvement to to this service would be turning the threading into a producer-consumer/queuing emailer.
 * 
 * @author bfung
 * 
 */
public class EmailService
{

private static final Logger logger = Logger.getLogger( EmailService.class );

public enum EmailFormat
{
	PLAIN_TEXT, HTML_MULTIPART, HTML_EXTERNAL_LINKS
}

/**
 * A template mail sender. Supposedly this can work with Apache Velocity as well.
 */
private TemplateMailSender templateMailSender;

/**
 * Takes an object as arugment to help in contructing a email context per email recipient.
 * 
 * @param emailContextBuilder
 */
public void sendEmail( final EmailContextBuilder emailContextBuilder )
{
	massEmail( emailContextBuilder.getRecipientsAndContext(), emailContextBuilder.getCcEmailAddresses(), emailContextBuilder.getTemplateName(), 
	           emailContextBuilder.getSubject(), emailContextBuilder.getReplyTo(), emailContextBuilder.getEmbeddedResources(), 
				emailContextBuilder.getEmbeddedResourcesContentTypes(), emailContextBuilder.getEmailFormats() );
}

/**
 * Send an email by simply specifying the fields directly, often used with simple messages. This method creates a new Thread and sends the mail
 * asynchronously to avoid user wait time.
 * 
 * @param emailAddress
 * @param emailContext
 * @param templateName
 * @param subject
 * @param subject
 * @param resources
 *            a map of resource names to resource paths to embed into the message.
 * @param emailMimetype
 */
public void sendEmail( final String emailAddress, final List<String> ccEmailAddresses, final Map emailContext, final String templateName, final String subject, final String replyTo,
						final Map< String, Resource > resources, final Map< String, String > resourceContentTypes,
						final EmailFormat emailMimetype )
{

	Thread mailSender = new Thread()
	{
		public void run()
		{
			try
			{
				templateMailSender.mail( emailAddress, ccEmailAddresses, emailContext, templateName, subject, replyTo, resources, resourceContentTypes, emailMimetype );
				if( logger.isInfoEnabled() )
					logger.info( new StringBuilder( "Email: " ).append( subject ).append( "sent successfully to " ).append( emailAddress ).toString() );
			}
			catch ( Exception ex )
			{
				logger.warn( new StringBuilder( "Failed Email(").append( subject ).append( "): " ).append( emailAddress ).toString());
				logger.warn( ex );
			}
		}
	};
	mailSender.start();
}

/**
 * Send an email by simply specifying the fields directly, often used with simple messages. This method creates a new Thread and sends the mail
 * 
 * @param emailAddress
 * @param emailContext
 * @param templateName
 * @param subject
 * @param subject
 * @param resources
 *            a map of resource names to resource paths to embed into the message.
 * @param emailMimetype
 */
public boolean sendSynchEmail(final String emailAddress, final List<String> ccEmailAddresses, final Map emailContext, final String templateName, 
        final String subject, final String replyTo, final Map<String, Resource> resources, 
        final Map<String, String> resourceContentTypes,final EmailFormat emailMimetype) {
    try {
        templateMailSender.mail(emailAddress, ccEmailAddresses, emailContext, templateName, subject, replyTo, resources, resourceContentTypes,emailMimetype);
        if (logger.isInfoEnabled())
            logger.info(new StringBuilder("Email: ").append(subject).append("sent successfully to ").append(emailAddress).toString());
        return true;
    } catch (Exception ex) {
    	ex.printStackTrace();
        logger.warn(new StringBuilder("Failed Email(").append(subject).append("): ").append(emailAddress).toString());
        logger.warn(ex);
        return false;
    }
}
    

/**
 * An extension of sendEmail. The email address and email context is associated
 * with a Map. This method creates a new Thread and sends emails asynchronously
 * to avoid user wait time.
 * 
 * @param addressAndContext
 * @param templateName
 * @param subject
 * @param resources
 *            a map of resource names to resource paths to embed into the
 *            message.
 * @param emailMimeType
 */
public void massEmail( final Map< String, Map< String, Object > > addressAndContext, final List<String> ccEmailAddresses, final String templateName, final String subject, final String replyTo,
                       final Map<String, Resource> resources, final Map<String, String> resourceContentTypes, final Map<String, EmailFormat> emailMimeType )
{
	Thread mailSender = new Thread()
	{
		public void run()
		{
			int successful = 0;
			int total = addressAndContext.keySet().size();
			List<String> failedEmails = new ArrayList<String>(); 
			for ( String address : addressAndContext.keySet() )
			{
				try
				{
					templateMailSender.mail( address, ccEmailAddresses, addressAndContext.get( address ), templateName, subject, replyTo, resources, resourceContentTypes, emailMimeType.get( address ) );
					if( logger.isInfoEnabled() )
						logger.info( new StringBuffer( "Email: ").append( subject ).append( " sent successfully to " ).append( address ).toString() );
					successful++;
				}
				catch( Exception ex )
				{
					failedEmails.add( address );
					logger.warn( ex );
				}
			}
			
			if( logger.isInfoEnabled() )
			{
				logger.info( new StringBuilder().append( successful ).append( " of " ).append( total ).append( " emails sent successfully.").toString() );
				logger.info( new StringBuilder().append( "Failed Emails(").append( subject ).append("): " ).append( failedEmails.toString() ) );
			}
		}
	};
	mailSender.start();
}

public void setTemplateMailSender( TemplateMailSender templateMailer )
{
	this.templateMailSender = templateMailer;
}

}
