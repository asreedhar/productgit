
package biz.firstlook.services.reprice.distribution;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for PriceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PriceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotDefined"/>
 *     &lt;enumeration value="Lot"/>
 *     &lt;enumeration value="Internet"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum PriceType {

    @XmlEnumValue("Internet")
    INTERNET("Internet"),
    @XmlEnumValue("Lot")
    LOT("Lot"),
    @XmlEnumValue("NotDefined")
    NOT_DEFINED("NotDefined");
    private final String value;

    PriceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PriceType fromValue(String v) {
        for (PriceType c: PriceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
