package biz.firstlook.services.reprice.distribution;

import java.io.StringWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.handler.AbstractHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;
import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class RequestLoggingHandler
    extends AbstractHandler
{
    private static final Logger log = Logger.getLogger(RequestLoggingHandler.class.getName());
    
    
    public void invoke(MessageContext context)
        throws Exception
    {
    
        Document doc = (Document) context.getCurrentMessage().getProperty(DOMOutHandler.DOM_MESSAGE);
        
        if (doc == null)
        {
            log.error("DOM Document was not found so the message could not be logged. " +
                    "Please add DOMInHandler/DOMOutHandler to your flow!");
            return;
        }
            
        StringWriter sw = new StringWriter();
        XMLSerializer ser = new XMLSerializer(sw, new OutputFormat(doc));
        ser.serialize(doc.getDocumentElement());
       
        if(log.isDebugEnabled()) {
        	String request = sw.toString().replaceAll("\n", "");
        	log.debug("Request XML to DistributorWebService: " + request);
        }
    }
}