
package biz.firstlook.services.reprice.distribution;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PublicationId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Providers" type="{http://services.firstlook.biz/Distribution/1.0/}ArrayOfProvider" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionDetails", propOrder = {
    "publicationId",
    "providers"
})
public class SubmissionDetails {

    @XmlElement(name = "PublicationId")
    protected int publicationId;
    @XmlElement(name = "Providers")
    protected ArrayOfProvider providers;

    /**
     * Gets the value of the publicationId property.
     * 
     */
    public int getPublicationId() {
        return publicationId;
    }

    /**
     * Sets the value of the publicationId property.
     * 
     */
    public void setPublicationId(int value) {
        this.publicationId = value;
    }

    /**
     * Gets the value of the providers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProvider }
     *     
     */
    public ArrayOfProvider getProviders() {
        return providers;
    }

    /**
     * Sets the value of the providers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProvider }
     *     
     */
    public void setProviders(ArrayOfProvider value) {
        this.providers = value;
    }

}
