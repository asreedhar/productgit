package biz.firstlook.services.reprice.distribution;

import java.lang.reflect.Proxy;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;



public class DistributorClientTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Protocol protocol = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
		//Protocol.registerProtocol("https", protocol);
		
		//protocol = new Protocol("http");
		
		DistributorClient distributorClient = new DistributorClient();
		DistributorSoap service = distributorClient.getDistributorSoap();
		
		Client client =  ((XFireProxy)
				Proxy.getInvocationHandler(service)).getClient();
		client.addInHandler(new DOMInHandler());
		
		Logger log = Logger.getLogger(DistributeDirectResponseHandler.class.getName());
	    //Log log = LogFactory.getLog(RequestLoggingHandler.class);
		log.addAppender(new ConsoleAppender(new PatternLayout("[%d][%5p](%c) - %m%n"),"System.out"));
		log.setLevel(Level.ALL);
		client.addInHandler(new DistributeDirectResponseHandler());
		
		client.addOutHandler(new DOMOutHandler());
		
		log = Logger.getLogger(RequestLoggingHandler.class.getName());
	    //Log log = LogFactory.getLog(RequestLoggingHandler.class);
		log.addAppender(new ConsoleAppender(new PatternLayout("[%d][%5p](%c) - %m%n"),"System.out"));
		log.setLevel(Level.ALL);
		
		client.addOutHandler(new RequestLoggingHandler());
		//client.setUrl("http://cclouston01.firstlook.biz:8088/Mock");
		client.setUrl("http://betaservices.firstlook.biz/Distribution/Services/V_1_0/Distributor.asmx");
		//client.setUrl("http://localhost:8088/mockDistributorSoap");
		
		Message message = new Message();
		
		Dealer dealer = new Dealer();
		dealer.setId(100509);
		
		message.setFrom(dealer);
		
		Vehicle subjectVehicle = new Vehicle();
		subjectVehicle.setId(0);
		subjectVehicle.setVehicleType(VehicleType.INVENTORY);
		
		message.setSubject(subjectVehicle);
		
		MessageBody messageBody = new MessageBody();
		
		ArrayOfBodyPart bodyParts = new ArrayOfBodyPart();
		
		Price price = new Price();
		
		price.setPriceType(PriceType.INTERNET);
		price.setValue(654321);
		
		bodyParts.getBodyPart().add(price);
		
		messageBody.setParts(bodyParts);
		
		message.setBody(messageBody);
		
		UserIdentity userIdentity = new UserIdentity();
		
		userIdentity.setUserName("jkoziol");
		
		SubmissionDetails data = service.distribute(message, userIdentity);
		
		
		System.out.println("finished: " + data);
	}
}
