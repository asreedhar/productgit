
package biz.firstlook.services.reprice.distribution;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

public class DistributorClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service1;

    public DistributorClient() {
        create1();
        Endpoint DistributorSoapEP = service1 .addEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), "http://betaservices.firstlook.biz/Distribution/Services/V_1_0/Distributor.asmx");
        endpoints.put(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), DistributorSoapEP);
        Endpoint DistributorSoapLocalEndpointEP = service1 .addEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint"), new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalBinding"), "xfire.local://Distributor");
        endpoints.put(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint"), DistributorSoapLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create1() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service1 = asf.create((DistributorSoap.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service1, new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service1, new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public DistributorSoap getDistributorSoap() {
        return ((DistributorSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap")));
    }

    public DistributorSoap getDistributorSoap(String url) {
        DistributorSoap var = getDistributorSoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public DistributorSoap getDistributorSoapLocalEndpoint() {
        return ((DistributorSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint")));
    }

    public DistributorSoap getDistributorSoapLocalEndpoint(String url) {
        DistributorSoap var = getDistributorSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
