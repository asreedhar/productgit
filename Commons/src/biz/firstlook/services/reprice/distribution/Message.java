
package biz.firstlook.services.reprice.distribution;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Message complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Message">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="From" type="{http://services.firstlook.biz/Distribution/1.0/}Dealer" minOccurs="0"/>
 *         &lt;element name="Subject" type="{http://services.firstlook.biz/Distribution/1.0/}Vehicle" minOccurs="0"/>
 *         &lt;element name="Body" type="{http://services.firstlook.biz/Distribution/1.0/}MessageBody" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Message", propOrder = {
    "from",
    "subject",
    "body"
})
public class Message {

    @XmlElement(name = "From", namespace="http://services.firstlook.biz/Distribution/1.0/")
    protected Dealer from;
    @XmlElement(name = "Subject", namespace="http://services.firstlook.biz/Distribution/1.0/")
    protected Vehicle subject;
    @XmlElement(name = "Body", namespace="http://services.firstlook.biz/Distribution/1.0/")
    protected MessageBody body;

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link Dealer }
     *     
     */
    public Dealer getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dealer }
     *     
     */
    public void setFrom(Dealer value) {
        this.from = value;
    }

    /**
     * Gets the value of the subject property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getSubject() {
        return subject;
    }

    /**
     * Sets the value of the subject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setSubject(Vehicle value) {
        this.subject = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link MessageBody }
     *     
     */
    public MessageBody getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageBody }
     *     
     */
    public void setBody(MessageBody value) {
        this.body = value;
    }

}
