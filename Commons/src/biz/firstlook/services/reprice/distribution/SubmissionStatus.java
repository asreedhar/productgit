
package biz.firstlook.services.reprice.distribution;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Provider" type="{http://services.firstlook.biz/Distribution/1.0/}Provider" minOccurs="0"/>
 *         &lt;element name="State" type="{http://services.firstlook.biz/Distribution/1.0/}SubmissionState"/>
 *         &lt;element name="Errors" type="{http://services.firstlook.biz/Distribution/1.0/}ArrayOfSubmissionError" minOccurs="0"/>
 *         &lt;element name="Successes" type="{http://services.firstlook.biz/Distribution/1.0/}ArrayOfSubmissionSuccess" minOccurs="0"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionStatus", propOrder = {
    "provider",
    "state",
    "errors",
    "successes",
    "message"
})
public class SubmissionStatus {

    @XmlElement(name = "Provider")
    protected Provider provider;
    @XmlElement(name = "State", required = true)
    protected SubmissionState state;
    @XmlElement(name = "Errors")
    protected ArrayOfSubmissionError errors;
    @XmlElement(name = "Successes")
    protected ArrayOfSubmissionSuccess successes;
    @XmlElement(name = "Message")
    protected String message;

    /**
     * Gets the value of the provider property.
     * 
     * @return
     *     possible object is
     *     {@link Provider }
     *     
     */
    public Provider getProvider() {
        return provider;
    }

    /**
     * Sets the value of the provider property.
     * 
     * @param value
     *     allowed object is
     *     {@link Provider }
     *     
     */
    public void setProvider(Provider value) {
        this.provider = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionState }
     *     
     */
    public SubmissionState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionState }
     *     
     */
    public void setState(SubmissionState value) {
        this.state = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionError }
     *     
     */
    public ArrayOfSubmissionError getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionError }
     *     
     */
    public void setErrors(ArrayOfSubmissionError value) {
        this.errors = value;
    }

    /**
     * Gets the value of the successes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionSuccess }
     *     
     */
    public ArrayOfSubmissionSuccess getSuccesses() {
        return successes;
    }

    /**
     * Sets the value of the successes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionSuccess }
     *     
     */
    public void setSuccesses(ArrayOfSubmissionSuccess value) {
        this.successes = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
