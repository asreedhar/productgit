package biz.firstlook.services.reprice.distribution;

import java.lang.reflect.Proxy;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;

import biz.firstlook.commons.util.PropertyFinder;

public class DistributorClientAdapter 
{
	public static void sendRepriceEventToWebService(Integer businessUnitId, Integer inventoryId, Integer newPrice, String userName)	{
		DistributorClient distributorClient = new DistributorClient();
		DistributorSoap service = distributorClient.getDistributorSoap();
		
		Client client = ((XFireProxy)Proxy.getInvocationHandler(service)).getClient();
		client.addInHandler(new DOMInHandler());
		
		Logger log = Logger.getLogger(DistributeDirectResponseHandler.class.getName());
		log.addAppender(new ConsoleAppender(new PatternLayout("[%d][%5p](%c) - %m%n"),"System.out"));
		//log.setLevel(Level.ALL);

		client.addInHandler(new DistributeDirectResponseHandler());

		client.addOutHandler(new DOMOutHandler());
		
		log = Logger.getLogger(RequestLoggingHandler.class.getName());
		log.addAppender(new ConsoleAppender(new PatternLayout("[%d][%5p](%c) - %m%n"),"System.out"));
		//log.setLevel(Level.ALL);
		
		client.addOutHandler(new RequestLoggingHandler());
		
	    PropertyFinder finder = new PropertyFinder( "Commons" );
	    String servername = null;
	    Context initialContext ;
		Context ctx;
		try {
			initialContext = new javax.naming.InitialContext(); 
			ctx = (Context) initialContext.lookup("java:comp/env");
			servername = (String) ctx.lookup("ServerName");
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    String serviceUrl = null;
    	if (servername.equalsIgnoreCase("prod"))
    		serviceUrl=finder.getProperty( "Prod.Distribution.Service.Url" );
		else if (servername.equalsIgnoreCase("beta"))
			serviceUrl=finder.getProperty( "Beta.Distribution.Service.Url" );
			else if (servername.equalsIgnoreCase("intb"))
				serviceUrl=finder.getProperty( "Intb.Distribution.Service.Url" );
				else if (servername.equalsIgnoreCase("dev"))
					serviceUrl=finder.getProperty( "Dev.Distribution.Service.Url" );    
    	
    	System.out.println("URL----"+servername+"-"+serviceUrl);
    	System.out.println("--------Calling Reprice Distribution Web Service---------");
    	if(serviceUrl==null)
    		serviceUrl=finder.getProperty( "Prod.Distribution.Service.Url" );
    	
		client.setUrl(serviceUrl);
		
		Message message = new Message();
		
		Dealer dealer = new Dealer();
		dealer.setId(businessUnitId);
		
		message.setFrom(dealer);
		
		Vehicle subjectVehicle = new Vehicle();
		subjectVehicle.setId(inventoryId);
		subjectVehicle.setVehicleType(VehicleType.INVENTORY);
		
		message.setSubject(subjectVehicle);
		
		MessageBody messageBody = new MessageBody();
		
		ArrayOfBodyPart bodyParts = new ArrayOfBodyPart();
		
		Price price = new Price();
		
		price.setPriceType(PriceType.INTERNET);
		price.setValue(newPrice);
		
		bodyParts.getBodyPart().add(price);
		
		messageBody.setParts(bodyParts);
		
		message.setBody(messageBody);
		
		UserIdentity userIdentity = new UserIdentity();
		
		userIdentity.setUserName(userName);
		
		SubmissionDetails data = service.distribute(message, userIdentity);
		System.out.println("--------Called Reprice Distribution Web Service----------");
		log.info("Reprice event sent to Distribution web service. Response publication ID = " + data.publicationId );
	}
}
