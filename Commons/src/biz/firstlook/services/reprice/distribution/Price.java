
package biz.firstlook.services.reprice.distribution;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Price complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Price">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.firstlook.biz/Distribution/1.0/}BodyPart">
 *       &lt;sequence>
 *         &lt;element name="PriceType" type="{http://services.firstlook.biz/Distribution/1.0/}PriceType"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Price", namespace="http://services.firstlook.biz/Distribution/1.0/", propOrder = {
    "priceType",
    "value"
})
public class Price
    extends BodyPart
{

    @XmlElement(name = "PriceType", namespace="http://services.firstlook.biz/Distribution/1.0/", required = true)
    protected PriceType priceType;
    @XmlElement(name = "Value", namespace="http://services.firstlook.biz/Distribution/1.0/")
    protected int value;

    /**
     * Gets the value of the priceType property.
     * 
     * @return
     *     possible object is
     *     {@link PriceType }
     *     
     */
    public PriceType getPriceType() {
        return priceType;
    }

    /**
     * Sets the value of the priceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceType }
     *     
     */
    public void setPriceType(PriceType value) {
        this.priceType = value;
    }

    /**
     * Gets the value of the value property.
     * 
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(int value) {
        this.value = value;
    }

}
