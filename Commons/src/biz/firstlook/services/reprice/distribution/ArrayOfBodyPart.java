
package biz.firstlook.services.reprice.distribution;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfBodyPart complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfBodyPart">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BodyPart" type="{http://services.firstlook.biz/Distribution/1.0/}BodyPart" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfBodyPart", propOrder = {
    "bodyPart"
})
public class ArrayOfBodyPart {

    @XmlElement(name = "BodyPart", namespace="http://services.firstlook.biz/Distribution/1.0/", required = true, nillable = true)
    protected List<BodyPart> bodyPart;

    /**
     * Gets the value of the bodyPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bodyPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBodyPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BodyPart }
     * 
     * 
     */
    public List<BodyPart> getBodyPart() {
        if (bodyPart == null) {
            bodyPart = new ArrayList<BodyPart>();
        }
        return this.bodyPart;
    }

}
