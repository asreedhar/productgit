
package biz.firstlook.services.reprice.distribution;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSubmissionSuccess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSubmissionSuccess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmissionSuccess" type="{http://services.firstlook.biz/Distribution/1.0/}SubmissionSuccess" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSubmissionSuccess", propOrder = {
    "submissionSuccess"
})
public class ArrayOfSubmissionSuccess {

    @XmlElement(name = "SubmissionSuccess", required = true, nillable = true)
    protected List<SubmissionSuccess> submissionSuccess;

    /**
     * Gets the value of the submissionSuccess property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the submissionSuccess property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubmissionSuccess().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubmissionSuccess }
     * 
     * 
     */
    public List<SubmissionSuccess> getSubmissionSuccess() {
        if (submissionSuccess == null) {
            submissionSuccess = new ArrayList<SubmissionSuccess>();
        }
        return this.submissionSuccess;
    }

}
