
package biz.firstlook.services.reprice.distribution;

import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.services.distribution._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfSubmissionStatus_QNAME = new QName("http://services.firstlook.biz/Distribution/1.0/", "ArrayOfSubmissionStatus");
    private final static QName _UserIdentity_QNAME = new QName("http://services.firstlook.biz/Distribution/1.0/", "UserIdentity");
    private final static QName _ArrayOfMessageTypesMessageTypes_QNAME = new QName("http://services.firstlook.biz/Distribution/1.0/", "MessageTypes");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.services.distribution._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link SubmissionStatus }
     * 
     */
    public SubmissionStatus createSubmissionStatus() {
        return new SubmissionStatus();
    }

    /**
     * Create an instance of {@link Provider }
     * 
     */
    public Provider createProvider() {
        return new Provider();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionSuccess }
     * 
     */
    public ArrayOfSubmissionSuccess createArrayOfSubmissionSuccess() {
        return new ArrayOfSubmissionSuccess();
    }

    /**
     * Create an instance of {@link SubmissionDetails }
     * 
     */
    public SubmissionDetails createSubmissionDetails() {
        return new SubmissionDetails();
    }

    /**
     * Create an instance of {@link SubmissionError }
     * 
     */
    public SubmissionError createSubmissionError() {
        return new SubmissionError();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionStatus }
     * 
     */
    public ArrayOfSubmissionStatus createArrayOfSubmissionStatus() {
        return new ArrayOfSubmissionStatus();
    }

    /**
     * Create an instance of {@link DistributeResponse }
     * 
     */
    public DistributeResponse createDistributeResponse() {
        return new DistributeResponse();
    }

    /**
     * Create an instance of {@link Price }
     * 
     */
    public Price createPrice() {
        return new Price();
    }

    /**
     * Create an instance of {@link UserIdentity }
     * 
     */
    public UserIdentity createUserIdentity() {
        return new UserIdentity();
    }

    /**
     * Create an instance of {@link Distribute }
     * 
     */
    public Distribute createDistribute() {
        return new Distribute();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionError }
     * 
     */
    public ArrayOfSubmissionError createArrayOfSubmissionError() {
        return new ArrayOfSubmissionError();
    }

    /**
     * Create an instance of {@link SubmissionSuccess }
     * 
     */
    public SubmissionSuccess createSubmissionSuccess() {
        return new SubmissionSuccess();
    }

    /**
     * Create an instance of {@link ArrayOfMessageTypes }
     * 
     */
    public ArrayOfMessageTypes createArrayOfMessageTypes() {
        return new ArrayOfMessageTypes();
    }

    /**
     * Create an instance of {@link Dealer }
     * 
     */
    public Dealer createDealer() {
        return new Dealer();
    }

    /**
     * Create an instance of {@link ArrayOfProvider }
     * 
     */
    public ArrayOfProvider createArrayOfProvider() {
        return new ArrayOfProvider();
    }

    /**
     * Create an instance of {@link ArrayOfBodyPart }
     * 
     */
    public ArrayOfBodyPart createArrayOfBodyPart() {
        return new ArrayOfBodyPart();
    }

    /**
     * Create an instance of {@link MessageBody }
     * 
     */
    public MessageBody createMessageBody() {
        return new MessageBody();
    }

    /**
     * Create an instance of {@link Vehicle }
     * 
     */
    public Vehicle createVehicle() {
        return new Vehicle();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSubmissionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.firstlook.biz/Distribution/1.0/", name = "ArrayOfSubmissionStatus")
    public JAXBElement<ArrayOfSubmissionStatus> createArrayOfSubmissionStatus(ArrayOfSubmissionStatus value) {
        return new JAXBElement<ArrayOfSubmissionStatus>(_ArrayOfSubmissionStatus_QNAME, ArrayOfSubmissionStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserIdentity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.firstlook.biz/Distribution/1.0/", name = "UserIdentity")
    public JAXBElement<UserIdentity> createUserIdentity(UserIdentity value) {
        return new JAXBElement<UserIdentity>(_UserIdentity_QNAME, UserIdentity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.firstlook.biz/Distribution/1.0/", name = "MessageTypes", scope = ArrayOfMessageTypes.class)
    public JAXBElement<List<String>> createArrayOfMessageTypesMessageTypes(List<String> value) {
        return new JAXBElement<List<String>>(_ArrayOfMessageTypesMessageTypes_QNAME, ((Class) List.class), ArrayOfMessageTypes.class, ((List<String> ) value));
    }

}
