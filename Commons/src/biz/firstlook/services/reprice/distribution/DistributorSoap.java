
package biz.firstlook.services.reprice.distribution;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
//import biz.firstlook.services.distribution._1.ArrayOfSubmissionStatus;
//import biz.firstlook.services.reprice.distribution.Message;
//import biz.firstlook.services.distribution._1.SubmissionDetails;
//import biz.firstlook.services.distribution._1.VehicleType;

@WebService(name = "DistributorSoap", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface DistributorSoap {


    @WebMethod(operationName = "Distribute", action = "http://services.firstlook.biz/Distribution/1.0/Distribute")
    @WebResult(name = "DistributeResult", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
    public SubmissionDetails distribute(
        @WebParam(name = "message", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
        Message message,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/", header = true)
        UserIdentity UserIdentity);
/*
    @WebMethod(operationName = "PublicationStatus", action = "http://services.firstlook.biz/Distribution/1.0/PublicationStatus")
    @WebResult(name = "PublicationStatusResult", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
    public ArrayOfSubmissionStatus publicationStatus(
        @WebParam(name = "publicationId", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
        int publicationId,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/", header = true)
        biz.firstlook.services.distribution._1.UserIdentity UserIdentity);

    @WebMethod(operationName = "VehicleStatus", action = "http://services.firstlook.biz/Distribution/1.0/VehicleStatus")
    @WebResult(name = "VehicleStatusResult", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
    public ArrayOfSubmissionStatus vehicleStatus(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
        int dealerId,
        @WebParam(name = "vehicleType", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
        VehicleType vehicleType,
        @WebParam(name = "vehicleId", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/")
        int vehicleId,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/", header = true)
        biz.firstlook.services.distribution._1.UserIdentity UserIdentity);
*/
}
