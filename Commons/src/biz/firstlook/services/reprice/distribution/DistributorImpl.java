
package biz.firstlook.services.reprice.distribution;

import javax.jws.WebService;
//import biz.firstlook.services.distribution._1.ArrayOfSubmissionStatus;
import biz.firstlook.services.reprice.distribution.Message;
import biz.firstlook.services.reprice.distribution.SubmissionDetails;
//import biz.firstlook.services.distribution._1.VehicleType;

@WebService(serviceName = "Distributor", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/", endpointInterface = "biz.firstlook.services.reprice.distribution.DistributorSoap")
public class DistributorImpl
    implements DistributorSoap
{


    public SubmissionDetails distribute(Message message, UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }
/*
    public ArrayOfSubmissionStatus publicationStatus(int publicationId, biz.firstlook.services.distribution._1.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public ArrayOfSubmissionStatus vehicleStatus(int dealerId, VehicleType vehicleType, int vehicleId, biz.firstlook.services.distribution._1.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }
*/
}
