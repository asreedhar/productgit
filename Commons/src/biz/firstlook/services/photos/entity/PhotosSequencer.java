package biz.firstlook.services.photos.entity;

import java.util.Comparator;

public class PhotosSequencer implements Comparator<PhotoListObjects>{

	public int compare(PhotoListObjects o1, PhotoListObjects o2) {
		
		Integer i1=(Integer.parseInt(o1.getsequenceNo()));
		Integer i2=(Integer.parseInt(o2.getsequenceNo()));
		return i1.compareTo(i2); 
	}
	
	
	
}
