package biz.firstlook.services.photos.entity;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class PhotosListResponse {

	@SerializedName("appraisalPhotos")
	List<PhotoListObjects> appraisalPhotos;

	@SerializedName("uniqueId")
	String uniqueId;
	
	
	
	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public List<PhotoListObjects> getAppraisalPhotos() {
		
		return appraisalPhotos; 
	}

	public void setAppraisalPhotos(List<PhotoListObjects> appraisalPhotos) {
		
		this.appraisalPhotos = appraisalPhotos;
	}
	
	
}
