package biz.firstlook.services.photos.entity;

import com.google.gson.annotations.SerializedName;

public class PhotoListObjects {

	@SerializedName("photoUrl")
	private String photoURl;
	
	@SerializedName("wide127Url")
	private String thumbUrl;
	
	@SerializedName("sequenceNo")
	private String sequenceNo;
	
	@SerializedName("source")
	private Integer source;
	
	
	
	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public Integer getSource() {
		return source;
	}
	public void setSource(Integer source) {
		this.source = source;
	}
	public String getsequenceNo() {
		return sequenceNo;
	}
	public void setsequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	
	public String getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}
	public String getPhotoURl() {
		return photoURl;
	}
	public void setPhotoURl(String photoURl) {
		this.photoURl = photoURl;
	}
	
	
	
	
	
	
	
	
}
