package biz.firstlook.services.photos.entity;

import com.google.gson.annotations.SerializedName;

public class PhotosUploadResponse {
	
	@SerializedName("photoKey")
	private String photoKey;
	
	
	@SerializedName("wide127Key")
	private String thumbnailKey;
	
	@SerializedName("photo")
	private String photo;
	
	
	@SerializedName("wide127Url")
	private String thumbnail;
	
	
	@SerializedName("uniqueId")
	String uniqueId;


	public String getPhotoKey() {
		return photoKey;
	}


	public void setPhotoKey(String photoKey) {
		this.photoKey = photoKey;
	}


	public String getThumbnailKey() {
		return thumbnailKey;
	}


	public void setThumbnailKey(String thumbnailKey) {
		this.thumbnailKey = thumbnailKey;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getThumbnail() {
		return thumbnail;
	}


	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}


	public String getUniqueId() {
		return uniqueId;
	}


	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	
	
	
	
}
