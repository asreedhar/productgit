package biz.firstlook.services.photos;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import biz.firstlook.services.photos.entity.PhotosListResponse;
import biz.firstlook.services.photos.entity.PhotosSequencer;
import biz.firstlook.services.photos.entity.PhotosUploadResponse;

import com.google.gson.Gson;


public class PhotosServiceClient
{
	static Logger log = Logger.getLogger(PhotosServiceClient.class);
	String urlBase;
	String basicAuthString;

	
	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

	public String getBasicAuthString() {
		return basicAuthString;
	}

	public void setBasicAuthString(String basicAuthString) {
		this.basicAuthString = basicAuthString;
	}

	// DefaultHttpClient client = null;
	
	public PhotosServiceClient(String basicAuthString) throws Exception 
	{
		try {
		     Context initialContext = new javax.naming.InitialContext(); 
		     Context ctx = (Context) initialContext.lookup("java:comp/env");
		     
		     this.urlBase = (String) ctx.lookup("flServiceHostUrlBase");
		}
		catch (NamingException e) 
		{
			 System.err.println(" PhotosServiceClient(String basicAuthString): Exception getting flserviceHostName from server.xml " + e.getMessage());
		     log.error("PhotosServiceClient(String basicAuthString): Exception getting flserviceHostName from server.xml", e);
		     throw new Exception("Could not retrieve FL service url base from server.xml - please update server.xml");
		}	
		
		this.basicAuthString = basicAuthString;
	}

	public PhotosServiceClient(String urlBase, String basicAuthString) {
		super();
		this.urlBase = urlBase;
		this.basicAuthString = basicAuthString;
	}

	public PhotosListResponse getPhotos(String dealerId, String appraisalId) throws Exception
	{
		String url = urlBase + "v1/appraisalsPhotos/dealers/"+dealerId+"/uniqueId/"+appraisalId+"/source/1";
		HttpGet request = new HttpGet(url);
		
		System.out.println("Copying Photos for Dealer-"+dealerId+", AppraisalId-"+appraisalId+", BAString-"+basicAuthString);

		
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		try 
		{
			httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				PhotosListResponse resp = gson.fromJson(reader, PhotosListResponse.class);
				Collections.sort(resp.getAppraisalPhotos(),new PhotosSequencer());
				return resp;
			}

			System.err.println("PhotosServiceClient.getPhotos() : Error getting Photos.  " + buildLogMsg(url, response));
			log.error("PhotosServiceClient.getPhotos() : Error getting Photos.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to retrieve Photos.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("PhotosServiceClient.getPhotos() : Exception getting Photos" + buildLogMsg(url, null) + e.getMessage());
			log.error("PhotosServiceClient.getPhotos() : Exception getting Photos" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public Boolean deletePhotos(String dealerId,String uniqueId,Integer sourceId, Integer sequence) throws Exception{
		
		String url=urlBase+"v1/appraisalsPhotosDelete/dealers/"+dealerId;
		HttpPost request = new HttpPost(url);
		
		System.out.println("Deleting Photos for Dealer-"+dealerId+", AppraisalId-"+uniqueId+", BAString-"+basicAuthString);

		
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		
		try 
		{
			httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> postParameters;
		   


		    postParameters = new ArrayList<NameValuePair>();
		    postParameters.add(new BasicNameValuePair("UniqueID", uniqueId));
		    postParameters.add(new BasicNameValuePair("Source", sourceId+""));
		    postParameters.add(new BasicNameValuePair("SequenceID", sequence+""));

		    request.setEntity(new UrlEncodedFormEntity(postParameters));
			
		    HttpResponse response = httpClient.execute(request);
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				return true;
			}

			System.err.println("PhotosServiceClient.getPhotos() : Error Deleting Photos.  " + buildLogMsg(url, response));
			log.error("PhotosServiceClient.getPhotos() : Error Deleting Photos.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to delete Photos.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("PhotosServiceClient.getPhotos() : Exception Deleting Photos" + buildLogMsg(url, null) + e.getMessage());
			log.error("PhotosServiceClient.getPhotos() : Exception Deleting Photos" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public PhotosListResponse copyPhotos(String dealerId,String uniqueId,Integer sourceId, String newUniqueID) throws Exception{
		
		String url=urlBase+"v1/appraisalCopyPhotos/dealers/"+dealerId;
		HttpPost request = new HttpPost(url);
		
		System.out.println("Copying Photos for Dealer-"+dealerId+", AppraisalId-"+uniqueId+", BAString-"+basicAuthString);

		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		
		try 
		{
			httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> postParameters;
		   


		    postParameters = new ArrayList<NameValuePair>();
		    postParameters.add(new BasicNameValuePair("UniqueID", uniqueId));
		    postParameters.add(new BasicNameValuePair("Source", sourceId+""));
		    postParameters.add(new BasicNameValuePair("NewUniqueID", newUniqueID));

		    request.setEntity(new UrlEncodedFormEntity(postParameters));
			
		    HttpResponse response = httpClient.execute(request);
		    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
		    	Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				PhotosListResponse resp = gson.fromJson(reader, PhotosListResponse.class);
				return resp;
			}

			System.err.println("PhotosServiceClient.getPhotos() : Error Copying Photos.  " + buildLogMsg(url, response));
			log.error("PhotosServiceClient.getPhotos() : Error Copying Photos.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to copy Photos.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("PhotosServiceClient.getPhotos() : Exception Copying Photos" + buildLogMsg(url, null) + e.getMessage());
			log.error("PhotosServiceClient.getPhotos() : Exception Copying Photos" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public PhotosUploadResponse uploadPhotos(String dealerId,String uniqueId,Integer sourceId, String fileName , Integer sequenceId , String photo) throws Exception{
		
		String url=urlBase+"v1/appraisalPhotos/dealers/"+dealerId;
		HttpPost request = new HttpPost(url);
		
		System.out.println("Uploading Photos for Dealer-"+dealerId+", AppraisalId-"+uniqueId+", BAString-"+basicAuthString);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		
		try 
		{
			httpClient = new DefaultHttpClient();
			
			ArrayList<NameValuePair> postParameters;
		   


		    postParameters = new ArrayList<NameValuePair>();
		    postParameters.add(new BasicNameValuePair("UniqueID", uniqueId));
		    postParameters.add(new BasicNameValuePair("Source", sourceId+""));
		    postParameters.add(new BasicNameValuePair("SequenceID", sequenceId+""));
		    postParameters.add(new BasicNameValuePair("FileName", fileName));
		    postParameters.add(new BasicNameValuePair("Photo", photo));
		    
		    
		    request.setEntity(new UrlEncodedFormEntity(postParameters));
			
		    HttpResponse response = httpClient.execute(request);
		    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
			{
				
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				PhotosUploadResponse resp = gson.fromJson(reader, PhotosUploadResponse.class);
				return resp;
			}

			System.err.println("PhotosServiceClient.getPhotos() : Error Upload Photos.  " + buildLogMsg(url, response));
			log.error("PhotosServiceClient.getPhotos() : Error Upload Photos.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to upload Photos.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("PhotosServiceClient.getPhotos() : Exception Uploading Photos" + buildLogMsg(url, null) + e.getMessage());
			log.error("PhotosServiceClient.getPhotos() : Exception Uploading Photos" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	private String buildLogMsg(String url, HttpResponse resp)
	{
		String httpResponse = "";
		
		try {
			if (resp != null)
			{
				httpResponse = " :: httpResponse code = " + resp.getStatusLine().getStatusCode() +
				   " :: httpResponse = " + resp.toString();
			}
		} catch( Exception e ) { System.err.println("buildLogMsg() : Failed ") ;}
		
		return "url = " + url + " :: basicAuthString = " + basicAuthString + httpResponse;
		
	}
	public static void main(String args[]){
		
		PhotosServiceClient psc2= new PhotosServiceClient("http://2k8bweb-svcs01x.int.firstlook.biz/","ZG1laHRhOk5AZEAxMjM=");
	//	PhotosServiceClient psc= new PhotosServiceClient("http://10.20.11.162:8079/","ZG1laHRhOk5AZEAxMjM=");
		try {

			//psc.getPhotos("101620", "0a979252-2814-465b-8743-ac716b084c58");
			psc2.copyPhotos("105239","ab09a1c9-370a-440d-9882-a0b94e3df8c2",1, "10001001");
			//psc2.deletePhotos("105239","10001001",7,1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
}
	




