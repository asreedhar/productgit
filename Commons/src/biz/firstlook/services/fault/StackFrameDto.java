
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StackFrameDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StackFrameDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClassName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MethodName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsNativeMethod" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsUnknownSource" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StackFrameDto", propOrder = {
    "className",
    "fileName",
    "methodName",
    "isNativeMethod",
    "isUnknownSource",
    "lineNumber"
})
public class StackFrameDto {

    @XmlElement(name = "ClassName")
    protected String className;
    @XmlElement(name = "FileName")
    protected String fileName;
    @XmlElement(name = "MethodName")
    protected String methodName;
    @XmlElement(name = "IsNativeMethod")
    protected boolean isNativeMethod;
    @XmlElement(name = "IsUnknownSource")
    protected boolean isUnknownSource;
    @XmlElement(name = "LineNumber", required = true, type = Integer.class, nillable = true)
    protected Integer lineNumber;

    /**
     * Gets the value of the className property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassName() {
        return className;
    }

    /**
     * Sets the value of the className property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassName(String value) {
        this.className = value;
    }

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the methodName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Sets the value of the methodName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodName(String value) {
        this.methodName = value;
    }

    /**
     * Gets the value of the isNativeMethod property.
     * 
     */
    public boolean isIsNativeMethod() {
        return isNativeMethod;
    }

    /**
     * Sets the value of the isNativeMethod property.
     * 
     */
    public void setIsNativeMethod(boolean value) {
        this.isNativeMethod = value;
    }

    /**
     * Gets the value of the isUnknownSource property.
     * 
     */
    public boolean isIsUnknownSource() {
        return isUnknownSource;
    }

    /**
     * Sets the value of the isUnknownSource property.
     * 
     */
    public void setIsUnknownSource(boolean value) {
        this.isUnknownSource = value;
    }

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineNumber(Integer value) {
        this.lineNumber = value;
    }

}
