
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.services.fault package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.services.fault
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MachineDto }
     * 
     */
    public MachineDto createMachineDto() {
        return new MachineDto();
    }

    /**
     * Create an instance of {@link ApplicationDto }
     * 
     */
    public ApplicationDto createApplicationDto() {
        return new ApplicationDto();
    }

    /**
     * Create an instance of {@link StackDto }
     * 
     */
    public StackDto createStackDto() {
        return new StackDto();
    }

    /**
     * Create an instance of {@link SaveFault }
     * 
     */
    public SaveFault createSaveFault() {
        return new SaveFault();
    }

    /**
     * Create an instance of {@link RequestInformationDto }
     * 
     */
    public RequestInformationDto createRequestInformationDto() {
        return new RequestInformationDto();
    }

    /**
     * Create an instance of {@link SaveFaultResponse }
     * 
     */
    public SaveFaultResponse createSaveFaultResponse() {
        return new SaveFaultResponse();
    }

    /**
     * Create an instance of {@link FaultEventDto }
     * 
     */
    public FaultEventDto createFaultEventDto() {
        return new FaultEventDto();
    }

    /**
     * Create an instance of {@link PlatformDto }
     * 
     */
    public PlatformDto createPlatformDto() {
        return new PlatformDto();
    }

    /**
     * Create an instance of {@link FaultDto }
     * 
     */
    public FaultDto createFaultDto() {
        return new FaultDto();
    }

    /**
     * Create an instance of {@link SaveRemoteArgumentsDto }
     * 
     */
    public SaveRemoteArgumentsDto createSaveRemoteArgumentsDto() {
        return new SaveRemoteArgumentsDto();
    }

    /**
     * Create an instance of {@link ArrayOfFaultDataDto }
     * 
     */
    public ArrayOfFaultDataDto createArrayOfFaultDataDto() {
        return new ArrayOfFaultDataDto();
    }

    /**
     * Create an instance of {@link FaultDataDto }
     * 
     */
    public FaultDataDto createFaultDataDto() {
        return new FaultDataDto();
    }

    /**
     * Create an instance of {@link ArrayOfStackFrameDto }
     * 
     */
    public ArrayOfStackFrameDto createArrayOfStackFrameDto() {
        return new ArrayOfStackFrameDto();
    }

    /**
     * Create an instance of {@link StackFrameDto }
     * 
     */
    public StackFrameDto createStackFrameDto() {
        return new StackFrameDto();
    }

    /**
     * Create an instance of {@link SaveRemoteResultsDto }
     * 
     */
    public SaveRemoteResultsDto createSaveRemoteResultsDto() {
        return new SaveRemoteResultsDto();
    }

}
