
package biz.firstlook.services.fault;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfStackFrameDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfStackFrameDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StackFrameDto" type="{http://services.firstlook.biz/Fault/}StackFrameDto" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfStackFrameDto", propOrder = {
    "stackFrameDto"
})
public class ArrayOfStackFrameDto {

    @XmlElement(name = "StackFrameDto", required = true, nillable = true)
    protected List<StackFrameDto> stackFrameDto;

    /**
     * Gets the value of the stackFrameDto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stackFrameDto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStackFrameDto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StackFrameDto }
     * 
     * 
     */
    public List<StackFrameDto> getStackFrameDto() {
        if (stackFrameDto == null) {
            stackFrameDto = new ArrayList<StackFrameDto>();
        }
        return this.stackFrameDto;
    }

}
