
package biz.firstlook.services.fault;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "FaultSoap", targetNamespace = "http://services.firstlook.biz/Fault/")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface FaultSoap {


    @WebMethod(operationName = "SaveFault", action = "http://services.firstlook.biz/Fault/SaveFault")
    @WebResult(name = "SaveFaultResult", targetNamespace = "http://services.firstlook.biz/Fault/")
    public SaveRemoteResultsDto saveFault(
        @WebParam(name = "arguments", targetNamespace = "http://services.firstlook.biz/Fault/")
        SaveRemoteArgumentsDto arguments);

}
