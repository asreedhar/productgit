
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FaultEventDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FaultEventDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FaultTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Fault" type="{http://services.firstlook.biz/Fault/}FaultDto" minOccurs="0"/>
 *         &lt;element name="FaultData" type="{http://services.firstlook.biz/Fault/}ArrayOfFaultDataDto" minOccurs="0"/>
 *         &lt;element name="RequestInformation" type="{http://services.firstlook.biz/Fault/}RequestInformationDto" minOccurs="0"/>
 *         &lt;element name="Application" type="{http://services.firstlook.biz/Fault/}ApplicationDto" minOccurs="0"/>
 *         &lt;element name="Machine" type="{http://services.firstlook.biz/Fault/}MachineDto" minOccurs="0"/>
 *         &lt;element name="Platform" type="{http://services.firstlook.biz/Fault/}PlatformDto" minOccurs="0"/>
 *         &lt;element name="IsNew" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultEventDto", propOrder = {
    "faultTime",
    "fault",
    "faultData",
    "requestInformation",
    "application",
    "machine",
    "platform",
    "isNew",
    "id"
})
public class FaultEventDto {

    @XmlElement(name = "FaultTime", required = true)
    protected XMLGregorianCalendar faultTime;
    @XmlElement(name = "Fault")
    protected FaultDto fault;
    @XmlElement(name = "FaultData")
    protected ArrayOfFaultDataDto faultData;
    @XmlElement(name = "RequestInformation")
    protected RequestInformationDto requestInformation;
    @XmlElement(name = "Application")
    protected ApplicationDto application;
    @XmlElement(name = "Machine")
    protected MachineDto machine;
    @XmlElement(name = "Platform")
    protected PlatformDto platform;
    @XmlElement(name = "IsNew")
    protected boolean isNew;
    @XmlElement(name = "Id")
    protected int id;

    /**
     * Gets the value of the faultTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFaultTime() {
        return faultTime;
    }

    /**
     * Sets the value of the faultTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFaultTime(XMLGregorianCalendar value) {
        this.faultTime = value;
    }

    /**
     * Gets the value of the fault property.
     * 
     * @return
     *     possible object is
     *     {@link FaultDto }
     *     
     */
    public FaultDto getFault() {
        return fault;
    }

    /**
     * Sets the value of the fault property.
     * 
     * @param value
     *     allowed object is
     *     {@link FaultDto }
     *     
     */
    public void setFault(FaultDto value) {
        this.fault = value;
    }

    /**
     * Gets the value of the faultData property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFaultDataDto }
     *     
     */
    public ArrayOfFaultDataDto getFaultData() {
        return faultData;
    }

    /**
     * Sets the value of the faultData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFaultDataDto }
     *     
     */
    public void setFaultData(ArrayOfFaultDataDto value) {
        this.faultData = value;
    }

    /**
     * Gets the value of the requestInformation property.
     * 
     * @return
     *     possible object is
     *     {@link RequestInformationDto }
     *     
     */
    public RequestInformationDto getRequestInformation() {
        return requestInformation;
    }

    /**
     * Sets the value of the requestInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestInformationDto }
     *     
     */
    public void setRequestInformation(RequestInformationDto value) {
        this.requestInformation = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationDto }
     *     
     */
    public ApplicationDto getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationDto }
     *     
     */
    public void setApplication(ApplicationDto value) {
        this.application = value;
    }

    /**
     * Gets the value of the machine property.
     * 
     * @return
     *     possible object is
     *     {@link MachineDto }
     *     
     */
    public MachineDto getMachine() {
        return machine;
    }

    /**
     * Sets the value of the machine property.
     * 
     * @param value
     *     allowed object is
     *     {@link MachineDto }
     *     
     */
    public void setMachine(MachineDto value) {
        this.machine = value;
    }

    /**
     * Gets the value of the platform property.
     * 
     * @return
     *     possible object is
     *     {@link PlatformDto }
     *     
     */
    public PlatformDto getPlatform() {
        return platform;
    }

    /**
     * Sets the value of the platform property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlatformDto }
     *     
     */
    public void setPlatform(PlatformDto value) {
        this.platform = value;
    }

    /**
     * Gets the value of the isNew property.
     * 
     */
    public boolean isIsNew() {
        return isNew;
    }

    /**
     * Sets the value of the isNew property.
     * 
     */
    public void setIsNew(boolean value) {
        this.isNew = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

}
