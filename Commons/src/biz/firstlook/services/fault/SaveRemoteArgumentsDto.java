
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveRemoteArgumentsDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveRemoteArgumentsDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FaultEvent" type="{http://services.firstlook.biz/Fault/}FaultEventDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveRemoteArgumentsDto", propOrder = {
    "faultEvent"
})
public class SaveRemoteArgumentsDto {

    @XmlElement(name = "FaultEvent")
    protected FaultEventDto faultEvent;

    /**
     * Gets the value of the faultEvent property.
     * 
     * @return
     *     possible object is
     *     {@link FaultEventDto }
     *     
     */
    public FaultEventDto getFaultEvent() {
        return faultEvent;
    }

    /**
     * Sets the value of the faultEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link FaultEventDto }
     *     
     */
    public void setFaultEvent(FaultEventDto value) {
        this.faultEvent = value;
    }

}
