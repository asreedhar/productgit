
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StackDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StackDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StackFrames" type="{http://services.firstlook.biz/Fault/}ArrayOfStackFrameDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StackDto", propOrder = {
    "stackFrames"
})
public class StackDto {

    @XmlElement(name = "StackFrames")
    protected ArrayOfStackFrameDto stackFrames;

    /**
     * Gets the value of the stackFrames property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfStackFrameDto }
     *     
     */
    public ArrayOfStackFrameDto getStackFrames() {
        return stackFrames;
    }

    /**
     * Sets the value of the stackFrames property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfStackFrameDto }
     *     
     */
    public void setStackFrames(ArrayOfStackFrameDto value) {
        this.stackFrames = value;
    }

}
