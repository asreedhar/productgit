
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaultDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FaultDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cause" type="{http://services.firstlook.biz/Fault/}FaultDto" minOccurs="0"/>
 *         &lt;element name="ExceptionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Stack" type="{http://services.firstlook.biz/Fault/}StackDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultDto", propOrder = {
    "cause",
    "exceptionType",
    "message",
    "stack"
})
public class FaultDto {

    @XmlElement(name = "Cause")
    protected FaultDto cause;
    @XmlElement(name = "ExceptionType")
    protected String exceptionType;
    @XmlElement(name = "Message")
    protected String message;
    @XmlElement(name = "Stack")
    protected StackDto stack;

    /**
     * Gets the value of the cause property.
     * 
     * @return
     *     possible object is
     *     {@link FaultDto }
     *     
     */
    public FaultDto getCause() {
        return cause;
    }

    /**
     * Sets the value of the cause property.
     * 
     * @param value
     *     allowed object is
     *     {@link FaultDto }
     *     
     */
    public void setCause(FaultDto value) {
        this.cause = value;
    }

    /**
     * Gets the value of the exceptionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * Sets the value of the exceptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionType(String value) {
        this.exceptionType = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }
    
    /**
     * Gets the value of the stack property.
     * 
     * @return
     *     possible object is
     *     {@link StackDto }
     *     
     */
    public StackDto getStack() {
        return stack;
    }

    /**
     * Sets the value of the stack property.
     * 
     * @param value
     *     allowed object is
     *     {@link StackDto }
     *     
     */
    public void setStack(StackDto value) {
        this.stack = value;
    }

}
