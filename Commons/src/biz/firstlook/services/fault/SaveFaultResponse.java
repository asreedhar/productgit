
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SaveFaultResult" type="{http://services.firstlook.biz/Fault/}SaveRemoteResultsDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "saveFaultResult"
})
@XmlRootElement(name = "SaveFaultResponse")
public class SaveFaultResponse {

    @XmlElement(name = "SaveFaultResult")
    protected SaveRemoteResultsDto saveFaultResult;

    /**
     * Gets the value of the saveFaultResult property.
     * 
     * @return
     *     possible object is
     *     {@link SaveRemoteResultsDto }
     *     
     */
    public SaveRemoteResultsDto getSaveFaultResult() {
        return saveFaultResult;
    }

    /**
     * Sets the value of the saveFaultResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveRemoteResultsDto }
     *     
     */
    public void setSaveFaultResult(SaveRemoteResultsDto value) {
        this.saveFaultResult = value;
    }

}
