
package biz.firstlook.services.fault;

import javax.jws.WebService;

@WebService(serviceName = "Fault", targetNamespace = "http://services.firstlook.biz/Fault/", endpointInterface = "biz.firstlook.services.fault.FaultSoap")
public class FaultImpl
    implements FaultSoap
{


    public SaveRemoteResultsDto saveFault(SaveRemoteArgumentsDto arguments) {
        throw new UnsupportedOperationException();
    }

}
