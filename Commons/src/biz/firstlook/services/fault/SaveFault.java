
package biz.firstlook.services.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arguments" type="{http://services.firstlook.biz/Fault/}SaveRemoteArgumentsDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "arguments"
})
@XmlRootElement(name = "SaveFault")
public class SaveFault {

    protected SaveRemoteArgumentsDto arguments;

    /**
     * Gets the value of the arguments property.
     * 
     * @return
     *     possible object is
     *     {@link SaveRemoteArgumentsDto }
     *     
     */
    public SaveRemoteArgumentsDto getArguments() {
        return arguments;
    }

    /**
     * Sets the value of the arguments property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveRemoteArgumentsDto }
     *     
     */
    public void setArguments(SaveRemoteArgumentsDto value) {
        this.arguments = value;
    }

}
