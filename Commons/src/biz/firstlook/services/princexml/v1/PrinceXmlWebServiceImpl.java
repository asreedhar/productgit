
package biz.firstlook.services.princexml.v1;

import javax.jws.WebService;

@WebService(serviceName = "PrinceXmlWebService", targetNamespace = "http://www.firstlook.biz/service/PrinceXml/v1", endpointInterface = "biz.firstlook.service.princexml.v1.PrinceXmlWebService")
public class PrinceXmlWebServiceImpl
    implements PrinceXmlWebService
{


    public PrinceXmlResponse generate(biz.firstlook.services.princexml.v1.PrinceXmlRequest PrinceXmlRequest) {
        throw new UnsupportedOperationException();
    }

}
