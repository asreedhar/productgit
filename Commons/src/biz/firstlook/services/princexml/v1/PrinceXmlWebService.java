
package biz.firstlook.services.princexml.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "PrinceXmlWebService", targetNamespace = "http://www.firstlook.biz/service/PrinceXml/v1")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface PrinceXmlWebService {


    @WebMethod(operationName = "generate", action = "https://www.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService/generate")
    @WebResult(name = "PrinceXmlResponse", targetNamespace = "http://www.firstlook.biz/service/PrinceXml/v1")
    public PrinceXmlResponse generate(
        @WebParam(name = "PrinceXmlRequest", targetNamespace = "http://www.firstlook.biz/service/PrinceXml/v1")
        biz.firstlook.services.princexml.v1.PrinceXmlRequest PrinceXmlRequest);

}
