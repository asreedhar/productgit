
package biz.firstlook.services.princexml.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.service.princexml.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Status_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "Status");
    private final static QName _PrinceXmlRequest_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlRequest");
    private final static QName _PrinceXmlResponse_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlResponse");
    private final static QName _SharedFileSystem_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "SharedFileSystem");
    private final static QName _OutputType_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "OutputType");
    private final static QName _Output_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "Output");
    private final static QName _Input_QNAME = new QName("http://www.firstlook.biz/service/PrinceXml/v1", "Input");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.service.princexml.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrinceXmlResponse }
     * 
     */
    public PrinceXmlResponse createPrinceXmlResponse() {
        return new PrinceXmlResponse();
    }

    /**
     * Create an instance of {@link SharedFileSystem }
     * 
     */
    public SharedFileSystem createSharedFileSystem() {
        return new SharedFileSystem();
    }

    /**
     * Create an instance of {@link Input }
     * 
     */
    public Input createInput() {
        return new Input();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link PrinceXmlRequest }
     * 
     */
    public PrinceXmlRequest createPrinceXmlRequest() {
        return new PrinceXmlRequest();
    }

    /**
     * Create an instance of {@link OutputType }
     * 
     */
    public OutputType createOutputType() {
        return new OutputType();
    }

    /**
     * Create an instance of {@link Output }
     * 
     */
    public Output createOutput() {
        return new Output();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Status }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "Status")
    public JAXBElement<Status> createStatus(Status value) {
        return new JAXBElement<Status>(_Status_QNAME, Status.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrinceXmlRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "PrinceXmlRequest")
    public JAXBElement<PrinceXmlRequest> createPrinceXmlRequest(PrinceXmlRequest value) {
        return new JAXBElement<PrinceXmlRequest>(_PrinceXmlRequest_QNAME, PrinceXmlRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrinceXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "PrinceXmlResponse")
    public JAXBElement<PrinceXmlResponse> createPrinceXmlResponse(PrinceXmlResponse value) {
        return new JAXBElement<PrinceXmlResponse>(_PrinceXmlResponse_QNAME, PrinceXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SharedFileSystem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "SharedFileSystem")
    public JAXBElement<SharedFileSystem> createSharedFileSystem(SharedFileSystem value) {
        return new JAXBElement<SharedFileSystem>(_SharedFileSystem_QNAME, SharedFileSystem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "OutputType")
    public JAXBElement<OutputType> createOutputType(OutputType value) {
        return new JAXBElement<OutputType>(_OutputType_QNAME, OutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Output }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "Output")
    public JAXBElement<Output> createOutput(Output value) {
        return new JAXBElement<Output>(_Output_QNAME, Output.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Input }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/PrinceXml/v1", name = "Input")
    public JAXBElement<Input> createInput(Input value) {
        return new JAXBElement<Input>(_Input_QNAME, Input.class, null, value);
    }

}
