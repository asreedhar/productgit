
package biz.firstlook.services.princexml.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for OutputTypeCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutputTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ByteStream"/>
 *     &lt;enumeration value="SharedFileSystem"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum OutputTypeCode {

    @XmlEnumValue("ByteStream")
    BYTE_STREAM("ByteStream"),
    @XmlEnumValue("SharedFileSystem")
    SHARED_FILE_SYSTEM("SharedFileSystem");
    private final String value;

    OutputTypeCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OutputTypeCode fromValue(String v) {
        for (OutputTypeCode c: OutputTypeCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
