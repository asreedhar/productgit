package biz.firstlook.services.vehicleConfigandValuations;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import biz.firstlook.services.vehicleConfigandValuations.entities.BookValuationRequests;
import biz.firstlook.services.vehicleConfigandValuations.entities.EquipmentExplicitActions;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsRequest;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;
import biz.firstlook.services.vehicleConfigandValuations.entities.VehicleConfigResponse;

import com.google.gson.Gson;

public class VehicleConfigAndValuationsServiceClient {

	static Logger log = Logger.getLogger(VehicleConfigAndValuationsServiceClient.class);
	
	String urlBase;
	String basicAuthString;

	
	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

	public String getBasicAuthString() {
		return basicAuthString;
	}

	public void setBasicAuthString(String basicAuthString) {
		this.basicAuthString = basicAuthString;
	}

	public VehicleConfigAndValuationsServiceClient(String basicAuthString) throws Exception {
		super();
		this.basicAuthString = basicAuthString;
		
		try {
		     Context initialContext = new javax.naming.InitialContext(); 
		     Context ctx = (Context) initialContext.lookup("java:comp/env");
		     
		     this.urlBase = (String) ctx.lookup("flServiceHostUrlBase");
		}
		catch (NamingException e) 
		{
			 System.err.println(" VehicleConfigAndValuationsServiceClient(String basicAuthString): Exception getting flserviceHostName from server.xml " + e.getMessage());
		     log.error("VehicleConfigAndValuationsServiceClient(String basicAuthString): Exception getting flserviceHostName from server.xml", e);
		     throw new Exception("Could not retrieve FL service url base from server.xml - please update server.xml");
		}	
		
		
	}
	
	
	public VehicleConfigAndValuationsServiceClient(String urlBase,
			String basicAuthString) {
		super();
		this.urlBase = urlBase;
		this.basicAuthString = basicAuthString;
	}

	public VehicleConfigResponse getVehicleConfig(String dealerId,String vin) throws Exception{
		
		String url = urlBase + "vehicleConfig/dealers/"+dealerId+"/vins/"+vin;
		HttpGet request = new HttpGet(url);
		
		System.out.println("Getting Vehicle Config for Dealer-"+dealerId+", Vin-"+vin+", BAString-"+basicAuthString);

		
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		try 
		{
			httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				
				
				BufferedReader reader2 = new BufferedReader(reader);
			    StringBuilder out = new StringBuilder();
			    String newLine = System.getProperty("line.separator");
			    String line;
			    while ((line = reader2.readLine()) != null) {
			        out.append(line);
			        out.append(newLine);
			    }
			    String json=(out.toString());
			    System.out.println("VehicleConfig-"+json);
				VehicleConfigResponse resp = gson.fromJson(json, VehicleConfigResponse.class);

				return resp;
			}

			System.err.println("VehicleConfigAndValuationsServiceClient.getVehicleConfig() : Error getting Vehicle Config.  " + buildLogMsg(url, response));
			log.error("VehicleConfigAndValuationsServiceClient.getVehicleConfig() : Error getting Vehicle Config.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to retrieve Vehicle Config.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("VehicleConfigAndValuationsServiceClient.getVehicleConfig() : Exception getting Vehicle Config" + buildLogMsg(url, null) + e.getMessage());
			log.error("VehicleConfigAndValuationsServiceClient.getVehicleConfig() : Exception getting Vehicle Config" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
		
	}
	
	
public ValuationsResponse getValuations(String dealerId,ValuationsRequest req) throws Exception{
		
		String url=urlBase+"valuations/dealers/"+dealerId;
		HttpPost request = new HttpPost(url);
		
		System.out.println("Posting Valuations for Dealer-"+dealerId+", BAString-"+basicAuthString);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		
		try 
		{
			httpClient = new DefaultHttpClient();
			Gson gson = new Gson();
			
		   StringEntity entity= new StringEntity(gson.toJson(req,req.getClass()));
		   entity.setContentType("application/json");
			

		    request.setEntity(entity);
			
		    HttpResponse response = httpClient.execute(request);
		    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED)
			{
				
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				BufferedReader reader2 = new BufferedReader(reader);
			    StringBuilder out = new StringBuilder();
			    String newLine = System.getProperty("line.separator");
			    String line;
			    while ((line = reader2.readLine()) != null) {
			        out.append(line);
			        out.append(newLine);
			    }
			    String json=(out.toString());
			    System.out.println("Valuations-"+json);
				
				
				ValuationsResponse resp = gson.fromJson(json, ValuationsResponse.class);
				return resp;
			}

			System.err.println("VehicleConfigAndValuationsServiceClient.getValuations : Error Getting Valuations.  " + buildLogMsg(url, response));
			log.error("VehicleConfigAndValuationsServiceClient.getValuations : Error Getting Valuations.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to Getting Valuations.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("VehicleConfigAndValuationsServiceClient.getValuations : Exception Getting Valuations" + buildLogMsg(url, null) + e.getMessage());
			log.error("VehicleConfigAndValuationsServiceClient.getValuations : Exception Getting Valuations" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	
	
	private String buildLogMsg(String url, HttpResponse resp)
	{
		String httpResponse = "";
		
		try {
			if (resp != null)
			{
				httpResponse = " :: httpResponse code = " + resp.getStatusLine().getStatusCode() +
				   " :: httpResponse = " + resp.toString();
			}
		} catch( Exception e ) { System.err.println("buildLogMsg() : Failed ") ;}
		
		return "url = " + url + " :: basicAuthString = " + basicAuthString + httpResponse;
		
	}
	
	
	public static void main(String[] args) {
		 VehicleConfigAndValuationsServiceClient client=new VehicleConfigAndValuationsServiceClient("http://2k8bweb-svcs01x.int.firstlook.biz/","ZG1laHRhOk5AZEAxMjM=");
		 
		 try {
			VehicleConfigResponse resp= client.getVehicleConfig("101590", "1FTZR45E98PB05731");
			System.out.println(resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	
	
	public static void mainf(String[] args) {
		VehicleConfigAndValuationsServiceClient client=new VehicleConfigAndValuationsServiceClient("http://2k8bweb-svcs01x.int.firstlook.biz/","ZG1laHRhOk5AZEAxMjM=");
		 
		 try {
			 
			 ValuationsRequest req= new ValuationsRequest();
			 
			 BookValuationRequests bookReq= new BookValuationRequests();

			 bookReq.setMileage("30000");
			 bookReq.setTrimId("1142606");
			 bookReq.setBook("nada");
			 bookReq.setReturnId("0c9a3e30-6cbd-11e2-bcfd-0800200c9a66");
			 bookReq.setEquipmentSelected(new String[]{"035","139","145","259"});

			 
			 bookReq.setEquipmentExplicitActions(
		 	new EquipmentExplicitActions[]{
		 			new EquipmentExplicitActions("A", "035"),
		 			new EquipmentExplicitActions("A", "139"),
		 			new EquipmentExplicitActions("A", "145"),
		 			new EquipmentExplicitActions("A", "259"),
		 			
		 	}	 
					 );
			 
			 
			 
			 req.setBookValuationRequests(new BookValuationRequests[]{bookReq});
			 
			 
			 client.getValuations("100506", req);
			 
//			 client.getVehicleConfig("100506", "1FTZR45E98PB05731");
		
		 
		 
		 
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
