package biz.firstlook.services.vehicleConfigandValuations.entities;

public class EquipmentRules
{
    private String rule;

    private String[] equipment;

    public String getRule ()
    {
        return rule;
    }

    public void setRule (String rule)
    {
        this.rule = rule;
    }

    public String[] getEquipment ()
    {
        return equipment;
    }

    public void setEquipment (String[] equipment)
    {
        this.equipment = equipment;
    }

    @Override
    public String toString()
    {
        return " [rule = "+rule+", equipment = "+equipment+"]";
    }
}