package biz.firstlook.services.vehicleConfigandValuations.entities;

public class BookValuationRequests
{
    private String trimId;

    private String mileage;

    private EquipmentExplicitActions[] equipmentExplicitActions;

    private String book;

    private String[] equipmentSelected;

    private String returnId;

    public String getTrimId ()
    {
        return trimId;
    }

    public void setTrimId (String trimId)
    {
        this.trimId = trimId;
    }

    public String getMileage ()
    {
        return mileage;
    }

    public void setMileage (String mileage)
    {
        this.mileage = mileage;
    }

    public EquipmentExplicitActions[] getEquipmentExplicitActions ()
    {
        return equipmentExplicitActions;
    }

    public void setEquipmentExplicitActions (EquipmentExplicitActions[] equipmentExplicitActions)
    {
        this.equipmentExplicitActions = equipmentExplicitActions;
    }

    public String getBook ()
    {
        return book;
    }

    public void setBook (String book)
    {
        this.book = book;
    }

    public String[] getEquipmentSelected ()
    {
        return equipmentSelected;
    }

    public void setEquipmentSelected (String[] equipmentSelected)
    {
        this.equipmentSelected = equipmentSelected;
    }

    public String getReturnId ()
    {
        return returnId;
    }

    public void setReturnId (String returnId)
    {
        this.returnId = returnId;
    }

    @Override
    public String toString()
    {
        return " [trimId = "+trimId+", mileage = "+mileage+", equipmentExplicitActions = "+equipmentExplicitActions+", book = "+book+", equipmentSelected = "+equipmentSelected+", returnId = "+returnId+"]";
    }
}