package biz.firstlook.services.vehicleConfigandValuations.entities;

public class VehicleConfigResponse
{
    private Configs[] configs;

    public Configs[] getConfigs ()
    {
        return configs;
    }

    public void setConfigs (Configs[] configs)
    {
        this.configs = configs;
    }

    @Override
    public String toString()
    {
        return " [configs = "+configs+"]";
    }
}