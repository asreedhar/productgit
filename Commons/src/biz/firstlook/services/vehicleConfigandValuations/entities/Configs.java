package biz.firstlook.services.vehicleConfigandValuations.entities;


public class Configs
{
	private EquipmentRules[] equipmentRules;

	private String trimId;

	private String model;

	private String selected;

	private String book;
	
	private String[][] explicitActions;
	
	private String year;

	private Equipment[] equipment;

	private String trim;

	private String make;
	
	private Boolean isSelectedTrim=false;
	
	public String[][] getExplicitActions() {
		return explicitActions;
	}

	public void setExplicitActions(String[][] explicitActions) {
		this.explicitActions = explicitActions;
	}

	public Boolean getIsSelectedTrim() {
		return isSelectedTrim;
	}

	public void setIsSelectedTrim(Boolean isSelectedTrim) {
		this.isSelectedTrim = isSelectedTrim;
	}

	public EquipmentRules[] getEquipmentRules ()
	{
		return equipmentRules;
	}

	public void setEquipmentRules (EquipmentRules[] equipmentRules)
	{
		this.equipmentRules = equipmentRules;
	}

	public String getTrimId ()
	{
		return trimId;
	}

	public void setTrimId (String trimId)
	{
		this.trimId = trimId;
	}

	public String getModel ()
	{
		return model;
	}

	public void setModel (String model)
	{
		this.model = model;
	}

	public String getSelected ()
	{
		return selected;
	}

	public void setSelected (String selected)
	{
		this.selected = selected;
	}

	public String getBook ()
	{
		return book;
	}

	public void setBook (String book)
	{
		this.book = book;
	}

	public String getYear ()
	{
		return year;
	}

	public void setYear (String year)
	{
		this.year = year;
	}

	public Equipment[] getEquipment ()
	{
		return equipment;
	}

	public void setEquipment (Equipment[] equipment)
	{
		this.equipment = equipment;
	}

	public String getTrim ()
	{
		return trim;
	}

	public void setTrim (String trim)
	{
		this.trim = trim;
	}

	public String getMake ()
	{
		return make;
	}

	public void setMake (String make)
	{
		this.make = make;
	}
	
	public Equipment getEquipmentById(String id){
		for(Equipment e:equipment){
			if(e.getId().equalsIgnoreCase(id))
				return e;
		}
		
		return null;
	}

	@Override
	public String toString()
	{
		return "[equipmentRules = "+equipmentRules+", trimId = "+trimId+", model = "+model+", selected = "+selected+", book = "+book+", year = "+year+", equipment = "+equipment+", trim = "+trim+", make = "+make+"]";
	}
}