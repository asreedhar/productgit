package biz.firstlook.services.vehicleConfigandValuations.entities;

public class Equipment
{
    private String id;

    @com.google.gson.annotations.SerializedName("default")
    private Boolean defaults=false;

    private String desc;

    private String sortOrder;

    private Boolean status=false;
    
    
    
    public Boolean isStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    

    public Boolean getDefaults() {
		return defaults;
	}

	public void setDefaults(Boolean defaults) {
		this.defaults = defaults;
	}

	public String getDesc ()
    {
        return desc;
    }

    public void setDesc (String desc)
    {
        this.desc = desc;
    }

    public String getSortOrder ()
    {
        return sortOrder;
    }

    public void setSortOrder (String sortOrder)
    {
        this.sortOrder = sortOrder;
    }

    @Override
    public String toString()
    {
        return " [id = "+id+", default = "+defaults+", desc = "+desc+", sortOrder = "+sortOrder+", status = "+status+"]";
    }
}