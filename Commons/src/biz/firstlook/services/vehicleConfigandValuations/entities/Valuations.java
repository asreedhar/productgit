package biz.firstlook.services.vehicleConfigandValuations.entities;


public class Valuations
{
	private String desc;

	private String value;

	public String getDesc ()
	{
		return desc;
	}

	public void setDesc (String desc)
	{
		this.desc = desc;
	}

	public String getValue ()
	{
		return value;
	}

	public void setValue (String value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return " [desc = "+desc+", value = "+value+"]";
	}
}

