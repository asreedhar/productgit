package biz.firstlook.services.vehicleConfigandValuations.entities;

public class ValuationsResponse
{
    private BookValuations[] bookValuations;

    public BookValuations[] getBookValuations ()
    {
        return bookValuations;
    }

    public void setBookValuations (BookValuations[] bookValuations)
    {
        this.bookValuations = bookValuations;
    }

    @Override
    public String toString()
    {
        return " [bookValuations = "+bookValuations+"]";
    }
}