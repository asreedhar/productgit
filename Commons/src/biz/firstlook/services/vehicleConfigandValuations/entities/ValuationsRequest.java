package biz.firstlook.services.vehicleConfigandValuations.entities;

public class ValuationsRequest {
	
	private BookValuationRequests[] bookValuationRequests;

	public BookValuationRequests[] getBookValuationRequests ()
	{
		return bookValuationRequests;
	}

	public void setBookValuationRequests (BookValuationRequests[] bookValuationRequests)
	{
		this.bookValuationRequests = bookValuationRequests;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [bookValuationRequests = "+bookValuationRequests+"]";
	}
}
