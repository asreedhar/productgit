package biz.firstlook.services.vehicleConfigandValuations.entities;

public class EquipmentExplicitActions
{
    private String action;

    private String equipment;

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

    public String getEquipment ()
    {
        return equipment;
    }

    public void setEquipment (String equipment)
    {
        this.equipment = equipment;
    }
    
    

    public EquipmentExplicitActions(String action, String equipment) {
		super();
		this.action = action;
		this.equipment = equipment;
	}

	@Override
    public String toString()
    {
        return " [action = "+action+", equipment = "+equipment+"]";
    }
}