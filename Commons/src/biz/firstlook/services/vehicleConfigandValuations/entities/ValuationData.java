package biz.firstlook.services.vehicleConfigandValuations.entities;


public class ValuationData
{
	private String market;

	private String condition;

	private Valuations[] valuations;

	public String getMarket ()
	{
		return market;
	}

	public void setMarket (String market)
	{
		this.market = market;
	}

	public String getCondition ()
	{
		return condition;
	}

	public void setCondition (String condition)
	{
		this.condition = condition;
	}

	public Valuations[] getValuations ()
	{
		return valuations;
	}

	public void setValuations (Valuations[] valuations)
	{
		this.valuations = valuations;
	}

	@Override
	public String toString()
	{
		return " [market = "+market+", condition = "+condition+", valuations = "+valuations+"]";
	}
}

