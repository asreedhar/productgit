package biz.firstlook.services.vehicleConfigandValuations.entities;

public class BookValuations
{
	private ValuationData[] valuationData;

	private String checkSum;

	private String returnId;

	public ValuationData[] getValuationData ()
	{
		return valuationData;
	}

	public void setValuationData (ValuationData[] valuationData)
	{
		this.valuationData = valuationData;
	}

	public String getCheckSum ()
	{
		return checkSum;
	}

	public void setCheckSum (String checkSum)
	{
		this.checkSum = checkSum;
	}

	public String getReturnId ()
	{
		return returnId;
	}

	public void setReturnId (String returnId)
	{
		this.returnId = returnId;
	}

	@Override
	public String toString()
	{
		return " [valuationData = "+valuationData+", checkSum = "+checkSum+", returnId = "+returnId+"]";
	}
}

