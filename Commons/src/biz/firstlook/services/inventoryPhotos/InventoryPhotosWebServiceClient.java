package biz.firstlook.services.inventoryPhotos;

import biz.firstlook.commons.util.PropertyLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import java.util.List;
import java.util.ArrayList;

import org.xml.sax.*;

import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;

public class InventoryPhotosWebServiceClient {

	private static final String epRest; // Example: "http://localhost/photoservices/v1.svc/rest/";
	private static final int timeoutMs;
	private static final String photoManagerContext; // IMS or MAX
	private static final Logger logger = Logger.getLogger( InventoryPhotosWebServiceClient.class );
	
	static
	{
		epRest = PropertyLoader.getProperty("firstlook.photoservices.url");
		timeoutMs = PropertyLoader.getIntProperty("firstlook.photoservices.timeout", 15000);
		photoManagerContext = PropertyLoader.getProperty("firstlook.photoservices.photoManagerContext");
	}
	
	private InventoryPhotosWebServiceClient()
	{
		
	}

	private static class SingletonHolder{
		public static final InventoryPhotosWebServiceClient INSTANCE = new InventoryPhotosWebServiceClient();
	}
	
	public static InventoryPhotosWebServiceClient getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	public String GetPhotoManagerUrl(String userName, String firstName, String lastName, String businessUnitCode, String vin, String stockNumber)
	{
		Document doc = null;
		try{
			String methodSig = "GetPhotoManagerUrl?username=" + URLEncoder.encode(userName,"UTF-8") + "&firstname=" + URLEncoder.encode(firstName,"UTF-8") + "&lastName=" + URLEncoder.encode(lastName,"UTF-8") + "&businessUnitCode=" + businessUnitCode + "&vin=" + vin + "&stockNumber=" + stockNumber + "&context=" + photoManagerContext;
			doc = getDocument(methodSig);			
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return getPhotoManagerUrl(doc);
	}
	
	public List<String> GetInventoryPhotoUrls(String businessUnitCode, String vin )
	{
		List<String> urls = null;
		
		String methodSig = "GetPhotoUrlsByVin?businessUnitCode=" + businessUnitCode + "&vin=" + vin + "&timeoutMilliseconds=" + timeoutMs;
		Document doc = getDocument(methodSig);

		try
		{
			urls = getInventoryPhotoUrls(doc);
		}
		catch(TransformerException e)
		{
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
		catch(XPathExpressionException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		catch(SAXException e)
		{
			e.printStackTrace();
		}

		if (urls == null) urls = new ArrayList<String>();
		
		return urls;
	}
	
	private static Document getDocument(String methodSig) {
		Document doc = null;
		HttpURLConnection connection = null;
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		StringBuilder sb = null;
		String line = null;

		URL serverAddress = null;

		String url = epRest + methodSig;

		try {

			serverAddress = new URL(url);
			connection = (HttpURLConnection)serverAddress.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.setReadTimeout(timeoutMs);
			connection.connect();

			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true);
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			InputSource inputSrc = new InputSource(connection.getInputStream());
			doc = builder.parse(inputSrc);			

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e){
			e.printStackTrace();
		} catch (SAXException e){
			e.printStackTrace();
		}
		finally
		{
			//close the connection, set all objects to null
			connection.disconnect();
			rd = null;
			sb = null;
			wr = null;
			connection = null;
		}		

		return doc;
	}
	

	private static String getPhotoManagerUrl(Document doc)
	{		
		if(doc != null && doc.hasChildNodes()){
			return doc.getChildNodes().item(0).getTextContent();
		}else{
			logger.warn("getPhotoManagerUrl: Document object was either null or contained no child elements.");
			return null;
		}
	}
	
	private static List<String> getInventoryPhotoUrls(Document doc)
	throws	ParserConfigurationException,	SAXException,	XPathExpressionException,	IOException,
	TransformerConfigurationException, TransformerException
	{
		
		List<String> urls = new ArrayList<String>();
	
		if(doc != null && doc.hasChildNodes()){	
			XPath xpath = XPathFactory.newInstance().newXPath();
			xpath.setNamespaceContext(new UniversalNamespaceCache(doc, false));
			
			NodeList nodes = (NodeList) xpath.evaluate("//:Vehicles/:VehiclePhotos/:PhotoUrls/*", doc, XPathConstants.NODESET);
			
			for (int i = 0; i < nodes.getLength(); i++) {
				urls.add(nodes.item(i).getTextContent());
			}		
		}else{
			logger.warn("getInventoryPhotoUrls: Document object was either null or contained no child elements.");	
		}
		
		return urls;
	}
	
}