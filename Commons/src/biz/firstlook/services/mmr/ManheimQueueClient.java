
package biz.firstlook.services.mmr;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

import biz.firstlook.commons.util.PropertyFinder;

public class ManheimQueueClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service2;

    public ManheimQueueClient() {
        create2();
        
        // 	********** Server Code ************
        
        
        
        String server="";
		Context initialContext ;
		Context ctx;
		try {
			initialContext = new javax.naming.InitialContext(); 
			ctx = (Context) initialContext.lookup("java:comp/env");
			server = (String) ctx.lookup("ServerName");
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	PropertyFinder finder = new PropertyFinder( "Commons" );
    	
    	String url = null;
    	if (server.equalsIgnoreCase("prod"))
    		url=finder.getProperty( "Prod.Manheim.Queue.Url" );
		else if (server.equalsIgnoreCase("beta"))
    			url=finder.getProperty( "Beta.Manheim.Queue.Url" );
			else if (server.equalsIgnoreCase("intb"))
				url=finder.getProperty( "Intb.Manheim.Queue.Url" );
				else if (server.equalsIgnoreCase("dev"))
					url=finder.getProperty( "Dev.Manheim.Queue.Url" );    	
    	
    	System.out.println(server+"---- server name---"+url);
        
        
        if(url==null)
        	url=finder.getProperty( "Prod.Manheim.Queue.Url" );
        
        Endpoint ManheimQueueSoapLocalEndpointEP = service2 .addEndpoint(new QName("http://tempuri.org/", "ManheimQueueSoapLocalEndpoint"), new QName("http://tempuri.org/", "ManheimQueueSoapLocalBinding"), "xfire.local://ManheimQueue");
        endpoints.put(new QName("http://tempuri.org/", "ManheimQueueSoapLocalEndpoint"), ManheimQueueSoapLocalEndpointEP);
        Endpoint ManheimQueueSoapEP = service2 .addEndpoint(new QName("http://tempuri.org/", "ManheimQueueSoap"), new QName("http://tempuri.org/", "ManheimQueueSoap"), url);
        endpoints.put(new QName("http://tempuri.org/", "ManheimQueueSoap"), ManheimQueueSoapEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create2() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service2 = asf.create((biz.firstlook.services.mmr.ManheimQueueSoap.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service2, new QName("http://tempuri.org/", "ManheimQueueSoapLocalBinding"), "urn:xfire:transport:local");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service2, new QName("http://tempuri.org/", "ManheimQueueSoap"), "http://schemas.xmlsoap.org/soap/http");
        }
    }

    public ManheimQueueSoap getManheimQueueSoapLocalEndpoint() {
        return ((ManheimQueueSoap)(this).getEndpoint(new QName("http://tempuri.org/", "ManheimQueueSoapLocalEndpoint")));
    }

    public ManheimQueueSoap getManheimQueueSoapLocalEndpoint(String url) {
        ManheimQueueSoap var = getManheimQueueSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public ManheimQueueSoap getManheimQueueSoap() {
        return ((ManheimQueueSoap)(this).getEndpoint(new QName("http://tempuri.org/", "ManheimQueueSoap")));
    }

    public ManheimQueueSoap getManheimQueueSoap(String url) {
        ManheimQueueSoap var = getManheimQueueSoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
