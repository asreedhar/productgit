
package biz.firstlook.services.mmr;

import javax.jws.WebService;

import biz.firstlook.services.mmr.objects.MMRInventoryRequest;
import biz.firstlook.services.mmr.objects.MMRResult;

@WebService(serviceName = "ManheimQueue", targetNamespace = "http://tempuri.org/", endpointInterface = "biz.firstlook.services.mmr.ManheimQueueSoap")
public class ManheimQueueImpl
    implements ManheimQueueSoap
{


    public void pushPeriodicDataToQueue(int days, biz.firstlook.services.mmr.objects.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public void decodeVIN(String vin, biz.firstlook.services.mmr.objects.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public void updateMMRAveragePriceFromQueue(int recordCount, biz.firstlook.services.mmr.objects.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public void purgeSuccessQueue(int days, biz.firstlook.services.mmr.objects.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public MMRResult updateAveragePrice(MMRInventoryRequest request, biz.firstlook.services.mmr.objects.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public void purgeFailureQueue(int days, biz.firstlook.services.mmr.objects.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

}
