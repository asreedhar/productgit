package biz.firstlook.services.mmr;

import java.io.InputStreamReader;
import java.io.Reader;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.log4j.Logger;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTransactions.Transaction;
import biz.firstlook.services.mmr.entity.MMRTrim;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


public class MMRServiceClient
{
	static Logger log = Logger.getLogger(MMRServiceClient.class);
	String urlBase;
	String basicAuthString;
	private Integer timeout;

	// DefaultHttpClient client = null;
	public MMRServiceClient(String basicAuthString) throws Exception 
	{
		try {
		     Context initialContext = new javax.naming.InitialContext(); 
		     Context ctx = (Context) initialContext.lookup("java:comp/env");
		     
		     this.urlBase = (String) ctx.lookup("flServiceHostUrlBase");
		}
		catch (NamingException e) 
		{
			 System.err.println(" MMRServiceClient(String basicAuthString): Exception getting mmrUrlBase from server.xml " + e.getMessage());
		     log.error("MMRServiceClient(String basicAuthString): Exception getting mmrUrlBase from server.xml", e);
		     throw new Exception("Could not retrieve Manheim service url base from server.xml - please update server.xml");
		}	
		
		this.basicAuthString = basicAuthString;
		
		Context initialContext;
		try {
			initialContext = new javax.naming.InitialContext();
			Context ctx = (Context) initialContext.lookup("java:comp/env");
			String user= (String) ctx.lookup("userNameForAdmin");
			String password= (String) ctx.lookup("passwordForAdmin");
			
			
			BasicAuthAdapter baa = new BasicAuthAdapter(user, password);
			this.basicAuthString= baa.basicAuthString();	
			
		
		} catch (NamingException e1) {
			e1.printStackTrace();
		} 
		try{
		timeout=Integer.parseInt(PropertyLoader.getProperty( "firstlook.flservices.mmr.timeout" ));
		}
		catch(Exception e){
			timeout=15000;
		}
		
	}

	public MMRArea[] getMMRReference() throws Exception
	{
		String url = urlBase + "auctions/mmr/reference";
		HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		System.out.println("Getting MMR Areas : BasicAuthString : "+basicAuthString);
		
		try 
		{
			HttpParams httpParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
			
		    httpClient = new DefaultHttpClient(httpParams);
			
			HttpResponse response = httpClient.execute(request);
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				MMRAreaResponse areaResp = gson.fromJson(reader, MMRAreaResponse.class);
				return areaResp.areas;
			}

			System.err.println("MMRServiceClient.getMMRReference() : Error getting MMR reference.  " + buildLogMsg(url, response));
			log.error("MMRServiceClient.getMMRReference() : Error getting MMR reference.  " + buildLogMsg(url, response));
			throw new Exception("We were unable to connect to Manheim.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("MMRServiceClient.getMMRReference() : Exception getting MMR reference" + buildLogMsg(url, null) + e.getMessage());
			log.error("MMRServiceClientgetMMRReference() : Exception getting MMR reference" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public MMRTrim[] getMMRTrims(String vin, String area) throws IllegalArgumentException, Exception
	{
		if(vin==null)
		{
			log.error("MMR Service has received the VIN as NULL");
			throw new IllegalArgumentException("MMR Service has received the VIN as NULL. Please enter a valid VIN");
		}
		
		String url = urlBase + "auctions/mmr/vins/" + vin+ "?area=" + area;
		HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		System.out.println("Getting MMR Trims: BasicAuthString : "+basicAuthString);
		try 
		{
			HttpParams httpParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
			
		    httpClient = new DefaultHttpClient(httpParams);
			HttpResponse response = httpClient.execute(request);
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				MMRAuctionsSummaryResponse auctSummResp = gson.fromJson(reader, MMRAuctionsSummaryResponse.class);
				return auctSummResp.auctionsSummary;
			}

			System.err.println("MMRServiceClient.getMMRTrims() : Error getting MMR Summary for vin = " + vin + ".  " + buildLogMsg(url, response));
			log.error("MMRServiceClient.getMMRTrims() : Error getting MMR Summary for vin = " + vin + ".  " + buildLogMsg(url, response));
			throw new Exception("Manheim was unable to find this vehicle.");
		} 
		catch (Exception e)
		{
			System.err.println("MMRServiceClient.getMMRTrims() : Error getting MMR Summary for vin = " + vin + ".  " + buildLogMsg(url, null) + e.getMessage() );
			log.error("MMRServiceClient.getMMRTrims() : Exception getting MMR Summary" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public MMRTransactions getMMRTransactions(String trimID, int year, String area) throws IllegalArgumentException, Exception
	{
		String url = urlBase + "auctions/mmr/transactions/trims/" + trimID + "/years/" + year + "?area=" + area;
		HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		System.out.println("Getting MMR Transactions: BasicAuthString : "+basicAuthString);
		
		try 
		{
			
			HttpParams httpParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
			
		    httpClient = new DefaultHttpClient(httpParams);
			HttpResponse response = httpClient.execute(request);
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				Reader reader = new InputStreamReader(response.getEntity().getContent());
				
				Gson gson = new Gson();
				MMRTransactions transactions = gson.fromJson(reader, MMRTransactions.class);
				Transaction [] transArray = transactions.getTransactions();
				//28554- Remove spl char \\ from color
				for(Transaction trans:transArray){
					 if(trans.getColor().contains("\\")) 
					  { 
					 trans.setColor(trans.getColor().replace("\\", "\\\\"));
					  }

				}
				return transactions;
			}

			System.err.println("MMRServiceClient.getMMRTransactions() : Error getting MMR Transactions for vin = " + trimID + " and year = " + year + ".  "  + buildLogMsg(url, response));
			log.error("MMRServiceClient.getMMRTransactions() : Error getting MMR Transactions for vin = " + trimID + " and year = " + year + ".  "  + buildLogMsg(url, response));
			throw new Exception("Manheim does not have any transactions for this vehicle.");
		} 
		catch (Exception e)
		{
			System.err.println("MMRServiceClient.getMMRTransactions() : Error getting MMR Transactions for vin = " + trimID + " and year = " + year + ".  "  + buildLogMsg(url, null) + e.getMessage());
			log.error("MMRServiceClient.getMMRTransactions() : Exception getting MMR Transactions" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	private String buildLogMsg(String url, HttpResponse resp)
	{
		String httpResponse = "";
		
		try {
			if (resp != null)
			{
				httpResponse = " :: httpResponse code = " + resp.getStatusLine().getStatusCode() +
				   " :: httpResponse = " + resp.toString();
			}
		} catch( Exception e ) { System.err.println("buildLogMsg() : Failed ") ;}
		
		return "url = " + url + " :: basicAuthString = " + basicAuthString + httpResponse;
		
	}
	
	private class MMRAreaResponse
	{
		@SerializedName("areas")
		public MMRArea[] areas;		
	}
	
	private class MMRAuctionsSummaryResponse
	{
		@SerializedName("auctionsSummary")
		public MMRTrim[] auctionsSummary;
	}
}
