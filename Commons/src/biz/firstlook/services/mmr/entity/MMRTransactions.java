package biz.firstlook.services.mmr.entity;

import com.google.gson.annotations.SerializedName;

public class MMRTransactions
{
	@SerializedName("trimId")
	private String trimId;
	
	@SerializedName("area")
	private String area;
	
	@SerializedName("year")
	private int year;
	
	@SerializedName("transactions")
	private Transaction[] transactions = {};
	
	public String getTrimId()
	{
		return trimId;
	}

	public void setTrimId(String trimId)
	{
		this.trimId = trimId;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public int getYear()
	{
		return year;
	}

	public void setYear(int year)
	{
		this.year = year;
	}

	public Transaction[] getTransactions()
	{
		return transactions;
	}

	public void setTransactions(Transaction[] transactions)
	{
		this.transactions = transactions;
	}

	public class Transaction
	{
		@SerializedName("date")
		private String date;
		
		@SerializedName("auction")
		private String auction;
		
		@SerializedName("salesType")
		private String salesType;
		
		@SerializedName("price")
		private int price;
		
		@SerializedName("mileage")
		private int mileage;
		
		@SerializedName("condition")
		private String condition;
		
		@SerializedName("color")
		private String color;
		
		@SerializedName("engine")
		private String engine;
		
		@SerializedName("transmission")
		private String transmission;
		
		@SerializedName("inSample")
		private String inSample;
		
		public String getDate()
		{
			return date;
		}

		public void setDate(String date)
		{
			this.date = date;
		}

		public String getAuction()
		{
			return auction;
		}

		public void setAuction(String auction)
		{
			this.auction = auction;
		}

		public String getSalesType()
		{
			return salesType;
		}

		public void setSalesType(String salesType)
		{
			this.salesType = salesType;
		}

		public int getPrice()
		{
			return price;
		}

		public void setPrice(int price)
		{
			this.price = price;
		}

		public int getMileage()
		{
			return mileage;
		}

		public void setMileage(int mileage)
		{
			this.mileage = mileage;
		}

		public String getCondition()
		{
			return condition;
		}

		public void setCondition(String condition)
		{
			this.condition = condition;
		}

		public String getColor()
		{
			return color;
		}

		public void setColor(String color)
		{
			this.color = color;
		}

		public String getEngine()
		{
			return engine;
		}

		public void setEngine(String engine)
		{
			this.engine = engine;
		}

		public String getTransmission()
		{
			return transmission;
		}

		public void setTransmission(String transmission)
		{
			this.transmission = transmission;
		}

		public String getInSample()
		{
			return inSample;
		}

		public void setInSample(String inSample)
		{
			this.inSample = inSample;
		}
	}
}
