package biz.firstlook.services.mmr.entity;

import com.google.gson.annotations.SerializedName;

public class MMRTrim
{
	@SerializedName("trimId")
	private String trimId;
	
	@SerializedName("area")
	private String area;
	
	@SerializedName("year")
	private int year;
	
	@SerializedName("make")
	private String make;
	
	@SerializedName("model")
	private String model;
	
	@SerializedName("trim")
	private String trim;
	
	@SerializedName("mileages")
	private Mileages mileages;
	
	@SerializedName("salesPrices")
	private SalesPrices salesPrices;
	
	@SerializedName("earliestSaleDate")
	private String earliestSaleDate;
	
	@SerializedName("latestSaleDate")
	private String latestSaleDate;
	
	public String getTrimId()
	{
		return trimId;
	}

	public void setTrimId(String trimId)
	{
		this.trimId = trimId;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getMake()
	{
		return make;
	}

	public void setMake(String make)
	{
		this.make = make;
	}

	public String getModel()
	{
		return model;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public String getTrim()
	{
		return trim;
	}

	public void setTrim(String trim)
	{
		this.trim = trim;
	}

	public int getYear()
	{
		return year;
	}

	public void setYear(int year)
	{
		this.year = year;
	}

	public Mileages getMileages()
	{
		return mileages;
	}

	public void setMileages(Mileages mileages)
	{
		this.mileages = mileages;
	}

	public SalesPrices getSalesPrices()
	{
		return salesPrices;
	}

	public void setSalesPrices(SalesPrices salesPrices)
	{
		this.salesPrices = salesPrices;
	}

	public String getEarliestSaleDate()
	{
		return earliestSaleDate;
	}

	public void setEarliestSaleDate(String earliestSaleDate)
	{
		this.earliestSaleDate = earliestSaleDate;
	}

	public String getLatestSaleDate()
	{
		return latestSaleDate;
	}

	public void setLatestSaleDate(String latestSaleDate)
	{
		this.latestSaleDate = latestSaleDate;
	}

	public class Mileages
	{
		@SerializedName("average")
		private int average;

		@SerializedName("min")
		private int min;

		@SerializedName("max")
		private int max;

		@SerializedName("sampleSize")
		private int sampleSize;
		
		public int getAverage()
		{
			return average;
		}

		public void setAverage(int average)
		{
			this.average = average;
		}

		public int getMin()
		{
			return min;
		}

		public void setMin(int min)
		{
			this.min = min;
		}

		public int getMax()
		{
			return max;
		}

		public void setMax(int max)
		{
			this.max = max;
		}
		
		public int getSampleSize()
		{
			return sampleSize;
		}

		public void setSampleSize(int sampleSize)
		{
			this.sampleSize = sampleSize;
		}
	}

	public class SalesPrices
	{
		@SerializedName("average")
		private int average;

		@SerializedName("min")
		private int min;

		@SerializedName("max")
		private int max;

		@SerializedName("sampleSize")
		private int sampleSize;
		
		public int getAverage()
		{
			return average;
		}

		public void setAverage(int average)
		{
			this.average = average;
		}

		public int getMin()
		{
			return min;
		}

		public void setMin(int min)
		{
			this.min = min;
		}

		public int getMax()
		{
			return max;
		}

		public void setMax(int max)
		{
			this.max = max;
		}
		
		public int getSampleSize()
		{
			return sampleSize;
		}

		public void setSampleSize(int sampleSize)
		{
			this.sampleSize = sampleSize;
		}
	}
}
