package biz.firstlook.services.mmr;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTrim;
import biz.firstlook.services.mmr.entity.MMRTransactions.Transaction;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


public class MockMMRServiceClient
{
	static Logger log = Logger.getLogger(MockMMRServiceClient.class);
	String urlBase;
	String basicAuthString;

	// DefaultHttpClient client = null;
	public MockMMRServiceClient(String basicAuthString) throws Exception 
	{
		try {
		     Context initialContext = new javax.naming.InitialContext(); 
		     Context ctx = (Context) initialContext.lookup("java:comp/env");
		     
		     this.urlBase = (String) ctx.lookup("flServiceHostUrlBase");
		}
		catch (NamingException e) 
		{
			 System.err.println(" MMRServiceClient(String basicAuthString): Exception getting mmrUrlBase from server.xml " + e.getMessage());
		     log.error("MMRServiceClient(String basicAuthString): Exception getting mmrUrlBase from server.xml", e);
		     throw new Exception("Could not retrieve Manheim service url base from server.xml - please update server.xml");
		}	
		
		this.basicAuthString = basicAuthString;
	}

	public MMRArea[] getMMRReference() throws Exception
	{
		String url = urlBase + "auctions/mmr/reference";
		HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		try 
		{
//			httpClient = new DefaultHttpClient();
//			HttpResponse response = httpClient.execute(request);
//			
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
//			{
//				Reader reader = new InputStreamReader(response.getEntity().getContent());
//				
				Gson gson = new Gson();
				MMRAreaResponse areaResp = gson.fromJson("{'areas':[{'id':'NA','name':'National'},{'id':'SE','name':'Southeast'},{'id':'NE','name':'Northeast'},{'id':'MW','name':'Midwest'},{'id':'SW','name':'Southwest'},{'id':'WC','name':'West Coast'}]}", MMRAreaResponse.class);
				return areaResp.areas;
//			}
//
//			System.err.println("MMRServiceClient.getMMRReference() : Error getting MMR reference.  " + buildLogMsg(url, response));
//			log.error("MMRServiceClient.getMMRReference() : Error getting MMR reference.  " + buildLogMsg(url, response));
//			throw new Exception("We were unable to connect to Manheim.  Please try again.");
		} 
		catch (Exception e)
		{
			System.err.println("MMRServiceClient.getMMRReference() : Exception getting MMR reference" + buildLogMsg(url, null) + e.getMessage());
			log.error("MMRServiceClientgetMMRReference() : Exception getting MMR reference" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public MMRTrim[] getMMRTrims(String vin, String area) throws IllegalArgumentException, Exception
	{
		if(vin==null)
		{
			log.error("MMR Service has received the VIN as NULL");
			throw new IllegalArgumentException("MMR Service has received the VIN as NULL. Please enter a valid VIN");
		}
		
		String url = urlBase + "auctions/mmr/vins/" + vin+ "?area=" + area;
		HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		try 
		{
//			httpClient = new DefaultHttpClient();
//			HttpResponse response = httpClient.execute(request);
//			
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
//			{
//				Reader reader = new InputStreamReader(response.getEntity().getContent());
//				
				Gson gson = new Gson();
				MMRAuctionsSummaryResponse auctSummResp = gson.fromJson("{'auctionsSummary':[{'trimId':'201301458731389','area':'NA','year':2008,'make':'DODGE','model':'3500 RAM PICKUP 4WD 6TD','trim':'QUAD CAB 6.7L TURBO DSL BIG HORN','mileages':{'average':2130,'min':23,'max':1230,'sampleSize':32213},'salesPrices':{'average':230,'min':234,'max':2340,'sampleSize':234},'earliestSaleDate':'','latestSaleDate':''},{'trimId':'200801426228271','area':'NA','year':2008,'make':'DODGE','model':'3500 RAM PICKUP 4WD 6TD','trim':'QUAD CAB 6.7L TURBO DSL LARAMIE','mileages':{'average':2340,'min':2340,'max':2340,'sampleSize':2340},'salesPrices':{'average':2340,'min':2340,'max':2340,'sampleSize':2340},'earliestSaleDate':'','latestSaleDate':''},{'trimId':'200801426228266','area':'NA','year':2008,'make':'DODGE','model':'3500 RAM PICKUP 4WD 6TD','trim':'QUAD CAB 6.7L TURBO DSL SLT','mileages':{'average':2340,'min':2340,'max':2340,'sampleSize':2340},'salesPrices':{'average':2340,'min':2340,'max':2340,'sampleSize':2340},'earliestSaleDate':'','latestSaleDate':''},{'trimId':'200801426228304','area':'NA','year':2008,'make':'DODGE','model':'3500 RAM PICKUP 4WD 6TD','trim':'QUAD CAB 6.7L TURBO DSL ST','mileages':{'average':2340,'min':2340,'max':2340,'sampleSize':2340},'salesPrices':{'average':2340,'min':2340,'max':2340,'sampleSize':2340},'earliestSaleDate':'','latestSaleDate':''}]}", MMRAuctionsSummaryResponse.class);
//				MMRAuctionsSummaryResponse auctSummResp = gson.fromJson("{'auctionsSummary':[{'trimId':'200801426228778','area':'NA','year':2008,'make':'DODGE','model':'3500 RAM PICKUP 4WD 6TD','trim':'QUAD CAB 6.7L TURBO DSL BIG HORN','mileages':{'average':2130,'min':23,'max':1230,'sampleSize':32213},'salesPrices':{'average':230,'min':234,'max':2340,'sampleSize':234},'earliestSaleDate':'','latestSaleDate':''}]}", MMRAuctionsSummaryResponse.class);
				return auctSummResp.auctionsSummary;
//			}

//			System.err.println("MMRServiceClient.getMMRTrims() : Error getting MMR Summary for vin = " + vin + ".  " + buildLogMsg(url, response));
//			log.error("MMRServiceClient.getMMRTrims() : Error getting MMR Summary for vin = " + vin + ".  " + buildLogMsg(url, response));
//			throw new Exception("Manheim was unable to find this vehicle.");
		} 
		catch (Exception e)
		{
			System.err.println("MMRServiceClient.getMMRTrims() : Error getting MMR Summary for vin = " + vin + ".  " + buildLogMsg(url, null) + e.getMessage() );
			log.error("MMRServiceClient.getMMRTrims() : Exception getting MMR Summary" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	public MMRTransactions getMMRTransactions(String trimID, int year, String area) throws IllegalArgumentException, Exception
	{
		String url = urlBase + "auctions/mmr/transactions/trims/" + trimID + "/years/" + year + "?area=" + area;
		HttpGet request = new HttpGet(url);
		request.addHeader("accept", "application/json");
		request.setHeader("Authorization", "Basic " + basicAuthString);
		
		DefaultHttpClient httpClient = null;
		
		try 
		{
			
//			httpClient = new DefaultHttpClient();
//			HttpResponse response = httpClient.execute(request);
//			
//			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
//			{
//				Reader reader = new InputStreamReader(response.getEntity().getContent());
//				
				Gson gson = new Gson();
				MMRTransactions transactions = gson.fromJson("{'trimId':'200801426228266','area':'NA','year':2008,'transactions':[{'date':'2013-01-03','auction':'OMAHA','salesType':'Regular','price':22500,'mileage':77484,'condition':'Avg','color':'TAN','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-12-26','auction':'ARENA IL','salesType':'Lease','price':21800,'mileage':115692,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-11-07','auction':'DALLAS','salesType':'Regular','price':13500,'mileage':129110,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-11-02','auction':'NEVADA','salesType':'Regular','price':23250,'mileage':52025,'condition':'Avg','color':'RED','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-08-01','auction':'DALLAS','salesType':'Regular','price':24500,'mileage':71521,'condition':'Avg','color':'RED','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-02-16','auction':'OMAHA','salesType':'Regular','price':23200,'mileage':58754,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'6','inSample':'No'},{'date':'2011-12-21','auction':'DENVER','salesType':'Lease','price':9700,'mileage':210843,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'6','inSample':'No'},{'date':'2011-12-06','auction':'DENVER','salesType':'Lease','price':24000,'mileage':35414,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-10-18','auction':'DALLAS','salesType':null,'price':26700,'mileage':29149,'condition':'Avg','color':'TAN','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-09-28','auction':'TUCSON','salesType':'Regular','price':26500,'mileage':46926,'condition':'Avg','color':'GOLD','engine':'6DT','transmission':'M','inSample':'No'},{'date':'2011-08-17','auction':'SAN ANTO','salesType':'Regular','price':18000,'mileage':127093,'condition':'Avg','color':'SILV','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-07-27','auction':'NASHVILL','salesType':'Regular','price':33800,'mileage':67625,'condition':'Avg','color':'MIN GRAY','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-07-06','auction':'NASHVILL','salesType':'Regular','price':29000,'mileage':56769,'condition':'Avg','color':'BRT WHTE','engine':'6DT','transmission':'6','inSample':'No'},{'date':'2011-06-15','auction':'DALLAS','salesType':'Regular','price':22700,'mileage':103056,'condition':'Avg','color':'GRAY','engine':'6DT','transmission':'A','inSample':'No'}]}", MMRTransactions.class);
				//MMRTransactions transactions = gson.fromJson("{'trimId':'200801426228266','area':'NA','year':2008,'transactions':[{'date':'2013-01-03','auction':'OMAHA','salesType':'Regular','price':22500,'mileage':77484,'condition':'Avg','color':'TAN','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-12-26','auction':'ARENA IL','salesType':'Lease','price':21800,'mileage':115692,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-11-07','auction':'DALLAS','salesType':'Regular','price':13500,'mileage':129110,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-11-02','auction':'NEVADA','salesType':'Regular','price':23250,'mileage':52025,'condition':'Avg','color':'RED','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-08-01','auction':'DALLAS','salesType':'Regular','price':24500,'mileage':71521,'condition':'Avg','color':'RED','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2012-02-16','auction':'OMAHA','salesType':'Regular','price':23200,'mileage':58754,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'6','inSample':'No'},{'date':'2011-12-21','auction':'DENVER','salesType':'Lease','price':9700,'mileage':210843,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'6','inSample':'No'},{'date':'2011-12-06','auction':'DENVER','salesType':'Lease','price':24000,'mileage':35414,'condition':'Avg','color':'WHITE','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-10-18','auction':'DALLAS','salesType':null,'price':26700,'mileage':29149,'condition':'Avg','color':'TAN','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-09-28','auction':'TUCSON','salesType':'Regular','price':26500,'mileage':46926,'condition':'Avg','color':'GOLD','engine':'6DT','transmission':'M','inSample':'No'},{'date':'2011-08-17','auction':'SAN ANTO','salesType':'Regular','price':18000,'mileage':127093,'condition':'Avg','color':'SILV','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-07-27','auction':'NASHVILL','salesType':'Regular','price':33800,'mileage':67625,'condition':'Avg','color':'MIN GRAY','engine':'6DT','transmission':'A','inSample':'No'},{'date':'2011-07-06','auction':'NASHVILL','salesType':'Regular','price':29000,'mileage':56769,'condition':'Avg','color':'BRT WHTE','engine':'6DT','transmission':'6','inSample':'No'},{'date':'2011-06-15','auction':'DALLAS','salesType':'Regular','pric]}", MMRTransactions.class);
				Transaction [] transArray = transactions.getTransactions();
				//28554- Remove spl char \\ from color
				for(Transaction trans:transArray){
					 if(trans.getColor().contains("\\")) 
					  { 
					 trans.setColor(trans.getColor().replace("\\", "\\\\"));
					  }

				}
				return transactions;
//			}
//
//			System.err.println("MMRServiceClient.getMMRTransactions() : Error getting MMR Transactions for vin = " + trimID + " and year = " + year + ".  "  + buildLogMsg(url, response));
//			log.error("MMRServiceClient.getMMRTransactions() : Error getting MMR Transactions for vin = " + trimID + " and year = " + year + ".  "  + buildLogMsg(url, response));
			//throw new Exception("Manheim does not have any transactions for this vehicle.");
		} 
		catch (Exception e)
		{
			System.err.println("MMRServiceClient.getMMRTransactions() : Error getting MMR Transactions for vin = " + trimID + " and year = " + year + ".  "  + buildLogMsg(url, null) + e.getMessage());
			log.error("MMRServiceClient.getMMRTransactions() : Exception getting MMR Transactions" + buildLogMsg(url, null), e);
			throw new Exception(e.getMessage());
		}
		finally
		{
			if (httpClient != null)
			{
				httpClient.getConnectionManager().shutdown();
			}
		}
	}
	
	private String buildLogMsg(String url, HttpResponse resp)
	{
		String httpResponse = "";
		
		try {
			if (resp != null)
			{
				httpResponse = " :: httpResponse code = " + resp.getStatusLine().getStatusCode() +
				   " :: httpResponse = " + resp.toString();
			}
		} catch( Exception e ) { System.err.println("buildLogMsg() : Failed ") ;}
		
		return "url = " + url + " :: basicAuthString = " + basicAuthString + httpResponse;
		
	}
	
	private class MMRAreaResponse
	{
		@SerializedName("areas")
		public MMRArea[] areas;		
	}
	
	private class MMRAuctionsSummaryResponse
	{
		@SerializedName("auctionsSummary")
		public MMRTrim[] auctionsSummary;
	}
}
