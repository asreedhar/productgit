
package biz.firstlook.services.mmr.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MMRRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MMRRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}BookRequest">
 *       &lt;sequence>
 *         &lt;element name="RegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UseSeasonalAdjustment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MMRRequest", propOrder = {
    "regionCode",
    "mid",
    "useSeasonalAdjustment"
})
public class MMRRequest
    extends BookRequest
{

    @XmlElement(name = "RegionCode")
    protected String regionCode;
    @XmlElement(name = "MID")
    protected String mid;
    @XmlElement(name = "UseSeasonalAdjustment")
    protected boolean useSeasonalAdjustment;

    /**
     * Gets the value of the regionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionCode() {
        return regionCode;
    }

    /**
     * Sets the value of the regionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionCode(String value) {
        this.regionCode = value;
    }

    /**
     * Gets the value of the mid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMID() {
        return mid;
    }

    /**
     * Sets the value of the mid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMID(String value) {
        this.mid = value;
    }

    /**
     * Gets the value of the useSeasonalAdjustment property.
     * 
     */
    public boolean isUseSeasonalAdjustment() {
        return useSeasonalAdjustment;
    }

    /**
     * Sets the value of the useSeasonalAdjustment property.
     * 
     */
    public void setUseSeasonalAdjustment(boolean value) {
        this.useSeasonalAdjustment = value;
    }

}
