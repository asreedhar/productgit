
package biz.firstlook.services.mmr.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateAveragePriceResult" type="{http://tempuri.org/}MMRResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateAveragePriceResult"
})
@XmlRootElement(name = "UpdateAveragePriceResponse")
public class UpdateAveragePriceResponse {

    @XmlElement(name = "UpdateAveragePriceResult")
    protected MMRResult updateAveragePriceResult;

    /**
     * Gets the value of the updateAveragePriceResult property.
     * 
     * @return
     *     possible object is
     *     {@link MMRResult }
     *     
     */
    public MMRResult getUpdateAveragePriceResult() {
        return updateAveragePriceResult;
    }

    /**
     * Sets the value of the updateAveragePriceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link MMRResult }
     *     
     */
    public void setUpdateAveragePriceResult(MMRResult value) {
        this.updateAveragePriceResult = value;
    }

}
