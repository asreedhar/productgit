
package biz.firstlook.services.mmr.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MMRInventoryRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MMRInventoryRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}MMRRequest">
 *       &lt;sequence>
 *         &lt;element name="InventoryId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="VehicleYear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsMultipleVin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDecodingError" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DecodeVinError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDbUpdateRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MMRInventoryRequest", propOrder = {
    "inventoryId",
    "vehicleYear",
    "isMultipleVin",
    "isDecodingError",
    "decodeVinError",
    "isDbUpdateRequired"
})
public class MMRInventoryRequest
    extends MMRRequest
{

    @XmlElement(name = "InventoryId")
    protected int inventoryId;
    @XmlElement(name = "VehicleYear")
    protected int vehicleYear;
    @XmlElement(name = "IsMultipleVin")
    protected boolean isMultipleVin;
    @XmlElement(name = "IsDecodingError")
    protected boolean isDecodingError;
    @XmlElement(name = "DecodeVinError")
    protected String decodeVinError;
    @XmlElement(name = "IsDbUpdateRequired")
    protected boolean isDbUpdateRequired;

    /**
     * Gets the value of the inventoryId property.
     * 
     */
    public int getInventoryId() {
        return inventoryId;
    }

    /**
     * Sets the value of the inventoryId property.
     * 
     */
    public void setInventoryId(int value) {
        this.inventoryId = value;
    }

    /**
     * Gets the value of the vehicleYear property.
     * 
     */
    public int getVehicleYear() {
        return vehicleYear;
    }

    /**
     * Sets the value of the vehicleYear property.
     * 
     */
    public void setVehicleYear(int value) {
        this.vehicleYear = value;
    }

    /**
     * Gets the value of the isMultipleVin property.
     * 
     */
    public boolean isIsMultipleVin() {
        return isMultipleVin;
    }

    /**
     * Sets the value of the isMultipleVin property.
     * 
     */
    public void setIsMultipleVin(boolean value) {
        this.isMultipleVin = value;
    }

    /**
     * Gets the value of the isDecodingError property.
     * 
     */
    public boolean isIsDecodingError() {
        return isDecodingError;
    }

    /**
     * Sets the value of the isDecodingError property.
     * 
     */
    public void setIsDecodingError(boolean value) {
        this.isDecodingError = value;
    }

    /**
     * Gets the value of the decodeVinError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecodeVinError() {
        return decodeVinError;
    }

    /**
     * Sets the value of the decodeVinError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecodeVinError(String value) {
        this.decodeVinError = value;
    }

    /**
     * Gets the value of the isDbUpdateRequired property.
     * 
     */
    public boolean isIsDbUpdateRequired() {
        return isDbUpdateRequired;
    }

    /**
     * Sets the value of the isDbUpdateRequired property.
     * 
     */
    public void setIsDbUpdateRequired(boolean value) {
        this.isDbUpdateRequired = value;
    }

}
