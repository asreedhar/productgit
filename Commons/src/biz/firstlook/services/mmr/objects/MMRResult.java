
package biz.firstlook.services.mmr.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MMRResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MMRResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}QueueResult">
 *       &lt;sequence>
 *         &lt;element name="InventoryID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AveragePrice" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MMRResult", propOrder = {
    "inventoryID",
    "mid",
    "averagePrice",
    "region"
})
public class MMRResult
    extends QueueResult
{

    @XmlElement(name = "InventoryID")
    protected int inventoryID;
    @XmlElement(name = "MID")
    protected String mid;
    @XmlElement(name = "AveragePrice")
    protected int averagePrice;
    @XmlElement(name = "Region")
    protected String region;

    /**
     * Gets the value of the inventoryID property.
     * 
     */
    public int getInventoryID() {
        return inventoryID;
    }

    /**
     * Sets the value of the inventoryID property.
     * 
     */
    public void setInventoryID(int value) {
        this.inventoryID = value;
    }

    /**
     * Gets the value of the mid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMID() {
        return mid;
    }

    /**
     * Sets the value of the mid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMID(String value) {
        this.mid = value;
    }

    /**
     * Gets the value of the averagePrice property.
     * 
     */
    public int getAveragePrice() {
        return averagePrice;
    }

    /**
     * Sets the value of the averagePrice property.
     * 
     */
    public void setAveragePrice(int value) {
        this.averagePrice = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

}
