
package biz.firstlook.services.mmr.objects;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserIdentity_QNAME = new QName("http://tempuri.org/", "UserIdentity");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateAveragePriceResponse }
     * 
     */
    public UpdateAveragePriceResponse createUpdateAveragePriceResponse() {
        return new UpdateAveragePriceResponse();
    }

    /**
     * Create an instance of {@link PushPeriodicDataToQueue }
     * 
     */
    public PushPeriodicDataToQueue createPushPeriodicDataToQueue() {
        return new PushPeriodicDataToQueue();
    }

    /**
     * Create an instance of {@link MMRRequest }
     * 
     */
    public MMRRequest createMMRRequest() {
        return new MMRRequest();
    }

    /**
     * Create an instance of {@link DecodeVINResponse }
     * 
     */
    public DecodeVINResponse createDecodeVINResponse() {
        return new DecodeVINResponse();
    }

    /**
     * Create an instance of {@link DecodeVIN }
     * 
     */
    public DecodeVIN createDecodeVIN() {
        return new DecodeVIN();
    }

    /**
     * Create an instance of {@link BookRequest }
     * 
     */
    public BookRequest createBookRequest() {
        return new BookRequest();
    }

    /**
     * Create an instance of {@link PushPeriodicDataToQueueResponse }
     * 
     */
    public PushPeriodicDataToQueueResponse createPushPeriodicDataToQueueResponse() {
        return new PushPeriodicDataToQueueResponse();
    }

    /**
     * Create an instance of {@link UpdateMMRAveragePriceFromQueueResponse }
     * 
     */
    public UpdateMMRAveragePriceFromQueueResponse createUpdateMMRAveragePriceFromQueueResponse() {
        return new UpdateMMRAveragePriceFromQueueResponse();
    }

    /**
     * Create an instance of {@link QueueResult }
     * 
     */
    public QueueResult createQueueResult() {
        return new QueueResult();
    }

    /**
     * Create an instance of {@link PurgeFailureQueue }
     * 
     */
    public PurgeFailureQueue createPurgeFailureQueue() {
        return new PurgeFailureQueue();
    }

    /**
     * Create an instance of {@link UserIdentity }
     * 
     */
    public UserIdentity createUserIdentity() {
        return new UserIdentity();
    }

    /**
     * Create an instance of {@link UpdateMMRAveragePriceFromQueue }
     * 
     */
    public UpdateMMRAveragePriceFromQueue createUpdateMMRAveragePriceFromQueue() {
        return new UpdateMMRAveragePriceFromQueue();
    }

    /**
     * Create an instance of {@link MMRResult }
     * 
     */
    public MMRResult createMMRResult() {
        return new MMRResult();
    }

    /**
     * Create an instance of {@link PurgeSuccessQueue }
     * 
     */
    public PurgeSuccessQueue createPurgeSuccessQueue() {
        return new PurgeSuccessQueue();
    }

    /**
     * Create an instance of {@link MMRInventoryRequest }
     * 
     */
    public MMRInventoryRequest createMMRInventoryRequest() {
        return new MMRInventoryRequest();
    }

    /**
     * Create an instance of {@link PurgeSuccessQueueResponse }
     * 
     */
    public PurgeSuccessQueueResponse createPurgeSuccessQueueResponse() {
        return new PurgeSuccessQueueResponse();
    }

    /**
     * Create an instance of {@link UpdateAveragePrice }
     * 
     */
    public UpdateAveragePrice createUpdateAveragePrice() {
        return new UpdateAveragePrice();
    }

    /**
     * Create an instance of {@link PurgeFailureQueueResponse }
     * 
     */
    public PurgeFailureQueueResponse createPurgeFailureQueueResponse() {
        return new PurgeFailureQueueResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserIdentity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "UserIdentity")
    public JAXBElement<UserIdentity> createUserIdentity(UserIdentity value) {
        return new JAXBElement<UserIdentity>(_UserIdentity_QNAME, UserIdentity.class, null, value);
    }

}
