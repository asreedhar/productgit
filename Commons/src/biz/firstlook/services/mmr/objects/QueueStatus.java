
package biz.firstlook.services.mmr.objects;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for QueueStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="QueueStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Awaiting"/>
 *     &lt;enumeration value="Processing"/>
 *     &lt;enumeration value="Success"/>
 *     &lt;enumeration value="Error"/>
 *     &lt;enumeration value="MMRError"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum QueueStatus {

    @XmlEnumValue("Awaiting")
    AWAITING("Awaiting"),
    @XmlEnumValue("Error")
    ERROR("Error"),
    @XmlEnumValue("MMRError")
    MMR_ERROR("MMRError"),
    @XmlEnumValue("Processing")
    PROCESSING("Processing"),
    @XmlEnumValue("Success")
    SUCCESS("Success");
    private final String value;

    QueueStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QueueStatus fromValue(String v) {
        for (QueueStatus c: QueueStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
