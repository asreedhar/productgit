
package biz.firstlook.services.mmr;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import biz.firstlook.services.mmr.objects.MMRInventoryRequest;
import biz.firstlook.services.mmr.objects.MMRResult;

@WebService(name = "ManheimQueueSoap", targetNamespace = "http://tempuri.org/")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface ManheimQueueSoap {


    @WebMethod(operationName = "PushPeriodicDataToQueue", action = "http://tempuri.org/PushPeriodicDataToQueue")
    public void pushPeriodicDataToQueue(
        @WebParam(name = "days", targetNamespace = "http://tempuri.org/")
        int days,
        @WebParam(name = "UserIdentity", targetNamespace = "http://tempuri.org/", header = true)
        biz.firstlook.services.mmr.objects.UserIdentity UserIdentity);

    @WebMethod(operationName = "DecodeVIN", action = "http://tempuri.org/DecodeVIN")
    public void decodeVIN(
        @WebParam(name = "vin", targetNamespace = "http://tempuri.org/")
        String vin,
        @WebParam(name = "UserIdentity", targetNamespace = "http://tempuri.org/", header = true)
        biz.firstlook.services.mmr.objects.UserIdentity UserIdentity);

    @WebMethod(operationName = "UpdateMMRAveragePriceFromQueue", action = "http://tempuri.org/UpdateMMRAveragePriceFromQueue")
    public void updateMMRAveragePriceFromQueue(
        @WebParam(name = "recordCount", targetNamespace = "http://tempuri.org/")
        int recordCount,
        @WebParam(name = "UserIdentity", targetNamespace = "http://tempuri.org/", header = true)
        biz.firstlook.services.mmr.objects.UserIdentity UserIdentity);

    @WebMethod(operationName = "PurgeSuccessQueue", action = "http://tempuri.org/PurgeSuccessQueue")
    public void purgeSuccessQueue(
        @WebParam(name = "days", targetNamespace = "http://tempuri.org/")
        int days,
        @WebParam(name = "UserIdentity", targetNamespace = "http://tempuri.org/", header = true)
        biz.firstlook.services.mmr.objects.UserIdentity UserIdentity);

    @WebMethod(operationName = "UpdateAveragePrice", action = "http://tempuri.org/UpdateAveragePrice")
    @WebResult(name = "UpdateAveragePriceResult", targetNamespace = "http://tempuri.org/")
    public MMRResult updateAveragePrice(
        @WebParam(name = "request", targetNamespace = "http://tempuri.org/")
        MMRInventoryRequest request,
        @WebParam(name = "UserIdentity", targetNamespace = "http://tempuri.org/", header = true)
        biz.firstlook.services.mmr.objects.UserIdentity UserIdentity);

    @WebMethod(operationName = "PurgeFailureQueue", action = "http://tempuri.org/PurgeFailureQueue")
    public void purgeFailureQueue(
        @WebParam(name = "days", targetNamespace = "http://tempuri.org/")
        int days,
        @WebParam(name = "UserIdentity", targetNamespace = "http://tempuri.org/", header = true)
        biz.firstlook.services.mmr.objects.UserIdentity UserIdentity);

}
