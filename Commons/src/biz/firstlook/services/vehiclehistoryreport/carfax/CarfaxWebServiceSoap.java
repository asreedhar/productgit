
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "CarfaxWebServiceSoap", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface CarfaxWebServiceSoap {


    @WebMethod(operationName = "PurchaseReports", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/PurchaseReports")
    public void purchaseReports(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "vehicleEntityType", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        VehicleEntityType vehicleEntityType,
        @WebParam(name = "reportType", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        CarfaxReportType reportType,
        @WebParam(name = "displayInHotList", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        boolean displayInHotList,
        @WebParam(name = "trackingCode", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        CarfaxTrackingCode trackingCode,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

    @WebMethod(operationName = "PurchaseReport", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/PurchaseReport")
    @WebResult(name = "PurchaseReportResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
    public CarfaxReportTO purchaseReport(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "vin", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        String vin,
        @WebParam(name = "reportType", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        CarfaxReportType reportType,
        @WebParam(name = "displayInHotList", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        boolean displayInHotList,
        @WebParam(name = "trackingCode", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        CarfaxTrackingCode trackingCode,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

    @WebMethod(operationName = "GetReports", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/GetReports")
    @WebResult(name = "GetReportsResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
    public ArrayOfCarfaxReportTO getReports(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "vehicleEntityType", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        VehicleEntityType vehicleEntityType,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

    @WebMethod(operationName = "GetReport", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/GetReport")
    @WebResult(name = "GetReportResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
    public CarfaxReportTO getReport(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "vin", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        String vin,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

    @WebMethod(operationName = "HasAccount", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/HasAccount")
    @WebResult(name = "HasAccountResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
    public boolean hasAccount(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

    @WebMethod(operationName = "CanPurchaseReport", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/CanPurchaseReport")
    @WebResult(name = "CanPurchaseReportResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
    public boolean canPurchaseReport(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

    @WebMethod(operationName = "GetReportPreference", action = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/GetReportPreference")
    @WebResult(name = "GetReportPreferenceResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
    public CarfaxReportPreferenceTO getReportPreference(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        int dealerId,
        @WebParam(name = "vehicleEntityType", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")
        VehicleEntityType vehicleEntityType,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", header = true)
        biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity);

}
