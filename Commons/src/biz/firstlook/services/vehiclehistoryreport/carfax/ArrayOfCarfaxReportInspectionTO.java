
package biz.firstlook.services.vehiclehistoryreport.carfax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCarfaxReportInspectionTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCarfaxReportInspectionTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarfaxReportInspectionTO" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportInspectionTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCarfaxReportInspectionTO", propOrder = {
    "carfaxReportInspectionTO"
})
public class ArrayOfCarfaxReportInspectionTO  implements Serializable {

    @XmlElement(name = "CarfaxReportInspectionTO", required = true, nillable = true)
    protected List<CarfaxReportInspectionTO> carfaxReportInspectionTO;

    /**
     * Gets the value of the carfaxReportInspectionTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the carfaxReportInspectionTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCarfaxReportInspectionTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CarfaxReportInspectionTO }
     * 
     * 
     */
    public List<CarfaxReportInspectionTO> getCarfaxReportInspectionTO() {
        if (carfaxReportInspectionTO == null) {
            carfaxReportInspectionTO = new ArrayList<CarfaxReportInspectionTO>();
        }
        return this.carfaxReportInspectionTO;
    }

}
