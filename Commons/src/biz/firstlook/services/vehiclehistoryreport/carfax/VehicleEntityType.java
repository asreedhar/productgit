
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for VehicleEntityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VehicleEntityType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Undefined"/>
 *     &lt;enumeration value="Inventory"/>
 *     &lt;enumeration value="Appraisal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum VehicleEntityType {

    @XmlEnumValue("Appraisal")
    APPRAISAL("Appraisal"),
    @XmlEnumValue("Inventory")
    INVENTORY("Inventory"),
    @XmlEnumValue("Undefined")
    UNDEFINED("Undefined");
    private final String value;

    VehicleEntityType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VehicleEntityType fromValue(String v) {
        for (VehicleEntityType c: VehicleEntityType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
