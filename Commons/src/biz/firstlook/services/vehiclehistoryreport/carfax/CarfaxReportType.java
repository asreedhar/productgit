
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for CarfaxReportType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CarfaxReportType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Undefined"/>
 *     &lt;enumeration value="BTC"/>
 *     &lt;enumeration value="VHR"/>
 *     &lt;enumeration value="CIP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum CarfaxReportType {

    BTC("BTC"),
    CIP("CIP"),
    @XmlEnumValue("Undefined")
    UNDEFINED("Undefined"),
    VHR("VHR");
    private final String value;

    CarfaxReportType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarfaxReportType fromValue(String v) {
        for (CarfaxReportType c: CarfaxReportType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
