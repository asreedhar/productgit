
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.jws.WebService;

@WebService(serviceName = "CarfaxWebService", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", endpointInterface = "biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxWebServiceSoap")
public class CarfaxWebServiceImpl
    implements CarfaxWebServiceSoap
{


    public void purchaseReports(int dealerId, VehicleEntityType vehicleEntityType, CarfaxReportType reportType, boolean displayInHotList, CarfaxTrackingCode trackingCode, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public CarfaxReportTO purchaseReport(int dealerId, String vin, CarfaxReportType reportType, boolean displayInHotList, CarfaxTrackingCode trackingCode, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public ArrayOfCarfaxReportTO getReports(int dealerId, VehicleEntityType vehicleEntityType, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public CarfaxReportTO getReport(int dealerId, String vin, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public boolean hasAccount(int dealerId, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public boolean canPurchaseReport(int dealerId, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public CarfaxReportPreferenceTO getReportPreference(int dealerId, VehicleEntityType vehicleEntityType, biz.firstlook.services.vehiclehistoryreport.carfax.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

}
