
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CarfaxReportPreferenceTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CarfaxReportPreferenceTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DisplayInHotListings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PurchaseReport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ReportType" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarfaxReportPreferenceTO", propOrder = {
    "displayInHotListings",
    "purchaseReport",
    "reportType"
})
public class CarfaxReportPreferenceTO {

    @XmlElement(name = "DisplayInHotListings")
    protected boolean displayInHotListings;
    @XmlElement(name = "PurchaseReport")
    protected boolean purchaseReport;
    @XmlElement(name = "ReportType", required = true)
    protected CarfaxReportType reportType;

    /**
     * Gets the value of the displayInHotListings property.
     * 
     */
    public boolean isDisplayInHotListings() {
        return displayInHotListings;
    }

    /**
     * Sets the value of the displayInHotListings property.
     * 
     */
    public void setDisplayInHotListings(boolean value) {
        this.displayInHotListings = value;
    }

    /**
     * Gets the value of the purchaseReport property.
     * 
     */
    public boolean isPurchaseReport() {
        return purchaseReport;
    }

    /**
     * Sets the value of the purchaseReport property.
     * 
     */
    public void setPurchaseReport(boolean value) {
        this.purchaseReport = value;
    }

    /**
     * Gets the value of the reportType property.
     * 
     * @return
     *     possible object is
     *     {@link CarfaxReportType }
     *     
     */
    public CarfaxReportType getReportType() {
        return reportType;
    }

    /**
     * Sets the value of the reportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarfaxReportType }
     *     
     */
    public void setReportType(CarfaxReportType value) {
        this.reportType = value;
    }

}
