
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.services.vehiclehistoryreport.carfax package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserIdentity_QNAME = new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "UserIdentity");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.services.vehiclehistoryreport.carfax
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetReports }
     * 
     */
    public GetReports createGetReports() {
        return new GetReports();
    }

    /**
     * Create an instance of {@link ArrayOfCarfaxReportTO }
     * 
     */
    public ArrayOfCarfaxReportTO createArrayOfCarfaxReportTO() {
        return new ArrayOfCarfaxReportTO();
    }

    /**
     * Create an instance of {@link GetReportResponse }
     * 
     */
    public GetReportResponse createGetReportResponse() {
        return new GetReportResponse();
    }

    /**
     * Create an instance of {@link GetReport }
     * 
     */
    public GetReport createGetReport() {
        return new GetReport();
    }

    /**
     * Create an instance of {@link HasAccount }
     * 
     */
    public HasAccount createHasAccount() {
        return new HasAccount();
    }

    /**
     * Create an instance of {@link CanPurchaseReport }
     * 
     */
    public CanPurchaseReport createCanPurchaseReport() {
        return new CanPurchaseReport();
    }

    /**
     * Create an instance of {@link PurchaseReport }
     * 
     */
    public PurchaseReport createPurchaseReport() {
        return new PurchaseReport();
    }

    /**
     * Create an instance of {@link GetReportsResponse }
     * 
     */
    public GetReportsResponse createGetReportsResponse() {
        return new GetReportsResponse();
    }

    /**
     * Create an instance of {@link PurchaseReportsResponse }
     * 
     */
    public PurchaseReportsResponse createPurchaseReportsResponse() {
        return new PurchaseReportsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCarfaxReportInspectionTO }
     * 
     */
    public ArrayOfCarfaxReportInspectionTO createArrayOfCarfaxReportInspectionTO() {
        return new ArrayOfCarfaxReportInspectionTO();
    }

    /**
     * Create an instance of {@link UserIdentity }
     * 
     */
    public UserIdentity createUserIdentity() {
        return new UserIdentity();
    }

    /**
     * Create an instance of {@link CanPurchaseReportResponse }
     * 
     */
    public CanPurchaseReportResponse createCanPurchaseReportResponse() {
        return new CanPurchaseReportResponse();
    }

    /**
     * Create an instance of {@link HasAccountResponse }
     * 
     */
    public HasAccountResponse createHasAccountResponse() {
        return new HasAccountResponse();
    }

    /**
     * Create an instance of {@link CarfaxReportInspectionTO }
     * 
     */
    public CarfaxReportInspectionTO createCarfaxReportInspectionTO() {
        return new CarfaxReportInspectionTO();
    }

    /**
     * Create an instance of {@link CarfaxReportTO }
     * 
     */
    public CarfaxReportTO createCarfaxReportTO() {
        return new CarfaxReportTO();
    }

    /**
     * Create an instance of {@link GetReportPreferenceResponse }
     * 
     */
    public GetReportPreferenceResponse createGetReportPreferenceResponse() {
        return new GetReportPreferenceResponse();
    }

    /**
     * Create an instance of {@link CarfaxReportPreferenceTO }
     * 
     */
    public CarfaxReportPreferenceTO createCarfaxReportPreferenceTO() {
        return new CarfaxReportPreferenceTO();
    }

    /**
     * Create an instance of {@link PurchaseReportResponse }
     * 
     */
    public PurchaseReportResponse createPurchaseReportResponse() {
        return new PurchaseReportResponse();
    }

    /**
     * Create an instance of {@link PurchaseReports }
     * 
     */
    public PurchaseReports createPurchaseReports() {
        return new PurchaseReports();
    }

    /**
     * Create an instance of {@link GetReportPreference }
     * 
     */
    public GetReportPreference createGetReportPreference() {
        return new GetReportPreference();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserIdentity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/", name = "UserIdentity")
    public JAXBElement<UserIdentity> createUserIdentity(UserIdentity value) {
        return new JAXBElement<UserIdentity>(_UserIdentity_QNAME, UserIdentity.class, null, value);
    }

}
