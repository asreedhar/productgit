
package biz.firstlook.services.vehiclehistoryreport.carfax;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class CarfaxWebServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private Map<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public CarfaxWebServiceClient() {
        create0();
        Endpoint CarfaxWebServiceSoapLocalEndpointEP = service0 .addEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoapLocalEndpoint"), new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoapLocalBinding"), "xfire.local://CarfaxWebService");
        endpoints.put(new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoapLocalEndpoint"), CarfaxWebServiceSoapLocalEndpointEP);
        Endpoint CarfaxWebServiceSoapEP = service0 .addEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoap"), new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoap"), "http://localhost:3880/VehicleHistoryReport/Services/CarfaxWebService.asmx");
        endpoints.put(new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoap"), CarfaxWebServiceSoapEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    @SuppressWarnings("unchecked")
    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxWebServiceSoap.class), props);
        asf.createSoap11Binding(service0, new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoapLocalBinding"), "urn:xfire:transport:local");
        asf.createSoap11Binding(service0, new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoap"), "http://schemas.xmlsoap.org/soap/http");
    }

    public CarfaxWebServiceSoap getCarfaxWebServiceSoapLocalEndpoint() {
        return ((CarfaxWebServiceSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoapLocalEndpoint")));
    }

    public CarfaxWebServiceSoap getCarfaxWebServiceSoapLocalEndpoint(String url) {
        CarfaxWebServiceSoap var = getCarfaxWebServiceSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public CarfaxWebServiceSoap getCarfaxWebServiceSoap() {
        return ((CarfaxWebServiceSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/Carfax/", "CarfaxWebServiceSoap")));
    }

    public CarfaxWebServiceSoap getCarfaxWebServiceSoap(String url) {
        CarfaxWebServiceSoap var = getCarfaxWebServiceSoap();
        
        Client client = Client.getInstance(var);
        client.setUrl(url);
        
        //Tell XFire to cache a DOM document for the various in/out/fault flows
        //This is required to enable logging
        client.addInHandler(new org.codehaus.xfire.util.dom.DOMInHandler());
        client.addOutHandler(new org.codehaus.xfire.util.dom.DOMOutHandler());
        client.addFaultHandler(new org.codehaus.xfire.util.dom.DOMInHandler());
        
        //Add a logging handler to each flow
        //to see the msgs, log4j.logger.org.codehaus.xfire=INFO
        client.addInHandler(new org.codehaus.xfire.util.LoggingHandler());
        client.addOutHandler(new org.codehaus.xfire.util.LoggingHandler());
        client.addFaultHandler(new org.codehaus.xfire.util.LoggingHandler());
        
        //10 minute timeout.  on avg, takes ~1 sec per vehicle
        //240 vehicle inventory ~= 4 min, so doubling/rounding up for safety.
        client.setTimeout(10 * 60 *1000);
        
        return var;
    }

}
