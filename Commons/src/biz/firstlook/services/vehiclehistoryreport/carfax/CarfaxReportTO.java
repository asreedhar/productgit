
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;


/**
 * <p>Java class for CarfaxReportTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CarfaxReportTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Vin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportType" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportType"/>
 *         &lt;element name="DisplayInHotList" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasProblem" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="OwnerCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Inspections" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}ArrayOfCarfaxReportInspectionTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarfaxReportTO", propOrder = {
    "expirationDate",
    "userName",
    "vin",
    "reportType",
    "displayInHotList",
    "hasProblem",
    "ownerCount",
    "inspections"
})
public class CarfaxReportTO implements Serializable {

    @XmlElement(name = "ExpirationDate", required = true)
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "Vin")
    protected String vin;
    @XmlElement(name = "ReportType", required = true)
    protected CarfaxReportType reportType;
    @XmlElement(name = "DisplayInHotList")
    protected boolean displayInHotList;
    @XmlElement(name = "HasProblem")
    protected boolean hasProblem;
    @XmlElement(name = "OwnerCount")
    protected int ownerCount;
    @XmlElement(name = "Inspections")
    protected ArrayOfCarfaxReportInspectionTO inspections;

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVin(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the reportType property.
     * 
     * @return
     *     possible object is
     *     {@link CarfaxReportType }
     *     
     */
    public CarfaxReportType getReportType() {
        return reportType;
    }

    /**
     * Sets the value of the reportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarfaxReportType }
     *     
     */
    public void setReportType(CarfaxReportType value) {
        this.reportType = value;
    }

    /**
     * Gets the value of the displayInHotList property.
     * 
     */
    public boolean isDisplayInHotList() {
        return displayInHotList;
    }

    /**
     * Sets the value of the displayInHotList property.
     * 
     */
    public void setDisplayInHotList(boolean value) {
        this.displayInHotList = value;
    }

    /**
     * Gets the value of the hasProblem property.
     * 
     */
    public boolean isHasProblem() {
        return hasProblem;
    }

    /**
     * Sets the value of the hasProblem property.
     * 
     */
    public void setHasProblem(boolean value) {
        this.hasProblem = value;
    }

    /**
     * Gets the value of the ownerCount property.
     * 
     */
    public int getOwnerCount() {
        return ownerCount;
    }

    /**
     * Sets the value of the ownerCount property.
     * 
     */
    public void setOwnerCount(int value) {
        this.ownerCount = value;
    }

    /**
     * Gets the value of the inspections property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCarfaxReportInspectionTO }
     *     
     */
    public ArrayOfCarfaxReportInspectionTO getInspections() {
        return inspections;
    }

    /**
     * Sets the value of the inspections property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCarfaxReportInspectionTO }
     *     
     */
    public void setInspections(ArrayOfCarfaxReportInspectionTO value) {
        this.inspections = value;
    }

}
