
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetReportsResult" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}ArrayOfCarfaxReportTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReportsResult"
})
@XmlRootElement(name = "GetReportsResponse")
public class GetReportsResponse {

    @XmlElement(name = "GetReportsResult")
    protected ArrayOfCarfaxReportTO getReportsResult;

    /**
     * Gets the value of the getReportsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCarfaxReportTO }
     *     
     */
    public ArrayOfCarfaxReportTO getGetReportsResult() {
        return getReportsResult;
    }

    /**
     * Sets the value of the getReportsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCarfaxReportTO }
     *     
     */
    public void setGetReportsResult(ArrayOfCarfaxReportTO value) {
        this.getReportsResult = value;
    }

}
