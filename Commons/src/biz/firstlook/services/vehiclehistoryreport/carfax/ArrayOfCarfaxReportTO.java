
package biz.firstlook.services.vehiclehistoryreport.carfax;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCarfaxReportTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCarfaxReportTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarfaxReportTO" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCarfaxReportTO", propOrder = {
    "carfaxReportTO"
})
public class ArrayOfCarfaxReportTO {

    @XmlElement(name = "CarfaxReportTO", required = true, nillable = true)
    protected List<CarfaxReportTO> carfaxReportTO;

    /**
     * Gets the value of the carfaxReportTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the carfaxReportTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCarfaxReportTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CarfaxReportTO }
     * 
     * 
     */
    public List<CarfaxReportTO> getCarfaxReportTO() {
        if (carfaxReportTO == null) {
            carfaxReportTO = new ArrayList<CarfaxReportTO>();
        }
        return this.carfaxReportTO;
    }

}
