
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for CarfaxTrackingCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CarfaxTrackingCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Undefined"/>
 *     &lt;enumeration value="FLA"/>
 *     &lt;enumeration value="FLN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum CarfaxTrackingCode {

    FLA("FLA"),
    FLN("FLN"),
    @XmlEnumValue("Undefined")
    UNDEFINED("Undefined");
    private final String value;

    CarfaxTrackingCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CarfaxTrackingCode fromValue(String v) {
        for (CarfaxTrackingCode c: CarfaxTrackingCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
