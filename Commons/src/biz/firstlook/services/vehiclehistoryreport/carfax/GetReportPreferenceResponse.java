
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetReportPreferenceResult" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportPreferenceTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReportPreferenceResult"
})
@XmlRootElement(name = "GetReportPreferenceResponse")
public class GetReportPreferenceResponse {

    @XmlElement(name = "GetReportPreferenceResult")
    protected CarfaxReportPreferenceTO getReportPreferenceResult;

    /**
     * Gets the value of the getReportPreferenceResult property.
     * 
     * @return
     *     possible object is
     *     {@link CarfaxReportPreferenceTO }
     *     
     */
    public CarfaxReportPreferenceTO getGetReportPreferenceResult() {
        return getReportPreferenceResult;
    }

    /**
     * Sets the value of the getReportPreferenceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarfaxReportPreferenceTO }
     *     
     */
    public void setGetReportPreferenceResult(CarfaxReportPreferenceTO value) {
        this.getReportPreferenceResult = value;
    }

}
