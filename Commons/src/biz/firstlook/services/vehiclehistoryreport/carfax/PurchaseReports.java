
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dealerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="vehicleEntityType" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}VehicleEntityType"/>
 *         &lt;element name="reportType" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxReportType"/>
 *         &lt;element name="displayInHotList" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="trackingCode" type="{http://services.firstlook.biz/VehicleHistoryReport/Carfax/}CarfaxTrackingCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dealerId",
    "vehicleEntityType",
    "reportType",
    "displayInHotList",
    "trackingCode"
})
@XmlRootElement(name = "PurchaseReports")
public class PurchaseReports {

    protected int dealerId;
    @XmlElement(required = true)
    protected VehicleEntityType vehicleEntityType;
    @XmlElement(required = true)
    protected CarfaxReportType reportType;
    protected boolean displayInHotList;
    @XmlElement(required = true)
    protected CarfaxTrackingCode trackingCode;

    /**
     * Gets the value of the dealerId property.
     * 
     */
    public int getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     */
    public void setDealerId(int value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the vehicleEntityType property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleEntityType }
     *     
     */
    public VehicleEntityType getVehicleEntityType() {
        return vehicleEntityType;
    }

    /**
     * Sets the value of the vehicleEntityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleEntityType }
     *     
     */
    public void setVehicleEntityType(VehicleEntityType value) {
        this.vehicleEntityType = value;
    }

    /**
     * Gets the value of the reportType property.
     * 
     * @return
     *     possible object is
     *     {@link CarfaxReportType }
     *     
     */
    public CarfaxReportType getReportType() {
        return reportType;
    }

    /**
     * Sets the value of the reportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarfaxReportType }
     *     
     */
    public void setReportType(CarfaxReportType value) {
        this.reportType = value;
    }

    /**
     * Gets the value of the displayInHotList property.
     * 
     */
    public boolean isDisplayInHotList() {
        return displayInHotList;
    }

    /**
     * Sets the value of the displayInHotList property.
     * 
     */
    public void setDisplayInHotList(boolean value) {
        this.displayInHotList = value;
    }

    /**
     * Gets the value of the trackingCode property.
     * 
     * @return
     *     possible object is
     *     {@link CarfaxTrackingCode }
     *     
     */
    public CarfaxTrackingCode getTrackingCode() {
        return trackingCode;
    }

    /**
     * Sets the value of the trackingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarfaxTrackingCode }
     *     
     */
    public void setTrackingCode(CarfaxTrackingCode value) {
        this.trackingCode = value;
    }

}
