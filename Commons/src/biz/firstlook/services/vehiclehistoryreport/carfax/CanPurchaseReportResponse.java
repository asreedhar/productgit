
package biz.firstlook.services.vehiclehistoryreport.carfax;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CanPurchaseReportResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "canPurchaseReportResult"
})
@XmlRootElement(name = "CanPurchaseReportResponse")
public class CanPurchaseReportResponse {

    @XmlElement(name = "CanPurchaseReportResult")
    protected boolean canPurchaseReportResult;

    /**
     * Gets the value of the canPurchaseReportResult property.
     * 
     */
    public boolean isCanPurchaseReportResult() {
        return canPurchaseReportResult;
    }

    /**
     * Sets the value of the canPurchaseReportResult property.
     * 
     */
    public void setCanPurchaseReportResult(boolean value) {
        this.canPurchaseReportResult = value;
    }

}
