
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import javax.jws.WebService;

@WebService(serviceName = "AutoCheckWebService", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", endpointInterface = "biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckWebServiceSoap")
public class AutoCheckWebServiceImpl
    implements AutoCheckWebServiceSoap
{


    public String getReportHtml(int dealerId, String vin, biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public boolean hasAccount(int dealerId, biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public AutoCheckReportTO getReport(int dealerId, String vin, biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public void purchaseReports(int dealerId, VehicleEntityType vehicleEntityType, biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public AutoCheckReportTO purchaseReport(int dealerId, String vin, biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public boolean canPurchaseReport(int dealerId, biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

}
