
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "AutoCheckWebServiceSoap", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface AutoCheckWebServiceSoap {


    @WebMethod(operationName = "GetReportHtml", action = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/GetReportHtml")
    @WebResult(name = "GetReportHtmlResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
    public String getReportHtml(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        int dealerId,
        @WebParam(name = "vin", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        String vin,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", header = true)
        biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity);

    @WebMethod(operationName = "HasAccount", action = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/HasAccount")
    @WebResult(name = "HasAccountResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
    public boolean hasAccount(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        int dealerId,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", header = true)
        biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity);

    @WebMethod(operationName = "GetReport", action = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/GetReport")
    @WebResult(name = "GetReportResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
    public AutoCheckReportTO getReport(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        int dealerId,
        @WebParam(name = "vin", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        String vin,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", header = true)
        biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity);

    @WebMethod(operationName = "PurchaseReports", action = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/PurchaseReports")
    public void purchaseReports(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        int dealerId,
        @WebParam(name = "vehicleEntityType", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        VehicleEntityType vehicleEntityType,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", header = true)
        biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity);

    @WebMethod(operationName = "PurchaseReport", action = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/PurchaseReport")
    @WebResult(name = "PurchaseReportResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
    public AutoCheckReportTO purchaseReport(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        int dealerId,
        @WebParam(name = "vin", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        String vin,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", header = true)
        biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity);

    @WebMethod(operationName = "CanPurchaseReport", action = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/CanPurchaseReport")
    @WebResult(name = "CanPurchaseReportResult", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
    public boolean canPurchaseReport(
        @WebParam(name = "dealerId", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")
        int dealerId,
        @WebParam(name = "UserIdentity", targetNamespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", header = true)
        biz.firstlook.services.vehiclehistoryreport.autocheck.UserIdentity UserIdentity);

}
