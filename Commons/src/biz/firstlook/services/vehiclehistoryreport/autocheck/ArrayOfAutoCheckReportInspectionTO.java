
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAutoCheckReportInspectionTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAutoCheckReportInspectionTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AutoCheckReportInspectionTO" type="{http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}AutoCheckReportInspectionTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAutoCheckReportInspectionTO", propOrder = {
    "autoCheckReportInspectionTO"
})
public class ArrayOfAutoCheckReportInspectionTO  implements Serializable {

    @XmlElement(name = "AutoCheckReportInspectionTO", required = true, nillable = true)
    protected List<AutoCheckReportInspectionTO> autoCheckReportInspectionTO;

    /**
     * Gets the value of the autoCheckReportInspectionTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the autoCheckReportInspectionTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoCheckReportInspectionTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoCheckReportInspectionTO }
     * 
     * 
     */
    public List<AutoCheckReportInspectionTO> getAutoCheckReportInspectionTO() {
        if (autoCheckReportInspectionTO == null) {
            autoCheckReportInspectionTO = new ArrayList<AutoCheckReportInspectionTO>();
        }
        return this.autoCheckReportInspectionTO;
    }

}
