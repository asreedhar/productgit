
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseReportResult" type="{http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}AutoCheckReportTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "purchaseReportResult"
})
@XmlRootElement(name = "PurchaseReportResponse")
public class PurchaseReportResponse {

    @XmlElement(name = "PurchaseReportResult")
    protected AutoCheckReportTO purchaseReportResult;

    /**
     * Gets the value of the purchaseReportResult property.
     * 
     * @return
     *     possible object is
     *     {@link AutoCheckReportTO }
     *     
     */
    public AutoCheckReportTO getPurchaseReportResult() {
        return purchaseReportResult;
    }

    /**
     * Sets the value of the purchaseReportResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoCheckReportTO }
     *     
     */
    public void setPurchaseReportResult(AutoCheckReportTO value) {
        this.purchaseReportResult = value;
    }

}
