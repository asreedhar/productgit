
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class AutoCheckWebServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private Map<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public AutoCheckWebServiceClient() {
        create0();
        Endpoint AutoCheckWebServiceSoapEP = service0 .addEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoap"), new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoap"), "http://localhost:3880/VehicleHistoryReport/Services/AutoCheckWebService.asmx");
        endpoints.put(new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoap"), AutoCheckWebServiceSoapEP);
        Endpoint AutoCheckWebServiceSoapLocalEndpointEP = service0 .addEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoapLocalEndpoint"), new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoapLocalBinding"), "xfire.local://AutoCheckWebService");
        endpoints.put(new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoapLocalEndpoint"), AutoCheckWebServiceSoapLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    @SuppressWarnings("unchecked")
    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckWebServiceSoap.class), props);
        asf.createSoap11Binding(service0, new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoap"), "http://schemas.xmlsoap.org/soap/http");
        asf.createSoap11Binding(service0, new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoapLocalBinding"), "urn:xfire:transport:local");
    }

    public AutoCheckWebServiceSoap getAutoCheckWebServiceSoap() {
        return ((AutoCheckWebServiceSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoap")));
    }

    public AutoCheckWebServiceSoap getAutoCheckWebServiceSoap(String url) {
        AutoCheckWebServiceSoap var = getAutoCheckWebServiceSoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public AutoCheckWebServiceSoap getAutoCheckWebServiceSoapLocalEndpoint() {
        return ((AutoCheckWebServiceSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "AutoCheckWebServiceSoapLocalEndpoint")));
    }

    public AutoCheckWebServiceSoap getAutoCheckWebServiceSoapLocalEndpoint(String url) {
        AutoCheckWebServiceSoap var = getAutoCheckWebServiceSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
