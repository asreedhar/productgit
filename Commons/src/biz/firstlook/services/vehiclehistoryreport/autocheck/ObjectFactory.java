
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.services.vehiclehistoryreport.autocheck package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserIdentity_QNAME = new QName("http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", "UserIdentity");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.services.vehiclehistoryreport.autocheck
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetReport }
     * 
     */
    public GetReport createGetReport() {
        return new GetReport();
    }

    /**
     * Create an instance of {@link HasAccountResponse }
     * 
     */
    public HasAccountResponse createHasAccountResponse() {
        return new HasAccountResponse();
    }

    /**
     * Create an instance of {@link PurchaseReportsResponse }
     * 
     */
    public PurchaseReportsResponse createPurchaseReportsResponse() {
        return new PurchaseReportsResponse();
    }

    /**
     * Create an instance of {@link AutoCheckReportTO }
     * 
     */
    public AutoCheckReportTO createAutoCheckReportTO() {
        return new AutoCheckReportTO();
    }

    /**
     * Create an instance of {@link CanPurchaseReportResponse }
     * 
     */
    public CanPurchaseReportResponse createCanPurchaseReportResponse() {
        return new CanPurchaseReportResponse();
    }

    /**
     * Create an instance of {@link GetReportHtml }
     * 
     */
    public GetReportHtml createGetReportHtml() {
        return new GetReportHtml();
    }

    /**
     * Create an instance of {@link CanPurchaseReport }
     * 
     */
    public CanPurchaseReport createCanPurchaseReport() {
        return new CanPurchaseReport();
    }

    /**
     * Create an instance of {@link GetReportResponse }
     * 
     */
    public GetReportResponse createGetReportResponse() {
        return new GetReportResponse();
    }

    /**
     * Create an instance of {@link AutoCheckReportInspectionTO }
     * 
     */
    public AutoCheckReportInspectionTO createAutoCheckReportInspectionTO() {
        return new AutoCheckReportInspectionTO();
    }

    /**
     * Create an instance of {@link PurchaseReports }
     * 
     */
    public PurchaseReports createPurchaseReports() {
        return new PurchaseReports();
    }

    /**
     * Create an instance of {@link PurchaseReportResponse }
     * 
     */
    public PurchaseReportResponse createPurchaseReportResponse() {
        return new PurchaseReportResponse();
    }

    /**
     * Create an instance of {@link UserIdentity }
     * 
     */
    public UserIdentity createUserIdentity() {
        return new UserIdentity();
    }

    /**
     * Create an instance of {@link PurchaseReport }
     * 
     */
    public PurchaseReport createPurchaseReport() {
        return new PurchaseReport();
    }

    /**
     * Create an instance of {@link HasAccount }
     * 
     */
    public HasAccount createHasAccount() {
        return new HasAccount();
    }

    /**
     * Create an instance of {@link ArrayOfAutoCheckReportInspectionTO }
     * 
     */
    public ArrayOfAutoCheckReportInspectionTO createArrayOfAutoCheckReportInspectionTO() {
        return new ArrayOfAutoCheckReportInspectionTO();
    }

    /**
     * Create an instance of {@link GetReportHtmlResponse }
     * 
     */
    public GetReportHtmlResponse createGetReportHtmlResponse() {
        return new GetReportHtmlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserIdentity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/", name = "UserIdentity")
    public JAXBElement<UserIdentity> createUserIdentity(UserIdentity value) {
        return new JAXBElement<UserIdentity>(_UserIdentity_QNAME, UserIdentity.class, null, value);
    }

}
