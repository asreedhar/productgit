
package biz.firstlook.services.vehiclehistoryreport.autocheck;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AutoCheckReportTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoCheckReportTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Vin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CompareScoreRangeLow" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CompareScoreRangeHigh" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Inspections" type="{http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/}ArrayOfAutoCheckReportInspectionTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoCheckReportTO", propOrder = {
    "expirationDate",
    "userName",
    "vin",
    "score",
    "compareScoreRangeLow",
    "compareScoreRangeHigh",
    "inspections"
})
public class AutoCheckReportTO implements Serializable {

    @XmlElement(name = "ExpirationDate", required = true)
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "Vin")
    protected String vin;
    @XmlElement(name = "Score")
    protected int score;
    @XmlElement(name = "CompareScoreRangeLow")
    protected int compareScoreRangeLow;
    @XmlElement(name = "CompareScoreRangeHigh")
    protected int compareScoreRangeHigh;
    @XmlElement(name = "Inspections")
    protected ArrayOfAutoCheckReportInspectionTO inspections;

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVin(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public int getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(int value) {
        this.score = value;
    }

    /**
     * Gets the value of the compareScoreRangeLow property.
     * 
     */
    public int getCompareScoreRangeLow() {
        return compareScoreRangeLow;
    }

    /**
     * Sets the value of the compareScoreRangeLow property.
     * 
     */
    public void setCompareScoreRangeLow(int value) {
        this.compareScoreRangeLow = value;
    }

    /**
     * Gets the value of the compareScoreRangeHigh property.
     * 
     */
    public int getCompareScoreRangeHigh() {
        return compareScoreRangeHigh;
    }

    /**
     * Sets the value of the compareScoreRangeHigh property.
     * 
     */
    public void setCompareScoreRangeHigh(int value) {
        this.compareScoreRangeHigh = value;
    }

    /**
     * Gets the value of the inspections property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAutoCheckReportInspectionTO }
     *     
     */
    public ArrayOfAutoCheckReportInspectionTO getInspections() {
        return inspections;
    }

    /**
     * Sets the value of the inspections property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAutoCheckReportInspectionTO }
     *     
     */
    public void setInspections(ArrayOfAutoCheckReportInspectionTO value) {
        this.inspections = value;
    }

}
