
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Advertisement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Advertisement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.firstlook.biz/Distribution/1.0/}BodyPart">
 *       &lt;sequence>
 *         &lt;element name="Contents" type="{http://services.firstlook.biz/Distribution/1.0/}ArrayOfContent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Advertisement", propOrder = {
    "contents"
})
public class Advertisement
    extends BodyPart
{

    @XmlElement(name = "Contents")
    protected ArrayOfContent contents;

    /**
     * Gets the value of the contents property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContent }
     *     
     */
    public ArrayOfContent getContents() {
        return contents;
    }

    /**
     * Sets the value of the contents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContent }
     *     
     */
    public void setContents(ArrayOfContent value) {
        this.contents = value;
    }

}
