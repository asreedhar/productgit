
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dealerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="vehicleType" type="{http://services.firstlook.biz/Distribution/1.0/}VehicleType"/>
 *         &lt;element name="vehicleId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dealerId",
    "vehicleType",
    "vehicleId"
})
@XmlRootElement(name = "VehicleStatus")
public class VehicleStatus {

    protected int dealerId;
    @XmlElement(required = true)
    protected VehicleType vehicleType;
    protected int vehicleId;

    /**
     * Gets the value of the dealerId property.
     * 
     */
    public int getDealerId() {
        return dealerId;
    }

    /**
     * Sets the value of the dealerId property.
     * 
     */
    public void setDealerId(int value) {
        this.dealerId = value;
    }

    /**
     * Gets the value of the vehicleType property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleType }
     *     
     */
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    /**
     * Sets the value of the vehicleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleType }
     *     
     */
    public void setVehicleType(VehicleType value) {
        this.vehicleType = value;
    }

    /**
     * Gets the value of the vehicleId property.
     * 
     */
    public int getVehicleId() {
        return vehicleId;
    }

    /**
     * Sets the value of the vehicleId property.
     * 
     */
    public void setVehicleId(int value) {
        this.vehicleId = value;
    }

}
