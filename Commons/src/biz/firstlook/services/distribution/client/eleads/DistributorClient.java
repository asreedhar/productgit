
package biz.firstlook.services.distribution.client.eleads;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

import biz.firstlook.commons.util.PropertyFinder;

public class DistributorClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service1;
    
    
//    public DistributorClient() {
//
//    	PropertyFinder finder = new PropertyFinder( "Commons" );
//    	
//    	String url=finder.getProperty( "Prod.Distribution.Service.Url" );
//    	
//        create1();
//        Endpoint DistributorSoapEP = service1 .addEndpoint(new QName("http://services.firstlook.biz/Dstribution/1.0/", "DistributorSoap"), new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), url);
//        endpoints.put(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), DistributorSoapEP);
//        Endpoint DistributorSoapLocalEndpointEP = service1 .addEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint"), new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalBinding"), "xfire.local://Distributor");
//        endpoints.put(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint"), DistributorSoapLocalEndpointEP);
//    }
    public DistributorClient() {
    	
    	String server="";
		Context initialContext ;
		Context ctx;
		try {
			initialContext = new javax.naming.InitialContext(); 
			ctx = (Context) initialContext.lookup("java:comp/env");
			server = (String) ctx.lookup("ServerName");
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	PropertyFinder finder = new PropertyFinder( "Commons" );
    	
    	String url = null;
    	if (server.equalsIgnoreCase("prod"))
    		url=finder.getProperty( "Prod.Distribution.Service.Url" );
		else if (server.equalsIgnoreCase("beta"))
    			url=finder.getProperty( "Beta.Distribution.Service.Url" );
			else if (server.equalsIgnoreCase("intb"))
				url=finder.getProperty( "Intb.Distribution.Service.Url" );
				else if (server.equalsIgnoreCase("dev"))
					url=finder.getProperty( "Dev.Distribution.Service.Url" );    	
    	
    	System.out.println(server+"---- server name---"+url);
        create1();
        
        if(url==null)
        	url=finder.getProperty( "Prod.Distribution.Service.Url" );
        
        Endpoint DistributorSoapEP = service1 .addEndpoint(new QName("http://services.firstlook.biz/Dstribution/1.0/", "DistributorSoap"), new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), url);
        endpoints.put(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), DistributorSoapEP);
        Endpoint DistributorSoapLocalEndpointEP = service1 .addEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint"), new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalBinding"), "xfire.local://Distributor");
        endpoints.put(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint"), DistributorSoapLocalEndpointEP);
    }


    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create1() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service1 = asf.create((biz.firstlook.services.distribution.client.eleads.DistributorSoap.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service1, new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service1, new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public DistributorSoap getDistributorSoap() {
   
        return ((DistributorSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoap")));
    }

    public DistributorSoap getDistributorSoap(String url) {
        DistributorSoap var = getDistributorSoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public DistributorSoap getDistributorSoapLocalEndpoint() {
        return ((DistributorSoap)(this).getEndpoint(new QName("http://services.firstlook.biz/Distribution/1.0/", "DistributorSoapLocalEndpoint")));
    }

    public DistributorSoap getDistributorSoapLocalEndpoint(String url) {
        DistributorSoap var = getDistributorSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
