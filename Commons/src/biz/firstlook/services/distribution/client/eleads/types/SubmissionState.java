
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for SubmissionState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SubmissionState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotDefined"/>
 *     &lt;enumeration value="Waiting"/>
 *     &lt;enumeration value="Processing"/>
 *     &lt;enumeration value="Submitted"/>
 *     &lt;enumeration value="Accepted"/>
 *     &lt;enumeration value="Rejected"/>
 *     &lt;enumeration value="Retracted"/>
 *     &lt;enumeration value="Superseded"/>
 *     &lt;enumeration value="NotSupported"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum SubmissionState {

    @XmlEnumValue("Accepted")
    ACCEPTED("Accepted"),
    @XmlEnumValue("NotDefined")
    NOT_DEFINED("NotDefined"),
    @XmlEnumValue("NotSupported")
    NOT_SUPPORTED("NotSupported"),
    @XmlEnumValue("Processing")
    PROCESSING("Processing"),
    @XmlEnumValue("Rejected")
    REJECTED("Rejected"),
    @XmlEnumValue("Retracted")
    RETRACTED("Retracted"),
    @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"),
    @XmlEnumValue("Superseded")
    SUPERSEDED("Superseded"),
    @XmlEnumValue("Waiting")
    WAITING("Waiting");
    private final String value;

    SubmissionState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SubmissionState fromValue(String v) {
        for (SubmissionState c: SubmissionState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
