
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MessageBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MessageBody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Parts" type="{http://services.firstlook.biz/Distribution/1.0/}ArrayOfBodyPart" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageBody", propOrder = {
    "parts"
})
public class MessageBody {

    @XmlElement(name = "Parts")
    protected ArrayOfBodyPart parts;

    /**
     * Gets the value of the parts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBodyPart }
     *     
     */
    public ArrayOfBodyPart getParts() {
        return parts;
    }

    /**
     * Sets the value of the parts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBodyPart }
     *     
     */
    public void setParts(ArrayOfBodyPart value) {
        this.parts = value;
    }

}
