
package biz.firstlook.services.distribution.client.eleads;

import javax.jws.WebService;

import biz.firstlook.services.distribution.client.eleads.types.ArrayOfSubmissionStatus;
import biz.firstlook.services.distribution.client.eleads.types.Message;
import biz.firstlook.services.distribution.client.eleads.types.SubmissionDetails;
import biz.firstlook.services.distribution.client.eleads.types.VehicleType;

@WebService(serviceName = "Distributor", targetNamespace = "http://services.firstlook.biz/Distribution/1.0/", endpointInterface = "biz.firstlook.services.distribution.client.eleads.DistributorSoap")
public class DistributorImpl
    implements DistributorSoap
{


    public SubmissionDetails distribute(Message message, biz.firstlook.services.distribution.client.eleads.types.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public ArrayOfSubmissionStatus publicationStatus(int publicationId, biz.firstlook.services.distribution.client.eleads.types.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

    public ArrayOfSubmissionStatus vehicleStatus(int dealerId, VehicleType vehicleType, int vehicleId, biz.firstlook.services.distribution.client.eleads.types.UserIdentity UserIdentity) {
        throw new UnsupportedOperationException();
    }

}
