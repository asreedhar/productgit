
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for VehicleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VehicleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotDefined"/>
 *     &lt;enumeration value="Inventory"/>
 *     &lt;enumeration value="Appraisal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum VehicleType {

    @XmlEnumValue("Appraisal")
    APPRAISAL("Appraisal"),
    @XmlEnumValue("Inventory")
    INVENTORY("Inventory"),
    @XmlEnumValue("NotDefined")
    NOT_DEFINED("NotDefined");
    private final String value;

    VehicleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VehicleType fromValue(String v) {
        for (VehicleType c: VehicleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
