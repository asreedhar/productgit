package biz.firstlook.services.distribution.client.eleads;

import biz.firstlook.services.distribution.client.eleads.types.AppraisalValue;
import biz.firstlook.services.distribution.client.eleads.types.ArrayOfBodyPart;
import biz.firstlook.services.distribution.client.eleads.types.Dealer;
import biz.firstlook.services.distribution.client.eleads.types.Message;
import biz.firstlook.services.distribution.client.eleads.types.MessageBody;
import biz.firstlook.services.distribution.client.eleads.types.UserIdentity;
import biz.firstlook.services.distribution.client.eleads.types.Vehicle;
import biz.firstlook.services.distribution.client.eleads.types.VehicleType;

public class testWSClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("STARTING CALL");
		DistributorClient dc = new DistributorClient();
		DistributorSoap ds = dc.getDistributorSoap();
		
		Message message = new Message();
		

		
		Dealer dealer = new Dealer();
		dealer.setId(105239); //dealer id needs to be set here
		
		Vehicle vehicle = new Vehicle();
		vehicle.setId(1014); // vehicle id needs to be set here, if vehicle type also needs to be set then create VehicleType object
		vehicle.setVehicleType(VehicleType.APPRAISAL);
		
		ArrayOfBodyPart arrayOfBodyParts = new ArrayOfBodyPart();
		AppraisalValue app= new AppraisalValue();
		app.setValue(25000);
		
		
		
		arrayOfBodyParts.getBodyPart().add(app);
		MessageBody messageBody = new MessageBody();
		messageBody.setParts(arrayOfBodyParts);
		
		message.setFrom(dealer);
		message.setSubject(vehicle);
		message.setBody(messageBody);
		
		UserIdentity userIdentity = new UserIdentity();
		userIdentity.setUserName("dmehta");
		
		ds.distribute(message, userIdentity);
		System.out.println("Called");

	}

}
