
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleStatusResult" type="{http://services.firstlook.biz/Distribution/1.0/}ArrayOfSubmissionStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleStatusResult"
})
@XmlRootElement(name = "VehicleStatusResponse")
public class VehicleStatusResponse {

    @XmlElement(name = "VehicleStatusResult")
    protected ArrayOfSubmissionStatus vehicleStatusResult;

    /**
     * Gets the value of the vehicleStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionStatus }
     *     
     */
    public ArrayOfSubmissionStatus getVehicleStatusResult() {
        return vehicleStatusResult;
    }

    /**
     * Sets the value of the vehicleStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionStatus }
     *     
     */
    public void setVehicleStatusResult(ArrayOfSubmissionStatus value) {
        this.vehicleStatusResult = value;
    }

}
