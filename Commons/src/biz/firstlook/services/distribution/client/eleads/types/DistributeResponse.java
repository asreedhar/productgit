
package biz.firstlook.services.distribution.client.eleads.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DistributeResult" type="{http://services.firstlook.biz/Distribution/1.0/}SubmissionDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "distributeResult"
})
@XmlRootElement(name = "DistributeResponse")
public class DistributeResponse {

    @XmlElement(name = "DistributeResult")
    protected SubmissionDetails distributeResult;

    /**
     * Gets the value of the distributeResult property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionDetails }
     *     
     */
    public SubmissionDetails getDistributeResult() {
        return distributeResult;
    }

    /**
     * Sets the value of the distributeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionDetails }
     *     
     */
    public void setDistributeResult(SubmissionDetails value) {
        this.distributeResult = value;
    }

}
