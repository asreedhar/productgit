package biz.firstlook.commons.servlet.http;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.mock.web.MockHttpServletRequest;

import junit.framework.TestCase;

public class TestHttpServletRequestHelper extends TestCase {

	private final String urlNoContextPathAndNoQueryString = "https://mock.firstlook.biz:8443";
	private final String urlContextPathNoQueryString = urlNoContextPathAndNoQueryString + "/skinnyTA/thirdOptionSecondTabFourthPage/something.html";
	private final String queryString = "?param1=foo&param2=bar&param1=baz";
	private final String urlNoContextPathWithQueryString = urlNoContextPathAndNoQueryString + "/" + queryString;
	private final String urlContextPathWithQueryString = urlContextPathNoQueryString + queryString;
	
	private MockHttpServletRequest request;

	public TestHttpServletRequestHelper() {
		super();
	}

	@Override
	public void setUp() {
		buildMinimalHttpServletRequest(urlContextPathNoQueryString);
	}
	
	private void buildMinimalHttpServletRequest(String url) {
		URI uri;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			throw new RuntimeException("Problem setting up test case.");
		}
		request = new MockHttpServletRequest();
		request.setSecure("https".equals(uri.getScheme()));
		request.setProtocol(uri.getScheme());
		request.setServerName(uri.getHost());
		request.setServerPort(uri.getPort());
		final String path = uri.getPath();
		request.setContextPath( path.length() < 2 ? path : path.substring(0, path.indexOf("/", 1)));
		request.setRequestURI(path);
		
		String query = uri.getQuery();
		if(query != null) {
			String[] keyValuePairs = query.split("&");
			for(String keyValuePair : keyValuePairs) {
				String[] keyValue = keyValuePair.split("=");
				request.addParameter(keyValue[0], keyValue[1]);
			}
		}
		
		request.setQueryString(query);
	}

	private void reset() {
		try {
			tearDown();
		} catch (Exception e) {
			// ignore;
		}
		setUp();
	}

	public void testGetScheme() {
		String expected = "https";
		String result = HttpServletRequestHelper.getScheme(request);
		assertEquals(expected, result);

		request.setSecure(Boolean.FALSE);
		request.setProtocol("http");
		expected = "http";
		result = HttpServletRequestHelper.getScheme(request);
		assertEquals(expected, result);

		request.setSecure(Boolean.FALSE);
		request.setProtocol(null);
		request.addHeader("X_FORWARDED_PROTO", "https");
		expected = "https";
		result = HttpServletRequestHelper.getScheme(request);
		assertEquals(expected, result);

		// reset request headers
		reset();

		// http
		request.setSecure(Boolean.FALSE);
		request.setProtocol("http");
		expected = "http";
		result = HttpServletRequestHelper.getScheme(request);
		assertEquals(expected, result);

		// https
		request.setSecure(Boolean.FALSE);
		request.setProtocol("http");
		request.addHeader("X-Forwarded-Proto", "https");
		expected = "https";
		result = HttpServletRequestHelper.getScheme(request);
		assertEquals(expected, result);

		reset();

		// http
		request.setSecure(Boolean.FALSE);
		request.setProtocol("https");
		request.addHeader("X-Proto", "https");
		expected = "http";
		result = HttpServletRequestHelper.getScheme(request);
		assertEquals(expected, result);
	}

	public void testGetHost() {
		String expected = "mock.firstlook.biz:8443";
		String result = HttpServletRequestHelper.getHost(request);
		assertEquals(expected, result);

		request.addHeader("Host", "spoofmyurl");
		expected = "spoofmyurl";
		result = HttpServletRequestHelper.getHost(request);
		assertEquals(expected, result);

		reset();

		request.addHeader("Host", "spoofmyurl");
		request.addHeader("X_FORWARDED_HOST", "spoofmyurl2");
		expected = "spoofmyurl2";
		result = HttpServletRequestHelper.getHost(request);
		assertEquals(expected, result);

		reset();

		request.addHeader("Host", "spoofmyurl");
		request.addHeader("X_FORWARDED_HOST", "spoofmyurl2");
		request.addHeader("X-Forwarded-Host", "spoofmyurl3");
		expected = "spoofmyurl3";
		result = HttpServletRequestHelper.getHost(request);
		assertEquals(expected, result);
	}

	public void testGetServiceUrl() {
		String expected = "https://mock.firstlook.biz:8443/skinnyTA/";
		String result = HttpServletRequestHelper.getServiceUrl(request);
		assertEquals(expected, result);
		
		reset();
		buildMinimalHttpServletRequest(urlNoContextPathWithQueryString);
		expected = "https://mock.firstlook.biz:8443/?param1=foo&param1=baz&param2=bar";
		result = HttpServletRequestHelper.getServiceUrl(request);
		assertEquals(expected, result);
		
		reset();
		buildMinimalHttpServletRequest(urlContextPathWithQueryString);
		expected = "https://mock.firstlook.biz:8443/skinnyTA/" + "?param1=foo&param1=baz&param2=bar";
		result = HttpServletRequestHelper.getServiceUrl(request);
		assertEquals(expected, result);
	}

}
