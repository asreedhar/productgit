package biz.firstlook.commons.util;

import javax.servlet.http.Cookie;

import junit.framework.TestCase;

public class TestCookieHelper extends TestCase {

	public TestCookieHelper() {
		super();
	}

	public TestCookieHelper(String name) {
		super(name);
	}

	public void testVersionZeroCookieValue() {
		String test = " a[b ]c{d} e(f)g=@h, i\"j\\k/l ?m@n:o  ;p";
		String expected = "abcdefghijklmnop";
		
		Cookie cookie = CookieHelper.createCookie("mycookie", test);
		
		assertEquals(expected, cookie.getValue());
	}
}
