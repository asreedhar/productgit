package biz.firstlook.commons.util;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.commons.util.ColorUtility.Color;

public class TestColorUtility extends TestCase
{

private static final String WHITESPACE_1 = " ";
private static final String WHITESPACE_2 = "  ";
private static final String WHITESPACE_27 = "                           ";

private static final String NULL = null;
private static final String EMPTY_STRING = "";
private static final String BLUE_27 = "BLUE                       ";
private static final String w3_GREEN = "   grEEn";
private static final String RED = "RED";
private static final String w1_maroon_4 = " maroon    ";
private static final String ATOMIC_ORAN_MET = "  ATOMIC ORAN MET  ";
private static final String ATOMIC_BLUE_MET = "ATOMIC BLUE MET";

public void testFormatColor()
{
	assertEquals( WHITESPACE_1, ColorUtility.formatColor( WHITESPACE_1 ) );
	assertEquals( WHITESPACE_2, ColorUtility.formatColor( WHITESPACE_2 ) );
	assertEquals( WHITESPACE_27, ColorUtility.formatColor( WHITESPACE_27 ) );
	assertEquals( Color.UNKNOWN.toString(), ColorUtility.formatColor( NULL ) );
	assertEquals( Color.UNKNOWN.toString(), ColorUtility.formatColor( EMPTY_STRING ) );
	assertEquals( Color.BLUE.toString(), ColorUtility.formatColor( BLUE_27 ) );
	assertEquals( Color.GREEN.toString(), ColorUtility.formatColor( w3_GREEN ) );
	assertEquals( Color.RED.toString(), ColorUtility.formatColor( RED) );
	assertEquals( Color.MAROON.toString(), ColorUtility.formatColor( w1_maroon_4 ) );
	assertEquals( ATOMIC_ORAN_MET.trim(), ColorUtility.formatColor( ATOMIC_ORAN_MET ) );
	assertEquals( ATOMIC_BLUE_MET, ColorUtility.formatColor( ATOMIC_BLUE_MET ) );
}

public void testGetStandardColors()
{
	List<String> colorEnums = new ArrayList< String >();
	for( Color color : Color.values() )
		colorEnums.add( color.toString() );
	
	assertEquals( colorEnums, ColorUtility.retrieveStandardColors() );
}

}
