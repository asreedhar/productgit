package biz.firstlook.commons.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import junit.framework.TestCase;

public class TestVinService extends TestCase
{

private String oldVin;
private String validVin;
private String realPre1990Vin;
private String badChecksumVin;
private String notRealVin;
private String seventeenDigitStockNumber;
private String nonSeventeenDigitVin;
private String nonUpperCaseVin;

//this is when the modelYear digit rotates, since 2010 and 1980 share the same digit for modelYear in the vin.
private String modelYear2010Vin;
private String modelYear2011Vin;

private List<String> valids = new ArrayList<String>();
private List<String> invalids = new ArrayList<String>();

public TestVinService( String arg1 )
{
	super( arg1 );
}

public void setUp()
{
	//	 JHMAN5522FC000570 - 1985 Honda Civic DX Wagon
	oldVin = "JHMAN5522FC000570";

	// JHMZE14772T001779 - 2002 Honda Insight 2 Door Hatchback
	validVin = "JHMZE14772T001779";
	
	//	1988 Toyota Supra
	realPre1990Vin = "JT2MA70L0J0102961";
	
	nonUpperCaseVin = "1g4hr54c7lh485328";
	
	valids.add(oldVin);
	valids.add(validVin);
	valids.add(realPre1990Vin);
	valids.add(nonUpperCaseVin);
	
	// Fabricated vin: JHMZE1477RT001779 - 1994 Honda Insight 2 Door Hatchback
	badChecksumVin = "JHMZE1477RT001779";
	
	notRealVin = "123456789Aabcdefg";
	
	seventeenDigitStockNumber = "testStockNumber17";
	
	nonSeventeenDigitVin = "testStockNumer";
	
	//2010 Chevrolet Tahoe LT, Sport Utility, Black, 
	//8 Cylinder Gasoline, 
	//6 Speed Automatic, 
	//4 wheel drive - rear
	//Flexible Fuel
	modelYear2010Vin = "1GNUKBE03AR111761";
	
	//2011 BMW 535i Sedan
	modelYear2011Vin = "WBAFR7C58BC266171";
	
	invalids.add(badChecksumVin);
	invalids.add(notRealVin);
	invalids.add(seventeenDigitStockNumber);
	invalids.add(nonSeventeenDigitVin);
}

public void testValidateFirstLookOlderVin()
{
	assertTrue( "The vin, JHMAN5522FC000570, should be allowed into the system.", VinUtility.isValidFirstLookVin( oldVin ) );
}

public void testValidateFirstLookValidVin()
{
	assertTrue( "The vin, JHMZE14772T001779, should be valid in the Edge", VinUtility.isValidFirstLookVin( validVin ) );
}

public void testValidateFirstLookBadCheckSumVin()
{
	assertFalse( "The vin, JHMZE1477RT001779, has a bad checksum and should be invalid", VinUtility.isValidFirstLookVin( badChecksumVin ) );
}

public void testFirstLookNotAVin()
{
	assertFalse( "The stock number, testStockNumber17, should not have been a valid vin", VinUtility.isValidFirstLookVin( seventeenDigitStockNumber ) );
}

public void testIsChecksumValidOldCar()
{
	assertTrue( "The vin, JHMAN5522FC000570, is an older car but should have a valid check digit.", VinUtility.isValidFirstLookVin( oldVin ) );
}

public void testIsChecksumValid()
{
	assertTrue( "The vin, JHMZE14772T001779, should have a valid check digit", VinUtility.isValidFirstLookVin( validVin ) );
}

public void testIsNonUpperCaseVinValid()
{
	assertTrue( "The vin, 1g4hr54c7lh485328, should have a valid check digit", VinUtility.isValidFirstLookVin( validVin ) );
}

public void testIsChecksumValidBadCheckDigit()
{
	assertFalse( VinUtility.isValidFirstLookVin( badChecksumVin ) );
}

public void testPre1990VinNotValidVin()
{
	assertFalse("This Vin is not a valid vin and the method call should've failed.", VinUtility.isValidFirstLookVin( notRealVin ));
}

public void testPre1990VinNotSeventeenDigitVin()
{
	assertFalse("This Vin is not a valid vin and the method call should've failed.", VinUtility.isValidFirstLookVin( nonSeventeenDigitVin ));
}

public void testPre1980Vin()
{
	assertTrue( "The vin, JT2MA70L0J0102961, is a valid pre 1990 vin.", VinUtility.isValidFirstLookVin( realPre1990Vin ) );
}

public void testValidate() {
	for(String invalid : invalids) {
		try {
			VinUtility.validate(invalid);
			fail("The vin: " + invalid + " passed the validate method but is suppose to be invalid.");
		} catch (InvalidVinException e) {
			continue; //passed test
		}
	}
	
	for(String valid : valids) {
		try {
			VinUtility.validate(valid);
		} catch (InvalidVinException e) {
			fail("The vin: " + valid + " did not pass the validate method but is suppose to be valid.");
		}
	}
	assertTrue(true);
}

public void testGetLatestModelYear() {
	Calendar cal = Calendar.getInstance();
	int currentYear = cal.get(Calendar.YEAR);
	if(currentYear - 2010 >= 30) {
		throw new RuntimeException("This test needs to get updated to decode a vin from your era.");
	}
	
	try {
		//a 1980 car SHOULD return 2010!
		int year = VinUtility.getLatestModelYear(modelYear2010Vin);
		assertEquals(2010, year);
	} catch (InvalidVinException e) {
		fail("The vin " + modelYear2010Vin + " should decode to a 2010 vehicle.");
	}
	
	try {
		//a 1981 car SHOULD return 2011!
		int year = VinUtility.getLatestModelYear(modelYear2011Vin);
		assertEquals(2011, year);
	} catch (InvalidVinException e) {
		fail("The vin " + modelYear2011Vin + " should decode to a 2011 vehicle.");
	}
	
	try {
		int year = VinUtility.getLatestModelYear(validVin);
		assertEquals(2002, year);
	} catch (InvalidVinException e) {
		fail("The vin " + validVin + " should decode to a 2002 vehicle.");
	}
}

}
