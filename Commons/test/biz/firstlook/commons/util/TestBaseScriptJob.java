package biz.firstlook.commons.util;

import junit.framework.TestCase;

public class TestBaseScriptJob extends TestCase
{

private BaseScriptJob baseJob = new BaseScriptJobTestImpl();

public TestBaseScriptJob( String name )
{
    super(name);
}

public void testFormatRunTime() throws Exception
{
    String time = baseJob.formatRuntime(90000);
    assertEquals("1m30s", time);

}

public void testFormatTime() throws Exception
{
    String time = baseJob.formatRuntime(0);
    assertEquals("0m0s", time);
}

public void testParseContext()
{
    assertEquals("firstlook", baseJob.parseContext("venusdev:80/firstlook"));
    assertEquals("", baseJob.parseContext("venusdev:80"));
}

public void testParseHost()
{
    assertEquals("venusdev", baseJob.parseHost("venusdev:8080/firstlook"));
}

public void testParsePort()
{
    assertEquals("8080", baseJob.parsePort("venusdev:8080/firstlook"));
    assertEquals("8080", baseJob.parsePort("venusdev:8080"));
}

}
