package biz.firstlook.commons.util;

import junit.framework.TestCase;

public class TestFunctions extends TestCase {

	public TestFunctions() {
		super();
	}

	public TestFunctions(String name) {
		super(name);
	}
	
	public void testNullSafeBoolean() {
		assertTrue(Functions.nullSafeBoolean(Boolean.TRUE));
		assertTrue(Functions.nullSafeBoolean(Boolean.TRUE.toString()));
		assertTrue(Functions.nullSafeBoolean(Integer.valueOf(1)));
		assertTrue(Functions.nullSafeBoolean(Integer.valueOf(2)));
		assertTrue(Functions.nullSafeBoolean(Byte.valueOf((byte)3)));
		assertTrue(Functions.nullSafeBoolean(Short.valueOf((short)4)));
		assertTrue(Functions.nullSafeBoolean(Long.valueOf(1L)));
		assertTrue(Functions.nullSafeBoolean(Float.valueOf(1.0F)));
		assertTrue(Functions.nullSafeBoolean(Double.valueOf(1.0D)));
		assertTrue(Functions.nullSafeBoolean("1"));
		assertTrue(Functions.nullSafeBoolean("1.034"));
		assertTrue(Functions.nullSafeBoolean("2"));
		assertTrue(Functions.nullSafeBoolean("2147483648"));  //Integer.MAX_VALUE + 1
		
		//this is just to show how perverse the method has become.
//		assertTrue(Functions.nullSafeBoolean(new ArrayList<URI>(){
//			private static final long serialVersionUID = 543665338758714572L;
//
//			public String toString() {
//				return "true";
//			}
//		}));
		
		assertFalse(Functions.nullSafeBoolean(Boolean.FALSE));
		assertFalse(Functions.nullSafeBoolean(Boolean.FALSE.toString()));
		assertFalse(Functions.nullSafeBoolean(Integer.valueOf(0)));
		assertFalse(Functions.nullSafeBoolean(Integer.valueOf(-1)));
		assertFalse(Functions.nullSafeBoolean(Byte.valueOf((byte)0)));
		assertFalse(Functions.nullSafeBoolean(Short.valueOf((short)0)));
		assertFalse(Functions.nullSafeBoolean(Long.valueOf(0L)));
		assertFalse(Functions.nullSafeBoolean(Float.valueOf(0.99F)));
		assertFalse(Functions.nullSafeBoolean(Double.valueOf(0.98D)));
		assertFalse(Functions.nullSafeBoolean(Double.valueOf(0.0D)));
		assertFalse(Functions.nullSafeBoolean("0"));
		assertFalse(Functions.nullSafeBoolean("-1"));
		assertFalse(Functions.nullSafeBoolean("-1.034"));
		assertFalse(Functions.nullSafeBoolean("-2147483649"));  //Integer.MIN_VALUE + 1		
		assertFalse(Functions.nullSafeBoolean("blahblahblah"));

		assertEquals(null, Functions.nullSafeBoolean(null));
	}
}
