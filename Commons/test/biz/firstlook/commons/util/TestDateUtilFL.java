package biz.firstlook.commons.util;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class TestDateUtilFL extends TestCase
{

private Calendar cst2005Feb11;
private Calendar cst2005Jan11;
private Calendar cdt2005Apr12;
private Calendar leapYear;
private Calendar springForwardDay;
private Calendar cst2006Feb11;
private Calendar cdt2005Jun13;
private Calendar cdt2006Jul18;

public void setUp()
{
    leapYear = Calendar.getInstance();
    leapYear.set( 2004, Calendar.FEBRUARY, 11, 0, 0, 0 );

    // Note: February 11, 2005 is in CST for a fact.
    cst2005Feb11 = Calendar.getInstance();
    cst2005Feb11.set( 2005, Calendar.FEBRUARY, 11, 0, 0, 0 );

    // Note: April 03, 2005 2am was spring forward.
    springForwardDay = Calendar.getInstance();
    springForwardDay.set( 2005, Calendar.APRIL, 03, 0, 0, 0 );

    // Note: April 12, 2005 is in CDT for a fact.
    cdt2005Apr12 = Calendar.getInstance();
    cdt2005Apr12.set( 2005, Calendar.APRIL, 12, 0, 0, 0 );

    cst2005Jan11 = Calendar.getInstance();
    cst2005Jan11.set( 2005, Calendar.JANUARY, 11, 0, 0, 0 );

    cst2006Feb11 = Calendar.getInstance();
    cst2006Feb11.set( 2006, Calendar.FEBRUARY, 11, 0, 0, 0 );

    cdt2005Jun13 = Calendar.getInstance();
    cdt2005Jun13.set( 2005, Calendar.JUNE, 13, 0, 0, 0 );

    cdt2006Jul18 = Calendar.getInstance();
    cdt2006Jul18.set( 2006, Calendar.JULY, 18, 0, 0, 0 );
}

public void testCalculateNumberOfCalendarDaysLeapYears()
{
	//SQLSERVER2005: SELECT DATEDIFF(dd, '2004-02-11', '2006-02-11'): 731
    assertEquals( "The number of days between the same day of year across a leap year, ", 731,
                  DateUtilFL.calculateNumberOfCalendarDays( leapYear.getTime(), cst2006Feb11.getTime() ) );
    
    //SQLSERVER2005: SELECT DATEDIFF(dd, '2005-01-11', 2004-02-11): -335
    assertEquals( "The number of days less than a year in different years, ", 335,
                  DateUtilFL.calculateNumberOfCalendarDays( leapYear.getTime(), cst2005Jan11.getTime() ) );
    
    //SQLSERVER2005: SELECT DATEDIFF(dd, '2004-02-11', '2005-01-11'): 335
    assertEquals( "The number of days less than a year in different years, ", 335,
                  DateUtilFL.calculateNumberOfCalendarDays( cst2005Jan11.getTime(), leapYear.getTime() ) );
    
    //SQLSERVER2005: SELECT DATEDIFF(dd, '2005-01-11', '2005-04-12'): 91
    assertEquals( "The number of days less than a year in the same year, ", 91,
                  DateUtilFL.calculateNumberOfCalendarDays( cst2005Jan11.getTime(), cdt2005Apr12.getTime() ) );
    
    //SQLSERVER2005: SELECT DATEDIFF(dd, '2005-04-12', '2005-01-11'): -91
    assertEquals( "The number of days less than a year in the same year, ", 91,
                  DateUtilFL.calculateNumberOfCalendarDays( cdt2005Apr12.getTime(), cst2005Jan11.getTime() ) );
}

public void testCalculateNumberOfLeapYears()
{
    int numberOfLeapYears2004_2006 = DateUtilFL.calculateNumberOfLeapYears( 2004, 2006 );
    int numberOfLeapYears1981_2005 = DateUtilFL.calculateNumberOfLeapYears( 1981, 2005 );

    assertEquals( "The number of Leap Years between 2004 and 2006, ", 1, numberOfLeapYears2004_2006 );
    assertEquals( "The number of Leap Years between 1981 and 2005, ", 6, numberOfLeapYears1981_2005 );
}

public void testNormalizeDST()
{
    Calendar returnCalender = DateUtilFL.normalizeDST( cdt2005Jun13.getTime() );

    assertEquals( "The time in CDT was not normalized correctly, ", "Mon Jun 13 01:00:00 CDT 2005", returnCalender.getTime().toString() );
}

public void testNormalizeDSTInStandardTime()
{
    Calendar returnCalender = DateUtilFL.normalizeDST( cst2005Feb11.getTime() );

    assertEquals( "The time in CST was not normalized correctly, ", "Fri Feb 11 00:00:00 CST 2005", returnCalender.getTime().toString() );
}

public void testGetWeekOfYear()
{
    Integer weekOfYear = DateUtilFL.getWeekOfYear( cst2005Feb11.getTime() );

    assertEquals( "The week of year for Feb. 11, 2005 was ", 7, weekOfYear.intValue() );
}

public void testGetWeekStartDateOnSunday()
{
    springForwardDay.set( Calendar.HOUR_OF_DAY, 1 );
    Date weekSevenSundayOf2005 = DateUtilFL.getWeekStartDate( springForwardDay.getTime() );

    assertEquals( "GetWeekStartDate failed, ", "Sun Apr 03 01:00:00 CST 2005", weekSevenSundayOf2005.toString() );
}

public void testGetWeekStartDate()
{
    Date week25SundayOf2005 = DateUtilFL.getWeekStartDate( cdt2005Jun13.getTime() );

    assertEquals( "GetWeekStartDate failed, ", "Sun Jun 12 00:00:00 CDT 2005", week25SundayOf2005.toString() );
}

public void testGetWeekEndDate()
{
    springForwardDay.set( Calendar.HOUR_OF_DAY, 5 );
    Date weekSevenSaturdayOf2005 = DateUtilFL.getWeekEndDate( springForwardDay.getTime() );

    assertEquals( "GetWeekEndDate failed, ", "Sat Apr 09 05:00:00 CDT 2005", weekSevenSaturdayOf2005.toString() );
}

public void testAddDaysToDateNextMonth()
{
    Date addTooMuchDaysToThisMonth = DateUtilFL.addDaysToDate( cdt2005Apr12.getTime(), 19 );

    assertEquals( "Expected to be in the next month, ", "Sun May 01 00:00:00 CDT 2005", addTooMuchDaysToThisMonth.toString() );
}

public void testAddDaysToDateNegativeDays()
{
    Date subtractTooMuchDaysToThisMonth = DateUtilFL.addDaysToDate( cdt2005Apr12.getTime(), -12 );

    assertEquals( "Expected to be in the next month, ", "Thu Mar 31 00:00:00 CST 2005", subtractTooMuchDaysToThisMonth.toString() );
}

public void testGetEndOfToday()
{
	Date aDate = new Date();
	Calendar cal = Calendar.getInstance();
    cal.setTime( aDate );
    int day = cal.get( Calendar.DAY_OF_MONTH );
    int month = cal.get( Calendar.MONTH );
    int year = cal.get( Calendar.YEAR );
    int hour = 23;
    int minutes = 59;
     
    Date endOfDay = DateUtilFL.setToEndOfDay( aDate );
    Calendar testCal = Calendar.getInstance();
    testCal.setTime( endOfDay );
    
    assertEquals( day, testCal.get( Calendar.DAY_OF_MONTH ) );
    assertEquals( month, testCal.get( Calendar.MONTH ) );
    assertEquals( year, testCal.get( Calendar.YEAR ) );
    assertEquals( hour, testCal.get( Calendar.HOUR_OF_DAY ) );
    assertEquals( minutes, testCal.get( Calendar.MINUTE ) );
}

}
