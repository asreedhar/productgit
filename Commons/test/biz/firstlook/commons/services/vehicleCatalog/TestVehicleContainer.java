package biz.firstlook.commons.services.vehicleCatalog;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class TestVehicleContainer extends TestCase
{

private VehicleContainer getBaseVehicleContainer()
{
	VehicleContainer vc = new VehicleContainer();
	vc.addBodyType( "bodyType" );
	vc.addCylinderQuantity( "4" );
	vc.addSegment( "segment" );
	vc.addSegmentId( 1 );
	vc.addDoors( "3" );
	vc.addDriveTypeCode( "driveTrain" );
	vc.addEngine( "engine" );
	vc.addFuelType( "fuelType" );
	vc.setGroupingDescription( "groupingDescription" );
	vc.setGroupingDescriptionId( 2 );
	vc.addLine( "line" );
	vc.setMake( "make" );
	vc.setMakeModelGroupingId( 11 );
	vc.setModel( "model" );
	vc.setModelYear( 2007 );
	vc.addTransmission( "transmission" );
	vc.setVin( "vin" );
	return vc;
}

public void testVehicleCatlogEntry()
{
	VehicleContainer vc = getBaseVehicleContainer();
	vc.addCatalogKey(new VehicleCatalogKey(1, "catalogKey1" ));
	vc.addCatalogKey( new VehicleCatalogKey(2, "catalogKey2" ) );
	vc.addCatalogKey( new VehicleCatalogKey(3, "catalogKey3" ));
	vc.addSeries( "series1" );
	vc.addSeries( "series2" );
	vc.addSeries( "series3" );
	VehicleCatalogEntry vcEntry = vc.getVehicleCatalogEntry();

	assertEquals("bodyType doesn't match", "bodyType" ,vcEntry.getBodyType() );
	assertEquals("cylinderCount doesn't match", new Integer(4) ,vcEntry.getCylinderCount() );
	assertEquals("segment doesn't match", "segment" ,vcEntry.getSegment() );
	assertEquals("segmentId doesn't match", new Integer(1) ,vcEntry.getSegmentId() );
	assertEquals("doors doesn't match", new Integer(3) ,vcEntry.getDoors() );
	assertEquals("driveTrain doesn't match", "driveTrain" ,vcEntry.getDriveTrain() );
	assertEquals("engine doesn't match", "engine" ,vcEntry.getEngine() );
	assertEquals("fuelType doesn't match", "fuelType" ,vcEntry.getFuelType() );
	assertEquals("groupingDescription doesn't match", "groupingDescription" ,vcEntry.getGroupingDescription() );
	assertEquals("groupingDescriptionId doesn't match", new Integer(2) ,vcEntry.getGroupingDescriptionId() );
	assertEquals("line doesn't match", "line" ,vcEntry.getLine() );
	assertEquals("make doesn't match", "make" ,vcEntry.getMake() );
	assertEquals("MakeModelGroupingId doesn't match", new Integer(11) ,vcEntry.getMakeModelGroupingId() );
	assertEquals("model doesn't match", "model" ,vcEntry.getModel() );
	assertEquals("ModelYear doesn't match", new Integer(2007) ,vcEntry.getModelYear() );
	assertEquals("transmission doesn't match", "transmission" ,vcEntry.getTransmission() );
	assertEquals("vin doesn't match", "vin" ,vcEntry.getVin() );
	
	assertEquals( "incorrectly returned a random catalogKey", null, vcEntry.getCatalogKey() );
	assertEquals( "incorrectly returned a random catalogKey", null, vcEntry.getSeries() );
}

public void testVehicleCatlogMultiEntry()
{
	
	List<VehicleCatalogKey> catalogKeys = new ArrayList< VehicleCatalogKey >();
	catalogKeys.add( new VehicleCatalogKey(1, "catalogKey1" ) );
	catalogKeys.add( new VehicleCatalogKey(2, "catalogKey2" ) );
	catalogKeys.add( new VehicleCatalogKey(3, "catalogKey3" ) );
	
	List<String> seriess = new ArrayList< String >();
	seriess.add( "series1" );
	seriess.add( "series2" );
	seriess.add( "series3" );
	
	VehicleContainer vc = getBaseVehicleContainer();
	vc.addCatalogKey(new VehicleCatalogKey(1, "catalogKey1" ));
	vc.addCatalogKey( new VehicleCatalogKey(2, "catalogKey2" ) );
	vc.addCatalogKey( new VehicleCatalogKey(3, "catalogKey3" ));
	vc.addSeries( "series1" );
	vc.addSeries( "series2" );
	vc.addSeries( "series3" );
	
	VehicleCatalogMultiEntry vcMutliEntry = vc.getVehicleCatalogMultiEntry();
	assertEquals("bodyType doesn't match", "bodyType" ,vcMutliEntry.getBodyType() );
	assertEquals("cylinderCount doesn't match", new Integer(4) ,vcMutliEntry.getCylinderCount() );
	assertEquals("segment doesn't match", "segment" ,vcMutliEntry.getSegment() );
	assertEquals("segmentId doesn't match", new Integer(1) ,vcMutliEntry.getSegmentId() );
	assertEquals("doors doesn't match", new Integer(3) ,vcMutliEntry.getDoors() );
	assertEquals("driveTrain doesn't match", "driveTrain" ,vcMutliEntry.getDriveTrain() );
	assertEquals("engine doesn't match", "engine" ,vcMutliEntry.getEngine() );
	assertEquals("fuelType doesn't match", "fuelType" ,vcMutliEntry.getFuelType() );
	assertEquals("groupingDescription doesn't match", "groupingDescription" ,vcMutliEntry.getGroupingDescription() );
	assertEquals("groupingDescriptionId doesn't match", new Integer(2) ,vcMutliEntry.getGroupingDescriptionId() );
	assertEquals("line doesn't match", "line" ,vcMutliEntry.getLine() );
	assertEquals("make doesn't match", "make" ,vcMutliEntry.getMake() );
	assertEquals("MakeModelGroupingId doesn't match", new Integer(11) ,vcMutliEntry.getMakeModelGroupingId() );
	assertEquals("model doesn't match", "model" ,vcMutliEntry.getModel() );
	assertEquals("ModelYear doesn't match", new Integer(2007) ,vcMutliEntry.getModelYear() );
	assertEquals("transmission doesn't match", "transmission" ,vcMutliEntry.getTransmission() );
	assertEquals("vin doesn't match", "vin" ,vcMutliEntry.getVin() );

	assertTrue( "returned wrong catalogKey", ((String)catalogKeys.get(0).getVehicleCatalogDescription()).equalsIgnoreCase((String)vcMutliEntry.getCatalogKeys().get(0).getVehicleCatalogDescription()) );
	assertTrue( "returned wrong catalogKey", ((String)catalogKeys.get(1).getVehicleCatalogDescription()).equalsIgnoreCase((String)vcMutliEntry.getCatalogKeys().get(1).getVehicleCatalogDescription()) );
	assertTrue( "returned wrong catalogKey", ((String)catalogKeys.get(2).getVehicleCatalogDescription()).equalsIgnoreCase((String)vcMutliEntry.getCatalogKeys().get(2).getVehicleCatalogDescription()) );
	assertEquals( "returned wrong trims", seriess, vcMutliEntry.getTrims() );

}

}
