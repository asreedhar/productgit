package biz.firstlook.commons.services.fault;

import javax.xml.datatype.DatatypeConfigurationException;

import junit.framework.TestCase;
import biz.firstlook.commons.services.fault.FaultService;
import biz.firstlook.services.fault.SaveRemoteResultsDto;

public class TestFaultService extends TestCase {
	
	/**
	 * This test will ensure that the exception being saved, does indeed get saved to the database.
	 * @throws DatatypeConfigurationException
	 */
	public void testFaultService() throws DatatypeConfigurationException
	{
		Exception e = new NullPointerException();
		FaultService service = FaultService.getInstance();
		SaveRemoteResultsDto result = service.saveException(e,"myapp","machine","platform", "user", "path", "url", "userHostAddress");
	
		//If the fault event was successfully saved then the Id of the FaultEvent should not be 0.
		assertTrue(result.getFaultEvent().getId()!=0);
		
	}
	
	/**
	 * This will test to see what happens when null input is specified for everything except the exception.
	 * The exception should still be saved.
	 * @throws DatatypeConfigurationException
	 */
	
	public void testFaultServiceNullInput() throws DatatypeConfigurationException
	{
		Exception e = new NullPointerException();
		FaultService service = FaultService.getInstance();
		SaveRemoteResultsDto result = service.saveException(e,null,null,null,null,null,null,null);
	
		//If the fault event was successfully saved then the Id of the FaultEvent should not be 0.
		assertTrue(result.getFaultEvent().getId()!=0);
		
	}


	/**
	 * This will test an inner exception example.
	 * @throws DatatypeConfigurationException
	 */
	
	public void testFaultServicePopulatedStackTrace() throws DatatypeConfigurationException
	{
		try{			
			generateException();
		}catch(Exception e){
			
			
			Exception y = new Exception("I have an inner exception",e);
			FaultService service = FaultService.getInstance();
			SaveRemoteResultsDto result = service.saveException(y,null,null,null,null,null,null,null);
			//If the fault event was successfully saved then the Id of the FaultEvent should not be 0.
			assertTrue(result.getFaultEvent().getId()!=0);
		}		
		
	}

	/**
	 * This is a helper method that will simply be called to generate an exception so that we can build an 
	 * exception chain.
	 * @throws Exception
	 */
	private void generateException() throws Exception{
		
		Exception e = new NullPointerException();
		throw e;
	}
	
}
