package biz.firstlook.commons.date;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;


public class TestBasisPeriod extends TestCase
{

private MockBasisPeriod period;

public TestBasisPeriod( String name )
{
    super(name);
}

public void setUp()
{
    period = new MockBasisPeriod();
}

public void testGetStartDateCurrent()
{
    period = new MockBasisPeriod(91, false);

    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar.set(2004, Calendar.OCTOBER, 13, 1, 0, 0); // Tuesday, October 13,
                                                        // 2004

    period.setCalendar(calendar);

    Date startDate = period.getStartDate();

    Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar2.set(2004, Calendar.JULY, 14, 1, 0, 0); // Tuesday, July 14,
                                                        // 2004
    calendar2 = DateUtils.truncate( calendar2, Calendar.DATE );

    assertEquals(calendar2.getTime(), startDate);
}

public void testGetStartDateForecast()
{
    period = new MockBasisPeriod(91, true);

    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar.set(2004, Calendar.OCTOBER, 13, 1, 0, 0); // Tuesday, October 13,
                                                        // 2004

    period.setCalendar(calendar);

    Date startDate = period.getStartDate();

    Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar2.set(2003, Calendar.OCTOBER, 15, 1, 0, 0); // Tuesday, October 15,
                                                        // 2003
    calendar2 = DateUtils.truncate( calendar2, Calendar.DATE );

    assertEquals(calendar2.getTime(), startDate);
}

public void testGetEndDateCurrent()
{
    period = new MockBasisPeriod(91, false);

    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar.set(2004, Calendar.OCTOBER, 13, 0, 0, 0); // Tuesday, October 13,
                                                        // 2004

    period.setCalendar(calendar);

    Date endDate = period.getEndDate();

    Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar2.set(2004, Calendar.OCTOBER, 13, 0, 0, 0); // Tuesday, October 12,
                                                        // 2004
    calendar2 = DateUtils.truncate( calendar2, Calendar.DATE );

    assertEquals(calendar2.getTime(), endDate);
}

public void testGetEndDateForecast()
{
    period = new MockBasisPeriod(91, true);

    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar.set(2004, Calendar.OCTOBER, 13, 0, 0, 0); // Tuesday, October 13,
                                                        // 2004

    period.setCalendar(calendar);

    Date endDate = period.getEndDate();

    Calendar calendar2 = Calendar.getInstance(TimeZone.getTimeZone("CST"));
    calendar2.set(2004, Calendar.JANUARY, 14, 0, 0, 0); // Monday, January 13,
                                                        // 2004
    calendar2 = DateUtils.truncate( calendar2, Calendar.DATE );

    assertEquals(calendar2.getTime(), endDate);
}

public void testGetStartInCSTEndInCST()
{
    period = new MockBasisPeriod(91, false);

    Calendar calendar = Calendar.getInstance();
    calendar.set(2004, Calendar.FEBRUARY, 14, 0, 0, 0); // February 14, 2004

    period.setCalendar(calendar);

    Date startDate = period.getStartDate();

    Calendar calendar2 = Calendar.getInstance();
    calendar2.set(2003, Calendar.NOVEMBER, 15, 0, 0, 0); // November 15, 2003
    calendar2 = DateUtils.truncate( calendar2, Calendar.DATE );

    assertEquals(calendar2.getTime(), startDate);
}

public void testGetStartInCSTEndInCDT()
{
    period = new MockBasisPeriod(91, false);

    Calendar calendar = Calendar.getInstance();
    calendar.set(2004, Calendar.JUNE, 14, 0, 0, 0); // February 14, 2004

    period.setCalendar(calendar);

    Date startDate = period.getStartDate();

    Calendar calendar2 = Calendar.getInstance();
    calendar2.set(2004, Calendar.MARCH, 15, 0, 0, 0); // November 15, 2003
    calendar2 = DateUtils.truncate( calendar2, Calendar.DATE );

    assertEquals(calendar2.getTime(), startDate);

}

}
