package biz.firstlook.commons.date;

import java.util.Calendar;

import org.apache.commons.lang.time.DateUtils;

public class MockBasisPeriod extends BasisPeriod
{

private Calendar calendar;

public MockBasisPeriod()
{
    super();
}

public MockBasisPeriod( int days, boolean forecast )
{
    super(days, forecast);
}

public Calendar getCalendar()
{
    calendar = DateUtils.truncate( calendar, Calendar.DATE );
    return calendar;
}

public void setCalendar( Calendar calendar )
{
    this.calendar = calendar;
}

}
