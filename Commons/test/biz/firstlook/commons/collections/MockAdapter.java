/**
 * 
 */
package biz.firstlook.commons.collections;


class MockAdapter implements Adapter<NewType, OldType>
{
	
	public MockAdapter() {
		super();
	}
	
	public NewType adaptItem(OldType oldItem) {
		NewType nt = new NewType();
		nt.setField1( oldItem.getField3() + 100 );
		nt.setField2( oldItem.getField4() + "_adapted" );
		return nt;
	}

	public OldType unadaptItem(NewType newItem) {
		OldType ot = new OldType();
		ot.field3 = newItem.field1 - 100;
		ot.field4 = newItem.field2.replaceFirst("_adapted", "");
		return ot;
	}
}