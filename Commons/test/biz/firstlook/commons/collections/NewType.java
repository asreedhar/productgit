/**
 * 
 */
package biz.firstlook.commons.collections;

class NewType
{
	
	int field1;
	String field2;
	public NewType(){
		super();
	}
	NewType(int i, String s)
	{
		field1 = i;
		field2 = s;
	}
	void setField1(int in)
	{
		field1 = in;
	}
	void setField2(String in)
	{
		field2 = in;
	}
}