/**
 * 
 */
package biz.firstlook.commons.collections;

class OldType 
{
	int field3;
	String field4;
	public OldType(){
		super();
	}
	OldType(int i, String s)
	{
		field3 = i;
		field4 = s;
	}
	int getField3()
	{
		return field3;
	}
	String getField4()
	{
		return field4;
	}
	public boolean equals( Object obj )
	{
		return  ( (((OldType)obj).field3 == this.field3) && (((OldType)obj).field4.equals( this.field4) )); 
	}
}