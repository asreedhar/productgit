package biz.firstlook.commons.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;

public class TestValidateableList extends TestCase {

	public void testValidatableList() {
		ValidatableList<Integer> emptyList = new ValidatableList<Integer>();
		assertTrue("new empty list is valid", emptyList.isValidState());
		
		ValidatableList<Integer> list = new ValidatableList<Integer>(new ArrayList<Integer>());
		assertFalse("new list should be invalid", list.isValidState() );
		list.setAsValid();
		assertTrue("set new list as valid", list.isValidState() );
		list.add( 1 );
		assertFalse("entry has been added", list.isValidState() );
		list.setAsValid();
		assertTrue( "setAsValid just called, should be valid", list.isValidState() );
		List<Integer> list2 = new ArrayList<Integer>();
		list2.add( 2 );
		list.addAll(list2);
		assertFalse("list has been added", list.isValidState() );
		list.setAsValid();
		assertTrue( "setAsValid just called, should be valid", list.isValidState() );
		list.remove( 2 );
		assertFalse("entry has been removed", list.isValidState() );
		list.setAsValid();
		assertTrue( "setAsValid just called, should be valid", list.isValidState() );
		list.add(2);
		list.setAsValid();
		assertTrue( list.isValidState());
		list.removeAll( list2 );
		assertFalse("entry has been removed", list.isValidState() );
		list.setAsValid();
		assertTrue( "setAsValid just called, should be valid", list.isValidState() );
		list.add(2);
		list.retainAll( list2 );
		assertFalse(" retain all was called", list.isValidState() );
		list.setAsValid();
		assertTrue( list.isValidState() );
		list.clear();
		assertTrue(" just calle clear", list.isValidState() );
		List<Integer> list3 = list.getList();
		
		boolean unsupportedOperatinoExceptionThrown = false;
		try {
			list3.add( 3 );
		} catch (UnsupportedOperationException e) {
			unsupportedOperatinoExceptionThrown = true;
		}
		assertTrue( "tried to modify refrence to validatable collection", unsupportedOperatinoExceptionThrown);

		Collection<Integer> col1 = list.getCollection();
		unsupportedOperatinoExceptionThrown = false;
		try {
			col1.add( 3 );
		} catch (UnsupportedOperationException e) {
			unsupportedOperatinoExceptionThrown = true;
		}
		assertTrue( "tried to modify refrence to validatable collection", unsupportedOperatinoExceptionThrown);
		
		list.setAsValid();
		assertTrue( "should be valid", list.isValidState() );
		list.setAsInvalid();
		assertFalse( "should be invalid", list.isValidState());
	}
	
}
