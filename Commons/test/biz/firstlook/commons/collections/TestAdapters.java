package biz.firstlook.commons.collections;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class TestAdapters extends TestCase 
{
	public TestAdapters() {
		super();
	}

	public TestAdapters( String name ) {
		super( name );
	}

	public void testNullCase()
	{
		assertNull("null list should return null from atapter!", Adapters.adapt(null, new MockAdapter()));
	}
	
	public void testListAdapter_adapt()
	{
		List<OldType> oldList = new ArrayList<OldType>();
		oldList.add( new OldType(1, "one") );
		oldList.add( new OldType(2, "two") );
		oldList.add( new OldType(3, "three") );
		oldList.add( new OldType(4, "four") );
		
		List<NewType> newList = Adapters.adapt(oldList, new MockAdapter() );
		assertEquals("adapted lists size is off!", oldList.size(), newList.size());
		assertEquals("element 1 int not adapted correctly!", 101, newList.get(0).field1 );
		assertEquals("element 2 int not adapted correctly!", 102, newList.get(1).field1 );
		assertEquals("element 3 int not adapted correctly!", 103, newList.get(2).field1 );
		assertEquals("element 4 int not adapted correctly!", 104, newList.get(3).field1 );
		assertEquals("element 1 string not adapted correctly!", "one_adapted", newList.get(0).field2 );
		assertEquals("element 2 string not adapted correctly!", "two_adapted", newList.get(1).field2 );
		assertEquals("element 3 string not adapted correctly!", "three_adapted", newList.get(2).field2 );
		assertEquals("element 4 string not adapted correctly!", "four_adapted", newList.get(3).field2 );
	}
	
	public void testListAdatper_unadapt()
	{
		List<NewType> newList = new ArrayList<NewType>();
		newList.add( new NewType(101, "one_adapted") );
		newList.add( new NewType(102, "two_adapted") );
		newList.add( new NewType(103, "three_adapted") );
		newList.add( new NewType(104, "four_adapted") );
		
		List<OldType> oldList = Adapters.unadapt(newList, new MockAdapter() );
		assertEquals("unadapted lists size is off!", oldList.size(), newList.size());
		assertEquals("element 1 int not unadapted correctly!", 1, oldList.get(0).field3 );
		assertEquals("element 2 int not unadapted correctly!", 2, oldList.get(1).field3 );
		assertEquals("element 3 int not unadapted correctly!", 3, oldList.get(2).field3 );
		assertEquals("element 4 int not unadapted correctly!", 4, oldList.get(3).field3 );
		assertEquals("element 1 string not unadapted correctly!", "one", oldList.get(0).field4 );
		assertEquals("element 2 string not unadapted correctly!", "two", oldList.get(1).field4 );
		assertEquals("element 3 string not unadapted correctly!", "three", oldList.get(2).field4 );
		assertEquals("element 4 string not unadapted correctly!", "four", oldList.get(3).field4 );
	}
	
	public void testListAdapter_adapt_then_unadapt()
	{
		List<OldType> oldList = new ArrayList<OldType>();
		oldList.add( new OldType(1, "one") );
		oldList.add( new OldType(2, "two") );
		oldList.add( new OldType(3, "three") );
		oldList.add( new OldType(4, "four") );
		
		List<NewType> newList = Adapters.adapt(oldList, new MockAdapter() );
		List<OldType> oldList_after_adapts = Adapters.unadapt(newList, new MockAdapter() );
		assertTrue("adapt then unadapt didn't fly!", oldList.get(0).equals( oldList_after_adapts.get(0) ) );
		assertTrue("adapt then unadapt didn't fly!", oldList.get(1).equals( oldList_after_adapts.get(1) ) );
		assertTrue("adapt then unadapt didn't fly!", oldList.get(2).equals( oldList_after_adapts.get(2) ) );
		assertTrue("adapt then unadapt didn't fly!", oldList.get(3).equals( oldList_after_adapts.get(3) ) );
	}
	
}
