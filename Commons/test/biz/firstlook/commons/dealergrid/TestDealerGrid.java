package biz.firstlook.commons.dealergrid;

import junit.framework.TestCase;

public class TestDealerGrid extends TestCase
{

private int GREEN_LIGHT = 3;
private int YELLOW_LIGHT = 2;
private int RED_LIGHT = 1;

public int POWERZONE = 1;
public int WINNERS = 2;
public int GOOD_BETS = 3;
public int MARKET_PERFORMER_OR_OTHER_GREEN = 4;
public int MARKET_PERFORMER_OR_MANAGERS_CHOICE = 5;
public int MANAGERS_CHOICE = 6;

private DealerGrid dealerGrid;
private IDealerGridPreference dealerGridPreference;

public TestDealerGrid( String arg0 )
{
    super(arg0);
}

public void setUp()
{
    dealerGridPreference = new MockDealerGridPreference();

    dealerGrid = new DealerGrid(dealerGridPreference);

    ((MockDealerGridPreference) dealerGridPreference)
            .setFirstValuationThreshold(0);
    ((MockDealerGridPreference) dealerGridPreference)
            .setSecondValuationThreshold(500);
    ((MockDealerGridPreference) dealerGridPreference)
            .setThirdValuationThreshold(1000);
    ((MockDealerGridPreference) dealerGridPreference)
            .setFourthValuationThreshold(1500);
    ((MockDealerGridPreference) dealerGridPreference).setFirstDealThreshold(2);
    ((MockDealerGridPreference) dealerGridPreference).setSecondDealThreshold(4);
    ((MockDealerGridPreference) dealerGridPreference).setThirdDealThreshold(7);
    ((MockDealerGridPreference) dealerGridPreference)
            .setFourthDealThreshold(12);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(0,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(1,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(2,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(3,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(4,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference)
            .setGridValue(5, RED_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(6,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(7,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(8,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(9,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(10,
            RED_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(11,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(12,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(13,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(14,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(15,
            RED_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(16,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(17,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(18,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(19,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(20,
            RED_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(21,
            YELLOW_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(22,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(23,
            GREEN_LIGHT);
    ((MockDealerGridPreference) dealerGridPreference).setGridValue(24,
            GREEN_LIGHT);

    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(0,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(1,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(2,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(3,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(4,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(5,
            MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(6,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(7,
            MARKET_PERFORMER_OR_OTHER_GREEN);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(8,
            MARKET_PERFORMER_OR_OTHER_GREEN);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(9,
            MARKET_PERFORMER_OR_OTHER_GREEN);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(10,
            MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(11,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(12,
            GOOD_BETS);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(13,
            GOOD_BETS);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(14,
            GOOD_BETS);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(15,
            MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(16,
            MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(17,
            WINNERS);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(18,
            WINNERS);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(19,
            WINNERS);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(20,
            MANAGERS_CHOICE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(21,
            POWERZONE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(22,
            POWERZONE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(23,
            POWERZONE);
    ((MockDealerGridPreference) dealerGridPreference).setCIAGridValue(24,
            POWERZONE);

}

public void testDetermineDealerGridCell()
{
    assertEquals("Grid light value is G", GREEN_LIGHT, (dealerGrid
            .determineDealerGridCell(1000, 7)).getLightValue());
    assertEquals("CIA value is good bet", GOOD_BETS, (dealerGrid
            .determineDealerGridCell(1000, 7)).getCiaTypeValue());

    assertEquals("Grid light value is Y", YELLOW_LIGHT, (dealerGrid
            .determineDealerGridCell(0, 2)).getLightValue());
    assertEquals("CIA value is MP_MC", MARKET_PERFORMER_OR_MANAGERS_CHOICE,
            (dealerGrid.determineDealerGridCell(0, 2)).getCiaTypeValue());

    assertEquals("Grid light value is Y", YELLOW_LIGHT, (dealerGrid
            .determineDealerGridCell(500, 4)).getLightValue());
    assertEquals("CIA value is MP_MC", MARKET_PERFORMER_OR_MANAGERS_CHOICE,
            (dealerGrid.determineDealerGridCell(500, 4)).getCiaTypeValue());

    assertEquals("Grid light value is G", GREEN_LIGHT, (dealerGrid
            .determineDealerGridCell(1500, 12)).getLightValue());
    assertEquals("CIA value is a winner", WINNERS, (dealerGrid
            .determineDealerGridCell(1500, 12)).getCiaTypeValue());

    assertEquals("Grid light value is G", GREEN_LIGHT, (dealerGrid
            .determineDealerGridCell(3000, 13)).getLightValue());
    assertEquals("CIA value is power zone", POWERZONE, (dealerGrid
            .determineDealerGridCell(3000, 13)).getCiaTypeValue());

    assertEquals("Grid light value is R", RED_LIGHT, (dealerGrid
            .determineDealerGridCell(0, 4)).getLightValue());
    assertEquals("CIA value is manager's choice", MANAGERS_CHOICE, (dealerGrid
            .determineDealerGridCell(0, 4)).getCiaTypeValue());

    assertEquals("Grid light value is G", GREEN_LIGHT, (dealerGrid
            .determineDealerGridCell(1500, 4)).getLightValue());
    assertEquals("CIA value is market performer or OG",
            MARKET_PERFORMER_OR_OTHER_GREEN, (dealerGrid
                    .determineDealerGridCell(1500, 4)).getCiaTypeValue());
}

}
