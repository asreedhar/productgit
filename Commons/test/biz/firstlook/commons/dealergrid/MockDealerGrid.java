package biz.firstlook.commons.dealergrid;


public class MockDealerGrid implements IDealerGrid
{

private DealerGridCell cell;
private IDealerGridPreference dlp;

public MockDealerGrid( DealerGridCell cell )
{
    this.cell = cell;
}

public IDealerGridPreference getDealerGridPreference()
{
    return dlp;
}

public void setDealerGridPreference( IDealerGridPreference preference )
{
    dlp = preference;
}

public DealerGridCell determineDealerGridCell( double valuation, int unitsSold )
{
    return cell;
}

public DealerGridCell getCell()
{
    return cell;
}

public void setCell( DealerGridCell cell )
{
    this.cell = cell;
}

}
