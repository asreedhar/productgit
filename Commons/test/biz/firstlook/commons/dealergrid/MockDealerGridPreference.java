package biz.firstlook.commons.dealergrid;

import java.util.ArrayList;
import java.util.List;

public class MockDealerGridPreference implements IDealerGridPreference
{

private int firstValuationThreshold;
private int secondValuationThreshold;
private int thirdValuationThreshold;
private int fourthValuationThreshold;
private int firstDealThreshold;
private int secondDealThreshold;
private int thirdDealThreshold;
private int fourthDealThreshold;
private List<Integer> gridValues;
private List<Integer> gridCIATypeValues;

public MockDealerGridPreference()
{
    super();
    gridValues = new ArrayList<Integer>();
    gridCIATypeValues = new ArrayList<Integer>();
}

public int getFirstDealThreshold()
{
    return firstDealThreshold;
}

public int getFirstValuationThreshold()
{
    return firstValuationThreshold;
}

public int getGridValue( int grid )
{
    return ((Integer) gridValues.get(grid)).intValue();
}

public int getSecondDealThreshold()
{
    return secondDealThreshold;
}

public int getSecondValuationThreshold()
{
    return secondValuationThreshold;
}

public int getThirdDealThreshold()
{
    return thirdDealThreshold;
}

public int getThirdValuationThreshold()
{
    return thirdValuationThreshold;
}

public void setFirstDealThreshold( int i )
{
    firstDealThreshold = i;
}

public void setFirstValuationThreshold( int i )
{
    firstValuationThreshold = i;
}

public void setGridValue( int position, int value )
{
    gridValues.add(position, new Integer(value));
}

public void setCIAGridValue( int position, int value )
{
    gridCIATypeValues.add(position, new Integer(value));
}

public void setSecondDealThreshold( int i )
{
    secondDealThreshold = i;
}

public void setSecondValuationThreshold( int i )
{
    secondValuationThreshold = i;
}

public void setThirdDealThreshold( int i )
{
    thirdDealThreshold = i;
}

public void setThirdValuationThreshold( int i )
{
    thirdValuationThreshold = i;
}

public void setFourthDealThreshold( int i )
{
    fourthDealThreshold = i;
}

public void setFourthValuationThreshold( int i )
{
    fourthValuationThreshold = i;
}

public int getGridCIATypeValue( int grid )
{
    return ((Integer) gridCIATypeValues.get(grid)).intValue();
}

public int getGridLightValue( int grid )
{
    return ((Integer) gridValues.get(grid)).intValue();
}

public int getFourthDealThreshold()
{
    return fourthDealThreshold;
}

public int getFourthValuationThreshold()
{
    return fourthValuationThreshold;
}

}
