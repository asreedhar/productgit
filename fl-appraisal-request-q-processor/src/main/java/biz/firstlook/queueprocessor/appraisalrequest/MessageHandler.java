package biz.firstlook.queueprocessor.appraisalrequest;

import com.amazonaws.services.sqs.model.Message;

public abstract class MessageHandler {

	public abstract String process(Message msg ) throws Exception;
	
}
