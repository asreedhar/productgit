package biz.firstlook.queueprocessor.appraisalrequest;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.services.sqs.AmazonSQSAsyncClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.util.json.JSONObject;

/**
 * 
 * Reads the request.
 * 
 * @author lcollins
 */
public class RequestReader extends SQSClient implements Runnable {
	static Logger log = Logger.getLogger( RequestReader.class );
	private boolean bAbort = false;
	MessageHandler handler = null;	
	RetryManager retryMgr = null;
	public RequestReader(String accessKey, String secretKey, String queueName,
			MessageHandler handler,
			RetryManager retryMgr) {
		super(accessKey, secretKey, queueName);
		this.handler = handler;
		this.retryMgr = retryMgr;
	}

	public void start() {
		new Thread(this).start();
	}
	public void run() {
		ReceiveMessageRequest rmr = new ReceiveMessageRequest(qUrl);
		rmr.setMaxNumberOfMessages(1);
		while (!bAbort) {
			ReceiveMessageResult msgResult = awsClient.receiveMessage(rmr);
			List<Message> lMsgs = msgResult.getMessages();
			if (lMsgs.size() ==0){
				log.info("No messages found in Q.");
				System.out.println("No messages found in Q.");
				try {
					Thread.currentThread();
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					log.error("interrupted while waiting to read Q.",e );
					System.err.println("Interrupted while waiting to read Q.");
				}
			}
			for (Iterator<Message> iterator = lMsgs.iterator(); (iterator.hasNext() && !bAbort);) {
				Message message = (Message) iterator.next();
				// simply get the message and pass it to the handler
				// That's it!
				try{
					System.out.println("Received message: " + message.getMessageId()+" : procesing ...");
					String response = handler.process(message);
					System.out.println("Message: " + message.getMessageId()+" : results: "+ response);
					log.info("Message: "+message.getMessageId()+" : results: "+ response);
				}catch(Exception e){
					log.error("Could not process message:"+message.getMessageId(), e);
				}
			}
		}
	}

	public void abort() {
		bAbort = true;
	}
}
