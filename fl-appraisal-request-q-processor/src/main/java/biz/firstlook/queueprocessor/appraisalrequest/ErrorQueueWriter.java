package biz.firstlook.queueprocessor.appraisalrequest;

import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

/**
 * 
 * Reads the request.
 * 
 * @author lcollins
 */
public class ErrorQueueWriter extends SQSClient {

	public ErrorQueueWriter(String accessKey, String secretKey, String queueName) {
		super(accessKey, secretKey, queueName);
	}

	public void writeBadRequest(String badRequest){
		SendMessageRequest sendMessageRequest = new SendMessageRequest(qUrl, badRequest);
		this.awsClient.sendMessage(sendMessageRequest);	
	}
}
