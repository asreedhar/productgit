package biz.firstlook.queueprocessor.appraisalrequest;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;

public class SQSClient {
	protected AmazonSQSClient awsClient;
	protected String qUrl = null;
	public SQSClient(String accessKey, String secretKey, String queueName) {
		AWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
		awsClient = new AmazonSQSClient(creds);
		// get the Q url
		GetQueueUrlResult result = awsClient.getQueueUrl(new GetQueueUrlRequest(queueName));
		qUrl = result.getQueueUrl();
	}
}
