package biz.firstlook.queueprocessor.appraisalrequest.exceptions;

public class ServiceAccessException extends Exception {

	public ServiceAccessException() {
		super();
	}

	public ServiceAccessException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceAccessException(String message) {
		super(message);
	}

	public ServiceAccessException(Throwable cause) {
		super(cause);
	}
	

}
