package biz.firstlook.queueprocessor.appraisalrequest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import biz.firstlook.queueprocessor.appraisalrequest.exceptions.ServiceAccessException;

public class AppraisalServiceClient {
	static Logger log = Logger.getLogger(AppraisalServiceClient.class);
	String resource = "firstlook-external-rs/resource/appraisal/mobile";
	String host;
	int port;
	String url;
	RestTemplate template = null;
	org.springframework.http.HttpHeaders headers = null;

	// DefaultHttpClient client = null;
	public AppraisalServiceClient(String protocol, String host, int port,
			String username, String password) {
		super();
		this.host = host;
		this.port = port;
		this.url = protocol + "://" + host + ":" + port + "/" + resource;
		URL urlObj;
		Map<String, String> hdrs = createHeaders(username, password);
		headers = new org.springframework.http.HttpHeaders();
		headers.setAll(hdrs);
		try {
			urlObj = new URL(this.url);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("URL:[" + this.url + "]", e);
		}

		log.debug("AppraisalServiceClient w/ username: " + username);
		template = new RestTemplate();
	}

	Map<String, String> createHeaders(String username, String password) {
		Map<String, String> hdrs = new HashMap<String, String>();
		String auth = username + ":" + password;
		byte[] encodedAuth = jodd.util.Base64.encodeToByte(auth.getBytes());
		String authHeader = "Basic " + new String(encodedAuth);
		hdrs.put("Authorization", authHeader);
		return hdrs;
	};

	public String postRequest(String jsonAppraisalRequest)
			throws ServiceAccessException {
		String ret = null;
		/// add the credentials to the call
		try {

			HttpEntity<String> entity = new HttpEntity<String>(
					jsonAppraisalRequest, headers);
			ret = template.exchange(url, HttpMethod.POST, entity, Object.class)
					.getBody().toString();

		} catch (Exception e) {
			throw new ServiceAccessException(
					"Error posting appraisal request to URL: " + url, e);
		}
		return ret;
	}
}
