package biz.firstlook.queueprocessor.appraisalrequest;

public class Constants {
	static class keys {
		final static String RETRY = "retry";
		final static String RETRY_COUNT = "retryCount";
		public static final String MSG_METADATA = "metadata";
		public static final String EXCEPTION = "exception";
		public static final String EXCEPTION_MSG = "exceptionMessage";
		public static final String ERROR_MSG = "errorMessage";
		

	}
	static class config {
		final static String SQS_ACCESS_KEY = "sqs.accessKey";
		final static String SQS_SECRET_KEY = "sqs.secretKey";
		final static String SQS_APPRAISAL_QUEUE_NAME = "sqs.appraisalQueueName";
		final static String SQS_ERROR_QUEUE_NAME = "sqs.errorQueueName";
		final static String RETRY_COUNT = "retryCount";
		public static final String SVC_HOST = "svc.host";
		public static final String SVC_PORT = "svc.port";
		public static final String SVC_USER = "svc.user";
		public static final String SVC_PASSWORD = "svc.password";
		public static final String SVC_PROTOCOL = "svc.protocol";

	}
	
	
	
}
