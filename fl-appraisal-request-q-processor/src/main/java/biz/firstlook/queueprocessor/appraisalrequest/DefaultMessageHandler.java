package biz.firstlook.queueprocessor.appraisalrequest;

import org.apache.log4j.Logger;

import biz.firstlook.queueprocessor.appraisalrequest.exceptions.ServiceAccessException;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

public class DefaultMessageHandler extends MessageHandler {
	static Logger log = Logger.getLogger(DefaultMessageHandler.class);

	Transformer transformer = new Transformer();
	AppraisalServiceClient svcClient = null;
	ErrorQueueWriter eqw = null;
	QueueManager qMgr = null;
	RetryManager retryMgr = null;

	public DefaultMessageHandler(AppraisalServiceClient svcClient,
			RetryManager retryMgr, QueueManager qMgr, ErrorQueueWriter eqw) {
		this.eqw = eqw;
		this.qMgr = qMgr;
		this.retryMgr = retryMgr;
		this.svcClient = svcClient;
	}

	@Override
	public String process(Message msg) throws Exception {
		String resp = null;
		try {

			JSONObject origReq = new JSONObject(msg.getBody());
			String request = transformer.transformRequest(origReq.toString(4));
			JSONObject jsonReq = new JSONObject(request);
			// Transform the message
			log.debug("DefaultMessageHandler: transformed to:"+jsonReq.toString(4));
			try {
				resp = svcClient.postRequest(request);
				log.debug("response:"+resp);
			} catch ( Exception e ) {
				log.error("Exception thrown accessing appraisal service.", e );
				resp = createErrorObject( e );
				log.debug("Created obj for exception:"+resp, e );				
			}
			JSONObject jsonResp = null;
			try {
				jsonResp = new JSONObject(resp==null ? "{}" : resp);
			} catch (Exception e) {
				qMgr.removeMessage(msg.getReceiptHandle());
				String errMsg = "Bad response from service (should be valid JSON): "+e.getLocalizedMessage();
				JSONObject meta = retryMgr.createMetadataObject(0, errMsg );
				origReq = retryMgr.addMetadata(origReq, meta);
				eqw.writeBadRequest(origReq.toString(4));
				return errMsg;
			}
			JSONObject metadata = 
				retryMgr.getMetadata(origReq);
			if (isErrorResponse(resp)) {
				JSONObject exception = jsonResp.getJSONObject(Constants.keys.EXCEPTION);
				boolean bRetry = exception.has(Constants.keys.RETRY) ? (exception).getBoolean(Constants.keys.RETRY) : false;
				if (bRetry) {
					// THIS MESSAGE may be RETRIED!
					// if this msg has retry info
					if (metadata != null) {
						// / if that was the last retry
						if (retryMgr.isLastRetry(origReq)) {
							// remove from Q
							qMgr.removeMessage(msg.getReceiptHandle());
							// send to the error Q
							//write the orig w/o metadata eqw.writeBadRequest(msg.getBody());
							eqw.writeBadRequest(origReq.toString(4));
						} else {
							// decrement retryCount
							origReq = retryMgr.decrementRetry(origReq);
							replaceMessage(msg.getReceiptHandle(), origReq
									.toString(4));
						}
					} else {
						// THERE IS NO METADATA ON MSG YET
						// create retryCount
						origReq = retryMgr.addMetadata(origReq, null);
						replaceMessage(msg.getReceiptHandle(), origReq
								.toString(4));
					}
				} else {
					// can't retry this one
					// remove from Q
					qMgr.removeMessage(msg.getReceiptHandle());//exception
					JSONObject meta = retryMgr.createMetadataObject(0, exception );
					origReq = retryMgr.addMetadata(origReq, meta);
					// send to the error Q
					eqw.writeBadRequest(origReq.toString());
				}
			} else {
				// / No Error in Response
				// remove from Q
				qMgr.removeMessage(msg.getReceiptHandle());
			}
		} catch (Exception ex) {
			log.error("Error processing message.", ex);
			qMgr.removeMessage(msg.getReceiptHandle());
			//System.err.println("Error processing message: "+ex.getMessage());
			resp = createErrorObject(ex);
		}
		return resp;
	};

	private String createErrorObject(Exception e) {
		JSONObject exception = new JSONObject();
		JSONObject obj = new JSONObject();
		String ret = null;
		try {
			exception.put(Constants.keys.EXCEPTION_MSG, e.getMessage());
			exception.put(Constants.keys.RETRY, false);
			if (e instanceof ServiceAccessException) {
				ServiceAccessException message = (ServiceAccessException) e;
				exception.put(Constants.keys.ERROR_MSG, message.getMessage());
				exception.put(Constants.keys.RETRY, true);
			}
			obj.put(Constants.keys.EXCEPTION, exception);
			ret = obj.toString(4);
		} catch (Exception ex) {
			// unlikely 
			ret = "{ \"exception\": \"exceptionMessage\":{\"Internal error\"}}";
		}
		return ret;
	}

	public String replaceMessage(String msgRcptHandle, String newMsgBody) {
		// remove from Q
		qMgr.removeMessage(msgRcptHandle);
		log.debug("Removed msg with rcptHandle: " + msgRcptHandle);
		// re-add the message with new metadata
		log.debug("Creating new msg: " + newMsgBody);
		return qMgr.writeMessage(newMsgBody);
	}

	boolean isErrorResponse(String jsonResponse) throws JSONException {
		JSONObject obj = new JSONObject(jsonResponse);
		return (obj.has(Constants.keys.EXCEPTION));
	}
}
