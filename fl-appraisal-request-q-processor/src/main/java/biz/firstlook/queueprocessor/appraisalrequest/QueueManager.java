package biz.firstlook.queueprocessor.appraisalrequest;

import org.apache.log4j.Logger;

import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

/**
 * 
 * Reads the request.
 * 
 * @author lcollins
 */
public class QueueManager extends SQSClient  {
	static Logger log = Logger.getLogger( QueueManager.class );
	public QueueManager(String accessKey, String secretKey, String queueName) {
		super(accessKey, secretKey, queueName);	 
	}
	public void removeMessage(String msgRecptHandle){
		DeleteMessageRequest dmr = new DeleteMessageRequest(qUrl, msgRecptHandle);
		awsClient.deleteMessage(dmr);		
	}
	public String writeMessage(String msgBody) {
		SendMessageRequest smr = new SendMessageRequest(qUrl, msgBody);
		SendMessageResult res = awsClient.sendMessage(smr);
		return res.getMessageId();
	}
}
