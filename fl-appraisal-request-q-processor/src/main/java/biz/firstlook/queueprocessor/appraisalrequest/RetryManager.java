package biz.firstlook.queueprocessor.appraisalrequest;

import java.util.Properties;

import org.apache.log4j.Logger;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

/**
 * This class manages the retryCount of a request. Determines whether a message 
 * is to be retried and if so, how many times.
 * 
 * Has functions to manipulate the retry value.
 * 
 * @author lcollins
 *
 */
public class RetryManager {
	static Logger log = Logger.getLogger( RetryManager.class );
	Properties config = null;
	int defaultRetry = 3;
	public RetryManager(Properties config){
		this.config = config;
		//log.debug("\nCreating RetryManager w/ config: "+ (config != null ? config.toString() : "NULL" ));
		try{
			this.defaultRetry = Integer.parseInt(config.getProperty(Constants.config.RETRY_COUNT));
		}catch(Exception e){
			log.error("Could not create retry msg: Bad or missing retryCount. Setting to: "+defaultRetry, e);
		}
	}

	public JSONObject getMetadata(JSONObject message)throws JSONException{
		return  message.has(Constants.keys.MSG_METADATA) ?  message.getJSONObject(Constants.keys.MSG_METADATA) : null;
	}

	public JSONObject addMetadata(JSONObject message, JSONObject metadata)throws JSONException{
		message.put(Constants.keys.MSG_METADATA, metadata==null ? createMetadataObject(defaultRetry) : metadata);
		return message;
	}

	public boolean isLastRetry(JSONObject message)throws JSONException{
		return  !getMetadata(message).has(Constants.keys.RETRY_COUNT) || getMetadata(message).getInt(Constants.keys.RETRY_COUNT) < 1;
	}

	public JSONObject decrementRetry(JSONObject message)throws JSONException{
		JSONObject meta = getMetadata(message);
		if(null == meta)
			return message;
		int retryCnt = meta.getInt(Constants.keys.RETRY_COUNT) - 1;
		meta.put(Constants.keys.RETRY_COUNT, retryCnt);
		return addMetadata(message, meta);
	}
	
	public JSONObject createMetadataObject(int retryCount) throws JSONException{		
		JSONObject obj = new JSONObject();
		obj.put(Constants.keys.RETRY_COUNT, defaultRetry);//
		return obj;
	}
	public JSONObject createMetadataObject(int retryCount, String message) throws JSONException{		
		JSONObject obj = new JSONObject();
		obj.put(Constants.keys.RETRY_COUNT, retryCount);//
		if(message != null)
			obj.put(Constants.keys.ERROR_MSG, message);//
		return obj;
	}

	public JSONObject createMetadataObject(int retryCount, JSONObject errorObj) throws JSONException{		
		JSONObject obj = new JSONObject();
		//String message = errorObj.toString(4);
		obj.append(Constants.keys.RETRY_COUNT, retryCount);//
		if(errorObj != null)
			obj.put(Constants.keys.ERROR_MSG, errorObj);//
		return obj;
	}

}
