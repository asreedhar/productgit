package biz.firstlook.queueprocessor.appraisalrequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

public class RequestProcessor {
	static Logger log = Logger.getLogger( DefaultMessageHandler.class );
	RestTemplate storeAppraisal = new RestTemplate();
	Properties config = null;

	public static void main( String[] argv ) {
		File f = new File(argv[0]);
		if(!f.exists()){
			String msg = "No such config file: "+argv[0];
			log.error(msg);
			System.err.println(msg);
			System.exit(-1);
		}
		Properties config = loadConfigFile(f);
		if(config == null){
			String msg = "Could not read config file: "+argv[0];
			log.error( msg );
			System.err.println(msg);
			System.exit(-2);
		}
		log.debug("Creating requestProcessor w/ config: "+ config.toString());
	
		new RequestProcessor( config );
	}
	
	static Properties loadConfigFile( File f ){
		Properties config = new Properties();
		try {
			InputStream in =
				new FileInputStream(f);
			config.load(in);
		} catch (Exception e) {
			log.error("Could not read config file: "+e.getMessage(), e);
			//System.err.println("Could not read config file: "+e.getMessage());	
			config=null;
		}
		return config;	
	}

	public RequestProcessor( Properties config ) {
		this(
				config.getProperty(Constants.config.SVC_PROTOCOL),
				config.getProperty(Constants.config.SVC_HOST),
				Integer.parseInt(config.getProperty(Constants.config.SVC_PORT)),
				config.getProperty(Constants.config.SQS_ACCESS_KEY),		
				config.getProperty(Constants.config.SQS_SECRET_KEY),		
				config.getProperty(Constants.config.SQS_APPRAISAL_QUEUE_NAME),		
				config.getProperty(Constants.config.SQS_ERROR_QUEUE_NAME),
				config
		);
		
	}

	public RequestProcessor( String protocol, String host, int port, String sqsAccessKey, String sqsSecretKey, 
		String queueName, String errorQueueName, Properties config) {
		this.config = config;
		RetryManager retryMgr = new RetryManager(config);
		final ErrorQueueWriter eqw = new ErrorQueueWriter(sqsAccessKey, sqsSecretKey, errorQueueName);
		final QueueManager qMgr = new QueueManager(sqsAccessKey, sqsSecretKey, queueName);
		AppraisalServiceClient svcClient = new AppraisalServiceClient(protocol, host, port, 
				config.getProperty(Constants.config.SVC_USER), 
				config.getProperty(Constants.config.SVC_PASSWORD));
		final DefaultMessageHandler msgHandler = 
			new DefaultMessageHandler(svcClient, retryMgr, qMgr, eqw);
		try {
			RequestReader rdr = new RequestReader(sqsAccessKey, sqsSecretKey, queueName,
					msgHandler, retryMgr);
			rdr.start();
			log.debug("RequestReader started.");
		} catch (Throwable e) {
			log.error("RequestReader was interrupted!",e);			
		}
	}
}
