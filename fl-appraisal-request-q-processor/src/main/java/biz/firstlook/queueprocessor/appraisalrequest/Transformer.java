package biz.firstlook.queueprocessor.appraisalrequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

public class Transformer {
	static Logger log = Logger.getLogger( Transformer.class );
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	private String dateString(Date d) {
		return sdf.format(d); 
	}
	public String transformRequest(String jsonAppraisal) throws JSONException {

		JSONObject inReq = new JSONObject(jsonAppraisal);
//		System.out.println("Transformer.transfornRequest(): transforming: ["+inReq.toString(4)+"]");
		JSONObject inAppraisalReq = inReq.getJSONObject("appraisalRequest");
		// the obj to return
		JSONObject outReq = new JSONObject();

		// / Get the trimid/mileage of the first request
//		String trimId = getFirstBookValuationRequestTrimId(inAppraisalReq);
		Integer mileage = getFirstBookValuationRequestMileage(inAppraisalReq);
		// //////////////////////////////////////////////////
		// ///// Main Object
		// //////////////////////////////////////////////////
		outReq.put("businessUnitId", inAppraisalReq.getString("dealer"));
		outReq.put("appraisalCreateTime", dateString(new Date()));
	    if( inAppraisalReq.has("salespersonId") && inAppraisalReq.get("salespersonId") !=null && !inAppraisalReq.getString("salespersonId").equalsIgnoreCase("null") && !inAppraisalReq.getString("salespersonId").equalsIgnoreCase("")){
	    	outReq.put ("salespersonId",inAppraisalReq.get("salespersonId"));
	    }
	    if( inAppraisalReq.has("uniqueId") && inAppraisalReq.get("uniqueId") !=null && !inAppraisalReq.getString("uniqueId").equalsIgnoreCase("null") && !inAppraisalReq.getString("uniqueId").equalsIgnoreCase("")){
	    	outReq.put ("uniqueId",inAppraisalReq.get("uniqueId"));
	    }
	   
	    if ( inAppraisalReq.has("photoUrl") && !inAppraisalReq.isNull("photoUrl")){
	    	outReq.put("photoUrl", inAppraisalReq.get("photoUrl"));
	    	 System.out.println("processed phots successfully");
	 	    log.info("PHOTS PROCESSED SUCCESSFULLY ____*-*-*-*-*-*-*-*-*--*-*-=*-*-*-");

	    }
	    
	    if( inAppraisalReq.has("reconNotes") && inAppraisalReq.get("reconNotes") !=null && !inAppraisalReq.getString("reconNotes").equalsIgnoreCase("null") && !inAppraisalReq.getString("reconNotes").equalsIgnoreCase("")){
	    	outReq.put("reconNotes", inAppraisalReq.get("reconNotes"));
	 	    log.info("Reconditioning Notes Processed");

	    }
	    

	   
	    
	    
		String appType = translateAppraisalType(inAppraisalReq
				.getString("appraisalType"));
		if(appType == null)
		{
			throw new JSONException("Bad value for 'appraisalType' : '"+inAppraisalReq
				.getString("appraisalType")+"'");
		}
		outReq.put("appraisalType", appType);
		outReq.put("appraisalSource", "Mobile");

		// //////////////////////////////////////////////////
		// ///// MMR Vehicle Object
		// //////////////////////////////////////////////////
		JSONObject mmrVehicleObject = null;
		if (inAppraisalReq.has("mmrVehicle")){
			mmrVehicleObject = inAppraisalReq.getJSONObject(("mmrVehicle"));
		}
		// //////////////////////////////////////////////////
		// ///// Vehicle Object
		// //////////////////////////////////////////////////
		Map<String, Object> vehicleObject = new HashMap<String, Object>();
		vehicleObject.put("vin", inAppraisalReq.getString("vin"));
		vehicleObject.put("color", inAppraisalReq.getString("color"));
		if( mileage != null ){
			vehicleObject.put("mileage", mileage);
		}

		// //////////////////////////////////////////////////
		// ///// Book Out Objects
		// //////////////////////////////////////////////////
		JSONArray bookOuts = new JSONArray();
		JSONArray bvrArr = inAppraisalReq.getJSONArray("bookValuationRequests");

		// / loop thru the book valuations creating bookOutObjects for each
		for (int i = 0; i != bvrArr.length(); ++i) {
			JSONObject bvr = bvrArr.getJSONObject(i).getJSONObject(
					"bookValuationRequest");
			JSONObject bookOutObj = transformBvgToBookOut(bvr);
			bookOuts.put(bookOutObj);
		}
		vehicleObject.put("bookouts", bookOuts);
		// //////////////////////////////////////////////////
		// ///// Prices Objects
		// //////////////////////////////////////////////////
		JSONObject prices = new JSONObject();
		prices.put("appraisalValue", inAppraisalReq
				.getString("appraisalAmount"));
		prices.put("targetGrossProfit", inAppraisalReq
				.getString("targetGrossProfit"));
		prices.put("estimatedReconditioning", inAppraisalReq
				.getString("estimatedRecon"));
	
		if ( mmrVehicleObject != null )
			outReq.put("mmrVehicle", mmrVehicleObject);
		outReq.put("vehicle", vehicleObject);
		outReq.put("prices", prices);
		return outReq.toString();

	}

	private String translateAppraisalType(String str) {
		// Only 'T' is allowed
		if (str.equals("T"))
			return "Trade-In";
		if (str.equals("P"))
			return "Purchase";
		return null;
	}

	JSONObject transformBvgToBookOut(JSONObject bvr)
			throws JSONException {
		JSONObject bookOut = new JSONObject();
		String trimId = bvr.getString("trimId");
		bookOut.put("bookId", translateBookId(bvr.getString("book")));
		bookOut.put("styleKey", trimId);// same for all

		JSONArray bkOptions = new JSONArray();
		JSONArray equipmentIds = bvr.getJSONArray("equipmentSelected");
		
		for (int j = 0; j != equipmentIds.length(); ++j) {
			String equipId = equipmentIds.getString(j);
			JSONObject bkOption = new JSONObject();
			bkOption.put("key", equipId);
			bkOption.put("value", "0");
			bkOption.put("selected", "true");
			bkOption.put("optionType", "Equipment");

			bkOptions.put(bkOption);
		}
		bookOut.put("bookOptions", bkOptions);
		return bookOut;
	}

	Map<String, String> bookIdMap = createBookIdMap();
//	Map<String, String> appraisalSourceMap = createAppraisalSourceMap();
	private String translateBookId(String flSvcBookId) {
		String ret = bookIdMap.get(flSvcBookId);
		return ret;
	}

	private Map<String, String> createBookIdMap() {
		Map<String, String> bookIdMap = new HashMap<String, String>();
		bookIdMap.put("nada", "NADA");
		bookIdMap.put("blackbook", "BLACKBOOK");
		bookIdMap.put("kbb", "KELLEY");
		bookIdMap.put("galves", "GALVES");
		return bookIdMap;
	}

	private Integer getFirstBookValuationRequestMileage(
			JSONObject appraisalRequest) throws JSONException {
		Integer mileage = null;
		JSONArray bvrList = appraisalRequest
				.getJSONArray("bookValuationRequests");
		JSONObject bvr = bvrList.getJSONObject(0).getJSONObject("bookValuationRequest");
		if(! bvr.isNull("mileage")){
			mileage = bvr.getInt("mileage");
		}
		return mileage;
	}   
}
