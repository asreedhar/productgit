#!/bin/sh
set classpath=""
for i in  ./lib/*.jar 
do
	classpath=${classpath}:${i}; 
done;

echo "Running...";

java -cp "fl-appraisal-request-q-processor.jar:${classpath}" biz.firstlook.queueprocessor.appraisalrequest.RequestProcessor  q-processor.properties
echo "Running is done."
