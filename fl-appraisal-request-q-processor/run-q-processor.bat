@echo off

SETLOCAL

set CLASSPATH=
for %%f  in (.\lib\*.jar) do call :cp %%f
	SET CLASSPATH=%CLASSPATH%;.\conf

java -cp fl-appraisal-request-q-processor.jar;%CLASSPATH% biz.firstlook.queueprocessor.appraisalrequest.RequestProcessor ./q-processor.properties

ENDLOCAL
echo.

:cp
SET CLASSPATH=%CLASSPATH%;%1
