package biz.firstlook.modules.rest.test;

import junit.framework.Assert;
import org.junit.Test;
import biz.firstlook.rest.auth.CASClient;

public class CasClientTest {

	@Test
	public void testAuthMe(){
		CASClient c = new CASClient("https://betaauth.firstlook.biz/cas/");
		boolean auth = c.authenticate("lcollins", "N@d@123");
		Assert.assertTrue("Failed to auth me. Absurd!", auth);
	}

	@Test
	public void testDenyStranger(){
		CASClient c = new CASClient("https://betaauth.firstlook.biz/cas/");
		boolean auth = c.authenticate("Stranger", "Danger");
		Assert.assertFalse("Authed the stranger. Auth is BROKEN!", auth);		
	}


	@Test
	public void testWithoutSlash(){
		CASClient c = new CASClient("https://betaauth.firstlook.biz/cas");
		boolean auth = c.authenticate("lcollins", "N@d@123");
		Assert.assertTrue("Failed to auth me without slash. Absurd!", auth);
	}

	@Test
	public void testIntBWithoutSlash(){
		CASClient c = new CASClient("https://auth-b.int.firstlook.biz/cas");
		boolean auth = c.authenticate("lcollins", "N@d@123");
		Assert.assertTrue("Failed to auth me without slash. Absurd!", auth);
	}
	@Test
	public void testIntBAuthMe(){
		CASClient c = new CASClient("https://auth-b.int.firstlook.biz/cas");
		boolean auth = c.authenticate("lcollins", "N@d@123");
		Assert.assertTrue("Failed to auth me. Absurd!", auth);
	}

	@Test
	public void testIntBDenyStranger(){
		CASClient c = new CASClient("https://auth-b.int.firstlook.biz/cas/");
		boolean auth = c.authenticate("Stranger", "Danger");
		Assert.assertFalse("Authed the stranger. Auth is BROKEN!", auth);		
	}
}
