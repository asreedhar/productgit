package biz.firstlook.modules.rest.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import junit.framework.TestCase;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.TypeMismatchDataAccessException;

import biz.firstlook.module.rest.model.AppraisalDto;
import biz.firstlook.module.rest.model.BookOptionDto;
import biz.firstlook.module.rest.model.BookoutDto;
import biz.firstlook.module.rest.model.PricesDto;
import biz.firstlook.module.rest.model.VehicleDto;
import biz.firstlook.module.rest.service.AppraisalWebServiceWrapper;
import biz.firstlook.module.rest.service.IncompleteAppraisalException;
import biz.firstlook.module.rest.service.MessageException;

public class AppraisalServiceTests extends TestCase {

	AppraisalWebServiceWrapper appraisalSvc = null;
	InitialContext ic = null;

	protected void setUp() throws Exception {

		// setup jndi datasource
		try {
			// Create initial context
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
					"org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");
			ic = new InitialContext();

			ic.createSubcontext("java:");
			ic.createSubcontext("java:/comp");
			ic.createSubcontext("java:/comp/env");
			ic.createSubcontext("java:/comp/env/jdbc");

			// Construct DataSource
			BasicDataSource ds = new BasicDataSource();
			// SQLServerConnectionPoolDataSource ds = new
			// SQLServerConnectionPoolDataSource();
			ds
					.setUrl("jdbc:jtds:sqlserver://devdb05:1433;DatabaseName=IMT;sendStringParametersAsUnicode=false;defaultAutoCommit=false;useLOBs=false;wsid=dreddy;maxStatements=0;socketTimeout=35;appName=tomcat-dreddy");
			ds.setUsername("firstlook");
			ds.setPassword("look@tm31st");
			ds.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");

			ic.rebind("java:/comp/env/jdbc/IMT", ds);

			// Construct DataSource
			ds = new BasicDataSource();
			ds
					.setUrl("jdbc:jtds:sqlserver://intdb01:1433;DatabaseName=FLDW;sendStringParametersAsUnicode=false;defaultAutoCommit=false;useLOBs=false;wsid=dreddy;maxStatements=0;socketTimeout=35;appName=tomcat-dreddy");
			ds.setUsername("firstlook");
			ds.setPassword("7ruHuquh");
			ds.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");

			ic.rebind("java:/comp/env/jdbc/FLDW", ds);

			// super.setUp();
			String[] contexts = new String[] {
					"WEB-INF/spring/applicationContext.xml",
					"WEB-INF/spring/applicationContext-pojo.xml" };
			ClassPathXmlApplicationContext ctxt = new ClassPathXmlApplicationContext(
					contexts);
			appraisalSvc = (AppraisalWebServiceWrapper) ctxt
					.getBean("appraisalWebServiceWrapper");

		} catch (NamingException ex) {
			Logger.getLogger(AppraisalServiceTests.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}

	protected void tearDown() throws Exception {
		ic.destroySubcontext("java:/comp/env/jdbc");
		ic.destroySubcontext("java:/comp/env");
		ic.destroySubcontext("java:/comp");
		ic.destroySubcontext("java:");

		ic = null;
		// super.tearDown();
	}

	public void testAppCtxt() {
		assertNotNull(appraisalSvc);
	}

	public void testPersonServiceIsInjected() {
		assertNotNull("PersonService is not being injected!", appraisalSvc
				.getPersonService());
	}

	public void testDealerPreferenceDAOInjected() {
		assertNotNull("DealerPreferenceDAO is not being injected!",
				appraisalSvc.getDealerPreferenceDAO());
	}

	public void testCreateAppraisalWithNull() {		
		try{
			appraisalSvc.createAppraisal( null );
			fail("createAppraisal(null) should have thrown exception");
		}
		catch(Throwable e){
			assertSame("createAppraisal(null): Wrong kind of exception thrown", IncompleteAppraisalException.class, e.getClass()); 
		}
	}

	
	public void testCreateAppraisalWithNoAppraisalType() {		
		try{
			AppraisalDto dto =  createAppraisalDto();
			dto.setAppraisalType(null);
			appraisalSvc.createAppraisal(dto );
			fail("createAppraisal(no AppraisalType) should have thrown exception");
		}
		catch(Throwable e){
			assertSame("createAppraisal(no AppraisalType): Wrong kind of exception thrown", IncompleteAppraisalException.class, e.getClass()); 
		}
	}
	
	public void testCreateAppraisalWithNoApraisalSource() {		
		try{
			AppraisalDto dto =  createAppraisalDto();
			dto.setAppraisalSource(null);
			appraisalSvc.createAppraisal(dto );
			fail("createAppraisal(no ApraisalSource) should have thrown exception");
		}
		catch(Throwable e){
			assertSame("createAppraisal(no ApraisalSource): Wrong kind of exception thrown", IncompleteAppraisalException.class, e.getClass()); 
		}
	}
	
	
	public void testCreateAppraisal() {		
		try{
			AppraisalDto dto =  createAppraisalDto();
			AppraisalDto dtoRet = appraisalSvc.createAppraisal(dto );
			assertNotNull("createAppraisal() returned null.", dtoRet);
			assertNotNull("createAppraisal() No id in response.", dtoRet.getId());
		}
		catch(Throwable e){
			
			fail("createAppraisal(): Failed unexpectedly: "+e.getMessage()); 
		}
	}
	///////////////////////////////////////////////////////////////////////////
	///// Helper functions
	///////////////////////////////////////////////////////////////////////////

	AppraisalDto createAppraisalDto( ){
		
		AppraisalDto appraisalObj = new AppraisalDto();
		
		appraisalObj.setBusinessUnitId(101590);
		appraisalObj.setAppraisalSource("mobile");
		appraisalObj.setAppraisalType("Trade-In");
		appraisalObj.setAppraisalCreateTime(new Date());
		//appraisalObj.setId(id);
		
		
		/// setup the prices
		PricesDto prices = new PricesDto();
		prices.setAppraisalValue(10000.0);
		prices.setEstimatedReconditioning(2423.0);
		prices.setTargetGrossProfit(15000);
		
		// add prices to appraisal
		appraisalObj.setPrices(prices);
		
		// Setup Vehicle
		VehicleDto vehicleDto = new VehicleDto();
		vehicleDto.setVin("1GAGG25F5Y1449188");
		vehicleDto.setColor("white");
		vehicleDto.setMileage(500);
		
		
		// setup bookOptionDto list
		List<BookOptionDto> bookOptions = new ArrayList<BookOptionDto>();
		BookOptionDto bkOpt = new BookOptionDto();
		bkOpt.setDescription("2door");
		bkOpt.setKey("2324");
		bkOpt.setValue(244);
		bkOpt.setSelected(true);
		bkOpt.setOptionType("Equipment");
		bookOptions.add(bkOpt);
		
		BookoutDto bookoutDto = new BookoutDto();
		bookoutDto.setBookId("BLACKBOOK");
		bookoutDto.setStyleKey("234324");
		bookoutDto.setBookOptions(bookOptions);			
		
		List<BookoutDto> bkOuts = new ArrayList<BookoutDto> ();
		bkOuts.add(bookoutDto);
		vehicleDto.setBookouts(bkOuts);
		
		// add vehicle to appraisal
		appraisalObj.setVehicle(vehicleDto);
		
		return appraisalObj;
	}
	
	Map<String, Object> createAppraisalObject( ){
		
		Map<String, Object> appraisalObj = new HashMap<String, Object>();
		
		
		appraisalObj.put("businessUnitId", "101590");
		appraisalObj.put("appraisalCreateTime", "101590");
		appraisalObj.put("businessUnitId", "101590");
		appraisalObj.put("businessUnitId", "101590");
		
		
		return appraisalObj;
	}
	
	
	
}
