package biz.firstlook.rest.auth;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Component;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

@Component
public class BasicAuthFilter implements ContainerRequestFilter {

	/**
	 * Apply the filter : check input request, validate or not with user auth
	 * 
	 * @param containerRequest
	 *            The request from Tomcat server
	 */
	public ContainerRequest filter(ContainerRequest containerRequest)
			throws WebApplicationException {
		// GET, POST, PUT, DELETE, ...
		// Get the authentification passed in HTTP headers parameters
		String auth = containerRequest.getHeaderValue("authorization");

		// If the user does not have the right (does not provide any HTTP Basic
		// Auth)
		if (auth == null) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

		// lap : loginAndPassword
		String[] lap = BasicAuth.decode(auth);

		// If login or password fail
		if (lap == null || lap.length != 2) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

		try
		{
			CASClient client = new CASClient();
			boolean bAuthResult = client.authenticate(lap[0], lap[1]);
			// Our system refuse login and password
			if (!bAuthResult) {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}
		}
		catch (WebApplicationException e)
		{
			throw e;
		}
		catch (Exception ex)
		{
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

		return containerRequest;
	}
}