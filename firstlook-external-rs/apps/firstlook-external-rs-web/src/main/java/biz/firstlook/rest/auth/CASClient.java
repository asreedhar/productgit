package biz.firstlook.rest.auth;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.params.HttpParams;
import org.apache.log4j.Logger;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

import com.discursive.cas.extend.client.CASFilterConfig;
import com.sun.jersey.api.JResponseAsResponse;
import com.sun.jersey.api.JResponse.JResponseBuilder;

/**
 * The CASClient allows users to programmatically login to CAS protected
 * services based on the CAS 2 protocol. This client behaves like a
 * browser-client in terms of cookie handling.<br>
 * 
 * @author Lee
 */
public class CASClient {

	public static Logger LOG = Logger.getLogger(CASClient.class);

	public static final String TICKET_BEGIN = "ticket=";
	private static final String LT_BEGIN = "name=\"lt\" value=\"";
	public static final String CAS_USER_BEGIN = "<cas:user>";
	public static final String CAS_USER_END = "</cas:user>";

	private HttpClient fClient;
	private String casLoginUrl;

	/**
	 * Construct a new CASClient.
	 * 
	 * @param casUrl
	 *            The base URL of the CAS service to be used.
	 */
	public CASClient() throws WebApplicationException
	{
		this(new HttpClient());
		LOG.debug("CASClient()");
	}
	
	public CASClient(String casLoginUrl ) throws WebApplicationException
	{
		LOG.debug("CASClient(casLoginURL="+casLoginUrl+")");
		fClient = new HttpClient();
		this.casLoginUrl = normalizeUrl(casLoginUrl);
		init();
	}

	/**
	 * Construct a new CasClient which uses the specified HttpClient for its
	 * HTTP calls.
	 * 
	 * @param client
	 */
	public CASClient(HttpClient client)  throws WebApplicationException
	{
		LOG.debug("CASClient(client="+client.getHost()+")");
		fClient = client;
		try
		{
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			Object tempCasConfig = envCtx.lookup("security/CASConfig");
			CASFilterConfig casConfig = new CASFilterConfig();
			PropertyUtils.copyProperties(casConfig, tempCasConfig);
			casLoginUrl = stripLastElement(casConfig.getLoginUrl()  );
			casLoginUrl = normalizeUrl( casLoginUrl);
		}
		catch (Throwable e)
		{
			LOG.error("CASClient failed:(casLoginURL="+casLoginUrl+")",e);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	public String stripLastElement(String url){
		if(url.endsWith("/"))
			return stripLastElement(
		url.substring(0, url.length()-1));
		String ret = (url.substring(0,url.lastIndexOf('/') ));
		return ret;
	}

	private void  init(){
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[]{new DummyTrustManager(){
		    public X509Certificate[] getAcceptedIssuers(){return null;}
		    public void checkClientTrusted(X509Certificate[] certs, String authType){}
		    public void checkServerTrusted(X509Certificate[] certs, String authType){}
		}};

		// Install the all-trusting trust manager
		try {
		    SSLContext sc = SSLContext.getInstance("TLS");
		    sc.init(null, trustAllCerts, new SecureRandom());
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
//			JResponseBuilder<String> b = new JResponseBuilder<String>();
//			Response r = new JResponse( );
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	private String normalizeUrl(String loginUrl) {
		String slash =  ( !loginUrl.endsWith("/") )
			? ("/") : "";
		return loginUrl + slash;
	}

	/**
	 * Authenticate the specified user with the specified password against the
	 * CAS service.
	 * 
	 * @param username
	 * @param password
	 * @return true if authenticated
	 */
	public boolean authenticate(String username,
			String password) {
		String lt = getLt(null);
		if (lt == null) {
			LOG.error("Cannot retrieve LT from CAS. Aborting authentication for '"
							+ username + "'");
			return false;
		}
		
		boolean result = false;
		PostMethod method = new PostMethod(casLoginUrl+"login");
		method.setParameter("_eventId", "submit");
		method.setParameter("username", username);
		method.setParameter("password", password);
		method.setParameter("lt", lt);
		method.setParameter("gateway", "true");
		//method.setFollowRedirects(true);
		try {
			int retCode = fClient.executeMethod(method);
			
			// if CAS does not return a login page with an LT authentication was
			// successful
			String respBody = new String(method.getResponseBody());//method.getResponseHeader("Location")
			if ( extractLt(respBody) != null) {
				LOG.error("Authentication for '" + username + "' unsuccessful");
				if (LOG.isDebugEnabled())
					LOG.debug("Authentication for '" + username
							+ "' unsuccessful.");
			} else {
				if (LOG.isDebugEnabled())
					LOG.debug("Authentication for '" + username
							+ "' successful.");
				result = true;
			}

		} catch (Exception x) {
			LOG.error("Could not authenticate'" + username + "':"
					+ x.toString());
		}
		finally
		{
			method.releaseConnection();
		}
		return result;
	}

	/**
	 * Helper method to extract the user name from a "service validate" call to
	 * CAS.
	 * 
	 * @param data
	 *            Response data.
	 * @return The clear text username, if it could be extracted, null
	 *         otherwise.
	 */
	protected String extractUser(String data) {
		String user = null;
		int start = data.indexOf(CAS_USER_BEGIN);
		if (start >= 0) {
			start += CAS_USER_BEGIN.length();
			int end = data.indexOf(CAS_USER_END);
			if (end > start)
				user = data.substring(start, end);
			else
				LOG.warn("Could not extract username from CAS validation response. Raw data is: '"
								+ data + "'");
		} else {
			LOG.warn("Could not extract username from CAS validation response. Raw data is: '"
							+ data + "'");
		}
		return user;
	}

	/**
	 * Helper method to extract the service ticket from a login call to CAS.
	 * 
	 * @param data
	 *            Response data.
	 * @return The service ticket, if it could be extracted, null otherwise.
	 */
	protected String extractServiceTicket(String data) {
		String serviceTicket = null;
		int start = data.indexOf(TICKET_BEGIN);
		if (start > 0) {
			start += TICKET_BEGIN.length();
			serviceTicket = data.substring(start);
		}
		return serviceTicket;
	}

	/**
	 * Helper method to extract the LT from a login form from CAS.
	 * 
	 * @param data
	 *            Response data.
	 * @return The LT, if it could be extracted, null otherwise.
	 */
	protected String extractLt(String data) {
		String token = null;
		
		int start = data.indexOf(LT_BEGIN);
		if (start < 0) {
			LOG.info("Could not obtain LT token from CAS (which is OK in some instances).");
		} else {
			start += LT_BEGIN.length();
			int end = data.indexOf("\"", start);
			token = data.substring(start, end);
		}
		return token;
	}

	/**
	 * This method requests the original login form from CAS. This form contains
	 * an LT, an initial token that must be presented to CAS upon sending it an
	 * authentication request with credentials.<br>
	 * If a service URL is provided (which is optional), this method will post
	 * the URL such that CAS authenticates against the specified service when a
	 * subsequent authentication request is sent.
	 * 
	 * @param serviceUrl
	 * @return The LT token if it could be extracted from the CAS response.
	 */
	protected String getLt(String serviceUrl) {
		String lt = null;
		HttpMethod method = null;
		if (serviceUrl == null)
		{
			method = new GetMethod(casLoginUrl);
		}
		try {
			int statusCode = fClient.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK) {
				LOG.error("Could not obtain LT token from CAS: "
						+ method.getStatusLine());
				return null;
			} else {
				String responseBody = new String(method.getResponseBody());
				return extractLt(responseBody);
			}
		} catch (Exception x) {
			LOG.error("Could not obtain LT token from CAS: " + x.toString());
		}
		finally
		{		
			method.releaseConnection();
		}
		return lt;
	}

	
	class DummyTrustManager implements X509TrustManager {

		public void checkClientTrusted(X509Certificate[] chain,
				String authType) throws CertificateException {
			//do nothing, blind trust
		}

		public void checkServerTrusted(X509Certificate[] chain,
				String authType) throws CertificateException {
			//do nothing, blind trust		
		}

		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}
	}
}
