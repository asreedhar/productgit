package biz.firstlook.module.rest.service;

public class MessageException extends Exception {

	private static final long serialVersionUID = -7581156777810073354L;
	
	private String errorMessage;
	private String object;
	
	public MessageException(String object, String errorMessage) {
		super(errorMessage);
		this.object = object;
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
}
