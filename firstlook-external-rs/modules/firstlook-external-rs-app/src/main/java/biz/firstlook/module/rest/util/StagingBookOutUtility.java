package biz.firstlook.module.rest.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import biz.firstlook.module.rest.model.BookOptionDto;
import biz.firstlook.module.rest.model.BookoutDto;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOutOption;

/**
 * @author dreddy
 *
 * Description: this class is responsible for pulling apart the incoming appraisal and constructing stagingbookouts for 
 *              persisting.
 *              
 *              based on firstlook-external-ws implementation biz.firstlook.service.appraisal.support.StagingBookOutUtility
 * 
 */
public class StagingBookOutUtility
{
	public static ArrayList< StagingBookOut > createStagingBookOuts( biz.firstlook.module.rest.model.AppraisalDto restAppraisal ) throws WebServiceException
	{
		ArrayList< StagingBookOut > bookOuts = new ArrayList<StagingBookOut>();
		for ( BookoutDto bookout : restAppraisal.getVehicle().getBookouts() )
		{
			StagingBookOut stagingBookOut = new StagingBookOut();
			ThirdPartyDataProvider dataProvider = null;
			
			if (bookout.getBookId().equalsIgnoreCase("BLACKBOOK")){
				dataProvider = ThirdPartyDataProvider.BLACKBOOK;
			} 
			
			else if (bookout.getBookId().equalsIgnoreCase("NADA")){
				dataProvider = ThirdPartyDataProvider.NADA;
			}
			
			else if (bookout.getBookId().equalsIgnoreCase("KELLEY")){
				dataProvider = ThirdPartyDataProvider.KELLEY;
			}
			
			else if (bookout.getBookId().equalsIgnoreCase("GALVES")){
				dataProvider = ThirdPartyDataProvider.GALVES;
			}
			
			else {
				throw new WebServiceException( ExitCode.MISSING_REQUIRED_ATTRIBUTES, "An unknown bookId was specified for this appraisal." );
			}
	
			stagingBookOut.setThirdPartyDataProvider( dataProvider );
	
			populateStagingBookOutOptions( dataProvider, bookout, stagingBookOut );
			
			stagingBookOut.setThirdPartyVehicleCode( bookout.getStyleKey() );
			
			bookOuts.add( stagingBookOut );
		}
		return bookOuts;
	}

	static void populateStagingBookOutOptions( ThirdPartyDataProvider dataProvider, BookoutDto bookout, StagingBookOut stagingBookOut ) throws WebServiceException
	{
		Set< StagingBookOutOption > options = new HashSet< StagingBookOutOption >();
		for ( BookOptionDto option : bookout.getBookOptions() )
		{
			StagingBookOutOption stagingBookOutOption = new StagingBookOutOption();
			stagingBookOutOption.setIsSelected( option.getSelected() );
			stagingBookOutOption.setOptionKey( option.getKey() );
			stagingBookOutOption.setOptionName( option.getDescription() );
			stagingBookOutOption.setValue( option.getValue() );
			stagingBookOutOption.setThirdPartyOptionType( getOptionType( dataProvider, option ) );
			stagingBookOutOption.setStagingBookOut( stagingBookOut );
			options.add(stagingBookOutOption);
		}
		stagingBookOut.setOptions( options );
	}


	static ThirdPartyOptionType getOptionType( ThirdPartyDataProvider dataProvider, BookOptionDto option )
	{
		ThirdPartyOptionType optionType = null;
		// Option type is not required
		if (option == null || option.getOptionType() == null) return ThirdPartyOptionType.EQUIPMENT;
		
		if (option.getOptionType().equalsIgnoreCase("DRIVETRAIN")){
			optionType = ThirdPartyOptionType.DRIVETRAIN;
		}
		
		else if (option.getOptionType().equalsIgnoreCase("ENGINE")){
			optionType = ThirdPartyOptionType.ENGINE;
		}
		
		else if (option.getOptionType().equalsIgnoreCase("EQUIPMENT")){
			optionType = ThirdPartyOptionType.EQUIPMENT;
		}
		
		else if (option.getOptionType().equalsIgnoreCase("TRANSMISSION")){
			optionType = ThirdPartyOptionType.TRANSMISSION;
		}
		
		else if (option.getOptionType().equalsIgnoreCase("EQUIPMENT")){
			optionType = ThirdPartyOptionType.EQUIPMENT;
		}
		
		else{
			optionType = ThirdPartyOptionType.EQUIPMENT;
		}
	
		return optionType;
	}
}
