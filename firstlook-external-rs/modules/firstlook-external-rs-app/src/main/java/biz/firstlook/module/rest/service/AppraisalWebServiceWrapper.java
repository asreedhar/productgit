package biz.firstlook.module.rest.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.module.rest.model.AppraisalDto;
import biz.firstlook.module.rest.model.Mapper;
import biz.firstlook.module.rest.model.PhotoKeysDto;
import biz.firstlook.module.rest.util.StagingBookOutUtility;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.appraisal.v1.AbstractAppraisalWebService;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.appraisal.AppraisalPhotoKeys;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IDeal;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

/*
 * Author:  D. Reddy
 * Purpose: This is mainly just a wrapper class around some existing functionality already provided by the AbstractAppraisalWebService
 *          class of firstlook-external-ws.  I was a bit hesitant to extend from this class as I wanted to keep the restful project
 *          a separate entity, but copying code also seemed like a bad idea.  There should probably be some refactoring done to pull
 *          out that common functionality somewhere else.
 */

//"wrapper" doesn't really describe this.

public class AppraisalWebServiceWrapper extends AbstractAppraisalWebService{
	
	//Wrapper around protected checkRejectConditions method of the AppraisalWebServiceImpl
	public void checkRejectConditions(biz.firstlook.module.rest.model.AppraisalDto restAppraisal) throws MessageException{
		validate(restAppraisal);
		Integer businessUnitId = restAppraisal.getBusinessUnitId();
		String vin = restAppraisal.getVehicle().getVin();
		AppraisalSource appraisalSource = AppraisalSource.createAppraisalSource(restAppraisal.getAppraisalSource());
		try{
			super.checkRejectConditions(businessUnitId, vin, appraisalSource);
		} catch(WebServiceException e){
			throw new InvalidAppraisalException(e.getExitCode().value(), e.getErrorMessage());
		}
	}

	public void validate(biz.firstlook.module.rest.model.AppraisalDto restAppraisal) throws IncompleteAppraisalException{

		if (restAppraisal == null){
			throw new IncompleteAppraisalException("appraisal", "appraisal was not specified.");
		}
		
		if (restAppraisal.getBusinessUnitId() == 0){
			throw new IncompleteAppraisalException("appraisal", "businessUnitId was not specified.");
		}

		if (restAppraisal.getAppraisalType() == null){
			throw new IncompleteAppraisalException("appraisal", "appraisalType was not specified.");
		}
		
		if (restAppraisal.getAppraisalSource() == null){
			throw new IncompleteAppraisalException("appraisal", "appraisalSource was not specified.");
		}

		if (restAppraisal.getVehicle() == null){
			throw new IncompleteAppraisalException("appraisal", "vehicle was not specified.");
		}
		
		biz.firstlook.module.rest.model.VehicleDto vehicle = restAppraisal.getVehicle();
		
		if (vehicle.getVin() == null){
			throw new IncompleteAppraisalException("vehicle", "vin was not specified.");
		}
			
		//we can add some validation against bookouts and bookoptions as we see fit
		
	}	
	
	public DealerPreferenceDAO getDealerPreferenceDAO(){
		return super.dealerPreferenceDAO;
	}
	
	//needed to add this complete horseshit to accomodate other complete horseshit
	//The createAppraisal method requires and IDeal object so it can update deal info in the DB
	private IDeal createDeal( final IAppraisal appraisal){
		final IPerson person = appraisal.getDealTrackSalesPerson();
		
		if( person != null && appraisal != null){
			
			return new IDeal(){
		
				private IPerson salesPerson = person;
				private Integer salesPersonId = salesPerson.getPersonId();
				private String firstName = salesPerson.getFirstName();
				private String lastName = salesPerson.getLastName();
				private InventoryTypeEnum dealType = appraisal.getDealTrackNewOrUsed();
				private String stockNumber = appraisal.getDealTrackStockNumber();
					
				public IPerson getSalesPerson(){ return salesPerson; }
				public Integer getSalesPersonId(){ return salesPersonId; }
				public String getFirstName(){ return firstName; }
				public String getLastName(){ return lastName; }
				public InventoryTypeEnum getDealType(){ return dealType; }
				public String getStockNumber(){ return stockNumber; }
			};
			
		}
		else { return null; }
	}
	
	//so we can be certain the list is out of memory
	private IAppraisal lookForOldAppraisal(Integer businessUnitId, String vin){
		List<IAppraisal> existingAppraisals = getAppraisalService().findBy(businessUnitId, vin);
		return existingAppraisals.size() > 0 ? existingAppraisals.get(0) : null;
	}
	
	@SuppressWarnings("null")
	public AppraisalDto createAppraisal(biz.firstlook.module.rest.model.AppraisalDto restAppraisal) throws MessageException
	{		
		checkRejectConditions(restAppraisal);
		Integer businessUnitId = restAppraisal.getBusinessUnitId();
		String vin = restAppraisal.getVehicle().getVin();
		//DealerPreference dealerPreference = getDealerPreferenceDAO().findByBusinessUnitId(businessUnitId);
		String color = restAppraisal.getVehicle().getColor();
		Customer customer = null;
		
		
		//for pulling from CRM if exists
		IDeal deal = null;
		String notes = null;
		String conditionDescription = null;
		IAppraisal oldAppraisal = lookForOldAppraisal(businessUnitId, vin);
		
		//Why this is retarded: Appraisal.customer but also Customer.appraisal + hibernate == "Good luck with deleting that."
		//So you can't just pull properties out and merge and create a new appraisal and delete the old one.
		//You have to use this cock-a-mamy factory to build components without leaving references to customer behind.
		//I believe it's basically two cascades getting into a fight and you the lucky inheritor of this code losing.
		
		if( oldAppraisal != null && oldAppraisal.getAppraisalSource().isCrmNotWavis() ){
			//Adding below condition as part of case 32540
			if((oldAppraisal.getCustomer().getFirstName()!=null)&&(oldAppraisal.getCustomer().getLastName()!=null)&&(oldAppraisal.getCustomer().getGender()!=null)&&(oldAppraisal.getCustomer().getEmail()!=null)&&(oldAppraisal.getCustomer().getPhoneNumber()!=null)){	
			//Yes. Horrible.True
			customer = getAppraisalFactory().createCustomer(
					oldAppraisal.getCustomer().getFirstName(),
					oldAppraisal.getCustomer().getLastName(),
					oldAppraisal.getCustomer().getGender(),
					oldAppraisal.getCustomer().getEmail(),
					oldAppraisal.getCustomer().getPhoneNumber()
			);
			}
			//this too but I buried a little
			deal = createDeal( oldAppraisal );
			
			//not sure notes exists at this stage
		    
		    conditionDescription = getAppraisalFactory().createAppraisalAction(
	    	    oldAppraisal.getAppraisalActionType(),
	    	    oldAppraisal.getConditionDescription(),
	    	    oldAppraisal.getEstimatedReconditioningCost(),
	    	    oldAppraisal.getWholesalePrice()
		    ).getConditionDescription();
		    
		    //At some point they intended to have a separate notes field. Now for some reason the two are merged.
		    notes = conditionDescription;
		    
		}
		
		notes=restAppraisal.getReconNotes();
		conditionDescription=restAppraisal.getReconNotes();
				
		
		Vehicle vehicle;
		List< StagingBookOut > bookOuts;
		
		try {
			vehicle = getVehicleService().identifyVehicle( vin );
			bookOuts = StagingBookOutUtility.createStagingBookOuts( restAppraisal );

		} catch (VehicleCatalogServiceException e) {
			throw new InvalidAppraisalException("vehicle", "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog." );
			
		} catch (InvalidVinException e) {
			throw new InvalidAppraisalException("vehicle", "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog." );

		} catch (WebServiceException e){
			throw new InvalidAppraisalException(e.getExitCode().value(), e.getErrorMessage());
		}
		
		vehicle.setBaseColor( color );
		Dealer dealer = getDealerService().findByDealerId( businessUnitId );
		Double estimateRecon = null;
		Integer targetGrossProfit = null;
		AppraisalValue value = null;
		
		if( restAppraisal != null )
		{
			if( restAppraisal.getPrices().getEstimatedReconditioning() != null )
				estimateRecon = restAppraisal.getPrices().getEstimatedReconditioning();
			
			if (restAppraisal.getPrices().getTargetGrossProfit() != null)
			{
				targetGrossProfit = restAppraisal.getPrices().getTargetGrossProfit();
			}
		
			//TODO: add appraiser name to json message
			value = getAppraisalFactory().createAppraisalValue( restAppraisal.getPrices().getAppraisalValue().intValue(), "firstName" + " " + "lastName" );
		}
		try{
		IAppraisal appraisal = getAppraisalService().createAppraisal(
			restAppraisal.getAppraisalCreateTime(),
			dealer,
			vehicle,	
			restAppraisal.getVehicle().getMileage(),
			estimateRecon,
			targetGrossProfit,
			conditionDescription,
			null, //wholesalePrice
			value,
			deal, //deal
			customer, //customer
			notes, //notes
			AppraisalSource.createAppraisalSource(restAppraisal.getAppraisalSource()),
			new HashSet<StagingBookOut>(bookOuts),
			AppraisalTypeEnum.getType(restAppraisal.getAppraisalType()),
			restAppraisal.getMmrVehicle());
		
		if(oldAppraisal != null){
			getAppraisalService().deleteAppraisal(oldAppraisal);
		}
		if (restAppraisal.getSalespersonId() != null)
		{
			try{
				
			 appraisal.setPotentialDeal(getPersonService().getPerson(restAppraisal.getSalespersonId()), null, InventoryTypeEnum.UNKNOWN);
			 getAppraisalService().save(appraisal);
			 System.out.println("Sales Person Id inserted into the Database from FL-External-RS");
			}
			catch(Exception e){
				//Log4JLogger  log = new Log4JLogger(AppraisalWebServiceWrapper.class.getName());
				System.out.println("Error Occured while inserting SalesPerson Id");
				//log.debug("Error Occured wile inserting SalesPersonId", e);
				e.printStackTrace();
				
				
			}
		}
		if (restAppraisal.getUniqueId() != null)
		{
			try{
				
			 appraisal.setUniqueId(restAppraisal.getUniqueId());
			 getAppraisalService().save(appraisal);
			 System.out.println("Unique Id inserted into the Database from FL-External-RS");
			}
			catch(Exception e){
				//Log4JLogger  log = new Log4JLogger(AppraisalWebServiceWrapper.class.getName());
				System.out.println("Error Occured while inserting Unique Id");
				//log.debug("Error Occured wile inserting UniqueId", e);
				e.printStackTrace();
				
				
			}
		}
		//Adding the photos from the q processor -> JSON -> DTO -> table
		if(restAppraisal.getPhotoUrl()!=null)
		{
			int i =0;
			List< AppraisalPhotoKeys > appraisalPhotoKeys = new ArrayList< AppraisalPhotoKeys >();
			Iterator<PhotoKeysDto> photoKeys = restAppraisal.getPhotoUrl().iterator() ;
			while(photoKeys.hasNext())
			{
				PhotoKeysDto pkd = photoKeys.next();
				if(pkd.getSequenceNo()==null)
				{
					pkd.setSequenceNo(i++);
				}
				appraisalPhotoKeys.add(new AppraisalPhotoKeys(appraisal,pkd.getWide127Url(),pkd.getPhotoUrl(),pkd.getSequenceNo()));
			}
			appraisal.setAppraisalPhotoKeys(appraisalPhotoKeys);
			getAppraisalService().save(appraisal);
		}
		//Perhaps return saved appraisal? ... after mapping to biz.firstlook.module.rest.model.Appraisal
		// ... simple call to mapper
		return  Mapper.toAppraisalDto(appraisal);
	}
		catch(Throwable e){
			System.err.println("AppraisalWebServiceWrapper():"+ e.getLocalizedMessage());
			e.printStackTrace();
			throw new MessageException(restAppraisal.toString(), e.getLocalizedMessage());
		}
	}
	
	public biz.firstlook.module.rest.model.AppraisalDto fetchAppraisal(Integer businessUnitId, String vin) throws MessageException{
		//TODO: check to see if we need to check for active vs. inactive - Erik 6-12-2013
		if (businessUnitId == null || businessUnitId == 0){
			throw new MessageException("businessUnitId", "businessUnitId was not specified.");
		}
		
		if (vin == null){
			throw new MessageException("vin", "vin was not specified.");
		}
		
		List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin);

		if (appraisals != null && !appraisals.isEmpty()){
		
			//We are assuming one appraisal per buid/vin combination
			return Mapper.toAppraisalDto(appraisals.get(0));
		}

		return null;
	}
}