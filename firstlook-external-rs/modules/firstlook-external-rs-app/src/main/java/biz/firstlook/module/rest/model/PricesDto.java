package biz.firstlook.module.rest.model;

/**
 * @author dreddy
 * 
 * Description - very basic price bean
 *             - used to serialize to/from client
 *     
 */
public final class PricesDto {
	
	Double appraisalValue;
	
	Integer targetGrossProfit;
	
	Double estimatedReconditioning;
	
	public Double getEstimatedReconditioning() {
		return estimatedReconditioning;
	}

	public void setEstimatedReconditioning(Double estimatedReconditioning) {
		this.estimatedReconditioning = estimatedReconditioning;
	}

	public Double getAppraisalValue() {
		return appraisalValue;
	}

	public void setAppraisalValue(Double appraisalValue) {
		this.appraisalValue = appraisalValue;
	}

	public Integer getTargetGrossProfit() {
		return targetGrossProfit;
	}

	public void setTargetGrossProfit(Integer targetGrossProfit) {
		this.targetGrossProfit = targetGrossProfit;
	}

	
}