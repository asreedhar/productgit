package biz.firstlook.module.rest.model;

import java.util.Date;
import java.util.List;

import biz.firstlook.transact.persist.model.MMRVehicle;

/**
 * @author dreddy
 * 
 * Description - very basic appraisal bean
 *             - used to serialize to/from client
 *     
 */
public final class AppraisalDto {

	private Integer id;

	private Date appraisalCreateTime;
	
	private String appraisalType;
	
	private String appraisalSource;
	
	private VehicleDto vehicle;
	
	private PricesDto prices;
	
	private Integer businessUnitId;
	
	private MMRVehicle mmrVehicle;
	
	private Integer salespersonId;
	
	private List<PhotoKeysDto> photoUrl;
	
	private String uniqueId;
	
	private String reconNotes;
	
	
	
	public String getReconNotes() {
		return reconNotes;
	}

	public void setReconNotes(String reconNotes) {
		this.reconNotes = reconNotes;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public List<PhotoKeysDto> getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(List<PhotoKeysDto> photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Integer getSalespersonId() {
		return salespersonId;
	}

	public void setSalespersonId(Integer salespersonId) {
		this.salespersonId = salespersonId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getAppraisalCreateTime() {
		return appraisalCreateTime;
	}

	public void setAppraisalCreateTime(Date appraisalCreateTime) {
		this.appraisalCreateTime = appraisalCreateTime;
	}

	public String getAppraisalType() {
		return appraisalType;
	}

	public void setAppraisalType(String appraisalType) {
		this.appraisalType = appraisalType;
	}

	public String getAppraisalSource() {
		return appraisalSource;
	}

	public void setAppraisalSource(String appraisalSource) {
		this.appraisalSource = appraisalSource;
	}

	public VehicleDto getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleDto vehicle) {
		this.vehicle = vehicle;
	}

	public PricesDto getPrices() {
		return prices;
	}

	public void setPrices(PricesDto prices) {
		this.prices = prices;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public MMRVehicle getMmrVehicle() {
		return mmrVehicle;
	}

	public void setMmrVehicle(MMRVehicle mmrVehicle) {
		this.mmrVehicle = mmrVehicle;
	}
}