package biz.firstlook.module.rest.model;

/**
 * @author dreddy
 * 
 * Description - very basic bookoption bean
 *             - used to serialize to/from client
 *     
 */
public final class BookOptionDto {
	
	String key;
	
	String description;
	
	int value;
	
	Boolean selected;
	
	String optionType;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	
}