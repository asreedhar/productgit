package biz.firstlook.module.rest.model;

public class PhotoKeysDto {
	
	private String photoUrl;
	private String wide127Url;
	private Integer sequenceNo;
	
	public Integer getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getWide127Url() {
		return wide127Url;
	}
	public void setWide127Url(String wide127Url) {
		this.wide127Url = wide127Url;
	}
	
	

}
