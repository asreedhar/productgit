package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class TestKbbVehicleOptionAdapter extends TestCase {
	
	public void testAdapt()
	{
		ThirdPartyVehicleOption tpvo = createThirdPartyOption("AC", "Air Condidtioning");
		
		VehicleOption vehicleOption = new Kbb_VehicleOptionAdapter().adaptItem(tpvo);
		assertEquals("Option Name does not match", tpvo.getOptionName(), vehicleOption.getDisplayName());
		KbbVehicleOption kbbVehicleOption = (KbbVehicleOption) vehicleOption;
		assertEquals("Option Key does not match", tpvo.getOptionKey(), kbbVehicleOption.getOptionKey());
	}
	
	public void testUnadapt()
	{
		KbbVehicleOption kbbOption = createKbbVehicleOption("AC", "Air Condidtioning");
		
		ThirdPartyVehicleOption tpvo = new Kbb_VehicleOptionAdapter().unadaptItem( kbbOption );
		assertEquals("Option Name does not match", kbbOption.getDisplayName(), tpvo.getOptionName());
		assertEquals("Option Key does not match", kbbOption.getOptionKey(), tpvo.getOptionKey());
	}
	
	public void testGenerateSelectedOptionsSet()
	{
		List<ThirdPartyVehicleOption> options = new ArrayList<ThirdPartyVehicleOption>();
		options.add( createThirdPartyOption("A1", "name1" ) );
		options.add( createThirdPartyOption("A2", "name2" ) );
		options.add( createThirdPartyOption("A3", "name3" ) );
		options.add( createThirdPartyOption("A4", "name4" ) );
		options.add( createThirdPartyOption("A5", "name5" ) );
		options.get(0).setStatus( false );
		options.get(1).setStatus( true );
		options.get(2).setStatus( true );
		options.get(3).setStatus( false );
		options.get(4).setStatus( true );
		
		KbbVehicleDescriptionImpl desc = new KbbVehicleDescriptionImpl();
		desc.setVehicleOptions(Adapters.adapt(options, new Kbb_VehicleOptionAdapter() ) );
		List<? extends VehicleOption> selectedOptionsSet = (List<? extends VehicleOption>)desc.getSelectedVehicleOptions();
		
		assertEquals( "didn't include name2", "name2", ((KbbVehicleOption)selectedOptionsSet.get(0)).getDisplayName() );
		assertEquals( "didn't include name3", "name3", ((KbbVehicleOption)selectedOptionsSet.get(1)).getDisplayName() );
		assertEquals( "didn't include name5", "name5", ((KbbVehicleOption)selectedOptionsSet.get(2)).getDisplayName() );
		
	}
	
	public void testGenerateSelectedOptionsSetNull()
	{
		KbbVehicleDescriptionImpl desc = new KbbVehicleDescriptionImpl();
		Collection<? extends VehicleOption> selectedOptionsSet = desc.getSelectedVehicleOptions();
		assertTrue( selectedOptionsSet.isEmpty() );
	}

	public void testUpdatedSelectedOptions() {
		List<ThirdPartyVehicleOption> options = new ArrayList<ThirdPartyVehicleOption>();
		options.add( createThirdPartyOption("A1", "name1" ) );
		options.add( createThirdPartyOption("A2", "name2" ) );
		options.add( createThirdPartyOption("A3", "name3" ) );
		options.add( createThirdPartyOption("A4", "name4" ) );
		options.add( createThirdPartyOption("A5", "name5" ) );
		options.get(0).setStatus( true );
		options.get(1).setStatus( false );
		options.get(2).setStatus( true );
		options.get(3).setStatus( false );
		options.get(4).setStatus( true );
		KbbVehicleDescriptionImpl vehicle = new KbbVehicleDescriptionImpl();
		vehicle.setVehicleOptions( Adapters.adapt(options, new Kbb_VehicleOptionAdapter()) );
		
		List<ThirdPartyVehicleOption> options2 = new ArrayList<ThirdPartyVehicleOption>();
		options2.add( createThirdPartyOption("A4", "name4" ) );
		options2.add( createThirdPartyOption("A5", "name5" ) );
		
		vehicle.setSelectedVehicleOptions( Adapters.adapt(options2, new Kbb_VehicleOptionAdapter() ) );
		
		assertEquals("wrong size for selected Optinos!", 2, vehicle.getSelectedVehicleOptions().size() );
		assertFalse(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A1", "name1") ) );
		assertFalse(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A2", "name2") ) );
		assertFalse(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A3", "name3") ) );
		assertTrue(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A4", "name4") ) );
		assertTrue(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A5", "name5") ) );
		
		List<VehicleOption> selectedOptions = new ArrayList<VehicleOption>();
		selectedOptions.add( createKbbVehicleOption("A2", "name2"));
		selectedOptions.add( createKbbVehicleOption("A3", "name3"));
		selectedOptions.add( createKbbVehicleOption("A5", "name5"));
		
		vehicle.setSelectedVehicleOptions(selectedOptions);
		assertEquals("wrong size for selected Optinos!", 3, vehicle.getSelectedVehicleOptions().size() );
		assertFalse(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A1", "name1") ) );
		assertTrue(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A2", "name2") ) );
		assertTrue(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A3", "name3") ) );
		assertFalse(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A4", "name4") ) );
		assertTrue(" options wasn't present", vehicle.getSelectedVehicleOptions().contains( createKbbVehicleOption("A5", "name5") ) );
	}
	
	private KbbVehicleOption createKbbVehicleOption( String key, String name ) {
		KbbVehicleOption kbbOption = new KbbVehicleOption();
		kbbOption.setDisplayName( name );
		kbbOption.setOptionKey( key );
		kbbOption.select();
		kbbOption.setThirdPartyOptionTypeId( 1 );
		return kbbOption;
	}
	
	public ThirdPartyVehicleOption createThirdPartyOption( String key, String name) {
		ThirdPartyOption tpo = new ThirdPartyOption(name, key, ThirdPartyOptionType.EQUIPMENT);
		ThirdPartyVehicleOption tpvo = new ThirdPartyVehicleOption(tpo, 1);
		return tpvo;
	}
}
