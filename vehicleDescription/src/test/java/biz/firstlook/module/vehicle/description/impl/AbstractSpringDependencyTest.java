package biz.firstlook.module.vehicle.description.impl;

import org.springframework.test.AbstractTransactionalSpringContextTests;

public abstract class AbstractSpringDependencyTest extends AbstractTransactionalSpringContextTests
{

static final String[] configLocations = new String[] { "Test-applicationContext-bookoutService.xml",
	"applicationContext-vehicleDescription.xml", "applicationContext-transact-persist.xml" };

public AbstractSpringDependencyTest()
{
	super();
	setAutowireMode( AUTOWIRE_BY_NAME );
}

@Override
protected final String[] getConfigLocations()
{
	return configLocations;
}

}
