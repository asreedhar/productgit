package biz.firstlook.module.vehicle.description.impl;

import java.util.Collection;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;


public class TestSpringBasedVehicleDescriptionContext extends AbstractSpringDependencyTest
{

private VehicleDescriptionContext context = SpringBasedVehicleDescriptionContext.getInstance();

public void testGetDescriptionProvider()
{
	VehicleDescriptionProviderEnum providerEnum = VehicleDescriptionProviderEnum.KBB;
	VehicleDescriptionProvider provider = context.getDescriptionProvider( providerEnum );
	assertEquals( providerEnum, provider.getDescriptionProviderEnum() );
	
	VehicleDescriptionProvider provider2 = context.getDescriptionProvider( VehicleDescriptionProviderEnum.Galves );
	assertNull( provider2 );
}

public void testGetRegisteredDescriptionProviders()
{
	Collection<VehicleDescriptionProvider> providers = context.getRegisteredDescriptionProviders();
	assertTrue( !providers.isEmpty() );
}

}
