package biz.firstlook.module.vehicle.description;

import java.util.HashSet;
import java.util.Set;

import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Dealership;


public class MockDealer extends Dealer implements Dealership {

	private static final long serialVersionUID = 936099218746529828L;

	public MockDealer( Integer dealerId ) {
    	super();
		this.dealerId = dealerId; 
		dealerCode = "Mock000001";
		shortName = "MockDealer";
    }
    
    public void setDealerPreference( DealerPreference preference ) {
    	Set<DealerPreference> newPreferences = new HashSet<DealerPreference>();
    	newPreferences.add( preference );
    	dealerPreferences = newPreferences;
    }
}
