package biz.firstlook.module.vehicle.description.provider.kbb;

import junit.framework.TestCase;
import biz.firstlook.module.vehicle.description.VehicleDescription;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;

public class TestKbbTPV_VehicleDescriptionAdapter extends TestCase {

	public void testAdapt()
	{
		ThirdPartyVehicle vehicle = new ThirdPartyVehicle();
		vehicle.setMake("Ford");
		vehicle.setMakeCode("FD");
		vehicle.setModelCode("A1");
		vehicle.setModel("F150");
		
		VehicleDescription newVehicle = new KbbTPV_VehicleAdapter().adaptItem( vehicle );
			
		assertEquals("Make does not match", vehicle.getMake(), newVehicle.getMake());
		assertEquals("Model does not match", vehicle.getModel(), newVehicle.getModel());
		KbbVehicleDescriptionImpl kbbVDesc = (KbbVehicleDescriptionImpl)newVehicle;
		assertEquals("ModelCode does not match", vehicle.getModelCode(), kbbVDesc.getModelCode().getCode());
		assertEquals("MakeCdoe does not match", vehicle.getMakeCode(), kbbVDesc.getMakeCode().getCode());
	}

	public void testUnadapt()
	{
		KbbVehicleDescriptionImpl kbbVehicle = new KbbVehicleDescriptionImpl();
		kbbVehicle.setMake("Ford");
		kbbVehicle.setMakeCode( new MakeId("FD"));
		kbbVehicle.setModelCode( new ModelId("A1"));
		kbbVehicle.setModel("F150");
		
		ThirdPartyVehicle vdpVehicle = new KbbTPV_VehicleAdapter().unadaptItem( kbbVehicle );
		
		assertEquals("Make does not match", kbbVehicle.getMake(), vdpVehicle.getMake());
		assertEquals("Model does not match", kbbVehicle.getModel(), vdpVehicle.getModel());
		assertEquals("ModelCode does not match", kbbVehicle.getModelCode().getCode(), vdpVehicle.getModelCode());
		assertEquals("MakeCdoe does not match", kbbVehicle.getMakeCode().getCode(), vdpVehicle.getMakeCode());
	}

	
}
