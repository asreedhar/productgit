package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.module.vehicle.description.VehicleOption;

import junit.framework.TestCase;

public class TestKbbVehicleBookoutState extends TestCase {

	public void testSetVehicleDescriptionAndSelctedOptions(){
		KbbVehicleDescriptionImpl vehicleDesc = new KbbVehicleDescriptionImpl();
		vehicleDesc.setMake("ford");
		vehicleDesc.setMakeCode(new MakeId("fo"));
		vehicleDesc.setModel("f150");
		vehicleDesc.setModelCode(new ModelId("f1"));
		KbbVehicleOption opt1 = new KbbVehicleOption();
		opt1.setDisplayName("AC");
		opt1.setOptionKey("AC");
		opt1.setThirdPartyOptionTypeId(1);
		opt1.setSortOrder(1);
		List<VehicleOption> options = new ArrayList<VehicleOption>();
		options.add( opt1 );
		vehicleDesc.setVehicleOptions( options );
		
		KbbVehicleDescriptionImpl vehicleDesc1 = new KbbVehicleDescriptionImpl();
		vehicleDesc1.setMake("ford");
		vehicleDesc1.setMakeCode(new MakeId("fo"));
		vehicleDesc1.setModel("f150");
		vehicleDesc1.setModelCode(new ModelId("f1"));
		KbbVehicleOption opt2 = new KbbVehicleOption();
		opt2.setDisplayName("CC");
		opt2.setOptionKey("CC");
		opt2.setThirdPartyOptionTypeId(1);
		opt2.setSortOrder(2);
		List<VehicleOption> options1 = new ArrayList<VehicleOption>();
		options1.add( opt1 );
		options1.add( opt2 );
		vehicleDesc1.setVehicleOptions(options1);
		
		KbbVehicleBookoutState bookoutState = new KbbVehicleBookoutState("1FTFE1160WHA00001", 100123, null);
		bookoutState.setVehicleDescription(vehicleDesc);
		assertEquals( "size of list of options is wrong!", 1, bookoutState.getVehicleDescription().getVehicleOptions().size() );
		// now overwrite old options with new options
		bookoutState.setVehicleDescription(vehicleDesc1);
		assertEquals( "size of list of options is wrong!", 2, bookoutState.getVehicleDescription().getVehicleOptions().size() );
	
		bookoutState.setVehicleDescription(vehicleDesc);
		assertEquals( "size of list of options is wrong!", 1, bookoutState.getVehicleDescription().getVehicleOptions().size() );
	}

}