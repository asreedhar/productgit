package biz.firstlook.module.vehicle.description.provider.kbb;

import biz.firstlook.module.bookout.BookValue;
import biz.firstlook.module.bookout.kbb.KbbBookValue;
import biz.firstlook.module.bookout.kbb.KbbVDP_BookValueAdapter;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import junit.framework.TestCase;

public class TestKbbVDP_BookValueAdater extends TestCase {

	public void testAdapt()
	{
		VDP_GuideBookValue vdpBV = new VDP_GuideBookValue();
		vdpBV.setValue(12345);
		
		BookValue kbbBV = (BookValue)new KbbVDP_BookValueAdapter().adaptItem(vdpBV);
		
		assertEquals("VDP and KBB values are not equal", vdpBV.getValue(), kbbBV.getValue());
	}
	
	public void testUnadapt()
	{
		KbbBookValue kbbBV = new KbbBookValue( 12345 );		
		VDP_GuideBookValue vdpBV = new KbbVDP_BookValueAdapter().unadaptItem(kbbBV);
		assertEquals("VDP and KBB values are not equal", vdpBV.getValue(), kbbBV.getValue());
	}
}
