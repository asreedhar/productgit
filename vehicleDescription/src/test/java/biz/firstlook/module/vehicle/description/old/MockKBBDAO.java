package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class MockKBBDAO implements IKBBDAO
{

public List< VDP_GuideBookValue > checkForEquipmentConflicts( String vehicleId, String selectedOptions, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public List< String > checkForOptionConflictsWithStdOptions( StringBuilder missingStdOptionCodes, StringBuilder selectedOptionCodes,
																BookOutDatasetInfo bookoutInfo )
{
	List< String > optionsThatConflictWithStdOptions = new ArrayList< String >();
	optionsThatConflictWithStdOptions.add( "T2" );
	return optionsThatConflictWithStdOptions;
}

public List< ThirdPartyVehicleOption > doOptionsLookup( String vin, String vehicleId, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public List< VDP_GuideBookVehicle > doVinLookup( String vin, Integer year, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public Map< String, String > getMetaInfo( BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public KbbPriceAdjustmentsTable getValues( String kbbModelId,int year, int mileage, String regionCode, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public List< VDP_GuideBookVehicle > retrieveMakes( String year, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public List< VDP_GuideBookVehicle > retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public List< VDP_GuideBookVehicle > retrieveTrims( String year, String makeId, String modelId, BookOutDatasetInfo bookoutInfo )
{
	return null;
}

public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo) {
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear(String vin,
		Integer year, BookOutDatasetInfo bookoutInfo) {
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId,
		String modelId, BookOutDatasetInfo bookoutInfo) {
	// TODO Auto-generated method stub
	return null;
}


}
