package biz.firstlook.module.vehicle.description;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;
import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbTPV_VehicleAdapter;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleOption;
import biz.firstlook.module.vehicle.description.provider.kbb.Kbb_VehicleOptionAdapter;
import biz.firstlook.module.vehicle.description.provider.kbb.ModelId;
import biz.firstlook.module.vehicle.description.provider.kbb.TestKbbVehicleOptionAdapter;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class TestVehicleBookoutState extends TestCase {
	
	private MockVehicleBookoutStateImpl testState; 

	public void setUp() {
		ThirdPartyVehicle tpv = new ThirdPartyVehicle();
		ThirdPartyVehicleOption tpo1 = (new TestKbbVehicleOptionAdapter()).createThirdPartyOption("AC", "Air Con");
		tpo1.setStatus( true );
		ThirdPartyVehicleOption tpo2 = (new TestKbbVehicleOptionAdapter()).createThirdPartyOption("CD", "CD");
		tpo2.setStatus( true );
		ThirdPartyVehicleOption tpo3 = (new TestKbbVehicleOptionAdapter()).createThirdPartyOption("PS", "Power Steering");
		tpo3.setStatus( false );
		ThirdPartyVehicleOption tpo4 = (new TestKbbVehicleOptionAdapter()).createThirdPartyOption("PW", "Power Windows");
		tpo4.setStatus( false );
		
		tpv.addThirdPartyVehicleOption(tpo1);
		tpv.addThirdPartyVehicleOption(tpo2);
		tpv.addThirdPartyVehicleOption(tpo3);
		tpv.addThirdPartyVehicleOption(tpo4);
		tpv.setThirdPartyId( 3 );
		tpv.setThirdPartyVehicleCode("code");
		tpv.setMake("make");
		tpv.setMakeCode("makeCode");
		tpv.setModel("Model");
		tpv.setModelCode("ModelCode");
		
		MockVehicleBookoutStateImpl bookoutState = new MockVehicleBookoutStateImpl();
		bookoutState.getThirdPartyVehicles().add( tpv );
		
		VehicleDescription vtestImpl = new KbbTPV_VehicleAdapter().adaptItem(tpv);
		KbbVehicleDescriptionImpl testImpl = (KbbVehicleDescriptionImpl)vtestImpl; 
		testImpl.setModelCode( new ModelId( "code" ));
		testImpl.setVehicleOptions(Adapters.adapt(tpv.getThirdPartyVehicleOptions(), new Kbb_VehicleOptionAdapter() ) );
		bookoutState.setVehicleDescription( testImpl );
		this.testState = bookoutState;
	}
	
	public void tearDown() {
		this.testState = null;
	}
	
	public void testModifyOptionsMethod() {
		KbbVehicleDescriptionImpl testVDesc = testState.getVehicleDescription();
		List<VehicleOption> selectedOptions = new ArrayList<VehicleOption>();
		KbbVehicleOption kbbOption1 = new KbbVehicleOption();
		kbbOption1.setOptionKey("AC");
		kbbOption1.setDisplayName("Air Con" );
		kbbOption1.setThirdPartyOptionTypeId(1);
		KbbVehicleOption kbbOption2 = new KbbVehicleOption();
		kbbOption2.setOptionKey("PS");
		kbbOption2.setDisplayName("Power Steering" );
		kbbOption2.setThirdPartyOptionTypeId(1);
		selectedOptions.add( kbbOption1 );
		selectedOptions.add( kbbOption2 );
		testVDesc.setSelectedVehicleOptions( selectedOptions );
		
		ThirdPartyVehicle newTpv = testState.getThirdPartyVehicles().iterator().next();
		
		for ( ThirdPartyVehicleOption newTpvo : newTpv.getThirdPartyVehicleOptions() ) {
			if ( "AC".equals( newTpvo.getOptionKey() ) || "PS".equals( newTpvo.getOptionKey() ) ) {
				assertTrue("option that should have been selected was not selected!", newTpvo.getStatus() );
			} else {
				assertFalse("option that should have been unslected was selected!", newTpvo.getStatus() );
			}
		}
	}
	
	public void testAddNewOptionInModifyOptionsMethod() {
		KbbVehicleDescriptionImpl testVDesc = testState.getVehicleDescription();
		List<VehicleOption> selectedOptions = new ArrayList<VehicleOption>();
		KbbVehicleOption kbbOption1 = new KbbVehicleOption();
		kbbOption1.setOptionKey("XY");
		kbbOption1.setDisplayName("New Option" );
		kbbOption1.setThirdPartyOptionTypeId(1);
		List<VehicleOption> defaultOptions = testVDesc.getVehicleOptions();
		defaultOptions.add(kbbOption1);
		testVDesc.setVehicleOptions( defaultOptions );
		selectedOptions.add( kbbOption1 );
		testVDesc.setSelectedVehicleOptions( selectedOptions );
		
		ThirdPartyVehicle newTpv = testState.getThirdPartyVehicles().iterator().next();
		assertEquals( " new option was not added to the thirdPartyOptionsList correctly!", 5, newTpv.getThirdPartyVehicleOptions().size() );
	}

	class MockVehicleBookoutStateImpl extends VehicleBookoutState {
		public KbbVehicleDescriptionImpl description;
		public KbbVehicleDescriptionImpl getVehicleDescription() {
			return description;
		}
		public void setVehicleDescription(VehicleDescription inVDesc) {
			this.description = (KbbVehicleDescriptionImpl)inVDesc;
		}
		
		public Set<ThirdPartyVehicle> getThirdPartyVehicles() {
			if ( description == null ) { return new HashSet<ThirdPartyVehicle>();}
			ThirdPartyVehicle newTpv = new KbbTPV_VehicleAdapter().unadaptItem( description );
			super.mergeOldAndNewOptions( newTpv );
			return this.thirdPartyVehicles;
		}
	}
	
//	class MockVehicleDescriptionImpl extends AbstractVehicleDescriptionImpl<KbbVehicleOption>{
//		public void assertSelectedOptionsSetInValidState() {}
//		public BookValueContainer getBookValueContainer(Integer mileage) throws ValuesRetrievalException { return null;}
//		public VehicleDescriptionProviderEnum getProvider() { return null; }
//		public List<KbbVehicleOption> getSelectedVehicleOptions() {return null; }
//		public List<KbbVehicleOption> getVehicleOptions() { return null; }
//		public void setSelectedVehicleOptions(List<KbbVehicleOption> setOfSelectedOptions) { 
//			List<ThirdPartyVehicleOption> selectedOptions = Adapters.unadapt(setOfSelectedOptions, new Kbb_VehicleOptionAdapter());
//			for( ThirdPartyVehicleOption option : this.options ) {
//				boolean updated = false;
//				for( ThirdPartyVehicleOption selectedOption : selectedOptions ) {
//					if ( selectedOption.getOptionKey().equals( option.getOptionKey() ) ) {
//						option.setStatus( true );
//						updated = true;
//						break;
//					}
//				}
//				if ( !updated ) { option.setStatus( false ); }
//			}
//		}
//		public void setVehicleDescriptionProvider(VehicleDescriptionProvider provider) {}
//		public void setVehicleOptions(List<KbbVehicleOption> options) {
//			this.options = new ValidatableList<ThirdPartyVehicleOption>( Adapters.unadapt(options, new Kbb_VehicleOptionAdapter()) ); 
//		}
//		public void validate() throws OptionsConflictException {		}
//		public boolean isBookoutable() {return false;	}
//	}
}
