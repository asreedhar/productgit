package biz.firstlook.module.vehicle.description.impl;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionProvider;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.module.vehicle.description.MockDealer;;

public class TestVehicleDescriptionContextFactory extends AbstractSpringDependencyTest
{

private MockDealer d;

public TestVehicleDescriptionContextFactory()
{
	super();
}

@Override
protected void onSetUpBeforeTransaction() throws Exception
{
	super.onSetUpBeforeTransaction();
	d = new MockDealer( 100147 );
	DealerPreference dp = new DealerPreference();
	dp.setGuideBookId( VehicleDescriptionProviderEnum.KBB.getThirdPartyId() );
	dp.setGuideBook2Id( null );
	d.setDealerPreference( dp );
}

public void testRegisteredProviderContextByEnum()
{
	VehicleDescriptionContext cxt = VehicleDescriptionContextFactory.getInstance().getBusinessUnitContext( d );
	assertEquals( 1, cxt.getRegisteredDescriptionProviders().size() );
	
	VehicleDescriptionProvider provider = cxt.getDescriptionProvider( VehicleDescriptionProviderEnum.KBB );
	assertEquals( KbbVehicleDescriptionProvider.class, provider.getClass() );
}

public void testNotRegisteredProviderContextByEnum()
{
	DealerPreference dp = new DealerPreference();
	dp.setGuideBookId( VehicleDescriptionProviderEnum.Galves.getThirdPartyId() );
	d.setDealerPreference( dp );
	VehicleDescriptionContext cxt = VehicleDescriptionContextFactory.getInstance().getBusinessUnitContext( d );
	assertEquals( 0, cxt.getRegisteredDescriptionProviders().size() );
	
	VehicleDescriptionProvider provider = cxt.getDescriptionProvider( VehicleDescriptionProviderEnum.Galves );
	assertNull( provider );
}

public void testRegisteredProviderContext()
{
	d.getDealerPreference().setGuideBook2Id( VehicleDescriptionProviderEnum.Galves.getThirdPartyId() );
	VehicleDescriptionContext cxt = VehicleDescriptionContextFactory.getInstance().getBusinessUnitContext( d );
	assertEquals( 1, cxt.getRegisteredDescriptionProviders().size() );
	
	VehicleDescriptionProvider provider = cxt.getDescriptionProvider( VehicleDescriptionProviderEnum.Galves );
	assertNull( provider );
	
	provider = cxt.getDescriptionProvider( VehicleDescriptionProviderEnum.KBB );
	assertEquals( KbbVehicleDescriptionProvider.class, provider.getClass() );
}

}
