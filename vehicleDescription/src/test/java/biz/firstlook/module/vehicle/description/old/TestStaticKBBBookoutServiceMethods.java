package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

public class TestStaticKBBBookoutServiceMethods extends TestCase
{

//this tests logic that says: if no default exists for engin/transmission/drivetrain - set the option with the value closest to 0 as default
public void testMakeSureDefaultExists()
{
	ThirdPartyOption option1 = new ThirdPartyOption("2W", "2W", ThirdPartyOptionType.ENGINE);
	ThirdPartyVehicleOption vehicleOption1 = new ThirdPartyVehicleOption(
			option1,
			false,
			0);
	vehicleOption1.setStatus( false );
	ThirdPartyVehicleOptionValue optionValue1 = new ThirdPartyVehicleOptionValue(vehicleOption1, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 100);
	Set<ThirdPartyVehicleOptionValue> valuesSet1 = new HashSet< ThirdPartyVehicleOptionValue >();
	valuesSet1.add( optionValue1 );
	vehicleOption1.setOptionValues( valuesSet1 );

	ThirdPartyOption option2 = new ThirdPartyOption("3W", "3W", ThirdPartyOptionType.ENGINE);
	ThirdPartyVehicleOption vehicleOption2 = new ThirdPartyVehicleOption(option2, false, 1);
	vehicleOption2.setStatus( false );
	ThirdPartyVehicleOptionValue optionValue2 = new ThirdPartyVehicleOptionValue(vehicleOption2, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 200);
	Set<ThirdPartyVehicleOptionValue> valuesSet2 = new HashSet< ThirdPartyVehicleOptionValue >();
	valuesSet2.add( optionValue2 );
	vehicleOption2.setOptionValues( valuesSet2 );
	
	ThirdPartyOption option3 = new ThirdPartyOption("4W", "4W", ThirdPartyOptionType.ENGINE);
	ThirdPartyVehicleOption vehicleOption3 = new ThirdPartyVehicleOption(option3, false, 2);
	vehicleOption3.setStatus( false );
	ThirdPartyVehicleOptionValue optionValue3 = new ThirdPartyVehicleOptionValue(vehicleOption3, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 300);
	Set<ThirdPartyVehicleOptionValue> valuesSet3 = new HashSet< ThirdPartyVehicleOptionValue >();
	valuesSet3.add( optionValue3 );
	vehicleOption3.setOptionValues( valuesSet3 );
	
	List< ThirdPartyVehicleOption > options = new ArrayList< ThirdPartyVehicleOption >();
	options.add( vehicleOption1 );
	options.add( vehicleOption2 );
	options.add( vehicleOption3 );
	
	// update list and set default
	biz.firstlook.module.vehicle.description.old.KBBBookoutService.makeSureDefaultExists( options, ThirdPartyOptionType.ENGINE );
	// get new default
	ThirdPartyVehicleOption newDefault = biz.firstlook.module.vehicle.description.old.KBBBookoutService.getDefaultOption( options, ThirdPartyOptionType.ENGINE );
	assertEquals( "new default was not closest to zero!", "2W", newDefault.getOptionKey() );
	
	// increase value of default option
	((ThirdPartyVehicleOptionValue)vehicleOption1.getOptionValues().iterator().next()).setValue( 800 );
	// make sure optoin remains default even though it is no longer the cheapest
	biz.firstlook.module.vehicle.description.old.KBBBookoutService.makeSureDefaultExists( options, ThirdPartyOptionType.ENGINE );
	// get new default
	newDefault = biz.firstlook.module.vehicle.description.old.KBBBookoutService.getDefaultOption( options, ThirdPartyOptionType.ENGINE );
	assertEquals( "default was incorrectly overridden!", "2W", newDefault.getOptionKey() );
	
	// unselect default
	vehicleOption1.setStatus( false );
	// set value closest to 0 to be negative to make sure we can handle positive and negative values
	((ThirdPartyVehicleOptionValue)vehicleOption2.getOptionValues().iterator().next()).setValue( -100 );

	biz.firstlook.module.vehicle.description.old.KBBBookoutService.makeSureDefaultExists( options, ThirdPartyOptionType.ENGINE );
	// get new default
	newDefault = biz.firstlook.module.vehicle.description.old.KBBBookoutService.getDefaultOption( options, ThirdPartyOptionType.ENGINE );
	assertEquals( "default was incorrectly overridden!", "3W", newDefault.getOptionKey() );
	
}

public void testGetValuesForDisplay()
{
	List< VDP_GuideBookValue > values = new ArrayList< VDP_GuideBookValue >();

	VDP_GuideBookValue value2 = new VDP_GuideBookValue();
	value2.setHasErrors( false );
	value2.setThirdPartyCategoryId( 9 );
	value2.setThirdPartyCategoryId( 9 );
	value2.setValueType( 1 );
	value2.setValue( 1 );
	values.add( value2 );

	VDP_GuideBookValue value4 = new VDP_GuideBookValue();
	value4.setHasErrors( false );
	value4.setThirdPartyCategoryId( 8 );
	value4.setThirdPartyCategoryId( 8 );
	value4.setValueType( 1 );
	value4.setValue( 2 );
	values.add( value4 );
	
	VDP_GuideBookValue value3 = new VDP_GuideBookValue();
	value3.setHasErrors( false );
	value3.setThirdPartyCategoryId( 11 );
	value3.setThirdPartyCategoryId( 11 );
	value3.setValueType( 1 );
	value3.setValue( 3 );
	values.add( value3 );
	
	VDP_GuideBookValue value6 = new VDP_GuideBookValue();
	value6.setHasErrors( false );
	value6.setThirdPartyCategoryId( 11 );
	value6.setThirdPartyCategoryId( 12 );
	value6.setValueType( 3 );
	value6.setValue( 4 );
	values.add( value6 );

	VDP_GuideBookValue value9 = new VDP_GuideBookValue();
	value9.setHasErrors( false );
	value9.setThirdPartyCategoryId( 11 );
	value9.setThirdPartyCategoryId( 13 );
	value9.setValueType( 2 ); // mileage adjustment
	value9.setValue( 5 );
	values.add( value9 );
	
	KBBBookoutService kbbBookoutService = new KBBBookoutService();
	
	List< VDP_GuideBookValue > excellentValues = kbbBookoutService.getValuesForDisplay( values, KBBConditionEnum.EXCELLENT );
	assertEquals( "wrong number of values returned!", 3, excellentValues.size() );
	assertEquals( "wrong retail value", 1, excellentValues.get(0).getValue().intValue() );
	assertEquals( "wrong wholesale value", 2, excellentValues.get(1).getValue().intValue() );
	assertEquals( "wrong trade-in value", 3, excellentValues.get(2).getValue().intValue() );
	
	List< VDP_GuideBookValue > goodValues = kbbBookoutService.getValuesForDisplay( values, KBBConditionEnum.GOOD );
	assertEquals( "wrong number of values returned!", 3, goodValues.size() );
	assertEquals( "wrong retail value", 1, goodValues.get(0).getValue().intValue() );
	assertEquals( "wrong wholesale value", 2, goodValues.get(1).getValue().intValue() );
	assertEquals( "wrong trade-in value", 4, goodValues.get(2).getValue().intValue() );
	
	List< VDP_GuideBookValue > fairValues = kbbBookoutService.getValuesForDisplay( values, KBBConditionEnum.FAIR );
	assertEquals( "wrong number of values returned!", 3, fairValues.size() );
	assertEquals( "wrong retail value", 1, fairValues.get(0).getValue().intValue() );
	assertEquals( "wrong wholesale value", 2, fairValues.get(1).getValue().intValue() );
	assertEquals( "wrong trade-in value", 5, fairValues.get(2).getValue().intValue() );
}

public void testComputeMileageIndependantValues()
{
	List< VDP_GuideBookValue > baseValues = new ArrayList< VDP_GuideBookValue >();

	VDP_GuideBookValue value1 = new VDP_GuideBookValue();
	value1.setHasErrors( false );
	value1.setThirdPartyCategoryId( 9 );
	value1.setThirdPartyCategoryId( 9 );
	value1.setValueType( 1 );
	value1.setValue( 100 );
	baseValues.add( value1 );

	VDP_GuideBookValue value2 = new VDP_GuideBookValue();
	value2.setHasErrors( false );
	value2.setThirdPartyCategoryId( 8 );
	value2.setThirdPartyCategoryId( 8 );
	value2.setValueType( 1 );
	value2.setValue( 200 );
	baseValues.add( value2 );
	
	VDP_GuideBookValue value3 = new VDP_GuideBookValue();
	value3.setHasErrors( false );
	value3.setThirdPartyCategoryId( 11 );
	value3.setThirdPartyCategoryId( 11 );
	value3.setValueType( 1 );
	value3.setValue( 300 );
	baseValues.add( value3 );
	
	List< VDP_GuideBookValue > optionValues = new ArrayList< VDP_GuideBookValue >();
	
	VDP_GuideBookValue value4 = new VDP_GuideBookValue();
	value4.setHasErrors( false );
	value4.setThirdPartyCategoryId( 9 );
	value4.setThirdPartyCategoryId( 9 );
	value4.setValueType( 4 );
	value4.setValue( 1 );
	optionValues.add( value4 );

	VDP_GuideBookValue value5 = new VDP_GuideBookValue();
	value5.setHasErrors( false );
	value5.setThirdPartyCategoryId( 8 );
	value5.setThirdPartyCategoryId( 8 );
	value5.setValueType( 4 );
	value5.setValue( 2 );
	optionValues.add( value5 );
	
	// this value should not be used - just making sure its not (condition doesn't match up)
	VDP_GuideBookValue value6 = new VDP_GuideBookValue();
	value6.setHasErrors( false );
	value6.setThirdPartyCategoryId( 11 );
	value6.setThirdPartyCategoryId( 12 );
	value6.setValueType( 4 );
	value6.setValue( 3 );
	optionValues.add( value6 );
	
	VDP_GuideBookValue value7 = new VDP_GuideBookValue();
	value7.setHasErrors( false );
	value7.setThirdPartyCategoryId( 11 );
	value7.setThirdPartyCategoryId( 11 );
	value7.setValueType( 4 );
	value7.setValue( 4 );
	optionValues.add( value7 );
	
	KBBBookoutService kbbBookoutService = new KBBBookoutService();
	
	List<VDP_GuideBookValue> mileageIndepentValues = kbbBookoutService.computeMileageIndependantValues( baseValues, optionValues );
	assertEquals( "wrong number of values returned!", 3, mileageIndepentValues.size() );
	assertEquals( "wrong retail value", 101, mileageIndepentValues.get(0).getValue().intValue() );
	assertEquals( "wrong wholesale value", 202, mileageIndepentValues.get(1).getValue().intValue() );
	assertEquals( "wrong trade-in value", 304, mileageIndepentValues.get(2).getValue().intValue() );
	
	// when no values for vehicle - don't apply options values (so price remains at 0) 
	baseValues.get(0).setValue( 0 );
	mileageIndepentValues = kbbBookoutService.computeMileageIndependantValues( baseValues, optionValues );
	assertEquals( "wrong number of values returned!", 3, mileageIndepentValues.size() );
	assertEquals( "wrong retail value", 0, mileageIndepentValues.get(0).getValue().intValue() );
	assertEquals( "wrong wholesale value", 202, mileageIndepentValues.get(1).getValue().intValue() );
	assertEquals( "wrong trade-in value", 304, mileageIndepentValues.get(2).getValue().intValue() );
}

public void testGetOptionValue()
{
	List< VDP_GuideBookValue > optionValues = new ArrayList< VDP_GuideBookValue >();

	VDP_GuideBookValue value1 = new VDP_GuideBookValue();
	value1.setHasErrors( false );
	value1.setThirdPartyCategoryId( 9 );
	value1.setThirdPartySubCategoryId( 9 );
	value1.setValueType( 1 );
	value1.setValue( 1 );
	optionValues.add( value1 );

	VDP_GuideBookValue value2 = new VDP_GuideBookValue();
	value2.setHasErrors( false );
	value2.setThirdPartyCategoryId( 8 );
	value2.setThirdPartySubCategoryId( 8 );
	value2.setValueType( 1 );
	value2.setValue( 2 );
	optionValues.add( value2 );
	
	VDP_GuideBookValue value3 = new VDP_GuideBookValue();
	value3.setHasErrors( false );
	value3.setThirdPartyCategoryId( 11 );
	value3.setThirdPartySubCategoryId( 11 );
	value3.setValueType( 1 );
	value3.setValue( 4 );
	optionValues.add( value3 );
	
	VDP_GuideBookValue value4 = new VDP_GuideBookValue();
	value4.setHasErrors( false );
	value4.setThirdPartyCategoryId( 11 );
	value4.setThirdPartySubCategoryId( 12 );
	value4.setValueType( 1 );
	value4.setValue( 8 );
	optionValues.add( value4 );
	
	VDP_GuideBookValue value5 = new VDP_GuideBookValue();
	value5.setHasErrors( false );
	value5.setThirdPartyCategoryId( 11 );
	value5.setThirdPartySubCategoryId( 13 );
	value5.setValueType( 1 );
	value5.setValue( 16 );
	optionValues.add( value5 );
	
	KBBBookoutService kbbBookoutService = new KBBBookoutService();

	VDP_GuideBookValue optionValue = kbbBookoutService.getOptionValue( optionValues, KBBConditionEnum.EXCELLENT );
	assertEquals( "retail option not summed correctly", 4, optionValue.getValue().intValue() );
	
	optionValue = kbbBookoutService.getOptionValue( optionValues, KBBConditionEnum.GOOD );
	assertEquals( "retail option not summed correctly", 8, optionValue.getValue().intValue() );
	
	optionValue = kbbBookoutService.getOptionValue( optionValues, KBBConditionEnum.FAIR );
	assertEquals( "retail option not summed correctly", 16, optionValue.getValue().intValue() );
	
}

public void testComputeOptionsValue()
{
	List<ThirdPartyVehicleOption> allOptions = getSmallSetOfOptions();
	
	KBBBookoutService kbbBookoutService = new KBBBookoutService();
	kbbBookoutService.setKbbDAO( new MockKBBDAO() );
	List<String> optionsWholeValuesShouldBeApplied = new ArrayList<String>();
	optionsWholeValuesShouldBeApplied.add( "T1" );
	optionsWholeValuesShouldBeApplied.add( "PW" );
	optionsWholeValuesShouldBeApplied.add( "PL" );
	
	List< VDP_GuideBookValue > fromService = kbbBookoutService.computeOptionsValue( allOptions, optionsWholeValuesShouldBeApplied, new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL, 100215) );
	
	VDP_GuideBookValue retailOptionsAdj = new VDP_GuideBookValue(131, ThirdPartyCategory.KELLEY_RETAIL_TYPE, ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartyCategory.KELLEY_RETAIL_TYPE );
	VDP_GuideBookValue wholesaleOptionsAdj = new VDP_GuideBookValue(-62, ThirdPartyCategory.KELLEY_WHOLESALE_TYPE, ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartyCategory.KELLEY_WHOLESALE_TYPE );
	VDP_GuideBookValue tradeIn_excAdj = new VDP_GuideBookValue(7, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT.getThirdPartySubCategoryId()  );
	VDP_GuideBookValue tradeIn_goodAdj = new VDP_GuideBookValue(14, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD.getThirdPartySubCategoryId() );
	VDP_GuideBookValue tradeIn_fairAdj = new VDP_GuideBookValue(28, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR.getThirdPartySubCategoryId()  );
	VDP_GuideBookValue pp_excAdj = new VDP_GuideBookValue(56, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_EXCELLENT.getThirdPartySubCategoryId()  );
	VDP_GuideBookValue pp_goodAdj = new VDP_GuideBookValue(112, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_GOOD.getThirdPartySubCategoryId()  );
	VDP_GuideBookValue pp_fairAdj = new VDP_GuideBookValue(-31, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_FAIR.getThirdPartySubCategoryId()  );

	List< VDP_GuideBookValue > expectedList = new ArrayList< VDP_GuideBookValue >();
	expectedList.add( retailOptionsAdj );
	expectedList.add( wholesaleOptionsAdj );
	expectedList.add( tradeIn_excAdj );
	expectedList.add( tradeIn_fairAdj );
	expectedList.add( tradeIn_goodAdj );
	expectedList.add( pp_excAdj );
	expectedList.add( pp_fairAdj );
	expectedList.add( pp_goodAdj );
	
	for ( int c = 0; c < expectedList.size(); c++ )
		assertEquals( "value " + c + " didn't match!", expectedList.get( c ), fromService.get( c ) );
}

public void testGetOptionsWhoseValuesShouldBeApplied()
{
	List<ThirdPartyVehicleOption> allOptions = getSmallSetOfOptions();
	
	KBBBookoutService kbbBookoutService = new KBBBookoutService();
	kbbBookoutService.setKbbDAO( new MockKBBDAO() );
	
	List<String> optionWhoseValuesShouldBeApplied = kbbBookoutService.getOptionSelections(
	                                                                       allOptions, new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL, 100215) );
	
	List<String> expectedList = new ArrayList< String >();
	expectedList.add( "T1" );
	expectedList.add( "PW" );
	expectedList.add( "PL" );
	
	assertEquals( "didn't get the right number of options back!", expectedList.size(), optionWhoseValuesShouldBeApplied.size() );
	assertEquals( "option 1 didn't match!", expectedList.get(0), optionWhoseValuesShouldBeApplied.get(0) );
	assertEquals( "option 2 didn't match!", expectedList.get(1), optionWhoseValuesShouldBeApplied.get(1) );
	assertEquals( "option 3 didn't match!", expectedList.get(2), optionWhoseValuesShouldBeApplied.get(2) );
	
}

private List< ThirdPartyVehicleOption > getSmallSetOfOptions()
{
	List<ThirdPartyVehicleOption> allOptions = new ArrayList<ThirdPartyVehicleOption>();
	
	ThirdPartyVehicleOption option1 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("FX4", "P1", ThirdPartyOptionType.EQUIPMENT),
		false,
		0);
	option1.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 1 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 2 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 4 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 8 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 16 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, 32 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, 64 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, 128 ) );
	option1.setStatus( false );

	ThirdPartyVehicleOption option2 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("XLT", "T1", ThirdPartyOptionType.EQUIPMENT),
		false,
		1);
	option2.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 2 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 4 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 8 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 16 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 32 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, 64 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, 128 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, 1 ) );
	option2.setStatus( true );

	ThirdPartyVehicleOption option3 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Lariat", "T2", ThirdPartyOptionType.EQUIPMENT),
		true,
		2);
	option3.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, -4 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, -8 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, -16 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, -32 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, -64 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, -128 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, -1 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, -2 ) );
	option3.setStatus( false );

	ThirdPartyVehicleOption option4 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Air Conditioning", "AC", ThirdPartyOptionType.EQUIPMENT),
		true,
		3);
	option4.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 8 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 16 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 32 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 64 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 128 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, 1 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, 2 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, 4 ) );
	option4.setStatus( true );

	ThirdPartyVehicleOption option5 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Power Steering", "PS", ThirdPartyOptionType.EQUIPMENT),
		true,
		4);
	option5.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, -32 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, -64 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 128 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 1 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 2 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, 4 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, 8 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, 16 ) );
	option5.setStatus( true );

	ThirdPartyVehicleOption option6 = new ThirdPartyVehicleOption(
	new ThirdPartyOption("Power Windows", "PW", ThirdPartyOptionType.EQUIPMENT),
		false,
		5);
	option6.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 64 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 128 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 1 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 2 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 4 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, 8 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, 16 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, 32 ) );
	option6.setStatus( true );

	ThirdPartyVehicleOption option7 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Power Door Locks", "PL", ThirdPartyOptionType.EQUIPMENT),
		true,
		6);
	option7.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, -128 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, -1 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, -2 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, -4 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, -8 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT, -16 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD, -32 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR, -64 ) );
	option7.setStatus( false );
	
	allOptions.add( option1 );
	allOptions.add( option2 );
	allOptions.add( option3 );
	allOptions.add( option4 );
	allOptions.add( option5 );
	allOptions.add( option6 );
	allOptions.add( option7 );
	return allOptions;
}




}
