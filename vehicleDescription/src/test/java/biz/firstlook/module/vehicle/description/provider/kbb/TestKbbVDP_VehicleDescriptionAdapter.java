package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.ArrayList;

import junit.framework.TestCase;
import biz.firstlook.module.vehicle.description.VehicleDescription;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class TestKbbVDP_VehicleDescriptionAdapter extends TestCase {

	public void testAdapt()
	{
		VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
		vehicle.setMake("Ford");
		vehicle.setMakeId("FD");
		vehicle.setYear("1998");
		vehicle.setModelId("A1");
		vehicle.setModel("F150");
		vehicle.setGuideBookOptions( new ArrayList<ThirdPartyVehicleOption>() );
		
		VehicleDescription newVehicle = new KbbVDP_VehicleAdapter(null).adaptItem( vehicle );
		
		assertEquals("Year does not match", vehicle.getYear(), newVehicle.getYear().toString());
		assertEquals("Make does not match", vehicle.getMake(), newVehicle.getMake());
		assertEquals("Model does not match", vehicle.getModel(), newVehicle.getModel());
		KbbVehicleDescriptionImpl kbbVDesc = (KbbVehicleDescriptionImpl)newVehicle;
		assertEquals("ModelCode does not match", vehicle.getModelId(), kbbVDesc.getModelCode().getCode());
		assertEquals("MakeCdoe does not match", vehicle.getMakeId(), kbbVDesc.getMakeCode().getCode());
	}

	public void testUnadapt()
	{
		KbbVehicleDescriptionImpl kbbVehicle = new KbbVehicleDescriptionImpl();
		kbbVehicle.setMake("Ford");
		kbbVehicle.setMakeCode(new MakeId("FD"));
		kbbVehicle.setYear( 1998 );
		kbbVehicle.setModelCode(new ModelId("A1"));
		kbbVehicle.setModel("F150");
		
		VDP_GuideBookVehicle vdpVehicle = new KbbVDP_VehicleAdapter(null).unadaptItem( kbbVehicle );
		
		assertEquals("Year does not match", kbbVehicle.getYear().toString(), vdpVehicle.getYear());
		assertEquals("Make does not match", kbbVehicle.getMake(), vdpVehicle.getMake());
		assertEquals("Model does not match", kbbVehicle.getModel(), vdpVehicle.getModel());
		assertEquals("ModelCode does not match", kbbVehicle.getModelCode().getCode(), vdpVehicle.getModelId());
		assertEquals("MakeCdoe does not match", kbbVehicle.getMakeCode().getCode(), vdpVehicle.getMakeId());
	}
	
}
