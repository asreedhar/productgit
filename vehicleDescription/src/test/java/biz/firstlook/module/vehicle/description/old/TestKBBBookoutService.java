package biz.firstlook.module.vehicle.description.old;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import biz.firstlook.module.bookout.IThirdPartyBookoutService;
import biz.firstlook.module.vehicle.description.impl.AbstractSpringDependencyTest;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class TestKBBBookoutService extends AbstractSpringDependencyTest
{

public TestKBBBookoutService() {
//	this.mockBookoutService = (IThirdPartyBookoutService)this.applicationContext.getBean( "kbbMockService" );
//	this.mockBookoutService2 = (IThirdPartyBookoutService)this.applicationContext.getBean( "kbbDBService" );
}

protected IThirdPartyBookoutService mockKbbBookoutService;
protected KBBBookoutService mockKbbBookoutService2;

public void setMockKbbBookoutService( IThirdPartyBookoutService mockBookoutService ) {
	this.mockKbbBookoutService = mockBookoutService;
}

public void setMockKbbBookoutService2( KBBBookoutService mockBookoutService2 ) {
	this.mockKbbBookoutService2 = mockBookoutService2;
}

class VehicleKeyComparator implements Comparator<VDP_GuideBookVehicle> {

	public int compare(VDP_GuideBookVehicle o1, VDP_GuideBookVehicle o2) {
		return o1.getBody().compareTo(o2.getBody());
	}
	
}

// implementatinos of this method will return the full set of options for the vin: 1FTPW14556KB27090
//			and should not use the retrieve options method of the service being tested
public void testVinLookup() throws GBException
{
	// single Trim
	List<VDP_GuideBookVehicle> list1 = mockKbbBookoutService.doVinLookup( "IL", "1FMCU95H66KA20685", new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, 100147) ); 
	List<VDP_GuideBookVehicle> list2 = mockKbbBookoutService2.doVinLookup( "IL", "1FMCU95H66KA20685", new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, 100147) );

	assertEquals( "different number of trims returned!", list1.size(), list2.size() );
	assertEquals( "Vehicles don't match!", list1.get(0), list2.get( 0 ) );
	
	// multiple Trims
	list1 = mockKbbBookoutService.doVinLookup( "IL", "1FTPW14556KB27090", new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, 100147));
	list2 = mockKbbBookoutService2.doVinLookup( "IL", "1FTPW14556KB27090", new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, 100147));
	
	VehicleKeyComparator vkc = new VehicleKeyComparator();
	Collections.sort(list1, vkc);
	Collections.sort(list2, vkc);
	
	assertEquals( "different number of trims returned!", list1.size(), list2.size() );
	for (int c = 0; c < list1.size(); c++ )
	{
		assertEquals( "Vehicles don't match!", list1.get(c), list2.get( c ) );
	}
}

// not the best test ever - but since kbb db has more values we can't really handle the case where on service doesn't return the 
// full set of values (ie for example if a service fails to return a mileage adjustment value but returns the rest - this test wouldn't catch it)
// it needs to be this way for now - but we can get rid of this type of check when we no longer need to compare the kbb dll to the db
protected boolean compareValues( VDP_GuideBookValue value1, List< VDP_GuideBookValue > list2 )
{
	for (VDP_GuideBookValue value2 : list2 )
	{
		if ( value1.getThirdPartyCategoryId() == value2.getThirdPartyCategoryId() && 
				value1.getValueType() == value2.getValueType() )
		{
			if ( value1.equals( value2 ) )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

// this vin was coming up with LS and Limited both being selected as default trims - these 2 options conflict
// DE 320
public void testDefaultOptionsConflictExample1() throws GBException
{
	GuideBookInput input = new GuideBookInput();
	input.setVin( "1D8HB58D94F136875" );
	input.setState( "IL" );
	VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
	vehicle.setMakeId( "DT" );
	vehicle.setModelId( "DW" );
	vehicle.setYear( "2004" );
	vehicle.setCondition( KBBConditionEnum.EXCELLENT );
	
	//
	List<ThirdPartyVehicleOption> options = mockKbbBookoutService2.retrieveOptions( input, vehicle, new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, 100147)); 
	for( ThirdPartyVehicleOption tpvo : options )
	{
		if ( "T0".equals( tpvo.getOptionKey() )) // "ST package" 
			assertFalse( "ST should not be selected", tpvo.isStatus() );
		if ( "T1".equals( tpvo.getOptionKey() )) // "SLT package" 
			assertFalse( "ST should not be selected", tpvo.isStatus() );
		if ( "T2".equals( tpvo.getOptionKey() )) // "LIMITED package" 
			assertTrue( "ST should not be selected", tpvo.isStatus() );
	}
}

public void testDefaultOptionsConflictExample2() throws GBException
{
	GuideBookInput input = new GuideBookInput();
	input.setVin( "1FMSU45P54EE06564" );
	input.setState( "IL" );
	VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
	vehicle.setMakeId( "FT" );
	vehicle.setModelId( "EX" );
	vehicle.setYear( "2004" );
	vehicle.setCondition( KBBConditionEnum.EXCELLENT );
	
	//
	List<ThirdPartyVehicleOption> options = mockKbbBookoutService2.retrieveOptions( input, vehicle, new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, 100147)); 
	for( ThirdPartyVehicleOption tpvo : options )
	{
		if ( "T0".equals( tpvo.getOptionKey() )) // "XLS package" 
			assertFalse( "XLS should not be selected", tpvo.isStatus() );
		if ( "T1".equals( tpvo.getOptionKey() )) // "XLT package" 
			assertFalse( "XLT should not be selected", tpvo.isStatus() );
		if ( "T2".equals( tpvo.getOptionKey() )) // "Eddie Bauer package" 
			assertTrue( "Eddie Bauer should be selected", tpvo.isStatus() );
		if ( "T3".equals( tpvo.getOptionKey() )) // "LIMITED package" 
			assertFalse( "LIMITED should not be selected", tpvo.isStatus() );
	}
}

}
