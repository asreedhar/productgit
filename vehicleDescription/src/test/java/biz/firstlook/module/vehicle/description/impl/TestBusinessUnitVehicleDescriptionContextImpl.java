package biz.firstlook.module.vehicle.description.impl;

import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.module.vehicle.description.MockDealer;

public class TestBusinessUnitVehicleDescriptionContextImpl extends AbstractSpringDependencyTest
{

private BusinessUnitVehicleDescriptionContextImpl context = null;
private MockDealer dealer;

public TestBusinessUnitVehicleDescriptionContextImpl()
{
	super();
	
}

@Override
protected void onSetUpBeforeTransaction() throws Exception
{
	super.onSetUpBeforeTransaction();
	dealer = new MockDealer(Integer.valueOf( 100147 ));
	DealerPreference dp = new DealerPreference();
	dp.setGuideBookId( VehicleDescriptionProviderEnum.KBB.getThirdPartyId() );
	dealer.setDealerPreference( dp );
	context = new BusinessUnitVehicleDescriptionContextImpl( dealer );
}

public void testCreateContextWithNullParam()
{
	boolean testPassed = false;
	try
	{
		context = new BusinessUnitVehicleDescriptionContextImpl( null );
	}
	catch ( NullPointerException npe )
	{
		testPassed = true;
	}

	if ( !testPassed )
		fail( "This test should've thrown an NullPointerException!" );
}

public void testGetNullProvider()
{
	// you can't add a null provider, as asserted from the test above
	VehicleDescriptionProvider provider = context.getDescriptionProvider( null );
	assertNull( provider );
	
	DealerPreference dp = new DealerPreference();
	dp.setGuideBookId( VehicleDescriptionProviderEnum.NADA.getThirdPartyId() );
	dealer.setDealerPreference( dp );
	provider = context.getDescriptionProvider( VehicleDescriptionProviderEnum.NADA );
	assertNull( provider ); //for now, since NADA has not been implemented.
}

public void testGetProviderByEnum() {
	VehicleDescriptionProvider provider = context.getDescriptionProvider( null );
	assertNull( provider );
}

}
