package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import biz.firstlook.module.bookout.IThirdPartyBookoutService;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;


public class MockKBBBookoutService implements IThirdPartyBookoutService
{

public List<VDP_GuideBookVehicle> doVinLookup( String region, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	List< VDP_GuideBookVehicle > toBeReturned = new ArrayList< VDP_GuideBookVehicle >();

	if ( vin.equals( "1FMCU95H66KA20685" ) )
	{
		VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
		vehicle.setBody( "Hybrid Sport Utility 4D" );
		vehicle.setDescription( "Escape Hybrid Sport Utility 4D" );
		vehicle.setKey( "531" );
		vehicle.setMake( "Ford" );
		vehicle.setMakeId( "15" );
		vehicle.setModel( "Escape" );
		vehicle.setModelId( "97" );
		vehicle.setSeries( "Escape Hybrid Sport Utility 4D" );
		vehicle.setTrim( "Hybrid Sport Utility 4D" );
		vehicle.setVin( "1FMCU95H66KA20685" );
		vehicle.setYear( "2006" );

		toBeReturned.add( vehicle );
	}
	else if ( "1FTPW14556KB27090".equals( vin ) )
	{
		VDP_GuideBookVehicle vehicle1 = new VDP_GuideBookVehicle();
		vehicle1.setBody( "SuperCrew 5 1/2'" );
		vehicle1.setDescription( "F150 SuperCrew 5 1/2'" );
		vehicle1.setKey( "556" );
		vehicle1.setMake( "Ford" );
		vehicle1.setMakeId( "15" );
		vehicle1.setModel( "F150" );
		vehicle1.setModelId( "99" );
		vehicle1.setSeries( "F150 SuperCrew 5 1/2'" );
		vehicle1.setTrim( "SuperCrew 5 1/2'" );
		vehicle1.setVin( "1FTPW14556KB27090" );
		vehicle1.setYear( "2006" );
		toBeReturned.add( vehicle1 );

		VDP_GuideBookVehicle vehicle2 = new VDP_GuideBookVehicle();
		vehicle2.setBody( "SuperCrew 6 1/2'" );
		vehicle2.setDescription( "F150 SuperCrew 6 1/2'" );
		vehicle2.setKey( "554" );
		vehicle2.setMake( "Ford" );
		vehicle2.setMakeId( "15" );
		vehicle2.setModel( "F150" );
		vehicle2.setModelId( "99" );
		vehicle2.setSeries( "F150 SuperCrew 6 1/2'" );
		vehicle2.setTrim( "SuperCrew 6 1/2'" );
		vehicle2.setVin( "1FTPW14556KB27090" );
		vehicle2.setYear( "2006" );
		toBeReturned.add( vehicle2 );

		VDP_GuideBookVehicle vehicle3 = new VDP_GuideBookVehicle();
		vehicle3.setBody( "King Ranch SuperCrew 5 1/2' 4D" );
		vehicle3.setDescription( "F150 King Ranch SuperCrew 5 1/2' 4D" );
		vehicle3.setKey( "548" );
		vehicle3.setMake( "Ford" );
		vehicle3.setMakeId( "15" );
		vehicle3.setModel( "F150" );
		vehicle3.setModelId( "99" );
		vehicle3.setSeries( "F150 King Ranch SuperCrew 5 1/2' 4D" );
		vehicle3.setTrim( "King Ranch SuperCrew 5 1/2' 4D" );
		vehicle3.setVin( "1FTPW14556KB27090" );
		vehicle3.setYear( "2006" );
		toBeReturned.add( vehicle3 );

		VDP_GuideBookVehicle vehicle4 = new VDP_GuideBookVehicle();
		vehicle4.setBody( "King Ranch SuperCrew 6 1/2' 4D" );
		vehicle4.setDescription( "F150 King Ranch SuperCrew 6 1/2' 4D" );
		vehicle4.setKey( "20119" );
		vehicle4.setMake( "Ford" );
		vehicle4.setMakeId( "15" );
		vehicle4.setModel( "F150" );
		vehicle4.setModelId( "99" );
		vehicle4.setSeries( "F150 King Ranch SuperCrew 6 1/2' 4D" );
		vehicle4.setTrim( "King Ranch SuperCrew 6 1/2' 4D" );
		vehicle4.setVin( "1FTPW14556KB27090" );
		vehicle4.setYear( "2006" );
		toBeReturned.add( vehicle4 );
	}

	return toBeReturned;
}

public GuideBookMetaInfo getMetaInfo( String region, BookOutDatasetInfo bookoutDataset ) throws GBException
{
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, 
								VDP_GuideBookVehicle vehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	List< VDP_GuideBookValue > values = new ArrayList< VDP_GuideBookValue >();
	if ( "singleConflict".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value = new VDP_GuideBookValue();
		BookOutError error = new BookOutError();
		error.setFirstOptionDescription( "Single Compact Disc" );
		error.setSecondOptionDescription( "Multi Compact Disc" );
		error.setErrorType( 3 );
		value.setError( error );
		value.setHasErrors( true );
		values.add( value );
	}
	else if ( "multipleConflict".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		BookOutError error1 = new BookOutError();
		error1.setFirstOptionDescription( "Single Compact Disc" );
		error1.setSecondOptionDescription( "Multi Compact Disc" );
		error1.setErrorType( 3 );
		value1.setError( error1 );
		value1.setHasErrors( true );
		values.add( value1 );
		
		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		BookOutError error2 = new BookOutError();
		error2.setFirstOptionDescription( "Single Compact Disc" );
		error2.setSecondOptionDescription( "MP3 (Single CD)" );
		error2.setErrorType( 3 );
		value2.setError( error2 );
		value2.setHasErrors( true );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		BookOutError error3 = new BookOutError();
		error3.setFirstOptionDescription( "Multi Compact Disc" );
		error3.setSecondOptionDescription( "MP3 (Single CD)" );
		error3.setErrorType( 3 );
		value3.setError( error3 );
		value3.setHasErrors( true );
		values.add( value3 );

		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		BookOutError error4 = new BookOutError();
		error4.setFirstOptionDescription( "Custom Paint"  );
		error4.setSecondOptionDescription( "Two-Tone Paint" );
		error4.setErrorType( 3 );
		value4.setError( error4 );
		value4.setHasErrors( true );
		values.add( value4 );

		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		BookOutError error5 = new BookOutError();
		error5.setFirstOptionDescription( "Flip-Up Roof" );
		error5.setSecondOptionDescription( "Moon Roof" );
		error5.setErrorType( 3 );
		value5.setError( error5 );
		value5.setHasErrors( true );
		values.add( value5 );

		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		BookOutError error6 = new BookOutError();
		error6.setFirstOptionDescription( "Flip-Up Roof" );
		error6.setSecondOptionDescription( "Sun Roof" );
		error6.setErrorType( 3 );
		value6.setError( error6 );
		value6.setHasErrors( true );
		values.add( value6 );

		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		BookOutError error7 = new BookOutError();
		error7.setFirstOptionDescription( "Sun Roof" );
		error7.setSecondOptionDescription( "Moon Roof" );
		error7.setErrorType( 3 );
		value7.setError( error7 );
		value7.setHasErrors( true );
		values.add( value7 );

	}
	else if ( "dependencyConflict".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value = new VDP_GuideBookValue();
		BookOutError error = new BookOutError();
		error.setFirstOptionDescription( "Cassette" );
		error.setSecondOptionDescription( "AM/FM Stereo" );
		error.setErrorType( 3 );
		value.setError( error );
		value.setHasErrors( true );
		values.add( value );
	}
	else if ( "mutexConflict".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value = new VDP_GuideBookValue();
		BookOutError error = new BookOutError();
		error.setFirstOptionDescription( "Cassette" );
		error.setSecondOptionDescription( "AM/FM Stereo" );
		error.setErrorType( 3 );
		value.setError( error );
		value.setHasErrors( true );
		values.add( value );
	}
	else if ( "valuesForDefaultOptions".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value0 = new VDP_GuideBookValue();
		value0.setHasErrors( false );
		value0.setThirdPartyCategoryId( 0 );
		value0.setValueType( 50 ); // mileage adjustment
		value0.setValue( 0 );
		values.add( value0 );
		
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		value1.setHasErrors( false );
		value1.setThirdPartyCategoryId( 9 );
		value1.setValueType( 4 ); // options value
		value1.setValue( 6065 );
		values.add( value1 );

		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		value2.setHasErrors( false );
		value2.setThirdPartyCategoryId( 9 );
		value2.setValueType( 1 );
		value2.setValue( 23700 );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		value3.setHasErrors( false );
		value3.setThirdPartyCategoryId( 11 );
		value3.setValueType( 1 );
		value3.setValue( 17200 );
		values.add( value3 );

		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		value4.setHasErrors( false );
		value4.setThirdPartyCategoryId( 8 );
		value4.setValueType( 1 );
		value4.setValue( 20600 );
		values.add( value4 );

		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		value5.setHasErrors( false );
		value5.setThirdPartyCategoryId( 9 );
		value5.setValueType( 3 );
		value5.setValue( 29765 );
		values.add( value5 );
		
		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		value6.setHasErrors( false );
		value6.setThirdPartyCategoryId( 11 );
		value6.setValueType( 3 );
		value6.setValue( 21750 );
		values.add( value6 );
		
		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		value7.setHasErrors( false );
		value7.setThirdPartyCategoryId( 8 );
		value7.setValueType( 3 );
		value7.setValue( 25150 );
		values.add( value7 );
		
		VDP_GuideBookValue value8 = new VDP_GuideBookValue();
		value8.setHasErrors( false );
		value8.setThirdPartyCategoryId( 9 );
		value8.setValueType( 2 );
		value8.setValue( 29765 );
		values.add( value2 );

		VDP_GuideBookValue value9 = new VDP_GuideBookValue();
		value9.setHasErrors( false );
		value9.setThirdPartyCategoryId( 11 );
		value9.setValueType( 2 ); // mileage adjustment
		value9.setValue( 21750 );
		values.add( value9 );

		VDP_GuideBookValue value10 = new VDP_GuideBookValue();
		value10.setHasErrors( false );
		value10.setThirdPartyCategoryId( 8 );
		value10.setValueType( 2 );
		value10.setValue( 25150 );
		values.add( value10 );
	}
	else if ( "valuesForLotsOfOptions".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value0 = new VDP_GuideBookValue();
		value0.setHasErrors( false );
		value0.setThirdPartyCategoryId( 0 );
		value0.setValueType( 50 ); // mileage adjustment
		value0.setValue( 0 );
		values.add( value0 );
		
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		value1.setHasErrors( false );
		value1.setThirdPartyCategoryId( 9 );
		value1.setValueType( 4 ); // options value
		value1.setValue( 17205 );
		values.add( value1 );

		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		value2.setHasErrors( false );
		value2.setThirdPartyCategoryId( 9 );
		value2.setValueType( 1 );
		value2.setValue( 23700 );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		value3.setHasErrors( false );
		value3.setThirdPartyCategoryId( 11 );
		value3.setValueType( 1 );
		value3.setValue( 17200 );
		values.add( value3 );

		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		value4.setHasErrors( false );
		value4.setThirdPartyCategoryId( 8 );
		value4.setValueType( 1 );
		value4.setValue( 20600 );
		values.add( value4 );

		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		value5.setHasErrors( false );
		value5.setThirdPartyCategoryId( 9 );
		value5.setValueType( 3 );
		value5.setValue( 40905 );
		values.add( value5 );
		
		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		value6.setHasErrors( false );
		value6.setThirdPartyCategoryId( 11 );
		value6.setValueType( 3 );
		value6.setValue( 30100 );
		values.add( value6 );
		
		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		value7.setHasErrors( false );
		value7.setThirdPartyCategoryId( 8 );
		value7.setValueType( 3 );
		value7.setValue( 33500 );
		values.add( value7 );
		
		VDP_GuideBookValue value8 = new VDP_GuideBookValue();
		value8.setHasErrors( false );
		value8.setThirdPartyCategoryId( 9 );
		value8.setValueType( 2 );
		value8.setValue( 40905 );
		values.add( value2 );

		VDP_GuideBookValue value9 = new VDP_GuideBookValue();
		value9.setHasErrors( false );
		value9.setThirdPartyCategoryId( 11 );
		value9.setValueType( 2 ); // mileage adjustment
		value9.setValue( 30100 );
		values.add( value9 );

		VDP_GuideBookValue value10 = new VDP_GuideBookValue();
		value10.setHasErrors( false );
		value10.setThirdPartyCategoryId( 8 );
		value10.setValueType( 2 );
		value10.setValue( 33500 );
		values.add( value10 );
	}
	else if ( "defaultOptionsLowMileage".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value0 = new VDP_GuideBookValue();
		value0.setHasErrors( false );
		value0.setThirdPartyCategoryId( 0 );
		value0.setValueType( 50 ); // mileage adjustment
		value0.setValue( 1600 );
		values.add( value0 );
		
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		value1.setHasErrors( false );
		value1.setThirdPartyCategoryId( 9 );
		value1.setValueType( 4 ); // options value
		value1.setValue( 6065 );
		values.add( value1 );

		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		value2.setHasErrors( false );
		value2.setThirdPartyCategoryId( 9 );
		value2.setValueType( 1 );
		value2.setValue( 23700 );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		value3.setHasErrors( false );
		value3.setThirdPartyCategoryId( 11 );
		value3.setValueType( 1 );
		value3.setValue( 17200 );
		values.add( value3 );

		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		value4.setHasErrors( false );
		value4.setThirdPartyCategoryId( 8 );
		value4.setValueType( 1 );
		value4.setValue( 20600 );
		values.add( value4 );

		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		value5.setHasErrors( false );
		value5.setThirdPartyCategoryId( 9 );
		value5.setValueType( 3 );
		value5.setValue( 29765 );
		values.add( value5 );
		
		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		value6.setHasErrors( false );
		value6.setThirdPartyCategoryId( 11 );
		value6.setValueType( 3 );
		value6.setValue( 21750 );
		values.add( value6 );
		
		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		value7.setHasErrors( false );
		value7.setThirdPartyCategoryId( 8 );
		value7.setValueType( 3 );
		value7.setValue( 25150 );
		values.add( value7 );
		
		VDP_GuideBookValue value8 = new VDP_GuideBookValue();
		value8.setHasErrors( false );
		value8.setThirdPartyCategoryId( 9 );
		value8.setValueType( 2 );
		value8.setValue( 31365 );
		values.add( value2 );

		VDP_GuideBookValue value9 = new VDP_GuideBookValue();
		value9.setHasErrors( false );
		value9.setThirdPartyCategoryId( 11 );
		value9.setValueType( 2 ); // mileage adjustment
		value9.setValue( 23350 );
		values.add( value9 );

		VDP_GuideBookValue value10 = new VDP_GuideBookValue();
		value10.setHasErrors( false );
		value10.setThirdPartyCategoryId( 8 );
		value10.setValueType( 2 );
		value10.setValue( 26750 );
		values.add( value10 );
	}
	else if ( "defaultOptionsHighMileage".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value0 = new VDP_GuideBookValue();
		value0.setHasErrors( false );
		value0.setThirdPartyCategoryId( 0 );
		value0.setValueType( 50 ); // mileage adjustment
		value0.setValue( -5325 );
		values.add( value0 );
		
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		value1.setHasErrors( false );
		value1.setThirdPartyCategoryId( 9 );
		value1.setValueType( 4 ); // options value
		value1.setValue( 6065 );
		values.add( value1 );

		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		value2.setHasErrors( false );
		value2.setThirdPartyCategoryId( 9 );
		value2.setValueType( 1 );
		value2.setValue( 23700 );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		value3.setHasErrors( false );
		value3.setThirdPartyCategoryId( 11 );
		value3.setValueType( 1 );
		value3.setValue( 17200 );
		values.add( value3 );

		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		value4.setHasErrors( false );
		value4.setThirdPartyCategoryId( 8 );
		value4.setValueType( 1 );
		value4.setValue( 20600 );
		values.add( value4 );

		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		value5.setHasErrors( false );
		value5.setThirdPartyCategoryId( 9 );
		value5.setValueType( 3 );
		value5.setValue( 29765 );
		values.add( value5 );
		
		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		value6.setHasErrors( false );
		value6.setThirdPartyCategoryId( 11 );
		value6.setValueType( 3 );
		value6.setValue( 21750 );
		values.add( value6 );
		
		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		value7.setHasErrors( false );
		value7.setThirdPartyCategoryId( 8 );
		value7.setValueType( 3 );
		value7.setValue( 25150 );
		values.add( value7 );
		
		VDP_GuideBookValue value8 = new VDP_GuideBookValue();
		value8.setHasErrors( false );
		value8.setThirdPartyCategoryId( 9 );
		value8.setValueType( 2 );
		value8.setValue( 24440 );
		values.add( value2 );

		VDP_GuideBookValue value9 = new VDP_GuideBookValue();
		value9.setHasErrors( false );
		value9.setThirdPartyCategoryId( 11 );
		value9.setValueType( 2 ); // mileage adjustment
		value9.setValue( 16425 );
		values.add( value9 );

		VDP_GuideBookValue value10 = new VDP_GuideBookValue();
		value10.setHasErrors( false );
		value10.setThirdPartyCategoryId( 8 );
		value10.setValueType( 2 );
		value10.setValue( 19825 );
		values.add( value10 );
	}
	else if ( "defaultOptionsFairCondition".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value0 = new VDP_GuideBookValue();
		value0.setHasErrors( false );
		value0.setThirdPartyCategoryId( 0 );
		value0.setValueType( 50 ); // mileage adjustment
		value0.setValue( 0 );
		values.add( value0 );
		
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		value1.setHasErrors( false );
		value1.setThirdPartyCategoryId( 11 );
		value1.setValueType( 4 ); // options value
		value1.setValue( 3910 );
		values.add( value1 );

		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		value2.setHasErrors( false );
		value2.setThirdPartyCategoryId( 9 );
		value2.setValueType( 1 );
		value2.setValue( 23700 );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		value3.setHasErrors( false );
		value3.setThirdPartyCategoryId( 11 );
		value3.setValueType( 1 );
		value3.setValue( 14300 );
		values.add( value3 );

		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		value4.setHasErrors( false );
		value4.setThirdPartyCategoryId( 8 );
		value4.setValueType( 1 );
		value4.setValue( 20600 );
		values.add( value4 );

		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		value5.setHasErrors( false );
		value5.setThirdPartyCategoryId( 9 );
		value5.setValueType( 3 );
		value5.setValue( 29765 );
		values.add( value5 );
		
		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		value6.setHasErrors( false );
		value6.setThirdPartyCategoryId( 11 );
		value6.setValueType( 3 );
		value6.setValue( 18210 );
		values.add( value6 );
		
		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		value7.setHasErrors( false );
		value7.setThirdPartyCategoryId( 8 );
		value7.setValueType( 3 );
		value7.setValue( 25150 );
		values.add( value7 );
		
		VDP_GuideBookValue value8 = new VDP_GuideBookValue();
		value8.setHasErrors( false );
		value8.setThirdPartyCategoryId( 9 );
		value8.setValueType( 2 );
		value8.setValue( 29765 );
		values.add( value2 );

		VDP_GuideBookValue value9 = new VDP_GuideBookValue();
		value9.setHasErrors( false );
		value9.setThirdPartyCategoryId( 11 );
		value9.setValueType( 2 ); // mileage adjustment
		value9.setValue( 18210 );
		values.add( value9 );

		VDP_GuideBookValue value10 = new VDP_GuideBookValue();
		value10.setHasErrors( false );
		value10.setThirdPartyCategoryId( 8 );
		value10.setValueType( 2 );
		value10.setValue( 25150 );
		values.add( value10 );
	}
	else if ( "defaultOptionsGoodCondition".equals( vehicle.getVin() ) )
	{
		VDP_GuideBookValue value0 = new VDP_GuideBookValue();
		value0.setHasErrors( false );
		value0.setThirdPartyCategoryId( 0 );
		value0.setValueType( 50 ); // mileage adjustment
		value0.setValue( 0 );
		values.add( value0 );
		
		VDP_GuideBookValue value1 = new VDP_GuideBookValue();
		value1.setHasErrors( false );
		value1.setThirdPartyCategoryId( 9 );
		value1.setValueType( 4 ); // options value
		value1.setValue( 4330 );
		values.add( value1 );
		
		VDP_GuideBookValue value2 = new VDP_GuideBookValue();
		value2.setHasErrors( false );
		value2.setThirdPartyCategoryId( 9 );
		value2.setValueType( 1 );
		value2.setValue( 23700 );
		values.add( value2 );
		
		VDP_GuideBookValue value3 = new VDP_GuideBookValue();
		value3.setHasErrors( false );
		value3.setThirdPartyCategoryId( 11 );
		value3.setValueType( 1 );
		value3.setValue( 16150 );
		values.add( value3 );
		
		VDP_GuideBookValue value4 = new VDP_GuideBookValue();
		value4.setHasErrors( false );
		value4.setThirdPartyCategoryId( 8 );
		value4.setValueType( 1 );
		value4.setValue( 20600 );
		values.add( value4 );
		
		VDP_GuideBookValue value5 = new VDP_GuideBookValue();
		value5.setHasErrors( false );
		value5.setThirdPartyCategoryId( 9 );
		value5.setValueType( 3 );
		value5.setValue( 29765 );
		values.add( value5 );
		
		VDP_GuideBookValue value6 = new VDP_GuideBookValue();
		value6.setHasErrors( false );
		value6.setThirdPartyCategoryId( 11 );
		value6.setValueType( 3 );
		value6.setValue( 20480 );
		values.add( value6 );
		
		VDP_GuideBookValue value7 = new VDP_GuideBookValue();
		value7.setHasErrors( false );
		value7.setThirdPartyCategoryId( 8 );
		value7.setValueType( 3 );
		value7.setValue( 25150 );
		values.add( value7 );
		
		VDP_GuideBookValue value8 = new VDP_GuideBookValue();
		value8.setHasErrors( false );
		value8.setThirdPartyCategoryId( 9 );
		value8.setValueType( 2 );
		value8.setValue( 29765 );
		values.add( value2 );
		
		VDP_GuideBookValue value9 = new VDP_GuideBookValue();
		value9.setHasErrors( false );
		value9.setThirdPartyCategoryId( 11 );
		value9.setValueType( 2 ); // mileage adjustment
		value9.setValue( 20480 );
		values.add( value9 );
		
		VDP_GuideBookValue value10 = new VDP_GuideBookValue();
		value10.setHasErrors( false );
		value10.setThirdPartyCategoryId( 8 );
		value10.setValueType( 2 );
		value10.setValue( 25150 );
		values.add( value10 );
	}
	return values;
}

public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo )
		throws GBException
{
	return null;
}

public List<VDP_GuideBookVehicle> retrieveMakes( String year, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	// TODO Auto-generated method stub
	return null;
}

public List<ThirdPartyVehicleOption> retrieveOptions( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	List< ThirdPartyVehicleOption > toBeReturned = new ArrayList< ThirdPartyVehicleOption >();
	if ( "1FTPW14556KB27090".equals( input.getVin() ) )
	{
		toBeReturned = getOptions();
	}
	return toBeReturned;
}

public List<ThirdPartyVehicleOption> retrieveOptions( String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	// TODO Auto-generated method stub
	return null;
}

public String retrieveRegionDescription( GuideBookInput input ) throws GBException
{
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveTrims( String year, String makeId, String modelId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	// TODO Auto-generated method stub
	return null;
}

public static List< ThirdPartyVehicleOption > getOptions()
{
	List< ThirdPartyVehicleOption > toBeReturned = new ArrayList< ThirdPartyVehicleOption >();

	ThirdPartyVehicleOption option1 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("FX4", "P1", ThirdPartyOptionType.EQUIPMENT),
			false,
			0);
	option1.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 200 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 265 ) );
	option1.getOptionValues().add( new ThirdPartyVehicleOptionValue( option1, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 200 ) );
	option1.setStatus( false );

	ThirdPartyVehicleOption option2 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("XLt", "T1", ThirdPartyOptionType.EQUIPMENT),
			false,
			1);
	option2.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 725 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 965 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 725 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 690 ) );
	option2.getOptionValues().add( new ThirdPartyVehicleOptionValue( option2, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 625 ) );
	option2.setStatus( true );

	ThirdPartyVehicleOption option3 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Lariat", "T2", ThirdPartyOptionType.EQUIPMENT),
			false,
			2);
	option3.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 1025 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 1365 ) );
	option3.getOptionValues().add( new ThirdPartyVehicleOptionValue( option3, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 1025 ) );
	option3.setStatus( false );

	ThirdPartyVehicleOption option4 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Air Conditioning", "AC", ThirdPartyOptionType.EQUIPMENT),
			true,
			3);
	option4.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, -550 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, -735 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, -550 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, -525 ) );
	option4.getOptionValues().add( new ThirdPartyVehicleOptionValue( option4, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, -475 ) );
	option4.setStatus( true );

	ThirdPartyVehicleOption option5 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Power Steering", "PS", ThirdPartyOptionType.EQUIPMENT),
			true,
			4);
	option5.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, -300 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, -400 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, -300 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, -285 ) );
	option5.getOptionValues().add( new ThirdPartyVehicleOptionValue( option5, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, -260 ) );
	option5.setStatus( true );

	ThirdPartyVehicleOption option6 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Power Windows", "PW", ThirdPartyOptionType.EQUIPMENT),
			false,
			5);
	option6.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 250 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 335 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 250 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 240 ) );
	option6.getOptionValues().add( new ThirdPartyVehicleOptionValue( option6, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 215 ) );
	option6.setStatus( true );

	ThirdPartyVehicleOption option7 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Power Door Locks", "PS", ThirdPartyOptionType.EQUIPMENT),
			false,
			6);
	option7.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 175 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 235 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 175 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 165 ) );
	option7.getOptionValues().add( new ThirdPartyVehicleOptionValue( option7, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 150 ) );
	option7.setStatus( true );

	ThirdPartyVehicleOption option8 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Tilt Wheel", "TW", ThirdPartyOptionType.EQUIPMENT),
			false,
			7);
	option8.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option8.getOptionValues().add( new ThirdPartyVehicleOptionValue( option8, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 200 ) );
	option8.getOptionValues().add( new ThirdPartyVehicleOptionValue( option8, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 265 ) );
	option8.getOptionValues().add( new ThirdPartyVehicleOptionValue( option8, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 200 ) );
	option8.getOptionValues().add( new ThirdPartyVehicleOptionValue( option8, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 190 ) );
	option8.getOptionValues().add( new ThirdPartyVehicleOptionValue( option8, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 170 ) );
	option8.setStatus( true );

	ThirdPartyVehicleOption option9 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Cruise Control", "CC", ThirdPartyOptionType.EQUIPMENT),
			false,
			8);
	option9.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option9.getOptionValues().add( new ThirdPartyVehicleOptionValue( option9, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 150 ) );
	option9.getOptionValues().add( new ThirdPartyVehicleOptionValue( option9, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 200 ) );
	option9.getOptionValues().add( new ThirdPartyVehicleOptionValue( option9, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 150 ) );
	option9.getOptionValues().add( new ThirdPartyVehicleOptionValue( option9, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 145 ) );
	option9.getOptionValues().add( new ThirdPartyVehicleOptionValue( option9, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 130 ) );
	option9.setStatus( true );

	ThirdPartyVehicleOption option10 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("AM/FM Stereo", "RA", ThirdPartyOptionType.EQUIPMENT),
			true,
			9);
	option10.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option10.getOptionValues().add( new ThirdPartyVehicleOptionValue( option10, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, -100 ) );
	option10.getOptionValues().add( new ThirdPartyVehicleOptionValue( option10, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, -135 ) );
	option10.getOptionValues().add( new ThirdPartyVehicleOptionValue( option10, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, -100 ) );
	option10.getOptionValues().add( new ThirdPartyVehicleOptionValue( option10, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, -95 ) );
	option10.getOptionValues().add( new ThirdPartyVehicleOptionValue( option10, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, -85 ) );
	option10.setStatus( true );

	ThirdPartyVehicleOption option11 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Cassette", "RC", ThirdPartyOptionType.EQUIPMENT),
			false,
			10);
	option11.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option11.getOptionValues().add( new ThirdPartyVehicleOptionValue( option11, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 150 ) );
	option11.getOptionValues().add( new ThirdPartyVehicleOptionValue( option11, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 200 ) );
	option11.getOptionValues().add( new ThirdPartyVehicleOptionValue( option11, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 150 ) );
	option11.setStatus( false );

	ThirdPartyVehicleOption option12 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Single Compact Disc", "CD", ThirdPartyOptionType.EQUIPMENT),
			false,
			11);
	option12.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option12.getOptionValues().add( new ThirdPartyVehicleOptionValue( option12, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 200 ) );
	option12.getOptionValues().add( new ThirdPartyVehicleOptionValue( option12, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 265 ) );
	option12.getOptionValues().add( new ThirdPartyVehicleOptionValue( option12, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 200 ) );
	option12.getOptionValues().add( new ThirdPartyVehicleOptionValue( option12, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 190 ) );
	option12.getOptionValues().add( new ThirdPartyVehicleOptionValue( option12, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 170 ) );
	option12.setStatus( true );

	ThirdPartyVehicleOption option13 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Multi Compact Disc", "CH", ThirdPartyOptionType.EQUIPMENT),
			false,
			12);
	option13.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option13.getOptionValues().add( new ThirdPartyVehicleOptionValue( option13, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 300 ) );
	option13.getOptionValues().add( new ThirdPartyVehicleOptionValue( option13, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 400 ) );
	option13.getOptionValues().add( new ThirdPartyVehicleOptionValue( option13, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 300 ) );
	option13.setStatus( false );

	ThirdPartyVehicleOption option14 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("MP3 (Single CD)", "MP", ThirdPartyOptionType.EQUIPMENT),
			false,
			13);
	option14.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option14.getOptionValues().add( new ThirdPartyVehicleOptionValue( option13, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 300 ) );
	option14.getOptionValues().add( new ThirdPartyVehicleOptionValue( option13, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 400 ) );
	option14.getOptionValues().add( new ThirdPartyVehicleOptionValue( option13, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 300 ) );
	option14.setStatus( false );

	ThirdPartyVehicleOption option15 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("MP3 (Multi CD)", "MC", ThirdPartyOptionType.EQUIPMENT),
			false,
			14);
	option15.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option15.getOptionValues().add( new ThirdPartyVehicleOptionValue( option15, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 400 ) );
	option15.getOptionValues().add( new ThirdPartyVehicleOptionValue( option15, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 535 ) );
	option15.getOptionValues().add( new ThirdPartyVehicleOptionValue( option15, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 400 ) );
	option15.setSortOrder( 14 );
	option15.setStandardOption( false );
	option15.setStatus( false );

	ThirdPartyVehicleOption option16 = new ThirdPartyVehicleOption(
			new ThirdPartyOption("Premium Sound", "RP", ThirdPartyOptionType.EQUIPMENT),
			false,
			15);
	option16.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option16.getOptionValues().add( new ThirdPartyVehicleOptionValue( option16, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 225 ) );
	option16.getOptionValues().add( new ThirdPartyVehicleOptionValue( option16, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 300 ) );
	option16.getOptionValues().add( new ThirdPartyVehicleOptionValue( option16, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 225 ) );
	option16.setStatus( false );

	ThirdPartyVehicleOption option17 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Navigation System", "NS", ThirdPartyOptionType.EQUIPMENT),
		false,
		16);
	option17.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option17.getOptionValues().add( new ThirdPartyVehicleOptionValue( option17, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 575 ) );
	option17.getOptionValues().add( new ThirdPartyVehicleOptionValue( option17, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 765 ) );
	option17.getOptionValues().add( new ThirdPartyVehicleOptionValue( option17, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 575 ) );
	option17.setStatus( false );

	ThirdPartyVehicleOption option18 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Parking Sensors", "PA", ThirdPartyOptionType.EQUIPMENT),
		false,
		17);
	option18.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option18.getOptionValues().add( new ThirdPartyVehicleOptionValue( option18, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 175 ) );
	option18.getOptionValues().add( new ThirdPartyVehicleOptionValue( option18, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 235 ) );
	option18.getOptionValues().add( new ThirdPartyVehicleOptionValue( option18, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 175 ) );
	option18.setStatus( false );

	ThirdPartyVehicleOption option19 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("DVD System", "VP", ThirdPartyOptionType.EQUIPMENT),
		false,
		18);
	option19.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option19.getOptionValues().add( new ThirdPartyVehicleOptionValue( option19, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 375 ) );
	option19.getOptionValues().add( new ThirdPartyVehicleOptionValue( option19, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 500 ) );
	option19.getOptionValues().add( new ThirdPartyVehicleOptionValue( option19, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 375 ) );
	option19.setStatus( false );

	ThirdPartyVehicleOption option20 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Video System", "VS", ThirdPartyOptionType.EQUIPMENT),
		false,
		19);
	option20.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option20.getOptionValues().add( new ThirdPartyVehicleOptionValue( option20, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 375 ) );
	option20.getOptionValues().add( new ThirdPartyVehicleOptionValue( option20, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 500 ) );
	option20.getOptionValues().add( new ThirdPartyVehicleOptionValue( option20, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 375 ) );
	option20.setStatus( false );

	ThirdPartyVehicleOption option21 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Dual Front Air Bags", "DB", ThirdPartyOptionType.EQUIPMENT),
		true,
		20);
	option21.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option21.getOptionValues().add( new ThirdPartyVehicleOptionValue( option21, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 0 ) );
	option21.getOptionValues().add( new ThirdPartyVehicleOptionValue( option21, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 0 ) );
	option21.getOptionValues().add( new ThirdPartyVehicleOptionValue( option21, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 0 ) );
	option21.getOptionValues().add( new ThirdPartyVehicleOptionValue( option21, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 0 ) );
	option21.getOptionValues().add( new ThirdPartyVehicleOptionValue( option21, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 0 ) );
	option21.setStatus( true );

	ThirdPartyVehicleOption option22 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "ABS (4-Wheel)", "AB", ThirdPartyOptionType.EQUIPMENT),
		false,
		21);
	option22.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option22.getOptionValues().add( new ThirdPartyVehicleOptionValue( option22, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 200 ) );
	option22.getOptionValues().add( new ThirdPartyVehicleOptionValue( option22, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 265 ) );
	option22.getOptionValues().add( new ThirdPartyVehicleOptionValue( option22, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 200 ) );
	option22.getOptionValues().add( new ThirdPartyVehicleOptionValue( option22, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 190 ) );
	option22.getOptionValues().add( new ThirdPartyVehicleOptionValue( option22, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 170 ) );
	option22.setStatus( true );

	ThirdPartyVehicleOption option23 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Leather", "LR", ThirdPartyOptionType.EQUIPMENT),
		false,
		22);
	option23.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option23.getOptionValues().add( new ThirdPartyVehicleOptionValue( option23, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 375 ) );
	option23.getOptionValues().add( new ThirdPartyVehicleOptionValue( option23, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 500 ) );
	option23.getOptionValues().add( new ThirdPartyVehicleOptionValue( option23, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 375 ) );
	option23.setStatus( false );

	ThirdPartyVehicleOption option24 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Power Seat", "SA", ThirdPartyOptionType.EQUIPMENT),
		false,
		23);
	option24.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option24.getOptionValues().add( new ThirdPartyVehicleOptionValue( option24, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 125 ) );
	option24.getOptionValues().add( new ThirdPartyVehicleOptionValue( option24, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 165 ) );
	option24.getOptionValues().add( new ThirdPartyVehicleOptionValue( option24, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 125 ) );
	option24.setStatus( false );

	ThirdPartyVehicleOption option25 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Dual Power Seats", "SP", ThirdPartyOptionType.EQUIPMENT),
		false,
		24);
	option25.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option25.getOptionValues().add( new ThirdPartyVehicleOptionValue( option25, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 225 ) );
	option25.getOptionValues().add( new ThirdPartyVehicleOptionValue( option25, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 300 ) );
	option25.getOptionValues().add( new ThirdPartyVehicleOptionValue( option25, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 225 ) );
	option25.setStatus( false );

	ThirdPartyVehicleOption option26 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Flip-Up Roof", "SR", ThirdPartyOptionType.EQUIPMENT),
		false,
		25);
	option26.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option26.getOptionValues().add( new ThirdPartyVehicleOptionValue( option26, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 0 ) );
	option26.getOptionValues().add( new ThirdPartyVehicleOptionValue( option26, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 0 ) );
	option26.getOptionValues().add( new ThirdPartyVehicleOptionValue( option26, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 0 ) );
	option26.setStatus( false );

	ThirdPartyVehicleOption option27 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Sun Roof", "SS", ThirdPartyOptionType.EQUIPMENT),
		false,
		26);
	option27.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option27.getOptionValues().add( new ThirdPartyVehicleOptionValue( option27, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 575 ) );
	option27.getOptionValues().add( new ThirdPartyVehicleOptionValue( option27, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 765 ) );
	option27.getOptionValues().add( new ThirdPartyVehicleOptionValue( option27, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 575 ) );
	option27.setStatus( false );

	ThirdPartyVehicleOption option28 = new ThirdPartyVehicleOption(
		new ThirdPartyOption("Moon Roof", "MR", ThirdPartyOptionType.EQUIPMENT),
		false,
		27);
	option28.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option28.getOptionValues().add( new ThirdPartyVehicleOptionValue( option28, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 575 ) );
	option28.getOptionValues().add( new ThirdPartyVehicleOptionValue( option28, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 765 ) );
	option28.getOptionValues().add( new ThirdPartyVehicleOptionValue( option28, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 575 ) );
	option28.setStatus( false );

	ThirdPartyVehicleOption option29 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Roof Rack", "RR", ThirdPartyOptionType.EQUIPMENT),
		false,
		28);
	option29.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option29.getOptionValues().add( new ThirdPartyVehicleOptionValue( option29, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 100 ) );
	option29.getOptionValues().add( new ThirdPartyVehicleOptionValue( option29, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 135 ) );
	option29.getOptionValues().add( new ThirdPartyVehicleOptionValue( option29, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 100 ) );
	option29.setStatus( false );

	ThirdPartyVehicleOption option30 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Sliding Rear Window", "SW", ThirdPartyOptionType.EQUIPMENT),
		false,
		29);
	option30.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option30.getOptionValues().add( new ThirdPartyVehicleOptionValue( option30, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 75 ) );
	option30.getOptionValues().add( new ThirdPartyVehicleOptionValue( option30, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 100 ) );
	option30.getOptionValues().add( new ThirdPartyVehicleOptionValue( option30, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 75 ) );
	option30.setStatus( false );

	ThirdPartyVehicleOption option31 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Stepside Bed", "SH", ThirdPartyOptionType.EQUIPMENT),
		false,
		30);
	option31.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option31.getOptionValues().add( new ThirdPartyVehicleOptionValue( option31, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 375 ) );
	option31.getOptionValues().add( new ThirdPartyVehicleOptionValue( option31, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 500 ) );
	option31.getOptionValues().add( new ThirdPartyVehicleOptionValue( option31, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 375 ) );
	option31.setStatus( false );

	ThirdPartyVehicleOption option32 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Running Boards", "RU", ThirdPartyOptionType.EQUIPMENT),
		false,
		31);
	option32.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option32.getOptionValues().add( new ThirdPartyVehicleOptionValue( option32, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 200 ) );
	option32.getOptionValues().add( new ThirdPartyVehicleOptionValue( option32, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 265 ) );
	option32.getOptionValues().add( new ThirdPartyVehicleOptionValue( option32, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 200 ) );
	option32.setStatus( false );

	ThirdPartyVehicleOption option33 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Pickup Shell/Cap", "FS", ThirdPartyOptionType.EQUIPMENT),
		false,
		32);
	option33.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option33.getOptionValues().add( new ThirdPartyVehicleOptionValue( option33, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 275 ) );
	option33.getOptionValues().add( new ThirdPartyVehicleOptionValue( option33, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 365 ) );
	option33.getOptionValues().add( new ThirdPartyVehicleOptionValue( option33, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 275 ) );
	option33.setStatus( false );

	ThirdPartyVehicleOption option34 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Hard Tonneau Cover", "HT", ThirdPartyOptionType.EQUIPMENT),
		false,
		33);
	option34.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option34.getOptionValues().add( new ThirdPartyVehicleOptionValue( option34, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 225 ) );
	option34.getOptionValues().add( new ThirdPartyVehicleOptionValue( option34, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 300 ) );
	option34.getOptionValues().add( new ThirdPartyVehicleOptionValue( option34, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 225 ) );
	option34.setStatus( false );

	ThirdPartyVehicleOption option35 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Bed Liner", "BL", ThirdPartyOptionType.EQUIPMENT),
		false,
		34);
	option35.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option35.getOptionValues().add( new ThirdPartyVehicleOptionValue( option35, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 175 ) );
	option35.getOptionValues().add( new ThirdPartyVehicleOptionValue( option35, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 235 ) );
	option35.getOptionValues().add( new ThirdPartyVehicleOptionValue( option35, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 175 ) );
	option35.setStatus( false );

	ThirdPartyVehicleOption option36 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Custom Bumper", "CB", ThirdPartyOptionType.EQUIPMENT),
		false,
		35);
	option36.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option36.getOptionValues().add( new ThirdPartyVehicleOptionValue( option36, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 100 ) );
	option36.getOptionValues().add( new ThirdPartyVehicleOptionValue( option36, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 135 ) );
	option36.getOptionValues().add( new ThirdPartyVehicleOptionValue( option36, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 100 ) );
	option36.setStatus( false );

	ThirdPartyVehicleOption option37 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Grille Guard", "GG", ThirdPartyOptionType.EQUIPMENT),
		false,
		36);
	option37.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option37.getOptionValues().add( new ThirdPartyVehicleOptionValue( option37, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 75 ) );
	option37.getOptionValues().add( new ThirdPartyVehicleOptionValue( option37, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 100 ) );
	option37.getOptionValues().add( new ThirdPartyVehicleOptionValue( option37, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 75 ) );
	option37.setStatus( false );

	ThirdPartyVehicleOption option38 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Winch", "WI", ThirdPartyOptionType.EQUIPMENT),
		false,
		37);
	option38.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option38.getOptionValues().add( new ThirdPartyVehicleOptionValue( option38, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 275 ) );
	option38.getOptionValues().add( new ThirdPartyVehicleOptionValue( option38, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 365 ) );
	option38.getOptionValues().add( new ThirdPartyVehicleOptionValue( option38, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 275 ) );
	option38.setStatus( false );

	ThirdPartyVehicleOption option39 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Optional Fuel Tank", "FT", ThirdPartyOptionType.EQUIPMENT),
		false,
		38);
	option39.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option39.getOptionValues().add( new ThirdPartyVehicleOptionValue( option39, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 0 ) );
	option39.getOptionValues().add( new ThirdPartyVehicleOptionValue( option39, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 0 ) );
	option39.getOptionValues().add( new ThirdPartyVehicleOptionValue( option39, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 0 ) );
	option39.setStatus( false );

	ThirdPartyVehicleOption option40 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Towing Pkg", "TO", ThirdPartyOptionType.EQUIPMENT),
		false,
		39);
	option40.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option40.getOptionValues().add( new ThirdPartyVehicleOptionValue( option40, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 300 ) );
	option40.getOptionValues().add( new ThirdPartyVehicleOptionValue( option40, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 400 ) );
	option40.getOptionValues().add( new ThirdPartyVehicleOptionValue( option40, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 300 ) );
	option40.setStatus( false );

	ThirdPartyVehicleOption option41 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Snow Plow", "SO", ThirdPartyOptionType.EQUIPMENT),
		false,
		40);
	option41.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option41.getOptionValues().add( new ThirdPartyVehicleOptionValue( option41, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 1825 ) );
	option41.getOptionValues().add( new ThirdPartyVehicleOptionValue( option41, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 2435 ) );
	option41.getOptionValues().add( new ThirdPartyVehicleOptionValue( option41, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 1825 ) );
	option41.setStatus( false );

	ThirdPartyVehicleOption option42 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Custom Paint", "CP", ThirdPartyOptionType.EQUIPMENT),
		false,
		41);
	option42.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option42.getOptionValues().add( new ThirdPartyVehicleOptionValue( option42, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 125 ) );
	option42.getOptionValues().add( new ThirdPartyVehicleOptionValue( option42, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 165 ) );
	option42.getOptionValues().add( new ThirdPartyVehicleOptionValue( option42, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 125 ) );
	option42.setStatus( false );

	ThirdPartyVehicleOption option43 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Two-Tone Paint", "TT", ThirdPartyOptionType.EQUIPMENT),
		false,
		42);
	option43.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option43.getOptionValues().add( new ThirdPartyVehicleOptionValue( option43, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 125 ) );
	option43.getOptionValues().add( new ThirdPartyVehicleOptionValue( option43, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 165 ) );
	option43.getOptionValues().add( new ThirdPartyVehicleOptionValue( option43, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 125 ) );
	option43.setStatus( false );

	ThirdPartyVehicleOption option44 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Alloy Wheels", "AW", ThirdPartyOptionType.EQUIPMENT),
		false,
		43);
	option44.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option44.getOptionValues().add( new ThirdPartyVehicleOptionValue( option44, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 300 ) );
	option44.getOptionValues().add( new ThirdPartyVehicleOptionValue( option44, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 400 ) );
	option44.getOptionValues().add( new ThirdPartyVehicleOptionValue( option44, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 300 ) );
	option44.setStatus( false );

	ThirdPartyVehicleOption option45 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Premium Wheels", "CW", ThirdPartyOptionType.EQUIPMENT),
		false,
		44);
	option45.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option45.getOptionValues().add( new ThirdPartyVehicleOptionValue( option45, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 475 ) );
	option45.getOptionValues().add( new ThirdPartyVehicleOptionValue( option45, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 635 ) );
	option45.getOptionValues().add( new ThirdPartyVehicleOptionValue( option45, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 475 ) );
	option45.setStatus( false );

	ThirdPartyVehicleOption option46 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Wide Tires", "MW", ThirdPartyOptionType.EQUIPMENT),
		false,
		45);
	option46.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option46.getOptionValues().add( new ThirdPartyVehicleOptionValue( option46, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 175 ) );
	option46.getOptionValues().add( new ThirdPartyVehicleOptionValue( option46, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 235 ) );
	option46.getOptionValues().add( new ThirdPartyVehicleOptionValue( option46, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 175 ) );
	option46.setStatus( false );

	ThirdPartyVehicleOption option49 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Ovrsize Off-Rd Tires", "MX", ThirdPartyOptionType.EQUIPMENT),
		false,
		46);
	option49.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option49.getOptionValues().add( new ThirdPartyVehicleOptionValue( option49, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 175 ) );
	option49.getOptionValues().add( new ThirdPartyVehicleOptionValue( option49, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 235 ) );
	option49.getOptionValues().add( new ThirdPartyVehicleOptionValue( option49, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 175 ) );
	option49.setStatus( false );

	ThirdPartyVehicleOption option47 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Oversize Wheels 20\"+", "OW", ThirdPartyOptionType.EQUIPMENT),
		false,
		47);
	option47.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option47.getOptionValues().add( new ThirdPartyVehicleOptionValue( option47, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 850 ) );
	option47.getOptionValues().add( new ThirdPartyVehicleOptionValue( option47, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 1135 ) );
	option47.getOptionValues().add( new ThirdPartyVehicleOptionValue( option47, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 850 ) );
	option47.setStatus( false );

	ThirdPartyVehicleOption option48 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Dual Rear Wheels", "DP", ThirdPartyOptionType.EQUIPMENT),
		false,
		48);
	option48.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option48.getOptionValues().add( new ThirdPartyVehicleOptionValue( option48, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 1550 ) );
	option48.getOptionValues().add( new ThirdPartyVehicleOptionValue( option48, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 2065 ) );
	option48.getOptionValues().add( new ThirdPartyVehicleOptionValue( option48, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 1550 ) );
	option48.setStatus( false );

	ThirdPartyVehicleOption option50 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Utility", "UB", ThirdPartyOptionType.EQUIPMENT),
		false,
		49);
	option50.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option50.getOptionValues().add( new ThirdPartyVehicleOptionValue( option50, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 1400 ) );
	option50.getOptionValues().add( new ThirdPartyVehicleOptionValue( option50, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 1865 ) );
	option50.getOptionValues().add( new ThirdPartyVehicleOptionValue( option50, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 1400 ) );
	option50.setStatus( false );

	ThirdPartyVehicleOption option51 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Underbody Hoist", "UH", ThirdPartyOptionType.EQUIPMENT),
		false,
		50);
	option51.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option51.getOptionValues().add( new ThirdPartyVehicleOptionValue( option51, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 625 ) );
	option51.getOptionValues().add( new ThirdPartyVehicleOptionValue( option51, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 835 ) );
	option51.getOptionValues().add( new ThirdPartyVehicleOptionValue( option51, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 625 ) );
	option51.setStatus( false );

	ThirdPartyVehicleOption option52 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Dump Bed", "UI", ThirdPartyOptionType.EQUIPMENT),
		false,
		51);
	option52.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option52.getOptionValues().add( new ThirdPartyVehicleOptionValue( option52, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 625 ) );
	option52.getOptionValues().add( new ThirdPartyVehicleOptionValue( option52, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 835 ) );
	option52.getOptionValues().add( new ThirdPartyVehicleOptionValue( option52, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 625 ) );
	option52.setStatus( false );

	ThirdPartyVehicleOption option53 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Hydraulic Lift" , "HL", ThirdPartyOptionType.EQUIPMENT),
		false,
		52);
	option53.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option53.getOptionValues().add( new ThirdPartyVehicleOptionValue( option53, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 625 ) );
	option53.getOptionValues().add( new ThirdPartyVehicleOptionValue( option53, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 835 ) );
	option53.getOptionValues().add( new ThirdPartyVehicleOptionValue( option53, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 625 ) );
	option53.setStatus( false );

	ThirdPartyVehicleOption option54 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "V8 4.6 Liter", "8G", ThirdPartyOptionType.ENGINE),
		false,
		53);
	option54.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option54.getOptionValues().add( new ThirdPartyVehicleOptionValue( option54, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 0 ) );
	option54.getOptionValues().add( new ThirdPartyVehicleOptionValue( option54, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 0 ) );
	option54.getOptionValues().add( new ThirdPartyVehicleOptionValue( option54, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 0 ) );
	option54.setStatus( false );

	ThirdPartyVehicleOption option55 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "V8 5.4 Liter","E1" , ThirdPartyOptionType.ENGINE),
		false,
		54);
	option55.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option55.getOptionValues().add( new ThirdPartyVehicleOptionValue( option55, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 400 ) );
	option55.getOptionValues().add( new ThirdPartyVehicleOptionValue( option55, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 535 ) );
	option55.getOptionValues().add( new ThirdPartyVehicleOptionValue( option55, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 400 ) );
	option55.getOptionValues().add( new ThirdPartyVehicleOptionValue( option55, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 380 ) );
	option55.getOptionValues().add( new ThirdPartyVehicleOptionValue( option55, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 345 ) );
	option55.setStatus( true );

	ThirdPartyVehicleOption option56 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "V8 5.4L Flex Fuel", "F1", ThirdPartyOptionType.ENGINE),
		false,
		55);
	option56.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option56.getOptionValues().add( new ThirdPartyVehicleOptionValue( option56, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 400 ) );
	option56.getOptionValues().add( new ThirdPartyVehicleOptionValue( option56, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 535 ) );
	option56.getOptionValues().add( new ThirdPartyVehicleOptionValue( option56, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 400 ) );
	option56.setStatus( false );

	ThirdPartyVehicleOption option57 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "Automatic", "AT", ThirdPartyOptionType.TRANSMISSION),
		false,
		56);
	option57.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option57.getOptionValues().add( new ThirdPartyVehicleOptionValue( option57, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 0 ) );
	option57.getOptionValues().add( new ThirdPartyVehicleOptionValue( option57, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 0 ) );
	option57.getOptionValues().add( new ThirdPartyVehicleOptionValue( option57, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 0 ) );
	option57.getOptionValues().add( new ThirdPartyVehicleOptionValue( option57, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 0 ) );
	option57.getOptionValues().add( new ThirdPartyVehicleOptionValue( option57, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 0 ) );
	option57.setStatus( true );

	ThirdPartyVehicleOption option58 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "2WD", "2W", ThirdPartyOptionType.DRIVETRAIN),
		true,
		57);
	option58.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option58.getOptionValues().add( new ThirdPartyVehicleOptionValue( option58, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 0 ) );
	option58.getOptionValues().add( new ThirdPartyVehicleOptionValue( option58, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 0 ) );
	option58.getOptionValues().add( new ThirdPartyVehicleOptionValue( option58, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 0 ) );
	option58.setStatus( false );

	ThirdPartyVehicleOption option59 = new ThirdPartyVehicleOption(
		new ThirdPartyOption( "4WD", "4W", ThirdPartyOptionType.DRIVETRAIN),
		false,
		58);
	option59.setOptionValues( new HashSet< ThirdPartyVehicleOptionValue >() );
	option59.getOptionValues().add( new ThirdPartyVehicleOptionValue( option59, ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 2250 ) );
	option59.getOptionValues().add( new ThirdPartyVehicleOptionValue( option59, ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 3000 ) );
	option59.getOptionValues().add( new ThirdPartyVehicleOptionValue( option59, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT, 2250 ) );
	option59.getOptionValues().add( new ThirdPartyVehicleOptionValue( option59, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD, 2140 ) );
	option59.getOptionValues().add( new ThirdPartyVehicleOptionValue( option59, ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR, 1935 ) );
	option59.setStatus( true );

	toBeReturned.add( option1 );
	toBeReturned.add( option2 );
	toBeReturned.add( option3 );
	toBeReturned.add( option4 );
	toBeReturned.add( option5 );
	toBeReturned.add( option6 );
	toBeReturned.add( option7 );
	toBeReturned.add( option8 );
	toBeReturned.add( option9 );
	toBeReturned.add( option10 );
	toBeReturned.add( option11 );
	toBeReturned.add( option12 );
	toBeReturned.add( option13 );
	toBeReturned.add( option14 );
	toBeReturned.add( option15 );
	toBeReturned.add( option16 );
	toBeReturned.add( option17 );
	toBeReturned.add( option18 );
	toBeReturned.add( option19 );
	toBeReturned.add( option20 );
	toBeReturned.add( option21 );
	toBeReturned.add( option22 );
	toBeReturned.add( option23 );
	toBeReturned.add( option24 );
	toBeReturned.add( option25 );
	toBeReturned.add( option26 );
	toBeReturned.add( option27 );
	toBeReturned.add( option28 );
	toBeReturned.add( option29 );
	toBeReturned.add( option30 );
	toBeReturned.add( option31 );
	toBeReturned.add( option32 );
	toBeReturned.add( option33 );
	toBeReturned.add( option34 );
	toBeReturned.add( option35 );
	toBeReturned.add( option36 );
	toBeReturned.add( option37 );
	toBeReturned.add( option38 );
	toBeReturned.add( option39 );
	toBeReturned.add( option40 );
	toBeReturned.add( option41 );
	toBeReturned.add( option42 );
	toBeReturned.add( option43 );
	toBeReturned.add( option44 );
	toBeReturned.add( option45 );
	toBeReturned.add( option46 );
	toBeReturned.add( option49 );
	toBeReturned.add( option47 );
	toBeReturned.add( option48 );
	toBeReturned.add( option50 );
	toBeReturned.add( option51 );
	toBeReturned.add( option52 );
	toBeReturned.add( option53 );
	toBeReturned.add( option54 );
	toBeReturned.add( option55 );
	toBeReturned.add( option56 );
	toBeReturned.add( option57 );
	toBeReturned.add( option58 );
	toBeReturned.add( option59 );

	return toBeReturned;
}

public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo)
		throws GBException {
	int thisYear = Calendar.getInstance().get(Calendar.YEAR);
	int startYear = thisYear - 20; //kbb only has 20 years valid 
	List<Integer> years = new ArrayList<Integer>();
	for(int y = startYear; y <= thisYear; y++)
	{
		years.add(y);
	}
	
	return years;
}

public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear(String vin,
		Integer year, BookOutDatasetInfo bookoutInfo) throws GBException {
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId,
		String modelId, BookOutDatasetInfo bookoutInfo) throws GBException {
	// TODO Auto-generated method stub
	return null;
}


}
