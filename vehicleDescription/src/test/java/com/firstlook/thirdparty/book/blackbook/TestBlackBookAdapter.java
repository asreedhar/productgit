package com.firstlook.thirdparty.book.blackbook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.firstlook.thirdparty.book.blackbook.xml.BlackBookCar;

import junit.framework.TestCase;
import biz.firstlook.commons.util.IPropertyFinder;
import biz.firstlook.commons.util.MockPropertyFinder;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.commons.util.StringConstants;

public class TestBlackBookAdapter extends TestCase {

    private BlackBookAdapter blackBookAdapter;

public void setUp() {
	initBlackBookAdapterProperties();
	
	blackBookAdapter = new BlackBookAdapter();	
}

public static void initBlackBookAdapterProperties() {
	MockPropertyFinder propFinder = new MockPropertyFinder();
	propFinder.setStringProperty("firstlook.blackbook.port", "80");
	propFinder.setStringProperty("firstlook.blackbook.host",
		"www.blackbookportals.com");
	propFinder.setStringProperty("firstlook.blackbook.url",
		"/bb/products/usedcarxmlportal.asp");
	propFinder.setStringProperty("firstlook.blackbook.maxConnectionToHost",
		"2");
	propFinder.setStringProperty("firstlook.blackbook.maxTotalConnection",
		"20");
	propFinder.setStringProperty("firstlook.blackbook.socketTimeout",
		"60000");
	propFinder.setStringProperty("firstlook.blackbook.connectionTimeout",
		"120000");
	propFinder.setStringProperty(
		"firstlook.blackbook.idleConnectionTimeout", "100");
	ArrayList<IPropertyFinder> list = new ArrayList<IPropertyFinder>();
	list.add(propFinder);
	PropertyLoader.setPropertyFinders(list);
}

public static String getBlackBookResponse(String filename) {
	InputStream is = TestBlackBookAdapter.class.getResourceAsStream(filename);
	if (is == null)
	    fail();

	StringBuilder xmlb = new StringBuilder();
	BufferedReader r = null;
	try {
	    r = new BufferedReader(new InputStreamReader(is));
	    String line;
	    while ((line = r.readLine()) != null) {
		xmlb.append(line).append(StringConstants.NEW_LINE);
	    }
	    return xmlb.toString();
	} catch (FileNotFoundException e) {
	    fail();
	} catch (IOException e) {
	    fail();
	} finally {
	    if (r != null) {
			try {
			    r.close();
			} catch (IOException e) {
			    // do nothing
			}
	    }
	}
	
	return null;
}

public void testGenerateBlackBookCarsFromBlackBookXML() {
	String xml = getBlackBookResponse("xml/test-xml.xml");
	List<BlackBookCar> cars = blackBookAdapter.generateBlackBookCarsFromBlackBookXML(xml);
	
	assertEquals(2, cars.size());
	
	//1st car
	BlackBookCar car = cars.get(0);
	assertEquals(1999, car.getYear().intValue());
	assertEquals("Toyota", car.getMake());
	assertEquals("Camry", car.getModel());
	assertEquals("LE", car.getSeries());
	assertEquals("4D Sedan", car.getBody_style());
	
	//2nd car
	BlackBookCar car2 = cars.get(1);
	assertEquals(1999, car2.getYear().intValue());
	assertEquals("Toyota", car2.getMake());
	assertEquals("Camry", car2.getModel());
	assertEquals("XLE", car2.getSeries());
	assertEquals("4D Sedan", car2.getBody_style());
}
    
}
