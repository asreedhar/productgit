package com.firstlook.thirdparty.book.blackbook;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import biz.firstlook.transact.persist.model.BookOutValueType;

import com.firstlook.thirdparty.book.blackbook.xml.BlackBookCar;
import com.firstlook.thirdparty.book.blackbook.xml.Option;

import junit.framework.TestCase;

//The tests are in order of the name of the .png taken from black book for quick comparison
public class TestBlackBookPriceTables extends TestCase {

private BlackBookAdapter blackBookAdapter;

@Override
protected void setUp() throws Exception {
	TestBlackBookAdapter.initBlackBookAdapterProperties();
	blackBookAdapter = new BlackBookAdapter();
}

/**
 * It seems that this car on the blackbook website does not allow mileage adjustment values!  See PNG file in same location as xml.
 */
public void testFifteenYearOldPlusCar() {
	Set<String> optionNames = new HashSet<String>();
	validateWholesalePriceTable("1994 Honda Accord LX 4D Wagon.xml", 
			1, 1994, "Honda", "Accord", "LX", 
			"4D Wagon", 180000, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			2100,		1125,		550,  		2075		},	//Base 
	new Integer[]{	null,			0,			0,			0,			0			},	//Options
	new Integer[]{	null,			0,   		0,   		0,     		0			},	//Mileage
	new Integer[]{	null,			2100, 		1125,		550, 		2075		});	//Total
}

public void testFiftyPercentMileageRule() {
	Set<String> optionNames = new HashSet<String>();
	validateWholesalePriceTable("1996 Toyota Corolla DX 4D Sedan.xml", 
			1, 1996, "Toyota", "Corolla", "DX", 
			"4D Sedan", 350000, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			1850,		1200,		750,  		1825		},	//Base 
	new Integer[]{	null,			0,			0,			0,			0			},	//Options
	new Integer[]{	null,			-(1850/2), 	-(1200/2),  -(750/2),	-(1825/2)	},	//Mileage
	new Integer[]{	null,			925, 		600,		375, 		913			});	//Total
}

public void testFiftyPercentMileageRuleAgain() {
	Set<String> optionNames = new HashSet<String>();
	validateWholesalePriceTable("2006 Ford Explorer XLT 4D Utility 4WD.xml", 
			1, 2006, "Ford", "Explorer", "XLT", 
			"4D Utility 4WD", 130812, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			null,		10825,		8675,  		14075		},	//Base 
	new Integer[]{	null,			null,			0,			0,			0			},	//Options
	new Integer[]{	null,			null,	 -3625,  -3025,	-4300	},	//Mileage
	new Integer[]{	null,			null, 		7200,		5650, 		9775			});	//Total
}

public void testZeroMileage() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Conversion w/o Raised Top");
	optionNames.add("Conversion w/Raised Top");
	validateWholesalePriceTable("2002 Chevrolet Astro Vans Base Ext Cargo Van.xml", 
			1, 2002, "Chevrolet", "Astro Vans", "Base", 
			"Ext Cargo Van", 0, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			5025,		3075,		1725,  		4950		},	//Base 
	new Integer[]{	null,			4100,		3100,		1850,		3100		},	//Options
	new Integer[]{	null,			0,   		0,   		0,     		0			},	//Mileage
	new Integer[]{	null,			9125, 		6175,		3575, 		8050		});	//Total
}

public void testOver6YearOldConversionVanRrRgOptions() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Conversion w/o Raised Top");
	optionNames.add("Conversion w/Raised Top");
	validateWholesalePriceTable("2002 Chevrolet Astro Vans Base Ext Cargo Van.xml", 
			1, 2002, "Chevrolet", "Astro Vans", "Base", 
			"Ext Cargo Van", 12345, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			5025,		3075,		1725,  		4950		},	//Base 
	new Integer[]{	null,			4100,		3100,		1850,		3100		},	//Options
	new Integer[]{	null,			600,   		700,   		0,     		600			},	//Mileage
	new Integer[]{	null,			9725, 		6875,		3575, 		8650		});	//Total
}

//Used Car XML Portal User Documentation page 7 of 29, the else block.
public void testSpecialModelAddDeducts() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Dual Factory Air");
	optionNames.add("Leather");
	optionNames.add("Traveler Package");
	validateWholesalePriceTable("2002 Ford E150 Vans XLT Club Wagon.xml", 
			1, 2002, "Ford", "E150 Vans", "XLT", 
			"Club Wagon", 12345, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			5525,		3675,		2050,  		5500		},	//Base 
	new Integer[]{	null,			850,		850,		850,		850			},	//Options
	new Integer[]{	null,			650,   		750,   		0,     		650			},	//Mileage
	new Integer[]{	null,			7025, 		5275,		2900, 		7000		});	//Total
}

public void testSpecialModelAddDeducts2() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Conversion w/o Raised Top");
	optionNames.add("Conversion w/Raised Top");
	validateWholesalePriceTable("2002 Ford E250 Vans Econoline Cargo Van.xml", 
			1, 2002, "Ford", "E250 Vans", "Econoline", 
			"Cargo Van", 12345, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			5425,		3675,		2200,  		5400		},	//Base 
	new Integer[]{	null,			4100,		3100,		1850,		3100		},	//Options
	new Integer[]{	null,			650,   		750,   		0,     		650			},	//Mileage
	new Integer[]{	null,			10175, 		7525,		4050, 		9150		});	//Total
}

public void testFiftyPercentOptionAdjust() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Hook Type Wrecker");
	optionNames.add("Rollback Wrecker");
	validateWholesalePriceTable("2003 Ford F450SD XL CabChas 4WD DRW.xml", 
			2, 2003, "Ford", "F450SD", "XL", 
			"Cab/Chas 4WD DRW", 2, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			11525,		9225,		7350,  		11475		},	//Base 
	new Integer[]{	null,			(11525/2),	(9225/2),	(7350/2),	(11475/2)	}, 	//Options
	new Integer[]{	null,			725,   		850,   		0,     		725			}, 	//Mileage
	new Integer[]{	null,			18012, 		14687,		11025, 		17937		});	//Total
}

public void testUnder6YearOldConversionVanRrRgOptions() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Conversion w/o Raised Top");
	optionNames.add("Conversion w/Raised Top");
	validateWholesalePriceTable("2004 Chevrolet Astro Vans Base Ext Cargo Van.xml", 
			1, 2004, "Chevrolet", "Astro Vans", "Base", 
			"Ext Cargo Van", 12345, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	8400,			7500,		5650,		4075,  		7875		},	//Base 
	new Integer[]{	7300,			6500,		5500,		4300,		5500		}, 	//Options
	new Integer[]{	375,			475,   		575,   		0,     		425			}, 	//Mileage
	new Integer[]{	16075,			14475, 		11725,		8375, 		13800		});	//Total
}

public void testUnder6YearOldConversionVanRrRgOptionsNumberedModel() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Conversion w/o Raised Top");
	optionNames.add("Conversion w/Raised Top");
	validateWholesalePriceTable("2007 Ford E150 Vans Econoline Cargo Van.xml", 
			1, 2007, "Ford", "E150 Vans", "Econoline", 
			"Cargo Van", 12345, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	12400,			11650,		10000,		7850,  		12150		},	//Base 
	new Integer[]{	10200,			9400,		8200,		6800,		8200		}, 	//Options
	new Integer[]{	225,			325,   		425,   		0,     		250			}, 	//Mileage
	new Integer[]{	22825,			21375, 		18625,		14650, 		20600		});	//Total
}

public void testExcessiveMileage() {
	Set<String> optionNames = new HashSet<String>();
	validateWholesalePriceTable("2008 Honda Accord EX 4D Sedan.xml", 
			1, 2008, "Honda", "Accord", "EX", 
			"4D Sedan", 93883, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	null,			null,		18550,		16800,  	20725		},	//Base 
	new Integer[]{	null,			null,		0,			0,			0			}, 	//Options
	new Integer[]{	null,			null,   	-3300,   	-2850,     	-6200		}, 	//Mileage
	new Integer[]{	null,			null, 		15250,		13950, 		14525		});	//Total
}

public void testZeroBaseValue() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Navigation System");
	validateWholesalePriceTable("2008 Saturn VUE Hybrid 4D Utility FWD.xml", 
			1, 2008, "Saturn", "VUE", "Hybrid", 
			"4D Utility FWD", 9387, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	21075,			19675,		null,		null,	  	20650		},	//Base 
	new Integer[]{	700,			700,		null,		null,		700			}, 	//Options
	new Integer[]{	0,				150,   		null,   	null,     	50			}, 	//Mileage
	new Integer[]{	21775,			20525, 		null,		null, 		21400		});	//Total
}

//This test is due to the fact that the BlackBook documentation pre May 2010 had an ambiguity between which Add/Deduct needed to be treated specially.
//pre May 2010: RR and RG options or RR and RS options are treated separately.
//post May 2010: RRa and RG options are treated specially, but NOT RS!
//FogBugz bug 10742: https://incisent.fogbugz.com/default.asp?10742#75191
public void testUnder6YearOldRSOption() {
	Set<String> optionNames = new HashSet<String>();
	optionNames.add("Leather");
	optionNames.add("Polished Aluminum Wheels");
	optionNames.add("Power Sunroof");
	optionNames.add("RS Package");
	validateWholesalePriceTable("2010 Chevrolet Camaro SS 2D Coupe.xml", 
			1, 2010, "Chevrolet", "Camaro", "SS", 
			"2D Coupe", 5000, 
			optionNames,
	//Wholesale		Extra Clean		Clean		Average		Rough		FinanceAdvance
	new Integer[]{	34100,			33250,		31450,		29550,	  	33725		},	//Base 
	new Integer[]{	2200,			2200,		2200,		2200,		2200		}, 	//Options
	new Integer[]{	0,				200,   		300,	   	0,    	 	100			}, 	//Mileage
	new Integer[]{	36300,			35650, 		33950,		31750, 		36025		});	//Total
}

private List<Option> selectOptions(BlackBookCar car, Set<String> optionNames) {
	List<Option> selectedOptions = new ArrayList<Option>();
	List<Option> carOptions = car.getOptions().getOption();
	for(Option option : carOptions) {
		String optionName = option.getDescription();
		if(optionNames.contains(optionName)) {
			selectedOptions.add(option);
		}
	}
	return selectedOptions;
}

/**
 * @param xmlfile
 * @param numBlackBookCars
 * @param modelYear
 * @param make
 * @param model
 * @param series
 * @param bodyStyle
 * @param mileage
 * @param optionNames
 * @param baseValues array of expected Wholesale base values: [%ExtraClean%, %Clean%, %Average%, %Rough%, %FinanceAdvance%]
 * @param optionAdjValues array of expected Wholesale option adjustments: [%ExtraClean%, %Clean%, %Average%, %Rough%, %FinanceAdvance%]
 * @param mileageAdjValues array of expected Wholesale mileage adjustments: [%ExtraClean%, %Clean%, %Average%, %Rough%, %FinanceAdvance%]
 * @param finalValues array of expected Wholesale final values: [%ExtraClean%, %Clean%, %Average%, %Rough%, %FinanceAdvance%]
 */
private void validateWholesalePriceTable(String xmlfile, int numBlackBookCars, int modelYear, String make, 
		String model, String series, String bodyStyle, Integer mileage, 
		Set<String> optionNames, Integer[] baseValues, Integer[] optionAdjValues, Integer[] mileageAdjValues, Integer[] finalValues) {
	
	String xml = TestBlackBookAdapter.getBlackBookResponse("xml/" + xmlfile);
	List<BlackBookCar> cars = blackBookAdapter.generateBlackBookCarsFromBlackBookXML(xml);
	
	assertEquals(numBlackBookCars, cars.size());
	
	BlackBookCar car = cars.get(0);
	
	assertEquals(modelYear, car.getYear().intValue());
	assertEquals(make, car.getMake());
	assertEquals(model, car.getModel());
	assertEquals(series, car.getSeries());
	assertEquals(bodyStyle, car.getBody_style());
	
	List<Option> selectedOptions = selectOptions(car, optionNames);
	
	BlackBookPriceTables table = new BlackBookPriceTables(car, selectedOptions, mileage);
	
	for(BlackBookCondition condition : BlackBookCondition.values()) {
		assertEquals(baseValues[condition.getOrder()],
				table.getValue(BlackBookMarketType.WHOLESALE, 
						condition, 
						BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
		
		assertEquals(optionAdjValues[condition.getOrder()],
				table.getValue(BlackBookMarketType.WHOLESALE, 
						condition, 
						BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));
		
		assertEquals(mileageAdjValues[condition.getOrder()],
				table.getValue(BlackBookMarketType.WHOLESALE, 
						condition, 
						BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT));
		
		assertEquals(finalValues[condition.getOrder()],
				table.getValue(BlackBookMarketType.WHOLESALE, 
						condition, 
						BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE));
	}
}

}
