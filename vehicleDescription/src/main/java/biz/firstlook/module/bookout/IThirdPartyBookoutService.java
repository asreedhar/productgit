package biz.firstlook.module.bookout;

import java.util.List;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public interface IThirdPartyBookoutService
{

public List<VDP_GuideBookVehicle> doVinLookup( String region, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException;

// Returns a list of ThirdPartyVehicleOptions from VIN!
public List<ThirdPartyVehicleOption> retrieveOptions( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo ) throws GBException;

// Returns a list of ThirdPartyVehicleOptions from make, model, trim ID's!
public List<ThirdPartyVehicleOption> retrieveOptions( String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo )
		throws GBException;

// Returns a list of VDP_GuideBookValues from VIN!
public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, VDP_GuideBookVehicle vehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException;

// Returns a list of VDP_GuideBookValues from make, model, trim ID's!
public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, String year, String makeId, String modelId,
								String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException;

public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo ) throws GBException;
public List< VDP_GuideBookVehicle > retrieveMakes( String year, BookOutDatasetInfo bookoutInfo ) throws GBException;
public List< VDP_GuideBookVehicle > retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo ) throws GBException;
public List< VDP_GuideBookVehicle > retrieveTrims( String year, String makeId, String modelId, BookOutDatasetInfo bookoutInfo ) throws GBException;

public GuideBookMetaInfo getMetaInfo( String region, BookOutDatasetInfo bookoutInfo ) throws GBException;
public String retrieveRegionDescription( GuideBookInput input ) throws GBException;
public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear( String vin,Integer year, BookOutDatasetInfo bookoutInfo ) throws GBException;
public List< VDP_GuideBookVehicle > retrieveTrimsWithoutYear( String makeId, String modelId, BookOutDatasetInfo bookoutInfo ) throws GBException;


}
