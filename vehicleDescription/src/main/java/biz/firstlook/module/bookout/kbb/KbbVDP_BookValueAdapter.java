package biz.firstlook.module.bookout.kbb;

import biz.firstlook.module.bookout.BookValue;
import biz.firstlook.module.vehicle.description.compatibility.BookValueAdapter;
import biz.firstlook.module.vehicle.description.old.KBBCategoryEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;

public class KbbVDP_BookValueAdapter implements BookValueAdapter<VDP_GuideBookValue> {

	public BookValue adaptItem(VDP_GuideBookValue oldItem) {
		ThirdPartySubCategoryEnum subCatEnum = ThirdPartySubCategoryEnum.getThirdPartySubCategoryById(oldItem.getThirdPartySubCategoryId()); 
		KBBCategoryEnum categoryEnum = KbbThirdPartyCategoryMapper.mapCategory( subCatEnum );
		KBBConditionEnum conditionEnum = KbbThirdPartyCategoryMapper.mapCondition(subCatEnum);
		return new KbbBookValue( oldItem.getValue(), categoryEnum, conditionEnum );
	}

	public VDP_GuideBookValue unadaptItem(BookValue newItem) {
		VDP_GuideBookValue oldValue = new VDP_GuideBookValue();
		oldValue.setValue(newItem.getValue());
		//throw new UnsupportedOperationException("this method not yet complete.");
		return oldValue;
	}

}
