package biz.firstlook.module.bookout.kbb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.collections.Filter;
import biz.firstlook.module.bookout.BookValue;
import biz.firstlook.module.bookout.BookValueContainer;
import biz.firstlook.module.vehicle.description.old.KBBCategoryEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbRegionalIdentifier;

public class KbbBookValueContainer implements BookValueContainer
{

private static final Logger logger = Logger.getLogger( KbbBookValueContainer.class );

private Collection< BookValue > kbbBookValues;
private KbbRegionalIdentifier kbbRegionalIdentifier;

public KbbBookValueContainer( Collection< BookValue > values, KbbRegionalIdentifier kbbRegionalIdentifier )
{
	this.kbbBookValues = values;
	this.kbbRegionalIdentifier = kbbRegionalIdentifier;
}

public Collection< BookValue > getBookValues()
{
	return kbbBookValues;
}

public KbbRegionalIdentifier getRegionalIdentifier() {
	return kbbRegionalIdentifier;
}

public KbbBookValue getValue( final KBBCategoryEnum type, final KBBConditionEnum condition )
{
	Collection< KbbBookValue > values = getValues( new KbbBookValueMarketTypeAndConditionFilter( type, condition ) );
	if ( values.isEmpty() )
	{
		return null;
	}
	else if ( values.size() > 1 )
	{
		logger.error( "More than one KbbBookValue was found for a MarketType and Condition: " + type + " " + condition );
		// should really throw an exception -bf.
		return null;
	}
	else
	{
		return values.iterator().next();
	}
}

public Collection< KbbBookValue > getValues( final KBBCategoryEnum type )
{
	return getValues( new KbbBookValueMarketTypeFilter( type ) );
}

public Collection< KbbBookValue > getValues( final KBBConditionEnum condition )
{
	return getValues( new KbbBookValueConditionFilter( condition ) );
}

/**
 * Single point of entry for looping over the collection of book values. This method makes a cast to check that the BookValues are indeed
 * KbbBookValues -bf.
 * 
 * @param filter
 * @return a Collection of KbbBookValue(s) of which had been accepted by the filter.
 */
private Collection< KbbBookValue > getValues( Filter< KbbBookValue > filter )
{
	List< KbbBookValue > values = new ArrayList< KbbBookValue >();
	for ( BookValue bv : kbbBookValues )
	{
		KbbBookValue castedBookValue = (KbbBookValue)bv;
		if ( filter.accept( castedBookValue ) )
			values.add( castedBookValue );
	}
	Collections.sort(values, new KbbBookValueConditionComparator());
	return values;
}

class KbbBookValueConditionComparator implements Comparator<KbbBookValue> {

	public int compare(KbbBookValue v1, KbbBookValue v2) {
		int v1Ordinal = v1.getCondition().ordinal();
		int v2Ordinal = v2.getCondition().ordinal();
		if( v1Ordinal < v2Ordinal )
			return -1;
		else if( v1Ordinal == v2Ordinal )
			return 0;
		else
			return 1;
	}
	
}

class KbbBookValueMarketTypeFilter implements Filter< KbbBookValue >
{
private KBBCategoryEnum marketType;

KbbBookValueMarketTypeFilter( KBBCategoryEnum marketType )
{
	this.marketType = marketType;
}

public boolean accept( KbbBookValue bookValue )
{
	return bookValue.getMarketType().equals( marketType );
}
}

class KbbBookValueConditionFilter implements Filter< KbbBookValue >
{
private KBBConditionEnum condition;

KbbBookValueConditionFilter( KBBConditionEnum condition )
{
	this.condition = condition;
}

public boolean accept( KbbBookValue bookValue )
{
	return bookValue.getCondition().equals( condition );
}
}

class KbbBookValueMarketTypeAndConditionFilter implements Filter< KbbBookValue >
{
KbbBookValueMarketTypeFilter marketFilter;
KbbBookValueConditionFilter conditionFilter;

KbbBookValueMarketTypeAndConditionFilter( KBBCategoryEnum marketType, KBBConditionEnum condition )
{
	this.marketFilter = new KbbBookValueMarketTypeFilter( marketType );
	this.conditionFilter = new KbbBookValueConditionFilter( condition );
}

public boolean accept( KbbBookValue bookValue )
{
	return marketFilter.accept( bookValue ) && conditionFilter.accept( bookValue );
}
}
}
