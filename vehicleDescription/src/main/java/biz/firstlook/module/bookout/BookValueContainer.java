package biz.firstlook.module.bookout;

import java.util.Collection;

public interface BookValueContainer {
	public Collection<BookValue> getBookValues();
}
