package biz.firstlook.module.bookout;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.ThirdPartyOptionService;


public abstract class AbstractBookoutService implements IThirdPartyBookoutService {

private ThirdPartyOptionService thirdPartyOptionService;

protected abstract List<ThirdPartyVehicleOption> retrieveOptionsInternal( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo ) throws GBException;

protected abstract List<ThirdPartyVehicleOption> retrieveOptionsInternal( String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException;

public final List<ThirdPartyVehicleOption> retrieveOptions( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	final List<ThirdPartyVehicleOption> tpvos = retrieveOptionsInternal(input, guideBookVehicle, bookoutInfo );
	synchThirdPartyOption(tpvos);
	return tpvos;
}

public final List<ThirdPartyVehicleOption> retrieveOptions( String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	final List<ThirdPartyVehicleOption> tpvos = retrieveOptionsInternal(regionCode, year, makeId, modelId, trimId, bookoutInfo);
	synchThirdPartyOption(tpvos);
	return tpvos;
}

private void synchThirdPartyOption(final List<ThirdPartyVehicleOption> tpvos) {
	List<ThirdPartyOption> tpos = new ArrayList<ThirdPartyOption>(tpvos.size());
	for( ThirdPartyVehicleOption tpvo : tpvos )
	{
		tpos.add(tpvo.getOption());
	}
	tpos = thirdPartyOptionService.getPersistedOption(tpos);
	for( ThirdPartyVehicleOption tpvo : tpvos )
	{
		ThirdPartyOption unsynchedOption = tpvo.getOption();
		for (ThirdPartyOption synchedOption : tpos) {
			if (unsynchedOption.getOptionKey().equalsIgnoreCase(synchedOption.getOptionKey())) 
			{
				tpvo.setOption(synchedOption);
				break;
			}
		}
	}
}

/**
 * Errors during book out are communicated via a VDP_GuideBookValue.
 * 
 * This is a poor way to do this and should be refactored with the great TA rewrite of 2006
 * 
 * @return
 */
protected static VDP_GuideBookValue createOptionConflictError( String exception, String conflict )
{
	VDP_GuideBookValue guideBookValue = new VDP_GuideBookValue( Integer.valueOf( 0 ), // value = 0 for error
																0, // error for BO Value Category type
																ThirdPartyCategory.getThirdPartyCategoryDescription( 0 ), // error
																0 );
	StringBuilder errorMessage = new StringBuilder();
	errorMessage.append("Options conflict: ");
	errorMessage.append(exception);
	errorMessage.append(" and ");
	errorMessage.append(conflict);
	
	// create bookout error to add to guidBookValue
	BookOutError bookOutError = new BookOutError();
	bookOutError.setErrorMessage(errorMessage.toString());
	bookOutError.setErrorType( BookOutError.OPTIONS_CONFLICT );
	bookOutError.setFirstOptionDescription( exception );
	bookOutError.setSecondOptionDescription( conflict );

	guideBookValue.setHasErrors( true );
	guideBookValue.setError( bookOutError );
	return guideBookValue;
}

protected ThirdPartyOptionService getThirdPartyOptionService() {
	return thirdPartyOptionService;
}

public final void setThirdPartyOptionService( ThirdPartyOptionService thirdPartyOptionService) {
	this.thirdPartyOptionService = thirdPartyOptionService;
}
	
}
