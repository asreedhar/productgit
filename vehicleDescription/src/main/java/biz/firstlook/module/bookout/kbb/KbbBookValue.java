package biz.firstlook.module.bookout.kbb;

import biz.firstlook.module.bookout.BookValue;
import biz.firstlook.module.vehicle.description.old.KBBCategoryEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;

public class KbbBookValue implements BookValue {

	private Integer value;
	private KBBCategoryEnum categoryEnum;
	private KBBConditionEnum conditionEnum;
	
	public KbbBookValue(Integer value) {
		super();
		this.value = value;
	}
	
	KbbBookValue(Integer value, KBBCategoryEnum categoryEnum, KBBConditionEnum conditionEnum) {
		super();
		this.value = value;
		this.categoryEnum = categoryEnum;
		this.conditionEnum = conditionEnum;
	}

	public Integer getValue() {
		return value;
	}
	
	public KBBCategoryEnum getMarketType(){
		return categoryEnum;
	}

	public KBBConditionEnum getCondition(){
		return conditionEnum;
	}
}

