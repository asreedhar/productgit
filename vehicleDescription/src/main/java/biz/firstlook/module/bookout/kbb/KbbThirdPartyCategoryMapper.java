package biz.firstlook.module.bookout.kbb;

import biz.firstlook.module.vehicle.description.old.KBBCategoryEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;

public class KbbThirdPartyCategoryMapper {
	
	public static KBBCategoryEnum mapCategory(ThirdPartySubCategoryEnum thirdPartySubCategory){
		KBBCategoryEnum category = null;
		switch(thirdPartySubCategory) {
		case KBB_PRIVATE_PARTY_EXCELLENT: //falls through
		case KBB_PRIVATE_PARTY_FAIR: 	//falls through
		case KBB_PRIVATE_PARTY_GOOD:
			category = KBBCategoryEnum.PRIVATE_PARTY;
			break;
		case KBB_TRADEIN_EXCELLENT: //falls through
		case KBB_TRADEIN_FAIR: 	//falls through
		case KBB_TRADEIN_GOOD:
			category = KBBCategoryEnum.TRADEIN;
			break;
		case KBB_RETAIL:
			category = KBBCategoryEnum.RETAIL;
			break;
		case KBB_WHOLESALE:
			category = KBBCategoryEnum.WHOLESALE;
			break;
		case KBB_TRADEIN_FAIR_RANGELOW: 
        case KBB_TRADEIN_GOOD_RANGELOW: 
        case KBB_TRADEIN_EXCELLENT_RANGELOW:
            category = KBBCategoryEnum.TRADEINRANGELOW;
            break;
        case KBB_TRADEIN_FAIR_RANGEHIGH: 
        case KBB_TRADEIN_GOOD_RANGEHIGH: 
        case KBB_TRADEIN_EXCELLENT_RANGEHIGH:
            category = KBBCategoryEnum.TRADEINRANGEHIGH;
            break;   
		default:
			//throw new Exception;
		}
		return category;
	}
	
	public static KBBConditionEnum mapCondition(ThirdPartySubCategoryEnum thirdPartySubCategory){
		KBBConditionEnum condition = null;
		switch(thirdPartySubCategory) {
		case KBB_PRIVATE_PARTY_EXCELLENT: //falls through
		case KBB_TRADEIN_EXCELLENT_RANGELOW:
		case KBB_TRADEIN_EXCELLENT_RANGEHIGH:
		case KBB_TRADEIN_EXCELLENT:
			condition = KBBConditionEnum.EXCELLENT;
			break;
		case KBB_PRIVATE_PARTY_FAIR: 	//falls through
	    case KBB_TRADEIN_FAIR_RANGELOW: //falls through
        case KBB_TRADEIN_FAIR_RANGEHIGH: //falls through
		case KBB_TRADEIN_FAIR:
			condition = KBBConditionEnum.FAIR;
			break;
		case KBB_PRIVATE_PARTY_GOOD:
		case KBB_TRADEIN_GOOD_RANGELOW: //falls through
        case KBB_TRADEIN_GOOD_RANGEHIGH: //falls through
		case KBB_TRADEIN_GOOD:
			condition = KBBConditionEnum.GOOD;
			break;
		case KBB_RETAIL:
			condition = KBBConditionEnum.N_A;
			break;
		case KBB_WHOLESALE:
			condition = KBBConditionEnum.N_A;
			break;
		default:
			//throw new Exception;
		}
		return condition;

	}
}
