package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.log4j.Logger;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.module.bookout.AbstractBookoutService;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

public class KBBBookoutService extends AbstractBookoutService
{

private static final Logger logger = Logger.getLogger( KBBBookoutService.class );

private GuideBookMetaInfo metaInfo;
private IKBBDAO kbbDAO;

public KBBBookoutService()
{
	try
	{
		populateMetaInfo();
	}
	catch ( Exception e )
	{
		logger.error( "Problem initializing KBB Bookout Service", e );
	}
}

private void populateMetaInfo() throws GBException
{
	if ( metaInfo == null )
	{
		metaInfo = new GuideBookMetaInfo();
	}

	metaInfo.init( ThirdPartyDataProvider.FOOTER_KELLEY, "Publish Info not initialized", ThirdPartyDataProvider.NAME_KELLEY,
					ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE, ThirdPartyDataProvider.IMAGE_NAME_KELLEY,
					ThirdPartyDataProvider.BGCOLOR_KELLEY );
}

public GuideBookMetaInfo getMetaInfo( String region, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	GuideBookMetaInfo completeMetaInfo = this.metaInfo;
	Map <String, String> guideBookInfo = kbbDAO.getMetaInfo( bookoutInfo );
	completeMetaInfo.setPublishInfo( region + ". " + guideBookInfo.get("timePeriod"));
	return completeMetaInfo;
}

public String retrieveRegionDescription( GuideBookInput input ) throws GBException
{
	return input.getState();
}

// Want to return a list of VDP_GuideBookVehicle
public List<VDP_GuideBookVehicle> doVinLookup( String region, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	Integer year = null;
	try	{
		year = VinUtility.getLatestModelYear( vin );
	}
	catch (InvalidVinException e) {
		throw new GBException( "VIN contained invalid year character.");
	}
	
	List<VDP_GuideBookVehicle> results = kbbDAO.doVinLookup( vin, year, bookoutInfo );
	for ( VDP_GuideBookVehicle result : results ) {
		result.setVin( vin );
	}
	
	return results;
}

@SuppressWarnings("unchecked")
protected List<ThirdPartyVehicleOption> retrieveOptionsInternal( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	List<ThirdPartyVehicleOption> toBeReturned = kbbDAO.doOptionsLookup( input.getVin(), guideBookVehicle.getKey(), bookoutInfo );
	
	BeanComparator sortOrderComparator = new BeanComparator( "sortOrder" );
	Collections.sort( toBeReturned, sortOrderComparator );
	
	makeSureDefaultExists( toBeReturned, ThirdPartyOptionType.DRIVETRAIN );
	makeSureDefaultExists( toBeReturned, ThirdPartyOptionType.ENGINE );
	makeSureDefaultExists( toBeReturned, ThirdPartyOptionType.TRANSMISSION );
	
	// sort order needs to be sequential for the way we are doing our hiberante mappings - this makes the order sequential
	// find normailzed version of option in ThirdPartyOption table and associate it with ThirdPartyVehicleOption
	/*
	int sortOrder = 0;
	ThirdPartyOption tpo = null;
	for( ThirdPartyVehicleOption option : toBeReturned )
	{
		option.setSortOrder( sortOrder );
		sortOrder++;
		
		tpo = getThirdPartyOptionService().getPersistedOption( option.getOption() );
		option.setOption( tpo );
	}
	*/
	return toBeReturned;
}

//Value object to help refactoring and consolidate logic
private static class BookValuesInput {
	
	private final List<VDP_GuideBookValue> baseValues;
	
	private final VDP_GuideBookValue mileageAdjustment;
	
	private final boolean isCertified;
	
	private final int certifiedValue;
	
	private final Double maxDeductPercentage;
	
	private final Integer cpoMileageRestriction;
	
	private final List<String> optionSelections;
	
	BookValuesInput(
			List<VDP_GuideBookValue> baseValues, 
			VDP_GuideBookValue mileageAdjustmentValue, 
			boolean isCertified,
			int certifiedValue,
			Double maxDeductPercentage,
			Integer cpoMileageRestriction,
			List<String> optionSelections) {
		this.baseValues = baseValues;
		this.mileageAdjustment = mileageAdjustmentValue;
		this.isCertified = isCertified;
		this.certifiedValue = certifiedValue;
		this.maxDeductPercentage = maxDeductPercentage;
		this.cpoMileageRestriction = cpoMileageRestriction;
		this.optionSelections = optionSelections;
	}
	
	public List<VDP_GuideBookValue> getBaseValues() {
		return Collections.unmodifiableList(baseValues);
	}

	public VDP_GuideBookValue getMileageAdjustment() {
		return mileageAdjustment;
	}
	
	public boolean isCertified() {
		return isCertified;
	}
	
	public int getCertifiedValue() {
		return certifiedValue;
	}
	
	public Double getMaxDeductPercentage() {
		return maxDeductPercentage;
	}
	
	public Integer getCpoMileageRestriction() {
		return cpoMileageRestriction;
	}

	public List<String> getOptionSelections() {
		return Collections.unmodifiableList(optionSelections);
	}
}

private BookValuesInput buildBookValueInput( String state, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, 
		VDP_GuideBookVehicle vehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException, OptionConflictException {
	//	 check for options conflicts
	List<VDP_GuideBookValue> conflicts = checkForOptionsConflicts(vehicle.getKey(), thirdPartyVehicleOptions, bookoutInfo);
	if ( conflicts != null ){
		throw new OptionConflictException(conflicts);
	}
	
	KbbPriceAdjustmentsTable baseValues = kbbDAO.getValues( vehicle.getKey(), Integer.parseInt(vehicle.getYear()), mileage, state, bookoutInfo ); 
	
	// get mileage VDP_GuideBookValue object - then remove it from list of base values
	VDP_GuideBookValue mileageAdjustmentValue = baseValues.getMileageAdjustment();
	
	Double maxDeductPercentage = baseValues.getMaximumDeductionPercentage() != null ? 
			baseValues.getMaximumDeductionPercentage().getPercentageAdjustment() : null;
			
	Integer cpoMileageRestriction = baseValues.getCpoMileageRestriction() != null ? 
			baseValues.getCpoMileageRestriction().getValue() : null;
	
	//CPO was hacked in as an "option", take it out so the sum of option values doesn't include cpo w/o the new logic.
	ThirdPartyVehicleOption certifiedOption = null;
	for(ThirdPartyVehicleOption opt : thirdPartyVehicleOptions) {
		if(opt.getOptionName().equalsIgnoreCase("CPO")) {
			certifiedOption = opt;
			break;
		}
	}
	int certifiedValue = 0;
	boolean isCertified = false;
	if(certifiedOption != null) {
		//ok, can't remove from collection because somehow the front end still reads from this collection and the cpo options disappears =(
		//thirdPartyVehicleOptions.remove(certifiedOption);
		certifiedValue = certifiedOption.getRetailValue();
		isCertified = certifiedOption.isStatus();
	}
	
	// this compiles the list of options whose values should be inlcluded in the final options adjustment value
	// look in method fo description of how this list is compiled
	List<String> optionSelections = getOptionSelections( thirdPartyVehicleOptions, bookoutInfo );
	
	BookValuesInput input = new BookValuesInput(
			baseValues.getBaseValues(), 
			mileageAdjustmentValue,
			isCertified,
			certifiedValue, 
			maxDeductPercentage, 
			cpoMileageRestriction,
			optionSelections);
	return input;
}

//Value object to help refactoring and consolidate logic.
private static class PriceTable {
	
	private final BookValuesInput bookValuesInput;
	
	private final List<VDP_GuideBookValue> optionsValues;
	
	//aka mileageIndependentValues
	private final List<VDP_GuideBookValue> basePlusOptionsAdjustedValues;
	
	private final List<VDP_GuideBookValue> finalValues;
	
	PriceTable(	BookValuesInput bookValuesInput,
				List<VDP_GuideBookValue> optionsValues,
				List<VDP_GuideBookValue> mileageIndependentValues, 
				List<VDP_GuideBookValue> finalValues) {
		this.bookValuesInput = bookValuesInput;
		this.optionsValues = optionsValues;
		this.basePlusOptionsAdjustedValues = mileageIndependentValues;
		this.finalValues = finalValues;
	}

	public List<VDP_GuideBookValue> getBaseValues() {
		return bookValuesInput.getBaseValues();
	}
	
	public VDP_GuideBookValue getMileageValue() {
		return bookValuesInput.getMileageAdjustment();
	}

	public List<VDP_GuideBookValue> getOptionsValues() {
		return Collections.unmodifiableList(optionsValues);
	}
	
	public List<VDP_GuideBookValue> getBasePlusOptionsAdjustedValues() {
		return Collections.unmodifiableList(basePlusOptionsAdjustedValues);
	}

	public List<VDP_GuideBookValue> getFinalValues() {
		return Collections.unmodifiableList(finalValues);
	}
}

private static class OptionConflictException extends Exception {

	private static final long serialVersionUID = 8672923104881555433L;
	
	private final List<VDP_GuideBookValue> conflicts; 
	
	public OptionConflictException(List<VDP_GuideBookValue> conflicts) {
		this.conflicts = conflicts;
	}

	public List<VDP_GuideBookValue> getConflicts() {
		return conflicts;
	}
}

private PriceTable buildPriceTable( BookValuesInput bookValuesInput, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOption, BookOutDatasetInfo bookoutInfo) throws GBException {

	// compute the options values
	List<VDP_GuideBookValue> optionsValues = computeOptionsValue( thirdPartyVehicleOption, bookValuesInput.getOptionSelections(), bookoutInfo );
			
	// compute mileage independent values (base + options)
	List<VDP_GuideBookValue> mileageIndepedentValues = computeMileageIndependantValues( bookValuesInput.getBaseValues(), optionsValues );
	
	int cpoValue = 0;
	boolean useCertifiedValue = 
		bookValuesInput.isCertified() 
		&& (bookValuesInput.getCpoMileageRestriction() != null) 
		&& (mileage <= bookValuesInput.getCpoMileageRestriction());
	
	for ( ThirdPartyVehicleOption tpvo : thirdPartyVehicleOption )
	{
		Iterator< ThirdPartyVehicleOptionValue > iter = tpvo.getOptionValues().iterator();
		while ( iter.hasNext() )
		{
			ThirdPartyVehicleOptionValue optionVal = iter.next();
			if((ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE.equals(optionVal.getThirdPartyVehicleOptionValueType())) && (optionVal.getThirdPartyVehicleOption().getOptionName().contains("CPO")))
			{
				cpoValue = optionVal.getValue();
			}
		}
	}
	
	int certifiedValue = (bookValuesInput.getCertifiedValue() == 0 ? cpoValue : bookValuesInput.getCertifiedValue());
	// compute final values ( base + options + mileage adju)
	List<VDP_GuideBookValue> finalValues = computeFinalValues( 
			bookValuesInput.getBaseValues(), 
			optionsValues, 
			bookValuesInput.getMileageAdjustment().getValue(),
			bookValuesInput.getMaxDeductPercentage(),
			useCertifiedValue,
			certifiedValue);
	
	PriceTable priceTable = new PriceTable(bookValuesInput, optionsValues, mileageIndepedentValues, finalValues);
	return priceTable;
}

// for KBB this returns base/final values for retail, wholesale and trade-in (only return trade-in for selected condition)
// also returns 1 options value (retail or trade depending on if its an inventory/appraisal bookout) and 1 mileage adjustment
public List<VDP_GuideBookValue> retrieveBookValues( String state, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, 
								VDP_GuideBookVehicle vehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	BookValuesInput bookValuesInput;
	try {
		bookValuesInput = buildBookValueInput(state, mileage, thirdPartyVehicleOptions, vehicle, vin, bookoutInfo);
	} catch (OptionConflictException e) {
		return e.getConflicts();
	}
	
	PriceTable table = buildPriceTable(bookValuesInput, mileage, thirdPartyVehicleOptions, bookoutInfo);
	
	List<VDP_GuideBookValue> toBeReturned = new ArrayList< VDP_GuideBookValue >();

	if (calculateAdjustments(table.getFinalValues())) {
		toBeReturned.add( table.getMileageValue() );
		// only want to save the total options adjustment that will be displayed (inventory - retail, appraisal - trade-in)
		toBeReturned.add( getOptionValue( table.getOptionsValues(), vehicle.getCondition() ) );
	}
	// for base, mileageIndepedentValues and final values - we only want to return the values to be display (no private party for now and only 3 trade-in values)
	// the trade in value type is determined by the condition set on the passed in vehicle
	toBeReturned.addAll( getValuesForDisplay( table.getBaseValues(), vehicle.getCondition() ) );
	toBeReturned.addAll( getValuesForDisplay( table.getBasePlusOptionsAdjustedValues(), vehicle.getCondition() ) );
	toBeReturned.addAll( getValuesForDisplay( table.getFinalValues(), vehicle.getCondition() ) );
	return toBeReturned;
}

//for KBB this returns the final values for all KBB trade-in values
public List<VDP_GuideBookValue> retrieveRetailAndTradeInBookValues( String state, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, 
								VDP_GuideBookVehicle vehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	BookValuesInput bookValuesInput;
	try {
		bookValuesInput = buildBookValueInput(state, mileage, thirdPartyVehicleOptions, vehicle, vin, bookoutInfo);
	} catch (OptionConflictException e) {
		return e.getConflicts();
	}

	// we have to do this look up becuause kbbVehicleOptions don't have any concept of value while ThirdPartyVehicleOption does, so we have to go bakc to DB
	List<ThirdPartyVehicleOption> tpvos = kbbDAO.doOptionsLookup( vehicle.getVin(), vehicle.getKey(), bookoutInfo );
	
	PriceTable table = buildPriceTable(bookValuesInput, mileage, tpvos, bookoutInfo);
	
	List<VDP_GuideBookValue> toBeReturned = new ArrayList< VDP_GuideBookValue >();
	
	ThirdPartySubCategoryEnum[] categories = new ThirdPartySubCategoryEnum[] {
			ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGEHIGH,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGELOW,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGEHIGH,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGELOW,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGEHIGH,
			ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGELOW,
			ThirdPartySubCategoryEnum.KBB_RETAIL
	};

	toBeReturned.addAll( getValuesForDisplay(table.getFinalValues(), categories) );
	return toBeReturned;
}

/**
 * KBB will return 0 values for all categories of values when it does not have values for a VIN. 
 * However, in this case KBB STILL returns adjustment values - bc adjustment values are not VIN(or Make/Model) specific.
 * Other books return NA or no results.
 * We don't want to calculate or show mileage or option adjustments values if we do not have base values.
 * This method is just a simple check to see if we have base values and should bother calculating adjustments.
 * @param finalValues
 * @return
 */
private boolean calculateAdjustments(List<VDP_GuideBookValue> finalValues) {
	boolean calculateAdjustments = false;
	int sum = 0;
	for (VDP_GuideBookValue value : finalValues) {
		sum += value.getValue();
	}
	if (sum > 0) {
		calculateAdjustments = true;
	}
	return calculateAdjustments;
}

public List<VDP_GuideBookValue> checkForOptionsConflicts(String vehicleKey, Collection<ThirdPartyVehicleOption> options, BookOutDatasetInfo bookoutInfo) {
	StringBuilder selectedOptionCDs = new StringBuilder();
	
	for ( ThirdPartyVehicleOption option : options )
	{
		if ( option.isStatus() )
		{
			selectedOptionCDs.append( option.getOptionKey() ).append( "," );
		}
	}
	
	// remove last ,
	if (selectedOptionCDs.length() > 0 ) {selectedOptionCDs.deleteCharAt( selectedOptionCDs.length() - 1 ); }
	
	List<VDP_GuideBookValue> conflicts = kbbDAO.checkForEquipmentConflicts(vehicleKey, selectedOptionCDs.toString(), bookoutInfo );
	if ( !conflicts.isEmpty() )
	{
		BookOutError error = null;
		// need to get the display names of the options - we only have equip code
		for( VDP_GuideBookValue conflict : conflicts)
		{
			error = conflict.getError();
			for( ThirdPartyVehicleOption option : options )
			{
				if ( option.getOptionKey().equals( error.getFirstOptionDescription() ) )
				{
					error.setFirstOptionDescription( option.getOptionName() );
				}
				if ( option.getOptionKey().equals( error.getSecondOptionDescription() ) )
				{
					error.setSecondOptionDescription( option.getOptionName() );
				}
			}
		}
		return conflicts;
	}
	return null;
}

//protected for unit testing
protected List< VDP_GuideBookValue > getValuesForDisplay( List< VDP_GuideBookValue > values, ThirdPartySubCategoryEnum... categories )
{
	List< VDP_GuideBookValue > toBeReturned = new ArrayList< VDP_GuideBookValue >();
	for ( VDP_GuideBookValue value : values )
	{
		for(ThirdPartySubCategoryEnum cat : categories)
		{
			// we can compare retail and whole to subcategoryID here because for retail and wholesale ThirdPartySubCategoryId = ThirdPartyCategoryId 
			if ( cat.getThirdPartySubCategoryId().equals(value.getThirdPartySubCategoryId()))
			{
				toBeReturned.add( value );
			}
		}
	}
	return toBeReturned;
}

// protected for unit testing
protected List< VDP_GuideBookValue > getValuesForDisplay( List< VDP_GuideBookValue > values, KBBConditionEnum condition )
{
	ThirdPartySubCategoryEnum thirdPartySubCategory = KBBValueEnum.getThirdPartySubCategoryByConditionAndCategory( condition, KBBCategoryEnum.TRADEIN );
	ThirdPartySubCategoryEnum tradeinRangeHighSubCategory = KBBValueEnum.getThirdPartySubCategoryByConditionAndCategory( condition, KBBCategoryEnum.TRADEINRANGEHIGH );
	ThirdPartySubCategoryEnum tradeinRangeLowSubCategory = KBBValueEnum.getThirdPartySubCategoryByConditionAndCategory( condition, KBBCategoryEnum.TRADEINRANGELOW );
	ThirdPartySubCategoryEnum[] categories = new ThirdPartySubCategoryEnum[] {
			ThirdPartySubCategoryEnum.KBB_RETAIL,
			ThirdPartySubCategoryEnum.KBB_WHOLESALE,
			thirdPartySubCategory,
			tradeinRangeHighSubCategory,
			tradeinRangeLowSubCategory
	}; 
	
	return getValuesForDisplay(values, categories);
}

// protected for unit testing
protected List< VDP_GuideBookValue > computeFinalValues( 
		List< VDP_GuideBookValue > baseValues,
		List< VDP_GuideBookValue > optionsValues,
		Integer mileageValue,
		Double maximumDeductionPercentage,
		boolean useCertifiedValue,
		int certifiedValue)
{
	List< VDP_GuideBookValue > finalValues = new ArrayList< VDP_GuideBookValue >();
	VDP_GuideBookValue finalValue = null;
	for( VDP_GuideBookValue baseValue : baseValues )
	{
		for( VDP_GuideBookValue optionValue : optionsValues )
		{
			if ( baseValue.getThirdPartySubCategoryId().equals( optionValue.getThirdPartySubCategoryId() ) )
			{
				finalValue = new VDP_GuideBookValue();
				finalValue.setHasErrors( baseValue.isHasErrors() );
				finalValue.setError( baseValue.getError() );
				finalValue.setThirdPartyCategoryId( baseValue.getThirdPartyCategoryId() );
				finalValue.setThirdPartyCategoryDescription( baseValue.getThirdPartyCategoryDescription() );
				finalValue.setThirdPartySubCategoryId( baseValue.getThirdPartySubCategoryId() );
				
				//if book is not found or value doesn't exist - don't add mileage adjustment - we want to keep values at 0
				if ( baseValue.getValue().intValue() > 0 ) {
					int finalAdjustment = calculateFinalAdjustment(baseValue.getValue(), optionValue.getValue(), 
							(mileageValue != null ? mileageValue : 0), 
							(maximumDeductionPercentage != null ? maximumDeductionPercentage.doubleValue() : 0));
					if(baseValue.getThirdPartySubCategoryId().equals(ThirdPartySubCategoryEnum.KBB_RETAIL.getThirdPartySubCategoryId())) {
						finalAdjustment = useCertifiedValue ? finalAdjustment + certifiedValue : finalAdjustment;
					}
					finalValue.setValue( baseValue.getValue() + finalAdjustment );
				} else {
					finalValue.setValue( 0 );
				}	
				
				finalValue.setValueType( BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
				
				finalValues.add( finalValue );
				break;
			}
		}
	}
	return finalValues;
}

/**
 * Implements KBB Data Syndication - Used Car Configuration Guide 02-24-10 Vers.2.1.doc, 1.14. Maximum Deduction Percentage
 * @param baseValue
 * @param optionValue
 * @param mileageValue
 * @param maximumDeductionPercentage
 * @return the final adjustment value.
 */
private int calculateFinalAdjustment(int baseValue, int optionValue, int mileageValue, Double maximumDeductionPercentage) {
	int finalAdjustment = optionValue + mileageValue;
	int totalAdjustment = finalAdjustment;
	if(maximumDeductionPercentage != null && totalAdjustment < 0) {
		double maxDeductTemp = ((double)baseValue) * maximumDeductionPercentage.doubleValue();
		int maxDeduct = (int)Math.round(maxDeductTemp);
		if(Math.abs(totalAdjustment) > maxDeduct) {
			finalAdjustment = -maxDeduct;
		} 
	}
	return finalAdjustment;
}

// protected for unit testing
protected List< VDP_GuideBookValue > computeMileageIndependantValues( List< VDP_GuideBookValue > baseValues, List< VDP_GuideBookValue > optionsValues )
{
	List< VDP_GuideBookValue > mileageIndependentValues = new ArrayList< VDP_GuideBookValue >();
	VDP_GuideBookValue mileageIndependentValue = null;
	
	for( VDP_GuideBookValue baseValue : baseValues )
	{
		for( VDP_GuideBookValue optionValue : optionsValues )
		{
			if ( baseValue.getThirdPartySubCategoryId().equals( optionValue.getThirdPartySubCategoryId() ) )
			{
				mileageIndependentValue = new VDP_GuideBookValue();
				mileageIndependentValue.setHasErrors( baseValue.isHasErrors() );
				mileageIndependentValue.setError( baseValue.getError() );
				mileageIndependentValue.setThirdPartyCategoryId( baseValue.getThirdPartyCategoryId() );
				mileageIndependentValue.setThirdPartyCategoryDescription( baseValue.getThirdPartyCategoryDescription() );
				mileageIndependentValue.setThirdPartySubCategoryId( baseValue.getThirdPartySubCategoryId() );
				
				// if book is not found - don't add option adjustment - we want to keep values at 0
				if ( baseValue.getValue().intValue() > 0 ) {
					mileageIndependentValue.setValue( baseValue.getValue() + optionValue.getValue() );
				} else {
					mileageIndependentValue.setValue( 0 );
				}		
				
				mileageIndependentValue.setValueType( BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT);
				
				mileageIndependentValues.add( mileageIndependentValue );
				break;
			}
		}
	}
	return mileageIndependentValues;
}

// protected for unit testing
protected VDP_GuideBookValue getOptionValue( List< VDP_GuideBookValue > optionsValues, KBBConditionEnum condition )
{
	for( VDP_GuideBookValue value : optionsValues )
	{
		if ( ( KBBConditionEnum.EXCELLENT.equals( condition ) &&
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId() ) ||
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGEHIGH.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId() ) ||
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGELOW.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId() )) ||
			 ( KBBConditionEnum.GOOD.equals( condition ) &&
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId()) ||
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGEHIGH.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId()) ||
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGELOW.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId())) ||
			 ( KBBConditionEnum.FAIR.equals( condition ) &&
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId()) ||
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGEHIGH.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId()) ||
				(ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGELOW.getThirdPartySubCategoryId().intValue() == value.getThirdPartySubCategoryId())))
		{
			return value;
		}
	}
	return new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, "Option Value", BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE );
}

// protected for unit testing
protected List< VDP_GuideBookValue > computeOptionsValue(List<ThirdPartyVehicleOption> options, List<String> optionsWhoseValuesShouldBeApplied, 
                                                         				BookOutDatasetInfo bookoutInfo)
{
	VDP_GuideBookValue retailOptionsAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_RETAIL_TYPE, ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_RETAIL.getThirdPartySubCategoryId());
	VDP_GuideBookValue wholesaleOptionsAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_WHOLESALE_TYPE, ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_WHOLESALE.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_excAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_excAdj_RangeHigh = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLY_TRADEIN_RANGEHIGH_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_RANGEHIGH_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGEHIGH.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_excAdj_RangeLow = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLY_TRADEIN_RANGELOW_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_RANGELOW_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGELOW.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_goodAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_goodAdj_RangeHigh = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLY_TRADEIN_RANGEHIGH_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_RANGEHIGH_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGEHIGH.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_goodAdj_RangeLow = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLY_TRADEIN_RANGELOW_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_RANGELOW_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGELOW.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_fairAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_TRADEIN_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_fairAdj_RangeHigh = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLY_TRADEIN_RANGEHIGH_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_RANGEHIGH_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGEHIGH.getThirdPartySubCategoryId());
	VDP_GuideBookValue tradeIn_fairAdj_RangeLow = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLY_TRADEIN_RANGELOW_TYPE, ThirdPartyCategory.KELLEY_TRADEIN_RANGELOW_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGELOW.getThirdPartySubCategoryId());
	VDP_GuideBookValue pp_excAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_EXCELLENT.getThirdPartySubCategoryId()  );
	VDP_GuideBookValue pp_goodAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_GOOD.getThirdPartySubCategoryId()  );
	VDP_GuideBookValue pp_fairAdj = new VDP_GuideBookValue(0, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE, ThirdPartyCategory.KELLEY_PRIVATE_PARTY_DESCRIPTION, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_FAIR.getThirdPartySubCategoryId()  );
	
	// sum up the option values for options whose values need to be applied for each type of value
	for ( ThirdPartyVehicleOption option : options )
	{
		if ( optionsWhoseValuesShouldBeApplied.contains( option.getOptionKey() ))	
		{
			Iterator< ThirdPartyVehicleOptionValue > iter = option.getOptionValues().iterator();
			while ( iter.hasNext() )
			{
				ThirdPartyVehicleOptionValue optionValue = iter.next();
				ThirdPartyVehicleOptionValueType theOptValueType = optionValue.getThirdPartyVehicleOptionValueType();
				
				if(ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE.equals(theOptValueType)) {
					retailOptionsAdj.setValue( retailOptionsAdj.getValue() + optionValue.getValue() );
				}
				else if (ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE.equals(theOptValueType)) {
					wholesaleOptionsAdj.setValue( wholesaleOptionsAdj.getValue() + optionValue.getValue() );
				}
				else if (ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD.equals(theOptValueType)) {
					tradeIn_goodAdj.setValue( tradeIn_goodAdj.getValue() + optionValue.getValue() );
					tradeIn_goodAdj_RangeHigh.setValue(tradeIn_goodAdj_RangeHigh.getValue() + optionValue.getValue() );
					tradeIn_goodAdj_RangeLow.setValue(tradeIn_goodAdj_RangeLow.getValue() + optionValue.getValue() );
				} 
				else if (ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR.equals(theOptValueType)) {
					tradeIn_fairAdj.setValue( tradeIn_fairAdj.getValue() + optionValue.getValue() );
					tradeIn_fairAdj_RangeHigh.setValue( tradeIn_fairAdj_RangeHigh.getValue() + optionValue.getValue() );
					tradeIn_fairAdj_RangeLow.setValue( tradeIn_fairAdj_RangeLow.getValue() + optionValue.getValue() );
				} 
				else if (ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT.equals(theOptValueType)) {
					tradeIn_excAdj.setValue( tradeIn_excAdj.getValue() + optionValue.getValue() );
					tradeIn_excAdj_RangeHigh.setValue( tradeIn_excAdj_RangeHigh.getValue() + optionValue.getValue() );
					tradeIn_excAdj_RangeLow.setValue( tradeIn_excAdj_RangeLow.getValue() + optionValue.getValue() );
				}
				else if (ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD.equals(theOptValueType)) {
					pp_goodAdj.setValue( pp_goodAdj.getValue() + optionValue.getValue() );
				}
				else if (ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR.equals(theOptValueType)) {
					pp_fairAdj.setValue( pp_fairAdj.getValue() + optionValue.getValue() );
				}
				else if (ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT.equals(theOptValueType)) {
					pp_excAdj.setValue( pp_excAdj.getValue() + optionValue.getValue() );
				}
			}
		}
	}

	List< VDP_GuideBookValue > toBeReturned = new ArrayList< VDP_GuideBookValue >();
	toBeReturned.add( retailOptionsAdj );
	toBeReturned.add( wholesaleOptionsAdj );
	toBeReturned.add( tradeIn_excAdj );
	toBeReturned.add( tradeIn_excAdj_RangeHigh );
	toBeReturned.add( tradeIn_excAdj_RangeLow );
	toBeReturned.add( tradeIn_fairAdj );
	toBeReturned.add( tradeIn_fairAdj_RangeHigh );
	toBeReturned.add( tradeIn_fairAdj_RangeLow );
	toBeReturned.add( tradeIn_goodAdj );
	toBeReturned.add( tradeIn_goodAdj_RangeHigh );
	toBeReturned.add( tradeIn_goodAdj_RangeLow );
	toBeReturned.add( pp_excAdj );
	toBeReturned.add( pp_fairAdj );
	toBeReturned.add( pp_goodAdj );
	return toBeReturned;
}

// creates a list of equipment codes for all options in the passed in list whose adjustment values were included in the calculation of
// the vehicles final value
public List< String > getOptionSelections( List< ThirdPartyVehicleOption > options, BookOutDatasetInfo bookoutInfo )
{
	// NOTE: func below is deprecated - 4/10/12 - all standard options should be applied
	// get list of missing std options (options whose values are applied when they are NOT selected
	/*
	StringBuilder missingStdOptionCodes = new StringBuilder();
	StringBuilder selectedOptionCodes = new StringBuilder();
	for ( ThirdPartyVehicleOption option : options )
	{
		if ( option.isStandardOption() && !option.isStatus() )
		{
			missingStdOptionCodes.append( option.getOptionKey() ).append(",");
		}
		if ( option.isStatus() )
		{
			selectedOptionCodes.append( option.getOptionKey() ).append(",");
		}
	}	
	// remove trailing ,'s
	if (missingStdOptionCodes.length() > 0) { missingStdOptionCodes.deleteCharAt( missingStdOptionCodes.length() - 1 ); }
	if (selectedOptionCodes.length() > 0) { selectedOptionCodes.deleteCharAt( selectedOptionCodes.length() - 1 ); }
	*/
	// for all missing std options - check to see if they have a conflicting option selected - if they do - the missing std option value is NOT applied
	// i.e. if CD player is standard and it's value is -300 -> if they have a multiCD player selected - don't include -300 for missing CD player in final value
	//List<String> missingStdOptionsWithValuesNotToApply = kbbDAO.checkForOptionConflictsWithStdOptions( missingStdOptionCodes, selectedOptionCodes, bookoutInfo );

	List<String> toBeReturned = new ArrayList< String >();
	
	for ( ThirdPartyVehicleOption option : options )
	{
		// Kbb update - all standard options should be applied regardless of whether selected or not
		if(option.isStatus()){
			if(!option.isStandardOption()){
				toBeReturned.add( option.getOptionKey() );
			}
		} else {
			if(option.isStandardOption()){
				toBeReturned.add( option.getOptionKey() );
			}
		}
		
		// this says if you're a missing standard option OR not standard and selected - then apply the value adjustment
		/*
		if ( ( ( option.isStandardOption() && !option.isStatus() ) &&
			  ( !missingStdOptionsWithValuesNotToApply.contains( option.getOptionKey() ) ) ) ||
			  ( !option.isStandardOption() && option.isStatus() ) )	
		{
			toBeReturned.add( option.getOptionKey() );
		}
		*/
	}
	
	return toBeReturned;
}

@Override
protected List<ThirdPartyVehicleOption> retrieveOptionsInternal(String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo) throws GBException {
	GuideBookInput input = new GuideBookInput();
	VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
	vehicle.setKey(trimId); // trimId=KKB.vehicleId since a trim unqiuely id's a vehicle
	
	return retrieveOptionsInternal( input, vehicle, bookoutInfo );
}

// this method is not needed as for manual bookout we are using the same retrieve values as we are for VIN bookouts
public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	return null;
}

// KBB documentation says if no engine/drivetrain/tranmission options is standard select the option that is closest to 0 as default
// also - for engine/drivetrain/tranmission - the adjustment is ALWAYS applied when selected - so set standardOption = false for all of these options
protected static void makeSureDefaultExists( List< ThirdPartyVehicleOption > options, ThirdPartyOptionType thirdPartyOptionType )
{
	if ( getDefaultOption( options, thirdPartyOptionType ) == null )
	{
		ThirdPartyVehicleOption optionClosestToZero = null;
		for( ThirdPartyVehicleOption option :  options )
		{
			if ( option.getOption().getThirdPartyOptionType().equals(thirdPartyOptionType))
			{
				option.setStandardOption( false );
				if ( (optionClosestToZero == null) || 
						Math.abs( option.getWholesaleValue() ) < Math.abs( optionClosestToZero.getWholesaleValue() ) )
				{
					optionClosestToZero = option;
				}
			}
		}
		if (optionClosestToZero != null) {
			optionClosestToZero.setStatus( true );
		}
	}
}

protected static ThirdPartyVehicleOption getDefaultOption( List< ThirdPartyVehicleOption > toBeReturned, ThirdPartyOptionType thirdPartyOptionType )
{
	List<ThirdPartyVehicleOption> options = KBBBookoutService.getAllOptionsOfType( toBeReturned, thirdPartyOptionType );
	for ( ThirdPartyVehicleOption option : options )
	{
		if ( option.isStatus() )
		{
			return option;
		}
	}
	return null;
}

/**
 * Returns a list of the Options in the options parameter which are of the type optionType.
 * 
 * @param options
 * @param optionType
 * @return
 */
public static List<ThirdPartyVehicleOption> getAllOptionsOfType( List<ThirdPartyVehicleOption> options, ThirdPartyOptionType optionType ) {
	List<ThirdPartyVehicleOption> optionsOfType = new ArrayList<ThirdPartyVehicleOption>();
	if ( options != null && !options.isEmpty() )
	{
		Iterator<ThirdPartyVehicleOption> optionsIter = options.iterator();
		ThirdPartyVehicleOption option;
		while ( optionsIter.hasNext() )
		{
			option = (ThirdPartyVehicleOption)optionsIter.next();
			if ( option != null ) {
				if ( option.getOption() != null ) {
					if ( option.getOption().getThirdPartyOptionType() != null ) {
						if ( option.getOption().getThirdPartyOptionType().equals(optionType)) {
							optionsOfType.add( option );
						}
					}
				}
			}
		}
	}
	return optionsOfType;
}

public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo ) throws GBException
{
	return kbbDAO.retrieveModelYears(bookoutInfo);
}

public List<VDP_GuideBookVehicle> retrieveMakes( String year, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	return kbbDAO.retrieveMakes( year, bookoutInfo );
}

public List<VDP_GuideBookVehicle> retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	return kbbDAO.retrieveModels(year, makeId, bookoutInfo);
}

public List<VDP_GuideBookVehicle> retrieveTrims( String year, String make, String model, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	return kbbDAO.retrieveTrims(year, make, model, bookoutInfo);
}

public void setKbbDAO( IKBBDAO kbbDAO )
{
	this.kbbDAO = kbbDAO;
}

public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear(String vin,Integer year, BookOutDatasetInfo bookoutInfo) throws GBException {
	return kbbDAO.retrieveModelsByMakeIDAndYear(vin, year, bookoutInfo);
}

public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId,String modelId, BookOutDatasetInfo bookoutInfo) throws GBException {
	return kbbDAO.retrieveTrimsWithoutYear(makeId, modelId, bookoutInfo);
}


}
