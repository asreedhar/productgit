package biz.firstlook.module.vehicle.description.compatibility;

import biz.firstlook.commons.collections.Adapter;
import biz.firstlook.module.vehicle.description.VehicleOption;

public interface VehicleOptionAdapter< T > extends Adapter<VehicleOption, T>
{

}
