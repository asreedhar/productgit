package biz.firstlook.module.vehicle.description;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;

public interface VehicleDescriptionDAO 
{
	public VehicleBookoutState getVehicleBookoutState( String vin, Integer businessUnitId, VehicleDescriptionProviderEnum providerEnum );
	
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdate( VehicleBookoutState bookoutState );
}
