package biz.firstlook.module.vehicle.description;

import java.util.Collection;

import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.transact.persist.model.Dealership;

public interface VehicleDescriptionProvider
{
void init( Dealership dealer );
Collection< MutableVehicleDescription > getVehicleDescription( String vin );
VehicleDescriptionProviderEnum getDescriptionProviderEnum();
}
