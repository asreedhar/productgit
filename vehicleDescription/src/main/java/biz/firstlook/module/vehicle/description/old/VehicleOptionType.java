package biz.firstlook.module.vehicle.description.old;

//from KBB.VehicleOption
enum VehicleOptionType {
	PICK_EXTERIOR_COLORS(2, "PICK EXTERIOR COLORS"),
	NO_CHARGE_ADDITIONS(3, "NO CHARGE ADDITIONS"),
	EQUIPMENT(4, "Equipment"),
	OPTION(5, "Option"),
	CPO(7, "CPO");
	
	private final int optionTypeId;
	private final String optionTypeDisplayName;
	
	private VehicleOptionType(int optionTypeId, String optionTypeDisplayName) {
		this.optionTypeId = optionTypeId;
		this.optionTypeDisplayName = optionTypeDisplayName;
	}
	
	public int getOptionTypeId() {
		return optionTypeId;
	}
	
	public String getOptionTypeDisplayName() {
		return optionTypeDisplayName;
	}
	
	/**
	 * @param optionTypeId
	 * @return the VehicleOptionType corresponding to the optionTypeId
	 * @throws IllegalArgumentException if the optionTypeId is not known to be part of the business logic.
	 */
	public static VehicleOptionType fromId(int optionTypeId) {
		VehicleOptionType vehicleOptionType = null;
		for(VehicleOptionType v : VehicleOptionType.values()) {
			if(v.getOptionTypeId() == optionTypeId) {
				vehicleOptionType = v;
				break;
			}
		}
		
		if(vehicleOptionType == null) {
			throw new IllegalArgumentException("OptionTypeId " + optionTypeId + " not understood by business logic.");
		}
		
		return vehicleOptionType;
	}
}
