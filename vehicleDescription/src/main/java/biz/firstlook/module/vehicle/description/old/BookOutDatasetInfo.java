package biz.firstlook.module.vehicle.description.old;


public class BookOutDatasetInfo
{
private BookOutTypeEnum bookoutTypeEnum;
private Integer businessUnitId;

public BookOutDatasetInfo( BookOutTypeEnum bookoutTypeEnum, Integer businessUnitId )
{
	this.bookoutTypeEnum = bookoutTypeEnum;
	this.businessUnitId = businessUnitId;
}

public BookOutTypeEnum getBookoutTypeEnum()
{
	return bookoutTypeEnum;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

}
