package biz.firstlook.module.vehicle.description.exception;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;

public class OptionsConflictException extends Exception {

	private static final long serialVersionUID = 3256719585087797046L;
	List<OptionConflictError> optionConflicts = new ArrayList<OptionConflictError>();

	public OptionsConflictException(List<VDP_GuideBookValue> conflicts) {
		super();
		for( VDP_GuideBookValue value : conflicts ) {
			if ( value.isHasErrors() ) {
				optionConflicts.add( new OptionConflictError( value.getError().getErrorMessage(), value.getError().getFirstOptionDescription(),
															value.getError().getSecondOptionDescription() ) );
			}
		}
	}

	public List<OptionConflictError> getOptionConflicts() { return optionConflicts; }
}
