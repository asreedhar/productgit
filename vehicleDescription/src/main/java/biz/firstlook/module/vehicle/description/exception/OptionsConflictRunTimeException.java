package biz.firstlook.module.vehicle.description.exception;

public class OptionsConflictRunTimeException extends RuntimeException {
    private static final long serialVersionUID = 3256719585087797045L;
	public OptionsConflictRunTimeException( String message ) {
		super( message );
	}
}
