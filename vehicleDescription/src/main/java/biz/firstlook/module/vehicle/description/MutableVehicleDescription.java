package biz.firstlook.module.vehicle.description;

import java.util.List;

public interface MutableVehicleDescription extends VehicleDescription
{
public void setMake(String make);
public void setModel(String model);
public void setYear(Integer year);

/**
 * this method takes in a list of selected options - it does not look at the status flag of the incoming options
 * but instead assumes that all options in the list should be selected!
 * @param selectedOptions
 */
public void setSelectedVehicleOptions( List<VehicleOption> selectedOptions );
public void setSelectedVehicleOptionsByKeys( List<String> selectedOptionsKeys );
public void setVehicleOptions(List<VehicleOption> options);
public void setVehicleDescriptionProvider( VehicleDescriptionProvider provider );
}
