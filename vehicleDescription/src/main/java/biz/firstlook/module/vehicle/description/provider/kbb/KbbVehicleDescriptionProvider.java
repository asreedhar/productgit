package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.commons.collections.ValidatableList;
import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.bookout.kbb.KbbVDP_BookValueAdapter;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.KBBBookoutService;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class KbbVehicleDescriptionProvider implements VehicleDescriptionProvider
{

private static final Logger logger = Logger.getLogger( KbbVehicleDescriptionProvider.class );

private BookOutDatasetInfo info;
private KbbRegionalIdentifier regionalIdentifier;
private KBBBookoutService kbbBookoutService;

public Collection< MutableVehicleDescription > getVehicleDescription( String vin )
{
	List< VDP_GuideBookVehicle > vehicles = null;
	try
	{
		vehicles = kbbBookoutService.doVinLookup( regionalIdentifier.getStateCode(), vin, info );
	
		GuideBookInput basicInput = new GuideBookInput();
		basicInput.setVin( vin ); // vin is the only method kbbBookoutService needs
		// for each vehicle - do options look up now - we can move this into the getOptions method as an optimization
		for( VDP_GuideBookVehicle vehicle : vehicles ) {
			vehicle.setGuideBookOptions( kbbBookoutService.retrieveOptions( basicInput, vehicle, info) );
		}
	}
	catch ( GBException e )
	{
		e.printStackTrace();
	}
	
	List<MutableVehicleDescription> vehicleDescriptions = Adapters.adapt(vehicles, new KbbVDP_VehicleAdapter( this ));
	
	return vehicleDescriptions;
}

public VehicleDescriptionProviderEnum getDescriptionProviderEnum()
{
	return VehicleDescriptionProviderEnum.KBB;
}

public void init( Dealership dealer )
{
	this.info = new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, dealer.getDealerId() );
	this.regionalIdentifier = new KbbRegionalIdentifier(dealer.getState());
	logger.info( getClass() + " was initialized with " + info + " " + regionalIdentifier );
}

public void setKbbBookoutService( KBBBookoutService kbbBookoutService )
{
	this.kbbBookoutService = kbbBookoutService;
}

public void checkForOptionsConflicts(String vehicleId, ValidatableList<ThirdPartyVehicleOption> options) throws OptionsConflictException{
	List<VDP_GuideBookValue> conflicts = kbbBookoutService.checkForOptionsConflicts( vehicleId, options, info );
	
	if ( conflicts != null ) {
		throw new OptionsConflictException( conflicts );
	}
}

/**
 * This method calls out to the provider associated with the concrete implmentatino to retrieve book values.  It is 
 * intentionall package scoped as this method should only be called by a concrete VehicleDescription implementation and
 * is not visibale to the client
 * 
 * @return bookValueContainer
 */
KbbBookValueContainer getValues( KbbVehicleDescriptionImpl description, Integer mileage, KBBConditionEnum condition ) throws ValuesRetrievalException {

	VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
	vehicle.setYear( description.getYear().toString() );
	vehicle.setMakeId( description.getMakeCode().getCode() );
	vehicle.setModelId( description.getModelCode().getCode() );
	vehicle.setCondition( condition );
	vehicle.setKey(description.getVehicleId());
	List<ThirdPartyVehicleOption> options = Adapters.unadapt(description.getAllOptions(), new Kbb_VehicleOptionAdapter() );

	try {
		List<VDP_GuideBookValue> values = kbbBookoutService.retrieveRetailAndTradeInBookValues(regionalIdentifier.getStateCode(), mileage, options, vehicle, null, info);
		return new KbbBookValueContainer( Adapters.adapt(values, new KbbVDP_BookValueAdapter() ), regionalIdentifier );
	} catch (GBException gbe ) {
		throw new ValuesRetrievalException( gbe );
	}
}
}
