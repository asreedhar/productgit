package biz.firstlook.module.vehicle.description.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.transact.persist.model.Dealership;

/**
 * The default VehicleDescriptionContext to use.  The minimal amount of information needed is the Dealership.
 * @author bfung
 *
 */
class BusinessUnitVehicleDescriptionContextImpl implements VehicleDescriptionContext
{

private static final Logger logger = Logger.getLogger( BusinessUnitVehicleDescriptionContextImpl.class );
private Map<VehicleDescriptionProviderEnum, VehicleDescriptionProvider> providerRegistry = new LinkedHashMap<VehicleDescriptionProviderEnum, VehicleDescriptionProvider>();

public BusinessUnitVehicleDescriptionContextImpl( Dealership dealer )
{
	super();
	List<Integer> guideBookIds = dealer.getDealerPreference().getOrderedGuideBookIds();
	for ( Integer guideBookId : guideBookIds )
	{
		VehicleDescriptionProviderEnum enumValue = VehicleDescriptionProviderEnum.valueOfId( guideBookId );
		VehicleDescriptionProvider provider = SpringBasedVehicleDescriptionContext.getInstance().getDescriptionProvider( enumValue );
		if( provider != null ) {
			provider.init( dealer );
			providerRegistry.put( provider.getDescriptionProviderEnum(), provider );
		}
	}
}

public VehicleDescriptionProvider getDescriptionProvider( VehicleDescriptionProviderEnum providerEnum )
{
	VehicleDescriptionProvider provider = providerRegistry.get( providerEnum );
	if( provider == null ) {
		logger.warn( "Could not find a VehicleDescriptionProvider for " + providerEnum );
	}
	return provider;
}

public Collection< VehicleDescriptionProvider > getRegisteredDescriptionProviders()
{	
	return Collections.unmodifiableCollection( providerRegistry.values() );
}

}
