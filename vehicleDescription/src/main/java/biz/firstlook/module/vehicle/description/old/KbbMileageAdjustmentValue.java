package biz.firstlook.module.vehicle.description.old;

import biz.firstlook.transact.persist.model.BookOutValueType;

/**
 * Hack to abstract out ugly logic.
 * @author bfung
 *
 */
public class KbbMileageAdjustmentValue extends VDP_GuideBookValue {

	private static final long serialVersionUID = -7082037052830234282L;

	public KbbMileageAdjustmentValue(Integer rawValue, Double percentageAdjustmentValue, int condition) {
		super.setValueType(BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT);
		
		/*
		 * yes, you didn't read this wrong.  
		 * This is ported from KBBValuesMapper, where the condition column from DB is used 
		 * and set onto ThirdPartyCategoryId as an indicator whether or not 
		 * the MileageAdjustment value is a percentage or a raw value.
		 */
		setThirdPartyCategoryId(condition);
		
		setValue(rawValue);
		setPercentageAdjustment(percentageAdjustmentValue);
	}
	
	public boolean isPercentage() {
		return getThirdPartyCategoryId() == 1;
	}
	
	@Override
	public void setValueType(int valueType) {
		super.setValueType(BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT);
	}
}
