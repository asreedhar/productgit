package biz.firstlook.module.vehicle.description.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.transact.persist.model.Dealership;

public class KbbConsumerValuesToolVehicleDescriptionContext implements
		VehicleDescriptionContext {

	private static final Logger logger = Logger.getLogger( BusinessUnitVehicleDescriptionContextImpl.class );
	private Map<VehicleDescriptionProviderEnum, VehicleDescriptionProvider> providerRegistry = new LinkedHashMap<VehicleDescriptionProviderEnum, VehicleDescriptionProvider>();

	public KbbConsumerValuesToolVehicleDescriptionContext( Dealership dealer )
	{
		super();
		VehicleDescriptionProvider provider = SpringBasedVehicleDescriptionContext.getInstance().getDescriptionProvider( VehicleDescriptionProviderEnum.KBB );
		if( provider != null ) {
			provider.init( dealer );
			providerRegistry.put( provider.getDescriptionProviderEnum(), provider );
		}
	}

	public VehicleDescriptionProvider getDescriptionProvider( VehicleDescriptionProviderEnum providerEnum )
	{
		VehicleDescriptionProvider provider = providerRegistry.get( providerEnum );
		if( provider == null ) {
			logger.warn( "Could not find a VehicleDescriptionProvider for " + providerEnum );
		}
		return provider;
	}

	public Collection< VehicleDescriptionProvider > getRegisteredDescriptionProviders()
	{	
		return Collections.unmodifiableCollection( providerRegistry.values() );
	}

}
