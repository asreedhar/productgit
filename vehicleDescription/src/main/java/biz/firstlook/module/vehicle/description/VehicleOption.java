package biz.firstlook.module.vehicle.description;


public interface VehicleOption {
    public String getDisplayName();
    public Integer getSortOrder();
    public String getOptionKey();
    public boolean isSelected();
    public void select();
    public void unselect();
}
