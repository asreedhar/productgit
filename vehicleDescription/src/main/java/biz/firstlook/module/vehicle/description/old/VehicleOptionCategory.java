package biz.firstlook.module.vehicle.description.old;

//from KBB.VehicleOptionCategory and join to reference table
/*
SELECT VOC.CategoryId, C.CategoryTypeId, C.DisplayName, C.CategoryTypeDisplayName
FROM KBB.KBB.VehicleOptionCategory VOC
JOIN KBB.KBB.Category C
	ON C.CategoryId = VOC.CategoryId	
	AND C.DataloadID = VOC.DataloadID
GROUP BY VOC.CategoryId, C.CategoryTypeId, C.DisplayName, C.CategoryTypeDisplayName
 */
enum VehicleOptionCategory {
	OPTION_EQUIPMENT_TYPE_ENGINE(31, 4, "Engine", "Option Equipment Type"),
	OPTION_EQUIPMENT_TYPE_TRANSMISSION(32, 4, "Transmission", "Option Equipment Type"),
	OPTION_EQUIPMENT_TYPE_DRIVETRAIN(33, 4, "Drivetrain", "Option Equipment Type"),
	APPLICATION_FILTER_ALL(34, 5, "ALL", "Application Filter"),
	APPLICATION_FILTER_CONSUMER(36, 5, "Consumer", "Application Filter"),
	APPLICATION_FILTER_DEALER(37, 5, "Dealer", "Application Filter"),
	OPTION_DRIVETRAIN(2654, 22, "Drivetrain", "Option"),
	OPTION_ENGINE(2655, 22, "Engine", "Option"),
	OPTION_TRANSMISSION(2670, 22, "Transmission", "Option");
	
	private final int categoryId; 
	private final int categoryTypeId; 
	private final String categoryDisplayName; 
	private final String categoryTypeDisplayName;
	
	private VehicleOptionCategory(
			int categoryId, 
			int categoryTypeId, 
			String categoryDisplayName, 
			String categoryTypeDisplayName) {
		this.categoryId = categoryId;
		this.categoryTypeId = categoryTypeId;
		this.categoryDisplayName = categoryDisplayName;
		this.categoryTypeDisplayName = categoryTypeDisplayName;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public int getCategoryTypeId() {
		return categoryTypeId;
	}

	public String getCategoryDisplayName() {
		return categoryDisplayName;
	}

	public String getCategoryTypeDisplayName() {
		return categoryTypeDisplayName;
	}
	
	/**
	 * @param categoryId
	 * @return the VehicleOptionCategory associated with categoryId
	 * @throws IllegalArgumentException if the categoryId is not known to be part of the business logic.
	 */
	public static VehicleOptionCategory fromId(int categoryId) {
		VehicleOptionCategory vehicleOptionCategory = null;
		for(VehicleOptionCategory c : VehicleOptionCategory.values()) {
			if(c.getCategoryId() == categoryId) {
				vehicleOptionCategory = c;
			}
		}
		
		if(vehicleOptionCategory == null) {
			throw new IllegalArgumentException("CategoryId " + categoryId + " in VehicleOptionCategory not understood by business logic.");
		}
		
		return vehicleOptionCategory;
	}
}
