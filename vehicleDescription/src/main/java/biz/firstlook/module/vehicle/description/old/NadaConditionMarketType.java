package biz.firstlook.module.vehicle.description.old;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public enum NadaConditionMarketType
{

ROUGH_TRADE_IN(ThirdPartyCategory.NADA_ROUGH_TRADE_IN, 0),
AVERAGE_TRADE_IN(ThirdPartyCategory.NADA_AVERAGE_TRADE_IN, 1),
CLEAN_TRADE_IN( ThirdPartyCategory.NADA_CLEAN_TRADE_IN, 2),
CLEAN_LOAN( ThirdPartyCategory.NADA_CLEAN_LOAN, 3),
CLEAN_RETAIL( ThirdPartyCategory.NADA_CLEAN_RETAIL, 4);

private final ThirdPartyCategory thirdPartyCategory;
private final int displayOrder;

private NadaConditionMarketType( ThirdPartyCategory thirdPartyCategory, int displayOrder )
{
	this.displayOrder = displayOrder;
	this.thirdPartyCategory = thirdPartyCategory;
}

public String getDescription()
{
	return thirdPartyCategory.getCategory();
}

public int getOrder()
{
	return displayOrder;
}

public Integer getId()
{
	return thirdPartyCategory.getThirdPartyCategoryId();
}

public static NadaConditionMarketType valueOfDescription( String description )
{
	if( description == null)
		return null;
	
	if( description.equalsIgnoreCase( CLEAN_RETAIL.getDescription() ) )
		return CLEAN_RETAIL;
	else if ( description.equalsIgnoreCase( CLEAN_TRADE_IN.getDescription() ))
		return CLEAN_TRADE_IN;
	else if (description.equalsIgnoreCase(AVERAGE_TRADE_IN.getDescription()))
		return AVERAGE_TRADE_IN;
	else if (description.equalsIgnoreCase(ROUGH_TRADE_IN.getDescription()))
		return ROUGH_TRADE_IN;
	else if ( description.equalsIgnoreCase( CLEAN_LOAN.getDescription()))
		return CLEAN_LOAN;
	else
		return null;
}

public static NadaConditionMarketType valueOf(ThirdPartyCategory thirdPartyCategory) {
	NadaConditionMarketType theValue = null;
	for(NadaConditionMarketType val : NadaConditionMarketType.values()) {
		if(val.thirdPartyCategory.equals(thirdPartyCategory)) {
			theValue = val;
			break;
		}
	}
	return theValue;
}

}