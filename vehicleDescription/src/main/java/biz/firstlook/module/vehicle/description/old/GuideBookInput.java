package biz.firstlook.module.vehicle.description.old;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

public class GuideBookInput
{
private String vin;
private String state;
private int nadaRegionCode;
private Integer mileage;
private int businessUnitId;
private Integer memberId;
private transient String selectedModelId;
private transient String make;
private transient String model;


public String getMake() {
	return make;
}

public void setMake(String make) {
	this.make = make;
}

public String getModel() {
	return model;
}

public void setModel(String model) {
	this.model = model;
}

public String getSelectedModelId() {
	return selectedModelId;
}

public void setSelectedModelId(String selectedModelId) {
	this.selectedModelId = selectedModelId;
}

public Integer getMileage()
{
	return mileage == null ? Integer.valueOf( 0 ) : mileage;
}

public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}

public int getNadaRegionCode()
{
	return nadaRegionCode;
}

public void setNadaRegionCode( int nadaRegionCode )
{
	this.nadaRegionCode = nadaRegionCode;
}

public String getState()
{
	return state;
}

public void setState( String state )
{
	this.state = state;
}

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public int getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( int businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Integer getMemberId()
{
	return memberId;
}

public void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}

public String getRegion( int thirdPartyId )
{
	switch ( thirdPartyId )
	{
		case ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE:
			return Integer.valueOf( this.getNadaRegionCode() ).toString();

		case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE:
		case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE:
		case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
			return this.getState();
		default:
			return this.getState();
	}
}

}
