package biz.firstlook.module.vehicle.description;

import java.util.HashSet;
import java.util.Set;

import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

/**
 * This class is mapped in hibernate to our old schema and part of our new schema.  This object has a list of thirdPartyVehicles,
 * which is the old part of the schema - that list is protected with its getters/setters as private.  The methods and field exist
 * only to allow us to write to the old schema.  While working with this object you should use getVehicleDescription and setVehicleDescription.
 * each concrete implmentation of this class will have a field that is a concreate VEhicleDescription.  That concrete description is adapted 
 * to/from thirdPartyVehicle when the ThirdPartyVehicles getters and settesr are called.
 * 
 * @author dweintrop
 *
 * @param <T>
 */
public abstract class VehicleBookoutState {
    
	private Integer id;
	protected String vin;
	protected Integer businessUnitId;
	
	protected Set<ThirdPartyVehicle> thirdPartyVehicles = new HashSet<ThirdPartyVehicle>();
	
	public VehicleBookoutState(String vin, Integer businessUnitId) {
		this.vin = vin;
		this.businessUnitId = businessUnitId;
	}
	
	public abstract VehicleDescription getVehicleDescription();
	public abstract void setVehicleDescription( VehicleDescription inVDesc); 
	
	public Integer getBusinessUnitId() { return businessUnitId;	}
	public String getVin() { return vin;	}
	public void setVin( String vin) {this.vin = vin; }
	public void setBusinessUnitId( Integer id ) { this.businessUnitId = id; }
	
	// methods for hibernate
	public VehicleBookoutState(){}
	protected Integer getId() { return id; }
	protected void setId(Integer id) { this.id = id; }

	/**
	 * there are a couple issues with merging the options that the client set on the vehicleDescription obejct 
	 * with the already persisted set of options on the thirdPartyVehicle objects wrapped by vehicleBookoutState.
	 * there are inline comments inside the method explaining whats going on.
	 * @param newTpv
	 */
	protected void mergeOldAndNewOptions(ThirdPartyVehicle newTpv) {
		// can't just add a new TPV onto the state because we'll end up with the same vehicle associated with the state mulitple times
		// so see if vehicle is already on the state - if so, update the options (since those can change), if not add vehicle
		boolean tpvWasInList = false;
		boolean updated = false;

		for( ThirdPartyVehicle tpv : this.thirdPartyVehicles)
		{	// didn't overload equals becuase not sure if rest of app relies on default implmenetation (id included)
			if ( tpv.getThirdPartyId().equals( newTpv.getThirdPartyId() ) &&
					tpv.getThirdPartyVehicleCode().equals( newTpv.getThirdPartyVehicleCode() ) ) {
				
				// merge list of options off vehicleDescription with persited set on thirdPartyVehicle
				for( ThirdPartyVehicleOption newOption : newTpv.getThirdPartyVehicleOptions() ) {
					updated = false;
					for ( ThirdPartyVehicleOption oldOption : tpv.getThirdPartyVehicleOptions() ) {
						// found matching optio in persisted set - copy over fields that may have changed
						if ( newOption.getOptionKey().equals( oldOption.getOptionKey() ) ) {
							oldOption.setStatus( newOption.getStatus() );
							
							// ignoring the option values stuff for now as it's not in scope of inital implemenation
							//oldOption.setCurrentOptionValue( newOption.getCurrentOptionValue() );
							//oldOption.setOptionValues( newOption.getOptionValues() );
							
							oldOption.setSortOrder( newOption.getSortOrder() );
							updated = true;
							break;
						}
					}
					// given option was not in original set of options - so must add it
					if ( !updated ) {
						newOption.setThirdPartyVehicle( tpv );
						tpv.addThirdPartyVehicleOption( newOption );
					}
				}
				tpvWasInList = true;
			} else {
				tpv.setStatus( false );
			}
		}
		if ( !tpvWasInList ) {
			for( ThirdPartyVehicleOption option : newTpv.getThirdPartyVehicleOptions() ) {
				option.setThirdPartyVehicle( newTpv );
			}
			this.thirdPartyVehicles.add( newTpv );
		}		
	}
}
