package biz.firstlook.module.vehicle.description.Enum;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

/**
 * This Enum class is the important information from dbo.ThirdParties (ThirdPartyDataProvider.java) as presented in the VehicleDescription
 * domain.
 * 
 * If there is a way to get around this hardcoding, please refactor.
 * 
 * @author bfung
 * 
 */
public enum VehicleDescriptionProviderEnum
{

	BlackBook( 1, "BlackBook", ThirdPartyDataProvider.BLACKBOOK )
	, NADA( 2, "NADA", ThirdPartyDataProvider.NADA )
	, KBB( 3, "Kelley Blue Book", ThirdPartyDataProvider.KELLEY )
	, Galves( 4, "GALVES", ThirdPartyDataProvider.GALVES );

	private Integer thirdPartyId;
	private String name;

	// Listed for compatibility
	private ThirdPartyDataProvider thirdPartyDataProvider;

	private VehicleDescriptionProviderEnum( Integer thirdPartyId, String name, ThirdPartyDataProvider thirdPartyDataProvider )
	{
		this.thirdPartyId = thirdPartyId;
		this.name = name;
		this.thirdPartyDataProvider = thirdPartyDataProvider;
	}

	public Integer getThirdPartyId()
	{
		return thirdPartyId;
	}

	public String getName()
	{
		return name;
	}
	
	public ThirdPartyDataProvider getThirdPartyDataProvider()
	{
		return thirdPartyDataProvider;
	}

	/**
	 * Returns the VehicleDescriptionProvider by Id
	 * 
	 * @param id
	 * @return VehicleDescriptionProvider. null if the id does not match any providers.
	 */
	public static VehicleDescriptionProviderEnum valueOfId( Integer id )
	{
		if ( id == null )
			throw new NullPointerException( "The id of a VehicleDescriptionProvider cannot be null." );

		for ( VehicleDescriptionProviderEnum p : VehicleDescriptionProviderEnum.values() )
			if ( id.equals( p.getThirdPartyId() ) )
				return p;

		return null;
	}
}
