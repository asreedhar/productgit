package biz.firstlook.module.vehicle.description.old;


public class GBException extends Exception
{

private static final long serialVersionUID = 4579629988396633440L;

public GBException()
{
    super();
}

public GBException( String message )
{
    super(message);
}

public GBException( String message, Throwable cause )
{
    super(message, cause);
}

public GBException( Throwable cause )
{
    super(cause);
}

}
