package biz.firstlook.module.vehicle.description.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.module.vehicle.description.VehicleBookoutState;
import biz.firstlook.module.vehicle.description.VehicleDescriptionDAO;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;

public class VehicleDescriptionDAOImpl extends HibernateDaoSupport implements VehicleDescriptionDAO
{

	private static Logger logger = Logger.getLogger( VehicleDescriptionDAOImpl.class );

	@SuppressWarnings("unchecked")
	public VehicleBookoutState getVehicleBookoutState(
			String vin, Integer businessUnitId, VehicleDescriptionProviderEnum providerEnum) 
	{
		String subTablePrefix = (VehicleDescriptionProviderEnum.KBB == providerEnum)?"Kbb":
								(VehicleDescriptionProviderEnum.BlackBook == providerEnum)?"BB":
									(VehicleDescriptionProviderEnum.NADA == providerEnum)?"NADA":
										(VehicleDescriptionProviderEnum.Galves == providerEnum)?"Galves":"";
		StringBuilder hqlQuery = new StringBuilder( " from "+subTablePrefix+"VehicleBookoutState vbState where vbState.vin = :vin " );
		hqlQuery.append( " and vbState.businessUnitId = :businessUnitId " );
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Query query = session.createQuery( hqlQuery.toString() );
		query.setParameter( "vin", vin );
		query.setParameter( "businessUnitId", businessUnitId );
		
		VehicleBookoutState vbState = null;
		try {
			vbState = (VehicleBookoutState)query.uniqueResult();
		} catch ( NonUniqueResultException e ) {
			logger.error( e );  //recover semi-gracefully, the db will be in a bad state.
			List<VehicleBookoutState> vbStates = query.list();
			if( vbStates != null && !vbStates.isEmpty())
				vbState = vbStates.get( 0 );
		}

		return vbState;
	}

	public void saveOrUpdate(VehicleBookoutState bookoutState) 
	{
		// make sure options are in a valid state before persisting.
		bookoutState.getVehicleDescription().assertSelectedOptionsSetInValidState();
		
		// update everytnig.
		getHibernateTemplate().saveOrUpdate(bookoutState);
	}

}