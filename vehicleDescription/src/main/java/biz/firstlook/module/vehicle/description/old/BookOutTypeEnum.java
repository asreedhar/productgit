package biz.firstlook.module.vehicle.description.old;

public enum BookOutTypeEnum
{

INVENTORY(1, "Inventory"),
APPRAISAL(2, "Appraisal" );

private Integer id;
private String description;

private BookOutTypeEnum( Integer id, String description )
{
	this.id = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public Integer getId()
{
	return id;
}

}
