package biz.firstlook.module.vehicle.description.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;

/**
 * This class acts as a hook into Spring's ApplicationContext.
 * @author bfung
 *
 */
public class SpringBasedVehicleDescriptionContext implements VehicleDescriptionContext, ApplicationContextAware, InitializingBean
{

private static final Logger logger = Logger.getLogger( SpringBasedVehicleDescriptionContext.class );
private static final SpringBasedVehicleDescriptionContext instance = new SpringBasedVehicleDescriptionContext();


public static SpringBasedVehicleDescriptionContext getInstance()
{
	return instance;
}

private ApplicationContext applicationContext;
private Map<VehicleDescriptionProviderEnum, String> enumsToBeanNames = new HashMap< VehicleDescriptionProviderEnum, String >();

private SpringBasedVehicleDescriptionContext()
{
}

public VehicleDescriptionProvider getDescriptionProvider( VehicleDescriptionProviderEnum providerEnum )
{
	String beanName = enumsToBeanNames.get( providerEnum );
	if( beanName != null ) {
		return (VehicleDescriptionProvider)applicationContext.getBean( beanName );
	}
	logger.info( providerEnum + " provider not found." );	
	return null;
}

public Collection< VehicleDescriptionProvider > getRegisteredDescriptionProviders()
{
	Collection<String> beanNames = enumsToBeanNames.values();
	List< VehicleDescriptionProvider > providerList = new ArrayList< VehicleDescriptionProvider >();
	for ( String name : beanNames )
	{
		VehicleDescriptionProvider provider = (VehicleDescriptionProvider)applicationContext.getBean( name );
		providerList.add( provider );
	}

	return Collections.unmodifiableList( providerList );
}

public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException
{
	this.applicationContext = applicationContext;
}

public void afterPropertiesSet() throws Exception
{
	String[] beanNames = applicationContext.getBeanNamesForType( VehicleDescriptionProvider.class );
	StringBuilder providerNames = new StringBuilder("Registered VehicleDescriptionProviders: [");
	for ( String name : beanNames )
	{
		VehicleDescriptionProvider provider = (VehicleDescriptionProvider)applicationContext.getBean( name );
		enumsToBeanNames.put( provider.getDescriptionProviderEnum(), name );
		providerNames.append(name).append(" ");
	}
	providerNames.append( "]" );
	logger.info( providerNames.toString() );
}

}
