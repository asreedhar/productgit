package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.List;

import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.compatibility.VehicleDescriptionAdapter;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;

public class KbbVDP_VehicleAdapter implements VehicleDescriptionAdapter< VDP_GuideBookVehicle > 
{
	KbbVehicleDescriptionProvider kbbVehicleDescriptionProvider;
	
	public KbbVDP_VehicleAdapter( KbbVehicleDescriptionProvider provider ) {
		this.kbbVehicleDescriptionProvider = provider;
	}
	
	public MutableVehicleDescription adaptItem(VDP_GuideBookVehicle oldVehicle) {
		KbbVehicleDescriptionImpl desc = new KbbVehicleDescriptionImpl( kbbVehicleDescriptionProvider );
		desc.setVehicleId(oldVehicle.getKey());
		desc.setMake( oldVehicle.getMake() );
		desc.setModel( oldVehicle.getModel() );
		desc.setYear( Integer.parseInt( oldVehicle.getYear() ) );
		desc.setMakeCode( new MakeId( oldVehicle.getMakeId() ) );
		desc.setModelCode( new ModelId( oldVehicle.getModelId() ) );
		desc.setTrim( oldVehicle.getTrim() );
		List<VehicleOption> options = Adapters.adapt( oldVehicle.getGuideBookOptions(), new Kbb_VehicleOptionAdapter() );
		desc.setVehicleOptions( options );
		return desc;
	}

	public VDP_GuideBookVehicle unadaptItem(MutableVehicleDescription newItem) {
		VDP_GuideBookVehicle vdp_vehicle = new VDP_GuideBookVehicle();
		KbbVehicleDescriptionImpl vDesc = (KbbVehicleDescriptionImpl)newItem;
		vdp_vehicle.setKey(vDesc.getVehicleId());
		vdp_vehicle.setMake( vDesc.getMake() );
		vdp_vehicle.setMakeId( vDesc.getMakeCode().getCode() );
		vdp_vehicle.setModel( vDesc.getModel() );
		vdp_vehicle.setYear( vDesc.getYear().toString() );
		vdp_vehicle.setModelId( vDesc.getModelCode().getCode() );
		vdp_vehicle.setTrim( vDesc.getTrim() );
		List<VehicleOption> kbbOptions = (List<VehicleOption>)vDesc.getVehicleOptions();
		vdp_vehicle.setGuideBookOptions( Adapters.unadapt( kbbOptions, new Kbb_VehicleOptionAdapter()) );
		return vdp_vehicle;
	}
}
