package biz.firstlook.module.vehicle.description.old;


public class GuideBookMetaInfo
{
private String footer;
private String publishInfo;
private String guideBookTitle;
private int guideBookId;
private String imageName;
private String bgColor;

public void init(String footer, String publishInfo, String guideBookTitle, int guideBookId, String imageName, String bgColor) throws GBException
{
	this.footer = footer;

	
	if(publishInfo == null || publishInfo.equals(""))
	{
		throw new GBException("MetaInfo init() publishInfo is not valid.");
	}	
	this.publishInfo = publishInfo;
	
	if(guideBookTitle == null || guideBookTitle.equals(""))
	{
		throw new GBException("MetaInfo init() guidebookTitle is not valid.");
	}
	this.guideBookTitle = guideBookTitle;

	
	if(guideBookId < 0)
	{
		throw new GBException("MetaInfo init() footer is not valid.");
	}		
	this.guideBookId = guideBookId;
	
	if(imageName == null || imageName.equals(""))
	{
		throw new GBException("MetaInfo init() guidebookTitle is not valid.");
	}	
	this.imageName = imageName;
	
	if(bgColor == null || bgColor.equals("") || !bgColor.startsWith("#"))
	{
		throw new GBException("MetaInfo init() bgColor is not valid. Needs format #RRGGBB");
	}
	this.bgColor = bgColor;
}

public String getFooter()
{
	return footer;
}

public void setFooter( String footer )
{
	this.footer = footer;
}

public String getPublishInfo()
{
	return publishInfo;
}

public void setPublishInfo( String publishInfo )
{
	this.publishInfo = publishInfo;
}

/**
 * @return Returns the guideBookTitle.
 */
public String getGuideBookTitle()
{
	return guideBookTitle;
}

/**
 * @param guideBookTitle The guideBookTitle to set.
 */
public void setGuideBookTitle( String guideBookTitle )
{
	this.guideBookTitle = guideBookTitle;
}

public int getGuideBookId()
{
	return guideBookId;
}

public void setGuideBookId( int guideBookId )
{
	this.guideBookId = guideBookId;
}

public String getImageName()
{
	return imageName;
}

public void setImageName( String imageName )
{
	this.imageName = imageName;
}

public String getBgColor()
{
	return bgColor;
}

public void setBgColor( String bgColor )
{
	this.bgColor = bgColor;
}

}
