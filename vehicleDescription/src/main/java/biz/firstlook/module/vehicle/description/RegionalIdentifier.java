package biz.firstlook.module.vehicle.description;

public interface RegionalIdentifier {
    String getDisplayName();
}
