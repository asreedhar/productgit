package biz.firstlook.module.vehicle.description;


public abstract class AbstractVehicleOption implements VehicleOption {

private String displayName;
private boolean selected;
private Integer sortOrder;

public String getDisplayName() { return displayName;}
public void setDisplayName(String displayName) { this.displayName = displayName;}
public boolean isSelected() { return selected;}
public void setSelected(boolean status) { this.selected = status;}
public Integer getSortOrder() {	return sortOrder;}
public void setSortOrder(Integer sortOrder)  { this.sortOrder = sortOrder;}

public void select(){ selected = true;}
public void unselect(){ selected = false;}

}
