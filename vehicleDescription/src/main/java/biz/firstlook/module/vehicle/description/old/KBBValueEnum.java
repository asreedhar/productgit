package biz.firstlook.module.vehicle.description.old;

import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;

public enum KBBValueEnum
{

	BASE( 1, "Base" ), 
	FINAL( 2, "Final" ), 
	OPTIONS( 3, "Options" ), 
	MILEAGE( 50, "Mileage" );

	private int id;
	private String description;

	private KBBValueEnum( int id, String description )
	{
		this.id = id;
		this.description = description;
	}

	public String getDescription()
	{
		return description;
	}

	public int getId()
	{
		return id;
	}

	public static KBBValueEnum getKBBValueEnumById( int id )
	{
		switch (id)
		{
			case 1:
				return BASE;
			case 2:
				return FINAL;
			case 3:
				return OPTIONS;
			case 50:
				return MILEAGE;
		}
		return null;
	}
	
	// this method maps KBB's DB idea's of category and condition to IMT's ThirdPartyCategories table
	public static ThirdPartySubCategoryEnum getThirdPartySubCategoryByConditionAndCategory( KBBConditionEnum condition, KBBCategoryEnum category )
	{
		if ( category == KBBCategoryEnum.RETAIL )
		{
			return ThirdPartySubCategoryEnum.KBB_RETAIL;
		}
		else if ( category == KBBCategoryEnum.WHOLESALE )
		{
			return ThirdPartySubCategoryEnum.KBB_WHOLESALE;
		}
		else if ( category == KBBCategoryEnum.CLASSIC_WHOLESALE )
		{
			return ThirdPartySubCategoryEnum.KBB_CLASSIC_WHOLESALE;
		}
		else if ( category == KBBCategoryEnum.TRADEIN )
		{
			if ( condition == KBBConditionEnum.EXCELLENT )
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT;
			}
			else if ( condition == KBBConditionEnum.GOOD )
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD;
			}
			else
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR;
			}
		}
		else if ( category == KBBCategoryEnum.PRIVATE_PARTY )
		{
			if ( condition == KBBConditionEnum.EXCELLENT )
			{
				return ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_EXCELLENT;
			}
			else if ( condition == KBBConditionEnum.GOOD )
			{
				return ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_GOOD;
			}
			else
			{
				return ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_FAIR;
			}
		}
		else if ( category == KBBCategoryEnum.TRADEINRANGELOW)
		{
			if ( condition == KBBConditionEnum.EXCELLENT )
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGELOW;
			}
			else if ( condition == KBBConditionEnum.GOOD )
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGELOW;
			}
			else
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGELOW;
			}
		}
		else if ( category == KBBCategoryEnum.TRADEINRANGEHIGH)
		{
			if ( condition == KBBConditionEnum.EXCELLENT )
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT_RANGEHIGH;
			}
			else if ( condition == KBBConditionEnum.GOOD )
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD_RANGEHIGH;
			}
			else
			{
				return ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR_RANGEHIGH;
			}
		}
		return ThirdPartySubCategoryEnum.N_A;
	}

	public static int getBookoutValueTypeByKBBValueEnumId( int kbbValueEnumId )
	{
		KBBValueEnum kbbValueEnum = getKBBValueEnumById( kbbValueEnumId );
		switch ( kbbValueEnum )
		{
			case BASE:
				return BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE;
			case FINAL:
				return BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE;
			case OPTIONS:
				return BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE;
			case MILEAGE:
				return BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT;
		}
		return 0;
	}

}
