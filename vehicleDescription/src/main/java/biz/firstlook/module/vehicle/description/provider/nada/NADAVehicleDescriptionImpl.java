package biz.firstlook.module.vehicle.description.provider.nada;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.commons.collections.ValidatableList;
import biz.firstlook.module.bookout.BookValueContainer;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.impl.AbstractVehicleDescriptionImpl;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class NADAVehicleDescriptionImpl extends AbstractVehicleDescriptionImpl {

	String vehicleId;
	String trim;
	String makeCode;
	String modelCode;
	String mileage;
	
	
	
	public String getMileage() {
		return mileage;
	}

	public void setMileage(String mileage) {
		this.mileage = mileage;
	}

	public String getMakeCode() {
		return makeCode;
	}

	public void setMakeCode(String makeCode) {
		this.makeCode = makeCode;
	}

	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getTrim() {
		return trim;
	}

	public void setTrim(String trim) {
		this.trim = trim;
	}

	public List<VehicleOption> getAllOptions() {
		if( this.options == null || this.options.isEmpty() ) {
			return Collections.emptyList();
		}
		List<ThirdPartyVehicleOption> selectedSet = new ArrayList<ThirdPartyVehicleOption>();
		List<ThirdPartyVehicleOption> equipmentOptions = filterOptions( ThirdPartyOptionType.EQUIPMENT );
		for( ThirdPartyVehicleOption option: equipmentOptions ) {
				selectedSet.add( option );
		}
		return Adapters.adapt( selectedSet, new NADA_VehicleOptionAdapter());
	}



	public void setSelectedVehicleOptionsByKeys(List<String> selectedOptionsKeys) {
		
		
		for( ThirdPartyVehicleOption option : this.options) {
			if ( selectedOptionsKeys.contains( option.getOptionKey() ) ) {
				option.setStatus( true );
			} else {
				option.setStatus( false );
			}
		}
		
	}

	public boolean isBookoutable() {
		return false;
	}

	@Override
	public VehicleDescriptionProviderEnum getProvider() {
		 return VehicleDescriptionProviderEnum.NADA;
	}

	@Override
	public void validate() throws OptionsConflictException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void assertSelectedOptionsSetInValidState() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BookValueContainer getBookValueContainer(Integer mileage)
			throws ValuesRetrievalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVehicleDescriptionProvider(
			VehicleDescriptionProvider provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSelectedVehicleOptions(List<VehicleOption> selectedOptions) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setVehicleOptions(List<VehicleOption> selectedOptions) {
		this.options = new ValidatableList<ThirdPartyVehicleOption>( Adapters.unadapt(selectedOptions, new NADA_VehicleOptionAdapter() ) );
		
	}

	@Override
	public List<VehicleOption> getSelectedVehicleOptions() {
		List<ThirdPartyVehicleOption> selectedSet = new ArrayList<ThirdPartyVehicleOption>();
		if( this.options == null || this.options.isEmpty() ) {
			return Collections.emptyList();
		}
		
		for( ThirdPartyVehicleOption option: this.options ) {
			if ( option.isStatus() ) {
				selectedSet.add( option );
			}
		}
		return Adapters.adapt( selectedSet, new NADA_VehicleOptionAdapter());
	}

	@Override
	public List<VehicleOption> getVehicleOptions() {
		
		return getAllOptions();
	}
	
	private List<ThirdPartyVehicleOption> filterOptions( ThirdPartyOptionType optionType ) {
		List<ThirdPartyVehicleOption> matches = new ArrayList<ThirdPartyVehicleOption>();
		for( ThirdPartyVehicleOption option : this.options) {
			if ( option.getOption().getThirdPartyOptionType().equals(optionType)) {
				matches.add( option );
			}
		}
		return matches;
	}

}
