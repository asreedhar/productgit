package biz.firstlook.module.vehicle.description.exception;

public class VehicleDescriptionNotBookoutableRuntimeException extends RuntimeException{

	private static final long serialVersionUID = 3256719585087799854L;

	public VehicleDescriptionNotBookoutableRuntimeException() {
		super("Attempt to use uninitialized VehicleDescriptionProvider on a vehicleDescription!");
	}
	
}
