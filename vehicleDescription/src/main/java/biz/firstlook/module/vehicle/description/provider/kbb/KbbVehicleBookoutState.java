package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.HashSet;
import java.util.Set;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.module.vehicle.description.VehicleBookoutState;
import biz.firstlook.module.vehicle.description.VehicleDescription;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

@SuppressWarnings("unused") // this method has unused private methods for hibernate
public class KbbVehicleBookoutState extends VehicleBookoutState {

	private Integer conditionId;
	private KbbVehicleDescriptionImpl kbbVehicleDescription;
	
	public KBBConditionEnum getCondition() {
		ThirdPartySubCategoryEnum tpSubCat = ThirdPartySubCategoryEnum.getThirdPartySubCategoryById( conditionId );
		return KBBConditionEnum.getKBBConditionEnum( tpSubCat );
	}

	public void setCondition(KBBConditionEnum condition) {
		this.conditionId = KBBConditionEnum.getThirdPartySubCategoryEnum( condition ).getThirdPartySubCategoryId();
	}
	
	public KbbVehicleBookoutState(String vin, Integer businessUnitId, KbbVehicleDescriptionImpl kbbVehicleDescription) {
		super(vin, businessUnitId);
		this.kbbVehicleDescription = kbbVehicleDescription;
	}
	
	@Override
	public KbbVehicleDescriptionImpl getVehicleDescription() {
		// check to make sure vehicleDescription has year populated since we can't populate it from ThirdPartyVehicle
		if ( kbbVehicleDescription != null && kbbVehicleDescription.getYear() == null ) {
			// get year from vin - swallow potential exception we're assuming vins in the system are in good state
			try {kbbVehicleDescription.setYear( VinUtility.getLatestModelYear( this.vin ) ); } catch (InvalidVinException e) {}
		}
		return kbbVehicleDescription;
	}

	@Override
	public void setVehicleDescription(VehicleDescription inVDesc) {
		this.kbbVehicleDescription = (KbbVehicleDescriptionImpl)inVDesc;
	}
	
	private void setThirdPartyVehicles(Set<ThirdPartyVehicle> thirdPartyVehicles) {
		this.thirdPartyVehicles = thirdPartyVehicles;
		for (ThirdPartyVehicle tpv : this.thirdPartyVehicles)
		{
			if ( VehicleDescriptionProviderEnum.KBB.getThirdPartyId().equals( tpv.getThirdPartyId() ) &&
					tpv.isStatus() ) {
				VehicleDescription vehicleDesc = new KbbTPV_VehicleAdapter().adaptItem( tpv );
				kbbVehicleDescription = (KbbVehicleDescriptionImpl)vehicleDesc;
				return;
			}
		}
	}

	private Set<ThirdPartyVehicle> getThirdPartyVehicles() { 
		ThirdPartyVehicle newTpv = new KbbTPV_VehicleAdapter().unadaptItem( kbbVehicleDescription );
		super.mergeOldAndNewOptions( newTpv );
		return this.thirdPartyVehicles;
	}

	// methods for hibernate
	public KbbVehicleBookoutState(){}
	private Integer getConditionId() { return conditionId; }
	private void setConditionId(Integer id) { this.conditionId = id; }
}
