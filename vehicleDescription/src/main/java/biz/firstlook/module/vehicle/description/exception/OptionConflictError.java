package biz.firstlook.module.vehicle.description.exception;


public class OptionConflictError implements BaseError {

	private String errorMessage;
	private String firstOptionDescription;
	private String secondOptionDescription;
	
	public OptionConflictError(String errorMessage, String option1, String option2)	{
		this.errorMessage = errorMessage;
		this.firstOptionDescription = option1;
		this.secondOptionDescription = option2;
	}

	public String getErrorMessage()	{ return errorMessage + " " + firstOptionDescription + " and " + secondOptionDescription ;	}
		
	public String getFirstOptionDescription() { return firstOptionDescription; }
	public String getSecondOptionDescription() { return secondOptionDescription; }

	public boolean equals( Object obj )
	{
	    if ( obj instanceof OptionConflictError )
	    {
	    	OptionConflictError conflict = (OptionConflictError) obj;

	        if ( this.getFirstOptionDescription().equals(conflict.getFirstOptionDescription())
	                && this.getSecondOptionDescription().equals(conflict.getSecondOptionDescription()) )
	        {
	            return true;
	        }

	        if ( this.getFirstOptionDescription().equals(conflict.getSecondOptionDescription())
	                && this.getSecondOptionDescription().equals(conflict.getFirstOptionDescription()) )
	        {
	            return true;
	        }
	    }
	    return false;
	}
	
}
