package biz.firstlook.module.vehicle.description.old;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class KBBDAO extends StoredProcedureTemplate implements IKBBDAO
{

@SuppressWarnings("unchecked")
public List<VDP_GuideBookVehicle> doVinLookup( String vin, Integer year, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@VIN", Types.VARCHAR, vin ) );
	parameters.add( new SqlParameterWithValue( "@Year", Types.INTEGER, year) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetVehiclesByVIN (?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new KBBVehicleMapper() );

	
	return (List< VDP_GuideBookVehicle >)results.get( StoredProcedureTemplate.RESULT_SET );
}

@SuppressWarnings("unchecked")
public List< ThirdPartyVehicleOption > doOptionsLookup( String vin, String vehicleId, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@VIN", Types.VARCHAR, vin ) );
	parameters.add( new SqlParameterWithValue( "@VehicleId", Types.VARCHAR, vehicleId) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetOptions(?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new KBBThirdPartyVehicleOptionMapper() );
	
	return (List< ThirdPartyVehicleOption >)results.get( StoredProcedureTemplate.RESULT_SET );
}

@SuppressWarnings("unchecked")
public List< VDP_GuideBookValue > checkForEquipmentConflicts( String vehicleId, String selectedOptions, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@VehicleId", Types.VARCHAR, vehicleId) );
	parameters.add( new SqlParameterWithValue( "@SelectedOptionList", Types.VARCHAR, selectedOptions ) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.CheckForEquipmentConflicts(?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new KBBEquipmentConflictMapper() );
	
	return (List< VDP_GuideBookValue >)results.get( StoredProcedureTemplate.RESULT_SET );
}

@SuppressWarnings("unchecked")
public KbbPriceAdjustmentsTable getValues( String vehicleId, int year, int mileage, String state, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@VehicleId", Types.VARCHAR, vehicleId) );
	parameters.add( new SqlParameterWithValue( "@Year", Types.INTEGER, year) );
	parameters.add( new SqlParameterWithValue( "@Mileage", Types.INTEGER, mileage ) );
	parameters.add( new SqlParameterWithValue( "@State", Types.VARCHAR, state) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetValues(?,?,?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new KBBValuesMapper() );
	List< VDP_GuideBookValue > values = (List< VDP_GuideBookValue >)results.get( StoredProcedureTemplate.RESULT_SET ); 
	
	return pivotResults(values);
}

private static KbbPriceAdjustmentsTable pivotResults(List< VDP_GuideBookValue > values) {
	KbbPriceAdjustmentsTable.Builder builder = new KbbPriceAdjustmentsTable.Builder();
	for(VDP_GuideBookValue value : values) {
		if(value.getThirdPartySubCategoryId() != null) {
			ThirdPartySubCategoryEnum tpsc = ThirdPartySubCategoryEnum.getThirdPartySubCategoryById(value.getThirdPartySubCategoryId()); 
			switch(tpsc) {
				case KBB_CLASSIC_WHOLESALE:
					builder.setClassicWholesale(value);
					break;
				case KBB_WHOLESALE:
					builder.setWholesale(value);
					break;
				case KBB_RETAIL:
					builder.setRetail(value);
					break;
				case KBB_TRADEIN_FAIR:
					builder.setTradeInFair(value);
					break;
				case KBB_TRADEIN_GOOD:
					builder.setTradeInGood(value);
					break;
				case KBB_TRADEIN_EXCELLENT:
					builder.setTradeInExcellent(value);
					break;
				case KBB_PRIVATE_PARTY_FAIR:
					builder.setPrivatePartyFair(value);
					break;
				case KBB_PRIVATE_PARTY_GOOD:
					builder.setPrivatePartyGood(value);
					break;
				case KBB_PRIVATE_PARTY_EXCELLENT:
					builder.setPrivatePartyExcellent(value);
					break;
				case KBB_TRADEIN_EXCELLENT_RANGELOW:
					builder.setTradeInExcellentRangeLow(value);
					break;
				case KBB_TRADEIN_EXCELLENT_RANGEHIGH:
					builder.setTradeInExcellentRangeHigh(value);
					break;
				case KBB_TRADEIN_GOOD_RANGELOW:
					builder.setTradeInGoodRangeLow(value);
					break;
				case KBB_TRADEIN_GOOD_RANGEHIGH:
					builder.setTradeInGoodRangeHigh(value);
					break;
				case KBB_TRADEIN_FAIR_RANGELOW:
					builder.setTradeInFairRangeLow(value);
					break;
				case KBB_TRADEIN_FAIR_RANGEHIGH:
					builder.setTradeInFairRangeHigh(value);
					break;
				case N_A:
					//handle some special cases
					if(value.getValueType() == BookOutValueType.KBB_CPO_MILEAGE_RESTRICTION) {
						builder.setCpoMileageRestriction(value);
					} else if (value.getValueType() == BookOutValueType.KBB_MAXIMUM_DEDUCTION_PERCENTAGE) {
						builder.setMaximumDeductionPercentage(value);
					} else if (value.getValueType() == BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT) {
						if (value instanceof KbbMileageAdjustmentValue) {
							builder.setMileageAdjustment((KbbMileageAdjustmentValue) value);
						}
					}
					break;
				default:
					//ignore other books
					break;
			}
		}
	}
	return builder.getAdjustmentsTable();
}

@SuppressWarnings("unchecked")
public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo) 
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetModelYears(?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
			
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				//ModelYear modelYear = new KbbModelYear();
				//modelYear.setId(yearId);
			 	//modelYear.setDescription(rs.getString( "Year" ));
				//modelYear.getDescriptionProvider() //returns kbb
				//return modelYear;
				return rs.getInt("YearId");
}
		}

	);
	
	return (List< Integer >)results.get( StoredProcedureTemplate.RESULT_SET );
}

@SuppressWarnings("unchecked")
public List< VDP_GuideBookVehicle > retrieveMakes( String year, BookOutDatasetInfo bookoutInfo )
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@Year", Types.INTEGER, year) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetMakes(?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
			
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
				vehicle.setMake( rs.getString( "Make" ) );
				vehicle.setMakeId( rs.getString( "MakeId" ) );
				return vehicle;
			}		
		}
	
	);
	
	return (List< VDP_GuideBookVehicle >)results.get( StoredProcedureTemplate.RESULT_SET );
}

@SuppressWarnings("unchecked")
public List< VDP_GuideBookVehicle > retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo )
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@MakeId", Types.INTEGER, makeId) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetModels(?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
			
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
				vehicle.setModelId( rs.getString( "ModelId" ) );
				vehicle.setModel( rs.getString( "Model" ) );
				return vehicle;
			}		
		}
	
	);
	
	return (List< VDP_GuideBookVehicle >)results.get( StoredProcedureTemplate.RESULT_SET );	
}

@SuppressWarnings("unchecked")
public List< VDP_GuideBookVehicle > retrieveTrims( String year, String make, String modelId, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@ModelId", Types.INTEGER, modelId) );
	parameters.add( new SqlParameterWithValue( "@Year", Types.INTEGER, year) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetTrims(?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
			
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
				vehicle.setTrim( rs.getString( "Trim" ) ); // trim display value
				vehicle.setKey( rs.getString( "VehicleId" ) ); // KBB.vehicleId. We use this rather than trimId since a trim uniqely id's a vehicle
				vehicle.setDescription(rs.getString( "Trim" ));// added this for KBB exception scenario when on selecting model.Trims will be displayed
				return vehicle;
			}		
		}
	
	);
	
	return (List< VDP_GuideBookVehicle >)results.get( StoredProcedureTemplate.RESULT_SET );	
}

@SuppressWarnings("unchecked")
public Map< String, String> getMetaInfo( BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetReleaseVersion(?,?)}");
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				Map<String, String> result = new HashMap< String, String>();
				result.put( "timePeriod", rs.getString( "Description" ) );
				result.put( "version", rs.getString( "ReleaseVersion" ) );
				return result;
			}		
		}
	); 
	return ((List<Map<String, String>>)results.get( StoredProcedureTemplate.RESULT_SET )).get(0);
}

@SuppressWarnings("unchecked")
public List< String > checkForOptionConflictsWithStdOptions( StringBuilder missingStdOptionCodes, StringBuilder selectedOptionCodes, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@MissingStandardOptionList", Types.VARCHAR, missingStdOptionCodes.toString() ) );
	parameters.add( new SqlParameterWithValue( "@SelectedOptionList", Types.VARCHAR, selectedOptionCodes.toString() ) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.CheckForMissingStandardOptionConflicts(?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				return rs.getString( "VehicleOptionId" );
			}		
		}
	); 
	return (List< String >)results.get( StoredProcedureTemplate.RESULT_SET );
}


@SuppressWarnings("unchecked")
public List< VDP_GuideBookVehicle > retrieveModelsByMakeIDAndYear( String vin, Integer year, BookOutDatasetInfo bookoutInfo )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@VIN", Types.VARCHAR, vin ) );
	parameters.add( new SqlParameterWithValue( "@Year", Types.INTEGER, year) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetModelsByMakeIdAndYear(?,?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
			
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
				vehicle.setModelId( rs.getString( "ModelId" ) );
				vehicle.setModel( rs.getString( "Model" ) );
				vehicle.setDescription(rs.getString( "Model" ));
				vehicle.setKey( rs.getString( "ModelId" ) );
				vehicle.setMakeId(rs.getString("MakeId"));
				return vehicle;
			}		
		}
	
	);
	
	return (List< VDP_GuideBookVehicle >)results.get( StoredProcedureTemplate.RESULT_SET );	
}

public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId, String modelId, BookOutDatasetInfo bookoutInfo) {
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
	parameters.add( new SqlParameterWithValue( "@ModelId", Types.INTEGER, modelId) );
	parameters.add( new SqlParameterWithValue( "@BusinessUnitID", Types.INTEGER, bookoutInfo.getBusinessUnitId() ) );
	parameters.add( new SqlParameterWithValue( "@BookoutTypeID", Types.INTEGER, bookoutInfo.getBookoutTypeEnum().getId() ) );
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call KBB.GetTrimsWithoutYear(?,?,?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );
	
	Map results = this.call( sprocRetrieverParams, new RowMapper(){
			
		public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
			{
				VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
				vehicle.setTrim( rs.getString( "Trim" ) ); // trim display value
				vehicle.setKey( rs.getString( "VehicleId" ) ); // KBB.vehicleId. We use this rather than trimId since a trim uniqely id's a vehicle
				vehicle.setDescription(rs.getString( "Trim" ));// added this for KBB exception scenario when on selecting model.Trims will be displayed
				vehicle.setBody(rs.getString( "Trim" ));
				return vehicle;
			}		
		}
	
	);
	
	return (List< VDP_GuideBookVehicle >)results.get( StoredProcedureTemplate.RESULT_SET );	
}


}
