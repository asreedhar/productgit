package biz.firstlook.module.vehicle.description.provider.kbb;

import java.io.Serializable;

import biz.firstlook.module.vehicle.description.RegionalIdentifier;

public class KbbRegionalIdentifier implements RegionalIdentifier, Serializable
{

private static final long serialVersionUID = -478040688924453411L;

private String stateCode;

public KbbRegionalIdentifier( String stateCode ) {
	this.stateCode = stateCode;
}

public String getDisplayName()
{
	return stateCode;
}

public String getStateCode()
{
	return stateCode;
}

}
