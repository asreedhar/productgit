package biz.firstlook.module.vehicle.description.old;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class KBBVehicleMapper implements RowMapper
{

public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
{
	VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
	vehicle.setDescription( rs.getString(  "Model" ) + " " + rs.getString( "Trim" )); 
	vehicle.setKey( rs.getString( "VehicleId") );
	vehicle.setMake( rs.getString( "Make" ) );
	vehicle.setMakeId( rs.getString( "MakeId") );
	vehicle.setModel( rs.getString( "Model") );
	vehicle.setModelId( rs.getString( "ModelId") );
	vehicle.setSeries( rs.getString(  "Model" ) + " " + rs.getString( "Trim" ));
	vehicle.setTrim( rs.getString( "Trim" ) );
	vehicle.setBody( rs.getString( "Trim" ) );
	vehicle.setYear( rs.getString( "Year") );

	return vehicle;
}

}
