package biz.firstlook.module.vehicle.description.provider.nada;

import java.util.Set;

import biz.firstlook.module.vehicle.description.VehicleBookoutState;
import biz.firstlook.module.vehicle.description.VehicleDescription;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;

public class NADAVehicleBookoutState extends VehicleBookoutState {
	
	NADAVehicleDescriptionImpl nadaVehicle;
	private Integer thirdPartyId=2;
	@Override
	public NADAVehicleDescriptionImpl getVehicleDescription() {
		// TODO Auto-generated method stub
		return nadaVehicle;
	}

	@Override
	public void setVehicleDescription(VehicleDescription inVDesc) {
		
		nadaVehicle= (NADAVehicleDescriptionImpl) inVDesc;
		
	}
	public NADAVehicleBookoutState() {
		// TODO Auto-generated constructor stub
	}

	public NADAVehicleBookoutState(String vin, Integer businessUnitId, NADAVehicleDescriptionImpl nadaVehicle) {
		super(vin, businessUnitId);
		this.nadaVehicle = nadaVehicle;
	}
	
	public void setThirdPartyVehicles(Set<ThirdPartyVehicle> thirdPartyVehicles) {
		this.thirdPartyVehicles = thirdPartyVehicles;
		for (ThirdPartyVehicle tpv : this.thirdPartyVehicles)
		{
			if ( VehicleDescriptionProviderEnum.NADA.getThirdPartyId().equals( tpv.getThirdPartyId() ) &&
					tpv.isStatus() ) {
				VehicleDescription vehicleDesc = new NADATPV_VehicleAdapter().adaptItem( tpv );
				nadaVehicle = (NADAVehicleDescriptionImpl)vehicleDesc;
				return;
			}
		}
	}

	public Set<ThirdPartyVehicle> getThirdPartyVehicles() { 
		ThirdPartyVehicle newTpv = new NADATPV_VehicleAdapter().unadaptItem( nadaVehicle );
		super.mergeOldAndNewOptions( newTpv );
		return this.thirdPartyVehicles;
	}

	public Integer getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(Integer thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}
	
}
