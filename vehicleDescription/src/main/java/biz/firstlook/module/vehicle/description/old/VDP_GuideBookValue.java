package biz.firstlook.module.vehicle.description.old;

import java.io.Serializable;

import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;


public class VDP_GuideBookValue implements Serializable
{

private static final long serialVersionUID = 8450646908095630069L;

private Integer value; // Dollar value of the vehicle

private int valueType; // BOOK_OUT_BASE_FINAL_VALUE or BOOK_OUT_VALUE_FINAL_VALUE

//These 2 represent the same thing: Retail, Trade-in, Rough etc.
private int thirdPartyCategoryId;
private String thirdPartyCategoryDescription;
private Double percentageAdjustment; // ONLY used by KBB mileage adjustments.

// for black book/nada/galves and kbb retail/wholesale these are the same as third party category
// for kbb trade in these values store condition excellent/good/fair -> they map the thirdPartySubCategory table
private Integer thirdPartySubCategoryId;

private boolean hasErrors;
private BookOutError error;

public VDP_GuideBookValue()
{
}

/**
 * this constructor should not be used with KBB - KBB requires a ThirdPartySubCategory!!!
 */
public VDP_GuideBookValue( Integer value, int thirdPartyCategoryId, String thirdPartyCategoryDescription, int valueType )
{
	super();
	this.value = value;
	this.thirdPartyCategoryId = thirdPartyCategoryId;
	this.thirdPartySubCategoryId = thirdPartyCategoryId;
	this.thirdPartyCategoryDescription = thirdPartyCategoryDescription;
	this.valueType = valueType;
}

/**
 * this constructor should be used with KBB since a thirdPartySubCategoryId is required
 */
public VDP_GuideBookValue( Integer value, int thirdPartyCategoryId, String thirdPartyCategoryDescription, int valueType, int thirdPartySubCategoryId )
{
	super();
	this.value = value;
	this.thirdPartyCategoryId = thirdPartyCategoryId;
	this.thirdPartySubCategoryId = thirdPartySubCategoryId;
	this.thirdPartyCategoryDescription = thirdPartyCategoryDescription;
	this.valueType = valueType;
}

public Integer getValue()
{
	return value;
}
public void setValue( Integer value )
{
	this.value = value;
}
public int getThirdPartyCategoryId()
{
	return thirdPartyCategoryId;
}
public void setThirdPartyCategoryId( int valueType )
{
	this.thirdPartyCategoryId = valueType;
}

public String toString()
{
	StringBuilder sb = new StringBuilder("GUIDE BOOK VALUE: ");
	ThirdPartyCategory tpc = ThirdPartyCategory.fromId(thirdPartyCategoryId);
	sb.append(tpc.getCategory());
	
	ThirdPartySubCategoryEnum subCat = ThirdPartySubCategoryEnum.getThirdPartySubCategoryById(thirdPartySubCategoryId);
	sb.append(subCat.getThirdPartySubCategoryDescription()).append(", ");
	
	switch(valueType) {
		case BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT:
			sb.append("Mileage Adjustment");
			break;
		case BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE:
			sb.append("Base");
			break;
		case BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE:
			sb.append("Final");
			break;
		case BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT:
			sb.append("Final excluding MileageAdjustment");
			break;
		case BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE:
			sb.append("Option Adjustment");
			break;
		default:
			break;
	}
	
	sb.append(" Value: ").append(value);
	return sb.toString();
}

public String getThirdPartyCategoryDescription()
{
	return thirdPartyCategoryDescription;
}

public void setThirdPartyCategoryDescription( String thirdPartyCategoryDescription )
{
	this.thirdPartyCategoryDescription = thirdPartyCategoryDescription;
}

/**
 * Get the value type.  The value types are: Base value, mileage adjusted, final bookout value type
 */
public int getValueType()
{
	return valueType;
}

/**
 * Set the value type.  Use BookoutValueType.(valuetype)
 */
public void setValueType( int valueType )
{
	this.valueType = valueType;
}

public BookOutError getError()
{
	return error;
}

public void setError( BookOutError error )
{
	this.error = error;
}

public boolean isHasErrors()
{
	return hasErrors;
}

public void setHasErrors( boolean hasErrors )
{
	this.hasErrors = hasErrors;
}

public String getThirdPartySubCategoryDescription()
{
	return ThirdPartySubCategoryEnum.getThirdPartySubCategoryById( getThirdPartySubCategoryId() ).getThirdPartySubCategoryDescription();
}

public Integer getThirdPartySubCategoryId()
{
	// this is just protection against non-kbb trade-in values not handling the subcategory stuff (since it doesn't need to)
	if ( thirdPartySubCategoryId != null )
	{
		return thirdPartySubCategoryId;
	}
	return thirdPartyCategoryId;
}

public void setThirdPartySubCategoryId( Integer thirdPartySubCategoryId )
{
	this.thirdPartySubCategoryId = thirdPartySubCategoryId;
}

public Double getPercentageAdjustment() {
	return percentageAdjustment;
}

public void setPercentageAdjustment(Double percentageAdjustment) {
	this.percentageAdjustment = percentageAdjustment;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + thirdPartyCategoryId;
	result = prime * result + ((value == null) ? 0 : value.hashCode());
	result = prime * result + valueType;
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	VDP_GuideBookValue other = (VDP_GuideBookValue) obj;
	if (thirdPartyCategoryId != other.thirdPartyCategoryId)
		return false;
	if (value == null) {
		if (other.value != null)
			return false;
	} else if (!value.equals(other.value))
		return false;
	if (valueType != other.valueType)
		return false;
	return true;
}

}
