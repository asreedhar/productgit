package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;


class KbbPriceAdjustmentsTable {

	private final VDP_GuideBookValue wholesale;
	private final VDP_GuideBookValue retail;
	
	private final VDP_GuideBookValue tradeInFair;
	private final VDP_GuideBookValue tradeInFairRangeLow;
	private final VDP_GuideBookValue tradeInFairRangeHigh;
	private final VDP_GuideBookValue tradeInGood;
	private final VDP_GuideBookValue tradeInGoodRangeLow;
	private final VDP_GuideBookValue tradeInGoodRangeHigh;
	private final VDP_GuideBookValue tradeInExcellent;
	private final VDP_GuideBookValue tradeInExcellentRangeLow;
	private final VDP_GuideBookValue tradeInExcellentRangeHigh;
	
	private final VDP_GuideBookValue privatePartyFair;
	private final VDP_GuideBookValue privatePartyGood;
	private final VDP_GuideBookValue privatePartyExcellent;
	
	private final VDP_GuideBookValue auctionFair;
	private final VDP_GuideBookValue auctionGood;
	private final VDP_GuideBookValue auctionExcellent;
	
	private final List<VDP_GuideBookValue> baseValues;
	
	private final VDP_GuideBookValue msrp;
	
	private final KbbMileageAdjustmentValue mileageAdjustment;
	private final VDP_GuideBookValue maximumDeductionPercentage;
	private final VDP_GuideBookValue cpoMileageRestriction;
	
	public KbbPriceAdjustmentsTable(VDP_GuideBookValue wholesale,
			VDP_GuideBookValue retail, VDP_GuideBookValue tradeInFair,
			VDP_GuideBookValue tradeInFairRangeLow,
			VDP_GuideBookValue tradeInFairRangeHigh,
			VDP_GuideBookValue tradeInGood,
			VDP_GuideBookValue tradeInGoodRangeLow,
			VDP_GuideBookValue tradeInGoodRangeHigh,
			VDP_GuideBookValue tradeInExcellent,
			VDP_GuideBookValue tradeInExcellentRangeLow,
			VDP_GuideBookValue tradeInExcellentRangeHigh,
			VDP_GuideBookValue privatePartyFair,
			VDP_GuideBookValue privatePartyGood,
			VDP_GuideBookValue privatePartyExcellent,
			VDP_GuideBookValue auctionFair, VDP_GuideBookValue auctionGood,
			VDP_GuideBookValue auctionExcellent, VDP_GuideBookValue msrp,
			KbbMileageAdjustmentValue mileageAdjustment,
			VDP_GuideBookValue maximumDeductionPercentage,
			VDP_GuideBookValue cpoMileageRestriction) {
		super();
		this.wholesale = wholesale;
		this.retail = retail;
		this.tradeInFair = tradeInFair;
		this.tradeInFairRangeLow = tradeInFairRangeLow;
		this.tradeInFairRangeHigh = tradeInFairRangeHigh;
		this.tradeInGood = tradeInGood;
		this.tradeInGoodRangeLow = tradeInGoodRangeLow;
		this.tradeInGoodRangeHigh = tradeInGoodRangeHigh;
		this.tradeInExcellent = tradeInExcellent;
		this.tradeInExcellentRangeLow = tradeInExcellentRangeLow;
		this.tradeInExcellentRangeHigh = tradeInExcellentRangeHigh;
		this.privatePartyFair = privatePartyFair;
		this.privatePartyGood = privatePartyGood;
		this.privatePartyExcellent = privatePartyExcellent;
		this.auctionFair = auctionFair;
		this.auctionGood = auctionGood;
		this.auctionExcellent = auctionExcellent;
		this.msrp = msrp;
		this.mileageAdjustment = mileageAdjustment;
		this.maximumDeductionPercentage = maximumDeductionPercentage;
		this.cpoMileageRestriction = cpoMileageRestriction;
		
		//should really be a Set, but the UI might depend on the order.
		//don't add the value in if the value is null!
		List<VDP_GuideBookValue> theBaseValues = new ArrayList<VDP_GuideBookValue>(){
			private static final long serialVersionUID = 7424065013446352803L;

			@Override
			public boolean add(VDP_GuideBookValue o) {
				if(o != null) {
					return super.add(o);
				} else {
					return false;
				}
			}
		};
		theBaseValues.add(wholesale);
		theBaseValues.add(retail);
		theBaseValues.add(tradeInFair);
		theBaseValues.add(tradeInFairRangeLow);
		theBaseValues.add(tradeInFairRangeHigh);
		theBaseValues.add(tradeInGood);
		theBaseValues.add(tradeInGoodRangeLow);
		theBaseValues.add(tradeInGoodRangeHigh);
		theBaseValues.add(tradeInExcellent);
		theBaseValues.add(tradeInExcellentRangeLow);
		theBaseValues.add(tradeInExcellentRangeHigh);
		theBaseValues.add(privatePartyFair);
		theBaseValues.add(privatePartyGood);
		theBaseValues.add(privatePartyExcellent);
		theBaseValues.add(auctionFair);
		theBaseValues.add(auctionGood);
		theBaseValues.add(auctionExcellent);
		baseValues = Collections.unmodifiableList(theBaseValues);
	}
	
	public VDP_GuideBookValue getWholesale() {
		return wholesale;
	}
	public VDP_GuideBookValue getRetail() {
		return retail;
	}
	public VDP_GuideBookValue getTradeInFair() {
		return tradeInFair;
	}
	public VDP_GuideBookValue getTradeInGood() {
		return tradeInGood;
	}
	public VDP_GuideBookValue getTradeInExcellent() {
		return tradeInExcellent;
	}
	public VDP_GuideBookValue getPrivatePartyFair() {
		return privatePartyFair;
	}
	public VDP_GuideBookValue getPrivatePartyGood() {
		return privatePartyGood;
	}
	public VDP_GuideBookValue getPrivatePartyExcellent() {
		return privatePartyExcellent;
	}
	public VDP_GuideBookValue getAuctionFair() {
		return auctionFair;
	}
	public VDP_GuideBookValue getAuctionGood() {
		return auctionGood;
	}
	public VDP_GuideBookValue getAuctionExcellent() {
		return auctionExcellent;
	}
	public VDP_GuideBookValue getMsrp() {
		return msrp;
	}
	public VDP_GuideBookValue getMileageAdjustment() {
		return mileageAdjustment;
	}
	public VDP_GuideBookValue getMaximumDeductionPercentage() {
		return maximumDeductionPercentage;
	}
	public VDP_GuideBookValue getCpoMileageRestriction() {
		return cpoMileageRestriction;
	}
	
	public VDP_GuideBookValue getTradeInFairRangeLow() {
		return tradeInFairRangeLow;
	}

	public VDP_GuideBookValue getTradeInFairRangeHigh() {
		return tradeInFairRangeHigh;
	}

	public VDP_GuideBookValue getTradeInGoodRangeLow() {
		return tradeInGoodRangeLow;
	}

	public VDP_GuideBookValue getTradeInGoodRangeHigh() {
		return tradeInGoodRangeHigh;
	}

	public VDP_GuideBookValue getTradeInExcellentRangeLow() {
		return tradeInExcellentRangeLow;
	}

	public VDP_GuideBookValue getTradeInExcellentRangeHigh() {
		return tradeInExcellentRangeHigh;
	}

	/**
	 * 
	 * @return An unmodifiable collection that only includes values of Category(Condition):
	 * <ul>
	 * <li>Wholesale(N/A)</li>
	 * <li>Retail(N/A)</li>
	 * <li>Trade-In(Fair)</li>
	 * <li>Trade-In(Good)</li>
	 * <li>Trade-In(Excellent)</li>
	 * <li>Private Party(Fair)</li>
	 * <li>Private Party(Good)</li>
	 * <li>Private Party(Excellent)</li>
	 * <li>Auction Fair(N/A)</li>
	 * <li>Auction Good(N/A)</li>
	 * <li>Auction Excellent(N/A)</li>
	 * </ul>
	 * Other values can be retrieved from calling the appropriate method exposed by this class.
	 */
	public List<VDP_GuideBookValue> getBaseValues() {
		return baseValues;
	}
	
	/**
	 * A builder to build a KbbPriceAdjustmentsTable object.
	 * @author bfung
	 */
	static class Builder {
		private VDP_GuideBookValue classicWholesale;
		
		private VDP_GuideBookValue wholesale;
		private VDP_GuideBookValue retail;
		
		private VDP_GuideBookValue tradeInFair;
		private VDP_GuideBookValue tradeInFairRangeLow;
		private VDP_GuideBookValue tradeInFairRangeHigh;
		private VDP_GuideBookValue tradeInGood;
		private VDP_GuideBookValue tradeInGoodRangeLow;
		private VDP_GuideBookValue tradeInGoodRangeHigh;
		private VDP_GuideBookValue tradeInExcellent;
		private VDP_GuideBookValue tradeInExcellentRangeLow;
		private VDP_GuideBookValue tradeInExcellentRangeHigh;
		
		private VDP_GuideBookValue privatePartyFair;
		private VDP_GuideBookValue privatePartyGood;
		private VDP_GuideBookValue privatePartyExcellent;
		
		private VDP_GuideBookValue auctionFair;
		private VDP_GuideBookValue auctionGood;
		private VDP_GuideBookValue auctionExcellent;
		
		private VDP_GuideBookValue msrp;
		
		private KbbMileageAdjustmentValue mileageAdjustment = new KbbMileageAdjustmentValue(null, null, 0);
		private VDP_GuideBookValue maximumDeductionPercentage;
		private VDP_GuideBookValue cpoMileageRestriction;
		
		public Builder(){}

		public void setClassicWholesale(VDP_GuideBookValue classicWholesale) {
			this.classicWholesale = classicWholesale;
		}

		public void setWholesale(VDP_GuideBookValue wholesale) {
			this.wholesale = wholesale;
		}

		public void setRetail(VDP_GuideBookValue retail) {
			this.retail = retail;
		}

		public void setTradeInFair(VDP_GuideBookValue tradeInFair) {
			this.tradeInFair = tradeInFair;
		}

		public void setTradeInGood(VDP_GuideBookValue tradeInGood) {
			this.tradeInGood = tradeInGood;
		}

		public void setTradeInExcellent(VDP_GuideBookValue tradeInExcellent) {
			this.tradeInExcellent = tradeInExcellent;
		}

		public void setPrivatePartyFair(VDP_GuideBookValue privatePartyFair) {
			this.privatePartyFair = privatePartyFair;
		}

		public void setPrivatePartyGood(VDP_GuideBookValue privatePartyGood) {
			this.privatePartyGood = privatePartyGood;
		}

		public void setPrivatePartyExcellent(VDP_GuideBookValue privatePartyExcellent) {
			this.privatePartyExcellent = privatePartyExcellent;
		}

		public void setAuctionFair(VDP_GuideBookValue auctionFair) {
			this.auctionFair = auctionFair;
		}

		public void setAuctionGood(VDP_GuideBookValue auctionGood) {
			this.auctionGood = auctionGood;
		}

		public void setAuctionExcellent(VDP_GuideBookValue auctionExcellent) {
			this.auctionExcellent = auctionExcellent;
		}

		public void setMsrp(VDP_GuideBookValue msrp) {
			this.msrp = msrp;
		}

		public void setMileageAdjustment(KbbMileageAdjustmentValue mileageAdjustment) {
			this.mileageAdjustment = mileageAdjustment;
		}

		public void setMaximumDeductionPercentage(
				VDP_GuideBookValue maximumDeductionPercentage) {
			this.maximumDeductionPercentage = maximumDeductionPercentage;
		}

		public void setCpoMileageRestriction(VDP_GuideBookValue cpoMileageRestriction) {
			this.cpoMileageRestriction = cpoMileageRestriction;
		}
		
		public void setTradeInFairRangeLow(VDP_GuideBookValue tradeInFairRangeLow) {
			this.tradeInFairRangeLow = tradeInFairRangeLow;
		}

		public void setTradeInFairRangeHigh(VDP_GuideBookValue tradeInFairRangeHigh) {
			this.tradeInFairRangeHigh = tradeInFairRangeHigh;
		}

		public void setTradeInGoodRangeLow(VDP_GuideBookValue tradeInGoodRangeLow) {
			this.tradeInGoodRangeLow = tradeInGoodRangeLow;
		}

		public void setTradeInGoodRangeHigh(VDP_GuideBookValue tradeInGoodRangeHigh) {
			this.tradeInGoodRangeHigh = tradeInGoodRangeHigh;
		}

		public void setTradeInExcellentRangeLow(
				VDP_GuideBookValue tradeInExcellentRangeLow) {
			this.tradeInExcellentRangeLow = tradeInExcellentRangeLow;
		}

		public void setTradeInExcellentRangeHigh(
				VDP_GuideBookValue tradeInExcellentRangeHigh) {
			this.tradeInExcellentRangeHigh = tradeInExcellentRangeHigh;
		}

		public KbbPriceAdjustmentsTable getAdjustmentsTable() {
			if(mileageAdjustment.isPercentage()) {
				mileageAdjustment.setValue(
						calculatePercentageMileageAdj(
								mileageAdjustment.getPercentageAdjustment(), 
								wholesale, 
								classicWholesale));
			}
			
			return new KbbPriceAdjustmentsTable(
					wholesale, retail, 
					tradeInFair, tradeInFairRangeLow, tradeInFairRangeHigh,
					tradeInGood,tradeInGoodRangeLow, tradeInGoodRangeHigh,
					tradeInExcellent,tradeInExcellentRangeLow,tradeInExcellentRangeHigh,
					privatePartyFair, privatePartyGood, privatePartyExcellent,
					auctionFair, auctionGood, auctionExcellent,
					msrp, mileageAdjustment, maximumDeductionPercentage, cpoMileageRestriction);
		}
		
		//returns the mileage adjustment as a percentage of wholesale value - rounded to 25 bucks
		private static Integer calculatePercentageMileageAdj( final Double percent, final VDP_GuideBookValue wholesaleValue, final VDP_GuideBookValue classicWholesaleValue)
		{
			//validate
			if (wholesaleValue != null &&
				!ThirdPartySubCategoryEnum.KBB_WHOLESALE.getThirdPartySubCategoryId().equals(wholesaleValue.getThirdPartySubCategoryId())) {
				throw new IllegalArgumentException("Expected wholesale value to have ThirdPartySubCategoryId = " + ThirdPartySubCategoryEnum.KBB_WHOLESALE.getThirdPartySubCategoryId() + " but was " + wholesaleValue.getThirdPartySubCategoryId());
			}
			
			if (classicWholesaleValue != null &&
				!ThirdPartySubCategoryEnum.KBB_CLASSIC_WHOLESALE.getThirdPartySubCategoryId().equals(classicWholesaleValue.getThirdPartySubCategoryId())) {
				throw new IllegalArgumentException("Expected wholesale value to have ThirdPartySubCategoryId = " + ThirdPartySubCategoryEnum.KBB_CLASSIC_WHOLESALE.getThirdPartySubCategoryId() + " but was " + classicWholesaleValue.getThirdPartySubCategoryId());
			}
			
			Integer value = classicWholesaleValue != null ? classicWholesaleValue.getValue() : wholesaleValue.getValue();
			
			// this is part if the contract with the stored prc and is done to maintain significant digits while keeping the data type an int
			double rawMileageAdj = ((double)(value * percent));
			
			// now round to nearest 25 dollars
			double temp = rawMileageAdj/25.0d;
			long mileageAdj = Math.round(temp) * 25l;
			
			return (int)mileageAdj;
		}
	}
}
