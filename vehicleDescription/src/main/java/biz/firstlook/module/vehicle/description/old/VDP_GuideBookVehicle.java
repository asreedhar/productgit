package biz.firstlook.module.vehicle.description.old;

import java.io.Serializable;
import java.util.List;

import biz.firstlook.commons.util.StringConstants;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

/**
 * NK  - This should be an abstract class!
 *
 */
public class VDP_GuideBookVehicle implements Serializable
{

private static final long serialVersionUID = -5390654337731649052L;

private String vin;
private String year;
private String make;
private int mileage;
private String model;
private String trim;
private String series;
private String body;
private String engineType; // used by galves
private int mileageAdjustment;
private String key; // VIC for NADA, UVC for Blackbook, Make Code for KBB.
private String modelId;
private String makeId;
private String description;
private List<ThirdPartyVehicleOption> guideBookOptions;
private KBBConditionEnum condition; // for KBB

private String vic; //NADA and NAAA integration.

private Integer msrp;
private Integer weight;

public VDP_GuideBookVehicle()
{
	super();
}

public java.lang.String getBody()
{
	return body;
}

public java.lang.String getKey()
{
	return key;
}

public java.lang.String getMake()
{
	return make;
}

public int getMileage()
{
	return mileage;
}

public int getMileageAdjustment()
{
	return mileageAdjustment;
}

public java.lang.String getModel()
{
	return model;
}

public java.lang.String getSeries()
{
	return series;
}

public java.lang.String getTrim()
{
	return trim;
}

public java.lang.String getVin()
{
	return vin;
}

public java.lang.String getYear()
{
	return year;
}

public String getEngineType()
{
	return engineType;
}

public void setEngineType( String engineType )
{
	this.engineType = engineType;
}

public void setBody( java.lang.String newBody )
{
	body = newBody;
}

public void setKey( java.lang.String newKey )
{
	key = newKey;
}

public void setMake( java.lang.String newMake )
{
	make = newMake;
}

public void setMileage( int newMileage )
{
	mileage = newMileage;
}

public void setMileageAdjustment( int newMileageAdjustment )
{
	mileageAdjustment = newMileageAdjustment;
}

public void setModel( java.lang.String newModel )
{
	model = newModel;
}

public void setSeries( java.lang.String newSeries )
{
	series = newSeries;
}

public void setTrim( java.lang.String newTrim )
{
	trim = newTrim;
}

public void setVin( java.lang.String newVin )
{
	vin = newVin;
}

public void setYear( java.lang.String newYear )
{
	year = newYear;
}

public java.lang.String getModelId()
{
	return modelId;
}

public void setModelId( java.lang.String string )
{
	modelId = string;
}

public String getDescription()
{
	return description;
}

public void setDescription( String selectedKeyDescription )
{
	this.description = selectedKeyDescription;
}
public String getMakeId()
{
	return makeId;
}

public void setMakeId( String string )
{
	makeId = string;
}

public String toString()
{
	StringBuilder s = new StringBuilder();
	s.append("VDP_GuideBookVehicle {").append(StringConstants.NEW_LINE);
	s.append("  VIN: ").append(vin).append(StringConstants.NEW_LINE);
	s.append("  Key: ").append(key).append(StringConstants.NEW_LINE);;
	s.append("  Year: ").append(year).append(StringConstants.NEW_LINE);
	s.append("  Make: ").append(make).append(StringConstants.NEW_LINE);
	s.append("  Model: ").append(model).append(StringConstants.NEW_LINE);
	s.append("  Trim: ").append(trim).append(StringConstants.NEW_LINE);
	s.append("  Series: ").append(series).append(StringConstants.NEW_LINE);
	s.append("  Body: ").append(body).append(StringConstants.NEW_LINE);
	s.append("  MakeCode: ").append(makeId).append(StringConstants.NEW_LINE);
	s.append("  ModelCode: ").append(modelId).append(StringConstants.NEW_LINE);
	s.append("}");
	
	return s.toString();
}

public boolean equals( Object obj )
{
	if ( obj == null || !(obj instanceof VDP_GuideBookVehicle) )
	{
		return false;
	}
	VDP_GuideBookVehicle inVehicle = (VDP_GuideBookVehicle)obj;
	
	if ( this.body.equals( inVehicle.getBody() ) &&
		 this.description.equals( inVehicle.getDescription() ) &&
		 this.key.equals( inVehicle.getKey() ) &&
		 this.make.equals( inVehicle.getMake() ) &&
		 this.makeId.equals( inVehicle.getMakeId() ) &&
		 this.model.equals( inVehicle.getModel() ) &&
		 this.modelId.equals( inVehicle.getModelId() ) &&
		 this.trim.equals( inVehicle.getTrim() ) &&
		 this.series.equals( inVehicle.getSeries() ) &&
		 this.year.equals( inVehicle.getYear() ) )
	{
		return true;
	}
	
	return false;
}

public List<ThirdPartyVehicleOption> getGuideBookOptions()
{
	return guideBookOptions;
}

public void setGuideBookOptions( List<ThirdPartyVehicleOption> guideBookOptions )
{
	this.guideBookOptions = guideBookOptions;
}

public KBBConditionEnum getCondition()
{
	return condition;
}

public void setCondition( KBBConditionEnum condition )
{
	this.condition = condition;
}

public String getVic() {
	return vic;
}

public void setVic(String vic) {
	this.vic = vic;
}

public Integer getWeight() {
	return weight;
}

public void setWeight(Integer weight) {
	this.weight = weight;
}

public Integer getMsrp() {
	return msrp;
}

public void setMsrp(Integer msrp) {
	this.msrp = msrp;
}
}
