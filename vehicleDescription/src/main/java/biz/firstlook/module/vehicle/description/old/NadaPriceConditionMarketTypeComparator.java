/**
 * 
 */
package biz.firstlook.module.vehicle.description.old;

import java.io.Serializable;
import java.util.Comparator;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public class NadaPriceConditionMarketTypeComparator implements Comparator<VDP_GuideBookValue>, Serializable {

	private static final long serialVersionUID = 1198588874996590553L;

	public int compare(VDP_GuideBookValue v0, VDP_GuideBookValue v1) {
		if(v0 == null || v1 == null)
			throw new NullPointerException("Cannot compare null VDP_GuideBookValues!");
		
		ThirdPartyCategory t0 = ThirdPartyCategory.fromId(v0.getThirdPartyCategoryId());
		ThirdPartyCategory t1 = ThirdPartyCategory.fromId(v1.getThirdPartyCategoryId());
		
		if(t0 == null || t1 == null)
			throw new NullPointerException("Cannot compare VDP_GuideBookValues, could not find the ThirdPartyCategory specified by VDP_GuideBookValue!");
		
		NadaConditionMarketType n0 = NadaConditionMarketType.valueOf(t0);
		NadaConditionMarketType n1 = NadaConditionMarketType.valueOf(t1);
		
		if(n0 == null || n1 == null)
			throw new NullPointerException("Cannot compare VDP_GuideBookValues, could not find the NadaConditionMarketType refered to by VDP_GuideBookValue using ThirdPartyCategoryId!");
		
		return Integer.valueOf(n0.getOrder()).compareTo(n1.getOrder());
	}
}