package biz.firstlook.module.vehicle.description.exception;

public interface BaseError {
	public String getErrorMessage();
}
