package biz.firstlook.module.vehicle.description.old;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class KBBEquipmentConflictMapper implements RowMapper
{
	
	private static final int DEPENDENCY = 1;
	private static final int CONFLICT = 2;
	private static final int REQUIRED = 3;
	
public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
{
	VDP_GuideBookValue value = new VDP_GuideBookValue();
	BookOutError error = new BookOutError();

	error.setErrorType( BookOutError.OPTIONS_CONFLICT );
	int optionRelationShipType = rs.getInt("OptionRelationshipTypeId");
	switch (optionRelationShipType) {
	case DEPENDENCY:
		error.setErrorMessage( "Options Dependency: " );
		break;
	case CONFLICT:
		error.setErrorMessage( "Options Conflict: " );
		break;
	case REQUIRED:
		error.setErrorMessage( "Options Requirement: " );		
		break;
	}
	
	error.setFirstOptionDescription( rs.getString( "VehicleOptionID#1" ) );
	error.setSecondOptionDescription( rs.getString( "VehicleOptionID#2" ) );

	value.setHasErrors( true );
	value.setError( error );

	return value;
}

}
