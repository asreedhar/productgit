package biz.firstlook.module.vehicle.description.impl;

import java.util.List;

import biz.firstlook.commons.collections.ValidatableList;
import biz.firstlook.module.bookout.BookValueContainer;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public abstract class AbstractVehicleDescriptionImpl
	implements MutableVehicleDescription {

	private String make;
	private String model;
	private Integer year;
	protected ValidatableList<ThirdPartyVehicleOption> options;
	
	public AbstractVehicleDescriptionImpl() {
		options = new ValidatableList<ThirdPartyVehicleOption>();  
	}
	
	public String getMake() { return make;	}
	public String getModel() { return model; }
	public Integer getYear() { return year; }

	public void setMake(String make) { this.make = make; }
	public void setModel(String model) { this.model = model; }
	public void setYear(Integer year) { this.year = year;}
	
	public abstract VehicleDescriptionProviderEnum getProvider();
	
	/**
	 * This method checks to make sure the set of Options is valid.  If there are conflicts, it throws an OptionsConflictException
	 * which should be handled by the client application.  This call only hit the thirdparty service if the options set has been 
	 * modified since the last time the validate method was called.
	 */
	public abstract void validate() throws OptionsConflictException;
	
	/**
	 * This method exists to double check that options are valid - it should be called before an option set is 
	 * persisted or submitted for Values.  If the valid set flag is false - and the options set proves to be invalid
	 * it throws a runtime exception since by the time you are persisting the set or using it for values the client
	 * should have already called the validate method to ensure the set was kosher.
	 */
	public abstract void assertSelectedOptionsSetInValidState();
	
	/**
	 * This method returns book values associated with the given vehicle description
	 * @return
	 */
	public abstract BookValueContainer getBookValueContainer( Integer mileage ) throws ValuesRetrievalException;
	
	public abstract void setVehicleDescriptionProvider( VehicleDescriptionProvider provider );
	
	public abstract void setSelectedVehicleOptions(List<VehicleOption> selectedOptions);
	public abstract void setVehicleOptions(List<VehicleOption> selectedOptions);
	public abstract List<VehicleOption> getSelectedVehicleOptions();
	public abstract List<VehicleOption> getVehicleOptions(); 
}
