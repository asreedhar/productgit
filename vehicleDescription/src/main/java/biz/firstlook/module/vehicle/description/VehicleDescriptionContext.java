package biz.firstlook.module.vehicle.description;

import java.util.Collection;

import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;

public interface VehicleDescriptionContext {

VehicleDescriptionProvider getDescriptionProvider( VehicleDescriptionProviderEnum providerEnum );

Collection< VehicleDescriptionProvider > getRegisteredDescriptionProviders();

}
