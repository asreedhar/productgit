package biz.firstlook.module.vehicle.description.old;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

public class KBBThirdPartyVehicleOptionMapper implements RowMapper
{
	
public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
{
	int categoryId = rs.getInt("CategoryId");
	int optionTypeId = rs.getInt("OptionTypeId");
	ThirdPartyOptionType tpoType = getThirdPartyOptionType(
			VehicleOptionCategory.fromId(categoryId), 
			VehicleOptionType.fromId(optionTypeId));
	ThirdPartyOption tpOption = new ThirdPartyOption(
			rs.getString( "Description"),
			rs.getString( "VehicleOptionId" ),
			tpoType);
	
	ThirdPartyVehicleOption tpvOption = new ThirdPartyVehicleOption(
			tpOption, 
			rs.getBoolean( "IsStandard" ), 
			rs.getInt("DisplayOrder"));
	tpvOption.setStatus( rs.getBoolean( "Status" ) );
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE,
			rs.getInt( "RetailValue" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE,
			rs.getInt( "WholesaleValue" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT,
			rs.getInt( "TradeInExcellent" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD,
			rs.getInt( "TradeInGood" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR,
			rs.getInt( "TradeInFair" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_EXCELLENT,
			rs.getInt( "PrivatePartyExcellent" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_GOOD,
			rs.getInt( "PrivatePartyGood" )));
	
	tpvOption.addOptionValue(new ThirdPartyVehicleOptionValue(
			tpvOption,
			ThirdPartyVehicleOptionValueType.PRIVATE_PARTY_OPTION_VALUE_TYPE_FAIR,
			rs.getInt( "PrivatePartyFair" )));
	
	return tpvOption;
}

//refactored from comparing ints (int1 == int2) to using enums with switch statements.
private static ThirdPartyOptionType getThirdPartyOptionType(VehicleOptionCategory vehicleOptionCategory, VehicleOptionType vehicleOptionType) {
	ThirdPartyOptionType tpot = null;
	switch(vehicleOptionType) {
		case EQUIPMENT:
			switch (vehicleOptionCategory) {
				case OPTION_DRIVETRAIN:
				case OPTION_EQUIPMENT_TYPE_DRIVETRAIN:
					tpot = ThirdPartyOptionType.DRIVETRAIN;	
					break;
				case OPTION_ENGINE:
				case OPTION_EQUIPMENT_TYPE_ENGINE:
					tpot = ThirdPartyOptionType.ENGINE;	
					break;
				case OPTION_TRANSMISSION:
				case OPTION_EQUIPMENT_TYPE_TRANSMISSION:
					tpot = ThirdPartyOptionType.TRANSMISSION;	
					break;
				default:
					// this is our default not KBBs. If the OptionType is Equipment, the Category should always be one of the 3 above.
					tpot = ThirdPartyOptionType.EQUIPMENT;				
					break;
			}
			break;
		case OPTION:
			switch(vehicleOptionCategory) {
				case APPLICATION_FILTER_CONSUMER:
					tpot = ThirdPartyOptionType.EQUIPMENT;
					break;
				default:
					tpot = ThirdPartyOptionType.GENERAL;
			}
			break;
		case CPO:
			//need to set as equipment to make it show up as an "Option"
			tpot = ThirdPartyOptionType.EQUIPMENT;
			break;
		default:
			throw new IllegalArgumentException("VehicleOptionType " + vehicleOptionType + " not supported.");
	}
	
	return tpot;
}

}
