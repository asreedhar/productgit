package biz.firstlook.module.vehicle.description.compatibility;

import biz.firstlook.commons.collections.Adapter;
import biz.firstlook.module.bookout.BookValue;


public interface BookValueAdapter<T> extends Adapter<BookValue, T>
{

}
