package biz.firstlook.module.vehicle.description.compatibility;

import biz.firstlook.commons.collections.Adapter;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;

public interface VehicleDescriptionAdapter<T> extends Adapter<MutableVehicleDescription, T>
{

}
