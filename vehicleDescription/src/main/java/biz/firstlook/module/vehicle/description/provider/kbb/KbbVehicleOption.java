package biz.firstlook.module.vehicle.description.provider.kbb;

import biz.firstlook.module.vehicle.description.AbstractVehicleOption;

public class KbbVehicleOption extends AbstractVehicleOption{

	private String optionKey;
	private Integer thirdPartyOptionTypeId; // legacy field needed to persist to old schema
	private Integer thirdPartyOptionId; // legacy field needed to persist to old schema
	private boolean standardOption; // not sure if this belongs in abstractVehicleOption
	
	public String getOptionKey() {
		return optionKey;
	}

	public void setOptionKey(String optionKey) {
		this.optionKey = optionKey;
	}

	public boolean isStandardOption() { return standardOption; }
	public void setStandardOption(boolean standardOption) { this.standardOption = standardOption;	}

	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( optionKey == null ) ? 0 : optionKey.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		final KbbVehicleOption other = (KbbVehicleOption)obj;
		if ( optionKey == null )
		{
			if ( other.optionKey != null )
				return false;
		}
		else if ( !optionKey.equals( other.optionKey ) )
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getDisplayName()).append(": ");
		if(isSelected()) {
			sb.append("selected.");
		} else {
			sb.append("not selected.");
		}
		return sb.toString();
	}

	// needed to write to old schema
	public Integer getThirdPartyOptionTypeId() { return thirdPartyOptionTypeId;	}
	public void setThirdPartyOptionTypeId(Integer thirdPartyOptionTypeId) { this.thirdPartyOptionTypeId = thirdPartyOptionTypeId; }
	public Integer getThirdPartyOptionId() { return thirdPartyOptionId;	}
	public void setThirdPartyOptionId(Integer thirdPartyOptionId) { this.thirdPartyOptionId = thirdPartyOptionId; }

}
