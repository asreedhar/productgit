package biz.firstlook.module.vehicle.description.exception;

public class ValuesRetrievalException extends Exception {

	private static final long serialVersionUID = 3256719585087797099L;
	
	public ValuesRetrievalException(Throwable arg0) {
		super(arg0);
	}
}
