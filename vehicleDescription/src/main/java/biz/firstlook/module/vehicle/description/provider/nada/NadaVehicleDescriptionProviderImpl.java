package biz.firstlook.module.vehicle.description.provider.nada;

import java.util.Collection;

import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.services.vehicleConfigandValuations.entities.BookValuationRequests;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsRequest;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;
import biz.firstlook.transact.persist.model.Dealership;

public class NadaVehicleDescriptionProviderImpl implements
		VehicleDescriptionProvider {

	private BookOutDatasetInfo info;
	private Integer regionCode;

	public BookOutDatasetInfo getInfo() {
		return info;
	}

	public void setInfo(BookOutDatasetInfo info) {
		this.info = info;
	}

	public Integer getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(Integer regionCode) {
		this.regionCode = regionCode;
	}

	public void init(Dealership dealer) {
		this.info = new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL,
				dealer.getDealerId());
		this.regionCode = dealer.getDealerPreference().getNadaRegionCode();
	}

	public Collection<MutableVehicleDescription> getVehicleDescription(
			String vin) {
		// TODO Auto-generated method stub
		return null;
	}

	public VehicleDescriptionProviderEnum getDescriptionProviderEnum() {
		return VehicleDescriptionProviderEnum.NADA;
	}

	ValuationsResponse getValues(NADAVehicleDescriptionImpl description,
			Integer mileage, KBBConditionEnum condition)
			throws ValuesRetrievalException {

		ValuationsRequest request = new ValuationsRequest();

		BookValuationRequests bookVals = new BookValuationRequests();

		bookVals.setBook("nada");
		bookVals.setMileage(description.getMileage());

		request.setBookValuationRequests(new BookValuationRequests[] { bookVals });

		try {
			// List<VDP_GuideBookValue> values =
			// kbbBookoutService.retrieveRetailAndTradeInBookValues(regionalIdentifier.getStateCode(),
			// mileage, options, vehicle, null, info);
			// return new KbbBookValueContainer( Adapters.adapt(values, new
			// KbbVDP_BookValueAdapter() ), regionalIdentifier );
		} catch (Exception gbe) {
			throw new ValuesRetrievalException(gbe);
		}
		return null;
	}
}
