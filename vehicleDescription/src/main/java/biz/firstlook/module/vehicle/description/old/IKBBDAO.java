package biz.firstlook.module.vehicle.description.old;

import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

interface IKBBDAO
{

public List<VDP_GuideBookVehicle> doVinLookup( String vin, Integer year, BookOutDatasetInfo bookoutInfo );
public List< ThirdPartyVehicleOption > doOptionsLookup( String vin, String vehicleId, BookOutDatasetInfo bookoutInfo );
public List< VDP_GuideBookValue > checkForEquipmentConflicts( String vehicleId, String selectedOptions, BookOutDatasetInfo bookoutInfo );
public KbbPriceAdjustmentsTable getValues( String kbbModelId, int year, int mileage, String regionCode, BookOutDatasetInfo bookoutInfo );

/**
 * Retrieves a list of valid Model Years.  This method should be refactored to return a list of ModelYear objects instead of Integers.
 * @param bookoutInfo
 * @return
 */
public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo);
public List<VDP_GuideBookVehicle> retrieveMakes( String year, BookOutDatasetInfo bookoutInfo );
public List<VDP_GuideBookVehicle> retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo );
public List<VDP_GuideBookVehicle> retrieveTrims( String year, String makeId, String modelId, BookOutDatasetInfo bookoutInfo );
public Map< String, String > getMetaInfo( BookOutDatasetInfo bookoutInfo );
public List< String > checkForOptionConflictsWithStdOptions( StringBuilder missingStdOptionCodes, StringBuilder selectedOptionCodes, BookOutDatasetInfo bookoutInfo );
public List< VDP_GuideBookVehicle > retrieveModelsByMakeIDAndYear( String vin, Integer year, BookOutDatasetInfo bookoutInfo );
public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear( String makeId, String modelId, BookOutDatasetInfo bookoutInfo );

}
