package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.List;

import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.compatibility.VehicleDescriptionAdapter;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;

public class KbbTPV_VehicleAdapter implements VehicleDescriptionAdapter< ThirdPartyVehicle > {

	public MutableVehicleDescription adaptItem(ThirdPartyVehicle oldVehicle) {
		KbbVehicleDescriptionImpl desc = new KbbVehicleDescriptionImpl();
		desc.setVehicleId(oldVehicle.getThirdPartyVehicleCode());
		desc.setMake( oldVehicle.getMake() );
		desc.setModel( oldVehicle.getModel() );
		desc.setTrim(  oldVehicle.getBody() );
		desc.setMakeCode( new MakeId( oldVehicle.getMakeCode() ) );
		desc.setModelCode( new ModelId( oldVehicle.getModelCode() ) );
		List<VehicleOption> options = Adapters.adapt( oldVehicle.getThirdPartyVehicleOptions(), new Kbb_VehicleOptionAdapter() );
		desc.setVehicleOptions( options );
		return desc;
	}

	public ThirdPartyVehicle unadaptItem(MutableVehicleDescription newItem) {
		ThirdPartyVehicle tpv = new ThirdPartyVehicle();
		KbbVehicleDescriptionImpl vDesc = (KbbVehicleDescriptionImpl)newItem;
		tpv.setThirdPartyVehicleCode(vDesc.getVehicleId());
		tpv.setMake( vDesc.getMake() );
		tpv.setMakeCode( vDesc.getMakeCode().getCode() );
		tpv.setModel( vDesc.getModel() );
		tpv.setModelCode( vDesc.getModelCode().getCode() );
		tpv.setBody( vDesc.getTrim() );
		tpv.setStatus( true ); // we should only be persisting user selected TPV's
		tpv.setThirdPartyId( vDesc.getProvider().getThirdPartyId() );
		List<VehicleOption> kbbOptions = (List<VehicleOption>)vDesc.getVehicleOptions();
		kbbOptions.addAll( vDesc.getDrivetrainOptions() );
		kbbOptions.addAll( vDesc.getEngineOptions() );
		kbbOptions.addAll( vDesc.getTransmissionOptions() );
		tpv.setThirdPartyVehicleOptions( Adapters.unadapt( kbbOptions, new Kbb_VehicleOptionAdapter()) );
		return tpv;
	}
}