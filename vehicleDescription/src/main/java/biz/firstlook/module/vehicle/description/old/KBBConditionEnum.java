package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;

public enum KBBConditionEnum
{

N_A (0, "N/A"),
EXCELLENT (1, "Excellent"),
GOOD (2, "Good"),
FAIR (3, "Fair");

private Integer id;
private String description;

private KBBConditionEnum(Integer id, String description)
{
	this.id = id;
	this.description = description;
}

static public KBBConditionEnum getKBBConditionEnumByDescriptin( String inDesc )
{
	if ( EXCELLENT.getDescription().equals( inDesc ) ) {
		return EXCELLENT;
	} else if ( GOOD.getDescription().equals( inDesc ) ) {
		return GOOD;
	} else if ( FAIR.getDescription().equals( inDesc ) ) {
		return FAIR;
	} 
	return N_A;
}

static public KBBConditionEnum getKBBConditionEnumById( Integer inId )
{
	switch (inId)
	{
		case 1:
			return EXCELLENT;
		case 2:
			return GOOD;
		case 3:
			return FAIR;
	}
	return N_A;
}

public String getDescription()
{
	return description;
}

public Integer getId()
{
	return id;
}

public static KBBConditionEnum getKBBConditionEnum(ThirdPartySubCategoryEnum tpSubCategory) {
	if ( ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT.equals(tpSubCategory ) ||
			 ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_EXCELLENT.equals(tpSubCategory ) )
			return EXCELLENT;
	else if ( ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD.equals(tpSubCategory ) ||
			 ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_GOOD.equals(tpSubCategory ) )
			return GOOD;
	else if ( ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR.equals(tpSubCategory ) || 
			 ThirdPartySubCategoryEnum.KBB_PRIVATE_PARTY_FAIR.equals(tpSubCategory ) )
			return FAIR;
	return N_A;
}

public static ThirdPartySubCategoryEnum getThirdPartySubCategoryEnum(KBBConditionEnum condition) {
	switch(condition)
	{
		case EXCELLENT: return ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT;
		case GOOD: return ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD;
		case FAIR: return ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR;
		default: return ThirdPartySubCategoryEnum.N_A;
	}
}

public static List<KBBConditionEnum> getConditionsForDisplay() {
	List<KBBConditionEnum> conditions = new ArrayList<KBBConditionEnum>();
	conditions.add( KBBConditionEnum.EXCELLENT );
	conditions.add( KBBConditionEnum.GOOD );
	conditions.add( KBBConditionEnum.FAIR );
	return conditions;
}

}
