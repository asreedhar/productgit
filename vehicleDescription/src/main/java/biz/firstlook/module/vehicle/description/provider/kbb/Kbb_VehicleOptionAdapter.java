package biz.firstlook.module.vehicle.description.provider.kbb;

import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.compatibility.VehicleOptionAdapter;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class Kbb_VehicleOptionAdapter implements VehicleOptionAdapter<ThirdPartyVehicleOption> {

	public VehicleOption adaptItem(ThirdPartyVehicleOption oldItem) {
		KbbVehicleOption newOption = new KbbVehicleOption();
		newOption.setOptionKey( oldItem.getOptionKey() );
		newOption.setDisplayName( oldItem.getOptionName() );
		newOption.setThirdPartyOptionId( oldItem.getOption().getThirdPartyOptionId() );
		newOption.setThirdPartyOptionTypeId( oldItem.getOption().getThirdPartyOptionType().getThirdPartyOptionTypeId() );
		newOption.setSelected( oldItem.isStatus() );
		newOption.setSortOrder( oldItem.getSortOrder() );
		newOption.setStandardOption( oldItem.isStandardOption() );
		return newOption;
	}

	public ThirdPartyVehicleOption unadaptItem(VehicleOption newItem) {
		KbbVehicleOption kbbItem = (KbbVehicleOption)newItem; 
		
		ThirdPartyOption tpo = new ThirdPartyOption(
				newItem.getDisplayName(), 
				kbbItem.getOptionKey(), 
				ThirdPartyOptionType.fromId(kbbItem.getThirdPartyOptionTypeId()));
		tpo.setThirdPartyOptionId( kbbItem.getThirdPartyOptionId() );
		ThirdPartyVehicleOption tpvo = new ThirdPartyVehicleOption(tpo, 
				kbbItem.isStandardOption(), 
				newItem.getSortOrder() == null ? 1 : newItem.getSortOrder());
		tpvo.setStatus( newItem.isSelected() );
		return tpvo;
	}
}
