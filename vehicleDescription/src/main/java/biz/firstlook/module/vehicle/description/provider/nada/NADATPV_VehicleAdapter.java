package biz.firstlook.module.vehicle.description.provider.nada;

import java.util.List;

import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.compatibility.VehicleDescriptionAdapter;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;

public class NADATPV_VehicleAdapter implements VehicleDescriptionAdapter< ThirdPartyVehicle > {

	public MutableVehicleDescription adaptItem(ThirdPartyVehicle oldVehicle) {
		NADAVehicleDescriptionImpl desc = new NADAVehicleDescriptionImpl();
		desc.setVehicleId(oldVehicle.getThirdPartyVehicleCode());
		desc.setMake( oldVehicle.getMake() );
		desc.setModel( oldVehicle.getModel() );
		desc.setTrim(  oldVehicle.getThirdPartyVehicleCode() );
		desc.setMakeCode( oldVehicle.getMakeCode()  );
		desc.setModelCode(  oldVehicle.getModelCode() );
		List<VehicleOption> options = Adapters.adapt( oldVehicle.getThirdPartyVehicleOptions(), new NADA_VehicleOptionAdapter() );
		desc.setVehicleOptions( options );
		return desc;
	}

	public ThirdPartyVehicle unadaptItem(MutableVehicleDescription newItem) {
		ThirdPartyVehicle tpv = new ThirdPartyVehicle();
		NADAVehicleDescriptionImpl vDesc = (NADAVehicleDescriptionImpl)newItem;
		tpv.setThirdPartyVehicleCode(vDesc.getVehicleId());
		tpv.setMake( vDesc.getMake() );
		tpv.setMakeCode( vDesc.getMakeCode() );
		tpv.setModel( vDesc.getModel() );
		tpv.setModelCode( vDesc.getModelCode() );
		tpv.setBody( vDesc.getVehicleId() );
		tpv.setStatus( true ); // we should only be persisting user selected TPV's
		tpv.setThirdPartyId( vDesc.getProvider().getThirdPartyId() );
		
		List<VehicleOption> kbbOptions = (List<VehicleOption>)vDesc.getVehicleOptions();
		
		tpv.setThirdPartyVehicleOptions( Adapters.unadapt( kbbOptions, new NADA_VehicleOptionAdapter()) );
		return tpv;
	}
}