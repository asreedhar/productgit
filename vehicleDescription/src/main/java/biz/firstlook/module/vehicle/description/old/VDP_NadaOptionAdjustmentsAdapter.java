package biz.firstlook.module.vehicle.description.old;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Hack to get print page to spit out option adjustments correctly while 
 * keeping compatibility with old code.
 * @author bfung
 *
 */
public class VDP_NadaOptionAdjustmentsAdapter extends VDP_GuideBookValue {
	
	private static final long serialVersionUID = 2380925482293283650L;
	
	private List<VDP_GuideBookValue> optionAdjustments = new ArrayList<VDP_GuideBookValue>();

	public List<VDP_GuideBookValue> getOptionAdjustments() {
		Collections.sort(optionAdjustments, new NadaPriceConditionMarketTypeComparator());
		return Collections.unmodifiableList(optionAdjustments);
	}
	
	public boolean add(VDP_GuideBookValue value) {
		//don't null pointer in case the calling class expected a VDP_GuideBookValue object
		if(optionAdjustments.isEmpty()) {
			setError(value.getError());
			setHasErrors(value.isHasErrors());
			setPercentageAdjustment(value.getPercentageAdjustment());
			setThirdPartyCategoryDescription(value.getThirdPartyCategoryDescription());
			setThirdPartyCategoryId(value.getThirdPartyCategoryId());
			setThirdPartySubCategoryId(value.getThirdPartySubCategoryId());
			setValue(value.getValue());
			setValueType(value.getValueType());
		}
		return optionAdjustments.add(value);
	}
}
