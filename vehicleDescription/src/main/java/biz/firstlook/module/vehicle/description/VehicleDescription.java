package biz.firstlook.module.vehicle.description;

import java.util.List;

import biz.firstlook.module.bookout.BookValueContainer;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;

/**
 * Represents the description of a vehicle given from some provider. 
 * @author bfung
 * @param <T>
 */
public interface VehicleDescription {
	public String getMake();
	public String getModel();
	public Integer getYear();
	public VehicleDescriptionProviderEnum getProvider();
	
	/**
	 * This method checks to make sure the set of Options is valid.  If there are conflicts, it throws an OptionsConflictException
	 * which should be handled by the client application.  This call only hit the thirdparty service if the options set has been 
	 * modified since the last time the validate method was called.
	 */
	public void validate() throws OptionsConflictException;
	
	/**
	 * This method exists to double check that options are valid - it should be called before an option set is 
	 * persisted or submitted for Values.  If the valid set flag is false - and the options set proves to be invalid
	 * it throws a runtime exception since by the time you are persisting the set or using it for values the client
	 * should have already called the validate method to ensure the set was kosher.
	 */
	public void assertSelectedOptionsSetInValidState();
	
	/**
	 * This method returns book values associated with the given vehicle description
	 * @return
	 */
	public BookValueContainer getBookValueContainer( Integer mileage ) throws ValuesRetrievalException;
	
	/** 
	 * this method checks to see if a vehicleDescription is in a bookoutable state - meaning the data provider field is properly initialized
	 * @return
	 */
	public boolean isBookoutable();
	
	List<VehicleOption> getVehicleOptions();
    public List<VehicleOption> getSelectedVehicleOptions();
}
