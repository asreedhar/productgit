package biz.firstlook.module.vehicle.description.old;

public enum KBBCategoryEnum
{

UNKNOWN(0, "Unknown"),
TRADEIN(1, "Trade-In"),
RETAIL(2, "Retail"),
WHOLESALE(3, "Wholesale"),
PRIVATE_PARTY(4, "Private Party"),
CLASSIC_WHOLESALE(5, "Classic Wholesale"),
TRADEINRANGELOW(6, "Trade-In + RangeLow"),
TRADEINRANGEHIGH(7, "Trade-In + RangeHigh");

private int id;
private String description;

private KBBCategoryEnum(int id, String description)
{	
	this.id = id;
	this.description = description;
}

static public KBBCategoryEnum getKBBCategoryEnumByDescriptin( String inDesc )
{
	if ( TRADEIN.getDescription().equals( inDesc.trim() ) ) {
		return TRADEIN;
	} else if ( RETAIL.getDescription().equals( inDesc.trim() ) ) {
		return RETAIL;
	} else if ( WHOLESALE.getDescription().equals( inDesc.trim() ) ) {
		return WHOLESALE;
	} else if ( PRIVATE_PARTY.getDescription().equals( inDesc.trim() ) ) {
		return PRIVATE_PARTY;
	} else if ( CLASSIC_WHOLESALE.getDescription().equals( inDesc.trim() ) ) {
		return CLASSIC_WHOLESALE;
	} else if ( TRADEINRANGELOW.getDescription().equals( inDesc.trim() ) ) {
		return TRADEINRANGELOW;
	} else if ( TRADEINRANGEHIGH.getDescription().equals( inDesc.trim() ) ) {
		return TRADEINRANGEHIGH;
	}
	return UNKNOWN;
}

public String getDescription()
{
	return description;
}

public int getId()
{
	return id;
}

}
