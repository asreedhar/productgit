package biz.firstlook.module.vehicle.description.impl;

import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.VehicleOptionWithValue;

public class VehicleOptionWithValueImpl implements VehicleOptionWithValue {

	private Integer value;
	private VehicleOption vehicleOption;
	
	public VehicleOptionWithValueImpl(VehicleOption vehicleOption, Integer value) {
		super();
		this.vehicleOption = vehicleOption;
		this.value = value;
	}
	
	public VehicleOption getVehicleOption() {
		return vehicleOption;
	}
	
	public Integer getValue() {
		return value;
	}
}
