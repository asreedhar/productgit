package biz.firstlook.module.vehicle.description.impl;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.transact.persist.model.Dealership;

public class VehicleDescriptionContextFactory {

	private static VehicleDescriptionContextFactory factory = new VehicleDescriptionContextFactory();
	
	protected VehicleDescriptionContextFactory() {
		super();
	}
	
	public static synchronized VehicleDescriptionContextFactory getInstance() {
		return factory;
	}
		
	public VehicleDescriptionContext getBusinessUnitContext( Dealership dealer ) {
		return new BusinessUnitVehicleDescriptionContextImpl( dealer );
	}

	public VehicleDescriptionContext getKbbConsumerValuesContext( Dealership dealer ) {
		return new KbbConsumerValuesToolVehicleDescriptionContext( dealer );
	}
}