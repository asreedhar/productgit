package biz.firstlook.module.vehicle.description;

public interface VehicleOptionWithValue {
	public VehicleOption getVehicleOption();
	public Integer getValue();
}
