package biz.firstlook.module.vehicle.description.old;

import java.io.Serializable;



public class BookOutError implements Serializable
{

private static final long serialVersionUID = -8787064604285742355L;

public static final int INVALID_VIN_BAD_FORMAT = 1;
public static final int INVALID_VIN_NOT_IN_GUIDEBOOK = 2;
public static final int OPTIONS_CONFLICT = 3;
public static final int CANT_CONNECT = 4;

private int errorType;
private String errorMessage;
private String firstOptionDescription;
private String secondOptionDescription;
public BookOutError()
{
	super();
}

public String getErrorMessage()
{
	return errorMessage;
}

public void setErrorMessage( String errorMessage )
{
	this.errorMessage = errorMessage;
}

public int getErrorType()
{
	return errorType;
}

public void setErrorType( int errorType )
{
	this.errorType = errorType;
}

public String getFirstOptionDescription()
{
	return firstOptionDescription;
}

public void setFirstOptionDescription( String firstOptionDescription )
{
	this.firstOptionDescription = firstOptionDescription;
}

public String getSecondOptionDescription()
{
	return secondOptionDescription;
}

public void setSecondOptionDescription( String secondOptionDescription )
{
	this.secondOptionDescription = secondOptionDescription;
}


public boolean equals( Object obj )
{
    if ( obj instanceof BookOutError )
    {
    	BookOutError error = (BookOutError) obj;

        if ( this.getFirstOptionDescription().equals(error.getFirstOptionDescription())
                && this.getSecondOptionDescription().equals(error.getSecondOptionDescription()) )
        {
            return true;
        }

        if ( this.getFirstOptionDescription().equals(error.getSecondOptionDescription())
                && this.getSecondOptionDescription().equals(error.getFirstOptionDescription()) )
        {
            return true;
        }
    }

    return false;
}


}
