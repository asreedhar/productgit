package biz.firstlook.module.vehicle.description.old;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;

class KBBValuesMapper implements RowMapper
{

public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
{
	VDP_GuideBookValue value = new VDP_GuideBookValue();

	String valueType = rs.getString( "ValueType");
	
	if( "Base".equals( valueType ) )
	{
		KBBConditionEnum condition = KBBConditionEnum.getKBBConditionEnumByDescriptin( rs.getString( "Condition") );
		KBBCategoryEnum category = KBBCategoryEnum.getKBBCategoryEnumByDescriptin( rs.getString( "Category") );
		ThirdPartySubCategoryEnum thirdPartySubCategory = KBBValueEnum.getThirdPartySubCategoryByConditionAndCategory( condition, category );	
		value.setThirdPartyCategoryId( thirdPartySubCategory.getThirdPartyCategory().getThirdPartyCategoryId() );
		value.setThirdPartyCategoryDescription( thirdPartySubCategory.getThirdPartyCategory().getCategory() );
		value.setThirdPartySubCategoryId( thirdPartySubCategory.getThirdPartySubCategoryId() );
		value.setValueType( BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE );
		value.setValue( (int)rs.getDouble("Adjustment"));
	}
	else if( "MileageAdjustment".equalsIgnoreCase(valueType))
	{
		int condition = rs.getInt( "Condition" );
		Integer rawValue = null;
		Double percentageAdjustmentValue = null;
		
		final String adjustmentColumnName = "Adjustment";
		Object mileageAdjustmentValue = rs.getObject(adjustmentColumnName);
		if (mileageAdjustmentValue != null && mileageAdjustmentValue instanceof Number) {
			Number adjValue = (Number) mileageAdjustmentValue;
			if (condition == 1) {
				percentageAdjustmentValue = adjValue.doubleValue();
			} else {
				rawValue = adjValue.intValue();
			}
		}		
		value = new KbbMileageAdjustmentValue(rawValue, percentageAdjustmentValue, condition);
		
	} else if("MaximumDeductionPercentage".equalsIgnoreCase(valueType)) {
		value.setValueType( BookOutValueType.KBB_MAXIMUM_DEDUCTION_PERCENTAGE );
		value.setThirdPartyCategoryId(0);  //no real meaning here.
		value.setPercentageAdjustment(rs.getDouble("Adjustment"));
	} else if ("CPO Mileage Restriction".equalsIgnoreCase(valueType)) {
		value.setValueType( BookOutValueType.KBB_CPO_MILEAGE_RESTRICTION );
		value.setThirdPartyCategoryId(0);  //no real meaning here.
		value.setValue((int)rs.getDouble("Adjustment"));
	}
	
	return value;
}

}
