package biz.firstlook.module.vehicle.description.provider.kbb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.collections.Adapters;
import biz.firstlook.commons.collections.ValidatableList;
import biz.firstlook.module.bookout.BookValueContainer;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictRunTimeException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.exception.VehicleDescriptionNotBookoutableRuntimeException;
import biz.firstlook.module.vehicle.description.impl.AbstractVehicleDescriptionImpl;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class KbbVehicleDescriptionImpl extends AbstractVehicleDescriptionImpl {
	
	private static final Logger logger = Logger.getLogger( KbbVehicleDescriptionImpl.class );
	
	private String vehicleId;
	private MakeId makeCode;
	private ModelId modelCode;
	private String trim;

	private KbbVehicleDescriptionProvider provider;
	private KBBConditionEnum kbbCondition; // this needs to be set before you can retrieve values - it is not persisted!

	// default constructor leaves you in a not-bookoutable state!
	KbbVehicleDescriptionImpl() { this.provider = null;}
	
	// construct with provider argument leaves you in a bookout able state
	KbbVehicleDescriptionImpl( KbbVehicleDescriptionProvider kbbVehicleDescriptionProvider ) {
		this.provider = kbbVehicleDescriptionProvider;
	}

	public String getVehicleId() { return vehicleId; }
	public void setVehicleId(String vehicleId) { this.vehicleId = vehicleId; }
	public MakeId getMakeCode() { return makeCode;	}
	public ModelId getModelCode() { return modelCode; }
	public String getTrim()	{ return trim; }
	public void setMakeCode(MakeId makeCode) { this.makeCode = makeCode; }
	public void setModelCode(ModelId modelCode) { this.modelCode = modelCode; }
	public void setTrim( String trim ){ this.trim = trim; }
	
	public String toString() { return super.toString() + " " + makeCode + " " + modelCode;	}
	public VehicleDescriptionProviderEnum getProvider() { return VehicleDescriptionProviderEnum.KBB; }

	
	public List<VehicleOption> getSelectedEquipmentVehicleOptions() {
		
		if( this.options == null || this.options.isEmpty() ) {
			return Collections.emptyList();
		}
		List<ThirdPartyVehicleOption> selectedSet = new ArrayList<ThirdPartyVehicleOption>();
		List<ThirdPartyVehicleOption> equipmentOptions = filterOptions( ThirdPartyOptionType.EQUIPMENT );
		for( ThirdPartyVehicleOption option: equipmentOptions ) {
			if ( option.isStatus() ) {
				selectedSet.add( option );
			}
		}
		return Adapters.adapt( selectedSet, new Kbb_VehicleOptionAdapter());
	}
	@Override
	public List<VehicleOption> getSelectedVehicleOptions() {
		List<ThirdPartyVehicleOption> selectedSet = new ArrayList<ThirdPartyVehicleOption>();
		if( this.options == null || this.options.isEmpty() ) {
			return Collections.emptyList();
		}
		
		for( ThirdPartyVehicleOption option: this.options ) {
			if ( option.isStatus() ) {
				selectedSet.add( option );
			}
		}
		return Adapters.adapt( selectedSet, new Kbb_VehicleOptionAdapter());
	}
	
	public void setSelectedVehicleOptions( List<VehicleOption> setOfSelectedOptions ) {
		List<ThirdPartyVehicleOption> selectedOptions = Adapters.unadapt(setOfSelectedOptions, new Kbb_VehicleOptionAdapter());
		for( ThirdPartyVehicleOption option : this.options ) {
			if( selectedOptions.contains( option ) ) {
				option.setStatus( true );
			} else {
				option.setStatus( false );
			}
		}
		options.setAsInvalid();
	}
	
	@Override
	public void setVehicleOptions(List<VehicleOption> options) {
		// new list of options will be in an invalid state
		this.options = new ValidatableList<ThirdPartyVehicleOption>( Adapters.unadapt(options, new Kbb_VehicleOptionAdapter() ) );
	}
	
	// For KBB - VehicleOptions is the same as Equipment Options - so when you call vehicle options you're getting back the 
	// set of equipment options - since we alwyas handle engine/drivetrain/transmission seperately
	@Override
	public List<VehicleOption> getVehicleOptions() {
		List<ThirdPartyVehicleOption> equipmentOptions = filterOptions( ThirdPartyOptionType.EQUIPMENT );
		return Adapters.adapt( equipmentOptions, new Kbb_VehicleOptionAdapter() );
	}

	public void assertSelectedOptionsSetInValidState() {
		if ( !options.isValidState() ) {
			try {
				validate();
			} catch( OptionsConflictException e ) {
				// if user gets here - they have attempted to bookout or save an invalid set of options
				// which means they did not call validate after setting the options which 
				throw new OptionsConflictRunTimeException("Tried to use (save or get values with) an invalid set of selected options.  " +
						"Make sure to called validate() before using a selected options set.");
			}
		}
	}
	
	public void validate() throws OptionsConflictException {
		if ( options.isValidState() ) {
			return;
		}
		if ( !isBookoutable() ) {
			throw new VehicleDescriptionNotBookoutableRuntimeException();
		}
		// validate
		provider.checkForOptionsConflicts(this.vehicleId, this.options );
		options.setAsValid();
	}

	public BookValueContainer getBookValueContainer( Integer mileage ) throws ValuesRetrievalException {
		if ( kbbCondition == null ) {
			logger.warn( "No condition was set before values were retrieve! using Excellent as default" );
			kbbCondition = KBBConditionEnum.EXCELLENT;
		}
		if ( !isBookoutable() ) {
			throw new VehicleDescriptionNotBookoutableRuntimeException();
		}
		return provider.getValues( this,  mileage, kbbCondition );
	}
	
	public boolean isBookoutable() {
		return (provider != null && (provider instanceof KbbVehicleDescriptionProvider) );
	}
	
	public void setKbbCondition( KBBConditionEnum condition) {
		this.kbbCondition = condition;
	}

	
	@Override
	public void setVehicleDescriptionProvider(VehicleDescriptionProvider provider) {
		this.provider = (KbbVehicleDescriptionProvider)provider;
	}
	
	public List<VehicleOption> getEngineOptions() {
		List<ThirdPartyVehicleOption> engineOptions = filterOptions( ThirdPartyOptionType.ENGINE );
		return Adapters.adapt( engineOptions, new Kbb_VehicleOptionAdapter() );
	}
	public List<VehicleOption> getTransmissionOptions() {
		List<ThirdPartyVehicleOption> transmissionOptions = filterOptions( ThirdPartyOptionType.TRANSMISSION );
		return Adapters.adapt( transmissionOptions, new Kbb_VehicleOptionAdapter() );
	}
	public List<VehicleOption> getDrivetrainOptions() {
		List<ThirdPartyVehicleOption> drivetrainOptions = filterOptions( ThirdPartyOptionType.DRIVETRAIN );
		return Adapters.adapt( drivetrainOptions, new Kbb_VehicleOptionAdapter() );
	}
	
	// needed to allow us to write to old schema
	protected String getThirdPartyVehicleCode() { return modelCode.getCode(); }
	protected void setThirdPartyVehicleCode(String thirdPartyVehicleCode) {}
	protected Integer getStatus() { return 1; } // status will always be 1 since we are 
	protected void setStatus( Integer i ) {}	  // only storing the selected vehicle

	private List<ThirdPartyVehicleOption> filterOptions( ThirdPartyOptionType optionType ) {
		List<ThirdPartyVehicleOption> matches = new ArrayList<ThirdPartyVehicleOption>();
		for( ThirdPartyVehicleOption option : this.options) {
			if ( option.getOption().getThirdPartyOptionType().equals(optionType)) {
				matches.add( option );
			}
		}
		return matches;
	}
	
	public void setSelectedVehicleOptionsByKeys(List<String> selectedOptionsKeys) {
		for( ThirdPartyVehicleOption option : this.options) {
			if ( selectedOptionsKeys.contains( option.getOptionKey() ) ) {
				option.setStatus( true );
			} else {
				option.setStatus( false );
			}
		}
	}

	public KBBConditionEnum getKbbCondition() {
		return kbbCondition;
	}

	public List< VehicleOption > getAllOptions() {
		return Adapters.adapt( this.options.getList(), new Kbb_VehicleOptionAdapter() );
	}	
}