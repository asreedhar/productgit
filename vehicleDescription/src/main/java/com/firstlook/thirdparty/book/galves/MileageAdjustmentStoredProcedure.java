package com.firstlook.thirdparty.book.galves;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class MileageAdjustmentStoredProcedure extends StoredProcedure {
	
	private static final String STORED_PROC_NAME = "dbo.MileageAdjustment#Fetch";
	//private static final String PARAM_RC = "@RC";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_PH_GUID = "@GalvesVehicleID";
	private static final String PARAM_MILEAGE = "@Mileage";
	private static final String PARAM_REGION = "@Region";

	public MileageAdjustmentStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new MileageAdjustmentMapper()));
		//declareParameter(new SqlOutParameter(PARAM_RC, Types.INTEGER) );
		declareParameter(new SqlParameter(PARAM_PH_GUID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_MILEAGE, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_REGION, Types.CHAR));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MileageAdjustmentValue getMileageAdjustment(final Integer galvesVehicleId, final Integer mileage, final String region) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_PH_GUID, galvesVehicleId);
		parameters.put(PARAM_MILEAGE, mileage);
		parameters.put(PARAM_REGION, region);

		Map results = execute(parameters);
		
		//check return code?
		List<MileageAdjustmentValue> resultList = (List<MileageAdjustmentValue>)results.get(PARAM_RESULT_SET);
		MileageAdjustmentValue theValue = null;
		if(resultList != null && !resultList.isEmpty()) {
			theValue = resultList.get(0);
		}
		
		return theValue;
	}
	
	private class MileageAdjustmentMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			return new MileageAdjustmentValue(
					rs.getInt("Mileage"),
					rs.getInt("BaseMileage"),
					rs.getInt("MileageAdjustment"));
		}
	}
}
