package com.firstlook.thirdparty.book.blackbook;

import java.io.Serializable;

/**
 * A class that represents a Year a certain Model was built, according to BlackBook.
 * @author bfung
 *
 */
class ModelYear implements Comparable<ModelYear>, Serializable {
	
	private static final long serialVersionUID = 1346707790670834472L;
	
	private Integer modelYear;
	
	public ModelYear(Integer modelYear) {
		if(modelYear == null) {
			throw new IllegalArgumentException("ModelYear value must be an Integer, found null.");
		}
		
		this.modelYear = modelYear;
	}
	
	public Integer getModelYear() {
		return modelYear;
	}

	public int compareTo(ModelYear other) {
		if(other == null) {
			throw new IllegalArgumentException("Cannot compare this model year to a null one.");
		}
		
		Integer theirs = other.getModelYear();
		return modelYear.compareTo(theirs);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((modelYear == null) ? 0 : modelYear.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ModelYear))
			return false;
		ModelYear other = (ModelYear) obj;
		if (modelYear == null) {
			if (other.modelYear != null)
				return false;
		} else if (!modelYear.equals(other.modelYear))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return modelYear.toString();
	}
}
