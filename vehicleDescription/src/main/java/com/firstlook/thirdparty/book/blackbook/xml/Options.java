package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Options implements Serializable
{

private static final long serialVersionUID = -5977338568841756865L;
protected List<Option> _Option = new ArrayList<Option>();

public Options()
{
}

public List<Option> getOption()
{
    return (_Option);
}

public Option getOptionAt( int index )
{
    if ( index < 0 || index >= _Option.size() )
    {
        return (null);
    }
    return _Option.get(index);
}

public int getOptionCount()
{
    if ( _Option == null )
    {
        return (0);
    }

    return (_Option.size());
}

public void setOption( List<Option> optionList )
{
    if ( optionList == null )
    {
        _Option.clear();
    } else
    {
        _Option = optionList;
    }
}

public void addOption( Option obj )
{
    if ( obj == null )
    {
        return;
    }

    _Option.add(obj);
}

public void setOptionAt( Option obj, int index )
{
    if ( obj == null )
    {
        return;
    }

    if ( index < 0 || index >= _Option.size() )
    {
        return;
    }
    _Option.add(index, obj);
}

public void removeOption( Option obj )
{
    if ( obj == null )
    {
        return;
    }

    _Option.remove(obj);
}

public void removeOptionAt( int index )
{
    if ( index < 0 || index >= _Option.size() )
    {
        return;
    }

    _Option.remove(index);
}

public boolean hasData()
{

    return (true);
}

protected static boolean _validators_created = false;

}
