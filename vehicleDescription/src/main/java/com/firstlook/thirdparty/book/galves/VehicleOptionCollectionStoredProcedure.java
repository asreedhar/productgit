package com.firstlook.thirdparty.book.galves;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class VehicleOptionCollectionStoredProcedure extends StoredProcedure {
	
	private static final String STORED_PROC_NAME = "dbo.VehicleOptionCollection#Fetch";
	//private static final String PARAM_RC = "@RC";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_PH_GUID = "@GalvesVehicleID";
	private static final String PARAM_REGION = "@Region";

	public VehicleOptionCollectionStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new GalvesVehicleOptionMapper()));
		//declareParameter(new SqlOutParameter( PARAM_RC, Types.INTEGER ) );
		declareParameter(new SqlParameter(PARAM_PH_GUID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_REGION, Types.CHAR));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<GalvesVehicleOption> getOptions(final Integer galvesVehicleId, final String region) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_PH_GUID, galvesVehicleId);
		parameters.put(PARAM_REGION, region);

		Map results = execute(parameters);
		
		//check return code?
		return (List<GalvesVehicleOption>)results.get(PARAM_RESULT_SET);
	}
	
	private class GalvesVehicleOptionMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			String description = rs.getString("Description");
			try {
				return new GalvesVehicleOption(
						description,
						description,
						GalvesVehicleOption.AmountCodeEnum.valueOfIgnoreCase(rs.getString("AMT_CODE")),
						(Double.valueOf(rs.getDouble("AMT"))).intValue());
			} catch (ParseException e) {
				throw new SQLException(e.getMessage());
			}
		}
	}
}
