package com.firstlook.thirdparty.book.galves;

class ExclusiveVehicleOption {
	
	private final String selectedOptionKey;
	private final String excludedOptionKey;
	
	public ExclusiveVehicleOption(String selectedOptionKey,
			String excludedOptionKey) {
		
		super();
		this.selectedOptionKey = selectedOptionKey;
		this.excludedOptionKey = excludedOptionKey;
	}
	public String getSelectedOptionKey() {
		return selectedOptionKey;
	}
	public String getExcludedOptionKey() {
		return excludedOptionKey;
	}
}
