package com.firstlook.thirdparty.book.blackbook;

import biz.firstlook.module.vehicle.description.old.GBException;

public class BlackBookException extends GBException
{

private static final long serialVersionUID = -6009822407984667086L;

public BlackBookException()
{
	super();
}

public BlackBookException( String message )
{
	super( message );
}

public BlackBookException( String message, Throwable cause )
{
	super( message, cause );
}

public BlackBookException( Throwable cause )
{
	super( cause );
}

}
