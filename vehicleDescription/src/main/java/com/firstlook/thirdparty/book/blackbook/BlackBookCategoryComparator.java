package com.firstlook.thirdparty.book.blackbook;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.PropertyUtils;

public class BlackBookCategoryComparator< T > implements Comparator< T >, Serializable
{

private static final long serialVersionUID = 193014650230144735L;

public int compare( T o1, T o2 )
{
	try
	{
		String category1 = (String)PropertyUtils.getProperty( o1, "category" );
		BlackBookCondition cat1 = BlackBookCondition.valueOfDescription( category1 );
		
		String category2 = (String)PropertyUtils.getProperty( o2, "category" );
		BlackBookCondition cat2 = BlackBookCondition.valueOfDescription( category2 );
		
		if( cat1.getOrder() == cat2.getOrder())
			return 0;
		else if( cat1.getOrder() > cat2.getOrder())
			return 1;
		else
			return -1;		
	}
	catch ( IllegalAccessException e )
	{
		e.printStackTrace();
	}
	catch ( InvocationTargetException e )
	{
		e.printStackTrace();
	}
	catch ( NoSuchMethodException e )
	{
		e.printStackTrace();
	}
	return 0;
}

}
