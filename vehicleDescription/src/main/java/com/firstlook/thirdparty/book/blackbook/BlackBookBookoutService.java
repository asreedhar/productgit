package com.firstlook.thirdparty.book.blackbook;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import biz.firstlook.module.bookout.AbstractBookoutService;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

import com.firstlook.thirdparty.book.blackbook.xml.BlackBookCar;
import com.firstlook.thirdparty.book.blackbook.xml.Option;
import com.firstlook.thirdparty.book.blackbook.xml.OptionAdjustmentSignum;
import com.firstlook.thirdparty.book.blackbook.xml.Options;

public class BlackBookBookoutService extends AbstractBookoutService {

	private static final Logger logger = Logger
			.getLogger(BlackBookBookoutService.class);

	/**
	 * The default length of time an entry is allowed to live in the cache.
	 */
	private static final long DEFAULT_CACHE_EXPIRATION = 7200000; // 2hrs

	/**
	 * The default length of time the sweeper task will sleep before checking to
	 * clean the cache.
	 */
	private static final long DEFAULT_CACHE_POLL_RATE = 600000; // 10min

	private IBlackBookAdapter blackBookAdapter;

	private GuideBookMetaInfo metaInfo;

	private static final int MAX_CACHE_ENTRIES = 50;

	// this cache is used to store vectors of blackBookCars returned from
	// BlackBook
	// limit the size of the cache to we don't use up unlimited amounts of
	// memory.
	protected Map<BlackBookCarCacheKey, List<BlackBookCar>> blackBookCarCache = new LinkedHashMap<BlackBookCarCacheKey, List<BlackBookCar>>(
			MAX_CACHE_ENTRIES, 0.75f, true) {
		
		private static final long serialVersionUID = 281408543111025359L;

		@Override
		protected boolean removeEldestEntry(
				Entry<BlackBookCarCacheKey, List<BlackBookCar>> eldest) {
			return size() >= MAX_CACHE_ENTRIES;
		}
	};

	// the monitor keeps track of when Cars were put in the cache.
	// A cleanup thread is created in the constructor which polls this monitor
	// and use it to remove expired vectors from the cache
	protected Map<BlackBookCarCacheKey, Long> blackBookCarCache_Monitor = new HashMap<BlackBookCarCacheKey, Long>(
			MAX_CACHE_ENTRIES);

	/**
	 * The length of time an entry is allowed to live in the cache. Once set, it
	 * cannot be changed for this instance.
	 */
	protected final long cacheExpiration;

	/**
	 * The length of time the sweeper task will sleep before checking to clean
	 * the cache. Once set, it cannot be changed for this instance.
	 */
	protected final long cachePollRate;

	private enum BlackBookCarCache_Monitor_ModificationMode {
		ADD, CLEAN
	}

	private Timer cacheTimer;

	public BlackBookBookoutService() throws GBException {
		this(DEFAULT_CACHE_EXPIRATION, DEFAULT_CACHE_POLL_RATE);
	}

	/**
	 * Constructor to specify how long to hold the cache around and how
	 * frequently it should be cleaned up. This affects the bookout processor.
	 * When using this class for bookout processor, setting the cache expiration
	 * and cache pool rate to low numbers is a must to preserve system memory.
	 * 
	 * @param cacheExpiration
	 *            time in milliseconds for marking entries in the cache as
	 *            invalid. Must be greater than 100 ms, to prevent too many http
	 *            requests out to blackbook. Defaults to 2 hrs (7200000 ms).
	 * @param cachePoolRate
	 *            time in milliseconds specifying the amount of time when the
	 *            cache cleaner will run next. Must be greater than 100 ms to
	 *            prevent this thread from hogging the cpu. Defaults to 10
	 *            minutes (600000 ms).
	 * @throws GBException
	 */
	public BlackBookBookoutService(long cacheExpiration, long cachePoolRate)
			throws GBException {
		super();
		// blackBookAdapter = new BlackBookAdapter();

		// prevent bad poll and expiration values
		this.cachePollRate = (cachePoolRate < 100) ? DEFAULT_CACHE_POLL_RATE
				: cachePoolRate;
		this.cacheExpiration = (cacheExpiration < 100) ? DEFAULT_CACHE_EXPIRATION
				: cacheExpiration;

		// TODO: This timerTask is causing processors to hang. The processors do
		// not exit because the TimerTask thread is still going even if the
		// bookouts are finished -bf. Jan. 11, 2006
		cacheTimer = new Timer("BlackBookBookoutService Cache Cleaner", true);
		cacheTimer.schedule(new CleanBlackBookCacheTask(), this.cachePollRate,
				this.cachePollRate);
		populateMetaInfo();
	}

	private void populateMetaInfo() throws GBException {
		metaInfo = new GuideBookMetaInfo();

		metaInfo.init(ThirdPartyDataProvider.FOOTER_BLACKBOOK,
				"PublishInfo not initialized",
				ThirdPartyDataProvider.NAME_BLACKBOOK,
				ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE,
				ThirdPartyDataProvider.IMAGE_NAME_BLACKBOOK,
				ThirdPartyDataProvider.BGCOLOR_BLACKBOOK);

	}

	public List<VDP_GuideBookVehicle> doVinLookup(String region, String vin,
			BookOutDatasetInfo bookoutInfo) throws GBException {
		List<BlackBookCar> blackBookCars = null;
		try {
			blackBookCars = getBlackBookCars(vin, region);
		} catch (ConnectException e) {
			throw new GBException(
					"Unable to Connect with BlackBook Web Service.");
		}

		List<VDP_GuideBookVehicle> guideBookVehicles = new ArrayList<VDP_GuideBookVehicle>();
		Iterator<BlackBookCar> blackBookCarsIter = blackBookCars.iterator();
		while (blackBookCarsIter.hasNext()) {
			BlackBookCar blackBookCar = blackBookCarsIter.next();
			VDP_GuideBookVehicle guideBookVehicle = new VDP_GuideBookVehicle();

			guideBookVehicle.setMake(blackBookCar.getMake());
			guideBookVehicle.setModel(blackBookCar.getModel());
			guideBookVehicle.setTrim(blackBookCar.getBody_style());
			guideBookVehicle.setBody(blackBookCar.getBody_style());
			guideBookVehicle.setSeries(blackBookCar.getSeries());
			guideBookVehicle.setKey(blackBookCar.getUvc());
			guideBookVehicle.setYear(blackBookCar.getYear().toString());
			guideBookVehicle.setWeight(blackBookCar.getWeight());

			StringBuffer description = new StringBuffer();
			description.append(blackBookCar.getModel());
			if (blackBookCar.getSeries() != null
					&& !blackBookCar.getSeries().equalsIgnoreCase("")) {
				description.append(" ").append(blackBookCar.getSeries());
			}
			if (blackBookCar.getBody_style() != null
					&& !blackBookCar.getBody_style().equalsIgnoreCase("")) {
				description.append(" ").append(blackBookCar.getBody_style());
			}
			guideBookVehicle.setDescription(description.toString());

			guideBookVehicles.add(guideBookVehicle);
		}
		return guideBookVehicles;
	}

	protected List<ThirdPartyVehicleOption> retrieveOptionsInternal(
			GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle,
			BookOutDatasetInfo bookoutInfo) throws GBException {
		BlackBookCar blackBookCar = getBlackBookCar(input.getVin(), input
				.getState(), guideBookVehicle.getKey());
		return extractThirdPartyVehicleOptions(blackBookCar);
	}

	private List<ThirdPartyVehicleOption> extractThirdPartyVehicleOptions(
			BlackBookCar blackBookCar) {
		List<ThirdPartyVehicleOption> carOptionList = new ArrayList<ThirdPartyVehicleOption>();
		int sortOrder = 0;

		Options options = null;
		if(blackBookCar != null) {
			options = blackBookCar.getOptions();
			if(options != null ) {
				List<Option> carOptions = options.getOption();
				Iterator<Option> iterator = carOptions.iterator();
				while (iterator.hasNext()) {
					Option option = iterator.next();
					
					// VVG is adding AA or DD onto the key.  However, the key coming from Blackbook doesn't have that
					// prefix added.  This is idiotic, but we need to add the prefix back on so that they match the
					// keys coming in on appraisals.
					String optionKey = "";
					
					if (option.getAddDeduct() == OptionAdjustmentSignum.ADD)
					{
						optionKey += "AA";
					}
					else if (option.getAddDeduct() == OptionAdjustmentSignum.DEDUCT)
					{
						optionKey += "DD";
					}
					
					optionKey += option.getAd_code();
					
					// Since black book has multiple values for a key, set the key to the object to the value.
					ThirdPartyOption thirdPartyOption = new ThirdPartyOption(
							option.getDescription(),
							option.getDescription(),
							ThirdPartyOptionType.EQUIPMENT);
					
					ThirdPartyVehicleOption thirdPartyVehicleOption = new ThirdPartyVehicleOption(
						thirdPartyOption, sortOrder);
					
					// However, black book needs the key stored somewhere so that mobile appraisals can be displayed correctly.
					thirdPartyVehicleOption.setTransientOptionKey(optionKey);
					
					thirdPartyVehicleOption.addOptionValue(new ThirdPartyVehicleOptionValue(
						thirdPartyVehicleOption,
						ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE,
						option.getSignedAmount()));
		
					carOptionList.add(thirdPartyVehicleOption);
					sortOrder++;
				}
			}
		}
		return carOptionList;
	}

	/**
	 * 
	 * @param vin
	 * @param state
	 * @param uvc
	 * @return the BlackBookCar that has the corresponding UVC
	 * @throws GBException
	 */
	private BlackBookCar getBlackBookCar(String vin, String state, String uvc)
			throws GBException {
		BlackBookCar result = null;
		List<BlackBookCar> blackBookCars = null;
		try {
			blackBookCars = getBlackBookCars(vin, state);
			Iterator<BlackBookCar> blackBookCarsIter = blackBookCars.iterator();
			while (blackBookCarsIter.hasNext()) {
				BlackBookCar blackBookCar = blackBookCarsIter.next();
				if (blackBookCar.getUvc().equalsIgnoreCase(uvc)) {
					result = blackBookCar;
					break;
				}
			}
		} catch (ConnectException e) {
			throw new GBException(
					"Unable to Connect with BlackBook Web Service.");
		}
		return result;
	}

	public List<VDP_GuideBookValue> retrieveBookValues(String regionCode,
			int mileage,
			List<ThirdPartyVehicleOption> thirdPartyVehicleOptions,
			VDP_GuideBookVehicle vehicle, String vin,
			BookOutDatasetInfo bookoutInfo) throws GBException {
		BlackBookCar car = getBlackBookCar(vin, regionCode, vehicle.getKey());
		
		//pass values back by reference.
		Integer msrp = null;
		if(car!= null) {
			try {
				msrp = Integer.parseInt(car.getMsrp());
			} catch (NumberFormatException nfe) {
				//do nothing
			}
			vehicle.setMsrp(msrp);
			vehicle.setWeight(car.getWeight());
		}
		
		return calculateBookValues(mileage, thirdPartyVehicleOptions, car);
	}

	private List<VDP_GuideBookValue> calculateBookValues(int mileage,
			List<ThirdPartyVehicleOption> thirdPartyVehicleOptions,
			BlackBookCar car) {
		ArrayList<VDP_GuideBookValue> guideBookValues = new ArrayList<VDP_GuideBookValue>();

		try {
			if (car != null) {
				Options optionsObj = car.getOptions();
				List<Option> selectedOptions = null;
				if(optionsObj != null) {
					selectedOptions = selectedOptions(thirdPartyVehicleOptions, optionsObj.getOption());
				} else {
					selectedOptions = new ArrayList<Option>();
				}
				
				BlackBookPriceTables tables = new BlackBookPriceTables(car, selectedOptions, mileage);
				BlackBookMarketType marketType = BlackBookMarketType.WHOLESALE;
				for(BlackBookCondition condition : BlackBookCondition.values()) {
					guideBookValues.add(new VDP_GuideBookValue(
							tables.getValue(marketType, condition, BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE),
							condition.getId(),
							condition.getDescription(),
							BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
					
					guideBookValues.add(new VDP_GuideBookValue(
							tables.getValue(marketType, condition, BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE),
							condition.getId(),
							condition.getDescription(),
							BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));
					
					guideBookValues.add(new VDP_GuideBookValue(
							tables.getValue(marketType, condition, BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE),
							condition.getId(),
							condition.getDescription(),
							BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE));
				}
				
				guideBookValues.add(new VDP_GuideBookValue(
						tables.getValue(marketType, BlackBookCondition.CLEAN, BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT),
						-1,
						"Mileage Adjustment",
						BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT));
			}
		} catch (Exception ae) {
			logger.error("Error calculating BlackBook values.", ae);
		}

		return guideBookValues;
	}
	
	private static List<Option> selectedOptions(
			Collection<ThirdPartyVehicleOption> thirdPartyVehicleOptions,
			List<Option> optionsFromBook) {
		
		List<Option> selectedOptions = new ArrayList<Option>();
		
		Map<String, Option> optionMap = new HashMap<String, Option>(optionsFromBook.size());
		for(Option option : optionsFromBook) {
			optionMap.put(option.getDescription().toLowerCase(), option);
		}
		
		if(thirdPartyVehicleOptions != null) {
			for(ThirdPartyVehicleOption optionFromDB : thirdPartyVehicleOptions) {
				if (optionFromDB != null && optionFromDB.isStatus()) {
					String key = optionFromDB.getOptionKey().toLowerCase();
					if(optionMap.containsKey(key)) {
						Option ref = optionMap.get(key);
						if(ref != null) {
							selectedOptions.add(ref);
						}
					}
				}
			}
		}

		return selectedOptions;
	}

	/**
	 * First checks the blackBookCarCache for the list of BlackBookCars
	 * matching the Vin and State. If no list is found, the blackBookAdapter
	 * is used to retreive the vector from the thirdparty BlackBook service.
	 * 
	 * @param request
	 * @param vin
	 * @param state
	 * @return
	 * @throws ConnectException
	 */
	protected List<BlackBookCar> getBlackBookCars(String vin, String state)
			throws ConnectException {
		// key should not be vin+state... vin is too granular! should be some
		// sort of squish vin
		// for the web application it might be ok, but this generates leaks in
		// the bookout processor
		BlackBookCarCacheKey key = new BlackBookCarCacheKey(vin, state);
		List<BlackBookCar> cars = blackBookCarCache.get(key);
		if (cars == null || cars.isEmpty()) {
			cars = getBlackBookAdapter().getBlackBookCars(vin, state);
			blackBookCarCache.put(key, cars);
			modifyBlackBookCarCache_Monitor(
					BlackBookCarCache_Monitor_ModificationMode.ADD, key);
		}
		return cars;
	}

	public IBlackBookAdapter getBlackBookAdapter() {
		return blackBookAdapter;
	}

	public void setBlackBookAdapter(IBlackBookAdapter blackBookAdapter) {
		this.blackBookAdapter = blackBookAdapter;
	}

	// ////////////////////INNER CLASSES
	// //////////////////////////////////////////////////

	private class BlackBookCarCacheKey {
		private String value;

		public BlackBookCarCacheKey(String vin, String state) {
			this.value = vin + "_" + state;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final BlackBookCarCacheKey other = (BlackBookCarCacheKey) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
	}

	/**
	 * A timerTask which, when called, removes any Vector in the
	 * blackBookCarCache that has existed for longer than the
	 * CAR_CACHE_EXPIRATION_LIMIT
	 */
	class CleanBlackBookCacheTask extends TimerTask {

		public void run() {
			modifyBlackBookCarCache_Monitor(
					BlackBookCarCache_Monitor_ModificationMode.CLEAN, null);
		}
	}

	// This exists to prevent concurrent modification exceptions on the
	// blackBookCarCache_Monitor becuase
	// it is written to and iterated over in two seperate methods - DW
	private synchronized void modifyBlackBookCarCache_Monitor(
			BlackBookCarCache_Monitor_ModificationMode mode,
			BlackBookCarCacheKey key) {
		switch (mode) {
		case CLEAN:
			List<BlackBookCarCacheKey> keysToRemove = new ArrayList<BlackBookCarCacheKey>();
			long now = System.currentTimeMillis();
			for (BlackBookCarCacheKey cacheKey : blackBookCarCache_Monitor
					.keySet()) {
				Long cacheEntryCreationTime = (Long) blackBookCarCache_Monitor
						.get(cacheKey);
				// if the entry in the cache has been around longer than the
				// expiration
				// limit
				if ((now - cacheEntryCreationTime.longValue()) > cacheExpiration) {
					blackBookCarCache.remove(cacheKey);
					keysToRemove.add(cacheKey);
				}
			}

			// remove key from monitor
			for (BlackBookCarCacheKey keyToRemove : keysToRemove) {
				blackBookCarCache_Monitor.remove(keyToRemove);
			}
			break;
		case ADD:
			blackBookCarCache_Monitor.put(key, Long.valueOf(System
					.currentTimeMillis()));
			break;
		default:
			break;
		}
	}

	public GuideBookMetaInfo getMetaInfo(String region,
			BookOutDatasetInfo bookoutDataset) {
		metaInfo.setPublishInfo(region);
		return metaInfo;
	}

	public void setMetaInfo(GuideBookMetaInfo footer) {
		this.metaInfo = footer;
	}

	public String retrieveRegionDescription(GuideBookInput input)
			throws GBException {
		return input.getState();
	}

	protected List<ThirdPartyVehicleOption> retrieveOptionsInternal(
			String regionCode, String year, String make, String model,
			String trim, BookOutDatasetInfo bookoutInfo) throws GBException {
		return retrieveOptionsManual(regionCode, year, make, model, trim, "");
	}

	public List<ThirdPartyVehicleOption> retrieveOptionsManual(
			String regionCode, String year, String make, String model,
			String trim, String style) throws GBException {
		Integer yearValue = null;
		try {
			yearValue = Integer.parseInt(year);
		} catch (NumberFormatException nfe) {
			throw new GBException(nfe);
		}
		BlackBookCar blackBookCar = blackBookAdapter.retrieveValues(new ModelYear(yearValue), new Make(make),
				model, trim, style, regionCode, 0);
		return extractThirdPartyVehicleOptions(blackBookCar);
	}

	public List<VDP_GuideBookValue> retrieveBookValues(String regionCode,
			int mileage,
			List<ThirdPartyVehicleOption> thirdPartyVehicleOptions,
			String year, String make, String model, String trim,
			BookOutDatasetInfo bookoutInfo) throws GBException {
		return retrieveBookValuesManual(regionCode, mileage,
				thirdPartyVehicleOptions, year, make, model, trim, "");
	}

	public List<VDP_GuideBookValue> retrieveBookValuesManual(String regionCode,
			int mileage,
			List<ThirdPartyVehicleOption> thirdPartyVehicleOptions,
			String year, String make, String model, String trim, String style)
			throws GBException {
		Integer yearValue = null;
		try {
			yearValue = Integer.parseInt(year);
		} catch (NumberFormatException nfe) {
			throw new GBException(nfe);
		}
		
		BlackBookCar car = blackBookAdapter.retrieveValues(new ModelYear(yearValue), new Make(make), model,
				trim, style, regionCode, mileage);
		return calculateBookValues(mileage, thirdPartyVehicleOptions, car);
	}

	public List<VDP_GuideBookVehicle> retrieveMakes(String year,
			BookOutDatasetInfo bookoutInfo) throws GBException {
		Integer yearValue = null;
		try {
			yearValue = Integer.parseInt(year);
		} catch (NumberFormatException nfe) {
			throw new GBException(nfe);
		}
		return blackBookAdapter.retrieveMakes(new ModelYear(yearValue));
	}

	public List<VDP_GuideBookVehicle> retrieveModels(String year, String make,
			BookOutDatasetInfo bookoutInfo) throws GBException {
		Integer yearValue = null;
		try {
			yearValue = Integer.parseInt(year);
		} catch (NumberFormatException nfe) {
			throw new GBException(nfe);
		}
		return blackBookAdapter.retrieveModels(new ModelYear(yearValue), new Make(make));
	}

	public List<VDP_GuideBookVehicle> retrieveTrims(String year, String make,
			String model, BookOutDatasetInfo bookoutInfo) throws GBException {
		Integer yearValue = null;
		try {
			yearValue = Integer.parseInt(year);
		} catch (NumberFormatException nfe) {
			throw new GBException(nfe);
		}
		return blackBookAdapter.retrieveSeries(new ModelYear(yearValue), new Make(make), model);
	}

	public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo)
			throws GBException {
		return blackBookAdapter.retrieveModelYears();
	}

	// specific to blackbook
	public List<VDP_GuideBookVehicle> retrieveStyles(String year, String make,
			String model, String series) throws GBException {
		Integer yearValue = null;
		try {
			yearValue = Integer.parseInt(year);
		} catch (NumberFormatException nfe) {
			throw new GBException(nfe);
		}
		return blackBookAdapter.retrieveStyles(new ModelYear(yearValue), new Make(make), model, series);
	}

	public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear(String vin,
			Integer year, BookOutDatasetInfo bookoutInfo) throws GBException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId,
			String modelId, BookOutDatasetInfo bookoutInfo) throws GBException {
		// TODO Auto-generated method stub
		return null;
	}



}
