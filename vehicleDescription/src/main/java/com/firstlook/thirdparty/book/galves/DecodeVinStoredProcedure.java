package com.firstlook.thirdparty.book.galves;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class DecodeVinStoredProcedure extends StoredProcedure {
	
	private static final String STORED_PROC_NAME = "dbo.VehicleCollection#DecodeVin";
	//private static final String PARAM_RC = "@RC";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_VIN = "@VIN";

	public DecodeVinStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new GalvesVehicleRowMapper()));
		//declareParameter( new SqlOutParameter( PARAM_RC, Types.INTEGER ) );
		declareParameter(new SqlParameter(PARAM_VIN, Types.VARCHAR));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<GalvesVehicle> decode(final String vin) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_VIN, vin);

		Map results = execute(parameters);
		
		//check return code?
		return (List<GalvesVehicle>)results.get(PARAM_RESULT_SET);
	}
}
