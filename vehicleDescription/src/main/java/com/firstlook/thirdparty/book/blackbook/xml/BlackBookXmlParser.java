package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class BlackBookXmlParser {
	
	private static final String RESID12 = "resid12";
	private static final String RESID24 = "resid24";
	private static final String RESID30 = "resid30";
	private static final String RESID36 = "resid36";
	private static final String RESID42 = "resid42";
	private static final String RESID48 = "resid48";
	private static final String RESID60 = "resid60";
	private static final String RESID72 = "resid72";
	
	private static final String EXTRA_CLEAN = "extra_clean";
	private static final String CLEAN = "clean";
	private static final String AVERAGE = "average";
	private static final String ROUGH = "rough";
	private static final String FINANCE_ADVANCE = "finance_advance";
	
	private String xml;
	private final List<BlackBookCar> cars = new ArrayList<BlackBookCar>();
	
	public BlackBookXmlParser(String xml) {
		this.xml = xml;
	}
	
	public void parse() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.parse(new InputSource(new StringReader(xml)));
        
        NodeList carNodes = doc.getElementsByTagName("car");
        for(int i = 0; i < carNodes.getLength(); ++i) {
        	Node carNode = carNodes.item(i);
        	BlackBookCar car = new BlackBookCar();
        	NodeList children = carNode.getChildNodes();
        	CarElementCallback cb = new CarElementCallback(car);
        	processChildrenNodes(children, cb);
        	cars.add(car);
        }
	}
	
	private static void processChildrenNodes(NodeList nodes, ElementCallback callback) {
		for(int j = 0; j < nodes.getLength(); ++j) {
    		Node node = nodes.item(j);
    		String name = node.getNodeName();
    		String value = node.getTextContent();
    		if(value != null) {
    			value = value.trim();
        		if(!"#text".equals(name)) {
	        		callback.process(node, name, value);
        		}
    		}
		}
	}
	
	private final static Integer parseIntegerValue(String value) {
		Integer theValue = null;
		try {
			theValue = Integer.valueOf(value);
		} catch (NumberFormatException nfe) {
			//log and ignore, leave null;
		}
		return theValue;
	}
	
	private final static Double parseDoubleValue(String value) {
		Double theValue = null;
		try {
			theValue = Double.valueOf(value);
		} catch (NumberFormatException nfe) {
			//log and ignore, leave null;
		}
		return theValue;
	}
	
	private interface ElementCallback {
		public void process(Node currentNode, String nodeName, String nodeValue);
	}
	
	private class CarElementCallback implements ElementCallback {
		
		private final BlackBookCar car;
		
		public CarElementCallback(BlackBookCar car) {
			this.car = car;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if("year".equals(nodeName)) {
				car.setYear(parseIntegerValue(nodeValue));
    		} else if ("make".equals(nodeName)){
    			car.setMake(nodeValue);
    		} else if ("model".equals(nodeName)) {
    			car.setModel(nodeValue);
    		} else if ("series".equals(nodeName)) {
    			car.setSeries(nodeValue);
    		} else if ("body_style".equals(nodeName)) {
    			car.setBody_style(nodeValue);
    		} else if ("vin".equals(nodeName)) {
    			car.setVin(nodeValue);
    		} else if ("uvc".equals(nodeName)) {
    			car.setUvc(nodeValue);
    		} else if ("mileage".equals(nodeName)) {
    			car.setMileage(nodeValue);
    		} else if ("msrp".equals(nodeName)) {
    			car.setMsrp(nodeValue);
    		} else if (FINANCE_ADVANCE.equals(nodeName)) {
    			car.setFinance_advance(nodeValue);
    		} else if ("residual_value".equals(nodeName)) {
    			Residual_value residualValue = new Residual_value();
    			ResidualValueElementCallback cb = new ResidualValueElementCallback(residualValue);
    			processChildrenNodes(currentNode.getChildNodes(), cb);
    			car.setResidual_value(residualValue);
    		} else if ("tradein_value".equals(nodeName)) {
    			Tradein_value value = new Tradein_value();
    			BookValueElementCallback cb = new BookValueElementCallback(value);
    			processChildrenNodes(currentNode.getChildNodes(), cb);
    			car.setTradein_value(value);
    		} else if ("wholesale_value".equals(nodeName)) {
    			Wholesale_value value = new Wholesale_value();
    			BookValueElementCallback cb = new BookValueElementCallback(value);
    			processChildrenNodes(currentNode.getChildNodes(), cb);
    			car.setWholesale_value(value);
    		} else if ("retail_value".equals(nodeName)) {
    			Retail_value value = new Retail_value();
    			BookValueElementCallback cb = new BookValueElementCallback(value);
    			processChildrenNodes(currentNode.getChildNodes(), cb);
    			car.setRetail_value(value);
    		} else if ("mileage_adjustments".equals(nodeName)) {
    			Mileage_adjustments adjustments = new Mileage_adjustments();
    			MileageAdjustmentsElementCallback cb = new MileageAdjustmentsElementCallback(adjustments);
    			processChildrenNodes(currentNode.getChildNodes(), cb);
    			car.setMileage_adjustments(adjustments);
    		} else if ("price_includes".equals(nodeName)) {
    			car.setPrice_includes(nodeValue);
    		} else if ("options".equals(nodeName)) {
    			Options options = new Options();
    			OptionSetElementCallback cb = new OptionSetElementCallback(options);
    			processChildrenNodes(currentNode.getChildNodes(), cb);
    			car.setOptions(options);
    		} else if ("wheel_base".equals(nodeName)) {
    			car.setWheel_base(parseDoubleValue(nodeValue));
    		} else if ("taxable_hp".equals(nodeName)) {
    			car.setTaxable_hp(parseDoubleValue(nodeValue));
    		} else if ("weight".equals(nodeName)) {
    			car.setWeight(parseIntegerValue(nodeValue));
    		} else if ("tire_size".equals(nodeName)) {
    			car.setTire_size(nodeValue);
    		} else if ("base_hp".equals(nodeName)) {
    			car.setBase_hp(nodeValue);
    		} else if ("fuel_type".equals(nodeName)) {
    			car.setFuel_type(nodeValue);
    		} else if ("cylinders".equals(nodeName)) {
    			car.setCylinders(parseIntegerValue(nodeValue));
    		} else if ("drive_train".equals(nodeName)) {
    			car.setDrive_train(nodeValue);
    		} else if ("transmission".equals(nodeName)) {
    			car.setTransmission(nodeValue);
    		} else if ("engine".equals(nodeName)) {
    			car.setEngine(nodeValue);
    		} else if ("veh_class".equals(nodeName)) {
    			car.setVeh_class(nodeValue);
    		} else if ("colors".equals(nodeName)) {
    			//hmm, not on the BlackBookCar class...
    		} else {
    			//unrecognized element.
    		}
		}
	}
	
	private class ResidualValueElementCallback implements ElementCallback {
		
		private final Residual_value residualValue;
		
		public ResidualValueElementCallback(Residual_value residualValue) {
			this.residualValue = residualValue;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if(RESID12.equals(nodeName)) {
    			residualValue.setResid12(nodeValue);
    		} else if (RESID24.equals(nodeName)) {
    			residualValue.setResid24(nodeValue);
    		} else if (RESID30.equals(nodeName)) {
    			residualValue.setResid30(nodeValue);
    		} else if (RESID36.equals(nodeName)) {
    			residualValue.setResid36(nodeValue);
    		} else if (RESID42.equals(nodeName)) {
    			residualValue.setResid42(nodeValue);
    		} else if (RESID48.equals(nodeName)) {
    			residualValue.setResid48(nodeValue);
    		} else if (RESID60.equals(nodeName)) {
    			residualValue.setResid60(nodeValue);
    		} else if (RESID72.equals(nodeName)) {
    			residualValue.setResid72(nodeValue);
    		} 
		}
	}
	
	private class BookValueElementCallback implements ElementCallback {
		
		private final BlackBookValueInternal value;
		
		public BookValueElementCallback(BlackBookValueInternal value) {
			this.value = value;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if(EXTRA_CLEAN.equals(nodeName)) {
				value.setExtra_clean(nodeValue);
    		} else if (CLEAN.equals(nodeName)) {
    			value.setClean(nodeValue);
    		} else if (AVERAGE.equals(nodeName)) {
    			value.setAverage(nodeValue);
    		} else if (ROUGH.equals(nodeName)) {
    			value.setRough(nodeValue);
    		}
		}
	}
	
	private class MileageAdjustmentsElementCallback implements ElementCallback {
		
		private final Mileage_adjustments adjustments;
		
		public MileageAdjustmentsElementCallback(Mileage_adjustments adjustments) {
			this.adjustments = adjustments;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if("mileage_range".equals(nodeName)) {
				Mileage_range range = new Mileage_range();
				MileageRangeElementCallback cb = new MileageRangeElementCallback(range);
				processChildrenNodes(currentNode.getChildNodes(), cb);
				adjustments.addMileage_range(range);
    		}
		}
	}
	
	private class MileageRangeElementCallback implements ElementCallback {
		
		private final Mileage_range range;
		
		public MileageRangeElementCallback(Mileage_range range) {
			this.range = range;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if("begin_range".equals(nodeName)) {
				range.setBegin_range(parseIntegerValue(nodeValue));
    		} else if ("end_range".equals(nodeName)) {
    			range.setEnd_range(parseIntegerValue(nodeValue));
    		} else if (FINANCE_ADVANCE.equals(nodeName)) {
    			range.setFinance_advance(parseIntegerValue(nodeValue));
    		} else if (EXTRA_CLEAN.equals(nodeName)) {
    			range.setExtra_clean(parseIntegerValue(nodeValue));
    		} else if (CLEAN.equals(nodeName)) {
    			range.setClean(parseIntegerValue(nodeValue));
    		} else if (AVERAGE.equals(nodeName)) {
    			range.setAverage(parseIntegerValue(nodeValue));
    		} else if (ROUGH.equals(nodeName)) {
    			range.setRough(parseIntegerValue(nodeValue));
    		}
		}
	}
	
	private class OptionSetElementCallback implements ElementCallback {
		
		private final Options options;
		
		public OptionSetElementCallback(Options options) {
			this.options = options;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if("option".equals(nodeName)) {
				Option option = new Option();
				OptionElementCallback cb = new OptionElementCallback(option);
				processChildrenNodes(currentNode.getChildNodes(), cb);
				options.addOption(option);
    		}
		}
	}
	
	private class OptionElementCallback implements ElementCallback {
		
		
		private final Option option;
		
		public OptionElementCallback(Option options) {
			this.option = options;
		}
		
		public void process(Node currentNode, String nodeName, String nodeValue) {
			if("ad_code".equals(nodeName)) {
				option.setAd_code(nodeValue);
    		} else if("description".equals(nodeName)) {
				option.setDescription(nodeValue);
    		} else if("add_deduct".equals(nodeName)) {
				option.setAdd_deduct(nodeValue);
    		} else if("amount".equals(nodeName)) {
				option.setAmount(nodeValue);
    		} else if(RESID12.equals(nodeName)) {
				option.setResid12(nodeValue);
    		} else if(RESID24.equals(nodeName)) {
				option.setResid24(nodeValue);
    		} else if(RESID30.equals(nodeName)) {
				option.setResid30(nodeValue);
    		} else if(RESID36.equals(nodeName)) {
				option.setResid36(nodeValue);
    		}  else if(RESID42.equals(nodeName)) {
				option.setResid42(nodeValue);
    		}  else if(RESID48.equals(nodeName)) {
				option.setResid48(nodeValue);
    		}  else if(RESID60.equals(nodeName)) {
				option.setResid60(nodeValue);
    		}  else if(RESID72.equals(nodeName)) {
				option.setResid72(nodeValue);
    		}  else if("flag".equals(nodeName)) {
				option.setFlag(nodeValue);
    		} 
		}
	}
	
	public List<BlackBookCar> getBlackBookCars() {
		return cars;
	}
}
