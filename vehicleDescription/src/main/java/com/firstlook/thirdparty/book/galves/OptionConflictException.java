package com.firstlook.thirdparty.book.galves;

class OptionConflictException extends OptionSelectionException {

	private static final long serialVersionUID = 8851561666513505218L;
	
	private String message;
	private String selectedOptionDescription;
	private String conflictingOptionDescription;
	
	public OptionConflictException(String selected, String conflictsWith) {
		super();
		
		StringBuilder errorMsgBdr = new StringBuilder();
		errorMsgBdr.append("Selected option '").append(selected).append("'");
		errorMsgBdr.append("conflicts with another selected option: ");
		if(conflictsWith != null) {
			errorMsgBdr.append("'").append(conflictsWith).append("'");
		} else {
			errorMsgBdr.append("ERROR");
		}
		this.message = errorMsgBdr.toString();
		this.selectedOptionDescription = selected;
		this.conflictingOptionDescription = conflictsWith;
	}
	
	@Override
	public String getMessage() {
		return message;
	}

	public String getSelectedOptionDescription() {
		return selectedOptionDescription;
	}

	public String getConflictingOptionDescription() {
		return conflictingOptionDescription;
	}
}
