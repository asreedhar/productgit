package com.firstlook.thirdparty.book.blackbook;

public enum BlackBookMarketType {
	WHOLESALE, RETAIL, TRADE_IN
}
