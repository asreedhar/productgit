package com.firstlook.thirdparty.book.blackbook;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.ConnectException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import biz.firstlook.commons.collections.Pair;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;

import com.firstlook.thirdparty.book.blackbook.xml.BlackBookCar;
import com.firstlook.thirdparty.book.blackbook.xml.BlackBookXmlParser;

/**
 * This class is responsible for calling out to the BlackBookService via HTTP.
 * One public method is exposed: getBlackBookCars() which takes a Vin and a
 * state. It returns a vector of BlackBookCars that BlackBook matched.
 * 
 * public scope for bookout processor.
 */
public class BlackBookAdapter extends JdbcDaoSupport implements IBlackBookAdapter, InitializingBean {

private static final String YEAR_MAKE_SQL = "SELECT * FROM VehicleCatalog.BlackBook.UsedCarYearMake UCYM ORDER BY make, year DESC";
private static final String connectionErrorMessage = PropertyLoader.getProperty("firstlook.errors.BlackBookError1");

private static Logger logger = Logger.getLogger( BlackBookAdapter.class );

private String host;
private int port;
private String url;

private MultiThreadedHttpConnectionManager connectionManager;
private HttpClient client;
private int socket_timeout = 3500;
private int connection_timeout = 5000;
// defaults
private int max_connections_to_blackBook = 2;
private int max_total_connections = 20;
private int idleConnectionTimeout = 60000;

private DocumentBuilderFactory blackBookXMLDocumentBuilderFactory;

private final Comparator<ModelYear> modelYearDescendingComparator = new Comparator<ModelYear>(){
	public int compare(ModelYear first, ModelYear second) {
		return second.compareTo(first);
	}};
	
//cache	
private TreeMap<ModelYear, TreeSet<Make>> modelYearMakeMap = new TreeMap<ModelYear, TreeSet<Make>>(modelYearDescendingComparator);


//cache timer
private Timer cacheTimer;

/**
 * This is SPRING managed class. You should probably not be calling this
 * constructor.
 * 
 */
public BlackBookAdapter() {
    configureBlackBookSettings();
}

private void configureBlackBookSettings() {
    max_connections_to_blackBook = Integer.valueOf( PropertyLoader.getProperty( "firstlook.blackbook.maxConnectionToHost", "2" ) ).intValue();
    max_total_connections = Integer.valueOf( PropertyLoader.getProperty( "firstlook.blackbook.maxTotalConnection", "20" ) ).intValue();
    socket_timeout = Integer.valueOf( PropertyLoader.getProperty( "firstlook.blackbook.socketTimeout", "3500" ) ).intValue();
    connection_timeout = Integer.valueOf( PropertyLoader.getProperty( "firstlook.blackbook.connectionTimeout", "5000" ) ).intValue();
    idleConnectionTimeout = Integer.valueOf( PropertyLoader.getProperty( "firstlook.blackbook.idleConnectionTimeout", "60000" ) ).intValue();

    HttpConnectionManagerParams httpConnectionManagerParams = new HttpConnectionManagerParams();
    httpConnectionManagerParams.setDefaultMaxConnectionsPerHost( max_connections_to_blackBook );
    httpConnectionManagerParams.setMaxTotalConnections( max_total_connections );
    httpConnectionManagerParams.setSoTimeout(socket_timeout); // no timeout
    httpConnectionManagerParams.setConnectionTimeout(connection_timeout); //
    // no timeout
    // httpConnectionManagerParams.setStaleCheckingEnabled(true);

    connectionManager = new MultiThreadedHttpConnectionManager();
    connectionManager.setParams( httpConnectionManagerParams );

    setHost( PropertyLoader.getProperty( "firstlook.blackbook.host" ) );
    setPort( Integer.valueOf( PropertyLoader.getProperty( "firstlook.blackbook.port", "80" ) ).intValue() );
    setUrl( PropertyLoader.getProperty( "firstlook.blackbook.url" ) );

    HostConfiguration conf = new HostConfiguration();
    conf.setHost( host, port );
    client = new HttpClient( connectionManager );
    client.setHostConfiguration( conf );
    
    blackBookXMLDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
	blackBookXMLDocumentBuilderFactory.setValidating(false);
	blackBookXMLDocumentBuilderFactory.setIgnoringComments(true);
	blackBookXMLDocumentBuilderFactory.setIgnoringElementContentWhitespace(true);
	blackBookXMLDocumentBuilderFactory.setNamespaceAware(false);

	cacheTimer = new Timer("BlackBookAdapter-CacheTimer", true);
	
	//blackBookRulesXmlUrl = "xml/Blackbook-rules.xml";
	//blackBookRulesXml = getClass().getResource(blackBookRulesXmlUrl);
}

/**
 * 
 * @param vin
 *            The VIN of the car being booked out.
 * @param state
 *            The state the trade is being analyzed.
 * @return A vector of BlackBookCar's
 * @throws ConnectionException
 * @throws ApplicationException -
 *             should throw specific blackbookexception instead
 */
public List<BlackBookCar> getBlackBookCars( String vin, String state ) throws ConnectException {
    String blackBookXML = retieveBlackBookXml( vin, state );
    List<BlackBookCar> cars = generateBlackBookCarsFromBlackBookXML( blackBookXML );
    return cars;
}

/**
 * Converts XML from BlackBook into a vector of our domain object BlackBookCar
 * 
 * @param xml
 * @return
 * @see <a href="http://wiki.apache.org/commons/Digester/FAQ">Digester best practices</a>
 */
synchronized List<BlackBookCar> generateBlackBookCarsFromBlackBookXML( String xml ) {
	List<BlackBookCar> carsList = new ArrayList<BlackBookCar>();
    try {        
    	BlackBookXmlParser parser = new BlackBookXmlParser(xml);
    	parser.parse();
    	carsList.addAll(parser.getBlackBookCars());
    } catch ( Exception e ) {
        logger.debug( "Problem parsing xml for blackbook", e );
    }
    
    return carsList;
}

/**
 * This is the method which makes the HTTP call out to the blackBook service.
 * Based on the VIN and State, BlackBook returns an XML response containing data
 * for 1 or more BlackBookCars.
 * 
 * @param vin
 * @param state
 * @return String The XML response containing data for one or more of
 * @throws ConnectionException
 * 
 */
private String retieveBlackBookXml( String vin, String state ) throws ConnectException {
    String xmlFromBlackBook = "";
    GetMethod method = createGetMethod( vin, state );
    try
    {
        // so   we don't use a stale connection
        // BB seems to reset connections regularly
        // so we can't keep it open continuously
        connectionManager.closeIdleConnections( idleConnectionTimeout );
        int httpStatusCode = client.executeMethod( method );

        if ( httpStatusCode != 200 )
        {
            logger.error( "BlackBookAdapter called failed with status code of " + httpStatusCode );
            throw new ConnectException( PropertyLoader.getProperty( "firstlook.errors.BlackBookError1" ) );
        }
        xmlFromBlackBook = readBlackBookResponse( method );
    }
    catch ( IOException he )
    {
        he.printStackTrace();
        throw new ConnectException( PropertyLoader.getProperty( "firstlook.errors.BlackBookError1" ) );
    }
    finally
    {
        method.releaseConnection();
    }
    return xmlFromBlackBook;
}

/**
 * @param method
 * @return
 * @throws IOException
 *
 **/
private String readBlackBookResponse( GetMethod method ) throws IOException {
    InputStream is = method.getResponseBodyAsStream();
    BufferedInputStream bis = new BufferedInputStream( is );
    String datastr = null;
    StringBuffer sb = new StringBuffer();
    byte[] bytes = new byte[8192]; // reading as chunk of 8192 bytes
    int count = bis.read( bytes );
    while ( count != -1 && count <= 8192 )
    {
        datastr = new String(bytes, 0, count);
        sb.append( datastr );
        count = bis.read( bytes );
    }
    bis.close();
    return sb.toString();
}

private GetMethod createGetMethod( String vin, String state ) {
    GetMethod method = new GetMethod( url );
    NameValuePair nvp1 = new NameValuePair( "vin", vin );
    NameValuePair nvp2 = new NameValuePair( "state", state );
    method.setQueryString( new NameValuePair[] { nvp1, nvp2 } );
    method.setFollowRedirects( true );
    return method;
}

public List<Integer> retrieveModelYears() {
	List<Integer> years = new ArrayList<Integer>();
	
	Set<ModelYear> modelYears = new LinkedHashSet<ModelYear>();
	if(logger.isDebugEnabled()) {
		logger.debug("Attempting to retrieve list of modelYears.");
	}
	synchronized (modelYearMakeMap) {
		modelYears.addAll(modelYearMakeMap.keySet());
	}
	if(logger.isDebugEnabled()) {
		logger.debug("Retrieved list of modelYears.");
	}
	
	for(ModelYear year : modelYears) {
		years.add(year.getModelYear());
	}

	return years;
}

public List<VDP_GuideBookVehicle> retrieveMakes(ModelYear year) {
	List<VDP_GuideBookVehicle> vehicles = new ArrayList<VDP_GuideBookVehicle>();
	
	Set<Make> makes = new LinkedHashSet<Make>();
	if(logger.isDebugEnabled()) {
		logger.debug("Attempting to retrieve list of makes for modelYear: " + year.toString());
	}
	synchronized (modelYearMakeMap) {
		makes.addAll(modelYearMakeMap.get(year));
	}
	if(logger.isDebugEnabled()) {
		logger.debug("Retrieved list of makes for modelYear: " + year.toString());
	}
	
	if(makes != null) {
		for(Make make : makes) {
			VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
			vehicle.setYear(year.getModelYear().toString());
			vehicle.setMake(make.toString());
			vehicle.setMakeId(make.toString());
			vehicles.add(vehicle);
		}
	}
	
	return vehicles;
}

public List<VDP_GuideBookVehicle> retrieveModels(ModelYear year, Make make) {
	List<VDP_GuideBookVehicle> result = new ArrayList<VDP_GuideBookVehicle>();
	String xmlFromBlackBook = "";
	GetMethod method = new GetMethod(url);
	NameValuePair nvp1 = new NameValuePair("year", year.toString());
	NameValuePair nvp2 = new NameValuePair("make", make.getName());
	method.setQueryString(new NameValuePair[] { nvp1, nvp2 });
	method.setFollowRedirects(true);
	try {
		connectionManager.closeIdleConnections(idleConnectionTimeout);
		int httpStatusCode = client.executeMethod(method);
		if (httpStatusCode != 200) {
			logger
					.error("BlackBookAdapter called failed with status code of "
							+ httpStatusCode);
			throw new ConnectException(connectionErrorMessage);
		}
		xmlFromBlackBook = readBlackBookResponse(method);
		DocumentBuilder newDocumentBuilder = blackBookXMLDocumentBuilderFactory
				.newDocumentBuilder();
		Document document = newDocumentBuilder.parse(new InputSource(
				new StringReader(xmlFromBlackBook)));
		Element root = document.getDocumentElement(); // <cars>
		NodeList cars = root.getElementsByTagName("car");
		for (int i = 0; i < cars.getLength(); i++) {
			NodeList children = ((Node) cars.item(i)).getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node child = (Node) children.item(j);
				if (child.getNodeName().equals("model")) {
					VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
					vehicle.setYear(year.toString());
					vehicle.setMake(make.toString());
					vehicle.setMakeId(make.getName());
					vehicle.setModel(child.getTextContent());
					vehicle.setModelId(child.getTextContent());
					result.add(vehicle);
				}
			}
		}
		if(logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("Found {0} models from blackbook for Year: {1} and Make: {2}", result.size(), year.toString(), make.getName()));
			logger.debug(xmlFromBlackBook);
		}
	} catch (IOException he) {
		logger.error("BlackBookAdapter called failed.", he);
	} catch (SAXException e) {
		logger.error(
				"BlackBookAdapter failed to parse the response from BlackBook: "
						+ xmlFromBlackBook, e);
	} catch (ParserConfigurationException e) {
		logger.error(
				"BlackBookAdapter failed to parse the response from BlackBook: "
						+ xmlFromBlackBook, e);
	} finally {
		method.releaseConnection();
	}

	return result;
}

public List<VDP_GuideBookVehicle> retrieveSeries(ModelYear year, Make make,
		String model) {
	List<VDP_GuideBookVehicle> result = new ArrayList<VDP_GuideBookVehicle>();
	String xmlFromBlackBook = "";
	GetMethod method = new GetMethod(url);
	NameValuePair nvp1 = new NameValuePair("year", year.toString());
	NameValuePair nvp2 = new NameValuePair("make", make.getName());
	NameValuePair nvp3 = new NameValuePair("model", model);
	NameValuePair nvp4 = new NameValuePair("stream", "MIN");
	method.setQueryString(new NameValuePair[] { nvp1, nvp2, nvp3, nvp4});
	method.setFollowRedirects(true);
	try {
		connectionManager.closeIdleConnections(idleConnectionTimeout);
		int httpStatusCode = client.executeMethod(method);
		if (httpStatusCode != 200) {
			logger
					.error("BlackBookAdapter called failed with status code of "
							+ httpStatusCode);
			throw new ConnectException(connectionErrorMessage);
		}
		xmlFromBlackBook = readBlackBookResponse(method);
		DocumentBuilder newDocumentBuilder = blackBookXMLDocumentBuilderFactory
				.newDocumentBuilder();
		Document document = newDocumentBuilder.parse(new InputSource(
				new StringReader(xmlFromBlackBook)));
		Element root = document.getDocumentElement(); // <cars>
		NodeList cars = root.getElementsByTagName("car");
		for (int i = 0; i < cars.getLength(); i++) {
			NodeList children = ((Node) cars.item(i)).getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node child = (Node) children.item(j);
				if (child.getNodeName().equals("series")
						&& child.getTextContent().length() > 0) {
					VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
					vehicle.setYear(year.toString());
					vehicle.setMake(make.toString());
					vehicle.setMakeId(make.getName());
					vehicle.setModel(model);
					vehicle.setModelId(model);
					vehicle.setSeries(child.getTextContent());
					result.add(vehicle);
				}
			}
		}
	} catch (IOException he) {
		logger.error("BlackBookAdapter called failed.", he);
	} catch (SAXException e) {
		logger.error(
				"BlackBookAdapter failed to parse the response from BlackBook: "
						+ xmlFromBlackBook, e);
	} catch (ParserConfigurationException e) {
		logger.error(
				"BlackBookAdapter failed to parse the response from BlackBook: "
						+ xmlFromBlackBook, e);
	} finally {
		method.releaseConnection();
	}

	return result;
}

public List<VDP_GuideBookVehicle> retrieveStyles(ModelYear year, Make make,
		String model, String series) {
	List<VDP_GuideBookVehicle> result = new ArrayList<VDP_GuideBookVehicle>();
	String xmlFromBlackBook = "";
	GetMethod method = new GetMethod(url);
	NameValuePair nvp1 = new NameValuePair("year", year.toString());
	NameValuePair nvp2 = new NameValuePair("make", make.getName());
	NameValuePair nvp3 = new NameValuePair("model", model);
	NameValuePair nvp4 = new NameValuePair("stream", "MIN");
	if (series != null && series.length() > 0) {
		NameValuePair nvp5 = new NameValuePair("series", series);			
		method.setQueryString(new NameValuePair[] { nvp1, nvp2, nvp3, nvp4, nvp5});
	} else {
		method.setQueryString(new NameValuePair[] { nvp1, nvp2, nvp3, nvp4});			
	}
	method.setFollowRedirects(true);
	try {
		connectionManager.closeIdleConnections(idleConnectionTimeout);
		int httpStatusCode = client.executeMethod(method);
		if (httpStatusCode != 200) {
			logger
					.error("BlackBookAdapter called failed with status code of "
							+ httpStatusCode);
			throw new ConnectException(connectionErrorMessage);
		}
		xmlFromBlackBook = readBlackBookResponse(method);
		DocumentBuilder newDocumentBuilder = blackBookXMLDocumentBuilderFactory
				.newDocumentBuilder();
		Document document = newDocumentBuilder.parse(new InputSource(
				new StringReader(xmlFromBlackBook)));
		Element root = document.getDocumentElement(); // <cars>
		NodeList cars = root.getElementsByTagName("car");
		for (int i = 0; i < cars.getLength(); i++) {
			NodeList children = ((Node) cars.item(i)).getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node child = (Node) children.item(j);
				if (child.getNodeName().equals("body_style")
						&& child.getTextContent().length() > 0) {
					VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
					vehicle.setYear(year.toString());
					vehicle.setMake(make.toString());
					vehicle.setMakeId(make.getName());
					vehicle.setModel(model);
					vehicle.setModelId(model);
					vehicle.setSeries(series);
					vehicle.setTrim(child.getTextContent());
					result.add(vehicle);
				}
			}
		}
	} catch (IOException he) {
		logger.error("BlackBookAdapter called failed.", he);
	} catch (SAXException e) {
		logger.error(
				"BlackBookAdapter failed to parse the response from BlackBook: "
						+ xmlFromBlackBook, e);
	} catch (ParserConfigurationException e) {
		logger.error(
				"BlackBookAdapter failed to parse the response from BlackBook: "
						+ xmlFromBlackBook, e);
	} finally {
		method.releaseConnection();
	}

	return result;
}

public BlackBookCar retrieveValues(ModelYear year, Make make, String model,
		String series, String style, String stateCode, int mileage) {
	BlackBookCar result = new BlackBookCar();
	String xmlFromBlackBook = "";
	List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	GetMethod method = new GetMethod(url);
	nvps.add(new NameValuePair("year", year.toString()));
	nvps.add(new NameValuePair("make", make.getName()));
	nvps.add(new NameValuePair("model", model));
	if (series != null && series.length() > 0) 
		nvps.add(new NameValuePair("series", series));
	if (style != null && style.length() > 0) 
		nvps.add(new NameValuePair("style", style));
	nvps.add(new NameValuePair("state", stateCode));
	nvps.add(new NameValuePair("mileage", String.valueOf(mileage)));
	method.setQueryString(nvps.toArray(new NameValuePair[nvps.size()]));
	method.setFollowRedirects(true);
	try {
		connectionManager.closeIdleConnections(idleConnectionTimeout);
		int httpStatusCode = client.executeMethod(method);
		if (httpStatusCode != 200) {
			logger
					.error("BlackBookAdapter called failed with status code of "
							+ httpStatusCode);
			throw new ConnectException(connectionErrorMessage);
		}
		xmlFromBlackBook = readBlackBookResponse(method);
		List<BlackBookCar> blackBookCars = generateBlackBookCarsFromBlackBookXML(xmlFromBlackBook);
		result = blackBookCars.get(0);
	} catch (IOException he) {
		logger.error("BlackBookAdapter called failed.", he);
	} finally {
		method.releaseConnection();
	}

	return result;
}


public String getHost()
{
    return host;
}

public void setHost( String host )
{
    this.host = host;
}

public int getPort()
{
    return port;
}

public void setPort( int port )
{
    this.port = port;
}

public String getUrl()
{
    return url;
}

public void setUrl( String url )
{
    this.url = url;
}

@Override
protected void initDao() throws Exception {
	super.initDao();
	
	long delay = 0; //run once right after spring is done loading.
	long period = 1000 * 60 * 60; //1000 millisecond/sec * 60 sec/min * 60 min/hour = X milli/hour
	cacheTimer.schedule(new TimerTask() {
		@SuppressWarnings("unchecked")
		public void run() {
			if(logger.isDebugEnabled()) {
				logger.debug("Refreshing the ModelYear-Make cache.");
			}
			
			List<Pair<ModelYear, Make>> results = (List<Pair<ModelYear, Make>>)getJdbcTemplate().query(YEAR_MAKE_SQL, new ModelYearMakeRowMapper());
				
			//order my makes!
			Map<ModelYear, TreeSet<Make>> tempCache = new TreeMap<ModelYear, TreeSet<Make>>(modelYearDescendingComparator);
			for(Pair<ModelYear, Make> pair : results) {
				ModelYear year = pair.getLeft();
				Make make = pair.getRight();
				TreeSet<Make> makes = tempCache.get(year);
				if(makes == null) {
					makes = new TreeSet<Make>();
					tempCache.put(year, makes);
				}
				makes.add(make);
			}
			synchronized (modelYearMakeMap) {
				modelYearMakeMap.clear();
				modelYearMakeMap.putAll(tempCache);
			}
			
			if(logger.isDebugEnabled()) {
				logger.debug("Refreshing the ModelYear-Make cache finished.");
			}
		}
	}, delay, period);
}


}
