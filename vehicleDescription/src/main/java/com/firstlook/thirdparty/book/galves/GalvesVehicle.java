package com.firstlook.thirdparty.book.galves;

import biz.firstlook.commons.util.StringConstants;

/**
 * A class representing a vehicle described by Galves.
 * 
 * @author bfung
 *
 */
class GalvesVehicle
{

private Integer id; /// Galves Id (ph_guid in DB)
private String vin;
private Integer year;
private String make;
private String model;
private String body;
private String engineType;

public GalvesVehicle(Integer id, String vin, Integer year, String make, String model,
		String body, String engineType) {
	super();
	this.id = id;
	this.vin = vin;
	this.year = year;
	this.make = make;
	this.model = model;
	this.body = body;
	this.engineType = engineType;
}

public Integer getId() {
	return id;
}

public String getVin() {
	return vin;
}

public Integer getYear() {
	return year;
}

public String getMake() {
	return make;
}

public String getModel() {
	return model;
}

public String getBody() {
	return body;
}

public String getEngineType() {
	return engineType;
}

@Override
public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("Id: ").append(id).append(StringConstants.NEW_LINE);
	sb.append("VIN: ").append(vin).append(StringConstants.NEW_LINE);
	sb.append("ModelYear: ").append(year).append(StringConstants.NEW_LINE);
	sb.append("Make: ").append(make).append(StringConstants.NEW_LINE);
	sb.append("Model: ").append(model).append(StringConstants.NEW_LINE);
	sb.append("Body: ").append(body).append(StringConstants.NEW_LINE);
	sb.append("EngineType: ").append(engineType).append(StringConstants.NEW_LINE);
	return sb.toString();
}

}
