package com.firstlook.thirdparty.book.blackbook;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.commons.collections.Pair;

class ModelYearMakeRowMapper implements RowMapper {

	/**
	 * Returns a list of stuff.
	 */
	public Object mapRow(ResultSet rs, int index) throws SQLException {
		Integer yearValue = Integer.valueOf(rs.getInt("Year"));
		String makeName = rs.getString("Make");
		return new Pair<ModelYear, Make>(new ModelYear(yearValue), new Make(makeName));
	}
}
