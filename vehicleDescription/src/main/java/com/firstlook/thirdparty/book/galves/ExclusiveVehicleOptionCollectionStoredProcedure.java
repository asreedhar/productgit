package com.firstlook.thirdparty.book.galves;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class ExclusiveVehicleOptionCollectionStoredProcedure extends StoredProcedure {
	
	private static final String STORED_PROC_NAME = "dbo.ExclusiveVehicleOptionCollection#Fetch";
	//private static final String PARAM_RC = "@RC";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_PH_GUID = "@GalvesVehicleID";

	public ExclusiveVehicleOptionCollectionStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new ExclusiveVehicleOptionMapper()));
		//declareParameter(new SqlOutParameter(PARAM_RC, Types.INTEGER) );
		declareParameter(new SqlParameter(PARAM_PH_GUID, Types.INTEGER));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ExclusiveVehicleOption> getExclusions(final Integer galvesVehicleId) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_PH_GUID, galvesVehicleId);

		Map results = execute(parameters);
		
		//check return code?
		return (List<ExclusiveVehicleOption>)results.get(PARAM_RESULT_SET);
	}
	
	private class ExclusiveVehicleOptionMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			return new ExclusiveVehicleOption(
					rs.getString("Selected"), 
					rs.getString("Excluded"));
		}
		
	}
}
