package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Mileage_adjustments implements Serializable
{

private static final long serialVersionUID = -7833726796249837464L;
protected List<Mileage_range> _Mileage_range = new ArrayList<Mileage_range>();

public Mileage_adjustments()
{
}

public List<Mileage_range> getMileage_range()
{
    return (_Mileage_range);
}

public Mileage_range getMileage_rangeAt( int index )
{
    if ( index < 0 || index >= _Mileage_range.size() )
    {
        return (null);
    }
    return _Mileage_range.get(index);
}

public int getMileage_rangeCount()
{
    if ( _Mileage_range == null )
    {
        return (0);
    }

    return (_Mileage_range.size());
}

public void setMileage_range( List<Mileage_range> newVector )
{

    if ( newVector == null )
    {
        _Mileage_range.clear();
    } else
    {
        _Mileage_range = newVector;
    }
}

public void addMileage_range( Mileage_range obj )
{
    if ( obj == null )
    {
        return;
    }

    _Mileage_range.add(obj);
}

public void setMileage_rangeAt( Mileage_range obj, int index )
{
    if ( obj == null )
    {
        return;
    }

    if ( index < 0 || index >= _Mileage_range.size() )
    {
        return;
    }
    _Mileage_range.add(index, obj);
}

public void removeMileage_range( Mileage_range obj )
{
    if ( obj == null )
    {
        return;
    }

    _Mileage_range.remove(obj);
}

public void removeMileage_rangeAt( int index )
{
    if ( index < 0 || index >= _Mileage_range.size() )
    {
        return;
    }

    _Mileage_range.remove(index);
}

public boolean hasData()
{

    return (true);
}

/**
 * @return the Mileage_range with the smallest Mileage_range.getBegin_range out of the collection.  
 * 		   null if there are no Mileage_range entries. 
 */
public Mileage_range getLowestMileageRange() {
	Mileage_range smallest = null;
	for(Mileage_range range : _Mileage_range) {
		if(smallest == null	|| 
			(smallest.getBegin_range() > range.getBegin_range())) {
			smallest = range;
		}
	}
	return smallest;
}

}
