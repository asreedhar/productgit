package com.firstlook.thirdparty.book.galves;

import java.text.MessageFormat;
import java.text.ParseException;

class GalvesVehicleOption {
	
	private final String id;
	private final String description;
	private final AmountCodeEnum amountCode;
	private final int amount;
	
	public GalvesVehicleOption(String id, String description,
			AmountCodeEnum amountCode, int amount) {
		super();
		this.id = id;
		this.description = description;
		this.amountCode = amountCode;
		this.amount = amount;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public AmountCodeEnum getAmountCode() {
		return amountCode;
	}

	public int getAmount() {
		return AmountCodeEnum.DED.equals(amountCode) ? -amount : amount;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(description).append(": ").append(getAmount());
		return sb.toString();
	}
	
	/**
	 * Indicates that this option is included in the price of the car and cannot be selected/deselected.
	 * @return
	 */
	public boolean isStandard() {
		return AmountCodeEnum.NUL.equals(amountCode);
	}
	
	public enum AmountCodeEnum {
		NUL, ADD, DED;
		
		public static AmountCodeEnum valueOfIgnoreCase(String amountCode) throws ParseException {
			AmountCodeEnum theAmountCode = null;
			if(NUL.toString().equalsIgnoreCase(amountCode)) {
				theAmountCode = NUL;
			} else if (ADD.toString().equalsIgnoreCase(amountCode)) {
				theAmountCode = ADD;
			} else if (DED.toString().equalsIgnoreCase(amountCode)) {
				theAmountCode = DED;
			} else {
				throw new ParseException(
						MessageFormat.format(
								"Unrecognized AmountCode: {0}, expecting NUL, ADD, or DED.", 
								amountCode), 0);
			}
			
			return theAmountCode;
		}
	}
}
