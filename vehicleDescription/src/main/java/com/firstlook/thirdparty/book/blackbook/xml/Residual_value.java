package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;

public class Residual_value implements Serializable
{

private static final long serialVersionUID = 6807719318646426160L;
protected String _Resid12 = null;
protected String _Resid24 = null;
protected String _Resid30 = null;
protected String _Resid36 = null;
protected String _Resid42 = null;
protected String _Resid48 = null;
protected String _Resid60 = null;
protected String _Resid72 = null;

public Residual_value()
{
}

public String getResid12()
{
    return (_Resid12);
}

public void setResid12( String newValue )
{
    _Resid12 = newValue;
}

public String getResid24()
{
    return (_Resid24);
}

public void setResid24( String newValue )
{
    _Resid24 = newValue;
}

public String getResid30()
{
    return (_Resid30);
}

public void setResid30( String newValue )
{
    _Resid30 = newValue;
}

public String getResid36()
{
    return (_Resid36);
}

public void setResid36( String newValue )
{
    _Resid36 = newValue;
}

public String getResid42()
{
    return (_Resid42);
}

public void setResid42( String newValue )
{
    _Resid42 = newValue;
}

public String getResid48()
{
    return (_Resid48);
}

public void setResid48( String newValue )
{
    _Resid48 = newValue;
}

public String getResid60() {
	return _Resid60;
}

public void setResid60(String resid60) {
	_Resid60 = resid60;
}

public String getResid72() {
	return _Resid72;
}

public void setResid72(String resid72) {
	_Resid72 = resid72;
}

}
