package com.firstlook.thirdparty.book.blackbook;

import java.io.Serializable;

/**
 * A Make as specified by blackbook.
 * @author bfung
 *
 */
class Make implements Comparable<Make>, Serializable {

	private static final long serialVersionUID = -9198895873969950054L;
	
	private String name;
	
	public Make(final String name) {
		if(name == null) {
			throw new IllegalArgumentException("Make expects a non null name.");
		}
		
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public int compareTo(Make other) {
		if(other == null) {
			throw new IllegalArgumentException("Cannot compare this Make object to null.");
		}
		
		String theirName = other.getName();
		return name.compareTo(theirName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Make))
			return false;
		Make other = (Make) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
