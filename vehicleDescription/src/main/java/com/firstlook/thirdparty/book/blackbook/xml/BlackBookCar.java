package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;


public class BlackBookCar implements Serializable
{

private static final long serialVersionUID = -4742061462590395587L;

protected Integer _Year = null;
protected String _Make = null;
protected String _Model = null;
protected String _Series = null;
protected String _Body_style = null;
protected String _Vin = null;
protected String _Uvc = null;
protected String _Mileage = null;
protected String _Msrp = null;
protected String _Finance_advance = null;
protected Residual_value _Residual_value = null;
protected Tradein_value _Tradein_value = null;
protected Wholesale_value _Wholesale_value = null;
protected Retail_value _Retail_value = null;
protected String _Price_includes = null;

protected Mileage_adjustments _Mileage_adjustments = null;
protected Options _Options = null;

//new fields as of May 1, 2009
protected Double wheel_base = null;
protected Double taxable_hp = null;
protected Integer weight = null;
protected String tire_size = null;
protected String base_hp = null;
protected String fuel_type = null;
protected Integer cylinders = null;
protected String drive_train = null;
protected String transmission = null;
protected String engine = null;
protected String veh_class = null;

public BlackBookCar()
{
}

public Integer getYear()
{
    return (_Year);
}

public void setYear( Integer newValue )
{
    _Year = newValue;
}

public boolean hasYear()
{
    return (_Year != null);
}

public void deleteYear()
{
    _Year = null;
}

public String getMake()
{
    return (_Make);
}

public void setMake( String newValue )
{
    _Make = newValue;
}

public boolean hasMake()
{
    return (_Make != null);
}

public void deleteMake()
{
    _Make = null;
}

public String getModel()
{
    return (_Model);
}

public void setModel( String newValue )
{
    _Model = newValue;
}

public boolean hasModel()
{
    return (_Model != null);
}

public void deleteModel()
{
    _Model = null;
}

public String getSeries()
{
    return (_Series);
}

public void setSeries( String newValue )
{
    _Series = newValue;
}

public boolean hasSeries()
{
    return (_Series != null);
}

public void deleteSeries()
{
    _Series = null;
}

public String getBody_style()
{
    return (_Body_style);
}

public void setBody_style( String newValue )
{
    _Body_style = newValue;
}

public boolean hasBody_style()
{
    return (_Body_style != null);
}

public void deleteBody_style()
{
    _Body_style = null;
}


public String getVin()
{
    return (_Vin);
}

public void setVin( String newValue )
{
    _Vin = newValue;
}

public boolean hasVin()
{
    return (_Vin != null);
}

public void deleteVin()
{
    _Vin = null;
}

public String getUvc()
{
    return (_Uvc);
}

public void setUvc( String newValue )
{
    _Uvc = newValue;
}

public boolean hasUvc()
{
    return (_Uvc != null);
}

public void deleteUvc()
{
    _Uvc = null;
}

public String getMileage()
{
    return (_Mileage);
}

public void setMileage( String newValue )
{
    _Mileage = newValue;
}

public boolean hasMileage()
{
    return (_Mileage != null);
}

public void deleteMileage()
{
    _Mileage = null;
}

public String getMsrp()
{
    return (_Msrp);
}

public void setMsrp( String newValue )
{
    _Msrp = newValue;
}

public boolean hasMsrp()
{
    return (_Msrp != null);
}

public void deleteMsrp()
{
    _Msrp = null;
}

public String getFinance_advance()
{
    return (_Finance_advance);
}

public void setFinance_advance( String newValue )
{
    _Finance_advance = newValue;
}

public boolean hasFinance_advance()
{
    return (_Finance_advance!= null);
}

public void deleteFinance_advance()
{
    _Finance_advance = null;
}

public Residual_value getResidual_value()
{
    return (_Residual_value);
}

public void setResidual_value( Residual_value obj )
{
    _Residual_value = obj;
}

public Tradein_value getTradein_value()
{
    return (_Tradein_value);
}

public void setTradein_value( Tradein_value obj )
{
    _Tradein_value = obj;
}

public Wholesale_value getWholesale_value()
{
    return (_Wholesale_value);
}

public void setWholesale_value( Wholesale_value obj )
{
    _Wholesale_value = obj;
}

public Retail_value getRetail_value()
{
    return (_Retail_value);
}

public void setRetail_value( Retail_value obj )
{
    _Retail_value = obj;
}

public String getPrice_includes()
{
    return (_Price_includes);
}

public void setPrice_includes( String newValue )
{
    _Price_includes = newValue;
}

public boolean hasPrice_includes()
{
    return (_Price_includes != null);
}

public void deletePrice_includes()
{
    _Price_includes = null;
}

public Mileage_adjustments getMileage_adjustments()
{
    return (_Mileage_adjustments);
}

public void setMileage_adjustments( Mileage_adjustments obj )
{
    _Mileage_adjustments = obj;
}

public Options getOptions()
{
    return (_Options);
}

public void setOptions( Options obj )
{
    _Options = obj;
}

public boolean hasData()
{

    if ( _Year == null )
    {
        return (false);
    }

    if ( _Make == null )
    {
        return (false);
    }

    if ( _Model == null )
    {
        return (false);
    }

    return (true);
}

public Double getWheel_base() {
	return wheel_base;
}

public void setWheel_base(Double wheel_base) {
	this.wheel_base = wheel_base;
}

public Double getTaxable_hp() {
	return taxable_hp;
}

public void setTaxable_hp(Double taxable_hp) {
	this.taxable_hp = taxable_hp;
}

public Integer getWeight() {
	return weight;
}

public void setWeight(Integer weight) {
	this.weight = weight;
}

public String getTire_size() {
	return tire_size;
}

public void setTire_size(String tire_size) {
	this.tire_size = tire_size;
}

public String getBase_hp() {
	return base_hp;
}

public void setBase_hp(String base_hp) {
	this.base_hp = base_hp;
}

public String getFuel_type() {
	return fuel_type;
}

public void setFuel_type(String fuel_type) {
	this.fuel_type = fuel_type;
}

public Integer getCylinders() {
	return cylinders;
}

public void setCylinders(Integer cylinders) {
	this.cylinders = cylinders;
}

public String getDrive_train() {
	return drive_train;
}

public void setDrive_train(String drive_train) {
	this.drive_train = drive_train;
}

public String getTransmission() {
	return transmission;
}

public void setTransmission(String transmission) {
	this.transmission = transmission;
}

public String getEngine() {
	return engine;
}

public void setEngine(String engine) {
	this.engine = engine;
}

public String getVeh_class() {
	return veh_class;
}

public void setVeh_class(String veh_class) {
	this.veh_class = veh_class;
}

}
