package com.firstlook.thirdparty.book.blackbook;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.util.StringConstants;
import biz.firstlook.transact.persist.model.BookOutValueType;

import com.firstlook.thirdparty.book.blackbook.xml.BlackBookCar;
import com.firstlook.thirdparty.book.blackbook.xml.BlackBook_value;
import com.firstlook.thirdparty.book.blackbook.xml.Mileage_adjustments;
import com.firstlook.thirdparty.book.blackbook.xml.Mileage_range;
import com.firstlook.thirdparty.book.blackbook.xml.Option;

/**
 * If the options change, the mileage adjustments must be recalculated, as
 * specified in Black Book: Used Car XML Portal User Documentation, 2009, page
 * 25.
 * 
 * 1. Get Base Values 2. Get Options Adjustment value 3. Get Mileage Adjustment
 * value 4. Get Final value
 * 
 * @author bfung
 * 
 */
class BlackBookPriceTables implements Serializable {

	private static final long serialVersionUID = -4320724412904117249L;

	private static final int MODEL_YEAR_DIFF_LIMIT = 6;

	private final BlackBookCar car;
	private final List<Option> selectedOptions = new ArrayList<Option>();
	private final Integer mileage;
	private final Map<BlackBookMarketType, PriceTable> priceTables = new HashMap<BlackBookMarketType, PriceTable>();

	public BlackBookPriceTables(final BlackBookCar car, final List<Option> selectedOptions, final Integer mileage) {
		super();

		this.car = car;
		this.selectedOptions.addAll(selectedOptions);
		this.mileage = mileage;

		validate();

		for (BlackBookMarketType marketType : BlackBookMarketType.values()) {
			priceTables.put(marketType, new PriceTable(marketType));
		}

		calculateBasePrices();
		calculateAddsDeducts();
		calculateMileageAdjustments();
		calculateFinalValues();
	}

	public Integer getValue(BlackBookMarketType marketType, BlackBookCondition condition, int bookOutValueTypeId) {
		PriceTable table = priceTables.get(marketType);
		return table.getValue(bookOutValueTypeId, condition);
	}

	private static class PriceTable implements Serializable {

		private static final long serialVersionUID = 6394649425424465981L;

		private final BlackBookMarketType marketType;

		private Map<BlackBookCondition, Integer> baseValues = new HashMap<BlackBookCondition, Integer>();
		private Map<BlackBookCondition, Integer> optionAdjustmentValues = new HashMap<BlackBookCondition, Integer>();
		private Map<BlackBookCondition, Integer> mileageAdjustmentValues = new HashMap<BlackBookCondition, Integer>();
		private Map<BlackBookCondition, Integer> finalValues = new HashMap<BlackBookCondition, Integer>();

		public PriceTable(BlackBookMarketType marketType) {
			this.marketType = marketType;
		}

		public void putValue(int bookOutValueTypeId, BlackBookCondition condition, Integer value) {
			Map<BlackBookCondition, Integer> valueMap = getValueMap(bookOutValueTypeId);
			valueMap.put(condition, value);
		}

		public Integer getValue(int bookOutValueTypeId, BlackBookCondition condition) {
			Map<BlackBookCondition, Integer> valueMap = getValueMap(bookOutValueTypeId);
			return valueMap.get(condition);
		}

		private Map<BlackBookCondition, Integer> getValueMap(int bookOutValueTypeId) {
			switch (bookOutValueTypeId) {
			case BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE:
				return baseValues;
			case BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE:
				return optionAdjustmentValues;
			case BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT:
				return mileageAdjustmentValues;
			case BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE:
				return finalValues;
			default:
				throw new IllegalArgumentException("BlackBook does not support BookOutValueTypeId: "
						+ bookOutValueTypeId);
			}
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();

			Formatter formatter = new Formatter(sb);
			formatter.format("Black Book %1$s %2$s", marketType, StringConstants.NEW_LINE);
			formatter.format("--------------------------------------------------------%1$s", StringConstants.NEW_LINE);
			formatter.format(
					"%1$7s %2$11s %3$5s %4$7s %5$5s %6$15s %7$s", // 56 char
					// in length
					" ", BlackBookCondition.EXTRA_CLEAN.getDescription(), BlackBookCondition.CLEAN.getDescription(),
					BlackBookCondition.AVERAGE.getDescription(), BlackBookCondition.ROUGH.getDescription(),
					BlackBookCondition.FINANCE_ADVANCE.getDescription(), StringConstants.NEW_LINE);
			formatter.format("%1$7s %2$11d %3$5d %4$7d %5$5d %6$15d %7$s", "Base", getValue(
					BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, BlackBookCondition.EXTRA_CLEAN), getValue(
					BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, BlackBookCondition.CLEAN), getValue(
					BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, BlackBookCondition.AVERAGE), getValue(
					BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, BlackBookCondition.ROUGH), getValue(
					BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, BlackBookCondition.FINANCE_ADVANCE),
					StringConstants.NEW_LINE);
			formatter.format("%1$7s %2$11d %3$5d %4$7d %5$5d %6$15d %7$s", "Options", getValue(
					BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, BlackBookCondition.EXTRA_CLEAN), getValue(
					BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, BlackBookCondition.CLEAN), getValue(
					BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, BlackBookCondition.AVERAGE), getValue(
					BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, BlackBookCondition.ROUGH), getValue(
					BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, BlackBookCondition.FINANCE_ADVANCE),
					StringConstants.NEW_LINE);
			formatter.format("%1$7s %2$11d %3$5d %4$7d %5$5d %6$15d %7$s", "Mileage", getValue(
					BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, BlackBookCondition.EXTRA_CLEAN), getValue(
					BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, BlackBookCondition.CLEAN), getValue(
					BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, BlackBookCondition.AVERAGE), getValue(
					BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, BlackBookCondition.ROUGH), getValue(
					BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, BlackBookCondition.FINANCE_ADVANCE),
					StringConstants.NEW_LINE);
			formatter.format("%1$7s %2$11d %3$5d %4$7d %5$5d %6$15d %7$s", "Total", getValue(
					BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE, BlackBookCondition.EXTRA_CLEAN), getValue(
					BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE, BlackBookCondition.CLEAN), getValue(
					BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE, BlackBookCondition.AVERAGE), getValue(
					BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE, BlackBookCondition.ROUGH), getValue(
					BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE, BlackBookCondition.FINANCE_ADVANCE),
					StringConstants.NEW_LINE);
			formatter.format("--------------------------------------------------------%1$s", StringConstants.NEW_LINE);
			return sb.toString();
		}
	}

	private void validate() {
		if (car == null) {
			throw new IllegalArgumentException("Expected a non-null instance of BlackBookCar.");
		}

		if (car.getYear() == null) {
			throw new NullPointerException("ModelYear not found on BlackBook car.");
		}

		if (car.getModel() == null) {
			throw new NullPointerException("Model not found on BlackBook car.");
		}

		if (mileage == null) {
			throw new IllegalArgumentException("Expected a non-null mileage value.");
		}
	}

	private void calculateBasePrices() {
		for (BlackBookMarketType marketType : BlackBookMarketType.values()) {
			PriceTable table = priceTables.get(marketType);
			for (BlackBookCondition condition : BlackBookCondition.values()) {
				table.putValue(BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, condition, getBasePrice(marketType,
						condition));

			}
		}
	}

	private Integer getBasePrice(BlackBookMarketType marketType, BlackBookCondition category) {
		switch (marketType) {
		case WHOLESALE:
			return getBasePrice(car.getWholesale_value(), category);
		case RETAIL:
			return getBasePrice(car.getRetail_value(), category);
		case TRADE_IN:
			return getBasePrice(car.getTradein_value(), category);
		default:
			throw new IllegalArgumentException("Unsupported BlackBookValuationType: " + marketType);
		}
	}

	private Integer getBasePrice(BlackBook_value values, BlackBookCondition category) {
		String stringValue = null;
		if (values != null) {
			switch (category) {
			case EXTRA_CLEAN:
				stringValue = values.getExtra_clean();
				break;
			case CLEAN:
				stringValue = values.getClean();
				break;
			case AVERAGE:
				stringValue = values.getAverage();
				break;
			case ROUGH:
				stringValue = values.getRough();
				break;
			case FINANCE_ADVANCE:
				stringValue = car.getFinance_advance();
				break;
			default:
				throw new IllegalArgumentException("Unsupported BlackBookPriceCategory: " + category);
			}
		}

		Integer value = null;
		try {
			value = Integer.parseInt(stringValue);
		}
		catch (NumberFormatException nfe) {
			// ignore
		}

		return value;
	}

	private void calculateAddsDeducts() {
		for (BlackBookCondition condition : BlackBookCondition.values()) {
			calculateAddsDeducts(condition);
		}
	}

	private void calculateAddsDeducts(BlackBookCondition condition) {
		int adjustment = 0;

		int yearDiff = calcYearDiff();
		boolean isExemptFromFiftyPercentRule = false;
		for (Option option : selectedOptions) {
			String adCode = option.getAd_code();
			if (adCode != null) {
				int optionValue = option.getSignedAmount();
				if (adCode.equalsIgnoreCase("RR") || adCode.equalsIgnoreCase("RG")) {
					isExemptFromFiftyPercentRule = true;
					if (yearDiff > MODEL_YEAR_DIFF_LIMIT) {
						switch (condition) {
						case EXTRA_CLEAN:
							optionValue = 0;
							break;
						case CLEAN:
							break;
						case FINANCE_ADVANCE: // use Average.
						case AVERAGE:
							optionValue += -500;
							break;
						case ROUGH:
							if (adCode.equalsIgnoreCase("RR")) {
								optionValue += -1200;
							}
							else {
								optionValue += -1050;
							}
							break;
						default:
							throw new IllegalArgumentException("Unsupported BlackBookPriceCategory: " + condition);
						}
					}
					else {
						String model = car.getModel();
						switch (condition) {
						case EXTRA_CLEAN:
							optionValue += 400;
							break;
						case CLEAN:
							break;
						case FINANCE_ADVANCE: // use Average.
						case AVERAGE:
							if (model.contains("1") || model.contains("2") || model.contains("3")) {
								optionValue += -600;
							}
							else {
								optionValue += -500;
							}
							break;
						case ROUGH:
							if (model.contains("1") || model.contains("2") || model.contains("3")) {
								optionValue += -1300;
							}
							else {
								optionValue += -1100;
							}
							break;
						default:
							throw new IllegalArgumentException("Unsupported BlackBookPriceCategory: " + condition);
						}
					}
				}
				adjustment += optionValue;
			}
		}

		// check 50% rule
		for (BlackBookMarketType marketType : BlackBookMarketType.values()) {
			PriceTable table = priceTables.get(marketType);
			Integer baseValue = table.getValue(BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, condition);
			if (baseValue != null) {

				if (yearDiff > MODEL_YEAR_DIFF_LIMIT && !isExemptFromFiftyPercentRule) {
					Integer fiftyPercentPublished = Math.round(baseValue.floatValue() / 2.0f);
					if (fiftyPercentPublished < Math.abs(adjustment)) {
						float newAdjustment = Integer.signum(adjustment) * (baseValue.floatValue() / 2.0f);
						adjustment = Float.valueOf(newAdjustment).intValue();
					}
				}

				if (baseValue.intValue() == 0) {
					// needs to denote N/A instead of 0
					table.putValue(BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, condition, 0);
				}
				else {
					table.putValue(BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, condition, adjustment);
				}
			}
			else {
				table.putValue(BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, condition, null);
			}
		}
	}

	private void calculateMileageAdjustments() {
		Mileage_adjustments adjustments = car.getMileage_adjustments();
		if (adjustments != null) {
			Mileage_range lowestRange = adjustments.getLowestMileageRange();
			Mileage_range myRange = null;
			if (lowestRange != null && mileage < lowestRange.getBegin_range()) {
				myRange = lowestRange;
				myRange.setExtra_clean(lowestRange.getExtra_clean() == null ? null : Integer.valueOf(0));
				myRange.setClean(lowestRange.getClean() == null ? null : Integer.valueOf(0));
				myRange.setAverage(lowestRange.getAverage() == null ? null : Integer.valueOf(0));
				myRange.setRough(lowestRange.getRough() == null ? null : Integer.valueOf(0));
				myRange.setFinance_advance(lowestRange.getFinance_advance() == null ? null : Integer.valueOf(0));
			}
			else if (adjustments.getMileage_range().isEmpty()) {
				myRange = new Mileage_range();
				myRange.setExtra_clean(Integer.valueOf(0));
				myRange.setClean(Integer.valueOf(0));
				myRange.setAverage(Integer.valueOf(0));
				myRange.setRough(Integer.valueOf(0));
				myRange.setFinance_advance(Integer.valueOf(0));
			}
			else {
				List<Mileage_range> ranges = adjustments.getMileage_range();
				if (ranges != null) {
					for (Mileage_range range : ranges) {
						if (range.covers(mileage)) {
							myRange = range;
							break;
						}
					}
				}
			}

			for (BlackBookCondition condition : BlackBookCondition.values()) {
				calculateMileageAdjustment(myRange, condition);
			}
		}
	}

	private void calculateMileageAdjustment(Mileage_range mileageRange, BlackBookCondition condition) {
		for (BlackBookMarketType marketType : BlackBookMarketType.values()) {
			PriceTable table = priceTables.get(marketType);
			Integer baseValue = table.getValue(BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, condition);
			Integer mileageAdjustment = null;
			if (baseValue != null) {
				if (baseValue.intValue() != 0 && mileageRange != null) {
					switch (condition) {
					case EXTRA_CLEAN:
						mileageAdjustment = mileageRange.getExtra_clean();
						break;
					case CLEAN:
						mileageAdjustment = mileageRange.getClean();
						break;
					case AVERAGE:
						mileageAdjustment = mileageRange.getAverage();
						break;
					case ROUGH:
						mileageAdjustment = mileageRange.getRough();
						break;
					case FINANCE_ADVANCE:
						mileageAdjustment = mileageRange.getFinance_advance();
						break;
					default:
						throw new IllegalArgumentException("Unsupported BlackBookPriceCategory: " + condition);
					}
				}
			}

			table.putValue(BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, condition, mileageAdjustment);
		}
	}

	private void calculateFinalValues() {
		for (BlackBookMarketType marketType : BlackBookMarketType.values()) {
			PriceTable table = priceTables.get(marketType);
			for (BlackBookCondition condition : BlackBookCondition.values()) {
				Integer baseValue = table.getValue(BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, condition);
				Integer baseAndOptions = null, publishedValue = null;
				if (baseValue != null) {
					baseAndOptions = publishedValue = baseValue;
					Integer optionAdjustment = table.getValue(BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, condition);
					if (optionAdjustment != null) {
						baseAndOptions += optionAdjustment;
						publishedValue += optionAdjustment;
					}
					Integer mileageAdjustment = table.getValue(BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, condition);
					if (mileageAdjustment != null) {
						 publishedValue += mileageAdjustment;
					}
					else {
						table.putValue(BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE, condition, null);
						table.putValue(BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE, condition, null);
						publishedValue = null;
					}

					// Mileage adjustment 50% rule, depends on opt adjustment,
					// hence publishedValue is carried through
					if (mileageAdjustment != null && publishedValue != null) {
						Integer fiftyPercentPublished = Math.round(baseAndOptions.floatValue() / 2.0f);
						if (fiftyPercentPublished < Math.abs(mileageAdjustment)) {
							// reverse out the mileageAdjustment to calculate a
							// new one
							float newMileageAdj = Integer.signum(mileageAdjustment)
									* (baseAndOptions.floatValue() / 2.0f);
							// there could be rounding errors between mileage
							// adjustment and the final value to be 1 off.
							table.putValue(BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT, condition, Math.round(newMileageAdj));
							publishedValue = Math.round(baseAndOptions.floatValue() + newMileageAdj);
						}
					}
				}
				table.putValue(BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE, condition, publishedValue);
			}
		}
	}

	/**
	 * Black Book: Used Car XML Portal User Documentation, 2009, page 6.
	 * 
	 * @return
	 */
	private int calcYearDiff() {
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);
		Date now = cal.getTime();
		cal.set(Calendar.MONTH, Calendar.APRIL);
		cal.set(Calendar.DAY_OF_MONTH, 24);
		Date aprilTwentyFourth = cal.getTime();

		int refYear;
		if (now.after(aprilTwentyFourth)) {
			refYear = cal.get(Calendar.YEAR);
		}
		else {
			refYear = cal.get(Calendar.YEAR) - 1;
		}

		return refYear - car.getYear();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (PriceTable table : priceTables.values()) {
			sb.append(table.toString());
			sb.append(StringConstants.NEW_LINE);
		}
		return sb.toString();
	}

}
