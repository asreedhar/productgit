package com.firstlook.thirdparty.book.galves;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

class GalvesVehicleRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int index) throws SQLException {
		return new GalvesVehicle(
				rs.getInt("ph_guid"),
				rs.getString("VIN"),
				rs.getInt("ph_year"),
				rs.getString("ph_manuf"),
				rs.getString("ph_model"),
				rs.getString("ph_body"), rs.getString("ph_engtype"));
	}
}
