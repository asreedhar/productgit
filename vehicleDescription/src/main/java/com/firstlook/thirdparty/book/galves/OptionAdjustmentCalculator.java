package com.firstlook.thirdparty.book.galves;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

class OptionAdjustmentCalculator {
	
	private final Map<String, List<String>> excludesReferenceList = new HashMap<String, List<String>>();
	
	private Map<String, String> excluded = new HashMap<String, String>();
	
	private int total = 0;
	
	public OptionAdjustmentCalculator(List<ExclusiveVehicleOption> rules) {
		//build excludesReferenceList
		if(rules != null) {
			for(ExclusiveVehicleOption rule : rules) {
				String key = rule.getSelectedOptionKey().trim().toUpperCase();
				List<String> list;
				if(excludesReferenceList.containsKey(key)) {
					list = excludesReferenceList.get(key);
				} else {
					list = new ArrayList<String>();
					excludesReferenceList.put(key, list);
				}
				list.add(rule.getExcludedOptionKey());
			}
		}
	}
	
	public void select(ThirdPartyVehicleOption opt) throws OptionConflictException, OptionSelectionException {
		String optionKey = opt.getOptionKey();
		if(optionKey != null) {
			optionKey = optionKey.trim().toUpperCase();
			if(excluded.containsKey(optionKey)) {
				throw new OptionConflictException(optionKey, excluded.get(optionKey));
			} else {
				total += opt.getValue();
				
				//add the opt's exclusions to the excluded map
				List<String> excludes = excludesReferenceList.get(optionKey);
				if(excludes != null) {
					for(String exclude : excludes) {
						//the exclude was excluded by the option
						excluded.put(exclude, optionKey);
					}
				}
			}
		} else {
			throw new OptionSelectionException(MessageFormat.format("Key was null for object: {0}", opt.toString()));
		}
	}
	
	public int getValue() {
		return total;
	}
}
