package com.firstlook.thirdparty.book.blackbook;

import java.net.ConnectException;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;

import com.firstlook.thirdparty.book.blackbook.xml.BlackBookCar;

public interface IBlackBookAdapter
{

public List<BlackBookCar> getBlackBookCars( String vin, String state ) throws ConnectException;

public String getHost();

public void setHost( String host );

public int getPort();

public void setPort( int port );

public String getUrl();

public void setUrl( String url );

// Manual Bookout Methods
public List<Integer> retrieveModelYears();
public List<VDP_GuideBookVehicle> retrieveMakes( ModelYear year);
public List< VDP_GuideBookVehicle > retrieveModels( ModelYear year, Make make);
public List< VDP_GuideBookVehicle > retrieveSeries( ModelYear year, Make make, String model);
public List<VDP_GuideBookVehicle> retrieveStyles(ModelYear year, Make make, String model, String series);
public BlackBookCar retrieveValues( ModelYear year, Make make, String model, String series, String style, String stateCode, int mileage);
}