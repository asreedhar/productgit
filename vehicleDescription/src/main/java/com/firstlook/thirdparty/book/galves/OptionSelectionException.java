package com.firstlook.thirdparty.book.galves;

class OptionSelectionException extends Exception {

	private static final long serialVersionUID = -6664272657813275908L;

	public OptionSelectionException() {
		super();
	}

	public OptionSelectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public OptionSelectionException(String message) {
		super(message);
	}

	public OptionSelectionException(Throwable cause) {
		super(cause);
	}
}
