package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;

public class Mileage_range implements Serializable
{

private static final long serialVersionUID = 285592371666677388L;
protected Integer _Begin_range = null;
protected Integer _End_range = null;
protected Integer _Loan_value = null;
protected Integer _Extra_clean = null;
protected Integer _Clean = null;
protected Integer _Average = null;
protected Integer _Rough = null;
protected Integer _Finance_advance = null;

public Mileage_range()
{
}

public boolean covers(int mileage) {
	boolean inRange = false;
	if(getBegin_range() != null && getEnd_range() != null) {
		if(mileage >= getBegin_range() && mileage <= getEnd_range()) {
			inRange = true;
		}
	}
	return inRange;
}

public Integer getBegin_range()
{
    return (_Begin_range);
}

public void setBegin_range( Integer newValue )
{
    _Begin_range = newValue;
}

public boolean hasBegin_range()
{
    return (_Begin_range != null);
}

public void deleteBegin_range()
{
    _Begin_range = null;
}

public Integer getEnd_range()
{
    return (_End_range);
}

public void setEnd_range( Integer newValue )
{
    _End_range = newValue;
}

public boolean hasEnd_range()
{
    return (_End_range != null);
}

public void deleteEnd_range()
{
    _End_range = null;
}

public Integer getLoan_value()
{
    return (_Loan_value);
}

public void setLoan_value( Integer newValue )
{
    _Loan_value = newValue;
}

public boolean hasLoan_value()
{
    return (_Loan_value != null);
}

public void deleteLoan_value()
{
    _Loan_value = null;
}

public Integer getExtra_clean()
{
    return (_Extra_clean);
}

public void setExtra_clean( Integer newValue )
{
    _Extra_clean = newValue;
}

public boolean hasExtra_clean()
{
    return (_Extra_clean != null);
}

public void deleteExtra_clean()
{
    _Extra_clean = null;
}

public Integer getFinance_advance()
{
    return (_Finance_advance);
}

public void setFinance_advance( Integer newValue )
{
    _Finance_advance = newValue;
}

public boolean hasFinance_advance()
{
    return (_Finance_advance != null);
}

public void deleteFinance_advance()
{
    _Finance_advance = null;
}


public Integer getClean()
{
    return (_Clean);
}

public void setClean( Integer newValue )
{
    _Clean = newValue;
}

public boolean hasClean()
{
    return (_Clean != null);
}

public void deleteClean()
{
    _Clean = null;
}

public Integer getAverage()
{
    return (_Average);
}

public void setAverage( Integer newValue )
{
    _Average = newValue;
}

public boolean hasAverage()
{
    return (_Average != null);
}

public void deleteAverage()
{
    _Average = null;
}

public Integer getRough()
{
    return (_Rough);
}

public void setRough( Integer newValue )
{
    _Rough = newValue;
}

public boolean hasRough()
{
    return (_Rough != null);
}

public void deleteRough()
{
    _Rough = null;
}

public boolean hasData()
{

    if ( _Begin_range == null )
    {
        return (false);
    }

    if ( _End_range == null )
    {
        return (false);
    }

    if ( _Loan_value == null )
    {
        return (false);
    }

    if ( _Extra_clean == null )
    {
        return (false);
    }

    if ( _Clean == null )
    {
        return (false);
    }

    if ( _Average == null )
    {
        return (false);
    }

    if ( _Rough == null )
    {
        return (false);
    }

    return (true);
}

}
