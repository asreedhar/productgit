package com.firstlook.thirdparty.book.blackbook.xml;

public interface BlackBook_value {
	public String getExtra_clean();
	public String getClean();
	public String getAverage();
	public String getRough();
}
