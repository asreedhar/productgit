package com.firstlook.thirdparty.book.galves;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.firstlook.thirdparty.book.galves.GalvesVehicleOption.AmountCodeEnum;

import biz.firstlook.module.bookout.AbstractBookoutService;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

public class GalvesService extends AbstractBookoutService
{

private Logger logger = Logger.getLogger(GalvesService.class);

private GuideBookMetaInfo metaInfo;

private DecodeVinStoredProcedure decodeVinStoredProcedure;
private VehicleOptionCollectionStoredProcedure vehicleOptionCollectionStoredProcedure;
private BaseValueStoredProcedure baseValueStoredProcedure;
private MileageAdjustmentStoredProcedure mileageAdjustmentStoredProcedure;
private ExclusiveVehicleOptionCollectionStoredProcedure exclusiveVehicleOptionCollectionStoredProcedure;

public GalvesService() throws GBException {
	super();
	logger.info("Initializing Galves Service");
	
	this.metaInfo = new GuideBookMetaInfo();
	metaInfo.init(
			ThirdPartyDataProvider.FOOTER_GALVES,
            "PublishInfo not initialized",
            ThirdPartyDataProvider.NAME_GALVES,
            ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE,
            ThirdPartyDataProvider.IMAGE_NAME_GAVLES,
            ThirdPartyDataProvider.BGCOLOR_GAVLES);
}

/**
 * Looks up the vin against the gavles DB and returns the list of VDP_GuideBookVehicles that match.
 * 
 * Note - Galves does not have multiple regions. This argument is ignored.
 */
public List<VDP_GuideBookVehicle> doVinLookup( String region, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	List< GalvesVehicle > galvesVehicle = decodeVinStoredProcedure.decode(vin);
	List< VDP_GuideBookVehicle > result = createVDP_GuideBookVehicleFromGalvesVehicles( galvesVehicle );
	return result;
}

public List<ThirdPartyVehicleOption> retrieveOptionsInternal( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo ) throws GBException {
	List<GalvesVehicleOption> options = vehicleOptionCollectionStoredProcedure.getOptions(
			Integer.valueOf(guideBookVehicle.getKey()), 
			input.getRegion(ThirdPartyDataProvider.GALVES.getThirdPartyId()));
	
	List<ThirdPartyVehicleOption> tpvos = new ArrayList<ThirdPartyVehicleOption>();
	if(options != null && !options.isEmpty()) {
		int size = options.size();
		for(int i = 0; i < size; ++i) {
			GalvesVehicleOption option = options.get(i);
			 
			String optionKey = null;
			
			if ((option.getAmountCode() == AmountCodeEnum.ADD) || (option.getAmountCode() == AmountCodeEnum.DED))
			{
				optionKey = option.getAmountCode().toString() + ": " + option.getId();
			}
			else
			{
				optionKey = option.getId();
			}
			
			ThirdPartyVehicleOption thirdPartyVehicleOption = new ThirdPartyVehicleOption(
					new ThirdPartyOption(
							option.getDescription(), 
							optionKey, 
							ThirdPartyOptionType.EQUIPMENT),
					option.isStandard(), i);

			thirdPartyVehicleOption.addOptionValue( 
					new ThirdPartyVehicleOptionValue(
							thirdPartyVehicleOption,
							ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE,
							option.getAmount()));
			
			tpvos.add(thirdPartyVehicleOption);
		}
	}
	
	return tpvos;
}

public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> incomingOptions, 
		VDP_GuideBookVehicle guideBookVehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException {
	
	final Integer galvesVehicleId = Integer.valueOf(guideBookVehicle.getKey());
	
	BaseValue base = baseValueStoredProcedure.getOptions(galvesVehicleId, regionCode);
	MileageAdjustmentValue mileageAdjustment = mileageAdjustmentStoredProcedure.getMileageAdjustment(galvesVehicleId, mileage, regionCode);
	
	OptionAdjustmentCalculator calc = new OptionAdjustmentCalculator(exclusiveVehicleOptionCollectionStoredProcedure.getExclusions(galvesVehicleId));
	try {
		for(ThirdPartyVehicleOption opt : incomingOptions) {
			if(!opt.isStandardOption() && opt.isStatus()) {
				calc.select(opt);
			}
		}
	} catch (OptionConflictException oce) {
		AbstractBookoutService.createOptionConflictError(oce.getSelectedOptionDescription(), oce.getConflictingOptionDescription());
	} catch (OptionSelectionException ose) {
		throw new GBException(ose.getMessage());
	}
	
	//add them up.
	//if no base value, result should be "N/A"
	int finalTradeInValue = base.getTradeInValue() <= 0 ? 0 : base.getTradeInValue() + mileageAdjustment.getValue() + calc.getValue();
	int finalMarketReadyValue = base.getMarketReadyValue() <= 0 ? 0 : base.getMarketReadyValue() + mileageAdjustment.getValue() + calc.getValue();
	
	//transform to VDP_GuideBookValue
	List<VDP_GuideBookValue> bookValues = new ArrayList<VDP_GuideBookValue>();
	bookValues.add( new VDP_GuideBookValue(
						finalTradeInValue,
						ThirdPartyCategory.GALVES_TRADEIN_TYPE,
						ThirdPartyCategory.GALVES_TRADEIN_DESCRIPTION,
						BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE));
	
	bookValues.add( new VDP_GuideBookValue(
						finalMarketReadyValue,
						ThirdPartyCategory.GALVES_MARKETREADY_TYPE,
						ThirdPartyCategory.GALVES_MARKETREADY_DESCRIPTION,
						BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE));
	
	bookValues.add( new VDP_GuideBookValue(
			mileageAdjustment.getValue(), 
			-1,
			"Mileage Adjustment",
			BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT));
	
	return bookValues;
}

public GuideBookMetaInfo getMetaInfo( String region, BookOutDatasetInfo bookoutDataset ) throws GBException
{
	// publisherInfo field for Galves Meta Info is blank 
	metaInfo.setPublishInfo( region );
	return metaInfo;
}

public String retrieveRegionDescription( GuideBookInput input ) throws GBException {
	return input.getState();
}

@Override
protected List<ThirdPartyVehicleOption> retrieveOptionsInternal( String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	throw new GBException("Method not implemented.");
}

public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	throw new GBException("Method not implemented.");
}

public List<VDP_GuideBookVehicle> retrieveMakes( String year, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	throw new GBException("Method not implemented.");
}

public List<VDP_GuideBookVehicle> retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	throw new GBException("Method not implemented.");
}

public List<VDP_GuideBookVehicle> retrieveTrims( String year, String makeId, String modelId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	throw new GBException("Method not implemented.");
}

public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo)
		throws GBException {
	throw new GBException("Method not implemented.");
}

/**
 * Simply maps all the GavlesVehicles in the list to a list of VDP_GuideBookVehicles
 * 
 * @param galvesVehicles
 * @return
 */
private static List< VDP_GuideBookVehicle > createVDP_GuideBookVehicleFromGalvesVehicles( List< GalvesVehicle > galvesVehicles ) {
	List< VDP_GuideBookVehicle > result = new ArrayList< VDP_GuideBookVehicle >();
	if(galvesVehicles != null && !galvesVehicles.isEmpty()) {
		for (GalvesVehicle gVehicle : galvesVehicles) {
			VDP_GuideBookVehicle vdpVehicle = new VDP_GuideBookVehicle();
			vdpVehicle.setKey( gVehicle.getId().toString());
			vdpVehicle.setYear( gVehicle.getYear().toString());
			vdpVehicle.setMake( gVehicle.getMake() );
			vdpVehicle.setModel( gVehicle.getModel() );
			vdpVehicle.setBody( gVehicle.getBody() );
			vdpVehicle.setEngineType( gVehicle.getEngineType() );
			
			StringBuilder description = new StringBuilder(gVehicle.getModel());
			description.append(" ").append(gVehicle.getEngineType());
			vdpVehicle.setDescription(description.toString());
			result.add( vdpVehicle );
		}
	}
	return result;
}

public void setDecodeVinStoredProcedure(
		DecodeVinStoredProcedure decodeVinStoredProcedure) {
	this.decodeVinStoredProcedure = decodeVinStoredProcedure;
}

public void setVehicleOptionCollectionStoredProcedure(
		VehicleOptionCollectionStoredProcedure vehicleOptionCollectionStoredProcedure) {
	this.vehicleOptionCollectionStoredProcedure = vehicleOptionCollectionStoredProcedure;
}

public void setBaseValueStoredProcedure(
		BaseValueStoredProcedure baseValueStoredProcedure) {
	this.baseValueStoredProcedure = baseValueStoredProcedure;
}

public void setMileageAdjustmentStoredProcedure(
		MileageAdjustmentStoredProcedure mileageAdjustmentStoredProcedure) {
	this.mileageAdjustmentStoredProcedure = mileageAdjustmentStoredProcedure;
}

public void setExclusiveVehicleOptionCollectionStoredProcedure(
		ExclusiveVehicleOptionCollectionStoredProcedure exclusiveVehicleOptionCollectionStoredProcedure) {
	this.exclusiveVehicleOptionCollectionStoredProcedure = exclusiveVehicleOptionCollectionStoredProcedure;
}

public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear(String vin,
		Integer year, BookOutDatasetInfo bookoutInfo) throws GBException {
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId,
		String modelId, BookOutDatasetInfo bookoutInfo) throws GBException {
	// TODO Auto-generated method stub
	return null;
}


}
