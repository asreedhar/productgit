package com.firstlook.thirdparty.book.blackbook;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public enum BlackBookCondition
{

EXTRA_CLEAN( ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE, 0, ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_DESCRIPTION), 
CLEAN( ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE, 1, ThirdPartyCategory.BLACKBOOK_CLEAN_DESCRIPTION), 
AVERAGE( ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE, 2, ThirdPartyCategory.BLACKBOOK_AVERAGE_DESCRIPTION), 
ROUGH( ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE, 3, ThirdPartyCategory.BLACKBOOK_ROUGH_DESCRIPTION),
FINANCE_ADVANCE( ThirdPartyCategory.BLACKBOOK_FINANCE_ADVANCE_TYPE, 4, ThirdPartyCategory.BLACKBOOK_FINANCE_ADVANCE_DESCRIPTION);

private Integer id;
private int displayOrder;
private String description;

private BlackBookCondition( Integer id, int displayOrder, String description )
{
	this.id = id;
	this.displayOrder = displayOrder;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public int getOrder()
{
	return displayOrder;
}

public Integer getId()
{
	return id;
}

public static BlackBookCondition valueOfDescription( String description )
{
	if( description == null)
		return null;
	
	if( description.equalsIgnoreCase( EXTRA_CLEAN.description ) )
		return EXTRA_CLEAN;
	else if ( description.equalsIgnoreCase( CLEAN.description ))
		return CLEAN;
	else if ( description.equalsIgnoreCase( AVERAGE.description ))
		return AVERAGE;
	else if ( description.equalsIgnoreCase( ROUGH.description ))
		return ROUGH;
	else if ( description.equalsIgnoreCase( FINANCE_ADVANCE.description ))
		return FINANCE_ADVANCE;
	else
		return null;
}

}
