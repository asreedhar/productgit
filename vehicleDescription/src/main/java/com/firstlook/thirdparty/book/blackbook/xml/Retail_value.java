package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;

public class Retail_value implements BlackBook_value, BlackBookValueInternal, Serializable
{

private static final long serialVersionUID = -6964841172292371368L;
protected String _Extra_clean = null;
protected String _Clean = null;
protected String _Average = null;
protected String _Rough = null;

public Retail_value()
{
}

public String getExtra_clean()
{
    return (_Extra_clean);
}

public void setExtra_clean( String newValue )
{
    _Extra_clean = newValue;
}

public boolean hasExtra_clean()
{
    return (_Extra_clean != null);
}

public void deleteExtra_clean()
{
    _Extra_clean = null;
}

public String getClean()
{
    return (_Clean);
}

public void setClean( String newValue )
{
    _Clean = newValue;
}

public boolean hasClean()
{
    return (_Clean != null);
}

public void deleteClean()
{
    _Clean = null;
}

public String getAverage()
{
    return (_Average);
}

public void setAverage( String newValue )
{
    _Average = newValue;
}

public boolean hasAverage()
{
    return (_Average != null);
}

public void deleteAverage()
{
    _Average = null;
}

public String getRough()
{
    return (_Rough);
}

public void setRough( String newValue )
{
    _Rough = newValue;
}

public boolean hasRough()
{
    return (_Rough != null);
}

public void deleteRough()
{
    _Rough = null;
}

public boolean hasData()
{

    if ( _Extra_clean == null )
    {
        return (false);
    }

    if ( _Clean == null )
    {
        return (false);
    }

    if ( _Average == null )
    {
        return (false);
    }

    if ( _Rough == null )
    {
        return (false);
    }

    return (true);
}

}
