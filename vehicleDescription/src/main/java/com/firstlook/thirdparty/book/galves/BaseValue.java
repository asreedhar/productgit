package com.firstlook.thirdparty.book.galves;

import java.util.Formatter;

import biz.firstlook.commons.util.StringConstants;

class BaseValue {
	
	private final int tradeInValue;
	private final int marketReadyValue;
	
	public BaseValue(int tradeInValue, int marketReadyValue) {
		this.tradeInValue = tradeInValue;
		this.marketReadyValue = marketReadyValue;
	}

	public int getTradeInValue() {
		return tradeInValue;
	}

	public int getMarketReadyValue() {
		return marketReadyValue;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		Formatter formatter = new Formatter(s);
		formatter.format("%1$26s %2$d %3$s", "Galves Trade In Value:", tradeInValue, StringConstants.NEW_LINE);
		formatter.format("%1$26s %2$d %3$s", "Galves Market Ready Value:", marketReadyValue, StringConstants.NEW_LINE);
		return formatter.toString();
	}
}
