package com.firstlook.thirdparty.book.blackbook.xml;

import java.io.Serializable;

public class Option implements Serializable
{

private static final long serialVersionUID = 8694266575418876135L;
private String ad_code = null;
private String flag = null;
protected String _Description = null;
protected String _Add_deduct = null;
protected String _Amount = null;
protected String _Resid12 = null;
protected String _Resid24 = null;
protected String _Resid30 = null;
protected String _Resid36 = null;
protected String _Resid42 = null;
protected String _Resid48 = null;
protected String _Resid60 = null;
protected String _Resid72 = null;

public Option()
{
}

public String getDescription()
{
    return (_Description);
}

public void setDescription( String newValue )
{
    _Description = newValue;
}

public boolean hasDescription()
{
    return (_Description != null);
}

public void deleteDescription()
{
    _Description = null;
}

public String getAdd_deduct()
{
    return (_Add_deduct);
}

public void setAdd_deduct( String newValue )
{
    _Add_deduct = newValue;
}

public boolean hasAdd_deduct()
{
    return (_Add_deduct != null);
}

public void deleteAdd_deduct()
{
    _Add_deduct = null;
}

public String getAmount()
{
    return (_Amount);
}

public void setAmount( String newValue )
{
    _Amount = newValue;
}

public boolean hasAmount()
{
    return (_Amount != null);
}

public void deleteAmount()
{
    _Amount = null;
}

public String getResid12()
{
    return (_Resid12);
}

public void setResid12( String newValue )
{
    _Resid12 = newValue;
}

public boolean hasResid12()
{
    return (_Resid12 != null);
}

public void deleteResid12()
{
    _Resid12 = null;
}

public String getResid24()
{
    return (_Resid24);
}

public void setResid24( String newValue )
{
    _Resid24 = newValue;
}

public boolean hasResid24()
{
    return (_Resid24 != null);
}

public void deleteResid24()
{
    _Resid24 = null;
}

public String getResid30()
{
    return (_Resid30);
}

public void setResid30( String newValue )
{
    _Resid30 = newValue;
}

public boolean hasResid30()
{
    return (_Resid30 != null);
}

public void deleteResid30()
{
    _Resid30 = null;
}

public String getResid36()
{
    return (_Resid36);
}

public void setResid36( String newValue )
{
    _Resid36 = newValue;
}

public boolean hasResid36()
{
    return (_Resid36 != null);
}

public void deleteResid36()
{
    _Resid36 = null;
}

public String getResid42()
{
    return (_Resid42);
}

public void setResid42( String newValue )
{
    _Resid42 = newValue;
}

public boolean hasResid42()
{
    return (_Resid42 != null);
}

public void deleteResid42()
{
    _Resid42 = null;
}

public String getResid48()
{
    return (_Resid48);
}

public void setResid48( String newValue )
{
    _Resid48 = newValue;
}

public boolean hasResid48()
{
    return (_Resid48 != null);
}

public void deleteResid48()
{
    _Resid48 = null;
}

public String getAd_code()
{
    return ad_code;
}

public void setAd_code( String string )
{
    ad_code = string;
}

public String getFlag()
{
    return flag;
}

public void setFlag( String string )
{
    flag = string;
}

public OptionAdjustmentSignum getAddDeduct() {
	OptionAdjustmentSignum signum = OptionAdjustmentSignum.UNKNOWN;
	String addDeduct = getAdd_deduct();
	if(addDeduct != null) {
		if(addDeduct.equalsIgnoreCase("Add")) {
			signum = OptionAdjustmentSignum.ADD;
		} else if(addDeduct.equalsIgnoreCase("Deduct")) {
			signum = OptionAdjustmentSignum.DEDUCT;
		}
	}	
	return signum;
}

public int getSignedAmount() {
	String stringValue = getAmount();
	int val = 0;
	try {
		val = Integer.parseInt(stringValue); 
	} catch(NumberFormatException nfe) {
		val = 0;
	}
	
	OptionAdjustmentSignum signum = getAddDeduct();
	switch(signum) {
		case ADD:
			return val;
		case DEDUCT:
			return -val;
		default:
			return 0;
	}
}

public String getResid60() {
	return _Resid60;
}

public void setResid60(String resid60) {
	_Resid60 = resid60;
}

public String getResid72() {
	return _Resid72;
}

public void setResid72(String resid72) {
	_Resid72 = resid72;
}

}
