package com.firstlook.thirdparty.book.blackbook.xml;

/**
 * Package scoped interface to faciliate building of objects.
 * @author bfung
 *
 */
interface BlackBookValueInternal extends BlackBook_value {
	public void setExtra_clean(String extraClean);
	public void setClean(String clean);
	public void setAverage(String average);
	public void setRough(String rough);
}
