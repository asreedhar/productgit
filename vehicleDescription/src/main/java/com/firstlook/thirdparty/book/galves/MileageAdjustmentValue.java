package com.firstlook.thirdparty.book.galves;

class MileageAdjustmentValue {

	private final int mileage;
	private final int baseMileage;
	private final int mileageAdjustmentValue;
	
	public MileageAdjustmentValue(int mileage, int baseMileage, int mileageAdjustmentValue) {
		this.mileage = mileage;
		this.baseMileage = baseMileage;
		this.mileageAdjustmentValue = mileageAdjustmentValue;
	}

	public int getMileage() {
		return mileage;
	}

	public int getBaseMileage() {
		return baseMileage;
	}

	public int getValue() {
		return mileageAdjustmentValue;
	}
}
