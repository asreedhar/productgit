package com.firstlook.thirdparty.book.galves;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class BaseValueStoredProcedure extends StoredProcedure {
	
	private static final String STORED_PROC_NAME = "dbo.BaseValue#Fetch";
	//private static final String PARAM_RC = "@RC";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_PH_GUID = "@GalvesVehicleID";
	private static final String PARAM_REGION = "@Region";

	public BaseValueStoredProcedure(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new BaseValueMapper()));
		//declareParameter(new SqlOutParameter(PARAM_RC, Types.INTEGER) );
		declareParameter(new SqlParameter(PARAM_PH_GUID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_REGION, Types.CHAR));
		compile();
	}

	/**
	 * Returns the BookoutId saved
	 * @param inventoryBookoutXml
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public BaseValue getOptions(final Integer galvesVehicleId, final String region) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_PH_GUID, galvesVehicleId);
		parameters.put(PARAM_REGION, region);

		Map results = execute(parameters);
		
		//check return code?
		List<BaseValue> resultList = (List<BaseValue>)results.get(PARAM_RESULT_SET);
		BaseValue theValue = null;
		if(resultList != null && !resultList.isEmpty()) {
			theValue = resultList.get(0);
		}
		
		return theValue; 
	}
	
	private class BaseValueMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			return new BaseValue(rs.getInt("GalvesValue"), rs.getInt("MarketReadyValue"));
		}
		
	}
}
