package biz.firstlook.service.insight.v1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;

import biz.firstlook.service.insight.GetInsightRequest;
import biz.firstlook.service.insight.GetInsightResponse;
import biz.firstlook.service.insight.support.PerformanceInsightRequestFixture;

public class CustomPerformanceInsightWebServiceClient {

	private static XFireProxyFactory proxyFactory = new XFireProxyFactory();

	private HashMap<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();

	private Service service0;

	public CustomPerformanceInsightWebServiceClient() {
		create0();
		Endpoint PerformanceInsightWebServiceLocalEndpointEP = service0.addEndpoint(new QName(
				"http://www.firstlook.biz/service/insight/v1",
				"PerformanceInsightWebServiceLocalEndpoint"), new QName(
				"http://www.firstlook.biz/service/insight/v1",
				"PerformanceInsightWebServiceLocalBinding"),
				"xfire.local://PerformanceInsightWebService");
		endpoints.put(new QName("http://www.firstlook.biz/service/insight/v1",
				"PerformanceInsightWebServiceLocalEndpoint"),
				PerformanceInsightWebServiceLocalEndpointEP);
		Endpoint PerformanceInsightWebServiceSOAPEP = service0
				.addEndpoint(new QName("http://www.firstlook.biz/service/insight/v1",
						"PerformanceInsightWebServiceSOAP"), new QName(
						"http://www.firstlook.biz/service/insight/v1",
						"PerformanceInsightWebServiceSOAP"),
						"http://localhost:8080/firstlook-external-ws/services/PerformanceInsightWebService");
		endpoints.put(new QName("http://www.firstlook.biz/service/insight/v1",
				"PerformanceInsightWebServiceSOAP"), PerformanceInsightWebServiceSOAPEP);
	}

	public Object getEndpoint(Endpoint endpoint) {
		try {
			return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
		} catch (MalformedURLException e) {
			throw new XFireRuntimeException("Invalid URL", e);
		}
	}

	public Object getEndpoint(QName name) {
		Endpoint endpoint = ((Endpoint) endpoints.get((name)));
		if ((endpoint) == null) {
			throw new IllegalStateException("No such endpoint!");
		}
		return getEndpoint((endpoint));
	}

	public Collection<Endpoint> getEndpoints() {
		return endpoints.values();
	}

	private void create0() {
		TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire()
				.getTransportManager());
		HashMap<String, Object> props = new HashMap<String, Object>();
		props.put("annotations.allow.interface", true);
		AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm,
				new AegisBindingProvider(new JaxbTypeRegistry()));
		asf.setBindingCreationEnabled(false);
		service0 = asf.create(
				(biz.firstlook.service.insight.v1.PerformanceInsightWebService.class), props);
		{
			asf.createSoap11Binding(service0, new QName(
					"http://www.firstlook.biz/service/insight/v1",
					"PerformanceInsightWebServiceLocalBinding"), "urn:xfire:transport:local");
		}
		{
			asf.createSoap11Binding(service0, new QName(
					"http://www.firstlook.biz/service/insight/v1",
					"PerformanceInsightWebServiceSOAP"), "http://schemas.xmlsoap.org/soap/http");
		}
	}

	public PerformanceInsightWebService getPerformanceInsightWebServiceLocalEndpoint() {
		return ((PerformanceInsightWebService) (this).getEndpoint(new QName(
				"http://www.firstlook.biz/service/insight/v1",
				"PerformanceInsightWebServiceLocalEndpoint")));
	}

	public PerformanceInsightWebService getPerformanceInsightWebServiceLocalEndpoint(String url) {
		PerformanceInsightWebService var = getPerformanceInsightWebServiceLocalEndpoint();
		org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
		return var;
	}

	public PerformanceInsightWebService getPerformanceInsightWebServiceSOAP() {
		return ((PerformanceInsightWebService) (this).getEndpoint(new QName(
				"http://www.firstlook.biz/service/insight/v1", "PerformanceInsightWebServiceSOAP")));
	}

	public PerformanceInsightWebService getPerformanceInsightWebServiceSOAP(String url) {
		PerformanceInsightWebService var = getPerformanceInsightWebServiceSOAP();
		org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
		return var;
	}

	public static void main(String[] args) throws DatatypeConfigurationException, JAXBException,
			FileNotFoundException {

		JAXBContext jaxbContext = JAXBContext.newInstance("biz.firstlook.service.insight");
		GetInsightRequest request = PerformanceInsightRequestFixture.createGetInsightRequest();
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);

		Protocol protocol = new Protocol("https",
				(ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
		Protocol.registerProtocol("https", protocol);

		PerformanceInsightWebServiceClient client = new PerformanceInsightWebServiceClient();
		PerformanceInsightWebService service = client.getPerformanceInsightWebServiceSOAP();

		marshaller.marshal(request, new FileOutputStream(
				"src/main/wsdl/insight-request-sample.xml"));

		GetInsightResponse response = service.getInsights(request);
		System.out.println("Exit code is : " + response.getExitCode().value());
		System.out.println("Response Message is : " + response.getMessage());

		marshaller.marshal(response, new FileOutputStream(
				"src/main/wsdl/insight-response-sample.xml"));

	}

}
