package biz.firstlook.service.appraisal.v1;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;

import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;
import biz.firstlook.service.appraisal.CreateFullAppraisalResponse;

class CustomWirelessClientRunner {
	public static void main(String[] args)
			throws DatatypeConfigurationException {

		Protocol protocol = new Protocol("https",
				(ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
		Protocol.registerProtocol("https", protocol);

		// HttpClient client = new HttpClient(new
		// MultiThreadedHttpConnectionManager());
		// client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);
		// GetMethod get = new
		// GetMethod("https://preprod.firstlook.biz/firstlook-external-ws/");
		// // GetMethod get = new GetMethod("https://www.google.com");
		// get.setFollowRedirects(true);
		// String strGetResponseBody;
		// try {
		// strGetResponseBody = get.getResponseBodyAsString();
		// int result = client.executeMethod(get);
		// System.out.println("result:  " + strGetResponseBody);
		// } catch (IOException e) {
		// e.printStackTrace();
		// } finally {
		// get.releaseConnection();
		// }
		//	    	
		// System.out.println("Done");

		AppraisalWebServiceWirelessClient client = new AppraisalWebServiceWirelessClient();
		AppraisalWebServiceWireless service = client
				.getAppraisalWebServiceWirelessSOAP();
		CreateFullAppraisalRequest request = CreateAppraisalRequestFixture
				.getCreateFullAppraisalRequest();

		CreateFullAppraisalResponse response = service
				.createFullAppraisal(request);
		System.out.println("Exit code is : " + response.getExitCode().value());
		System.out.println("Response Message is : " + response.getMessage());

	}
}
