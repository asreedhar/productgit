
package biz.firstlook.service.appraisal.highergear;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

import biz.firstlook.service.appraisal.CreateAppraisalResponse;
import biz.firstlook.service.appraisal.support.CreateHighGearCRMRequestFixture;

public class HigherGearWebServiceCrmClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public HigherGearWebServiceCrmClient() {
        create0();
        Endpoint AppraisalWebServiceCrmLocalEndpointEP = service0 .addEndpoint(new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmLocalEndpoint"), new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmLocalBinding"), "xfire.local://AppraisalWebServiceCrm");
        endpoints.put(new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmLocalEndpoint"), AppraisalWebServiceCrmLocalEndpointEP);
        Endpoint AppraisalWebServiceCrmSOAPEP = service0 .addEndpoint(new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmSOAP"), new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmSOAP"), "http://beta.firstlook.biz/firstlook-external-ws/services/HigherGearWebServiceCrm");
        endpoints.put(new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmSOAP"), AppraisalWebServiceCrmSOAPEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String, Object> props = new HashMap<String, Object>();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((HigherGearWebServiceCrm.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmLocalBinding"), "urn:xfire:transport:local");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmSOAP"), "http://schemas.xmlsoap.org/soap/http");
        }
    }

    public HigherGearWebServiceCrm getAppraisalWebServiceCrmLocalEndpoint() {
        return ((HigherGearWebServiceCrm)(this).getEndpoint(new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmLocalEndpoint")));
    }

    public HigherGearWebServiceCrm getAppraisalWebServiceCrmLocalEndpoint(String url) {
        HigherGearWebServiceCrm var = getAppraisalWebServiceCrmLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public HigherGearWebServiceCrm getAppraisalWebServiceCrmSOAP() {
        return ((HigherGearWebServiceCrm)(this).getEndpoint(new QName("http://beta.firstlook.biz/service/appraisal/highergear", "AppraisalWebServiceCrmSOAP")));
    }

    public HigherGearWebServiceCrm getAppraisalWebServiceCrmSOAP(String url) {
        HigherGearWebServiceCrm var = getAppraisalWebServiceCrmSOAP();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

	public static void main(String[] args) throws DatatypeConfigurationException {
	    	
	    //	Protocol protocol = new Protocol("http", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 8443);
	    //	Protocol.registerProtocol("https", protocol);
	    	
	    	HigherGearWebServiceCrmClient client = new HigherGearWebServiceCrmClient();
	    	HigherGearWebServiceCrm service = client.getAppraisalWebServiceCrmSOAP();
	    	AddAppraisal request = CreateHighGearCRMRequestFixture.getCreateFullAppraisalRequest();
	    	
	    	CreateAppraisalResponse response;
			try {
				response = service.createAppraisal(request);
				System.out.println("Exit code is : " + response.getExitCode().value());
				System.out.println("Response Message is : " + response.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
   }       
    
    
}
