package biz.firstlook.service.appraisal.support;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import biz.firstlook.service.appraisal.BookValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

public class TestStagingBookOutUtility extends TestCase
{

private static final Map< ThirdPartyDataProvider, Set< BookValueType >> thirdPartyDataProvidersCategories;
private static final Map< BookValueType, ThirdPartyCategory> bvtTotpcMapping;

static
{
	thirdPartyDataProvidersCategories = new HashMap< ThirdPartyDataProvider, Set< BookValueType > >();
	Set< BookValueType > blackBookCategories = new HashSet< BookValueType >();
	blackBookCategories.add( BookValueType.BLACKBOOK_AVERAGE );
	blackBookCategories.add( BookValueType.BLACKBOOK_CLEAN );
	blackBookCategories.add( BookValueType.BLACKBOOK_EXTRA_CLEAN );
	blackBookCategories.add( BookValueType.BLACKBOOK_ROUGH );
	thirdPartyDataProvidersCategories.put( ThirdPartyDataProvider.BLACKBOOK, blackBookCategories );

	Set< BookValueType > galvesCategories = new HashSet< BookValueType >();
	galvesCategories.add( BookValueType.GALVES_TRADE_IN);
	galvesCategories.add( BookValueType.GALVES_MARKET_READY);
	thirdPartyDataProvidersCategories.put( ThirdPartyDataProvider.GALVES, galvesCategories); 

	Set< BookValueType > kbbCategories = new HashSet< BookValueType >();
	kbbCategories.add( BookValueType.KELLEY_RETAIL );
	kbbCategories.add( BookValueType.KELLEY_WHOLESALE );
	thirdPartyDataProvidersCategories.put( ThirdPartyDataProvider.KELLEY, kbbCategories );

	Set< BookValueType > nadaCategories = new HashSet< BookValueType >();
	nadaCategories.add( BookValueType.NADA_LOAN );
	nadaCategories.add( BookValueType.NADA_RETAIL );
	nadaCategories.add( BookValueType.NADA_TRADE_IN );
	thirdPartyDataProvidersCategories.put( ThirdPartyDataProvider.NADA, nadaCategories );
	
	
	bvtTotpcMapping = new HashMap< BookValueType, ThirdPartyCategory>();
	bvtTotpcMapping.put( BookValueType.BLACKBOOK_AVERAGE, ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE ));
	bvtTotpcMapping.put( BookValueType.BLACKBOOK_CLEAN, ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE ));
	bvtTotpcMapping.put( BookValueType.BLACKBOOK_EXTRA_CLEAN, ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE));
	bvtTotpcMapping.put( BookValueType.BLACKBOOK_ROUGH, ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE ));
	bvtTotpcMapping.put( BookValueType.KELLEY_RETAIL, ThirdPartyCategory.fromId( ThirdPartyCategory.KELLEY_RETAIL_TYPE ));
	bvtTotpcMapping.put( BookValueType.KELLEY_WHOLESALE, ThirdPartyCategory.fromId( ThirdPartyCategory.KELLEY_WHOLESALE_TYPE ));
	bvtTotpcMapping.put( BookValueType.NADA_LOAN, ThirdPartyCategory.fromId( ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE ));
	bvtTotpcMapping.put( BookValueType.NADA_RETAIL, ThirdPartyCategory.fromId( ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE ));
	bvtTotpcMapping.put( BookValueType.NADA_TRADE_IN, ThirdPartyCategory.fromId( ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE ));
	bvtTotpcMapping.put( BookValueType.GALVES_TRADE_IN, ThirdPartyCategory.fromId( ThirdPartyCategory.GALVES_TRADEIN_TYPE ));
	bvtTotpcMapping.put( BookValueType.GALVES_MARKET_READY, ThirdPartyCategory.fromId( ThirdPartyCategory.GALVES_MARKETREADY_TYPE));
}

/**
 * Iterates over the books and bookValueTypes. The static hashmap contains the set of legal bookValueTypes for a book, so if the hashmap doesn't
 * contain the object during our loop, we expect a failure.
 * 
 */
public void testTranslateThirdPartyCategory()
{
	int failures = 0, success = 0;
	for ( ThirdPartyDataProvider tpdp : thirdPartyDataProvidersCategories.keySet() )
	{
		for ( BookValueType bvt : BookValueType.values() )
		{
			Set< BookValueType > legalBookValuesForbook = thirdPartyDataProvidersCategories.get( tpdp );
			if ( legalBookValuesForbook != null && legalBookValuesForbook.contains( bvt ) )
			{
				//BookValueType found in hashmap for the book(ThirdPartyDataProvider), success
				try
				{
					ThirdPartyCategory tpc = StagingBookOutUtility.translateThirdPartyCategory( tpdp, bvt );
					
					//now check the correct result
					ThirdPartyCategory result = bvtTotpcMapping.get( bvt );
					if( result != null && result.getThirdPartyCategoryId().intValue() == tpc.getThirdPartyCategoryId().intValue() )
					{
						success++;
					}
					else
					{
						failures++;
					}
				}
				catch ( WebServiceException e )
				{
					failures++;
					//fail( "Expected to find a ThirdPartyCategory for " + tpdp.getDescription() + " " + bvt.toString() );
					//System.out.println( "Expected to find a ThirdPartyCategory for " + tpdp.getName() + " " + bvt.toString() );
				}
			}
			else
			{
				// failure; not a legal category for book
				//fail( "ThirdPartyCategory " + bvt.toString() + " is not legal for " + tpdp.getDescription() );
				//System.out.println( "ThirdPartyCategory " + bvt.toString() + " is not legal for " + tpdp.getName() );
				failures++;
			}
		}
	}
	
	assertEquals( 11, success );
	assertEquals( 30, failures );
}

}
