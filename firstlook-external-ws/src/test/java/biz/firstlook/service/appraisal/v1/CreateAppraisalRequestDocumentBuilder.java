package biz.firstlook.service.appraisal.v1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;

import biz.firstlook.service.appraisal.CreateAppraisalRequest;
import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;

class CreateAppraisalRequestDocumentBuilder
{

public static void main(String[] args) throws JAXBException, DatatypeConfigurationException, FileNotFoundException
{
	JAXBContext jaxbContext= JAXBContext.newInstance("biz.firstlook.service.appraisal");
	
	CreateAppraisalRequest appraisalSmallRequestAllElements = CreateAppraisalRequestFixture.getCreateAppraisalRequest();
	CreateFullAppraisalRequest appraisalFullRequest = CreateAppraisalRequestFixture.getCreateFullAppraisalRequest();

	Marshaller marshaller = jaxbContext.createMarshaller();
	marshaller.setProperty( "jaxb.formatted.output", Boolean.TRUE );
	marshaller.marshal( appraisalFullRequest, new FileOutputStream( "src/main/wsdl/appraisal-full-sample.xml" ) );
	marshaller.marshal( appraisalSmallRequestAllElements, new FileOutputStream( "src/main/wsdl/appraisal-small-sample.xml" ) );
	
	System.out.println( "Done." );
	System.exit(0);
}


}
