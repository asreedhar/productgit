
package biz.firstlook.service.appraisal.v1;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class AppraisalWebServiceCrmClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public AppraisalWebServiceCrmClient() {
        create0();
        Endpoint AppraisalWebServiceCrmLocalEndpointEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmLocalEndpoint"), new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmLocalBinding"), "xfire.local://AppraisalWebServiceCrm");
        endpoints.put(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmLocalEndpoint"), AppraisalWebServiceCrmLocalEndpointEP);
        Endpoint AppraisalWebServiceCrmSOAPEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmSOAP"), new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmSOAP"), "https://www.firstlook.biz/firstlook-external-ws/services/AppraisalWebServiceCrm");
        endpoints.put(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmSOAP"), AppraisalWebServiceCrmSOAPEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String, Object> props = new HashMap<String, Object>();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.service.appraisal.v1.AppraisalWebServiceCrm.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmLocalBinding"), "urn:xfire:transport:local");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmSOAP"), "http://schemas.xmlsoap.org/soap/http");
        }
    }

    public AppraisalWebServiceCrm getAppraisalWebServiceCrmLocalEndpoint() {
        return ((AppraisalWebServiceCrm)(this).getEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmLocalEndpoint")));
    }

    public AppraisalWebServiceCrm getAppraisalWebServiceCrmLocalEndpoint(String url) {
        AppraisalWebServiceCrm var = getAppraisalWebServiceCrmLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public AppraisalWebServiceCrm getAppraisalWebServiceCrmSOAP() {
        return ((AppraisalWebServiceCrm)(this).getEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceCrmSOAP")));
    }

    public AppraisalWebServiceCrm getAppraisalWebServiceCrmSOAP(String url) {
        AppraisalWebServiceCrm var = getAppraisalWebServiceCrmSOAP();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }
}
