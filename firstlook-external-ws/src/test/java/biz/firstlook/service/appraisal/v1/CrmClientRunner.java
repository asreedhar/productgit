package biz.firstlook.service.appraisal.v1;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;

import biz.firstlook.service.appraisal.CreateAppraisalRequest;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;
import biz.firstlook.service.appraisal.support.CreateCRMAppraisalRequestFixture;

public class CrmClientRunner {
	public static void main(String[] args) throws DatatypeConfigurationException {
    	
    	Protocol protocol = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 8443);
    	Protocol.registerProtocol("https", protocol);
    	
    	AppraisalWebServiceCrmClient client = new AppraisalWebServiceCrmClient();
    	AppraisalWebServiceCrm service = client.getAppraisalWebServiceCrmSOAP();
    	CreateAppraisalRequest request = CreateCRMAppraisalRequestFixture.getCreateAppraisalRequest();
    	
    	CreateAppraisalResponse response;
		try {
			response = service.createAppraisal(request);
			System.out.println("Exit code is : " + response.getExitCode().value());
			System.out.println("Response Message is : " + response.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
}       
}
