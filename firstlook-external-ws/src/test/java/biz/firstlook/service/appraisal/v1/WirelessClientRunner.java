package biz.firstlook.service.appraisal.v1;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;

import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;
import biz.firstlook.service.appraisal.CreateFullAppraisalResponse;

class WirelessClientRunner {
	   public static void main(String[] args) throws DatatypeConfigurationException {
	    	
	    	Protocol protocol = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
	    	Protocol.registerProtocol("https", protocol);
	    	
	    	AppraisalWebServiceWirelessClient client = new AppraisalWebServiceWirelessClient();
//	    	AppraisalWebServiceCrm service = client.getAppraisalWebServiceWirelessSOAP();
	    	AppraisalWebServiceWireless service = client.getAppraisalWebServiceWirelessSOAP();
//	    	CreateAppraisalRequest request = CreateAppraisalRequestFixture.getCreateAppraisalRequest();
	    	CreateFullAppraisalRequest request = CreateAppraisalRequestFixture.getCreateFullAppraisalRequest();
	    	
	    	CreateFullAppraisalResponse response;
			try {
				response = service.createFullAppraisal(request);
				System.out.println("Exit code is : " + response.getExitCode().value());
				System.out.println("Response Message is : " + response.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	    }
}
