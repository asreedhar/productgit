package biz.firstlook.service.appraisal.v1;

import java.math.BigInteger;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import biz.firstlook.service.appraisal.Appraisal;
import biz.firstlook.service.appraisal.Book;
import biz.firstlook.service.appraisal.BookOption;
import biz.firstlook.service.appraisal.BookValue;
import biz.firstlook.service.appraisal.BookValueType;
import biz.firstlook.service.appraisal.Bookout;
import biz.firstlook.service.appraisal.CreateAppraisalRequest;
import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;
import biz.firstlook.service.appraisal.Credentials;
import biz.firstlook.service.appraisal.Customer;
import biz.firstlook.service.appraisal.DealType;
import biz.firstlook.service.appraisal.Gender;
import biz.firstlook.service.appraisal.ObjectFactory;
import biz.firstlook.service.appraisal.OptionType;
import biz.firstlook.service.appraisal.Person;
import biz.firstlook.service.appraisal.PotentialDeal;
import biz.firstlook.service.appraisal.Report;
import biz.firstlook.service.appraisal.ReportType;
import biz.firstlook.service.appraisal.Vehicle;

class CreateAppraisalRequestFixture {
	private final static ObjectFactory factory = new ObjectFactory();

	
	public static CreateFullAppraisalRequest getCreateFullAppraisalRequest() throws DatatypeConfigurationException {
		
		Credentials creds = factory.createCredentials();
		creds.setDealerId( "EASTERNC01" );
		creds.setPartnerId( "5" );
		creds.setPasscode( "ba@$o243" );
		
		Vehicle v = factory.createVehicle();
		v.setVin( "SCFAC23332B500403" );
		v.setMileage( new BigInteger( "4300" ) );
		v.setColor( "Midnight Blue" );
		v.setNotes( "Traded-in by James Bond.  A few scratches." );
		
		PotentialDeal pd = factory.createPotentialDeal();
		pd.setNewOrUsedDeal( DealType.NEW );
		pd.setStockNumber( "LTK007" );
		Person salesPerson = createPerson( "Mike", "Seller" );
		pd.setPerson( salesPerson );
		
		Customer customer = factory.createCustomer();
		customer.setPerson( createPerson( "Joe", "Buyer" ) );
		customer.setGender( Gender.M ); //need to specifiy this as well.
		customer.setPhoneNumber( "5559874" );
		customer.setEmail( "JoeBuyer@blahblahblah.com" );
		
		Appraisal appraisal = factory.createAppraisal();
		appraisal.setEstimatedRecon( new BigInteger("1000") );
		appraisal.setAppraisalAmount( new BigInteger( "20000" ) );
		appraisal.setConditionDescription( "scratches, bullet holes, needs patching" );
		appraisal.setPerson( createPerson( "Billy", "Appraiser" ) );
		
		Report carfaxReport = factory.createReport();
		carfaxReport.setType( ReportType.CAR_FAX );
		carfaxReport.setUserId( "CC4353" );
		
		Report autocheckReport = factory.createReport();
		autocheckReport.setType( ReportType.AUTO_CHECK );
		autocheckReport.setUserId( "239834" );
		
		Bookout kbb = factory.createBookout();
		kbb.setBookId( Book.KELLEY );
		kbb.setStyleKey( "V1" );
		
		BookOption kbbOption1r = factory.createBookOption();
		kbbOption1r.setDescription( "Heated Seats" );
		kbbOption1r.setKey( "HS" );
		kbbOption1r.setOptionType( OptionType.EQUIPMENT );
		kbbOption1r.setSelected( true );
		kbbOption1r.setValue( new BigInteger( "630" ) );
		kbbOption1r.setValueType( BookValueType.KELLEY_RETAIL );
		
		BookOption kbbOption1w = factory.createBookOption();
		kbbOption1w.setDescription( "Heated Seats" );
		kbbOption1w.setKey( "HS" );
		kbbOption1w.setOptionType( OptionType.EQUIPMENT );
		kbbOption1w.setSelected( true );
		kbbOption1w.setValue( new BigInteger( "590" ) );
		kbbOption1w.setValueType( BookValueType.KELLEY_WHOLESALE );
		
		BookValue kbbValueR = factory.createBookValue();
		kbbValueR.setBaseValue( new BigInteger( "19000" ) );
		kbbValueR.setMileageAdjustment( new BigInteger( "-100" ) );
		kbbValueR.setValueType( BookValueType.KELLEY_RETAIL );
		kbbValueR.setFinalValue( new BigInteger( "19530" ) );
		
		BookValue kbbValueW = factory.createBookValue();
		kbbValueW.setBaseValue( new BigInteger( "18500" ) );
		kbbValueW.setMileageAdjustment( new BigInteger( "-100" ) );
		kbbValueW.setValueType( BookValueType.KELLEY_WHOLESALE );
		kbbValueW.setFinalValue( new BigInteger( "19030" ) );
		
		kbb.getBookOption().add( kbbOption1r );
		kbb.getBookOption().add( kbbOption1w );
		kbb.getBookValue().add( kbbValueR );
		kbb.getBookValue().add( kbbValueW );
		
		Bookout bb = factory.createBookout();
		bb.setBookId( Book.BLACKBOOK );
		bb.setStyleKey( "Vanquish" );
		
		BookOption bbOption1 = factory.createBookOption();
		bbOption1.setDescription( "Htd Seats" );
		bbOption1.setKey( "Htd Seats" );
		bbOption1.setSelected( true );
		bbOption1.setValue( new BigInteger( "520" ) );
		
		BookOption bbOption2 = factory.createBookOption();
		bbOption2.setDescription( "Moon Roof" );
		bbOption2.setKey( "Moon Roof" );
		bbOption2.setSelected( true );
		bbOption2.setValue( new BigInteger( "300" ) );
		
		BookValue bbValueC = factory.createBookValue();
		bbValueC.setBaseValue( new BigInteger( "18500" ) );
		bbValueC.setMileageAdjustment( new BigInteger( "-100" ) );
		bbValueC.setValueType( BookValueType.BLACKBOOK_CLEAN );
		bbValueC.setFinalValue( new BigInteger( "19220" ) );
		
		BookValue bbValueE = factory.createBookValue();
		bbValueE.setBaseValue( new BigInteger( "19000" ) );
		bbValueE.setMileageAdjustment( new BigInteger( "-100" ) );
		bbValueE.setValueType( BookValueType.BLACKBOOK_EXTRA_CLEAN );
		bbValueE.setFinalValue( new BigInteger( "19720" ) );
		
		BookValue bbValueR = factory.createBookValue();
		bbValueR.setBaseValue( new BigInteger( "18000" ) );
		bbValueR.setMileageAdjustment( new BigInteger( "-100" ) );
		bbValueR.setValueType( BookValueType.BLACKBOOK_ROUGH );
		bbValueR.setFinalValue( new BigInteger( "18720" ) );
		
		bb.getBookOption().add( bbOption1 );
		bb.getBookOption().add( bbOption2 );
		bb.getBookValue().add( bbValueC );
		bb.getBookValue().add( bbValueE );
		bb.getBookValue().add( bbValueR );
		
		CreateFullAppraisalRequest appraisalFullRequest = factory.createCreateFullAppraisalRequest();
		appraisalFullRequest.setAppraisalCreateTime( DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()) );
		appraisalFullRequest.setAlertUCM( Boolean.TRUE );
		appraisalFullRequest.setCredentials( creds );
		appraisalFullRequest.setVehicle( v );
		appraisalFullRequest.setPotentialDeal( pd );
		appraisalFullRequest.setCustomer( customer );
		appraisalFullRequest.setAppraisal( appraisal );
		appraisalFullRequest.getReport().add( carfaxReport );
		appraisalFullRequest.getReport().add( autocheckReport );
		appraisalFullRequest.getBookout().add( kbb );
		appraisalFullRequest.getBookout().add( bb );
		
		return appraisalFullRequest;
	}
	
	public static CreateFullAppraisalRequest getCreateFullAppraisalRequest2() throws DatatypeConfigurationException {
		
		Credentials creds = factory.createCredentials();
		creds.setDealerId( "HENDRICK01" );
		creds.setPartnerId( "5" );
		creds.setPasscode( "ba@$o243" );
		
		Vehicle v = factory.createVehicle();
		v.setVin( "2A4GM68426R744933" );
		v.setMileage( new BigInteger( "4300" ) );
		v.setColor( "Midnight Blue" );
		v.setNotes( "Traded-in by James Bond.  A few scratches." );
		
		PotentialDeal pd = factory.createPotentialDeal();
		pd.setNewOrUsedDeal( DealType.NEW );
		pd.setStockNumber( "LTK004" );
		Person salesPerson = createPerson( "Mike", "Seller" );
		pd.setPerson( salesPerson );
		
		Customer customer = factory.createCustomer();
		customer.setPerson( createPerson( "Joe", "Buyer" ) );
		customer.setGender( Gender.M ); //need to specifiy this as well.
		customer.setPhoneNumber( "5559874" );
		customer.setEmail( "JoeBuyer@blahblahblah.com" );
		
		Appraisal appraisal = factory.createAppraisal();
		appraisal.setEstimatedRecon( new BigInteger( "1000" ) );
		appraisal.setAppraisalAmount( new BigInteger( "20000" ) );
		appraisal.setConditionDescription( "scratches, bullet holes, needs patching" );
		appraisal.setPerson( createPerson( "Billy", "Appraiser" ) );
		
		Report carfaxReport = factory.createReport();
		carfaxReport.setType( ReportType.CAR_FAX );
		carfaxReport.setUserId( "CC4353" );
		
		Report autocheckReport = factory.createReport();
		autocheckReport.setType( ReportType.AUTO_CHECK );
		autocheckReport.setUserId( "239834" );
		
		Bookout kbb = factory.createBookout();
		kbb.setBookId( Book.KELLEY );
		kbb.setStyleKey( "LX" );
		
		BookOption kbbOption1r = factory.createBookOption();
		kbbOption1r.setDescription( "Heated Seats" );
		kbbOption1r.setKey( "HS" );
		kbbOption1r.setOptionType( OptionType.EQUIPMENT );
		kbbOption1r.setSelected( true );
		kbbOption1r.setValue( new BigInteger( "630" ) );
		kbbOption1r.setValueType( BookValueType.KELLEY_RETAIL );
		
		BookOption kbbOption1w = factory.createBookOption();
		kbbOption1w.setDescription( "Heated Seats" );
		kbbOption1w.setKey( "HS" );
		kbbOption1w.setOptionType( OptionType.EQUIPMENT );
		kbbOption1w.setSelected( true );
		kbbOption1w.setValue( new BigInteger( "590" ) );
		kbbOption1w.setValueType( BookValueType.KELLEY_WHOLESALE );
		
		BookValue kbbValueR = factory.createBookValue();
		kbbValueR.setBaseValue( new BigInteger( "19000" ) );
		kbbValueR.setMileageAdjustment( new BigInteger( "-100" ) );
		kbbValueR.setValueType( BookValueType.KELLEY_RETAIL );
		kbbValueR.setFinalValue( new BigInteger( "19530" ) );
		
		BookValue kbbValueW = factory.createBookValue();
		kbbValueW.setBaseValue( new BigInteger( "18500" ) );
		kbbValueW.setMileageAdjustment( new BigInteger( "-100" ) );
		kbbValueW.setValueType( BookValueType.KELLEY_WHOLESALE );
		kbbValueW.setFinalValue( new BigInteger( "19030" ) );
		
		kbb.getBookOption().add( kbbOption1r );
		kbb.getBookOption().add( kbbOption1w );
		kbb.getBookValue().add( kbbValueR );
		kbb.getBookValue().add( kbbValueW );
		
		Bookout bb = factory.createBookout();
		bb.setBookId( Book.BLACKBOOK );
		bb.setStyleKey( "Pacifica" );
		
		BookOption bbOption1 = factory.createBookOption();
		bbOption1.setDescription( "Htd Seats" );
		bbOption1.setKey( "Htd Seats" );
		bbOption1.setSelected( true );
		bbOption1.setValue( new BigInteger( "520" ) );
		
		BookOption bbOption2 = factory.createBookOption();
		bbOption2.setDescription( "Moon Roof" );
		bbOption2.setKey( "Moon Roof" );
		bbOption2.setSelected( true );
		bbOption2.setValue( new BigInteger( "300" ) );
		
		BookValue bbValueC = factory.createBookValue();
		bbValueC.setBaseValue( new BigInteger( "18500" ) );
		bbValueC.setMileageAdjustment( new BigInteger( "-100" ) );
		bbValueC.setValueType( BookValueType.BLACKBOOK_CLEAN );
		bbValueC.setFinalValue( new BigInteger( "19220" ) );
		
		BookValue bbValueE = factory.createBookValue();
		bbValueE.setBaseValue( new BigInteger( "19000" ) );
		bbValueE.setMileageAdjustment( new BigInteger( "-100" ) );
		bbValueE.setValueType( BookValueType.BLACKBOOK_EXTRA_CLEAN );
		bbValueE.setFinalValue( new BigInteger( "19720" ) );
		
		BookValue bbValueR = factory.createBookValue();
		bbValueR.setBaseValue( new BigInteger( "18000" ) );
		bbValueR.setMileageAdjustment( new BigInteger( "-100" ) );
		bbValueR.setValueType( BookValueType.BLACKBOOK_ROUGH );
		bbValueR.setFinalValue( new BigInteger( "18720" ) );
		
		bb.getBookOption().add( bbOption1 );
		bb.getBookOption().add( bbOption2 );
		bb.getBookValue().add( bbValueC );
		bb.getBookValue().add( bbValueE );
		bb.getBookValue().add( bbValueR );
		
		CreateFullAppraisalRequest appraisalFullRequest = factory.createCreateFullAppraisalRequest();
		appraisalFullRequest.setAppraisalCreateTime( DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()) );
		appraisalFullRequest.setAlertUCM( Boolean.TRUE );
		appraisalFullRequest.setCredentials( creds );
		appraisalFullRequest.setVehicle( v );
		appraisalFullRequest.setPotentialDeal( pd );
		appraisalFullRequest.setCustomer( customer );
		appraisalFullRequest.setAppraisal( appraisal );
		appraisalFullRequest.getReport().add( carfaxReport );
		appraisalFullRequest.getReport().add( autocheckReport );
		appraisalFullRequest.getBookout().add( kbb );
		appraisalFullRequest.getBookout().add( bb );
		
		return appraisalFullRequest;
	}	
public static CreateAppraisalRequest getCreateAppraisalRequest() throws DatatypeConfigurationException {
		
		Credentials creds = factory.createCredentials();
		creds.setDealerId( "EASTERNC01" );
		creds.setPartnerId( "5" );
		creds.setPasscode( "ba@$o243" );
		
		Vehicle v = factory.createVehicle();
		v.setVin( "SCFAC23332B500403" );
		v.setMileage( new BigInteger( "4300" ) );
		v.setColor( "Midnight Blue" );
		v.setNotes( "Traded-in by James Bond.  A few scratches." );
		
		PotentialDeal pd = factory.createPotentialDeal();
		pd.setNewOrUsedDeal( DealType.NEW );
		pd.setStockNumber( "LTK007" );
		Person salesPerson = createPerson( "Mike", "Seller" );
		pd.setPerson( salesPerson );
		
		Customer customer = factory.createCustomer();
		customer.setPerson( createPerson( "Joe", "Buyer" ) );
		customer.setGender( Gender.M ); //need to specifiy this as well.
		customer.setPhoneNumber( "5559874" );
		customer.setEmail( "JoeBuyer@blahblahblah.com" );
		
		CreateAppraisalRequest appraisalSmallRequestAllElements = factory.createCreateAppraisalRequest();
		appraisalSmallRequestAllElements.setAppraisalCreateTime( DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()) );
		appraisalSmallRequestAllElements.setAlertUCM( Boolean.TRUE );
		appraisalSmallRequestAllElements.setCredentials( creds );
		appraisalSmallRequestAllElements.setVehicle( v );
		appraisalSmallRequestAllElements.setPotentialDeal( pd );
		appraisalSmallRequestAllElements.setCustomer( customer );
		
		return appraisalSmallRequestAllElements;
	}
	
	private static Person createPerson( String firstname, String lastname )
	{
		Person person = factory.createPerson();
		person.setFirstName( firstname );
		person.setLastName( lastname );
		
		return person;
	}
	
}
