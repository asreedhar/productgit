
package biz.firstlook.service.appraisal.v1;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class CustomAppraisalWebServiceWirelessClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public CustomAppraisalWebServiceWirelessClient() {
        create0();
        Endpoint AppraisalWebServiceWirelessLocalEndpointEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessLocalEndpoint"), new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessLocalBinding"), "xfire.local://AppraisalWebServiceWireless");
        endpoints.put(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessLocalEndpoint"), AppraisalWebServiceWirelessLocalEndpointEP);
        Endpoint AppraisalWebServiceWirelessSOAPEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessSOAP"), new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessSOAP"), "https://preprod.firstlook.biz/firstlook-external-ws/services/AppraisalWebServiceWireless");
        endpoints.put(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessSOAP"), AppraisalWebServiceWirelessSOAPEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String, Object> props = new HashMap<String, Object>();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.service.appraisal.v1.AppraisalWebServiceWireless.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessSOAP"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public AppraisalWebServiceWireless getAppraisalWebServiceWirelessLocalEndpoint() {
        return ((AppraisalWebServiceWireless)(this).getEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessLocalEndpoint")));
    }

    public AppraisalWebServiceWireless getAppraisalWebServiceWirelessLocalEndpoint(String url) {
        AppraisalWebServiceWireless var = getAppraisalWebServiceWirelessLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public AppraisalWebServiceWireless getAppraisalWebServiceWirelessSOAP() {
        return ((AppraisalWebServiceWireless)(this).getEndpoint(new QName("http://www.firstlook.biz/service/appraisal/v1", "AppraisalWebServiceWirelessSOAP")));
    }

    public AppraisalWebServiceWireless getAppraisalWebServiceWirelessSOAP(String url) {
        AppraisalWebServiceWireless var = getAppraisalWebServiceWirelessSOAP();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }    
}
