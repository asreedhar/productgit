
package biz.firstlook.client.dms.rd;

import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.security.wss4j.WSS4JOutHandler;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;

import biz.firstlook.client.dms.rd.util.RequestLoggingHandler;
import biz.firstlook.client.dms.rd.util.ReynoldsDirectResponseHandler;
import biz.firstlook.service.appraisal.reynolds.crm.StarTransportPortTypes;
import biz.firstlook.service.appraisal.reynolds.transport.Content;
import biz.firstlook.service.appraisal.reynolds.transport.Manifest;
import biz.firstlook.service.appraisal.reynolds.transport.ObjectFactory;
import biz.firstlook.service.appraisal.reynolds.transport.Payload;
import biz.firstlook.service.appraisal.reynolds.transport.PayloadManifest;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.ApplicationAreaType;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.Customer;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.DestinationType;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.ReyVehicleTradeInAppraisal;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.SenderType;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.Vehicle;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.VehicleTradeInType;

public class TestReynoldsWebServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public TestReynoldsWebServiceClient() {
        create0();
        Endpoint STAREndpointEP = service0 .addEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "STAREndpoint"), new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransport"), "https://beta.firstlook.biz/firstlook-external-ws/services/ReynoldsWebServiceCrm");
        endpoints.put(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "STAREndpoint"), STAREndpointEP);
        Endpoint StarTransportPortTypesLocalEndpointEP = service0 .addEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalEndpoint"), new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalBinding"), "xfire.local://STARWebService");
        endpoints.put(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalEndpoint"), StarTransportPortTypesLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String, Object> props = new HashMap<String, Object>();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.service.appraisal.reynolds.crm.StarTransportPortTypes.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransport"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public StarTransportPortTypes getSTAREndpoint() {
        return ((StarTransportPortTypes)(this).getEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "STAREndpoint")));
    }

    public StarTransportPortTypes getSTAREndpoint(String url) {
        StarTransportPortTypes var = getSTAREndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public StarTransportPortTypes getStarTransportPortTypesLocalEndpoint() {
        return ((StarTransportPortTypes)(this).getEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalEndpoint")));
    }

    public StarTransportPortTypes getStarTransportPortTypesLocalEndpoint(String url) {
        StarTransportPortTypes var = getStarTransportPortTypesLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }
    
	public static void main(String[] args) throws DatatypeConfigurationException {
    	
	    Protocol protocol = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
	    Protocol.registerProtocol("https", protocol);
	    	
		TestReynoldsWebServiceClient reyClient = new TestReynoldsWebServiceClient();
		StarTransportPortTypes service = reyClient.getSTAREndpoint("https://bfung02.firstlook.biz/firstlook-external-ws/services/ReynoldsWebServiceCrm");
		
		
		Client client =  ((XFireProxy)
				Proxy.getInvocationHandler(service)).getClient();
		client.addInHandler(new DOMInHandler());
		client.addInHandler(new ReynoldsDirectResponseHandler());
		
		client.addOutHandler(new DOMOutHandler());
		client.addOutHandler(new RequestLoggingHandler());
		Properties p = new Properties();
		
		//Action to perform : user token
		p.setProperty(WSHandlerConstants.ACTION,
				WSHandlerConstants.USERNAME_TOKEN);
		
		//Set password type to text
		p.setProperty(WSHandlerConstants.PASSWORD_TYPE,
		WSConstants.PW_TEXT);
		
		p.setProperty(WSHandlerConstants.USER, "ReynoldsCRM");
		
		//Used do retrive password for given user name
		String passwordCallbackHandlerName;
		
		passwordCallbackHandlerName = TestPasswordCallbackHandlerService.class.getName();
		
		p.setProperty(WSHandlerConstants.PW_CALLBACK_CLASS,
				passwordCallbackHandlerName);
		
		client.addOutHandler(new WSS4JOutHandler(p));
		
		PayloadManifest payloadManifest1 = new PayloadManifest();
		
		//Create Manifest
		Manifest manifest = new Manifest(); 
		manifest.setVersion("2.0");
        manifest.setElement("rey_VehicleTradeInAppraisal"); 
        manifest.setNamespaceURI("http://www.starstandards.org/webservices/2005/10/transport"); 
        manifest.setContentID("Content0");
      
        payloadManifest1.getManifest().add(manifest);      
        
		ObjectFactory factory = new ObjectFactory();
			
		ReyVehicleTradeInAppraisal reyVehicleTradeInAppraisal = new ReyVehicleTradeInAppraisal(); 
		
		ApplicationAreaType applicationAreaType = new ApplicationAreaType();
		SenderType sender = new SenderType();
		sender.setComponent("CM");
		sender.setTask("VTA");
		sender.setCreatorNameCode("RR");
		sender.setSenderNameCode("RR");		
		sender.setAreaNumber("01");
		sender.setDealerNumber("pperasv02000000");
		sender.setStoreNumber("01");
		
		applicationAreaType.setBODId("19087d3a-01b2-1eef-9cfb-a827f0eed15f");
		
		DestinationType destinationType = new DestinationType();
		destinationType.setDestinationNameCode("RCI");
		applicationAreaType.setDestination(destinationType);
		
		applicationAreaType.setSender(sender);
		
		VehicleTradeInType vehicleTradeInType = new VehicleTradeInType();
		
		Vehicle vehicle = new Vehicle();
		vehicle.setVIN("2FMDA5143WBB07382");
		//vehicle.setVIN("1G6ET1299TU605469");
		//vehicle.setTradeExteriorColor("");
		vehicle.setMileage("123");
		
//		vehicle.setTradeInteriorColor("");
//		vehicle.setCompanyCode("");
//		vehicle.setLenderAccount("");
//		vehicle.setLenderName("");
//		vehicle.setLenderPhone("");
//		vehicle.setLicensePlate("");
//		vehicle.setPayoffAmount("");
//		vehicle.setPlateRegistrationState("");
//		vehicle.setRegistrationNumber("");
		
		
		vehicleTradeInType.setVehicle(vehicle);
		vehicleTradeInType.setCreatedByFName("");
		vehicleTradeInType.setCreatedByLName("");
		vehicleTradeInType.setCreatedByUserName("");
		
		Customer customer = new Customer();
//		customer.setCustFName("RICHARD");
//		customer.setCustLName("CASE");
//		customer.setCustEmail("case@reyrey.com");
//		customer.setCustPhone("444020412");
//		customer.setCustAddress("1356 CHEW STREET");
//		customer.setCustCity("ALLENTOWN");
//		customer.setCustMInitial("");
//		customer.setCustState("PA");
//		customer.setCustZip("18103");
//		customer.setSSN("");
		
		vehicleTradeInType.setCustomer(customer);
		
		vehicleTradeInType.setReynoldsTransactionID("1013081");
		vehicleTradeInType.setReynoldsUserID("911620");
		
		reyVehicleTradeInAppraisal.setApplicationArea(applicationAreaType);
		reyVehicleTradeInAppraisal.setVehicleTradeIn(vehicleTradeInType);
		
		Content payloadContent = factory.createContent();
		payloadContent.setId("Content0");
		payloadContent.setReyVehicleTradeInAppraisal(reyVehicleTradeInAppraisal);
		
		Payload payload = factory.createPayload();
		payload.getContent().add(payloadContent);
  	
		try {
			Payload response = service.putMessage(payload, payloadManifest1);
			if(response != null) {
				response.getContent();
			}
		//	service.hello();
			System.out.println("Exit code is : ");
			System.out.println("Response Message is : ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	    	
   }       
    
    

}
