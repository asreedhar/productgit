package biz.firstlook.client.dms.sis;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

import biz.firstlook.client.dms.sis.util.SISTransactionUtil;
import biz.firstlook.service.dms.sis.client.UserInfoHeader;

public class SISTransmissionClientRunner {

	public static void main(String[] args) {
		Logger log = Logger.getLogger(SISTransmissionClientRunner.class);
		SISTransmissionClient client = new SISTransmissionClient();
		
		String stockNumber = "70648A";
		String vin = "2HGES16505H521460";
		Double listPrice = Double.valueOf(22995d);
		UserInfoHeader infoHeader = new UserInfoHeader();
		infoHeader.setUserid("12604FLO");
		infoHeader.setPassword("P@SSW0RD");
		
		String result = null;
		try {
			// Currently all exception and error handling of SIS transmissions is string based. 
			// We could get into specially typed SIS exception if future reqs require it.
			String xml = SISTransactionUtil.buildRepriceVehicleUpdateString(stockNumber, vin, listPrice, null);
			result = client.transmit(xml, infoHeader);
			if(result != null) {
				if (result.indexOf("ERROR") == -1) {
					if(log.isInfoEnabled()) {
						log.info(MessageFormat.format("Submitted stock: {0} with list price of {1, number, currency} to SIS.", 
								stockNumber, 
								listPrice));
					}
				} else { // we got one of their standard error messages back from SIS
					log.error(MessageFormat.format("Error Submitting stock: {0} with list price of {1, number, currency} to SIS.", 
							stockNumber, 
							listPrice));
				}
				if(log.isInfoEnabled()) {
					log.info(result);
				}
			} else {
				log.error(MessageFormat.format("Null response from SIS when submitting xml {0}", xml));
			}
		} catch (Exception e) {
			if (e.getMessage().indexOf("CompanyNotFoundException") > -1) {
				// Per case: 178 - DM asked that we handle this error differently. Since it identifies the need for specific operational action with SIS.
				result = "ERROR : Company Not Found Exception"; 
			} else {
				result = MessageFormat.format("ERROR Submitting stock: {0} with list price of {1, number, currency} to SIS.", 
						stockNumber, 
						listPrice);				
			}
			log.error(result);
		}
	}
}
