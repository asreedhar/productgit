package biz.firstlook.service.appraisal.dealersocket;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;
import biz.firstlook.service.appraisal.Credentials;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.appraisal.v1.AbstractAppraisalWebService;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

public class DealerSocketCRMService extends AbstractAppraisalWebService{
	
	private static Logger log = Logger.getLogger(DealerSocketCRMService.class);
	
	@SuppressWarnings("unchecked")
	public CreateAppraisalResponse submitCRM(Document submittedDoc) throws WebServiceException {
		
		// assume error
		CreateAppraisalResponse createAppraisalResponse = new CreateAppraisalResponse();
		createAppraisalResponse.setExitCode(ExitCode.MISSING_REQUIRED_ATTRIBUTES);
	
		try {
			Element root = getChildElement(submittedDoc.getContent(), "Appraisal");
			String dealerCode =  getChildElement(root.getContent(), "DealerID").getValue();
			Element vehicleElement =  getChildElement(getChildElement(root.getContent(), "Vehicles").getContent(), "Vehicle");
			String password = getChildElement(vehicleElement.getContent(), "Password").getValue();

			//	Credential Type is not sent by dealer socket. Implicit from servlet request mapping
			Credentials credential = setUpCredentials(dealerCode, password, CredentialType.DEALERSOCKET);
			
			// authenticate and get business unit info
			BusinessUnitCredential businessUnitCredential = authenticate(credential); // throws AUTHENTICATION exception
			Integer businessUnitId = businessUnitCredential.getBusinessUnitId();
			DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(businessUnitId);

			
			// extract content
			String vin = getChildElement(vehicleElement.getContent(), "VIN").getValue();
			String color = getChildElement(vehicleElement.getContent(), "ExteriorColor").getValue();
			Integer mileage = new Integer(getChildElement(vehicleElement.getContent(), "Mileage").getValue());
			DealTrackIDealAdapter dealTrack = new DealTrackIDealAdapter( createPotentialDeal(vehicleElement, "CreatedByFirst", "CreatedByLast") );
			CustomerIPersonAdapter customer = new CustomerIPersonAdapter( createCustomer(vehicleElement, "FirstName",  "LastName", "PrimaryPhone", null) );
			
			// check reject conditions and delete existing if appraisal already exists for this VIN/BUid 
			checkRejectConditions(businessUnitId, vin, AppraisalSource.DEALERSOCKET);

			// Update existing
			List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
			if (appraisals != null && !appraisals.isEmpty()) {
				updateAppraisal(color, dealTrack, customer, appraisals, AppraisalSource.DEALERSOCKET);
			} else {
				// Create New
				Vehicle vehicle;
				log.debug("Looking up vehicle for VIN : " + vin);
				vehicle = findVin(vin);
				vehicle.setBaseColor( color );
				Dealer dealer = getDealerService().findByDealerId( businessUnitId );
				createNewAppraisal(mileage, dealTrack, customer, vehicle, dealer,AppraisalSource.DEALERSOCKET);
			}
			// success
			createAppraisalResponse.setExitCode(ExitCode.SUCCESS);
			createAppraisalResponse.setMessage("Appraisal Successfully created for Dealer Socket");
		} catch ( WebServiceException e )
		{
			log.warn("A web service expection was thrown when attempting to submit a Full Appraisal.");
			log.warn("Exit Code : " + e.getExitCode().value() + " , Msg : " + e.getMessage());

			createAppraisalResponse.setExitCode( e.getExitCode() );
			createAppraisalResponse.setMessage( e.getMessage() );
		}
		catch ( InvalidVinException e )
		{
			log.warn("An unknown VIN was submitted to the Appraisal web service.");
			log.warn("Exit Code : " + ExitCode.INVALID_VIN.value() + " , Msg : " + e.getMessage());

			createAppraisalResponse.setExitCode( ExitCode.INVALID_VIN );
			createAppraisalResponse.setMessage( e.getMessage() );
		}
		
		return createAppraisalResponse;
	}
}
