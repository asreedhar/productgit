
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for optionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="optionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Equipment"/>
 *     &lt;enumeration value="Engine"/>
 *     &lt;enumeration value="Transmission"/>
 *     &lt;enumeration value="DriveTrain"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum OptionType {

    @XmlEnumValue("DriveTrain")
    DRIVE_TRAIN("DriveTrain"),
    @XmlEnumValue("Engine")
    ENGINE("Engine"),
    @XmlEnumValue("Equipment")
    EQUIPMENT("Equipment"),
    @XmlEnumValue("Transmission")
    TRANSMISSION("Transmission");
    private final String value;

    OptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OptionType fromValue(String v) {
        for (OptionType c: OptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
