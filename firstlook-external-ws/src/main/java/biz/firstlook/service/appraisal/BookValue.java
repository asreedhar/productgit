
package biz.firstlook.service.appraisal;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valueType" type="{http://www.firstlook.biz/service/appraisal}bookValueType"/>
 *         &lt;element name="baseValue" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="mileageAdjustment" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="finalValue" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "valueType",
    "baseValue",
    "mileageAdjustment",
    "finalValue"
})
@XmlRootElement(name = "bookValue")
public class BookValue {

    @XmlElement(required = true)
    protected BookValueType valueType;
    @XmlElement(required = true)
    protected BigInteger baseValue;
    protected BigInteger mileageAdjustment;
    protected BigInteger finalValue;

    /**
     * Gets the value of the valueType property.
     * 
     * @return
     *     possible object is
     *     {@link BookValueType }
     *     
     */
    public BookValueType getValueType() {
        return valueType;
    }

    /**
     * Sets the value of the valueType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookValueType }
     *     
     */
    public void setValueType(BookValueType value) {
        this.valueType = value;
    }

    /**
     * Gets the value of the baseValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBaseValue() {
        return baseValue;
    }

    /**
     * Sets the value of the baseValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBaseValue(BigInteger value) {
        this.baseValue = value;
    }

    /**
     * Gets the value of the mileageAdjustment property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMileageAdjustment() {
        return mileageAdjustment;
    }

    /**
     * Sets the value of the mileageAdjustment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMileageAdjustment(BigInteger value) {
        this.mileageAdjustment = value;
    }

    /**
     * Gets the value of the finalValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFinalValue() {
        return finalValue;
    }

    /**
     * Sets the value of the finalValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFinalValue(BigInteger value) {
        this.finalValue = value;
    }

}
