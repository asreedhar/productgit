package biz.firstlook.service.appraisal;

import java.io.StringReader;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;
import org.codehaus.xfire.spring.ServiceBean;
import org.codehaus.xfire.util.jdom.StaxBuilder;
import org.jdom.Document;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.service.appraisal.dealersocket.DealerSocketCRMService;
import biz.firstlook.service.appraisal.highergear.HigherGearCRMService;
import biz.firstlook.service.appraisal.v1.AppraisalWebServiceCrm;
import biz.firstlook.service.appraisal.v1.AppraisalWebServiceWireless;

public class CreateAppraisalServlet implements Controller {

	private static final Logger log = Logger.getLogger(CreateAppraisalServlet.class);
	private static final String NEW_LINE = System.getProperty("line.separator");
	
	private static final StaxBuilder builder = new StaxBuilder();
	private static final long serialVersionUID = 3719830325777159552L;
	private ServiceBean appraisalWebServiceWireless;
	private ServiceBean appraisalWebServiceCrm;
	
	// custom services
	private DealerSocketCRMService dealerSocketCRMService;
	private HigherGearCRMService higherGearCRMService;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String xmlResponse = null;
		String errorMessage = null;
		ExitCode exitCode = ExitCode.SUCCESS;
		
		// get xml off request
		String appraisalXML = request.getParameter("appraisal"); 
		log.info("CRM/Wireless Appraisal: " + appraisalXML);
		
		Object appraisalResponse = null;
		try {
			Document doc = builder.build(new StringReader(appraisalXML));
			
			String requestContext = request.getRequestURI();
			
			if (requestContext.indexOf("dealerSocket") != -1) {
				// a custom appraisal request from dealer socket
				log.info("Processing a custom appraisal from dealerSocket.");
				appraisalResponse = dealerSocketCRMService.submitCRM(doc);
			} 
			else if (requestContext.indexOf("higherGear") != -1){
				// a custom appraisal request from dealer socket
				log.info("Processing a custom appraisal from higherGear.");
				appraisalResponse = higherGearCRMService.submitCRM(doc);
			}
			else {
				// a standard appraisal request in the published FL format
				Object appraisalRequest = parseAppraisalRequest(appraisalXML);
				if (appraisalRequest instanceof CreateFullAppraisalRequest) {
					CreateFullAppraisalRequest createFullAppraisalRequest = (CreateFullAppraisalRequest) appraisalRequest;
					//validate the object not useing Schema.validator... it is too strict
					AppraisalWebServiceWireless wirlessService = (AppraisalWebServiceWireless) appraisalWebServiceWireless.getServiceBean();
					appraisalResponse = wirlessService.createFullAppraisal(createFullAppraisalRequest);
				} else if (appraisalRequest instanceof CreateAppraisalRequest) {
					CreateAppraisalRequest createAppraisalRequest = (CreateAppraisalRequest) appraisalRequest;
					//validate the object not useing Schema.validator... it is too strict
					AppraisalWebServiceCrm crmService = (AppraisalWebServiceCrm)appraisalWebServiceCrm.getServiceBean();
					appraisalResponse = crmService.createAppraisal(createAppraisalRequest);
				}
			}
		} catch (XMLStreamException xmle) {
			exitCode = ExitCode.SCHEMA_INVALID;
			StringBuilder messBuilder = new StringBuilder("Schema of Request not understood by server.");
			messBuilder.append(NEW_LINE).append(xmle.getMessage());
			errorMessage = messBuilder.toString();
		} catch (JAXBException jaxbe) {
			exitCode = ExitCode.SCHEMA_INVALID;
			StringBuilder messBuilder = new StringBuilder("Schema of Request not understood by server.");
			Throwable linkedException = jaxbe.getLinkedException();
			messBuilder.append(NEW_LINE).append(linkedException == null ? jaxbe.getMessage() : linkedException.getMessage());
			errorMessage = messBuilder.toString();
		} catch (Exception e) {
			exitCode = ExitCode.MISSING_REQUIRED_ATTRIBUTES;
			errorMessage = "Unable to post Appraisal";
			StringBuilder errorBuilder = new StringBuilder("An error occured while parsing the incoming appraisal request.");
			errorBuilder.append(NEW_LINE);
			errorBuilder.append("The following XML was invalid:");
			errorBuilder.append(NEW_LINE);
			errorBuilder.append(appraisalXML);
			errorBuilder.append(NEW_LINE);
			log.error(errorBuilder.toString(), e);
		} 

		if (appraisalResponse != null) {
			xmlResponse = parseResponseToXML(appraisalResponse);
		} else {
			// error
			xmlResponse = getErrorXML(exitCode, errorMessage);
		}
		return new ModelAndView("postResponse", "response", xmlResponse);
	}
	
	private String getErrorXML(ExitCode exitCode, String message) {
		StringBuilder errorXml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		errorXml.append("<createFullAppraisalResponse xmlns=\"http://www.firstlook.biz/service/appraisal\">");
		errorXml.append("<exitCode>").append(exitCode.value()).append("</exitCode>");
		errorXml.append("<message>").append(message).append("</message>");
		errorXml.append("</createFullAppraisalResponse>");
		return errorXml.toString();
	}

	private String parseResponseToXML(Object appraisalResponse) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance("biz.firstlook.service.appraisal");
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(appraisalResponse, writer);
		return writer.getBuffer().toString();

	}

	private Object parseAppraisalRequest(String appraisalRequestXML) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance("biz.firstlook.service.appraisal");
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		return unmarshaller.unmarshal(new StringReader(appraisalRequestXML));
	}

	public void setAppraisalWebServiceWireless(
			ServiceBean appraisalWebServiceWireless) {
		this.appraisalWebServiceWireless = appraisalWebServiceWireless;
	}

	public void setAppraisalWebServiceCrm(ServiceBean appraisalWebServiceCrm) {
		this.appraisalWebServiceCrm = appraisalWebServiceCrm;
	}

	public void setDealerSocketCRMService(
			DealerSocketCRMService dealerSocketCRMService) {
		this.dealerSocketCRMService = dealerSocketCRMService;
	}


	public void setHigherGearCRMService(HigherGearCRMService higherGearCRMService) {
		this.higherGearCRMService = higherGearCRMService;
	}


}
