package biz.firstlook.service.appraisal.highergear;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;
import biz.firstlook.service.appraisal.Credentials;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.appraisal.v1.AbstractAppraisalWebService;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

public class HigherGearCRMService extends AbstractAppraisalWebService{
	
	private static Logger log = Logger.getLogger(HigherGearCRMService.class);
	
	@SuppressWarnings("unchecked")
	public CreateAppraisalResponse submitCRM(Document submittedDoc) throws WebServiceException {
		
		// assume error
		CreateAppraisalResponse createAppraisalResponse = new CreateAppraisalResponse();
		createAppraisalResponse.setExitCode(ExitCode.MISSING_REQUIRED_ATTRIBUTES);
	
		try {
			Element root = getChildElement(submittedDoc.getContent(), "AddAppraisal");
			Element data = getChildElement(root.getContent(), "data");
			String dealerCode =  getChildElement(data.getContent(), "CompanyCode").getValue();
			String password = getChildElement(data.getContent(), "LoginName").getValue();

			Credentials credential = setUpCredentials(dealerCode, password, CredentialType.HIGHERGEAR);
			
			// authenticate and get business unit info
			BusinessUnitCredential businessUnitCredential = authenticate(credential); // throws AUTHENTICATION exception
			Integer businessUnitId = businessUnitCredential.getBusinessUnitId();
			DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(businessUnitId);

			// extract content
			Element vinElement = getChildElement(data.getContent(), "Vin");
			if(vinElement == null) {
				throw new WebServiceException(ExitCode.INVALID_VIN, "The Vin element must be specified.");
			}
			
			String vin = vinElement.getValue();
			
			Element colorElement = getChildElement(data.getContent(), "TradeExteriorColor");
			String color = colorElement == null ? null : colorElement.getValue();
			
			Element mileageElement = getChildElement(data.getContent(), "Mileage");
			Double mileage = mileageElement == null ? Double.valueOf(0.0d) : Double.valueOf(mileageElement.getValue());
			
			DealTrackIDealAdapter dealTrack = new DealTrackIDealAdapter( createPotentialDeal(data, "CreatedByFName", "CreatedByLName") );
			CustomerIPersonAdapter customer = new CustomerIPersonAdapter( createCustomer(data, "CustFName", "CustLName", "CustPhone", "CustEmail") );
			
			// check reject conditions and delete existing if appraisal already exists for this VIN/BUid 
			checkRejectConditions(businessUnitId, vin, AppraisalSource.HIGHERGEAR);

			// Update existing
			List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
			if (appraisals != null && !appraisals.isEmpty()) {
				updateAppraisal(color, dealTrack, customer, appraisals, AppraisalSource.HIGHERGEAR);
			} else {
				// Create New
				Vehicle vehicle;
				log.debug("Looking up vehicle for VIN : " + vin);
				vehicle = findVin(vin);
				vehicle.setBaseColor( color );
				Dealer dealer = getDealerService().findByDealerId( businessUnitId );
				getAppraisalService().createAppraisal( new Date(System.currentTimeMillis()), dealer, vehicle,
													mileage.intValue(), dealTrack, customer,
													null, AppraisalSource.HIGHERGEAR, AppraisalTypeEnum.TRADE_IN);
			}
			// success
			createAppraisalResponse.setExitCode(ExitCode.SUCCESS);
			createAppraisalResponse.setMessage("Appraisal Successfully created for Higher Gear");
		} catch ( WebServiceException e )
		{
			log.warn("A web service expection was thrown when attempting to submit a Full Appraisal.");
			log.warn("Exit Code : " + e.getExitCode().value() + " , Msg : " + e.getMessage());

			createAppraisalResponse.setExitCode( e.getExitCode() );
			createAppraisalResponse.setMessage( e.getMessage() );
		}
		catch ( InvalidVinException e )
		{
			log.warn("An unknown VIN was submitted to the Appraisal web service.");
			log.warn("Exit Code : " + ExitCode.INVALID_VIN.value() + " , Msg : " + e.getMessage());

			createAppraisalResponse.setExitCode( ExitCode.INVALID_VIN );
			createAppraisalResponse.setMessage( e.getMessage() );
		}
		
		return createAppraisalResponse;
	}
}
