
package biz.firstlook.service.appraisal.reynolds.crm;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.codehaus.xfire.annotations.InHandlers;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.Person;
import biz.firstlook.service.appraisal.PotentialDeal;
import biz.firstlook.service.appraisal.reynolds.transport.Content;
import biz.firstlook.service.appraisal.reynolds.transport.Payload;
import biz.firstlook.service.appraisal.reynolds.transport.PayloadManifest;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.ApplicationAreaType;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.Customer;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.ReyVehicleTradeInAppraisal;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.SenderType;
import biz.firstlook.service.appraisal.reynolds.transport.bindings.VehicleTradeInType;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.appraisal.v1.AbstractAppraisalWebService;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

@WebService(serviceName = "ReynoldsWebServiceCrm", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport/bindings", endpointInterface = "biz.firstlook.service.appraisal.reynolds.crm.StarTransportPortTypes")
@InHandlers(handlers={
 "biz.firstlook.service.appraisal.reynolds.crm.WSSecurityHandler",
 "org.codehaus.xfire.util.dom.DOMInHandler",
 "biz.firstlook.client.dms.rd.util.ReynoldsDirectResponseHandler"
})
public class STARWebServiceImpl extends AbstractAppraisalWebService
    implements StarTransportPortTypes
{
	protected static Logger logger = Logger.getLogger( STARWebServiceImpl.class );

    public Payload putMessage(Payload payload, PayloadManifest payloadManifest) {
      
    	logger.info("Processing a Small AppraisalRequest from Partner: Reynolds" );
    	
    	try {
    		logger.debug("Getting payload contet" );
			Content content = payload.getContent().get(0);
			logger.debug("Getting Trade In Appraisal request" );
			ReyVehicleTradeInAppraisal reyVehicleTradeInAppraisal = content
					.getReyVehicleTradeInAppraisal();
			logger.debug("Getting Trade In Application Area Type" );
			ApplicationAreaType applicationAreaType = reyVehicleTradeInAppraisal
					.getApplicationArea();
			
			SenderType senderType = null;

			if (applicationAreaType != null) {
				senderType = applicationAreaType.getSender();
			} else {
				logger.error("Reynolds CRM: Request does not comtain a ApplicationArea element");
			}

			String dealerNumber = null;
			Integer areaNumber = null, storeNumber = null;

			if (senderType != null) {
				dealerNumber = senderType.getDealerNumber();
				areaNumber = Integer.parseInt(senderType.getAreaNumber());
				storeNumber = Integer.parseInt(senderType.getStoreNumber());
			} else {
				logger.error("Reynolds CRM: Request does not contain a Sender element");
			}

			if(logger.isDebugEnabled()) {
				logger.debug("Request from Reynolds - " + "DealerNumber: " + dealerNumber + " AreaNumber: " + areaNumber + " StoreNumber: " + storeNumber );
			}
			
			BusinessUnitCredential buc = null;
			Integer businessUnitId = null;
			if (dealerNumber != null && areaNumber != null
					&& storeNumber != null) {
				// get dealer
				buc = partnerAuthenticator.getReynoldsBusinessUnitCredential(
						dealerNumber, areaNumber, storeNumber,
						CredentialType.REYNOLDS.getId());
				if (buc != null) {
					businessUnitId = buc.getBusinessUnitId();
				}
				else{
					StringBuilder errorString = new StringBuilder("Could not find a firstlook dealer that maps to");
					errorString.append(" DealerNumber: ").append(dealerNumber);
					errorString.append(" AreaNumber: ").append(areaNumber);
					errorString.append(" StoreNumber: ").append(storeNumber);
					throw new RuntimeException(errorString.toString());
				}
			}
			else{
				StringBuilder errorString = new StringBuilder("The transaction did not contain valid values for");
				errorString.append(" DealerNumber: ").append(dealerNumber);
				errorString.append(" AreaNumber: ").append(areaNumber);
				errorString.append(" StoreNumber: ").append(storeNumber);
				throw new RuntimeException(errorString.toString());
			}
			DealerPreference dealerPreference = dealerPreferenceDAO
					.findByBusinessUnitId(businessUnitId);

			if(logger.isDebugEnabled()) {
				logger.debug("Getting Trade In Vehicle Trade In Type" );
			}
			VehicleTradeInType vehicleTradeInType = reyVehicleTradeInAppraisal.getVehicleTradeIn();
			if (vehicleTradeInType != null) {
				
				biz.firstlook.service.appraisal.reynolds.transport.bindings.Vehicle vehicle = vehicleTradeInType.getVehicle();
			
				String vin = null;
				if(vehicle != null){
					vin = vehicleTradeInType.getVehicle().getVIN();
				}
				else{
					logger.error("Reynolds CRM: Request does not contain the Vehicle element under VehicleTradeInType");
				}
					
				checkRejectConditions(businessUnitId, vin, AppraisalSource.REYNOLDS);
	
				Customer customer = vehicleTradeInType.getCustomer();
				CustomerIPersonAdapter customerAdapter = null;
				if (customer != null && hasData(customer)) {
					customerAdapter = new CustomerIPersonAdapter(
							createCustomer(customer));
				}
	
				DealTrackIDealAdapter dealTrack = null;
					dealTrack = new DealTrackIDealAdapter(createPotentialDeal(
							vehicleTradeInType.getCreatedByFName(),
							vehicleTradeInType.getCreatedByLName()));
	
				String color = vehicleTradeInType.getVehicle()
						.getTradeExteriorColor();
				
				Double mileage = null;
				try{
					mileage = Double.parseDouble(vehicleTradeInType.getVehicle()
						.getMileage());
				} catch(NumberFormatException nfe) {
					throw new WebServiceException(ExitCode.MISSING_REQUIRED_ATTRIBUTES, "Invalid value for the Mileage element, expecting an Integer.");
				}
	
				List<IAppraisal> appraisals = getAppraisalService().findBy(
						businessUnitId,
						vin,
						dealerPreference
								.getSearchAppraisalDaysBackThreshold());
				if (appraisals != null && !appraisals.isEmpty()) {
					updateAppraisal(color, dealTrack, customerAdapter, appraisals,
							AppraisalSource.REYNOLDS);
				} else {
					// Create New
					Vehicle newVehicle;
					logger.debug("Looking up vehicle for VIN : " + vin);
					newVehicle = findVin(vin);
					newVehicle.setBaseColor(color);
					Dealer dealer = getDealerService().findByDealerId(
							businessUnitId);
					getAppraisalService().createAppraisal(
							new Date(), dealer, newVehicle,
							mileage.intValue(), dealTrack, customerAdapter, null,
							AppraisalSource.REYNOLDS, AppraisalTypeEnum.TRADE_IN);
					logger.debug("Reynolds CRM Appraisal successfully created");
				}
			} else {
				throw new WebServiceException(ExitCode.MISSING_REQUIRED_ATTRIBUTES, "Missing VehicleTradeInType element.");
			}
		} catch (WebServiceException e) {
			logger.warn("A web service expection was thrown when attempting to submit a Full Appraisal.");
			logger.warn("Exit Code : " + e.getExitCode().value() + " , Msg : "
					+ e.getMessage());
		} catch (InvalidVinException e) {
			logger.warn("An unknown VIN was submitted to the Appraisal web service.");
			logger.warn("Exit Code : " + ExitCode.INVALID_VIN.value() + " , Msg : " + e.getMessage());
		} catch (Exception e) {
			logger.error("Error with Reynolds Crm (web service):", e);
		}
    	return null;
    }
    
    

    private PotentialDeal createPotentialDeal(String firstName, String lastName) {
    	PotentialDeal potentialDeal = new PotentialDeal();
    	Person salesPerson = new Person();
    	salesPerson.setFirstName(firstName);
    	salesPerson.setLastName(lastName);
    	potentialDeal.setPerson(salesPerson);
    	return potentialDeal;
    }
    
    protected biz.firstlook.service.appraisal.Customer createCustomer(Customer data) {  	
    	biz.firstlook.service.appraisal.Customer customer = new biz.firstlook.service.appraisal.Customer();
    	Person person = new Person();
    	person.setFirstName(data.getCustFName());
    	person.setLastName(data.getCustLName());
    	customer.setPerson(person);
    	customer.setPhoneNumber(data.getCustPhone());
    	customer.setEmail(data.getCustEmail());
    	return customer;
    }
    
    private boolean hasData(Customer data) {
    	return (data.getCustFName() != null
    		|| data.getCustLName() != null
    		|| data.getCustPhone() != null
    		|| data.getCustEmail() != null);
    }
}
