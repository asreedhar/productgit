
package biz.firstlook.service.appraisal.reynolds.transport.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehicleTradeInType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleTradeInType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Customer"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Vehicle"/>
 *         &lt;element name="CreatedByFName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreatedByLName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreatedByUserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoginName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ReynoldsTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ReynoldsUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleTradeInType", propOrder = {
    "customer",
    "vehicle",
    "createdByFName",
    "createdByLName",
    "createdByUserName",
    "loginName",
    "reynoldsTransactionID",
    "reynoldsUserID"
})
public class VehicleTradeInType {

    @XmlElement(name = "Customer")
    protected Customer customer;
    @XmlElement(name = "Vehicle", required = true)
    protected Vehicle vehicle;
    @XmlElement(name = "CreatedByFName", required = true)
    protected String createdByFName;
    @XmlElement(name = "CreatedByLName", required = true)
    protected String createdByLName;
    @XmlElement(name = "CreatedByUserName", required = true)
    protected String createdByUserName;
    @XmlElement(name = "LoginName", required = true)
    protected String loginName;
    @XmlElement(name = "ReynoldsTransactionID", required = true)
    protected String reynoldsTransactionID;
    @XmlElement(name = "ReynoldsUserID", required = true)
    protected String reynoldsUserID;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link Customer }
     *     
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer }
     *     
     */
    public void setCustomer(Customer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the createdByFName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByFName() {
        return createdByFName;
    }

    /**
     * Sets the value of the createdByFName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByFName(String value) {
        this.createdByFName = value;
    }

    /**
     * Gets the value of the createdByLName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByLName() {
        return createdByLName;
    }

    /**
     * Sets the value of the createdByLName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByLName(String value) {
        this.createdByLName = value;
    }

    /**
     * Gets the value of the createdByUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByUserName() {
        return createdByUserName;
    }

    /**
     * Sets the value of the createdByUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByUserName(String value) {
        this.createdByUserName = value;
    }

    /**
     * Gets the value of the loginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * Sets the value of the loginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginName(String value) {
        this.loginName = value;
    }

    /**
     * Gets the value of the reynoldsTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReynoldsTransactionID() {
        return reynoldsTransactionID;
    }

    /**
     * Sets the value of the reynoldsTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReynoldsTransactionID(String value) {
        this.reynoldsTransactionID = value;
    }

    /**
     * Gets the value of the reynoldsUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReynoldsUserID() {
        return reynoldsUserID;
    }

    /**
     * Sets the value of the reynoldsUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReynoldsUserID(String value) {
        this.reynoldsUserID = value;
    }

}
