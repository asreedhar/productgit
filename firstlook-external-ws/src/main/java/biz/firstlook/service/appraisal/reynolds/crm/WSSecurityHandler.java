package biz.firstlook.service.appraisal.reynolds.crm;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.namespace.QName;

import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.handler.AbstractHandler;
import org.codehaus.xfire.handler.Handler;
import org.codehaus.xfire.security.wss4j.WSS4JInHandler;

public class WSSecurityHandler extends AbstractHandler {
	List<Handler> inHandlers;

	public WSSecurityHandler() {
		WSS4JInHandler wss4jInHandler;
		ReynoldsValidateUserTokenHandler userTokenHandler;
		Properties props = new Properties();

		props.setProperty(WSHandlerConstants.ACTION,
				WSHandlerConstants.USERNAME_TOKEN);
		
		props.setProperty(WSHandlerConstants.PASSWORD_TYPE,
		WSConstants.PW_TEXT);
	
		props.setProperty(WSHandlerConstants.PW_CALLBACK_CLASS,
				ReynoldsPasswordHandler.class.getName());

		wss4jInHandler = new WSS4JInHandler(props);
		userTokenHandler = new ReynoldsValidateUserTokenHandler();
		inHandlers = new ArrayList<Handler>();
		inHandlers.add(wss4jInHandler);
		inHandlers.add(userTokenHandler);
	}

	public QName[] getUnderstoodHeaders() {
		return new QName[] { new QName(
				"http://docs.oasis-open.org/wss/2004/01/"
						+ "oasis-200401-wss-wssecurity-secext-1.0.xsd",
				"Security") };
	}

	public void invoke(MessageContext messageContext) throws Exception {
		for (Handler handler : inHandlers) {
			handler.invoke(messageContext);
		}
		// User should be set by ValidateUserTokenHandler
		// You can also choose to do all your security checks in
		// ValidateUserTokenHandler which does a lot of the work for you
		if (messageContext.getProperty(WSHandlerConstants.ENCRYPTION_USER) == null) {
			throw new Exception("Principal not found");
		}

	}
}
