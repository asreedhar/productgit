package biz.firstlook.service.appraisal.v1;

import java.util.Date;
import java.util.List;

import org.jdom.Content;
import org.jdom.Element;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.Credentials;
import biz.firstlook.service.appraisal.Customer;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.Person;
import biz.firstlook.service.appraisal.PotentialDeal;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.authenticate.PartnerAuthenticationException;
import biz.firstlook.service.authenticate.PartnerAuthenticator;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.InventoryDAO;
import biz.firstlook.transact.persist.service.DealerService;
import biz.firstlook.transact.persist.person.IPersonService;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFactory;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;
import biz.firstlook.transact.persist.service.vehicle.VehicleService;

/* Okay, if you're here, you may or may not yet be wondering wtf is up with the insanity. As far as I can tell,
 * this whole crazy Appraisals contraption boils down to the fact that they hosed the hibernate config in the
 * first place such that it is near-impossible to do simple things like delete an appraisal without running
 * into trouble. I'm no hibernate expert but see for instance both appraisal and customer hbm.xml files establishing
 * one way relationships with each other. So basically, all of this crap that you see is being done to avoid (this is
 * an educated-through-pain guess) actually holding a reference to anything from the appraisal you're actually working with.
 * Thus the pattern of .getAbsolutelyEverything being fed into another thing to rebuild crap that already exists.
 * 
 * Note: I'm the JavaScript guy. I could be totally wrong about the reasoning behind it all or whether config is wrong
 * so take the 'why' stuff with a grain of salt if you're fixing this and verify yourself since I didn't know diddly about
 * hibernate a couple weeks ago.
 * 
 * So here's the players:
 * 
 * GENERAL
 * Appraisal - Three now. Highest level classes and not much in the way of implementation. One for the DB-layer, one for Soap, 
 * and one for our recently added rest service (and no, if it had been me, I wouldn't have called them all Appraisal).
 * 
 * REST SERVICE
 * AppraisalWebServiceWrapper - Not what I'd call a wrapper. The intent was to insulate from the mess that is appraisals so we could re-use
 * existing code.
 * 
 * WIRELESS/SOAP SERVICE
 * WS was written long after the rest of this disaster and appears to resolve the problem in its own way by using these Adapter wrappers. If
 * I knew more, I would have just re-used that but it also looked a touch batty and my instinct was to avoid adding as few classes and new
 * interdependencies as possible to this stumbling drunk juggernaut.
 * 
 * MOBILE/REST (RS) AND ORIGINAL WIRELESS/SOAP (WS)
 * AbstractAppraisalWebService - You're here so you've no doubt figured out this is the real class behind the curtain. This is where
 * most of the real implementation details are kicked off.
 * 
 * DB LAYER
 * AppraisalService - Two, but I'm ignoring firstlookServices since I have no idea what that's for at this time. AppraisalService is the DB
 * layer that actually makes use of DAOs and also where most of the really screw stuff all comes together. Screwy stuff follows.
 * 
 * ACTUALLY CREATING A NEW APPRAISAL
 * AppraisalFactory - It doesn't build appraisals. It builds things you need to createAppraisals.
 * AppraisalBuilder - It uses AppraisalFactory to take a bunch of args and create things to stick in to a new Appraisal object before updating it.
 * AppraisalService.buildAppraisal - having fun yet? This takes most of createAppraisals args and sets off the AppraisalBuilder.
 * 
 * WHAT? WHY?!
 * As I said I'm guessing but the idea appears to be not to re-use any object directly connected to hibernate. And yes,
 * the workarounds are bloated as all getout. If you're fixing this mess, I'd recommend looking at whether they aren't unnecessarily tying child
 * and parent objects together confusing the heck out of cascading behavior, so you can fix the problem at its foundation and rebuild the whole
 * thing with a tenth of the code.
 * 
 * WORKING WITH EXISTING APPRAISALS
 * If you need to pull appraisal data from an existing Appraisal to create a new appraisal or to determine whether it needs to be deleted, updated,
 * or saved, avoid creating references to any of the actual objects pointed at in the hbm.xml files. Just use the AppraisalFactory to rebuild new
 * objects with properties grabbed from the larger appraisal child objects via getters/setters as-needed.
 * 
 * GENERAL TIP
 * Treat this whole damn thing like the Death Star of happy fun balls. Don't experiment or play with it. Don't even taunt it. Everything is here
 * for a bad reason. Just assume you can't work outside of this twisted puzzle box and that every method is inextrciably protected and every
 * reasonable thing you might think to do won't work for all manner of batshit reasons. If you're not here to nuke most of it from orbit, just
 * play by its stupid rules and be prepared to do some drinking tonight.
 * 
 * -Erik Reppn 6-19-2013
 */

public abstract class AbstractAppraisalWebService  {
	protected PartnerAuthenticator partnerAuthenticator;
	protected AppraisalFactory appraisalFactory;
	protected IAppraisalService appraisalService;
	protected VehicleService vehicleService;
	protected DealerService dealerService;
	protected InventoryDAO inventoryDAO;
	protected DealerPreferenceDAO dealerPreferenceDAO;
	protected IPersonService personService;

	protected BusinessUnitCredential authenticate( Credentials credential ) throws WebServiceException {
		if(credential == null) {
			throw new WebServiceException(ExitCode.AUTHENTICATION_FAILED,"Missing credentials.");
		}
		
		try {
			return partnerAuthenticator.getBusinessUnitCredential( 
					credential.getDealerId(), 
					Integer.parseInt( credential.getPartnerId() ), 
					credential.getPasscode() );
		} catch (NumberFormatException e) {
			throw new WebServiceException(ExitCode.AUTHENTICATION_FAILED, "PartnerId invalid.");
		} catch (PartnerAuthenticationException e) {
			throw new WebServiceException(ExitCode.AUTHENTICATION_FAILED, e.getMessage());
		}
	}
	
	protected Credentials setUpCredentials(String dealerCode, String password, CredentialType partner) {
		Credentials credential = new Credentials();
		credential.setDealerId(dealerCode);
		credential.setPasscode(password);
		credential.setPartnerId(partner.getId().toString());
		return credential;
	}
	
//	 f-ing jdom
	protected Element getChildElement(List<Content> content, String string) {
		Element result = null;
		for (Content c : content) {
			if (c instanceof Element) {
				Element e = (Element)c;
				if (e.getName().equals(string)) {
					result = e;
					break;
				}
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	protected PotentialDeal createPotentialDeal(Element vehicle, String firstName, String lastName) {
		PotentialDeal potentialDeal = new PotentialDeal();
		potentialDeal.setPerson(createPerson(
				getChildElement(vehicle.getContent(), firstName),
				getChildElement(vehicle.getContent(), lastName)));
		return potentialDeal;
	}
	
	@SuppressWarnings("unchecked")
	protected Customer createCustomer(Element vehicle, String firstName, String lastName, String phoneNumber, String email) {
		Customer customer = new Customer();
		customer.setPerson(createPerson(
						getChildElement(vehicle.getContent(), firstName),
						getChildElement(vehicle.getContent(), lastName)));
		customer.setPhoneNumber(vehicle.getChildText(phoneNumber));
		customer.setEmail(email);
		return customer;
	}
	
	private Person createPerson(final Element firstName, final Element lastName) {
		Person person = new Person();
		person.setFirstName(firstName == null ? null : firstName.getValue());
		person.setLastName(lastName == null ? null : lastName.getValue());
		return person;
	}
	
	protected Vehicle findVin(String vin) throws InvalidVinException, WebServiceException {
		try {
			return vehicleService.identifyVehicle( vin );
		} catch (VehicleCatalogServiceException e) {
			throw new WebServiceException(ExitCode.INVALID_VIN, "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog " );
		}
	}
	
	protected void updateAppraisal(String color, DealTrackIDealAdapter dealTrack, CustomerIPersonAdapter customer, List<IAppraisal> appraisals, AppraisalSource appraisalSource) throws WebServiceException {
		// get appraisal - there should only be one
		biz.firstlook.transact.persist.service.appraisal.IAppraisal appraisal = (IAppraisal)appraisals.get(0);
		if (appraisal.getAppraisalSource().equals(AppraisalSource.THE_EDGE) || appraisal.getAppraisalSource().equals(appraisalSource) ) {
			getAppraisalService().updateAppraisal(appraisal, dealTrack, customer, null, color);
		} else {
			// should not happen
			throw new WebServiceException( ExitCode.CONFLICTS_WITH_EXISTING_APPRAISAL, "An error occured while trying to merge appraisal info");
		}
	}	
	
	protected void createNewAppraisal(Integer mileage, DealTrackIDealAdapter dealTrack, CustomerIPersonAdapter customer, Vehicle vehicle, Dealer dealer, AppraisalSource appraisalSource) {
		getAppraisalService().createAppraisal( new Date(System.currentTimeMillis()), dealer, vehicle,
											mileage, dealTrack, customer,
											null, appraisalSource, AppraisalTypeEnum.TRADE_IN);
	}
	
	
	/**
	 * Reject conditions have changed per S1845.
	 * 
	 * We reject a CRM Appraisal when:
	 * 		1) A CRM appraisal already exists for this VIN and BU
	 * Otherwise, depending on state, we merge the data in the incoming CRM appraisal into an existing CRM, Wireless or Edge appraisal. 	
	 *
	 * 
	 * We reject a Wireless Appraisal when:
	 * 		1) A Wireless appraisal already exists for this VIN and BU
	 * Otherwise, depending on state, we merge the data in the incoming Wireless appraisal with an existing CRM, Wireless or Edge appraisal. 	
	 * 
	 * @param businessUnitId
	 * @param vin
	 * @throws WebServiceException
	 */
	protected void checkRejectConditions( Integer businessUnitId, String vin, AppraisalSource source) throws WebServiceException
	{
		DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(businessUnitId);

		Inventory inventory = inventoryDAO.searchVinInActiveInventory(businessUnitId, vin);
		if( inventory != null )
			throw new WebServiceException( ExitCode.EXISTS_IN_ACTIVE_INVENTORY, "An appraisal for this vehicle already exists in active inventory" );
		
		inventory = inventoryDAO.searchVinInInactiveInventory(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
		if( inventory != null )
			throw new WebServiceException( ExitCode.EXISTS_IN_IN_ACTIVE_INVENTORY, "An appraisal for this vehicle already exists in InActive inventory" );

		List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
		if (appraisals != null && !appraisals.isEmpty()) {
			// get appraisal - there should only be one
			biz.firstlook.transact.persist.service.appraisal.IAppraisal appraisal = (IAppraisal)appraisals.get(0);
			if (appraisal.getAppraisalSource().equals(AppraisalSource.THE_EDGE)) {
				if ((source.equals(AppraisalSource.WAVIS)) || (source.isMobile())) {
					throw new WebServiceException( ExitCode.CONFLICTS_WITH_EXISTING_APPRAISAL, "  A complete appraisal already exists in the system");
				}
				//if AUTOBASE - combine in next step
			} else if (appraisal.getAppraisalSource().equals(AppraisalSource.AUTOBASE)){
				if (source.equals(AppraisalSource.AUTOBASE)) {
					//Replace the pending appraisal with this new one.
					getAppraisalService().deleteAppraisal( appraisal ); 
				}
			} else if (appraisal.getAppraisalSource().equals(AppraisalSource.WAVIS)){
				if ((source.equals(AppraisalSource.WAVIS)) || (source.isMobile())) {
					//Replace the pending appraisal with this new one.
					getAppraisalService().deleteAppraisal( appraisal ); 
				}
			} else if (appraisal.getAppraisalSource().equals(AppraisalSource.DEALERSOCKET)){
				if (source.equals(AppraisalSource.DEALERSOCKET)) {
					//Replace the pending appraisal with this new one.
					getAppraisalService().deleteAppraisal( appraisal ); 
				}
			}else if (appraisal.getAppraisalSource().equals(AppraisalSource.HIGHERGEAR)){
				if (source.equals(AppraisalSource.HIGHERGEAR)) {
					//Replace the pending appraisal with this new one.
					getAppraisalService().deleteAppraisal( appraisal ); 
				}
			}else if (appraisal.getAppraisalSource().equals(AppraisalSource.REYNOLDS)){
				if (source.equals(AppraisalSource.REYNOLDS)) {
					//Replace the pending appraisal with this new one.
					getAppraisalService().deleteAppraisal( appraisal ); 
				}
			}
			else if (appraisal.getAppraisalSource().equals(AppraisalSource.MOBILE)) {
				if ((source.equals(AppraisalSource.WAVIS)) || (source.isCRMOrMobile())) {
					throw new WebServiceException( ExitCode.CONFLICTS_WITH_EXISTING_APPRAISAL, "  An appraisal with this vin and dealer already exists in the system");					
				}
			}
			
		}
	}

	protected PartnerAuthenticator getPartnerAuthenticator() {
		return partnerAuthenticator;
	}

	public void setPartnerAuthenticator(PartnerAuthenticator partnerAuthenticator) {
		this.partnerAuthenticator = partnerAuthenticator;
	}

	protected AppraisalFactory getAppraisalFactory()
	{
		return appraisalFactory;
	}

	public void setAppraisalFactory( AppraisalFactory appraisalFactory )
	{
		this.appraisalFactory = appraisalFactory;
	}

	protected IAppraisalService getAppraisalService()
	{
		return appraisalService;
	}

	public void setAppraisalService( IAppraisalService appraisalService )
	{
		this.appraisalService = appraisalService;
	}

	protected DealerService getDealerService()
	{
		return dealerService;
	}

	public void setDealerService( DealerService dealerService )
	{
		this.dealerService = dealerService;
	}

	protected VehicleService getVehicleService()
	{
		return vehicleService;
	}

	public void setVehicleService( VehicleService vehicleService )
	{
		this.vehicleService = vehicleService;
	}
	
	public void setInventoryDAO(InventoryDAO inventoryDAO) {
		this.inventoryDAO = inventoryDAO;
	}

	public void setDealerPreferenceDAO(DealerPreferenceDAO dealerPreferenceDAO) {
		this.dealerPreferenceDAO = dealerPreferenceDAO;
	}

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	
}
