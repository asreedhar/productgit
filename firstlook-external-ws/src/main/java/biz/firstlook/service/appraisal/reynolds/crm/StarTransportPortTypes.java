
package biz.firstlook.service.appraisal.reynolds.crm;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import biz.firstlook.service.appraisal.reynolds.transport.Payload;
import biz.firstlook.service.appraisal.reynolds.transport.PayloadManifest;

@WebService(name = "ReynoldsWebServiceCrm", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport/bindings")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface StarTransportPortTypes {

    @WebMethod(operationName = "PutMessage")
    @WebResult(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
    public Payload putMessage(
       @WebParam(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
        Payload payload,
        @WebParam(name = "payloadManifest", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport", header = true)
        PayloadManifest payloadManifest);
}
