
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for bookValueType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="bookValueType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BLACKBOOK_ExtraClean"/>
 *     &lt;enumeration value="BLACKBOOK_Clean"/>
 *     &lt;enumeration value="BLACKBOOK_Average"/>
 *     &lt;enumeration value="BLACKBOOK_Rough"/>
 *     &lt;enumeration value="NADA_Retail"/>
 *     &lt;enumeration value="NADA_TradeIn"/>
 *     &lt;enumeration value="NADA_Loan"/>
 *     &lt;enumeration value="Kelley_Retail"/>
 *     &lt;enumeration value="Kelley_Wholesale"/>
 *     &lt;enumeration value="Galves_TradeIn"/>
 *     &lt;enumeration value="Galves_MarketReady"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum BookValueType {

    @XmlEnumValue("BLACKBOOK_Average")
    BLACKBOOK_AVERAGE("BLACKBOOK_Average"),
    @XmlEnumValue("BLACKBOOK_Clean")
    BLACKBOOK_CLEAN("BLACKBOOK_Clean"),
    @XmlEnumValue("BLACKBOOK_ExtraClean")
    BLACKBOOK_EXTRA_CLEAN("BLACKBOOK_ExtraClean"),
    @XmlEnumValue("BLACKBOOK_Rough")
    BLACKBOOK_ROUGH("BLACKBOOK_Rough"),
    @XmlEnumValue("Galves_MarketReady")
    GALVES_MARKET_READY("Galves_MarketReady"),
    @XmlEnumValue("Galves_TradeIn")
    GALVES_TRADE_IN("Galves_TradeIn"),
    @XmlEnumValue("Kelley_Retail")
    KELLEY_RETAIL("Kelley_Retail"),
    @XmlEnumValue("Kelley_Wholesale")
    KELLEY_WHOLESALE("Kelley_Wholesale"),
    @XmlEnumValue("NADA_Loan")
    NADA_LOAN("NADA_Loan"),
    @XmlEnumValue("NADA_Retail")
    NADA_RETAIL("NADA_Retail"),
    @XmlEnumValue("NADA_TradeIn")
    NADA_TRADE_IN("NADA_TradeIn");
    private final String value;

    BookValueType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BookValueType fromValue(String v) {
        for (BookValueType c: BookValueType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
