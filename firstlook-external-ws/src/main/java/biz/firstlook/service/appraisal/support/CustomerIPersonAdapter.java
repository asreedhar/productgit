package biz.firstlook.service.appraisal.support;

import biz.firstlook.service.appraisal.Customer;

public class CustomerIPersonAdapter extends biz.firstlook.transact.persist.service.appraisal.Customer
{

private static final long serialVersionUID = -3285163459684432521L;

private Customer customer;

public CustomerIPersonAdapter( Customer customer )
{
	this.customer = customer;
}

public String getEmail()
{
	return customer.getEmail();
}

public String getFirstName()
{
	return (customer.getPerson() != null ? customer.getPerson().getFirstName() : null );
}

public String getGender()
{
	return (customer.getGender() != null ? customer.getGender().toString() : null );
}

public String getLastName()
{
	return (customer.getPerson() != null ? customer.getPerson().getLastName() : null);
}

public String getPhoneNumber()
{
	return customer.getPhoneNumber();
}

}
