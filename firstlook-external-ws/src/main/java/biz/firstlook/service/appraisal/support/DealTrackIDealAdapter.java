package biz.firstlook.service.appraisal.support;

import biz.firstlook.service.appraisal.DealType;
import biz.firstlook.service.appraisal.PotentialDeal;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.appraisal.IDeal;

public class DealTrackIDealAdapter implements IDeal
{

private String firstName;
private String lastName;
private DealType dealType;
private String stockNumber;

public DealTrackIDealAdapter( PotentialDeal potentialDeal )
{
	if ( potentialDeal.getPerson() == null )
	{
		this.firstName = null;
		this.lastName = null;
	}
	else {
		this.firstName = potentialDeal.getPerson().getFirstName();
		this.lastName = potentialDeal.getPerson().getLastName();
	}
	this.dealType = potentialDeal.getNewOrUsedDeal();
	this.stockNumber = potentialDeal.getStockNumber();
}

public InventoryTypeEnum getDealType()
{
	if ( dealType == null )
	{
		return InventoryTypeEnum.UNKNOWN;
	}
	else
	{
		switch ( dealType )
		{
			case NEW:
				return InventoryTypeEnum.NEW;
			case USED:
				return InventoryTypeEnum.USED;
			default:
				return InventoryTypeEnum.UNKNOWN;
		}
	}
}

public String getFirstName()
{
	return firstName;
}

public String getLastName()
{
	return lastName;
}

public String getStockNumber()
{
	return stockNumber;
}

/**
 * Always return null. 
 * A sales person in a Wireless/CRM appraisal does not have a FK relationship to our people tbl.
 * A null value indicates at saving time [ AppraisalBuilder.makePotentialDeal() ] to create a new temp(implied) person.
 */
public Integer getSalesPersonId() { return null;}
public IPerson getSalesPerson() { return null; }

}
