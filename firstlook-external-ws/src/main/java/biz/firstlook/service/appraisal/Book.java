
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlEnum;


/**
 * <p>Java class for book.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="book">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BLACKBOOK"/>
 *     &lt;enumeration value="NADA"/>
 *     &lt;enumeration value="KELLEY"/>
 *     &lt;enumeration value="GALVES"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum Book {

    BLACKBOOK,
    GALVES,
    KELLEY,
    NADA;

    public String value() {
        return name();
    }

    public static Book fromValue(String v) {
        return valueOf(v);
    }

}
