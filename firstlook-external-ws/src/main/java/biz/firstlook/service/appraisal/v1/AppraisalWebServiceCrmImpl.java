package biz.firstlook.service.appraisal.v1;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.List;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.CreateAppraisalRequest;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

@WebService( serviceName = "AppraisalWebServiceCrm", targetNamespace = "http://www.firstlook.biz/service/appraisal/v1", endpointInterface = "biz.firstlook.service.appraisal.v1.AppraisalWebServiceCrm" )
public class AppraisalWebServiceCrmImpl extends AbstractAppraisalWebService implements AppraisalWebServiceCrm
{

protected static final Logger logger = Logger.getLogger( AppraisalWebServiceCrmImpl.class );

private static final String MISSING_MALFORMED_MSG = "{0} is missing or malformed.";

public CreateAppraisalResponse createAppraisal( CreateAppraisalRequest createAppraisalRequest )
{
	CreateAppraisalResponse response = new CreateAppraisalResponse();
	response.setExitCode( ExitCode.SUCCESS );
	response.setMessage( ExitCode.SUCCESS.toString() );

	try
	{
		logger.info("Processing a Small AppraisalRequest from Partner: " + createAppraisalRequest.getCredentials().getPartnerId() );
		
		BusinessUnitCredential businessUnitCredential = authenticate( createAppraisalRequest.getCredentials() );
		
		checkRejectConditions( businessUnitCredential.getBusinessUnitId(), createAppraisalRequest.getVehicle().getVin(), AppraisalSource.AUTOBASE);
		
		createAppraisal( createAppraisalRequest, businessUnitCredential );
	}
	catch ( WebServiceException e )
	{
		logger.warn("A web service expection was thrown when attempting to submit a Full Appraisal.");
		logger.warn("Exit Code : " + e.getExitCode().value() + " , Msg : " + e.getMessage());

		response.setExitCode( e.getExitCode() );
		response.setMessage( e.getMessage() );
		return response;
	}
	catch ( InvalidVinException e )
	{
		logger.warn("An unknown VIN was submitted to the Appraisal web service.");
		logger.warn("Exit Code : " + ExitCode.INVALID_VIN.value() + " , Msg : " + e.getMessage());

		response.setExitCode( ExitCode.INVALID_VIN );
		response.setMessage( e.getMessage() );
		return response;
	}

	// find a way to alert the lundie 
	//Boolean alertUCM = createAppraisalRequest.isAlertUCM();

	return response;
}

/**
 * 
 * As per S1845:
 * 	1) Combine this CRM info into an existing Edge/Wireless one if it exists
 *  2) Write a new CRM appraisal if none exists
 *  3) (If a CRM appraisal already existed for this VIN/BU, it was deleted in the previous rejectCondition 
 *  method and the case is the same as #2
 *  
 * @param createAppraisalRequest
 * @param businessUnitCredential
 * @throws InvalidVinException
 * @throws WebServiceException
 */
private void createAppraisal( CreateAppraisalRequest createAppraisalRequest, BusinessUnitCredential businessUnitCredential )
		throws InvalidVinException, WebServiceException
{
	validate(createAppraisalRequest);
	Integer businessUnitId = businessUnitCredential.getBusinessUnitId();
	String vin = createAppraisalRequest.getVehicle().getVin();
	String color = createAppraisalRequest.getVehicle().getColor();
	String notes = createAppraisalRequest.getVehicle().getNotes();
	DealTrackIDealAdapter dealTrack = null;
	if ( createAppraisalRequest.getPotentialDeal() != null )
		dealTrack = new DealTrackIDealAdapter( createAppraisalRequest.getPotentialDeal() );
	CustomerIPersonAdapter customer = null;
	if ( createAppraisalRequest.getCustomer() != null )
		customer = new CustomerIPersonAdapter( createAppraisalRequest.getCustomer() );
	DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(businessUnitId);

	// Update existing
	List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
	if (appraisals != null && !appraisals.isEmpty()) {
		// get appraisal - there should only be one
		biz.firstlook.transact.persist.service.appraisal.IAppraisal appraisal = (IAppraisal)appraisals.get(0);
		if (appraisal.getAppraisalSource().equals(AppraisalSource.THE_EDGE) || appraisal.getAppraisalSource().equals(AppraisalSource.WAVIS) ) {
			getAppraisalService().updateAppraisal(appraisal, dealTrack, customer, notes, color);
		} else {
			// should not happen
			throw new WebServiceException( ExitCode.CONFLICTS_WITH_EXISTING_APPRAISAL, "An error occured while trying to merge appraisal info");
		}
	} else {
		// Create New
		Vehicle vehicle;
		try {
			logger.info("Looking up vehicle for VIN : " + vin);
			vehicle = getVehicleService().identifyVehicle( vin );
		} catch (VehicleCatalogServiceException e) {
			throw new WebServiceException(ExitCode.INVALID_VIN, "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog " );
		}
		vehicle.setBaseColor( color );
		Dealer dealer = getDealerService().findByDealerId( businessUnitId );
		getAppraisalService().createAppraisal( createAppraisalRequest.getAppraisalCreateTime().toGregorianCalendar().getTime(), dealer, vehicle,
											createAppraisalRequest.getVehicle().getMileage().intValue(), dealTrack, customer,
											notes, AppraisalSource.AUTOBASE, AppraisalTypeEnum.TRADE_IN ); // all wireless are Trade-in appraisals
	}
}

//JAXB is lame since the @XMLElment(required=true) doesn't perform validation during runtime for performance reasons.
private void validate(final CreateAppraisalRequest createAppraisalRequest) throws WebServiceException {
	
	if(createAppraisalRequest == null) {
		throw new WebServiceException(ExitCode.SCHEMA_INVALID, MessageFormat.format(MISSING_MALFORMED_MSG, "createAppraisalRequest"));
	}
	
	biz.firstlook.service.appraisal.Vehicle v = createAppraisalRequest.getVehicle();
	if(v == null) {
		throw new WebServiceException(ExitCode.SCHEMA_INVALID, MessageFormat.format(MISSING_MALFORMED_MSG, "vehicle"));
	}
	
	String vin = v.getVin();
	if(vin == null || vin.trim().equals("")) {
		throw new WebServiceException(ExitCode.INVALID_VIN, MessageFormat.format(MISSING_MALFORMED_MSG, "vin"));
	}
	
	BigInteger mileage = v.getMileage();
	if(mileage == null) {
		throw new WebServiceException(ExitCode.MISSING_REQUIRED_ATTRIBUTES, 
			"The mileage xml element seems to be missing or malformed.  Check that the value does not contain non numeric characters, as specified by the wsdl.");
	}
}

}

