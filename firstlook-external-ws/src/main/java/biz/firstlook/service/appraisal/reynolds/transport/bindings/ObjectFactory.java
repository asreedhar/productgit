
package biz.firstlook.service.appraisal.reynolds.transport.bindings;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.starstandards.star package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StoreNumber_QNAME = new QName("http://www.starstandards.org/STAR", "StoreNumber");
    private final static QName _ReyVehicleTradeInAppraisal_QNAME = new QName("http://www.starstandards.org/STAR", "rey_VehicleTradeInAppraisal");
    private final static QName _DealerCountry_QNAME = new QName("http://www.starstandards.org/STAR", "DealerCountry");
    private final static QName _DealerNumber_QNAME = new QName("http://www.starstandards.org/STAR", "DealerNumber");
    private final static QName _AreaNumber_QNAME = new QName("http://www.starstandards.org/STAR", "AreaNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.starstandards.star
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DestinationType }
     * 
     */
    public DestinationType createDestinationType() {
        return new DestinationType();
    }

    /**
     * Create an instance of {@link Vehicle }
     * 
     */
    public Vehicle createVehicle() {
        return new Vehicle();
    }

    /**
     * Create an instance of {@link ApplicationAreaType }
     * 
     */
    public ApplicationAreaType createApplicationAreaType() {
        return new ApplicationAreaType();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link ReyVehicleTradeInAppraisal }
     * 
     */
    public ReyVehicleTradeInAppraisal createReyVehicleTradeInAppraisal() {
        return new ReyVehicleTradeInAppraisal();
    }

    /**
     * Create an instance of {@link VehicleTradeInType }
     * 
     */
    public VehicleTradeInType createVehicleTradeInType() {
        return new VehicleTradeInType();
    }

    /**
     * Create an instance of {@link SenderType }
     * 
     */
    public SenderType createSenderType() {
        return new SenderType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "StoreNumber")
    public JAXBElement<String> createStoreNumber(String value) {
        return new JAXBElement<String>(_StoreNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReyVehicleTradeInAppraisal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "rey_VehicleTradeInAppraisal")
    public JAXBElement<ReyVehicleTradeInAppraisal> createReyVehicleTradeInAppraisal(ReyVehicleTradeInAppraisal value) {
        return new JAXBElement<ReyVehicleTradeInAppraisal>(_ReyVehicleTradeInAppraisal_QNAME, ReyVehicleTradeInAppraisal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DealerCountry")
    public JAXBElement<String> createDealerCountry(String value) {
        return new JAXBElement<String>(_DealerCountry_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DealerNumber")
    public JAXBElement<String> createDealerNumber(String value) {
        return new JAXBElement<String>(_DealerNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "AreaNumber")
    public JAXBElement<String> createAreaNumber(String value) {
        return new JAXBElement<String>(_AreaNumber_QNAME, String.class, null, value);
    }

}
