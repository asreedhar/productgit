package biz.firstlook.service.appraisal.highergear;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;
import biz.firstlook.service.appraisal.Credentials;
import biz.firstlook.service.appraisal.Customer;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.Person;
import biz.firstlook.service.appraisal.PotentialDeal;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.appraisal.v1.AbstractAppraisalWebService;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

@WebService( serviceName = "HigherGearWebServiceCrm", targetNamespace = "http://www.firstlook.biz/service/appraisal/highergear", endpointInterface = "biz.firstlook.service.appraisal.highergear.HigherGearWebServiceCrm" )
public class HigherGearWebServiceCrmImpl extends AbstractAppraisalWebService implements HigherGearWebServiceCrm
{

protected static Logger logger = Logger.getLogger( HigherGearWebServiceCrmImpl.class );

public CreateAppraisalResponse createAppraisal( AddAppraisal addAppraisal )
{
	CreateAppraisalResponse response = new CreateAppraisalResponse();
	response.setExitCode( ExitCode.SUCCESS );
	response.setMessage( ExitCode.SUCCESS.toString() );

	try
	{
		logger.info("Processing a Small AppraisalRequest from Partner: Higher Gear" );
		
		Data data = addAppraisal.getData();
		
		String dealerCode =  data.getCompanyCode();
		String password = data.getLoginName();

		Credentials credential = setUpCredentials(dealerCode, password, CredentialType.HIGHERGEAR);
		
		// authenticate and get business unit info
		BusinessUnitCredential businessUnitCredential = authenticate(credential); // throws AUTHENTICATION exception
		Integer businessUnitId = businessUnitCredential.getBusinessUnitId();
		DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(businessUnitId);

		// extract content
		String vin = data.getVin();
		String color = data.getTradeExteriorColor();
		Double mileage = Double.valueOf(0.0d);
		try {
			mileage = Double.valueOf(data.getMileage());
		} catch(Exception e) {
			//ignore
		}
		
		DealTrackIDealAdapter dealTrack = new DealTrackIDealAdapter( createPotentialDeal(data) );
		CustomerIPersonAdapter customer = new CustomerIPersonAdapter( createCustomer(data) );
//		 check reject conditions and delete existing if appraisal already exists for this VIN/BUid 
		checkRejectConditions(businessUnitId, vin, AppraisalSource.HIGHERGEAR);

		// Update existing
		List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
		if (appraisals != null && !appraisals.isEmpty()) {
			updateAppraisal(color, dealTrack, customer, appraisals, AppraisalSource.HIGHERGEAR);
		} else {
			// Create New
			Vehicle vehicle;
			logger.debug("Looking up vehicle for VIN : " + vin);
			vehicle = findVin(vin);
			vehicle.setBaseColor( color );
			Dealer dealer = getDealerService().findByDealerId( businessUnitId );
			getAppraisalService().createAppraisal( new Date(System.currentTimeMillis()), dealer, vehicle,
												mileage.intValue(), dealTrack, customer,
												null, AppraisalSource.HIGHERGEAR, AppraisalTypeEnum.TRADE_IN);
		}
		// success
	} catch ( WebServiceException e )
	{
		logger.warn("A web service expection was thrown when attempting to submit a Full Appraisal.");
		logger.warn("Exit Code : " + e.getExitCode().value() + " , Msg : " + e.getMessage());

		response.setExitCode( e.getExitCode() );
		response.setMessage( e.getMessage() );
	}
	catch ( InvalidVinException e )
	{
		logger.warn("An unknown VIN was submitted to the Appraisal web service.");
		logger.warn("Exit Code : " + ExitCode.INVALID_VIN.value() + " , Msg : " + e.getMessage());

		response.setExitCode( ExitCode.INVALID_VIN );
		response.setMessage( e.getMessage() );
	}
	
	return response;
}

private PotentialDeal createPotentialDeal(Data data) {
	PotentialDeal potentialDeal = new PotentialDeal();
	Person salesPerson = new Person();
	salesPerson.setFirstName(data.getCreatedByFName());
	salesPerson.setLastName(data.getCreatedByLName());
	potentialDeal.setPerson(salesPerson);
	return potentialDeal;
}

protected Customer createCustomer(Data data) {
	Customer customer = new Customer();
	Person person = new Person();
	person.setFirstName(data.getCustFName());
	person.setLastName(data.getCustLName());
	customer.setPerson(person);
	customer.setPhoneNumber(data.getCustPhone());
	customer.setEmail(data.getCustEmail());
	return customer;
}

}

