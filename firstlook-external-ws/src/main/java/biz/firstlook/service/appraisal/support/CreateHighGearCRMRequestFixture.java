package biz.firstlook.service.appraisal.support;

import javax.xml.datatype.DatatypeConfigurationException;

import biz.firstlook.service.appraisal.highergear.AddAppraisal;
import biz.firstlook.service.appraisal.highergear.Data;

public class CreateHighGearCRMRequestFixture {
	
	public static AddAppraisal getCreateFullAppraisalRequest() throws DatatypeConfigurationException {

		AddAppraisal appraisalRequest = new AddAppraisal();
		appraisalRequest.setData(new Data());
		appraisalRequest.getData().setVin("19UYA1254VL007779");
		appraisalRequest.getData().setCompanyCode("WINDYCIT01");
		appraisalRequest.getData().setCustFName("John");
		appraisalRequest.getData().setCustLName("Doe");
		appraisalRequest.getData().setCustEmail("john@email.com");
		appraisalRequest.getData().setCreatedByFName("joe");
		appraisalRequest.getData().setCreatedByLName("salesperson");
		appraisalRequest.getData().setLoginName("HIGHERGEAR");
		appraisalRequest.getData().setMileage("24341.0");
		appraisalRequest.getData().setTradeExteriorColor("Beige");
			
		return appraisalRequest;
	}	
}
