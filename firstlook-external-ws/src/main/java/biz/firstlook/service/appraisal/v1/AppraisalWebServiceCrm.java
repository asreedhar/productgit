
package biz.firstlook.service.appraisal.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import biz.firstlook.service.appraisal.CreateAppraisalRequest;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;

@WebService(name = "AppraisalWebServiceCrm", targetNamespace = "http://www.firstlook.biz/service/appraisal/v1")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AppraisalWebServiceCrm {


    @WebMethod(operationName = "createAppraisal")
    @WebResult(name = "createAppraisalResponse", targetNamespace = "http://www.firstlook.biz/service/appraisal")
    public CreateAppraisalResponse createAppraisal(
        @WebParam(name = "createAppraisalRequest", targetNamespace = "http://www.firstlook.biz/service/appraisal")
        CreateAppraisalRequest createAppraisalRequest);

}
