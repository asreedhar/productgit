package biz.firstlook.service.appraisal.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import biz.firstlook.service.appraisal.BookOption;
import biz.firstlook.service.appraisal.BookValue;
import biz.firstlook.service.appraisal.BookValueType;
import biz.firstlook.service.appraisal.Bookout;
import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOutOption;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOutValue;

public class StagingBookOutUtility
{

public static ArrayList< StagingBookOut > createStagingBookOuts( CreateFullAppraisalRequest createFullAppraisalRequest ) throws WebServiceException
{
	ArrayList< StagingBookOut > bookOuts = new ArrayList<StagingBookOut>();
	for ( Bookout bookout : createFullAppraisalRequest.getBookout() )
	{
		StagingBookOut stagingBookOut = new StagingBookOut();
		ThirdPartyDataProvider dataProvider = null;
		switch ( bookout.getBookId() )
		{
			case BLACKBOOK:
				dataProvider = ThirdPartyDataProvider.BLACKBOOK;
				break;
			case NADA:
				dataProvider = ThirdPartyDataProvider.NADA;
				break;
			case KELLEY:
				dataProvider = ThirdPartyDataProvider.KELLEY;
				break;
			case GALVES:
				dataProvider = ThirdPartyDataProvider.GALVES;
				break;
			default:
				throw new WebServiceException( ExitCode.MISSING_REQUIRED_ATTRIBUTES, "An unknown bookId was specified for this appraisal." );
		}
		stagingBookOut.setThirdPartyDataProvider( dataProvider );
		populateBookValues( dataProvider, bookout, stagingBookOut );
		populateStagingBookOutOptions( dataProvider, bookout, stagingBookOut );

		stagingBookOut.setThirdPartyVehicleCode( bookout.getStyleKey() );
		bookOuts.add( stagingBookOut );
	}
	return bookOuts;
}

/**
 * Translates all the bookOutOptions in the incoming bookOut to StagingBookOutOptions and sets the container on the stagingBookout for
 * reference.
 * 
 * @param dataProvider
 * 
 * @param bookout
 * @param stagingBookOut
 * @throws WebServiceException 
 */
static void populateStagingBookOutOptions( ThirdPartyDataProvider dataProvider, Bookout bookout, StagingBookOut stagingBookOut ) throws WebServiceException
{
	Set< StagingBookOutOption > options = new HashSet< StagingBookOutOption >();
	for ( BookOption option : bookout.getBookOption() )
	{
		StagingBookOutOption stagingBookOutOption = new StagingBookOutOption();
		stagingBookOutOption.setIsSelected( option.isSelected() );
		stagingBookOutOption.setOptionKey( option.getKey() );
		stagingBookOutOption.setOptionName( option.getDescription() );
		stagingBookOutOption.setValue( option.getValue().intValue() );
		stagingBookOutOption.setThirdPartyOptionType( getOptionType( dataProvider, option ) );
		if (option.getValueType() != null) {
			stagingBookOutOption.setThirdPartyCategory( translateThirdPartyCategory( dataProvider, option.getValueType() ) );
		}
		stagingBookOutOption.setStagingBookOut( stagingBookOut );
		options.add(stagingBookOutOption);
	}
	stagingBookOut.setOptions( options );
}

/**
 * Gets the sent optionType.
 * 
 * If Kelley, it sets the corresponding ThirdPartyOptionType If not Kelley, everything gets set to an equipment option by default.
 * 
 * @param dataProvider
 * @param option
 * @return
 */
static ThirdPartyOptionType getOptionType( ThirdPartyDataProvider dataProvider, BookOption option )
{
	ThirdPartyOptionType optionType = null;
	// Option type is not required
	if (option == null || option.getOptionType() == null) return ThirdPartyOptionType.EQUIPMENT;
	switch ( option.getOptionType() )
	{
		case DRIVE_TRAIN:
			optionType = ThirdPartyOptionType.DRIVETRAIN;
			break;
		case ENGINE:
			optionType = ThirdPartyOptionType.ENGINE;
			break;
		case EQUIPMENT:
			optionType = ThirdPartyOptionType.EQUIPMENT;
			break;
		case TRANSMISSION:
			optionType = ThirdPartyOptionType.TRANSMISSION;
			break;
		default:
			optionType = ThirdPartyOptionType.EQUIPMENT;
			break;
	}
	return optionType;
}

/**
 * Populates the stagingBookOuts bookout values. Method will throw an exception if the BookValueType does not match the given dataProvider.
 * 
 * @param dataProvider
 * @param incomingBookout
 * @param stagingBookOut
 * @throws WebServiceException
 */
static void populateBookValues( ThirdPartyDataProvider dataProvider, Bookout incomingBookout, StagingBookOut stagingBookOut )
		throws WebServiceException
{
	Set< StagingBookOutValue > allBookValues = new HashSet< StagingBookOutValue >();
	for ( BookValue value : incomingBookout.getBookValue() )
	{
		StagingBookOutValue bookOutValue = new StagingBookOutValue();
		bookOutValue.setThirdPartyCategory( translateThirdPartyCategory( dataProvider, value.getValueType() ) );
		bookOutValue.setBaseValue( value.getBaseValue().intValue() );
		bookOutValue.setFinalValue( value.getFinalValue().intValue() );
		bookOutValue.setMileageAdjustment( value.getMileageAdjustment().intValue() );
		bookOutValue.setStagingBookOut( stagingBookOut );
		allBookValues.add( bookOutValue );
	}
	stagingBookOut.setValues( allBookValues );
}

static ThirdPartyCategory translateThirdPartyCategory( ThirdPartyDataProvider dataProvider, BookValueType valueType )
		throws WebServiceException
{
	ThirdPartyCategory result = null;
	switch ( dataProvider.getThirdPartyId() )
	{
		case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE:
			switch ( valueType )
			{
				case BLACKBOOK_AVERAGE:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE );
					break;
				case BLACKBOOK_CLEAN:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE );
					break;
				case BLACKBOOK_ROUGH:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_ROUGH_TYPE );
					break;
				case BLACKBOOK_EXTRA_CLEAN:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.BLACKBOOK_EXTRACLEAN_TYPE );
					break;
			}
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE:
			switch ( valueType )
			{
				case NADA_LOAN:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE );
					break;
				case NADA_RETAIL:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE );
					break;
				case NADA_TRADE_IN:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE );
					break;
			}
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE:
			switch ( valueType )
			{
				case KELLEY_RETAIL:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.KELLEY_RETAIL_TYPE );
					break;
				case KELLEY_WHOLESALE:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.KELLEY_WHOLESALE_TYPE );
			}
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
			switch ( valueType )
			{
				case GALVES_TRADE_IN:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.GALVES_TRADEIN_TYPE);
					break;
				case GALVES_MARKET_READY:
					result = ThirdPartyCategory.fromId( ThirdPartyCategory.GALVES_MARKETREADY_TYPE);
					
			}
			break;			
	}

	if ( result == null )
		throw new WebServiceException(	ExitCode.BOOK_TYPE_AND_BOOK_VALUE_TYPE_DO_NOT_MATCH,
										"The specified BookType and BookValue Types do not match." );

	return result;
}

}
