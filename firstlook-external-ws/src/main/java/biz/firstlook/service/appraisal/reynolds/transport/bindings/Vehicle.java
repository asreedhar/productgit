
package biz.firstlook.service.appraisal.reynolds.transport.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="CompanyCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LenderAccount" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LenderName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LenderPhone" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LicensePlate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Mileage" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PayoffAmount" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PlateRegistrationState" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RegistrationNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TradeExteriorColor" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TradeInteriorColor" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="VIN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Vehicle")
public class Vehicle {

    @XmlAttribute(name = "CompanyCode")
    protected String companyCode;
    @XmlAttribute(name = "LenderAccount")
    protected String lenderAccount;
    @XmlAttribute(name = "LenderName")
    protected String lenderName;
    @XmlAttribute(name = "LenderPhone")
    protected String lenderPhone;
    @XmlAttribute(name = "LicensePlate")
    protected String licensePlate;
    @XmlAttribute(name = "Mileage", required = true)
    protected String mileage;
    @XmlAttribute(name = "PayoffAmount")
    protected String payoffAmount;
    @XmlAttribute(name = "PlateRegistrationState")
    protected String plateRegistrationState;
    @XmlAttribute(name = "RegistrationNumber")
    protected String registrationNumber;
    @XmlAttribute(name = "TradeExteriorColor")
    protected String tradeExteriorColor;
    @XmlAttribute(name = "TradeInteriorColor")
    protected String tradeInteriorColor;
    @XmlAttribute(name = "VIN", required = true)
    protected String vin;

    /**
     * Gets the value of the companyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * Sets the value of the companyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCode(String value) {
        this.companyCode = value;
    }

    /**
     * Gets the value of the lenderAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLenderAccount() {
        return lenderAccount;
    }

    /**
     * Sets the value of the lenderAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLenderAccount(String value) {
        this.lenderAccount = value;
    }

    /**
     * Gets the value of the lenderName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLenderName() {
        return lenderName;
    }

    /**
     * Sets the value of the lenderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLenderName(String value) {
        this.lenderName = value;
    }

    /**
     * Gets the value of the lenderPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLenderPhone() {
        return lenderPhone;
    }

    /**
     * Sets the value of the lenderPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLenderPhone(String value) {
        this.lenderPhone = value;
    }

    /**
     * Gets the value of the licensePlate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     * Sets the value of the licensePlate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlate(String value) {
        this.licensePlate = value;
    }

    /**
     * Gets the value of the mileage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileage() {
        return mileage;
    }

    /**
     * Sets the value of the mileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileage(String value) {
        this.mileage = value;
    }

    /**
     * Gets the value of the payoffAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayoffAmount() {
        return payoffAmount;
    }

    /**
     * Sets the value of the payoffAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayoffAmount(String value) {
        this.payoffAmount = value;
    }

    /**
     * Gets the value of the plateRegistrationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlateRegistrationState() {
        return plateRegistrationState;
    }

    /**
     * Sets the value of the plateRegistrationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlateRegistrationState(String value) {
        this.plateRegistrationState = value;
    }

    /**
     * Gets the value of the registrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * Sets the value of the registrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationNumber(String value) {
        this.registrationNumber = value;
    }

    /**
     * Gets the value of the tradeExteriorColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeExteriorColor() {
        return tradeExteriorColor;
    }

    /**
     * Sets the value of the tradeExteriorColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeExteriorColor(String value) {
        this.tradeExteriorColor = value;
    }

    /**
     * Gets the value of the tradeInteriorColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeInteriorColor() {
        return tradeInteriorColor;
    }

    /**
     * Sets the value of the tradeInteriorColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeInteriorColor(String value) {
        this.tradeInteriorColor = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIN() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIN(String value) {
        this.vin = value;
    }

}
