
package biz.firstlook.service.appraisal;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appraisalCreateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="alertUCM" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}credentials"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}vehicle" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}potentialDeal" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}customer" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}appraisal" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}report" maxOccurs="2" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}bookout" maxOccurs="3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "appraisalCreateTime",
    "alertUCM",
    "credentials",
    "vehicle",
    "potentialDeal",
    "customer",
    "appraisal",
    "report",
    "bookout"
})
@XmlRootElement(name = "createFullAppraisalRequest")
public class CreateFullAppraisalRequest {

    @XmlElement(required = true)
    protected XMLGregorianCalendar appraisalCreateTime;
    protected Boolean alertUCM;
    @XmlElement(required = true)
    protected Credentials credentials;
    protected Vehicle vehicle;
    protected PotentialDeal potentialDeal;
    protected Customer customer;
    protected Appraisal appraisal;
    @XmlElement(required = true)
    protected List<Report> report;
    @XmlElement(required = true)
    protected List<Bookout> bookout;

    /**
     * Gets the value of the appraisalCreateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAppraisalCreateTime() {
        return appraisalCreateTime;
    }

    /**
     * Sets the value of the appraisalCreateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAppraisalCreateTime(XMLGregorianCalendar value) {
        this.appraisalCreateTime = value;
    }

    /**
     * Gets the value of the alertUCM property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertUCM() {
        return alertUCM;
    }

    /**
     * Sets the value of the alertUCM property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertUCM(Boolean value) {
        this.alertUCM = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link Credentials }
     *     
     */
    public Credentials getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link Credentials }
     *     
     */
    public void setCredentials(Credentials value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the potentialDeal property.
     * 
     * @return
     *     possible object is
     *     {@link PotentialDeal }
     *     
     */
    public PotentialDeal getPotentialDeal() {
        return potentialDeal;
    }

    /**
     * Sets the value of the potentialDeal property.
     * 
     * @param value
     *     allowed object is
     *     {@link PotentialDeal }
     *     
     */
    public void setPotentialDeal(PotentialDeal value) {
        this.potentialDeal = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link Customer }
     *     
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer }
     *     
     */
    public void setCustomer(Customer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the appraisal property.
     * 
     * @return
     *     possible object is
     *     {@link Appraisal }
     *     
     */
    public Appraisal getAppraisal() {
        return appraisal;
    }

    /**
     * Sets the value of the appraisal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Appraisal }
     *     
     */
    public void setAppraisal(Appraisal value) {
        this.appraisal = value;
    }

    /**
     * Gets the value of the report property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the report property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Report }
     * 
     * 
     */
    public List<Report> getReport() {
        if (report == null) {
            report = new ArrayList<Report>();
        }
        return this.report;
    }

    /**
     * Gets the value of the bookout property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookout property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookout().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Bookout }
     * 
     * 
     */
    public List<Bookout> getBookout() {
        if (bookout == null) {
            bookout = new ArrayList<Bookout>();
        }
        return this.bookout;
    }

}
