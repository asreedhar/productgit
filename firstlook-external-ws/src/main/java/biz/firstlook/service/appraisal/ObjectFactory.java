
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.service.appraisal package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.service.appraisal
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BookValue }
     * 
     */
    public BookValue createBookValue() {
        return new BookValue();
    }

    /**
     * Create an instance of {@link Report }
     * 
     */
    public Report createReport() {
        return new Report();
    }
    
    /**
     * Create an instance of {@link CreateFullAppraisalResponse }
     * 
     */
    public CreateAppraisalResponse createCreateAppraisalResponse() {
        return new CreateAppraisalResponse();
    }
    

    /**
     * Create an instance of {@link CreateFullAppraisalResponse }
     * 
     */
    public CreateFullAppraisalResponse createCreateFullAppraisalResponse() {
        return new CreateFullAppraisalResponse();
    }

    /**
     * Create an instance of {@link PotentialDeal }
     * 
     */
    public PotentialDeal createPotentialDeal() {
        return new PotentialDeal();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link CreateFullAppraisalRequest }
     * 
     */
    public CreateFullAppraisalRequest createCreateFullAppraisalRequest() {
        return new CreateFullAppraisalRequest();
    }

    /**
     * Create an instance of {@link CreateAppraisalRequest }
     * 
     */
    public CreateAppraisalRequest createCreateAppraisalRequest() {
        return new CreateAppraisalRequest();
    }

    /**
     * Create an instance of {@link Bookout }
     * 
     */
    public Bookout createBookout() {
        return new Bookout();
    }

    /**
     * Create an instance of {@link Vehicle }
     * 
     */
    public Vehicle createVehicle() {
        return new Vehicle();
    }

    /**
     * Create an instance of {@link BookOption }
     * 
     */
    public BookOption createBookOption() {
        return new BookOption();
    }

    /**
     * Create an instance of {@link Appraisal }
     * 
     */
    public Appraisal createAppraisal() {
        return new Appraisal();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

}
