
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}person" minOccurs="0"/>
 *         &lt;element name="newOrUsedDeal" type="{http://www.firstlook.biz/service/appraisal}dealType" minOccurs="0"/>
 *         &lt;element name="stockNumber" type="{http://www.firstlook.biz/service/appraisal}stockNumber" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "person",
    "newOrUsedDeal",
    "stockNumber"
})
@XmlRootElement(name = "potentialDeal")
public class PotentialDeal {

    protected Person person;
    protected DealType newOrUsedDeal;
    protected String stockNumber;

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link Person }
     *     
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link Person }
     *     
     */
    public void setPerson(Person value) {
        this.person = value;
    }

    /**
     * Gets the value of the newOrUsedDeal property.
     * 
     * @return
     *     possible object is
     *     {@link DealType }
     *     
     */
    public DealType getNewOrUsedDeal() {
        return newOrUsedDeal;
    }

    /**
     * Sets the value of the newOrUsedDeal property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealType }
     *     
     */
    public void setNewOrUsedDeal(DealType value) {
        this.newOrUsedDeal = value;
    }

    /**
     * Gets the value of the stockNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockNumber() {
        return stockNumber;
    }

    /**
     * Sets the value of the stockNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockNumber(String value) {
        this.stockNumber = value;
    }

}
