
package biz.firstlook.service.appraisal.highergear;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import biz.firstlook.service.appraisal.CreateAppraisalResponse;

@WebService(name = "HigherGearWebServiceCrm", targetNamespace = "http://www.firstlook.biz/service/appraisal/highergear")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface HigherGearWebServiceCrm {


    @WebMethod(operationName = "createAppraisal")
    @WebResult(name = "createAppraisalResponse", targetNamespace = "http://www.firstlook.biz/service/appraisal")
    public CreateAppraisalResponse createAppraisal(
        @WebParam(name = "addAppraisal", targetNamespace = "http://www.firstlook.biz/service/appraisal/highergear")
        AddAppraisal addAppraisal);

}
