
package biz.firstlook.service.appraisal.reynolds.transport.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DestinationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DestinationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DestinationNameCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DestinationURI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestinationSoftwareCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestinationSoftware" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}DealerNumber" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}StoreNumber" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}AreaNumber" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}DealerCountry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DestinationType", propOrder = {
    "destinationNameCode",
    "destinationURI",
    "destinationSoftwareCode",
    "destinationSoftware",
    "dealerNumber",
    "storeNumber",
    "areaNumber",
    "dealerCountry"
})
public class DestinationType {

    @XmlElement(name = "DestinationNameCode", required = true)
    protected String destinationNameCode;
    @XmlElement(name = "DestinationURI")
    protected String destinationURI;
    @XmlElement(name = "DestinationSoftwareCode")
    protected String destinationSoftwareCode;
    @XmlElement(name = "DestinationSoftware")
    protected String destinationSoftware;
    @XmlElement(name = "DealerNumber")
    protected String dealerNumber;
    @XmlElement(name = "StoreNumber")
    protected String storeNumber;
    @XmlElement(name = "AreaNumber")
    protected String areaNumber;
    @XmlElement(name = "DealerCountry")
    protected String dealerCountry;

    /**
     * Gets the value of the destinationNameCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationNameCode() {
        return destinationNameCode;
    }

    /**
     * Sets the value of the destinationNameCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationNameCode(String value) {
        this.destinationNameCode = value;
    }

    /**
     * Gets the value of the destinationURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationURI() {
        return destinationURI;
    }

    /**
     * Sets the value of the destinationURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationURI(String value) {
        this.destinationURI = value;
    }

    /**
     * Gets the value of the destinationSoftwareCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationSoftwareCode() {
        return destinationSoftwareCode;
    }

    /**
     * Sets the value of the destinationSoftwareCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationSoftwareCode(String value) {
        this.destinationSoftwareCode = value;
    }

    /**
     * Gets the value of the destinationSoftware property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationSoftware() {
        return destinationSoftware;
    }

    /**
     * Sets the value of the destinationSoftware property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationSoftware(String value) {
        this.destinationSoftware = value;
    }

    /**
     * Gets the value of the dealerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerNumber() {
        return dealerNumber;
    }

    /**
     * Sets the value of the dealerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerNumber(String value) {
        this.dealerNumber = value;
    }

    /**
     * Gets the value of the storeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * Sets the value of the storeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreNumber(String value) {
        this.storeNumber = value;
    }

    /**
     * Gets the value of the areaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaNumber() {
        return areaNumber;
    }

    /**
     * Sets the value of the areaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaNumber(String value) {
        this.areaNumber = value;
    }

    /**
     * Gets the value of the dealerCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCountry() {
        return dealerCountry;
    }

    /**
     * Sets the value of the dealerCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCountry(String value) {
        this.dealerCountry = value;
    }

}
