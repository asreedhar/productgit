
package biz.firstlook.service.appraisal.reynolds.transport.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="CustAddress" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustCity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustEmail" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustFName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustLName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustMInitial" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustPhone" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustState" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CustZip" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SSN" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Customer")
public class Customer {

    @XmlAttribute(name = "CustAddress")
    protected String custAddress;
    @XmlAttribute(name = "CustCity")
    protected String custCity;
    @XmlAttribute(name = "CustEmail")
    protected String custEmail;
    @XmlAttribute(name = "CustFName")
    protected String custFName;
    @XmlAttribute(name = "CustLName")
    protected String custLName;
    @XmlAttribute(name = "CustMInitial")
    protected String custMInitial;
    @XmlAttribute(name = "CustPhone")
    protected String custPhone;
    @XmlAttribute(name = "CustState")
    protected String custState;
    @XmlAttribute(name = "CustZip")
    protected String custZip;
    @XmlAttribute(name = "SSN")
    protected String ssn;

    /**
     * Gets the value of the custAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustAddress() {
        return custAddress;
    }

    /**
     * Sets the value of the custAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustAddress(String value) {
        this.custAddress = value;
    }

    /**
     * Gets the value of the custCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustCity() {
        return custCity;
    }

    /**
     * Sets the value of the custCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustCity(String value) {
        this.custCity = value;
    }

    /**
     * Gets the value of the custEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustEmail() {
        return custEmail;
    }

    /**
     * Sets the value of the custEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustEmail(String value) {
        this.custEmail = value;
    }

    /**
     * Gets the value of the custFName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustFName() {
        return custFName;
    }

    /**
     * Sets the value of the custFName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustFName(String value) {
        this.custFName = value;
    }

    /**
     * Gets the value of the custLName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLName() {
        return custLName;
    }

    /**
     * Sets the value of the custLName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLName(String value) {
        this.custLName = value;
    }

    /**
     * Gets the value of the custMInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustMInitial() {
        return custMInitial;
    }

    /**
     * Sets the value of the custMInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustMInitial(String value) {
        this.custMInitial = value;
    }

    /**
     * Gets the value of the custPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPhone() {
        return custPhone;
    }

    /**
     * Sets the value of the custPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPhone(String value) {
        this.custPhone = value;
    }

    /**
     * Gets the value of the custState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustState() {
        return custState;
    }

    /**
     * Sets the value of the custState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustState(String value) {
        this.custState = value;
    }

    /**
     * Gets the value of the custZip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustZip() {
        return custZip;
    }

    /**
     * Sets the value of the custZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustZip(String value) {
        this.custZip = value;
    }

    /**
     * Gets the value of the ssn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSN() {
        return ssn;
    }

    /**
     * Sets the value of the ssn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSN(String value) {
        this.ssn = value;
    }

}
