
package biz.firstlook.service.appraisal.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;
import biz.firstlook.service.appraisal.CreateFullAppraisalResponse;

@WebService(name = "AppraisalWebServiceWireless", targetNamespace = "http://www.firstlook.biz/service/appraisal/v1")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AppraisalWebServiceWireless {


    @WebMethod(operationName = "createFullAppraisal", action = "https://preprod.firstlook.biz/firstlook-external-ws/services/AppraisalWebServiceWireless/CreateFullAppraisal")
    @WebResult(name = "createFullAppraisalResponse", targetNamespace = "http://www.firstlook.biz/service/appraisal")
    public CreateFullAppraisalResponse createFullAppraisal(
        @WebParam(name = "createFullAppraisalRequest", targetNamespace = "http://www.firstlook.biz/service/appraisal")
        CreateFullAppraisalRequest createFullAppraisalRequest);

}
