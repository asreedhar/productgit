
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlEnum;


/**
 * <p>Java class for dealType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="dealType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NEW"/>
 *     &lt;enumeration value="USED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum DealType {

    NEW,
    USED;

    public String value() {
        return name();
    }

    public static DealType fromValue(String v) {
        return valueOf(v);
    }

}
