package biz.firstlook.service.appraisal.v1;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.service.appraisal.Appraisal;
import biz.firstlook.service.appraisal.CreateFullAppraisalRequest;
import biz.firstlook.service.appraisal.CreateFullAppraisalResponse;
import biz.firstlook.service.appraisal.Customer;
import biz.firstlook.service.appraisal.DealType;
import biz.firstlook.service.appraisal.ExitCode;
import biz.firstlook.service.appraisal.Gender;
import biz.firstlook.service.appraisal.Person;
import biz.firstlook.service.appraisal.PotentialDeal;
import biz.firstlook.service.appraisal.support.CustomerIPersonAdapter;
import biz.firstlook.service.appraisal.support.DealTrackIDealAdapter;
import biz.firstlook.service.appraisal.support.StagingBookOutUtility;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

@WebService( serviceName = "AppraisalWebServiceWireless", targetNamespace = "http://www.firstlook.biz/service/appraisal/v1", endpointInterface = "biz.firstlook.service.appraisal.v1.AppraisalWebServiceWireless" )
public class AppraisalWebServiceWirelessImpl extends AbstractAppraisalWebService implements AppraisalWebServiceWireless
{

protected static final Logger logger = Logger.getLogger( AppraisalWebServiceWirelessImpl.class );

private static final String MISSING_XML_ELEMENT_MSG = "The {0} xml element seems to be missing or malformed.";

public CreateFullAppraisalResponse createFullAppraisal( CreateFullAppraisalRequest createFullAppraisalRequest )
{

	CreateFullAppraisalResponse response = new CreateFullAppraisalResponse();
	response.setExitCode( ExitCode.SUCCESS );
	response.setMessage( ExitCode.SUCCESS.toString() );

	try
	{
		logger.info("Processing a FullAppraisalRequest from Partner: " + createFullAppraisalRequest.getCredentials().getPartnerId() );
		
		// authenticate
		BusinessUnitCredential businessUnitCredential = authenticate( createFullAppraisalRequest.getCredentials() );
		
		checkRejectConditions(businessUnitCredential.getBusinessUnitId(), createFullAppraisalRequest.getVehicle().getVin(), AppraisalSource.WAVIS);
		
		// create appraisal
		createAppraisal( createFullAppraisalRequest, businessUnitCredential );
	} catch ( WebServiceException e ) {
		logger.warn("A web service expection was thrown when attempting to submit a Full Appraisal.");
		logger.warn("Exit Code : " + e.getExitCode().value() + " , Msg : " + e.getMessage());
		response.setExitCode( e.getExitCode() );
		response.setMessage( e.getMessage() );
		return response;
	} catch ( InvalidVinException e ) {
		logger.warn("An unknown VIN was submitted to the Appraisal web service.");
		logger.warn("Exit Code : " + ExitCode.INVALID_VIN.value() + " , Msg : " + e.getMessage());
		response.setExitCode( ExitCode.INVALID_VIN );
		response.setMessage( e.getMessage() );
		return response;
	} catch ( NumberFormatException nfe ) {   //usually due to bad credentials
		logger.warn("An error occured parsing the submitted appraisal. Please check your Credentials and the contents of your appraisal.");
		logger.warn("Exit Code : " + ExitCode.AUTHENTICATION_FAILED.value() + " , Msg : " + nfe.getMessage());
		response.setExitCode( ExitCode.AUTHENTICATION_FAILED );
		response.setMessage( nfe.getMessage() );
		return response;
	} catch (Exception e) {
		
	}

	return response;

}

/**
 * Creates a new Appraisal with the data submitted to the service
 * 
 * @param createAppraisalRequest
 * @param businessUnitCredential
 * @throws InvalidVinException
 */
private void createAppraisal( CreateFullAppraisalRequest createAppraisalRequest, BusinessUnitCredential businessUnitCredential )
		throws InvalidVinException, WebServiceException
{
	Integer businessUnitId = businessUnitCredential.getBusinessUnitId();
	
	//do some quick and dirty validation
	validate(createAppraisalRequest);
	
	String vin = createAppraisalRequest.getVehicle().getVin();
	DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(businessUnitId);
	String color = createAppraisalRequest.getVehicle().getColor();
	// set defaults for dealTrack and customer based on incoming content
	DealTrackIDealAdapter dealTrack = null;
	if ( createAppraisalRequest.getPotentialDeal() != null )
		dealTrack = new DealTrackIDealAdapter( createAppraisalRequest.getPotentialDeal() );
	CustomerIPersonAdapter customer = null;
	if ( createAppraisalRequest.getCustomer() != null )
		customer = new CustomerIPersonAdapter( createAppraisalRequest.getCustomer() );
	
	// update deal, customer, notes, from a matching CRM appraisal if it exists
	List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin, dealerPreference.getSearchInactiveInventoryDaysBackThreshold());
	String notes = createAppraisalRequest.getVehicle().getNotes();
	if (appraisals != null && !appraisals.isEmpty()) {
		// get appraisal - there should only be one
		biz.firstlook.transact.persist.service.appraisal.IAppraisal appraisal = (IAppraisal)appraisals.get(0);
		if (appraisal.getAppraisalSource().equals(AppraisalSource.AUTOBASE)) {
			if (createAppraisalRequest.getPotentialDeal() == null) {
				PotentialDeal crmDeal = new PotentialDeal();
				if (appraisal.getDealTrackNewOrUsed() != null && appraisal.getDealTrackNewOrUsed().equals(InventoryTypeEnum.NEW) ) {
					crmDeal.setNewOrUsedDeal(DealType.NEW);
				} else {
					crmDeal.setNewOrUsedDeal(DealType.USED);
				}
				if (appraisal.getDealTrackSalesPerson() != null) {
					Person personCopy = new Person();
					personCopy.setFirstName(appraisal.getDealTrackSalesPerson().getFirstName());
					personCopy.setLastName(appraisal.getDealTrackSalesPerson().getLastName());
					crmDeal.setPerson(personCopy);
				}
				crmDeal.setStockNumber(appraisal.getDealTrackStockNumber());
				dealTrack = new DealTrackIDealAdapter( crmDeal );
			}
			if ( createAppraisalRequest.getCustomer() == null) {
				Person crmPersom = new Person();
				if (appraisal.getCustomer() != null) {
					crmPersom.setFirstName(appraisal.getCustomer().getFirstName());
					crmPersom.setLastName(appraisal.getCustomer().getLastName());
					Customer crmCutomer = new Customer();
					crmCutomer.setPerson(crmPersom);
					crmCutomer.setEmail(appraisal.getCustomer().getEmail());
					if (appraisal.getCustomer().getGender().equalsIgnoreCase("m") ) {
						crmCutomer.setGender(Gender.M);
					} else {
						crmCutomer.setGender(Gender.F);
					}
					crmCutomer.setPhoneNumber(appraisal.getCustomer().getPhoneNumber());
					customer = new CustomerIPersonAdapter( crmCutomer);
				}
				
			}
			if (notes == null || notes.trim().length() == 0) {
				notes = appraisal.getNotes();
			}
			if (color == null || color.trim().length() == 0) {
				color = appraisal.getColor();
			}
			
			// finally delete the CRM apparaisal - we will be replacing it with a new wireless one
			getAppraisalService().deleteAppraisal(appraisal);
		} else {
			// should not happen
			throw new WebServiceException( ExitCode.CONFLICTS_WITH_EXISTING_APPRAISAL, "An error occured while trying to merge appraisal info");
		}
	} 
	
	logger.info("Looking up vehicle for VIN : " + vin);
	Vehicle vehicle;
	try {
		vehicle = getVehicleService().identifyVehicle( vin );
	} catch (VehicleCatalogServiceException e) {
		throw new WebServiceException(ExitCode.INVALID_VIN, "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog " );
	}
	vehicle.setBaseColor( color );

	List< StagingBookOut > bookOuts = StagingBookOutUtility.createStagingBookOuts( createAppraisalRequest );

	Dealer dealer = getDealerService().findByDealerId( businessUnitId );
	
	//clean this up in a bit. -bf.
	Appraisal appraisal = createAppraisalRequest.getAppraisal();
	Double estimateRecon = null;
	String conditionDescription = null;
	AppraisalValue value = null;
	
	if( appraisal != null )
	{
		if( appraisal.getEstimatedRecon() != null )
			estimateRecon = Double.parseDouble( appraisal.getEstimatedRecon().toString() );
		
		conditionDescription = appraisal.getConditionDescription();
		
		Person person = appraisal.getPerson();
		String firstName = "";
		String lastName = "";
		if (person != null) {
			firstName = person.getFirstName();
			lastName = person.getLastName();
		}
		value = getAppraisalFactory().createAppraisalValue( appraisal.getAppraisalAmount().intValue(), firstName + " " + lastName );
	}
	
	
	
	// nk - note, all wireless appraisals are currently purchase ONLY
	getAppraisalService().createAppraisal( createAppraisalRequest.getAppraisalCreateTime().toGregorianCalendar().getTime(), dealer, vehicle,
										createAppraisalRequest.getVehicle().getMileage().intValue(), estimateRecon, null, conditionDescription, null , value, dealTrack, customer,
										notes, AppraisalSource.WAVIS, 
										new HashSet< StagingBookOut >( bookOuts ), AppraisalTypeEnum.TRADE_IN, null );
}

//JAXB is lame since the @XMLElment(required=true) doesn't perform validation during runtime for performance reasons.
private void validate(CreateFullAppraisalRequest createAppraisalRequest) throws WebServiceException {
	
	if(createAppraisalRequest == null) {
		throw new WebServiceException(ExitCode.SCHEMA_INVALID, MessageFormat.format(MISSING_XML_ELEMENT_MSG, "createAppraisalRequest"));
	}
	
	biz.firstlook.service.appraisal.Vehicle v = createAppraisalRequest.getVehicle();
	if(v == null) {
		throw new WebServiceException(ExitCode.SCHEMA_INVALID, MessageFormat.format(MISSING_XML_ELEMENT_MSG, "vehicle"));
	}
	
	String vin = v.getVin();
	if(vin == null) {
		throw new WebServiceException(ExitCode.INVALID_VIN, MessageFormat.format(MISSING_XML_ELEMENT_MSG, "vin"));
	}
	
	BigInteger mileage = v.getMileage();
	if(mileage == null) {
		throw new WebServiceException(ExitCode.MISSING_REQUIRED_ATTRIBUTES, 
			"The mileage xml element seems to be missing or malformed.  Check that the value does not contain non numeric characters, as specified by XMLSchema from the wsdl.");
	}
}

}
