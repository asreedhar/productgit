
package biz.firstlook.service.appraisal;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bookId" type="{http://www.firstlook.biz/service/appraisal}book"/>
 *         &lt;element name="styleKey" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}bookOption" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/appraisal}bookValue" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookId",
    "styleKey",
    "bookOption",
    "bookValue"
})
@XmlRootElement(name = "bookout")
public class Bookout {

    @XmlElement(required = true)
    protected Book bookId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String styleKey;
    @XmlElement(required = true)
    protected List<BookOption> bookOption;
    @XmlElement(required = true)
    protected List<BookValue> bookValue;

    /**
     * Gets the value of the bookId property.
     * 
     * @return
     *     possible object is
     *     {@link Book }
     *     
     */
    public Book getBookId() {
        return bookId;
    }

    /**
     * Sets the value of the bookId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Book }
     *     
     */
    public void setBookId(Book value) {
        this.bookId = value;
    }

    /**
     * Gets the value of the styleKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStyleKey() {
        return styleKey;
    }

    /**
     * Sets the value of the styleKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStyleKey(String value) {
        this.styleKey = value;
    }

    /**
     * Gets the value of the bookOption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookOption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookOption }
     * 
     * 
     */
    public List<BookOption> getBookOption() {
        if (bookOption == null) {
            bookOption = new ArrayList<BookOption>();
        }
        return this.bookOption;
    }

    /**
     * Gets the value of the bookValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookValue }
     * 
     * 
     */
    public List<BookValue> getBookValue() {
        if (bookValue == null) {
            bookValue = new ArrayList<BookValue>();
        }
        return this.bookValue;
    }

}
