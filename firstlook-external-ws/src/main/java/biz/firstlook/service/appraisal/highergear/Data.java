package biz.firstlook.service.appraisal.highergear;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vin",
    "mileage",
    "custFName",
    "custMInitial",
    "custLName",
    "custAddress",
    "custCity",
    "custState",
    "custZip",
    "custPhone",
    "custEmail",
    "tradeExteriorColor",
    "ssn",
    "payoffAmount",
    "lenderName",
    "companyCode",
    "createdByFName",
    "createdByLName",
    "createdByUserName",
    "loginName"
})
@XmlRootElement(name = "data")

public class Data {

	private String vin;
	private String mileage;
	private String custFName;
	private String custMInitial;
	private String custLName;
	private String custAddress;
	private String custCity;
	private String custState;
	private String custZip;
	private String custPhone;
	private String custEmail;
	private String tradeExteriorColor;
	private String ssn;
	private String payoffAmount;
	private String lenderName;
	private String companyCode;
	private String createdByFName;
	private String createdByLName;
	private String createdByUserName;
	private String loginName;
	
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCreatedByFName() {
		return createdByFName;
	}
	public void setCreatedByFName(String createdByFName) {
		this.createdByFName = createdByFName;
	}
	public String getCreatedByLName() {
		return createdByLName;
	}
	public void setCreatedByLName(String createdByLName) {
		this.createdByLName = createdByLName;
	}
	public String getCreatedByUserName() {
		return createdByUserName;
	}
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}
	public String getCustAddress() {
		return custAddress;
	}
	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}
	public String getCustCity() {
		return custCity;
	}
	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustFName() {
		return custFName;
	}
	public void setCustFName(String custFName) {
		this.custFName = custFName;
	}
	public String getCustLName() {
		return custLName;
	}
	public void setCustLName(String custLName) {
		this.custLName = custLName;
	}
	public String getCustMInitial() {
		return custMInitial;
	}
	public void setCustMInitial(String custMInitial) {
		this.custMInitial = custMInitial;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	public String getCustState() {
		return custState;
	}
	public void setCustState(String custState) {
		this.custState = custState;
	}
	public String getCustZip() {
		return custZip;
	}
	public void setCustZip(String custZip) {
		this.custZip = custZip;
	}
	public String getLenderName() {
		return lenderName;
	}
	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getMileage() {
		return mileage;
	}
	public void setMileage(String mileage) {
		this.mileage = mileage;
	}
	public String getPayoffAmount() {
		return payoffAmount;
	}
	public void setPayoffAmount(String payoffAmount) {
		this.payoffAmount = payoffAmount;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getTradeExteriorColor() {
		return tradeExteriorColor;
	}
	public void setTradeExteriorColor(String tradeExteriorColor) {
		this.tradeExteriorColor = tradeExteriorColor;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
}
