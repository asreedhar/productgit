
package biz.firstlook.service.appraisal;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for exitCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="exitCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Success"/>
 *     &lt;enumeration value="Invalid Vin"/>
 *     &lt;enumeration value="Authentication Failed"/>
 *     &lt;enumeration value="Missing Required Attributes"/>
 *     &lt;enumeration value="Schema Invalid"/>
 *     &lt;enumeration value="BookType and BookValueType do not match"/>
 *     &lt;enumeration value="Exists in Active Inventory"/>
 *     &lt;enumeration value="Exists in InActive Inventory"/>
 *     &lt;enumeration value="Conflicts with existing Appraisal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum ExitCode {

    @XmlEnumValue("Authentication Failed")
    AUTHENTICATION_FAILED("Authentication Failed"),
    @XmlEnumValue("BookType and BookValueType do not match")
    BOOK_TYPE_AND_BOOK_VALUE_TYPE_DO_NOT_MATCH("BookType and BookValueType do not match"),
    @XmlEnumValue("Conflicts with existing Appraisal")
    CONFLICTS_WITH_EXISTING_APPRAISAL("Conflicts with existing Appraisal"),
    @XmlEnumValue("Exists in Active Inventory")
    EXISTS_IN_ACTIVE_INVENTORY("Exists in Active Inventory"),
    @XmlEnumValue("Exists in InActive Inventory")
    EXISTS_IN_IN_ACTIVE_INVENTORY("Exists in InActive Inventory"),
    @XmlEnumValue("Invalid Vin")
    INVALID_VIN("Invalid Vin"),
    @XmlEnumValue("Missing Required Attributes")
    MISSING_REQUIRED_ATTRIBUTES("Missing Required Attributes"),
    @XmlEnumValue("Schema Invalid")
    SCHEMA_INVALID("Schema Invalid"),
    @XmlEnumValue("Success")
    SUCCESS("Success");
    private final String value;

    ExitCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExitCode fromValue(String v) {
        for (ExitCode c: ExitCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
