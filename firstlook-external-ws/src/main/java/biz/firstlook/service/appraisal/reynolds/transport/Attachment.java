
package biz.firstlook.service.appraisal.reynolds.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Attachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Attachment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="binary" type="{http://www.starstandards.org/webservices/2005/10/transport}BinaryData"/>
 *         &lt;element name="text" type="{http://www.starstandards.org/webservices/2005/10/transport}TextData"/>
 *         &lt;element name="uriReference" type="{http://www.starstandards.org/webservices/2005/10/transport}UriReference"/>
 *         &lt;element name="attachmentReference" type="{http://www.starstandards.org/webservices/2005/10/transport}AttachmentReference"/>
 *       &lt;/choice>
 *       &lt;attGroup ref="{http://www.starstandards.org/webservices/2005/10/transport}AttachmentAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", propOrder = {
    "binary",
    "text",
    "uriReference",
    "attachmentReference"
})
public class Attachment {

    protected BinaryData binary;
    protected TextData text;
    protected UriReference uriReference;
    protected AttachmentReference attachmentReference;
    @XmlAttribute
    protected String action;
    @XmlAttribute
    protected XMLGregorianCalendar dateTime;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fileName;
    @XmlAttribute
    protected String from;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String mimeCode;
    @XmlAttribute
    protected Long size;
    @XmlAttribute
    protected String to;

    /**
     * Gets the value of the binary property.
     * 
     * @return
     *     possible object is
     *     {@link BinaryData }
     *     
     */
    public BinaryData getBinary() {
        return binary;
    }

    /**
     * Sets the value of the binary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BinaryData }
     *     
     */
    public void setBinary(BinaryData value) {
        this.binary = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link TextData }
     *     
     */
    public TextData getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextData }
     *     
     */
    public void setText(TextData value) {
        this.text = value;
    }

    /**
     * Gets the value of the uriReference property.
     * 
     * @return
     *     possible object is
     *     {@link UriReference }
     *     
     */
    public UriReference getUriReference() {
        return uriReference;
    }

    /**
     * Sets the value of the uriReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link UriReference }
     *     
     */
    public void setUriReference(UriReference value) {
        this.uriReference = value;
    }

    /**
     * Gets the value of the attachmentReference property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentReference }
     *     
     */
    public AttachmentReference getAttachmentReference() {
        return attachmentReference;
    }

    /**
     * Sets the value of the attachmentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentReference }
     *     
     */
    public void setAttachmentReference(AttachmentReference value) {
        this.attachmentReference = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the dateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }

    /**
     * Sets the value of the dateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTime(XMLGregorianCalendar value) {
        this.dateTime = value;
    }

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrom(String value) {
        this.from = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the mimeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeCode() {
        return mimeCode;
    }

    /**
     * Sets the value of the mimeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeCode(String value) {
        this.mimeCode = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSize(Long value) {
        this.size = value;
    }

    /**
     * Gets the value of the to property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTo(String value) {
        this.to = value;
    }

}
