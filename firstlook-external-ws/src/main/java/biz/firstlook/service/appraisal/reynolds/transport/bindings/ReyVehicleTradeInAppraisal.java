
package biz.firstlook.service.appraisal.reynolds.transport.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rey_VehicleTradeInAppraisal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rey_VehicleTradeInAppraisal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationArea" type="{http://www.starstandards.org/STAR}ApplicationAreaType"/>
 *         &lt;element name="VehicleTradeIn" type="{http://www.starstandards.org/STAR}VehicleTradeInType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="revision" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="1.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rey_VehicleTradeInAppraisal", propOrder = {
    "applicationArea",
    "vehicleTradeIn"
})
public class ReyVehicleTradeInAppraisal {

    @XmlElement(name = "ApplicationArea", required = true)
    protected ApplicationAreaType applicationArea;
    @XmlElement(name = "VehicleTradeIn", required = true)
    protected VehicleTradeInType vehicleTradeIn;
    @XmlAttribute
    protected String revision;

    /**
     * Gets the value of the applicationArea property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationAreaType }
     *     
     */
    public ApplicationAreaType getApplicationArea() {
        return applicationArea;
    }

    /**
     * Sets the value of the applicationArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationAreaType }
     *     
     */
    public void setApplicationArea(ApplicationAreaType value) {
        this.applicationArea = value;
    }

    /**
     * Gets the value of the vehicleTradeIn property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleTradeInType }
     *     
     */
    public VehicleTradeInType getVehicleTradeIn() {
        return vehicleTradeIn;
    }

    /**
     * Sets the value of the vehicleTradeIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleTradeInType }
     *     
     */
    public void setVehicleTradeIn(VehicleTradeInType value) {
        this.vehicleTradeIn = value;
    }

    /**
     * Gets the value of the revision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevision() {
        if (revision == null) {
            return "1.0";
        } else {
            return revision;
        }
    }

    /**
     * Sets the value of the revision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevision(String value) {
        this.revision = value;
    }

}
