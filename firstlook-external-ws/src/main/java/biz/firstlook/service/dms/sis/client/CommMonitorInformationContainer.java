
package biz.firstlook.service.dms.sis.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommMonitorInformationContainer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommMonitorInformationContainer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentUnreadRequests" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CommMonitorInformationArray" type="{http://www.menuv.com/webservices/CommManager/V4/DMSDeals}ArrayOfCommMonitorInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommMonitorInformationContainer", propOrder = {
    "currentUnreadRequests",
    "commMonitorInformationArray"
})
public class CommMonitorInformationContainer {

    @XmlElement(name = "CurrentUnreadRequests")
    protected int currentUnreadRequests;
    @XmlElement(name = "CommMonitorInformationArray")
    protected ArrayOfCommMonitorInformation commMonitorInformationArray;

    /**
     * Gets the value of the currentUnreadRequests property.
     * 
     */
    public int getCurrentUnreadRequests() {
        return currentUnreadRequests;
    }

    /**
     * Sets the value of the currentUnreadRequests property.
     * 
     */
    public void setCurrentUnreadRequests(int value) {
        this.currentUnreadRequests = value;
    }

    /**
     * Gets the value of the commMonitorInformationArray property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCommMonitorInformation }
     *     
     */
    public ArrayOfCommMonitorInformation getCommMonitorInformationArray() {
        return commMonitorInformationArray;
    }

    /**
     * Sets the value of the commMonitorInformationArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCommMonitorInformation }
     *     
     */
    public void setCommMonitorInformationArray(ArrayOfCommMonitorInformation value) {
        this.commMonitorInformationArray = value;
    }

}
