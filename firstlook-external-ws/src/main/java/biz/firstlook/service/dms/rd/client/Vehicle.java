
package biz.firstlook.service.dms.rd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Vehicle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Vehicle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}VehicleDetail" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Option" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}DlrOption" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OptionPackage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="AccentClr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Carline" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ExtClrCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ExtClrDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IntClrCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IntClrDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MakePfx" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MdlNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ModelDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RoofClr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="VehicleMake" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="VehicleYr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Vin" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vehicle", propOrder = {
    "vehicleDetail",
    "option",
    "dlrOption",
    "optionPackage"
})
public class Vehicle {

    @XmlElement(name = "VehicleDetail")
    protected VehicleDetail vehicleDetail;
    @XmlElement(name = "Option", required = true)
    protected List<Option> option;
    @XmlElement(name = "DlrOption", required = true)
    protected List<DlrOption> dlrOption;
    @XmlElement(name = "OptionPackage", required = true)
    protected List<OptionPackage> optionPackage;
    @XmlAttribute(name = "AccentClr")
    protected String accentClr;
    @XmlAttribute(name = "Carline")
    protected String carline;
    @XmlAttribute(name = "ExtClrCode")
    protected String extClrCode;
    @XmlAttribute(name = "ExtClrDesc")
    protected String extClrDesc;
    @XmlAttribute(name = "IntClrCode")
    protected String intClrCode;
    @XmlAttribute(name = "IntClrDesc")
    protected String intClrDesc;
    @XmlAttribute(name = "MakePfx")
    protected String makePfx;
    @XmlAttribute(name = "MdlNo")
    protected String mdlNo;
    @XmlAttribute(name = "ModelDesc")
    protected String modelDesc;
    @XmlAttribute(name = "RoofClr")
    protected String roofClr;
    @XmlAttribute(name = "VehicleMake")
    protected String vehicleMake;
    @XmlAttribute(name = "VehicleYr")
    protected String vehicleYr;
    @XmlAttribute(name = "Vin")
    protected String vin;

    /**
     * Gets the value of the vehicleDetail property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleDetail }
     *     
     */
    public VehicleDetail getVehicleDetail() {
        return vehicleDetail;
    }

    /**
     * Sets the value of the vehicleDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleDetail }
     *     
     */
    public void setVehicleDetail(VehicleDetail value) {
        this.vehicleDetail = value;
    }

    /**
     * Gets the value of the option property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the option property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Option }
     * 
     * 
     */
    public List<Option> getOption() {
        if (option == null) {
            option = new ArrayList<Option>();
        }
        return this.option;
    }

    /**
     * Gets the value of the dlrOption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dlrOption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDlrOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DlrOption }
     * 
     * 
     */
    public List<DlrOption> getDlrOption() {
        if (dlrOption == null) {
            dlrOption = new ArrayList<DlrOption>();
        }
        return this.dlrOption;
    }

    /**
     * Gets the value of the optionPackage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the optionPackage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOptionPackage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OptionPackage }
     * 
     * 
     */
    public List<OptionPackage> getOptionPackage() {
        if (optionPackage == null) {
            optionPackage = new ArrayList<OptionPackage>();
        }
        return this.optionPackage;
    }

    /**
     * Gets the value of the accentClr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccentClr() {
        return accentClr;
    }

    /**
     * Sets the value of the accentClr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccentClr(String value) {
        this.accentClr = value;
    }

    /**
     * Gets the value of the carline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarline() {
        return carline;
    }

    /**
     * Sets the value of the carline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarline(String value) {
        this.carline = value;
    }

    /**
     * Gets the value of the extClrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtClrCode() {
        return extClrCode;
    }

    /**
     * Sets the value of the extClrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtClrCode(String value) {
        this.extClrCode = value;
    }

    /**
     * Gets the value of the extClrDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtClrDesc() {
        return extClrDesc;
    }

    /**
     * Sets the value of the extClrDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtClrDesc(String value) {
        this.extClrDesc = value;
    }

    /**
     * Gets the value of the intClrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntClrCode() {
        return intClrCode;
    }

    /**
     * Sets the value of the intClrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntClrCode(String value) {
        this.intClrCode = value;
    }

    /**
     * Gets the value of the intClrDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntClrDesc() {
        return intClrDesc;
    }

    /**
     * Sets the value of the intClrDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntClrDesc(String value) {
        this.intClrDesc = value;
    }

    /**
     * Gets the value of the makePfx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakePfx() {
        return makePfx;
    }

    /**
     * Sets the value of the makePfx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakePfx(String value) {
        this.makePfx = value;
    }

    /**
     * Gets the value of the mdlNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdlNo() {
        return mdlNo;
    }

    /**
     * Sets the value of the mdlNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdlNo(String value) {
        this.mdlNo = value;
    }

    /**
     * Gets the value of the modelDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelDesc() {
        return modelDesc;
    }

    /**
     * Sets the value of the modelDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelDesc(String value) {
        this.modelDesc = value;
    }

    /**
     * Gets the value of the roofClr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoofClr() {
        return roofClr;
    }

    /**
     * Sets the value of the roofClr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoofClr(String value) {
        this.roofClr = value;
    }

    /**
     * Gets the value of the vehicleMake property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleMake() {
        return vehicleMake;
    }

    /**
     * Sets the value of the vehicleMake property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleMake(String value) {
        this.vehicleMake = value;
    }

    /**
     * Gets the value of the vehicleYr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleYr() {
        return vehicleYr;
    }

    /**
     * Sets the value of the vehicleYr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleYr(String value) {
        this.vehicleYr = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVin(String value) {
        this.vin = value;
    }

}
