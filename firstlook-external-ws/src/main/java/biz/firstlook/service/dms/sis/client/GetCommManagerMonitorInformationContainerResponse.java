
package biz.firstlook.service.dms.sis.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommMonitorInformationContainer" type="{http://www.menuv.com/webservices/CommManager/V4/DMSDeals}CommMonitorInformationContainer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commMonitorInformationContainer"
})
@XmlRootElement(name = "GetCommManagerMonitorInformationContainerResponse")
public class GetCommManagerMonitorInformationContainerResponse {

    @XmlElement(name = "CommMonitorInformationContainer", required = true, nillable = true)
    protected CommMonitorInformationContainer commMonitorInformationContainer;

    /**
     * Gets the value of the commMonitorInformationContainer property.
     * 
     * @return
     *     possible object is
     *     {@link CommMonitorInformationContainer }
     *     
     */
    public CommMonitorInformationContainer getCommMonitorInformationContainer() {
        return commMonitorInformationContainer;
    }

    /**
     * Sets the value of the commMonitorInformationContainer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommMonitorInformationContainer }
     *     
     */
    public void setCommMonitorInformationContainer(CommMonitorInformationContainer value) {
        this.commMonitorInformationContainer = value;
    }

}
