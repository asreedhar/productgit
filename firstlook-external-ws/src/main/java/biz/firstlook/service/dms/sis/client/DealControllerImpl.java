
package biz.firstlook.service.dms.sis.client;

import javax.jws.WebService;

@WebService(serviceName = "DealController", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", endpointInterface = "com.menuv.webservices.commmanager.v4.dmsdeals.DealControllerSoap")
public class DealControllerImpl
    implements DealControllerSoap
{


    public RequestStatus getRequestStatus(int dmsID, boolean includeRequestResponseData, biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public int submitDealInfoRequest(int dealNumber, biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public int submitDealEntryRequest(String xml, biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public int submitRequest(String xml, biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public String getDealResponse(int dmsID, biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public String pollForResponse(int requestID, biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public void purgeUnreadRequests(biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public CommMonitorInformationContainer getCommManagerMonitorInformationContainer(biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

    public ArrayOfCommMonitorInformation getCommManagerMonitorInformation(biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader) {
        throw new UnsupportedOperationException();
    }

}
