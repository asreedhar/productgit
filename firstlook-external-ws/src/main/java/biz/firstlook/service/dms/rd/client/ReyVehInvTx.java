
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rey_VehInvTx complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rey_VehInvTx">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationArea" type="{http://www.starstandards.org/STAR}ApplicationAreaType"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}VehInvExchange"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OtherCriteria"/>
 *       &lt;/sequence>
 *       &lt;attribute name="revision" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rey_VehInvTx", propOrder = {
    "applicationArea",
    "vehInvExchange",
    "otherCriteria"
})
public class ReyVehInvTx {

    @XmlElement(name = "ApplicationArea", required = true)
    protected ApplicationAreaType applicationArea;
    @XmlElement(name = "VehInvExchange", required = true)
    protected VehInvExchange vehInvExchange;
    @XmlElement(name = "OtherCriteria", required = true)
    protected OtherCriteria otherCriteria;
    @XmlAttribute(required = true)
    protected String revision; 
    @XmlAttribute(required = true)
    protected String xmlns;
    @XmlAttribute(required = true, name="xsi:schemaLocation")
    protected String schemaLocation;
    
    /**
     * Gets the value of the applicationArea property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationAreaType }
     *     
     */
    public ApplicationAreaType getApplicationArea() {
        return applicationArea;
    }

    /**
     * Sets the value of the applicationArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationAreaType }
     *     
     */
    public void setApplicationArea(ApplicationAreaType value) {
        this.applicationArea = value;
    }

    /**
     * Gets the value of the vehInvExchange property.
     * 
     * @return
     *     possible object is
     *     {@link VehInvExchange }
     *     
     */
    public VehInvExchange getVehInvExchange() {
        return vehInvExchange;
    }

    /**
     * Sets the value of the vehInvExchange property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehInvExchange }
     *     
     */
    public void setVehInvExchange(VehInvExchange value) {
        this.vehInvExchange = value;
    }

    /**
     * Gets the value of the otherCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link OtherCriteria }
     *     
     */
    public OtherCriteria getOtherCriteria() {
        return otherCriteria;
    }

    /**
     * Sets the value of the otherCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link OtherCriteria }
     *     
     */
    public void setOtherCriteria(OtherCriteria value) {
        this.otherCriteria = value;
    }

    /**
     * Gets the value of the revision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Sets the value of the revision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevision(String value) {
        this.revision = value;
    }

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public String getSchemaLocation() {
		return schemaLocation;
	}

	public void setSchemaLocation(String schemaLocation) {
		this.schemaLocation = schemaLocation;
	}

}
