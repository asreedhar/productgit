
package biz.firstlook.service.dms.sis.client;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "DealControllerSoap", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface DealControllerSoap {


    @WebMethod(operationName = "GetRequestStatus", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/GetRequestStatus")
    @WebResult(name = "GetRequestStatusResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public RequestStatus getRequestStatus(
        @WebParam(name = "dmsID", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        int dmsID,
        @WebParam(name = "includeRequestResponseData", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        boolean includeRequestResponseData,
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "SubmitDealInfoRequest", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/SubmitDealInfoRequest")
    @WebResult(name = "SubmitDealInfoRequestResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public int submitDealInfoRequest(
        @WebParam(name = "dealNumber", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        int dealNumber,
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "SubmitDealEntryRequest", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/SubmitDealEntryRequest")
    @WebResult(name = "SubmitDealEntryRequestResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public int submitDealEntryRequest(
        @WebParam(name = "xml", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        String xml,
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "SubmitRequest", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/SubmitRequest")
    @WebResult(name = "SubmitRequestResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public int submitRequest(
        @WebParam(name = "xml", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        String xml,
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "GetDealResponse", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/GetDealResponse")
    @WebResult(name = "GetDealResponseResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public String getDealResponse(
        @WebParam(name = "dmsID", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        int dmsID,
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "PollForResponse", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/PollForResponse")
    @WebResult(name = "PollForResponseResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public String pollForResponse(
        @WebParam(name = "requestID", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
        int requestID,
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "PurgeUnreadRequests", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/PurgeUnreadRequests")
    public void purgeUnreadRequests(
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "GetCommManagerMonitorInformationContainer", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/GetCommManagerMonitorInformationContainer")
    @WebResult(name = "CommMonitorInformationContainer", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public CommMonitorInformationContainer getCommManagerMonitorInformationContainer(
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

    @WebMethod(operationName = "GetCommManagerMonitorInformation", action = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals/GetCommManagerMonitorInformation")
    @WebResult(name = "GetCommManagerMonitorInformationResult", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals")
    public ArrayOfCommMonitorInformation getCommManagerMonitorInformation(
        @WebParam(name = "UserInfoHeader", targetNamespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", header = true, mode = WebParam.Mode.INOUT)
        biz.firstlook.service.dms.sis.client.UserInfoHeader UserInfoHeader);

}
