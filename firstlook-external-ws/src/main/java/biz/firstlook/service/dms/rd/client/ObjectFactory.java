
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.starstandards.star package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VehInvOrigin_QNAME = new QName("http://www.starstandards.org/STAR", "VehInvOrigin");
    private final static QName _BODId_QNAME = new QName("http://www.starstandards.org/STAR", "BODId");
    private final static QName _DestinationNameCode_QNAME = new QName("http://www.starstandards.org/STAR", "DestinationNameCode");
    private final static QName _OptionDescLines_QNAME = new QName("http://www.starstandards.org/STAR", "OptionDescLines");
    private final static QName _StockingInfo_QNAME = new QName("http://www.starstandards.org/STAR", "StockingInfo");
    private final static QName _OptionPackage_QNAME = new QName("http://www.starstandards.org/STAR", "OptionPackage");
    private final static QName _AreaNumber_QNAME = new QName("http://www.starstandards.org/STAR", "AreaNumber");
    private final static QName _StoreNumber_QNAME = new QName("http://www.starstandards.org/STAR", "StoreNumber");
    private final static QName _Task_QNAME = new QName("http://www.starstandards.org/STAR", "Task");
    private final static QName _DealerNumber_QNAME = new QName("http://www.starstandards.org/STAR", "DealerNumber");
    private final static QName _PriceCost_QNAME = new QName("http://www.starstandards.org/STAR", "PriceCost");
    private final static QName _ReyVehInvTx_QNAME = new QName("http://www.starstandards.org/STAR", "rey_VehInvTx");
    private final static QName _Option_QNAME = new QName("http://www.starstandards.org/STAR", "Option");
    private final static QName _DlrOption_QNAME = new QName("http://www.starstandards.org/STAR", "DlrOption");
    private final static QName _Component_QNAME = new QName("http://www.starstandards.org/STAR", "Component");
    private final static QName _DestinationURI_QNAME = new QName("http://www.starstandards.org/STAR", "DestinationURI");
    private final static QName _VehInvExchange_QNAME = new QName("http://www.starstandards.org/STAR", "VehInvExchange");
    private final static QName _OtherCriteria_QNAME = new QName("http://www.starstandards.org/STAR", "OtherCriteria");
    private final static QName _DestinationSoftwareCode_QNAME = new QName("http://www.starstandards.org/STAR", "DestinationSoftwareCode");
    private final static QName _OrderInfo_QNAME = new QName("http://www.starstandards.org/STAR", "OrderInfo");
    private final static QName _SenderNameCode_QNAME = new QName("http://www.starstandards.org/STAR", "SenderNameCode");
    private final static QName _Vehicle_QNAME = new QName("http://www.starstandards.org/STAR", "Vehicle");
    private final static QName _MiscInfo_QNAME = new QName("http://www.starstandards.org/STAR", "MiscInfo");
    private final static QName _CreatorNameCode_QNAME = new QName("http://www.starstandards.org/STAR", "CreatorNameCode");
    private final static QName _Language_QNAME = new QName("http://www.starstandards.org/STAR", "Language");
    private final static QName _Signature_QNAME = new QName("http://www.starstandards.org/STAR", "Signature");
    private final static QName _CreationDateTime_QNAME = new QName("http://www.starstandards.org/STAR", "CreationDateTime");
    private final static QName _OptionAccounting_QNAME = new QName("http://www.starstandards.org/STAR", "OptionAccounting");
    private final static QName _ReferenceId_QNAME = new QName("http://www.starstandards.org/STAR", "ReferenceId");
    private final static QName _AuthorizationId_QNAME = new QName("http://www.starstandards.org/STAR", "AuthorizationId");
    private final static QName _UserDefinedField_QNAME = new QName("http://www.starstandards.org/STAR", "UserDefinedField");
    private final static QName _VehInvRec_QNAME = new QName("http://www.starstandards.org/STAR", "VehInvRec");
    private final static QName _SenderURI_QNAME = new QName("http://www.starstandards.org/STAR", "SenderURI");
    private final static QName _DeliverPendingMailInd_QNAME = new QName("http://www.starstandards.org/STAR", "DeliverPendingMailInd");
    private final static QName _DestinationSoftware_QNAME = new QName("http://www.starstandards.org/STAR", "DestinationSoftware");
    private final static QName _DealerCountry_QNAME = new QName("http://www.starstandards.org/STAR", "DealerCountry");
    private final static QName _ROPOInfo_QNAME = new QName("http://www.starstandards.org/STAR", "ROPOInfo");
    private final static QName _VehicleDetail_QNAME = new QName("http://www.starstandards.org/STAR", "VehicleDetail");
    private final static QName _Password_QNAME = new QName("http://www.starstandards.org/STAR", "Password");
    private final static QName _LogicalId_QNAME = new QName("http://www.starstandards.org/STAR", "LogicalId");
    private final static QName _Floorplan_QNAME = new QName("http://www.starstandards.org/STAR", "Floorplan");
    private final static QName _FloorplanInfo_QNAME = new QName("http://www.starstandards.org/STAR", "FloorplanInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.starstandards.star
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OtherCriteria }
     * 
     */
    public OtherCriteria createOtherCriteria() {
        return new OtherCriteria();
    }

    /**
     * Create an instance of {@link OptionDescLines }
     * 
     */
    public OptionDescLines createOptionDescLines() {
        return new OptionDescLines();
    }

    /**
     * Create an instance of {@link SenderType }
     * 
     */
    public SenderType createSenderType() {
        return new SenderType();
    }

    /**
     * Create an instance of {@link VehInvOrigin }
     * 
     */
    public VehInvOrigin createVehInvOrigin() {
        return new VehInvOrigin();
    }

    /**
     * Create an instance of {@link PriceCost }
     * 
     */
    public PriceCost createPriceCost() {
        return new PriceCost();
    }

    /**
     * Create an instance of {@link OptionPackage }
     * 
     */
    public OptionPackage createOptionPackage() {
        return new OptionPackage();
    }

    /**
     * Create an instance of {@link MiscInfo }
     * 
     */
    public MiscInfo createMiscInfo() {
        return new MiscInfo();
    }

    /**
     * Create an instance of {@link UserDefinedField }
     * 
     */
    public UserDefinedField createUserDefinedField() {
        return new UserDefinedField();
    }

    /**
     * Create an instance of {@link StockingInfo }
     * 
     */
    public StockingInfo createStockingInfo() {
        return new StockingInfo();
    }

    /**
     * Create an instance of {@link OrderInfo }
     * 
     */
    public OrderInfo createOrderInfo() {
        return new OrderInfo();
    }

    /**
     * Create an instance of {@link ApplicationAreaType }
     * 
     */
    public ApplicationAreaType createApplicationAreaType() {
        return new ApplicationAreaType();
    }

    /**
     * Create an instance of {@link DestinationType }
     * 
     */
    public DestinationType createDestinationType() {
        return new DestinationType();
    }

    /**
     * Create an instance of {@link VehicleDetail }
     * 
     */
    public VehicleDetail createVehicleDetail() {
        return new VehicleDetail();
    }

    /**
     * Create an instance of {@link OptionAccounting }
     * 
     */
    public OptionAccounting createOptionAccounting() {
        return new OptionAccounting();
    }

    /**
     * Create an instance of {@link Option }
     * 
     */
    public Option createOption() {
        return new Option();
    }

    /**
     * Create an instance of {@link VehInvExchange }
     * 
     */
    public VehInvExchange createVehInvExchange() {
        return new VehInvExchange();
    }

    /**
     * Create an instance of {@link Vehicle }
     * 
     */
    public Vehicle createVehicle() {
        return new Vehicle();
    }

    /**
     * Create an instance of {@link VehInvRec }
     * 
     */
    public VehInvRec createVehInvRec() {
        return new VehInvRec();
    }

    /**
     * Create an instance of {@link Floorplan }
     * 
     */
    public Floorplan createFloorplan() {
        return new Floorplan();
    }

    /**
     * Create an instance of {@link ROPOInfo }
     * 
     */
    public ROPOInfo createROPOInfo() {
        return new ROPOInfo();
    }

    /**
     * Create an instance of {@link ReyVehInvTx }
     * 
     */
    public ReyVehInvTx createReyVehInvTx() {
        return new ReyVehInvTx();
    }

    /**
     * Create an instance of {@link FloorplanInfo }
     * 
     */
    public FloorplanInfo createFloorplanInfo() {
        return new FloorplanInfo();
    }

    /**
     * Create an instance of {@link DlrOption }
     * 
     */
    public DlrOption createDlrOption() {
        return new DlrOption();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehInvOrigin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "VehInvOrigin")
    public JAXBElement<VehInvOrigin> createVehInvOrigin(VehInvOrigin value) {
        return new JAXBElement<VehInvOrigin>(_VehInvOrigin_QNAME, VehInvOrigin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "BODId")
    public JAXBElement<String> createBODId(String value) {
        return new JAXBElement<String>(_BODId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DestinationNameCode")
    public JAXBElement<String> createDestinationNameCode(String value) {
        return new JAXBElement<String>(_DestinationNameCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionDescLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "OptionDescLines")
    public JAXBElement<OptionDescLines> createOptionDescLines(OptionDescLines value) {
        return new JAXBElement<OptionDescLines>(_OptionDescLines_QNAME, OptionDescLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StockingInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "StockingInfo")
    public JAXBElement<StockingInfo> createStockingInfo(StockingInfo value) {
        return new JAXBElement<StockingInfo>(_StockingInfo_QNAME, StockingInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "OptionPackage")
    public JAXBElement<OptionPackage> createOptionPackage(OptionPackage value) {
        return new JAXBElement<OptionPackage>(_OptionPackage_QNAME, OptionPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "AreaNumber")
    public JAXBElement<String> createAreaNumber(String value) {
        return new JAXBElement<String>(_AreaNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "StoreNumber")
    public JAXBElement<String> createStoreNumber(String value) {
        return new JAXBElement<String>(_StoreNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Task")
    public JAXBElement<String> createTask(String value) {
        return new JAXBElement<String>(_Task_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DealerNumber")
    public JAXBElement<String> createDealerNumber(String value) {
        return new JAXBElement<String>(_DealerNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PriceCost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "PriceCost")
    public JAXBElement<PriceCost> createPriceCost(PriceCost value) {
        return new JAXBElement<PriceCost>(_PriceCost_QNAME, PriceCost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReyVehInvTx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "rey_VehInvTx")
    public JAXBElement<ReyVehInvTx> createReyVehInvTx(ReyVehInvTx value) {
        return new JAXBElement<ReyVehInvTx>(_ReyVehInvTx_QNAME, ReyVehInvTx.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Option }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Option")
    public JAXBElement<Option> createOption(Option value) {
        return new JAXBElement<Option>(_Option_QNAME, Option.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DlrOption }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DlrOption")
    public JAXBElement<DlrOption> createDlrOption(DlrOption value) {
        return new JAXBElement<DlrOption>(_DlrOption_QNAME, DlrOption.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Component")
    public JAXBElement<String> createComponent(String value) {
        return new JAXBElement<String>(_Component_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DestinationURI")
    public JAXBElement<String> createDestinationURI(String value) {
        return new JAXBElement<String>(_DestinationURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehInvExchange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "VehInvExchange")
    public JAXBElement<VehInvExchange> createVehInvExchange(VehInvExchange value) {
        return new JAXBElement<VehInvExchange>(_VehInvExchange_QNAME, VehInvExchange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "OtherCriteria")
    public JAXBElement<OtherCriteria> createOtherCriteria(OtherCriteria value) {
        return new JAXBElement<OtherCriteria>(_OtherCriteria_QNAME, OtherCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DestinationSoftwareCode")
    public JAXBElement<String> createDestinationSoftwareCode(String value) {
        return new JAXBElement<String>(_DestinationSoftwareCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "OrderInfo")
    public JAXBElement<OrderInfo> createOrderInfo(OrderInfo value) {
        return new JAXBElement<OrderInfo>(_OrderInfo_QNAME, OrderInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "SenderNameCode")
    public JAXBElement<String> createSenderNameCode(String value) {
        return new JAXBElement<String>(_SenderNameCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Vehicle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Vehicle")
    public JAXBElement<Vehicle> createVehicle(Vehicle value) {
        return new JAXBElement<Vehicle>(_Vehicle_QNAME, Vehicle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MiscInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "MiscInfo")
    public JAXBElement<MiscInfo> createMiscInfo(MiscInfo value) {
        return new JAXBElement<MiscInfo>(_MiscInfo_QNAME, MiscInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "CreatorNameCode")
    public JAXBElement<String> createCreatorNameCode(String value) {
        return new JAXBElement<String>(_CreatorNameCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Language")
    public JAXBElement<String> createLanguage(String value) {
        return new JAXBElement<String>(_Language_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Signature")
    public JAXBElement<String> createSignature(String value) {
        return new JAXBElement<String>(_Signature_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "CreationDateTime")
    public JAXBElement<String> createCreationDateTime(String value) {
        return new JAXBElement<String>(_CreationDateTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionAccounting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "OptionAccounting")
    public JAXBElement<OptionAccounting> createOptionAccounting(OptionAccounting value) {
        return new JAXBElement<OptionAccounting>(_OptionAccounting_QNAME, OptionAccounting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "ReferenceId")
    public JAXBElement<String> createReferenceId(String value) {
        return new JAXBElement<String>(_ReferenceId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "AuthorizationId")
    public JAXBElement<String> createAuthorizationId(String value) {
        return new JAXBElement<String>(_AuthorizationId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDefinedField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "UserDefinedField")
    public JAXBElement<UserDefinedField> createUserDefinedField(UserDefinedField value) {
        return new JAXBElement<UserDefinedField>(_UserDefinedField_QNAME, UserDefinedField.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehInvRec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "VehInvRec")
    public JAXBElement<VehInvRec> createVehInvRec(VehInvRec value) {
        return new JAXBElement<VehInvRec>(_VehInvRec_QNAME, VehInvRec.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "SenderURI")
    public JAXBElement<String> createSenderURI(String value) {
        return new JAXBElement<String>(_SenderURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DeliverPendingMailInd")
    public JAXBElement<String> createDeliverPendingMailInd(String value) {
        return new JAXBElement<String>(_DeliverPendingMailInd_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DestinationSoftware")
    public JAXBElement<String> createDestinationSoftware(String value) {
        return new JAXBElement<String>(_DestinationSoftware_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "DealerCountry")
    public JAXBElement<String> createDealerCountry(String value) {
        return new JAXBElement<String>(_DealerCountry_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ROPOInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "ROPOInfo")
    public JAXBElement<ROPOInfo> createROPOInfo(ROPOInfo value) {
        return new JAXBElement<ROPOInfo>(_ROPOInfo_QNAME, ROPOInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "VehicleDetail")
    public JAXBElement<VehicleDetail> createVehicleDetail(VehicleDetail value) {
        return new JAXBElement<VehicleDetail>(_VehicleDetail_QNAME, VehicleDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_Password_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "LogicalId")
    public JAXBElement<String> createLogicalId(String value) {
        return new JAXBElement<String>(_LogicalId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Floorplan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "Floorplan")
    public JAXBElement<Floorplan> createFloorplan(Floorplan value) {
        return new JAXBElement<Floorplan>(_Floorplan_QNAME, Floorplan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FloorplanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/STAR", name = "FloorplanInfo")
    public JAXBElement<FloorplanInfo> createFloorplanInfo(FloorplanInfo value) {
        return new JAXBElement<FloorplanInfo>(_FloorplanInfo_QNAME, FloorplanInfo.class, null, value);
    }

}
