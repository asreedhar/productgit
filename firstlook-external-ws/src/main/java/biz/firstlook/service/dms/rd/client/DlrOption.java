
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DlrOption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DlrOption">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OptionAccounting" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DlrAccCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="DlrAccDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DlrOption", propOrder = {
    "optionAccounting"
})
public class DlrOption {

    @XmlElement(name = "OptionAccounting")
    protected OptionAccounting optionAccounting;
    @XmlAttribute(name = "DlrAccCode")
    protected String dlrAccCode;
    @XmlAttribute(name = "DlrAccDesc")
    protected String dlrAccDesc;

    /**
     * Gets the value of the optionAccounting property.
     * 
     * @return
     *     possible object is
     *     {@link OptionAccounting }
     *     
     */
    public OptionAccounting getOptionAccounting() {
        return optionAccounting;
    }

    /**
     * Sets the value of the optionAccounting property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionAccounting }
     *     
     */
    public void setOptionAccounting(OptionAccounting value) {
        this.optionAccounting = value;
    }

    /**
     * Gets the value of the dlrAccCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlrAccCode() {
        return dlrAccCode;
    }

    /**
     * Sets the value of the dlrAccCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlrAccCode(String value) {
        this.dlrAccCode = value;
    }

    /**
     * Gets the value of the dlrAccDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlrAccDesc() {
        return dlrAccDesc;
    }

    /**
     * Sets the value of the dlrAccDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlrAccDesc(String value) {
        this.dlrAccDesc = value;
    }

}
