
package biz.firstlook.service.dms.sis.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCommMonitorInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCommMonitorInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommMonitorInformation" type="{http://www.menuv.com/webservices/CommManager/V4/DMSDeals}CommMonitorInformation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCommMonitorInformation", propOrder = {
    "commMonitorInformation"
})
public class ArrayOfCommMonitorInformation {

    @XmlElement(name = "CommMonitorInformation", required = true, nillable = true)
    protected List<CommMonitorInformation> commMonitorInformation;

    /**
     * Gets the value of the commMonitorInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commMonitorInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommMonitorInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommMonitorInformation }
     * 
     * 
     */
    public List<CommMonitorInformation> getCommMonitorInformation() {
        if (commMonitorInformation == null) {
            commMonitorInformation = new ArrayList<CommMonitorInformation>();
        }
        return this.commMonitorInformation;
    }

}
