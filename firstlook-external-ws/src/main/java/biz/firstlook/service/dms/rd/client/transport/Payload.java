
package biz.firstlook.service.dms.rd.client.transport;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * <p>Java class for payload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="content" type="{http://www.starstandards.org/webservices/2005/10/transport}Content"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace="http://www.starstandards.org/webservices/2003/10/transport",name = "payload", propOrder = {
    "content"
})
@XmlRootElement
public class Payload {

    @XmlElement(required = true)
    protected Content content;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();
    
    @XmlAttribute(required = true, name="xmlns:wsa")
    private String wsa;
    @XmlAttribute(required = true, name="xmlns:soap")
    private String soap;
    @XmlAttribute(required = true, name="xmlns:wsu")
    private String wsu;
    @XmlAttribute(required = true, name="xmlns:wsse")
    private String wsse;
    @XmlAttribute(required = true, name="xmlns")
    private String xmlns;
    @XmlAttribute(required = true, name="xmlns:soapenv")
    private String soapenv;
    @XmlAttribute(required = true, name="xmlns:xsd")
    private String xsd;
    @XmlAttribute(required = true, name="xmlns:soapenc")
    private String soapenc;
    @XmlAttribute(required = true, name="xmlns:xsi")
    private String xsi;
  
    
    
    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link Content }
     *     
     */
    public Content getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link Content }
     *     
     */
    public void setContent(Content value) {
        this.content = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

	public String getSoap() {
		return soap;
	}

	public String getSoapenc() {
		return soapenc;
	}

	public String getSoapenv() {
		return soapenv;
	}

	public String getWsa() {
		return wsa;
	}

	public String getWsse() {
		return wsse;
	}

	public String getWsu() {
		return wsu;
	}

	public String getXsi() {
		return xsi;
	}

	public void setSoap(String soap) {
		this.soap = soap;
	}

	public void setSoapenc(String soapenc) {
		this.soapenc = soapenc;
	}

	public void setSoapenv(String soapenv) {
		this.soapenv = soapenv;
	}

	public void setWsa(String wsa) {
		this.wsa = wsa;
	}

	public void setWsse(String wsse) {
		this.wsse = wsse;
	}

	public void setWsu(String wsu) {
		this.wsu = wsu;
	}

	public void setXsi(String xsi) {
		this.xsi = xsi;
	}

	public String getXsd() {
		return xsd;
	}

	public void setXsd(String xsd) {
		this.xsd = xsd;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
}
