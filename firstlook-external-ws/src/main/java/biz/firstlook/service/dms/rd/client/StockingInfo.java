
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StockingInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StockingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="AcctgMake" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Loc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RcptDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="StatCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="StatDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="UsedGroup" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StockingInfo")
public class StockingInfo {

    @XmlAttribute(name = "AcctgMake")
    protected String acctgMake;
    @XmlAttribute(name = "Id")
    protected String id;
    @XmlAttribute(name = "Loc")
    protected String loc;
    @XmlAttribute(name = "RcptDate")
    protected String rcptDate;
    @XmlAttribute(name = "StatCode")
    protected String statCode;
    @XmlAttribute(name = "StatDesc")
    protected String statDesc;
    @XmlAttribute(name = "UsedGroup")
    protected String usedGroup;
    @XmlAttribute(name = "VehicleType")
    protected String vehicleType;

    /**
     * Gets the value of the acctgMake property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctgMake() {
        return acctgMake;
    }

    /**
     * Sets the value of the acctgMake property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctgMake(String value) {
        this.acctgMake = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the loc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoc() {
        return loc;
    }

    /**
     * Sets the value of the loc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoc(String value) {
        this.loc = value;
    }

    /**
     * Gets the value of the rcptDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRcptDate() {
        return rcptDate;
    }

    /**
     * Sets the value of the rcptDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRcptDate(String value) {
        this.rcptDate = value;
    }

    /**
     * Gets the value of the statCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatCode() {
        return statCode;
    }

    /**
     * Sets the value of the statCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatCode(String value) {
        this.statCode = value;
    }

    /**
     * Gets the value of the statDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatDesc() {
        return statDesc;
    }

    /**
     * Sets the value of the statDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatDesc(String value) {
        this.statDesc = value;
    }

    /**
     * Gets the value of the usedGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsedGroup() {
        return usedGroup;
    }

    /**
     * Sets the value of the usedGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsedGroup(String value) {
        this.usedGroup = value;
    }

    /**
     * Gets the value of the vehicleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     * Sets the value of the vehicleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleType(String value) {
        this.vehicleType = value;
    }

}
