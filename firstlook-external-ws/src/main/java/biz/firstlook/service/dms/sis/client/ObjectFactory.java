
package biz.firstlook.service.dms.sis.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.menuv.webservices.commmanager.v4.dmsdeals package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserInfoHeader_QNAME = new QName("http://www.menuv.com/webservices/CommManager/V4/DMSDeals", "UserInfoHeader");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.menuv.webservices.commmanager.v4.dmsdeals
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserInfoHeader }
     * 
     */
    public UserInfoHeader createUserInfoHeader() {
        return new UserInfoHeader();
    }

    /**
     * Create an instance of {@link GetCommManagerMonitorInformationContainerResponse }
     * 
     */
    public GetCommManagerMonitorInformationContainerResponse createGetCommManagerMonitorInformationContainerResponse() {
        return new GetCommManagerMonitorInformationContainerResponse();
    }

    /**
     * Create an instance of {@link GetCommManagerMonitorInformationContainer }
     * 
     */
    public GetCommManagerMonitorInformationContainer createGetCommManagerMonitorInformationContainer() {
        return new GetCommManagerMonitorInformationContainer();
    }

    /**
     * Create an instance of {@link GetDealResponse }
     * 
     */
    public GetDealResponse createGetDealResponse() {
        return new GetDealResponse();
    }

    /**
     * Create an instance of {@link PollForResponseResponse }
     * 
     */
    public PollForResponseResponse createPollForResponseResponse() {
        return new PollForResponseResponse();
    }

    /**
     * Create an instance of {@link RequestStatus }
     * 
     */
    public RequestStatus createRequestStatus() {
        return new RequestStatus();
    }

    /**
     * Create an instance of {@link ArrayOfCommMonitorInformation }
     * 
     */
    public ArrayOfCommMonitorInformation createArrayOfCommMonitorInformation() {
        return new ArrayOfCommMonitorInformation();
    }

    /**
     * Create an instance of {@link SubmitRequestResponse }
     * 
     */
    public SubmitRequestResponse createSubmitRequestResponse() {
        return new SubmitRequestResponse();
    }

    /**
     * Create an instance of {@link SubmitDealEntryRequestResponse }
     * 
     */
    public SubmitDealEntryRequestResponse createSubmitDealEntryRequestResponse() {
        return new SubmitDealEntryRequestResponse();
    }

    /**
     * Create an instance of {@link PurgeUnreadRequestsResponse }
     * 
     */
    public PurgeUnreadRequestsResponse createPurgeUnreadRequestsResponse() {
        return new PurgeUnreadRequestsResponse();
    }

    /**
     * Create an instance of {@link GetRequestStatus }
     * 
     */
    public GetRequestStatus createGetRequestStatus() {
        return new GetRequestStatus();
    }

    /**
     * Create an instance of {@link GetCommManagerMonitorInformationResponse }
     * 
     */
    public GetCommManagerMonitorInformationResponse createGetCommManagerMonitorInformationResponse() {
        return new GetCommManagerMonitorInformationResponse();
    }

    /**
     * Create an instance of {@link GetRequestStatusResponse }
     * 
     */
    public GetRequestStatusResponse createGetRequestStatusResponse() {
        return new GetRequestStatusResponse();
    }

    /**
     * Create an instance of {@link SubmitDealInfoRequest }
     * 
     */
    public SubmitDealInfoRequest createSubmitDealInfoRequest() {
        return new SubmitDealInfoRequest();
    }

    /**
     * Create an instance of {@link GetCommManagerMonitorInformation }
     * 
     */
    public GetCommManagerMonitorInformation createGetCommManagerMonitorInformation() {
        return new GetCommManagerMonitorInformation();
    }

    /**
     * Create an instance of {@link GetDealResponseResponse }
     * 
     */
    public GetDealResponseResponse createGetDealResponseResponse() {
        return new GetDealResponseResponse();
    }

    /**
     * Create an instance of {@link PollForResponse }
     * 
     */
    public PollForResponse createPollForResponse() {
        return new PollForResponse();
    }

    /**
     * Create an instance of {@link SubmitRequest }
     * 
     */
    public SubmitRequest createSubmitRequest() {
        return new SubmitRequest();
    }

    /**
     * Create an instance of {@link CommMonitorInformation }
     * 
     */
    public CommMonitorInformation createCommMonitorInformation() {
        return new CommMonitorInformation();
    }

    /**
     * Create an instance of {@link PurgeUnreadRequests }
     * 
     */
    public PurgeUnreadRequests createPurgeUnreadRequests() {
        return new PurgeUnreadRequests();
    }

    /**
     * Create an instance of {@link CommMonitorInformationContainer }
     * 
     */
    public CommMonitorInformationContainer createCommMonitorInformationContainer() {
        return new CommMonitorInformationContainer();
    }

    /**
     * Create an instance of {@link SubmitDealInfoRequestResponse }
     * 
     */
    public SubmitDealInfoRequestResponse createSubmitDealInfoRequestResponse() {
        return new SubmitDealInfoRequestResponse();
    }

    /**
     * Create an instance of {@link SubmitDealEntryRequest }
     * 
     */
    public SubmitDealEntryRequest createSubmitDealEntryRequest() {
        return new SubmitDealEntryRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserInfoHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals", name = "UserInfoHeader")
    public JAXBElement<UserInfoHeader> createUserInfoHeader(UserInfoHeader value) {
        return new JAXBElement<UserInfoHeader>(_UserInfoHeader_QNAME, UserInfoHeader.class, null, value);
    }

}
