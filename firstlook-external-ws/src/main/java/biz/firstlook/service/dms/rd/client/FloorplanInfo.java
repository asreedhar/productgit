
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FloorplanInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FloorplanInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="FlrplnAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FlrplnIntrRate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IntrStartDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FloorplanInfo")
public class FloorplanInfo {

    @XmlAttribute(name = "FlrplnAmt")
    protected String flrplnAmt;
    @XmlAttribute(name = "FlrplnIntrRate")
    protected String flrplnIntrRate;
    @XmlAttribute(name = "IntrStartDate")
    protected String intrStartDate;

    /**
     * Gets the value of the flrplnAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlrplnAmt() {
        return flrplnAmt;
    }

    /**
     * Sets the value of the flrplnAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlrplnAmt(String value) {
        this.flrplnAmt = value;
    }

    /**
     * Gets the value of the flrplnIntrRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlrplnIntrRate() {
        return flrplnIntrRate;
    }

    /**
     * Sets the value of the flrplnIntrRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlrplnIntrRate(String value) {
        this.flrplnIntrRate = value;
    }

    /**
     * Gets the value of the intrStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntrStartDate() {
        return intrStartDate;
    }

    /**
     * Sets the value of the intrStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntrStartDate(String value) {
        this.intrStartDate = value;
    }

}
