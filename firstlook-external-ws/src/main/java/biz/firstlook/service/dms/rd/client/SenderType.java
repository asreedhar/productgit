
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SenderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SenderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}LogicalId" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Component"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Task"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}ReferenceId" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}AuthorizationId" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}CreatorNameCode"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}SenderNameCode"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}SenderURI" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}DealerNumber" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}StoreNumber" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}AreaNumber" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}DealerCountry" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Language" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}DeliverPendingMailInd" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Password" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SenderType", propOrder = {
    "logicalId",
    "component",
    "task",
    "referenceId",
    "authorizationId",
    "creatorNameCode",
    "senderNameCode",
    "senderURI",
    "dealerNumber",
    "storeNumber",
    "areaNumber",
    "dealerCountry",
    "language",
    "deliverPendingMailInd",
    "password"
})
public class SenderType {

    @XmlElement(name = "LogicalId")
    protected String logicalId;
    @XmlElement(name = "Component", required = true)
    protected String component;
    @XmlElement(name = "Task", required = true)
    protected String task;
    @XmlElement(name = "ReferenceId")
    protected String referenceId;
    @XmlElement(name = "AuthorizationId")
    protected String authorizationId;
    @XmlElement(name = "CreatorNameCode", required = true)
    protected String creatorNameCode;
    @XmlElement(name = "SenderNameCode", required = true)
    protected String senderNameCode;
    @XmlElement(name = "SenderURI")
    protected String senderURI;
    @XmlElement(name = "DealerNumber")
    protected String dealerNumber;
    @XmlElement(name = "StoreNumber")
    protected String storeNumber;
    @XmlElement(name = "AreaNumber")
    protected String areaNumber;
    @XmlElement(name = "DealerCountry")
    protected String dealerCountry;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "DeliverPendingMailInd")
    protected String deliverPendingMailInd;
    @XmlElement(name = "Password")
    protected String password;

    /**
     * Gets the value of the logicalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogicalId() {
        return logicalId;
    }

    /**
     * Sets the value of the logicalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogicalId(String value) {
        this.logicalId = value;
    }

    /**
     * Gets the value of the component property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponent() {
        return component;
    }

    /**
     * Sets the value of the component property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponent(String value) {
        this.component = value;
    }

    /**
     * Gets the value of the task property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTask() {
        return task;
    }

    /**
     * Sets the value of the task property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTask(String value) {
        this.task = value;
    }

    /**
     * Gets the value of the referenceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * Sets the value of the referenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceId(String value) {
        this.referenceId = value;
    }

    /**
     * Gets the value of the authorizationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationId() {
        return authorizationId;
    }

    /**
     * Sets the value of the authorizationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationId(String value) {
        this.authorizationId = value;
    }

    /**
     * Gets the value of the creatorNameCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatorNameCode() {
        return creatorNameCode;
    }

    /**
     * Sets the value of the creatorNameCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatorNameCode(String value) {
        this.creatorNameCode = value;
    }

    /**
     * Gets the value of the senderNameCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderNameCode() {
        return senderNameCode;
    }

    /**
     * Sets the value of the senderNameCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderNameCode(String value) {
        this.senderNameCode = value;
    }

    /**
     * Gets the value of the senderURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderURI() {
        return senderURI;
    }

    /**
     * Sets the value of the senderURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderURI(String value) {
        this.senderURI = value;
    }

    /**
     * Gets the value of the dealerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerNumber() {
        return dealerNumber;
    }

    /**
     * Sets the value of the dealerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerNumber(String value) {
        this.dealerNumber = value;
    }

    /**
     * Gets the value of the storeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * Sets the value of the storeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreNumber(String value) {
        this.storeNumber = value;
    }

    /**
     * Gets the value of the areaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaNumber() {
        return areaNumber;
    }

    /**
     * Sets the value of the areaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaNumber(String value) {
        this.areaNumber = value;
    }

    /**
     * Gets the value of the dealerCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCountry() {
        return dealerCountry;
    }

    /**
     * Sets the value of the dealerCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCountry(String value) {
        this.dealerCountry = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the deliverPendingMailInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliverPendingMailInd() {
        return deliverPendingMailInd;
    }

    /**
     * Sets the value of the deliverPendingMailInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliverPendingMailInd(String value) {
        this.deliverPendingMailInd = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
