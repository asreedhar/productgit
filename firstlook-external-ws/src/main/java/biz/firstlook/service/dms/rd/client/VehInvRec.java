
package biz.firstlook.service.dms.rd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehInvRec complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehInvRec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}VehInvOrigin"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Vehicle"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}StockingInfo" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}MiscInfo" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}ROPOInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}Floorplan" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OrderInfo" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}PriceCost" minOccurs="0"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}UserDefinedField" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehInvRec", propOrder = {
    "vehInvOrigin",
    "vehicle",
    "stockingInfo",
    "miscInfo",
    "ropoInfo",
    "floorplan",
    "orderInfo",
    "priceCost",
    "userDefinedField"
})
public class VehInvRec {

    @XmlElement(name = "VehInvOrigin", required = true)
    protected VehInvOrigin vehInvOrigin;
    @XmlElement(name = "Vehicle", required = true)
    protected Vehicle vehicle;
    @XmlElement(name = "StockingInfo")
    protected StockingInfo stockingInfo;
    @XmlElement(name = "MiscInfo")
    protected MiscInfo miscInfo;
    @XmlElement(name = "ROPOInfo", required = true)
    protected List<ROPOInfo> ropoInfo;
    @XmlElement(name = "Floorplan")
    protected Floorplan floorplan;
    @XmlElement(name = "OrderInfo")
    protected OrderInfo orderInfo;
    @XmlElement(name = "PriceCost")
    protected PriceCost priceCost;
    @XmlElement(name = "UserDefinedField", required = true)
    protected List<UserDefinedField> userDefinedField;

    /**
     * Gets the value of the vehInvOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link VehInvOrigin }
     *     
     */
    public VehInvOrigin getVehInvOrigin() {
        return vehInvOrigin;
    }

    /**
     * Sets the value of the vehInvOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehInvOrigin }
     *     
     */
    public void setVehInvOrigin(VehInvOrigin value) {
        this.vehInvOrigin = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the stockingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StockingInfo }
     *     
     */
    public StockingInfo getStockingInfo() {
        return stockingInfo;
    }

    /**
     * Sets the value of the stockingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StockingInfo }
     *     
     */
    public void setStockingInfo(StockingInfo value) {
        this.stockingInfo = value;
    }

    /**
     * Gets the value of the miscInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MiscInfo }
     *     
     */
    public MiscInfo getMiscInfo() {
        return miscInfo;
    }

    /**
     * Sets the value of the miscInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscInfo }
     *     
     */
    public void setMiscInfo(MiscInfo value) {
        this.miscInfo = value;
    }

    /**
     * Gets the value of the ropoInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ropoInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getROPOInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ROPOInfo }
     * 
     * 
     */
    public List<ROPOInfo> getROPOInfo() {
        if (ropoInfo == null) {
            ropoInfo = new ArrayList<ROPOInfo>();
        }
        return this.ropoInfo;
    }

    /**
     * Gets the value of the floorplan property.
     * 
     * @return
     *     possible object is
     *     {@link Floorplan }
     *     
     */
    public Floorplan getFloorplan() {
        return floorplan;
    }

    /**
     * Sets the value of the floorplan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Floorplan }
     *     
     */
    public void setFloorplan(Floorplan value) {
        this.floorplan = value;
    }

    /**
     * Gets the value of the orderInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderInfo }
     *     
     */
    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    /**
     * Sets the value of the orderInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderInfo }
     *     
     */
    public void setOrderInfo(OrderInfo value) {
        this.orderInfo = value;
    }

    /**
     * Gets the value of the priceCost property.
     * 
     * @return
     *     possible object is
     *     {@link PriceCost }
     *     
     */
    public PriceCost getPriceCost() {
        return priceCost;
    }

    /**
     * Sets the value of the priceCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceCost }
     *     
     */
    public void setPriceCost(PriceCost value) {
        this.priceCost = value;
    }

    /**
     * Gets the value of the userDefinedField property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userDefinedField property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDefinedField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDefinedField }
     * 
     * 
     */
    public List<UserDefinedField> getUserDefinedField() {
        if (userDefinedField == null) {
            userDefinedField = new ArrayList<UserDefinedField>();
        }
        return this.userDefinedField;
    }

}
