
package biz.firstlook.service.dms.sis.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCommManagerMonitorInformationResult" type="{http://www.menuv.com/webservices/CommManager/V4/DMSDeals}ArrayOfCommMonitorInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCommManagerMonitorInformationResult"
})
@XmlRootElement(name = "GetCommManagerMonitorInformationResponse")
public class GetCommManagerMonitorInformationResponse {

    @XmlElement(name = "GetCommManagerMonitorInformationResult")
    protected ArrayOfCommMonitorInformation getCommManagerMonitorInformationResult;

    /**
     * Gets the value of the getCommManagerMonitorInformationResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCommMonitorInformation }
     *     
     */
    public ArrayOfCommMonitorInformation getGetCommManagerMonitorInformationResult() {
        return getCommManagerMonitorInformationResult;
    }

    /**
     * Sets the value of the getCommManagerMonitorInformationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCommMonitorInformation }
     *     
     */
    public void setGetCommManagerMonitorInformationResult(ArrayOfCommMonitorInformation value) {
        this.getCommManagerMonitorInformationResult = value;
    }

}
