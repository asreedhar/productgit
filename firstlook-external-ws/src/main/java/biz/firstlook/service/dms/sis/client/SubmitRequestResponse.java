
package biz.firstlook.service.dms.sis.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmitRequestResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "submitRequestResult"
})
@XmlRootElement(name = "SubmitRequestResponse")
public class SubmitRequestResponse {

    @XmlElement(name = "SubmitRequestResult")
    protected int submitRequestResult;

    /**
     * Gets the value of the submitRequestResult property.
     * 
     */
    public int getSubmitRequestResult() {
        return submitRequestResult;
    }

    /**
     * Sets the value of the submitRequestResult property.
     * 
     */
    public void setSubmitRequestResult(int value) {
        this.submitRequestResult = value;
    }

}
