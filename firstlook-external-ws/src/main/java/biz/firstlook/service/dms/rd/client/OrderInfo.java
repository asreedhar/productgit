
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="AsmblyPlant" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="EstArrivalDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FactOrdNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MfrSchedDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MfrStat" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MfrStatDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Omal" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="OrdDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="OrdType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="OrderProcDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="OrderPrty" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PriceLvl" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ProdDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SpecOrdr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderInfo")
public class OrderInfo {

    @XmlAttribute(name = "AsmblyPlant")
    protected String asmblyPlant;
    @XmlAttribute(name = "EstArrivalDate")
    protected String estArrivalDate;
    @XmlAttribute(name = "FactOrdNo")
    protected String factOrdNo;
    @XmlAttribute(name = "MfrSchedDate")
    protected String mfrSchedDate;
    @XmlAttribute(name = "MfrStat")
    protected String mfrStat;
    @XmlAttribute(name = "MfrStatDate")
    protected String mfrStatDate;
    @XmlAttribute(name = "Omal")
    protected String omal;
    @XmlAttribute(name = "OrdDate")
    protected String ordDate;
    @XmlAttribute(name = "OrdType")
    protected String ordType;
    @XmlAttribute(name = "OrderProcDate")
    protected String orderProcDate;
    @XmlAttribute(name = "OrderPrty")
    protected String orderPrty;
    @XmlAttribute(name = "PriceLvl")
    protected String priceLvl;
    @XmlAttribute(name = "ProdDate")
    protected String prodDate;
    @XmlAttribute(name = "SpecOrdr")
    protected String specOrdr;

    /**
     * Gets the value of the asmblyPlant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsmblyPlant() {
        return asmblyPlant;
    }

    /**
     * Sets the value of the asmblyPlant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsmblyPlant(String value) {
        this.asmblyPlant = value;
    }

    /**
     * Gets the value of the estArrivalDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstArrivalDate() {
        return estArrivalDate;
    }

    /**
     * Sets the value of the estArrivalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstArrivalDate(String value) {
        this.estArrivalDate = value;
    }

    /**
     * Gets the value of the factOrdNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactOrdNo() {
        return factOrdNo;
    }

    /**
     * Sets the value of the factOrdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactOrdNo(String value) {
        this.factOrdNo = value;
    }

    /**
     * Gets the value of the mfrSchedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMfrSchedDate() {
        return mfrSchedDate;
    }

    /**
     * Sets the value of the mfrSchedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMfrSchedDate(String value) {
        this.mfrSchedDate = value;
    }

    /**
     * Gets the value of the mfrStat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMfrStat() {
        return mfrStat;
    }

    /**
     * Sets the value of the mfrStat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMfrStat(String value) {
        this.mfrStat = value;
    }

    /**
     * Gets the value of the mfrStatDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMfrStatDate() {
        return mfrStatDate;
    }

    /**
     * Sets the value of the mfrStatDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMfrStatDate(String value) {
        this.mfrStatDate = value;
    }

    /**
     * Gets the value of the omal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOmal() {
        return omal;
    }

    /**
     * Sets the value of the omal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOmal(String value) {
        this.omal = value;
    }

    /**
     * Gets the value of the ordDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdDate() {
        return ordDate;
    }

    /**
     * Sets the value of the ordDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdDate(String value) {
        this.ordDate = value;
    }

    /**
     * Gets the value of the ordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdType() {
        return ordType;
    }

    /**
     * Sets the value of the ordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdType(String value) {
        this.ordType = value;
    }

    /**
     * Gets the value of the orderProcDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderProcDate() {
        return orderProcDate;
    }

    /**
     * Sets the value of the orderProcDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderProcDate(String value) {
        this.orderProcDate = value;
    }

    /**
     * Gets the value of the orderPrty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPrty() {
        return orderPrty;
    }

    /**
     * Sets the value of the orderPrty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPrty(String value) {
        this.orderPrty = value;
    }

    /**
     * Gets the value of the priceLvl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceLvl() {
        return priceLvl;
    }

    /**
     * Sets the value of the priceLvl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceLvl(String value) {
        this.priceLvl = value;
    }

    /**
     * Gets the value of the prodDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdDate() {
        return prodDate;
    }

    /**
     * Sets the value of the prodDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdDate(String value) {
        this.prodDate = value;
    }

    /**
     * Gets the value of the specOrdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecOrdr() {
        return specOrdr;
    }

    /**
     * Sets the value of the specOrdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecOrdr(String value) {
        this.specOrdr = value;
    }

}
