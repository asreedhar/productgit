
package biz.firstlook.service.dms.rd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OptionPackage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OptionPackage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OptionAccounting" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.starstandards.org/STAR}Option"/>
 *           &lt;element ref="{http://www.starstandards.org/STAR}OptionDescLines"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="PkgCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PkgCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OptionPackage", propOrder = {
    "optionAccounting",
    "optionOrOptionDescLines"
})
public class OptionPackage {

    @XmlElement(name = "OptionAccounting")
    protected OptionAccounting optionAccounting;
    @XmlElements({
        @XmlElement(name = "OptionDescLines", required = true, type = OptionDescLines.class),
        @XmlElement(name = "Option", required = true, type = Option.class)
    })
    protected List<Object> optionOrOptionDescLines;
    @XmlAttribute(name = "PkgCode")
    protected String pkgCode;
    @XmlAttribute(name = "PkgCodeDesc")
    protected String pkgCodeDesc;

    /**
     * Gets the value of the optionAccounting property.
     * 
     * @return
     *     possible object is
     *     {@link OptionAccounting }
     *     
     */
    public OptionAccounting getOptionAccounting() {
        return optionAccounting;
    }

    /**
     * Sets the value of the optionAccounting property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionAccounting }
     *     
     */
    public void setOptionAccounting(OptionAccounting value) {
        this.optionAccounting = value;
    }

    /**
     * Gets the value of the optionOrOptionDescLines property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the optionOrOptionDescLines property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOptionOrOptionDescLines().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OptionDescLines }
     * {@link Option }
     * 
     * 
     */
    public List<Object> getOptionOrOptionDescLines() {
        if (optionOrOptionDescLines == null) {
            optionOrOptionDescLines = new ArrayList<Object>();
        }
        return this.optionOrOptionDescLines;
    }

    /**
     * Gets the value of the pkgCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkgCode() {
        return pkgCode;
    }

    /**
     * Sets the value of the pkgCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkgCode(String value) {
        this.pkgCode = value;
    }

    /**
     * Gets the value of the pkgCodeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkgCodeDesc() {
        return pkgCodeDesc;
    }

    /**
     * Sets the value of the pkgCodeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkgCodeDesc(String value) {
        this.pkgCodeDesc = value;
    }

}
