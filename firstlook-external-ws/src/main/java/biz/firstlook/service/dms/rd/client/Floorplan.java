
package biz.firstlook.service.dms.rd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Floorplan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Floorplan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}FloorplanInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="FlrplnVend" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FlrplnVendName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Floorplan", propOrder = {
    "floorplanInfo"
})
public class Floorplan {

    @XmlElement(name = "FloorplanInfo", required = true)
    protected List<FloorplanInfo> floorplanInfo;
    @XmlAttribute(name = "FlrplnVend")
    protected String flrplnVend;
    @XmlAttribute(name = "FlrplnVendName")
    protected String flrplnVendName;

    /**
     * Gets the value of the floorplanInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the floorplanInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFloorplanInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FloorplanInfo }
     * 
     * 
     */
    public List<FloorplanInfo> getFloorplanInfo() {
        if (floorplanInfo == null) {
            floorplanInfo = new ArrayList<FloorplanInfo>();
        }
        return this.floorplanInfo;
    }

    /**
     * Gets the value of the flrplnVend property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlrplnVend() {
        return flrplnVend;
    }

    /**
     * Sets the value of the flrplnVend property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlrplnVend(String value) {
        this.flrplnVend = value;
    }

    /**
     * Gets the value of the flrplnVendName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlrplnVendName() {
        return flrplnVendName;
    }

    /**
     * Sets the value of the flrplnVendName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlrplnVendName(String value) {
        this.flrplnVendName = value;
    }

}
