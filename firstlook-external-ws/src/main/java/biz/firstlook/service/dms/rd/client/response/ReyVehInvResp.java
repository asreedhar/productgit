
package biz.firstlook.service.dms.rd.client.response;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import biz.firstlook.service.dms.rd.client.ApplicationAreaType;
import biz.firstlook.service.dms.rd.client.OtherCriteria;


/**
 * <p>Java class for rey_VehInvResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rey_VehInvResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationArea" type="{http://www.starstandards.org/STAR}ApplicationAreaType"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}GenTransStatus"/>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OtherCriteria" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="revision" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReyVehInvResp", propOrder = {
    "applicationArea",
    "genTransStatus",
    "otherCriteria"
})
public class ReyVehInvResp {

    @XmlElement(name = "ApplicationArea", required = true)
    protected ApplicationAreaType applicationArea;
    @XmlElement(name = "GenTransStatus", required = true)
    protected GenTransStatus genTransStatus;
    @XmlElement(name = "OtherCriteria", required = true)
    protected List<OtherCriteria> otherCriteria;
    @XmlAttribute(required = true)
    protected String revision;
    @XmlAttribute(required = true)
    protected String xmlns;
    
    /**
     * Gets the value of the applicationArea property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationAreaType }
     *     
     */
    public ApplicationAreaType getApplicationArea() {
        return applicationArea;
    }

    /**
     * Sets the value of the applicationArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationAreaType }
     *     
     */
    public void setApplicationArea(ApplicationAreaType value) {
        this.applicationArea = value;
    }

    /**
     * Gets the value of the genTransStatus property.
     * 
     * @return
     *     possible object is
     *     {@link GenTransStatus }
     *     
     */
    public GenTransStatus getGenTransStatus() {
        return genTransStatus;
    }

    /**
     * Sets the value of the genTransStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenTransStatus }
     *     
     */
    public void setGenTransStatus(GenTransStatus value) {
        this.genTransStatus = value;
    }

    /**
     * Gets the value of the otherCriteria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherCriteria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherCriteria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherCriteria }
     * 
     * 
     */
    public List<OtherCriteria> getOtherCriteria() {
        if (otherCriteria == null) {
            otherCriteria = new ArrayList<OtherCriteria>();
        }
        return this.otherCriteria;
    }

    /**
     * Gets the value of the revision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Sets the value of the revision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevision(String value) {
        this.revision = value;
    }

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

}
