
package biz.firstlook.service.dms.rd.client.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import biz.firstlook.service.dms.rd.client.ReyVehInvTx;


/**
 * <p>Java class for Content complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Content">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rey_VehInvTx" type="{http://www.starstandards.org/STAR}rey_VehInvTx"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Content", propOrder = {
    "reyVehInvTx"
})
public class Content {

    @XmlElement(name = "rey_VehInvTx", required = true)
    protected ReyVehInvTx reyVehInvTx;
    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;

    /**
     * Gets the value of the reyVehInvTx property.
     * 
     * @return
     *     possible object is
     *     {@link ReyVehInvTx }
     *     
     */
    public ReyVehInvTx getReyVehInvTx() {
        return reyVehInvTx;
    }

    /**
     * Sets the value of the reyVehInvTx property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReyVehInvTx }
     *     
     */
    public void setReyVehInvTx(ReyVehInvTx value) {
        this.reyVehInvTx = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
