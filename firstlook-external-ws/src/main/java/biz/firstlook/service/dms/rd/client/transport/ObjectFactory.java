
package biz.firstlook.service.dms.rd.client.transport;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.starstandards.webservices._2005._10.transport package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PayloadManifest_QNAME = new QName("http://www.starstandards.org/webservices/2005/10/transport", "payloadManifest");
    private final static QName _Attachment_QNAME = new QName("http://www.starstandards.org/webservices/2005/10/transport", "attachment");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.starstandards.webservices._2005._10.transport
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessMessageResponse }
     * 
     */
    public ProcessMessageResponse createProcessMessageResponse() {
        return new ProcessMessageResponse();
    }

    /**
     * Create an instance of {@link PutMessageResponse }
     * 
     */
    public PutMessageResponse createPutMessageResponse() {
        return new PutMessageResponse();
    }

    /**
     * Create an instance of {@link PayloadManifest }
     * 
     */
    public PayloadManifest createPayloadManifest() {
        return new PayloadManifest();
    }

    /**
     * Create an instance of {@link Content }
     * 
     */
    public Content createContent() {
        return new Content();
    }

    /**
     * Create an instance of {@link TextData }
     * 
     */
    public TextData createTextData() {
        return new TextData();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link PutMessage }
     * 
     */
    public PutMessage createPutMessage() {
        return new PutMessage();
    }

    /**
     * Create an instance of {@link UriReference }
     * 
     */
    public UriReference createUriReference() {
        return new UriReference();
    }

    /**
     * Create an instance of {@link BinaryData }
     * 
     */
    public BinaryData createBinaryData() {
        return new BinaryData();
    }

    /**
     * Create an instance of {@link AttachmentReference }
     * 
     */
    public AttachmentReference createAttachmentReference() {
        return new AttachmentReference();
    }

    /**
     * Create an instance of {@link Payload }
     * 
     */
    public Payload createPayload() {
        return new Payload();
    }

    /**
     * Create an instance of {@link PullMessageResponse }
     * 
     */
    public PullMessageResponse createPullMessageResponse() {
        return new PullMessageResponse();
    }

    /**
     * Create an instance of {@link Manifest }
     * 
     */
    public Manifest createManifest() {
        return new Manifest();
    }

    /**
     * Create an instance of {@link PullMessage }
     * 
     */
    public PullMessage createPullMessage() {
        return new PullMessage();
    }

    /**
     * Create an instance of {@link ProcessMessage }
     * 
     */
    public ProcessMessage createProcessMessage() {
        return new ProcessMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayloadManifest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/webservices/2005/10/transport", name = "payloadManifest")
    public JAXBElement<PayloadManifest> createPayloadManifest(PayloadManifest value) {
        return new JAXBElement<PayloadManifest>(_PayloadManifest_QNAME, PayloadManifest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Attachment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.starstandards.org/webservices/2005/10/transport", name = "attachment")
    public JAXBElement<Attachment> createAttachment(Attachment value) {
        return new JAXBElement<Attachment>(_Attachment_QNAME, Attachment.class, null, value);
    }

}
