
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * WARNING:  This file was manually modified on 2/7/2013 to add internetPrice.  You cannot use the xsd in source control to generate java files
 * (unless you are sure that it is the latest xsd from Reynolds).
 */

/**
 * <p>Java class for PriceCost complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceCost">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="BestPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CodedCost" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CostPackAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="DMVAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="HbkAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="InvAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="InvenAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="InvenGlAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LicFee" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ListPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PdiAmt" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SlsCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SlsCost" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceCost")
public class PriceCost {

    @XmlAttribute(name = "BestPrice")
    protected String bestPrice;
    @XmlAttribute(name = "CodedCost")
    protected String codedCost;
    @XmlAttribute(name = "CostPackAmt")
    protected String costPackAmt;
    @XmlAttribute(name = "DMVAmt")
    protected String dmvAmt;
    @XmlAttribute(name = "HbkAmt")
    protected String hbkAmt;
    @XmlAttribute(name = "InvAmt")
    protected String invAmt;
    @XmlAttribute(name = "InvenAmt")
    protected String invenAmt;
    @XmlAttribute(name = "InvenGlAmt")
    protected String invenGlAmt;
    @XmlAttribute(name = "LicFee")
    protected String licFee;
    @XmlAttribute(name = "ListPrice")
    protected String listPrice;
    @XmlAttribute(name = "PdiAmt")
    protected String pdiAmt;
    @XmlAttribute(name = "SlsCode")
    protected String slsCode;
    @XmlAttribute(name = "SlsCost")
    protected String slsCost;
    @XmlAttribute(name = "InternetPrice")
    protected String internetPrice;

    /**
     * Gets the value of the bestPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBestPrice() {
        return bestPrice;
    }

    /**
     * Sets the value of the bestPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBestPrice(String value) {
        this.bestPrice = value;
    }

    /**
     * Gets the value of the codedCost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodedCost() {
        return codedCost;
    }

    /**
     * Sets the value of the codedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodedCost(String value) {
        this.codedCost = value;
    }

    /**
     * Gets the value of the costPackAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostPackAmt() {
        return costPackAmt;
    }

    /**
     * Sets the value of the costPackAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostPackAmt(String value) {
        this.costPackAmt = value;
    }

    /**
     * Gets the value of the dmvAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDMVAmt() {
        return dmvAmt;
    }

    /**
     * Sets the value of the dmvAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDMVAmt(String value) {
        this.dmvAmt = value;
    }

    /**
     * Gets the value of the hbkAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHbkAmt() {
        return hbkAmt;
    }

    /**
     * Sets the value of the hbkAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHbkAmt(String value) {
        this.hbkAmt = value;
    }

    /**
     * Gets the value of the invAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvAmt() {
        return invAmt;
    }

    /**
     * Sets the value of the invAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvAmt(String value) {
        this.invAmt = value;
    }

    /**
     * Gets the value of the invenAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvenAmt() {
        return invenAmt;
    }

    /**
     * Sets the value of the invenAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvenAmt(String value) {
        this.invenAmt = value;
    }

    /**
     * Gets the value of the invenGlAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvenGlAmt() {
        return invenGlAmt;
    }

    /**
     * Sets the value of the invenGlAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvenGlAmt(String value) {
        this.invenGlAmt = value;
    }

    /**
     * Gets the value of the licFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicFee() {
        return licFee;
    }

    /**
     * Sets the value of the licFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicFee(String value) {
        this.licFee = value;
    }

    /**
     * Gets the value of the listPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListPrice() {
        return listPrice;
    }

    /**
     * Sets the value of the listPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListPrice(String value) {
        this.listPrice = value;
    }

    /**
     * Gets the value of the pdiAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdiAmt() {
        return pdiAmt;
    }

    /**
     * Sets the value of the pdiAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdiAmt(String value) {
        this.pdiAmt = value;
    }

    /**
     * Gets the value of the slsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlsCode() {
        return slsCode;
    }

    /**
     * Sets the value of the slsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlsCode(String value) {
        this.slsCode = value;
    }

    /**
     * Gets the value of the slsCost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlsCost() {
        return slsCost;
    }

    /**
     * Sets the value of the slsCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlsCost(String value) {
        this.slsCost = value;
    }

    /**
     * Gets the value of the internetPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternetPrice() {
        return internetPrice;
    }

    /**
     * Sets the value of the internetPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternetPrice(String value) {
        this.internetPrice = value;
    }

}
