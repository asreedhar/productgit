
package biz.firstlook.service.dms.rd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MiscInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MiscInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}ROPOInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MemoLine1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MemoLine2" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PurchDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="WarrExpDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MiscInfo", propOrder = {
    "ropoInfo"
})
public class MiscInfo {

    @XmlElement(name = "ROPOInfo", required = true)
    protected List<ROPOInfo> ropoInfo;
    @XmlAttribute(name = "MemoLine1")
    protected String memoLine1;
    @XmlAttribute(name = "MemoLine2")
    protected String memoLine2;
    @XmlAttribute(name = "PurchDate")
    protected String purchDate;
    @XmlAttribute(name = "WarrExpDate")
    protected String warrExpDate;

    /**
     * Gets the value of the ropoInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ropoInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getROPOInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ROPOInfo }
     * 
     * 
     */
    public List<ROPOInfo> getROPOInfo() {
        if (ropoInfo == null) {
            ropoInfo = new ArrayList<ROPOInfo>();
        }
        return this.ropoInfo;
    }

    /**
     * Gets the value of the memoLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemoLine1() {
        return memoLine1;
    }

    /**
     * Sets the value of the memoLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemoLine1(String value) {
        this.memoLine1 = value;
    }

    /**
     * Gets the value of the memoLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemoLine2() {
        return memoLine2;
    }

    /**
     * Sets the value of the memoLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemoLine2(String value) {
        this.memoLine2 = value;
    }

    /**
     * Gets the value of the purchDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchDate() {
        return purchDate;
    }

    /**
     * Sets the value of the purchDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchDate(String value) {
        this.purchDate = value;
    }

    /**
     * Gets the value of the warrExpDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarrExpDate() {
        return warrExpDate;
    }

    /**
     * Sets the value of the warrExpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarrExpDate(String value) {
        this.warrExpDate = value;
    }

}
