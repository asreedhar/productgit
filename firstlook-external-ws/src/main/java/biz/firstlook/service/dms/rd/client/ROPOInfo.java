
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for ROPOInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ROPOInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="ROPONumber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ROPOType" default="R">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="P"/>
 *             &lt;enumeration value="R"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ROPOInfo")
public class ROPOInfo {

    @XmlAttribute(name = "ROPONumber", required = true)
    protected String ropoNumber;
    @XmlAttribute(name = "ROPOType")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String ropoType;

    /**
     * Gets the value of the ropoNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getROPONumber() {
        return ropoNumber;
    }

    /**
     * Sets the value of the ropoNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROPONumber(String value) {
        this.ropoNumber = value;
    }

    /**
     * Gets the value of the ropoType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getROPOType() {
        if (ropoType == null) {
            return "R";
        } else {
            return ropoType;
        }
    }

    /**
     * Sets the value of the ropoType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROPOType(String value) {
        this.ropoType = value;
    }

}
