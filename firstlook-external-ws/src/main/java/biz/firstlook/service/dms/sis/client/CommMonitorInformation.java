
package biz.firstlook.service.dms.sis.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CommMonitorInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommMonitorInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DiffInSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LastResponse" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PollingIntervalSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CurrentTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommMonitorInformation", propOrder = {
    "number",
    "diffInSeconds",
    "lastResponse",
    "pollingIntervalSeconds",
    "currentTime"
})
public class CommMonitorInformation {

    @XmlElement(name = "Number")
    protected int number;
    @XmlElement(name = "DiffInSeconds")
    protected int diffInSeconds;
    @XmlElement(name = "LastResponse", required = true)
    protected XMLGregorianCalendar lastResponse;
    @XmlElement(name = "PollingIntervalSeconds")
    protected int pollingIntervalSeconds;
    @XmlElement(name = "CurrentTime", required = true)
    protected XMLGregorianCalendar currentTime;

    /**
     * Gets the value of the number property.
     * 
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     */
    public void setNumber(int value) {
        this.number = value;
    }

    /**
     * Gets the value of the diffInSeconds property.
     * 
     */
    public int getDiffInSeconds() {
        return diffInSeconds;
    }

    /**
     * Sets the value of the diffInSeconds property.
     * 
     */
    public void setDiffInSeconds(int value) {
        this.diffInSeconds = value;
    }

    /**
     * Gets the value of the lastResponse property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastResponse() {
        return lastResponse;
    }

    /**
     * Sets the value of the lastResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastResponse(XMLGregorianCalendar value) {
        this.lastResponse = value;
    }

    /**
     * Gets the value of the pollingIntervalSeconds property.
     * 
     */
    public int getPollingIntervalSeconds() {
        return pollingIntervalSeconds;
    }

    /**
     * Sets the value of the pollingIntervalSeconds property.
     * 
     */
    public void setPollingIntervalSeconds(int value) {
        this.pollingIntervalSeconds = value;
    }

    /**
     * Gets the value of the currentTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCurrentTime() {
        return currentTime;
    }

    /**
     * Sets the value of the currentTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCurrentTime(XMLGregorianCalendar value) {
        this.currentTime = value;
    }

}
