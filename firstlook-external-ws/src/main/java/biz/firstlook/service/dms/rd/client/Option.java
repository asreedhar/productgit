
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Option complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Option">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.starstandards.org/STAR}OptionAccounting" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="FactAccCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FactAccDesc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Option", propOrder = {
    "optionAccounting"
})
public class Option {

    @XmlElement(name = "OptionAccounting")
    protected OptionAccounting optionAccounting;
    @XmlAttribute(name = "FactAccCode")
    protected String factAccCode;
    @XmlAttribute(name = "FactAccDesc")
    protected String factAccDesc;

    /**
     * Gets the value of the optionAccounting property.
     * 
     * @return
     *     possible object is
     *     {@link OptionAccounting }
     *     
     */
    public OptionAccounting getOptionAccounting() {
        return optionAccounting;
    }

    /**
     * Sets the value of the optionAccounting property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionAccounting }
     *     
     */
    public void setOptionAccounting(OptionAccounting value) {
        this.optionAccounting = value;
    }

    /**
     * Gets the value of the factAccCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactAccCode() {
        return factAccCode;
    }

    /**
     * Sets the value of the factAccCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactAccCode(String value) {
        this.factAccCode = value;
    }

    /**
     * Gets the value of the factAccDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactAccDesc() {
        return factAccDesc;
    }

    /**
     * Sets the value of the factAccDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactAccDesc(String value) {
        this.factAccDesc = value;
    }

}
