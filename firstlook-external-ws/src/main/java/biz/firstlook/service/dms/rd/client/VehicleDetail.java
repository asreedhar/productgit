
package biz.firstlook.service.dms.rd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for VehicleDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="Aircond" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="AlarmNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Axle" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Bodysize" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CategCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ChassisNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="DriveType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="EngNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="EngineConfig" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Fuel" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FuelEcon" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="KeyChipNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="KeyNo1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="KeyNo2" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="KeylessNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LicExpDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LicNo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="NewUsed" default="N">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="N"/>
 *             &lt;enumeration value="O"/>
 *             &lt;enumeration value="U"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="NoOfCyl" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="OdomReading" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Pwrstr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Transm" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Trim" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Turbo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="UnitsofMeasure" default="E">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="E"/>
 *             &lt;enumeration value="M"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="VehClass" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Weight" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Wheelbase" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleDetail")
public class VehicleDetail {

    @XmlAttribute(name = "Aircond")
    protected String aircond;
    @XmlAttribute(name = "AlarmNo")
    protected String alarmNo;
    @XmlAttribute(name = "Axle")
    protected String axle;
    @XmlAttribute(name = "Bodysize")
    protected String bodysize;
    @XmlAttribute(name = "CategCode")
    protected String categCode;
    @XmlAttribute(name = "ChassisNo")
    protected String chassisNo;
    @XmlAttribute(name = "DriveType")
    protected String driveType;
    @XmlAttribute(name = "EngNo")
    protected String engNo;
    @XmlAttribute(name = "EngineConfig")
    protected String engineConfig;
    @XmlAttribute(name = "Fuel")
    protected String fuel;
    @XmlAttribute(name = "FuelEcon")
    protected String fuelEcon;
    @XmlAttribute(name = "KeyChipNo")
    protected String keyChipNo;
    @XmlAttribute(name = "KeyNo1")
    protected String keyNo1;
    @XmlAttribute(name = "KeyNo2")
    protected String keyNo2;
    @XmlAttribute(name = "KeylessNo")
    protected String keylessNo;
    @XmlAttribute(name = "LicExpDate")
    protected String licExpDate;
    @XmlAttribute(name = "LicNo")
    protected String licNo;
    @XmlAttribute(name = "NewUsed")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String newUsed;
    @XmlAttribute(name = "NoOfCyl")
    protected String noOfCyl;
    @XmlAttribute(name = "OdomReading")
    protected String odomReading;
    @XmlAttribute(name = "Pwrstr")
    protected String pwrstr;
    @XmlAttribute(name = "Transm")
    protected String transm;
    @XmlAttribute(name = "Trim")
    protected String trim;
    @XmlAttribute(name = "Turbo")
    protected String turbo;
    @XmlAttribute(name = "UnitsofMeasure")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String unitsofMeasure;
    @XmlAttribute(name = "VehClass")
    protected String vehClass;
    @XmlAttribute(name = "Weight")
    protected String weight;
    @XmlAttribute(name = "Wheelbase")
    protected String wheelbase;

    /**
     * Gets the value of the aircond property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAircond() {
        return aircond;
    }

    /**
     * Sets the value of the aircond property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAircond(String value) {
        this.aircond = value;
    }

    /**
     * Gets the value of the alarmNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmNo() {
        return alarmNo;
    }

    /**
     * Sets the value of the alarmNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmNo(String value) {
        this.alarmNo = value;
    }

    /**
     * Gets the value of the axle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAxle() {
        return axle;
    }

    /**
     * Sets the value of the axle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAxle(String value) {
        this.axle = value;
    }

    /**
     * Gets the value of the bodysize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodysize() {
        return bodysize;
    }

    /**
     * Sets the value of the bodysize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodysize(String value) {
        this.bodysize = value;
    }

    /**
     * Gets the value of the categCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategCode() {
        return categCode;
    }

    /**
     * Sets the value of the categCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategCode(String value) {
        this.categCode = value;
    }

    /**
     * Gets the value of the chassisNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNo() {
        return chassisNo;
    }

    /**
     * Sets the value of the chassisNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNo(String value) {
        this.chassisNo = value;
    }

    /**
     * Gets the value of the driveType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriveType() {
        return driveType;
    }

    /**
     * Sets the value of the driveType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriveType(String value) {
        this.driveType = value;
    }

    /**
     * Gets the value of the engNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngNo() {
        return engNo;
    }

    /**
     * Sets the value of the engNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngNo(String value) {
        this.engNo = value;
    }

    /**
     * Gets the value of the engineConfig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineConfig() {
        return engineConfig;
    }

    /**
     * Sets the value of the engineConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineConfig(String value) {
        this.engineConfig = value;
    }

    /**
     * Gets the value of the fuel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuel() {
        return fuel;
    }

    /**
     * Sets the value of the fuel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuel(String value) {
        this.fuel = value;
    }

    /**
     * Gets the value of the fuelEcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelEcon() {
        return fuelEcon;
    }

    /**
     * Sets the value of the fuelEcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelEcon(String value) {
        this.fuelEcon = value;
    }

    /**
     * Gets the value of the keyChipNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyChipNo() {
        return keyChipNo;
    }

    /**
     * Sets the value of the keyChipNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyChipNo(String value) {
        this.keyChipNo = value;
    }

    /**
     * Gets the value of the keyNo1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyNo1() {
        return keyNo1;
    }

    /**
     * Sets the value of the keyNo1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyNo1(String value) {
        this.keyNo1 = value;
    }

    /**
     * Gets the value of the keyNo2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyNo2() {
        return keyNo2;
    }

    /**
     * Sets the value of the keyNo2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyNo2(String value) {
        this.keyNo2 = value;
    }

    /**
     * Gets the value of the keylessNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeylessNo() {
        return keylessNo;
    }

    /**
     * Sets the value of the keylessNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeylessNo(String value) {
        this.keylessNo = value;
    }

    /**
     * Gets the value of the licExpDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicExpDate() {
        return licExpDate;
    }

    /**
     * Sets the value of the licExpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicExpDate(String value) {
        this.licExpDate = value;
    }

    /**
     * Gets the value of the licNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicNo() {
        return licNo;
    }

    /**
     * Sets the value of the licNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicNo(String value) {
        this.licNo = value;
    }

    /**
     * Gets the value of the newUsed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewUsed() {
        if (newUsed == null) {
            return "N";
        } else {
            return newUsed;
        }
    }

    /**
     * Sets the value of the newUsed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewUsed(String value) {
        this.newUsed = value;
    }

    /**
     * Gets the value of the noOfCyl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoOfCyl() {
        return noOfCyl;
    }

    /**
     * Sets the value of the noOfCyl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoOfCyl(String value) {
        this.noOfCyl = value;
    }

    /**
     * Gets the value of the odomReading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOdomReading() {
        return odomReading;
    }

    /**
     * Sets the value of the odomReading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOdomReading(String value) {
        this.odomReading = value;
    }

    /**
     * Gets the value of the pwrstr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwrstr() {
        return pwrstr;
    }

    /**
     * Sets the value of the pwrstr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwrstr(String value) {
        this.pwrstr = value;
    }

    /**
     * Gets the value of the transm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransm() {
        return transm;
    }

    /**
     * Sets the value of the transm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransm(String value) {
        this.transm = value;
    }

    /**
     * Gets the value of the trim property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrim() {
        return trim;
    }

    /**
     * Sets the value of the trim property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrim(String value) {
        this.trim = value;
    }

    /**
     * Gets the value of the turbo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTurbo() {
        return turbo;
    }

    /**
     * Sets the value of the turbo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTurbo(String value) {
        this.turbo = value;
    }

    /**
     * Gets the value of the unitsofMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitsofMeasure() {
        if (unitsofMeasure == null) {
            return "E";
        } else {
            return unitsofMeasure;
        }
    }

    /**
     * Sets the value of the unitsofMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitsofMeasure(String value) {
        this.unitsofMeasure = value;
    }

    /**
     * Gets the value of the vehClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehClass() {
        return vehClass;
    }

    /**
     * Sets the value of the vehClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehClass(String value) {
        this.vehClass = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeight(String value) {
        this.weight = value;
    }

    /**
     * Gets the value of the wheelbase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWheelbase() {
        return wheelbase;
    }

    /**
     * Sets the value of the wheelbase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWheelbase(String value) {
        this.wheelbase = value;
    }

}
