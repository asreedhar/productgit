
package biz.firstlook.service.insight;

import javax.xml.bind.annotation.XmlEnum;


/**
 * <p>Java class for light.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="light">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GREEN"/>
 *     &lt;enumeration value="YELLOW"/>
 *     &lt;enumeration value="RED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum Light {

    GREEN,
    RED,
    YELLOW;

    public String value() {
        return name();
    }

    public static Light fromValue(String v) {
        return valueOf(v);
    }

}
