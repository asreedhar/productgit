
package biz.firstlook.service.insight.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import biz.firstlook.service.insight.GetInsightRequest;
import biz.firstlook.service.insight.GetInsightResponse;

@WebService(name = "PerformanceInsightWebService", targetNamespace = "http://www.firstlook.biz/service/insight/v1")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface PerformanceInsightWebService {


    @WebMethod(operationName = "getInsights", action = "https://preprod.firstlook.biz/firstlook-external-ws/services/PerformanceInsightWebService/GetInsights")
    @WebResult(name = "getInsightResponse", targetNamespace = "http://www.firstlook.biz/service/insight")
    public GetInsightResponse getInsights(
        @WebParam(name = "getInsightRequest", targetNamespace = "http://www.firstlook.biz/service/insight")
        GetInsightRequest getInsightRequest);

}
