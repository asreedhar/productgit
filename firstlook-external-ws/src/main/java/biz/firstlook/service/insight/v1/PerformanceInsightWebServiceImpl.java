package biz.firstlook.service.insight.v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.jws.WebService;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.log4j.Logger;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightService;
import biz.firstlook.commons.services.insights.InsightServiceException;
import biz.firstlook.commons.services.insights.InsightTypeEnum;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogMultiEntry;
import biz.firstlook.service.authenticate.PartnerAuthenticationException;
import biz.firstlook.service.authenticate.PartnerAuthenticator;
import biz.firstlook.service.insight.Credentials;
import biz.firstlook.service.insight.ExitCode;
import biz.firstlook.service.insight.GetInsightRequest;
import biz.firstlook.service.insight.GetInsightResponse;
import biz.firstlook.service.insight.Light;
import biz.firstlook.service.insight.ModelPerformance;
import biz.firstlook.service.insight.PerformanceData;
import biz.firstlook.service.insight.TrimPerformance;
import biz.firstlook.service.insight.YearPerformance;
import biz.firstlook.service.insight.support.WebServiceException;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.DealerUpgrade;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.PerformanceCategoryEnum;
import biz.firstlook.transact.persist.persistence.DealerDAO;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.DealerUpgradeDAO;
import biz.firstlook.transact.persist.persistence.InventoryDAO;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;
import biz.firstlook.transact.persist.service.performance.PerformanceMetricsRetriever;

@WebService(serviceName = "PerformanceInsightWebService", targetNamespace = "http://www.firstlook.biz/service/insight/v1", endpointInterface = "biz.firstlook.service.insight.v1.PerformanceInsightWebService")
public class PerformanceInsightWebServiceImpl implements PerformanceInsightWebService {

	protected static Logger logger = Logger.getLogger(PerformanceInsightWebServiceImpl.class);

	private final static Integer NUMBER_OF_WEEKS = 26;

	private PartnerAuthenticator partnerAuthenticator;
	private PerformanceMetricsRetriever performanceMetricsRetriever;
	private DealerDAO dealerDAO;
	private DealerUpgradeDAO dealerUpgradeDAO;
	private IVehicleCatalogService vehicleCatalogService;
	private InsightService insightService;
	private IAppraisalService appraisalService;
	private DealerPreferenceDAO dealerPreferenceDAO;
	private InventoryDAO inventoryDAO;

	public PerformanceInsightWebServiceImpl() {
		super();
	}
	
	public GetInsightResponse getInsights(GetInsightRequest getInsightRequest) {

		GetInsightResponse response = new GetInsightResponse();
		response.setExitCode(ExitCode.SUCCESS);

		String errorMessage = "An error occured when submitting the request to the Performance Insights Web Service.";
		try {
			// authenticate
			BusinessUnitCredential businessUnitCredential = authenticate(getInsightRequest
					.getCredentials());

			// get basic info
			String vin = getInsightRequest.getVin();
			int dealerId = businessUnitCredential.getBusinessUnitId();
			DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId(dealerId);
			
			// using static value 9 for Annual ROI
			// Can refactor when we do the dealer upgrade effort 
			DealerUpgrade upgrade = dealerUpgradeDAO.findUpgrade(dealerId, 9);
			boolean showAnnualROIInsight = false;
			if (upgrade != null && upgrade.isActive()) {
				showAnnualROIInsight = true;
			}
			
			// current specs ask that the AppraisalExist flag communicate
			// whether a PendingAppraisal can be submitted by this dealer for the VIN
			if (!isVinValidForPendingAppraisalSubmission(vin, dealerId, dealerPreference.getSearchInactiveInventoryDaysBackThreshold())) {
					response.setAppraisalExists(true);
			} else {
					response.setAppraisalExists(false);
			}
			
			int mileage = getInsightRequest.getMileage().intValue();
			VehicleCatalogMultiEntry vcMultiEntry = vehicleCatalogService.retrieveVehicleCatalogMultiEntry(vin);
			int year = vcMultiEntry.getModelYear().intValue();
			int groupingDescriptionId = vcMultiEntry.getGroupingDescriptionId(); 

			// Set Performance metrics
			setPerformanceMetrics(response, vin, dealerId, mileage, vcMultiEntry);

			// Set Insights
			setInsights(response, dealerId, mileage, year, groupingDescriptionId, showAnnualROIInsight);

			// set basics header info
			response.setVin(vin);
			response.setDealerName(dealerDAO.retrieveByDealerId(dealerId).getShortName());
		} catch (WebServiceException wse) {
			logger
					.error(
							"A web service expection was thrown when attempting to retrieve Performance Insights from the Web Service.",
							wse);
			logger.error("Exit Code : " + wse.getExitCode().value() + " , Msg : "
					+ wse.getMessage());
			response.setExitCode(wse.getExitCode());
			response.setMessage(wse.getMessage());
		} catch (Exception e) {
			logger.error(errorMessage, e);
			logger.error("Exit Code : " + ExitCode.UNABLE_TO_DETERMINE_INSIGHTS_FOR_SPECIFIED_VIN
					+ " , Msg : " + e.getMessage());
			response.setExitCode(ExitCode.UNABLE_TO_DETERMINE_INSIGHTS_FOR_SPECIFIED_VIN);
			response.setMessage("Unable to determine insights for specified VIN");
		}

		return response;
	}

	/**
	 * Performs a check using the same criteria as the Wireless/CRM Webservice to determine
	 * if a PendingAppraisal can be submitted for this VIN by this dealer. Returns true if a
	 * PendingAppraisal can be submitted and false otherwise.
	 * 
	 * @param vin
	 * @param dealerId
	 * @throws WebServiceException
	 */
	private boolean isVinValidForPendingAppraisalSubmission(String vin, int dealerId, int daysBackThreshold) throws WebServiceException {

		Inventory inventory = inventoryDAO.searchVinInActiveInventory(dealerId, vin);
		if( inventory != null ) {
			return false;
		}
		
		inventory = inventoryDAO.searchVinInInactiveInventory(dealerId, vin, daysBackThreshold);
		if( inventory != null ) {
			return false;
		}
		
		List<IAppraisal> appraisals = appraisalService.findBy(dealerId, vin, daysBackThreshold);
		if (appraisals != null && !appraisals.isEmpty()) {
			// get appraisal - there should only be one
			biz.firstlook.transact.persist.service.appraisal.IAppraisal appraisal = appraisals.get(0);
			if (appraisal.getAppraisalSource().equals(AppraisalSource.THE_EDGE)) {
				return false;
			}
		}
		
		return true;
	}
	

	/**
	 * Call the Stored Proc GetPerformanceServiceMultiSet to retreive the
	 * Performance Metrics for Year, Model and Trim levels.
	 * 
	 * These results are then packed onto the GetInsightResponse.
	 * 
	 * @param response
	 * @param vin
	 * @param dealerId
	 * @param mileage
	 * @param vehicle 
	 */
	@SuppressWarnings("unchecked")
	private void setPerformanceMetrics(GetInsightResponse response, String vin, int dealerId,
			int mileage, VehicleCatalogMultiEntry vcMultiEntry) {
		MultiValueMap performanceMetrics = performanceMetricsRetriever.retrievePerformanceMetrics(
				dealerId + "", vin, mileage, NUMBER_OF_WEEKS);

		// get Model Metrics
		ModelPerformance modelPerformance = new ModelPerformance();
		ArrayList<biz.firstlook.transact.persist.model.PerformanceData> modelMetricsFromDB = (ArrayList) performanceMetrics
				.get(PerformanceCategoryEnum.MODEL);
		modelPerformance.setPerformanceData(convertPerformanceData(modelMetricsFromDB.get(0)));
		response.setModelPerformance(modelPerformance);
		// description for modelLevel is the Grouping Description
		response.setGroupingDescription(modelMetricsFromDB.get(0).getDescription());

		// get Year Metrics
		YearPerformance yearPerformance = new YearPerformance();
		ArrayList<biz.firstlook.transact.persist.model.PerformanceData> yearMetricsFromDB = (ArrayList) performanceMetrics
				.get(PerformanceCategoryEnum.YEAR);
		yearPerformance.setYear(yearMetricsFromDB.get(0).getDescription());
		yearPerformance.setPerformanceData(convertPerformanceData(yearMetricsFromDB.get(0)));
		response.setYearPerformance(yearPerformance);

		
		// We return trim based performance info for each trim in the Catalog that matches this vin
		List<String> trims = vcMultiEntry.getTrims();
		
		// get Trim Metrics
		ArrayList<biz.firstlook.transact.persist.model.PerformanceData> trimMetricsFromDB = (ArrayList) performanceMetrics
				.get(PerformanceCategoryEnum.TRIM);
		for (biz.firstlook.transact.persist.model.PerformanceData metricFromDB : trimMetricsFromDB) {
			// NK - Temp add. We filter out any trim level results that do not match 
			//      a trim from the catalog
			if (isValidTrimDescription(trims, metricFromDB.getDescription())) {
				TrimPerformance trimPerformance = new TrimPerformance();
				trimPerformance.setPerformanceData(convertPerformanceData(metricFromDB));
				trimPerformance.setDescription(metricFromDB.getDescription());
				response.getTrimPerformance().add(trimPerformance);
			}
		}
	}

	/**
	 * 
	 * Returns true if the descriptions exists in the arrayList of trims and false otherwise.
	 *  
	 * @param trims
	 * @param description
	 * @return
	 */
	private boolean isValidTrimDescription(List<String> trims, String description) {
		boolean result = false;
		for (String trim : trims) {
			if (trim.trim().equalsIgnoreCase(description.trim())) {
				result = true;
				break;
			}
		}
		return result;
	}

	private void setInsights(GetInsightResponse response, int dealerId, int mileage, int year,
			int groupingDescriptionId, boolean showAnnualROIInsight) {

		InsightParameters insightParameters = new InsightParameters(dealerId, groupingDescriptionId, InventoryTypeEnum.USED.ordinal(), NUMBER_OF_WEEKS, null, year, mileage);
		try {
			List<Insight> insights = Collections.emptyList();
			if (getInsightService() == null) {
				logger.error("InsightService is null. Check the spring configuration!");
			}
			else {
				try {
					insights = getInsightService().getInsights(insightParameters);
				}
				catch (InsightServiceException ise) {
					logger.error("Failed to get Insights", ise);
				}
			}
			
			if (!insights.isEmpty()) {
				for (Insight insight : insights) {
					// traffic light insights are special cases. No natural language
					// Insight String
					if (insight.getInsightType() != InsightTypeEnum.TRAFFIC_LIGHT) {
						if ( (insight.getInsightType() == InsightTypeEnum.ANNUAL_ROI) && (!showAnnualROIInsight) ) {
							// don't show the annual roi insight if they don't have the upgrade
							continue;
						}
						biz.firstlook.service.insight.Insights in = new biz.firstlook.service.insight.Insights();
						in.setInsightString(insight.toString());
						response.getInsights().add(in);
					} else {
						response.setLightIndicator(getLightIndicator(insight));
					}
				}
			}
		}
		catch (RuntimeException e) {
			logger.error("Error whilst loading Insights for :" + insightParameters);
		}
	}

	private Light getLightIndicator(Insight insight) {
		Light result = null;
		switch (insight.getInsightValue()) {
		case 1:
			result = Light.RED;
			break;
		case 2:
			result = Light.YELLOW;
			break;
		case 3:
			result = Light.GREEN;
			break;
		}
		return result;
	}

	private PerformanceData convertPerformanceData(
			biz.firstlook.transact.persist.model.PerformanceData performanceDataFromDB) {
		PerformanceData result = new PerformanceData();
		result.setAverageDaysToSale(new BigInteger(performanceDataFromDB.getAverageDaysToSale()
				.toString()));
		result.setAverageMileage(new BigInteger(performanceDataFromDB.getAverageMileage()
				.toString()));
		result.setAverageRetailGrossProfit(new BigInteger(performanceDataFromDB
				.getAverageRetailGrossProfit().toString()));
		result.setNoSales(new BigInteger(performanceDataFromDB.getNoSales().toString()));
		result.setUnitsInStock(new BigInteger(performanceDataFromDB.getUnitsInStock().toString()));
		result.setUnitsSold(new BigInteger(performanceDataFromDB.getUnitsSold().toString()));
		return result;
	}

	protected BusinessUnitCredential authenticate(Credentials credential)
			throws WebServiceException {
		if(credential == null) {
			throw new WebServiceException(ExitCode.AUTHENTICATION_FAILED,"Missing credentials.");
		}
		
		try {
			return partnerAuthenticator.getBusinessUnitCredential( 
					credential.getDealerId(), 
					Integer.parseInt( credential.getPartnerId() ), 
					credential.getPasscode() );
		} catch (NumberFormatException e) {
			throw new WebServiceException(ExitCode.AUTHENTICATION_FAILED, "PartnerId invalid.");
		} catch (PartnerAuthenticationException e) {
			throw new WebServiceException(ExitCode.AUTHENTICATION_FAILED, e.getMessage());
		}
	}

	public void setPerformanceMetricsRetriever(
			PerformanceMetricsRetriever performanceMetricsRetriever) {
		this.performanceMetricsRetriever = performanceMetricsRetriever;
	}

	public void setPartnerAuthenticator(PartnerAuthenticator partnerAuthenticator) {
		this.partnerAuthenticator = partnerAuthenticator;
	}

	public void setDealerDAO(DealerDAO dealerDAO) {
		this.dealerDAO = dealerDAO;
	}

	public InsightService getInsightService() {
		return insightService;
	}

	public void setInsightService(InsightService insightService) {
		this.insightService = insightService;
	}

	public void setDealerPreferenceDAO(DealerPreferenceDAO dealerPreferenceDAO) {
		this.dealerPreferenceDAO = dealerPreferenceDAO;
	}

	public void setInventoryDAO(InventoryDAO inventoryDAO) {
		this.inventoryDAO = inventoryDAO;
	}

	public void setAppraisalService(IAppraisalService appraisalService) {
		this.appraisalService = appraisalService;
	}

	public void setDealerUpgradeDAO(DealerUpgradeDAO dealerUpgradeDAO) {
		this.dealerUpgradeDAO = dealerUpgradeDAO;
	}

	public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
	{
		this.vehicleCatalogService = vehicleCatalogService;
	}
}
