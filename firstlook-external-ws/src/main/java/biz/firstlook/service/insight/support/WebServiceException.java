package biz.firstlook.service.insight.support;


import biz.firstlook.service.insight.ExitCode;

public class WebServiceException extends Exception {

	private static final long serialVersionUID = 2101804330148365069L;

	private String errorMessage;
	private ExitCode exitCode;
	
	public WebServiceException(ExitCode code, String errorMessage) {
		this.exitCode = code;
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ExitCode getExitCode() {
		return exitCode;
	}
	public void setExitCode(ExitCode exitCode) {
		this.exitCode = exitCode;
	}
	
}
