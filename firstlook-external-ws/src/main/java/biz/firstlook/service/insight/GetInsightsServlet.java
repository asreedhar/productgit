package biz.firstlook.service.insight;

import java.io.StringReader;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.codehaus.xfire.spring.ServiceBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.service.insight.v1.PerformanceInsightWebService;

public class GetInsightsServlet implements Controller {

	private static final long serialVersionUID = -7846087503599101463L;
	private ServiceBean performanceInsightWebService;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String xmlResponse = "";
		// get xml off request
		String insightResquestXML = request.getParameter("insightRequest");
		GetInsightResponse insightResponse = null;
		try {
			GetInsightRequest insightRequest = parseInsightRequest(insightResquestXML);
			PerformanceInsightWebService service = (PerformanceInsightWebService) performanceInsightWebService.getServiceBean();
			insightResponse = service.getInsights(insightRequest);
		} catch (Exception e) {
			xmlResponse = getErrorXML(ExitCode.UNABLE_TO_DETERMINE_INSIGHTS_FOR_SPECIFIED_VIN, "Request for insight unable to be processed.");
			return new ModelAndView("postResponse", "response", xmlResponse);
		}
		xmlResponse = parseResponseToXML(insightResponse);
		return new ModelAndView("postResponse", "response", xmlResponse);
	}
	
	private String getErrorXML(ExitCode exitCode, String Message) {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
				+ "<getInsightResponse xmlns=\"http://www.firstlook.biz/service/insight\">"
				+ "<exitCode>" + exitCode.value() + "</exitCode>" + "<message>" + Message
				+ "</message>" + "</getInsightResponse>";
	}

	private String parseResponseToXML(Object response) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance("biz.firstlook.service.insight");
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(response, writer);
		return writer.getBuffer().toString();

	}

	private GetInsightRequest parseInsightRequest(String requestXML) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance("biz.firstlook.service.insight");
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		return (GetInsightRequest)unmarshaller.unmarshal(new StringReader(requestXML));
	}

	public void setPerformanceInsightWebService(
			ServiceBean performanceInsightWebService) {
		this.performanceInsightWebService = performanceInsightWebService;
	}





}
