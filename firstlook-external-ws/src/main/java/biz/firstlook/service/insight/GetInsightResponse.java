
package biz.firstlook.service.insight;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exitCode" type="{http://www.firstlook.biz/service/insight}exitCode"/>
 *         &lt;element name="AppraisalExists" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dealerName" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element name="vin" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element name="groupingDescription" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element name="lightIndicator" type="{http://www.firstlook.biz/service/insight}light"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/insight}insights" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/insight}modelPerformance"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/insight}yearPerformance"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/insight}trimPerformance" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exitCode",
    "appraisalExists",
    "message",
    "dealerName",
    "vin",
    "groupingDescription",
    "lightIndicator",
    "insights",
    "modelPerformance",
    "yearPerformance",
    "trimPerformance"
})
@XmlRootElement(name = "getInsightResponse")
public class GetInsightResponse {

    @XmlElement(required = true)
    protected ExitCode exitCode;
    @XmlElement(name = "AppraisalExists")
    protected boolean appraisalExists;
    protected String message;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String dealerName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String vin;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String groupingDescription;
    @XmlElement(required = true)
    protected Light lightIndicator;
    @XmlElement(required = true)
    protected List<Insights> insights;
    @XmlElement(required = true)
    protected ModelPerformance modelPerformance;
    @XmlElement(required = true)
    protected YearPerformance yearPerformance;
    @XmlElement(required = true)
    protected List<TrimPerformance> trimPerformance;

    /**
     * Gets the value of the exitCode property.
     * 
     * @return
     *     possible object is
     *     {@link ExitCode }
     *     
     */
    public ExitCode getExitCode() {
        return exitCode;
    }

    /**
     * Sets the value of the exitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExitCode }
     *     
     */
    public void setExitCode(ExitCode value) {
        this.exitCode = value;
    }

    /**
     * Gets the value of the appraisalExists property.
     * 
     */
    public boolean isAppraisalExists() {
        return appraisalExists;
    }

    /**
     * Sets the value of the appraisalExists property.
     * 
     */
    public void setAppraisalExists(boolean value) {
        this.appraisalExists = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the dealerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerName() {
        return dealerName;
    }

    /**
     * Sets the value of the dealerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerName(String value) {
        this.dealerName = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVin(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the groupingDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupingDescription() {
        return groupingDescription;
    }

    /**
     * Sets the value of the groupingDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupingDescription(String value) {
        this.groupingDescription = value;
    }

    /**
     * Gets the value of the lightIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Light }
     *     
     */
    public Light getLightIndicator() {
        return lightIndicator;
    }

    /**
     * Sets the value of the lightIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Light }
     *     
     */
    public void setLightIndicator(Light value) {
        this.lightIndicator = value;
    }

    /**
     * Gets the value of the insights property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insights property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsights().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Insights }
     * 
     * 
     */
    public List<Insights> getInsights() {
        if (insights == null) {
            insights = new ArrayList<Insights>();
        }
        return this.insights;
    }

    /**
     * Gets the value of the modelPerformance property.
     * 
     * @return
     *     possible object is
     *     {@link ModelPerformance }
     *     
     */
    public ModelPerformance getModelPerformance() {
        return modelPerformance;
    }

    /**
     * Sets the value of the modelPerformance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelPerformance }
     *     
     */
    public void setModelPerformance(ModelPerformance value) {
        this.modelPerformance = value;
    }

    /**
     * Gets the value of the yearPerformance property.
     * 
     * @return
     *     possible object is
     *     {@link YearPerformance }
     *     
     */
    public YearPerformance getYearPerformance() {
        return yearPerformance;
    }

    /**
     * Sets the value of the yearPerformance property.
     * 
     * @param value
     *     allowed object is
     *     {@link YearPerformance }
     *     
     */
    public void setYearPerformance(YearPerformance value) {
        this.yearPerformance = value;
    }

    /**
     * Gets the value of the trimPerformance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trimPerformance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrimPerformance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TrimPerformance }
     * 
     * 
     */
    public List<TrimPerformance> getTrimPerformance() {
        if (trimPerformance == null) {
            trimPerformance = new ArrayList<TrimPerformance>();
        }
        return this.trimPerformance;
    }

}
