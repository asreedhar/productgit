
package biz.firstlook.service.insight;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.firstlook.biz/service/insight}performanceData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "performanceData"
})
@XmlRootElement(name = "modelPerformance")
public class ModelPerformance {

    @XmlElement(required = true)
    protected PerformanceData performanceData;

    /**
     * Gets the value of the performanceData property.
     * 
     * @return
     *     possible object is
     *     {@link PerformanceData }
     *     
     */
    public PerformanceData getPerformanceData() {
        return performanceData;
    }

    /**
     * Sets the value of the performanceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PerformanceData }
     *     
     */
    public void setPerformanceData(PerformanceData value) {
        this.performanceData = value;
    }

}
