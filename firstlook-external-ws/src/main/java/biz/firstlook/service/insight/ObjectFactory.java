
package biz.firstlook.service.insight;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.service.insight package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.service.insight
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link YearPerformance }
     * 
     */
    public YearPerformance createYearPerformance() {
        return new YearPerformance();
    }

    /**
     * Create an instance of {@link GetInsightRequest }
     * 
     */
    public GetInsightRequest createGetInsightRequest() {
        return new GetInsightRequest();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link GetInsightResponse }
     * 
     */
    public GetInsightResponse createGetInsightResponse() {
        return new GetInsightResponse();
    }

    /**
     * Create an instance of {@link Insights }
     * 
     */
    public Insights createInsights() {
        return new Insights();
    }

    /**
     * Create an instance of {@link PerformanceData }
     * 
     */
    public PerformanceData createPerformanceData() {
        return new PerformanceData();
    }

    /**
     * Create an instance of {@link ModelPerformance }
     * 
     */
    public ModelPerformance createModelPerformance() {
        return new ModelPerformance();
    }

    /**
     * Create an instance of {@link TrimPerformance }
     * 
     */
    public TrimPerformance createTrimPerformance() {
        return new TrimPerformance();
    }

}
