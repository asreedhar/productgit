
package biz.firstlook.service.insight;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="averageRetailGrossProfit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="unitsSold" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="averageDaysToSale" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="averageMileage" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="noSales" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="unitsInStock" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "averageRetailGrossProfit",
    "unitsSold",
    "averageDaysToSale",
    "averageMileage",
    "noSales",
    "unitsInStock"
})
@XmlRootElement(name = "performanceData")
public class PerformanceData {

    @XmlElement(required = true)
    protected BigInteger averageRetailGrossProfit;
    @XmlElement(required = true)
    protected BigInteger unitsSold;
    @XmlElement(required = true)
    protected BigInteger averageDaysToSale;
    @XmlElement(required = true)
    protected BigInteger averageMileage;
    @XmlElement(required = true)
    protected BigInteger noSales;
    @XmlElement(required = true)
    protected BigInteger unitsInStock;

    /**
     * Gets the value of the averageRetailGrossProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageRetailGrossProfit() {
        return averageRetailGrossProfit;
    }

    /**
     * Sets the value of the averageRetailGrossProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageRetailGrossProfit(BigInteger value) {
        this.averageRetailGrossProfit = value;
    }

    /**
     * Gets the value of the unitsSold property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnitsSold() {
        return unitsSold;
    }

    /**
     * Sets the value of the unitsSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnitsSold(BigInteger value) {
        this.unitsSold = value;
    }

    /**
     * Gets the value of the averageDaysToSale property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageDaysToSale() {
        return averageDaysToSale;
    }

    /**
     * Sets the value of the averageDaysToSale property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageDaysToSale(BigInteger value) {
        this.averageDaysToSale = value;
    }

    /**
     * Gets the value of the averageMileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageMileage() {
        return averageMileage;
    }

    /**
     * Sets the value of the averageMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageMileage(BigInteger value) {
        this.averageMileage = value;
    }

    /**
     * Gets the value of the noSales property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNoSales() {
        return noSales;
    }

    /**
     * Sets the value of the noSales property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNoSales(BigInteger value) {
        this.noSales = value;
    }

    /**
     * Gets the value of the unitsInStock property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnitsInStock() {
        return unitsInStock;
    }

    /**
     * Sets the value of the unitsInStock property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnitsInStock(BigInteger value) {
        this.unitsInStock = value;
    }

}
