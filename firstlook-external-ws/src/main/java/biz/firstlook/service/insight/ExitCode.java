
package biz.firstlook.service.insight;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for exitCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="exitCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Success"/>
 *     &lt;enumeration value="Invalid Vin"/>
 *     &lt;enumeration value="Authentication Failed"/>
 *     &lt;enumeration value="Schema Invalid"/>
 *     &lt;enumeration value="Unable to determine insights for specified vin"/>
 *     &lt;enumeration value="Missing required attributes on request"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum ExitCode {

    @XmlEnumValue("Authentication Failed")
    AUTHENTICATION_FAILED("Authentication Failed"),
    @XmlEnumValue("Invalid Vin")
    INVALID_VIN("Invalid Vin"),
    @XmlEnumValue("Missing required attributes on request")
    MISSING_REQUIRED_ATTRIBUTES_ON_REQUEST("Missing required attributes on request"),
    @XmlEnumValue("Schema Invalid")
    SCHEMA_INVALID("Schema Invalid"),
    @XmlEnumValue("Success")
    SUCCESS("Success"),
    @XmlEnumValue("Unable to determine insights for specified vin")
    UNABLE_TO_DETERMINE_INSIGHTS_FOR_SPECIFIED_VIN("Unable to determine insights for specified vin");
    private final String value;

    ExitCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExitCode fromValue(String v) {
        for (ExitCode c: ExitCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
