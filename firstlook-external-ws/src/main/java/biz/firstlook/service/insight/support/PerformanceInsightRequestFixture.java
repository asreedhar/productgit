package biz.firstlook.service.insight.support;

import java.math.BigInteger;

import biz.firstlook.service.insight.Credentials;
import biz.firstlook.service.insight.GetInsightRequest;
import biz.firstlook.service.insight.ObjectFactory;

public class PerformanceInsightRequestFixture {

	private final static ObjectFactory factory = new ObjectFactory();

	public static GetInsightRequest createGetInsightRequest() {
		GetInsightRequest request = factory.createGetInsightRequest();
	
		Credentials creds = factory.createCredentials();
		creds.setDealerId( "WINDYCIT01" );
		creds.setPartnerId( "5" );
		creds.setPasscode( "ba@$o243" );
		
		request.setMileage(new BigInteger("1000"));
		request.setCredentials(creds);
		request.setVin("1FMRU186XXLB07359");
		
		return request;
	}
	
}
