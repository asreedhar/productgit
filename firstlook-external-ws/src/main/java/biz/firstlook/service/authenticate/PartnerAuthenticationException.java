package biz.firstlook.service.authenticate;


public class PartnerAuthenticationException extends Exception {

	private static final long serialVersionUID = -3461709627079102255L;
	
	public PartnerAuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}

	public PartnerAuthenticationException(String message) {
		super(message);
	}
}
