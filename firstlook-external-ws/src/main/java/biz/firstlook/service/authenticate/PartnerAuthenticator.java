package biz.firstlook.service.authenticate;

import java.text.MessageFormat;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.persistence.DealerDAO;
import biz.firstlook.transact.persist.persistence.IDealerDAO;

public class PartnerAuthenticator {
	
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
private IDealerDAO dealerDAO;

/**
 * 
 * @param dealerCode
 * @param partnerId
 * @param password
 * @return <code>{@link BusinessUnitCredential}</code>.  Never null!
 * @throws PartnerAuthenticationException <ul>
 * 			<li>{@link Dealership} is not found for the dealerCode</li> 
 * 			<li>the partnerId was not setup for the Dealership</li> 
 * 			<li>and/or the supplied password does not match the password stored.</li>
 * 			</ul>
 */
public BusinessUnitCredential getBusinessUnitCredential(String dealerCode, Integer partnerId, String password) 
	throws PartnerAuthenticationException {
	
	Dealership dealer = dealerDAO.retrieveByDealerCode( dealerCode );
	if (dealer == null) {
		throw new PartnerAuthenticationException(
				MessageFormat.format("DealerId {0} not found, please check the zeros and letter O's!", dealerCode));
	}
	
	BusinessUnitCredential creds = businessUnitCredentialDAO.findCredentialByBusinessIdAndCredentialTypeId(dealer.getDealerId(), CredentialType.convertFromPartnerId(partnerId));
	if(creds == null) {
		throw new PartnerAuthenticationException(
				MessageFormat.format("PartnerId {0} not associated with DealerId {1}", partnerId, dealerCode));
	} else {
		if(password != null) {
			//we used to be querying the dealerid, partnerid, and password from SQL Server 2005, so it's case insensitive.
			if(!password.equalsIgnoreCase(creds.getPassword())) {
				throw new PartnerAuthenticationException("Supplied password incorrect.");
			}
		}
	}
		
	return creds;
}

public BusinessUnitCredential getReynoldsBusinessUnitCredential(String dealerNumber,
			Integer areaNumber, Integer storeNumber, Integer credentialTypeId) {

		return businessUnitCredentialDAO.findByReynoldsMapping(dealerNumber, areaNumber, storeNumber, credentialTypeId);
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

public void setDealerDAO( DealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}
	
}
