package biz.firstlook.client.dms.rd.util;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.util.STAXUtils;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.stax.W3CDOMStreamReader;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class ReynoldsDirectResponseHandler extends DOMInHandler {
	private String transactionStatus;
	private Integer transactionStatusCode = 0; //success
	private String failureMessage = null;
	private static Logger log = Logger.getLogger(ReynoldsDirectResponseHandler.class);
	@Override
	public void invoke(MessageContext context) throws Exception {			
		Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        dbf.setIgnoringComments(false);
        dbf.setIgnoringElementContentWhitespace(false);
        dbf.setNamespaceAware(true);
        dbf.setCoalescing(false);
        
        doc = STAXUtils.read(dbf.newDocumentBuilder(), context.getInMessage().getXMLStreamReader(), false);
        
        StringWriter sw = new StringWriter();
        XMLSerializer ser = new XMLSerializer(sw, new OutputFormat(doc));
        ser.serialize(doc.getDocumentElement());
        
        String response = sw.toString().replaceFirst("\n", "");
       
        log.info("XML from Reynolds: " + response);
        
        
        context.getInMessage().setProperty(DOM_MESSAGE, doc);
        context.getInMessage().setXMLStreamReader(new W3CDOMStreamReader(doc.getDocumentElement()));
        
        NodeList nl = doc.getElementsByTagName("GenTransStatus");
        Node genTransStatusNode =  nl.item(0);
        if(genTransStatusNode!= null){
        	NamedNodeMap namedNodeMap = genTransStatusNode.getAttributes();
        
        	Node status = namedNodeMap.getNamedItem("Status");
        	if(status != null)
        		transactionStatus = status.getNodeValue();
        
        	Node statusCode = namedNodeMap.getNamedItem("StatusCode"); //there is a statusCode on failure
        	if(statusCode != null){
        		transactionStatusCode = Integer.parseInt(statusCode.getNodeValue());
        		failureMessage = genTransStatusNode.getFirstChild().getNodeValue();
        	}
        }
        else{
        	//Confirm BOD response
        	nl = doc.getElementsByTagName("NounFailure");
        	Node nounFailure =  nl.item(0);
        	
        	if(nounFailure != null){
        		failureMessage = nounFailure.getLocalName() + ": ";
        	}
        	
        	nl = doc.getElementsByTagName("ErrorMessage");
        	Node errorMessage =  nl.item(0);
        	
        	if(errorMessage != null){
        		failureMessage += errorMessage.getLocalName() + ": ";
        	}
        	nl = doc.getElementsByTagName("Description");
        	Node description =  nl.item(0);
        	
        	if(description != null){
        		failureMessage += description.getLocalName() + ": ";
        	}
        	nl = doc.getElementsByTagName("ReasonCode");
        	Node reasonCode =  nl.item(0);
        	
        	if(reasonCode != null){
        		failureMessage += reasonCode.getLocalName() + ": " + reasonCode.getFirstChild().getNodeValue();
        	}
        	failureMessage += " (Further investigation is probably required to check configuration settings)";
        }
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}

	public Integer getTransactionStatusCode() {
		return transactionStatusCode;
	}
	public String getFailureMessage() {
		return failureMessage;
	}
	
}