
package biz.firstlook.client.dms.rd;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class STARWebServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;

    public STARWebServiceClient() {
        create0();
        Endpoint STAREndpointEP = service0 .addEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "STAREndpoint"), new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransport"), "http://b2b-test.reyrey.com:80/STAR40WS/services/STAREndpoint");
        endpoints.put(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "STAREndpoint"), STAREndpointEP);
        Endpoint StarTransportPortTypesLocalEndpointEP = service0 .addEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalEndpoint"), new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalBinding"), "xfire.local://STARWebService");
        endpoints.put(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalEndpoint"), StarTransportPortTypesLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String, Object> props = new HashMap<String, Object>();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.client.dms.rd.StarTransportPortTypes.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransport"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public StarTransportPortTypes getSTAREndpoint() {
        return ((StarTransportPortTypes)(this).getEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "STAREndpoint")));
    }

    public StarTransportPortTypes getSTAREndpoint(String url) {
        StarTransportPortTypes var = getSTAREndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public StarTransportPortTypes getStarTransportPortTypesLocalEndpoint() {
        return ((StarTransportPortTypes)(this).getEndpoint(new QName("http://www.starstandards.org/webservices/2005/10/transport/bindings", "StarTransportPortTypesLocalEndpoint")));
    }

    public StarTransportPortTypes getStarTransportPortTypesLocalEndpoint(String url) {
        StarTransportPortTypes var = getStarTransportPortTypesLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
