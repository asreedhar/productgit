package biz.firstlook.client.dms.sis;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

import biz.firstlook.client.dms.sis.util.SISTransactionUtil;
import biz.firstlook.service.dms.sis.client.UserInfoHeader;
import biz.firstlook.transact.persist.model.DMSExportBusinessUnitPreference;
import biz.firstlook.transact.persist.persistence.DMSExportBusinessUnitPreferenceDAO;

public class SISInventoryUpdateClient {

	private final static Logger log = Logger.getLogger(SISInventoryUpdateClient.class);

	// this is a Incisent pass that we use to submit all dealer's info.
	// a dealer is unqiuely identified by its ClientSystemId
	private String password = "P@SSW0RD"; // <--- thats a zero
	
	//number of seconds before giving up on the SIS request.
	private long timeout = 540;
	
	private DMSExportBusinessUnitPreferenceDAO dmsExportBusinessUnitPreferenceDAO;

	// All exceptions and standard errors are handled/consumed by this method
	// Any error messages - either from stadard from SIS or of type Exception are wrapped into the result string
	// This is because we can't really deal with errors or exceptions. We simply log and can store them.
	public String sendRepriceEvent(Integer businessUnitId, Integer dmsThirdPartyEntityId, String stockNumber, String vin, Double listPrice) {
		final DMSExportBusinessUnitPreference preference = dmsExportBusinessUnitPreferenceDAO.findByBusinessUnit(businessUnitId, dmsThirdPartyEntityId);
		
		final UserInfoHeader infoHeader = new UserInfoHeader();
		infoHeader.setUserid(preference.getClientSystemId());
		infoHeader.setPassword(password);

		final StringBuilder result = new StringBuilder();
		try {
			// Currently all exception and error handling of SIS transmissions is string based. 
			// We could get into specially typed SIS exception if future reqs require it.
			String xml = SISTransactionUtil.buildRepriceVehicleUpdateString(stockNumber, vin, listPrice, preference.getDmsExportInternetPriceMappingDescription());
			SISTransmissionClient transmissionClient = new SISTransmissionClient(timeout);
			result.append(transmissionClient.transmit(xml, infoHeader));
			if(result != null) {
				if (result.indexOf("ERROR") == -1) {
					if(log.isInfoEnabled()) {
						log.info(MessageFormat.format("Submitted stock: {0} with list price of {1, number, currency} to SIS.", 
								stockNumber, 
								listPrice));
					}
				} else { // we got one of their standard error messages back from SIS
					log.error(MessageFormat.format("Error Submitting stock: {0} with list price of {1, number, currency} to SIS.", 
							stockNumber, 
							listPrice));
				}
				if(log.isInfoEnabled()) {
					log.info(result);
				}
			} else {
				log.error(MessageFormat.format("Null response from SIS when submitting xml {0}", xml));
			}
		} catch (Exception e) {
			if (e.getMessage().indexOf("CompanyNotFoundException") > -1) {
				// Per case: 178 - DM asked that we handle this error differently. Since it identifies the need for specific operational action with SIS.
				result.insert(0, "ERROR : Company Not Found Exception, "); 
			} else {
				result.append(MessageFormat.format("ERROR Submitting stock: {0} with list price of {1, number, currency} to SIS.", 
						stockNumber, 
						listPrice));				
			}
			log.error(result);
		}
		
		return result.toString();
	}

	public void setDmsExportBusinessUnitPreferenceDAO(
			DMSExportBusinessUnitPreferenceDAO dmsExportBusinessUnitPreferenceDAO) {
		this.dmsExportBusinessUnitPreferenceDAO = dmsExportBusinessUnitPreferenceDAO;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
}
