
package biz.firstlook.client.dms.rd;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Holder;


import biz.firstlook.service.dms.rd.client.response.Payload;
import biz.firstlook.service.dms.rd.client.transport.PayloadManifest;

@WebService(name = "StarTransportPortTypes", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport/bindings")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface StarTransportPortTypes {


    @WebMethod(operationName = "ProcessMessage", action = "http://www.starstandards.org/webservices/2005/10/transport/operations/ProcessMessage")
    @WebResult(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
    public Payload processMessage(
        @WebParam(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
        biz.firstlook.service.dms.rd.client.transport.Payload payload,
        @WebParam(name = "payloadManifest", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport", header = true, mode = WebParam.Mode.INOUT)
        Holder<PayloadManifest> payloadManifest);

    @WebMethod(operationName = "PullMessage", action = "http://www.starstandards.org/webservices/2005/10/transport/operations/PullMessage")
    @WebResult(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
    public Payload pullMessage(
        @WebParam(name = "payloadManifest", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport", header = true, mode = WebParam.Mode.OUT)
        Holder<PayloadManifest> payloadManifest);

    @WebMethod(operationName = "PutMessage", action = "http://www.starstandards.org/webservices/2005/10/transport/operations/PutMessage")
    @WebResult(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
    public Payload putMessage(
        @WebParam(name = "payload", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport")
        Payload payload,
        @WebParam(name = "payloadManifest", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport", header = true)
        PayloadManifest payloadManifest);

}
