package biz.firstlook.client.dms.rd.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import biz.firstlook.service.dms.rd.client.ApplicationAreaType;
import biz.firstlook.service.dms.rd.client.DestinationType;
import biz.firstlook.service.dms.rd.client.Floorplan;
import biz.firstlook.service.dms.rd.client.FloorplanInfo;
import biz.firstlook.service.dms.rd.client.MiscInfo;
import biz.firstlook.service.dms.rd.client.OtherCriteria;
import biz.firstlook.service.dms.rd.client.PriceCost;
import biz.firstlook.service.dms.rd.client.ReyVehInvTx;
import biz.firstlook.service.dms.rd.client.SenderType;
import biz.firstlook.service.dms.rd.client.StockingInfo;
import biz.firstlook.service.dms.rd.client.VehInvExchange;
import biz.firstlook.service.dms.rd.client.VehInvOrigin;
import biz.firstlook.service.dms.rd.client.VehInvRec;
import biz.firstlook.service.dms.rd.client.Vehicle;
import biz.firstlook.service.dms.rd.client.VehicleDetail;

public class ReynoldsDirectTransactionUtil {

	public static ReyVehInvTx buildReyVehInv(String stockNumber,
			String vin, Double listPrice, String dealerNumber, String storeNumber, String branchNumber, String usedGroup, Integer areaNumber, String dealerNo,
			Integer dmsExportInternetPriceMappingId) {
		
		ReynoldsExportPriceMappingsEnum internetPriceMapping = dmsExportInternetPriceMappingId != null ? ReynoldsExportPriceMappingsEnum.decode(dmsExportInternetPriceMappingId) : ReynoldsExportPriceMappingsEnum.LIST_PRICE;
		
		PriceCost priceCost = new PriceCost();
		MiscInfo miscInfo = new MiscInfo();
		StockingInfo stockingInfo = new StockingInfo();
		stockingInfo.setId(stockNumber);
		stockingInfo.setUsedGroup(usedGroup);
		FloorplanInfo flrPlanInfo = new FloorplanInfo();

		DecimalFormat df = new DecimalFormat( "#.00" );
		String listPriceValue = df.format(listPrice);
		
		switch(internetPriceMapping){
			case LIST_PRICE:
				priceCost.setListPrice(listPriceValue); break;
			case CODED_COST:
				priceCost.setCodedCost(listPriceValue); break;
			case LOC:
				stockingInfo.setLoc(listPriceValue); break;
			case MEMO1:
				miscInfo.setMemoLine1(listPriceValue); break;
			case MEMO2:
				miscInfo.setMemoLine2(listPriceValue); break;
			case INTERNET_PRICE:
				priceCost.setInternetPrice(listPriceValue); break;
			case FLRPLN_INTRRATE:
				flrPlanInfo.setFlrplnIntrRate(listPriceValue); break;
			default:
				priceCost.setListPrice(listPriceValue); break;
		}
		
		VehicleDetail vehicleDetail = new VehicleDetail();
		vehicleDetail.setNewUsed("U");
		
		Vehicle vehicle = new Vehicle();
		vehicle.setVin(vin);
		vehicle.setVehicleDetail(vehicleDetail);
		vehicle.setMakePfx("ZZ");
		
		VehInvOrigin vehInvOrigin = new VehInvOrigin();
		vehInvOrigin.setBranchNo(getTwoDigString(branchNumber));
		vehInvOrigin.setDealerNo(dealerNo);
		vehInvOrigin.setStoreNo(getTwoDigString(storeNumber));
		vehInvOrigin.setTransType("Update");
		
		Floorplan flrPlan = new Floorplan();
		List<FloorplanInfo> flrPlanInfoList = flrPlan.getFloorplanInfo();
		flrPlanInfoList.add(flrPlanInfo);
		
		VehInvRec vehInvRec = new VehInvRec();
		vehInvRec.setVehicle(vehicle);
		vehInvRec.setPriceCost(priceCost);
		vehInvRec.setStockingInfo(stockingInfo);
		vehInvRec.setMiscInfo(miscInfo);
		vehInvRec.setVehInvOrigin(vehInvOrigin);
		vehInvRec.setFloorplan(flrPlan);
		
		VehInvExchange vehInvExchange = new VehInvExchange();
		vehInvExchange.getVehInvRec().add(vehInvRec);
		
		DestinationType destinationType = new DestinationType();
		destinationType.setAreaNumber(getTwoDigString(branchNumber));
		destinationType.setStoreNumber(getTwoDigString(storeNumber));
		destinationType.setDealerNumber(dealerNumber);
		destinationType.setDestinationSoftware("EAI");
		destinationType.setDestinationSoftwareCode("RCI");
		destinationType.setDestinationNameCode("RR");
		
		SenderType senderType = new SenderType();
		senderType.setSenderNameCode("RCI");
		senderType.setCreatorNameCode("Firstlook");
		senderType.setReferenceId("Update");
		senderType.setTask("VI");
		senderType.setComponent("Firstlook");
		senderType.setLogicalId("");
		
		ApplicationAreaType applicationAreaType = new ApplicationAreaType();
		applicationAreaType.setSender(senderType);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
        String datetime = dateFormat.format(date);
		applicationAreaType.setCreationDateTime(datetime);
		applicationAreaType.setBODId(System.currentTimeMillis() + "");
		applicationAreaType.setDestination(destinationType);
		
		OtherCriteria otherCritera = new OtherCriteria();
		
		ReyVehInvTx reyVehInvTx = new ReyVehInvTx();
		reyVehInvTx.setRevision("1.0");
		reyVehInvTx.setXmlns("http://www.starstandards.org/STAR");
		reyVehInvTx.setSchemaLocation("http://www.starstandards.org/STAR rey_VehInvTx.xsd");
		
		
		reyVehInvTx.setApplicationArea(applicationAreaType);
		reyVehInvTx.setVehInvExchange(vehInvExchange);
		reyVehInvTx.setOtherCriteria(otherCritera);
	
		return reyVehInvTx;
	}
	
	public static String getTwoDigString(Integer value){
		if(value < 10 && value > 0){
			return "0" + value.toString();
		}	
		return value.toString();
	}
	
	public static String getTwoDigString(String valueStr)
	{
		String result = valueStr;
		
		while( result.length() < 2 )
		{
			result = "0" + result;
		}
		
		return result;
	}
}
