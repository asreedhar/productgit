package biz.firstlook.client.dms.sis;


import jargs.gnu.CmdLineParser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;

import org.apache.log4j.Logger;

import biz.firstlook.client.dms.sis.util.SISTransactionUtil;
import biz.firstlook.service.dms.sis.client.UserInfoHeader;

public class SISExtractionClient {

	private static final Logger log = Logger.getLogger(SISExtractionClient.class);
	
	public void extractInventory(String userId, String password, String outputFileString) {
		SISTransmissionClient transmissionClient = new SISTransmissionClient();
		
		final UserInfoHeader infoHeader = new UserInfoHeader();
		infoHeader.setPassword(password);
		infoHeader.setUserid(userId);
		
		FileOutputStream fileOutputStream = null;
		try {
			String result = transmissionClient.transmit(SISTransactionUtil.getRequestForInventoryCSVString(), infoHeader);
			File outputFile = new File(outputFileString);
			fileOutputStream = new FileOutputStream(outputFile);
			fileOutputStream.write(result.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(fileOutputStream != null) {
				try {
					fileOutputStream.flush();
					fileOutputStream.close();
				} catch (IOException e) {
					log.warn(MessageFormat.format("Failed to flush and close fileOutputStream for {0}, {1}", outputFileString, e.getMessage()));
				}
			}
		}
		log.info("Success! Output written to : " + outputFileString);
	}
	
	public static String getUsage()
	{
		StringBuffer usage = new StringBuffer();
		usage.append( "Usage: java SISExtractionClient [OPTION]..." );
		usage.append( "Makes a connection to the SIS WebService and extracts a CSV inventory file for the dealer specific by the --d option.\n" );
		usage.append( "  -u, --userId		the SIS user (Dealer) Id. E.g. : 12172FLO. Note - Not the FL BusinessUnit Id. \n" );
		usage.append( "  -p, --password 	the password to use to connect to SIS (default: P@SSW0RD ).\n" );
		usage.append( "  -o, --outputFile 	location to write the output file (default: ./SISInvInfo_ + userId + .csv ).\n" );
		return usage.toString();
	}	

	public static void main(String[] args) {

		log.info("Starting SIS data extraction....");

		CmdLineParser cmdLineParser = new CmdLineParser();
		CmdLineParser.Option userIdOption = new CmdLineParser.Option.StringOption( 'u', "userId" );
		CmdLineParser.Option passwordOption = new CmdLineParser.Option.StringOption( 'p', "password" );
		CmdLineParser.Option outputFileOption = new CmdLineParser.Option.StringOption( 'o', "outputFile" );
		cmdLineParser.addOption(userIdOption);
		cmdLineParser.addOption(passwordOption);
		cmdLineParser.addOption(outputFileOption);

		try
		{
			cmdLineParser.parse( args );
		}
		catch ( CmdLineParser.OptionException oe )
		{
			log.error( "Illegal Argument: " + oe.getMessage() );
			log.error( getUsage() );
		}	
		
		String userId = (String)cmdLineParser.getOptionValue( userIdOption);
		if ( userId== null )
		{
			log.error("No user Id specified\n" + getUsage());
			System.exit(-1);
		}
		String password = (String)cmdLineParser.getOptionValue( passwordOption);
		if ( password == null )
		{
			password = "P@SSW0RD";
		}
		String outputFileString = (String)cmdLineParser.getOptionValue( outputFileOption);
		if ( outputFileString == null )
		{
			outputFileString = "SISInvInfo_" + userId +".xml";
		}
		
		// do it
		SISExtractionClient client = new SISExtractionClient();
		client.extractInventory(userId, password, outputFileString);

	}

}
