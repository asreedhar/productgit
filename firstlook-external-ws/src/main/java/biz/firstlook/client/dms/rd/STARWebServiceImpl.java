
package biz.firstlook.client.dms.rd;

import javax.jws.WebService;
import javax.xml.ws.Holder;

import biz.firstlook.service.dms.rd.client.response.Payload;
import biz.firstlook.service.dms.rd.client.transport.PayloadManifest;

@WebService(serviceName = "STARWebService", targetNamespace = "http://www.starstandards.org/webservices/2005/10/transport/bindings", endpointInterface = "biz.firstlook.client.dms.rd2.StarTransportPortTypes")
public class STARWebServiceImpl
    implements StarTransportPortTypes
{


    public Payload processMessage(biz.firstlook.service.dms.rd.client.transport.Payload payload, Holder<PayloadManifest> payloadManifest) {
        throw new UnsupportedOperationException();
    }

    public Payload pullMessage(Holder<PayloadManifest> payloadManifest) {
        throw new UnsupportedOperationException();
    }

    public Payload putMessage(Payload payload, PayloadManifest payloadManifest) {
        throw new UnsupportedOperationException();
    }

}
