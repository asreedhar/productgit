package biz.firstlook.client.dms.rd.util;

public enum ReynoldsExportPriceMappingsEnum {
	LIST_PRICE(5, "List Price"),
	CODED_COST(10, "Coded Cost"),
	MEMO1(11, "Memo1"),
	MEMO2(12, "Memo2"),
	LOC(15, "Location"),
	INTERNET_PRICE(31, "Internet Price"),
	FLRPLN_INTRRATE(32, "Floor Plan Intrrate");
	
	private int id;
	private String description;
	
	private ReynoldsExportPriceMappingsEnum(int id, String desc) {
		this.id = id;
		this.description = desc;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return id;
	}
	
	public static final ReynoldsExportPriceMappingsEnum decode(int id){
		switch(id){
			case 5:
				return ReynoldsExportPriceMappingsEnum.LIST_PRICE;
			case 10:
				return ReynoldsExportPriceMappingsEnum.CODED_COST;
			case 11:
				return ReynoldsExportPriceMappingsEnum.MEMO1;
			case 12:
				return ReynoldsExportPriceMappingsEnum.MEMO2;
			case 15:
				return ReynoldsExportPriceMappingsEnum.LOC;
			case 31:
				return ReynoldsExportPriceMappingsEnum.INTERNET_PRICE;
			case 32:
				return ReynoldsExportPriceMappingsEnum.FLRPLN_INTRRATE;
			default:
				throw new IllegalArgumentException("Not a recognized Reynolds Internet Price DMS mapping ID: " + id);
		}
	}
	
	
}
