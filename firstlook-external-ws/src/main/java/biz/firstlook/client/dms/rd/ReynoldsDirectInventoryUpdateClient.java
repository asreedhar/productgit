package biz.firstlook.client.dms.rd;

import java.lang.reflect.Proxy;
import java.util.Properties;

import javax.xml.ws.Holder;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbServiceFactory;
import org.codehaus.xfire.security.wss4j.WSS4JOutHandler;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.util.dom.DOMOutHandler;

import biz.firstlook.client.dms.rd.util.PasswordCallbackHandler;
import biz.firstlook.client.dms.rd.util.RequestLoggingHandler;
import biz.firstlook.client.dms.rd.util.ReynoldsDirectResponseHandler;
import biz.firstlook.client.dms.rd.util.ReynoldsDirectTransactionUtil;
import biz.firstlook.client.dms.rd.util.ReynoldsERACodesEnum;
import biz.firstlook.client.dms.rd.util.TestPasswordCallbackHandler;
import biz.firstlook.service.dms.rd.client.ReyVehInvTx;
import biz.firstlook.service.dms.rd.client.transport.Content;
import biz.firstlook.service.dms.rd.client.transport.Manifest;
import biz.firstlook.service.dms.rd.client.transport.ObjectFactory;
import biz.firstlook.service.dms.rd.client.transport.Payload;
import biz.firstlook.service.dms.rd.client.transport.PayloadManifest;
import biz.firstlook.service.dms.rd.client.transport.ProcessMessage;
import biz.firstlook.transact.persist.model.DMSExportBusinessUnitPreference;
import biz.firstlook.transact.persist.persistence.DMSExportBusinessUnitPreferenceDAO;


public class ReynoldsDirectInventoryUpdateClient {
	
	private static Logger log = Logger.getLogger(ReynoldsDirectInventoryUpdateClient.class);
	private DMSExportBusinessUnitPreferenceDAO dmsExportBusinessUnitPreferenceDAO;
	private String endpoint = "http://b2b-test.reyrey.com:80/STAR40WS/services/STAREndpoint";
	
	public String sendWithWSS(Payload payload, PayloadManifest payloadManifest, Holder<PayloadManifest> payloadManifest2){

		ReynoldsDirectResponseHandler reynoldsDirectResponseHandler = new ReynoldsDirectResponseHandler();
		try{
			Service serviceModel = new JaxbServiceFactory().
			create(StarTransportPortTypes.class, 
			 "STARWebService",
					"",
			 null);
			StarTransportPortTypes service = (StarTransportPortTypes) new XFireProxyFactory().
			create(serviceModel, endpoint);
	
			Client client =  ((XFireProxy)
					Proxy.getInvocationHandler(service)).getClient();
			
			client.addOutHandler(new DOMOutHandler());
			//client.addOutHandler(new RequestLoggingHandler());
			Properties p = new Properties();
			
			//Action to perform : user token
			p.setProperty(WSHandlerConstants.ACTION,
					WSHandlerConstants.USERNAME_TOKEN);
			
			//Set password type to text
			p.setProperty(WSHandlerConstants.PASSWORD_TYPE,
			WSConstants.PW_TEXT);
			
			p.setProperty(WSHandlerConstants.USER, "FRSTLk");
			
			//Used do retrive password for given user name
			String passwordCallbackHandlerName;
			if(endpoint.equalsIgnoreCase("http://b2b-test.reyrey.com:80/STAR40WS/services/STAREndpoint")){
				passwordCallbackHandlerName = TestPasswordCallbackHandler.class.getName();
			}
			else{
				passwordCallbackHandlerName = PasswordCallbackHandler.class.getName();				
			}
			p.setProperty(WSHandlerConstants.PW_CALLBACK_CLASS,
					passwordCallbackHandlerName);
			
			client.addOutHandler(new WSS4JOutHandler(p));
			client.addOutHandler(new RequestLoggingHandler());
			
			client.addInHandler(reynoldsDirectResponseHandler);
			
			service.processMessage(payload, payloadManifest2);
			
			ReynoldsERACodesEnum reynoldsERACodesEnum = ReynoldsERACodesEnum.decode(reynoldsDirectResponseHandler.getTransactionStatusCode());
			
			if(reynoldsERACodesEnum == ReynoldsERACodesEnum.SUCCESS ){
				log.info("SUCCESS: " + reynoldsERACodesEnum.getDescription());
				return "SUCCESS: " + reynoldsERACodesEnum.getDescription();
			}
			else{
				log.error( "ERROR " + reynoldsERACodesEnum.getDescription() + " " + reynoldsDirectResponseHandler.getFailureMessage());
				return "ERROR: " + reynoldsERACodesEnum.getDescription() + ": " + reynoldsDirectResponseHandler.getFailureMessage();
			}
		}
		catch(XFireRuntimeException xfre){
			log.error("Error with calling Reynolds Webservice: ", xfre);
		}
		catch(Exception e){
			log.error("Error with calling Reynolds Webservice: ", e);
		}

		return "ERROR: " + reynoldsDirectResponseHandler.getFailureMessage();
	}
		
	public String sendRepriceEvent(Integer businessUnitId, Integer dmsThirdPartyEntityId, String stockNumber, String vin, Double listPrice, String storeNumber, String branchNumber, String usedGroup){
		
		DMSExportBusinessUnitPreference preference = dmsExportBusinessUnitPreferenceDAO.findByBusinessUnit(businessUnitId, dmsThirdPartyEntityId);
		// Create Payload Manifest 
		PayloadManifest payloadManifest1 = new PayloadManifest();
		Holder<PayloadManifest> payloadManifest2 = new Holder<PayloadManifest>();
		
		//Create Manifest
		Manifest manifest = new Manifest(); 
		manifest.setVersion("2.0");
        manifest.setElement("rey_VehInvTx"); 
        manifest.setNamespaceURI("http://www.starstandards.org/webservices/2005/10/transport"); 
        manifest.setContentID("Content1");
      
        payloadManifest1.getManifest().add(manifest);      
        
		try{
			ObjectFactory factory = new ObjectFactory();
			
			ReyVehInvTx xml = ReynoldsDirectTransactionUtil.buildReyVehInv(stockNumber, vin, listPrice, preference.getDmsLogin(), storeNumber, branchNumber, usedGroup, preference.getAreaNumber(), preference.getReynoldsDealerNo(), preference.getDmsExportInternetPriceMappingId());	
			
			Content payloadContent = factory.createContent();
			payloadContent.setId("Content1");
			payloadContent.setReyVehInvTx(xml);
			
			Payload payload = factory.createPayload();
			payload.setXsi("http://www.w3.org/2001/XMLSchema-instance");
			payload.setSoapenc("http://schemas.xmlsoap.org/soap/encoding/");
			payload.setXsd("http://www.w3.org/2001/XMLSchema");	
			payload.setSoapenv("http://schemas.xmlsoap.org/soap/envelope/");
			payload.setXmlns("http://www.starstandards.org/webservices/2005/10/transport");
			payload.setWsse("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			payload.setWsu("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
			payload.setSoap("http://schemas.xmlsoap.org/soap/envelope/");
			payload.setWsa("http://schemas.xmlsoap.org/ws/2004/03/addressing");
		
		       
	        payload.setContent(payloadContent);
	        
	        ProcessMessage message = factory.createProcessMessage();
	        message.setPayload(payload);
	        	        
	        return sendWithWSS(payload, payloadManifest1, payloadManifest2 );
		
		}
		catch(Exception e){ 
			log.error("Error with calling Reynolds Webservice: ", e);
			return "ERROR: " + e.toString();
		}    
	}

	public void setDmsExportBusinessUnitPreferenceDAO(
			DMSExportBusinessUnitPreferenceDAO dmsExportBusinessUnitPreferenceDAO) {
		this.dmsExportBusinessUnitPreferenceDAO = dmsExportBusinessUnitPreferenceDAO;
	}

	public void setEndpoint(String reynoldsEndpoint) {
		this.endpoint = reynoldsEndpoint;
	}	
}
