package biz.firstlook.client.dms.sis;

import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.log4j.Logger;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;

import biz.firstlook.service.dms.sis.client.DealControllerSoap;
import biz.firstlook.service.dms.sis.client.UserInfoHeader;



public class SISTransmissionClient {

	private static Logger log = Logger.getLogger(SISTransmissionClient.class);

	private static XFireProxyFactory proxyFactory = new XFireProxyFactory();

	private Map<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();

	private Service service0;

	private static String SISWebServiceEndPoint = "https://uset.datadms.com/DMSDealsV4/DealController.asmx";

	private DealControllerSoap sisService = null;

	private final long timeout;

	public SISTransmissionClient() {
		create0();
		initEndpoint();
		sisService = init();
		timeout = 540;
	}
	
	public SISTransmissionClient(long timeout) {
		create0();
		initEndpoint();
		sisService = init();
		this.timeout = timeout;
	}
	
	private void initEndpoint() {
		final String namespaceURI = "http://www.menuv.com/webservices/CommManager/V4/DMSDeals";
		final QName dealerControllerSoap = new QName(namespaceURI, "DealControllerSoap");
		Endpoint DealControllerSoapLocalEndpointEP = service0
				.addEndpoint(
						new QName(
								namespaceURI,
								"DealControllerSoapLocalEndpoint"),
						new QName(
								namespaceURI,
								"DealControllerSoapLocalBinding"),
						"xfire.local://DealController");
		endpoints.put(new QName(
				namespaceURI,
				"DealControllerSoapLocalEndpoint"),
				DealControllerSoapLocalEndpointEP);
		Endpoint DealControllerSoapEP = service0.addEndpoint(
				dealerControllerSoap, 
				dealerControllerSoap, 
				SISWebServiceEndPoint);
		endpoints.put(dealerControllerSoap, DealControllerSoapEP);
	}

	private Object getEndpoint(Endpoint endpoint) {
		try {
			return proxyFactory.create((endpoint).getBinding(), (endpoint)
					.getUrl());
		} catch (MalformedURLException e) {
			throw new XFireRuntimeException("Invalid URL", e);
		}
	}

	private Object getEndpoint(QName name) {
		Endpoint endpoint = ((Endpoint) endpoints.get((name)));
		if ((endpoint) == null) {
			throw new IllegalStateException("No such endpoint!");
		}
		return getEndpoint((endpoint));
	}

	private void create0() {
		TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance()
				.getXFire().getTransportManager());
		HashMap<String, Boolean> props = new HashMap<String, Boolean>();
		props.put("annotations.allow.interface", true);
		AnnotationServiceFactory asf = new AnnotationServiceFactory(
				new Jsr181WebAnnotations(), tm, new AegisBindingProvider(
						new JaxbTypeRegistry()));
		asf.setBindingCreationEnabled(false);
		service0 = asf.create((DealControllerSoap.class), props);
		{
			asf.createSoap11Binding(
							service0,
							new QName(
									"http://www.menuv.com/webservices/CommManager/V4/DMSDeals",
									"DealControllerSoapLocalBinding"),
							"urn:xfire:transport:local");
		}
		{
			asf.createSoap11Binding(
							service0,
							new QName(
									"http://www.menuv.com/webservices/CommManager/V4/DMSDeals",
									"DealControllerSoap"),
							"http://schemas.xmlsoap.org/soap/http");
		}
	}

	private DealControllerSoap getDealControllerSoap() {
		return ((DealControllerSoap) (this).getEndpoint(new QName(
				"http://www.menuv.com/webservices/CommManager/V4/DMSDeals",
				"DealControllerSoap")));
	}

	private DealControllerSoap init() {
		DealControllerSoap service = getDealControllerSoap();
		Protocol protocol = new Protocol("https",
				(ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
		Protocol.registerProtocol("https", protocol);
		return service;
	}

	// caller's responsibility to handle exception. pls. don't catch it here.
	public String transmit(String transactionXML, UserInfoHeader infoHeader) throws Exception {

		if(log.isInfoEnabled()) {
			log.info(MessageFormat.format("Connecting to SIS WebService at address: {0} with User: {1} and Pass: {2}"
					,SISWebServiceEndPoint
					,infoHeader.getUserid()
					,infoHeader.getPassword()));
		}

		
		int dmsId = sisService.submitRequest(transactionXML, infoHeader);
		if(log.isInfoEnabled()) {
			log.info("Request Submitted. SIS DMSId Id assigned is: " + dmsId);
		}
		long tick = 0;
		
		final StringBuilder result = new StringBuilder("SisRequestID: ").append(dmsId).append(" ");
		String response = null;
		while ((response == null || response.trim().length() == 0) && tick < timeout) {
			response = sisService.pollForResponse(dmsId, infoHeader);
			tick++;
			if(log.isDebugEnabled()) {
				log.debug(MessageFormat.format("Waiting for response.... Tick : {0}", tick));
			}
			Thread.sleep(1000);
		}
		result.append(response);
		return result.toString();
	}

}
