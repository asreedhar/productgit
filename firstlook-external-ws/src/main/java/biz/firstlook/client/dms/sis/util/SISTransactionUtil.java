package biz.firstlook.client.dms.sis.util;

public class SISTransactionUtil {

	public static String buildRepriceVehicleUpdateString(String stockNumber,
			String vin, Double listPrice, String dmsExportInternetPriceMappingDescription) {
		StringBuilder xml = new StringBuilder();
//		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		xml.append("<DMS_TRANSACTION>");
		xml.append("<TRANSACTION_ID></TRANSACTION_ID>");
		xml.append("<TRANSACTION_TYPE>VEH_UPDATE</TRANSACTION_TYPE>");
		xml.append("<COMPANY_ID></COMPANY_ID>");
		xml.append("<DMS_TYPE></DMS_TYPE>");
		xml.append("<VENDOR_TYPE>FLO</VENDOR_TYPE>");
		xml.append("<DMS_ACCT></DMS_ACCT>");
		xml.append("<DMS_BRANCH></DMS_BRANCH>");
		xml.append("<DMS_VEH>");
		xml.append("<VIN>").append(vin).append("</VIN>");
		xml.append("<STOCK_NUMBER>").append(stockNumber).append("</STOCK_NUMBER>");
		if(dmsExportInternetPriceMappingDescription != null){
			xml.append("<").append(dmsExportInternetPriceMappingDescription).append(">");
			xml.append(listPrice);
			xml.append("</").append(dmsExportInternetPriceMappingDescription).append(">");
		} else {
			xml.append("<LIST_PRICE>").append(listPrice).append("</LIST_PRICE>");
		}
		xml.append("</DMS_VEH>");
		xml.append("</DMS_TRANSACTION>");
		return xml.toString();
	}

	public static String getRequestForInventoryCSVString() {
		StringBuffer xml = new StringBuffer();
//		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		xml.append("<DMS_TRANSACTION>");
		xml.append("<TRANSACTION_ID></TRANSACTION_ID>");
		xml.append("<TRANSACTION_TYPE>BATCH_VEH_CDATA</TRANSACTION_TYPE>");
		xml.append("<COMPANY_ID></COMPANY_ID>");
		xml.append("<VENDOR_TYPE>FLO</VENDOR_TYPE>");
		xml.append("<DMS_TYPE/>");
		xml.append("<DMS_ACCT/>");
		xml.append("<DMS_BRANCH/>");
		xml.append("</DMS_TRANSACTION>");
		return xml.toString();
	}
}
