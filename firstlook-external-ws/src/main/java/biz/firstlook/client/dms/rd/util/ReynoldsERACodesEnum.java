package biz.firstlook.client.dms.rd.util;

public enum ReynoldsERACodesEnum {
	SUCCESS(0, "Success"),
	FILE_NOT_FOUND(1, "File Not Found"),
	RECORD_NOT_FOUND(2, "Record Not Found"),
	RECORD_LOCKED(3, "Record Locked"),
	FILE_WRITE_ERROR(5, "File With Error"),
	REQUIRED_DATA_IS_MISSING(201, "Required Data Is Missing"),
	FAILED_NEW_OR_USED_CHECK(202, "Failed Check For New/Used flag"),
	INVALID_NUM_OF_PARAMETERS(205, "Invalid Number Of Parameters"),
	INVALID_PARAMETER_PASSING(206, "Invalid parameters Passing"),
	INVALID_RECORD_TYPE(208, "Invalid Record Type"),
	UKNOWN_ERROR(-1, "");
	
	
	private int id;
	private String description;
	
	private ReynoldsERACodesEnum(int id, String desc) {
		this.id = id;
		this.description = desc;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public static final ReynoldsERACodesEnum decode(int id){
		switch(id){
			case 0: 
				return ReynoldsERACodesEnum.SUCCESS; 
			case 1: 
				return ReynoldsERACodesEnum.FILE_NOT_FOUND;
			case 2: 
				return ReynoldsERACodesEnum.RECORD_NOT_FOUND;
			case 3: 
				return ReynoldsERACodesEnum.RECORD_LOCKED;
			case 5:
				return ReynoldsERACodesEnum.FILE_WRITE_ERROR;
			case 201:
				return ReynoldsERACodesEnum.REQUIRED_DATA_IS_MISSING;
			case 202:
				return ReynoldsERACodesEnum.FAILED_NEW_OR_USED_CHECK;
			case 205:
				return ReynoldsERACodesEnum.INVALID_NUM_OF_PARAMETERS; 
			case 206:
				return ReynoldsERACodesEnum.INVALID_PARAMETER_PASSING; 
			case 208:
				return ReynoldsERACodesEnum.INVALID_RECORD_TYPE;
			default:
				return ReynoldsERACodesEnum.UKNOWN_ERROR;
		}
	}
	
}
