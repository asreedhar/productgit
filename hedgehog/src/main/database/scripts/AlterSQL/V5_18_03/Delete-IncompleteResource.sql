
CREATE TABLE #Resource (Id INT NOT NULL, PRIMARY KEY (Id))

INSERT INTO #Resource (Id) 

SELECT	Id FROM track.Resource R
WHERE	R.Name LIKE 'text=%'
OR	R.Name LIKE 'id=%'
OR	R.Name LIKE 'value=%'

-- 32% of records (2,732,214 rows)
DELETE	A
FROM	track.Action A
JOIN	#Resource R ON R.Id = A.ResourceId

-- 7% of records (147,990 rows)
DELETE	R
FROM	track.Resource R
JOIN	#Resource X ON R.Id = X.Id

DROP TABLE #Resource
