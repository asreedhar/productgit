 
function Hedgehog$Enhance$Attribute$Loop (enhance, elements) {
	var pageName = enhance.doc.Name();
	for (var i = 0; i < elements.length; i++) {
		var attr = elements[i].getAttribute('hh:interaction');
		if (attr) {
			var pair = attr.split(':',2);
			if (pair.length == 2) {
				enhance.Observe(elements[i], pair[0], function(event) {
					var element = (event.target || event.srcElement);
					var attr = element.getAttribute('hh:interaction');
					if (attr) {
						var pair = attr.split(':',2);
						if (pair.length == 2) {
							var name = pageName + "/event/click/link/" + pair[1];
		            		Hedgehog.Track(name);
						}
					}
		        }, false);
			}
		}
    }
}

Hedgehog$Enhance$Attribute = function ()
{
	this.doc = new Hedgehog$Document();
    
    this.observers = [];
    
    this.Initialize.apply(this, arguments);
}

Hedgehog$Enhance$Attribute.prototype.Initialize = function ()
{
	Hedgehog$Enhance$Attribute$Loop(this, document.getElementsByTagName('A'));
	
	Hedgehog$Enhance$Attribute$Loop(this, document.getElementsByTagName('DIV'));
	
    Hedgehog.Enhancement = this;
    
    this.Observe(window, 'unload', function () { Hedgehog.Enhancement.Dispose(); });
}

Hedgehog$Enhance$Attribute.prototype.Dispose = function ()
{
    if (!this.observers)
        return;
    for (var i = 0, length = this.observers.length; i < length; i++) {
        this.StopObserving.apply(this, this.observers[i]);
        this.observers[i][0] = null;
    }
    this.observers = false
}

Hedgehog$Enhance$Attribute.prototype.Observe = function (element, name, observer, useCapture)
{
    if (element.addEventListener) {
        this.observers.push([element, name, observer, useCapture]);
        element.addEventListener(name, observer, useCapture);
    }
    else if (element.attachEvent) {
        this.observers.push([element, name, observer, useCapture]);
        element.attachEvent('on' + name, observer);
    }
}

Hedgehog$Enhance$Attribute.prototype.StopObserving = function (element, name, observer, useCapture)
{
    if (element.removeEventListener) {
        element.removeEventListener(name, observer, useCapture);
    }
    else if (element.detachEvent) {
        try {
            element.detachEvent('on' + name, observer);
        } catch (e) {}
    }
}

if (window.addEventListener) {
    window.addEventListener('load', function () { Hedgehog.Track(); new Hedgehog$Enhance$Attribute(); }, false);
}
else if (window.attachEvent) {
    window.attachEvent('onload', function () { Hedgehog.Track(); new Hedgehog$Enhance$Attribute(); });
}
