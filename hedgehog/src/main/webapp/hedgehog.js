/*
 * <h1>Hedgehog Analysis Module</h1>
 * <p>Adaption of Google Urchin JS module.</p>
 * <p>2007, Simon Wenmouth</p>
 * <h2>How It Works</h2>
 * <p>There are three cookies used to track web site events</p>
 * <dl>
 *   <dt>a</dt>
 *     <dd>The "a" cookie is a permanent cookie and tracks user access times.</dd>
 *   <dt>b</dt>
 *     <dd>The "b" cookie is a "session" cookie and expires 30 minutes after the last "page view".</dd>
 *   <dt>c</dt>
 *     <dd>The "c" cookie is the "browser" cookie and expires when its window is closed.</dd>
 * </dl>
 * <p>The format of the "a" cookie value is:
 *    <code>{domain_hash}.{user}.{t0}.{tn-1}.{tn}.{counter}</code>. The "b" and "c" cookies are
 *    used to track the client session. The values in the "a" cookie are updated on every new session,
 *    as are the expiry dates on the "b" and "c" cookies.</p>
 */

var Hedgehog = {
    Version: '1',
    CookiePath: '/',
    SessionTimeout: "1800",
    User: Math.round(Math.random()*2147483647),
    SystemIdentifier: "-",
    NoArgs: 0,
    ImagePath: "/hedgehog/__htm.gif",
    RemoveCasTicket: true
}

Hedgehog.Encode = function (s,u) {
    if (typeof(encodeURIComponent) == 'function') {
        if (u)
            return encodeURI(s);
		else
            return encodeURIComponent(s);
	}
	else {
		return escape(s);
	}
}
    
Hedgehog.Hash = function (d) {
	if (!d || d=="")
		return 1;
	var h=0,g=0;
	for (var i=d.length-1;i>=0;i--) {
		var c=parseInt(d.charCodeAt(i));
		h=((h << 6) & 0xfffffff) + c + (c << 14);
		if ((g=h & 0xfe00000)!=0)
			h=(h ^ (g >> 21));
	}
	return h;
}
    
Hedgehog.EmptyFunction = function () {
    return;
}

Hedgehog.Track = function (page)
{
    // arg check
	if (document.location.protocol=="file:")
		return;
	if (Hedgehog.NoArgs == 1 && (!page || page==""))
		return;
	// vars
    var d = new Hedgehog$Document(page);
    var b = new Hedgehog$Browser();
    // track
    new Hedgehog$Sticker(d.DomainName()).Attach(d,b);
	// flag no args execution (if any)
	if (!page || page=="")
		Hedgehog.NoArgs = 1;
}

Hedgehog$Browser = function ()
{
    this._screenResolution = "auto";
    this._screenColorDepth = "auto";
    this._language = "auto";
    this._javaEnabled = "auto";
    this._characterSet = "auto";
    this._shockwaveVersion = "auto";
}

Hedgehog$Browser.prototype.ScreenResolution = function ()
{
    if (this._screenResolution == "auto") {
        if (self.screen) {
    		this._screenResolution=screen.width+"x"+screen.height;
    	}
    	else if (self.java) {
    		var j=java.awt.Toolkit.getDefaultToolkit();
    		var s=j.getScreenSize();
    		this._screenResolution=s.width+"x"+s.height;
    	}
    	else {
        	this._screenResolution = "-";
    	}
    }
    return this._screenResolution;
}

Hedgehog$Browser.prototype.ScreenColorDepth = function ()
{
    if (this._screenColorDepth == "auto") {
        if (self.screen) {
    		this._screenColorDepth=screen.colorDepth+"-bit";
    	}
    	else {
        	this._screenColorDepth = "-";
    	}
    }
    return this._screenColorDepth;
}

Hedgehog$Browser.prototype.Language = function ()
{
    if (this._language == "auto") {
    	if (navigator.language) {
    		this._language=navigator.language.toLowerCase();
    	}
    	else if (navigator.browserLanguage) {
    		this._language=navigator.browserLanguage.toLowerCase();
    	}
    	else {
            this._language = "-";
    	}
    }
    return this._language;
}

Hedgehog$Browser.prototype.JavaEnabled = function ()
{
    if (this._javaEnabled == "auto") {
        this._javaEnabled=navigator.javaEnabled()?1:0;
    }
    return this._javaEnabled;
}

Hedgehog$Browser.prototype.CharacterSet = function ()
{
    if (this._characterSet == "auto") {
    	if (document.characterSet) {
    		this._characterSet=document.characterSet;
        }
    	else if (document.charset) {
    		this._characterSet=document.charset;
        }
        else {
            this._characterSet="-";
        }
    }
    return this._characterSet;
}

Hedgehog$Browser.prototype.ShockwaveVersion = function ()
{
    if (this._shockwaveVersion == "auto") {
        this._shockwaveVersion = "-";
    	if (navigator.plugins && navigator.plugins.length) {
    		for (var ii=0;ii<navigator.plugins.length;ii++) {
    			if (navigator.plugins[ii].name.indexOf('Shockwave Flash')!=-1) {
    				this._shockwaveVersion=navigator.plugins[ii].description.split('Shockwave Flash ')[1];
    				break;
    			}
    		}
    	}
    	else if (window.ActiveXObject) {
    		for (var ii=10;ii>=2;ii--) {
    			try {
    				var fl=eval("new ActiveXObject('ShockwaveFlash.ShockwaveFlash."+ii+"');");
    				if (fl) {
    					f=ii + '.0';
    					break;
    				}
    			}
    			catch(e) {
    				// ignore
    			}
    		}
    	}
    }
    return this._shockwaveVersion;
}

Hedgehog$Browser.prototype.ToExternalForm = function ()
{
    return   "&htmcs=" + Hedgehog.Encode(this.CharacterSet())
            +"&htmsr=" + this.ScreenResolution()
            +"&htmsc=" + this.ScreenColorDepth()
            +"&htmul=" + Hedgehog.Encode(this.Language())
            +"&htmje=" + this.JavaEnabled()
            +"&htmfl=" + Hedgehog.Encode(this.ShockwaveVersion());
}

Hedgehog$Document = function()
{
    this._name  = "auto";
    this._title = "auto";
    this._referrer = "auto";
    this._hostName = "auto";
    this._domainName = "auto";
    this.Initialize.apply(this, arguments);
}

Hedgehog$Document.prototype.Initialize = function(name)
{
    if (name && name!="") {
		this._name=name;
    }
}

Hedgehog$Document.prototype.Name = function()
{
    if (this._name == "auto") {
    	if (Hedgehog.RemoveCasTicket) {
	    	if (document.location.search.length > 1) {
	    		var queryString = document.location.search.substring(1);
	    		var parameters = queryString.split('&');
	    		var output = '';
	    		for (var i = 0; i < parameters.length; i++) {
	    			var atoms = parameters[i].split('=');
	    			if (atoms.length == 1 && atoms[0].length == 0)
	    				continue;
	    			if (atoms.length == 2 && atoms[0] == 'ticket')
	    				continue;
	   				if (output.length > 0)
	   					output += '&';
	   				output += parameters[i];
	    		}
	    		this._name = document.location.pathname + '?' + output;
	    	}
	    	else {
	    		this._name = document.location.pathname;
	    	}
    	}
    	else {
    		this._name = document.location.pathname + document.location.search;
    	}
    }
    return this._name;
}

Hedgehog$Document.prototype.Title = function()
{
    if (this._title == "auto") {
        this._title = "-";
        if (document.title && document.title!="") {
    		this._title = document.title;
        }
    }
    return this._title;
}

Hedgehog$Document.prototype.Referrer = function()
{
    if (this._referrer == "auto") {
        this._referrer = "-";
        if (document.referrer && document.referrer!="") {
    		var r = document.referrer, d = document.domain;
    		if (Hedgehog.CookiePath && Hedgehog.CookiePath != "/") {
    			d += Hedgehog.CookiePath;
    		}
    		var p = r.indexOf(d);
    		if ((p>=0) && (p<8)) { // "https://".length == 8
    			this._referrer = "0";
    		}
    		else if (r.indexOf("[")==0 && r.lastIndexOf("]")==(r.length-1)) {
    			this._referrer = "-";
    		}
    		else {
                this._referrer = document.referrer;
    		}
        }
    }
    return this._referrer;
}

Hedgehog$Document.prototype.HostName = function()
{
    if (this._hostName == "auto") {
        this._hostName = "-";
        if (document.location.hostname && document.location.hostname!="") {
    		this._hostName=document.location.hostname;
        }
    }
    return this._hostName;
}

Hedgehog$Document.prototype.DomainName = function()
{
    if (this._domainName == "auto") {
        var d = document.domain;
		if (d.substring(0,4)=="www.")
            this._domainName = d.substring(4,d.length)
		else
            this._domainName = d;
    }
    return this._domainName;
}

Hedgehog$Document.prototype.ToExternalForm = function()
{
    return  "&htmdt=" + Hedgehog.Encode(this.Title())
          + "&htmhn=" + Hedgehog.Encode(this.HostName())
          + "&htmr=" + Hedgehog.Encode(this.Referrer())
          + "&htmp=" + Hedgehog.Encode(this.Name());
}

Hedgehog$Sticker = function()
{
    this.Initialize.apply(this, arguments);
}

Hedgehog$Sticker.prototype.Initialize = function(domain_name)
{
    // date time
	var datetime = new Date();
	// time
	var time = Math.round(datetime.getTime()/1000);
	// cookie expiry values
	var never_expire = " expires=Sun, 18 Jan 2038 00:00:00 GMT;";
	var expire = new Date(datetime.getTime()+(Hedgehog.SessionTimeout*1000));
	expire = " expires="+expire.toGMTString()+";";
	// domain name hash
	domain_hash = Hedgehog.Hash(domain_name);
	// index of a, b and c cookie values in the document cookie
	var dc = document.cookie;
	a=dc.indexOf("__htma="+domain_hash);
	b=dc.indexOf("__htmb="+domain_hash);
	c=dc.indexOf("__htmc="+domain_hash);
	// the path fragment
	var path_frag = " path=" + Hedgehog.CookiePath;
	// if we have a, b and c cookies update the b cookie
	if (a>=0 && b>=0 && c>=0) {
		document.cookie="__htmb="+domain_hash+";"+expire+path_frag;
	}
	// else create a, b and c cookies
	else {
		a = (a>=0)
			? this.UpdateA(document.cookie, ";", time, domain_hash)
			: this.CreateA(domain_hash, time);
		document.cookie="__htma="+a+";"+never_expire+path_frag;
		document.cookie="__htmb="+domain_hash+";"+expire+path_frag;
		document.cookie="__htmc="+domain_hash+";"+path_frag;
	}
}

Hedgehog$Sticker.prototype.CreateA = function(h,t)
{
    return h+"."+Hedgehog.User+"."+t+"."+t+"."+t+".1";
}

Hedgehog$Sticker.prototype.UpdateA = function (c,s,t,h) {
	if (!c || c=="" || !s || s=="" || !t || t=="")
		return "-";
	var a=this.ParameterValue(c,"__htma="+h,s);
	var lt=0,i=0;
	if ((i=a.lastIndexOf(".")) > 9) {
		// get and increment the counter
		counter=a.substring(i+1,a.length);
		counter=(counter*1)+1;
		// pop off the counter from a
		a=a.substring(0,i);
		// pop off and save the last time
		if ((i=a.lastIndexOf(".")) > 7) {
			last_time=a.substring(i+1,a.length);
			a=a.substring(0,i);
		}
		// pop off the last time but one
		if ((i=a.lastIndexOf(".")) > 5) {
			a=a.substring(0,i);
		}
		// append the new times and counter to "a"
		a+="."+last_time+"."+t+"."+counter;
	}
	return a;
}

Hedgehog$Sticker.prototype.ParameterValue = function(l,n,s)
{
    if (!l || l=="" || !n || n=="" || !s || s=="")
		return "-";
	var i, i2, i3, c = "-";
	i = l.indexOf(n);
	i3 = n.indexOf("=")+1;
	if (i > -1) {
		i2 = l.indexOf(s,i);
		if (i2 < 0)
			i2 = l.length;
		c = l.substring((i+i3),i2);
	}
	return c;
}

Hedgehog$Sticker.prototype.ToExternalForm = function(domain_name)
{
    var t,c="",dc=document.cookie,dh=Hedgehog.Hash(domain_name);
	if ((t=this.ParameterValue(dc,"__htma="+dh,";"))!="-") c+=Hedgehog.Encode("__htma="+t+";+");
	if ((t=this.ParameterValue(dc,"__htmb="+dh,";"))!="-") c+=Hedgehog.Encode("__htmb="+t+";+");
	if ((t=this.ParameterValue(dc,"__htmc="+dh,";"))!="-") c+=Hedgehog.Encode("__htmc="+t+";+");
	if (c.charAt(c.length-1)=="+") c=c.substring(0,c.length-1);
	return c;
}

Hedgehog$Sticker.prototype.Attach = function(document, browser)
{
    if (!document)
        document = new Hedgehog$Document();
    if (!browser)
        browser = new Hedgehog$Browser();
    var i2 = new Image(1,1);
	i2.src = Hedgehog.ImagePath
                + "?"
                + "htmwv=" + Hedgehog.Version
                + document.ToExternalForm()
                + browser.ToExternalForm()
                + "&htmsi=" + Hedgehog.SystemIdentifier
                + "&htmct=" + new Date().getTime()
                + "&htmcc=" + this.ToExternalForm(document.DomainName());
	i2.onload = function() { Hedgehog.EmptyFunction(); }
}

if (typeof(Hedgehog$Bootstrap) == 'function')
{
    if (window.addEventListener) {
        window.addEventListener('load', Hedgehog$Bootstrap, false);
    }
    else if (window.attachEvent) {
        window.attachEvent('onload', Hedgehog$Bootstrap);
    }
}
