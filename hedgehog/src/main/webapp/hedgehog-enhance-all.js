function Hedgehog$Enhance$All() {
    this.doc = new Hedgehog$Document();
    this.events = ['click', 'keyup', 'mousemove'];
    this.elementEvents = {
        'select': ['change'],
        'a': ['mouseover']
    };
    this.elementsByEvents = [];
    this.Initialize();

    // ====================
    // = Legacy Bindingss =
    // ====================
    Hedgehog.Enhancement = this;
    Hedgehog.Enhancement.Dispose = Hedgehog$Event.EventCache.flush;
}
Hedgehog$Enhance$All.prototype = {
    Initialize: function() {
        for (var i = 0, l = this.events.length; i < l; i++) {
            Hedgehog$Event.addEvent(document, this.events[i], this.TrackAction, this);
        };
    },
    getReportTag: function(el) {
        switch (el.tagName) {
        case 'A':
            return 'link';
            break;
        case 'INPUT':
            if (el.type.toLowerCase() != 'submit') return false;
            return 'button';
            break;
        case 'AREA':
            return 'area';
            break;
        case 'SELECT':
            return 'select';
            break;
        default:
            return false;
        }
    },
    addElementEvent: function(e, el) {
        if ( !! this.elementsByEvents[e]) {
            for (var j = 0, l = this.elementsByEvents[e].length; j < l; j++) {
                if (this.elementsByEvents[e][j] == el) return false;
            };
        };
        Hedgehog$Event.addEvent(el, e, this.TrackAction, this);
        if (!this.elementsByEvents[e]) this.elementsByEvents[e] = [];
        this.elementsByEvents[e].push(el);
        return true;
    },
    getLabel: function(el) {
        if (el.parent && el.parent.tagName.toUpperCase() == "LABEL") {
            return el.parent;
        };
        if (el.id) {
            var labels = document.getElementsByTagName('label');
            for (var i = 0, l = labels.length; i < l; i++) {
                if (labels[i].id && labels[i].id == el.id) {
                    return labels[i];
                }
            };
        };
    },
    getTrackingString: function(el) {
        var name = '',
        label;
        if ( !! el.id && el.id != '')
        name += "id=" + el.id + "/";
        label = this.getLabel(el);
        if (label && label.innerHTML)
        name += "label=" + label.innerHTML + "/";
        if ( !! el.value && el.value != '') {
            name += "value=" + el.value + "/";
        } else if ( !! el.options && el.options[el.selectedIndex] &&
        !!el.options[el.selectedIndex].value && el.options[el.selectedIndex].value != '') {
            name += "value=" + el.options[el.selectedIndex].value + "/";
        }
        if ( !! el.text && el.text != '') {
            name += "text=" + el.text + "/";
        } else if ( !! el.options && el.options[el.selectedIndex] &&
        !!el.options[el.selectedIndex].text && el.options[el.selectedIndex].text != '') {
            name += "text=" + el.options[el.selectedIndex].text + "/";
        } else if ( !! el.innerHTML && el.innerHTML != '') {
            name += "text=" + el.innerHTML + "/";
        }
        return name;
    },
    TrackAction: function(e) {
        e = new Hedgehog$Event(e);
        var pageName = this.doc.Name(),
        reportTag = this.getReportTag(e.target),
        type = e.type,
        name = '',
        reportThisEvent = true;
        if (e.target.onclick && type == 'click') reportTag = e.target.tagName;
        if (!reportTag) return;
        name = pageName + '/event/' + type + "/" + reportTag + "/";
        if (type == 'mousemove') reportThisEvent = false;
        if (this.elementEvents[reportTag]) {
            reportThisEvent = false;
            for (var i = 0, l = this.elementEvents[reportTag].length; i < l; i++) {
                if (e.type == this.elementEvents[reportTag][i]) {
                    reportThisEvent = true;
                    continue;
                } else {
                    this.addElementEvent(this.elementEvents[reportTag][i], e.target);
                }
            };
        };
        if (!reportThisEvent) return;
        name += this.getTrackingString(e.target);
        Hedgehog.Track(name);
        if (window.debugService) {
            window.debugService.trace("Hedgehog.Track('" + pageName + name.replace(/(.*)\/event\//, '.../event/') + "');");
        }
    }
};

function Hedgehog$Event(e) {
    if (!e.target) e.target = event.srcElement;
    e.returnValue = true;
    return e;
}
Hedgehog$Event.Delegate = function(scope, fn) {
    var listDelegates = {};
    if (listDelegates[[scope, fn]]) return listDelegates[scope + fn];
    return listDelegates[[scope, fn]] = function(e) {
        fn.call(scope, e);
    };
};
Hedgehog$Event.addEvent = function(obj, type, fn, scope) {
    if (scope) {
        fn = Hedgehog$Event.Delegate(scope, fn);
    }
    if (obj.attachEvent) {
        obj['e' + type + fn] = fn;
        obj[type + fn] = function() {
            obj['e' + type + fn](window.event);
        };
        obj.attachEvent('on' + type, obj[type + fn]);
    } else
    obj.addEventListener(type, fn, false);

    Hedgehog$Event.EventCache.add(obj, type, fn);
};

Hedgehog$Event.removeEvent = function(obj, type, fn) {
    if (obj.detachEvent) {
        obj.detachEvent('on' + type, obj[type + fn]);
        obj[type + fn] = null;
    } else
    obj.removeEventListener(type, fn, false);
};

Hedgehog$Event.EventCache = function() {
    var listEvents = [];
    return {
        listEvents: listEvents,
        add: function(obj, type, fn) {
            listEvents.push([obj, type, fn]);
        },
        flush: function() {
            var i,
            item;
            for (i = listEvents.length - 1; i >= 0; i = i - 1) {
                try {
                    item = listEvents[i];
                    Hedgehog$Event.removeEvent(item[0], item[1], item[2]);
                } catch(e) {
                    if (window.debugService) {
                        window.debugService.trace("==> Hedgehog$Event.EventCache.flush(): " + e.message);
                    }
                }
            };
            Hedgehog.Track();
            if (window.debugService) {
                window.debugService.trace("Hedgehog.Track('window.unload');\n----------------->");
            }
        }
    };
} ();
Hedgehog$Event.addEvent(window, 'load',
function() {
    Hedgehog.Track();
    if (window.debugService) {
        window.debugService.trace("Hedgehog.Track('window.load');");
    }
    new Hedgehog$Enhance$All();

    Hedgehog$Event.addEvent(window, 'unload',
    function() {
        Hedgehog$Event.EventCache.flush();
    });
});