package biz.firstlook.webapp.hedgehog;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biz.firstlook.module.hedgehog.Builder;

public class Track extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(Track.class.toString());

	private static final long serialVersionUID = 830619959922047968L;

	/**
     * Is an unsigned byte array of a 1x1 transparent GIF.
     */
	public static final int[] GIF = new int[] { 0x47, 0x49, 0x46, 0x38, 0x39,
	        0x61, 0x01, 0x00, 0x01, 0x00, 0x80, 0xFF, 0x00, 0xFF, 0xFF, 0xFF,
	        0x00, 0x00, 0x00, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01,
	        0x00, 0x00, 0x02, 0x02, 0x44, 0x01, 0x00, 0x3B };
	
	

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request,
	        HttpServletResponse response) throws ServletException, IOException {
		// map the parameters onto the builder
		final Map<String, String[]> parameterMap = (Map<String, String[]>) request.getParameterMap();
		final Set<Map.Entry<String, String[]>> entrySet = (Set<Map.Entry<String, String[]>>) parameterMap.entrySet();
		final Builder builder = new Builder();
		for (Map.Entry<String, String[]> entry : entrySet) {
			if (entry.getValue().length > 0) {
				builder.setParameter(entry.getKey(), entry.getValue()[0]);
				LOGGER.log(Level.FINE, "Hedgehog Parameters: " + entry.getKey() + " = " + entry.getValue()[0] );
			}
		}
		
		builder.setParameter("remoteAddr", request.getRemoteAddr());
		// build the db record

		// might choose not to build if data does not validate, but for the moment none of it is relevant
		builder.validate();
		
		try {
			builder.build();
		}
		catch (RuntimeException e) {
			LOGGER.log(Level.WARNING, "Failed to record action", e);
		}
		
		// send the transparent gif
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentLength(GIF.length);
		response.setContentType("image/gif");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("cache-control", "no-cache");
		response.addHeader("expires", "0");
		for (int i = 0; i < GIF.length; i++) {
			response.getOutputStream().write(GIF[i]);
		}
		response.flushBuffer();
	}
}
