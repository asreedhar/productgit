package biz.firstlook.module.hedgehog;

import static biz.firstlook.module.hedgehog.Constants.JNDI_DATA_SOURCE_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.sql.DataSource;

import biz.firstlook.module.sql.DataSourceUtils;
import biz.firstlook.module.sql.PreparedStatementCreator;
import biz.firstlook.module.sql.ResultSetUtils;
import biz.firstlook.module.sql.RowMapper;

public class Reference {

	private Integer id;

	private String name;

	public Reference() {
		super();
	}

	public Reference(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Reference(Reference copy) {
		this(copy.getId(), copy.getName());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Integer Id(Reference entity) {
		return (entity == null) ? null : entity.getId();
	}

	protected static <T extends Reference> T find(final Integer id,
	        final String tableName, final ReferenceMapper<T> mapper) {
		T entity = null;
		try {
			PreparedStatementCreator creator = new PreparedStatementCreator() {
				public PreparedStatement createPrepareStatement(
				        Connection connection) throws SQLException {
					PreparedStatement stmt = connection.prepareStatement(
							String.format(
									"SELECT Id, Name FROM [track].[%s] WHERE Id = ?",
									tableName));
					ResultSetUtils.setInt(stmt, 1, id);
					return stmt;
				}
			};
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			entity = DataSourceUtils.first(DataSourceUtils.select(ds, creator, mapper));
		}
		catch (NamingException e) {
			// ignore
		}
		catch (SQLException e) {
			// ignore
		}
		return entity;
	}

	protected static <T extends Reference> T findOrCreate(String name,
	        String tableName, ReferenceMapper<T> mapper) {
		if (Builder.isNullOrEmpty(name))
			return null;
		T entity = null;
		try {
			T criteria = mapper.newReference();
			criteria.setName(name);
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			entity = DataSourceUtils.first(DataSourceUtils.select(ds,
			        new Select(criteria, tableName), mapper));
			if (entity == null) {
				criteria.setId(DataSourceUtils.insert(ds, new Insert(criteria, tableName)));
				if (criteria.getId() != null) {
					entity = criteria;
				}
			}
		}
		catch (NamingException e) {
			// ignore
		}
		catch (SQLException e) {
			// ignore
		}
		return entity;
	}

	static abstract class ReferenceStatementCreator extends Reference implements
	        PreparedStatementCreator {
		private String tableName;

		public ReferenceStatementCreator(Reference copy, String tableName) {
			super(copy);
			this.tableName = tableName;
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			PreparedStatement stmt = connection.prepareStatement(
					String.format(query(), tableName));
			stmt.setString(1, getName());
			return stmt;
		}

		protected abstract String query();
	}

	static class Select extends ReferenceStatementCreator implements
	        PreparedStatementCreator {
		public Select(Reference copy, String tableName) {
			super(copy, tableName);
		}

		protected String query() {
			return "SELECT Id, Name FROM [track].[%s] WHERE Name = ?";
		}
	}

	static class Insert extends ReferenceStatementCreator implements
	        PreparedStatementCreator {
		public Insert(Reference copy, String tableName) {
			super(copy, tableName);
		}

		protected String query() {
			return "INSERT INTO [track].[%s] (Name) VALUES (?)";
		}
	}

	static abstract class ReferenceMapper<T extends Reference> implements
	        RowMapper<T> {
		public T mapRow(ResultSet rs, int rowNum) throws SQLException {
			T entity = newReference();
			entity.setId(rs.getInt("Id"));
			entity.setName(rs.getString("Name"));
			return entity;
		}

		public abstract T newReference();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append("Id=").append(getId());
		sb.append(", Name=").append(getName());
		return sb.append("}").toString();
	}
}
