package biz.firstlook.module.hedgehog;

import static biz.firstlook.module.hedgehog.Constants.JNDI_DATA_SOURCE_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.sql.DataSource;

import biz.firstlook.module.sql.DataSourceUtils;
import biz.firstlook.module.sql.PreparedStatementCreator;
import biz.firstlook.module.sql.ResultSetUtils;
import biz.firstlook.module.sql.RowMapper;

public class Session {

	private static final Logger LOGGER = Logger.getLogger(Session.class.toString());
	
	private Integer id;
	private User user;
	private Integer sequence;
	private Browser browser;
	private String remoteAddr;
	private Date createdRoot;
	private Date createdPrev;
	private Date createdThis;

	public Session() {
		super();
	}

	public Session(Integer id, User user, Integer sequence, Browser browser,
			String remoteAddr, Date createdRoot, Date createdPrev, Date createdThis) {
		super();
		this.id = id;
		this.user = user;
		this.sequence = sequence;
		this.browser = browser;
		this.remoteAddr = remoteAddr;
		this.createdRoot = createdRoot;
		this.createdPrev = createdPrev;
		this.createdThis = createdThis;
	}

	public Session(Session copy) {
		this(copy.getId(), copy.getUser(), copy.getSequence(),
				copy.getBrowser(), copy.getRemoteAddr(), copy.getCreatedRoot(), copy.getCreatedPrev(),
		        copy.getCreatedThis());
	}

	public Browser getBrowser() {
		return browser;
	}

	public void setBrowser(Browser browser) {
		this.browser = browser;
	}

	public Date getCreatedPrev() {
		return createdPrev;
	}

	public void setCreatedPrev(Date createdPrev) {
		this.createdPrev = createdPrev;
	}

	public Date getCreatedRoot() {
		return createdRoot;
	}

	public void setCreatedRoot(Date createdRoot) {
		this.createdRoot = createdRoot;
	}

	public Date getCreatedThis() {
		return createdThis;
	}

	public void setCreatedThis(Date createdThis) {
		this.createdThis = createdThis;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRemoteAddr() {
    	return remoteAddr;
    }

	public void setRemoteAddr(String remoteAddr) {
    	this.remoteAddr = remoteAddr;
    }

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public static Integer Id(Session session) {
		return (session == null) ? null : session.getId();
	}

	public static Session findOrCreate(User user, Browser browser,
	        String remoteAddr, int sequence, long ct, long lr, long lp, long lt) {
		// convert the seconds to dates
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long clientDiff = c.getTimeInMillis() - ct;
		Date dr = null, dp = null, dt = null;
		c.setTimeInMillis(lr * 1000 + clientDiff);
		dr = c.getTime();
		c.setTimeInMillis(lp * 1000 + clientDiff);
		dp = c.getTime();
		c.setTimeInMillis(lt * 1000 + clientDiff);
		dt = c.getTime();
		// build the criteria
		Session criteria = new Session();
		criteria.setUser(user);
		criteria.setBrowser(browser);
		criteria.setRemoteAddr(remoteAddr);
		criteria.setSequence(sequence);
		criteria.setCreatedRoot(dr);
		criteria.setCreatedPrev(dp);
		criteria.setCreatedThis(dt);
		// find the active session for the user
		Session session = select(new Select(criteria));
		if (session != null) {
			// turn off old sessions
			if (session.getSequence() < sequence) {
				if (update(new Update(session)) > 0) {
					session = null;
				}
			}
		}
		// insert the new session
		if (session == null) {
			PreparedStatementCreator creator = new Insert(criteria);
			try
			{
				LOGGER.log(Level.FINE, "Attempting to insert session: " + creator + "...");
				criteria.setId(insert(creator));
				if (criteria.getId() != null) {
					session = criteria;
				}
				LOGGER.log(Level.FINE, "...successfully insert session: " + creator);
			}
			catch (SQLException e) {
				LOGGER.log(Level.WARNING, "...failed to insert Session (probably race to sequence)" );
				
				LOGGER.log(Level.FINE, "Trying to select again: " + creator);
				
				session = select(new Select(criteria));
				
				//need something here to turn off old sessions (again)
			}
		}
		
		if( session == null )
			LOGGER.log(Level.SEVERE, "Session is null." );
		
		return session;
	}

	static class Select extends Session implements PreparedStatementCreator {
		public Select(Session copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT [Id]");
			sb.append(",[UserId]");
			sb.append(",[Sequence]");
			sb.append(",[BrowserId]");
			sb.append(",[RemoteAddr]");
			sb.append(",[Active]");
			sb.append(",[CreatedRoot]");
			sb.append(",[CreatedPrev]");
			sb.append(",[CreatedThis]");
			sb.append(" FROM [track].[Session]");
			sb.append(" WHERE");
			sb.append(" [UserId] = ? AND [Active] = 1");
			PreparedStatement stmt = connection.prepareStatement(sb.toString());
			ResultSetUtils.setInt(stmt, 1, getUser() == null ? null : getUser().getId());
			return stmt;
		}
	}

	static class Update extends Session implements PreparedStatementCreator {
		public Update(Session copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			PreparedStatement stmt = connection.prepareStatement(
					"UPDATE [track].[Session] SET Active = 0 WHERE Id = ?");
			ResultSetUtils.setInt(stmt, 1, getId());
			return stmt;
		}
	}

	static class Insert extends Session implements PreparedStatementCreator {
		public Insert(Session copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			StringBuffer sb = new StringBuffer();
			sb.append("INSERT INTO [track].[Session]");
			sb.append("([UserId]");
			sb.append(",[Sequence]");
			sb.append(",[BrowserId]");
			sb.append(",[RemoteAddr]");
			sb.append(",[Active]");
			sb.append(",[CreatedRoot]");
			sb.append(",[CreatedPrev]");
			sb.append(",[CreatedThis])");
			sb.append(" VALUES");
			sb.append("(?,?,?,?,?,?,?,?)");
			PreparedStatement stmt = connection.prepareStatement(sb.toString());
			ResultSetUtils.setInt(stmt, 1, getUser().getId());
			ResultSetUtils.setInt(stmt, 2, getSequence());
			ResultSetUtils.setInt(stmt, 3, getBrowser().getId());
			ResultSetUtils.setString(stmt, 4, getRemoteAddr());
			ResultSetUtils.setBoolean(stmt, 5, Boolean.TRUE);
			ResultSetUtils.setDate(stmt, 6, getCreatedRoot());
			ResultSetUtils.setDate(stmt, 7, getCreatedPrev());
			ResultSetUtils.setDate(stmt, 8, getCreatedThis());
			return stmt;
		}
	}

	static class Mapper implements RowMapper<Session> {
		public Session mapRow(ResultSet rs, int rowNum) throws SQLException {
			Session entity = new Session();
			entity.setId(rs.getInt("Id"));
			entity.setUser(User.find(rs.getInt("UserId")));
			entity.setSequence(rs.getInt("Sequence"));
			entity.setBrowser(Browser.find(rs.getInt("BrowserId")));
			entity.setRemoteAddr(rs.getString("RemoteAddr"));
			entity.setCreatedRoot(rs.getTimestamp("CreatedRoot"));
			entity.setCreatedPrev(rs.getTimestamp("CreatedPrev"));
			entity.setCreatedThis(rs.getTimestamp("CreatedThis"));
			return entity;
		}
	}

	static Session select(PreparedStatementCreator creator) {
		Session session = null;
		try {
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			session = DataSourceUtils.first(DataSourceUtils.select(ds, creator, new Mapper()));
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to load Session", e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to load Session", e);
		}
		return session;
	}

	static Integer insert(PreparedStatementCreator creator) throws SQLException {
		Integer id = null;
		try {
			id = DataSourceUtils.insert(DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME), creator);
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to insert Session " + creator, e);
		}
		return id;
	}

	static int update(PreparedStatementCreator creator) {
		int rows = -1;
		try {
			rows = DataSourceUtils.update(
					DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME),
					creator);
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to update Session " + creator, e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to update Session " + creator, e);
		}
		return rows;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append("Id=").append(getId());
		sb.append(", UserId=").append(getUser());
		sb.append(", Sequence=").append(getSequence());
		sb.append(", BrowserId=").append(getBrowser());
		sb.append(", RemoteAddr=").append(getRemoteAddr());
		sb.append(", Active=").append(true);
		sb.append(", CreatedRoot=").append(getCreatedRoot());
		sb.append(", CreatedPrev=").append(getCreatedPrev());
		sb.append(", CreatedThis=").append(getCreatedThis());
		sb.append("}");
		return sb.toString();
	}
	
}
