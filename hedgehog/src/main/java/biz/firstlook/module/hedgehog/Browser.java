package biz.firstlook.module.hedgehog;

import static biz.firstlook.module.hedgehog.Constants.JNDI_DATA_SOURCE_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.sql.DataSource;

import biz.firstlook.module.sql.DataSourceUtils;
import biz.firstlook.module.sql.PreparedStatementCreator;
import biz.firstlook.module.sql.ResultSetUtils;
import biz.firstlook.module.sql.RowMapper;

public class Browser {

	private static final Logger LOGGER = Logger.getLogger(Browser.class.toString());

	private Integer id;
	private ScreenResolution screenResolution;
	private ColorDepth colorDepth;
	private Language language;
	private CharacterSet characterSet;
	private ShockwaveVersion shockwaveVersion;
	private boolean javaEnabled;

	public Browser() {
		super();
	}

	public Browser(Integer id, ScreenResolution screenResolution,
	        ColorDepth colorDepth, Language language,
	        CharacterSet characterSet, ShockwaveVersion shockwaveVersion,
	        boolean javaEnabled) {
		super();
		this.id = id;
		this.screenResolution = screenResolution;
		this.colorDepth = colorDepth;
		this.language = language;
		this.characterSet = characterSet;
		this.shockwaveVersion = shockwaveVersion;
		this.javaEnabled = javaEnabled;
	}

	public Browser(Browser copy) {
		this(copy.getId(), copy.getScreenResolution(), copy.getColorDepth(),
		        copy.getLanguage(), copy.getCharacterSet(),
		        copy.getShockwaveVersion(), copy.isJavaEnabled());
	}

	public CharacterSet getCharacterSet() {
		return characterSet;
	}

	public void setCharacterSet(CharacterSet characterSet) {
		this.characterSet = characterSet;
	}

	public ColorDepth getColorDepth() {
		return colorDepth;
	}

	public void setColorDepth(ColorDepth colorDepth) {
		this.colorDepth = colorDepth;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isJavaEnabled() {
		return javaEnabled;
	}

	public void setJavaEnabled(boolean javaEnabled) {
		this.javaEnabled = javaEnabled;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public ScreenResolution getScreenResolution() {
		return screenResolution;
	}

	public void setScreenResolution(ScreenResolution screenResolution) {
		this.screenResolution = screenResolution;
	}

	public ShockwaveVersion getShockwaveVersion() {
		return shockwaveVersion;
	}

	public void setShockwaveVersion(ShockwaveVersion shockwaveVersion) {
		this.shockwaveVersion = shockwaveVersion;
	}

	public static Browser find(final Integer id) {
		Browser browser = null;
		try {
			// criteria
			PreparedStatementCreator creator = new PreparedStatementCreator() {
				public PreparedStatement createPrepareStatement(
				        Connection connection) throws SQLException {
					StringBuffer sb = new StringBuffer();
					sb.append("SELECT [Id]");
					sb.append(",[ScreenResolutionId]");
					sb.append(",[ColorDepthId]");
					sb.append(",[LanguageId]");
					sb.append(",[CharacterSetId]");
					sb.append(",[JavaEnabled]");
					sb.append(",[ShockwaveVersionId]");
					sb.append(" FROM [track].[Browser]");
					sb.append(" WHERE [Id] = ?");
					PreparedStatement stmt = connection.prepareStatement(sb.toString());
					stmt.setInt(1, id);
					return stmt;
				}
			};
			// select or insert
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			browser = DataSourceUtils.first(DataSourceUtils.select(ds, creator, new Mapper()));
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to load Browser", e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to load Browser", e);
		}
		return browser;
	}

	public static Browser findOrCreate(String screenResolution,
	        String colorDepth, String language, String characterSet,
	        String shockwaveVersion, Boolean javaEnabled) {
		Browser browser = null;
		try {
			// criteria
			Browser criteria = new Browser();
			criteria.setScreenResolution(ScreenResolution.findOrCreate(screenResolution));
			criteria.setColorDepth(ColorDepth.findOrCreate(colorDepth));
			criteria.setLanguage(Language.findOrCreate(language));
			criteria.setCharacterSet(CharacterSet.findOrCreate(characterSet));
			criteria.setShockwaveVersion(ShockwaveVersion.findOrCreate(shockwaveVersion));
			criteria.setJavaEnabled(javaEnabled);
			// select or insert
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			browser = DataSourceUtils.first(DataSourceUtils.select(ds,
			        new Select(criteria), new Mapper()));
			if (browser == null) {
				criteria.setId(DataSourceUtils.insert(ds, new Insert(criteria)));
				if (criteria.getId() != null) {
					browser = criteria;
				}
			}
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to find/create Browser", e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to find/create Browser", e);
		}
		return browser;
	}

	static class Select extends Browser implements PreparedStatementCreator {
		public Select(Browser copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT [Id]");
			sb.append(",[ScreenResolutionId]");
			sb.append(",[ColorDepthId]");
			sb.append(",[LanguageId]");
			sb.append(",[CharacterSetId]");
			sb.append(",[JavaEnabled]");
			sb.append(",[ShockwaveVersionId]");
			sb.append(" FROM [track].[Browser]");
			sb.append(" WHERE");
			sb.append(" [ScreenResolutionId] = ?");
			sb.append("AND [ColorDepthId] = ?");
			sb.append("AND [LanguageId] = ?");
			sb.append("AND [CharacterSetId] = ?");
			sb.append("AND [JavaEnabled] = ?");
			sb.append("AND [ShockwaveVersionId] = ?");
			PreparedStatement stmt = connection.prepareStatement(sb.toString());
			ApplyCriteria(stmt, this);
			return stmt;
		}
	}

	static class Insert extends Browser implements PreparedStatementCreator {
		public Insert(Browser copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			StringBuffer sb = new StringBuffer();
			sb.append("INSERT INTO [track].[Browser]");
			sb.append("([ScreenResolutionId]");
			sb.append(",[ColorDepthId]");
			sb.append(",[LanguageId]");
			sb.append(",[CharacterSetId]");
			sb.append(",[JavaEnabled]");
			sb.append(",[ShockwaveVersionId])");
			sb.append(" VALUES");
			sb.append("(?,?,?,?,?,?)");
			PreparedStatement stmt = connection.prepareStatement(sb.toString());
			ApplyCriteria(stmt, this);
			return stmt;
		}
	}

	static void ApplyCriteria(PreparedStatement stmt, Browser criteria)
	        throws SQLException {
		ResultSetUtils.setInt(stmt, 1, Reference.Id(criteria.getScreenResolution()));
		ResultSetUtils.setInt(stmt, 2, Reference.Id(criteria.getColorDepth()));
		ResultSetUtils.setInt(stmt, 3, Reference.Id(criteria.getLanguage()));
		ResultSetUtils.setInt(stmt, 4, Reference.Id(criteria.getCharacterSet()));
		ResultSetUtils.setBoolean(stmt, 5, criteria.isJavaEnabled());
		ResultSetUtils.setInt(stmt, 6, Reference.Id(criteria.getShockwaveVersion()));
	}

	static class Mapper implements RowMapper<Browser> {
		public Browser mapRow(ResultSet rs, int rowNum) throws SQLException {
			Browser entity = new Browser();
			entity.setId(rs.getInt("Id"));
			entity.setScreenResolution(ScreenResolution.find(ResultSetUtils.getInt(rs, "ScreenResolutionId")));
			entity.setColorDepth(ColorDepth.find(ResultSetUtils.getInt(rs, "ColorDepthId")));
			entity.setLanguage(Language.find(ResultSetUtils.getInt(rs, "LanguageId")));
			entity.setCharacterSet(CharacterSet.find(ResultSetUtils.getInt(rs, "CharacterSetId")));
			entity.setJavaEnabled(ResultSetUtils.getBoolean(rs, "JavaEnabled"));
			entity.setShockwaveVersion(ShockwaveVersion.find(ResultSetUtils.getInt(rs, "ShockwaveVersionId")));
			return entity;
		}
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append(" Id=").append(getId());
		sb.append(", ScreenResolution=").append(getScreenResolution());
		sb.append(", ColorDepth=").append(getColorDepth());
		sb.append(", Language=").append(getLanguage());
		sb.append(", CharacterSet=").append(getCharacterSet());
		sb.append(", JavaEnabled=").append(isJavaEnabled());
		sb.append(", ShockwaveVersion=").append(getShockwaveVersion());
		return sb.append("}").toString();
	}
}
