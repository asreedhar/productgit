package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class CharacterSet extends Reference {

	private static final String TABLE_NAME = "CharacterSet";

	public CharacterSet() {
		super();
	}

	public CharacterSet(Integer id, String name) {
		super(id, name);
	}

	public CharacterSet(CharacterSet copy) {
		super(copy);
	}

	public static CharacterSet find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static CharacterSet findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<CharacterSet> implements
	        RowMapper<CharacterSet> {
		public CharacterSet newReference() {
			return new CharacterSet();
		}
	}

}
