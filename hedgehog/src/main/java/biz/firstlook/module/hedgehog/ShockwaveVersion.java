package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class ShockwaveVersion extends Reference {

	private static final String TABLE_NAME = "ShockwaveVersion";

	public ShockwaveVersion() {
		super();
	}

	public ShockwaveVersion(Integer id, String name) {
		super(id, name);
	}

	public ShockwaveVersion(ShockwaveVersion copy) {
		super(copy);
	}

	public static ShockwaveVersion find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static ShockwaveVersion findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<ShockwaveVersion> implements
	        RowMapper<ShockwaveVersion> {
		public ShockwaveVersion newReference() {
			return new ShockwaveVersion();
		}
	}

}
