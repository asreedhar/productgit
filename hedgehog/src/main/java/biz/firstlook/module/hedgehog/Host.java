package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class Host extends Reference {

	private static final String TABLE_NAME = "Host";

	public Host() {
		super();
	}

	public Host(Integer id, String name) {
		super(id, name);
	}

	public Host(Host copy) {
		super(copy);
	}

	public static Host find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static Host findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<Host> implements
	        RowMapper<Host> {
		public Host newReference() {
			return new Host();
		}
	}

}
