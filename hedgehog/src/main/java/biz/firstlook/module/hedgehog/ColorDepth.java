package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class ColorDepth extends Reference {

	private static final String TABLE_NAME = "ColorDepth";

	public ColorDepth() {
		super();
	}

	public ColorDepth(Integer id, String name) {
		super(id, name);
	}

	public ColorDepth(ColorDepth copy) {
		super(copy);
	}

	public static ColorDepth find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static ColorDepth findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<ColorDepth> implements
	        RowMapper<ColorDepth> {
		public ColorDepth newReference() {
			return new ColorDepth();
		}
	}

}
