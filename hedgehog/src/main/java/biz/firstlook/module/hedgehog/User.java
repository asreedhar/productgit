package biz.firstlook.module.hedgehog;

import static biz.firstlook.module.hedgehog.Constants.JNDI_DATA_SOURCE_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.sql.DataSource;

import biz.firstlook.module.sql.DataSourceUtils;
import biz.firstlook.module.sql.PreparedStatementCreator;
import biz.firstlook.module.sql.ResultSetUtils;
import biz.firstlook.module.sql.RowMapper;

public class User {

	private static final Logger LOGGER = Logger.getLogger(DataSourceUtils.class.toString());
	 
	private Integer id;
	private Integer domain;
	private Integer identifier;
	private Integer systemIdentifier;

	public User() {
		super();
	}

	public User(Integer id, Integer domain, Integer identifier,
	        Integer systemIdentifier) {
		super();
		this.id = id;
		this.domain = domain;
		this.identifier = identifier;
		this.systemIdentifier = systemIdentifier;
	}

	public User(User copy) {
		this(copy.getId(), copy.getDomain(), copy.getIdentifier(), copy.getSystemIdentifier());
	}

	public Integer getDomain() {
		return domain;
	}

	public void setDomain(Integer domain) {
		this.domain = domain;
	}

	public Integer getSystemIdentifier() {
		return systemIdentifier;
	}

	public void setSystemIdentifier(Integer systemIdentifier) {
		this.systemIdentifier = systemIdentifier;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	public static User find(final Integer id) {
		User user = null;
		try {
			// criteria
			PreparedStatementCreator creator = new PreparedStatementCreator() {
				public PreparedStatement createPrepareStatement(
				        Connection connection) throws SQLException {
					PreparedStatement stmt = connection.prepareStatement(
							"SELECT Id, Domain, Identifier, SystemIdentifier FROM [track].[User] WHERE Id = ?");
					ResultSetUtils.setInt(stmt, 1, id);
					return stmt;
				}
			};
			// select or insert
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			user = DataSourceUtils.first(DataSourceUtils.select(ds, creator, new Mapper()));
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to load User", e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to load User", e);
		}
		return user;
	}

	public static User findOrCreate(Integer domain, Integer identifier,
	        Integer systemIdentifier) {
		User user = null;
		try {
			// criteria
			User criteria = new User();
			criteria.setDomain(domain);
			criteria.setIdentifier(identifier);
			criteria.setSystemIdentifier(systemIdentifier);
			// select or insert
			DataSource ds = DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME);
			user = DataSourceUtils.first(DataSourceUtils.select(
					ds, new Select(criteria), new Mapper()));
			if (user == null) {
				criteria.setId(DataSourceUtils.insert(ds, new Insert(criteria)));
				if (criteria.getId() != null) {
					user = criteria;
				}
			}
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to find/create User", e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to find/create User", e);
		}
		return user;
	}

	static class Select extends User implements PreparedStatementCreator {
		public Select(User copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT Id, Domain, Identifier, SystemIdentifier FROM [track].[User] WHERE Domain = ? AND Identifier = ?");
			ResultSetUtils.setInt(stmt, 1, getDomain());
			ResultSetUtils.setInt(stmt, 2, getIdentifier());
			return stmt;
		}
	}

	static class Insert extends User implements PreparedStatementCreator {
		public Insert(User copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO [track].[User] (Domain, Identifier, SystemIdentifier) VALUES (?,?,?)");
			ResultSetUtils.setInt(stmt, 1, getDomain());
			ResultSetUtils.setInt(stmt, 2, getIdentifier());
			ResultSetUtils.setInt(stmt, 3, getSystemIdentifier());
			return stmt;
		}
	}

	static class Mapper implements RowMapper<User> {
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getInt("Id"));
			user.setDomain(rs.getInt("Domain"));
			user.setIdentifier(rs.getInt("Identifier"));
			user.setSystemIdentifier(rs.getInt("SystemIdentifier"));
			return user;
		}
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append("Id=").append(getId());
		sb.append(", Domain=").append(getDomain());
		sb.append(", Identifier=").append(getIdentifier());
		sb.append(", SystemIdentifier=").append(getSystemIdentifier());
		return sb.append("}").toString();
	}
}
