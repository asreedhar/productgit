package biz.firstlook.module.hedgehog;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Builder {

	private static final Logger LOGGER = Logger.getLogger(Builder.class.toString());

	// decoder charset
	private static final String CHARSET = "US-ASCII";

	// page constants
	public static final String P_TITLE = "htmdt";
	public static final String P_HOSTNAME = "htmhn";
	public static final String P_REFERRER = "htmr";
	public static final String P_PAGE = "htmp";
	public static final String P_CLIENTTIME = "htmct";
	public static final String P_COOKIES = "htmcc";
	public static final String P_SYSTEM_IDENTIFIER = "htmsi";

	// browser constants
	public static final String B_CHARACTER_SET = "htmcs";
	public static final String B_SCREEN_RESOLUTION = "htmsr";
	public static final String B_SCREEN_COLOR_DEPTH = "htmsc";
	public static final String B_LANGUAGE = "htmul";
	public static final String B_JAVA_ENABLED = "htmje";
	public static final String B_SHOCKWAVE_VERSION = "htmfl";

	// server constants
	public static final String S_REMOTE_ADDR = "remoteAddr";
	
	// parameter values
	private String characterSet;
	private String colorDepth;
	private String hostname;
	private String language;
	private String pageName;
	private String pageTitle;
	private String referrer;
	private String screenResolution;
	private String shockwaveVersion;
	private Boolean javaEnabled;

	// cookie concatenation values
	private Integer domainHashA, domainHashB, domainHashC;
	private Integer identifier, systemIdentifier;
	private Integer counter;
	private Long clientTime;
	private Long t0, tm, tn;

	// server information
	private String remoteAddr;
	
	public Builder() {
		super();
	}

	public void setParameter(final String name, final String value) {
		// ignore bad parameters
		if (isNullOrEmpty(name)) {
			return;
		}
		// page attributes
		if (P_TITLE.equals(name)) {
			pageTitle = value;
		}
		else if (P_HOSTNAME.equals(name)) {
			hostname = value;
		}
		else if (P_REFERRER.equals(name)) {
			referrer = value;
		}
		else if (P_PAGE.equals(name)) {
			pageName = value;
		}
		else if (P_SYSTEM_IDENTIFIER.equals(name)) {
			systemIdentifier = toInt(value);
		}
		// browser parameters
		else if (B_CHARACTER_SET.equals(name)) {
			characterSet = value;
		}
		else if (B_JAVA_ENABLED.equals(name)) {
			javaEnabled = Boolean.parseBoolean(value);
		}
		else if (B_LANGUAGE.equals(name)) {
			language = value;
		}
		else if (B_SCREEN_COLOR_DEPTH.equals(name)) {
			colorDepth = value;
		}
		else if (B_SCREEN_RESOLUTION.equals(name)) {
			screenResolution = value;
		}
		else if (B_SHOCKWAVE_VERSION.equals(name)) {
			shockwaveVersion = value;
		}
		// cookies
		else if (P_CLIENTTIME.equals(name)) {
			clientTime = toLong(value);
		}
		else if (P_COOKIES.equals(name)) {
			if (!isNullOrEmpty(value)) {
				try {
					// extract values
					final String v = URLDecoder.decode(value, CHARSET);
					final String a = parameterValue(v, "__htma=", ";");
					final String b = parameterValue(v, "__htmb=", ";");
					final String c = parameterValue(v, "__htmc=", ";");
					// a values
					if (!isNullOrEmpty(a)) {
						String[] info = a.split("\\.");
						if (info.length == 6) {
							domainHashA = toInt(info[0]);
							identifier = toInt(info[1]);
							t0 = toLong(info[2]);
							tm = toLong(info[3]);
							tn = toLong(info[4]);
							counter = toInt(info[5]);
						}
					}
					// b values
					if (!isNullOrEmpty(b)) {
						domainHashB = toInt(b);
					}
					// c values
					if (!isNullOrEmpty(c)) {
						domainHashC = toInt(c);
					}
				}
				catch (UnsupportedEncodingException e) {
					LOGGER.log(Level.WARNING,
					        "Failed to decode cookie concatenation", e);
				}
			}
		}
		// server data
		else if (S_REMOTE_ADDR.equals(name)) {
			if (!isNullOrEmpty(value)) {
				remoteAddr = value;
			}
		}
	}
	
	public boolean validateReferrer()
	{
		boolean result = true;
		
		if( referrer != null && referrer.equals("-") )
		{
			result = false;
			LOGGER.log(Level.WARNING, "Referrer set to -");
		}
		
		if( referrer != null && referrer.equals("0") )
		{
			result = false;
			LOGGER.log(Level.WARNING, "Referrer set to 0");
		}
		
		return result;
	}
	
	public boolean validateClientTime()
	{
		boolean result = true;
		
		if( null == clientTime )
		{
			clientTime = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
		}
		
		return result;
	}
	
	public boolean validate()
	{
		boolean result = true;
		
		result &= validateReferrer();
		result &= validateClientTime();
		
		return result;
	}
	

	public void build() {
		// check for data sufficiency
		if (hasSufficientInformation()) {
			Session session = buildSession();
			
			if( null == session ) //possible race condition exists, try again
			{
				session = buildSession();
			}
			
			if( session != null )
				Action.create(session, pageName, pageTitle, hostname,
			        referrer);
			else
				throw new RuntimeException( "Session was null " ); 
		}
	}

	private Session buildSession() {
		return Session.findOrCreate(buildUser(), buildBrowser(), remoteAddr, counter, clientTime, t0,
		        tm, tn);
	}

	private Browser buildBrowser() {
		return Browser.findOrCreate(screenResolution, colorDepth, language,
		        characterSet, shockwaveVersion, javaEnabled);
	}

	private User buildUser() {
		return User.findOrCreate(domainHashA, identifier, systemIdentifier);
	}

	private boolean hasSufficientInformation() {
		boolean sufficient = true;
		// 1. User => Domain + Identifier
		if (domainHashA == null || identifier == null) {
			sufficient = false;
		}
		// 2. Session => User + Sequence
		else if (counter == null) {
			sufficient = false;
		}
		// 3. Action => Resource => Page Name
		else if (isNullOrEmpty(pageName)) {
			sufficient = false;
		}
		return sufficient;
	}

	private static String parameterValue(String text, String key,
	        String seperator) {
		if (isNullOrEmpty(text) || isNullOrEmpty(key)
		        || isNullOrEmpty(seperator))
			return null;
		String value = null;
		int i;
		int i2;
		int i3;
		i = text.indexOf(key);
		i3 = key.indexOf('=') + 1;
		if (i > -1) {
			i2 = text.indexOf(seperator, i);
			if (i2 < 0)
				i2 = text.length();
			value = text.substring(i + i3, i2);
		}
		return value;
	}

	public static Long toLong(String str) {
		Long l = null;
		try {
			l = new Long(str);
		}
		catch (NumberFormatException nfe) {
			l = null;
		}
		return l;
	}

	public static Integer toInt(String str) {
		Integer i = null;
		try {
			i = new Integer(str);
		}
		catch (NumberFormatException nfe) {
			i = null;
		}
		return i;
	}

	public static boolean isNullOrEmpty(String str) {
		return (str == null || str.trim().length() == 0);
	}

}
