package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class ScreenResolution extends Reference {

	private static final String TABLE_NAME = "ScreenResolution";

	public ScreenResolution() {
		super();
	}

	public ScreenResolution(Integer id, String name) {
		super(id, name);
	}

	public ScreenResolution(ScreenResolution copy) {
		super(copy);
	}

	public static ScreenResolution find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static ScreenResolution findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<ScreenResolution> implements
	        RowMapper<ScreenResolution> {
		public ScreenResolution newReference() {
			return new ScreenResolution();
		}
	}

}
