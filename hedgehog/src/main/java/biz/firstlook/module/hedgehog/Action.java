package biz.firstlook.module.hedgehog;

import static biz.firstlook.module.hedgehog.Constants.JNDI_DATA_SOURCE_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import biz.firstlook.module.sql.DataSourceUtils;
import biz.firstlook.module.sql.PreparedStatementCreator;
import biz.firstlook.module.sql.ResultSetUtils;

public class Action {

	private static final Logger LOGGER = Logger.getLogger(Action.class.toString());
	
	private Integer id;
	private Session session;
	private Resource resource;
	private ResourceTitle resourceTitle;
	private Resource referrer;
	private Host host;
	private Date timestamp;

	public Action() {
		super();
	}

	public Action(Integer id, Session session, Resource resource,
	        ResourceTitle resourceTitle, Resource referrer, Host host,
	        Date timestamp) {
		super();
		this.id = id;
		this.session = session;
		this.resource = resource;
		this.resourceTitle = resourceTitle;
		this.referrer = referrer;
		this.host = host;
		this.timestamp = timestamp;
	}

	public Action(Action copy) {
		this(copy.getId(), copy.getSession(), copy.getResource(),
				copy.getResourceTitle(), copy.getReferrer(), copy.getHost(),
				copy.getTimestamp());
	}

	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Resource getReferrer() {
		return referrer;
	}

	public void setReferrer(Resource referrer) {
		this.referrer = referrer;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceTitle getResourceTitle() {
		return resourceTitle;
	}

	public void setResourceTitle(ResourceTitle resourceTitle) {
		this.resourceTitle = resourceTitle;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public static void create(Session session, String pageName,
	        String pageTitle, String hostname, String referrer) {
		try {
			// criteria
			Action criteria = new Action();
			criteria.setSession(session);
			criteria.setResource(Resource.findOrCreate(pageName));
			criteria.setResourceTitle(ResourceTitle.findOrCreate(pageTitle));
			criteria.setHost(Host.findOrCreate(hostname));
			criteria.setReferrer(Resource.findOrCreate(referrer));
			criteria.setTimestamp(Calendar.getInstance(
			        TimeZone.getTimeZone("GMT")).getTime());
			// insert
			DataSourceUtils.insert(
					DataSourceUtils.getDataSource(JNDI_DATA_SOURCE_NAME),
					new Insert(criteria));
		}
		catch (NamingException e) {
			LOGGER.log(Level.WARNING, "Failed to create Action", e);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Failed to create Action", e);
		}
	}

	static class Insert extends Action implements PreparedStatementCreator {
		public Insert(Action copy) {
			super(copy);
		}

		public PreparedStatement createPrepareStatement(Connection connection)
		        throws SQLException {
			StringBuffer sb = new StringBuffer();
			sb.append("INSERT INTO [track].[Action]");
			sb.append("([SessionId]");
			sb.append(",[ResourceId]");
			sb.append(",[ResourceTitleId]");
			sb.append(",[ReferrerId]");
			sb.append(",[HostId]");
			sb.append(",[Timestamp])");
			sb.append(" VALUES");
			sb.append("(?,?,?,?,?,?)");
			PreparedStatement stmt = connection.prepareStatement(sb.toString());
			ResultSetUtils.setInt(stmt, 1, Session.Id(getSession()));
			ResultSetUtils.setInt(stmt, 2, Reference.Id(getResource()));
			ResultSetUtils.setInt(stmt, 3, Reference.Id(getResourceTitle()));
			ResultSetUtils.setInt(stmt, 4, Reference.Id(getReferrer()));
			ResultSetUtils.setInt(stmt, 5, Reference.Id(getHost()));
			ResultSetUtils.setDate(stmt, 6, getTimestamp());
			return stmt;
		}
	}

}
