package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class Resource extends Reference {

	private static final String TABLE_NAME = "Resource";

	public Resource() {
		super();
	}

	public Resource(Integer id, String name) {
		super(id, name);
	}

	public Resource(Resource copy) {
		super(copy);
	}

	public static Resource find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static Resource findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<Resource> implements
	        RowMapper<Resource> {
		public Resource newReference() {
			return new Resource();
		}
	}

}
