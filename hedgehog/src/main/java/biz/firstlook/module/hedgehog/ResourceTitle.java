package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class ResourceTitle extends Reference {

	private static final String TABLE_NAME = "ResourceTitle";

	public ResourceTitle() {
		super();
	}

	public ResourceTitle(Integer id, String name) {
		super(id, name);
	}

	public ResourceTitle(ResourceTitle copy) {
		super(copy);
	}

	public static ResourceTitle find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static ResourceTitle findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<ResourceTitle> implements
	        RowMapper<ResourceTitle> {
		public ResourceTitle newReference() {
			return new ResourceTitle();
		}
	}
}
