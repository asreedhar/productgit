package biz.firstlook.module.hedgehog;

import biz.firstlook.module.sql.RowMapper;

public class Language extends Reference {

	private static final String TABLE_NAME = "Language";

	public Language() {
		super();
	}

	public Language(Integer id, String name) {
		super(id, name);
	}

	public Language(Language copy) {
		super(copy);
	}

	public static Language find(Integer id) {
		return find(id, TABLE_NAME, new Mapper());
	}

	public static Language findOrCreate(String name) {
		return findOrCreate(name, TABLE_NAME, new Mapper());
	}

	static class Mapper extends ReferenceMapper<Language> implements
	        RowMapper<Language> {
		public Language newReference() {
			return new Language();
		}
	}
}
