package biz.firstlook.module.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class ResultSetUtils {

	public static final void setBoolean(PreparedStatement stmt, int idx,
	        Boolean val) throws SQLException {
		if (val == null)
			stmt.setNull(idx, java.sql.Types.BOOLEAN);
		else
			stmt.setBoolean(idx, val);
	}

	public static final Boolean getBoolean(ResultSet rs, String columnName)
	        throws SQLException {
		Boolean r = null;
		boolean i = rs.getBoolean(columnName);
		if (!rs.wasNull()) {
			r = Boolean.valueOf(i);
		}
		return r;
	}

	public static final void setInt(PreparedStatement stmt, int idx, Integer val)
	        throws SQLException {
		if (val == null)
			stmt.setNull(idx, java.sql.Types.INTEGER);
		else
			stmt.setInt(idx, val);
	}

	public static final Integer getInt(ResultSet rs, String columnName)
	        throws SQLException {
		Integer r = null;
		int i = rs.getInt(columnName);
		if (!rs.wasNull()) {
			r = new Integer(i);
		}
		return r;
	}

	public static final void setDate(PreparedStatement stmt, int idx, Date val)
	        throws SQLException {
		if (val == null)
			stmt.setNull(idx, java.sql.Types.TIMESTAMP);
		else
			stmt.setTimestamp(idx, new Timestamp(val.getTime()));
	}

	public static final void setString(PreparedStatement stmt, int idx, String val)
			throws SQLException {
		if (val == null)
			stmt.setNull(idx, java.sql.Types.VARCHAR);
		else
			stmt.setString(idx, val);
	}
	
}
