package biz.firstlook.module.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface PreparedStatementCreator {
	public PreparedStatement createPrepareStatement(Connection connection) throws SQLException;
}
