package biz.firstlook.module.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * <p>
 * Utility methods for SQL DataSource's.
 * </p>
 * 
 * @author swenmouth
 */
public class DataSourceUtils {

	private static final Logger LOGGER = Logger.getLogger(DataSourceUtils.class.toString());

	public static final DataSource getDataSource(String jndiName)
	        throws NamingException {
		InitialContext initialContext = new InitialContext();
		Context context = ((Context) initialContext.lookup("java:comp/env"));
		return (DataSource) context.lookup(jndiName);
	}

	public static final <T> T first(final List<T> items) throws SQLException {
		int s = items.size();
		if (s == 0) {
			return null;
		}
		else if (s == 1) {
			return items.get(0);
		}
		else {
			throw new IllegalStateException(String.format(
			        "Expected 0 rows but loaded %s rows", s));
		}
	}

	public static final <T> List<T> select(DataSource dataSource,
	        PreparedStatementCreator preparedStatementCreator,
	        RowMapper<T> rowMapper) throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<T> objects = new ArrayList<T>();
		try {
			connection = dataSource.getConnection();
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			rs = stmt.executeQuery();
			int rowNum = 0;
			while (rs.next()) {
				objects.add(rowMapper.mapRow(rs, rowNum++));
			}
			return objects;
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Select Failed", e);
			throw e;
		}
		finally {
			close(rs);
			close(stmt);
			close(connection);
		}
	}

	public static final int update(DataSource dataSource,
	        PreparedStatementCreator preparedStatementCreator)
	        throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = dataSource.getConnection();
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			return stmt.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Update Failed", e);
			throw e;
		}
		finally {
			close(stmt);
			close(connection);
		}
	}

	public static final Integer insert(DataSource dataSource,
	        PreparedStatementCreator preparedStatementCreator)
	        throws SQLException {
		Connection connection = null;
		PreparedStatement stmt = null;
		PreparedStatement read = null;
		ResultSet rs = null;
		try {
			connection = dataSource.getConnection();
			connection.setReadOnly(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
			connection.setAutoCommit(false);
			stmt = preparedStatementCreator.createPrepareStatement(connection);
			read = connection.prepareStatement("SELECT @@IDENTITY Id");
			int rowsUpdated = stmt.executeUpdate();
			Integer identity = null;
			if (rowsUpdated == 1) {
				connection.setReadOnly(true);
				rs = read.executeQuery();
				while (rs.next()) {
					identity = rs.getInt("Id");
					if (rs.wasNull()) {
						identity = null;
					}
				}
				connection.commit();
			}
			else {
				throw new SQLException(String.format(
				        "%d rows updated; expected 1", rowsUpdated));
			}
			return identity;
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Insert Failed", e);
			connection.rollback();
			throw e;
		}
		catch (RuntimeException e) {
			LOGGER.log(Level.WARNING, "Insert Failed", e);
			connection.rollback();
			throw e;
		}
		finally {
			close(rs);
			close(read);
			close(stmt);
			close(connection);
		}
	}

	public static final void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			}
			catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ResultSet Close Failed", e);
			}
		}
	}

	public static final void close(PreparedStatement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			}
			catch (SQLException e) {
				LOGGER.log(Level.WARNING, "PreparedStatement Close Failed", e);
			}
		}
	}

	public static final void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			}
			catch (SQLException e) {
				LOGGER.log(Level.WARNING, "Connection Close Failed", e);
			}
		}
	}
}
