package com.discursive.web.taglib.reference;

import java.lang.reflect.Method;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ReferenceTag extends TagSupport {

	private static final long serialVersionUID = -8962095528918324109L;

	private String beanName;
	private String method = "findAll";
	
	private String var = "values";
	private String scope = "request";
	
	@Override
	public int doEndTag() throws JspException {

		Collection values;
		
		if( StringUtils.isEmpty( beanName ) ) { 
			throw new JspException("Attribute beanName is required, and must name an accessible bean named in a spring context" );
		}
		if( StringUtils.isEmpty( method ) ) { 
			throw new JspException("Attribute method must name a method" );
		}
		if( StringUtils.isEmpty( var ) ) {
			throw new JspException("Attribute var must provides a variable name for the collection of values returned by beanName.method" );
		}
		if( StringUtils.isEmpty( scope ) || 
				!(  scope.equalsIgnoreCase( "page" ) ||
					scope.equalsIgnoreCase( "request" ) ||
					scope.equalsIgnoreCase( "session" ) ||
					scope.equalsIgnoreCase( "application" ) 
				) 
		   ) {
			throw new JspException("Attribute scope must provide a valid scope: page, request, session, application, you supplied: " + scope );
		}
		ServletContext context = pageContext.getServletContext();
		ApplicationContext applicationContext;
		try {
			applicationContext = WebApplicationContextUtils.getWebApplicationContext( context );
			if( applicationContext == null ) {
				throw new JspException( "Application Context is null" );
			}
		} catch( Exception e ) {
			throw new JspException("Exception while trying to retrieve WebApplicationContext", e );
		}
		
		try {
			Object bean = applicationContext.getBean( beanName );

			Method beanMethod = MethodUtils.getMatchingAccessibleMethod( bean.getClass(), method, (Class[]) null );
			if( beanMethod == null ) {
				throw new JspException("The bean named: " + beanName + ", of class " + bean.getClass().getName() +", has no method named: " + method );
			}
	/*		if( !(beanMethod.getReturnType().isAssignableFrom(Collection.class)) ) {
				throw new JspException("The method: " + method + ", does not return a Collection. It returns a " + beanMethod.getReturnType().getClass().getName() );
			}*/
			
			try {
				values = (Collection) beanMethod.invoke( bean );
			} catch( Exception e ) {
				throw new JspException("Serious Error invoking Method: " + beanMethod.getName() + ", on bean: " + beanName, e );
			}
			
			
		} catch( NoSuchBeanDefinitionException nsbe ) {
			throw new JspException("Application Context does not contain bean: " + beanName );
		} catch( BeansException be ) {
			throw new JspException("Error retrieving bean from context: " + beanName, be );
		}

		
		if( scope.equalsIgnoreCase("page") ) {
			pageContext.setAttribute( var, values );
		} else if( scope.equalsIgnoreCase("request") ) {
			pageContext.getRequest().setAttribute( var, values );
		} else if( scope.equalsIgnoreCase("session" ) ) {
			HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
			HttpSession session = request.getSession();
			session.setAttribute( var, values );
		} else if( scope.equalsIgnoreCase("application") ) {
			pageContext.getServletContext().setAttribute( var, values );
		}

		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {
		return TagSupport.EVAL_BODY_INCLUDE;
	}

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
	
	
	
	

}
