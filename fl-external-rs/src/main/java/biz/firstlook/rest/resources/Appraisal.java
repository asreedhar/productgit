package biz.firstlook.rest.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.springframework.stereotype.Component;

import biz.firstlook.commons.services.fault.FaultService;
import biz.firstlook.module.rest.model.AppraisalDto;
import biz.firstlook.module.rest.service.AppraisalWebServiceWrapper;
import biz.firstlook.module.rest.service.MessageException;
import biz.firstlook.services.fault.SaveRemoteResultsDto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

//TODO: Exit codes were just arbitrarily pulled out of the air... 
//TODO: Appraiser name is not supported ... amongst other things
//TODO: Have Jason / Lee take a look through this to see what could/should be done for architectural improvements
//TODO: I don't think the AppraisalWebServiceWrapper is sprung in a good way (ie should getter/setter be here!)
//TODO: Is it ok to make a service call to Fault out of here if an exception is thrown?

@Component
@Path("/appraisal")
@Produces("application/json")
public class Appraisal {

	//This wrapper class sits on top of some SOAP functionality in firstlook-external-ws
	private AppraisalWebServiceWrapper appraisalWrapper;
	
	private JsonObject response = new JsonObject();
	
	private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

	//to access the httprequest object
	@Context 
	private HttpServletRequest httpRequest;
	
	
	@GET
	@Path("/test")
	public String test(){
		return "{ \"test\": true }";		
	}// test
	
	@POST
	@Path("/mobile")
	public String createAppraisal(String jsonAppraisalRequest) {

			try {
				
				//deserialize into an appraisal object to more easily work with
				AppraisalDto appraisal = gson.fromJson(jsonAppraisalRequest,AppraisalDto.class);

				//create the appraisal
				appraisalWrapper.createAppraisal(appraisal);

			} catch (MessageException e) {
				// e.printStackTrace();
				return processException(e);
			} catch (JsonSyntaxException e) {
				// e.printStackTrace();
				return processException(e);
			} catch (Exception e) {
				// e.printStackTrace();
				return processException(e);				
			}

		response.addProperty("exitCode", "0");

		return gson.toJson(response);

	}
	
	/**
	 * @param businessUnitId
	 * @param vin
	 * @return json success or informative failure message
	 * 
	 * Description: call this method to fetch an appraisal by buid/vin combo
	 */
	@GET
	@Path("/mobile")
	public String fetchAppraisal(@QueryParam("businessUnitId") int businessUnitId, @QueryParam("vin") String vin) {

		try {
			
			//fetch the appraisal
			AppraisalDto dto = appraisalWrapper.fetchAppraisal(businessUnitId, vin);
			
			String appraisal = gson.toJson(dto);
			
			response.addProperty("appraisal", appraisal);

		} catch (MessageException e) {
			// e.printStackTrace();
			return processException(e);
		} catch (JsonSyntaxException e) {
			// e.printStackTrace();
			return processException(e);
		} catch (Exception e) {
			// e.printStackTrace();
			return processException(e);				
		}
		
		response.addProperty("exitCode", "0");

		return gson.toJson(response);

	}
	
	/**
	 * @param e
	 * @return
	 * 
	 * Description: build a json exception element, also record the fault in the FaultDb
	 *              if it fails while saving the fault, then use generic string for faulteventid
	 */
	private String processException(Exception e){
		
		JsonObject exception = new JsonObject();
		
		exception.addProperty("exceptionMessage", e.getMessage());
		
		if (e instanceof MessageException){
			
			MessageException message = (MessageException) e;
			
			exception.addProperty("appraisalObject", message.getObject());

			exception.addProperty("errorMessage", message.getErrorMessage());
		}
		
		try {
			
			FaultService service = FaultService.getInstance();
			
			SaveRemoteResultsDto result = service.saveException(e, "firstlook-external-rs-web", httpRequest.getServerName(), "JAVA", httpRequest.getRemoteUser(), httpRequest.getContextPath(), httpRequest.getRequestURL().toString(), httpRequest.getRemoteHost());
			
			Integer faultEventId = new Integer(result.getFaultEvent().getId());
			
			exception.addProperty("faultEventId", faultEventId);

		}
		catch (Exception fe) {
			
			exception.addProperty("faultEventId", "Unable to save fault");

		}
		
		response.add("exception", exception);

		response.addProperty("exitCode", "1");
		
		return gson.toJson(response);
	}
	
	//TODO: this seems wrong at this layer... what's a better approach?
	public AppraisalWebServiceWrapper getAppraisalWrapper() {
		return appraisalWrapper;
	}

	public void setAppraisalWrapper(AppraisalWebServiceWrapper appraisalWrapper) {
		this.appraisalWrapper = appraisalWrapper;
	}
}