package biz.firstlook.module.rest.model;

import java.util.Date;

/**
 * @author dreddy
 * 
 * Description - very basic appraisal bean
 *             - used to serialize to/from client
 *     
 */
public final class AppraisalDto {

	private Integer id;

	private Date appraisalCreateTime;
	
	private String appraisalType;
	
	private String appraisalSource;
	
	private VehicleDto vehicle;
	
	private PricesDto prices;
	
	private Integer businessUnitId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getAppraisalCreateTime() {
		return appraisalCreateTime;
	}

	public void setAppraisalCreateTime(Date appraisalCreateTime) {
		this.appraisalCreateTime = appraisalCreateTime;
	}

	public String getAppraisalType() {
		return appraisalType;
	}

	public void setAppraisalType(String appraisalType) {
		this.appraisalType = appraisalType;
	}

	public String getAppraisalSource() {
		return appraisalSource;
	}

	public void setAppraisalSource(String appraisalSource) {
		this.appraisalSource = appraisalSource;
	}

	public VehicleDto getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleDto vehicle) {
		this.vehicle = vehicle;
	}

	public PricesDto getPrices() {
		return prices;
	}

	public void setPrices(PricesDto prices) {
		this.prices = prices;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
}