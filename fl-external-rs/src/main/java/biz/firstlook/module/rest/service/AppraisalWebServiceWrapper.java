package biz.firstlook.module.rest.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.module.rest.model.AppraisalDto;
import biz.firstlook.module.rest.model.Mapper;
import biz.firstlook.module.rest.util.StagingBookOutUtility;
import biz.firstlook.service.appraisal.support.WebServiceException;
import biz.firstlook.service.appraisal.v1.AbstractAppraisalWebService;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

/*
 * Author:  D. Reddy
 * Purpose: This is mainly just a wrapper class around some existing functionality already provided by the AbstractAppraisalWebService
 *          class of firstlook-external-ws.  I was a bit hesitant to extend from this class as I wanted to keep the restful project
 *          a separate entity, but copying code also seemed like a bad idea.  There should probably be some refactoring done to pull
 *          out that common functionality somewhere else.
 */
public class AppraisalWebServiceWrapper extends AbstractAppraisalWebService{

	//Wrapper around protected checkRejectConditions method of the AppraisalWebServiceImpl
	public void checkRejectConditions(biz.firstlook.module.rest.model.AppraisalDto restAppraisal) throws MessageException{
		
		validate(restAppraisal);
		
		Integer businessUnitId = restAppraisal.getBusinessUnitId();
		
		String vin = restAppraisal.getVehicle().getVin();
		
		AppraisalSource appraisalSource = AppraisalSource.createAppraisalSource(restAppraisal.getAppraisalSource());
		
		try{

			super.checkRejectConditions(businessUnitId, vin, appraisalSource);

		} catch(WebServiceException e){
			
			throw new InvalidAppraisalException(e.getExitCode().value(), e.getErrorMessage());

		}
	}
	
	public void validate(biz.firstlook.module.rest.model.AppraisalDto restAppraisal) throws IncompleteAppraisalException{

		if (restAppraisal == null){
			throw new IncompleteAppraisalException("appraisal", "appraisal was not specified.");
		}
		
		if (restAppraisal.getBusinessUnitId() == 0){
			throw new IncompleteAppraisalException("appraisal", "businessUnitId was not specified.");
		}

		if (restAppraisal.getAppraisalType() == null){
			throw new IncompleteAppraisalException("appraisal", "appraisalType was not specified.");
		}
		
		if (restAppraisal.getAppraisalSource() == null){
			throw new IncompleteAppraisalException("appraisal", "appraisalSource was not specified.");
		}

		if (restAppraisal.getVehicle() == null){
			throw new IncompleteAppraisalException("appraisal", "vehicle was not specified.");
		}
		
		biz.firstlook.module.rest.model.VehicleDto vehicle = restAppraisal.getVehicle();
		
		if (vehicle.getVin() == null){
			throw new IncompleteAppraisalException("vehicle", "vin was not specified.");
		}
			
		//we can add some validation against bookouts and bookoptions as we see fit
		
	}	
	
	

	public DealerPreferenceDAO getDealerPreferenceDAO(){
		return super.dealerPreferenceDAO;
	}
	
		public AppraisalDto createAppraisal(biz.firstlook.module.rest.model.AppraisalDto restAppraisal) throws MessageException
		{		
		checkRejectConditions(restAppraisal);

		Integer businessUnitId = restAppraisal.getBusinessUnitId();
	
		String vin = restAppraisal.getVehicle().getVin();
		
		DealerPreference dealerPreference = getDealerPreferenceDAO().findByBusinessUnitId(businessUnitId);

		String color = restAppraisal.getVehicle().getColor();

		//TODO: Merging logic here
		// update deal, customer, notes, from a matching CRM appraisal if it exists

		Vehicle vehicle;
		
		List< StagingBookOut > bookOuts;
		
		try {
			
			vehicle = getVehicleService().identifyVehicle( vin );
		
			bookOuts = StagingBookOutUtility.createStagingBookOuts( restAppraisal );

		} catch (VehicleCatalogServiceException e) {
			
			throw new InvalidAppraisalException("vehicle", "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog." );
			
		} catch (InvalidVinException e) {
			
			throw new InvalidAppraisalException("vehicle", "Firstlook is unable to find the VIN: " + vin + " in the vehicle catalog." );

		} catch (WebServiceException e){
			
			throw new InvalidAppraisalException(e.getExitCode().value(), e.getErrorMessage());
		}
		
		vehicle.setBaseColor( color );
		
		Dealer dealer = getDealerService().findByDealerId( businessUnitId );
		
		Double estimateRecon = null;
		
		Integer targetGrossProfit = null;
		
		String conditionDescription = null;
		
		AppraisalValue value = null;
		
		if( restAppraisal != null )
		{
			if( restAppraisal.getPrices().getEstimatedReconditioning() != null )
				estimateRecon = restAppraisal.getPrices().getEstimatedReconditioning();
			
			if (restAppraisal.getPrices().getTargetGrossProfit() != null)
			{
				targetGrossProfit = restAppraisal.getPrices().getTargetGrossProfit();
			}
		
			//TODO: add appraiser name to json message
			value = getAppraisalFactory().createAppraisalValue( restAppraisal.getPrices().getAppraisalValue().intValue(), "firstName" + " " + "lastName" );
		}
		
		IAppraisal appraisal = getAppraisalService().createAppraisal(
				restAppraisal.getAppraisalCreateTime(),
				dealer,
				vehicle,
				restAppraisal.getVehicle().getMileage(),
				estimateRecon,
				targetGrossProfit,
				conditionDescription,
				null,
				value,
				null,
				null,
				null,
				AppraisalSource.createAppraisalSource(restAppraisal.getAppraisalSource()),
				new HashSet<StagingBookOut>(bookOuts),
				AppraisalTypeEnum.getType(restAppraisal.getAppraisalType()));
		
		//Perhaps return saved appraisal? ... after mapping to biz.firstlook.module.rest.model.Appraisal
		// ... simple call to mapper
		return  Mapper.toAppraisalDto(appraisal);
	}
	
	public biz.firstlook.module.rest.model.AppraisalDto fetchAppraisal(Integer businessUnitId, String vin) throws MessageException{
		
		if (businessUnitId == null || businessUnitId == 0){
			throw new MessageException("businessUnitId", "businessUnitId was not specified.");
		}
		
		if (vin == null){
			throw new MessageException("vin", "vin was not specified.");
		}
		
		List<IAppraisal> appraisals = getAppraisalService().findBy(businessUnitId, vin);

		if (appraisals != null && !appraisals.isEmpty()){
		
			//We are assuming one appraisal per buid/vin combination
			return Mapper.toAppraisalDto(appraisals.get(0));
		}
		
		return null;
	}



}