package biz.firstlook.module.rest.model;

import java.util.List;

/**
 * @author dreddy
 * 
 *         Description - very basic bookout bean - used to serialize to/from
 *         client
 * 
 */
public final class BookoutDto {

	private int id;

	private String bookId;

	private String styleKey;

	private List<BookOptionDto> bookOptions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getStyleKey() {
		return styleKey;
	}

	public void setStyleKey(String styleKey) {
		this.styleKey = styleKey;
	}

	public List<BookOptionDto> getBookOptions() {
		return bookOptions;
	}

	public void setBookOptions(List<BookOptionDto> bookOptions) {

		this.bookOptions = bookOptions;
	}
}