package biz.firstlook.module.rest.model;

import java.util.List;

/**
 * @author dreddy
 * 
 * Description - very basic vehicle bean
 *             - used to serialize to/from client
 *     
 */
public final class VehicleDto {
	
	private int id;
	
	private String vin;
	
	private int mileage;
	
	private String color;
	
	private List<BookoutDto> bookouts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public List<BookoutDto> getBookouts() {
		return bookouts;
	}

	public void setBookouts(List<BookoutDto> bookouts) {
		this.bookouts = bookouts;
	}
}