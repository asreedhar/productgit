package biz.firstlook.module.rest.model;

import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.AbstractAppraisal;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOutOption;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author dreddy
 *
 * Description - this will transform an appraisal at the persisted layer
 *               into an appraisaldto, to be serialized and sent back to caller
 *               
 */
public class Mapper extends AbstractAppraisal {
	
	/**
	 * @param appraisal
	 * @return AppraisalDto
	 * 
	 * Description - from appraisal business object to appraisaldto
	 */
	public static AppraisalDto toAppraisalDto(IAppraisal appraisal){
		
		if (appraisal == null){
			
			return null;
			
		} 
		
		AppraisalDto dto = new AppraisalDto();
		
		dto.setId(appraisal.getAppraisalId());
		
		dto.setAppraisalCreateTime(appraisal.getDateCreated());
		
		dto.setBusinessUnitId(appraisal.getBusinessUnitId());
		
		dto.setAppraisalType(AppraisalTypeEnum.getType(appraisal.getAppraisalTypeId()).getDescription());
		
		dto.setAppraisalSource(appraisal.getAppraisalSource().getDescription());
		
		dto.setVehicle(toVehicleDto(appraisal));
		
		dto.setPrices(toPricesDto(appraisal));
		
		return dto;
	}

	/**
	 * @param appraisal
	 * @return PricesDto
	 * 
	 * Description - from price info in appraisal business object to PricesDto
	 */
	public static PricesDto toPricesDto(IAppraisal appraisal){
		
		if (appraisal == null){

			return null;

		}
		
		PricesDto dto = new PricesDto();
		
		dto.estimatedReconditioning = appraisal.getEstimatedReconditioningCost();
		
		if (appraisal.getLatestAppraisalValue() != null){
			
			dto.appraisalValue = new Double(appraisal.getLatestAppraisalValue().getValue());

		}
		
		return dto;
	}

	/**
	 * @param appraisal
	 * @return VehicleDto
	 * 
	 * Description - from vehicle info in appraisal business object to VehicleDto
	 */
	public static VehicleDto toVehicleDto(IAppraisal appraisal){

		if (appraisal == null){
			
			return null;

		}
		
		VehicleDto dto = new VehicleDto();

		Vehicle vehicle = appraisal.getVehicle();
		
		if (vehicle != null) {

			dto.setId(vehicle.getVehicleId());

			dto.setVin(vehicle.getVin());

		}
		
		dto.setColor(appraisal.getColor());
		
		dto.setMileage(appraisal.getMileage());
		
		dto.setBookouts(toBookoutsDto(appraisal));
		
		return dto;
	}

	/**
	 * @param appraisal
	 * @return List<BookoutDto>
	 * 
	 * Description - from bookout for vehicle in appraisal business object to List<BookoutDto>
	 */
	public static List<BookoutDto> toBookoutsDto(IAppraisal appraisal){
		
		List<BookoutDto> dtos = new ArrayList<BookoutDto>();
		
		Set<BookOut> appraisalBookOuts = appraisal.getBookOuts();
		
		Set<StagingBookOut> stagingBookOuts = appraisal.getStagingBookOuts();
		
		if (appraisalBookOuts != null && !appraisalBookOuts.isEmpty()) {
			
			Iterator<BookOut> bookOutIter = appraisalBookOuts.iterator();
		    
		    while ( bookOutIter.hasNext() ){
		    	
		    	BookOut bOut = bookOutIter.next();
		    
		    	BookoutDto dto = new BookoutDto();

		    	dto.setId(bOut.getBookOutId());

		    	switch (bOut.getThirdPartyId()){
			    	case 1: dto.setBookId("BLACKBOOK"); 
			    			break;
			    	case 2: dto.setBookId("NADA"); 
			    			break;
			    	case 3: dto.setBookId("KELLEY"); 
	    					break;
			    	case 4: dto.setBookId("GALVES"); 
	    					break;
			    	default:		
				    		break;
		    	}
		    	
		    	ThirdPartyVehicle vehicle;

		    	try {

		    		vehicle = bOut.getSelectedThirdPartyVehicle();

	    			dto.setStyleKey(vehicle.getThirdPartyVehicleCode());
	    			
			    	dto.setBookOptions(toBookOptionDto(vehicle));

		    	} catch (SelectedThirdPartyVehicleNotFoundException e) {
					// swallow vehicle not found exception
				}
		    	
		    	dtos.add(dto);
		    }
		    
		}
		
		else if (stagingBookOuts != null && !stagingBookOuts.isEmpty()){

			Iterator<StagingBookOut> bookOutIter = stagingBookOuts.iterator();
			
			while ( bookOutIter.hasNext() ){
				
				StagingBookOut bOut = bookOutIter.next();
				
				BookoutDto dto = new BookoutDto();
				
				dto.setId(bOut.getStagingBookOutId());
				
				switch (bOut.getThirdPartyDataProvider().getThirdPartyId()){
					case 1: dto.setBookId("BLACKBOOK"); 
							break;
			    	case 2: dto.setBookId("NADA"); 
			    			break;
			    	case 3: dto.setBookId("KELLEY"); 
							break;
			    	case 4: dto.setBookId("GALVES"); 
							break;
			    	default:		
				    		break;
				}
				
				dto.setStyleKey(bOut.getThirdPartyVehicleCode());
				
				dto.setBookOptions(toBookOptionDto(bOut));
				
				dtos.add(dto);
				
			}
		}
		
		return dtos;
	}

	/**
	 * @param vehicle
	 * @return List<BookOptionDto>
	 * 
	 * Description - from bookoutoptions for vehicle in appraisal business object to List<BookOptionDto>
	 *             - for actual bookouts
	 */
	public static List<BookOptionDto> toBookOptionDto(ThirdPartyVehicle vehicle){

		List<BookOptionDto> dtos = new ArrayList<BookOptionDto>();
		
		List<ThirdPartyVehicleOption> options = vehicle.getThirdPartyVehicleOptions();
		
		Iterator<ThirdPartyVehicleOption> optionIter = options.iterator();
		
		while (optionIter.hasNext()){
			
			ThirdPartyVehicleOption option = optionIter.next();
			
			if (option.getStatus()){

				BookOptionDto dto = new BookOptionDto();
			
				ThirdPartyOption opt = option.getOption();
				
				dto.setDescription(option.getOptionName());
				
				dto.setKey(option.getOptionKey());
				
				dto.setOptionType(opt.getThirdPartyOptionType().getOptionTypeName());

				dto.setSelected(true);
				
				dto.setValue(option.getValue());
				
				dtos.add(dto);
			}
			
		}
		
		return dtos;
	}

	/**
	 * @param bookOut
	 * @return List<BookOptionDto>
	 * 
	 * Description - from stagingbookout in appraisal business object to List<BookOptionDto>
	 *             - for staging bookouts
	 */
	public static List<BookOptionDto> toBookOptionDto(StagingBookOut bookOut){
		
		List<BookOptionDto> dtos = new ArrayList<BookOptionDto>();

		Set<StagingBookOutOption> options = bookOut.getOptions();
		
		Iterator<StagingBookOutOption> optionIter = options.iterator();
		
		while (optionIter.hasNext()){
			
			StagingBookOutOption option = optionIter.next();
			
			if (option.getIsSelected()){
				
				BookOptionDto dto = new BookOptionDto();
				
				dto.setDescription(option.getOptionName());
				
				dto.setKey(option.getOptionKey());
				
				dto.setOptionType(option.getThirdPartyOptionType().getOptionTypeName());

				dto.setSelected(true);
				
				dto.setValue(option.getValue());
				
				dtos.add(dto);
			}
			
		}
		
		return dtos;
	}
}