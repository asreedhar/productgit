package biz.firstlook.module.rest.service;

public class IncompleteAppraisalException extends MessageException {

	private static final long serialVersionUID = -3617349016601238846L;

	public IncompleteAppraisalException(String appraisalObject, String errorMessage) {
		super(appraisalObject, errorMessage);
	}
	
}
