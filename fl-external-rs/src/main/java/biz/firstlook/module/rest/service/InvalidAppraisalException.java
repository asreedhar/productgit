package biz.firstlook.module.rest.service;

public class InvalidAppraisalException extends MessageException {

	private static final long serialVersionUID = -4845157056571193432L;

	public InvalidAppraisalException(String appraisalObject, String errorMessage) {
		super(appraisalObject, errorMessage);
	}
	
}
