from time import clock

class StopWatch:

    def __init__(self):
        self.startTime = 0
        self.endTime = 0
    
    def start( self ):
        self.startTime = clock()
   
    def stop( self ):
        self.endTime = clock()
    
    def getTime( self ):
        return self.endTime - self.startTime

    def getTimeInMinutes( self ):
        return (self.endTime - self.startTime) / 60
