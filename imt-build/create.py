import sys, os, string, shutil

from getpass import getpass
from os.path import join, getsize
from sys import stdout

from misc.StopWatch import StopWatch

from create.createIMT import CreateIMT
from create.createStaticIMT import CreateStaticIMT
from create.createProcessors import CreateProcessors
from create.createKelley import CreateKelley
from create.createAET import CreateAET
from create.createCIA import CreateCIA
from create.createPack import CreatePack
from create.createMarketService import CreateMarketService

from database.ScriptIMT import ScriptIMT
from database.ScriptIE import ScriptIE

from create.CVSUtil import CVSUtil
from create.createUtil import CreateUtil

class Create:

    def __init__(self, tag):
        self._success = 0
        createMap = {}
        createMap["1"] = CreateIMT( tag )
        createMap["2"] = CreateKelley( tag )
        createMap["3"] = CreateAET( tag )
        createMap["4"] = CreateProcessors( tag )
        createMap["5"] = CreateCIA( tag )
        createMap["6"] = CreateStaticIMT( tag )
        createMap["7"] = CreatePack( tag )
        createMap["8"] = CreateMarketService( tag )
        self._createMap = createMap

        scriptMap = {}
        scriptMap["IMT"] = ScriptIMT( tag )
        scriptMap["IE"] = ScriptIE( tag )
        self._scriptMap = scriptMap

        self._util = CreateUtil()
        self._cvsUtil = CVSUtil()


    def printProducts(self):
        print "Product:\n"
        keys = self._createMap.keys()
        keys.sort()
        for key in keys:
            print "  %s %s" % ( key, self._createMap[key].name() )
        print ""

    def selectProduct(self):
        self.printProducts()
        product = raw_input( "Select Product(s): " )
        return product

    def executeProduct(self, product, tag):
        self._success = 1
        util = self._util
        
        products = product.split( " " )

        stopWatch = StopWatch()
        util.setUpEnvironment()
    
        stopWatch.start()
        print "Creating Output Directory"
        util.createOutputDir()

        print "Creating Work Directory"
        util.createWorkDir()
        os.chdir( "work" )

        ''' Query '''
        for product in products:
            createObject = self._createMap[product]
            print "\n*********" + createObject.name() + " OPTIONS **********\n"
            createObject.query()

        sourcesWeHave = {}
        databasesWeHaveBuilt = {}

        print "\n\nCHECKING OUT SOURCES, TAG: " + tag + "\n"
        print "\n\nCHECKING OUT Configuration: "
        os.popen( self._cvsUtil.createCvsCheckOutCommand( "imt", tag, "configuration" ) )
        for product in products:
            createObject = self._createMap[product]
            source = createObject.source()
            if( sourcesWeHave.has_key( source ) ):
                print "Skipping " + createObject.name() + ", we've already got it!"
            else:
                print ( " * Checking out " + createObject.name() )
                os.popen( self._cvsUtil.createCvsCheckOutCommand( source[0], tag, source[1]) )
                sourcesWeHave[source] = ""

        print "\n\nBUILDING PRODUCTS: " + tag + "\n"
        for product in products:
            currentDir = os.getcwd()
            createObject = self._createMap[product]
            print "Building " + createObject.name()
            createObject.execute()
            print "Finished Building " + createObject.name()
            os.chdir( currentDir )

        
        print "\n\nBUILDING DATABASE ZIPs, TAG: " + tag + "\n"
        for product in products:
            createObject = self._createMap[product]
            databases = createObject.databases()
            for database in databases:
                if( databasesWeHaveBuilt.has_key( database ) ):
                    print "Skipping " + createObject.name() + ", we've already built the database zip."
                else:
                    scriptObject = self._scriptMap[database]
                    source = scriptObject.source()
                    if( sourcesWeHave.has_key( source ) ):
                        print "Skipping " + scriptObject.name() + ", we've already got it!"
                    else:
                        print ( " * Checking out " + scriptObject.name() )
                        os.popen( self._cvsUtil.createCvsCheckOutCommand( source[0], tag, source[1]) )
                        sourcesWeHave[source] = ""
                    
                    scriptObject.execute()
                    databasesWeHaveBuilt[database] = ""

        


        os.chdir( ".." ) 
            

        '''        keys = self._createMap.keys()

        if( product in keys ):
            product = self._createMap[product].execute()
        else:
            print "Selection %s is not valid" % (product)
            self._success = 0
        print ""
        print ""
        print "OUTPUT IS AVAILABLE IN THE OUTPUT DIRECTORY"
        print "" '''
        return self._success

    def execute(self):
        executed = 0
        while( executed == 0 ):
            product = self.selectProduct()
            stopWatch = StopWatch()
            stopWatch.start()
            try:
                executed = self.executeProduct( product, tag )
                stopWatch.stop()
                print( "Total Build Time: %d" % stopWatch.getTime() )            
            except IOError, text:
                print text
                executed = -1


if __name__ == '__main__':
    executed = 0

    if( len( sys.argv ) >= 2 ):
        tag = sys.argv[1]
    else:
        print "ERROR: Tag name must be supplied. (i.e. create.py Production_03_17)"
        sys.exit(1)

    print "Running create with tag: " + tag
    create = Create( tag );
    create.execute()


