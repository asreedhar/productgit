"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

from create.CVSUtil import CVSUtil
from createUtil import CreateUtil
from buildStamp import BuildStamp
from misc.StopWatch import StopWatch

class CreateMarketService:

    mapping = { 1:"dev", 2:"auto", 3:"accept", 4:"prod", 5:"local", 6:"regression" }
    dictionary = { "dev":"dev.dictionary", "auto":"auto.dictionary", "accept":"test.dictionary", "prod":"production.dictionary", "local":"localhost.dictionary", "regression":"regression.dictionary" }
    expertDictionary = { "dev":"dev.dictionary", "auto":"test.dictionary", "accept":"test.dictionary", "prod":"production.dictionary", "local":"dev.dictionary", "regression":"regression.dictionary" }

    def __init__(self, tag):
        self._util = CreateUtil()
        self._tag = tag
        self._warName = ""

    def name(self):
        return "Market-Service"

    def query(self):
        self._databasePassword = self._util.retrievePassword( "market database" )
        self._warName = self._util.getTextWithDefault( "Desired Name of WAR", "market-service.war" )

    def source(self):
        return ("imt", "market-service")

    def databases(self):
        return ()

    def createAntCommand(self):
        antOptions = {}
        return self._util.createAntCommand( "build.xml", antOptions, "war" )

    def execute(self):
        util = self._util
        tag= self._tag
        warName = self._warName
        stopWatch = StopWatch()
        stopWatch.start()

        cvsUtil = CVSUtil()
        command = cvsUtil.createCvsExportCommand( "imt", tag, "configuration" )
        os.popen( command )
        rootPath = os.getcwd()
        marketPath = rootPath + "\\market-service"
        confPath = marketPath + "\\conf-stage"
        os.mkdir( confPath )
        os.chdir( "configuration" )
        configCommand = os.getcwd() + "\\scripts\\buildConfigFiles.py %s %s %s %s" % ( os.getcwd() + "\\dictionaries\\dev.dictionary", rootPath + "\\market-service\\resources", confPath, "marketdbpassword=" + self._databasePassword )
        os.popen( configCommand )
        
        os.chdir( marketPath )

        buildStamp = BuildStamp( "buildStamp.txt", "Market-Service", tag, "version.txt" )
        buildStamp.generate()
        
        shutil.copy( "buildStamp.txt", "docBase\\" ) 
        
        marketAntCommand = self.createAntCommand()
        
        print "ANT Command: " + marketAntCommand
        
        os.system( marketAntCommand )

        shutil.copy( "dist\\Market-Service.war", "..\\..\\output\\" + warName ) 
        
        stopWatch.stop()
        print( "Market-Service Build Time: %d" % stopWatch.getTime() )
        
if __name__ == '__main__':
    CreateMarketService().execute()
