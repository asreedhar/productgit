"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from zipfile import ZipFile
from glob import glob

from createUtil import CreateUtil
from buildStamp import BuildStamp
from createDependency import CreateDependency
from dependencyUtil import DependencyUtil
from misc.StopWatch import StopWatch

class CreateAET:

    dictionary = { "dev":"dev.dictionary", "test":"test.dictionary", "regression":"regression.dictionary", "colo":"colo.dictionary", "production":"production.dictionary" }

    def __init__(self, tag):
        self._util = CreateUtil();
        self._tag = tag
        self._installDir = ""
        self._dataDir = ""
        self._databaseServer = ""
        self._databaseName = ""
        self._databaseUser = ""
        self._databasePassword = ""

    def name(self):
        return "Expert System"

    def query(self):
        self._installDrive = self._util.getTextWithDefault( "Installation Drive", "c:" )
        self._installDir = self._util.getTextWithDefault( "Installation Directory", "c:\\apps\\rule-engine" )
        self._dataDir = self._util.getTextWithDefault( "Data Directory", "c:\\apps\\data" )
        self._logLevel = self._util.getTextWithDefault( "Log Level {debug, info, warn, error}", "warn" )
        self._databaseServer = self._util.getTextWithDefault( "Database Server", "PLUTODEV" )
        self._databaseName = self._util.getTextWithDefault( "Database Name", "IE" )
        self._databaseUser = self._util.getTextWithDefault( "Database User", "firstlook" )
        self._databasePassword = self._util.retrievePassword( "Database Password" )

    def source(self):
        return ("aet", "prototype")
        
    def databases(self):
        return ('IE',)

    def createAntCommand(self, configParams):
        antOptions = {}
        antOptions["configParams"] = configParams
        return self._util.createAntCommand( "build.xml", antOptions, "jar.build" )

    def execute(self):
        installDrive = self._installDrive
        installDir = self._installDir
        dataDir = self._dataDir
        logLevel = self._logLevel
        databaseServer = self._databaseServer
        databaseName = self._databaseName
        databaseUser = self._databaseUser
        databasePassword = self._databasePassword

        stopWatch = StopWatch()
        util = self._util;

        stopWatch.start()
        installDir = installDir.replace( "\\", "\\\\")
        dataDir = dataDir.replace( "\\", "\\\\")

        configParams="installDrive=%s installDir=%s dataDir=%s loglevel=%s dbhostname=%s database=%s dbnumber= dbuser=%s dbpassword=%s" % ( installDrive, installDir, dataDir, logLevel, databaseServer, databaseName, databaseUser, databasePassword )

        dependencyUtil = DependencyUtil()
        dependencyUtil.dependencies( self._util, os.path.join( os.getcwd(), "expert-dependencies" ), os.path.join( os.getcwd(), "prototype\\lib" ), self._tag, os.path.join( os.getcwd(), "..\\dependencies\\aet.dependencies" ) )

        os.chdir( "prototype" )                
        aetAntCommand = self.createAntCommand( configParams )

        print "ANT Command: " + aetAntCommand

        os.system( aetAntCommand )

        buildStamp = BuildStamp( "buildStamp.txt", self.name(), self._tag, "version.txt" )
        buildStamp.generate()

        ruleXmlFileName = "rule-xml.zip"

        zipFileName = "expert-system.zip"

        try:
            os.remove( zipFileName )
            os.remove( ruleXmlFileName )
        except OSError:
            print ""

        zip = ZipFile( zipFileName, "w" )
        ruleXmlZip = ZipFile( ruleXmlFileName, "w" )

        zip.write( "buildStamp.txt", "buildStamp.txt" )
        zip.write( "dist/aet-1.0.jar", "aet.jar" )
        zip.write( "target/classes/aet.hibernate.cfg.xml", "conf/aet.hibernate.cfg.xml" )
        zip.write( "target/classes/expert.properties", "conf/expert.properties" )
        zip.write( "target/classes/log4j.properties", "conf/log4j.properties" )
        zip.write( "target/classes/engine.bat", "engine.bat" )
        zip.write( "target/classes/ruleBuilder.bat", "ruleBuilder.bat" )
        zip.write( "target/classes/constantEdit.bat", "constantEdit.bat" )
        zip.write( "target/classes/_cp.bat", "_cp.bat" )

        os.mkdir( "logs" )
        open( "logs/.blank", "w" )
        zip.write( "logs/.blank" )

        libs = glob( "lib/*.jar" )
        for lib in libs:
            if( lib != 'CVS' ):
                zip.write( lib )

        dtds = glob( "data/*.dtd" )
        for dtd in dtds:
            if( dtd != 'CVS' ):
                zip.write( dtd )

        zip.close()

        ruleXmlZip.write( "buildStamp.txt", "buildStamp.txt" )
        ruleXmlZip.write( "data/derivations.xml", "derivations.xml" )
        ruleXmlZip.write( "data/inferences.xml", "inferences.xml" )
        ruleXmlZip.write( "data/priorities.xml", "priorities.xml" )
        ruleXmlZip.write( "data/variables.xml", "variables.xml" )

        ruleXmlZip.write( "data/constants/Constant.data", "constants/Constant.data" )
        ruleXmlZip.write( "data/constants/ConstantValue.data", "constants/ConstantValue.data" )
        ruleXmlZip.write( "data/constants/constantTables.txt", "constants/constantTables.txt" )
        ruleXmlZip.write( "data/constants/ReadConstants.bat", "constants/ReadConstants.bat" )
        ruleXmlZip.write( "data/constants/WriteConstants.bat", "constants/WriteConstants.bat" )

        ruleXmlZip.close()

        shutil.copy( zipFileName, "../../" + "output/" + zipFileName )
        shutil.copy( ruleXmlFileName, "../../" + "output/" + ruleXmlFileName )

        print( "Output available in directory, output/" )

        stopWatch.stop()
        print( "AET Build Time: %d" % stopWatch.getTime() )
