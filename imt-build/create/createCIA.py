"""This file is used to run the whole build process.
    WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from zipfile import ZipFile
from glob import glob

from createUtil import CreateUtil
from buildStamp import BuildStamp
from misc.StopWatch import StopWatch
from dependencyUtil import DependencyUtil
from configure import Configure

class CreateCIA:

    dictionary = { "dev":"dev.dictionary", "test":"test.dictionary", "regression":"regression.dictionary", "colo":"colo.dictionary", "production":"production.dictionary" }

    def __init__(self, tag):
        self._util = CreateUtil();
        self._tag = tag
        self._installDir = ""
        self._databaseServer = ""
        self._databaseName = ""
        self._databaseUser = ""
        self._databasePassword = ""
        self._configure = Configure()
        
    def name(self):
        return "CIA Engine"

    def displayDictionaries(self):
        self._dictionaryMap = {}
        dicts = self._configure.listDictionaries(self._tag)
        i = 1
        print "Please select a dictionary"
        for dict in dicts:
            self._dictionaryMap[str(i)] = dict
            print "\t" + str(i) + " - " + self._dictionaryMap[str(i)]
            i = i+1
        
    def query(self):
        self._installDir = self._util.getTextWithDefault( "Installation Directory", "c:\\apps\\cia-engine" )
        self._databasePassword = self._util.retrievePassword( "IMT Database Password" )
        self._iedbPassword = self._util.retrievePassword( "IE Database Password" )
        self.displayDictionaries()
        self._dictionaryNum = self._util.getTextWithDefault( "Dictionary", "1" )
        
    def source(self):
        return ("imt", "cia-engine")
        
    def databases(self):
        return ('IE',)

    def createAntCommand(self, configParams):
        antOptions = {}
        antOptions["configParams"] = configParams
        return self._util.createAntCommand( "build.xml", antOptions, "jar" )
    
    def writeToZip(self, zipFile, fileNames, dir=""):
        for fileName in fileNames:
            zipFile.write(os.path.join(dir, fileName), fileName)    
    
    def execute(self):
        installDir = self._installDir
    
        stopWatch = StopWatch()
        util = self._util;

        stopWatch.start()
        installDir = installDir.replace( "\\", "\\\\")
    
        workDir = os.getcwd()
        os.chdir("..")
        baseDir = os.getcwd()
        os.chdir(workDir)
        dependencyUtil = DependencyUtil()
        dependencyUtil.dependencies( self._util, os.path.join( workDir, "cia-dependencies" ), os.path.join( workDir, "cia-engine\\lib" ), self._tag, os.path.join( baseDir, "dependencies\\cia.dependencies" ) )
        
        configParams={"installDir":installDir, "dbpassword":self._databasePassword, "iedbpassword":self._iedbPassword}
        self._configure.configure( "cia-engine", self._tag, configParams, self._dictionaryMap[self._dictionaryNum] )

        os.chdir( "cia-engine" )                       
        aetAntCommand = self.createAntCommand( "" )
                
        print "ANT Command: " + aetAntCommand
                
        os.system( aetAntCommand )

        buildStamp = BuildStamp( "buildStamp.txt", "CIA Engine", self._tag, "version.txt", self._dictionaryMap[self._dictionaryNum] )
        buildStamp.generate()

        zipFileName = "cia-engine.zip"     
        confFiles = glob("conf\*.properties")
        xmlFiles = glob("conf\*.xml")
        os.chdir("conf")
        batFiles = glob("*.bat")
        os.chdir("../")
        zip = ZipFile(zipFileName, "w")
        zip.write("buildStamp.txt", "buildStamp.txt")
        zip.write("dist/cia-engine-1.0.jar", "cia-engine.jar")
        self.writeToZip(zip, confFiles)
        self.writeToZip(zip, xmlFiles)
        self.writeToZip(zip, batFiles, "conf")
        os.mkdir("logs")
        open( "logs/.blank", "w" )
        zip.write( "logs/.blank" )
        libs = glob( "lib/*.jar" )
        for lib in libs:
            if( lib != 'CVS' ):
                zip.write( lib )
        zip.close()

        curDir = os.getcwd()
        os.chdir("../")
        os.chdir("../")
        shutil.copy( os.path.join(curDir, zipFileName), os.getcwd() + "\\output\\" + zipFileName )
        os.chdir(curDir)
        print( "Output available in directory, output/" )

        stopWatch.stop()
        print( "CIA Build Time: %d" % stopWatch.getTime() )

            

