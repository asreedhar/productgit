"""This file is used to run the whole build process.
    WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil

from createUtil import CreateUtil
from CVSUtil import CVSUtil
from buildStamp import BuildStamp
from misc.StopWatch import StopWatch

class CreateDependency:

    def __init__(self, tag, dependency, cvsModule, outputFile, destinationDir):
        self._util = CreateUtil()
        self._cvsUtil = CVSUtil()
        self._tag = tag
        self._project = dependency
        self._module = cvsModule
        self._outputFile = outputFile
        self._destinationDir = destinationDir

    def name(self):
        return self._project

    def source(self):
        return (self._module, self._project)

    def createAntCommand(self, configParams):
        antOptions = {}
        antOptions["configParams"] = configParams
        return self._util.createAntCommand( "build.xml", antOptions, "jar.build" )

    def execute(self):
        tag = self._tag

        stopWatch = StopWatch()
        stopWatch.start()

        os.popen( self._cvsUtil.createCvsExportCommand( self._module, self._tag, self._project ) )
        print "checked out project"     

        os.chdir( self._project )

        print "building to %s" % self._destinationDir
        aetAntCommand = self._util.createAntCommand( "build.xml", {"build.home":".", "dist.home":self._destinationDir, "dependency.artifact.dir":self._destinationDir}, "build" )
                
        print "ANT Command: " + aetAntCommand
                
        os.system( aetAntCommand )

        buildStamp = BuildStamp( "buildStamp.txt", self._project, tag, os.getcwd() + "\\version.txt" )
        buildStamp.generate()

        stopWatch.stop()
        print( "%s Build Time: %d" % (self._project, stopWatch.getTime() ) )
        os.chdir( ".." )

if __name__ == '__main__':
    dependency = CreateDependency("HEAD", "ie-calculate", "aet", "dist/ie-calculate-1.0.jar", os.getcwd())
    dependency.execute()


