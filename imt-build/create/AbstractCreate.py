class AbstractCreate:

    def name(self):
        assert 0, 'name is not defined in AbstractCreate'

    def query(self):
        assert 0, 'query is not defined in AbstractCreate'

     def checkout(self):
        assert 0, 'checkout is not defined in AbstractCreate'

    def execute(self):
        assert 0, 'execute is not defined in AbstractCreate'

    def alter(self):
        assert 0, 'alter is not defined in AbstractCreate'
