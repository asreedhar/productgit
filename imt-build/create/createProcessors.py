"""This file is used to run the whole build process.
    WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

from createUtil import CreateUtil
from misc.StopWatch import StopWatch
from buildStamp import BuildStamp
from dependencyUtil import DependencyUtil


class CreateProcessors:

    def __init__(self, tag):
            self._util = CreateUtil();
            self._tag = tag

    def name(self):
            return "Processors"

    def query(self):
            return
            
    def source(self):
            return ("imt", "IMT")

    def databases(self):
            return ("IMT",)
    
    def createProcessorsAntCommand( self ):
            antOptions = {}
            path = os.getcwd();
            antOptions["projectDir"] = os.getcwd()
            os.chdir( "../configuration" )
            print "configDir = " + os.getcwd()
            antOptions["configDir"] = os.getcwd()
            os.chdir( path )
            return self._util.createAntCommand( "build//build.xml", antOptions, "processors" )
         
    def execute(self):
            tag = self._tag
            util = self._util

            stopWatch = StopWatch()
            stopWatch.start()

            dependencyUtil = DependencyUtil()
            dependencyUtil.dependencies( self._util, os.path.join( os.getcwd(), "imt-dependencies" ), os.path.join( os.getcwd(), "imt\\docBase\\WEB-INF\\lib" ), self._tag, os.path.join( os.getcwd(), "..\\dependencies\\imt.dependencies" ) )

            os.chdir( "IMT" )
            
            buildStamp = BuildStamp( "buildStamp.txt", "PROCESSORS", tag, "version.txt", "" )
            buildStamp.generate()
    
            shutil.copy( "buildStamp.txt", "docBase\\WEB-INF\\src" ) 

            antCommand = self.createProcessorsAntCommand( )   
            os.system( antCommand )
            shutil.copy( "build/firstlookapplicationProcessors.zip", "..//..//output//firstlookapplicationProcessors.zip" )
                    
            os.chdir( ".." )
            stopWatch.stop()
            print( "IMT Build Time: %d" % stopWatch.getTime() )
