import os
from CVSUtil import CVSUtil
from glob import glob

class Configure:
    def __init__(self):
            self._isExported=0;
            
    def createConfigureCommand(self, rootPath, destPath, projectPath, params, dictionary ):

        configCommand = os.getcwd() + "\\scripts\\buildConfigFiles.py %s %s %s " % ( rootPath + "\\configuration\\dictionaries\\" + dictionary, projectPath + "\\resources", destPath )
        options = ""
        optionKeys = params.keys()
        for key in optionKeys:
            value = params[key]
            option = "%s=%s " % ( key, value )
            options = options + option

        config = configCommand + " " + options
        return config

    def exportConfiguration(self,tag):
         if not self._isExported == 1:
            cvsUtil = CVSUtil()
            command = cvsUtil.createCvsExportCommand( "imt", tag, "configuration" )
            os.popen( command )
            self._isExported = 1
        
    def configure(self, project, tag, configParams, dictionary ):
        startDir = os.getcwd()
        self.exportConfiguration(tag)
        rootPath = os.getcwd()
        projectPath = rootPath + "\\" + project
        destPath = projectPath + "\\conf"
        os.chdir( "configuration" )
        configCommand = self.createConfigureCommand( rootPath, destPath, projectPath, configParams, dictionary )
        os.popen( configCommand )
        os.chdir( startDir )

    def listDictionaries(self,tag):
        startDir = os.getcwd()
        self.exportConfiguration(tag)
        os.chdir("configuration\\dictionaries")
        dictionaries = glob("*.dictionary")
        os.chdir(startDir)
        return dictionaries

