"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

from createUtil import CreateUtil
from buildStamp import BuildStamp
from misc.StopWatch import StopWatch

class CreateKelley:

    mapping = { 1:"dev", 2:"auto", 3:"accept", 4:"prod", 5:"local", 6:"regression" }
    dictionary = { "dev":"dev.dictionary", "auto":"auto.dictionary", "accept":"test.dictionary", "prod":"production.dictionary", "local":"localhost.dictionary", "regression":"regression.dictionary" }
    expertDictionary = { "dev":"dev.dictionary", "auto":"test.dictionary", "accept":"test.dictionary", "prod":"production.dictionary", "local":"dev.dictionary", "regression":"regression.dictionary" }

    def __init__(self, tag):
        self._util = CreateUtil()
        self._tag = tag
        self._warName = ""
        self._kelleyDll = ""
        self._kelleyVersion = ""
        self._kelleyPath = ""
        self._kelleyPassword = ""

    def name(self):
        return "Kelley Web Service"

    def query(self):
        self._warName = self._util.getTextWithDefault( "Desired Name of WAR", "kbb.war" )
        self._kelleyDll = self._util.getTextWithDefault( "Location of Kelley DLL", "d:\\kbb\\KelleyDLLWrapper.dll" )
        self._kelleyVersion = self._util.getTextWithDefault( "Kelley Version (number in both DLL and Database)", "04010" )
        self._kelleyPath = self._util.getTextWithDefault( "Kelley Path (No Trailing Backslash!)", "d:\\kbb" )
        self._kelleyPassword = self._util.getTextWithDefault( "Kelley Unique Access ID", "]x94*D%fd;L")

    def source(self):
        return ("imt", "KelleyService")

    def databases(self):
        return ()

    def createAntCommand(self, kelleyDll, kelleyVersion, kelleyPath, kelleyPassword):
        antOptions = {}
        antOptions["kelley.dll"] = kelleyDll
        antOptions["kelley.version"] = kelleyVersion
        antOptions["kelley.path"] = kelleyPath
        antOptions["kelley.password"] = kelleyPassword

        return self._util.createAntCommand( "build.xml", antOptions, "war" )
	
    def execute(self):
        util = self._util
        tag= self._tag
        warName = self._warName
        kelleyDll = self._kelleyDll
        kelleyVersion = self._kelleyVersion
        kelleyPath = self._kelleyPath
        kelleyPassword = self._kelleyPassword

        stopWatch = StopWatch()
        stopWatch.start()
        kelleyDll = kelleyDll.replace( "\\", "\\\\")
        kelleyVersion = kelleyVersion.replace( "\\", "\\\\" )
        kelleyPath = kelleyPath.replace( "\\", "\\\\" )
        kelleyPassword = kelleyPassword.replace( "\\", "\\\\" )

        print "Supplied Parameters: " + warName + ", " + kelleyDll + ", " + kelleyVersion + ", " + kelleyPath + ", " + tag

        os.chdir( "KelleyService" )

        buildStamp = BuildStamp( "buildStamp.txt", "Kelley Service", tag, "version.txt" )
        buildStamp.generate()
        
        shutil.copy( "buildStamp.txt", "docBase\\" ) 
        
        kelleyAntCommand = self.createAntCommand( kelleyDll, kelleyVersion, kelleyPath, kelleyPassword )
        
        print "ANT Command: " + kelleyAntCommand
        
        os.system( kelleyAntCommand )

        shutil.copy( "dist\\KelleyService.war", "..\\..\\output\\" + warName ) 
        
        stopWatch.stop()
        print( "Kelley Build Time: %d" % stopWatch.getTime() )

if __name__ == '__main__':
	kelley = CreateKelley()
	kelley.execute()
