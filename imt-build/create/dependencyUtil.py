import sys, os, string, shutil

from createUtil import CreateUtil
from createDependency import CreateDependency
from glob import glob

class DependencyUtil:

    def createDependencies(self, dependencyFile):
        dependencies = []
        file = open(dependencyFile)
        lines = file.readlines()
        file.close()
        for line in lines:
            dependencies.append( line.split( ":" ) )
        return dependencies

    def dependencies(self, util, dependencyDir, libDir, tag, dependencyFile):
        startDir = os.getcwd()
        print "*******" + startDir + "**********"
        dependencyDir = os.path.join( startDir, dependencyDir ) 
        util.createDir( dependencyDir )
        dependencies = self.createDependencies(dependencyFile)
        for depend in dependencies:
            dependency = CreateDependency(tag, depend[0].strip(), depend[1].strip(), depend[2].strip(), dependencyDir)
            dependency.execute()
        jars = glob( os.path.join( dependencyDir , "*.jar" ) )
        for jar in jars:
            shutil.copy( jar, libDir )
        os.chdir(startDir)