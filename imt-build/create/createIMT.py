"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

from createUtil import CreateUtil
from misc.StopWatch import StopWatch
from buildStamp import BuildStamp
from dependencyUtil import DependencyUtil


class CreateIMT:

        mapping = { 1:"dev", 2:"dataqa", 3:"prod", 4:"regression", 5:"demo", 6:"uat", 7:"regression2" }
        dictionary = { "dev":"dev.dictionary", "dataqa":"dataqa.dictionary", "prod":"production.dictionary", "regression":"regression.dictionary", "demo":"demo.dictionary", "uat":"uat.dictionary", "regression2":"regression2.dictionary" }

        def __init__(self, tag):
            self._util = CreateUtil();
            self._tag = tag
            self._buildType = ""   
            self._databasePassword = ""
            self._ieDatabasePassword = ""
            self._nadaPassword = ""
            self._war = ""

        def name(self):
            return "IMT Web Application"

        def query(self):
            self.printDictionaries()
            
            self._buildType = input( "Enter the build type number: " )   
            self._databasePassword = self._util.retrievePassword( "database" )
            self._ieDatabasePassword = self._util.retrievePassword( "IE database" )
            self._nadaPassword = self._util.retrievePassword( "nada" )
            self._war = raw_input( "Enter the name of the war file excluding '.war': " )

	def source(self):
            return ("imt", "IMT")

        def databases(self):
            return ("IE", "IMT")

	
        def createAntCommand( self, dictionaryName, databasePassword, ieDatabasePassword, nadaPassword, warName ):
            antOptions = {}
            antOptions["compile.dictionary"] = dictionaryName
            antOptions["dbPassword"] = databasePassword
            antOptions["ieDbPassword"] = ieDatabasePassword
            antOptions["nadaPassword"] = nadaPassword
            antOptions["warFilename"] = warName
            path = os.getcwd();
            antOptions["projectDir"] = os.getcwd()
            os.chdir( "../configuration" )
            antOptions["configDir"] = os.getcwd()
            os.chdir( path )
            return self._util.createAntCommand( "build//build.xml", antOptions, "release" )                
                
        def printDictionaries(self):
            print ""
            print "  1  Development"
            print "  2  DataQA"
            print "  3  Production"
            print "  4  Regression"
            print "  5  Demo"
            print "  6  UAT"
            print "  7  Regression 2"
            print ""
		
        def execute(self):
            tag = self._tag
            util = self._util
            buildType = self._buildType   
            databasePassword = self._databasePassword
            ieDatabasePassword = self._ieDatabasePassword
            nadaPassword = self._nadaPassword
            war = self._war

            stopWatch = StopWatch()
            stopWatch.start()


            dependencyUtil = DependencyUtil()
            dependencyUtil.dependencies( self._util, os.path.join( os.getcwd(), "imt-dependencies" ), os.path.join( os.getcwd(), "imt\\docBase\\WEB-INF\\lib" ), self._tag, os.path.join( os.getcwd(), "..\\dependencies\\imt.dependencies" ) )


            os.chdir( "IMT" )
            
            buildStamp = BuildStamp( "buildStamp.txt", "IMT", tag, "version.txt", self.mapping[buildType] )
            buildStamp.generate()
    
            shutil.copy( "buildStamp.txt", "docBase\\" ) 

            antCommand = self.createAntCommand( self.dictionary[self.mapping[buildType]], databasePassword, ieDatabasePassword, nadaPassword, war )   
            os.system( antCommand )
            war = war + ".war"
            shutil.copy( war, "..//..//output//" + war )
  
            stopWatch.stop()
            print( "IMT Build Time: %d" % stopWatch.getTime() )
