"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

from zipfile import ZipFile
from createUtil import CreateUtil
from misc.StopWatch import StopWatch
from buildStamp import BuildStamp
from glob import glob


class CreateStaticIMT:

        def __init__(self, tag):
            self._util = CreateUtil();
            self._tag = tag
            self._buildType = ""   

        def name(self):
            return "IMT Web App - Static Content"

        def query(self):
            return

	def source(self):
            return ("imt", "IMT/docBase/marketing")

        def databases(self):
            return ()
		
        def execute(self):
            tag = self._tag
            util = self._util

            stopWatch = StopWatch()
            stopWatch.start()

            os.chdir( "IMT/docBase/marketing" )
            
            zip = ZipFile( "imt-static.zip", "w" )

            files = glob( "*.jsp" )
            for file in files:
                zip.write( file, "marketing/" + file )

            zip.close()

            shutil.copy( "imt-static.zip", "../../../../output/imt-static.zip" )
  
            stopWatch.stop()
            print( "IMT Static Build Time: %d" % stopWatch.getTime() )
