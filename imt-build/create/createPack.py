"""This file is used to run the whole build process.
    WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from zipfile import ZipFile
from glob import glob

from createUtil import CreateUtil
from buildStamp import BuildStamp
from misc.StopWatch import StopWatch
from configure import Configure

class CreatePack:

    dictionary = { "dev":"dev.dictionary", "test":"test.dictionary", "regression":"regression.dictionary", "colo":"colo.dictionary", "production":"production.dictionary" }

    def __init__(self, tag):
        self._util = CreateUtil();
        self._tag = tag
        self._installDir = ""
        self._databaseServer = ""
        self._databaseName = ""
        self._databaseUser = ""
        self._databasePassword = ""
        self._configure = Configure()
        
    def name(self):
        return "Pack Adjuster"

    def displayDictionaries(self):
        self._dictionaryMap = {}
        dicts = self._configure.listDictionaries(self._tag)
        i = 1
        print "Please select a dictionary"
        for dict in dicts:
            self._dictionaryMap[str(i)] = dict
            print "\t" + str(i) + " - " + self._dictionaryMap[str(i)]
            i = i+1
      
    def query(self):
        self._installDir = self._util.getTextWithDefault( "Installation Directory", "c:\\apps\\pack-adjust" )
        self._databaseServer = self._util.getTextWithDefault( "Database Server", "PLUTODEV" )
        self._databaseName = self._util.getTextWithDefault( "Database Name", "DBASTAT" )
        self._databaseUser = self._util.getTextWithDefault( "Database User", "firstlook" )
        self._databasePassword = self._util.retrievePassword( "Database Password" )
        self.displayDictionaries()
        self._dictionaryNum = self._util.getTextWithDefault( "Dictionary", "1" )
        
    def source(self):
        return ("imt", "pack-adjust")
        
    def databases(self):
        return ()

    def createAntCommand(self, configParams):
        antOptions = {}
        antOptions["configParams"] = configParams
        return self._util.createAntCommand( "build.xml", antOptions, "jar.build" )
    
    def execute(self):
        installDir = self._installDir
        databaseServer = self._databaseServer
        databaseName = self._databaseName
        databaseUser = self._databaseUser
        databasePassword = self._databasePassword
        
        tag = self._tag

        stopWatch = StopWatch()
        util = self._util;

        stopWatch.start()
        installDir = installDir.replace( "\\", "\\\\")

        configParams={"installDir":installDir, "dbhostname":databaseServer, "packadjustdb":databaseName, "user":databaseUser, "password":databasePassword, "appName":"pack-adjust"}
        self._configure.configure( "pack-adjust", self._tag, configParams, self._dictionaryMap[self._dictionaryNum] )
        
        os.chdir( "pack-adjust" )
        
        aetAntCommand = self.createAntCommand( "" )
        print "ANT Command: " + aetAntCommand
        os.system( aetAntCommand )

        buildStamp = BuildStamp( "buildStamp.txt", "Pack Adjuster", tag, "version.txt", self._dictionaryMap[self._dictionaryNum] )
        buildStamp.generate()

        zipFileName = "pack-adjust.zip"
            
        try:
            os.remove( zipFileName )
        except OSError:
            print ""
               
        zip = ZipFile( zipFileName, "w" )

        zip.write( "buildStamp.txt", "buildStamp.txt" )
        zip.write( "dist/pack-adjust-1.0.jar", "pack-adjust.jar" )
        zip.write( "conf/hibernate.cfg.xml", "conf/hibernate.cfg.xml" )
        zip.write( "conf/cityhendrick.properties", "conf/cityhendrick.properties" )
        zip.write( "conf/dynamicpack.properties", "conf/dynamicpack.properties" )
        zip.write( "conf/flatpack.properties", "conf/flatpack.properties" )
        zip.write( "conf/flatpacktype.properties", "conf/flatpacktype.properties" )
        zip.write( "conf/log4j.properties", "conf/log4j.properties" )
        zip.write( "conf/pack-adjust.bat", "pack-adjust.bat" )
        zip.write( "conf/_cp.bat", "_cp.bat" )

        os.mkdir( "logs" )
        open( "logs/.blank", "w" )
        zip.write( "logs/.blank" )

        libs = glob( "lib/*.jar" )
        for lib in libs:
            if( lib != 'CVS' ):
                zip.write( lib )

        zip.close()

        shutil.copy( zipFileName, "../../" + "output/" + zipFileName )

        print( "Output available in directory, output/" )

        stopWatch.stop()
        print( "Pack Adjuster Build Time: %d" % stopWatch.getTime() )
