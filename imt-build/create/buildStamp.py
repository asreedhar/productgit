
import sys, os, string, shutil, time, socket
from getpass import getpass
from os.path import join, getsize
from sys import stdout

class BuildStamp:

    def __init__(self, fileName, productName, tagName, versionFileName, buildType = ""):
        self._fileName = fileName
        self._productName = productName
        self._tagName = tagName
        self._versionFileName = versionFileName
        self._buildType = buildType

    def generate(self):
        versionFile = open( self._versionFileName, "r" )
        version = versionFile.readline()
        versionFile.close()

        stampFile = open( self._fileName, "w" )
        stampFile.write( "***** FIRST LOOK BUILD STAMP *****\n\n" )
        stampFile.write( " PRODUCT: " + self._productName + "\n" )
        if( self._buildType != "" ):
            stampFile.write( "    TYPE: " + self._buildType + "\n" )
        stampFile.write( "     TAG: " + self._tagName + "\n" )
        stampFile.write( " VERSION: " + version + "\n\n" )

        stampFile.write( "    TIME: " + time.strftime( '%m/%d/%y %H:%M:%S' ) + "\n")
        stampFile.write( "HOSTNAME: " + socket.gethostname() + "\n")
        stampFile.close()
        
        
        
