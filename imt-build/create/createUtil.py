"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

class CreateUtil:

	def removeDirTree( self, dirName ):
		if os.path.exists( dirName ):
			for root, dirs, files in os.walk(dirName, topdown=False):
				for name in files:
					os.remove(join(root, name))
				for name in dirs:
					os.rmdir(join(root, name))
			os.rmdir( dirName )
	
	def createAntCommand( self, file, dictionary, target ):

		antCommand = "-f %s " % file
                options = ""

                antOptionKeys = dictionary.keys(  )
                for key in antOptionKeys:
                    value = dictionary[key]
                    option = "-D%s=\"%s\" " % ( key, value )
                    options = options + option

                ant = "ant " + antCommand + " " + options + " " +  target

		return ant

	def retrievePassword( self, type ):
		password = getpass( "Enter the " + type + " password: " )
		passwordCheck = getpass( "Please Re-Enter the " + type + " password: " )
		while( password != passwordCheck or password == "" ):
			print ""
			print "YOU HAVE ENTERED IN 2 DIFFERENT PASSWORDS!!!! PLEASE REENTER"
			print ""
			password = getpass( "Enter the " + type + " password: " )
			passwordCheck = getpass( "Please Re-Enter the " + type + " password: " )
		return password

	def getTextWithDefault( self, prompt, default ):
		supplied = raw_input( prompt + " [" + default + "] : " )
		if( supplied == "" ):
			supplied = default
		return supplied

	def setUpEnvironment(self):
		topDir = os.getcwd()
		if ( not os.environ.has_key("JAVA_HOME") ):
			javaHome = "C:/j2sdk1.4.1_03"
			os.environ["JAVA_HOME"] = javaHome
			print "Setting JAVA_HOME: " + javaHome
		else:
			print "JAVA_HOME already set: " + os.environ["JAVA_HOME"]
		antHome = topDir + "/tools/ant-1.5.3"
		os.environ["ANT_HOME"] = antHome
		os.environ["PATH"] += ";%s/bin;%s/bin" % ( os.environ["JAVA_HOME"], os.environ["ANT_HOME"])  
	
	def createWorkDir(self):
		self.createDir("work")
		
	def createOutputDir(self):
		self.createDir("output")
		
	def createDir(self, dirName):
		try:
			self.removeDirTree( dirName )
			os.mkdir(dirName)
		except OSError:
			errText = "\nERROR: Unable to remove directory: '%s'.\nMake sure NO file in '%s' is in use by the operating system!" % (dirName, dirName)
			raise IOError(errText)
