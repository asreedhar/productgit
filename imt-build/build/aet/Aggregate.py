import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

import logging

class Aggregate:

    def __init__(self, dtsServer, sourceServer, sourceDatabase, targetServer, targetDatabase):
        self._logger = logging.getLogger( "aggregate" )
        self._dtsServer = dtsServer
        self._sourceServer = sourceServer
        self._sourceDatabase = sourceDatabase
        self._targetServer = targetServer
        self._targetDatabase = targetDatabase

    def execute(self):

        currentDir = os.getcwd()

	print "Running aggregation on %s from %s.%s to %s.%s" % ( self._dtsServer, self._sourceServer, self._sourceDatabase, self._targetServer, self._targetDatabase )

        command = "DTSRun /S \"%s\" /N \"IE Load\" /A \"SourceDB\":\"8\"=\"%s\" /A \"SourceSQLServer\":\"8\"=\"%s\" /A \"TargetDB\":\"8\"=\"%s\" /A \"TargetSQLServer\":\"8\"=\"%s\" /W \"0\" /E"
        command = command  % ( self._dtsServer, self._sourceDatabase, self._sourceServer,  self._targetDatabase, self._targetServer )

        print command

	os.system( command )

	print "Finished aggregating"

        os.chdir( currentDir )
        

def getOptions():
    optionsMap = {};

    if( len( sys.argv ) < 4 ):
        print "Must supply at least five arguments: <server> <sourcedatabase> <targetdatabase>"

        print ""
        print "This script is used to build a database, examples of usage:\n"

        print "Aggregate.py PLUTODEV IMT3 IE3"
        sys.exit(1)

    optionsMap["dtsServer"] = sys.argv[1]
    optionsMap["sourceServer"] = sys.argv[1]
    optionsMap["targetServer"] = sys.argv[1]
    print "Using Server: " + optionsMap["dtsServer"]

    optionsMap["sourceDatabase"] = sys.argv[2]
    print "Source Database: " + optionsMap["sourceDatabase"]
    
    optionsMap["targetDatabase"] = sys.argv[3]
    print "Target Database: " + optionsMap["targetDatabase"]

    return optionsMap

if __name__ == '__main__':
    logger = logging.getLogger()
    hdlr = logging.StreamHandler( sys.stdout )
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%H:%M:%S')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)

    optionsMap = getOptions()

    aggregate = Aggregate(optionsMap["dtsServer"],
                          optionsMap["sourceServer"],
                          optionsMap["sourceDatabase"],
                      	  optionsMap["targetServer"],
                      	  optionsMap["targetDatabase"])

    aggregate.execute()
