import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

import logging

class Constants:

    def __init__(self, directory, ieDatabase):
        self._logger = logging.getLogger( "constants" )
        self._directory = directory
        self._ieDatabase = ieDatabase

    def store(self):
        currentDir = os.getcwd()
        os.chdir( self._directory + "/data/constants" )
        logPipe = os.popen( "ReadConstants.bat %s %s" % ( "PLUTODEV", self._ieDatabase) )
        self._logger.debug( "\n\t" + string.replace( logPipe.read(), "\n", "\n\t" ) )
        os.chdir( currentDir )

    def restore(self):
        currentDir = os.getcwd()
        os.chdir( self._directory + "/data/constants" )
        logPipe = os.popen( "WriteConstants.bat %s %s" % ( "PLUTODEV", self._ieDatabase) )
        self._logger.debug( "\n\t" + string.replace( logPipe.read(), "\n", "\n\t" ) )
        os.chdir( currentDir )
