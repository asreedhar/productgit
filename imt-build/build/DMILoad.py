import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout

import logging

class DMILoad:

    def __init__(self, stagingDataDir, demoFilesDir, imtDatabase):
        self._logger = logging.getLogger( "dmiLoad" )
        self._stagingDataDir = stagingDataDir
        self._demoFilesDir = demoFilesDir
        self._imtDatabase = imtDatabase

    def execute(self):
        currentDir = os.getcwd()

        try:
            os.chdir( self._stagingDataDir )

            self._logger.debug( "Executing Populate First Look Staging Data" )
            os.system( "isql -E -SPLUTODEV -d%s < %s" % (self._imtDatabase, "PopulateFirstlookStagingData.sql") )

            self._logger.debug( "Executing Populate First Look Staging Data" )
            logPipe = os.popen( "isql -E -SPLUTODEV -dDBASTAT -Q\"\"dbo.usp_testprep\"\"" )
            self._logger.debug( "\n\t" + string.replace( logPipe.read(), "\n", "\n\t" ) )

            os.chdir( self._demoFilesDir )
            try:
                invFile = open('firstlook_inv_2003120201.txt', 'r')
            except:
                raise DMILoadError("The file 'firstlook_inv_2003120201.txt does not exist in " + self._demoFilesDir )

            try:
                invFile = open('firstlook_sales_2003120201.txt', 'r')
            except:
                raise DMILoadError("The file 'firstlook_sales_2003120201.txt does not exist in " + self._demoFilesDir )

            command = "DTSRun /S \"PLUTODEV\" /N \"DMI Load: Process Files\" /A \"fileserver\":\"8\"=\"PLUTODEV\" /A \"SQLServer\":\"8\"=\"PLUTODEV\" /A \"targetdb\":\"8\"=\"%s\" /A \"path\":\"8\"=\"\\datafeeds\\DMIReceived\\\" /W \"0\" /E" % (self._imtDatabase)
            os.system( command )

        finally:
            os.chdir( currentDir )
        
class DMILoadError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
