import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from glob import glob

import getopt

import logging

class BuildDB:

    def __init__(self, directory, mode, sqlServer, dbName, testRun = 0):
        self._logger = logging.getLogger( "buildDB" )
        self._directory = string.lower( directory )
        self._mode = string.lower( mode )
        self._sqlServer = string.lower( sqlServer )
        self._dbName = string.lower( dbName )
        self._testRun = testRun

    def execute(self):

        testRun = self._testRun

        self._logger.info( "Building Database: %s, on Server: %s, in Mode: %s" % ( self._dbName, self._sqlServer, self._mode ) )
        if( testRun == 1 ):
            self._logger.info( "This is a test run ONLY, no action will be taken." )

        if( ((self._mode == 'd') or (self._mode == 'p')) ):
            if( string.find( self._dbName, "imt" ) == -1 ):
                self._logger.error( "Only the IMT database has a 'd' or 'p' build option." )
                return
            
        currentDir = os.getcwd()
        os.chdir( self._directory )

        self.clearOutUsers()
        self.buildDatabase()

        if( ( self._mode == 'u' ) or ( self._mode == 'r' ) ):
            self.buildDatabaseStructure()
            self.loadBaseData()            
        
        if( ( self._mode == 'u' ) ):
            self.loadUnitTestData()
            
        if( ( self._mode != 'p' ) ):
            self.executeAlterScripts()
            self.buildStoredProcs()       
            
        os.chdir( currentDir )

    def alter(self):

        currentDir = os.getcwd()
        os.chdir( self._directory )

        self._logger.info( "Alter Database: %s, on Server: %s, in Mode: %s" % ( self._dbName, self._sqlServer, self._mode ) )
        if( self._testRun == 1 ):
            self._logger.info( "This is a test run ONLY, no action will be taken." )

        self.executeAlterScripts()
        self.buildStoredProcs()       
            
        os.chdir( currentDir )
            

    def clearOutUsers(self):
        self._logger.info( "Clearing Out Users" )
        if( self._testRun == 0 ):
            os.system( "isql -b -m1 -n -dDBASTAT -S %s -E -Qusp_killusers'%s'" % ( self._sqlServer, self._dbName ) )

    def buildDatabase(self):
        if( ( self._mode == 'u' ) or ( self._mode == 'r' ) ):
            ''' Execute USP_BuildIMT '''
            self._logger.info( "Building a Blank Database" )
            if( self._testRun == 0 ):
                os.system( "isql -b -m-1 -n -dDBASTAT -S %s -E -Qusp_BuildIMT'%s'" % (self._sqlServer, self._dbName));

        else:
            ''' Execute USP_BuildIMTwithData '''
            self._logger.info( "Building a Database with Data" )
            if( self._testRun == 0 ):
                os.system( "isql -b -m-1 -n -dDBASTAT -S %s -E -Qusp_BuildIMTwithData'%s'" % (self._sqlServer, self._dbName));

    def buildDatabaseStructure(self):
        ''' Execute Objects '''
        self._logger.info( "Executing Base Objects Script" )
        sqlScripts = glob( "scripts/objects/*.sql" )
        for script in sqlScripts:
            self._logger.debug( "Executing Base Object: " + script )
            if( self._testRun == 0 ):
                os.system( "isql -b -m-1 -n -d%s -S %s -E -i%s" % ( self._dbName, self._sqlServer, script ) )

    def loadBaseData(self):
        try:
            ''' Load base data '''
            self._logger.info( "Loading Base Data" )
            baseDataTableListFile = open( "scripts/basedata/basetableslist.txt", "r" )
            baseDataTable = baseDataTableListFile.readline()
            while( baseDataTable != "" ):
                if( string.find( baseDataTable, ";" ) == -1 ):
                    baseDataTable = string.strip( baseDataTable )
                    self._logger.debug( "Loading Data File: " + baseDataTable )
                    if( self._testRun ==  0 ):
                        logPipe = os.popen( "bcp %s..%s in .\\scripts\\basedata\\%s.data -n -b100000 -S%s -T -E" % (self._dbName, baseDataTable, baseDataTable, self._sqlServer) )
                baseDataTable = baseDataTableListFile.readline()
        except IOError:
            self._logger.info( "No basedata objects exist in this project" )

    def loadUnitTestData(self):
        ''' Unit Test data '''
        self._logger.info( "Loading Unit Test Data" )
        unitTestDataFiles = glob( "scripts/UnitTestBaseData/*.sql" )
        for testDataFile in unitTestDataFiles:
            self._logger.debug( "Executing Unit Test Script: " + testDataFile )
            if( self._testRun ==  0 ):
                os.system( "isql -b -m-1 -n -d%s -S %s -E -i%s" % ( self._dbName, self._sqlServer, testDataFile ) )

    def buildStoredProcs(self):
        ''' StoredProcs '''
        self._logger.info( "Executing Stored Procedure Scripts" )
        storedProcs = glob( "scripts/storedProcs/*.PRC" )
        for proc in storedProcs:
            self._logger.debug( "Executing Stored Proc: " + proc )
            if( self._testRun ==  0 ):
                os.system( "isql -b -m-1 -n -d%s -S %s -E -i%s" % ( self._dbName, self._sqlServer, proc ) )


    def executeAlterScripts(self):
        ''' Alter '''
        self._logger.info( "Executing Alter Script" )
        alterScripts = glob( "scripts/AlterSQL/*.sql" )
        for alterScript in alterScripts:
            self._logger.info( "Executing Alter Script: " + alterScript )
            if( self._testRun ==  0 ):
                os.system( "isql -b -m-1 -n -d%s -S %s -E -i%s" % ( self._dbName, self._sqlServer, alterScript ) )


def getOptions():
    dir = "."
    optionsMap = {};

    if( len( sys.argv ) < 4 ):
        print "Must supply at least three arguments: <type> <server> <database>"

        print ""
        print "This script is used to build a database, examples of usage:\n"

        print "Unit Test Build:  BuildDB.py u PLUTODEV IE3"
        print "Build with data:  BuildDB.py d PLUTODEV IMT5"
        print "Build last production:  BuildDB.py p PLUTODEV IMT5"
        print "Build for regression:  BuildDB.py r PLUTODEV IMT3"
        print ""
        print "Alter a Production DB: BuildDB.py alter PLUTOTEST IE"
        print ""
        sys.exit(1)

    optionsMap["type"] = sys.argv[1]
    print "Using Type: " + optionsMap["type"]
    
    optionsMap["server"] = sys.argv[2]
    print "Using Server: " + optionsMap["server"]
    
    optionsMap["database"] = sys.argv[3]
    print "Using Database: " + optionsMap["database"]

    if( len( sys.argv ) == 5 ):
        dir = sys.argv[4]
    optionsMap["directory"] = dir
    print "Using Directory: " + optionsMap["directory"]

    return optionsMap

if __name__ == '__main__':
    logger = logging.getLogger()
    hdlr = logging.StreamHandler( sys.stdout )
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%H:%M:%S')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)

    optionsMap = getOptions()

    buildDB = BuildDB(optionsMap["directory"],
                      optionsMap["type"],
                      optionsMap["server"],
                      optionsMap["database"])

    if( optionsMap["type"] != "alter" ):
        buildDB.execute()
    else:
        buildDB.alter()
    
