"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from zipfile import ZipFile
from glob import glob

from create.createUtil import CreateUtil
from create.buildStamp import BuildStamp
from misc.StopWatch import StopWatch

class ScriptIE:

    def __init__(self, tag):
        self._util = CreateUtil();
        self._tag = tag

    def name(self):
        return "IE Database"

    def query(self):
        print ""

    def source(self):
        return ("aet", "ie-calculate")

    def execute(self):
        util = self._util;
        tag = self._tag;
        stopWatch = StopWatch()

        currentDir = os.getcwd()

	try:

            stopWatch.start()
            os.chdir( "ie-calculate" )

            buildStamp = BuildStamp( "buildStamp.txt", "IE-Calculate", tag, "version.txt" )
            buildStamp.generate()

            ieDBZipFileName = "ieDb.zip"
                
            try:
                os.remove( ieDBZipFileName )
            except OSError:
                print ""
                   
            zip = ZipFile( ieDBZipFileName, "w" )

            zip.write( "buildStamp.txt", "iedb/buildStamp.txt" )

            rootDir = os.getcwd()

            os.chdir( "database/management/scripts/AlterSQL/" )
            alterScripts = glob( "*.*" )
            for alterScript in alterScripts:
                if( alterScript != 'CVS' ):
                    zip.write( alterScript, "iedb/scripts/AlterSQL/" + alterScript )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/basedata/" )
            basedataFiles = glob( "*.*" )
            for basedata in basedataFiles:
                if( basedata != 'CVS' ):
                    zip.write( basedata, "iedb/scripts/basedata/" + basedata )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/objects/" )
            objectFiles = glob( "*.*" )
            for objectFile in objectFiles:
                if( objectFile != 'CVS' ):
                    zip.write( objectFile, "iedb/scripts/objects/" + objectFile )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/UnitTestBaseData/" )
            utbdFiles = glob( "*.*" )
            for utbdFile in utbdFiles:
                if( utbdFile != 'CVS' ):
                    zip.write( utbdFile, "iedb/scripts/UnitTestBaseData/" + utbdFile )
            os.chdir( rootDir )

            zip.write( "../../build/BuildDB.py", "iedb/BuildDB.py" )
            zip.write( "../../build/aet/Aggregate.py", "iedb/Aggregate.py" )
            zip.close()

            shutil.copy( ieDBZipFileName, "../../" + "output/" + ieDBZipFileName )

        finally:
            os.chdir( currentDir )
            stopWatch.stop()
            print( "IE DB Build Time: %d" % stopWatch.getTime() )

            

