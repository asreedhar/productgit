"""This file is used to run the whole build process.
	WARNING: This file is not os independent!!!!
"""

import sys, os, string, shutil
from getpass import getpass
from os.path import join, getsize
from sys import stdout
from zipfile import ZipFile
from glob import glob

from create.createUtil import CreateUtil
from create.buildStamp import BuildStamp
from misc.StopWatch import StopWatch

class ScriptIMT:

    def __init__(self, tag):
        self._util = CreateUtil();
        self._tag = tag

    def name(self):
        return "IMT Database"

    def query(self):
        print ""

    def source(self):
        return ("imt", "IMT")

    def execute(self):
        util = self._util;
        tag = self._tag;
        stopWatch = StopWatch()

        currentDir = os.getcwd()

	try:

            stopWatch.start()
            os.chdir( "imt" )

            buildStamp = BuildStamp( "buildStamp.txt", "IMT", tag, "version.txt" )
            buildStamp.generate()

            imtDBZipFileName = "imtDb.zip"
                
            try:
                os.remove( imtDBZipFileName )
            except OSError:
                print ""
                   
            zip = ZipFile( imtDBZipFileName, "w" )

            zip.write( "buildStamp.txt", "imtdb/buildStamp.txt" )

            rootDir = os.getcwd()

            os.chdir( "database/management/scripts/AlterSQL/" )
            alterScripts = glob( "*.*" )
            for alterScript in alterScripts:
                if( alterScript != 'CVS' ):
                    zip.write( alterScript, "imtdb/scripts/AlterSQL/" + alterScript )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/basedata/" )
            basedataFiles = glob( "*.*" )
            for basedata in basedataFiles:
                if( basedata != 'CVS' ):
                    zip.write( basedata, "imtdb/scripts/basedata/" + basedata )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/objects/" )
            objectFiles = glob( "*.*" )
            for objectFile in objectFiles:
                if( objectFile != 'CVS' ):
                    zip.write( objectFile, "imtdb/scripts/objects/" + objectFile )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/storedProcs/" )
            storedProcs = glob( "*.*" )
            for storedProc in storedProcs:
                if( storedProc != 'CVS' ):
                    zip.write( storedProc, "imtdb/scripts/storedProcs/" + storedProc )
            os.chdir( rootDir )

            os.chdir( "database/management/scripts/UnitTestBaseData/" )
            utbdFiles = glob( "*.*" )
            for utbdFile in utbdFiles:
                if( utbdFile != 'CVS' ):
                    zip.write( utbdFile, "imtdb/scripts/UnitTestBaseData/" + utbdFile )
            os.chdir( rootDir )

            zip.write( "../../build/BuildDB.py", "imtdb/BuildDB.py" )
            zip.close()

            shutil.copy( imtDBZipFileName, "../../" + "output/" + imtDBZipFileName )

        finally:
            os.chdir( currentDir )
            stopWatch.stop()
            print( "IMT DB Build Time: %d" % stopWatch.getTime() )

            

