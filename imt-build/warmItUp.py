import sys, os, string, re, time
from os.path import join, getsize
from sys import stdout

name = "First Look Warm It Up!"
major_version = "0.1"
minor_version = "a24"

resin_exec = "httpd.exe -e %s"
defaultResinDir = "c:\\resin-2.1.8"

class WarmItUp:

	docBase = "."
	jspPaths = []

	def __init__(self, docBaseArg):
		self.docBase = docBaseArg
	
	def setUpEnvironment(self, resinDir):
		os.environ["PATH"] += ";%s/bin;" % ( resinDir )  
	
	def execute(self):
		print self.docBase

		for root, dirs, files in os.walk( self.docBase ):
			for file in files:
				if( re.compile(".*\.jsp$").match( file ) ):
					relativeRoot = string.replace( root, self.docBase, "" )
					
					path = relativeRoot + "/" + file
					path = string.replace( path, "\\", "/" )

					if( re.compile("^/root.*$").match( path ) ):
						path = path[5:]

					self.jspPaths.append( path )
					
		print "Compiling %s pages" % ( len( self.jspPaths ) );		
		
		while ( len( self.jspPaths ) > 0 ):
			
			try:
				tempCombined = self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
				tempCombined = tempCombined + " " + self.jspPaths.pop();
			except IndexError:
				print ""
			
			print "\tCompiling: " + tempCombined[:50] + "..."			
			os.popen( resin_exec % ( tempCombined) )
			
			print " %s pages left" % ( len( self.jspPaths ) );
	
def printUsage():
	print '''
warmItUp.py - A utility to precompile all the webapps in a Resin instance:


There is one REQUIRED argument - the webapps directory of Resin.  For
example, on our dev machine this would be c:\\development\\resin-2.1.8\\webapps.

There is one optional argument - the location of resin.  The default is C:\\resin-2.1.8.

Go forth and conquer.
'''
	
	
if __name__ == '__main__':

	""" Print Usage Information """
	print name + ", " + major_version + " " + minor_version

	if '-h' in sys.argv or '--help' in sys.argv or '--help' in sys.argv or '-?' in sys.argv: 
		printUsage()
		sys.exit(0)

	if( len( sys.argv ) < 2 ):
		printUsage()
		sys.exit(0)
	suppliedDocBase = sys.argv[1]
	
	if( len( sys.argv ) == 3 ):
		resinDir = sys.argv[2]
	else:
		resinDir = defaultResinDir

	
	print "Using directory " + suppliedDocBase
	print "Using resin " + resinDir

	""" Start Warm Up Process """

	warmItUp = WarmItUp(suppliedDocBase)
	warmItUp.setUpEnvironment( resinDir )
	warmItUp.execute()
