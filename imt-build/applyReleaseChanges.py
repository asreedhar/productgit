import os, sys
from glob import glob
from create.createUtil import CreateUtil

class ApplyReleaseChanges:
	logFileLocation = "\\management\\BuildManagementDBLogs\\BuildIMT.log"
	scriptsDir = "\\management\\scripts\\AlterSQL"
	procsDir = "\\management\\scripts\\storedProcs"

	def printUsage(self):
		print "applyReleaseChanges <database server> <database>"

	def writeLog( self, message, text ):
		f = open( self.dir + self.logFileLocation, "a")
		f.write( message )
		f.write( text )
		f.flush()
		f.close()

	def executeSQLWithPattern( self, pattern, message, dir ):
		files = glob( os.path.join( dir, pattern ) )
		for file in files:
			self.executeSQL( file, message )

	def executeSQL( self, file, message ):
		print message + file
		logFile = os.popen( "isql -b -m1 -n -d" + self.db + " -S " + self.dbServer + " -E -i" + file )	
		text = logFile.read()
		if( len( text ) > 0 ): 
			print "ERROR: %s" % (text)
			self.writeLog( message + file + "\n", text )
			return -1
		else:
			return 1

	def parseManifest( self, dir ):
		manifest = open(os.path.join( dir, "manifest.txt" ) )
		scripts = []
		lines = manifest.readlines()
		for line in lines:
			if( len( line.strip() ) > 0 ):
				scripts.append(line.strip())
		manifest.close()
		return scripts
		
	def checkForScriptExistence( self, scriptName ):
		query = "\"EXIT(SELECT count(*) from Alter_Script where AlterScriptName = \'" + scriptName + "\')\""
		log = os.popen( "isql -b -m-1 -n -d " + self.db + " -S " + self.dbServer + " -E -q " + query )
		lines = log.readlines()
		log.close()
		if( len( lines ) > 2 ):
			return int(lines[2].strip())
			
	def insertAlterScript( self, scriptName ):
		query = "\"INSERT into Alter_Script values(\'" + scriptName + "\')\""
		log = os.popen( "isql -b -m-1 -n -d " + self.db + " -S " + self.dbServer + " -E -q " + query )
		lines = log.readlines()
		log.close()

	def applyAlterFileChanges( self ):
		alterFiles = self.parseManifest( self.dir + self.scriptsDir )
		for alter in alterFiles:
			doesExist = self.checkForScriptExistence( alter )
			if( doesExist ):
				print alter + " has already been applied -- skipping."
			else:
				error = self.executeSQL( alter, "Applying Alter Script: " )
				if( error < 0 ):
					print "ERROR executing %s!  Stopping script." % (alter)
					return error
				else:
					self.insertAlterScript( alter )
		return 1

if __name__ == "__main__":
	changes = ApplyReleaseChanges()
	if( len(sys.argv) < 3 ):
		changes.printUsage()
	else:
		error = 0
		util = CreateUtil();
		changes.dbServer = sys.argv[1]
		changes.db = sys.argv[2]
		changes.dir = util.getTextWithDefault( "Enter location of database directory:", "c:\\projects\\imt-build\\work\\IMT\\database" )
		os.chdir( changes.dir + changes.scriptsDir )
		error = changes.applyAlterFileChanges()
		if( error >= 0 ):
			os.chdir( changes.dir + changes.procsDir )
			changes.executeSQLWithPattern( "*.PRC", "Applying Stored Procedure: ", changes.dir + changes.procsDir )
	