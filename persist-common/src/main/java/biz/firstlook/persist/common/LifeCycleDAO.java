package biz.firstlook.persist.common;

import java.io.Serializable;

import biz.firstlook.common.model.Identifiable;

public interface LifeCycleDAO<T extends Identifiable<?>, ID extends Serializable> extends GenericDAO<T, ID> {

	
}
