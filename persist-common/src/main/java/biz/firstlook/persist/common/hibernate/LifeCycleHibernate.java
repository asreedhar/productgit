package biz.firstlook.persist.common.hibernate;

import java.io.Serializable;

import org.apache.log4j.Logger;

import biz.firstlook.common.model.Identifiable;
import biz.firstlook.persist.common.LifeCycleDAO;

public abstract class LifeCycleHibernate<T extends biz.firstlook.common.model.Identifiable<?>, ID extends Serializable>
	extends GenericHibernate<T, ID> implements LifeCycleDAO<T, ID> {

    private static Logger log = Logger.getLogger(LifeCycleHibernate.class);

    @Override
    public T makePersistent(T entity) {
	Identifiable<?> identifiable = (Identifiable<?>) entity;
	boolean isInsert = (identifiable.getId() == null);
	log.debug("Make Persistent: Is Insert? " + isInsert);

	if (isInsert) {
	    preInsert(entity);
	} else {
	    preUpdate(entity);
	}
	prePersist(entity);
	T result = super.makePersistent(entity);
	postPersist(result);
	if (isInsert) {
	    postInsert(result);
	} else {
	    postUpdate(result);
	}
	return result;
    }

    @Override
    public void makeTransient(T entity) {
	preDelete(entity);
	super.makeTransient(entity);
	postDelete(entity);
    }

    public void preInsert(T entity) {
	log.debug("Default Do Nothing preInsert");
    }

    public void postInsert(T result) {
	log.debug("Default Do Nothing postInsert");
    }

    public void preUpdate(T entity) {
	log.debug("Default Do Nothing preUpdate");
    }

    public void postUpdate(T result) {
	log.debug("Default Do Nothing postUpdate");
    }

    public void preDelete(T entity) {
	log.debug("Default Do Nothing preDelete");
    }

    public void postDelete(T entity) {
	log.debug("Default Do Nothing postDelete");
    }

    public void prePersist(T entity) {
	log.debug("Default Do Nothing prePersist");
    }

    public void postPersist(T entity) {
	log.debug("Default Do Nothing postPersist");
    }

}
