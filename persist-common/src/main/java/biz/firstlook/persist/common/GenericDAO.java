package biz.firstlook.persist.common;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface GenericDAO<T, ID extends Serializable> {

	T findById(ID id);
	
	T findById(ID id, boolean lock);

	List<T> findAll();

	List<T> findByExample(T exampleInstance, String... excludeProperty);
	T findOneByExample(T exampleInstance, String... excludeProperty);

	@Transactional(readOnly=false, rollbackFor=Exception.class)
	T makePersistent(T entity);

	@Transactional(readOnly=false, rollbackFor=Exception.class)
	Collection<T> makePersistent(Collection<T> entity);

	@Transactional(readOnly=false, rollbackFor=Exception.class)
	void makeTransient(T entity);

	@Transactional(readOnly=false, rollbackFor=Exception.class)
	void makeTransient(Collection<T> entity);

}
