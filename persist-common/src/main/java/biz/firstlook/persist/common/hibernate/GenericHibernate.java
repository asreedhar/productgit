package biz.firstlook.persist.common.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.persist.common.GenericDAO;

public abstract class GenericHibernate<T, ID extends Serializable> extends
		HibernateDaoSupport implements GenericDAO<T, ID> {

	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public GenericHibernate() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                        .getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public T findById(ID id) {
		return findById(id, false);
	}
	
	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getHibernateTemplate().load(getPersistentClass(), id,
					org.hibernate.LockMode.UPGRADE);
		} else {
			entity = (T) getHibernateTemplate().load(getPersistentClass(), id);
		}
		getHibernateTemplate().refresh( entity );
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return getHibernateTemplate().loadAll(getPersistentClass());
	}

	@SuppressWarnings("unchecked")
	public List<T> findByExample(final T exampleInstance,
			final String... excludeProperty) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				// Using Hibernate, more difficult with EntityManager and EJB-QL
				Criteria crit = session.createCriteria(getPersistentClass());
				Example example = Example.create(exampleInstance);
				for (String exclude : excludeProperty) {
					example.excludeProperty(exclude);
				}
				crit.add(example);
				return crit.list();
			}

		};
		return (List) getHibernateTemplate().execute(callback);
	}

	@SuppressWarnings("unchecked")
	public T findOneByExample(final T exampleInstance, final String... excludeProperty) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				// Using Hibernate, more difficult with EntityManager and EJB-QL
				Criteria crit = session.createCriteria(getPersistentClass());
				Example example = Example.create(exampleInstance);
				for (String exclude : excludeProperty) {
					example.excludeProperty(exclude);
				}
				crit.add(example);
				return crit.uniqueResult();
			}

		};
		return (T) getHibernateTemplate().execute(callback);
	}

	@SuppressWarnings("unchecked")
	public T makePersistent(T entity) {
		return (T) getHibernateTemplate().merge(entity);
	}

	public void makeTransient(Collection<T> entities) {
		getHibernateTemplate().deleteAll(entities);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<T> makePersistent(Collection<T> entities) {
		Collection<T> nEntities = new HashSet<T>();
		for( T entity : entities ) {
			nEntities.add( (T) getHibernateTemplate().merge(entity) );
		}
		return nEntities;
	}

	public void makeTransient(T entity) {
		getHibernateTemplate().delete(entity);
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
    protected List<T> findByCriteria(final Criterion... criterion) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(getPersistentClass());
				for (Criterion c : criterion) {
					crit.add(c);
				}
				return crit.list();
			}
		};
		return (List) getHibernateTemplate().execute( callback );
   }

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
    protected T findUniqueByCriteria(final Criterion... criterion) {
		HibernateCallback callback = new HibernateCallback() {
			public T doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(getPersistentClass());
				for (Criterion c : criterion) {
					crit.add(c);
				}
				return (T) crit.uniqueResult();
			}
		};
		return (T) getHibernateTemplate().execute( callback );
   }
}
