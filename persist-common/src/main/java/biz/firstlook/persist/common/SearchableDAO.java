package biz.firstlook.persist.common;

import java.io.Serializable;
import java.util.List;

public interface SearchableDAO<T, ID extends Serializable> extends GenericDAO<T, ID> {

	public List<T> search( String searchStr );
	
}
