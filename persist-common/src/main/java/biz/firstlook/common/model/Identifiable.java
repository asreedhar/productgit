package biz.firstlook.common.model;

// TODO: This interface should be in some general First look Utility or generic model package
public interface Identifiable<T> {
	public T getId();
	public void setId(T id);
}
