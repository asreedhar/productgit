@echo off
echo.
echo Running Buying Alerts Mailer
echo.

SETLOCAL

SET CLASSPATH=

for %%f in (.\lib\*.jar) do call :cp %%f

SET CLASSPATH=%CLASSPATH%;.\conf;

echo Using CLASSPATH: %CLASSPATH%

java -cp %classpath% biz.firstlook.buyingAlerts.main.BuyingAlertsMailer %1 %2 %3

ENDLOCAL

echo.

:cp
SET CLASSPATH=%CLASSPATH%;%1