<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
	div{font-family:Arial,Verdana,Helvetica,Sans-serif;}
</style>

<div style="background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-496795to16356A-74h.gif);background-repeat:repeat-x;width:95%;padding:10px;border-left:solid 1px black;border-right:solid 1px black;">
	<div style="width:50%;float:right;">
		<div style="font-size:1.9em;font-style:italic;font-weight:bolder;color:white;float:right;">
			<div style="float:right;">Daily Buying Alert</div>
		</div>
		<div style="font-size:0.7em;color:white;float:right;">
			<a href="https://www.firstlook.biz/NextGen/OptimalInventorySearchSummary.go" style="color:white;float:right;padding-left:120px;">Go to First Look</a>
			<span style="float:right">${currentDate}</span>
		</div>
	</div>
	<img src="http://images.firstlook.biz/buyingAlertsEmail/FL-wTagline-transOnBlue.gif">
</div>
<div style="width:95%;padding:10px;border:solid 1px black;border-top:none;">
	<span style="font-size:1.1em;font-weight:bolder;">${businessUnitName}</span><br />
	<span style="margin-bottom:5px;font-size:0.8em;">
		Immediately available potential matches in Online Auctions, In-Group, and Live Auctions based on
		your First Look Search Engine Profile.
	<span>
	
	<#if hasOnlineAuctions>
		<div style="padding-bottom:10px;">
			<div style="background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-496795to16356A-22h.gif);background-color:#496799;background-position:bottom center;background-repeat:repeat-x;color:white;font-size:1.2em;font-weight:bolder;padding:10px;height:14px;">
				<span style="float:right;font-size:0.7em;color:">Within ${maxDistanceOnline} miles</span>
				ONLINE PURCHASING
			</div>
			<#list onlineResults as market>
				<#if market.market != 'IN GROUP'>
					<#if market_index % 2 = 0><div style="background-repeat:repeat-x;background-position:bottom center;font-size:1.2em;font-weight:bolder;padding:10px;background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-FFtoE8E8EA-16h.gif);border:solid 1px #999;border-top:none;">
					<#else><div style="background-repeat:repeat-x;background-position:bottom center;font-size:1.2em;font-weight:bolder;padding:10px; background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-F2toDBDBDD-16h.gif);background-color:#f2f2f2;border:solid 1px #999;border-top:none;">
					</#if>
						<div>
							<span>
								<a href="https://www.firstlook.biz/NextGen/OptimalInventorySearchSummary.go" style="float:right;color:#323d5c;font-size:0.7em;font-weight:normal;">VIEW</a>
							</span>
							<img src="http://images.firstlook.biz/buyingAlertsEmail/caratRight-4x7-A6.gif"> ${market.market} 	
						</div>
						<div style="font-size:0.9em;font-weight:bold;padding:10px;padding-top:0px;">
						${market.buyingOps} Buying Opportunities<br />
						<span style="text-decoration:underline;font-size:0.8em;">Top 3 Models available <span style="font-weight:normal">(by total retail contribution):</span></span><br />
							<#list market.candidateGroupings as grouping>
								<li style="font-size:0.8em; font-weight:bold;margin-left:5px;">${grouping.count} ${grouping.groupingDescription}S  </li>
								<#if grouping_index == 2>
									<#if grouping_has_next><br />${market.candidateCount - 3} More...</#if>
									<#break>
								</#if>
							</#list>
						</div>
					</div>
				</#if>
			</#list>
		</div>
	</#if>
	
	<#if hasInGroupAuctions>
		<div style="padding-bottom:10px;">
			<div style="background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-496795to16356A-22h.gif);background-color:#496799;background-position:bottom center;background-repeat:repeat-x;color:white;font-size:1.2em;font-weight:bolder;padding:10px;height:14px;">
				<span style="float:right;font-size:0.7em;color:">Within ${maxDistanceOnline} miles</span>
				IN-GROUP PURCHASING
			</div>
			<div style="background-repeat:repeat-x;background-position:bottom center;font-size:1.2em;font-weight:bolder;padding:10px; background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-FFtoE8E8EA-16h.gif);border:solid 1px #999;border-top:none;">
			<#list onlineResults as market>
				<#if market.market = 'IN GROUP'>
					<div>
						<span>
							<a href="https://www.firstlook.biz/NextGen/OptimalInventorySearchSummary.go" style="float:right;color:#323d5c;font-size:0.7em;font-weight:normal;">VIEW</a>
						</span>
						<img src="http://images.firstlook.biz/buyingAlertsEmail/caratRight-4x7-A6.gif"> ${groupName}	
					</div>
					<div style="font-size:0.9em;font-weight:bold;padding:10px;padding-top:0px;">
					${market.buyingOps} Buying Opportunities<br />
					<span style="text-decoration:underline;font-size:0.8em;">Top 3 Models available <span style="font-weight:normal">(by total retail contribution):</span></span><br />
						<#list market.candidateGroupings as grouping>
							<li style="font-size:0.8em; font-weight:bold;margin-left:5px;">${grouping.count} ${grouping.groupingDescription}S  </li>
							<#if grouping_index == 2>
								<#if grouping_has_next><br />${market.candidateCount - 3} More...</#if>
								<#break>
							</#if>
						</#list>
					</div>
				</#if>
			</#list>
		</div>	
	</#if>
	
	<#if hasLiveAuctions>
		<div style="padding-bottom:10px;">
			<div style="color:#496799;font-size:1.2em;font-weight:bolder;padding:10px;height:14px;border-bottom:solid 1px #999;">
				<span style="float:right;font-size:0.7em;color:">Within ${maxDistanceOnline} miles</span>
				LIVE AUCTIONS
			</div>
			<#list liveAuctions as liveAuction>
				<#if liveAuction_index = 5><#break></#if>
				<#if liveAuction_index % 2 = 0><div style="background-repeat:repeat-x;background-position:bottom center;font-size:1.2em;font-weight:bold;padding:10px; background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-FFtoE8E8EA-16h.gif);border:solid 1px #999;border-top:none;">
				<#else><div style="background-repeat:repeat-x;background-position:bottom center;font-size:1.2em;font-weight:bold;padding:10px; background-image:url(http://images.firstlook.biz/buyingAlertsEmail/grad-F2toDBDBDD-16h.gif);background-color:#f2f2f2;border:solid 1px #999;border-top:none;">
				</#if>
					<div>
						<span><img src="http://images.firstlook.biz/buyingAlertsEmail/caratRight-4x7-A6.gif"> ${liveAuction.auction} <br /></span>
					</div>
					<div style="font-size:0.8em;font-weight:normal;padding:10px;padding-top:0px;">
						${liveAuction.title}<br />
						${liveAuction.dateHeld} - ${liveAuction.auction}<br />
						<span>
							<a href="https://www.firstlook.biz/NextGen/LiveAuctionSummary.go" style="float:right;color:#323d5c;font-size:0.7em;font-weight:normal;">VIEW</a>
						</span>
						<div style="font-weight:bold;">${liveAuction.buyingOps} Buying Opportunities</div>
					</div>
				</div>
			</#list>
		</div>
	</#if>
	<span style="font-size:0.7em; color: #c0c0c0;">
		<span style="float:right">2007 INCISENT Technologies, Inc. All rights reserved.</span>
		* Search completed ${currentDateTime}  Time
	</span>
</div>
