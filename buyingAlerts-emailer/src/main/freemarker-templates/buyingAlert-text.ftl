First Look Daily Buying Alert for ${currentDate} 
Please visit the First Look Search Engine for more information

Immediately available potential matches in Online Auctions, In-Group, and Live Auctions based on
your First Look Search Engine Profile.

<#if hasOnlineAuctions>
Online Purchasing Within ${maxDistanceOnline} miles
<#list onlineResults as market>
<#if market.market != 'IN GROUP'>
${market.market} 	
${market.buyingOps} Buying Opportunities
Top 3 Models available (by total retail contribution):
<#list market.candidateGroupings as grouping>
 - ${grouping.count} ${grouping.groupingDescription}S  
<#if grouping_index == 2>
<#if grouping_has_next> - ${market.candidateCount -  3} More...</#if>
<#break>
</#if>
</#list>
</#if>
</#list>
</#if>
	
<#if hasInGroupAuctions>
In Group Purchasing Within ${maxDistanceOnline} miles
<#list onlineResults as market>
<#if market.market = 'IN GROUP'>
${groupName} 	
${market.buyingOps} Buying Opportunities
Top 3 Models available (by total retail contribution):
<#list market.candidateGroupings as grouping>
 - ${grouping.count} ${grouping.groupingDescription}S  
<#if grouping_index == 2>
<#if grouping_has_next> - ${market.candidateCount -  3} More...</#if>
<#break>
</#if>
</#list>
</#if>
</#list>
</#if>
	
<#if hasLiveAuctions>
Live Auctions Within ${maxDistanceOnline} miles
<#list liveAuctions as liveAuction>
<#if liveAuction_index = 5><#break></#if>
${liveAuction.auction} ${liveAuction.title}
${liveAuction.dateHeld} -  ${liveAuction.auction}
${liveAuction.buyingOps} Buying Opportunities

</#list>
</#if>

2007 INCISENT Technologies, Inc. All rights reserved.
Search completed ${currentDate} Central Time
