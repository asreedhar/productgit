package biz.firstlook.buyingAlerts.model;

import java.util.Date;

public class LiveAuction {

    private String title;
    private String auction;
    private String location;
    private Date dateHeld;
    private int buyingOps;

    public LiveAuction(String title, String auction, String location, Date dateHeld, int buyingOps) {
        this.title = title;
        this.auction = auction;
        this.location = location;
        this.dateHeld = dateHeld;
        this.buyingOps = buyingOps;
    }
    
    public String getAuction() {
        return auction;
    }

    public void setAuction(String auction) {
        this.auction = auction;
    }

    public int getBuyingOps() {
        return buyingOps;
    }

    public void setBuyingOps(int buyingOps) {
        this.buyingOps = buyingOps;
    }

    public Date getDateHeld() {
        return dateHeld;
    }

    public void setDateHeld(Date dateHeld) {
        this.dateHeld = dateHeld;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
