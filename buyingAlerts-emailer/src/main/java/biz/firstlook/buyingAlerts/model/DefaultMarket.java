package biz.firstlook.buyingAlerts.model;

public class DefaultMarket {

    private Integer accessGroupId;
    private String accessGroup;
    
    public DefaultMarket(Integer accessGroupId, String accessGroup) {
        this.accessGroupId = accessGroupId;
        this.accessGroup = accessGroup;
    }

    public String getAccessGroup() {
        return accessGroup;
    }

    public void setAccessGroup(String accessGroup) {
        this.accessGroup = accessGroup;
    }

    public Integer getAccessGroupId() {
        return accessGroupId;
    }

    public void setAccessGroupId(Integer accessGroupId) {
        this.accessGroupId = accessGroupId;
    }
}
