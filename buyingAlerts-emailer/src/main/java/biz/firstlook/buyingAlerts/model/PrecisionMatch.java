package biz.firstlook.buyingAlerts.model;

public class PrecisionMatch {

    private String customerFacingDescription;
    private int precisionMatches;
    
    public PrecisionMatch(String custFaceDesc, int precMatch){
        this.customerFacingDescription = custFaceDesc;
        this.precisionMatches = precMatch;
    }
    
    public String getCustomerFacingDescription() {
        return customerFacingDescription;
    }
    public void setCustomerFacingDescription(String customerFacingDescription) {
        this.customerFacingDescription = customerFacingDescription;
    }
    public int getPrecisionMatches() {
        return precisionMatches;
    }
    public void setPrecisionMatches(int precisionMatches) {
        this.precisionMatches = precisionMatches;
    }
    
}
