package biz.firstlook.buyingAlerts.model;

import java.util.List;

public class OnlineMarket {
    
    private int buyingOps;
    private List<CandidateGrouping> candidateGroupings;
    private int candidateCount;
    private String market;
    
    public OnlineMarket(Integer buyingOps,List<CandidateGrouping> candidateGroupings,
            String market) {
        this.buyingOps = buyingOps;
        this.candidateGroupings = candidateGroupings;
        this.market = market.toUpperCase();
        this.candidateCount = candidateGroupings.size();
    }
    
    
    
    public Integer getBuyingOps() {
        return buyingOps;
    }
    public void setBuyingOps(Integer buyingOps) {
        this.buyingOps = buyingOps;
    }
    public List<CandidateGrouping> getCandidateGroupings() {
        return candidateGroupings;
    }
    public void setCandidateGroupings(List<CandidateGrouping> candidateGroupings) {
        this.candidateGroupings = candidateGroupings;
    }
    public String getMarket() {
        return market;
    }
    public void setMarket(String market) {
        this.market = market;
    }



    public Integer getCandidateCount() {
        return candidateCount;
    }



    public void setCandidateCount(Integer candidateCount) {
        this.candidateCount = candidateCount;
    }
    
    

}
