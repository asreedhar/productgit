package biz.firstlook.buyingAlerts.model;

public class AuctionVehicle {

    private Integer accessGroupId;
    private String customerFacingDescription;
    private Integer groupingDescriptionId;
    private String groupingDescription;
    private String dealerNickName;
    
    public AuctionVehicle(Integer accessGroupId, String customerFacingDescription, Integer groupingDescriptionId,
            String groupingDescription, String dealerNickName) {
        this.accessGroupId = accessGroupId;
        this.customerFacingDescription = customerFacingDescription;
        this.groupingDescriptionId = groupingDescriptionId;
        this.groupingDescription = groupingDescription;
        this.dealerNickName = dealerNickName;
    }
    
    public Integer getAccessGroupId() {
        return accessGroupId;
    }
    public void setAccessGroupId(Integer accessGroupId) {
        this.accessGroupId = accessGroupId;
    }
    public String getCustomerFacingDescription() {
        return customerFacingDescription;
    }
    public void setCustomerFacingDescription(String customerFacingDescription) {
        this.customerFacingDescription = customerFacingDescription;
    }
    public String getDealerNickName() {
        return dealerNickName;
    }
    public void setDealerNickName(String dealerNickName) {
        this.dealerNickName = dealerNickName;
    }
    public String getGroupingDescription() {
        return groupingDescription;
    }
    public void setGroupingDescription(String groupingDescription) {
        this.groupingDescription = groupingDescription;
    }
    public Integer getGroupingDescriptionId() {
        return groupingDescriptionId;
    }
    public void setGroupingDescriptionId(Integer groupingDescriptionId) {
        this.groupingDescriptionId = groupingDescriptionId;
    }
    
    
    
}
