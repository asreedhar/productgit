package biz.firstlook.buyingAlerts.model;

public class BuyingAlertsSubscriber {
    
    private String businessUnitName;
    private String firstName;
    private String lastName;
    private String email;
    private Integer deliveryType;
    private Integer businessUnitId;
    private Integer memberId;
    private Integer purchasingDistance;
    private Integer liveAuctionDistance;
    private Integer subscriptionId;
    private boolean atcEnabled;
    private boolean tfsEnabled;
    private boolean gmacEnabled;
    private String groupName;

    public BuyingAlertsSubscriber(String businessUnitName, String firstName,String lastName, String email, Integer deliveryType,
            Integer businessUnitId, Integer memberId, Integer purchasingDistance, Integer liveAuctionDistance, Integer subscriptionId,
            boolean atcEnabled, boolean tfsEnabled, boolean gmacEnabled, String groupName) {
        this.businessUnitName = businessUnitName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.deliveryType = deliveryType;
        this.businessUnitId = businessUnitId;
        this.memberId = memberId;
        this.purchasingDistance = purchasingDistance;
        this.liveAuctionDistance = liveAuctionDistance;
        this.subscriptionId = subscriptionId;
        this.atcEnabled = atcEnabled;
        this.tfsEnabled = tfsEnabled;
        this.gmacEnabled = gmacEnabled;
        this.groupName = groupName.toUpperCase();
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public String getEmail() {
        return email;
    }

    public Integer getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(Integer deliveryType) {
        this.deliveryType = deliveryType;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public boolean isAtcEnabled() {
        return atcEnabled;
    }

    public void setAtcEnabled(boolean atcEnabled) {
        this.atcEnabled = atcEnabled;
    }

    public boolean isGmacEnabled() {
        return gmacEnabled;
    }

    public void setGmacEnabled(boolean gmacEnabled) {
        this.gmacEnabled = gmacEnabled;
    }

    public Integer getLiveAuctionDistance() {
        return liveAuctionDistance;
    }

    public void setLiveAuctionDistance(Integer liveAuctionDistance) {
        this.liveAuctionDistance = liveAuctionDistance;
    }

    public Integer getPurchasingDistance() {
        return purchasingDistance;
    }

    public void setPurchasingDistance(Integer purchasingDistance) {
        this.purchasingDistance = purchasingDistance;
    }

    public boolean isTfsEnabled() {
        return tfsEnabled;
    }

    public void setTfsEnabled(boolean tfsEnabled) {
        this.tfsEnabled = tfsEnabled;
    }

    public Integer getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Integer subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
    

}
