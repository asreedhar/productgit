package biz.firstlook.buyingAlerts.model;

public class SearchCandidateContribution {

    private Integer groupingDescriptionId;

    private String groupingDescription;

    private Double contribution;

    public SearchCandidateContribution(Integer groupingDescriptionId,
            String groupingDescription, Double contribution) {
        this.groupingDescription = groupingDescription;
        this.groupingDescriptionId = groupingDescriptionId;
        this.contribution = contribution;
    }

    public Double getContribution() {
        return contribution;
    }

    public void setContribution(Double contribution) {
        this.contribution = contribution;
    }

    public String getGroupingDescription() {
        return groupingDescription;
    }

    public void setGroupingDescription(String groupingDescription) {
        this.groupingDescription = groupingDescription;
    }

    public Integer getGroupingDescriptionId() {
        return groupingDescriptionId;
    }

    public void setGroupingDescriptionId(Integer groupingDescriptionId) {
        this.groupingDescriptionId = groupingDescriptionId;
    }

}
