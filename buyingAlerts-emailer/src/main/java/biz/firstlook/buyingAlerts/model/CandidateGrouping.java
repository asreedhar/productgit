package biz.firstlook.buyingAlerts.model;

public class CandidateGrouping{
    
    private int count;
    private double contribution;
    private String groupingDescription;
    
    public CandidateGrouping(double contribution, String groupingDescription) {
        this.count = 1;
        this.contribution = contribution;
        this.groupingDescription = groupingDescription;
    }
    
    public void increment(){
        count++;
    }
    
    public double getContribution() {
        return contribution;
    }
    public void setContribution(double contribution) {
        this.contribution = contribution;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

    public String getGroupingDescription() {
        return groupingDescription;
    }

    public void setGroupingDescription(String groupingDescription) {
        this.groupingDescription = groupingDescription;
    }
}
