package biz.firstlook.buyingAlerts.model;

public class SearchCandidate {

    private String make;
    private String line;
    private Integer segmentId;
    private Integer year;
    private Integer searchCandidateListId;
    
    public SearchCandidate(String make, String line, int segmentId, int year) {
        this.make = make;
        this.line = line;
        this.segmentId = segmentId;
        this.year = year;
    }
    
    public String getLine() {
        return line;
    }
    public void setLine(String line) {
        this.line = line;
    }
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public Integer getSegmentId() {
        return segmentId;
    }
    public void setSegmentId(Integer segmentId) {
        this.segmentId = segmentId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getSearchCandidateListId() {
        return searchCandidateListId;
    }

    public void setSearchCandidateListId(Integer searchCandidateListId) {
        this.searchCandidateListId = searchCandidateListId;
    }
}
