package biz.firstlook.buyingAlerts.model;

public class BuyingAlertsSearchPreference {

    private Integer accessGroupId;
    private Integer maxDistanceFromDealer;
    private Integer maxVehicleMileage;
    private Integer minVehicleMileage;
    private String customerFacingDescription;
    
    public BuyingAlertsSearchPreference(Integer accessGroupId,Integer maxDistanceFromDealer, 
            Integer maxVehicleMileage, Integer minVehicleMileage, String customerFacingDescription) {
        this.accessGroupId = accessGroupId;
        this.maxDistanceFromDealer = maxDistanceFromDealer;
        this.maxVehicleMileage = maxVehicleMileage;
        this.minVehicleMileage = minVehicleMileage;
        this.customerFacingDescription = customerFacingDescription;
    }
    
    public Integer getAccessGroupId() {
        return accessGroupId;
    }
    public void setAccessGroupId(Integer accessGroupId) {
        this.accessGroupId = accessGroupId;
    }
    public Integer getMaxDistanceFromDealer() {
        return maxDistanceFromDealer;
    }
    public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
        this.maxDistanceFromDealer = maxDistanceFromDealer;
    }
    public Integer getMaxVehicleMileage() {
        return maxVehicleMileage;
    }
    public void setMaxVehicleMileage(Integer maxVehicleMileage) {
        this.maxVehicleMileage = maxVehicleMileage;
    }
    public Integer getMinVehicleMileage() {
        return minVehicleMileage;
    }
    public void setMinVehicleMileage(Integer minVehicleMileage) {
        this.minVehicleMileage = minVehicleMileage;
    }

    public String getCustomerFacingDescription() {
        return customerFacingDescription;
    }

    public void setCustomerFacingDescription(String customerFacingDescription) {
        this.customerFacingDescription = customerFacingDescription;
    }
}
