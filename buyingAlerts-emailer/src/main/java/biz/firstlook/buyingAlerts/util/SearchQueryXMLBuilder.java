package biz.firstlook.buyingAlerts.util;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.SearchCandidate;

public class SearchQueryXMLBuilder {

    public String buildXML(BuyingAlertsSubscriber member, List<BuyingAlertsSearchPreference> prefs,
            List<SearchCandidate> candidates) {
        DocumentBuilder builder;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.newDocument();
            Element root = doc.createElement("Search");
            Element businessUnitElement = doc.createElement("BusinessUnit");
            businessUnitElement.setAttribute("ID", member.getBusinessUnitId().toString());

            if (prefs != null && !prefs.isEmpty()) {
                if (prefs.get(0).getMaxDistanceFromDealer() != null) {
                    businessUnitElement.setAttribute("MaxDistanceFromDealer",prefs.get(0).getMaxDistanceFromDealer().toString());
                }
                if (prefs.get(0).getMaxVehicleMileage() != null && prefs.get(0).getMaxVehicleMileage() != 0) {
                    businessUnitElement.setAttribute("MaxVehicleMileage", prefs.get(0).getMaxVehicleMileage().toString());
                }
                if (prefs.get(0).getMinVehicleMileage() != null && prefs.get(0).getMaxVehicleMileage() != 0) {
                    businessUnitElement.setAttribute("MinVehicleMileage", prefs.get(0).getMinVehicleMileage().toString());
                }
            } else {
                businessUnitElement.setAttribute("MaxDistanceFromDealer",member.getPurchasingDistance().toString());
            }
            root.appendChild(businessUnitElement);

            addAccessGroups(doc, root, prefs);
            addSearchCandidates(doc, root, candidates);

            StringWriter w = new StringWriter();
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(root), new StreamResult(w));
            
            return w.toString();
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private void addAccessGroups(Document doc, Element root, List<BuyingAlertsSearchPreference> prefs) {
        Element accessGroups = doc.createElement("AccessGroups");
        for(BuyingAlertsSearchPreference pref:prefs) {
            Element accessGroup = doc.createElement("AccessGroup");
            accessGroup.setAttribute("ID",pref.getAccessGroupId().toString());
            accessGroups.appendChild(accessGroup);
        }
        root.appendChild(accessGroups);
    }
    
    private void addSearchCandidates(Document doc, Element root, List<SearchCandidate> candidates) {
        Element filter = doc.createElement("Filter");
        for (SearchCandidate candidate:candidates) {
            Element model = doc.createElement("MakeModelGrouping");
            model.setAttribute("Make", candidate.getMake());
            model.setAttribute("Model", candidate.getLine());
            model.setAttribute("SegmentID", candidate.getSegmentId().toString());
            filter.appendChild(model);
            if (candidate.getYear() != null && candidate.getYear() != 0) {
                Element value = doc.createElement("Value");
                Element criteria = doc.createElement("Criteria");
                criteria.setAttribute("Type", "Year");
                value.appendChild(doc.createTextNode(candidate.getYear().toString()));
                criteria.appendChild(value);
                model.appendChild(criteria);
            }
        }
        root.appendChild(filter);
    }
    
}
