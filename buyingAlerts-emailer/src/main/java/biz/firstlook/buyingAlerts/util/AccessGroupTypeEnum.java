package biz.firstlook.buyingAlerts.util;

// See dbo.AccessGroupType in ATC
public enum AccessGroupTypeEnum {

    MANUFACTURER(1, "Manufacturer"), 
    ENTERPRISE(2, "Enterprise"), 
    ATC_OPEN(3,"ATC Open"), 
    GMAC(4, "GMAC"), 
    TFS(5, "TFS/LFS"), 
    IN_GROUP(6,"In Group"), 
    LIVE(7, "Live Auction"), 
    OVE(8, "OVE");

    private int accessGroupId;

    private String accessGroupTypeDescription;

    AccessGroupTypeEnum(int accessGroupId, String accessGroupTypeDescription) {
        this.accessGroupId = accessGroupId;
        this.accessGroupTypeDescription = accessGroupTypeDescription;
    }

    public int getAccessGroupId() {
        return accessGroupId;
    }

    public String getAccessGroupTypeDescription() {
        return accessGroupTypeDescription;
    }

}
