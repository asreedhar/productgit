package biz.firstlook.buyingAlerts.dao;

import java.sql.Date;
import java.util.List;

import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.DefaultMarket;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.PrecisionMatch;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;

public interface BuyingAlertsDao {
    public List<Integer> getSubscriptionIds(Integer count, Boolean failRetry);
    public List<BuyingAlertsSubscriber> getBuyingAlertSubscribers(String inClause);
    public List<BuyingAlertsSearchPreference> getBuyingAlertsSubscriberSearchPrefs(
            int memberId, int businessUnitId);
    public List<DefaultMarket> getDefaultLocalMarkets(int businessUnitId);
    public List<DefaultMarket> getDefaultInGroupMarkets();
    public List<DefaultMarket> getDefaultOpenMarkets(String inClause);
    public List<SearchCandidate> getSearchCandidates(int memberId,
            int businessUnitId);
    public List<SearchCandidate> getDefaultSearchCandidates(int businessUnitId);
    public List<LiveAuction> getLiveAuctions(String searchQuery,
            Date searchDate, Date endDate);
    public List<AuctionVehicle> getOnlineVehicles(String searchQuery,
            Date searchDate);
    public List<PrecisionMatch>  getPrecisionMatches(String searchQuery,
            Date searchDate);
    public List<SearchCandidateContribution> getSearchCandidateContributionList(
            Integer businessUnitId, String inClause);
    public void updateRunList(int statusId, Date now, int subscriptionId);

}
