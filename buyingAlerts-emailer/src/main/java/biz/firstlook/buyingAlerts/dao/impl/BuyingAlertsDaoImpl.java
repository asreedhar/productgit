package biz.firstlook.buyingAlerts.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.buyingAlerts.dao.BuyingAlertsDao;
import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.DefaultMarket;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.PrecisionMatch;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;
import biz.firstlook.buyingAlerts.util.AccessGroupTypeEnum;

@SuppressWarnings("unchecked")
public class BuyingAlertsDaoImpl extends JdbcDaoSupport implements BuyingAlertsDao{

    private SubscriptionIdsRowMapper subscriptionsRunListRowMapper = new SubscriptionIdsRowMapper();
    private BuyingAlertsSubscriberRowMapper buyingAlertsSubscriberRowMapper = new BuyingAlertsSubscriberRowMapper();
    private DefaultSearchPrefsRowMapper defaultSearchPrefsRowMapper = new DefaultSearchPrefsRowMapper();
    private SearchCandidateContributionRowMapper searchCandidateContributionRowMapper = new SearchCandidateContributionRowMapper();
    private BuyingAlertsSearchPrefsRowMapper buyingAlertsSearchPrefsRowMapper = new BuyingAlertsSearchPrefsRowMapper();
    private SearchCandidateRowMapper searchCandidateRowMapper = new SearchCandidateRowMapper();
    private LiveAuctionRowMapper liveAuctionRowMapper = new LiveAuctionRowMapper();
    private OnlineAuctionRowMapper onlineAuctionRowMapper = new OnlineAuctionRowMapper();
    private PrecisionMatchRowMapper precisionMatchRowMapper = new PrecisionMatchRowMapper();
    
    private static final String getBuyingAlertsSubscriberQuery;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append( "SELECT bu.BusinessUnitShortName, m.FirstName, m.LastName, m.EmailAddress, s.SubscriptionId, " );
        queryBuilder.append( "bu.BusinessUnitId, m.MemberId, dp.PurchasingDistanceFromDealer, s.SubscriptionDeliveryTypeId, " );
        queryBuilder.append( "dp.LiveAuctionDistanceFromDealer, dp.ATCEnabled, dp.TFSEnabled, dp.GMACEnabled, p.BusinessUnitShortName as groupName " );
        queryBuilder.append( "FROM Subscriptions s " );
        queryBuilder.append( "JOIN Member m ON m.MemberId = s.SubscriberId " );
        queryBuilder.append( "JOIN MemberAccess ma ON ma.MemberId = m.MemberId " );
        queryBuilder.append( "JOIN BusinessUnit bu ON bu.BusinessUnitId = ma.BusinessUnitId " );
        queryBuilder.append( "JOIN BusinessUnitRelationship bur ON bur.BusinessUnitId = bu.BusinessUnitId " );
        queryBuilder.append( "JOIN BusinessUnit p ON p.BusinessUnitId = bur.ParentId " );
        queryBuilder.append( "JOIN DealerPreference dp ON dp.BusinessUnitId = bu.BusinessUnitId " );
        getBuyingAlertsSubscriberQuery = queryBuilder.toString();
    }
    private class BuyingAlertsSubscriberRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            BuyingAlertsSubscriber member = new BuyingAlertsSubscriber(
                    rs.getString("BusinessUnitShortName"), rs.getString("FirstName"), 
                    rs.getString("LastName"), rs.getString("EmailAddress"), rs.getInt("SubscriptionDeliveryTypeId"),
                    rs.getInt("BusinessUnitId"), rs.getInt("memberId"),
                    rs.getInt("PurchasingDistanceFromDealer"), rs.getInt("LiveAuctionDistanceFromDealer"), rs.getInt("SubscriptionId"),
                    rs.getBoolean("ATCEnabled"),rs.getBoolean("TFSEnabled"),rs.getBoolean("GMACEnabled"),
                    rs.getString("groupName"));
            return member;
        }
    }
    private static final int BUYING_ALERTS = 1; //See dbo.SubscriptionTypes
    private static final String getBuyingAlertsSubscriberPrefsQuery;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append( "SELECT MAG.AccessGroupId,MAG.MaxDistanceFromDealer, MAG.MaxVehicleMileage,  " );
        queryBuilder.append( "MAG.MinVehicleMileage, MAG.Position, AG.CustomerFacingDescription " );
        queryBuilder.append( "FROM tbl_MemberATCAccessGroupList MAGL " );
        queryBuilder.append( "JOIN tbl_MemberATCAccessGroup  MAG ON MAG.MemberATCAccessGroupListId = MAGL.MemberATCAccessGroupListId " );
        queryBuilder.append( "JOIN AccessGroup AG ON AG.AccessGroupId = MAG.AccessGroupId " );
        queryBuilder.append( "WHERE MAGL.MemberId = ? " );
        queryBuilder.append( "AND MAGL.BusinessUnitId = ? " );
        queryBuilder.append( "AND MAGL.MemberATCAccessGroupListTypeId =  " ).append(BUYING_ALERTS);
        queryBuilder.append( " ORDER BY MAG.AccessGroupId ");
        getBuyingAlertsSubscriberPrefsQuery = queryBuilder.toString();
    }
    private class BuyingAlertsSearchPrefsRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            BuyingAlertsSearchPreference searchPrefs = new BuyingAlertsSearchPreference(
                    rs.getInt("AccessGroupId"), rs.getInt("MaxDistanceFromDealer"), 
                    rs.getInt("MaxVehicleMileage"), rs.getInt("MinVehicleMileage"),
                    rs.getString("CustomerFacingDescription"));
            return searchPrefs;
        }
    }
    
    private static final String getDefaultOpenMarkets;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT AccessGroupID, CustomerFacingDescription ");
        queryBuilder.append("FROM AccessGroup ");
        queryBuilder.append("WHERE Public_Group = 1 ");
        getDefaultOpenMarkets = queryBuilder.toString();
    }
    private static final String getDefaultInGroupAndLiveMarkets;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT AccessGroupID, CustomerFacingDescription ");
        queryBuilder.append("FROM AccessGroup ");
        queryBuilder.append("WHERE accessGroupTypeId =  ").append(AccessGroupTypeEnum.IN_GROUP.getAccessGroupId());
        queryBuilder.append(" OR accessGroupTypeId = ").append(AccessGroupTypeEnum.LIVE.getAccessGroupId());
        getDefaultInGroupAndLiveMarkets = queryBuilder.toString();
    }
    private static final String getDefaultLocalMarkets;
    static {
      StringBuilder queryBuilder = new StringBuilder();
      queryBuilder.append("SELECT l.AccessGroupId, ag.CustomerFacingDescription ");
      queryBuilder.append("FROM tbl_DealerATCAccessGroups  l ");
      queryBuilder.append("JOIN AccessGroup ag ON ag.AccessGroupId = l.AccessGroupId ");
      queryBuilder.append("WHERE l.businessUnitID = ? ");
      getDefaultLocalMarkets = queryBuilder.toString();
    }
    private class DefaultSearchPrefsRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            DefaultMarket defaultPrefs = new DefaultMarket(
                    rs.getInt("AccessGroupId"), rs.getString("CustomerFacingDescription"));
            return defaultPrefs;
        }
    }
    
    
    private static final String getSearchCandidatesQuery;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append( "SELECT SC.Make, SC.Line, SC.SegmentId, SCO.ModelYear, SC.SearchCandidateListId " );
        queryBuilder.append( "FROM SearchCandidateList SCL " );
        queryBuilder.append( "JOIN SearchCandidate SC ON SC.SearchCandidateListId = SCL.SearchCandidateListId " );
        queryBuilder.append( "JOIN SearchCandidateOption SCO ON SCO.SearchCandidateId = SC.SearchCandidateId " );
        queryBuilder.append( "WHERE SCL.SearchCandidateListTypeId = 2 " );
        queryBuilder.append( "AND SCL.memberId = ? " );
        queryBuilder.append( "AND SCL.businessUnitId = ? " );
        getSearchCandidatesQuery = queryBuilder.toString();
    }
    private static final String getDefaultSearchCandidatesQuery;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append( "SELECT M.Make AS Make, M.Model AS Line, M.SegmentID SegmentId, D.ModelYear ModelYear  " );
        queryBuilder.append( "FROM dbo.GetDefaultBuyingPlanForSearchAndAquisition(?) D  " );
        queryBuilder.append( "JOIN MakeModelGrouping M ON M.GroupingDescriptionID = D.GroupingDescriptionId  " );
        queryBuilder.append( "JOIN dbo.tbl_GroupingDescription G ON G.GroupingDescriptionID = D.GroupingDescriptionId  " );
        queryBuilder.append( "GROUP BY M.Make, M.Model, M.SegmentID, D.ModelYear" );
        getDefaultSearchCandidatesQuery = queryBuilder.toString();
    }
    private class SearchCandidateRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            SearchCandidate candidate = new SearchCandidate(
                    rs.getString("Make"), rs.getString("Line"), 
                    rs.getInt("SegmentId"), rs.getInt("ModelYear"));
            return candidate;
        }
    }

    private static final String getLiveAuctionsProc;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append( "EXEC GetPurchasingCenterAuctionsByMakeModelGroupingXML " );
        queryBuilder.append( "?, ");
        queryBuilder.append( "?, " );
        queryBuilder.append( "?, " );
        queryBuilder.append( "?, " );
        queryBuilder.append( "?  " );
        getLiveAuctionsProc = queryBuilder.toString();
    }
    private class LiveAuctionRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            LiveAuction live = new LiveAuction(rs.getString("Title"), 
                    rs.getString("Auction"),rs.getString("Location"), 
                    rs.getDate("DateHeld"), rs.getInt("VehiclesMatched"));
            return live;
        }
    }
    
    private static final String getOnlineAuctionsProc;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append( "EXEC GetPurchasingCenterVehiclesByMakeModelGroupingXML " );
        queryBuilder.append( "?, ");
        queryBuilder.append( "?, " );
        queryBuilder.append( "?, " );
        queryBuilder.append( "? " );
        getOnlineAuctionsProc = queryBuilder.toString();
    }
    private class OnlineAuctionRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            AuctionVehicle vehicle = new AuctionVehicle(rs.getInt("AccessGroupId"), 
                    rs.getString("CustomerFacingDescription"), rs.getInt("GroupingDescriptionId"), 
                    rs.getString("GroupingDescription"), rs.getString("DealerNickName"));
            return vehicle;
        }
    }
    private class PrecisionMatchRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            PrecisionMatch precisionMatch = new PrecisionMatch(
                    rs.getString("CustomerFacingDescription"), 
                    rs.getInt("PrecisionMatches"));
            return precisionMatch;
        }
    }
    
    private static final String getSearchCandidateContributionListQuery;
    static {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT DISTINCT ");
        queryBuilder.append("g.GroupingDescriptionID, g.GroupingDescription, ISNULL(C.Contribution,0.0) AS Contribution ");
        queryBuilder.append("FROM dbo.GroupingDescription G ");
        queryBuilder.append("JOIN dbo.MakeModelGrouping MMG ON G.GroupingDescriptionID = MMG.GroupingDescriptionID ");
        queryBuilder.append("JOIN dbo.Segment S ON S.SegmentID = MMG.SegmentID ");
        queryBuilder.append("LEFT JOIN (    SELECT  A1.BusinessUnitID, A1.GroupingDescriptionID, SUM(A1.TotalGrossMargin) / TotalFrontEndGross Contribution ");
        queryBuilder.append("FROM   [FLDW]..Sales_A1 A1 ");
        queryBuilder.append("JOIN ( SELECT  BusinessUnitID, SUM(TotalGrossMargin) TotalFrontEndGross ");
        queryBuilder.append("FROM   [FLDW]..Sales_A1 A ");
        queryBuilder.append("WHERE  InventoryTypeID = 2 ");
        queryBuilder.append("AND SaleTypeID = 1 ");
        queryBuilder.append("AND BusinessUnitID = ? ");
        queryBuilder.append("AND PeriodID = 5 ");
        queryBuilder.append("GROUP BY BusinessUnitID ) A2 ON A1.BusinessUnitID = A2.BusinessUnitID  ");
        queryBuilder.append("WHERE  A1.InventoryTypeID = 2 ");
        queryBuilder.append("AND A1.SaleTypeID = 1 ");
        queryBuilder.append("AND A1.BusinessUnitID = ? ");
        queryBuilder.append("AND A1.PeriodID = 5 ");
        queryBuilder.append("GROUP BY ");
        queryBuilder.append("A1.BusinessUnitID, A1.GroupingDescriptionID, A2.TotalFrontEndGross )C ON G.GroupingDescriptionID = C.GroupingDescriptionID ");
        getSearchCandidateContributionListQuery = queryBuilder.toString();
    }
    private class SearchCandidateContributionRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            SearchCandidateContribution scc = new SearchCandidateContribution(
                    rs.getInt("GroupingDescriptionID"), rs.getString("GroupingDescription"),
                    rs.getDouble("Contribution"));
            return scc;
        }
    }
    
    private static final String updateRunListQuery;
    static{
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("UPDATE BuyingAlertsRunList SET BuyingAlertsStatusId = ?, ");
        queryBuilder.append("SendDate = ? ");
        queryBuilder.append("WHERE SubscriptionId = ? ");
        updateRunListQuery= queryBuilder.toString();
    }
    
    public List<Integer> getSubscriptionIds(Integer count, Boolean failedRetry){
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        if (count != null){
            query.append("TOP ").append(count.toString());
        }
        query.append(" SubscriptionId FROM BuyingAlertsRunList " );
        query.append("WHERE BuyingAlertsStatusId <> 2");
        if (failedRetry != null && failedRetry == Boolean.TRUE){
            query.append("AND BuyingAlertsStatusId = 3");
        }
        List<Integer> subscriptions = getJdbcTemplate().query(
                query.toString(), new Object[]{},
                subscriptionsRunListRowMapper);
        return subscriptions;
    }
    private class SubscriptionIdsRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return rs.getInt("SubscriptionId");
        }
    }
    
    public List<BuyingAlertsSubscriber> getBuyingAlertSubscribers(String inClause) {
        StringBuilder query = new StringBuilder();
        query.append(getBuyingAlertsSubscriberQuery);
        query.append(inClause);
        query.append(" ORDER BY m.MemberId");
        List<BuyingAlertsSubscriber> subscriber = getJdbcTemplate().query(
                query.toString(), new Object[] {} ,
                buyingAlertsSubscriberRowMapper);
        return subscriber;
    }
    
    
    public List<BuyingAlertsSearchPreference> getBuyingAlertsSubscriberSearchPrefs(int memberId, int businessUnitId) {
        List<BuyingAlertsSearchPreference> candidates = getJdbcTemplate().query(
                getBuyingAlertsSubscriberPrefsQuery, new Object[] { memberId, businessUnitId },
                buyingAlertsSearchPrefsRowMapper);
        return candidates;
    }
    
    public List<DefaultMarket> getDefaultLocalMarkets(int businessUnitId){
        List<DefaultMarket> candidates = getJdbcTemplate().query(
                getDefaultLocalMarkets, new Object[] { businessUnitId },
                defaultSearchPrefsRowMapper);
        return candidates;
    }
    
    public List<DefaultMarket> getDefaultInGroupMarkets(){
        List<DefaultMarket> candidates = getJdbcTemplate().query(
                getDefaultInGroupAndLiveMarkets, new Object[] {},
                defaultSearchPrefsRowMapper);
        return candidates;
    }
    
    public List<DefaultMarket> getDefaultOpenMarkets(String inClause){
        StringBuilder query = new StringBuilder();
        query.append(getDefaultOpenMarkets);
        query.append(inClause);
        List<DefaultMarket> candidates = getJdbcTemplate().query(
                query.toString(), new Object[] {},
                defaultSearchPrefsRowMapper);
        return candidates;
    }
    
    public List<SearchCandidate> getSearchCandidates(int memberId,int businessUnitId) {
        List<SearchCandidate> candidates = getJdbcTemplate().query(
                getSearchCandidatesQuery, new Object[] { memberId, businessUnitId },
                searchCandidateRowMapper);
        return candidates;
    }
    public List<SearchCandidate> getDefaultSearchCandidates(int businessUnitId) {
        List<SearchCandidate> candidates = getJdbcTemplate().query(
                getDefaultSearchCandidatesQuery, new Object[] { businessUnitId },
                searchCandidateRowMapper);
        return candidates;
    }
    
    public List<LiveAuction> getLiveAuctions(String searchQuery,
            Date searchDate, Date endDate) {
        List<LiveAuction> liveAuctions = getJdbcTemplate().query(
                getLiveAuctionsProc, new Object[] { searchQuery, 0, searchDate, endDate, 0 },
                liveAuctionRowMapper);
        return liveAuctions;
    }

    public List<AuctionVehicle> getOnlineVehicles(String searchQuery,
            Date searchDate) {
        List<AuctionVehicle> onlineAuctionVehicles = getJdbcTemplate().query(
                getOnlineAuctionsProc, new Object[] { searchQuery, searchDate, 0, 0 },
                onlineAuctionRowMapper);
        return onlineAuctionVehicles;
    }
    
    public List<PrecisionMatch>  getPrecisionMatches(String searchQuery,
            Date searchDate) {
        List<PrecisionMatch> precisionMatches = getJdbcTemplate().query(
                getOnlineAuctionsProc, new Object[] { searchQuery, searchDate, 1, 0 },
                precisionMatchRowMapper);
        return precisionMatches;
    }

    public List<SearchCandidateContribution> getSearchCandidateContributionList(
            Integer businessUnitId, String inClause) {
        StringBuilder query = new StringBuilder();
        query.append(getSearchCandidateContributionListQuery);
        query.append(inClause);
        List<SearchCandidateContribution> contributionList = getJdbcTemplate().query(
                query.toString(), new Object[] { businessUnitId, businessUnitId },
                searchCandidateContributionRowMapper);
        return contributionList;
    }

    public void updateRunList(int statusId, Date now, int subscriptionId) {
        getJdbcTemplate().update(updateRunListQuery, new Object[]{statusId, now, subscriptionId});
    }
    
}
