package biz.firstlook.buyingAlerts.service.impl;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.buyingAlerts.dao.BuyingAlertsDao;
import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.CandidateGrouping;
import biz.firstlook.buyingAlerts.model.DefaultMarket;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.OnlineMarket;
import biz.firstlook.buyingAlerts.model.PrecisionMatch;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;
import biz.firstlook.buyingAlerts.service.BuyingAlertsService;
import biz.firstlook.buyingAlerts.util.AccessGroupTypeEnum;
import biz.firstlook.buyingAlerts.util.SearchQueryXMLBuilder;

public class BuyingAlertsServiceImpl implements BuyingAlertsService
{
    private BuyingAlertsDao buyingAlertsDao;
    private SearchQueryXMLBuilder xmlBuilder;

    
    public List<BuyingAlertsSubscriber> getBuyingAlertSubscribers(Integer count, Boolean failRetry){
        String inClause = createInClause(count,failRetry);
        if (inClause == null) {
            return null;
        }
        return buyingAlertsDao.getBuyingAlertSubscribers(inClause);
    }
    
    private String createInClause(Integer count, Boolean failRetry){
        List<Integer> subscriptionIds = buyingAlertsDao.getSubscriptionIds(count,failRetry);
        if (subscriptionIds == null || subscriptionIds.isEmpty()) {
            return null;
        }
        String idString = subscriptionIds.toString();
        StringBuilder inClause = new StringBuilder();
        inClause.append("WHERE s.SubscriptionId in ");
        idString = idString.replace('[','(');
        idString = idString.replace(']',')');
        inClause.append(idString);
        return inClause.toString();
    }
   
    public List<BuyingAlertsSearchPreference> retrievePreferences(BuyingAlertsSubscriber member) {
        List<BuyingAlertsSearchPreference> prefs = buyingAlertsDao.getBuyingAlertsSubscriberSearchPrefs(member.getMemberId(), member.getBusinessUnitId());
        if (prefs == null || prefs.isEmpty()) {
            prefs = new ArrayList<BuyingAlertsSearchPreference>();
            List<DefaultMarket> buyingAlertSearchPrefs = new ArrayList<DefaultMarket>();
            buyingAlertSearchPrefs.addAll(buyingAlertsDao.getDefaultLocalMarkets(member.getBusinessUnitId()));
            buyingAlertSearchPrefs.addAll(buyingAlertsDao.getDefaultInGroupMarkets());
            StringBuilder inClause = new StringBuilder();
            inClause.append("AND accessGroupTypeId IN (  ").append(AccessGroupTypeEnum.OVE.getAccessGroupId());//OVE for all!
            if (member.isAtcEnabled()){
                inClause.append(",").append(AccessGroupTypeEnum.ATC_OPEN.getAccessGroupId());
            }
            if (member.isGmacEnabled()){
                inClause.append(",").append(AccessGroupTypeEnum.GMAC.getAccessGroupId());
            }
            if (member.isTfsEnabled()){
                inClause.append(",").append(AccessGroupTypeEnum.TFS.getAccessGroupId());
            }
            inClause.append(")");
            buyingAlertSearchPrefs.addAll(buyingAlertsDao.getDefaultOpenMarkets(inClause.toString()));
            for (DefaultMarket pref:buyingAlertSearchPrefs) {
                BuyingAlertsSearchPreference defaultPref = new BuyingAlertsSearchPreference(pref.getAccessGroupId(), 
                        member.getPurchasingDistance(), null, null, pref.getAccessGroup());
                prefs.add(defaultPref);
            }
        }
        return prefs;
    }
    
    public List<SearchCandidate> retrieveCandidates(BuyingAlertsSubscriber member) {
        List<SearchCandidate> candidates = buyingAlertsDao.getSearchCandidates(member.getMemberId(),member.getBusinessUnitId());
        if (candidates == null || candidates.isEmpty()) {
            candidates = buyingAlertsDao.getDefaultSearchCandidates(member.getBusinessUnitId());
        }
        return candidates;
    }
    
    public String buildXMLQuery(BuyingAlertsSubscriber member, List<BuyingAlertsSearchPreference> prefs, List<SearchCandidate> candidates){
        return xmlBuilder.buildXML(member, prefs, candidates);
    }
    
    public List<LiveAuction> getLiveAuctions(String searchQuery,Date startDate,Date endDate){
        return  buyingAlertsDao.getLiveAuctions(searchQuery, startDate, endDate);
    }
    public List<AuctionVehicle> getOnlineVehicles(String searchQuery, Date startDate){
        return buyingAlertsDao.getOnlineVehicles(searchQuery, startDate);
    }
    
    public Map<String, Object> getPrecisionMatches(String searchQuery, Date startDate){
        Map<String, Object> precisionMatches = new HashMap<String, Object>();
        List<PrecisionMatch> matches= buyingAlertsDao.getPrecisionMatches(searchQuery, startDate);
        for (PrecisionMatch match:matches){
            precisionMatches.put(match.getCustomerFacingDescription(), match.getPrecisionMatches());
        }
        return precisionMatches;
    }
    
    public List<SearchCandidateContribution> retriveCandidateContribution(
            BuyingAlertsSubscriber member, List<AuctionVehicle> vehicles) {
        //if there are no vehicles then just return before doing query
    	if(vehicles == null || vehicles.isEmpty()){
        	return null;
        }
        StringBuilder inClause = new StringBuilder();
        inClause.append("WHERE G.GroupingDescriptionId IN ( ");
        int i=0;
        for(AuctionVehicle vehicle:vehicles){
            if (i != 0) {inClause.append(", ");}
            inClause.append(vehicle.getGroupingDescriptionId());
            i++;
        }
        inClause.append(") ");
        
        List<SearchCandidateContribution> scc = buyingAlertsDao.getSearchCandidateContributionList(member.getBusinessUnitId(), inClause.toString());
        return scc;
    }
    
    @SuppressWarnings("unchecked")
    public List<OnlineMarket> retrieveGroupings(List<String> accessGroups,List<AuctionVehicle> vehicles, 
            List<SearchCandidateContribution> scc, Map<String, Object> precisionMatches) {
        Map<String, List<AuctionVehicle>> groupedResults = new HashMap<String, List<AuctionVehicle>>();
        List<String> keySet = new ArrayList<String>();
        for (AuctionVehicle vehicle:vehicles) {
            for (String accessGroup:accessGroups){
                if (accessGroup.equals(vehicle.getCustomerFacingDescription())) {
                    if (groupedResults.containsKey(accessGroup)){
                        List<AuctionVehicle> vehs = groupedResults.get(accessGroup);
                        vehs.add(vehicle);
                        groupedResults.put(accessGroup, vehs);
                    } else {
                        List<AuctionVehicle> vehs = new ArrayList<AuctionVehicle>();
                        vehs.add(vehicle);
                        groupedResults.put(accessGroup, vehs);
                        keySet.add(accessGroup);
                    }
                }
            }
        }
        List<OnlineMarket> onlineResults = new ArrayList<OnlineMarket>();
        for(String key:keySet){
            HashMap<String, Object> candidateVehicles = new HashMap<String, Object>();
            List<AuctionVehicle> auctionVehicles = groupedResults.get(key);
            for(SearchCandidateContribution cont:scc) {
                for(AuctionVehicle vehicle:auctionVehicles){
                    if (vehicle.getGroupingDescriptionId().equals(cont.getGroupingDescriptionId())) {
                        if (candidateVehicles.containsKey(vehicle.getGroupingDescription())){
                            CandidateGrouping grouping = (CandidateGrouping) candidateVehicles.get(vehicle.getGroupingDescription());
                            grouping.increment();
                            candidateVehicles.put(vehicle.getGroupingDescription(), grouping);
                        } else {
                            CandidateGrouping grouping = new CandidateGrouping(cont.getContribution(), cont.getGroupingDescription());
                            candidateVehicles.put(vehicle.getGroupingDescription(), grouping);
                        }
                        
                    }
                }
            }
            List<CandidateGrouping> candidateGroupings = new ArrayList<CandidateGrouping>();
            Set groupingKeys = candidateVehicles.keySet();
            Iterator itr = groupingKeys.iterator();
            Integer buyingOpps = 0;
            while(itr.hasNext()) {
                String candKey = (String) itr.next();
                CandidateGrouping cg = (CandidateGrouping) candidateVehicles.get(candKey);
                buyingOpps += cg.getCount();
                candidateGroupings.add(cg);
            }
            Collections.sort(candidateGroupings,new BeanComparator("contribution"));
            Integer buyingOps = 0;
            Collections.reverse(candidateGroupings);
            if (precisionMatches.get(key)!= null){
                buyingOps = (Integer)precisionMatches.get(key);
            }
            
            OnlineMarket onlineMarket = new OnlineMarket(buyingOps, candidateGroupings, key);
            onlineResults.add(onlineMarket);
        }
        Collections.sort(onlineResults, new BeanComparator("market"));
        return onlineResults;
    }
    
    public Map<String, Object> prepareEmailContext(BuyingAlertsSubscriber subscriber, 
            List<LiveAuction> liveAuctions, List<OnlineMarket> onlineAuctions) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm a zzzz");
        
        Map< String, Object > emailContext = new HashMap< String, Object >();
        emailContext.put("businessUnitName", subscriber.getBusinessUnitName());
        emailContext.put("groupName", subscriber.getGroupName());
        emailContext.put("maxDistanceOnline", subscriber.getPurchasingDistance());
        emailContext.put("onlineResults", onlineAuctions);
        emailContext.put("liveAuctions", liveAuctions);
        emailContext.put("hasLiveAuctions", (liveAuctions.size() == 0 ? false:true));
        emailContext.put("hasOnlineAuctions", (onlineAuctions.size() == 0 ? false:true));
        emailContext.put("currentDate", new Date(System.currentTimeMillis()));
        emailContext.put("currentDateTime", sdf.format(new Date(System.currentTimeMillis())));
        boolean hasInGroupMarket = false;
        for(OnlineMarket market:onlineAuctions){
            if (market.getMarket().equalsIgnoreCase("IN GROUP")){
                hasInGroupMarket = true;
                break;
            }
        }
        emailContext.put("hasInGroupAuctions", hasInGroupMarket);
        return emailContext;
    }
    
    private static final int SUCCESS = 2; //See dbo.BuyingAlertsStatus
    private static final int FAILURE = 3;
    
    public void updateRunList(boolean success, int subscriberId) {
        if (success) {
            Date now = new Date(System.currentTimeMillis());
            buyingAlertsDao.updateRunList(SUCCESS, now, subscriberId);
        } else {
            buyingAlertsDao.updateRunList(FAILURE, null, subscriberId);
        }
    }
    
    public void setBuyingAlertsDao(BuyingAlertsDao buyingAlertsDao) {
        this.buyingAlertsDao = buyingAlertsDao;
    }

    public void setXmlBuilder(SearchQueryXMLBuilder xmlBuilder) {
        this.xmlBuilder = xmlBuilder;
    }


}
