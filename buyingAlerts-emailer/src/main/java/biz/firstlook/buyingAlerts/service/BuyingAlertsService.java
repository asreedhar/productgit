package biz.firstlook.buyingAlerts.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.OnlineMarket;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;

public interface BuyingAlertsService {

    public List<BuyingAlertsSubscriber> getBuyingAlertSubscribers(Integer count, Boolean failRetry);
    public List<BuyingAlertsSearchPreference> retrievePreferences(
            BuyingAlertsSubscriber member);
    public List<SearchCandidate> retrieveCandidates(
            BuyingAlertsSubscriber member);
    public String buildXMLQuery(BuyingAlertsSubscriber member,
            List<BuyingAlertsSearchPreference> prefs,
            List<SearchCandidate> candidates);
    public List<LiveAuction> getLiveAuctions(String searchQuery,
            Date startDate, Date endDate);
    public List<AuctionVehicle> getOnlineVehicles(String searchQuery,
            Date startDate);
    public Map<String, Object> getPrecisionMatches(String searchQuery, Date startDate);
    public List<SearchCandidateContribution> retriveCandidateContribution(
            BuyingAlertsSubscriber member, List<AuctionVehicle> vehicles);
    public List<OnlineMarket> retrieveGroupings(List<String> accessGroups,
            List<AuctionVehicle> vehicles, List<SearchCandidateContribution> scc,
            Map<String, Object> precisionMatches);
    public Map<String, Object> prepareEmailContext(BuyingAlertsSubscriber subscriber, 
            List<LiveAuction> liveAuctions, List<OnlineMarket> onlineAuctions);
    public void updateRunList(boolean success, int subscriberId);
    
}
