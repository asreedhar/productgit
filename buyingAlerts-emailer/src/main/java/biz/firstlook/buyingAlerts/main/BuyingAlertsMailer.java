package biz.firstlook.buyingAlerts.main;

import jargs.gnu.CmdLineParser;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.OnlineMarket;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;
import biz.firstlook.buyingAlerts.service.BuyingAlertsService;
import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.email.EmailService.EmailFormat;

public class BuyingAlertsMailer implements Runnable, InitializingBean {
    private static final int HTML_DELIVERY_TYPE = 1; //See dbo.SubscriptionDeliveryTypes

    private EmailService emailService;
    private BuyingAlertsService buyingAlertsService;
    private static Boolean resendFailures = false;
    private static Integer numberOfEmails = null;
    private static final Log log = LogFactory.getLog(BuyingAlertsMailer.class);
    
    public static void main(String[] args) {
        AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                new String[] { "applicationContext.xml","applicationContext-dataSource.xml" });
        BuyingAlertsMailer mailer = (BuyingAlertsMailer) applicationContext.getBean("buyingAlertsMailer");

        parseCommandLineArgs(args);
        log.info("Starting Buying Alerts Mailer");
        mailer.run();
        log.info("Shutting down...");
        applicationContext.destroy();
        log.info("Finished.");
    }

    public void run() {
        Calendar c = Calendar.getInstance();
        Date startDate = new Date(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 7 * 2);
        Date endDate = new Date(c.getTimeInMillis());
        try {
            log.info("Retrieving Subscribers...");
            List<BuyingAlertsSubscriber> members = buyingAlertsService.getBuyingAlertSubscribers(
                    numberOfEmails, resendFailures);
            if (members == null || members.size() == 0) {
                log.warn("Nothing in run list waiting to be sent");
                return;
            }

            for (BuyingAlertsSubscriber member : members) {
                try {
                    log.info("Retrieving Search Preferences for member: "+ member.getMemberId());
                    List<BuyingAlertsSearchPreference> prefs = buyingAlertsService.retrievePreferences(member);

                    log.info("Retrieving Candidate list...");
                    List<SearchCandidate> candidates = buyingAlertsService.retrieveCandidates(member);

                    log.info("Building XML Query...");
                    String searchQuery = buyingAlertsService.buildXMLQuery(member, prefs, candidates);

                    long start = System.currentTimeMillis();
                    log.info("Retrieving Live Auctions...");
                    List<LiveAuction> liveAuctions = buyingAlertsService.getLiveAuctions(searchQuery, startDate, endDate);

                    log.info("Retrieving Online Vehicles...");
                    List<AuctionVehicle> vehicles = buyingAlertsService.getOnlineVehicles(searchQuery, startDate);
                    Map<String, Object> precisionMatches = buyingAlertsService.getPrecisionMatches(searchQuery, startDate);
                    System.out.println(System.currentTimeMillis() - start);

                    log.info("Retrieving Candidate Contribution...");
                    List<SearchCandidateContribution> scc = buyingAlertsService.retriveCandidateContribution(member, vehicles);

                    List<String> accessGroups = new ArrayList<String>();
                    for (BuyingAlertsSearchPreference pref : prefs) {
                        if (!accessGroups.contains(pref.getCustomerFacingDescription())) {
                            accessGroups.add(pref.getCustomerFacingDescription());
                        }
                    }

                    log.info("Retrieving Online Markets...");
                    List<OnlineMarket> onlineResults = buyingAlertsService.retrieveGroupings(accessGroups, vehicles, scc,precisionMatches);

                    if(!onlineResults.isEmpty() || !liveAuctions.isEmpty()){
	                    // log.info("Preparing email...");
	                    Map<String, Object> emailContext = buyingAlertsService.prepareEmailContext(member, liveAuctions,onlineResults);
	
	                    log.info("Sending email via EmailService");
	                    boolean success =  emailService.sendSynchEmail(member.getEmail(), Collections.EMPTY_LIST,
	                                    emailContext, "buyingAlert","First Look Buying Alerts",
	                                    "helpdesk@firstlook.biz", null,null,
	                                    (member.getDeliveryType() == HTML_DELIVERY_TYPE ? EmailFormat.HTML_MULTIPART:EmailFormat.PLAIN_TEXT));
	                    buyingAlertsService.updateRunList(success, member.getSubscriptionId());
                    }
                    else{
                    	log.info("Member does not have results for online and live auctions, not sending e-mail");
                    }
                } catch (NullPointerException e) {
                    log.error(e);
                }
            }
        } catch (DataAccessException e) {
            log.error(e);
        }
    }

    public static String getUsage() {
        StringBuffer usage = new StringBuffer();
        usage.append("\n\nInvalid options specified. \n");
        usage.append("Runs the buying alerts emailer.\n");
        usage.append("  -n, --numberOfEmails      the number of emails to send, defaults to all.\n");
        usage.append("  -r, --resendFailures      flag, runs the mailer only trying to send those with a status of failed.\n");
        usage.append("runBuyingAlertsMailer.bat -r -n 25           Send 25 failed emails from the run list\n");
        usage.append("runBuyingAlertsMailer.bat -n 50              Send 50 emails from the run list\n");
        usage.append("runBuyingAlertsMailer.bat -r                 Attempt to send all failed emails in the run list\n\n");
        return usage.toString();
    }

    private static void parseCommandLineArgs(String[] args) {
        CmdLineParser cmdLineParser = new CmdLineParser();
        CmdLineParser.Option numberOfEmailsO = cmdLineParser.addIntegerOption('n', "numberOfEmails");
        CmdLineParser.Option resendFailuresO = cmdLineParser.addBooleanOption('r', "resendFailures");
        try {
            cmdLineParser.parse(args);
        } catch (CmdLineParser.OptionException oe) {
            log.error("Illegal Argument: " + oe.getMessage());
            System.out.println(getUsage());
        }
        try {
            resendFailures = (Boolean) cmdLineParser.getOptionValue(resendFailuresO);
            numberOfEmails = (Integer) cmdLineParser.getOptionValue(numberOfEmailsO);
        } catch (Exception e) {
            log.error("Error parsing arguments");
            System.out.println(getUsage());
        }
    }

    public void afterPropertiesSet() throws Exception {
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setBuyingAlertsService(BuyingAlertsService buyingAlertsService) {
        this.buyingAlertsService = buyingAlertsService;
    }
}
