package biz.firstlook.buyingAlerts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.CandidateGrouping;
import biz.firstlook.buyingAlerts.model.OnlineMarket;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;

public class TestBuyingAlertsService extends AbstractBuyingAlertsTest{
    
//  Test the getBuyingAlertsSubscriber method.
    public void testGetBuyingAlertSubscribers(){
        List<BuyingAlertsSubscriber> subscribers = buyingAlertsService.getBuyingAlertSubscribers(null, null);
        assertEquals("There should be 1 subscriptions waiting to be sent.", 1, subscribers.size());
        
        subscribers = buyingAlertsService.getBuyingAlertSubscribers(2, null);
        assertEquals("There should be 2 subscriptions waiting to be sent.", 2, subscribers.size());
        
        subscribers = buyingAlertsService.getBuyingAlertSubscribers(null, true);
        assertEquals("There should be 0 failed subscriptions to resend.", null, subscribers );
    }
    

//  Test the retrivePreferences method.
    public void testRetrievePreferences(){
        List<BuyingAlertsSubscriber> subscribers = buyingAlertsService.getBuyingAlertSubscribers(null, null);
        
        BuyingAlertsSubscriber memberWithProfile = subscribers.get(0);
        List<BuyingAlertsSearchPreference> userPrefs = buyingAlertsService.retrievePreferences(memberWithProfile);
        List<Integer> marketIds = new ArrayList<Integer>();
        for (BuyingAlertsSearchPreference pref:userPrefs){
            marketIds.add(pref.getAccessGroupId());
            assertEquals( Integer.valueOf( 100000 ), pref.getMaxVehicleMileage());
            assertEquals(Integer.valueOf( 0 ), pref.getMinVehicleMileage());
            assertEquals("Max distance from dealer should be 1000", pref.getMaxDistanceFromDealer().equals(1000), true);
        }
        assertEquals("User has access to this inGroup market.", marketIds.contains(999), true);
        assertEquals("User has access to this Adessa market.", marketIds.contains(888), true);
        assertEquals("User has access to this OVE market.", marketIds.contains(777), true);
    }
    
    //Test that the xml is built properly.
    public void testBuildXMLQuery(){        
        BuyingAlertsSubscriber subscriber = new BuyingAlertsSubscriber(
                "BusinessUnitName", "TestFirstName", "LastName", "Email", 1,
                1234, 1234, 100, 200, 12345, true, true, true, "GroupName");
        List<SearchCandidate> searchCandidates = buyingAlertsService.retrieveCandidates(subscriber);
        
        List<BuyingAlertsSearchPreference> searchPrefs = buyingAlertsService.retrievePreferences(subscriber);
        BuyingAlertsSearchPreference searchPref = searchPrefs.get(0);
        assertNotNull("Buying Alerts Search Pref Null", searchPref);
        assertEquals("Buying Alerts Search Prefs AccessGroupID Not Correct ", searchPref.getAccessGroupId().intValue(), 999 );
        assertEquals("Buying Alerts Search Prefs MaxDistanceFromDealer Not Correct ", searchPref.getMaxDistanceFromDealer().intValue(), 1000 );
        assertEquals("Buying Alerts Search Prefs MaxVehicleMileage Not Correct ", searchPref.getMaxVehicleMileage().intValue(), 100000 );
        assertEquals("Buying Alerts Search Prefs MinVehicleMileage Not Correct ", searchPref.getMinVehicleMileage().intValue(), 0);
        assertTrue("Buying Alerts Search Prefs CustomerFacingDesc Not Correct ", searchPref.getCustomerFacingDescription().equals("In Group from profile.") );
     
        String expectedXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Search><BusinessUnit ID=\"1234\" MaxDistanceFromDealer=\"1000\" MaxVehicleMileage=\"100000\" MinVehicleMileage=\"0\"/><AccessGroups><AccessGroup ID=\"999\"/><AccessGroup ID=\"888\"/><AccessGroup ID=\"777\"/></AccessGroups><Filter><MakeModelGrouping SegmentID=\"1\" Make=\"make 1\" Model=\"line 1\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"1\" Make=\"make 2\" Model=\"line 2\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"1\" Make=\"make 3\" Model=\"line 3\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"1\" Make=\"make 4\" Model=\"line 4\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping></Filter></Search>";
        assertEquals("Built XML Not Correct: ", buyingAlertsService.buildXMLQuery(subscriber, searchPrefs, searchCandidates), expectedXML);
    }
    
    
    //  Test that the list of groupings (e.g. FORD TAURUS SEDAN) are sorted by contribution descending.
    public void testRetrieveGroupings() {        
        BuyingAlertsSubscriber member  = new BuyingAlertsSubscriber("Dave's Store","Jon","Smith","jon@email.com", 1,
                123456, 987654, 1000, 1000, 12345, true, false, false, "GroupName");
        Map<String, Object> precisionMatches = new HashMap<String, Object>();
        
        List<String> accessGroups = new ArrayList<String>();
        accessGroups.add("OVE");
        accessGroups.add("IN GROUP");
        accessGroups.add("ATC");
        
        List<AuctionVehicle> vehicles = buyingAlertsService.getOnlineVehicles("", null);
        List<SearchCandidateContribution> scc = buyingAlertsService.retriveCandidateContribution(member, vehicles);
        
        List<OnlineMarket> onlineResults = buyingAlertsService.retrieveGroupings(accessGroups, vehicles, scc, precisionMatches);
        
        for (OnlineMarket market:onlineResults){
            double prev = 1.0;
            for(CandidateGrouping grouping:market.getCandidateGroupings()){
                assertEquals("Not correctly sorted by contribution.", prev > grouping.getContribution(), true);
                prev = grouping.getContribution();
            }
        }
    }
    
    //Verify that the email context(map) is created with the correcty keys and that
    //those keys have some value(aren't null).
    @SuppressWarnings("unchecked")
    public void testEmailPrepare(){
        BuyingAlertsSubscriber subscriber = new BuyingAlertsSubscriber(
                "BusinessUnitName", "TestFirstName", "LastName", "Email", 1,
                1234, 1234, 100, 200, 12345, true, true, true, "GroupName");
        List liveAuctions = new ArrayList();
        liveAuctions.add(new Object());
        List onlineAuctions = new ArrayList();
        onlineAuctions.add(new OnlineMarket(1, new ArrayList(), "IN GROUP"));
        
        Map<String, Object> emailContext = buyingAlertsService.prepareEmailContext(subscriber, liveAuctions, onlineAuctions);
        
        assertEquals("Does not have dealer name", emailContext.containsKey("businessUnitName"), true);
        assertFalse("Value is null", emailContext.get("businessUnitName") == null);
        assertEquals("Does not have live auction results", emailContext.containsKey("liveAuctions"), true);
        assertFalse("Value is null", emailContext.get("liveAuctions") == null);
        assertEquals("Does not have online auction results", emailContext.containsKey("onlineResults"), true);
        assertFalse("Value is null", emailContext.get("onlineResults") == null);
        
    }    
}
