package biz.firstlook.buyingAlerts;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.buyingAlerts.dao.BuyingAlertsDao;
import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.DefaultMarket;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.PrecisionMatch;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;

public class MockBuyingAlertsDaoImpl implements BuyingAlertsDao{
    
	static final BuyingAlertsSubscriber subscriber = new BuyingAlertsSubscriber("BusinessUnitName", "TestFirstName","LastName", "Email", 1,
	                                                               Integer.MIN_VALUE, Integer.MAX_VALUE, 100, 200, 12345,
	                                                               true, true, true, "GroupName");
	
	static final BuyingAlertsSearchPreference inGroupPref = new BuyingAlertsSearchPreference(999,1000, 100000, 0, "In Group from profile.");
	static final BuyingAlertsSearchPreference adessaPref = new BuyingAlertsSearchPreference(888,1000, 100000, 0, "Adessa from profile.");
	static final BuyingAlertsSearchPreference ovePref = new BuyingAlertsSearchPreference(777,1000, 100000, 0, "OVE from profile.");

    public Integer getSubscriptionFrequencyDetail(String day) {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_WEEK);
    }
    
    public List<BuyingAlertsSubscriber> getBuyingAlertSubscribers(String inClause) {
    	List<BuyingAlertsSubscriber> member = new ArrayList<BuyingAlertsSubscriber>();
    	
    	//not sure what the count variable is for in the calling method.
    	int count = parseInClause( inClause );
    	if( count == 0 ) {
    		member.add( subscriber );
    	} else {
	    	for( int i = 0; i < count; i++ ) {
	    		BuyingAlertsSubscriber subscriber =  new BuyingAlertsSubscriber("BusinessUnitName", "TestFirstName","LastName", "Email", 1,
	    			                                                               Integer.MIN_VALUE + i, Integer.MAX_VALUE - i, 100, 200, 12345,
	    			                                                               true, true, true, "GroupName");
	    		member.add( subscriber );
	    	}
    	}
        return member;
    }
    
    private int parseInClause(String inClause){
    	if( StringUtils.isBlank( inClause )) {
    		return 0;
    	}
    	
    	String arrayString = inClause.substring(inClause.indexOf( "(" ));
    	String[] elements = arrayString.split( "," );
    	return elements.length;
    }
    
    public List<BuyingAlertsSearchPreference> getBuyingAlertsSubscriberSearchPrefs(int memberId, int businessUnitId) {
        List<BuyingAlertsSearchPreference> prefs = new ArrayList<BuyingAlertsSearchPreference>();
        if (memberId != 987654) { 
            prefs.add(inGroupPref);
            prefs.add(adessaPref);
            prefs.add(ovePref);
        } 
        else { //Get default preferences.
            return null;
        }
        return prefs;
    }

    public List<DefaultMarket> getDefaultLocalMarkets(int businessUnitId) {
        List<DefaultMarket> localMarkets = new ArrayList<DefaultMarket>();
        localMarkets.add(new DefaultMarket(123, "ATC"));
        return localMarkets;
    }

    public List<DefaultMarket> getDefaultInGroupMarkets() {
        List<DefaultMarket> defaultMarkets = new ArrayList<DefaultMarket>();
        defaultMarkets.add(new DefaultMarket(141, "IN GROUP"));
        return defaultMarkets;
    }

    public List<DefaultMarket> getDefaultOpenMarkets(String inClause) {
        List<DefaultMarket> defaultOpenMarkets = new ArrayList<DefaultMarket>();
        defaultOpenMarkets.add(new DefaultMarket(146, "OVE"));
        return defaultOpenMarkets;
    }

    public List<SearchCandidate> getSearchCandidates(int memberId, int businessUnitId) {
        List<SearchCandidate> searchCandidates = new ArrayList<SearchCandidate>();
        searchCandidates.add(new SearchCandidate("make 1", "line 1", 1, 2000));
        searchCandidates.add(new SearchCandidate("make 2", "line 2", 1, 2000));
        searchCandidates.add(new SearchCandidate("make 3", "line 3", 1, 2000));
        searchCandidates.add(new SearchCandidate("make 4", "line 4", 1, 2000));
        return searchCandidates;
    }

    public List<SearchCandidate> getDefaultSearchCandidates(int businessUnitId) {
        List<SearchCandidate> searchCandidates = new ArrayList<SearchCandidate>();
        searchCandidates.add(new SearchCandidate("make 1", "line 1", 1, 2000));
        searchCandidates.add(new SearchCandidate("make 2", "line 2", 1, 2000));
        searchCandidates.add(new SearchCandidate("make 3", "line 3", 1, 2000));
        searchCandidates.add(new SearchCandidate("make 4", "line 4", 1, 2000));
        return searchCandidates;
    }

    public List<LiveAuction> getLiveAuctions(String searchQuery, Date searchDate, Date endDate) {
        List<LiveAuction> auctions = new ArrayList<LiveAuction>();
        auctions.add(new LiveAuction("Consignment Sale", "Adesa Chicago", "Chicago", new Date(System.currentTimeMillis()), 500));
        auctions.add(new LiveAuction("Consignment Sale", "Adesa Oakland", "Oakland", new Date(System.currentTimeMillis()), 400));
        auctions.add(new LiveAuction("Consignment Sale", "Adesa Dallas", "Chicago", new Date(System.currentTimeMillis()), 300));
        auctions.add(new LiveAuction("Consignment Sale", "Adesa Miami", "Chicago", new Date(System.currentTimeMillis()), 200));
        auctions.add(new LiveAuction("Consignment Sale", "Adesa New York", "Chicago", new Date(System.currentTimeMillis()), 100));
        return auctions;
    }

    public List<AuctionVehicle> getOnlineVehicles(String searchQuery, Date searchDate) {
        List<AuctionVehicle> vehicles = new ArrayList<AuctionVehicle>();
        vehicles.add(new AuctionVehicle(321, "IN GROUP", 123456,"FORD EXPLORER SUV", "Dave's Dealership"));
        vehicles.add(new AuctionVehicle(321, "IN GROUP", 987654,"FORD F-150 TRUCK", "Dave's Dealership"));
        vehicles.add(new AuctionVehicle(456, "OVE", 123456,"FORD EXPLORER SUV", null));
        vehicles.add(new AuctionVehicle(456, "OVE", 456789,"FORD TAURUS SEDAN", null));
        vehicles.add(new AuctionVehicle(456, "OVE", 987654,"FORD F-150 TRUCK", null));
        vehicles.add(new AuctionVehicle(789, "ATC", 123456,"FORD EXPLORER SUV", null));
        vehicles.add(new AuctionVehicle(789, "ATC", 456789,"FORD TAURUS SEDAN", null));
        vehicles.add(new AuctionVehicle(789, "ATC", 987654,"FORD F-150 TRUCK", null));
        return vehicles;
    }

    public List<SearchCandidateContribution> getSearchCandidateContributionList(Integer businessUnitId, String inClause) {
        List<SearchCandidateContribution> searchCandidates = new ArrayList<SearchCandidateContribution>();
        searchCandidates.add(new SearchCandidateContribution(123456,"FORD EXPLORER SUV", .75));
        searchCandidates.add(new SearchCandidateContribution(456789,"FORD TAURUS SEDAN", .25));
        searchCandidates.add(new SearchCandidateContribution(987654,"FORD F-150 TRUCK", .50));
        return searchCandidates;
    }

    public List<Integer> getSubscriptionIds(Integer count, Boolean failRetry) {
    	List<Integer> list = new ArrayList<Integer>();
    	if( count != null ) {
    		for( int i = 0; i < count.intValue(); i++ )
	    	list.add(Integer.valueOf( i ));    		
    	} else {
    		//not sure why the test passes in 2 nulls and expects 4 values!
    		if( !(failRetry != null && failRetry.booleanValue())) {
    			list.add( Integer.valueOf( 1 ) );
    		} 
    	}
        return list;
    }

    public List<PrecisionMatch> getPrecisionMatches(String searchQuery, Date searchDate) {
        // TODO Auto-generated method stub
        return null;
    }

    public void updateRunList(int statusId, Date now, int subscriptionId) {
        // TODO Auto-generated method stub
        
    }

    
   
}
