package biz.firstlook.buyingAlerts;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends TestSuite{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTest(new TestSuite( TestBuyingAlertsMailer.class ));
        suite.addTest(new TestSuite( TestBuyingAlertsService.class ));
        return suite;
    }

}
