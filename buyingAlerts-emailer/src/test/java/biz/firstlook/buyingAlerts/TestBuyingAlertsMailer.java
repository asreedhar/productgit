package biz.firstlook.buyingAlerts;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import biz.firstlook.buyingAlerts.model.AuctionVehicle;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSearchPreference;
import biz.firstlook.buyingAlerts.model.BuyingAlertsSubscriber;
import biz.firstlook.buyingAlerts.model.DefaultMarket;
import biz.firstlook.buyingAlerts.model.LiveAuction;
import biz.firstlook.buyingAlerts.model.OnlineMarket;
import biz.firstlook.buyingAlerts.model.SearchCandidate;
import biz.firstlook.buyingAlerts.model.SearchCandidateContribution;
import biz.firstlook.buyingAlerts.util.SearchQueryXMLBuilder;

public class TestBuyingAlertsMailer extends AbstractBuyingAlertsTest{
    
    public void testXMLBuilder(){
        MockBuyingAlertsDaoImpl mockBuyingAlertDaoImpl = new MockBuyingAlertsDaoImpl();
       
        Integer freqDetailDay = mockBuyingAlertDaoImpl.getSubscriptionFrequencyDetail("Monday");       
        Calendar c = Calendar.getInstance();
        Integer expectedDay = c.get(Calendar.DAY_OF_WEEK);
        assertEquals("Subscription Frequency Detail Not Correct", freqDetailDay, expectedDay);
        
        //buying alert subscriber
        List<BuyingAlertsSubscriber> subscribers = mockBuyingAlertDaoImpl.getBuyingAlertSubscribers("");
        BuyingAlertsSubscriber subscriber = subscribers.get(0);
        assertNotNull("Buying Alerts Subscriber Null", subscriber);
        assertTrue("Buying Alerts Subscriber BusinessUnitName Not Correct ", subscriber.getBusinessUnitName().equals("BusinessUnitName") );
        assertTrue("Buying Alerts Subscriber FirstName Not Correct ", subscriber.getFirstName().equals("TestFirstName") );
        assertTrue("Buying Alerts Subscriber LastName Not Correct ", subscriber.getLastName().equals("LastName") );
        assertTrue("Buying Alerts Subscriber Email Not Correct ", subscriber.getEmail().equals("Email") );
        assertEquals("Buying Alerts Subscriber BusinessUnitID Not Correct ", MockBuyingAlertsDaoImpl.subscriber.getBusinessUnitId(), subscriber.getBusinessUnitId() );
        assertEquals("Buying Alerts Subscriber MemberID Not Correct ", MockBuyingAlertsDaoImpl.subscriber.getMemberId(), subscriber.getMemberId() );
        assertEquals("Buying Alerts Subscriber PurchasingDistance Not Correct ", 100, subscriber.getPurchasingDistance().intValue());
        assertEquals("Buying Alerts Subscriber LiveAuctionDistance Not Correct ", 200, subscriber.getLiveAuctionDistance().intValue());
        assertTrue("Buying Alerts Subscriber ATCEnabled Not Correct ", subscriber.isAtcEnabled());
        assertTrue("Buying Alerts Subscriber GmacEnabled Not Correct ", subscriber.isGmacEnabled());
        assertTrue("Buying Alerts Subscriber TfsEnabled Not Correct ", subscriber.isTfsEnabled());
       
        //buying alert subscriber prefs
        List<BuyingAlertsSearchPreference> searchPrefs = mockBuyingAlertDaoImpl.getBuyingAlertsSubscriberSearchPrefs(1,1);
        BuyingAlertsSearchPreference searchPref = searchPrefs.get(0);
        assertNotNull("Buying Alerts Search Pref Null", searchPref);
        assertEquals("Buying Alerts Search Prefs AccessGroupID Not Correct ", searchPref.getAccessGroupId().intValue(), 999 );
        assertEquals("Buying Alerts Search Prefs MaxDistanceFromDealer Not Correct ", searchPref.getMaxDistanceFromDealer().intValue(), 1000 );
        assertEquals("Buying Alerts Search Prefs MaxVehicleMileage Not Correct ", searchPref.getMaxVehicleMileage().intValue(), 100000 );
        assertEquals("Buying Alerts Search Prefs MinVehicleMileage Not Correct ", searchPref.getMinVehicleMileage().intValue(), 0);
        assertTrue("Buying Alerts Search Prefs CustomerFacingDesc Not Correct ", searchPref.getCustomerFacingDescription().equals("In Group from profile.") );
     
        //default local markets
        List<DefaultMarket> defLocalMarkets = mockBuyingAlertDaoImpl.getDefaultLocalMarkets(1);
        assertNotNull("Default Local Markets Null", defLocalMarkets);
        DefaultMarket localMarket = defLocalMarkets.get(0);
        assertEquals("Default Local Markets AccessGroupID Not Correct ", localMarket.getAccessGroupId().intValue(), 123);
        assertTrue("Default Local Markets AccessGroup Not Correct ", localMarket.getAccessGroup().equals("ATC") );
        
        //default in group markets
        List<DefaultMarket> defInGroupMarkets = mockBuyingAlertDaoImpl.getDefaultInGroupMarkets();
        assertNotNull("Default InGroup Null", defInGroupMarkets);
        DefaultMarket inGroupMarket = defInGroupMarkets.get(0);
        assertEquals("Default InGroup Markets AccessGroupID Not Correct ", inGroupMarket.getAccessGroupId().intValue(), 141);
        assertTrue("Default InGroup Markets AccessGroup Not Correct ", inGroupMarket.getAccessGroup().equals("IN GROUP") );
        
        //default open markets
        List<DefaultMarket> defOpenMarkets = mockBuyingAlertDaoImpl.getDefaultOpenMarkets("IN (1,2,3)");
        assertNotNull("Default Open Markets Null", defOpenMarkets);
        DefaultMarket openMarket = defOpenMarkets.get(0);
        assertEquals("Default Open Markets AccessGroupID Not Correct ", openMarket.getAccessGroupId().intValue(), 146);
        assertTrue("Default Open Markets AccessGroup Not Correct ", openMarket.getAccessGroup().equals("OVE") );
        
        //search candidates
        List<SearchCandidate> searchCandidates =  mockBuyingAlertDaoImpl.getSearchCandidates(1,1);
        //can this be null?
        assertNotNull("Search Candidates Null", searchCandidates);
        //go through list of candidates and verify
        for(int i = 0; i < searchCandidates.size(); i++){
            SearchCandidate sc = searchCandidates.get(i);
            assertTrue("Search Candidates Make Not Correct ", sc.getMake().equals("make " + (i + 1)));
            assertTrue("Search Candidates Line Not Correct ", sc.getLine().equals("line " + (i + 1)));
            assertEquals("Search Candidates SegmentID Not Correct ", sc.getSegmentId().intValue(), 1);
            assertEquals("Search Candidates Year Not Correct" , sc.getYear().intValue(), 2000);
        }
        
        //default search candidates
        List<SearchCandidate> defSearchCandidates =  mockBuyingAlertDaoImpl.getSearchCandidates(1,1);
        //can this be null?
        assertNotNull("Default Search Candidates Null", defSearchCandidates);
        //go through list of candidates and verify
        for(int i = 0; i < defSearchCandidates.size(); i++){
            SearchCandidate sc = searchCandidates.get(i);
            assertTrue("Default Search Candidates Make Not Correct ", sc.getMake().equals("make " + (i + 1)));
            assertTrue("Default Search Candidates Line Not Correct ", sc.getLine().equals("line " + (i + 1)));
            assertEquals("Default Search Candidates SegmentID Not Correct ", sc.getSegmentId().intValue(), 1);
            assertEquals("Default Search Candidates Year Not Correct ", sc.getYear().intValue(), 2000);
        }
        
        //build XML file from MockDaoData
        SearchQueryXMLBuilder sqxb = new SearchQueryXMLBuilder();
        //compare xml builder string to expected
        String expectedXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Search><BusinessUnit ID=\"-2147483648\" MaxDistanceFromDealer=\"1000\" MaxVehicleMileage=\"100000\" MinVehicleMileage=\"0\"/><AccessGroups><AccessGroup ID=\"999\"/><AccessGroup ID=\"888\"/><AccessGroup ID=\"777\"/></AccessGroups><Filter><MakeModelGrouping SegmentID=\"1\" Make=\"make 1\" Model=\"line 1\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"1\" Make=\"make 2\" Model=\"line 2\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"1\" Make=\"make 3\" Model=\"line 3\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"1\" Make=\"make 4\" Model=\"line 4\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping></Filter></Search>";
        assertTrue("Built XML Not Correct: ", sqxb.buildXML(subscriber, searchPrefs, searchCandidates).equals(expectedXML));
    }
    
    
    public void MailerProcess(){
        Calendar c = Calendar.getInstance();
        Date startDate = new Date(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH, 7 * 2);
        Date endDate = new Date(c.getTimeInMillis());
        
        List<BuyingAlertsSubscriber> subscribers = buyingAlertsService.getBuyingAlertSubscribers(4, null);
        BuyingAlertsSubscriber subscriber = subscribers.get(0);
        assertEquals("First subscriber name should be Joe", subscriber.getFirstName().equalsIgnoreCase("Joe"), true );
        assertEquals("First subscriber name should be Tejada", subscriber.getLastName().equalsIgnoreCase("Tejada"), true );
        assertEquals("First subscriber business unit should be Goodson Honda West", subscriber.getBusinessUnitName().equalsIgnoreCase("Goodson Honda West"), true );
        assertEquals("GMAC Not enabled", subscriber.isGmacEnabled(), false);
        subscriber = subscribers.get(1);
        assertEquals("First subscriber name should be Jose", subscriber.getFirstName().equalsIgnoreCase("Jose"), true );
        assertEquals("First subscriber name should be Lopez", subscriber.getLastName().equalsIgnoreCase("Lopez"), true );
        assertEquals("First subscriber business unit should be Goodson Honda West", subscriber.getBusinessUnitName().equalsIgnoreCase("Goodson Honda West"), true );
        assertEquals("GMAC Not enabled", subscriber.isGmacEnabled(), false);
        subscriber = subscribers.get(2);
        assertEquals("Third subscriber name should be Donald", subscriber.getFirstName().equalsIgnoreCase("Donald"), true );
        assertEquals("Third subscriber name should be Brown", subscriber.getLastName().equalsIgnoreCase("Brown"), true );
        assertEquals("Third subscriber business unit should be R H Chevy Durham", subscriber.getBusinessUnitName().equalsIgnoreCase("R H Chevy Durham"), true );
        assertEquals("GMAC Not enabled", subscriber.isGmacEnabled(), true);
        subscriber = subscribers.get(3);
        assertEquals("Third subscriber name should be Roderick", subscriber.getFirstName().equalsIgnoreCase("Roderick"), true );
        assertEquals("Third subscriber name should be Hinton", subscriber.getLastName().equalsIgnoreCase("Hinton"), true );
        assertEquals("Third subscriber business unit should be R H Chevy Durham", subscriber.getBusinessUnitName().equalsIgnoreCase("R H Chevy Durham"), true );
        assertEquals("GMAC Not enabled", subscriber.isGmacEnabled(), true);
        
        BuyingAlertsSubscriber bas = subscribers.get(0);
        List<BuyingAlertsSearchPreference> prefs = buyingAlertsService.retrievePreferences(bas);
        BuyingAlertsSearchPreference pref = prefs.get(0);
        assertEquals("Incorrect Access Group.", pref.getAccessGroupId().equals(1), true);
        assertEquals("Incorrect custome facing description.", pref.getCustomerFacingDescription().equalsIgnoreCase("ATC Open Auction"), true);
        assertEquals("Incorrect max distance from dealer.", pref.getMaxDistanceFromDealer().equals(1000), true);
        assertEquals("Incorrect max mileage range.", pref.getMaxVehicleMileage().equals(100000), true);
        assertEquals("Incorrect min mileage range.", pref.getMinVehicleMileage().equals(0), true);
        pref = prefs.get(3);
        assertEquals("Incorrect access group id.", pref.getAccessGroupId().equals(141), true);
        assertEquals("Incorrect custome facing description.", pref.getCustomerFacingDescription().equalsIgnoreCase("In Group"), true);
        pref = prefs.get(5);
        assertEquals("Incorrect access group id.", pref.getAccessGroupId().equals(146), true);
        assertEquals("Incorrect custome facing description.", pref.getCustomerFacingDescription().equalsIgnoreCase("Ove Open Auction"), true);
        
        List<SearchCandidate> candidates = buyingAlertsService.retrieveCandidates(bas);
        
        
        String searchQuery = buyingAlertsService.buildXMLQuery(bas ,prefs, candidates);
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Search><BusinessUnit ID=\"100138\" MaxDistanceFromDealer=\"1000\" MaxVehicleMileage=\"100000\" MinVehicleMileage=\"0\"/><AccessGroups><AccessGroup ID=\"1\"/><AccessGroup ID=\"2\"/><AccessGroup ID=\"3\"/><AccessGroup ID=\"141\"/><AccessGroup ID=\"142\"/><AccessGroup ID=\"146\"/></AccessGroups><Filter><MakeModelGrouping SegmentID=\"3\" Make=\"Acura\" Model=\"TL\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>1999</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2002</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>2002</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>2002</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"CR-V\"><Criteria Type=\"Year\"><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"CR-V\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"CR-V\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"CR-V\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"Element\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"Element\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Fit\"><Criteria Type=\"Year\"><Value>2007</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"5\" Make=\"Honda\" Model=\"Odyssey\"><Criteria Type=\"Year\"><Value>2001</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"5\" Make=\"Honda\" Model=\"Odyssey\"><Criteria Type=\"Year\"><Value>2002</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"5\" Make=\"Honda\" Model=\"Odyssey\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"Pilot\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"Pilot\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"Pilot\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"7\" Make=\"Honda\" Model=\"S2000\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Nissan\" Model=\"Murano\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Toyota\" Model=\"Corolla\"><Criteria Type=\"Year\"><Value>1999</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Toyota\" Model=\"Corolla\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping></Filter></Search>";
        assertEquals("Incorrect xml query", searchQuery.equalsIgnoreCase(expected), true);
        
        
        List<LiveAuction> liveAuctions = buyingAlertsService.getLiveAuctions(searchQuery, startDate, endDate);
        LiveAuction la = liveAuctions.get(0);
        assertEquals("Incorrect auction name.", la.getAuction().equalsIgnoreCase("ADESA Charlotte"), true);
        assertEquals("Incorrect number of buying opportunities.", la.getBuyingOps(), 94);
        assertEquals("Incorrect auction title", la.getTitle().equalsIgnoreCase("Consignment Sale"), true);
        la = liveAuctions.get(1);
        assertEquals("Incorrect auction name.", la.getAuction().equalsIgnoreCase("ADESA Dallas"), true);
        assertEquals("Incorrect number of buying opportunities.", la.getBuyingOps(), 75);
        assertEquals("Incorrect auction title", la.getTitle().equalsIgnoreCase("Consignment Sale"), true);
        la = liveAuctions.get(3);
        assertEquals("Incorrect auction name.", la.getAuction().equalsIgnoreCase("ADESA Lexington"), true);
        assertEquals("Incorrect number of buying opportunities.", la.getBuyingOps(), 7);
        assertEquals("Incorrect auction title", la.getTitle().equalsIgnoreCase("Consignment Sale"), true);
        
        List<AuctionVehicle> vehicles = buyingAlertsService.getOnlineVehicles(searchQuery, startDate);
        assertEquals("Incorrect number of online vehicle results.", vehicles.size(), 74);
        Map<String, Object> precisionMatches = buyingAlertsService.getPrecisionMatches(searchQuery, startDate);
        assertEquals("Incorrect number of in group precision matches", precisionMatches.get("In Group").equals(23), true);
        assertEquals("Incorrect number of OVE precision matches", precisionMatches.get("OVE Open Auction").equals(32), true);
        
        List<SearchCandidateContribution> scc = buyingAlertsService.retriveCandidateContribution(bas, vehicles);
        
        List<String> accessGroups = new ArrayList<String>();
        for (BuyingAlertsSearchPreference basp : prefs) {
            if (!accessGroups.contains(basp.getCustomerFacingDescription())) {
                accessGroups.add(basp.getCustomerFacingDescription());
            }
        }
        
        List<OnlineMarket> onlineMarkets = buyingAlertsService.retrieveGroupings(accessGroups, vehicles, scc,precisionMatches);
        
        OnlineMarket oa = onlineMarkets.get(0);
        assertEquals("Incorrect number of buying opportunities", oa.getBuyingOps().equals(23), true);
        assertEquals("Incorrect market", oa.getMarket().equalsIgnoreCase("In Group"), true);
        
        oa = onlineMarkets.get(1);
        assertEquals("Incorrect number of buying opportunities", oa.getBuyingOps().equals(32), true);
        assertEquals("Incorrect market", oa.getMarket().equalsIgnoreCase("OVE Open Auction"), true);
        
        
        // Second subscriber.
        bas = subscribers.get(3);
        prefs = buyingAlertsService.retrievePreferences(bas);
        pref = prefs.get(3);
        assertEquals("Incorrect Access Group.", pref.getAccessGroupId().equals(127), true);
        assertEquals("Incorrect custome facing description.", pref.getCustomerFacingDescription().equalsIgnoreCase("Smart Auction - by GMAC"), true);
        assertEquals("Incorrect max distance from dealer.", pref.getMaxDistanceFromDealer().equals(1000), true);
        assertEquals("Incorrect max mileage range.", pref.getMaxVehicleMileage().intValue(), 0);
        assertEquals("Incorrect min mileage range.", pref.getMinVehicleMileage().intValue(), 0);
        pref = prefs.get(5);
        assertEquals("Incorrect access group id.", pref.getAccessGroupId().equals(141), true);
        assertEquals("Incorrect custome facing description.", pref.getCustomerFacingDescription().equalsIgnoreCase("In Group"), true);
        pref = prefs.get(6);
        assertEquals("Incorrect access group id.", pref.getAccessGroupId().equals(142), true);
        assertEquals("Incorrect custome facing description.", pref.getCustomerFacingDescription().equalsIgnoreCase("ADESA"), true);
        
        candidates = buyingAlertsService.retrieveCandidates(bas);

        searchQuery = buyingAlertsService.buildXMLQuery(bas ,prefs, candidates);
                   
        expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Search><BusinessUnit ID=\"100650\" MaxDistanceFromDealer=\"1000\"/><AccessGroups><AccessGroup ID=\"1\"/><AccessGroup ID=\"2\"/><AccessGroup ID=\"3\"/><AccessGroup ID=\"127\"/><AccessGroup ID=\"128\"/><AccessGroup ID=\"141\"/><AccessGroup ID=\"142\"/><AccessGroup ID=\"146\"/></AccessGroups><Filter><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"C/K 1500 Series\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"CHEVROLET\" Model=\"C1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Chevrolet\" Model=\"Cavalier\"/><MakeModelGrouping SegmentID=\"4\" Make=\"Chevrolet\" Model=\"Cobalt\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Chevrolet\" Model=\"Impala\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"CHEVROLET\" Model=\"K1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Chevrolet\" Model=\"Malibu\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"CHEVROLET\" Model=\"SILVERADO\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"Silverado 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"Silverado 1500 SS\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"Silverado 1500HD\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Chevrolet\" Model=\"Tahoe\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Chevrolet\" Model=\"Tahoe\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Chevrolet\" Model=\"Tahoe\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"5\" Make=\"Chevrolet\" Model=\"Uplander\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"5\" Make=\"Chevrolet\" Model=\"Uplander\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Ford\" Model=\"F-150\"/><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"CLASSIC C/K 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"K1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"SIERRA\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"Sierra 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"Sierra 1500HD\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"Sierra Classic 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"GMC\" Model=\"Yukon\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"GMC\" Model=\"Yukon\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"GMC\" Model=\"Yukon\"><Criteria Type=\"Year\"><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Pontiac\" Model=\"G6\"><Criteria Type=\"Year\"><Value>2006</Value></Criteria></MakeModelGrouping></Filter></Search>";
        assertEquals("Incorrect xml query", searchQuery, expected);
        
        
        liveAuctions = buyingAlertsService.getLiveAuctions(searchQuery, startDate, endDate);
        la = liveAuctions.get(0);
        assertEquals("Incorrect auction name.", la.getAuction().equalsIgnoreCase("ADESA Kansas City"), true);
        assertEquals("Incorrect number of buying opportunities.", la.getBuyingOps(), 9);
        assertEquals("Incorrect auction title", la.getTitle().equalsIgnoreCase("Consignment Sale"), true);
        la = liveAuctions.get(1);
        assertEquals("Incorrect auction name.", la.getAuction().equalsIgnoreCase("ADESA Cleveland"), true);
        assertEquals("Incorrect number of buying opportunities.", la.getBuyingOps(), 6);
        assertEquals("Incorrect auction title", la.getTitle().equalsIgnoreCase("Consignment Sale"), true);
        la = liveAuctions.get(3);
        assertEquals("Incorrect auction name.", la.getAuction().equalsIgnoreCase("ADESA Memphis"), true);
        assertEquals("Incorrect number of buying opportunities.", la.getBuyingOps(), 86);
        assertEquals("Incorrect auction title", la.getTitle().equalsIgnoreCase("GM Factory Sale"), true);
        
        vehicles = buyingAlertsService.getOnlineVehicles(searchQuery, startDate);
        assertEquals("Incorrect number of online vehicle results.", vehicles.size(), 170);
        precisionMatches = buyingAlertsService.getPrecisionMatches(searchQuery, startDate);
        assertEquals("Incorrect number of in group precision matches", precisionMatches.get("In Group").equals(18), true);
        assertEquals("Incorrect number of OVE precision matches", precisionMatches.get("OVE Open Auction").equals(18), true);
        
        scc = buyingAlertsService.retriveCandidateContribution(bas, vehicles);
        
        accessGroups = new ArrayList<String>();
        for (BuyingAlertsSearchPreference basp : prefs) {
            if (!accessGroups.contains(basp.getCustomerFacingDescription())) {
                accessGroups.add(basp.getCustomerFacingDescription());
            }
        }
        
        onlineMarkets = buyingAlertsService.retrieveGroupings(accessGroups, vehicles, scc,precisionMatches);
        
        oa = onlineMarkets.get(0);
        assertEquals("Incorrect number of buying opportunities", oa.getBuyingOps().equals(18), true);
        assertEquals("Incorrect market", oa.getMarket().equalsIgnoreCase("In Group"), true);
        
        oa = onlineMarkets.get(1);
        assertEquals("Incorrect number of buying opportunities", oa.getBuyingOps().equals(18), true);
        assertEquals("Incorrect market", oa.getMarket().equalsIgnoreCase("OVE Open Auction"), true);
    }
}