package biz.firstlook.buyingAlerts;

import junit.framework.TestCase;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import biz.firstlook.buyingAlerts.service.BuyingAlertsService;
import biz.firstlook.buyingAlerts.service.impl.BuyingAlertsServiceImpl;


public abstract class AbstractBuyingAlertsTest extends TestCase
{

static final String[] configLocations = new String[] { "applicationContext.xml", "test-applicationContext-dataSource.xml" };

protected static AbstractApplicationContext applicationContext;
protected static BuyingAlertsService buyingAlertsService;

public AbstractBuyingAlertsTest()
{
	super();
	if( applicationContext == null ) {
		applicationContext = new ClassPathXmlApplicationContext( configLocations );
	}
    if (buyingAlertsService == null) {
        buyingAlertsService = (BuyingAlertsServiceImpl) applicationContext.getBean("buyingAlertsService");
    }
}

protected String[] getConfigLocations()
{
	return configLocations;
}

}
