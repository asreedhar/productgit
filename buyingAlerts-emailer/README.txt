For deploying and running the buying alerts mailer.

1.)  Retrieve .jar from cruise.

2.)  Extract to desired location (where it will be executed from).

3.)  There is a Stored Procecdure in IMT called PopulateBuyingAlertsRunList,
	 this needs to be called every morning before the emailer is launched.  It
	 populates a run list table (similar to how the bookout proc works) from 
	 members having a subscription to the buying alerts for that day.

4.)  It needs to be scheduled to run after all the days auctions have been loaded.
	 (Check with Phillip for time).

5.)  There is a .sh and .bat file called runBuyingAlertsMailer that executes the process.
	 These scripts take parameters
	 
	 -r		A flag that tells the program to only send those in the run list with a status of failed.
	 -n X   A number, tells the program to only send the specified number of emails.
	 
	 e.g. 	runBuyingAlertsMailer.bat -n 100 -r     sends up to 100 emails that have a status of failed
	 		runBuyingAlertsMailer.bat -n 100		sends up to 100 emails from the run list.
	 		runBuyingAlertsMailer.bat -r			sends all failed emails from the run list.
	 		
	 Parameters are not required and if none are specified it will send all the emails in the run list.
		





