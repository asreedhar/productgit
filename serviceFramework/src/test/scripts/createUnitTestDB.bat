@echo off

rem ******************************************************
rem * This script is used to build the following DBs     *
rem * on your localhost SQL Server:                      *
rem *  					DBASTAT                          *
rem *  					IMT	                             *
rem *  					FLDW                             *
rem *  	                                                 *
rem *  													 *
rem * Note: Script expects buildDB.py and supporting     *
rem *	    code to be present in current directory.	 *
rem *							                         *
rem ******************************************************


set SQL_SERVER_HOME=D:\Program Files\Microsoft SQL Server\MSSQL
set _DBASTATScriptsDir=C:\projects\db-scripts\DBASTAT
set _fldwDBManagementScripts=C:\projects\fldw\database\management
set _imtDBManagementScripts=C:\projects\IMT\database\management
set _imtBackUp=\\pluto\SandboxFileSource\IMT_SMALL.BAK


rem ***************** Build DBASTAT *****************************************
rem **  THIS IS NOT REQUIRED FOR A PLUTO BUILD. Only local.
rem *************************************************************************
rem buildDB.py s PLUTO DBASTAT -d %_DBASTATScriptsDir%

rem ***************** Build IMT *********************************************
buildDB.py d PLUTO IMT_Unit -d %_imtDBManagementScripts% -b %_imtBackUp% -w FLDW_Unit

rem ***************** Build FLDW ********************************************
rem **  We need to set the SQL Home var to point to the path on PLUTO
rem *************************************************************************
SET SQL_SERVER_HOME=D:\MSSQL
buildDB.py s PLUTO FLDW_Unit -d %_fldwDBManagementScripts% 

rem ***************** Build Aggregate Views/Tables **************************
osql -b -m1 -n -dFLDW_Unit -S PLUTO -E -Q"EXEC dbo.BuildWarehouseTableGroup @WarehouseTableGroupID = 6"
osql -b -m1 -n -dFLDW_Unit -S PLUTO -E -Q"EXEC dbo.BuildWarehouseTableGroup @WarehouseTableGroupID = 5, @BuildFlags = 0"



