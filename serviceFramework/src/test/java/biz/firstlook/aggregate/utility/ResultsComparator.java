package biz.firstlook.aggregate.utility;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

/**
 * Utitlity class for comparing results from a Aggregate call to Expected Results in an excel sheet.
 * 
 * @author nkeen
 * 
 */
public class ResultsComparator extends TestCase
{

public static void compareFullResultSets( List< Map <String, Object> > expected_results, List< Map< String, Object > > actual_results )
{
	
	assertEquals( "didn't get same number of rows as results!" , expected_results.size(), actual_results.size() );
	for( int c = 0; c < actual_results.size(); c++ )
	{
		Iterator< String > iter = expected_results.get(c).keySet().iterator();
		while ( iter.hasNext() )
		{
			String key = iter.next();
			assertEquals( "row: " + c + " column: " + key + " didn't match!", expected_results.get(c).get( key ), actual_results.get(c).get(key) );
		}
	}
}
}
