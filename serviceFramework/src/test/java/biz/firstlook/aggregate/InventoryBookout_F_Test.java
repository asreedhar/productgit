package biz.firstlook.aggregate;

import java.util.List;
import java.util.Map;

import biz.firstlook.aggregate.utility.ResultsComparator;

public class InventoryBookout_F_Test extends GenericAggregateSetup
{

private static final String ACTUAL_QUERY = "select * from InventoryBookout_A1 where businessUnitId = 100147 ";
private static final String EXPECTED_QUERY = "" +
		"SELECT	I.BusinessUnitID, InventoryType AS InventoryTypeID, TPC.ThirdPartyCategoryID, SUM(UnitCost) AS UnitCost, SUM(BV.VAlue) AS BookValue " +
		" FROM 	InventoryBookouts IB" +
		"	JOIN InventoryActive I ON IB.InventoryID = I.InventoryID" +
		"	JOIN Bookouts B ON IB.BookoutID = B.BookoutID " +
		"	JOIN (  SELECT	I.BusinessUnitID, I.InventoryID, B.ThirdPartyID, MAX(B.DateCreated) MaxDateCreated" +
		"		FROM 	InventoryBookouts IB" +
		"			JOIN InventoryActive I ON IB.InventoryID = I.InventoryID" +
		"			JOIN Bookouts B ON IB.BookoutID = B.BookoutID " +
		"		WHERE	B.BookoutSourceID in (2,3) 				" + 
		"		GROUP" +
		"		BY	I.BusinessUnitID, I.InventoryID, B.ThirdPartyID) CB ON I.BusinessUnitID = CB.BusinessUnitID AND I.InventoryID = CB.InventoryID AND B.ThirdPartyID = CB.ThirdPartyID AND B.DateCreated = CB.MaxDateCreated" +
		" " +
		 " 	JOIN BookoutThirdPartyCategories BTPC ON B.BookoutID = BTPC.BookoutID" +
		" " +
		"	JOIN ThirdPartyCategories TPC ON BTPC.ThirdPartyCategoryID = TPC.ThirdPartyCategoryID" +
		"	JOIN ThirdParties TP ON TPC.ThirdPartyID = TP.ThirdPartyID" +
		"	JOIN BookoutValues BV ON  BV.BookoutValueTypeID = 2 AND BTPC.BookoutThirdPartyCategoryID = BV.BookoutThirdPartyCategoryID" +
		"	JOIN Time_D T ON T.TypeCD = 1 AND cast(convert(varchar(10),BV.DateCreated,101) as datetime) = T.BeginDate " +
		" " +
		" WHERE	B.BookoutSourceID in (2,3)" + 
		"	AND I.businessUnitid = 100147" +
		"	AND I.inventorytype = 2" +
		"	AND I.UnitCost > 0 " +
		"	AND BV.Value > 0 " +
		"	AND I.inventoryactive = 1 " +
		" GROUP" +
		" BY	I.BusinessUnitId, InventoryType, TPC.ThirdPartyCategoryID" +
		" ORDER " +
		" BY	TPC.ThirdPartyCategoryID";
	

public void testInventoryBookout_F()
{
	List< Map<String, Object> > actual_results = fldwDAO.getResults( ACTUAL_QUERY, new Object[0] );
	List< Map<String, Object> > expected_results = fldwDAO.getResults( EXPECTED_QUERY, new Object[0] );

	ResultsComparator.compareFullResultSets( expected_results, actual_results );

}	


}
