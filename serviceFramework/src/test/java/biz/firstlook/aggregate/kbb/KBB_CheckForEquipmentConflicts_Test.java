package biz.firstlook.aggregate.kbb;

import java.util.List;
import java.util.Map;

import biz.firstlook.aggregate.GenericAggregateSetup;

public class KBB_CheckForEquipmentConflicts_Test extends GenericAggregateSetup
{

public void test_forEquipmentConflicts()
{
	String stdOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT";
	String selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT";
	List<Map<String, Object>> results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("found conflicts when there shouldn't ahve been any", 0, results.size() );
	
	selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT,IC";
	results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("didn't find CC/IC conflict", 1, results.size() );
	
	selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT,CH,MC,MP";
	results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("didn't find multiple conflict CD/CH/MC/MP conflict", 6, results.size() );
	
	selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT,CH,IC";
	results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("didn't find multiple conflict CD/CH and CC/IC conflict", 2, results.size() );
}

public void test_forDependantConflicts()
{
	String stdOptions = "AC";
	String selectedOptions = "DA";
	
	List<Map<String, Object>> results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );

	assertEquals("did not find dependency conflict!", 1, results.size() );
	
	selectedOptions = "DA,AC";
	results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("found conflicts when there shouldn't ahve been any", 0, results.size() );
}

public void test_forMutexConflixt()
{
	String stdOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT,DP";
	String selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT";
	List<Map<String, Object>> results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("didn't find the fact that you must have 1 of a mutex group", 1, results.size() );

	selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT,DR";
	results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("didn't find the fact that you can have any member of a mutex group", 0, results.size() );
	
	selectedOptions = "2W,AC,PS,PW,PL,TW,CC,RA,CD,DB,AB,8G,AT,DR,DP";
	results = kbbDAO.getResults( "EXEC CheckForEquipmentConflicts ?, ?, 100215, 1", new Object[]{selectedOptions, stdOptions } );
	assertEquals("didn't find the fact that you can't have more then one member of a mutex group", 1, results.size() );
}

public void test_missingStandardOptions()
{
	String missingStdOption = "CD";
	String selectedOptions = "CH";
	List<Map<String, Object>> results = kbbDAO.getResults( "EXEC CheckForMissingStandardOptionConflicts ?, ?, 100215, 1", new Object[]{missingStdOption, selectedOptions } );
	assertEquals("didn't find the 1 missing std option who'se adjustment should not be applied", 1, results.size() );
	
	missingStdOption = "CD";
	selectedOptions = "2W,AC";
	results = kbbDAO.getResults( "EXEC CheckForMissingStandardOptionConflicts ?, ?, 100215, 1", new Object[]{missingStdOption, selectedOptions } );
	assertEquals("returned in incorrect option whose value will need to be applied", 0, results.size() );
}

}
