package biz.firstlook.aggregate.kbb;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.UncategorizedSQLException;

import biz.firstlook.aggregate.GenericAggregateSetup;
import biz.firstlook.aggregate.utility.ResultsComparator;
import biz.firstlook.commons.util.DateUtilFL;

public class KBB_GetControlID_Test extends GenericAggregateSetup
{

public void test_base_case()
{
	// test inventory dataset
	List< Map< String, Object > > sprocResults = kbbDAO.getResults( "SELECT	dbo.getControlID( 100147, 1, GETDATE()) as controlID", new Object[0] );
	List< Map< String, Object > > queryResults = kbbDAO.getResults( " SELECT controlID FROM	control WHERE	DataStatusCode = ( " +
	           " SELECT KBBInventoryBookoutDatasetPreference FROM	imt..dealerpreference WHERE	businessUnitId = 100147 )", new Object[0] );
	ResultsComparator.compareFullResultSets( queryResults, sprocResults );	

	// test appraisal dataset
	sprocResults = kbbDAO.getResults( "SELECT	dbo.getControlID( 100147, 2, GETDATE()) as controlID", new Object[0] );
	queryResults = kbbDAO.getResults( " SELECT controlID FROM	control WHERE	DataStatusCode = ( " +
	           " SELECT KBBAppraisalBookoutDatasetPreference FROM	imt..dealerpreference WHERE	businessUnitId = 100147 )", new Object[0] );
	
	ResultsComparator.compareFullResultSets( queryResults, sprocResults );	
}

public void test_archive_dataset_stuff()
{
	List< Map< String, Object > > expected_no_overrideResults = kbbDAO.getResults( "SELECT	dbo.getControlID( 100147, 1, GETDATE()) as controlID", new Object[0] );
	
	Date startDate = Calendar.getInstance().getTime();
	Calendar midnightTonight = Calendar.getInstance();
	midnightTonight.set(Calendar.HOUR_OF_DAY, 0);
	midnightTonight.set(Calendar.MINUTE, 0);
	midnightTonight.set(Calendar.SECOND, 0);
	midnightTonight.add(Calendar.DAY_OF_MONTH, 1);
	Date endDate = midnightTonight.getTime();
	
	// insert row into kbb override table so for bu 100147 - we want to use the archived dataset
	try{
	kbbDAO.getResults( "INSERT INTO IMT..KBBArchiveDatasetOverride VALUES (100147, 100000, ?,?)", new Object[]{startDate, endDate} );
	} catch (UncategorizedSQLException e)
	{
		// don't need to do anything here - i'm cheating by calling the insert here - becuae spring expects a results set
		// spring throws an error due to lack of result set - but the insert does get excecuted
	}
	
	List< Map< String, Object > > sprocResults = kbbDAO.getResults( "SELECT	dbo.getControlID( 100147, 1, GETDATE()) as controlID", new Object[0] );
	List< Map< String, Object > > queryResults = kbbDAO.getResults( "SELECT	controlID FROM 	control WHERE	DataStatusCode = " + KBB_Enum.ARCHIVE , new Object[0]);
	
	ResultsComparator.compareFullResultSets( queryResults, sprocResults );
	
	// change the dates on the row to be yesterady so it eseential expires which means the results set should 
	
	Date yesterdayStartDate = DateUtilFL.addDaysToDate( startDate, -1 );
	Date yesterdayEndDate = DateUtilFL.addDaysToDate( endDate, -1 );
	try{
		kbbDAO.getResults( "UPDATE IMT..KBBArchiveDatasetOverride SET startDate = ?, endDate = ? WHERE startDate = ? and endDate = ?", 
		                   new Object[]{yesterdayStartDate, yesterdayEndDate, startDate, endDate} );
		} catch (UncategorizedSQLException e)
		{
			// don't need to do anything here - i'm cheating by calling the update here - becuae spring expects a results set
			// spring throws an error due to lack of result set - but the insert does get excecuted
		}
	
	// should be back to normal now
	sprocResults = kbbDAO.getResults( "SELECT	dbo.getControlID( 100147, 1, GETDATE()) as controlID", new Object[0] );
	
	ResultsComparator.compareFullResultSets( expected_no_overrideResults, sprocResults );
		
}

}
