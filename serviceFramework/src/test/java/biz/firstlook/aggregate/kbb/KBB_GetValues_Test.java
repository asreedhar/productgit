package biz.firstlook.aggregate.kbb;

import java.util.List;
import java.util.Map;

import biz.firstlook.aggregate.GenericAggregateSetup;

public class KBB_GetValues_Test extends GenericAggregateSetup
{

public void test_mileageArugmentOfGetValuesSproc()
{
	runValuesSprocCompare( "2005FTEX", 5, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "2005FTEX", 15, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "2005FTEX", 30, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "2005FTEX", 50, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "2005FTEX", 100, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "2005FTEX", 150, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "1997GMC1", 10, "NY", 100147, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "1997GMC1", 40, "NY", 100147, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "1997GMC1", 80, "NY", 100147, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "1997GMC1", 160, "NY", 100147, KBB_Enum.APPRAISAL );
}

public void test_LotsOfModelsFromLotsOfYears()
{
	runValuesSprocCompare( "1987VTB9", 25, "CA", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "1988BMA5", 35, "SD", 100147, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "1990MPAE", 75, "FL", 100215, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "1991SAAF", 135, "TX", 100216, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "1993CTF2", 75, "NH", 100136, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "1994POR6", 5, "MO", 100138, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "1996SUH5", 85, "CA", 100215, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "1999MEE8", 75, "IL", 100216, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "2001FTZ8", 75, "MI", 100216, KBB_Enum.INVENTORY );
	runValuesSprocCompare( "2004VWJC", 15, "WV", 100216, KBB_Enum.APPRAISAL );
	runValuesSprocCompare( "2006TTS5", 35, "CO", 100216, KBB_Enum.APPRAISAL );
}

// fancy vehicles don't have wholesale values, they only have classic wholesale values, only used for mileage adjustment
public void test_Classic_Wholesale_Vehicles()
{
	
}

public void runValuesSprocCompare( String kbbmodel, Integer mileage, String stateCode, Integer buId, Integer inventoryType )
{
	List<Map<String, Object>> sprocResults = kbbDAO.getResults( "EXEC GetValues ?, ?, ?, ?, ?", 
	                                                            new Object[]{kbbmodel, mileage, stateCode, buId, inventoryType});

	List<Map<String, Object>> controlIdResults = kbbDAO.getResults( "SELECT	dbo.getControlID( ?, ?, GETDATE()) as controlID" , 
	                                                                new Object[]{buId, inventoryType} );
	Integer controlId = (Integer)controlIdResults.get(0).get( "controlID" );

	List<Map<String, Object>> regionCodeResults = kbbDAO.getResults( "select regioncode from RegionMapping WHERE StateCode = ? "
	                                                                 + "AND ControlID = " + controlId.toString() , new Object[]{stateCode} );
	Integer regionCode = (Integer)regionCodeResults.get(0).get( "regioncode" );
	
	List<Map<String, Object>> wholesaleAdjustmentResults = kbbDAO.getResults( "SELECT adjustment FROM modeladjustment ma JOIN AdjustmentContext AC ON AC.Category = 'Wholesale' "
	                                                              + "AND MA.AdjustmentcontextID = AC.AdjustmentContextID AND AC.ControlID = ? "
	                                                              + "WHERE 	MA.ControlID = ? AND KBBModelCode = ? AND MA.RegionCode = ?" 
	                                                              , new Object[]{controlId, controlId, kbbmodel, regionCode} );
	Integer wholesaleAdjustment = (Integer)wholesaleAdjustmentResults.get(0).get( "adjustment" );
	
	List<Map<String, Object>> mileageAdjustmentResults = kbbDAO.getResults( "SELECT	Adjustment FROM 	MileageAdjustment WHERE	ControlID = ? "
	                                                              + "AND mileage = ? AND YEAR = ? AND ? > RangeLow "
	                                                              + " AND ? <= RangeHigh", new Object[]{ controlId, mileage, kbbmodel.substring( 0,4 ), wholesaleAdjustment, wholesaleAdjustment } );
	Integer mileageAdjustment = (Integer)mileageAdjustmentResults.get(0).get("Adjustment");

	// check to make sure mileage adjustment is correct
	for ( Map<String, Object> row : sprocResults )
	{
		if ( "MileageAdjustment".equals( row.get( "ValueType" ) ) )
		{
			assertEquals( "mileage adjustment was off for bu: " + buId + " and mileage: " + mileage, mileageAdjustment, (Integer)row.get("Adjustment") );
			break;
		}
	}
	
	List<Map<String, Object>> modelAdjustmentResults = kbbDAO.getResults( "SELECT Adjustment, Category, Condition "
	                                     + "FROM ModelAdjustment MA JOIN AdjustmentContext AC ON MA.AdjustmentContextID = AC.AdjustmentContextID "
	                                     + " AND AC.ControlID = ? WHERE MA.ControlID = ? AND KBBModelCode = ? "
	                                     + " AND MA.RegionCode = ? "
	                                     , new Object[]{controlId, controlId, kbbmodel, regionCode } );
	
	// check to make sure the model adjustment is correct
	for ( Map<String, Object> sprocRow : sprocResults )
	{
		for ( Map<String, Object> expectedRow : modelAdjustmentResults )
		{
			if ( expectedRow.get( "Category" ).equals( sprocRow.get("Category") ) &&
  				 expectedRow.get( "Condition" ).equals( sprocRow.get("Condition") )	)
			{
				assertEquals( "model adjustment was off for: " + expectedRow.get( "Category" ) + " " + expectedRow.get( "Condition" ),
						expectedRow.get("Adjustment"), sprocRow.get("Adjustment") );
				break;
			}
		}
	}
	
}

public void runValuesSprocCompareForClassicVehicles( String kbbmodel, Integer mileage, String stateCode, Integer buId, Integer inventoryType )
{
	List<Map<String, Object>> sprocResults = kbbDAO.getResults( "EXEC GetValues ?, ?, ?, ?, ?", 
	                                                            new Object[]{kbbmodel, mileage, stateCode, buId, inventoryType});
	
	List<Map<String, Object>> controlIdResults = kbbDAO.getResults( "SELECT	dbo.getControlID( ?, ?, GETDATE()) as controlID" , 
	                                                                new Object[]{buId, inventoryType} );
	Integer controlId = (Integer)controlIdResults.get(0).get( "controlID" );
	
	List<Map<String, Object>> regionCodeResults = kbbDAO.getResults( "select regioncode from RegionMapping WHERE StateCode = ? "
	                                                                 + "AND ControlID = " + controlId.toString() , new Object[]{stateCode} );
	Integer regionCode = (Integer)regionCodeResults.get(0).get( "regioncode" );
	
	List<Map<String, Object>> wholesaleAdjustmentResults = kbbDAO.getResults( "SELECT adjustment FROM modeladjustment ma JOIN AdjustmentContext AC ON AC.Category = 'Classic Wholesale' "
	                                                                          + "AND MA.AdjustmentcontextID = AC.AdjustmentContextID AND AC.ControlID = ? "
	                                                                          + "WHERE 	MA.ControlID = ? AND KBBModelCode = ? AND MA.RegionCode = ?" 
	                                                                          , new Object[]{controlId, controlId, kbbmodel, regionCode} );
	Integer wholesaleAdjustment = (Integer)wholesaleAdjustmentResults.get(0).get( "adjustment" );
	
	List<Map<String, Object>> mileageAdjustmentResults = kbbDAO.getResults( "SELECT	Adjustment FROM 	MileageAdjustment WHERE	ControlID = ? "
	                                                                        + "AND mileage = ? AND YEAR = ? AND ? > RangeLow "
	                                                                        + " AND ? <= RangeHigh", new Object[]{ controlId, mileage, kbbmodel.substring( 0,4 ), wholesaleAdjustment, wholesaleAdjustment } );
	Integer mileageAdjustment = (Integer)mileageAdjustmentResults.get(0).get("Adjustment");
	
	// check to make sure mileage adjustment is correct
	for ( Map<String, Object> row : sprocResults )
	{
		if ( "MileageAdjustment".equals( row.get( "ValueType" ) ) )
		{
			assertEquals( "mileage adjustment was off for bu: " + buId + " and mileage: " + mileage, mileageAdjustment, (Integer)row.get("Adjustment") );
			break;
		}
	}
	
	List<Map<String, Object>> modelAdjustmentResults = kbbDAO.getResults( "SELECT Adjustment, Category, Condition "
	                                                                      + "FROM ModelAdjustment MA JOIN AdjustmentContext AC ON MA.AdjustmentContextID = AC.AdjustmentContextID "
	                                                                      + " AND AC.ControlID = ? WHERE MA.ControlID = ? AND KBBModelCode = ? "
	                                                                      + " AND MA.RegionCode = ? "
	                                                                      , new Object[]{controlId, controlId, kbbmodel, regionCode } );
	
	// check to make sure the model adjustment is correct
	for ( Map<String, Object> sprocRow : sprocResults )
	{
		for ( Map<String, Object> expectedRow : modelAdjustmentResults )
		{
			if ( expectedRow.get( "Category" ).equals( sprocRow.get("Category") ) &&
					expectedRow.get( "Condition" ).equals( sprocRow.get("Condition") )	)
			{
				assertEquals( "model adjustment was off for: " + expectedRow.get( "Category" ) + " " + expectedRow.get( "Condition" ),
				              expectedRow.get("Adjustment"), sprocRow.get("Adjustment") );
				break;
			}
		}
	}
	
}

}
