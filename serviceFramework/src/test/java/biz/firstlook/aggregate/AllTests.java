package biz.firstlook.aggregate;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class AllTests
{

public static void main( String[] args )
{
	TestRunner.run( suite() );
}

public static Test suite()
{
	// test
	TestSuite suite = new TestSuite( "Aggregate Tests" );
	suite.addTestSuite(InventoryBookout_F_Test.class);
	suite.addTestSuite( InventorySales_AV1_Test.class );
	return suite;
}

}
