package biz.firstlook.aggregate;

import junit.framework.TestCase;
import biz.firstlook.report.dao.IBasicIMTLookUpDAO;
import biz.firstlook.utility.ContextLoader;

public abstract class GenericAggregateSetup extends TestCase
{

// I know they are both insances of IBasicIMTLookUpDAO - but fldwDAO injects the fldw datasource
protected static IBasicIMTLookUpDAO imtDAO;
protected static IBasicIMTLookUpDAO fldwDAO;
protected static IBasicIMTLookUpDAO kbbDAO;

public GenericAggregateSetup()
{
	super();
}

protected void setUp()
{
	if ( imtDAO == null ){
		imtDAO = (IBasicIMTLookUpDAO)ContextLoader.getBean( "imtDAO" );
	}
	
	if( fldwDAO == null ) {
		fldwDAO = (IBasicIMTLookUpDAO)ContextLoader.getBean( "fldwDAO" );
	}
	
	if( kbbDAO == null ) {
		kbbDAO = (IBasicIMTLookUpDAO)ContextLoader.getBean( "kbbDAO" );
	}
}

protected void tearDown() throws Exception
{
	super.tearDown();
}

public GenericAggregateSetup( String arg0 )
{
	super( arg0 );
}

}
