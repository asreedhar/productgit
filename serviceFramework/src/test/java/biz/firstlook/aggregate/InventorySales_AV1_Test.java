package biz.firstlook.aggregate;

import java.util.List;
import java.util.Map;

public class InventorySales_AV1_Test extends GenericAggregateSetup
{

private static final String ACTUAL_QUERY = "select * from InventorySales_A1 where businessUnitId=100147";
private static final String EXPECTED_QUERY = "select COUNT ( distinct mmg.model) AS DistinctModelsInStock, cast(round(isnull(sum(unitCost),0),0) as int) as TotalInventoryDollars, " +
											 "		 count(i.vehicleID) as unitsInStock " +
									 							" 	from 	inventory i join vehicle v on i.vehicleID = v.vehicleID " +
																"	join MakeModelGrouping MMG on V.MakeModelGroupingID = MMG.MakeModelGroupingID " +
																"WHERE	i.inventoryActive = 1 and i.inventoryType = 2 and i.businessUnitID = 100147";

public void testInventorySales_AV1()
{
	List< Map<String, Object> > actual_results = fldwDAO.getResults( ACTUAL_QUERY, new Object[0] );
	
	List< Map<String, Object> > expected_results = imtDAO.getResults( EXPECTED_QUERY, new Object[0] );
	for ( int c = 0; c < actual_results.size(); c++)
	{
		assertEquals( "units in stock not correct for row: " + c, 
		              expected_results.get(0).get( "unitsInStock" ), 
		              		actual_results.get( c ).get("UnitsInStock") );
		assertEquals( "units in stock not correct for row: " + c, 
		              expected_results.get(0).get( "DistinctModelsInStock" ), 
		              actual_results.get( c ).get("DistinctModelsInStock") );
		assertEquals( "totalInventoryDollars not correct for row: " + c, 
		              expected_results.get(0).get( "TotalInventoryDollars" ), 
		              actual_results.get( c ).get("TotalInventoryDollars") );
	}

}

}