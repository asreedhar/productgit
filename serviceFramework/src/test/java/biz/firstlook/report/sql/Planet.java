package biz.firstlook.report.sql;

/**
 * This enum class was taken from http://java.sun.com/j2se/1.5.0/docs/guide/language/enums.html. Modified for testing.
 */
public enum Planet
{
	MERCURY( 3.303e+23, 2, "MERCURY" ), VENUS( 4.869e+24, 6, "VENUS" ), EARTH( 5.976e+24, 6, "EARTH" ), MARS( 6.421e+23, 3, "MARS" ), JUPITER( 1.9e+27, 7, "JUPITER" ), SATURN( 5.688e+26, 6, "SATURN" ), URANUS( 8.686e+25, 2, "URANUS" ), NEPTUNE( 1.024e+26, 2, "NEPTUNE" ), PLUTO( 1.27e+22, 1, "PLUTO" );

	private final double mass; // in kilograms
	final int radius; // in meters
	final String name;

	Planet( double mass, int radius, String name )
	{
		this.mass = mass;
		this.radius = radius;
		this.name = name;
	}

	public double mass()
	{
		return mass;
	}

	public int getRadius()
	{
		return radius;
	}

	public String getName()
	{
		return name;
	}

	// universal gravitational constant (m3 kg-1 s-2)
	public static final double G = 6.67300E-11;

	public double surfaceGravity()
	{
		return G * mass / ( radius * radius );
	}

	public double surfaceWeight( double otherMass )
	{
		return otherMass * surfaceGravity();
	}
}