package biz.firstlook.report.sql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.report.reportBuilders.MockReportBuilder;
import biz.firstlook.report.reportBuilders.MockStoredProcedureReportBuilder;

public class TestSQLGenerator extends TestCase
{

private MockReportBuilder reportBuilder;
private SQLGenerator sqlGenerator;

public void setUp()
{
	reportBuilder = new MockReportBuilder();
	sqlGenerator = new SQLGenerator();
}

public void testGetSql()
{
	String returnedSQL = null;
	try
	{
		returnedSQL = sqlGenerator.getSQL( reportBuilder );
	}
	catch ( SQLException sqlException )
	{
		fail( "Bad 'IN' clause values passed in." );
	}

	StringBuffer expectedSQL = new StringBuffer( "select * " );
	expectedSQL.append( "from tableName " );
	expectedSQL.append( "where column1 = ?" );
	expectedSQL.append( " and column2 <> ?" );
	expectedSQL.append( " and column3 > ?" );
	expectedSQL.append( " and column4 <= ?" );
	expectedSQL.append( " and inClauseStrings in ( 'inOne', 'inTwo' )" );
	expectedSQL.append( " and (mass < ? or mass >= ?)" );
	expectedSQL.append( " and name in ( 'EARTH', 'MARS' )" );
	expectedSQL.append( " order by column1 desc" );

	assertEquals( "Incorrect SQL statement generated", expectedSQL.toString(), returnedSQL );
}

public void testStringInClause()
{
	List< String > listOfStrings = new ArrayList< String >();
	for ( Planet p : Planet.values() )
		listOfStrings.add( "" + p.radius );

	String inClause = null;
	try
	{
		inClause = sqlGenerator.generateInClause( listOfStrings, null );
	}
	catch ( SQLException e )
	{
		fail( e.getMessage() );
	}

	assertEquals( "Bad 'IN' clause format: ", "( '2', '6', '6', '3', '7', '6', '2', '2', '1' )", inClause );
}

public void testBeanStringPropInClause()
{
	List< Planet > listOfStrings = new ArrayList< Planet >();
	for ( Planet p : Planet.values() )
		listOfStrings.add( p );

	String inClause = null;
	try
	{
		inClause = sqlGenerator.generateInClause( listOfStrings, "name" );
	}
	catch ( SQLException e )
	{
		fail( e.getMessage() );
	}

	assertEquals( "Bad 'IN' clause format: ", "( 'MERCURY', 'VENUS', 'EARTH', 'MARS', 'JUPITER', 'SATURN', 'URANUS', 'NEPTUNE', 'PLUTO' )",
					inClause );
}

public void testGetNonStringInClause()
{
	String inClause = null;
	try
	{
		inClause = sqlGenerator.generateInClause( Arrays.asList( Planet.values() ), "radius" );
	}
	catch ( SQLException e )
	{
		fail( e.getMessage() );
	}

	assertEquals( "Bad 'IN' clause format: ", "( 2, 6, 6, 3, 7, 6, 2, 2, 1 )", inClause );
}

public void testEnumEquals()
{
	Planet planetX = Planet.MARS;

	assertEquals( planetX, Planet.MARS );
}

public void testGetStoredProcedureSQL()
{
    MockStoredProcedureReportBuilder storedProcedureReportBuilder = new MockStoredProcedureReportBuilder(); 
    String returnedSQL = sqlGenerator.getStoredProcedureSQL( storedProcedureReportBuilder );
    
    String expectedSQL = "EXEC SomeStoredProc 444, arg2";
    
    assertEquals( "Returned SQL is incorrect", expectedSQL, returnedSQL );
    
}
}
