package biz.firstlook.report.reportFramework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.report.reportBuilders.ReportBuilder;

public class MockReportDisplayBeanDAO implements IReportDisplayBeanDAO
{

public MockReportDisplayBeanDAO()
{
	super();
}

public List< Map > getResults( ReportBuilder reportBuilder )
{
	List<Map> toBeReturned = new ArrayList<Map>();
	Map<String, String> toBePutInToBeReturned = new HashMap<String, String>();
	toBePutInToBeReturned.put( reportBuilder.fromClause(), reportBuilder.fromClause() );
	toBeReturned.add( toBePutInToBeReturned );
	return toBeReturned;
}

public List< Map > getResultsFromStoredProcedure( ReportBuilder reportBuilder )
{
    // method needs to be implemented
	return null;	
}
}
