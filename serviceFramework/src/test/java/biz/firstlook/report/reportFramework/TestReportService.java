package biz.firstlook.report.reportFramework;

import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import biz.firstlook.report.reportBuilders.MockCompositeReportBuilder;
import biz.firstlook.report.reportBuilders.MockSimpleReportBuilder;

public class TestReportService extends TestCase
{

private ReportService reportService;
private MockCompositeReportBuilder compositeReportBuilder;
private MockCompositeReportBuilder compositeReportBuilder1;
private MockCompositeReportBuilder compositeReportBuilder2;

public void setUp()
{
	MockReportDisplayBeanDAO mockReportDisplayBeanDAO = new MockReportDisplayBeanDAO();
	reportService = new ReportService();
	reportService.setReportDisplayBeanDAO( mockReportDisplayBeanDAO );
	
	MockSimpleReportBuilder reportBuilder1 = new MockSimpleReportBuilder();
	reportBuilder1.setTableName("Report 1");
	
	MockSimpleReportBuilder reportBuilder2 = new MockSimpleReportBuilder();
	reportBuilder2.setTableName("Report 2");
	
	MockSimpleReportBuilder reportBuilder3 = new MockSimpleReportBuilder();
	reportBuilder3.setTableName("Report 3");
		
	compositeReportBuilder1 = new MockCompositeReportBuilder();
	compositeReportBuilder1.setCompositeReportName("compositeReport 1");
	compositeReportBuilder1.addReport( reportBuilder1 );
	compositeReportBuilder1.addReport( reportBuilder2 );
	
	compositeReportBuilder = new MockCompositeReportBuilder();
	compositeReportBuilder.setCompositeReportName("compositeReport");
	compositeReportBuilder.addReport( reportBuilder3 );
	compositeReportBuilder.addCompositeReport( compositeReportBuilder1 );
	
	compositeReportBuilder2 = new MockCompositeReportBuilder();
	compositeReportBuilder2.setCompositeReportName("compositeReport 2");
	compositeReportBuilder2.addCompositeReport( compositeReportBuilder1 );
	compositeReportBuilder2.addCompositeReport( compositeReportBuilder );
	
}

public void testOneCompositeReportOneNormalReport()
{
	List<Map> results = reportService.getCompositeResults( compositeReportBuilder );
	Map helper = results.get(0);
	assertTrue( "didn't include report 3", helper.containsKey("Report 3"));
	helper = results.get(1);
	assertTrue( "didn't include compositeReport 1", helper.containsKey("compositeReport 1"));
}

public void testTwoSimpleReports()
{
	List<Map> results = reportService.getCompositeResults( compositeReportBuilder1 );
	Map helper = results.get(0);
	assertTrue( "didn't include report 1", helper.containsKey("Report 1"));
	helper = results.get(1);
	assertTrue( "didn't include report 2", helper.containsKey("Report 2"));
}

public void testTwoCompositeReports()
{
	List<Map> results = reportService.getCompositeResults( compositeReportBuilder2 );
	Map helper = results.get(0);
	assertTrue( "didn't include compositeReport", helper.containsKey("compositeReport"));
	helper = results.get(1);
	assertTrue( "didn't include compositeReport 1", helper.containsKey("compositeReport 1"));

}

}
