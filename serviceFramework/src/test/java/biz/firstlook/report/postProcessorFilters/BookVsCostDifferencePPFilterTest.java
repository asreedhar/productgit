package biz.firstlook.report.postProcessorFilters;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

public class BookVsCostDifferencePPFilterTest extends TestCase
{

public BookVsCostDifferencePPFilterTest( String name )
{
	super( name );
}

public void testPlanningInputs()
{
	Map< String, Object > rowResult = new HashMap< String, Object >();
	rowResult.put( "PrimaryBookValue", "6000" );
	rowResult.put( "UnitCost", "5990" );

	BookVsCostDifferencePPFilter filter = new BookVsCostDifferencePPFilter();
	filter.filterResults( rowResult );

	assertEquals( new BigDecimal( "10" ), rowResult.get( "BookVsCost" ) );
}

public void testNonPlanningInputs()
{
	Map< String, Object > rowResult = new HashMap< String, Object >();
	rowResult.put( "BookValue", new BigDecimal( "6000" ) );
	rowResult.put( "UnitCost", new BigDecimal( "5990" ) );
	
	BookVsCostDifferencePPFilter filter = new BookVsCostDifferencePPFilter();
	filter.filterResults( rowResult );
	
	assertEquals( new BigDecimal( "10" ), rowResult.get( "BookVsCost" ) );
}

public void testNonBigDecimalInputs()
{
	Map< String, Object > rowResult = new HashMap< String, Object >();
	rowResult.put( "BookValue", new Integer( "6000" ) );
	rowResult.put( "UnitCost", new String( "5990.5" ) );
	
	BookVsCostDifferencePPFilter filter = new BookVsCostDifferencePPFilter();
	filter.filterResults( rowResult );
	
	assertEquals( new BigDecimal( "9.5" ), rowResult.get( "BookVsCost" ) );
}

public void testNoBookValue()
{
	Map< String, Object > rowResult = new HashMap< String, Object >();
	rowResult.put( "BookValue", null );
	rowResult.put( "UnitCost", new BigDecimal( "5990" ) );
	
	BookVsCostDifferencePPFilter filter = new BookVsCostDifferencePPFilter();
	filter.filterResults( rowResult );
	
	assertNull( rowResult.get( "BookVsCost" ) );
}

public void testNoUnitCost()
{
	Map< String, Object > rowResult = new HashMap< String, Object >();
	rowResult.put( "BookValue", new BigDecimal( "6000" ) );
	rowResult.put( "UnitCost", null );

	BookVsCostDifferencePPFilter filter = new BookVsCostDifferencePPFilter();
	filter.filterResults( rowResult );

	assertNull( rowResult.get( "BookVsCost" ) );
}

}
