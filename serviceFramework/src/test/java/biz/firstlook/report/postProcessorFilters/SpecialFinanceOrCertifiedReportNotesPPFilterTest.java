package biz.firstlook.report.postProcessorFilters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

public class SpecialFinanceOrCertifiedReportNotesPPFilterTest extends TestCase {
	
	public SpecialFinanceOrCertifiedReportNotesPPFilterTest ( String name )
	{
		super( name );
	}

	public void testAdvertiseNotes ()
	{
		Map< String, Object > rowResult = new HashMap< String, Object >();
		rowResult.put( "retailStrats", "Advertisement" );
		rowResult.put( "notes", "findabletext" );
		rowResult.put( "ThirdParty", "craigs list" );

		SpecialFinanceOrCertifiedReportNotesPPFilter sf = new SpecialFinanceOrCertifiedReportNotesPPFilter();
		sf.filterResults( rowResult );

		assertEquals( "advertisement notes not formatted correctly", "Advertisement craigs list (findabletext)", rowResult.get( "notes" ) );
	}
	
	public void testAdvertiseWithNullNotes ()
	{
		Map< String, Object > rowResult = new HashMap< String, Object >();
		rowResult.put( "retailStrats", "Advertisement" );
		rowResult.put( "notes", null );
		rowResult.put( "ThirdParty", "craigs list" );

		SpecialFinanceOrCertifiedReportNotesPPFilter sf = new SpecialFinanceOrCertifiedReportNotesPPFilter();
		sf.filterResults( rowResult );

		assertEquals( "null advertisement notes bubble up to JSP", "Advertisement craigs list", rowResult.get( "notes" ) );
	}

	public void testNoStrategy ()
	{
		Map< String, Object > rowResult = new HashMap< String, Object >();
		rowResult.put( "retailStrats", "No Strategy" );

		SpecialFinanceOrCertifiedReportNotesPPFilter sf = new SpecialFinanceOrCertifiedReportNotesPPFilter();
		sf.filterResults( rowResult );

		assertEquals( "'No Strategy' not filtered out of notes", "", rowResult.get( "retailStrats" ) );
	}

	public void testServiceDepartment ()
	{
		Map< String, Object > rowResult = new HashMap< String, Object >();
		Date today = new Date();
		rowResult.put( "retailStrats", "Service Department" );
		rowResult.put( "BeginDate", today );

		SpecialFinanceOrCertifiedReportNotesPPFilter sf = new SpecialFinanceOrCertifiedReportNotesPPFilter();
		sf.filterResults( rowResult );
		
		SimpleDateFormat sdf = new SimpleDateFormat( "MM-dd-yyyy" );
		String beginDate = sdf.format( (Date) rowResult.get( "BeginDate" ) );

		assertEquals( "service department notes not formatted correctly", "Service " + beginDate, rowResult.get( "notes" ) );
	}
}
