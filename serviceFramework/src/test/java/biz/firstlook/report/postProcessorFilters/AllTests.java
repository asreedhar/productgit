package biz.firstlook.report.postProcessorFilters;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite( SpecialFinanceOrCertifiedReportNotesPPFilterTest.class );
    suite.addTestSuite( BookVsCostDifferencePPFilterTest.class );
    
    return suite;
}
}