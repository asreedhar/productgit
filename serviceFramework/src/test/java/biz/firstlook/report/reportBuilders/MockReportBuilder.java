package biz.firstlook.report.reportBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.Planet;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class MockReportBuilder extends ReportBuilder
{

public MockReportBuilder()
{
	super();

	addWhereStatement( new SQLWhereSearchStatement( "column1", SQLOperationEnum.EQ, "arg1" ) );
	addWhereStatement( new SQLWhereSearchStatement( "column2", SQLOperationEnum.NOT_EQ, new Integer( 100 ) ) );
	addWhereStatement( new SQLWhereSearchStatement( "column3", SQLOperationEnum.GT, new Date( 1140553161929l ) ) );
	addWhereStatement( new SQLWhereSearchStatement( "column4", SQLOperationEnum.LTE, new Integer( 6 ) ) );

	List< String > listOfStringParams = new ArrayList< String >();
	listOfStringParams.add( "inOne" );
	listOfStringParams.add( "inTwo" );
	addWhereStatement( new SQLWhereSearchStatement( "inClauseStrings", SQLOperationEnum.IN, listOfStringParams ) );

	List< Planet > listOfBeansWithStringProp = new ArrayList< Planet >();
	listOfBeansWithStringProp.add( Planet.EARTH );
	listOfBeansWithStringProp.add( Planet.MARS );
	addWhereStatement( new SQLWhereSearchStatement( "name", SQLOperationEnum.IN, listOfBeansWithStringProp ) );
	
	Set<SQLWhereSearchStatement> orClause = new TreeSet<SQLWhereSearchStatement>();
	orClause.add( new SQLWhereSearchStatement( "mass", SQLOperationEnum.GTE, Planet.URANUS.mass() ) );
	orClause.add( new SQLWhereSearchStatement( "mass", SQLOperationEnum.LT, Planet.MERCURY.mass() ) );
	addWhereStatement( new SQLWhereSearchStatement( "mass", SQLOperationEnum.OR, orClause, null ) );
	
	orderBy = "column1";
	desc = true;
}

@Override
public String fromClause()
{
	return "tableName";
}

@Override
public String getReportName()
{
	return "MockReportBuilder";
}

}
