package biz.firstlook.report.reportBuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.report.compositeReportBuilders.CompositeReportBuilder;

public class MockCompositeReportBuilder extends CompositeReportBuilder
{

private String name;

public MockCompositeReportBuilder()
{
	name = "MockCompsiteReportBuilder";

}

@Override
public String getCompositeReportName()
{
	return name;
}

public void setCompositeReportName( String name )
{
	this.name = name;
}

@Override
public List< Map > mergeReports( Map< String, List< Map >> subReports )
{
	List< Map > toBeReturned = new ArrayList<Map>();
	Iterator keysIter = subReports.keySet().iterator();
	while ( keysIter.hasNext() )
	{
		String element = (String)keysIter.next();
		Map<String, String> addMe = new HashMap<String, String>();
		addMe.put( element, element );
		toBeReturned.add( addMe );
	}
	return toBeReturned;
}

}
