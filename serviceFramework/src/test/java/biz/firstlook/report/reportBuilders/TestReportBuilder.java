package biz.firstlook.report.reportBuilders;

import java.util.Date;

import junit.framework.TestCase;
import biz.firstlook.report.sql.Planet;

public class TestReportBuilder extends TestCase
{

private MockReportBuilder reportBuilder;

public void setUp()
{
	reportBuilder = new MockReportBuilder();
}

public void testGetFilterValues()
{
	Object[] expectedFilterValues = new Object[] { "arg1", new Integer( 100 ), new Date( 1140553161929l ), new Integer( 6 ), Planet.URANUS.mass(), Planet.MERCURY.mass()};
	Object[] filterValuesFromMock = reportBuilder.getFilterValues();

	assertEquals( "Incorrect number of filter values:", expectedFilterValues.length, filterValuesFromMock.length );
	
	for( int i = 0; i < expectedFilterValues.length; i++ )
	{	
		Object expected = expectedFilterValues[i];
		// need to do this because result order is not garaunteed
		boolean found = false;
		for( int j = 0; j < filterValuesFromMock.length; j++ )
		{	
			Object actual = filterValuesFromMock[j];
			if (actual.equals(expected)) {
				found = true;
				break;
			}
		}
		assertTrue("Filter Values didn't match", found);
	}
}
}
