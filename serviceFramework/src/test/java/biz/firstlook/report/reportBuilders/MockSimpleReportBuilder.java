package biz.firstlook.report.reportBuilders;


public class MockSimpleReportBuilder extends ReportBuilder
{

private String name;

public MockSimpleReportBuilder()
{
	super();
	name = "MockSimpleReportBuilder";
}

@Override
public String fromClause()
{
	return name;
}

public void setTableName( String name )
{
	this.name = name;
}

@Override
public String getReportName()
{
	return name;
}

}
