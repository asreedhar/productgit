package biz.firstlook.report.reportBuilders;

public class MockStoredProcedureReportBuilder extends ReportBuilder 
{
	public MockStoredProcedureReportBuilder()
	{
	    super();
	}

	public String fromClause() 
	{
		return "SomeStoredProc 444, arg2";
	}

	public String getReportName() 
	{
		return "MockReport";
	}

}
