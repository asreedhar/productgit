package biz.firstlook.report.compositeReportBuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.report.compositeReportBuilders.InventoryOverviewBodyCompositeReportBuilder.InventoryOverviewBodyReportEnum;

public class TestInventoryOverviewBodyCompositeReportBuilder extends TestCase
{

private Map< String, List< Map >> mergedResults;
private InventoryOverviewBodyCompositeReportBuilder inventoryOverviewBodyCompositeReportBuilderBySegment;
private InventoryOverviewBodyCompositeReportBuilder inventoryOverviewBodyCompositeReportBuilderByAlphabetic;
private List< Map > inventoryDetailReportResults;
private List< Map > performanceBubbleResults;

public TestInventoryOverviewBodyCompositeReportBuilder()
{
	super();
}

public void setUp()
{
	inventoryDetailReportResults = new ArrayList< Map >();
	Map< String, String > inventoryLineItem1 = new HashMap< String, String >();
	inventoryLineItem1.put( "VehicleGroupingID", "101509" );
	inventoryLineItem1.put( "AgeInDays", "36" );
	inventoryLineItem1.put( "VehicleYear", "2004" );
	inventoryLineItem1.put( "VehicleDescription", "SILVERADO / SIERRA 1500 TRUCK C1500" );
	inventoryLineItem1.put( "Color", "GRAY" );
	inventoryLineItem1.put( "Mileage", "19368" );
	inventoryLineItem1.put( "ListPrice", "23995.00" );
	inventoryLineItem1.put( "UnitCost", "17583.58" );
	inventoryLineItem1.put( "BookVsCost", "641.42" );
	inventoryLineItem1.put( "TradeOrPurchase", "1" );
	inventoryLineItem1.put( "StockNumber", "P3895" );

	Map< String, String > inventoryLineItem2 = new HashMap< String, String >();
	inventoryLineItem2.put( "VehicleGroupingID", "101509" );
	inventoryLineItem2.put( "AgeInDays", "22" );
	inventoryLineItem2.put( "VehicleYear", "2005" );
	inventoryLineItem2.put( "VehicleDescription", "SILVERADO / SIERRA 1500 TRUCK C1500" );
	inventoryLineItem2.put( "Color", "GREEN" );
	inventoryLineItem2.put( "Mileage", "5000" );
	inventoryLineItem2.put( "ListPrice", "500.00" );
	inventoryLineItem2.put( "UnitCost", "8050.58" );
	inventoryLineItem2.put( "BookVsCost", "500.00" );
	inventoryLineItem2.put( "TradeOrPurchase", "1" );
	inventoryLineItem2.put( "StockNumber", "P3892" );

	Map< String, String > oddBallINventoryLineItem = new HashMap< String, String >();
	oddBallINventoryLineItem.put( "VehicleGroupingID", "101510" );
	oddBallINventoryLineItem.put( "AgeInDays", "17" );
	oddBallINventoryLineItem.put( "VehicleYear", "2005" );
	oddBallINventoryLineItem.put( "VehicleDescription", "SILVERADO / SIERRA 1500 TRUCK C1500" );
	oddBallINventoryLineItem.put( "Color", "GREEN" );
	oddBallINventoryLineItem.put( "Mileage", "5000" );
	oddBallINventoryLineItem.put( "ListPrice", "500.00" );
	oddBallINventoryLineItem.put( "UnitCost", "8050.58" );
	oddBallINventoryLineItem.put( "BookVsCost", "500.00" );
	oddBallINventoryLineItem.put( "TradeOrPurchase", "1" );
	oddBallINventoryLineItem.put( "StockNumber", "P3892" );

	Map< String, String > oddBallINventoryLineItem2 = new HashMap< String, String >();
	oddBallINventoryLineItem2.put( "VehicleGroupingID", "101511" );
	oddBallINventoryLineItem2.put( "AgeInDays", "17" );
	oddBallINventoryLineItem2.put( "VehicleYear", "2005" );
	oddBallINventoryLineItem2.put( "VehicleDescription", "SILVERADO / SIERRA 1500 TRUCK C1500" );
	oddBallINventoryLineItem2.put( "Color", "GREEN" );
	oddBallINventoryLineItem2.put( "Mileage", "5000" );
	oddBallINventoryLineItem2.put( "ListPrice", "500.00" );
	oddBallINventoryLineItem2.put( "UnitCost", "8050.58" );
	oddBallINventoryLineItem2.put( "BookVsCost", "500.00" );
	oddBallINventoryLineItem2.put( "TradeOrPurchase", "1" );
	oddBallINventoryLineItem2.put( "StockNumber", "P3892" );

	inventoryDetailReportResults.add( inventoryLineItem1 );
	inventoryDetailReportResults.add( inventoryLineItem2 );
	inventoryDetailReportResults.add( oddBallINventoryLineItem );
	inventoryDetailReportResults.add( oddBallINventoryLineItem2 );

	performanceBubbleResults = new ArrayList< Map >();
	Map< String, String > performanceBubbleLineItem1 = new HashMap< String, String >();

	performanceBubbleLineItem1.put( "AGP", "1575.00" );
	performanceBubbleLineItem1.put( "UnitsSold", "9" );
	performanceBubbleLineItem1.put( "AvgDaysToSale", "23" );
	performanceBubbleLineItem1.put( "NoSaleUnits", "3" );
	performanceBubbleLineItem1.put( "VehicleGroupingID", "101509" );

	performanceBubbleResults.add( performanceBubbleLineItem1 );

	mergedResults = new HashMap< String, List< Map >>();
	mergedResults.put( "InventoryDetailReport", inventoryDetailReportResults );
	mergedResults.put( "PerformanceBubbleReport", performanceBubbleResults );

	inventoryOverviewBodyCompositeReportBuilderBySegment = new InventoryOverviewBodyCompositeReportBuilder( InventoryOverviewBodyReportEnum.SEGMENT,
																											new Integer( 100147 ),
																											PeriodEnum.TWENTYSIX_WEEKS,
																											new Integer( 2 ),
																											true );

	inventoryOverviewBodyCompositeReportBuilderByAlphabetic = new InventoryOverviewBodyCompositeReportBuilder(	InventoryOverviewBodyReportEnum.SEGMENT,
																												new Integer( 100147 ),
																												PeriodEnum.TWENTYSIX_WEEKS,
																												new Integer( 2 ),
																												true );

}

@SuppressWarnings( "unchecked" )
public void testMergeBySegmentReport()
{
	List< Map > results = inventoryOverviewBodyCompositeReportBuilderBySegment.mergeReports( mergedResults );

	assertEquals( "the results list size wrong!", 1, results.size() );

	Map firstLineItem = results.get( 0 );
	assertNotNull( "map entry was null!", firstLineItem );

	Map< String, String > bubbleReport = (Map< String, String >)firstLineItem.get( "performanceBubble" );
	List< Map > inventoryLineItem = (List< Map >)firstLineItem.get( "InventoryLineItems" );

	assertEquals( "number of entries in inventoryLIneItem is wrong", 2, inventoryLineItem.size() );

	Map< String, String > firstInventoryLineItem = (Map< String, String >)inventoryLineItem.get( 0 );

	assertNotNull( "didn't populate line items correctly", firstInventoryLineItem );

	assertEquals( "didn't match on vehicleGroupingId correctly", firstInventoryLineItem.get( "VehicleGroupingID" ),
					bubbleReport.get( "VehicleGroupingID" ) );
}

@SuppressWarnings( "unchecked" )
public void testMergeByAlphabeticReport()
{
	Map< String, String > performanceBubbleLineItem2 = new HashMap< String, String >();
	Map< String, String > performanceBubbleLineItem3 = new HashMap< String, String >();

	performanceBubbleLineItem2.put( "AGP", "1575.00" );
	performanceBubbleLineItem2.put( "UnitsSold", "9" );
	performanceBubbleLineItem2.put( "AvgDaysToSale", "23" );
	performanceBubbleLineItem2.put( "NoSaleUnits", "3" );
	performanceBubbleLineItem2.put( "VehicleGroupingID", "101510" );

	performanceBubbleLineItem3.put( "AGP", "1575.00" );
	performanceBubbleLineItem3.put( "UnitsSold", "9" );
	performanceBubbleLineItem3.put( "AvgDaysToSale", "23" );
	performanceBubbleLineItem3.put( "NoSaleUnits", "3" );
	performanceBubbleLineItem3.put( "VehicleGroupingID", "101511" );

	performanceBubbleResults = mergedResults.get( "PerformanceBubbleReport" );
	performanceBubbleResults.add( performanceBubbleLineItem2 );
	performanceBubbleResults.add( performanceBubbleLineItem3 );
	mergedResults.put( "PerformanceBubbleReport", performanceBubbleResults );

	List< Map > results = inventoryOverviewBodyCompositeReportBuilderByAlphabetic.mergeReports( mergedResults );

	assertEquals( "the results list size wrong!", 3, results.size() );

	Map firstLineItem = results.get( 0 );
	assertNotNull( "map entry was null!", firstLineItem );

	Map< String, String > bubbleReport = (Map< String, String >)firstLineItem.get( "performanceBubble" );
	List< Map > inventoryLineItem = (List< Map >)firstLineItem.get( "InventoryLineItems" );

	assertEquals( "number of entries in inventoryLIneItem is wrong", 2, inventoryLineItem.size() );

	Map< String, String > firstInventoryLineItem = (Map< String, String >)inventoryLineItem.get( 0 );

	assertNotNull( "didn't populate line items correctly", firstInventoryLineItem );

	assertEquals( "didn't match on vehicleGroupingId correctly", firstInventoryLineItem.get( "VehicleGroupingID" ),
					bubbleReport.get( "VehicleGroupingID" ) );

	Map secondLineItem = results.get( 1 );
	assertNotNull( "map entry was null!", secondLineItem );

	bubbleReport = (Map< String, String >)firstLineItem.get( "performanceBubble" );
	inventoryLineItem = (List< Map >)firstLineItem.get( "InventoryLineItems" );

	assertNotNull( "didn't match bubble correctly", bubbleReport );
	assertNotNull( "didn't match inventoryLineItems correctly", inventoryLineItem );

	Map thirdLineItem = results.get( 1 );
	assertNotNull( "map entry was null!", thirdLineItem );

	bubbleReport = (Map< String, String >)firstLineItem.get( "performanceBubble" );
	inventoryLineItem = (List< Map >)firstLineItem.get( "InventoryLineItems" );

	assertNotNull( "didn't match bubble correctly", bubbleReport );
	assertNotNull( "didn't match inventoryLineItems correctly", inventoryLineItem );

}

public void testEmptyResults()
{
	List< Map > results = inventoryOverviewBodyCompositeReportBuilderBySegment.mergeReports( new HashMap< String, List< Map > >() );

	Map emptyMap = new HashMap();
	List< Map > emptyList = new ArrayList< Map >();
	emptyList.add( emptyMap );
	assertEquals( "didn't return expected empty map", emptyList, results );
}

public void testAverageAgeInDaysCalulator()
{
	Map< String, Integer > LineItem1 = new HashMap< String, Integer >();
	LineItem1.put( "AgeInDays", new Integer( 36 ) );

	Map< String, Integer > LineItem2 = new HashMap< String, Integer >();
	LineItem2.put( "AgeInDays", new Integer( 22 ) );

	Map< String, Integer > LineItem3 = new HashMap< String, Integer >();
	LineItem3.put( "AgeInDays", new Integer( 17 ) );

	inventoryDetailReportResults = new ArrayList< Map >();
	inventoryDetailReportResults.add( LineItem1 );
	inventoryDetailReportResults.add( LineItem2 );
	inventoryDetailReportResults.add( LineItem3 );

	int avgAge = InventoryOverviewBodyCompositeReportBuilder.getAvgAgeInDays( inventoryDetailReportResults );
	assertEquals( "wrong avg age!!", 25, avgAge );

	Map< String, String > lineItem4 = new HashMap< String, String >();
	lineItem4.put( "AgeInDays", "string" );

	inventoryDetailReportResults.add( lineItem4 );

	avgAge = InventoryOverviewBodyCompositeReportBuilder.getAvgAgeInDays( inventoryDetailReportResults );
	assertEquals( "didn't handle case where a vehicle didn't have an age!!", 0, avgAge );
}

}
