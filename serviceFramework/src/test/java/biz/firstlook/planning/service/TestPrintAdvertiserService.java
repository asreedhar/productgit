package biz.firstlook.planning.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;

import junit.framework.TestCase;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.planning.entity.MockInventoryPlanEventDAO;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserAdBuilderOption;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookup;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookupEnum;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;

public class TestPrintAdvertiserService extends TestCase
{

private PrintAdvertiserService printAdvertiserService;

public void setUp()
{
	printAdvertiserService = new PrintAdvertiserService();
}

public void tearDown()
{}

public void testGetSelectedOptionsAsArrayEverythingChecked()
{
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	List< PrintAdvertiserOptionLookup > theOptionsLookUp = new ArrayList< PrintAdvertiserOptionLookup >();
	
	for (int c = 1; c <= 7; c++)
	{
		theOptionsLookUp.add( TestPrintAdvertiserAdBuilder.getPrintAdvertiserOption(
		                        PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
	}
	theOptions = printAdvertiserService.makeDefaultListOfOptions( theOptionsLookUp );
	
	String[] selectedOptions = printAdvertiserService.getSelectedOptionsAsArray( theOptions );
	String[] exptectSelectedOptions = {"1", "2", "3", "4", "5", "6", "7"};

	assertEquals(" sizes of 2 arrays are off", exptectSelectedOptions.length, selectedOptions.length );
	for (int c = 0; c < exptectSelectedOptions.length; c++)
		assertEquals(" full set of selected options not populated correctly ", exptectSelectedOptions[c], selectedOptions[c] );
}

public void testGetSelectedOptionsAsArrayEvensChecked()
{
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	List< PrintAdvertiserOptionLookup > theOptionsLookUp = new ArrayList< PrintAdvertiserOptionLookup >();
	
	for (int c = 1; c <= 7; c++)
	{
		theOptionsLookUp.add( TestPrintAdvertiserAdBuilder.getPrintAdvertiserOption(
		                                                                            PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
	}
	theOptions = printAdvertiserService.makeDefaultListOfOptions( theOptionsLookUp );
	
	theOptions.get( 0 ).setStatus( Boolean.FALSE );
	theOptions.get( 2 ).setStatus( Boolean.FALSE );
	theOptions.get( 4 ).setStatus( Boolean.FALSE );
	theOptions.get( 6 ).setStatus( Boolean.FALSE );
	
	String[] selectedOptions = printAdvertiserService.getSelectedOptionsAsArray( theOptions );
	String[] exptectSelectedOptions = {"2","4", "6"};
	
	assertEquals(" sizes of 2 arrays are off", exptectSelectedOptions.length, selectedOptions.length );
	for (int c = 0; c < exptectSelectedOptions.length; c++)
		assertEquals(" full set of selected options not populated correctly ", exptectSelectedOptions[c], selectedOptions[c] );
}

public void testRetreivePrintAdvertisingHisotry()
{
	IInventoryPlanEventDAO planEventDAO = new MockInventoryPlanEventDAO();
	printAdvertiserService.setInventoryPlanEventDAO( planEventDAO );
	
	Date twoDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -2 );
	Date threeDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -3 );
	Date fourDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -4 );
	Date fiveDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -5 );
	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy");
	
	// for the expected results, look at the retrievePrintAdvertiserEventsGroupedByEndDate method on MockInventoryPlanEventDAO
	Map< String, List< String >> adHistory = printAdvertiserService.retreivePrintAdvertisingHisotry( 123456 );
	assertEquals( " map was the wrong size!", 3, adHistory.size() );
	
	assertNotNull( adHistory.get( dateFormat.format( twoDaysAgo) ) );
	List< String > twoDaysAgoAdvertisers = adHistory.get( dateFormat.format( twoDaysAgo) );
	assertEquals( "number of entries in 2 day old queue is wrong", 1, twoDaysAgoAdvertisers.size() );
	assertEquals( " twoDayOld history wrong" , "thirdParty 1", twoDaysAgoAdvertisers.get( 0 ) );
	
	assertNotNull( adHistory.get( dateFormat.format( threeDaysAgo) ) );
	List< String > threeDaysAgoAdvertisers = adHistory.get( dateFormat.format( threeDaysAgo) );
	assertEquals( "number of entries in 3 day old queue is wrong", 3, threeDaysAgoAdvertisers.size() );
	assertEquals( " 3 DayOld history wrong" , "thirdParty 1", threeDaysAgoAdvertisers.get( 0 ) );
	assertEquals( " 3 DayOld history wrong" , "thirdParty 2", threeDaysAgoAdvertisers.get( 1 ) );
	assertEquals( " 3 DayOld history wrong" , "thirdParty 3", threeDaysAgoAdvertisers.get( 2 ) );

	assertNotNull( adHistory.get( dateFormat.format( fourDaysAgo) ) );
	List< String > fourDaysAgoAdvertisers = adHistory.get( dateFormat.format( fourDaysAgo) );
	assertEquals( "number of entries in 3 day old queue is wrong", 2, fourDaysAgoAdvertisers.size() );
	assertEquals( " 3 DayOld history wrong" , "thirdParty 1", fourDaysAgoAdvertisers.get( 0 ) );
	assertEquals( " 3 DayOld history wrong" , "thirdParty 4", fourDaysAgoAdvertisers.get( 1 ) );
	
	assertNull( adHistory.get( dateFormat.format( fiveDaysAgo) ) );
	
	
}

}
