package biz.firstlook.planning.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.InventoryBucketRange;
import biz.firstlook.main.commonDAOs.IInventoryBucketRangeDAO;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.MockInventoryDAO;
import biz.firstlook.planning.entity.MockInventoryPlanEventDAO;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.service.InventoryPlanningUtilService.SaveAction;
import biz.firstlook.planning.transactionFramework.ITransactionDAO;

public class TestInventoryPlanningService extends TestCase
{
private Date yesterday = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -1 ), Calendar.DATE );
private Date tomorrow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 1 ), Calendar.DATE );
private Date today = DateUtils.truncate( new Date(), Calendar.DATE );
private Date threeDaysAgo = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -3 ), Calendar.DATE );

public TestInventoryPlanningService()
{
	super();
}

public TestInventoryPlanningService( String name )
{
	super( name );
}

protected void setUp()
{
}

protected void tearDown() throws Exception
{
	super.tearDown();
}





/**
 * all of an inventory's current events should be of the same objective, 
 * so the current objective should be the objective associated with any
 * of the events; also, if there are no current events, then there is
 * no current objective
 */
public void testDetermineObjective()
{
    InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();	
    InventoryPlanEvent event = new InventoryPlanEvent();
    event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
    
    List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
    events.add( event );
    
    assertEquals( PlanObjectiveEnum.WHOLESALE , service.determineObjective( events ) );
}

public void testDetermineObjectiveNoEvents()
{
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
    assertEquals( PlanObjectiveEnum.UNKNOWN, service.determineObjective( new ArrayList<InventoryPlanEvent>() ) );
}

/**
 * Service department events (detail and service) should be ignored
 * when we determine the current objective.
 */
public void testDetermineObjectiveWithServiceEvents()
{
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();	
    InventoryPlanEvent auctionEvent = new InventoryPlanEvent();
    auctionEvent.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
    
    InventoryPlanEvent detailEvent = new InventoryPlanEvent();
    detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
    
    //InventoryPlanEvent reminderDateEvent = new InventoryPlanEvent();
    //detailEvent.setEventTypeId( PlanningEventTypeEnum.REMINDER_DATE.getId() );
    
    List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
    events.add( detailEvent );
    events.add( auctionEvent );
    //events.add( reminderDateEvent );
    
    assertEquals( PlanObjectiveEnum.WHOLESALE, service.determineObjective( events ) );
}

/**
 * Service department events (detail and service) should be ignored
 * when we determine the current objective.
 */
public void testDetermineObjectiveOnlyServiceEvent()
{
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
    
    InventoryPlanEvent detailEvent = new InventoryPlanEvent();
    detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
    
    List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
    events.add( detailEvent );
    
    assertEquals( PlanObjectiveEnum.UNKNOWN, service.determineObjective( events ) );
}

public void testDetermineObjectiveWithRepriceEvent()
{
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();	
	InventoryPlanEvent repriceEvent = new InventoryPlanEvent();
	repriceEvent.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
	
	InventoryPlanEvent auctionEvent = new InventoryPlanEvent();
	auctionEvent.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( repriceEvent );
	events.add( auctionEvent );
	
	assertEquals( PlanObjectiveEnum.WHOLESALE, service.determineObjective( events ) );	
}

public void testDetermineObjectiveOnlyRepriceEvent()
{
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();	
    InventoryPlanEvent repriceEvent = new InventoryPlanEvent();
    repriceEvent.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
    
    List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
    events.add( repriceEvent );
    
    assertEquals( PlanObjectiveEnum.RETAIL, service.determineObjective( events ) );	
}

public void testExtractCurrentEventsRetail()
{
	InventoryPlanEvent currentAdvertismentEvent = new InventoryPlanEvent();
	currentAdvertismentEvent.setId( 1 );
	currentAdvertismentEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	currentAdvertismentEvent.setBeginDate( threeDaysAgo );
	currentAdvertismentEvent.setEndDate( tomorrow );

	InventoryPlanEvent notCurrentAdvertismentEvent = new InventoryPlanEvent();
	notCurrentAdvertismentEvent.setId( 2 );
	notCurrentAdvertismentEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	notCurrentAdvertismentEvent.setBeginDate( threeDaysAgo );
	notCurrentAdvertismentEvent.setEndDate( yesterday );

	InventoryPlanEvent currentPromotionEvent = new InventoryPlanEvent();
	currentPromotionEvent.setId( 3 );
	currentPromotionEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	currentPromotionEvent.setBeginDate( threeDaysAgo );
	currentPromotionEvent.setEndDate( tomorrow );

	InventoryPlanEvent notCurrentPromotionEvent = new InventoryPlanEvent();
	notCurrentPromotionEvent.setId( 4 );
	notCurrentPromotionEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	notCurrentPromotionEvent.setBeginDate( threeDaysAgo );
	notCurrentPromotionEvent.setEndDate( yesterday );

	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.add( currentAdvertismentEvent );
	allEvents.add( notCurrentAdvertismentEvent );
	allEvents.add( currentPromotionEvent );
	allEvents.add( notCurrentAdvertismentEvent );

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	List< InventoryPlanEvent > currentRetailEvents = service.extractCurrentPlanEvents( allEvents, new Integer( 123 ), true);

	assertEquals( 2, currentRetailEvents.size() );
	assertTrue( currentRetailEvents.contains( currentAdvertismentEvent ) );
	assertTrue( currentRetailEvents.contains( currentPromotionEvent ) );
}

public void testExtractCurrentEventsRetailNoneCurrent()
{
	InventoryPlanEvent notCurrentAdvertismentEvent = new InventoryPlanEvent();
	notCurrentAdvertismentEvent.setId( 2 );
	notCurrentAdvertismentEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	notCurrentAdvertismentEvent.setBeginDate( threeDaysAgo );
	notCurrentAdvertismentEvent.setEndDate( yesterday );

	InventoryPlanEvent notCurrentPromotionEvent = new InventoryPlanEvent();
	notCurrentPromotionEvent.setId( 4 );
	notCurrentPromotionEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	notCurrentPromotionEvent.setBeginDate( threeDaysAgo );
	notCurrentPromotionEvent.setEndDate( yesterday );

	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.add( notCurrentAdvertismentEvent );
	allEvents.add( notCurrentAdvertismentEvent );

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	List< InventoryPlanEvent > currentRetailEvents = service.extractCurrentPlanEvents( allEvents, new Integer( 123 ), true );

	assertEquals( 1, currentRetailEvents.size() );
	assertEquals( PlanningEventTypeEnum.RETAIL_NO_STRATEGY, currentRetailEvents.get( 0 ).getEventTypeEnum() );
}

public void testExtractCurrentEventsWholesale()
{
	InventoryPlanEvent currentAuctionEvent = new InventoryPlanEvent();
	currentAuctionEvent.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	currentAuctionEvent.setBeginDate( threeDaysAgo );
	currentAuctionEvent.setEndDate( tomorrow );

	InventoryPlanEvent notCurrentAuctionEvent = new InventoryPlanEvent();
	notCurrentAuctionEvent.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	notCurrentAuctionEvent.setBeginDate( threeDaysAgo ); 
	notCurrentAuctionEvent.setEndDate( yesterday );

	InventoryPlanEvent currentWholesalerEvent = new InventoryPlanEvent();
	currentWholesalerEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
	currentWholesalerEvent.setBeginDate( threeDaysAgo );
	currentWholesalerEvent.setEndDate( tomorrow );

	InventoryPlanEvent notCurrentWholesalerEvent = new InventoryPlanEvent();
	notCurrentWholesalerEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
	notCurrentWholesalerEvent.setBeginDate( threeDaysAgo );
	notCurrentWholesalerEvent.setEndDate( yesterday );

	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.add( currentAuctionEvent );
	allEvents.add( notCurrentAuctionEvent );
	allEvents.add( currentWholesalerEvent );
	allEvents.add( notCurrentWholesalerEvent );

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	List< InventoryPlanEvent > currentWholesaleEvents = service.extractCurrentPlanEvents( allEvents, new Integer( 123 ), true );

	assertEquals( 2, currentWholesaleEvents.size() );
	assertTrue( currentWholesaleEvents.contains( currentAuctionEvent ) );
	assertTrue( currentWholesaleEvents.contains( currentWholesalerEvent ) );
}

public void testExtractCurrentEventsWholesaleNoneCurrent()
{
	InventoryPlanEvent notCurrentAuctionEvent = new InventoryPlanEvent();
	notCurrentAuctionEvent.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	notCurrentAuctionEvent.setBeginDate( threeDaysAgo );
	notCurrentAuctionEvent.setEndDate( yesterday );

	InventoryPlanEvent notCurrentWholesalerEvent = new InventoryPlanEvent();
	notCurrentWholesalerEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
	notCurrentWholesalerEvent.setBeginDate( threeDaysAgo );
	notCurrentWholesalerEvent.setEndDate( yesterday );

	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.add( notCurrentAuctionEvent );
	allEvents.add( notCurrentWholesalerEvent );

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	List< InventoryPlanEvent > currentWholesaleEvents = service.extractCurrentPlanEvents( allEvents, new Integer( 123 ), true );

	assertEquals( 1, currentWholesaleEvents.size() );
	assertEquals( PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY, currentWholesaleEvents.get( 0 ).getEventTypeEnum() );

}


public void testExtractCurrentEventsSold()
{
	InventoryPlanEvent retailEvent = new InventoryPlanEvent();
	retailEvent.setEventTypeId( PlanningEventTypeEnum.RETAIL.getId() );
	//retailEvent.setEndDate( yesterday );

	InventoryPlanEvent wholesaleEvent = new InventoryPlanEvent();
	wholesaleEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALE.getId() );
	//wholesalerEvent.setEndDate( yesterday );

	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.add( retailEvent );
	allEvents.add( wholesaleEvent );

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	List< InventoryPlanEvent > currentSoldEvents = service.extractCurrentPlanEvents( allEvents, new Integer( 123 ), true);

	assertEquals( 2, currentSoldEvents.size() );
}

public void testExtractCurrentEventsOther()
{
	InventoryPlanEvent otherEvent = new InventoryPlanEvent();
	otherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER.getId() );

	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.add( otherEvent );

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	List< InventoryPlanEvent > currentOtherEvents = service.extractCurrentPlanEvents( allEvents, new Integer( 123 ), true );

	assertEquals( 1, currentOtherEvents.size() );
}

public void testConvertEventToJSONAdvertise()
{
	Calendar beginDateCal = Calendar.getInstance();
	beginDateCal.set( 2006, 6, 5 );
	
	Calendar endDateCal = Calendar.getInstance();
	endDateCal.set( 2006, 6, 10 );
	
	Calendar plannedDateCal = Calendar.getInstance();
	plannedDateCal.set( 2006, 6, 3 );
	
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setId( 333 );
	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );        
	event.setBeginDate( beginDateCal.getTime() );
	event.setEndDate( endDateCal.getTime() );
	event.setLastModified( plannedDateCal.getTime() );
	PrintAdvertiser_InventoryThirdParty thirdParty = new PrintAdvertiser_InventoryThirdParty();
	thirdParty.setName( "print advertiser 1" );
	event.setInventoryThirdParty( thirdParty );
	event.setNotes( "ad text" );
	
	JSONObject jsonPlan = new JSONObject();
	
	try
	{
		jsonPlan.put( "advertise", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "advertise" ).get( 0 );
		assertNotNull( testEvent );
			
		assertEquals( 333, testEvent.getInt( "id" ) );
		assertEquals( "print advertiser 1", testEvent.getString( "thirdParty" ) );
		assertEquals( "ad text", testEvent.getString( "adText" ) );
		
		Calendar testCal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "planUntilDate" ) ) );
		assertEquals( 10, testCal.get( Calendar.DAY_OF_MONTH ) );
		
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
	catch ( ParseException pe )
	{
	    fail( pe.getMessage() );
	}
}

public void testConvertEventToJSONLotPromote()
{
	Calendar beginDateCal = Calendar.getInstance();
	beginDateCal.set( 2006, 6, 5 );
	
	Calendar endDateCal = Calendar.getInstance();
	endDateCal.set( 2006, 6, 15 );

	Calendar plannedDateCal = Calendar.getInstance();
	plannedDateCal.set( 2006, 6, 3 );	
	
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setId( 334 );
	event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );        
	event.setBeginDate( beginDateCal.getTime() );
	event.setEndDate( endDateCal.getTime() );
	event.setLastModified( plannedDateCal.getTime() );
	event.setNotes( "lot promote notes" );
	
	JSONObject jsonPlan = new JSONObject();
	
	try
	{
		jsonPlan.put( "lotPromote", new JSONArray() );
		
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "lotPromote" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 334, testEvent.getInt( "id" ) );
		assertEquals( "lot promote notes", testEvent.getString( "strategyNotes" ) );
		
		Calendar testCal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "beginDate" ) ) );
		assertEquals( 5, testCal.get( Calendar.DAY_OF_MONTH ) );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "endDate" ) ) );
		assertEquals( 15, testCal.get( Calendar.DAY_OF_MONTH ) );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "plannedDate" ) ) );
		assertEquals( 3, testCal.get( Calendar.DAY_OF_MONTH ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
	catch ( ParseException pe )
	{
	    fail( pe.getMessage() );
	}
}

public void testConvertEventToJSONSPIFF()
{  
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setId( 335 );
	event.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	event.setNotes( "spiff notes" );
	
	JSONObject jsonPlan = new JSONObject();
	try
	{	
		jsonPlan.put( "spiff", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "spiff" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 335, testEvent.getInt( "id" ) );
		assertEquals( "spiff notes", testEvent.getString( "spiffNotes" ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
}

public void testConvertEventToJSONRetailOther()
{  
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setId( 336 );
	event.setEventTypeId( PlanningEventTypeEnum.OTHER_RETAIL.getId() );
	event.setNotes( "retail other notes" );
	
	JSONObject jsonPlan = new JSONObject();
	
	try
	{	
		jsonPlan.put( "retailOther", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "retailOther" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 336, testEvent.getInt( "id" ) );
		assertEquals( "retail other notes", testEvent.getString( "retailOtherNotes" ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
}

public void testConvertEventToJSONAuction()
{  
	Calendar endDateCal = Calendar.getInstance();
	endDateCal.set( 2006, 6, 15 );
	
	Calendar plannedDateCal = Calendar.getInstance();
	plannedDateCal.set( 2006, 6, 3 );
	
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setId( 337 );
	event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	event.setInventoryThirdParty(new InventoryThirdParty());
	event.getInventoryThirdParty().setName( "Joe Auction" );
	event.setEndDate( endDateCal.getTime() );
	event.setLastModified( plannedDateCal.getTime() );
	event.setNotes( "auction notes" );
	event.setNotes2( "a location" );
	
	JSONObject jsonPlan = new JSONObject();
	
	try
	{	
		jsonPlan.put( "auction", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "auction" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 337, testEvent.getInt( "id" ) );
		assertEquals( "Joe Auction", testEvent.getString( "auctionName" ) );
		assertEquals( "auction notes", testEvent.getString( "strategyNotes" ) );
		assertEquals( "a location", testEvent.getString( "location" ) );
		
		Calendar testCal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "endDate" ) ) );
		assertEquals( 15, testCal.get( Calendar.DAY_OF_MONTH ) );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "plannedDate" ) ) );
		assertEquals( 3, testCal.get( Calendar.DAY_OF_MONTH ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
	catch ( ParseException pe )
	{
	    fail( pe.getMessage() );
	}
}

public void testConvertEventToJSONWholesaler()
{  
	Calendar endDateCal = Calendar.getInstance();
	endDateCal.set( 2006, 6, 15 );
	
	Calendar plannedDateCal = Calendar.getInstance();
	plannedDateCal.set( 2006, 6, 3 );
	
	InventoryPlanEvent event = new InventoryPlanEvent();
    event.setId( 338 );
    event.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
    event.setInventoryThirdParty( new InventoryThirdParty() );
    event.getInventoryThirdParty().setName( "Joe Wholesaler" );
    event.setEndDate( endDateCal.getTime() );
    event.setLastModified( plannedDateCal.getTime() );
    event.setNotes( "wholesaler notes" );

	JSONObject jsonPlan = new JSONObject();
	
	try
	{	
		jsonPlan.put( "wholesaler", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "wholesaler" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 338, testEvent.getInt( "id" ) );
		assertEquals( "Joe Wholesaler", testEvent.getString( "wholesalerName" ) );
		//assertEquals( endDate, testEvent.get( "date" ) );
        assertEquals( "wholesaler notes", testEvent.getString( "strategyNotes" ) );

		Calendar testCal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "endDate" ) ) );
		assertEquals( 15, testCal.get( Calendar.DAY_OF_MONTH ) );
		
		testCal.setTime( dateFormat.parse( testEvent.getString( "plannedDate" ) ) );
		assertEquals( 3, testCal.get( Calendar.DAY_OF_MONTH ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
	catch ( ParseException pe )
	{
	    fail( pe.getMessage() );
	}
}

public void testConvertEventToJSONWholesaleOther()
{  
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setId( 336 );
	event.setEventTypeId( PlanningEventTypeEnum.OTHER_WHOLESALE.getId() );
	event.setNotes( "wholesale other notes" );
	
	JSONObject jsonPlan = new JSONObject();
	
	try
	{	
		jsonPlan.put( "wholesaleOther", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "wholesaleOther" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 336, testEvent.getInt( "id" ) );
		assertEquals( "wholesale other notes", testEvent.getString( "wholesaleOtherNotes" ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
}

public void testConvertEventToJSONSoldRetail()
{
    InventoryPlanEvent event = new InventoryPlanEvent();
    event.setId( 336 );
    event.setNotes( "some notes" );
    event.setNotes2( "Bob" );

    event.setEventTypeId( PlanningEventTypeEnum.RETAIL.getId() );
    event.setBeginDate( new Date() );

	JSONObject jsonPlan = new JSONObject();
	
	try
	{	
		jsonPlan.put( "sold", new JSONArray() );
		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
		
		JSONObject testEvent = (JSONObject) jsonPlan.getJSONArray( "sold" ).get( 0 );
		assertNotNull( testEvent );
		assertEquals( 336, testEvent.getInt( "id" ) );
		assertEquals( "some notes", testEvent.getString( "strategyNotes" ) );
		assertEquals( "Bob", testEvent.getString( "name" ) );
		assertEquals( PlanningEventTypeEnum.RETAIL.getJsonKey(), testEvent.getString( "soldAs" ) );
		assertNotNull( testEvent.getString( "beginDate" ) );
	}
	catch (JSONException je )
	{
		fail( je.getMessage() );	
	}
}

public void testAddNextPlanDateToJSON()
{
    JSONObject jsonObject = new JSONObject();
    
    try
    {
        jsonObject.put( "nextPlanDate", InventoryPlanningUtilService.convertDateToString( new Date() ) );	
        assertNotNull( jsonObject.get( "nextPlanDate" ) );
    }
    catch (JSONException je)
    {
        fail( je.getMessage() );
    }
}
/**
public void testFindLastRepriceDate()
{
	PlanningEventType repriceEventType = new PlanningEventType();
	repriceEventType.setId( PlanningEventTypeEnum.REPRICE.getPlanEventTypeId() );

	PlanningEventType advertisementEventType = new PlanningEventType();
	advertisementEventType.setId( PlanningEventTypeEnum.ADVERTISEMENT.getPlanEventTypeId() );

	PlanningEventType promotionEventType = new PlanningEventType();
	promotionEventType.setId( PlanningEventTypeEnum.PROMOTION.getPlanEventTypeId() );

	InventoryPlanEvent event1 = new InventoryPlanEvent();
	event1.setId( 1 );
	event1.setPlanningEventType( repriceEventType );
	Calendar cal1 = Calendar.getInstance();
	cal1.set( Calendar.MONTH, 5 );
	cal1.set( Calendar.DAY_OF_MONTH, 14 );
	cal1.set( Calendar.YEAR, 2006 );
	event1.setCreated( cal1.getTime() );

	InventoryPlanEvent event2 = new InventoryPlanEvent();
	event2.setId( 2 );
	event2.setPlanningEventType( advertisementEventType );

	InventoryPlanEvent event3 = new InventoryPlanEvent();
	event3.setId( 3 );
	event3.setPlanningEventType( advertisementEventType );

	InventoryPlanEvent event4 = new InventoryPlanEvent();
	event4.setId( 4 );
	event4.setPlanningEventType( repriceEventType );
	Calendar cal4 = Calendar.getInstance();
	cal4.set( Calendar.MONTH, 5 );
	cal4.set( Calendar.DAY_OF_MONTH, 16 );
	cal4.set( Calendar.YEAR, 2006 );
	event4.setCreated( cal4.getTime() );

	InventoryPlanEvent event5 = new InventoryPlanEvent();
	event5.setId( 5 );
	event5.setPlanningEventType( promotionEventType );

	List< InventoryPlanEvent > events = new ArrayList< InventoryPlanEvent >();
	events.add( event1 );
	events.add( event2 );
	events.add( event3 );
	events.add( event4 );
	events.add( event5 );

	InventoryPlanningService service = new InventoryPlanningService();
	Date lastRepriceDate = service.findLastRepriceDate( events );

	Calendar testCal = Calendar.getInstance();
	testCal.setTime( lastRepriceDate );

	assertEquals( 5, testCal.get( Calendar.MONTH ) );
	assertEquals( 16, testCal.get( Calendar.DAY_OF_MONTH ) );
	assertEquals( 2006, testCal.get( Calendar.YEAR ) );
}

public void testFindLastRepriceDateNoReprices()
{
	PlanningEventType advertisementEventType = new PlanningEventType();
	advertisementEventType.setId( PlanningEventTypeEnum.ADVERTISEMENT.ordinal() );

	PlanningEventType promotionEventType = new PlanningEventType();
	promotionEventType.setId( PlanningEventTypeEnum.PROMOTION.ordinal() );

	InventoryPlanEvent event1 = new InventoryPlanEvent();
	event1.setId( 1 );
	event1.setPlanningEventType( advertisementEventType );

	InventoryPlanEvent event2 = new InventoryPlanEvent();
	event2.setId( 2 );
	event2.setPlanningEventType( advertisementEventType );

	InventoryPlanEvent event3 = new InventoryPlanEvent();
	event3.setId( 3 );
	event3.setPlanningEventType( promotionEventType );

	List< InventoryPlanEvent > events = new ArrayList< InventoryPlanEvent >();
	events.add( event1 );
	events.add( event2 );
	events.add( event3 );

	InventoryPlanningService service = new InventoryPlanningService();
	Date lastRepriceDate = service.findLastRepriceDate( events );

	assertNull( lastRepriceDate );
}**/

// BROKEN TEST CASES - NEED TO FIX
/**
 * Find the next plan date when the plan's reminder date is today
 * (should be the next bucket jump date)
 */
/**
public void testFindNextPlanDateExpirationDate()
{
    Date thirtyDaysBeforeToday = DateUtilFL.addDaysToDate( new Date(), -30 );
    Date fiveDaysFromNow = DateUtilFL.addDaysToDate( new Date(), 5 );
	
	MockInventoryPlanHistoryDAO mockPlanHistoryDAO = new MockInventoryPlanHistoryDAO();
	mockPlanHistoryDAO.expirationDate = new Date(); // today
	
	// set the age of the vehicle to be 30
	MockInventoryDAO mockInventoryDAO = new MockInventoryDAO();
	mockInventoryDAO.inventoryReceivedDate = thirtyDaysBeforeToday;

	// set up a mock bucket range
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 25;
	mockBucketRangeDAO.high = 35;	
	
	InventoryPlanningService service = new InventoryPlanningService();
	service.setInventoryPlanHistoryDAO( mockPlanHistoryDAO );
	service.setInventoryDAO( mockInventoryDAO );
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	Date testDate = service.findNextPlanDate( 222, 223 );

	// the next plan date should be 5 days from now (the next bucket jump date)
	Calendar testCalendar = Calendar.getInstance();
	testCalendar.setTime( testDate );
	
	Calendar fiveDaysAfterTodayCal = Calendar.getInstance();
	fiveDaysAfterTodayCal.setTime( fiveDaysFromNow );
	
	assertEquals( fiveDaysAfterTodayCal.get( Calendar.DAY_OF_MONTH ), testCalendar.get( Calendar.DAY_OF_MONTH ) );   
	assertEquals( fiveDaysAfterTodayCal.get( Calendar.MONTH ), testCalendar.get( Calendar.MONTH ) );   
	assertEquals( fiveDaysAfterTodayCal.get( Calendar.YEAR ), testCalendar.get( Calendar.YEAR ) ); 
}**/

/**
 * If today is equal to or past the expiration date, then the reminder
 * date should be the next bucket jump date
 */
/**
public void testFindNextPlanDatePastExpirationDate()
{
	//Date thirtyDaysBeforeToday = DateUtilFL.addDaysToDate( new Date(), -30 );
	//Date fiveDaysAfterToday = DateUtilFL.addDaysToDate( new Date(), 5 );
	//Calendar fiveDaysAfterTodayCal = Calendar.getInstance();
	//fiveDaysAfterTodayCal.setTime( fiveDaysAfterToday );
	
	// 7/15/2005
	Calendar planReminderDate = Calendar.getInstance();
	planReminderDate.set( Calendar.DAY_OF_MONTH, 15 );
	planReminderDate.set( Calendar.MONTH, 6 );
	planReminderDate.set( Calendar.YEAR, 2006 );
	
	// 7/16/2006
	Calendar planComparisonDate = Calendar.getInstance();
	planComparisonDate.set( Calendar.DAY_OF_MONTH, 16 );
	planComparisonDate.set( Calendar.MONTH, 6 );
	planComparisonDate.set( Calendar.YEAR, 2006 );
	
	// 7/1/2006
	Calendar inventoryReceivedDateCal = Calendar.getInstance();
	inventoryReceivedDateCal.set( Calendar.DAY_OF_MONTH, 1 );
	inventoryReceivedDateCal.set( Calendar.MONTH, 6 );
	inventoryReceivedDateCal.set( Calendar.YEAR, 2006 );
	
	// 7/20/2006
	Calendar expectedNextPlanDateCal = Calendar.getInstance();
	expectedNextPlanDateCal.set( Calendar.DAY_OF_MONTH, 20 );
	expectedNextPlanDateCal.set( Calendar.MONTH, 6 );
	expectedNextPlanDateCal.set( Calendar.YEAR, 2006 );
	
    MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
    mockBucketRangeDAO.low = 0;
    mockBucketRangeDAO.high = 20;	
	
	MockInventoryDAO mockInventoryDAO = new MockInventoryDAO();
	mockInventoryDAO.planReminderDate = planReminderDate.getTime();
	mockInventoryDAO.inventoryReceivedDate = inventoryReceivedDateCal.getTime();
	
	InventoryPlanningService service = new InventoryPlanningService();
	service.setInventoryDAO( mockInventoryDAO );
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	Date testNextPlanDate = service.findNextPlanDate( 333, 333, planComparisonDate.getTime() );
	
	Calendar testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	
	// we're past the next reminder date, so
    // inventory age is 16, the high end of the bucket range is 20, and the reference date
	// is the 16th, so the next plan date should be the 20th
	
	assertEquals( expectedNextPlanDateCal.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH ) );   
	assertEquals( expectedNextPlanDateCal.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH ) );   
	assertEquals( expectedNextPlanDateCal.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR ) );  
}**/

public void testFindNextPlanDateBeforeExpirationDate3Days()
{
	Date planReminderDate = DateUtilFL.addDaysToDate( new Date(), -25 ); // age = 25
	Date planComparisonDate = DateUtilFL.addDaysToDate( new Date(), 2 ); // plan expires at age = 27
	
	//MockInventoryDAO mockInventoryDAO = new MockInventoryDAO();
	//mockInventoryDAO.planReminderDate = planComparisonDate;
	//mockInventoryDAO.inventoryReceivedDate = planReminderDate;
	
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 25;
	mockBucketRangeDAO.high = 35;
	
	MockInventoryPlanEventDAO mockPlanEventDAO = new MockInventoryPlanEventDAO();

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	//service.setInventoryDAO( mockInventoryDAO );	
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	service.setInventoryPlanEventDAO( mockPlanEventDAO );
	
	Inventory inventory = new Inventory();
	inventory.setId( 333 );
	inventory.setPlanReminderDate( planComparisonDate );
	inventory.setInventoryReceivedDate( planReminderDate );

	// if on planning list this is behavior
	Date testNextPlanDate = service.findNextPlanDate( inventory.getId());
	
	Date bucketOfReminderDate = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), 11 ); // replan at age = 35
	Calendar testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	Calendar testBucketOfReminderDate = Calendar.getInstance();
	testBucketOfReminderDate.setTime( bucketOfReminderDate );
	
	assertEquals( testBucketOfReminderDate.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR )  );	

	// if not planning list this is behavior
	mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 20;
	mockBucketRangeDAO.high = 27;
	
	testNextPlanDate = service.findNextPlanDate( inventory.getId());
	
	bucketOfReminderDate = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), 2 ); // // replan at age = 30 because has an active plan
	testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	testBucketOfReminderDate = Calendar.getInstance();
	testBucketOfReminderDate.setTime( bucketOfReminderDate );
	
	assertEquals( testBucketOfReminderDate.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR )  );	
}

public void testFindNextPlanDateBeforeExpirationDate5Days()
{
	Date planReminderDate = DateUtilFL.addDaysToDate( new Date(), -25 ); // age = 25
	Date planComparisonDate = DateUtilFL.addDaysToDate( new Date(), 4 ); // plan expires at age = 29
	
	Inventory inventory = new Inventory();
	inventory.setId( 333 );
	inventory.setBusinessUnitId( 333 );
	inventory.setPlanReminderDate( planComparisonDate );
	inventory.setInventoryReceivedDate( planReminderDate );
	
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 25;
	mockBucketRangeDAO.high = 35;
	
	MockInventoryPlanEventDAO mockPlanEventDAO = new MockInventoryPlanEventDAO();

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	//service.setInventoryDAO( mockInventoryDAO );	
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	service.setInventoryPlanEventDAO( mockPlanEventDAO );

	// if on planning list this is behavior
	Date testNextPlanDate = service.findNextPlanDate( inventory.getId() );
	
	Date bucketOfReminderDate = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), 11 ); // replan at age = 35
	Calendar testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	Calendar testBucketOfReminderDate = Calendar.getInstance();
	testBucketOfReminderDate.setTime( bucketOfReminderDate );
	
	assertEquals( testBucketOfReminderDate.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR )  );	

	// if not planning list this is behavior
	mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 20;
	mockBucketRangeDAO.high = 27;
	
	testNextPlanDate = service.findNextPlanDate( inventory.getId());
	
	bucketOfReminderDate = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), 4 ); // // replan at age = 30 because has an active plan
	testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	testBucketOfReminderDate = Calendar.getInstance();
	testBucketOfReminderDate.setTime( bucketOfReminderDate );
	
	assertEquals( testBucketOfReminderDate.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR )  );	
}

public void testNextPlanDateWhenPlanExpiresTodayButLookingAtToPlanNext3DaysFilter()
{
	Date planReminderDate = DateUtilFL.addDaysToDate( new Date(), -25 ); // age = 25
	Date planComparisonDate = DateUtilFL.addDaysToDate( new Date(), 0 ); // plan expires at age = 25
	
	Inventory inventory = new Inventory();
	inventory.setId( 333 );
	inventory.setPlanReminderDate( planComparisonDate );
	inventory.setInventoryReceivedDate( planReminderDate );
	
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 20;
	mockBucketRangeDAO.high = 28;

	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	service.setInventoryPlanEventDAO( new MockInventoryPlanEventDAO() );

	// looking at to be planned in next 3 days view
	Date testNextPlanDate = service.findNextPlanDate( inventory.getId());
	
	Date bucketOfReminderDate = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), 4 ); // replan at age = 25
	Calendar testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	Calendar testBucketOfReminderDate = Calendar.getInstance();
	testBucketOfReminderDate.setTime( bucketOfReminderDate );
	
	assertEquals( testBucketOfReminderDate.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR )  );	

	// now looking at to be planned today 
	testNextPlanDate = service.findNextPlanDate( inventory.getId());
	
	testNextPlanDateCal = Calendar.getInstance();
	testNextPlanDateCal.setTime( testNextPlanDate );
	
	assertEquals( testBucketOfReminderDate.get( Calendar.MONTH ), testNextPlanDateCal.get( Calendar.MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.DAY_OF_MONTH ), testNextPlanDateCal.get( Calendar.DAY_OF_MONTH )  );	
	assertEquals( testBucketOfReminderDate.get( Calendar.YEAR ), testNextPlanDateCal.get( Calendar.YEAR )  );	
}

public void testFindNextPlanDateExpirationNoPlans()
{
	Date thirtyDaysBeforeToday = DateUtilFL.addDaysToDate( new Date(), -30 );
	Date sixDaysAfterToday = DateUtilFL.addDaysToDate( new Date(), 6 );
	
	Inventory inventory = new Inventory();
	inventory.setId( 222 );
	inventory.setBusinessUnitId( 223 );
	inventory.setInventoryReceivedDate( thirtyDaysBeforeToday );
	inventory.setPlanReminderDate( null );
	
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 25;
	mockBucketRangeDAO.high = 35;
	
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	
	Date testDate = service.findNextPlanDate( inventory.getId());
	
	Calendar testCalendar = Calendar.getInstance();
	testCalendar.setTime( testDate );
	
	Calendar sixDaysAfterTodayCal = Calendar.getInstance();
	sixDaysAfterTodayCal.setTime( sixDaysAfterToday );
	
	assertEquals( sixDaysAfterTodayCal.get( Calendar.DAY_OF_MONTH ), testCalendar.get( Calendar.DAY_OF_MONTH ) );   
	assertEquals( sixDaysAfterTodayCal.get( Calendar.MONTH ), testCalendar.get( Calendar.MONTH ) );   
	assertEquals( sixDaysAfterTodayCal.get( Calendar.YEAR ), testCalendar.get( Calendar.YEAR ) );   
}

public void testFindNextPlanDateExpirationNoLastBucket()
{
	Date sixtyTwoDaysBeforeToday = DateUtilFL.addDaysToDate( new Date(), -62 );
    Date sixtyOneDaysBeforeToday = DateUtilFL.addDaysToDate( new Date(), -61 );
    Date eightDaysAfterToday = DateUtilFL.addDaysToDate( new Date(), 8 );
    
	Inventory inventory = new Inventory();
	inventory.setId( 222 );
	inventory.setBusinessUnitId( 223 );
	inventory.setInventoryReceivedDate( sixtyTwoDaysBeforeToday );
	inventory.setPlanReminderDate( sixtyOneDaysBeforeToday );
    
    MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
    mockBucketRangeDAO.low = 60;
    mockBucketRangeDAO.high = null;
    
    InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
    service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
    
    Date testDate = service.findNextPlanDate( inventory.getId());
    
    Calendar testCalendar = Calendar.getInstance();
    testCalendar.setTime( testDate );
    
    Calendar eightDaysAfterTodayCal = Calendar.getInstance();
    eightDaysAfterTodayCal.setTime( eightDaysAfterToday );
    
    assertEquals( eightDaysAfterTodayCal.get( Calendar.DAY_OF_MONTH ), testCalendar.get( Calendar.DAY_OF_MONTH ) );   
    assertEquals( eightDaysAfterTodayCal.get( Calendar.MONTH ), testCalendar.get( Calendar.MONTH ) );   
    assertEquals( eightDaysAfterTodayCal.get( Calendar.YEAR ), testCalendar.get( Calendar.YEAR ) );   
}

/**
 * if a car is 25 days old, and has a plan that goes to day 31
 * the prepopulated replan date would be age 40 (assuming a 30-40 bucket)
 */
public void testFindNextPlanDateUsingExpiresNotAge()
{
	Date twentyFiveDaysBeforeToday = DateUtilFL.addDaysToDate( new Date(), -25 );
	Date sixDaysAfterToday = DateUtilFL.addDaysToDate( new Date(), 6 );
	
	Inventory inventory = new Inventory();
	inventory.setId( 222 );
	inventory.setBusinessUnitId( 223 );
	inventory.setInventoryReceivedDate( twentyFiveDaysBeforeToday ); // age = 25 
	inventory.setPlanReminderDate( sixDaysAfterToday ); // current plan ends on age 31
	
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 30;
	mockBucketRangeDAO.high = 40;
	
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
	service.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	service.setInventoryPlanEventDAO( new MockInventoryPlanEventDAO() );
	
	Date testDate = service.findNextPlanDate( inventory.getId());
	
	Calendar testCalendar = Calendar.getInstance();
	testCalendar.setTime( testDate );
	
	Date sixteenDaysAfterToday = DateUtilFL.addDaysToDate( new Date(), 16 );
	Calendar sixteenDaysAfterTodayCal = Calendar.getInstance();
	sixteenDaysAfterTodayCal.setTime( sixteenDaysAfterToday );
	
	assertEquals( sixteenDaysAfterTodayCal.get( Calendar.DAY_OF_MONTH ), testCalendar.get( Calendar.DAY_OF_MONTH ) );   
	assertEquals( sixteenDaysAfterTodayCal.get( Calendar.MONTH ), testCalendar.get( Calendar.MONTH ) );   
	assertEquals( sixteenDaysAfterTodayCal.get( Calendar.YEAR ), testCalendar.get( Calendar.YEAR ) );   
}

public void testAddCurrentObjectiveToJSON()
{
    JSONObject jsonObject = new JSONObject();
    try
    {
    	jsonObject.put( "objective", PlanObjectiveEnum.RETAIL.getJsonKey() );
        assertEquals( "retail", jsonObject.get( "objective" ) );
    }
    catch ( JSONException je )
    {
        fail( je.getMessage() );
    }   
}


/** Need to fix
public void testBuildEndedPlanEvents()
{
    try
    {
	    InventoryPlanningService service = new InventoryPlanningService();
		//service.setInventoryPlanEventDAO( new MockInventoryPlanEventDAO() );
	    InventoryPlanEvent currentEvent = new InventoryPlanEvent();
	    currentEvent.setId( 300 );
		
	    List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	    currentEvents.add( currentEvent );
	    
		JSONArray endedEventIds = new JSONArray();
		endedEventIds.put( 300 );
		
	    JSONObject jsonPlan = new JSONObject();
	    jsonPlan.put( "D", endedEventIds );
		
		List<InventoryPlanEvent> events = service.extractEndedPlanEvents( jsonPlan, currentEvents, "aUser" );
		
		assertTrue( events.size() == 1 );
		
		InventoryPlanEvent testEvent = events.get( 0 );
		assertEquals( new Integer(300), testEvent.getId() );
		assertEquals( "aUser", testEvent.getLastModifiedBy() );
		assertEquals( yesterday, testEvent.getEndDate() );
	}
	catch (JSONException je)
	{
	    fail( je.getMessage() );
	}
}**/

public void testBuildEndedPlanEventsNoEvents()
{
	try
	{
		InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
        //service.setInventoryPlanEventDAO( new MockInventoryPlanEventDAO() );
    
	    JSONArray endedEventIds = new JSONArray();  
	    List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	    
	    JSONObject planObject = new JSONObject();
	    planObject.put( "D", endedEventIds );
	    
	    List<InventoryPlanEvent> events = service.extractEndedPlanEvents( planObject, currentEvents, "aUser" );
	    
	    assertTrue( events.size() == 0 );
	}
	catch (JSONException je)
	{
	    fail( je.getMessage() );
	}
}

public void testCreateMarkerEvent()
{
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
    InventoryPlanEvent event = service.createMarkerEvent( 123, PlanningEventTypeEnum.REMINDER_DATE, "aUser" );
    
    assertEquals( new Integer(123), event.getInventoryId() );
    assertEquals( new Integer(PlanningEventTypeEnum.REMINDER_DATE.getId()), event.getEventTypeId() );
    assertEquals( "aUser", event.getLastModifiedBy() );
    assertEquals( "aUser", event.getCreatedBy() );
    assertEquals( DateUtils.truncate( new Date(), Calendar.DATE ), event.getBeginDate() );
    assertEquals( DateUtils.truncate( new Date(), Calendar.DATE ), event.getEndDate() );
}

public void testBuildReminderDateEvent()
{
	MockInventoryPlanEventDAO mockPlanEventDAO = new MockInventoryPlanEventDAO();
	mockPlanEventDAO.hasReplanningEventForToday = false;
	mockPlanEventDAO.id = 123;
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();	
	service.setInventoryPlanEventDAO( mockPlanEventDAO );
	
	InventoryPlanEvent event = service.buildReminderDateEvent( 444, "aUser" );
	assertEquals( null, event.getId() );
	assertEquals( DateUtils.truncate( new Date(), Calendar.DATE ), event.getBeginDate() );
}

public void testBuildReminderDateEventExistingEvent()
{
    MockInventoryPlanEventDAO mockPlanEventDAO = new MockInventoryPlanEventDAO();
    mockPlanEventDAO.hasReplanningEventForToday = true;
    mockPlanEventDAO.id = 123;
	
    InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();	
    service.setInventoryPlanEventDAO( mockPlanEventDAO );
    
    InventoryPlanEvent event = service.buildReminderDateEvent( 444, "aUser" );
    assertEquals( new Integer(123), event.getId() );
    assertEquals( "aUser", event.getLastModifiedBy() );
    assertEquals( DateUtils.truncate( new Date(), Calendar.DATE ), event.getBeginDate() );
}

/**
 * 
 * An InventoryPlanEvent's date range is inclusive, so if an event is ended right 
 * now, the end date should be set to yesterday because the event was active
 * as of yesterday.  The exception is when the event's begin date is today.
 * If the even't begin date is today, it doesn't make sense to set the end
 * date to yesterday, so it is set to today.  The "current plan" logic
 * elsewhere checks for this case.
 * @param event
 */
public void testEndEvent()
{
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setBeginDate( threeDaysAgo );
	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
    service.endEvent( event );
    assertEquals( yesterday, event.getEndDate() );
}

/**
 * see testEndEvent()'s comments
 */
public void testEndEventBeganToday()
{
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setBeginDate( today );
	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
    service.endEvent( event );
    assertEquals( today, event.getEndDate() );	
}

public void testBuildNoStrategyEvent()
{
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
    InventoryPlanEvent event = service.buildNoStrategyEvent( PlanObjectiveEnum.RETAIL, 333, "aUser" );
    assertEquals( new Integer(PlanningEventTypeEnum.RETAIL_NO_STRATEGY.getId()), event.getEventTypeId() );
    assertEquals( new Integer(333), event.getInventoryId() );
}

/**
 * make sure that the plan reminder date is set on inventory
 * when it is repriced inline
 */
public void xtestInlineRepriceTrackingInventoryPlanReminderDate()
{
	Inventory inventory = new Inventory();
    inventory.setBusinessUnitId(123456);
	inventory.setInventoryReceivedDate( DateUtilFL.addDaysToDate( new Date(), -2 ) );
	MockInventoryDAO mockInventoryDAO = new MockInventoryDAO();
	mockInventoryDAO.testInventory = inventory;
	
	MockInventoryBucketRangeDAO mockBucketRangeDAO = new MockInventoryBucketRangeDAO();
	mockBucketRangeDAO.low = 0;
	mockBucketRangeDAO.high = 29;
	
	MockInventoryPlanEventDAO mockInventoryPlanEventDAO = new MockInventoryPlanEventDAO();
		
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	InventoryPlanningRetrievalService service2 = new InventoryPlanningRetrievalService();
	service.setInventoryDAO( mockInventoryDAO );
	service.setInventoryPlanEventDAO( mockInventoryPlanEventDAO );
	service.setTransactionDAO( new MockTransactionDAO() );
	
	service2.setInventoryDAO( mockInventoryDAO );
	service2.setInventoryPlanEventDAO( mockInventoryPlanEventDAO );
	service2.setInventoryBucketRangeDAO( mockBucketRangeDAO );
	
	service.inlineRepriceTracking( 444, 4000.95, "aUser", true );
	
	assertNotNull( inventory.getPlanReminderDate() );
	assertEquals( DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 28 ), Calendar.DATE ), inventory.getPlanReminderDate() );
}

/**
 * tests closing events when the previous objective is retail
 */
public void testCloseEventsFromPreviousObjectiveRetail()
{
	List<InventoryPlanEvent> existingEvents = new ArrayList<InventoryPlanEvent>();
	InventoryPlanEvent advertisingEvent = new InventoryPlanEvent();
	advertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	advertisingEvent.setBeginDate( threeDaysAgo );
	
	InventoryPlanEvent lotPromoteEvent = new InventoryPlanEvent();
	lotPromoteEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	lotPromoteEvent.setBeginDate( threeDaysAgo );
	
	// service department events should not be closed
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	serviceEvent.setBeginDate( threeDaysAgo );
	
	// detail events should not be closed
	InventoryPlanEvent detailEvent = new InventoryPlanEvent();
	detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
	detailEvent.setBeginDate( threeDaysAgo );
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	spiffEvent.setBeginDate( threeDaysAgo );
	
	InventoryPlanEvent otherEvent = new InventoryPlanEvent();
	otherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER_RETAIL.getId() );
	otherEvent.setBeginDate( threeDaysAgo );
	
	existingEvents.add( advertisingEvent );
	existingEvents.add( lotPromoteEvent );
	existingEvents.add( serviceEvent );
	existingEvents.add( detailEvent );
	existingEvents.add( spiffEvent );
	existingEvents.add( otherEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	List<InventoryPlanEvent> closedEvents = service.closeEventsFromPreviousObjective( existingEvents, PlanObjectiveEnum.RETAIL, "aUser" );
	
	assertEquals( 4, closedEvents.size() );
	List<PlanningEventTypeEnum> eventTypes = new ArrayList<PlanningEventTypeEnum>();
	eventTypes.add( PlanningEventTypeEnum.ADVERTISEMENT );
	eventTypes.add( PlanningEventTypeEnum.PROMOTION );
	eventTypes.add( PlanningEventTypeEnum.SPIFF );
	eventTypes.add( PlanningEventTypeEnum.OTHER_RETAIL );
	
	for ( InventoryPlanEvent event : closedEvents )
	{
		eventTypes.remove( event.getEventTypeEnum() );	
	}
	
	// only the advertising events should be included in
	// the closed events list
	assertEquals( 0, eventTypes.size() );
}

/**
 * tests closing events when the previous objective is wholesale
 */
public void testCloseEventsFromPreviousObjectiveWholesale()
{
	List<InventoryPlanEvent> existingEvents = new ArrayList<InventoryPlanEvent>();
	InventoryPlanEvent auctionEvent = new InventoryPlanEvent();
	auctionEvent.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	auctionEvent.setBeginDate( threeDaysAgo );
	
	InventoryPlanEvent wholesalerEvent = new InventoryPlanEvent();
	wholesalerEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
	wholesalerEvent.setBeginDate( threeDaysAgo );
	
	// service department events should not be closed
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	serviceEvent.setBeginDate( threeDaysAgo );
	
	// detail events should not be closed
	InventoryPlanEvent detailEvent = new InventoryPlanEvent();
	detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
	detailEvent.setBeginDate( threeDaysAgo );
	
	InventoryPlanEvent otherEvent = new InventoryPlanEvent();
	otherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER_WHOLESALE.getId() );
	otherEvent.setBeginDate( threeDaysAgo );
	
	existingEvents.add( auctionEvent );
	existingEvents.add( wholesalerEvent );
	existingEvents.add( serviceEvent );
	existingEvents.add( detailEvent );
	existingEvents.add( otherEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	List<InventoryPlanEvent> closedEvents = service.closeEventsFromPreviousObjective( existingEvents, PlanObjectiveEnum.WHOLESALE, "aUser" );
	
	assertEquals( 3, closedEvents.size() );
	List<PlanningEventTypeEnum> eventTypes = new ArrayList<PlanningEventTypeEnum>();
	eventTypes.add( PlanningEventTypeEnum.AUCTION );
	eventTypes.add( PlanningEventTypeEnum.WHOLESALER );
	eventTypes.add( PlanningEventTypeEnum.OTHER_WHOLESALE );
	
	for ( InventoryPlanEvent event : closedEvents )
	{
		eventTypes.remove( event.getEventTypeEnum() );	
	}
	
	// only the wholesale events should be included in
	// the closed events list
	assertEquals( 0, eventTypes.size() );
}

/**
 * tests closing events when the previous objective is sold (retail)
 */
public void testCloseEventsFromPreviousObjectiveSoldRetail()
{
	List<InventoryPlanEvent> existingEvents = new ArrayList<InventoryPlanEvent>();
	InventoryPlanEvent soldRetailEvent = new InventoryPlanEvent();
	soldRetailEvent.setEventTypeId( PlanningEventTypeEnum.RETAIL.getId() );
	soldRetailEvent.setBeginDate( threeDaysAgo );
	
	// service department events should not be closed
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	serviceEvent.setBeginDate( threeDaysAgo );
	
	// detail events should not be closed
	InventoryPlanEvent detailEvent = new InventoryPlanEvent();
	detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
	detailEvent.setBeginDate( threeDaysAgo );
	
	existingEvents.add( soldRetailEvent );
	existingEvents.add( serviceEvent );
	existingEvents.add( detailEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	List<InventoryPlanEvent> closedEvents = service.closeEventsFromPreviousObjective( existingEvents, PlanObjectiveEnum.SOLD, "aUser" );
	
	assertEquals( 1, closedEvents.size() );
	InventoryPlanEvent testEvent = closedEvents.get( 0 );
	assertEquals( PlanningEventTypeEnum.RETAIL, testEvent.getEventTypeEnum() );
}

/**
 * tests closing events when the previous objective is sold (retail)
 */
public void testCloseEventsFromPreviousObjectiveSoldWholesale()
{
	List<InventoryPlanEvent> existingEvents = new ArrayList<InventoryPlanEvent>();
	InventoryPlanEvent soldWholesaleEvent = new InventoryPlanEvent();
	soldWholesaleEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALE.getId() );
	soldWholesaleEvent.setBeginDate( threeDaysAgo );
	
	// service department events should not be closed
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	serviceEvent.setBeginDate( threeDaysAgo );
	
	// detail events should not be closed
	InventoryPlanEvent detailEvent = new InventoryPlanEvent();
	detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
	detailEvent.setBeginDate( threeDaysAgo );
	
	existingEvents.add( soldWholesaleEvent );
	existingEvents.add( serviceEvent );
	existingEvents.add( detailEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	List<InventoryPlanEvent> closedEvents = service.closeEventsFromPreviousObjective( existingEvents, PlanObjectiveEnum.SOLD, "aUser" );
	
	assertEquals( 1, closedEvents.size() );
	InventoryPlanEvent testEvent = closedEvents.get( 0 );
	assertEquals( PlanningEventTypeEnum.WHOLESALE, testEvent.getEventTypeEnum() );
}

/**
 * tests closing events when the previous objective is other
 */
public void testCloseEventsFromPreviousObjectiveOther()
{
	List<InventoryPlanEvent> existingEvents = new ArrayList<InventoryPlanEvent>();
	InventoryPlanEvent otherEvent = new InventoryPlanEvent();
	otherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER.getId() );
	otherEvent.setBeginDate( threeDaysAgo );
	
	// service department events should not be closed
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	serviceEvent.setBeginDate( threeDaysAgo );
	
	// detail events should not be closed
	InventoryPlanEvent detailEvent = new InventoryPlanEvent();
	detailEvent.setEventTypeId( PlanningEventTypeEnum.DETAILER.getId() );
	detailEvent.setBeginDate( threeDaysAgo );
	
	existingEvents.add( otherEvent );
	existingEvents.add( serviceEvent );
	existingEvents.add( detailEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	List<InventoryPlanEvent> closedEvents = service.closeEventsFromPreviousObjective( existingEvents, PlanObjectiveEnum.OTHER, "aUser" );

	assertEquals( 1, closedEvents.size() );
	InventoryPlanEvent testEvent = closedEvents.get( 0 );
	assertEquals( PlanningEventTypeEnum.OTHER, testEvent.getEventTypeEnum() );
}

public void testHasEventsForObjective()
{
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	spiffEvent.setId( new Integer( 2 ) );
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( spiffEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertTrue( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveNoEvents()
{
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
    assertFalse( service.hasEventsForObjective( new ArrayList<InventoryPlanEvent>(), new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveWithService()
{
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	serviceEvent.setId( new Integer( 2 ) );
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );	
	spiffEvent.setId( new Integer( 3 ) );
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( serviceEvent );
	events.add( spiffEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertTrue( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveWithReprice()
{
	InventoryPlanEvent repriceEvent = new InventoryPlanEvent();
	repriceEvent.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
	repriceEvent.setId(new Integer(2) );
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	spiffEvent.setId(new Integer(3) );
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( repriceEvent );
	events.add( spiffEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertTrue( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveOnlyReprice()
{
	InventoryPlanEvent repriceEvent = new InventoryPlanEvent();
	repriceEvent.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( repriceEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertFalse( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveOnlyService()
{
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( serviceEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertFalse( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveOnlyNoStrategy()
{
	InventoryPlanEvent retailNoStrategyEvent = new InventoryPlanEvent();
	retailNoStrategyEvent.setEventTypeId( PlanningEventTypeEnum.RETAIL_NO_STRATEGY.getId() );
	retailNoStrategyEvent.setId( new Integer(2));
	
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	events.add( retailNoStrategyEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertTrue( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.RETAIL ) );
}

public void testThatAPrepopulatedWholeSaleEventWillNotBeFilteredOutAsAPreviousPlan()
{
	InventoryPlanEvent wholesaleEvent = new InventoryPlanEvent();
	wholesaleEvent.setEventTypeId( PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY.getId() );
	
	List<InventoryPlanEvent> events = new ArrayList< InventoryPlanEvent >();
	events.add( wholesaleEvent );
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertFalse( service.hasEventsForObjective( events, new ArrayList<InventoryPlanEvent>(), PlanObjectiveEnum.WHOLESALE ) );
}

public void testHasEventsForObjectiveDeleted()
{
	InventoryPlanEvent serviceEvent = new InventoryPlanEvent();
	serviceEvent.setId( 1 );
	serviceEvent.setEventTypeId( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() );
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setId( 2 );
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );	
	
	List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	currentEvents.add( serviceEvent );
	currentEvents.add( spiffEvent );
	
	List<InventoryPlanEvent> deletedEvents = new ArrayList<InventoryPlanEvent>();
	deletedEvents.add( spiffEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertFalse( service.hasEventsForObjective( currentEvents, deletedEvents, PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveAllDeleted()
{
	InventoryPlanEvent advertisingEvent = new InventoryPlanEvent();
	advertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	advertisingEvent.setId( 1 );
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	spiffEvent.setId( 2 );
	
	List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	currentEvents.add( advertisingEvent );
	currentEvents.add( spiffEvent );
	
	List<InventoryPlanEvent> deletedEvents = new ArrayList<InventoryPlanEvent>();
	deletedEvents.add( spiffEvent );
	deletedEvents.add( advertisingEvent );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertFalse( service.hasEventsForObjective( currentEvents, deletedEvents, PlanObjectiveEnum.RETAIL ) );
}

public void testHasEventsForObjectiveOneDeleted()
{
	InventoryPlanEvent advertisingEvent = new InventoryPlanEvent();
	advertisingEvent.setId( 1 );
	advertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );

	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setId( 2 );
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );	
	
	List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	currentEvents.add( advertisingEvent );
	currentEvents.add( spiffEvent );
	
	List<InventoryPlanEvent> deletedEvents = new ArrayList<InventoryPlanEvent>();
	deletedEvents.add( spiffEvent );;
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	assertTrue( service.hasEventsForObjective( currentEvents, deletedEvents, PlanObjectiveEnum.RETAIL ) );
}

public void testExtractNewEvents() throws JSONException
{
	String jsonText = "{N:{\"advertise\":[{\"thirdParty\":\"abc\",\"strategyNotes\":\"some notes\"},{\"thirdParty\":\"xyz\",\"strategyNotes\":\"some notes2\"}]}}";
	JSONObject jsonObject = new JSONObject( jsonText );
	
	List<JSONObject> newEventObjects = InventoryPlanningUtilService.extractEventsByType( jsonObject, "N" );
	assertEquals( 2, newEventObjects.size() );
	assertEquals( "advertise", (String)(newEventObjects.get(0)).get( "strategy" ) );
}

public void testExtractNewEventsToggledAndNotToggled() throws JSONException
{
    String jsonText = "{N:{\"advertise\":[{\"thirdParty\":\"abc\",\"strategyNotes\":\"some notes\"},{\"thirdParty\":\"xyz\",\"strategyNotes\":\"some notes2\"}],\"detail\":{\"strategyNotes\":\"paint\"}}}";
    JSONObject jsonObject = new JSONObject( jsonText );
    
    List<JSONObject> newEventObjects = InventoryPlanningUtilService.extractEventsByType( jsonObject, "N" );
    assertEquals( 3, newEventObjects.size() );
    int advertiseCount = 0;
    int detailCount = 0;
    
    for ( JSONObject testJSONObject : newEventObjects )
    {
        String strategy = testJSONObject.optString( "strategy" );  
        if ( strategy.equals( PlanningEventTypeEnum.ADVERTISEMENT.getJsonKey() ) )
        {
            advertiseCount++;
        }
        else if ( strategy.equals( PlanningEventTypeEnum.DETAILER.getJsonKey() ) )
        {
            detailCount++;
        }
    }
    
    assertEquals( 2, advertiseCount );
    assertEquals( 1, detailCount );
}

public void testConvertJSONEventToInventoryPlanEventNewToggled() throws JSONException
{
	JSONObject newSpiffObject = new JSONObject();
	newSpiffObject.put( "strategy", PlanningEventTypeEnum.SPIFF.getJsonKey() );
	newSpiffObject.put( "strategyNotes", "test notes" );
	
	InventoryPlanEvent event = InventoryPlanningUtilService.convertJSONEventToInventoryPlanEvent( newSpiffObject, new ArrayList<InventoryPlanEvent>(), SaveAction.NEW );
	
	assertNull( event.getId() );
	assertEquals( "test notes", event.getNotes() );
	assertNull( event.getCreatedBy() );
}

public void testConvertJSONEventToInventoryPlanEventUpdateToggled() throws JSONException
{
	InventoryPlanEvent existingSpiff = new InventoryPlanEvent();
	existingSpiff.setId( 123 );
	existingSpiff.setCreatedBy( "aUser" );
	
	List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	currentEvents.add( existingSpiff );
	
	JSONObject newSpiffObject = new JSONObject();
    newSpiffObject.put( "strategy", PlanningEventTypeEnum.SPIFF.getJsonKey() );
    newSpiffObject.put( "strategyNotes", "test notes" );
    newSpiffObject.put( "id", 123 );
	
	InventoryPlanEvent event = InventoryPlanningUtilService.convertJSONEventToInventoryPlanEvent( newSpiffObject, currentEvents, SaveAction.UPDATE );
    
	assertEquals( new Integer(123), event.getId() );
	assertEquals( "test notes", event.getNotes() );
	assertEquals( "aUser", event.getCreatedBy() );
}

public void testConvertJSONEventToInventoryPlanEventNewAuction() throws JSONException
{
	JSONObject newSpiffObject = new JSONObject();
	newSpiffObject.put( "strategy", PlanningEventTypeEnum.AUCTION.getJsonKey() );
	newSpiffObject.put( "name", "Jane" );
	newSpiffObject.put( "thirdPartyId", "1234" );
	newSpiffObject.put( "location", "Chicago" );
	newSpiffObject.put( "strategyNotes", "test notes" );
	
	InventoryPlanEvent event = InventoryPlanningUtilService.convertJSONEventToInventoryPlanEvent( newSpiffObject, new ArrayList<InventoryPlanEvent>(), SaveAction.NEW );
	
	assertNull( event.getId() );
	assertEquals( "test notes", event.getNotes() );
	assertEquals( "Jane", event.getInventoryThirdParty().getName() );
	assertEquals( "Chicago", event.getNotes2() );
	assertNull( event.getCreatedBy() );
}

public void testConvertJSONEventToInventoryPlanEventUpdatedAuction() throws JSONException
{
	InventoryPlanEvent existingAuction = new InventoryPlanEvent();
	existingAuction.setId( 123 );
	existingAuction.setCreatedBy( "aUser" );
	
	List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
	currentEvents.add( existingAuction );

	JSONObject newSpiffObject = new JSONObject();
	newSpiffObject.put( "id", 123 );
	newSpiffObject.put( "strategy", PlanningEventTypeEnum.AUCTION.getJsonKey() );
	newSpiffObject.put( "name", "Jane" );
	newSpiffObject.put( "thirdPartyId", 123 );
	newSpiffObject.put( "location", "Chicago" );
	newSpiffObject.put( "strategyNotes", "test notes" );
	
	InventoryPlanEvent event = InventoryPlanningUtilService.convertJSONEventToInventoryPlanEvent( newSpiffObject, currentEvents, SaveAction.UPDATE );
	
	assertEquals( new Integer(123), event.getId() );
	assertEquals( "test notes", event.getNotes() );
	assertEquals( "Jane", event.getInventoryThirdParty().getName() );
	assertEquals( "Chicago", event.getNotes2() );
	assertEquals( "aUser", event.getCreatedBy() );
}

public void testApplyDateRulesNoBeginDatesRetailWindowed()
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE );
	Date tenDaysFromNow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 10 ), Calendar.DATE );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	
	InventoryPlanEvent promotionEvent = new InventoryPlanEvent();
	promotionEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	service.applyDateRules( promotionEvent, tenDaysFromNow );
	assertEquals( today, promotionEvent.getBeginDate() );
	assertEquals( tenDaysFromNow, promotionEvent.getEndDate() );	
}

public void testApplyDateRulesExistingBeginDatesRetailWindowed()
{
	Date twoDaysAgo = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -2 ), Calendar.DATE );
	Date tenDaysFromNow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 10 ), Calendar.DATE );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	
	InventoryPlanEvent promotionEvent = new InventoryPlanEvent();
	promotionEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	promotionEvent.setBeginDate( twoDaysAgo );
	service.applyDateRules( promotionEvent, tenDaysFromNow );
	assertEquals( twoDaysAgo, promotionEvent.getBeginDate() );
	assertEquals( tenDaysFromNow, promotionEvent.getEndDate() );
}

public void testApplyDateRulesNoBeginDatesRetailToggled()
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE );
	Date tenDaysFromNow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 10 ), Calendar.DATE );
	
	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	service.applyDateRules( spiffEvent, tenDaysFromNow );
	assertEquals( today, spiffEvent.getBeginDate() );
	assertNull( spiffEvent.getEndDate() );
	
	InventoryPlanEvent retailOtherEvent = new InventoryPlanEvent();
	retailOtherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER_RETAIL.getId() );
	service.applyDateRules( retailOtherEvent, tenDaysFromNow );
	assertEquals( today, retailOtherEvent.getBeginDate() );
	assertNull( retailOtherEvent.getEndDate() );
}

public void testApplyDateRulesExistingBeginDatesRetailToggled()
{
	Date twoDaysAgo = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -2 ), Calendar.DATE );
	Date tenDaysFromNow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 10 ), Calendar.DATE );

	InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
	
	InventoryPlanEvent spiffEvent = new InventoryPlanEvent();
	spiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
	spiffEvent.setBeginDate( twoDaysAgo );
	service.applyDateRules( spiffEvent, tenDaysFromNow );
    assertEquals( twoDaysAgo, spiffEvent.getBeginDate() );
    assertNull( spiffEvent.getEndDate() );
	
	InventoryPlanEvent retailOtherEvent = new InventoryPlanEvent();
	retailOtherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER_RETAIL.getId() );
	retailOtherEvent.setBeginDate( twoDaysAgo );
	service.applyDateRules( retailOtherEvent, tenDaysFromNow );
    assertEquals( twoDaysAgo, retailOtherEvent.getBeginDate() );
    assertNull( retailOtherEvent.getEndDate() );
}

public void testFindUnmodifiedAdvertisingAndLotPromoteEvents()
{ 
	// .... current events....
    InventoryPlanEvent currentAdvertisingEvent = new InventoryPlanEvent();
    currentAdvertisingEvent.setId( 1 );
    currentAdvertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    currentAdvertisingEvent.setInventoryThirdParty( new InventoryThirdParty() );
    currentAdvertisingEvent.getInventoryThirdParty().setName( "Trib" );
    
    InventoryPlanEvent secondCurrentAdvertisingEvent = new InventoryPlanEvent();
    secondCurrentAdvertisingEvent.setId( 2 );
    secondCurrentAdvertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    secondCurrentAdvertisingEvent.setInventoryThirdParty( new InventoryThirdParty());
    secondCurrentAdvertisingEvent.getInventoryThirdParty().setName( "SunTimes" );
    
    InventoryPlanEvent currentSpiffEvent = new InventoryPlanEvent();
    currentSpiffEvent.setId( 3 );
    currentSpiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
    
    InventoryPlanEvent currentLotPromoteEvent = new InventoryPlanEvent();
    currentLotPromoteEvent.setId( 4 ); 
    currentLotPromoteEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );

    InventoryPlanEvent secondCurrentLotPromoteEvent = new InventoryPlanEvent();
    secondCurrentLotPromoteEvent.setId( 5 ); 
    secondCurrentLotPromoteEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );  
    
    List<InventoryPlanEvent> currentEvents = new ArrayList<InventoryPlanEvent>();
    currentEvents.add( currentAdvertisingEvent );
    currentEvents.add( secondCurrentAdvertisingEvent );
    currentEvents.add( currentSpiffEvent ); 
    currentEvents.add( currentLotPromoteEvent ); 

	// .... updated events....    
    InventoryPlanEvent updatedAdvertisingEvent = new InventoryPlanEvent();
    updatedAdvertisingEvent.setId( 2 );
    updatedAdvertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() ); 
    updatedAdvertisingEvent.setInventoryThirdParty(new InventoryThirdParty());
    updatedAdvertisingEvent.getInventoryThirdParty().setName( "Times" );

    InventoryPlanEvent updatedSpiffEvent = new InventoryPlanEvent();
    updatedSpiffEvent.setId( 3 );
    updatedSpiffEvent.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );    
    
    List<InventoryPlanEvent> updatedEvents = new ArrayList<InventoryPlanEvent>();
    updatedEvents.add( updatedAdvertisingEvent );
    updatedEvents.add( updatedSpiffEvent );

	// .... deleted events....    
    InventoryPlanEvent deletedLotPromoteEvent = new InventoryPlanEvent();
    deletedLotPromoteEvent.setId( 5 ); 
    deletedLotPromoteEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );      
    
    List<InventoryPlanEvent> deletedEvents = new ArrayList<InventoryPlanEvent>();
    deletedEvents.add( deletedLotPromoteEvent );

    InventoryPlanningPersistenceService service = new InventoryPlanningPersistenceService();
    List<InventoryPlanEvent> unmodifiedEvents = service.findUnmodifiedAdvertisingAndLotPromoteEvents( currentEvents, updatedEvents, deletedEvents );
    assertEquals( 2, unmodifiedEvents.size() );
    
    List<Integer> expectedEventIds = new ArrayList<Integer>();
    expectedEventIds.add( 1 );
    expectedEventIds.add( 4 );
    
    for( InventoryPlanEvent event: unmodifiedEvents )
    {
        expectedEventIds.remove( event.getId() );
    }
    
    assertEquals( 0, expectedEventIds.size() );
}



public void testFindRepriceEvents()
{
    InventoryPlanEvent advertisingEvent = new InventoryPlanEvent();
    advertisingEvent.setId( 1 );
    advertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    
    InventoryPlanEvent repriceEvent = new InventoryPlanEvent();
    repriceEvent.setId( 2 );
    repriceEvent.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
    
    InventoryPlanEvent lotPromoteEvent = new InventoryPlanEvent();
    lotPromoteEvent.setId( 3 );
    lotPromoteEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
    
    InventoryPlanEvent repriceEvent2 = new InventoryPlanEvent();
    repriceEvent2.setId( 4 );
    repriceEvent2.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
    
    List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
    events.add( advertisingEvent );
    events.add( repriceEvent );
    events.add( lotPromoteEvent );
    events.add( repriceEvent2 );
    
    InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
    List<InventoryPlanEvent> repriceEvents = service.findRepriceEvents( events );
    assertEquals( 2, repriceEvents.size() );
    
    for( InventoryPlanEvent event : repriceEvents )
    {
        if ( event.getEventTypeEnum() != PlanningEventTypeEnum.REPRICE )
        {
            fail();	
        }
    }
}

public void testFindRepriceEventsNoRepriceEvents()
{
    InventoryPlanEvent advertisingEvent = new InventoryPlanEvent();
    advertisingEvent.setId( 1 );
    advertisingEvent.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    
    InventoryPlanEvent lotPromoteEvent = new InventoryPlanEvent();
    lotPromoteEvent.setId( 3 );
    lotPromoteEvent.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
    
    List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
    events.add( advertisingEvent );
    events.add( lotPromoteEvent );
    
    InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
    List<InventoryPlanEvent> repriceEvents = service.findRepriceEvents( events );
    assertTrue( repriceEvents.isEmpty() );
}

public void testFindRepriceEventsNoEvents()
{
	InventoryPlanningRetrievalService service = new InventoryPlanningRetrievalService();
    List<InventoryPlanEvent> repriceEvents = service.findRepriceEvents( new ArrayList<InventoryPlanEvent>() );
    assertTrue( repriceEvents.isEmpty() );
}

public class MockTransactionDAO implements ITransactionDAO
{

public void execute(Collection<? extends Object> objs) {
}

public void execute(Collection<? extends Object> objs,
		Collection<? extends Object> objsToDelete) {
}
}


public class MockInventoryBucketRangeDAO implements IInventoryBucketRangeDAO
{
public Integer low;
public Integer high;

public InventoryBucketRange loadBucketForInventoryAge( Integer age, Integer businessUnitId, InventoryBucketTypeEnum inventoryBucketType )
{
    InventoryBucketRange bucketRange = new InventoryBucketRange();
    bucketRange.setLow( low );
    bucketRange.setHigh( high );
    
    return bucketRange;
}

public List<InventoryBucketRange> loadBuckets(Integer businessUnitId, InventoryBucketTypeEnum inventoryBucketType) {
	return null;
}
}





}