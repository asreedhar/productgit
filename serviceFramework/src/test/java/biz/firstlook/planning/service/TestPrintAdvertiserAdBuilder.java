package biz.firstlook.planning.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.action.InventoryVehicleInfoBean;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserAdBuilderOption;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookup;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookupEnum;
import biz.firstlook.planning.persistence.InventoryVehicleOptionsDAO;
import biz.firstlook.planning.persistence.ThirdPartyEnumFilter;

public class TestPrintAdvertiserAdBuilder extends TestCase
{

private PrintAdvertiserService printAdvertiserService;

public void setUp()
{
	printAdvertiserService = new PrintAdvertiserService();
}

public void tearDown()
{}

public void testBuildAd_BasicAd()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO();
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	
	for (int c = 1; c <= 7; c++)
	{
		PrintAdvertiserAdBuilderOption option1 = new PrintAdvertiserAdBuilderOption();
		option1.setDisplayOrder( c );
		option1.setId( c );
		option1.setOption( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
		option1.setStatus( (c == 6)? Boolean.FALSE : Boolean.TRUE ); // don't want to have to deal with the 
		theOptions.add( option1 );
	}
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.EDMUNDS, theOptions );
	assertEquals( "part 1 of ad not right", "2006 Ford Focus LX VIN: 2HGFG21506H704281 Stock #: ABC123 RED Mileage: 100,000 ", adText[0] );
	assertEquals( "part 2 of ad not right", "", adText[1] );
}

public void testBuildAd_AdDescInMiddle()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO();
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	
	for (int c = 1; c <= 7; c++)
	{
		PrintAdvertiserAdBuilderOption option1 = new PrintAdvertiserAdBuilderOption();
		option1.setDisplayOrder( ( (c == 7) ? 3: ((c==3)? 7  : c) ) ); // switch positition of stock number and ad desc
		option1.setId( c );
		option1.setOption( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
		option1.setStatus( (c == 6)? Boolean.FALSE : Boolean.TRUE ); // don't want to have to deal with the 
		theOptions.add( option1 );
	}
	Collections.sort( theOptions, new BeanComparator("displayOrder") );
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.EDMUNDS, theOptions );
	assertEquals( "part 1 of ad not rigth", "2006 Ford Focus LX VIN: 2HGFG21506H704281 ", adText[0] );
	assertEquals( "part 2 of ad not rigth", "RED Mileage: 100,000 Stock #: ABC123", adText[1] );
}

public void testBuildAd_JustVehicleDescAndStockNumber()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO();
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	
	for (int c = 1; c <= 7; c++)
	{
		PrintAdvertiserAdBuilderOption option1 = new PrintAdvertiserAdBuilderOption();
		option1.setDisplayOrder( c ); 
		option1.setId( c );
		option1.setOption( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
		option1.setStatus( (c == 1 || c == 3 )? Boolean.TRUE: Boolean.FALSE ); // don't want to have to deal with the 
		theOptions.add( option1 );
	}
	Collections.sort( theOptions, new BeanComparator("displayOrder") );
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.EDMUNDS, theOptions );
	assertEquals( "part 1 of ad not rigth", "2006 Ford Focus LX Stock #: ABC123 ", adText[0] );
	assertEquals( "part 2 of ad not rigth", "", adText[1] );
}

public void testBuildAd_EverythingIncludingOptionsForNADAandTestMakeDefaultListOfOptionsMethod()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO();
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	List< PrintAdvertiserOptionLookup > theOptionsLookUp = new ArrayList< PrintAdvertiserOptionLookup >();
	
	for (int c = 1; c <= 7; c++)
	{
		theOptionsLookUp.add( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
	}
	theOptions = printAdvertiserService.makeDefaultListOfOptions( theOptionsLookUp );
	
	Collections.sort( theOptions, new BeanComparator("displayOrder") );
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.NADA, theOptions );
	assertEquals( "part 1 of ad not rigth", "2006 Ford Focus-V6 Sedan 4D 3.2 VIN: 2HGFG21506H704281 Stock #: ABC123 RED Mileage: 100,000 Leather Seats, Power Sunroof ", adText[0] );
	assertEquals( "part 2 of ad not rigth", "", adText[1] );
}

public void testBuildAd_EverythingIncludingOptionsForNADAwithAdDescInMiddle()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO();
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	
	for (int c = 1; c <= 7; c++)
	{
		PrintAdvertiserAdBuilderOption option1 = new PrintAdvertiserAdBuilderOption();
		option1.setDisplayOrder( ( (c == 7) ? 3: ((c==3)? 7  : c) ) ); // switch positition of stock number and ad desc
		option1.setId( c );
		option1.setOption( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
		option1.setStatus( Boolean.TRUE ); // don't want to have to deal with the 
		theOptions.add( option1 );
	}
	Collections.sort( theOptions, new BeanComparator("displayOrder") );
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.NADA, theOptions );
	assertEquals( "part 1 of ad not rigth", "2006 Ford Focus-V6 Sedan 4D 3.2 VIN: 2HGFG21506H704281 ", adText[0] );
	assertEquals( "part 2 of ad not rigth", "RED Mileage: 100,000 Leather Seats, Power Sunroof Stock #: ABC123", adText[1] );
}

public void testBuildAd_EverythingIncludingOptionsForNADAwithOnly1Option()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO(true, false);
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	
	for (int c = 1; c <= 7; c++)
	{
		PrintAdvertiserAdBuilderOption option1 = new PrintAdvertiserAdBuilderOption();
		option1.setDisplayOrder( ( (c == 7) ? 3: ((c==3)? 7  : c) ) ); // switch positition of stock number and ad desc
		option1.setId( c );
		option1.setOption( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
		option1.setStatus( Boolean.TRUE ); // don't want to have to deal with the 
		theOptions.add( option1 );
	}
	Collections.sort( theOptions, new BeanComparator("displayOrder") );
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.NADA, theOptions );
	assertEquals( "part 1 of ad not rigth", "2006 Ford Focus-V6 Sedan 4D 3.2 VIN: 2HGFG21506H704281 ", adText[0] );
	assertEquals( "part 2 of ad not rigth", "RED Mileage: 100,000 Leather Seats Stock #: ABC123", adText[1] );
}

public void testBuildAd_EverythingIncludingOptionsForNADAbutVehicleIsNotInNADA()
{
	InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO = getMockInventoryVehicleOptionsDAO(false, true);
	printAdvertiserService.setInventoryVehicleOptionsDAO( inventoryVehicleOptionsDAO );
	
	List< PrintAdvertiserAdBuilderOption > theOptions = new ArrayList< PrintAdvertiserAdBuilderOption >();
	
	for (int c = 1; c <= 7; c++)
	{
		PrintAdvertiserAdBuilderOption option1 = new PrintAdvertiserAdBuilderOption();
		option1.setDisplayOrder( c );
		option1.setId( c );
		option1.setOption( getPrintAdvertiserOption(PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( c )) );
		option1.setStatus( Boolean.TRUE ); // don't want to have to deal with the 
		theOptions.add( option1 );
	}
	Collections.sort( theOptions, new BeanComparator("displayOrder") );
	
	// default ad description
	String[] adText = printAdvertiserService.buildAd( 123456, ThirdPartyEnum.NADA, theOptions );
	assertEquals( "part 1 of ad not rigth", "2006 Ford Focus LX VIN: 2HGFG21506H704281 Stock #: ABC123 RED Mileage: 100,000 Leather Seats, Power Sunroof ", adText[0] );
	assertEquals( "part 2 of ad not rigth", "", adText[1] );
}

public static PrintAdvertiserOptionLookup getPrintAdvertiserOption( PrintAdvertiserOptionLookupEnum inOption )
{
	PrintAdvertiserOptionLookup option = new PrintAdvertiserOptionLookup();
	option.setDescription( inOption.getDescription() );
	option.setId( inOption.getId());
	return option;
}

private InventoryVehicleOptionsDAO getMockInventoryVehicleOptionsDAO() {
	return getMockInventoryVehicleOptionsDAO(false, false);
}

private InventoryVehicleOptionsDAO getMockInventoryVehicleOptionsDAO(final boolean oneOptionOnly, final boolean useDefaultVDesc)
{
	InventoryVehicleOptionsDAO mockDao = new InventoryVehicleOptionsDAO(){

		public InventoryVehicleInfoBean getVehicleInfo( Integer inventoryId )
		{
			return getMockInventoryVehicleInfoBean(oneOptionOnly, useDefaultVDesc);
		}

		public InventoryVehicleInfoBean getVehicleInfo( Integer inventoryId, ThirdPartyEnumFilter... thirdPartyFitler )
		{
			return getMockInventoryVehicleInfoBean(oneOptionOnly, useDefaultVDesc);
		}
	};
	return mockDao;
}

private InventoryVehicleInfoBean getMockInventoryVehicleInfoBean(boolean oneOptionOnly, boolean useDefaultVDesc) {
	InventoryVehicleInfoBean bean = new InventoryVehicleInfoBean();
	bean.setColor( "RED" );
	bean.setMake( "Ford" );
	bean.setMileage( 100000 );
	bean.setStockNumber( "ABC123" );
	bean.setVehicleDescription( "Focus LX" );
	bean.setVin( "2HGFG21506H704281" );
	bean.setYear( 2006 );
	
	String vDesc = null;
	if(useDefaultVDesc) {
		vDesc = bean.getVehicleDescription();
	} else {
		vDesc = "Focus-V6 Sedan 4D 3.2";
	}
		
	bean.add(ThirdPartyEnum.NADA, vDesc, "Leather Seats", true);
	if(!oneOptionOnly) {
		bean.add(ThirdPartyEnum.NADA, vDesc, "Power Sunroof", true);
	}

	return bean;
}


}
