package biz.firstlook.planning;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import biz.firstlook.planning.email.TestActionPlanEmailService;
import biz.firstlook.planning.service.TestInventoryPlanningService;
import biz.firstlook.planning.service.TestPrintAdvertiserAdBuilder;
import biz.firstlook.planning.service.TestPrintAdvertiserService;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    // this has been commented out becuase it was failing for a local running but 
    // passing in cruise control (or vice versa), so we deiced to remove it from
    // the all tests, but it will still be run by cruise control.
    // if you want to try and figure out why cruise control and a local run 
    // behave differently be my guest, as long as you tell me what the difference
    // was if you figure it out -DW 8/31/06
    //suite.addTestSuite(TestReplanningDataUtil.class);
    suite.addTestSuite(TestInventoryPlanningService.class);
    suite.addTestSuite(TestPrintAdvertiserAdBuilder.class);
    suite.addTestSuite(TestPrintAdvertiserService.class);
    suite.addTestSuite(TestActionPlanEmailService.class);
    
    return suite;
}
}
