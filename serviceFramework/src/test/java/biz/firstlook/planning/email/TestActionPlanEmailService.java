package biz.firstlook.planning.email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import biz.firstlook.planning.entity.MockBasicIMTLookUpDAO;
import biz.firstlook.report.dao.IBasicIMTLookUpDAO;

public class TestActionPlanEmailService extends TestCase
{
ActionPlanEmailService actionPlanEmailService;

public void setUp()
{
	actionPlanEmailService = new ActionPlanEmailService();
}

public void tearDown()
{
	
}

public void testGetContactInfoWithExtention()
{
	IBasicIMTLookUpDAO lookUpDAO = new MockBasicIMTLookUpDAO();
	
	List< Map< String, Object >> ourReults = new ArrayList< Map< String, Object >>();
	
	Map< String, Object > map1 = new HashMap< String, Object >();
	map1.put( "BusinessUnit", "First Look Business Unit" );
	map1.put( "Address1", "815 W. Van Buren" );
	map1.put( "Address2", "Suite 500" );
	map1.put( "City", "Chicago" );
	map1.put( "State", "IL" );
	map1.put( "ZipCode", "60613" );
	map1.put( "FirstName", "Pat" );
	map1.put( "LastName", "Ryan" );
	// this phone number is area code + number + extension
	map1.put( "OfficePhoneNumber", "773123456712345" );
	ourReults.add( map1 );

	((MockBasicIMTLookUpDAO)lookUpDAO).setResults( ourReults );
	actionPlanEmailService.setBasicIMTLookUpDAO( lookUpDAO );
	
	String actualResults = actionPlanEmailService.getContactInfo( 123456, 123456 );
	String expectedResults = "Pat Ryan - (773) 123-4567 ext. 12345\nFirst Look Business Unit\n815 W. Van Buren Suite 500\nChicago, IL 60613\n";
	
	assertEquals( "our generated address didn't match expected", expectedResults, actualResults );
}

public void testGetContactInfoWithoutExtention()
{
	IBasicIMTLookUpDAO lookUpDAO = new MockBasicIMTLookUpDAO();
	
	List< Map< String, Object >> ourReults = new ArrayList< Map< String, Object >>();
	
	Map< String, Object > map1 = new HashMap< String, Object >();
	map1.put( "BusinessUnit", "First Look Business Unit" );
	map1.put( "Address1", "815 W. Van Buren" );
	map1.put( "Address2", "Suite 500" );
	map1.put( "City", "Chicago" );
	map1.put( "State", "IL" );
	map1.put( "ZipCode", "60613" );
	map1.put( "FirstName", "Pat" );
	map1.put( "LastName", "Ryan" );
	// this phone number is area code + number
	map1.put( "OfficePhoneNumber", "7731234567" );
	ourReults.add( map1 );

	((MockBasicIMTLookUpDAO)lookUpDAO).setResults( ourReults );
	actionPlanEmailService.setBasicIMTLookUpDAO( lookUpDAO );
	
	String actualResults = actionPlanEmailService.getContactInfo( 123456, 123456 );
	String expectedResults = "Pat Ryan - (773) 123-4567\nFirst Look Business Unit\n815 W. Van Buren Suite 500\nChicago, IL 60613\n";
	
	assertEquals( "our generated address didn't match expected", expectedResults, actualResults );
	
}

}
