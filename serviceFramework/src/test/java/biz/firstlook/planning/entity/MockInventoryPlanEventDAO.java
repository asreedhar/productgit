package biz.firstlook.planning.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;

/**
 * Mock implementation of IInventoryPlanEventDAO for testing
 * @author kkelly
 */
public class MockInventoryPlanEventDAO implements IInventoryPlanEventDAO
{
public Integer eventTypeId;
public Integer id;
public boolean hasReplanningEventForToday = false;
public Date beginDate = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -3 ), Calendar.DATE );

public List< InventoryPlanEvent > findInChronologicalOrder( final Integer inventoryId )
{
	return null;
}

public InventoryPlanEvent findById(Integer id)
{
    InventoryPlanEvent event = new InventoryPlanEvent();
    event.setId( id );
    event.setEventTypeId( eventTypeId );
    event.setBeginDate( beginDate );
    
    return event;
}

public List<InventoryPlanEvent> findByExample(InventoryPlanEvent exampleInstance, String... excludeProperty)
{
    return null;	
}

@Transactional(readOnly=false)
public InventoryPlanEvent makePersistent(InventoryPlanEvent entity)
{
    return null;	
}

@Transactional(readOnly=false)
public void saveOrUpdate(InventoryPlanEvent entity)
{
}

public List< InventoryPlanEvent > findEventsFromLastPlan( Integer inventoryId )
{
    return null;
}

public List< InventoryPlanEvent > findPlanHistory( final Integer inventoryId )
{
    return null;
}

public List< InventoryPlanEvent > findOpenEventsByObjective( Integer inventoryId, Integer objectiveId )
{
    return null;
}

public InventoryPlanEvent findByInventoryIdAndCreatedDate( Integer inventoryId, Date createDate, Integer eventTypeId )
{
    if ( hasReplanningEventForToday )
    {
        InventoryPlanEvent event = new InventoryPlanEvent();
        event.setId( id );
        event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
        return event;
    }
    
    return null;
}

public void saveOrUpdateAll( Collection< InventoryPlanEvent > entity )
{
}

public List< InventoryPlanEvent > findEventsByThirdPartyId( Integer businessUnitId, List< InventoryThirdParty > toBeDeletedThirdParties )
{
	return null;
}

@SuppressWarnings("unchecked")
public List< InventoryPlanEvent > retrievePrintAdvertiserEventsGroupedByEndDate( Integer inventoryId )
{
	InventoryThirdParty tp1 = new InventoryThirdParty();
	tp1.setName( "thirdParty 1" );
	InventoryThirdParty tp2 = new InventoryThirdParty();
	tp2.setName( "thirdParty 2" );
	InventoryThirdParty tp3 = new InventoryThirdParty();
	tp3.setName( "thirdParty 3" );
	InventoryThirdParty tp4 = new InventoryThirdParty();
	tp4.setName( "thirdParty 4" );
	
	Date twoDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -2 );
	Date threeDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -3 );
	Date fourDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -4 );
	Date fiveDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -5 );
	
	InventoryPlanEvent event1 = getEvent( tp1, twoDaysAgo );
	
	InventoryPlanEvent event2 = getEvent( tp1, threeDaysAgo );
	InventoryPlanEvent event3 = getEvent( tp2, threeDaysAgo );
	InventoryPlanEvent event4 = getEvent( tp3, threeDaysAgo );

	InventoryPlanEvent event5 = getEvent( tp1, fourDaysAgo );
	InventoryPlanEvent event6 = getEvent( tp4, fourDaysAgo );

	InventoryPlanEvent event7 = getEvent( tp1, fiveDaysAgo ); // won't show up because only look back at past 3 dates
	
	List<InventoryPlanEvent> results = new ArrayList();
	results.add( event1 );
	results.add( event2 );
	results.add( event3 );
	results.add( event4 );
	results.add( event5 );
	results.add( event6 );
	results.add( event7 );
	
	return results;
}

private InventoryPlanEvent getEvent( InventoryThirdParty tp1, Date twoDaysAgo )
{
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setEndDate( twoDaysAgo );
	event.setInventoryThirdParty( tp1 );
	return event;
}

public List< InventoryPlanEvent > findAllEventsByTypeInTimeRange( Integer businessUnitId, Date thirtyDaysAgo )
{
	return null;
}

public InventoryPlanEvent findByIdandEventType( Integer inventoryId, PlanningEventTypeEnum internet_marketplace )
{
	return null;
}

public List<InventoryPlanEvent> retrieveCurrentAdvertisingEvents(Integer inventoryId) {
    return null;
}

public InventoryPlanEvent retrieveCurrentAdvertisingEvent(Integer inventoryId, Integer thirdPartyEntityId) {
    return null;
}

public InventoryPlanEvent findSpiffEventByInvIdAndEventType(Integer inventoryId, PlanningEventTypeEnum eventType) {
    return null;
}
}
