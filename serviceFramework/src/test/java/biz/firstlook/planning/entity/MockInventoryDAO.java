package biz.firstlook.planning.entity;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.entity.Inventory;
import biz.firstlook.main.commonDAOs.IInventoryDAO;

/**
 * Mock implementation of IInventoryDAO
 */
public class MockInventoryDAO implements IInventoryDAO {
    public Date inventoryReceivedDate;

    public Date planReminderDate;

    public Inventory testInventory = null;;

    public Inventory findById(Integer inventoryId) {
        Inventory inventory = null;

        if (testInventory != null) {
            inventory = testInventory;
        } else {
            inventory = new Inventory();
            inventory.setInventoryReceivedDate(inventoryReceivedDate);
            inventory.setPlanReminderDate(planReminderDate);
        }

        return inventory;
    }

    public List<Inventory> findByExample(Inventory exampleInstance,
            String... excludeProperty) {
        return null;
    }

    @Transactional(readOnly = false)
    public Inventory makePersistent(Inventory entity) {
        return null;
    }

    @Transactional(readOnly = false)
    public void saveOrUpdate(Inventory entity) {
    }

    @Transactional(readOnly = false)
    public void saveOrUpdateAll(Collection<Inventory> entity) {
    }

    @Transactional(readOnly = false)
    public Inventory findByStockNumber(String stockNum, Integer businessUnitId) {
        return null;
    }
}
