package biz.firstlook.planning.entity;

import java.util.List;
import java.util.Map;

import biz.firstlook.report.dao.IBasicIMTLookUpDAO;

/**
 * This is a mock version of the BasicIMTLookUpDAO which allows you to set the results set onto the dao
 * and then that results set is returned by the getRestulsMethod. 
 * @author dweintrop
 *
 */
public class MockBasicIMTLookUpDAO implements IBasicIMTLookUpDAO
{

List< Map< String, Object >> results;

public void setResults( List< Map< String, Object >> inResults )
{
	results = inResults;
}

public List< Map< String, Object >> getResults( String sql, Object[] parameterValues )
{
	return results;
}

}
