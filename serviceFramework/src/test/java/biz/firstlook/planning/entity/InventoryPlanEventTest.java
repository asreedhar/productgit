package biz.firstlook.planning.entity;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;

public class InventoryPlanEventTest extends TestCase
{

private Date today = DateUtils.truncate( new Date(), Calendar.DATE );
private Date oneDayAgo = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -3 ), Calendar.DATE );
private Date threeDaysAgo = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -3 ), Calendar.DATE );
private Date threeDaysFromNow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 3 ), Calendar.DATE );
private Date tenDaysFromNow = DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 10 ), Calendar.DATE );

public InventoryPlanEventTest()
{
	super();
}

public InventoryPlanEventTest( String name )
{
	super( name );
}

public void testIsCurrentEventSPIFF()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
    assertTrue( event.isCurrent() );
}

public void testIsCurrentEventSPIFFNotCurrent()
{  
    InventoryPlanEvent event = new InventoryPlanEvent();
    event.setEventTypeId( PlanningEventTypeEnum.SPIFF.getId() );
    event.setEndDate( new Date() );
    assertTrue( !event.isCurrent() );	
}

/**
 * a reprice event is current if its end date is 
 * the same as today's date
 */
public void testIsCurrentEventReprice()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
    event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
    event.setEndDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
    
    assertTrue( event.isCurrent() );
}

/**
 * a reprice event is not current if its end date is 
 * the same as today's date
 *
 */
public void testIsCurrentEventRepriceNotCurrent()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.REPRICE.getId() );
       
    Calendar cal = Calendar.getInstance();
    cal.set( Calendar.MONTH, Calendar.JUNE );
    cal.set( Calendar.DAY_OF_MONTH, 5 );
    cal.set( Calendar.YEAR, 2006 );
    
    event.setEndDate( cal.getTime() );  
    event.setBeginDate( cal.getTime() );
    
    assertTrue( !event.isCurrent() );
}

/**
 * a promotion event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventPromotionAtEndDate()
{	
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
    event.setBeginDate( threeDaysAgo );
	event.setEndDate( today );
	
	assertTrue( event.isCurrent() );
}

/**
 * an promotion event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventPromotionBeforeEndDate()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( threeDaysFromNow );
	assertTrue( event.isCurrent() );
}

public void testIsCurrentEventPromotionNotCurrent()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( oneDayAgo );
	assertTrue( !event.isCurrent() );
}

/** 
 * a lot promote that is set to begin in the future
 * should be considered current
 */
public void testIsCurrentEventPromotionNotYetStarted()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
	event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	event.setBeginDate( threeDaysFromNow );
	event.setEndDate( tenDaysFromNow );
	assertTrue( event.isCurrent() );
}

/** 
 * a lot promote that is set to begin today
 * should be considered current
 */
public void testIsCurrentEventPromotionBeginsToday()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
	event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	event.setBeginDate( today );
	event.setEndDate( tenDaysFromNow );
	assertTrue( event.isCurrent() );
}

/** 
 * a lot promote that is set to begin today
 * and end today should be considered current
 */
public void testIsCurrentEventPromotionBeginsAndEncsToday()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
    event.setBeginDate( today );
    event.setEndDate( today );
	assertTrue( event.isCurrent() );
}

/**
 * an advertisement event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventAdvertisementAtEndDate()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( today );	
	assertTrue( event.isCurrent() );
}

/**
 * an advertisement event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventAdvertisementBeforeEndDate()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( threeDaysFromNow );
	assertTrue( event.isCurrent() );
}

public void testIsCurrentEventAdvertisementNotCurrent()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	event.setBeginDate( threeDaysAgo );
	event.setEndDate( oneDayAgo );
	assertTrue( !event.isCurrent() );
}

/** 
 * an advertisement that is set to begin in the future
 * should be considered current
 */
public void testIsCurrentEventAdvertisementNotYetStarted()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	event.setBeginDate( threeDaysFromNow );
	event.setEndDate( tenDaysFromNow );
	assertTrue( event.isCurrent() );
}

/** 
 * an advertisement that is set to begin today
 * should be considered current
 */
public void testIsCurrentEventAdvertisementBeginsToday()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    event.setBeginDate( today );
    event.setEndDate( tenDaysFromNow );
	assertTrue( event.isCurrent() );
}

/** 
 * a lot promote that is set to begin today
 * and end today should be considered current
 */
public void testIsCurrentEventAdvertisementBeginsAndEncsToday()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
    event.setBeginDate( today );
    event.setEndDate( today );
	assertTrue( event.isCurrent() );
}

/**
 * an auction event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventAuctionAtEndDate()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( threeDaysFromNow );
	assertTrue( event.isCurrent() );
}

/**
 * an auction event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventAuctionBeforeEndDate()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( threeDaysFromNow );
	assertTrue( event.isCurrent() );
}

public void testIsCurrentEventAuctionNotCurrent()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( oneDayAgo );
	assertTrue( !event.isCurrent() );
}

/**
 * auction events with no end date should be 
 * considered current
 */
public void testIsCurrentEventAuctionNoEndDate()
{
    InventoryPlanEvent event = new InventoryPlanEvent();
    event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
    event.setBeginDate( threeDaysAgo );
	assertTrue( event.isCurrent() );
}

/**
 * a wholesaler event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventWholesalerAtEndDate()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( threeDaysFromNow );
    assertTrue( event.isCurrent() );
}

/**
 * an wholesaler event is current if its end date is 
 * less than or equal to today's date
 */
public void testIsCurrentEventWholesalerBeforeEndDate()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( threeDaysFromNow );
    assertTrue( event.isCurrent() );
}

/**
 * wholesaler events with no end date should be 
 * considered current
 */
public void testIsCurrentEventWholesalerNotCurrent()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
    event.setBeginDate( threeDaysAgo );
    event.setEndDate( oneDayAgo );
    assertTrue( !event.isCurrent() );
}

public void testIsCurrentEventWholesalerNoEndDate()
{
    InventoryPlanEvent event = new InventoryPlanEvent();
    event.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
    event.setBeginDate( threeDaysAgo );
	assertTrue( event.isCurrent() );
}

/**
 * A retail event is never current because it is part of the sold
 * strategy, and the sold strategy does not define a current plan.
 * Also, the begin date and end date of a retail event are the same,
 * which indicates that it does not have a time period associated with it.
 */
public void testIsCurrentEventRetail()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.RETAIL.getId() );
	event.setEndDate( new Date() );
	
	assertTrue( !event.isCurrent() );	
}

//*** fix the sold strategy ***//
/**
 * A wholesaler event is never current because it is part of the sold
 * strategy, and the sold strategy does not define a current plan.
 * Also, the begin date and end date of a wholesaler event are the same,
 * which indicates that it does not have a time period associated with it.
 */
/**
public void testIsCurrentEventWholesaler()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    PlanningEventType eventType = new PlanningEventType();
    eventType.setId( PlanningEventTypeEnum.RETAIL.getId() );
    eventType.setObjectiveId( PlanningEventTypeEnum.RETAIL.getObjectiveId() );
    event.setPlanningEventType( eventType );
    event.setEndDate( new Date() );
    
    assertTrue( !event.isCurrent() );	
}**/

/**
 * a retail "other" event is current if is doesn't have an end date
 */
public void testIsCurrentEventRetailOther()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.OTHER_RETAIL.getId() );
	
	assertTrue( event.isCurrent() );		
}

/**
 * a retail "other" event is current if is doesn't have an end date
 */
public void testIsCurrentEventRetailOtherNotCurrent()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.OTHER_RETAIL.getId() );
	event.setEndDate( new Date() );	
	assertTrue( !event.isCurrent() );		
}

/**
 * a wholesale "other" event is current if is doesn't have an end date
 */
public void testIsCurrentEventWholesaleOther()
{
	InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.OTHER_WHOLESALE.getId() );	
	assertTrue( event.isCurrent() );		
}

/**
 * a wholesale "other" event is current if is doesn't have an end date
 */
public void testIsCurrentEventWholesaleOtherNotCurrent()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    event.setEventTypeId( PlanningEventTypeEnum.OTHER_WHOLESALE.getId() );
    event.setEndDate( new Date() );
    assertTrue( !event.isCurrent() );		
}

/**
 * If an advertise event has the same begin and end date and that date is today, 
 * we assume the event is not current because they meant to delete it.  Windowed 
 * events are considered current for dates up to and including their end date
 * except in this case. 
 */
public void testAdvertisementSameBeginAndEndDate()
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE ); 
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
	event.setBeginDate( today );
	event.setEndDate( today );
	assertTrue( event.isCurrent() );
}

/**
 * If a lot promote event has the same begin and end date and that date is today, 
 * we assume the event is not current because they meant to delete it.  Windowed 
 * events are considered current for dates up to and including their end date
 * except in this case. 
 */
public void testLotPromoteSameBeginAndEndDate()
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE ); 
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setEventTypeId( PlanningEventTypeEnum.PROMOTION.getId() );
	event.setBeginDate( today );
	event.setEndDate( today );
	assertTrue( event.isCurrent() );
}
/**
 * If an auction event has the same begin and end date and that date is today, 
 * we assume the event is not current because they meant to delete it.  Windowed 
 * events are considered current for dates up to and including their end date
 * except in this case. 
 */
public void testAuctionSameBeginAndEndDate()
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE ); 
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setEventTypeId( PlanningEventTypeEnum.AUCTION.getId() );
	event.setBeginDate( today );
	event.setEndDate( today );
	assertTrue( event.isCurrent() );
}
/**
 * If a wholesaler event has the same begin and end date and that date is today, 
 * we assume the event is not current because they meant to delete it.  Windowed 
 * events are considered current for dates up to and including their end date
 * except in this case. 
 */
public void testWholesalerSameBeginAndEndDate()
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE ); 
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setEventTypeId( PlanningEventTypeEnum.WHOLESALER.getId() );
	event.setBeginDate( today );
	event.setEndDate( today );
	assertTrue( event.isCurrent() );
}

// *** fix this *** //
/**
 * An "other" event that is part of the "other" strategy is never current
 * because the "other" strategy does not define a current plan.
 */
/**
public void testIsCurrentEventOther()
{
    InventoryPlanEvent event = new InventoryPlanEvent(); 
    PlanningEventType eventType = new PlanningEventType();
    eventType.setId( PlanningEventTypeEnum.OTHER.getId() );
    eventType.setObjectiveId( PlanningEventTypeEnum.OTHER.getObjectiveId() );
    event.setPlanningEventType( eventType );
    event.setEndDate( new Date() );
    
    assertTrue( !event.isCurrent() );	
}**/
}
