package biz.firstlook.planning.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import biz.firstlook.main.enumerator.PlanningGroupByEnum;
import biz.firstlook.planning.form.InventoryItemPlanInfo;

public class TestReplanningDataUtil extends TestCase
{

public TestReplanningDataUtil()
{
	super();
}

public TestReplanningDataUtil( String arg0 )
{
	super( arg0 );
}

public void testGroupReplanningDataByRange()
{
	List< InventoryItemPlanInfo > ungroupedVehicles = new ArrayList< InventoryItemPlanInfo >();

	InventoryItemPlanInfo firstVehicle = new InventoryItemPlanInfo();
	firstVehicle.setRangeId(1);

	InventoryItemPlanInfo secondVehicle = new InventoryItemPlanInfo();
	secondVehicle.setRangeId(1);

	InventoryItemPlanInfo thirdVehicle = new InventoryItemPlanInfo();
	thirdVehicle.setRangeId(2);

	InventoryItemPlanInfo fourthVehicle = new InventoryItemPlanInfo();
	fourthVehicle.setRangeId(2);

	InventoryItemPlanInfo fifthVehicle = new InventoryItemPlanInfo();
	fifthVehicle.setRangeId(3);

	ungroupedVehicles.add( firstVehicle );
	ungroupedVehicles.add( secondVehicle );
	ungroupedVehicles.add( thirdVehicle );
	ungroupedVehicles.add( fourthVehicle );
	ungroupedVehicles.add( fifthVehicle );

	Map<Object, List<InventoryItemPlanInfo>> groupedVehicles = InventoryPlanningUtil.groupReplanningDataByRange( ungroupedVehicles, PlanningGroupByEnum.AGE_BUCKET );

	assertEquals( 3, groupedVehicles.keySet().size() );
	assertEquals( firstVehicle, ( groupedVehicles.get( "1" ) ).get( 0 ) );
	assertEquals( secondVehicle, ( groupedVehicles.get( "1"  ) ).get( 1 ) );
	assertEquals( thirdVehicle, ( groupedVehicles.get( "2" ) ).get( 0 ) );
	assertEquals( fourthVehicle, ( groupedVehicles.get( "2" ) ).get( 1 ) );
	assertEquals( fifthVehicle, ( groupedVehicles.get( "3" ) ).get( 0 ) );
}

/**
 * test that the list of vehicles is places into the MultiValueMap under a single key, when "no grouping" is specified
 */
public void testGroupReplanningDataByRangeNoGrouping()
{
	List< InventoryItemPlanInfo > ungroupedVehicles = new ArrayList< InventoryItemPlanInfo >();

	InventoryItemPlanInfo firstVehicle = new InventoryItemPlanInfo();
	firstVehicle.setRangeId(1);

	InventoryItemPlanInfo secondVehicle = new InventoryItemPlanInfo();
	secondVehicle.setRangeId(1);

	InventoryItemPlanInfo thirdVehicle = new InventoryItemPlanInfo();
	thirdVehicle.setRangeId(2);

	InventoryItemPlanInfo fourthVehicle = new InventoryItemPlanInfo();
	fourthVehicle.setRangeId(2);

	InventoryItemPlanInfo fifthVehicle = new InventoryItemPlanInfo();
	fifthVehicle.setRangeId(3);

	ungroupedVehicles.add( firstVehicle );
	ungroupedVehicles.add( secondVehicle );
	ungroupedVehicles.add( thirdVehicle );
	ungroupedVehicles.add( fourthVehicle );
	ungroupedVehicles.add( fifthVehicle );

	Map<Object, List<InventoryItemPlanInfo>> groupedVehicles = InventoryPlanningUtil.groupReplanningDataByRange( ungroupedVehicles, PlanningGroupByEnum.NO_GROUPING );

	assertEquals( 1, groupedVehicles.keySet().size() );
	assertEquals( ungroupedVehicles, groupedVehicles.get( PlanningGroupByEnum.NO_GROUPING.getId() ));
}

/**
 * Method made to correct sort order of lineitems of Replanning tab. This is a quick fix; see dbo.GetAgingInventoryPlanSummary.UDF. RangeID is
 * used for sorting, as is the SortOrder column. -bf
 * 
 */
public void testSortGroupingDescriptions()
{
	//bucketKeys is RangeId of InventoryBucketRanges
	//Order of buckets is:
	//1:WATCHLIST, 2:0-29, 3:30-39, ... 6:60+, 0:REPLANNING
	Set<Integer> bucketKeys = new HashSet<Integer>();
	for( int i = 1; i < 7; i++ )
		bucketKeys.add( i );
	
	bucketKeys.add( 0 );
	
	Object[] sortedBucketKeys = InventoryPlanningUtil.sortGroupingDescriptions( bucketKeys, PlanningGroupByEnum.AGE_BUCKET );
	
	for( int j = 0; j < 7; j++)
		assertEquals( j, sortedBucketKeys[6-j] );
}

/**
 * Method made to correct sort order of lineitems of Replanning tab. This is a quick fix; see dbo.GetAgingInventoryPlanSummary.UDF. RangeID is
 * used for sorting, as is the SortOrder column. -bf
 * EMPTY buckets! - this means no vehicles for Replanning.
 */
public void testSortEmptyGroupingDescriptions()
{
	//bucketKeys is RangeId of InventoryBucketRanges
	//Order of buckets is:
	//1:WATCHLIST, 2:0-29, 3:30-39, ... 6:60+, 0:REPLANNING
	Set<Integer> bucketKeys = new HashSet<Integer>();
	
	Object[] sortedBucketKeys = null;
	try
	{
		 sortedBucketKeys = InventoryPlanningUtil.sortGroupingDescriptions( bucketKeys, PlanningGroupByEnum.AGE_BUCKET );
	}
	catch( Exception e )
	{
		fail( "Jim Lundie wants to see his buckets." );
	}
	
	assertEquals( 0, sortedBucketKeys.length );
}

}