package biz.firstlook.main.actionFilter;

import junit.framework.TestCase;

public class NextGenSessionTest extends TestCase
{

public NextGenSessionTest( String name )
{
	super( name );
}

public void testIsEdgeStore()
{
    NextGenSession nextGenSession = new NextGenSession();
    nextGenSession.setProgramTypeCode( 2 );
    assertTrue( nextGenSession.isEdgeStore() );
}

public void testIsEdgeStoreReallyVIPStore()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 1 );
	assertFalse( nextGenSession.isEdgeStore() );
}

public void testIsEdgeStoreReallyDealersResourceStore()
{
    NextGenSession nextGenSession = new NextGenSession();
    nextGenSession.setProgramTypeCode( 3 );
    assertFalse( nextGenSession.isEdgeStore() );
}

public void testIsVIPStore()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 1 );
	assertTrue( nextGenSession.isVIPStore() );
}

public void testIsVIPStoreReallyEdgeStore()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 2 );
	assertFalse( nextGenSession.isVIPStore() );
}

public void testIsVIPStoreReallyDealersResourceStore()
{
    NextGenSession nextGenSession = new NextGenSession();
    nextGenSession.setProgramTypeCode( 3 );
    assertFalse( nextGenSession.isVIPStore() );
}

public void testIsDealersResourceStore()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 3 );
	assertTrue( nextGenSession.isDealersResourceStore() );
}

public void testIsDealersResourceStoreReallyVIPStore()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 1 );
	assertFalse( nextGenSession.isDealersResourceStore() );
}

public void testIsDealersResourceStoreReallyEdgeStore()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 2 );
	assertFalse( nextGenSession.isDealersResourceStore() );
}

public void testShowNavigatorVIP()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 1 );
	nextGenSession.setIncludeNavigatorVIPAndDealersResource( true );
	assertTrue( nextGenSession.getHasNavigator() );
}

public void testShouldntShowNavigatorVIP()
{
	NextGenSession nextGenSession = new NextGenSession();
	nextGenSession.setProgramTypeCode( 1 );
	nextGenSession.setIncludeNavigatorVIPAndDealersResource( false );
	assertFalse( nextGenSession.getHasNavigator() );    	
}

public void testShowNavigatorDealersResource()
{
    NextGenSession nextGenSession = new NextGenSession();
    nextGenSession.setProgramTypeCode( 3 );
    nextGenSession.setIncludeNavigatorVIPAndDealersResource( true );
    assertTrue( nextGenSession.getHasNavigator() );
}

public void testShouldntShowNavigatorDealersResource()
{
    NextGenSession nextGenSession = new NextGenSession();
    nextGenSession.setProgramTypeCode( 3 );
    nextGenSession.setIncludeNavigatorVIPAndDealersResource( false );
    assertFalse( nextGenSession.getHasNavigator() );    	
}

public void testShowNavigatorEdgeStore()
{
    NextGenSession nextGenSession = new NextGenSession();
    nextGenSession.setProgramTypeCode( 2 );
    nextGenSession.setIncludeNavigatorVIPAndDealersResource( false );
    assertTrue( nextGenSession.getHasNavigator() );  
}

}
