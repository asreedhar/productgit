package biz.firstlook.main.commonServices;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import biz.firstlook.entity.ThirdPartyCategories;
import biz.firstlook.main.commonDAOs.IPreferencesDAO;
import biz.firstlook.main.commonDAOs.IThirdPartyCategoriesDAO;

public class ThirdPartyCategoriesServiceTest extends TestCase
{
public ThirdPartyCategoriesServiceTest( String name )
{
	super( name );
}

@SuppressWarnings("unchecked")
public void testGetGuideBookIdsAsList()
{
	Map< String, Object > preferences = new HashMap< String, Object >();
	preferences.put( "GuideBookID", new Integer( 1 ) );
	preferences.put( "GuideBook2Id", new Integer( 2 ) );

	Class[] paramTypes = { Map.class };
	try
	{
		Method method = ThirdPartyCategoriesService.class.getDeclaredMethod( "getGuideBookIds", paramTypes );
		method.setAccessible( true );

		ThirdPartyCategoriesService service = new ThirdPartyCategoriesService();
		List< Integer > guideBookIds = (List< Integer >)method.invoke( service, preferences );

		assertTrue( guideBookIds.size() == 2 );
		assertTrue( guideBookIds.contains( 1 ) );
		assertTrue( guideBookIds.contains( 2 ) );
	}
	catch ( Exception e )
	{
		fail( e.getMessage() );
	}
}

@SuppressWarnings("unchecked")
public void testGetGuideBookIdsAsListOneGuidebook()
{
	Map< String, Object > preferences = new HashMap< String, Object >();
	preferences.put( "GuideBookID", new Integer( 1 ) );
	preferences.put( "GuideBook2Id", new Integer( 0 ) );

	Class[] paramTypes = { Map.class };
	try
	{
		Method method = ThirdPartyCategoriesService.class.getDeclaredMethod( "getGuideBookIds", paramTypes );
		method.setAccessible( true );

		ThirdPartyCategoriesService service = new ThirdPartyCategoriesService();
		List< Integer > guideBookIds = (List< Integer >)method.invoke( service, preferences );

		assertTrue( guideBookIds.size() == 1 );
		assertTrue( guideBookIds.contains( 1 ) );
	}
	catch ( Exception e )
	{
		fail( e.getMessage() );
	}
}

public void testSplitListByThirdPartyId()
{
	ThirdPartyCategories nadaRetail = new ThirdPartyCategories();
	nadaRetail.setId( 1 );
	nadaRetail.setThirdPartyId( 2 );

	ThirdPartyCategories nadaTradeIn = new ThirdPartyCategories();
	nadaTradeIn.setId( 2 );
	nadaTradeIn.setThirdPartyId( 2 );

	ThirdPartyCategories nadaLoan = new ThirdPartyCategories();
	nadaLoan.setId( 3 );
	nadaLoan.setThirdPartyId( 2 );

	ThirdPartyCategories kbbRough = new ThirdPartyCategories();
	kbbRough.setId( 7 );
	kbbRough.setThirdPartyId( 3 );

	ThirdPartyCategories kbbWholesale = new ThirdPartyCategories();
	kbbWholesale.setId( 8 );
	kbbWholesale.setThirdPartyId( 3 );

	List< ThirdPartyCategories > thirdPartyCategories = new ArrayList< ThirdPartyCategories >();
	thirdPartyCategories.add( nadaRetail );
	thirdPartyCategories.add( nadaTradeIn );
	thirdPartyCategories.add( nadaLoan );
	thirdPartyCategories.add( kbbWholesale );
	thirdPartyCategories.add( kbbWholesale );
	
	List<Integer> guidebookIds = new ArrayList<Integer>();
	guidebookIds.add(2);
	guidebookIds.add(3);

	Class[] paramTypes = { List.class, List.class };
	try
	{
		Method method = ThirdPartyCategoriesService.class.getDeclaredMethod( "splitListByThirdPartyId", paramTypes );
		method.setAccessible( true );

		Object[] args = { thirdPartyCategories, guidebookIds };
		ThirdPartyCategoriesService service = new ThirdPartyCategoriesService();
		List result = (List)method.invoke( service, args );

		List nadaThirdPartyCategories = (List)result.get( 0 );
		List kbbThirdPartyCategories = (List)result.get( 1 );

		assertTrue( nadaThirdPartyCategories.size() == 3 );
		assertTrue( kbbThirdPartyCategories.size() == 2 );
	}
	catch ( Exception e )
	{
		fail( e.getMessage() );
	}
}

@SuppressWarnings("unchecked")
public void testGetThirdPartyCategories()
{
	int primaryGuideBookId = 2;
	int secondaryGuideBookId = 3;

	MockThirdPartyPreferencesDAO thirdPartyPreferencesDAO = new MockThirdPartyPreferencesDAO();
	thirdPartyPreferencesDAO.primaryGuideBookId = primaryGuideBookId;
	thirdPartyPreferencesDAO.secondaryGuideBookId = secondaryGuideBookId;

	PreferenceService preferenceService = new PreferenceService();
	preferenceService.setPreferencesDAO( thirdPartyPreferencesDAO );

	ThirdPartyCategories primaryCat1 = new ThirdPartyCategories();
	primaryCat1.setThirdPartyId( primaryGuideBookId );
	primaryCat1.setId( 3 );

	ThirdPartyCategories primaryCat2 = new ThirdPartyCategories();
	primaryCat2.setThirdPartyId( primaryGuideBookId );
	primaryCat2.setId( 4 );

	ThirdPartyCategories secondaryCat1 = new ThirdPartyCategories();
	secondaryCat1.setThirdPartyId( secondaryGuideBookId );
	secondaryCat1.setId( 7 );

	ThirdPartyCategories secondaryCat2 = new ThirdPartyCategories();
	secondaryCat2.setThirdPartyId( secondaryGuideBookId );
	secondaryCat2.setId( 8 );

	ThirdPartyCategories secondaryCat3 = new ThirdPartyCategories();
	secondaryCat3.setThirdPartyId( secondaryGuideBookId );
	secondaryCat3.setId( 9 );

	List< ThirdPartyCategories > testThirdPartyCategories = new ArrayList< ThirdPartyCategories >();
	testThirdPartyCategories.add( primaryCat1 );
	testThirdPartyCategories.add( primaryCat2 );
	testThirdPartyCategories.add( secondaryCat1 );
	testThirdPartyCategories.add( secondaryCat2 );
	testThirdPartyCategories.add( secondaryCat3 );

	MockThirdPartyCategoriesDAO mockThirdPartyCategoriesDAO = new MockThirdPartyCategoriesDAO();
	mockThirdPartyCategoriesDAO.thirdPartyCategories = testThirdPartyCategories;

	ThirdPartyCategoriesService thirdPartyCategoriesService = new ThirdPartyCategoriesService();
	thirdPartyCategoriesService.setThirdPartyCategoriesDAO( mockThirdPartyCategoriesDAO );
	thirdPartyCategoriesService.setPreferenceService( preferenceService );

	List thirdPartyData = thirdPartyCategoriesService.getThirdPartyCategories( 100215 );

	assertTrue( thirdPartyData.size() == 2 );

	List< ThirdPartyCategories > primaryThirdPartyCategories = (List< ThirdPartyCategories >)thirdPartyData.get( 0 );
	List< ThirdPartyCategories > secondaryThirdPartyCategories = (List< ThirdPartyCategories >)thirdPartyData.get( 1 );

	assertTrue( primaryThirdPartyCategories.size() == 2 );
	assertTrue( secondaryThirdPartyCategories.size() == 3 );

	assertTrue( primaryThirdPartyCategories.contains( primaryCat1 ) && primaryThirdPartyCategories.contains( primaryCat2 ) );
	assertTrue( secondaryThirdPartyCategories.contains( secondaryCat1 )
			&& secondaryThirdPartyCategories.contains( secondaryCat2 ) && secondaryThirdPartyCategories.contains( secondaryCat3 ) );
}

@SuppressWarnings("unchecked")
public void testGetThirdPartyCategoriesOneBook()
{
	int primaryGuideBookId = 2;

	MockThirdPartyPreferencesDAO thirdPartyPreferencesDAO = new MockThirdPartyPreferencesDAO();
	thirdPartyPreferencesDAO.primaryGuideBookId = primaryGuideBookId;

	PreferenceService preferenceService = new PreferenceService();
	preferenceService.setPreferencesDAO( thirdPartyPreferencesDAO );

	ThirdPartyCategories primaryCat1 = new ThirdPartyCategories();
	primaryCat1.setThirdPartyId( primaryGuideBookId );
	primaryCat1.setId( 3 );

	ThirdPartyCategories primaryCat2 = new ThirdPartyCategories();
	primaryCat2.setThirdPartyId( primaryGuideBookId );
	primaryCat2.setId( 4 );

	List< ThirdPartyCategories > testThirdPartyCategories = new ArrayList< ThirdPartyCategories >();
	testThirdPartyCategories.add( primaryCat1 );
	testThirdPartyCategories.add( primaryCat2 );

	MockThirdPartyCategoriesDAO mockThirdPartyCategoriesDAO = new MockThirdPartyCategoriesDAO();
	mockThirdPartyCategoriesDAO.thirdPartyCategories = testThirdPartyCategories;

	ThirdPartyCategoriesService thirdPartyCategoriesService = new ThirdPartyCategoriesService();
	thirdPartyCategoriesService.setThirdPartyCategoriesDAO( mockThirdPartyCategoriesDAO );
	thirdPartyCategoriesService.setPreferenceService( preferenceService );

	List thirdPartyData = thirdPartyCategoriesService.getThirdPartyCategories( 100215 );

	assertTrue( thirdPartyData.size() == 2 );

	List< ThirdPartyCategories > primaryThirdPartyCategories = (List< ThirdPartyCategories >)thirdPartyData.get( 0 );
	List< ThirdPartyCategories > secondaryThirdPartyCategories = (List< ThirdPartyCategories >)thirdPartyData.get( 1 );

	assertTrue( primaryThirdPartyCategories.size() == 2 );
	assertTrue( secondaryThirdPartyCategories.size() == 0 );
	assertTrue( primaryThirdPartyCategories.contains( primaryCat1 ) && primaryThirdPartyCategories.contains( primaryCat1 ) );
}

public class MockThirdPartyCategoriesDAO implements IThirdPartyCategoriesDAO
{
public List< ThirdPartyCategories > thirdPartyCategories;

// public List<ThirdPartyCategories> secondaryThirdPartyCategories;

public List< ThirdPartyCategories > findAllByThirdPartyId( final List< Integer > thirdPartyIds )
{
	List< ThirdPartyCategories > results = new ArrayList< ThirdPartyCategories >();
	results.addAll( thirdPartyCategories );
	// results.addAll( secondaryThirdPartyCategories );

	return results;
}
}

public class MockThirdPartyPreferencesDAO implements IPreferencesDAO
{
public int primaryGuideBookId;
public int secondaryGuideBookId;

public Map< String, Object > getPreferences( Integer businessUnitId )
{
	Map< String, Object > preferences = new HashMap< String, Object >();
	preferences.put( "GuideBookID", primaryGuideBookId );
	preferences.put( "GuideBook2Id", secondaryGuideBookId );

	return preferences;
}

public List<String> getMemberStatusCodePreferences(Integer memberId) {
	// TODO Auto-generated method stub
	return null;
}

public Integer getMemberInventoryOverviewSortOrderType(Integer memberId) {
	return null;
}

public void setMemberInventoryOverviewSortOrderType(Integer sortBy, Integer memberId) {}

public Map<String, Object> fetchMMR() {
	return null;
}
}
}  
