package biz.firstlook.module.purchasing.vehicle.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;

public class MockVehicleServiceImpl implements VehicleService {

	private Map<Integer, Model> models = new HashMap<Integer, Model>();
	
	public List<Make> getMakes() {
		return null;
	}

	public Model getModel(Integer id) {
		return models.get(id);
	}
	
	public Model putModel(Model model) {
		return models.put(model.getId(), model);
	}

	public ModelGroup getModelGroup(Model model) {
		return null;
	}

	public ModelGroup getModelGroup(String make, String line, Integer segmentID) {
		return null;
	}

	public List<ModelYear> getModelYears(Model model) {
		return null;
	}

	public List<ModelYear> getModelYears(ModelGroup modelGroup) {
		return null;
	}

	public List<Model> getModels(Make make) {
		return null;
	}

	public List<Model> getModels(String make, String line) {
		return null;
	}

	public List<Model> getModels(ModelGroup modelGroup) {
		return null;
	}

}
