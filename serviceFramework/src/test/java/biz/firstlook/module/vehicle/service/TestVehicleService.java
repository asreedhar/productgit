package biz.firstlook.module.vehicle.service;

import java.util.Collection;

import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleLineImpl;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleMakeImpl;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleModelImpl;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleSegmentImpl;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.utility.ContextLoader;
import biz.firstlook.utility.HibernateTestCase;

public class TestVehicleService extends HibernateTestCase {

	private VehicleService vehicleService;
	
	public TestVehicleService() {
		super();
	}

	public TestVehicleService(String name) {
		super(name);
	}
	
	protected String getSessionFactoryName() {
		return "sessionFactory";
	}
	
	protected void setUp() throws Exception {
		if (vehicleService == null) {
			vehicleService = (VehicleService) ContextLoader.getBean("purchasingVehicleService");
		}
		super.setUp();
	}
	
	public void testGetMakes() {
		Collection<Make> makes = vehicleService.getMakes();
		assertNotNull(makes);
		assertTrue(!makes.isEmpty());
	}
	
	public void testGetModels() {
		Collection<Model> models = vehicleService.getModels(
				new SimpleMakeImpl(46, "Ferrari"));
		assertNotNull(models);
		assertTrue(!models.isEmpty());
	}
	
	public void testGetModelYears() {
		Collection<ModelYear> modelYears = vehicleService.getModelYears(
				new SimpleModelImpl(
						590,
						"360",
						new SimpleMakeImpl(46, "Ferrari"),
						new SimpleLineImpl(353, "360"),
						new SimpleSegmentImpl(new Integer(4)),
						null));
		assertNotNull(modelYears);
		assertTrue(!modelYears.isEmpty());
	}
	
}
