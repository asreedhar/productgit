package biz.firstlook.module.appraisal.entity.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.module.appraisal.entity.Appraisal;

public class TestAppraisalLookUpImplAndAppraisalActionLookupImpl extends TestCase
{

private Map<String, Object> testAppraisalResultMap1;
private Map<String, Object> testAppraisalResultMap2;

@Override
protected void setUp() throws Exception
{
	// TODO Auto-generated method stub
	super.setUp();
	
	testAppraisalResultMap1 = new HashMap< String, Object >();
	testAppraisalResultMap1.put( "AppraisalID", new Integer(307501) );
	testAppraisalResultMap1.put( "BusinessUnitID", new Integer(100927) );
	testAppraisalResultMap1.put( "DateCreated", DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -30 ), Calendar.DATE ) );
	testAppraisalResultMap1.put( "VehicleID", new Integer(2455230) );
	testAppraisalResultMap1.put( "AppraisalActionID", new Integer(307442) );
	testAppraisalResultMap1.put( "AppraisalActionTypeID", new Integer(3) );

	testAppraisalResultMap2 = new HashMap< String, Object >();
	testAppraisalResultMap2.put( "AppraisalID", new Integer(296891) );
	testAppraisalResultMap2.put( "BusinessUnitID", new Integer(100215) );
	testAppraisalResultMap2.put( "DateCreated", DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -25 ), Calendar.DATE ));
	testAppraisalResultMap2.put( "VehicleID", new Integer(2952437) );
	testAppraisalResultMap2.put( "AppraisalActionID", new Integer(296894) );
	testAppraisalResultMap2.put( "AppraisalActionTypeID", new Integer(5) );
}

public void testCreationOfAppraisalLookUpImpl()
{
	Appraisal appraisal1 = new AppraisalLookUpImpl( testAppraisalResultMap1 );
	
	assertEquals("appraisalId is wrong", appraisal1.getId(), new Integer(307501) );
	assertEquals("BusinessUnitID is wrong", appraisal1.getBusinessUnitId(), new Integer(100927) );
	assertEquals("DateCreated is wrong", appraisal1.getDateCrated(),  DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -30 ), Calendar.DATE ) );
	assertEquals("AppraisalActionID is wrong", appraisal1.getAppraisalAction().getId(), new Integer(307442) );
	assertEquals("AppraisalActionType is wrong", appraisal1.getAppraisalAction().getAppraisalActionType(), AppraisalActionTypeEnum.WHOLESALE_OR_AUCTION );
	assertEquals("AppraisalAction's appraisal ID is wrong", appraisal1.getAppraisalAction().getAppraisalId(), new Integer(307501) );
	
	Appraisal appraisal2 = new AppraisalLookUpImpl( testAppraisalResultMap2 );
	assertEquals("AppraisalActionType is wrong", appraisal2.getAppraisalAction().getAppraisalActionType(), AppraisalActionTypeEnum.DECIDE_LATER );
}

public void testCompareToAndEquals()
{
	Appraisal appraisal1 = new AppraisalLookUpImpl( testAppraisalResultMap1 );
	Appraisal appraisal2 = new AppraisalLookUpImpl( testAppraisalResultMap2 );
	Appraisal appraisal3 = new AppraisalLookUpImpl( testAppraisalResultMap1 );
	
	assertEquals("equals ain't right", appraisal1, appraisal1);
	assertEquals("equals ain't right", appraisal1, appraisal3);
	assertFalse( "equals ain't right", appraisal1.equals( appraisal2 ) );
}

}
