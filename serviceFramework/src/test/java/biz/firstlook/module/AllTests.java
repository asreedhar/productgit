package biz.firstlook.module;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite( biz.firstlook.module.appraisal.entity.impl.TestAppraisalLookUpImplAndAppraisalActionLookupImpl.class);
//  suite.addTestSuite( biz.firstlook.module.core.service.TestCoreService.class);
//	suite.addTestSuite( biz.firstlook.module.market.search.TestMarketSearch.class);
//  suite.addTestSuite( biz.firstlook.module.vehicle.service.TestVehicleService.class);
    return suite;
}
}

