package biz.firstlook.module.market.search.impl.entity.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import junit.framework.TestCase;

import org.springframework.jdbc.core.JdbcTemplate;

import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.impl.SimpleMarketVehicleImpl;
import biz.firstlook.module.market.search.impl.SimpleMarketVehicleImplTestWrapper;
import biz.firstlook.utility.ContextLoader;

public class TestSearchResultDAO extends TestCase
{

private SearchResultDAO searchResultDAO;
private JdbcTemplate jdbcTemplate;

private static String LONGO_QUERY_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Search><BusinessUnit ID=\"100215\" MaxDistanceFromDealer=\"1000\"/><AccessGroups><AccessGroup ID=\"138\"/><AccessGroup ID=\"140\"/><AccessGroup ID=\"1\"/><AccessGroup ID=\"2\"/><AccessGroup ID=\"3\"/><AccessGroup ID=\"146\"/><AccessGroup ID=\"141\"/></AccessGroups><Auction/><Filter><MakeModelGrouping SegmentID=\"2\" Make=\"CHEVROLET\" Model=\"SILVERADO\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"Sierra Classic 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Ford\" Model=\"Mustang\"/><MakeModelGrouping SegmentID=\"2\" Make=\"CHEVROLET\" Model=\"K1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Civic\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2000</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"8\" Make=\"Toyota\" Model=\"Matrix\"><Criteria Type=\"Year\"><Value>2003</Value><Value>2005</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Toyota\" Model=\"Avalon\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2004</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Ford\" Model=\"Explorer\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2001</Value><Value>2002</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Accord\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Chevrolet\" Model=\"TrailBlazer EXT\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Toyota\" Model=\"RAV4\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2003</Value><Value>2004</Value><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Nissan\" Model=\"Pathfinder\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Nissan\" Model=\"Xterra\"><Criteria Type=\"Year\"><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"8\" Make=\"Scion\" Model=\"xB\"><Criteria Type=\"Year\"><Value>2005</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Volkswagen\" Model=\"Jetta\"/><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"K1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Nissan\" Model=\"Sentra\"><Criteria Type=\"Year\"><Value>2001</Value><Value>2003</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Honda\" Model=\"Civic\"/><MakeModelGrouping SegmentID=\"2\" Make=\"Dodge\" Model=\"Ram Pickup 1500\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Toyota\" Model=\"FJ Cruiser\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Toyota\" Model=\"Camry\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2003</Value><Value>2004</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"Sierra 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Scion\" Model=\"xA\"/><MakeModelGrouping SegmentID=\"7\" Make=\"Toyota\" Model=\"Camry Solara\"/><MakeModelGrouping SegmentID=\"5\" Make=\"Toyota\" Model=\"Sienna\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2004</Value><Value>2005</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"5\" Make=\"Chevrolet\" Model=\"Venture\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Nissan\" Model=\"Altima\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Ford\" Model=\"F-150\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Scion\" Model=\"tC\"/><MakeModelGrouping SegmentID=\"2\" Make=\"Toyota\" Model=\"Tundra\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2003</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Hyundai\" Model=\"Accent\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Toyota\" Model=\"Sequoia\"><Criteria Type=\"Year\"><Value>2001</Value><Value>2002</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Audi\" Model=\"A4\"/><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"CLASSIC C/K 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Toyota\" Model=\"Highlander\"><Criteria Type=\"Year\"><Value>2001</Value><Value>2002</Value><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"Silverado 1500 SS\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"8\" Make=\"Chrysler\" Model=\"PT Cruiser\"><Criteria Type=\"Year\"><Value>2002</Value><Value>2003</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Toyota\" Model=\"Celica\"><Criteria Type=\"Year\"><Value>2001</Value><Value>2003</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"Sierra 1500HD\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"4\" Make=\"Toyota\" Model=\"Camry Solara\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2002</Value><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Hyundai\" Model=\"Sonata\"/><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"Silverado 1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Honda\" Model=\"Accord\"><Criteria Type=\"Year\"><Value>2001</Value><Value>2002</Value><Value>2003</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"Element\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Toyota\" Model=\"4Runner\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2000</Value><Value>2001</Value><Value>2003</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Toyota\" Model=\"Prius\"/><MakeModelGrouping SegmentID=\"2\" Make=\"CHEVROLET\" Model=\"C1500\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Chevrolet\" Model=\"Malibu\"/><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"C/K 1500 Series\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Toyota\" Model=\"Land Cruiser\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Dodge\" Model=\"Neon\"/><MakeModelGrouping SegmentID=\"2\" Make=\"Toyota\" Model=\"Tacoma\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2003</Value><Value>2004</Value><Value>2005</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"Jeep\" Model=\"Grand Cherokee\"><Criteria Type=\"Year\"><Value>2001</Value><Value>2003</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"6\" Make=\"GMC\" Model=\"Envoy XL\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Hyundai\" Model=\"Accent\"/><MakeModelGrouping SegmentID=\"2\" Make=\"Chevrolet\" Model=\"Silverado 1500HD\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Mitsubishi\" Model=\"Galant\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Toyota\" Model=\"Corolla\"><Criteria Type=\"Year\"><Value>1999</Value><Value>2000</Value><Value>2001</Value><Value>2002</Value><Value>2003</Value><Value>2004</Value><Value>2006</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"Nissan\" Model=\"Frontier\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2001</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"3\" Make=\"Kia\" Model=\"Rio\"/><MakeModelGrouping SegmentID=\"3\" Make=\"Volkswagen\" Model=\"Passat\"/><MakeModelGrouping SegmentID=\"6\" Make=\"Honda\" Model=\"CR-V\"><Criteria Type=\"Year\"><Value>2000</Value><Value>2003</Value><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"2\" Make=\"GMC\" Model=\"SIERRA\"><Criteria Type=\"Year\"><Value>2004</Value></Criteria></MakeModelGrouping><MakeModelGrouping SegmentID=\"8\" Make=\"Suzuki\" Model=\"Aerio\"/><MakeModelGrouping SegmentID=\"4\" Make=\"Mitsubishi\" Model=\"Eclipse\"/></Filter></Search>";
private static String CREATE_ORIGINAL_PROC;
private static String DROP_ORIGINAL_PROC;

public TestSearchResultDAO(){
	DataSource dataSource = (DataSource)ContextLoader.getBean( "imtDataSource" );
	jdbcTemplate = new JdbcTemplate( dataSource );
	searchResultDAO = (SearchResultDAO)ContextLoader.getBean( "searchResultDAO" );
}

static {
	//Read stored procedure sql into memory
	try {
		BufferedReader bufferedFileReader = new BufferedReader( new FileReader("src/test/java/biz/firstlook/module/market/search/impl/entity/dao/dbo.GetPurchasingCenterVehiclesByMakeModelGroupingXML_v1.sql") );
		StringBuilder sql = new StringBuilder();
		try {
			while( true ) {
				String line;
					line = bufferedFileReader.readLine();
					if( line == null )
						break;
					sql.append( line ).append( "\r\n" );
			}
		} catch ( IOException e ) {
			throw new RuntimeException(e);
		}
		finally {
			try	{
				bufferedFileReader.close();
			}catch ( IOException e ){
				throw new RuntimeException(e);
			}
		}
		CREATE_ORIGINAL_PROC = sql.toString();
	} catch ( FileNotFoundException e )	{
		throw new RuntimeException(e);
	}
	
	//Read drop procedure statement into memory.
	StringBuilder dropOriginalProc = new StringBuilder();
	dropOriginalProc.append( "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetPurchasingCenterVehiclesByMakeModelGroupingXML_ORIGINAL_FOR_TEST_ONLY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" );
	dropOriginalProc.append( "drop procedure [dbo].[GetPurchasingCenterVehiclesByMakeModelGroupingXML_ORIGINAL_FOR_TEST_ONLY];");
	DROP_ORIGINAL_PROC = dropOriginalProc.toString();
}

public void testOptimalInventorySearchSummary1() {
	List<MarketVehicle> list = searchResultDAO.getSearchResults(LONGO_QUERY_XML, 1, false, null);
	
	//setup
	jdbcTemplate.execute( DROP_ORIGINAL_PROC );
	jdbcTemplate.execute( CREATE_ORIGINAL_PROC );
	//run
	List<MarketVehicle> list2 = jdbcTemplate.query( SearchResultDAO.SQL_SEARCH_PURCHASING, 
	                                                new Object[] { LONGO_QUERY_XML, new Integer(0) },
	                            					new int[] { java.sql.Types.VARCHAR, java.sql.Types.INTEGER },
	                            					searchResultDAO.new SearchResultRowCallbackHandler() );
	//MarketVehicle implementations does not implement equals() and hashCode(),
	//user a wrapper and implement the methods in fear of breaking production code
	//for the sake of testing.
	List<SimpleMarketVehicleImplTestWrapper> results = cast( list );
	List<SimpleMarketVehicleImplTestWrapper> expected = cast( list2 );
	assertEquals( expected, results );
}

private static List<SimpleMarketVehicleImplTestWrapper> cast( List<MarketVehicle> marketVehicles ) {
	List<SimpleMarketVehicleImplTestWrapper> comparableList = new ArrayList< SimpleMarketVehicleImplTestWrapper >();
	for( MarketVehicle marketVehicle : marketVehicles ) {
		if ( marketVehicle instanceof SimpleMarketVehicleImpl )	{
			SimpleMarketVehicleImpl sMarketVehicle = (SimpleMarketVehicleImpl)marketVehicle;
			comparableList.add( new SimpleMarketVehicleImplTestWrapper( sMarketVehicle ) );
		} else {
			throw new RuntimeException( "Expecting a SimpleMarketVehicleImpl but got " + marketVehicle.getClass());
		}
	}
	return comparableList.isEmpty() ? null : comparableList;
}

public void tearDown() {
	jdbcTemplate.execute( DROP_ORIGINAL_PROC );
}

}
