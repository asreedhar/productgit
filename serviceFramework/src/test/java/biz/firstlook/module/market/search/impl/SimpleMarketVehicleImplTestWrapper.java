package biz.firstlook.module.market.search.impl;


public class SimpleMarketVehicleImplTestWrapper extends SimpleMarketVehicleImpl
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3599116877916700004L;
	public SimpleMarketVehicleImplTestWrapper( SimpleMarketVehicleImpl simpleMarketVehicle )
	{
		super(	simpleMarketVehicle.market,
		      	simpleMarketVehicle.vehicle,
		      	simpleMarketVehicle.unitCost,
		      	simpleMarketVehicle.currentBid,
		      	simpleMarketVehicle.buyPrice,
		      	simpleMarketVehicle.location,
		      	simpleMarketVehicle.dealerName,
		      	simpleMarketVehicle.expires,
		      	simpleMarketVehicle.inventoryReceivedDate,
		      	simpleMarketVehicle.distanceFromDealer,
		      	simpleMarketVehicle.lot,
		      	simpleMarketVehicle.run,
		      	simpleMarketVehicle.candidateMatch,
		      	simpleMarketVehicle.consignor,
				simpleMarketVehicle.amsOption,
				simpleMarketVehicle.amsAnnouncement,
				simpleMarketVehicle.inventoryId,
				simpleMarketVehicle.url,
				simpleMarketVehicle.makeModelGroupingId,
				simpleMarketVehicle.groupingDescriptionId,
				simpleMarketVehicle.marketId,
				simpleMarketVehicle.vehicleEntityId,
				simpleMarketVehicle.stockNumber,
				simpleMarketVehicle.transferPrice,
				simpleMarketVehicle.transferForRetailOnly,
				simpleMarketVehicle.title);
	}
	
	@Override
	public int hashCode()
	{
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ( ( amsAnnouncement == null ) ? 0 : amsAnnouncement.hashCode() );
		result = PRIME * result + ( ( amsOption == null ) ? 0 : amsOption.hashCode() );
		result = PRIME * result + ( ( buyPrice == null ) ? 0 : buyPrice.hashCode() );
		result = PRIME * result + ( ( candidateMatch == null ) ? 0 : candidateMatch.hashCode() );
		result = PRIME * result + ( ( consignor == null ) ? 0 : consignor.hashCode() );
		result = PRIME * result + ( ( currentBid == null ) ? 0 : currentBid.hashCode() );
		result = PRIME * result + ( ( dealerName == null ) ? 0 : dealerName.hashCode() );
		result = PRIME * result + ( ( distanceFromDealer == null ) ? 0 : distanceFromDealer.hashCode() );
		result = PRIME * result + ( ( expires == null ) ? 0 : expires.hashCode() );
		result = PRIME * result + ( ( groupingDescriptionId == null ) ? 0 : groupingDescriptionId.hashCode() );
		result = PRIME * result + ( ( inventoryId == null ) ? 0 : inventoryId.hashCode() );
		result = PRIME * result + ( ( inventoryReceivedDate == null ) ? 0 : inventoryReceivedDate.hashCode() );
		result = PRIME * result + ( ( location == null ) ? 0 : location.hashCode() );
		result = PRIME * result + ( ( lot == null ) ? 0 : lot.hashCode() );
		result = PRIME * result + ( ( makeModelGroupingId == null ) ? 0 : makeModelGroupingId.hashCode() );
		result = PRIME * result + ( ( market == null ) ? 0 : market.hashCode() );
		result = PRIME * result + ( ( marketId == null ) ? 0 : marketId.hashCode() );
		result = PRIME * result + ( ( run == null ) ? 0 : run.hashCode() );
		result = PRIME * result + ( ( unitCost == null ) ? 0 : unitCost.hashCode() );
		result = PRIME * result + ( ( url == null ) ? 0 : url.hashCode() );
		result = PRIME * result + ( ( vehicle == null ) ? 0 : vehicle.hashCode() );
		result = PRIME * result + ( ( stockNumber == null ) ? 0 : stockNumber.hashCode() );
		return result;
	}
	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		final SimpleMarketVehicleImpl other = (SimpleMarketVehicleImpl)obj;
		if ( amsAnnouncement == null )
		{
			if ( other.amsAnnouncement != null )
				return false;
		}
		else if ( !amsAnnouncement.equals( other.amsAnnouncement ) )
			return false;
		if ( amsOption == null )
		{
			if ( other.amsOption != null )
				return false;
		}
		else if ( !amsOption.equals( other.amsOption ) )
			return false;
		if ( buyPrice == null )
		{
			if ( other.buyPrice != null )
				return false;
		}
		else if ( !buyPrice.equals( other.buyPrice ) )
			return false;
		if ( candidateMatch == null )
		{
			if ( other.candidateMatch != null )
				return false;
		}
		else if ( !candidateMatch.equals( other.candidateMatch ) )
			return false;
		if ( consignor == null )
		{
			if ( other.consignor != null )
				return false;
		}
		else if ( !consignor.equals( other.consignor ) )
			return false;
		if ( currentBid == null )
		{
			if ( other.currentBid != null )
				return false;
		}
		else if ( !currentBid.equals( other.currentBid ) )
			return false;
		if ( dealerName == null )
		{
			if ( other.dealerName != null )
				return false;
		}
		else if ( !dealerName.equals( other.dealerName ) )
			return false;
		if ( distanceFromDealer == null )
		{
			if ( other.distanceFromDealer != null )
				return false;
		}
		else if ( !distanceFromDealer.equals( other.distanceFromDealer ) )
			return false;
		if ( expires == null )
		{
			if ( other.expires != null )
				return false;
		}
		else if ( !expires.equals( other.expires ) )
			return false;
		if ( groupingDescriptionId == null )
		{
			if ( other.groupingDescriptionId != null )
				return false;
		}
		else if ( !groupingDescriptionId.equals( other.groupingDescriptionId ) )
			return false;
		if ( inventoryId == null )
		{
			if ( other.inventoryId != null )
				return false;
		}
		else if ( !inventoryId.equals( other.inventoryId ) )
			return false;
		if ( inventoryReceivedDate == null )
		{
			if ( other.inventoryReceivedDate != null )
				return false;
		}
		else if ( !inventoryReceivedDate.equals( other.inventoryReceivedDate ) )
			return false;
		if ( location == null )
		{
			if ( other.location != null )
				return false;
		}
		else if ( !location.equals( other.location ) )
			return false;
		if ( lot == null )
		{
			if ( other.lot != null )
				return false;
		}
		else if ( !lot.equals( other.lot ) )
			return false;
		if ( makeModelGroupingId == null )
		{
			if ( other.makeModelGroupingId != null )
				return false;
		}
		else if ( !makeModelGroupingId.equals( other.makeModelGroupingId ) )
			return false;
		if ( market == null )
		{
			if ( other.market != null )
				return false;
		}
		else if ( !market.equals( other.market ) )
			return false;
		if ( marketId == null )
		{
			if ( other.marketId != null )
				return false;
		}
		else if ( !marketId.equals( other.marketId ) )
			return false;
		if ( run == null )
		{
			if ( other.run != null )
				return false;
		}
		else if ( !run.equals( other.run ) )
			return false;
		if ( unitCost == null )
		{
			if ( other.unitCost != null )
				return false;
		}
		else if ( !unitCost.equals( other.unitCost ) )
			return false;
		if ( url == null )
		{
			if ( other.url != null )
				return false;
		}
		else if ( !url.equals( other.url ) )
			return false;
		if ( vehicle == null )
		{
			if ( other.vehicle != null )
				return false;
		}
		else if ( !vehicle.equals( other.vehicle ) )
			return false;
		if ( stockNumber == null )
		{
			if ( other.stockNumber != null )
				return false;
		}
		else if ( !stockNumber.equals( other.stockNumber ) )
			return false;
		
		return true;
	}

}
