SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
/****** Object:  StoredProcedure [dbo].[GetPurchasingCenterVehiclesByMakeModelGroupingXML_ORIGINAL_FOR_TEST_ONLY]    Script Date: 04/23/2007 15:27:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPurchasingCenterVehiclesByMakeModelGroupingXML_ORIGINAL_FOR_TEST_ONLY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPurchasingCenterVehiclesByMakeModelGroupingXML_ORIGINAL_FOR_TEST_ONLY]
GO

CREATE PROCEDURE [dbo].[GetPurchasingCenterVehiclesByMakeModelGroupingXML_ORIGINAL_FOR_TEST_ONLY]
---Parmeters----------------------------------------------------------------------------------------
-- 
@SearchXML 	XML, 			-- XML search criteria
@RunDate 	DATETIME = null,	-- Optional run date; defaults to current.  Req'd to compute
					-- time left in auction 
@ResultsMode	BIT = 0,		-- 0 = Detail, 1 = Summary
@Debug		BIT = 0			-- IF 1, returns the criteria tables AS parsed from the XML
--
---History------------------------------------------------------------------------------------------
--	
--	SBW	09/07/2006	Initial design/development from original WGHs stored-proc
--	WGH	10/19/2006	Removed embedded function call to GetChannel.. to optimize for speed
--		12/12/2006	Added VehicleMileage filter.  Be careful, MileageReceived is set to 
--				-1 if it is not passed in from the DMS.  If there is no filter set,
--		01/22/2007	Fixed bug in on-line query's where clause
--	BYF	04/24/2007	Optimizing speed.  Removed the UNION and inserted Online Purchasing
--					data and In-Group data into table variable seperately.  For In Group,
--					distance between dealership is calculated, then used in inventory query.
--	SBW	10/26/2007	Converted to VehicleCatalog ModelID
--	BYF 	01/10/2008	Excluded duplicates entries when the CustomerFacingDescription is the same.
--				AccessGroupID is now an almost meaningless return field.
--	ARP	03/26/2008	Returns Vehicle data if there is no Auction End for Auctions that don't expire
--	FFO	6/12/2008	Optimizing speed.
--					Replaced table variables with temp tables
--					Moved MakeModel filtering critera (#Filters) to join clause from where clause
--					Restructured Mileage filters to allow use of indexed columns (if available)
----------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON

CREATE TABLE #SisterStores ( BusinessUnitID INT
				, BusinessUnitShortName VARCHAR(30)
				, City VARCHAR(20)
				, State VARCHAR(2)
				, Zipcode VARCHAR(10)
				, DistanceFromDealer int
)
CREATE TABLE #DebugMessage (MessageText varchar(1000), InsDate datetime, RowCnt int)
CREATE TABLE #AccessGroups (AccessGroupID int)
CREATE TABLE #FiltersTriple (ModelID INT, Type VARCHAR(25), Value VARCHAR(50), Low INT, High INT)
CREATE TABLE #Filters (MakeModelGroupingID INT, Type VARCHAR(25), Value VARCHAR(50), HasYearDetail bit)

CREATE TABLE #Results (
	VehicleID			INT,		-- ATC (Vehicle_ID), InGroup (VehicleID)
	AccessGroupTypeId		INT,
	AccessGroupId			INT,
	CustomerFacingDescription	VARCHAR(50),
	MakeModelGroupingID 		INT,		-- ATC, InGroup
	[Year] 				VARCHAR(4), 	-- ATC, InGroup
	[Make]				VARCHAR(50), 	-- ATC
	Model 				VARCHAR(50), 	-- ATC
	Trim 				VARCHAR(50), 	-- ATC, InGroup
	Color 				VARCHAR(50), 	-- ATC, InGroup
	Mileage 			INT, 		-- ATC, InGroup
	Location 			VARCHAR(50), 	-- ATC, InGroup
	DistanceFromDealer		DECIMAL(8,2),	-- ATC, InGroup
	VIN 				VARCHAR(50), 	-- ATC, InGroup
	HighBid 			INT, 		-- ATC
	Expires 			VARCHAR(20),	-- ATC
	Series 				VARCHAR(50), 	-- ATC
	BodyStyle 			VARCHAR(50), 	-- ATC
	CurrentBid 			INT, 		-- ATC
	BuyPrice 			INT, 		-- ATC
	DetailURL 			VARCHAR(250), 	-- ATC
	ImageURL 			VARCHAR(250),	-- ATC
	AuctionTypeCD			TINYINT,	-- ATC 	
	InventoryID 			INT,		-- InGroup	
	InventoryReceivedDate 		SMALLDATETIME,	-- InGroup	 
	GroupingDescription 		VARCHAR(80),	-- InGroup
	GroupingDescriptionID 		INT,		-- InGroup
	DealerNickName 			VARCHAR(30),	-- InGroup		
	UnitCost 			DECIMAL(9,2),	-- InGroup	
	SegmentID 		INT, 			-- ATC, InGroup
	PrivateAccessGroup		BIT,		-- ATC
	DisplayUnitCostToDealerGroup	TINYINT,	-- InGroup
	VehicleDriveTrain		VARCHAR(50), 	-- ATC, InGroup
	ModelID INT,
	StockNumber	VARCHAR(15) -- InGroup
)

DECLARE @err 			INT,
	@rc			INT,
	@msg			VARCHAR(255),
	@sql			NVARCHAR(1000),
	@SearchFunctionName	VARCHAR(128),
	@HasAccessGroups	BIT

SET @RunDate = ISNULL(@RunDate,GETDATE())

--------------------------------------------------------------------------------
--	BusinessUnit
--	For simplicity, put the global filters here
--------------------------------------------------------------------------------

DECLARE @BusinessUnitID 	INT,
	@MaxDistanceFromDealer	INT,
	@MinVehicleMileage	INT,
	@MaxVehicleMileage	INT
SELECT	@BusinessUnitID		= x.row.value('@ID[1]','INT'), 
	@MaxDistanceFromDealer	= x.row.value('@MaxDistanceFromDealer[1]','INT'),
	@MinVehicleMileage	= x.row.value('@MinVehicleMileage[1]','INT'),
	@MaxVehicleMileage	= x.row.value('@MaxVehicleMileage[1]','INT')
FROM	@SearchXML.nodes('/Search/BusinessUnit') AS x(row)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------
--	AccessGroups
--------------------------------------------------------------------------------

INSERT
INTO	#AccessGroups (AccessGroupId)
SELECT DISTINCT x.row.value('@ID[1]','INT') AccessGroupId
FROM	@SearchXML.nodes('/Search/AccessGroups/AccessGroup') AS x(row)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--------------------------------------------------------------------------------
--	Filters
--------------------------------------------------------------------------------

INSERT
INTO	#FiltersTriple (ModelID, Type)
SELECT DISTINCT x.row.value('@ModelID[1]','INT'), 'MakeModelGrouping'
FROM   @SearchXML.nodes('/Search/Filter/MakeModelGrouping') AS x(row)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

INSERT
INTO	#FiltersTriple (ModelID, Type, Value)
SELECT	ModelID	= x.row.value('../../@ModelID[1]','INT'), 
	Type	= x.row.value('../@Type[1]','VARCHAR(20)'),
	Value	= x.row.value('text()[1]','VARCHAR(50)')
FROM	@SearchXML.nodes('/Search/Filter/MakeModelGrouping/Criteria/Value') AS x(row)


SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

INSERT INTO #Filters
SELECT
	MM.MakeModelGroupingID,
	F.Type,
	F.Value,
	0
FROM	#FiltersTriple F
JOIN	[IMT].dbo.MakeModelGrouping MM ON MM.ModelID = F.ModelID
WHERE	MM.ModelID <> 0

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

UPDATE 	#Filters
SET 	HasYearDetail = CASE WHEN EXISTS(SELECT 1 FROM #Filters WHERE TYPE = 'Year' AND MakeModelGroupingId = f.MakeModelGroupingId) THEN 1 ELSE 0 END
FROM 	#Filters f


IF @Debug = 1 BEGIN
	
	PRINT 'AccessGroups'
	SELECT * FROM #AccessGroups

	PRINT 'Filters'
	SELECT * FROM #Filters

end

--------------------------------------------------------------------------------
--	Search for matching vehicles
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--	Process overall query
--------------------------------------------------------------------------------

INSERT
INTO	#Results (VehicleID, AccessGroupTypeId, AccessGroupId, CustomerFacingDescription, MakeModelGroupingID, ModelID, [Year], Make, Model, Trim, Color, Mileage, Location, DistanceFromDealer, VIN, HighBid, Expires, Series, BodyStyle, CurrentBid, BuyPrice, DetailURL, ImageURL, AuctionTypeCD, InventoryID, InventoryReceivedDate, GroupingDescription, GroupingDescriptionID, DealerNickName, UnitCost, SegmentID, PrivateAccessGroup, DisplayUnitCostToDealerGroup, VehicleDriveTrain)

SELECT	V1.VehicleID, V1.AccessGroupTypeId, V1.AccessGroupID, V1.CustomerFacingDescription, V1.MakeModelGroupingID, ModelID, Year, Make, Model, Trim, Color, Mileage, Location, round(DistanceFromDealer, 0) AS DistanceFromDealer, VIN, HighBid, Expires, Series, BodyStyle, CurrentBid, BuyPrice, DetailURL, ImageURL, NULL AuctionTypeCD, InventoryID, InventoryReceivedDate, GroupingDescription, GroupingDescriptionID, DealerNickName, UnitCost, SegmentID, PrivateAccessGroup, DisplayUnitCostToDealerGroup, VehicleDriveTrain

FROM	(

SELECT	VehicleID			= V.VehicleID, 
		AccessGroupTypeID		= AG.AccessGroupTypeID, 
		AccessGroupID			= AG.AccessGroupID, 
		AccessGroupDescr		= AG.CustomerFacingDescription, 
		CustomerFacingDescription	= AG.CustomerFacingDescription,
		BodyStyle 			= V.BODY_STYLE,
		BuyPrice 			= V.BUY_PRICE,
		Color 				= C.COLOR,
		CurrentBid			= V.CURRENT_BID,
		DetailURL			= V.DETAIL_URL,
		SegmentID			= SegmentID,
		Expires				= dbo.TimeDiff(@RunDate, dateadd(hh, +2,Cast(V.AUCTION_END AS DATETIME))),
		MakeModelGroupingID		= MMG.MakeModelGroupingID,
		ModelID				= MMG.ModelID,
		GroupingDescription		= GD.GroupingDescription,
		GroupingDescriptionID		= GD.GroupingDescriptionID,
		InventoryID			= NULL,
		InventoryReceivedDate 		= NULL,
		HighBid				= ISNULL(V.CURRENT_BID,V.BUY_PRICE),
		ImageURL			= V.IMAGE_URL,
		Make				= MMG.Make,
		Model				= MMG.Line,
		Mileage				= Mileage,
		Location			= V.LOCATION,
		ZipCode				= V.ZIP_CODE,
		DistanceFromDealer		= dbo.ZipCodeDistance(V.ZIP_CODE, BU.ZipCode),
		Series				= Series,
		Trim				= VP.VehicleTrim,
		VIN				= VIN,
		[Year]				= [Year],
		PrivateAccessGroup		= AG.PrivateAccessGroup,
		DisplayUnitCostToDealerGroup	= 1,
		DealerNickName			= NULL,
		UnitCost			= NULL, 
		VehicleDriveTrain		= V.DriveTrain,
		StockNumber = NULL

	FROM	[ATC]..Vehicles V WITH (NOLOCK)
		JOIN [ATC]..VehicleProperties VP  WITH (NOLOCK) ON V.VehicleID = VP.VehicleID
		JOIN [ATC]..StandardColor C ON VP.ColorID = C.ColorID
		JOIN MakeModelGrouping MMG ON VP.MakeModelGroupingID = MMG.MakeModelGroupingID
		JOIN tbl_GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID
		JOIN BusinessUnit BU ON BU.BusinessUnitID = @BusinessUnitID
		JOIN (
			SELECT	VAG.VehicleID
					, AG.AccessGroupTypeID
					, MIN(AG.AccessGroupID) AS AccessGroupID --ignore this
					, AG.CustomerFacingDescription
					, AG.Public_Group
					, CASE WHEN SUM(DAG.AccessGroupID) IS NOT NULL THEN 1 ELSE 0 END AS PrivateAccessGroup
			FROM [ATC]..VehicleAccessGroups VAG WITH (NOLOCK) 
			JOIN [ATC]..AccessGroup AG 
				ON VAG.AccessGroupID = AG.AccessGroupId
			JOIN #AccessGroups SAG 
				ON VAG.AccessGroupID = SAG.AccessGroupID 
			LEFT JOIN tbl_DealerATCAccessGroups DAG		
				on DAG.BusinessUnitID = @BusinessUnitID 
				AND VAG.AccessGroupID = DAG.AccessGroupID
			GROUP BY VAG.VehicleID
					, AG.AccessGroupTypeID
					, AG.CustomerFacingDescription
					, AG.Public_Group
		) AG ON V.VehicleID = AG.VehicleID
		INNER JOIN #Filters f on f.MakeModelGroupingId = mmg.MakeModelGroupingId and (f.HasYearDetail = 0 or f.Value = v.Year)
	WHERE	(V.AUCTION_END IS NULL OR DATEADD(HH, +2,CAST(V.AUCTION_END AS DATETIME)) > @RunDate)
		AND (AG.Public_Group = 1 OR AG.PrivateAccessGroup = 1)
) V1
WHERE	--Validate MaxDistance
	(@MaxDistanceFromDealer is null or V1.DistanceFromDealer IS NULL or V1.DistanceFromDealer <= @MaxDistanceFromDealer)
	--Validate Vehicle Mileage - A mileage of -1, indicating unknown, is converted to 0 for this calculation
	AND (@MinVehicleMileage is null or (V1.Mileage = -1 and @MinVehicleMileage = 0) or V1.Mileage BETWEEN @MinVehicleMileage and @MaxVehicleMileage)
	
--------------------------------------------------------------------------------
--	Process In-Group 
--------------------------------------------------------------------------------

INSERT INTO #SisterStores
SELECT BU2.BusinessUnitID
				, BU2.BusinessUnitShortName
				, BU2.City
				, BU2.State
				, BU2.Zipcode
				, dbo.ZipCodeDistance(BU.ZipCode, BU2.ZipCode) as DistanceFromDealer
			FROM BusinessUnit BU
			JOIN BusinessUnitRelationship BR1 ON BU.BusinessUnitID = BR1.BusinessUnitID
			JOIN BusinessUnitRelationship BR2 ON BR1.ParentID = BR2.ParentID
			JOIN BusinessUnit BU2 ON BR2.BusinessUnitID = BU2.BusinessUnitID
			WHERE BU.BusinessUnitID = @BusinessUnitID
			AND BU2.BusinessUnitID <> @BusinessUnitID
			AND BU2.Active = 1
			AND dbo.ZipCodeDistance(BU.ZipCode, BU2.ZipCode) <= @MaxDistanceFromDealer 

INSERT
INTO	#Results (VehicleID, AccessGroupTypeId, AccessGroupId, CustomerFacingDescription, MakeModelGroupingID, ModelID, [Year], Make, Model, Trim, Color, Mileage, Location, DistanceFromDealer, VIN, HighBid, Expires, Series, BodyStyle, CurrentBid, BuyPrice, DetailURL, ImageURL, AuctionTypeCD, InventoryID, InventoryReceivedDate, GroupingDescription, GroupingDescriptionID, DealerNickName, UnitCost, SegmentID, PrivateAccessGroup, DisplayUnitCostToDealerGroup, VehicleDriveTrain)

SELECT	V2.VehicleID, V2.AccessGroupTypeId, V2.AccessGroupID, V2.CustomerFacingDescription, V2.MakeModelGroupingID, ModelID, Year, Make, Model, Trim, Color, Mileage, Location, round(DistanceFromDealer, 0) AS DistanceFromDealer, VIN, HighBid, Expires, Series, BodyStyle, CurrentBid, BuyPrice, DetailURL, ImageURL, NULL AuctionTypeCD, InventoryID, InventoryReceivedDate, GroupingDescription, GroupingDescriptionID, DealerNickName, UnitCost, SegmentID, PrivateAccessGroup, DisplayUnitCostToDealerGroup, VehicleDriveTrain

FROM	(
	SELECT	VehicleID			= V.VehicleID, 
		AccessGroupTypeID		= 6, 
		AccessGroupID			= 141, 
		AccessGroupDescr		= 'In Group', 
		CustomerFacingDescription	= 'In Group',
		BodyStyle			= NULL,
		BuyPrice			= NULL,
		Color				= V.BaseColor,
		CurrentBid			= NULL,
		DetailURL			= NULL,
		SegmentID			= V.SegmentID,
		Expires				= NULL,
		MakeModelGroupingID		= MMG.MakeModelGroupingID,
		ModelID				= MMG.ModelID,
		GroupingDescription		= GD.GroupingDescription,
		GroupingDescriptionID		= GD.GroupingDescriptionID,
		InventoryID			= I.InventoryID,
		InventoryReceivedDate		= I.InventoryReceivedDate, 
		HighBid				= NULL,
		ImageURL			= NULL,
		Make				= MMG.Make,
		Model				= MMG.Line,
		Mileage				= I.MileageReceived,
		Location			= ISNULL(I.VehicleLocation, BU.City + ', ' + BU.State),
		ZipCode				= BU.ZipCode, 
		DistanceFromDealer		= BU.DistanceFromDealer,
		Series				= NULL,
		Trim				= V.VehicleTrim,
		VIN				= V.VIN,
		[Year]				= V.VehicleYear,
		PrivateAccessGroup		= 0,
		DisplayUnitCostToDealerGroup	= DP.DisplayUnitCostToDealerGroup,
		DealerNickName			= BU.BusinessUnitShortName,
		UnitCost			= CASE WHEN DP.FlashLocatorHideUnitCostDays > DATEDIFF(dayofyear,I.InventoryReceivedDate,@RunDate) THEN 0 ELSE UnitCost END,
		VehicleDriveTrain	= V.VehicleDriveTrain,
		StockNumber = I.StockNumber

	FROM	[FLDW]..InventoryActive  I WITH (NOLOCK)
		JOIN [FLDW]..Vehicle V WITH (NOLOCK) ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
		JOIN #SisterStores BU ON I.BusinessUnitID = BU.BusinessUnitID
		JOIN DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID AND DP.GoLiveDate IS NOT NULL
		JOIN MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
		JOIN tbl_GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID
		JOIN #AccessGroups AG ON AG.AccessGroupID = 141	-- In Group
		INNER JOIN #Filters f on f.MakeModelGroupingId = mmg.MakeModelGroupingId and (f.HasYearDetail = 0 or f.Value = v.VehicleYear)
WHERE I.InventoryType = 2 -- Used
	--Validate Vehicle Mileage - A mileage of -1, indicating unknown, is converted to 0 for this calculation
	AND (@MinVehicleMileage is null or (I.MileageReceived = -1 and @MinVehicleMileage = 0) or I.MileageReceived BETWEEN @MinVehicleMileage and @MaxVehicleMileage)
) V2
	

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @ResultsMode = 0 BEGIN
	
	SELECT	AccessGroupTypeId, AccessGroupId, CustomerFacingDescription, MakeModelGroupingID, ModelID, [Year], Make, Model, Trim, Color, Mileage, Location, round(DistanceFromDealer, 0) AS DistanceFromDealer,
		VIN, Expires, Series, BodyStyle, UnitCost, CurrentBid, BuyPrice, DetailURL, ImageURL, InventoryID, InventoryReceivedDate,
		GroupingDescription,  GroupingDescriptionID, DealerNickName, UnitCost, SegmentID, DisplayUnitCostToDealerGroup, @BusinessUnitID AS BusinessUnitID, R.VehicleID, VehicleDriveTrain
	FROM	#Results R
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF @err <> 0 GOTO Failed

END
ELSE BEGIN

	DELETE #Filters 
	WHERE Type = 'MakeModelGrouping' AND HasYearDetail = 1

	SELECT 	R.AccessGroupTypeId, R.CustomerFacingDescription,
		COUNT(DISTINCT F.MakeModelGroupingId) PrecisionMatches,
		COUNT(*) TotalMatches
	FROM #Results r INNER JOIN #Filters f 
		on f.MakeModelGroupingId = r.MakeModelGroupingId and (f.Type = 'MakeModelGrouping' or f.Value = r.Year)
	GROUP BY R.AccessGroupTypeId, R.CustomerFacingDescription

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	IF (@err <> 0) GOTO Failed

END

--Cleanup

DROP TABLE #SisterStores 
DROP TABLE #DebugMessage
DROP TABLE #AccessGroups 
DROP TABLE #FiltersTriple 
DROP TABLE #Filters 
DROP TABLE #Results 


RETURN 0

Failed:

RETURN @err
