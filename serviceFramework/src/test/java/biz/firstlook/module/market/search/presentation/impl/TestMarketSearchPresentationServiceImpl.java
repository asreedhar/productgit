package biz.firstlook.module.market.search.presentation.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import junit.framework.TestCase;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.impl.adapter.SearchCandidateAdapter;
import biz.firstlook.module.market.search.impl.entity.CIACategory;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOption;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.Segment;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;
import biz.firstlook.module.purchasing.vehicle.service.impl.MockVehicleServiceImpl;

public class TestMarketSearchPresentationServiceImpl extends TestCase {

	private MarketSearchPresentationServiceImpl marketSearchPresentationServiceImpl;
	
	private MockVehicleServiceImpl vehicleServiceImpl;
	
	private InternalVehicleServiceFactory vehicleServiceFactory;
	
	class InternalVehicleServiceFactory extends VehicleServiceFactory {

		private VehicleService service;
		public InternalVehicleServiceFactory(MockVehicleServiceImpl service) {
			super();
			VehicleServiceFactory.setFactory(this);
			this.service = service;
		}
		
		@Override
		public VehicleService getService() {
			return service;
		}
	}
	
	public TestMarketSearchPresentationServiceImpl() {
		super();
		
	}

	public TestMarketSearchPresentationServiceImpl(String name) {
		super(name);
	}

	@Override
	protected void setUp() throws Exception {
		marketSearchPresentationServiceImpl = new MarketSearchPresentationServiceImpl();
		vehicleServiceImpl = new MockVehicleServiceImpl();
		vehicleServiceFactory = new InternalVehicleServiceFactory(vehicleServiceImpl);
		vehicleServiceFactory.getService();
	}

	@Override
	protected void tearDown() throws Exception {
		marketSearchPresentationServiceImpl = null;
		vehicleServiceImpl = null;
		vehicleServiceFactory = null;
	}

	public void testBuildPresentationDataQueryXml() {
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Models><Model ID=\"0\"/><Model ID=\"1\"/><Model ID=\"2\"/><Model ID=\"3\"/><Model ID=\"4\"/><Model ID=\"6\"/><Model ID=\"7\"/><Model ID=\"8\"/><Model ID=\"9\"/></Models>";
		
		Collection<SearchCandidate> scs = new ArrayList<SearchCandidate>();
		for(int id  = 0; id < 10; id++) {
			final int modelId = id;
			final Model mdl = new Model(){

				public Integer getId() {
					return modelId;
				}

				public Line getLine() {
					return null;
				}

				public Make getMake() {
					return null;
				}

				public ModelGroup getModelGroup() {
					return null;
				}

				public String getName() {
					return null;
				}

				public Segment getSegment() {
					return null;
				}

				public int compareTo(Model o) {
					return 0;
				}};
			
			vehicleServiceImpl.putModel(mdl);
			
			biz.firstlook.module.market.search.impl.entity.SearchCandidate sc = 
				new biz.firstlook.module.market.search.impl.entity.SearchCandidate(
						id,
						mdl.getId(),
						(mdl.getId() == 5), //suppress the when model id = 5;
						new ArrayList<SearchCandidateOption>(),
						new HashSet<CIACategory>());
			
			scs.add(new SearchCandidateAdapter(sc));
		}
		
		String xml = marketSearchPresentationServiceImpl.buildPresentationDataQueryXml(scs);
		assertEquals(expected, xml);
	}
}
