package biz.firstlook.module.market.search;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.utility.ContextLoader;
import biz.firstlook.utility.HibernateTestCase;

public class TestMarketSearch extends HibernateTestCase {

	private static final int MEMBER_ID = 101133;
	
	private CoreService coreService;
	private VehicleService vehicleService;
	private MarketSearchService marketSearchService;
	
	private Make make = null;
	private Model model = null;
	private ModelYear modelYear = null;
	private Member member;
	
	public TestMarketSearch() {
		super();
	}

	public TestMarketSearch(String name) {
		super(name);
	}
	
	protected String getSessionFactoryName() {
		return "sessionFactory";
	}
	
	protected void setUp() throws Exception {
		if (coreService == null) {
			coreService = (CoreService) ContextLoader.getBean("coreService");
		}
		if (vehicleService == null) {
			vehicleService = (VehicleService) ContextLoader.getBean("purchasingVehicleService");
		}
		if (marketSearchService == null) {
			marketSearchService = (MarketSearchService) ContextLoader.getBean("marketSearchService");
		}
		if (make == null || model == null || modelYear == null) {
			Random r = new Random();
			List<Make> makes = vehicleService.getMakes();
			do {
				make = makes.get(r.nextInt(makes.size()));
				List<Model> models = vehicleService.getModels(make);
				if (!models.isEmpty()) {
					model = models.get(r.nextInt(models.size()));
					List<ModelYear> modelYears = vehicleService.getModelYears(model);
					if (!modelYears.isEmpty()) {
						modelYear = modelYears.get(r.nextInt(modelYears.size()));
					}
				}
			}
			while (make == null || model == null || modelYear == null);
		}
		super.setUp();
	}
	
	protected void tearDown() throws Exception {
		member = null;
		super.tearDown();
	}

	public void testGetCandidateList() {
		// get instance of member
		member = coreService.findMemberByID(new Integer(MEMBER_ID));
		member.setCurrentBusinessUnit(member.getBusinessUnits().iterator().next());
		// see if we can get the list
		Collection<SearchCandidate> searchCandidates = marketSearchService.getCandidateList(member, PersistenceScope.PERMANENT);
		assertNotNull(searchCandidates);
	}

	public void testAddItemToCandidateList() {
		// get instance of member
		member = coreService.findMemberByID(new Integer(MEMBER_ID));
		member.setCurrentBusinessUnit(member.getBusinessUnits().iterator().next());
		// make new search candidate
		MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		SearchCandidate searchCandidate = builder.newSearchCandidate(model);
		SearchCandidateCriteria searchCandidateCriteria = builder.newSearchCandidateCriteria(modelYear, Calendar.getInstance().getTime());
		searchCandidate.getCriteria().add(searchCandidateCriteria);
		// add it to the users search candidate list
		Collection<SearchCandidate> searchCandidates = marketSearchService.getCandidateList(member, PersistenceScope.PERMANENT);
		searchCandidates.add(searchCandidate);
	}

	public void testRemoveItemFromCandidateList() {
		// get instance of member
		member = coreService.findMemberByID(new Integer(MEMBER_ID));
		member.setCurrentBusinessUnit(member.getBusinessUnits().iterator().next());
		// get the users search candidate list
		Collection<SearchCandidate> searchCandidates = marketSearchService.getCandidateList(member, PersistenceScope.PERMANENT);
		// pick an item to remove
		int sizeBefore = searchCandidates.size();
		SearchCandidate removed = null;
		if (!searchCandidates.isEmpty()) {
			removed = searchCandidates.iterator().next();
			searchCandidates.remove(removed);
		}
		// check it removed something 
		int sizeAfter = searchCandidates.size();
		assertTrue(sizeBefore == 0 ? sizeAfter == 0 : sizeBefore > sizeAfter);
	}

	public void testGetContext() {
		testGetContext(MEMBER_ID);
	}

	public void testGetAdminContext() {
		testGetContext(100000);
	}

	private void testGetContext(Integer memberID) {
		// get instance of member
		member = coreService.findMemberByID(memberID);
		assertNotNull(member);
		for (BusinessUnit b : member.getBusinessUnits()) {
			member.setCurrentBusinessUnit(b);
			break;
		}
		if (member.getCurrentBusinessUnit() == null) {
			member.setCurrentBusinessUnit(coreService.findBusinessUnitByID(100147));
		}
		assertNotNull(member.getCurrentBusinessUnit());
		// see if we can get the list
		SearchContext context = marketSearchService.getContext(member, PersistenceScope.PERMANENT);
		assertNotNull(context);
		assertNotNull(context.getMarkets());
		assertEquals(false, context.getMarkets().isEmpty());
	}

	public void testSearch() {
		// get instance of member
		member = coreService.findMemberByID(new Integer(MEMBER_ID));
		member.setCurrentBusinessUnit(member.getBusinessUnits().iterator().next());
		// get the search candidate list
		Collection<SearchCandidate> searchCandidates = marketSearchService.getCandidateList(member, PersistenceScope.PERMANENT);
		assertNotNull(searchCandidates);
		// get the context
		SearchContext context = marketSearchService.getContext(member, PersistenceScope.PERMANENT);
		assertNotNull(context);
		// perform the search
		List<MarketVehicle> results = marketSearchService.search(context, searchCandidates,false);
		assertNotNull(results);
	}

	public void testSummary() {
		// get instance of member
		member = coreService.findMemberByID(new Integer(MEMBER_ID));
		member.setCurrentBusinessUnit(member.getBusinessUnits().iterator().next());
		// get the search candidate list
		Collection<SearchCandidate> searchCandidates = marketSearchService.getCandidateList(member, PersistenceScope.PERMANENT);
		assertNotNull(searchCandidates);
		// get the context
		SearchContext context = marketSearchService.getContext(member, PersistenceScope.PERMANENT);
		assertNotNull(context);
		// perform the search
		List<MarketSummary> summary = marketSearchService.summary(context, searchCandidates,false);
		assertNotNull(summary);
	}

}
