package biz.firstlook.module.core.service;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.utility.ContextLoader;
import biz.firstlook.utility.HibernateTestCase;

public class TestCoreService extends HibernateTestCase {

	private CoreService coreService;
	private Integer businessUnitID = new Integer(100215);
	private Integer memberID = new Integer(100865);
	
	public TestCoreService() {
		super();
	}

	public TestCoreService(String name) {
		super(name);
	}
	
	protected String getSessionFactoryName() {
		return "sessionFactory";
	}
	
	protected void setUp() throws Exception {
		if (coreService == null) {
			coreService = (CoreService) ContextLoader.getBean("coreService");
		}
		super.setUp();
	}
	
	public void testFindBusinessUnitByID() {
		BusinessUnit businessUnit = coreService.findBusinessUnitByID(businessUnitID);
		assertNotNull(businessUnit);
		assertEquals(businessUnitID, businessUnit.getBusinessUnitID());
	}
	
	public void testFindMemberByID() {
		Member member = coreService.findMemberByID(memberID);
		assertNotNull(member);
		assertEquals(memberID, member.getMemberID());
	}

}
