package biz.firstlook.module.audit.dao;

import biz.firstlook.main.enumerator.LogClickEventType;
import biz.firstlook.module.audit.entity.LogClickEvent;
import biz.firstlook.utility.ContextLoader;
import biz.firstlook.utility.HibernateTestCase;

public class LogClickEventDAOTest extends HibernateTestCase
{
private LogClickEventDAO logClickEventDAO;

public LogClickEventDAOTest()
{
	super();
}

public LogClickEventDAOTest( String arg0 )
{
	super( arg0 );
}

@Override
protected String getSessionFactoryName()
{
	return "sessionFactory";
}

protected void setUp() throws Exception
{
	if ( logClickEventDAO == null )
	{
		logClickEventDAO = (LogClickEventDAO)ContextLoader.getBean( "logClickEventDAO" );
	}

	super.setUp();
}

protected void tearDown() throws Exception
{
	super.tearDown();
}

public void testCreateEvent()
{
    LogClickEvent viewVehicleEvent = new LogClickEvent( LogClickEventType.SEARCH_AND_ACQUISITION, 100147, 100000, 1, "1FTPX025X5KE51998", "www.test.com" );  
    LogClickEvent insertedEvent = logClickEventDAO.makePersistent( viewVehicleEvent );
    assertNotNull( insertedEvent.getId() );
}

}
