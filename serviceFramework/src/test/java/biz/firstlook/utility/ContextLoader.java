package biz.firstlook.utility;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Static Class loader. So every test isnt having to read AppContext
 */
public class ContextLoader
{

private static ApplicationContext applicationContext;

static
{
	List< String > applicationContexts = new ArrayList< String >();
	applicationContexts.add( "test-applicationContext-dataAccess.xml" );
	applicationContexts.add( "applicationContext-dataAccess-main.xml" );
	applicationContexts.add( "applicationContext-dataAccess-planning.xml" );
	applicationContexts.add( "applicationContext-dataAccess-purchasing.xml" );
	applicationContexts.add( "applicationContext-dataAccess-reporting.xml" );
	applicationContexts.add( "applicationContext-service-main.xml" );
	applicationContexts.add( "applicationContext-service-planning.xml" );
	applicationContexts.add( "applicationContext-service-purchasing.xml" );
	applicationContexts.add( "applicationContext-service-reporting.xml" );
	// The context definition contains a datasource declaration using jndi.
	// applicationContexts.add( "applicationContext-auction.xml" );

	applicationContext = new ClassPathXmlApplicationContext( applicationContexts.toArray( new String[applicationContexts.size()] ) );
}

public static Object getBean( String beanName )
{
	return applicationContext.getBean( beanName );
}

}
