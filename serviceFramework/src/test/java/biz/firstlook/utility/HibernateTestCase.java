package biz.firstlook.utility;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public abstract class HibernateTestCase extends TestCase {

	public HibernateTestCase() {
		super();
	}

	public HibernateTestCase(String name) {
		super(name);
	}

	protected SessionFactory sessionFactory;
	
	protected abstract String getSessionFactoryName();

	protected void setUp() throws Exception {
		if (sessionFactory == null) {
			sessionFactory = (SessionFactory) ContextLoader.getBean(getSessionFactoryName());
		}
	}

	protected void runTest() throws Throwable {
		Session session = SessionFactoryUtils.getSession(sessionFactory, true);
		TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
		try {
			super.runTest();
			session.flush();
		}
		catch (RuntimeException t) {
			throw t;
		}
		finally {
			TransactionSynchronizationManager.unbindResource(sessionFactory);
			SessionFactoryUtils.releaseSession(session, sessionFactory);
		}
	}
	
}
