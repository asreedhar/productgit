package biz.firstlook.entity;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;
import biz.firstlook.commons.util.DateUtilFL;

public class InventoryTest extends TestCase
{

public InventoryTest( String name )
{
	super( name );
}

protected void setUp() throws Exception
{
	super.setUp();
}

protected void tearDown() throws Exception
{
	super.tearDown();
}

public void testGetAge()
{
    Inventory inventory = new Inventory();
    Date fiveDaysAgo = DateUtilFL.addDaysToDate( new Date(), -5 );
    inventory.setInventoryReceivedDate( fiveDaysAgo );
    assertEquals( new Integer(5), inventory.getAge() );
}

public void testHasCurrentPlanAsOfEarlierDate()
{
	Calendar reminderDateCal = Calendar.getInstance();
	reminderDateCal.set( Calendar.DAY_OF_MONTH, 15 );
	reminderDateCal.set( Calendar.MONTH, 6 );
	reminderDateCal.set( Calendar.YEAR, 2006 );
	
	Calendar testDateCal = Calendar.getInstance();
	testDateCal.set( Calendar.DAY_OF_MONTH, 14 );
	testDateCal.set( Calendar.MONTH, 6 );
	testDateCal.set( Calendar.YEAR, 2006 );
	
	Inventory inventory = new Inventory();
	inventory.setPlanReminderDate( reminderDateCal.getTime() );
	
	assertTrue( inventory.hasCurrentPlanAsOf( testDateCal.getTime() ) );	
}

public void testHasCurrentPlanAsOfSameDate()
{
	Calendar reminderDateCal = Calendar.getInstance();
	reminderDateCal.set( Calendar.DAY_OF_MONTH, 15 );
	reminderDateCal.set( Calendar.MONTH, 6 );
	reminderDateCal.set( Calendar.YEAR, 2006 );
	
	Calendar testDateCal = Calendar.getInstance();
	testDateCal.set( Calendar.DAY_OF_MONTH, 15 );
	testDateCal.set( Calendar.MONTH, 6 );
	testDateCal.set( Calendar.YEAR, 2006 );
	
	Inventory inventory = new Inventory();
	inventory.setPlanReminderDate( reminderDateCal.getTime() );
	
	assertFalse( inventory.hasCurrentPlanAsOf( testDateCal.getTime() ) );	
}

public void testHasCurrentPlanAsOfLaterDate()
{
	Calendar reminderDateCal = Calendar.getInstance();
	reminderDateCal.set( Calendar.DAY_OF_MONTH, 15 );
	reminderDateCal.set( Calendar.MONTH, 6 );
	reminderDateCal.set( Calendar.YEAR, 2006 );
	
    Calendar testDateCal = Calendar.getInstance();
    testDateCal.set( Calendar.DAY_OF_MONTH, 16 );
    testDateCal.set( Calendar.MONTH, 6 );
    testDateCal.set( Calendar.YEAR, 2006 );
	
	Inventory inventory = new Inventory();
    inventory.setPlanReminderDate( reminderDateCal.getTime() );
    
    assertFalse( inventory.hasCurrentPlanAsOf( testDateCal.getTime() ) );
}

public void testHasCurrentPlanAsOfNullDate()
{
    Calendar testDateCal = Calendar.getInstance();
    testDateCal.set( Calendar.DAY_OF_MONTH, 16 );
    testDateCal.set( Calendar.MONTH, 6 );
    testDateCal.set( Calendar.YEAR, 2006 );
	
	Inventory inventory = new Inventory();
    assertFalse( inventory.hasCurrentPlanAsOf( testDateCal.getTime() ) );
}

}
