package biz.firstlook.thirdparty.marketplace;


import junit.framework.Test;
import junit.framework.TestSuite;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.TestGMSmartAuctionAdapter;

public class AllTests extends junit.framework.TestCase
{
// This is a test.
public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestGMSmartAuctionAdapter.class));
    return suite;
}
}
