package biz.firstlook.thirdparty.marketplace.GMSmartAuction;

import junit.framework.TestCase;

import org.apache.axis.message.MessageElement;

import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.MakeModelGrouping;
import biz.firstlook.entity.Vehicle;
import biz.firstlook.module.core.impl.entity.MemberCredential;
import biz.firstlook.thirdparty.marketplace.ThirdPartyMarketPlaceException;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionResponse;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionResponseQueryString;

public class TestGMSmartAuctionAdapter extends TestCase
{

private static GMSmartAuctionAdapter adapter;
private GMSmartAuctionSubmissionInformation info;

public void setUp() throws ThirdPartyMarketPlaceException
{
	Vehicle vehicle = new Vehicle();
	Inventory inventory = new Inventory();
	
	// setting only the required fields on vehicle
	vehicle.setVin( "JH4KB16515C000122" );
	vehicle.setYear( new Integer( 2005 ) );
	vehicle.setOriginalMake( "Acura" );
	vehicle.setOriginalModel( "RL" );
	vehicle.setBaseColor( "Blue" );
	
	MakeModelGrouping mmg = new MakeModelGrouping();
	mmg.setMake( "Acura" );
	mmg.setModel( "RL" );
	
	vehicle.setMakeModelGrouping( mmg );

	inventory.setMileageReceived( new Integer( 41995 ));
	inventory.setVehicle( vehicle );
	
	info = new GMSmartAuctionSubmissionInformation();

	info.addInventory( inventory );
	MemberCredential credential = new MemberCredential();
	credential.setUsername( "firstlook" );
	credential.setPassword( "webservice7!" );
	
	info.setCredentials( credential );
	info.setStockNo( "P3954" );
	info.setVcDesc( "4DR Sedan" );	

	if (adapter == null)
	{
		adapter = new GMSmartAuctionAdapter("http://test.inspections.ally.com/GMACSAPosterService/DiPostService.asmx");
	}
}

public void tearDown()
{
}

// the point of this test is just to make sure no exceptions are thrown by the webservice
public void testConnectivityToWebserviceIENoExceptions() throws Exception, ThirdPartyMarketPlaceException
{
	GMSmartAuctionResponse response = adapter.submit( info.generateInspectionReports(), info.getVcDesc(), info.getStockNo(), info.getCredentials() );
	assertNotNull(response);
}

public void testBadCredentails() throws ThirdPartyMarketPlaceException
{
	MemberCredential credential = new MemberCredential();
	credential.setUsername( "BadUsername" );
	credential.setPassword( "badPassword" );
	GMSmartAuctionResponse response = adapter.submit( info.generateInspectionReports(), info.getVcDesc(), info.getStockNo(), credential );
	assertNotNull(response);
	assertFalse( response.isValid() );
	assertEquals (info.getFirstVehicle().getVin(), response.getVIN() );
	assertTrue (response.getMessage().equals("Login failed") );
}

public void testGoodSubmission() throws ThirdPartyMarketPlaceException
{
	GMSmartAuctionResponse response = adapter.submit( info.generateInspectionReports(), info.getVcDesc(), info.getStockNo(), info.getCredentials() );
	assertNotNull(response);
	assertTrue( response.isValid() );
	assertEquals (info.getFirstVehicle().getVin(), response.getVIN() );
	assertTrue( response.getMessage().equals("OK") );
	GMSmartAuctionResponseQueryString queryString = response.getQueryString();
	MessageElement messageElement = queryString.get_any()[0];
	assertTrue( "reponse was not a URL", messageElement.toString().startsWith("https://"));
}

}
