<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<c:set var="sOn" value="${menuGroup}"/>
<c:if test="${isPrintable}">
	<c:set var="sPrintPage" value="${pageId}"/>
</c:if>

<%-- upgrades --%>
<c:set var="reducedToolsMenu" value="${sessionScope.nextGenSession.reducedToolsMenu}"/>	
<c:set var="includeAgingPlan" value="${sessionScope.nextGenSession.includeAgingPlan}"/>	
<c:set var="includeCIA" value="${sessionScope.nextGenSession.includeCIA}"/>	
<c:set var="includePerformanceDashboard" value="${sessionScope.nextGenSession.includePerformanceDashboard}"/>	
<c:set var="includeRedistribution" value="${sessionScope.nextGenSession.includeRedistribution}"/>
<c:set var="includeAppraisal" value="${sessionScope.nextGenSession.includeAppraisal}"/>
<c:set var="includeEquity" value="${sessionScope.nextGenSession.includeEquityAnalyzer}"/>
<c:set var="includeMarketStockingGuide" value="${sessionScope.nextGenSession.includeMarketStockingGuide}"/>
<c:set var="includeInTransitInventory" value="${sessionScope.nextGenSession.showInTransitInventoryForm}"/>

<input type="hidden" id="dTodayHide" name="dToday" value="<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>" readonly="readonly"/>

<div id="menu">
		<c:if test="${isPrintable}">
			<div class="printOnly">
				<img src="<c:url value="/common/_images/logos/FL-print.gif"/>" class="logoFL"/>
			</div>
		</c:if>
		<c:if test="${isLogoDisplay}">
			<div id="HeaderLogo" style="display:inline;width:215px;height:32px;position:absolute">
				<img src="<c:url value="/common/_images/logos/FL-89x21-transOnE5.gif"/>" height="100%" width="200px" class="logoFL"/>
			</div>
		</c:if>
	<ul id="dropnav">
		<li class="home">
			<html:link forward="home">HOME</html:link>
		</li>
		<li><tt>|</tt></li>
		<li${sOn=='tools'?' class="on"':''}><a href="#" onclick="return false;">TOOLS <font class = "arrow">&#x25BC;</font><!--  <img src="<c:url value="/common/_images/icons/arrowDown-menu${sOn=='tools'?'On':'Off'}.gif"/>" width="8" height="8"/>--></a>
			<ul class="tools">
		<c:if test="${!reducedToolsMenu}">
			<c:if test="${includeCIA}">
				<li>
					<html:link forward="cia">Custom Inventory Analysis</html:link>
				</li>
				<li><a href="<c:url value="/SearchHomePage.go"/>">First Look Search Engine</a></li>
			</c:if>
			<c:if test="${includeCIA}">
				<li>
					<html:link action="FlashLocateSummary.go" module="NextGen">Flash Locator</html:link>
				</li>
			</c:if>
			<c:if test="${includeCIA}">
				<li>
					<html:link forward="forecaster">Forecaster</html:link>
				</li>
			</c:if>
			<c:if test="${includeAgingPlan}">
				<li>
					<html:link forward="inventoryPlan"><fmt:message key="plan.pageTitle"/></html:link>
				</li>
			</c:if>
			<c:if test="${includeEquity}">
				<li>
					<html:link forward="equityAnalyzer"><fmt:message key="equityAnalyzer.pageTitle"/></html:link>
				</li>
			</c:if>
			<c:if test="${includeRedistribution}">
				<li><a href="javascript:pop('<c:url value="oldIMT/ucbp/TileManagementCenter.go"/>','mgmtCenter')">Management Summary</a></li>
			</c:if>
			<c:if test="${includeMarketStockingGuide}">
				<li>
					<a href="<c:url value="oldIMT/MarketStockingGuideRedirectionAction.go"/>">Market Velocity Stocking Guide</a>
				</li>
			</c:if>
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="oldIMT/PerformanceAnalyzerDisplayAction.go"/>">Performance Analyzer</a></li>
			</c:if>	
			<c:if test="${includePerformanceDashboard}">
				<li><a href="<c:url value="oldIMT/EdgeScoreCardDisplayAction.go"/>">Performance Mgmt Dashboard</a></li>
			</c:if>
		</c:if>
			<c:if test="${includeAppraisal}">
				<li><a href="<c:url value="oldIMT/QuickAppraise.go"/>">Trade Analyzer</a></li>
				<li><a href="<c:url value="oldIMT/AppraisalManagerAction.go"/>">Appraisal Manager</a></li>
			</c:if>
			<c:if test="${includeCIA}">	
				<li><a href="<c:url value="oldIMT/VehicleAnalyzerDisplayAction.go"/>">Vehicle Analyzer</a></li>
			</c:if>
			<c:if test="${includeInTransitInventory}">	
				<li><a href="<c:url value="oldIMT/InTransitInventoryRedirectionAction.go"/>">Create New Inventory</a></li>
			</c:if>
			</ul>
		</li>
<c:if test="${!reducedToolsMenu}">
		<li><tt>|</tt></li>
		<li${sOn=='reports'?' class="on"':''}><a href="#" onclick="return false;">REPORTS <font class = "arrow">&#x25BC;</font><!-- <img src="<c:url value="/common/_images/icons/arrowDown-menu${sOn=='reports'?'On':'Off'}.gif"/>" width="8" height="8"/>--></a>
			<ul class="reports">
				<li><a href="<c:url value="/ActionPlansReports.go"><c:param name="fromTab" value="${param.rangeId}"/></c:url>"><fmt:message key="link.actionPlans"/></a></li>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="oldIMT/DealLogDisplayAction.go"/>">Deal Log</a></li>			
				</c:if>
				<li><a href="<c:url value="oldIMT/FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26"/>">Fastest Sellers</a></li>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="oldIMT/DashboardDisplayAction.go?module=U"/>">Inventory Manager</a></li>			
				</c:if>

<!-- out until further notice					
				<c:if test="${sessionScope.nextGenSession.internetAdvertisersEnabled}">
					<li><a href="<c:url value="/ExceptionReportDisplayAction.go" />" />Inventory Transfer Report</a></li>
				</c:if>
-->				
				
				<c:if test="${includeAgingPlan}">
					<li>
						<html:link forward="inventoryOverview"><fmt:message key="overview.pageTitle"/></html:link>
					</li>
					
				</c:if>
				<li><a href="<c:url value="oldIMT/FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26"/>">Most Profitable Vehicles</a></li>	
				<c:if test="${includeRedistribution}">
					<li><a href="javascript:pop('<c:url value="oldIMT/ReportCenterRedirectionAction.go"/>','reportCenter')">Performance Management Reports</a></li>	
				</c:if>
				<c:if test="${sessionScope.nextGenSession.internetAdvertisersEnabled}"> 
					<li><a href="<c:url value="/PriceChangeFailureReportAction.go" />" />Price Change Failure Report</a></li>
			    </c:if>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="oldIMT/PricingListDisplayAction.go"/>">Pricing List</a></li>				
				</c:if>
				<c:if test="${includeCIA}">
					<li>
						<html:link forward="stockingReports">
							<fmt:message key="link.text.stockingReports"/>
						</html:link>	
					</li>					
				</c:if>
				<li><a href="<c:url value="oldIMT/FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26"/>">Top Sellers</a></li>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="oldIMT/TotalInventoryReportDisplayAction.go"/>">Total Inventory Report</a></li>
					<li><a href="<c:url value="oldIMT/DealerTradesDisplayAction.go"/>">Trades &amp; Purchases Report</a></li>		
				</c:if>
			</ul>		
		</li>
</c:if>
<c:if test="${!reducedToolsMenu}">
		<li><tt>|</tt></li>
		<li${sOn=='redist'?' class="on"':''}><c:url value="oldIMT/RedistributionHomeDisplayAction.go" var="redistURL"/><a href="${includeRedistribution ? '#':redistURL}" onclick="return false;">REDISTRIBUTION<c:if test="${includeRedistribution}"><font class = "arrow">&#x25BC;</font><!--  <img src="<c:url value="/common/_images/icons/arrowDown-menu${sOn=='redist'?'On':'Off'}.gif"/>" width="8" height="8"/>--></c:if></a>
	<c:if test="${includeRedistribution}">
			<ul class="redist">
				<li><a href="<c:url value="oldIMT/InventoryExchangeDisplayAction.go"/>">Aged Inventory Exchange</a></li>
				<li><a href="<c:url value="oldIMT/ShowroomDisplayAction.go"/>">Showroom</a></li>
			</ul>
	</c:if>			
		</li>
</c:if>
		<li><tt>|</tt></li>
		<c:choose>
			<%--Inventory Plan--%>
			<c:when test="${isPrintable&&sPrintPage=='plan'}">
				<li><a href="#" onclick="return false;">PRINT<font class = "arrow">&#x25BC;</font><%-- <img src="<c:url value="/common/_images/icons/arrowDown-menuOff.gif"/> " width="8" height="8"/>--%></a>
					<ul class="print">
						<li><a href="javascript:pop('<c:url value="/PrintInventoryPlan.go"/>','print')">Full Report</a></li>
						<li><a href="javascript:printPage('${sPrintPage}')">Current Page</a></li>
					</ul>
				</li>
			</c:when>
			<%--Inventory Overview--%>
			<c:when test="${isPrintable&&sPrintPage=='overview'}">
				<!-- for using iframe from old IMT (temporary) -->
				<li id="printCell">PRINT</li>		
			</c:when>			
			<%--Standard--%>
			<c:otherwise>
				<c:choose>
					<c:when test="${isPrintable}">
						<li><a href="javascript:printPage('${sPrintPage}')" id="printHref">PRINT</a></li>			
					</c:when>
					<c:otherwise>
						<li style="color:#666;">PRINT</li>	
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
		<li><tt>|</tt></li>
		<li><a href="<c:url value="/CloseSessionAction.go?forwardTo=/ExitStoreAction.go"/>">EXIT STORE</a></li>
	</ul>

	<div class="textnav">
<a href="javascript:pop('<c:url value="oldIMT/EditMemberProfileAction.go"/>','profile');">Member Profile</a><tt>|</tt><a href="javascript:pop('/support/Marketing_Pages/AboutUs.aspx','about');">About First Look</a><tt>|</tt><a href="javascript:pop('/support/Marketing_Pages/ContactUs.aspx','contact');">Contact First Look</a><tt>|</tt><a href="<c:url value="/CloseSessionAction.go?forwardTo=/LogoutAction.go"/>">Log Out</a> <img src="<c:url value="/common/_images/d.gif" context="/IMT"><c:param name="arg"><fmt:formatDate value="${TODAY}" pattern="yyyyddMMssss"/></c:param></c:url>" width="1" height="1" />	
	<!-- the image above (d.gif) is there to keep NextGen and IMT sessions in synch -->
	</div>
</div>
<%-- required to prevent select boxes showing through in IE --%>
<!--[if lte IE 6.5]><iframe src="javascript:false;" id="selectFix" class="selectFix"></iframe><![endif]-->

	<c:if test="${pageId=='io'}">
		<!-- ////
		Importing a temporary JSP so that we can use the old print functionality.
		Please remove it when the new printing functionality is in place
		//// -->
		<c:import url="/common/ioPrintingHack.jsp"/>
		<%--<%@ include file="ioPrintingHack.jsp" %>--%>

	</c:if>
