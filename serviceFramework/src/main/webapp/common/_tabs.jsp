<tiles:importAttribute/>
	<fmt:message key="${pageId}.tabAction" var="sURL"/>
<!--names of tab properties must be passed-->
	<c:set var="sID" value="${sTabID}"/>
	<c:set var="sUnits" value="${sTabUnits}"/>
	<c:set var="sDesc" value="${sTabText}"/>
<!--active tab-->
	<c:set var="iOnID" value="${param[sID]}"/>
<div id="tabs">

	<ul>
		<c:forEach items="${addtlTabs}" var="aTab" varStatus="a">
			<c:set var="aAdd" value="${fn:split(aTab,'|')}"/><c:set var="aID" value="${aAdd[0]}"/><c:set var="aUnits" value="${aAdd[1]}"/><c:set var="aDesc" value="${aAdd[2]}"/><c:if test="${!empty aAdd[3]}"><c:set var="aURLParam" value="${aAdd[3]}"/></c:if>		
			<c:if test="${iOnID==aID||(empty iOnID && a.first)}" var="bOn"/>
				<li class="tab${a.index+1}${bOn?' active':bSuppressNextVLine?'':' vline'}"><a ${bOn?'no':''}href="<c:url value="${sURL}"><c:param name="${sID}" value="${aID}"/></c:url><c:if test="${!empty aURLParam}">&${aURLParam}</c:if>" id="tab${a.index+1}"><h5>${aDesc} <c:if test="${!empty aUnits}"><span>(${aUnits})</span></c:if></h5></a></li>
			<c:if test="${bOn}" var="bSuppressNextVLine"/>
		</c:forEach>
		
		<c:forEach items="${mTabs}" var="tab" varStatus="t">
			<c:set var="iID"><bean:write name="tab" property="${sID}"/></c:set><c:if test="${iOnID==iID||(empty iOnID && t.first && empty addtlTabs)}" var="bOn"/>
			<li class="tab${t.index+1+fn:length(addtlTabs)}${bOn?' active':bSuppressNextVLine?'':' vline'}">
				<a ${bOn||(tab.Units==0&&!t.first)?'no':''}href="<c:url value="${sURL}"><c:param name="${sID}" value="${iID}"/></c:url><c:if test="${iID==0}">&ResultsMode=2</c:if>" id="tab${t.index+1+fn:length(addtlTabs)}">
					<h5${tab.Units==0&&!t.first?' class="none"':''}><fmt:message key="${pageId}.tabText"><fmt:param><bean:write name="tab" property="${sDesc}"/></fmt:param></fmt:message>
					<c:set var="iUnits"><bean:write name="tab" property="${sUnits}"/></c:set><c:if test="${!empty iUnits}"><span id="tab${t.index+1}Value">(${iUnits})</span></h5></c:if>
				</a>
			</li><c:if test="${bOn}" var="bSuppressNextVLine"/>
		</c:forEach>
	</ul>
</div>