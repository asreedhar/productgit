<fmt:message key="link.url.optimalSummary" var="optimalSummaryLink"/>
<span class="analytics"><div id="header">
<c:choose>
<c:when test="${pageId=='navigator'}">
	<a href="<c:url value="${optimalSummaryLink}"/>" onclick="return preventClicks(this);" title="<fmt:message key="link.text.optimalSummary.returnTo"/>"><img src="<c:url value="/common/_images/logos/FL-89x21-transOnE5.gif"/>" class="fl"/></a>
	<img src="<c:url value="/common/_images/logos/Navigator-103x13-transOnE5.gif"/>" class="navigator"/>
	<img src="<c:url value="/common/_images/logos/WithEdgeAnalytics-86x11-transOnE5.gif"/>" class="analytics"/>
	<hr/>
</c:when>
<c:otherwise>
	<fmt:message key="url.edge.home" var="edgeLink"/><c:set var="backToStr"><fmt:message key="edge"/></c:set>
	<img src="<c:url value="/common/_images/logos/FL-wTagline-transOnBlue.gif"/>" class="logo"/>
	<h5>${sessionScope.nextGenSession.dealerNickName}</h5>
	<h2><fmt:message key="fl"/> <fmt:message key="l.searchEngine"/></h2>
	<h1><fmt:message key="${pageId}.pageTitle" var="sPageTitle"/>${sPageTitle}</h1>
	<c:set var="comingFrom" value="${isDR ? 'dr':isVIP ? 'vip': isFL ? 'firstlook': isMax ? 'max' : 'edge'}" />
	<fmt:message key="${comingFrom}" var="backToStr"/><fmt:message key="link.url.${comingFrom}" var="urlStr"><fmt:param>${sessionScope.nextGenSession.businessUnitId}</fmt:param></fmt:message>
	<c:choose>
		<c:when test="${isDR}">
			<c:url value="${urlStr}" context="/PrivateLabel" var="backToUrl"/>
		</c:when>
		<c:otherwise>
			<c:url value="${urlStr}" var="backToUrl"/>
		</c:otherwise>
	</c:choose>
	<a href="${backToUrl}" class="backLink"/><fmt:message key="link.text.${comingFrom}.backTo"><fmt:param>${fn:toUpperCase(backToStr)}</fmt:param></fmt:message></a>
	<c:if test="${not isErrorPage}">
		<a id="profileEdit" href="javascript:showProfileEdit('profileEdit');" class="profileLink"/><fmt:message key="pref.profile"/></a>
	</c:if>
</c:otherwise>
</c:choose>
</div>
</span>
