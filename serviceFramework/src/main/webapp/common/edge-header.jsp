<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:message key="${pageId}.helpText" var="sHelpText" scope="request"/>
<c:if test="${!(fn:contains(sHelpText,'???'))}" var="bHasHelp"/>
<div id="header">
	<c:if test="${not isErrorPage}"><h3>${sessionScope.nextGenSession.dealerNickName}</h3></c:if>
	<h1><fmt:message key="${pageId}.pageTitle" var="sPageTitle"/>${sPageTitle}<c:if test="${bHasHelp}"><a href="javascript:show('pageHelp');" id="pageHelpLink"><img src="<c:url value="/common/_images/icons/help-on333.gif"/>" width="22" height="22" class="help"/></a></c:if></h1>
<c:if test="${bHasHelp}"><c:import url="/common/_help.jsp"><c:param name="helpType" value="pageHelp"/></c:import></c:if>
	<c:if test="${isPrintable}">
<div class="printOnly">
	<%--print version--%>
	<h2>${sPageTitle} for ${sessionScope.nextGenSession.dealerNickName}</h2>
</div>
	</c:if>
</div>