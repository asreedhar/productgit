<tiles:importAttribute/>
<c:choose>
	<c:when test="${!empty context}">
		<%-- used for outgoing redirection to IMT --%>
		<c:redirect url="${scheme}://${header['host']}/${context}/${forwardTo}" />
	</c:when>
	<c:otherwise>
		<%-- used for intra-NextGen redirection 
			(context is not needed since links use <c:url/> --%>
		<c:redirect url="${forwardTo}"/>
	</c:otherwise>
</c:choose>