<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%--
This is a TEMPORARY jsp that is used for the "Printing...."
IFrame.  We are using the old IMT printing functionality so as
to not hold up the release, and we need this IFrame so that printing
works the same way as it does on the old site.  We weren't able to
do a <c:import .../> to pull in the JSPs from the old site, so
we're using this jsp instead.
--%>

<script language="javascript" type="text/javascript">
var IFrameObj;
var isViewDeals = false;

window.onload=function()
{
    loadPrintIframe();
} 

function enablePrintLink()
{
	printFrameIsLoaded = "true";
	var printCell = document.getElementById("printCell");

	if(isViewDeals)
	{
			printCell.innerHTML = '<a href="#" onclick="printPage()" id="printHref"><img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage"></a><br>'
	}
	else
	{
			printCell.innerHTML = '<a href="#" onclick="printPage()" id="printHref">PRINT</a>';
			printCell.className = "navTextoff";
	}
}

function loadPrintIframe()
{
	enablePrintLink();
}

function printTheIframe()
{
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();

}

function printPage()
{
	loadPrintIFrameOnDemand();
	progressBarInit();
}

function lightPrint()
{
	progressBarDestroy();
	printTheIframe();
} 

function loadPrintIFrameOnDemand( )
{
	if (!document.createElement) {return true};
	var IFrameDoc;
	//PrintableInventoryOverviewEdgeReportDisplayAction
	var URL = "<c:url value="oldIMT/PrintableInventoryOverviewReportDisplayAction.go?weeks=26&reportType=${reportType}"/>";
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		/* create the IFrame and assign a reference to the
		object to our global variable IFrameObj.
		this will only happen the first time
		callToServer() is called */
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','<c:url value="oldIMT/javascript/printHolder.html"/>');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';		
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	IFrameDoc.location.replace(URL);
	return false;
}
</script>


<div id="fred">
	<iframe style="display: none; left: 0px; position: absolute; TOP: 0px" id="printProgressIFrame" src="javascript:false;" frameBorder="0" scrolling="no"></iframe>
	
		<div id="printingProgressBox"  STYLE="layer-background-color:#333333; visibility:hidden; z-index:10; background:#333333; position:absolute; height:120px; left:400px; top:50px; width:300px; border:1px solid #ffcc33;">
			<br>
			<div align="center" >
				<font style="font-size:12px;color:#ffcc33;font-weight:bold">Printing . . . .</font>
			</div>
			<br>
			<div align="center" >
				<script language="javascript" src="<c:url value="common/_scripts/timerbar.js"/>"></script> 
			</div>
			<br>
			<div align="center" >
				<font style="font-size:10px;color:#ffffff;font-weight:bold">
				Please wait while your printable page loads...
				</font>
			</div>
		</div>	
</div>