<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="sPrintPage" value="${pageId}"/>

<div id="menu">
	<%--for printing--%>
	<img src="<c:url value="/common/_images/logos/FL-print.gif"/>" class="logoFL printOnly"/>
	<img src="<c:url value="/common/_images/logos/FL-popupOn333.gif"/>" width="115" height="27" class="logo screenOnly"/>
	<div class="textnav">
<c:if test="${isPrintable}"><a href="javascript:printPage('${sPrintPage}');"><img src="<c:url value="/common/_images/text/printOn333.gif"/>" alt="Print" width="24" height="11"/></a>
<tt>|</tt>
</c:if>
<a href="javascript:closeWindow();"><img src="<c:url value="/common/_images/text/closeWindowOn333.gif"/>" alt="Close Window" width="69" height="11"/></a>
	</div>
</div>