<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- receives helpType & sHelpText from request --%>
<div style="display:none;" id="${param.helpType}">
	<div class="help">
		<h4><a href="javascript:hide('${param.helpType}');" class="close">X</a><fmt:message key="l.help"/></h4>
		<div class="text">
	${sHelpText}
		</div>
	</div>
</div>