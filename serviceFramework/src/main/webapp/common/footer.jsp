<jsp:useBean id="_now" class="java.util.Date"/>
<div id="footer">
	<em>SECURE AREA</em> <tt>|</tt>	<cite>&copy; <fmt:formatDate value="${_now}" pattern="yyyy"/> <fmt:message key="incisent"/> All rights reserved.</cite> 
	<c:if test="${isLogoDisplayFooter}">
			<div id="FooterLogo" style="display:inline;width:125px;height:16px;float:right">
				<img src="<c:url value="/common/_images/logos/FL-89x21-transOnE5.gif"/>" height="16px" width="100px" class="logoFL"/>
			</div>
		</c:if>
</div>