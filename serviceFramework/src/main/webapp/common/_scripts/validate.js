String.prototype.trim =  function(){

	a = this.replace(/^\s+/, '');
	return a.replace(/\s+$/, '');
};

var Validate = {

	initialize:function(){
	},
	/**/
	/*Method validates a date of the format mm/dd/yyyy
	/*
	/**/
	validateDate:function (calendarElement, currentDate, defaultValue, allowPast){
	var validator = /^\d{2}(\/)\d{2}\1\d{4}$/;  //regexpression date validator
	
	//if nothing entered don't bother to check
	if($(calendarElement).value != '' && $(calendarElement).value != null){
	
		if( validator.test( $(calendarElement).value ) ){
		
			var today = Date.parse(currentDate); //today in milliseconds
			var newDate = Date.parse( $(calendarElement).value );	//the date being entered	
			var limit = today;  //default limit to todays date (prevent dates before today from being entered)		
		
			if( newDate >= limit || allowPast ){  //make sure only future dates are selected, unless allow past is true
				return true;
			}else{
			
				alert("Date cannot be earlier than " + currentDate);
				$(calendarElement).value = currentDate;
				return false
			}
		}
		
		alert("Date should use the format 'mm/dd/yyyy'");
		
		if(defaultValue == null){
			defaultValue = '';
		}
		$(calendarElement).value = defaultValue;
		return false;
	}
	return false;
},

 checkEMail:function(ele)
{
	var x = $(ele).value;
	return this.checkEmailString(x);
},
checkEmailString:function(str){

		var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (filter.test(str)) {
			return true;
		}
		return false;

},
checkEmails:function(emails, separator){

	if(emails.charAt(emails.length-1) == separator){
		emails = emails.substring(0,emails.length-1);
	}
	var badEmails = new Array();	
	emails = emails.replace(/ /g,'');
	var incoming = emails.split(separator);
	
	for(var i = 0; i < incoming.length; i++){	
		if(!this.checkEmailString(incoming[i])  ){
	
			badEmails.push(incoming[i]);
		}	
	}
	return badEmails;

},
	/**/
	/*Method is to be used in conjunction with a text field
	/*Only allows numeric values to be entered
	/**/
	checkChars:function(){
		
		 var key = event.keyCode;
		
		 if((key < 48 || key > 57) && (key != 46 && key!=8)){ //only nueric values allowed
		 	return false;
		 }
		return true;
	},
	/**/
	/*Method checks each char in a string
	/*Only accepts $ , . and numeric
	/**/
	checkCurrency:function(val){ 
	
		if( val != '' ){
		
			var bad = true;
			
			for(var i = 0; i < val.length; i++){
			
				if( val.charAt(i) != '$' && val.charAt(i) != '.' && val .charAt(i) != ',' ){
				
					if(isNaN(val.charAt(i))){
					
						bad = false;
					}
				}			
			}		
		}
		return bad;	
	},
	/**/
	/*Method assumes element has a label of format 'element id'Label
	/*Method assumes 2 css classes exist, req for required and ok for satisfied
	/**/
	toggleReq:function(e,errorElement) {
		var defalt = ($(e).type!='select-one' && $(e).type!='select')?'':0;
		if(!$(e.id+'Label')) return;
		//rehide error message
		$(e).value = $(e).value.trim();
		if($(e).value !=defalt && errorElement){
		
			Element.addClassName(errorElement,'hide');
		}
	
		$(e.id+'Label').className=$(e).value==defalt?'req':'ok';
	},
	/**/
	/*Method verifies that a value falls within a range
	/*
	/**/
	checkRange:function( low, high, val ){

		if(val <  low  || val > high ){
			return false;
		}
		return true;
	},
	/**/
	/*Method checks whether a text field has been filled or not
	/*
	/**/
	checkRequired:function(e){	
		if( $(e) ){
			$(e).value = $(e).value.trim();
			if( $(e).value == '' || $(e).value == null ){			
				return false;
			}			
			return true;		
		}else{		
			this.debug("Element, "+e+", does not exist. (Validate.checkRequired)");
			return false;			
		}
	},
	debug:function(msg){
	
		alert("Error: "+msg);
		
	}
}