
/*
function makeTimedLayer(id,interval){
	makePopLayer(id,'warning','listPriceDisplay');
alert(id);
	setTimeout(removeAdPriceLayer, interval);
}

*/


// Create a new window for our event handler to display event details in.
/*

*/

var d = null;
function make() {
	var ewin = window.open("", "debug","width=300,height=600,scrollbars,resizable,menubar");
	d = ewin.document;
	d.open("text/plain");
}
function clearIt() {
	if(d == null) {
		make();	
	}
	else {
		d.close();
		d.open("text/plain");
	}
}

function write(str,clearIt) {
	if(d == null) {
		make();
	} else {
		if(clearIt) {
			d.close();
			d.open("text/plain");	
		}
	}
	var _d = new Date();
		_d = _d.getHours()+':'+_d.getMinutes()+':'+ _d.getSeconds()+':'+ _d.getMilliseconds();
	d.writeln(_d + ' -- ' + str);
	d.focus()
}

Event.observe(document, 'mouseover', showFunction);
function showFunction() {
	var el = Event.element(event);
	if(el.onclick) {
		window.status = el.onclick;
	}
	return true;
}

function writeAttributes(id) {
	clearIt();
	if(!$(id)) {
		$('debugMsg').innerHTML = 'No element with id = '+id;
		return false;
	}
	var el = $(id);
	var str = '';
	$H(el).each(function(attr) {
      str += attr.key + ': ' + attr.value+ '\n';
    });
	write(str);
}


function writeHTML(id) {
	if(!$(id)) {
		$('debugMsg').innerHTML = 'No element with id = '+id;
		return false;
	}	
	var obj = $(id);
	write(obj.innerHTML,true);
}


function getCoords(el) {
	var _el = $(el);
	var _dim = Element.getDimensions(_el); //{ width: 0, height: 0 }
	var _pos = Position.cumulativeOffset(_el); // [0, 0]
	var _coords = [_pos[0] + _dim.width, _pos[1] + _dim.height];
	return _coords;
}


var Debug = Class.create();
Debug.prototype = {
    initialize: function(){ 
		this.hdr = '// ';
		this.ftr = '// ';
    },
  	z: function(a){
  		this.type=typeof(a);

		//if(isNaN(a.length)) type='array';
		head=this.type.toUpperCase();
		foot=this.type.toUpperCase();
		//z='';
		switch(this.type) {	
			//case 'number':
			case 'function':
				z = 'nothing defined yet for '+this.type.toUpperCase();
				break;
			case 'array':
				head += ' / length : '+ a.length;
	  			head += ' / converted to JSON string';
				z = a.toJSONString();
				break;
			case 'object':
	  			head += ' / converted to JSON string';
				z = a.toJSONString();
				break;
			default:
				z = a;
		}
		this.msg = [debug.hdr+head,'\n\n'+z+'\n\n',debug.ftr+foot];
		write(this.msg.join(''));
    }      
}
debug = new Debug();
$Z = debug.z;