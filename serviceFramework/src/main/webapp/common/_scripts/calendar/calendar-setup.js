// This function gets called when the end-user clicks on some date.
var field = null;

function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked){
	    // if we add this call we close the calendar on single-click.
	    // just to exemplify both cases, we are using this only for the 1st
	    // and the 3rd field, while 2nd and 4th will still require double-click.

		//added for updating display text instead of field
		if(cal.sel.type == 'hidden') {
			var el;
			el = document.getElementById(cal.sel.id+'Display');
			if(el != null) {
				el.innerHTML = cal.sel.value;
			}
		} else {
			cal.sel.focus();
		}
	    cal.callCloseHandler();
    }
}
var sel = selected;
function setDays(cal, date){

		var today = new Date();
		var selected = Date.parse(date);
		var DAY = 1000 * 60 * 60 * 24; //milliseconds in a day
		
		var diff = selected - today;
		
		var days = Math.ceil(diff / DAY);
	    cal.sel.value = days;
	    if (cal.dateClicked){
	   		cal.sel.focus();
			cal.callCloseHandler();
			
		}

}


var sd = setDays;

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendarFloosan(id, format, showsTime, showsOtherMonths, showsPast,today) {
  var el = document.getElementById(id);
  field = el;

  //if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    //_dynarch_popupCalendar.hide();                 // so we hide it first.
  ////} else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, sd, closeHandler, showsPast,el,today);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
   document.getElementsByTagName('body')[document.getElementsByTagName('body').length-1]
    cal.setRange(2006, 2076);        // min/max year allowed.
    cal.create();
    
  //}
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  
  var today = new Date();
  if( el.value != ''){
  		
		
		var DAY = 1000 * 60 * 60 * 24; //milliseconds in a day
		
		var milliDays = DAY * parseInt(el.value);
		var selected = today.getTime() + milliDays;
		 	
  }else{
  
  	selected = today;
  }
  _dynarch_popupCalendar.parseDate(selected,'none');      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el,'Br');        // show the calendar
  
   
  return false;
}
// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths, showsPast,today) {
  var el = document.getElementById(id);
  field = el;

  //if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    //_dynarch_popupCalendar.hide();                 // so we hide it first.
  ////} else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, sel, closeHandler, showsPast,el,today);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
   document.getElementsByTagName('body')[document.getElementsByTagName('body').length-1]
    cal.setRange(2006, 2076);        // min/max year allowed.
    cal.create();
    
  //}
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el,'Br');        // show the calendar
  
   
  return false;
}

function showCalendarByPointer(id, format, showsTime, showsOtherMonths, showsPast,today) {
	showCalendar(id, format, showsTime, showsOtherMonths, showsPast,today);
	if (	_dynarch_popupCalendar && _dynarch_popupCalendar.element && 
			window.event && window.event.x && window.event.y &&
			document.documentElement && document.documentElement.scrollTop ) {
		
		var top = document.documentElement.scrollTop + window.event.y,
			left = document.documentElement.scrollLeft + window.event.x,
			innerWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
			calWidth = 230;
		_dynarch_popupCalendar.element.style.top = top;
		_dynarch_popupCalendar.element.style.left = (left + calWidth < innerWidth) ? left : (innerWidth - calWidth);
	}
	return false;
}