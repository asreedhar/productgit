
/* fix for IE - prevent dropdowns showing through */
function obscureDropdown(how) {
	if(document.getElementById('selectFix') != null) {
		iFrameObj = document.getElementById('selectFix');
		iFrameObj.style.display = how=='hide'?'none':'block';
	}
}
activateMenu = function() {
	var navRoot;	
	navRoot = document.getElementById("dropnav");
	if(!navRoot) return; 	
	if (document.all&&document.getElementById) {
		for (i=0; i<navRoot.childNodes.length; i++) {
			node = navRoot.childNodes[i];
			if (node.nodeName=="LI") {
				node.onmouseover=function() {
					this.className+=" over";
					if ( this.getElementsByTagName('li').length > 10 ) {
						obscureDropdown('show');						
					}
				};
				node.onmouseout=function() {
	  				this.className=this.className.replace(" over", "");	  				
	  				obscureDropdown('hide');
	   			};
	   		}
	  	}
 	}
};
window.onload=activateMenu;
