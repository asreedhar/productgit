
/*
function Loading() {
	var el = document.createElement('div');
	var txt = document.createTextNode('Loading ...');
	//var img = document.createElement('img');
	//img.src = 'common/_images/icons/loading.gif';
	el.appendChild(txt);
	//el.appendChild(img);
	el.style.className = 'loading';
	return el;
}
*/


var UI = {
	initialize:function(){
	
	},

	swap:function(idToHide, idToShow){
		
		this.hide(idToHide);
		this.show(idToShow);
	},
	/**/
	/*Method to show a hidden item
	/**/
	show:function(id){
	
		if($(id)){
		
			$(id).style.display = 'block';
			
		}else{
		
			this.debug("Could not locate item:"+id+"  to show (UI.show)");
			
		}
	
	},
	swapSelect:function(parentId){
	
		var children = $(parentId).children;
		var link = null;
		var select = null;
		
		for(var i = 0; i < children.length; i++){
		
			if( children[i].className == 'hide' ){
			
				children[i].className = 'show';
				
			}else{
			
				children[i].className = 'hide';
			}
			
			if( children[i].tagName == 'A' ){
				
				link = children[i];
				
			}else if( children[i].tagName == 'SPAN' ){
				select  = children[i].children[0];
				
				if(children[i].className == 'show'){
					
					$(parentId).className = $(parentId).className+' noBack';
			
					if(Element.visible(select)){
						select.focus();			
					}			
					
				}else{
					$(parentId).className = $(parentId).className.replace(' noBack','');
				}
				
			
			}
			
		}
		
		link.innerHTML = select.options[select.selectedIndex].text;
	
	
	},
	/**/
	/*Method to hide a visible item
	/**/
	hide:function(id){
	
		if($(id)){
		
			$(id).style.display = 'none';
			
		}else{
		
			this.debug("Could not locate item:"+id+"  to hide (UI.hide)");
			
		}
	},
	debug:function(msg){
	
		alert("Error: "+msg);
		
	},
	createPopupLayer:function(id, clss){
	
		var layer = '<div id="'+id+'"';
		if(clss){
		
			layer+= ' class="'+clss+'"></div>\n';
		}
	
		return layer;
	},
	
	createConfirmDialog:function(id, msg, buttons, clss){
	
		var layer = this.createPopupLayer(id, clss);
		var buttonsString = '';
		new Insertion.After($('wrap'),layer); //create the div element
		
		if(Util.isArray(buttons)){
			for(var i = 0; i < buttons.length; i++){
			
					buttonsString += buttons[i];
			
			}
		}else{
		
			buttonsString += buttons;
		}
		
		if($(id)){
		
			$(id).innerHTML = msg+'<br\>'+buttonString;
			
			return true;
		}
		return false;
	}	

}




// Copyright (C) 2005 Ilya S. Lyubinskiy. All rights reserved.
// Technical support: http://www.php-development.ru/


// USAGE
//
// function popup_show(id, drag_id, exit_id, position, x, y, position_id)
//
// id          - id of a popup window;
// drag_id     - id of an element within popup window intended for dragging it
// exit_id     - id of an element within popup window intended for hiding it
// position    - positioning type:
//               "screen-corner", "screen-center"
//               "mouse-corner" , "mouse-center"
//               "element-right", "element-bottom"
// x, y        - offset
// position_id - for the last two types of positioning popup window will be
//               positioned relative to this element
//Modified by John Caron
//drag_id is optional, enter '' or null to eliminate window dragging.

// ----- Variables -------------------------------------------------------------


function Popup(){

	var popup_dragging = false;
	var popup_target;
	var popup_mouseX;
    var popup_mouseY;
	var popup_mouseposX;
	var popup_mouseposY;
	var popup_oldfunction;
	var popup_height;
	var animate = false;
	var element;
	var position_element;
	var selects = null;
	var preventClip = false;
	
	var ie = navigator.appName == "Microsoft Internet Explorer";
	this.popup_display = popup_display;
	this.popup_mousedown = popup_mousedown;
	this.popup_mousemove = popup_mousemove;
	this.popup_mouseup =popup_mouseup;
	this.popup_exit = popup_exit;
	this.popup_show = popup_show;
	this.popup_mousepos = popup_mousepos;
	this.refresh = refresh;
	this.noClip=noClip;
	
	function popup_display(x){
	
		 var win = window.open();
		  for (var i in x) win.document.write(i+' = '+x[i]+'<br>');
	
	}
	
	function noClip(b){//turn off selectbox clipping
	
		preventClip=b;
	}
	
	function popup_mousedown(e){
	
	 	var ie = navigator.appName == "Microsoft Internet Explorer";

		  if ( ie && window.event.button != 1) return;
		  if (!ie && e.button            != 0) return;
		
		 popup_dragging = true;
		 popup_target   = this['target'];
		 popup_mouseX   = ie ? window.event.clientX : e.clientX;
		 popup_mouseY   = ie ? window.event.clientY : e.clientY;
	
		  if (ie)
		       popup_oldfunction      = document.onselectstart;
		  else popup_oldfunction      = document.onmousedown;
		
		  if (ie)
		       document.onselectstart = new Function("return false;");
		  else document.onmousedown   = new Function("return false;");
	
	}
	
	
	function popup_mousemove(e){
	
//	alert(this.popup_dragging);
			 if (!popup_dragging) return;
			
			  var ie      = navigator.appName == "Microsoft Internet Explorer";
			//  var element = document.getElementById(this.popup_target);
			
			  var mouseX = ie ? window.event.clientX : e.clientX;
			  var mouseY = ie ? window.event.clientY : e.clientY;
			
			var maxHeight = document.documentElement.clientHeight + document.documentElement.scrollTop;
			var maxWidth = document.documentElement.clientWidth;
			var newX =  (element.offsetLeft+mouseX-popup_mouseX);
			var newY = (element.offsetTop +mouseY-popup_mouseY);
			
			if( newX + element.clientWidth > maxWidth-1 ){
			
				newX =( maxWidth-1) - element.clientWidth;
					
			}else if(newX <1){
			
				newX = 1;
			}
			
			if(newY + element.clientHeight > maxHeight -1){
			
				newY =( maxHeight-1) - element.clientHeight;
			}else if(newY < document.documentElement.scrollTop + 1){
			
				newY = document.documentElement.scrollTop + 1;
			}
			element.style.left = newX+'px';
			element.style.top  = newY+'px';
			
			
			element.left = newX+'px';
			element.top = newY+'px';
			
		    popup_mouseX = ie ? window.event.clientX : e.clientX;
			popup_mouseY = ie ? window.event.clientY : e.clientY;
	}
	
	function popup_mouseup(e){
	
			 if (!popup_dragging) return;
		 		popup_dragging = false;
		
		  var ie      = navigator.appName == "Microsoft Internet Explorer";
		  var element = document.getElementById(popup_target);
		
		  if (ie)
		       document.onselectstart = popup_oldfunction;
		  else document.onmousedown   = popup_oldfunction;			
	
	}
	
	function popup_exit(e){
	
			 var ie      =Util.isIE();
		  //	 var element = document.getElementById(this.popup_target);
		
			if(exit_element){
		  		popup_mouseup(e);
		  	}
		
		  if(animate){
			  	var myEffect = new fx.Height(element , {duration: 500, onComplete: function()
				  {
				    element.style.visibility = 'hidden';
			 		 element.style.display    = 'none';
				  }
				});
				myEffect.custom(popup_height,0);
				myEffect.toggle();
			}else{
					 element.style.visibility = 'hidden';
			 		 element.style.display    = 'none';
			}
			if(drag_element){
				var selects = document.getElementsByTagName('SELECT');
					for(var i=0; i< selects.length; i++){
					selects[i].style.visibility='visible';
					}
			}
		
			if(position_element){
				if(Util.isIE()){
					 position_element.detachEvent('onclick',preventNewWindow);
				}else{
					 position_element.removeEventListener('click',preventNewWindow, false);
				}
			}
	}
	
	function preventNewWindow(){
		return false;
	}
	
	function popup_show(id, drag_id, exit_id, position,height,anim, x, y, position_id){
	
		  element      = document.getElementById(id);
		  drag_element = document.getElementById(drag_id);
		  if(Util.isArray(exit_id)){
			  exit_element = new Array();
		  		for(var i = 0; i < exit_id.length; i++){
		  		
		  			exit_element[i] = document.getElementById(exit_id[i]);
		  		}
		  
		  }else{
			  exit_element = document.getElementById(exit_id);
		 }
		  animate = anim;
		
		  if(element.style.visibility == "visible" && element.style.display == "block"){
		  
		 	popup_exit();
		
		  }
		
		
		  element.style.position   = "absolute";
		  element.style.visibility = "visible";
		  element.style.display    = "block";
		  popup_height = height;
		  
		  if(animate){
		
		    var myEffect = new fx.Opacity(element , {duration: 500, onComplete: function(){ }});
		
			myEffect.toggle();
		 }
			
		  if (position == "screen-corner")
		  {
		     element.style.left = (document.documentElement.scrollLeft+x)+'px';
		     element.style.top  = (document.documentElement.scrollTop +y)+'px';
		  }
		
		  if (position == "screen-center")
		  {
		  
		      element.style.left = (document.documentElement.scrollLeft+(document.documentElement.clientWidth -element.clientWidth )/2+x)+'px';
		   
		      element.style.top  = (document.documentElement.scrollTop +(document.documentElement.clientHeight-element.clientHeight)/2+y)+'px';
		  
		 
		  }
		
		  if (position == "mouse-corner")
		  {
		      element.left = (document.documentElement.scrollLeft+popup_mouseposX+x)+'px';
		      element.top  = (document.documentElement.scrollTop +popup_mouseposY+y)+'px';
		  }
		
		  if (position == "mouse-center")
		  {
		     element.left = (document.documentElement.scrollLeft+popup_mouseposX-element.clientWidth /2+x)+'px';
		     element.top  = (document.documentElement.scrollTop +popup_mouseposY-element.clientHeight/2+y)+'px';
		  }
		
		  if (position == "element-right" || position == "element-bottom")
		  {
		     position_element = document.getElementById(position_id);
			
			if(Util.isIE()){ //prevent duplicate windows
				 position_element.attachEvent('onclick',preventNewWindow);
			}else{
				 position_element.addEventListener('click',preventNewWindow, false);
			}
			
		    for (var p = position_element; p; p = p.offsetParent)
		      if (p.style.position != 'absolute')
		      {
		        x += p.offsetLeft;
		        y += p.offsetTop ;
		      }
				
		    if (position == "element-right" ) x += position_element.clientWidth;
		    if (position == "element-bottom") y += position_element.clientHeight;
			
			if(x + element.clientWidth > document.body.clientWidth){
		   
				x = x - ((x+ element.clientWidth) - document.body.clientWidth) - 15;
				
			}
		
		
		  	if(  element.style.left ){
		
		  		  element.style.left =  x+'px';
		  		  element.style.top =  (y - 7)+'px';
		  	
		  	}
		  	
		  }
			
			if(drag_element){
			    drag_element['target']   = id;
			    popup_target = id;
			    drag_element.onmousedown =   this.popup_mousedown;
			 }else{
			 
			 	  popup_target = id;
			 }
	
			if(exit_element){
		
				if(Util.isArray(exit_element)){
				
					for(var i = 0; i < exit_element.length; i++){
					
						if(ie){
						 	exit_element[i].attachEvent('onclick', popup_exit);			
						 }else{
						  	exit_element[i].addEventListener('click', popup_exit,false);			
						 }		
					}
				}else{
		   			
						if(ie){
						 	exit_element.attachEvent('onclick', popup_exit);			
						 }else{
						  	exit_element.addEventListener('click', popup_exit, false);			
						 }		
		   		}
		   	}
	
		if (element && !preventClip) {
			clip(element)
		};
	}
	
	function popup_mousepos(e){
	
		var ie = navigator.appName == "Microsoft Internet Explorer";

 		popup_mouseposX = ie ? window.event.clientX : e.clientX;
  		popup_mouseposY = ie ? window.event.clientY : e.clientY;
  		
	}
	function refresh(){}
	function clip(el){
		var html = '<iframe class="bgiframe"frameborder="0"tabindex="-1"src="javascript:false;"style="display:block;position:absolute;z-index:-1;filter:Alpha(Opacity=\'0\');top:0;left:0;bottom:0;right:0;width:100%;height:100%;"/>';
		var bgiframe = document.createElement(html);
    	el.insertBefore(bgiframe, el.firstChild);
	}	
	
	if (navigator.appName == "Microsoft Internet Explorer")
					    		 document.attachEvent('onmousedown', popup_mousepos);
	else document.addEventListener('mousedown', popup_mousepos, false);
					
	if (navigator.appName == "Microsoft Internet Explorer")
					    		 document.attachEvent('onmousemove',popup_mousemove);
	else document.addEventListener('mousemove',popup_mousemove, false);
					
	if (navigator.appName == "Microsoft Internet Explorer")
					   		  document.attachEvent('onmouseup', popup_mouseup);
	else document.addEventListener('mouseup', popup_mouseup, false);
}
















