
1) To enable ...

	Open /layout/tmpl-open.jspf and uncomment __ <c:set var="log" value="on" scope="request"/>
		- or -
	Add __ request.setAttribute("log", "on") __ to any page display action you desire
	
	**** JUST DON'T CHECK EITHER IN ****

2) To use ...
	
	Add any of the following statements throughout JavaScript code
	
	**** NOTE **** 
	It is not necessary to comment any of these statements when logger is not enabled.
		__ util.js provides return false implementations for each method
	
	log.debug();
		//Scatter debug messages to provide a high-level overview of
	  		what your code is doing, such as which function is currently in scope and the
	  		values of loop counters
		
	log.info();
		//Information messages are the meat and potatoes of logging messages; 
				sprinkle them around to reveal more detailed information
	  		about your script\'s execution, such as the values of variables and function/method
	  		return values.
	  
	log.warn();
		//Warning messages are used to indicate potentially hazardous situations, such as
	  		missing function arguments.
	
	log.error();
		//While error messages are used to indicate that something bad is about to happen; 
				note that these kinds of errors are considered to be run-time errors, 
				which are a different type of beast from the fatal errors generated automatically.
				
	**** NOTE **** 
	When trying to debug events executed on page load use the following to ensure the messages
		get logged -
		this is necessary since the console itself doesn't get added until the page loads

	LogOnLoad(function() {
     log.info();
  });
    
