/*
	fvlogger v1.0 (c) 2005 davidfmiller http://www.fivevoltlogic.com/code/fvlogger/
	see readme.txt for documentation
*/
var Log = Class.create();
Log.prototype = {
    initialize: function() { 
        this.ready = false;
        // version number
        this.FVLOGGER_VERSION = '1.0';
        // turn logging on or off;
        this.FVL_LOG_ON = true;
        // all logging statements that whose level is greater than or equal to FVL_DEFAULT_LOG_LEVEL will be processed;
        // all others will be ignored
        this.FVL_DEFAULT_LOG_LEVEL = this.FVL_DEBUG;
        // the id of the node that will have the logging statements appended to it
        this.FVL_LOG_ID = 'log_';
        // the element that should be wrapped around the log messages
        this.FVL_LOG_ELEMENT = 'p';
        // constants for logging levels
        this.FVL_DEBUG = 0;
        this.FVL_INFO = 1;
        this.FVL_WARN = 2;
        this.FVL_ERROR = 3;
        this.FVL_FATAL = 4;
        // the css classes that will be applied to the logging elements
        this.FVL_LOG_CLASSES = new Array("debug","info","warn","error","fatal");  
        // only override the window's error handler if we logging is turned on
        if (this.FVL_LOG_ON) {
        	window.onerror = this.windowError;
        }
    },
    // retrieves the element whose id is equal to FVL_LOG_ID
    getLogger: function(id) {
	   if (arguments.length == 0) { id = this.FVL_LOG_ID; }
	   return document.getElementById(id);
	},
    showDebug: function() { this.FVL_showMessages(this.FVL_DEBUG); },
    showInfo: function() { this.FVL_showMessages(this.FVL_INFO); },
    showWarn: function() { this.FVL_showMessages(this.FVL_WARN); },
    showError: function() { this.FVL_showMessages(this.FVL_ERROR); },
    showFatal: function() { this.FVL_showMessages(this.FVL_FATAL); },
    showAll: function() { this.FVL_showMessages(); },
    // removes all logging information from the logging element    
    eraseLog: function(ask) {
    	var debug = this.getLogger();
    	if (!debug) { return false; }
     	if (ask && ! confirm("Are you sure you wish to erase the log?")) {
    		return false;
    	}
    	var ps = debug.getElementsByTagName(this.FVL_LOG_ELEMENT);
    	var length = ps.length;
    	for (var i = 0; i < length; i++) { debug.removeChild(ps[length - i - 1]); }
    	return true;	   
	},
    getLogger: function(id) {
	   if (arguments.length == 0) { id = this.FVL_LOG_ID; }
	   return document.getElementById(id);
	},
    debug: function(message) { this.FVL_log("" + message, this.FVL_DEBUG); },
    warn: function(message) { this.FVL_log("" + message, this.FVL_WARN); },
    info: function(message) { this.FVL_log("" + message, this.FVL_INFO); },
    error: function(message) { this.FVL_log("" + message, this.FVL_ERROR); },
    //fatal: function(message) { this.FVL_log("" + message, this.FVL_FATAL); },
    windowError: function(message, url, line) {
	   FVL_log('Error on line ' + line + ' of document ' + url + ': ' + message, this.FVL_FATAL);
	   return true; //
	},
    FVL_showMessages: function(level, hideOthers) {
    	var showAll = false;
    	// if no level has been specified, use the default
    	if (arguments.length == 0) { level = this.FVL_DEFAULT_LOG_LEVEL; showAll = true; }
    	if (arguments.length < 2) { hideOthers = true; }
    	// retrieve the element and current statements
    	var debug = this.getLogger();
    	if (!debug) { return false; }
    	var ps = debug.getElementsByTagName("p");
    	if (ps.length == 0) { return true; }
    	// get the number of nodes in the list
    	var l = ps.length; 
    	// get the class name for the specified level
    	var lookup = this.FVL_LOG_CLASSES[level];
    	// loop through all logging statements/<p> elements...
    	for (var i = l - 1; i >= 0; i--) {
       		// hide all elements by default, if specified
    		if (hideOthers) { this._hide(ps[i]); } 
    		// get the class name for this <p>
    		var c = this.getNodeClass(ps[i]);
    	//alert(c);
  		//alert("Node #" + i + "'s class is:" + c);
    		if (c && c.indexOf(lookup) > -1 || showAll) { this._show(ps[i]); } 
    	}
	},
    FVL_log: function(message, level) {
    	// check to make sure logging is turned on
    	if (!this.FVL_LOG_ON) { return false; } 
    	// retrieve the infrastructure
    	if (arguments.length == 1) { level = this.FVL_INFO;}
    	if (level < this.FVL_DEFAULT_LOG_LEVEL) { return false; }
    	var div = this.getLogger();
    	if (!div) { return false; }
    	// append the statement
    	var p = document.createElement(this.FVL_LOG_ELEMENT);
    	// this is a hack work around a bug in ie
    	if (p.getAttributeNode("class")) {
    		for (var i = 0; i < p.attributes.length; i++) {
    			if (p.attributes[i].name.toUpperCase() == 'CLASS') {
    				p.attributes[i].value = this.FVL_LOG_CLASSES[level];
    			}
    		}
    	} else {
    		p.setAttribute("class", this.FVL_LOG_CLASSES[level]);
    	}
    	var text = document.createTextNode(message);
    	p.appendChild(text);
    	div.appendChild(p);
    	return true;
	},	
    exampleLogs: function() {
    	// describe the four types of logging messages
    	this.debug('Scatter debug messages throughout your code to provide a high-level overview of what your code is doing, such as which function is currently in scope and the values of loop counters.');
    	this.info('Information messages are the meat and potatoes of logging messages; sprinkle them around to reveal more detailed information about your script\'s execution, such as the values of variables and function/method return values.');
    	this.warn('Warning messages are used to indicate potentially hazardous situations, such as missing function arguments...');
    	this.error('While error messages are used to indicate that something bad is about to happen; note that these kinds of errors are considered to be run-time errors, which are a different type of beast from the parse errors mentioned below.');
    
        // generate an error to demonstrate the fatal error in ie and mozilla browsers
    	a 
	},	
	// show a node
    _show: function(target) {
   	    target.style.display = '';
        return true; 	
	},
	// hide a node
    _hide: function(target) {
        target.style.display = 'none';
        return true;	
	},	
    // returns the class attribute of a node
    getNodeClass: function(obj) {
    	var result = false;
    	if (obj.getAttributeNode("class")) {
    		result = obj.attributes.getNamedItem("class").value;
    	}
    	return result;	
	},
	html: function() {
        var p = _make('div', { 'id': this.FVL_LOG_ID });
        var c = _make('dl', [_make('dt', 'logger : toggle w/ F12')]);
        ['All','Debug','Info','Warn','Error','Fatal'].each(function(s) {
	        var _p = _make('dd');
	        _p.className = s.toLowerCase();
	        var _c = _make('a', { 'href':'#logger', 'title':'Show ' + s }, s)
            _c.className = s.toLowerCase();

            var _meth;
            switch (s) {
                case 'All': _meth = function() { log.showAll(); };
                    break;
                case 'Debug': _meth = function() { log.showDebug(); };
                    break; 
                case 'Info': _meth = function() { log.showInfo(); };
                break;        
                case 'Warn': _meth = function() { log.showWarn(); };
                break;         
                case 'Error': _meth = function() { log.showError(); };
                break;
                case 'Fatal': _meth = function() { log.showFatal(); };
                break;                 
            }
            _c.onclick = _meth;
            _p.appendChild(_c);
            c.appendChild(_p);
        });
        //add erase link
        var _erase = _make('dd', [_make('a', { 'href':'#logger',  'title':'erase' }, 'erase')]);
        _erase.onclick = function() { log.eraseLog(true); };
        c.appendChild(_erase);
        p.appendChild(c);
        return p; 	
	}
}

//helper
function _make(tagname, attributes, children) {
    // Example: make("p", ["This is a ", make("b", "bold"), " word."]);
    // If we were invoked with two arguments the attributes argument is
    // an array or string, it should really be the children arguments.
    if (arguments.length == 2 && 
        (attributes instanceof Array || typeof attributes == "string")) {
        children = attributes;
        attributes = null;
    }
    var e = document.createElement(tagname);
    // Set attributes
    if (attributes) {
        for(var name in attributes) e.setAttribute(name, attributes[name]);
    }
    // Add children, if any were specified.
    if (children != null) {
        if (children instanceof Array) {  // If it really is an array
            for(var i = 0; i < children.length; i++) { // Loop through kids
                var child = children[i];
                if (typeof child == "string")          // Handle text nodes
                    child = document.createTextNode(child);
                e.appendChild(child);  // Assume anything else is a Node
            }
        }
        else if (typeof children == "string") // Handle single text child
            e.appendChild(document.createTextNode(children));
        else e.appendChild(children);         // Handle any other single child
    }
      // Finally, return the element.
    return e;
}	

var log = new Log();

function LogOnLoad(f) {
    if($(log.FVL_LOG_ID) != null) { f(); };
    else if(log.ready == false) { setTimeout('LogOnLoad(' + f + ')',100); }
}

Event.observe(window, 'load', function() {
    document.body.appendChild(log.html());
    log.ready = true;
    //log.exampleLogs();
});

Event.observe(document, 'keypress', function(event) {
    if($(log.FVL_LOG_ID)) {
       if(event.keyCode == 123) {
            $(log.FVL_LOG_ID).toggle();
        }
    }
});   
