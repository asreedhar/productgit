
function showResponse(originalRequest) {
	alert(originalRequest.responseText);
}

//AJAX response handlers
var globalHandlers = {
	onCreate: function(){
		$('systemWorking').innerHTML = 'Loading...'
	},
	onComplete: function() {
		if(Ajax.activeRequestCount == 0){
		$('systemWorking').innerHTML = ''
		}
	},
	onFailure: showResponse	
};
Ajax.Responders.register(globalHandlers);
