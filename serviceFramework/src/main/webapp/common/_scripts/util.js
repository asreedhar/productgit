


var Format = {
	initialize:function(){	},
	currency:function(value){
		value = value.toString();
		var retString =new Array();
		var commaCount = 0;
		for(var i=  value.length - 1; i >=0; i--){		
			commaCount++;
			retString.push(value.charAt(i));			
			if(commaCount == 3 && i != 0){			
				retString.push(',');
				commaCount = 0;			
			}
			if(i ==0){
				retString.push('$')
			}	
			
		}
		retString = retString.reverse();
		return retString.join('');
	}
}




var Util = {
	name: 'Util',
// METHODS
	initialize: function(){
		
	
	},
//EVENT METHODS	
	enterPressed: function(e) {	
		return (e.keyCode == Event.KEY_RETURN);	
	},
//DOM METHODS
	closeWindow: function(){
		window.close();

	},
	printPage:function(){
		window.print();
	},
	


toggle: function(/*target object*/el, /*opt. string [hide|show]*/how) {
	if(how) {
		var isHidden = Element.hasClassName(el,'hide');
		if(how == 'hide') {
			if(isHidden) {
				return false;
			}
		}		
		if(how == 'show') {
			if(!isHidden) {
				return false;
			}
		}
	}
	Element.toggleClassName(el,'hide')
},

removeAllClassNames: function(/*targets array*/els, /*string */cla) {
	if(!cla) return;
	for(var i = 0; i < els.length; i++) {
		var _el = els[i];
		Element.removeClassName(_el,cla);
	}
},
setAllClassNames: function(/*targets array*/els, /*string */cla) {
	if(!cla) return;
	for(var i = 0; i < els.length; i++) {
		var _el = els[i];
		Element.addClassName(_el,cla);
	}
},


toggleAll: function(/*targets array*/els, /*opt. string [hide|show]*/how) {
//alert('running toggleAll() '+els.length)

	for(var i = 0; i < els.length; i++) {
		var _el = els[i];
		this.toggle(_el,how);
	}
},


	
	
	toggleByRel:function(trigger){
		var _e = this.name + ': requires <A> with "rel" attribute'
			_e += '\n? "rel=" is a single string or comma separated list of element ids'
		var tars = trigger.rel;
		if(!trigger.rel) {
			alert(_e);
			return false;
		}	
		if(tars.search(',') == -1) {
			tars = [el.rel];
		} else {
			tars = tars.split(',');
		}
		toggleAll(tars);
		//for(var i=0; i<tars.length; i++) {
		//	this.toggleClassName(tars[i],'hide');
		//} 
		//return false;
	},

	
	toggleClassNames:function(obj,class1,class2) {
	/*
	// TEST : should do the same thing
		Element.toggleClassName(obj,class1);
		Element.toggleClassName(obj,class2);
	*/
		var has1 = Element.hasClassName(obj,class1);
		var has2 = Element.hasClassName(obj,class2);
		if(has1) {
			Element.removeClassName(obj,class1);
			Element.addClassName(obj,class2);
		}
		if(has2) {
			Element.removeClassName(obj,class2);
			Element.addClassName(obj,class1);
		}
	},
	getValue: function(ele){ //null save get value
			if($(ele)) {
				return $(ele).value;
			}
			return '';
	},
// BOOLEAN METHODS
	isIE:function(){
		return navigator.appName.indexOf('Microsoft Internet Explorer') != -1;
	},
	isArray:function(obj){
 		 return this.isObject(obj) && obj.constructor == Array;
	},
	isFunction:function(a) {
  		  return typeof a == 'function';
	},
	isObject:function(a) {
   		 return (a && typeof a == 'object') || this.isFunction(a);
	},
//OBJECT METHODS
	getPropertyNames: function(o) {
		var r = [];
		for(name in o) r.push(name);
		return r;
	},
	copyProperties: function(/*obj*/ from, /*opt obj*/ to) {
		if(!to) to = {};
		for(p in from) to[p] = from[p];
		return to;
	},
	copyUndefinedProperties: function(/*obj*/ from, /*opt obj*/ to) {
		for(p in from) {
			if(!p in to) to[p] = from[p];			
			to[p] = from[p];
		}
	},
//ARRAY METHODS
	filterArray: function(/*array*/ a, /*bool function*/ predicate) {
		var len = a.length;
		var r = [];
		for(var i = 0; i < len; i++) {
			var _el = a[i];
			if(predicate(_el)) r.push(_el)
		}
		return r;
	},
	mapArray: function(/*array*/ a, /*function*/ f) {
		var r = [];
		var len = a.length;
		for(var i = o; i < len; i++) r[i] = f(a[i]);
		return r;
	},
//STRING METHODS
	stripTags:function(str) {
		var s = 0;
		var e = -1;
		var r = "";
	
		s = str.indexOf("<",e);	
	
		do {
			r += str.substring(e + 1,s);
			e = str.indexOf(">",s);
			s = str.indexOf("<",e);
		}
		while ((s != -1) && (e != -1))
	
		r += str.substring(e + 1,str.length);
	
		return r;
	},
//NUMBER METHODS
	/* removes non-numeric chars : returns a number */
	 formatPrice:function(/* string */ price) {
/*
// TEST
	 	if(typeof price == 'number') return price;
*/	 
		var indexOfPeriod = price.indexOf(".");
		if (indexOfPeriod >= 0)
		{
			price = price.substring(0, indexOfPeriod);
		}
		var indexOfComma = price.indexOf(",");
		if (indexOfComma >= 0)
		{
			price = price.substring(0, indexOfComma) + price.substring( (indexOfComma + 1), price.length );
		}
		var indexOfDollar = price.indexOf("$");
		if (indexOfDollar >= 0)
		{
			price = price.substring( (indexOfDollar + 1), price.length );
		}
		return price;
	},
	
	
/// UI UI UI UI UI METHODS
	/*
	Method to scroll and center an element on screen
	
	*/
	scrollWindowTo:function(id){
		var coords = this.findPosition($(id));  //find coordinates of the element
		var winHeight =  this.getViewportDims();  //get height of browser window
		var eleHeight = $(id).offsetHeight;
		
		if(!eleHeight){		
			eleHeight = 0;
		}		
		var yLoc = coords[1] - ((winHeight[1]/2) -  (eleHeight/2));  //y location of element =element  position - .5 window height - .5 element height
		
		window.scrollTo(0,yLoc);   //scroll window to this location
	},
	
	/*
	Method accepts an element as a parameter and returns an array representing
	the x and y coordinates of the element
	*/
	findPosition:function(oElem) {
		if ($(oElem).offsetParent) {	
			for (var posX = 0, posY = 0; $(oElem).offsetParent; oElem = $(oElem).offsetParent) {
				posX += $(oElem).offsetLeft;
				posY += $(oElem).offsetTop;
	    	}
			return [posX, posY];
		} 
		else {
			return [$(oElem).x, $(oElem).y];
	  	}
	},
	/*
	Method a returns an array representing
	the x and y coordinates of the screen
	*/
	findScreenSize:function() {
		var x; var y;
		var x = window.screen.width;
		var y = window.screen.height;
		return [x, y];	
	},
	/*
	Method a returns an array representing
	the x and y coordinates of the viewport - chrome
	*/
	findViewportDims:function() {
		var x; var y;
			// non-explorer
		if (self.innerHeight) {
			x = self.innerWidth;
			y = self.innerHeight;
		}
			// explorer 6 strict
		else if (document.documentElement && document.documentElement.clientHeight)	{
			x = document.documentElement.clientWidth;
			y = document.documentElement.clientHeight;
		}
			// other explorers
		else if (document.body) {
			x = document.body.clientWidth;
			y = document.body.clientHeight;
		}
		//viewX = x;
		//viewY = y;
		return [x, y];
	},


/* Accepts a date format and date string as parameters, defaults to today
Format should be a string consisting of the following characters
Other characters will be displayed in place.
 d: 2 digit day of month
 j: 1 digit day of month
M: abbreviated month (Oct, Nov)
F: full month
m: 2 digit month
n: 1 digit month
D: abbreviated day (Sun, Mon)
l (lower case L): full day name
S: day suffix (st,th,rd,nd)
Y: 4 digit year
y: 2 digit year
A: uppercase am/pm
a: lower case am/pm
g: 12 hour hours without leading 0
G: 24 hour hours without leading 0
h: 12 hour hours with leading 0 (2 digit)
H: 24 hour hours with leading 0 (2 digit)
i: minutes 2 digit
s: seconds 2 digit
*/
getFormattedDate:function(fmt, dte){

	if(!fmt){
		alert('A date format is required');
		return;
	}
	var d = new Date();
	
	if(dte){
	
		d.setTime(Date.parse(dte));
	}
	var month = d.getMonth() +1;
	var date = d.getDate();
	var year = d.getFullYear();
	var shortYear = d.getYear();
	var hour = d.getHours();
	var minutes = d.getMinutes();
	var day = d.getDay();
	var seconds = d.getSeconds();
	var retStr = '';
	
	for(var index = 0; index < fmt.length; index++){
	
		switch( fmt.charAt(index)){
			case  'd': //2 digit day of month
				var dateStr = date < 10?'0'+date:date;
				retStr += dateStr;
			break;
			case 'j': //1 digit day of month
				var dateStr = date;
				retStr += dateStr;	
			break;
			case 'M': //abbreviated month
				var monthStr = '';
					switch(month){	
						case 1:
							monthStr = 'Jan';
						break;
						case 2:
							monthStr = 'Feb';
						break;
						case 3:
							monthStr = 'Mar';
						break;
						case 4:
							monthStr = 'Apr';
						break;
						case 5:
							monthStr = 'May';
						break;
						case 6:
							monthStr = 'Jun';
						break;
						case 7:
							monthStr = 'Jul';
						break;
						case 8:
							monthStr = 'Aug';
						break;
						case 9:
							monthStr = 'Sep';
						break;
						case 10:
							monthStr = 'Oct';
						break;
						case 11:
							monthStr = 'Nov';
						break;
						default:
							monthStr = 'Dec';
						break;	
					}
					retStr += monthStr;	
			break;
			case 'F': //full month
				var monthStr = '';
				
				switch(month){	
						case 1:
							monthStr = 'January';
						break;
						case 2:
							monthStr = 'February';
						break;
						case 3:
							monthStr = 'March';
						break;
						case 4:
							monthStr = 'April';
						break;
						case 5:
							monthStr = 'May';
						break;
						case 6:
							monthStr = 'June';
						break;
						case 7:
							monthStr = 'July';
						break;
						case 8:
							monthStr = 'August';
						break;
						case 9:
							monthStr = 'September';
						break;
						case 10:
							monthStr = 'October';
						break;
						case 11:
							monthStr = 'November';
						break;
						default:
							monthStr = 'December';
						break;	
				
				}
			retStr += monthStr;	
			break;
			case 'm': //2 digit  month
				var monthStr = month < 10?'0'+month:month;
				retStr += monthStr;
			break;
			case 'n': //1 digit month
				var monthStr = month;
				retStr +=monthStr;
			break;
			case 'D': //3 letter day abbreviation
				var dayStr = '';
				
				switch(day){
				
					case 0:
						dayStr = 'Sun';
					break;
					case 1:
						dayStr = 'Mon';
					break;
					case 2:
						dayStr = 'Tue';
					break;
					case 3:
						dayStr = 'Wed';
					break;
					case 4:
						dayStr = 'Thu';
					break;
					case 5:
						dayStr = 'Fri';
					break;
					default:
						dayStr = 'Sat';
					break;
				
				}
				retStr+= dayStr;
			break;
			case 'l'://full day 'L' lowercase
				var dayStr = '';
				
				switch(day){
				
					case 0:
						dayStr = 'Sunday';
					break;
					case 1:
						dayStr = 'Monday';
					break;
					case 2:
						dayStr = 'Tuesday';
					break;
					case 3:
						dayStr = 'Wednesday';
					break;
					case 4:
						dayStr = 'Thursday';
					break;
					case 5:
						dayStr = 'Friday';
					break;
					default:
						dayStr = 'Saturday';
					break;
				
				}
				retStr += dayStr;
			break;
			
			case 'S': //day suffix
				var daySuffix = '';
				
				switch(date){
				
					case 1 :			
					case 21:			
					case 31:
							daySuffix = 'st';
					break;
					case 2 :			
					case 22:			
							daySuffix = 'nd';
					break;
					case 3 :			
					case 23:			
							daySuffix = 'rd';
					break;
					default:
						daySuffix = 'th';
					break;
				
				}
				retStr += daySuffix;
			break;
			case 'Y': //4 digit year
			
					retStr += year;
			
			break;
			case 'y'://2 digit year
			
					retStr += shortYear;
			
			break;
			case 'a'://lowercase ampm
				var ampm = 'am';
				if(hour >= 12){
					ampm = 'pm';
				}
			retStr += ampm;
			break;
			case 'A'://capital ampm
				var ampm = 'AM';
				if(hour >= 12){
					ampm = 'PM';
				}
				retStr += ampm;
			break;
			case 'g': //12 hour hours without leading 0
				var hourStr = '';
				if( hour > 12){
					hourStr = hour - 12;
				}else{
					hourStr = hour;
				}
				retStr += hourStr;
			break;
			case 'G'://24 hour hours without leading 0
				var hourStr = '';
				
				hourStr = hour;		
				
				retStr += hourStr;
			break;
			case 'h': //12 hour hours with leading 0
				var hourStr = '';
				if( hour > 12){
					hourStr = hour - 12;
				}else{
					hourStr = hour;
				}
				if(hour < 10){
					hourStr = '0'+hourStr;
				}
				retStr += hourStr;
			break;
			case 'H'://24 hour hours with leading 0
				var hourStr = '';
				
				hourStr = hour;
				
				if(hour < 10){
					hourStr = '0'+hourStr;
				}
			retStr += hourStr;
			break;
			case 'i': //minutes with leading 0
				var minutesStr = minutes;
				if(minutes < 10){
					minutesStr = '0'+minutesStr;
				}
				retStr += minutesStr;
			break;
			
			case 's':	//seconds with leading 0
				var secondStr = seconds;
				
				if(seconds < 10){
					secondStr = '0'+secondStr;
				}
			retStr += secondStr;
			break;
			default:
				retStr += fmt.charAt(index);
			break;
		} //end switch
	}
	
	return retStr;
	

},
	debug:function(msg){
	
		alert("Error: "+msg);
		
	},
	preventInput:function(){
	
			var dims = this.findViewportDims();
		
			 var  div = '<div id="preventInputScreen" onclick="return false;"  style="z-index:5001; position:absolute; top:0; left:0; width:'+dims[0]+';height:'+dims[1]+'"><img src="common/_images/d.gif" width="100%" height="100%"/></div>';
			if(!$('preventInputScreen')){
						new Insertion.After($('wrap'),div);
			}
	
	},
	enableInput:function(){
		Element.remove($('preventInputScreen'));
	}  
}


//AjaxLoader class
function AjaxLoader(msg, pth, params){

	var p;
	var div;
	var path = pth;
	var parameters = params;
	var element;
	var message = msg;
	var success;
	var failure;
	var insertion;
	var complete;
	var showAtElement;
	var working = false;
	var exception;
	var perform;
	
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.setParameters = setParameters;
	this.setElement = setElement;
	this.performAction = performAction;
	this.setPath = setPath;
	this.setOnSuccess =  setOnSuccess;
	this.setOnFailure =  setOnFailure;
	this.setMessage =  setMessage;
	this.setOnComplete = setOnComplete;
	this.setInsertion = setInsertion;
	this.setShowAt = setShowAt;
	this.getWorking = getWorking;
	this.reset = reset;
	this.setOnException = setOnException;
	this.setOnPerform = setOnPerform;
	
	function showWindow(){  //create loading window if does not exist, fill it with message, then show it.
	
	        div = '<div id="loadingDiv"  style="z-index:1050; border:1px solid black;background-color:white; padding:10px; width:50px; height:20px; position:absolute; display:none;text-align:center; font-weight:bold;"><img src="common/_images/icons/loading.gif"/><br/>'+message+'</div>';
			if(!$('loadingDiv')){
						new Insertion.After($('wrap'),div);
			}else{
				$('loadingDiv').innerHTML='<img src="common/_images/icons/loading.gif"/><br/>'+message;
			}
		 p = new Popup();
		 if(!showAtElement){
			  p.popup_show('loadingDiv', '', '', 'screen-center',100,false,0,0);
		 }else{
		 
			 p.popup_show('loadingDiv', '', '', 'element-bottom',100,false,0,10,showAtElement.id);
		}
	}
	function hideWindow(){
		p.popup_exit();
	}
	function getWorking(){
		return working;
	}
	function setParameters(params){
		parameters = params;
	}
	function reset(){
		p = null;
		div =null;
		path = null;
		parameters = '';
		element = '';
		message = null;
		success = null;
		failure = null;
		insertion = null;
		complete = null;
		showAtElement = null;
		working = false;
		exception = null;
	
	}
	function setShowAt(ele){

		showAtElement = $(ele);
	}
	function setPath(pth){
	
		path = pth;
	}
	function setElement(ele){
	
		element = ele;
	}

	function setMessage(msg){
	
		message = msg;
	}
	function setOnComplete(fun){
	
		complete = fun;
	}
	
	//set function to perform on success
	function setOnSuccess(fun){
	
	 	success = fun;

	}
	//set function to perform on failure
	function setOnFailure(fun){
	
	 	failure = fun;

	}
	//set function to perform on exception
	function setOnException(fun){
	
	 	exception = fun;

	}
	function setInsertion(ins){
	
		insertion = ins;
	}
	function setOnPerform(fun){
		perform = fun;
	}
	function performAction(){

		if(message != null && message != ''){
			this.showWindow();  //display "loading window"
		}
		
		
		if(path && !working){
			working = true; //request in progress
			if(perform) {
				perform();
			}
			
			if(insertion){
				if(!element){
					alert('Element must be set for ajax updater call.');
					if(message != null && message != '')
						hideWindow();
					return;
				}
			
				new Ajax.Updater(
					element,
					path,
					{
						method:'post',
						parameters: stampURL(parameters),
						insertion: insertion,
						onSuccess: function(r,j) {
												
							if(success){
								success(r,j);
							}
							
						},
						onComplete: function(r,j){
					
							if(message != null && message != ''){
								hideWindow();
							}
							
							if(complete){
								complete(r,j);
							}
							reset(); //reset the state of the object
							
						},
						onFailure: function(){
				
							if(failure){
								failure(r,j);
							}
							
						}
					}
				);
			
			}else{

				new Ajax.Request(
					path,
					{
						method: 'post',
						parameters: stampURL(parameters),
						onSuccess: function(r,j) {						
					
							if($(element)){
												
									$(element).innerHTML =r.responseText;
							}
							if(success){
								success(r,j);
							
							}
						
						},
						onComplete: function(r,j){
					
							if(message != null && message != ''){
								hideWindow();
							}
							if(complete){
								complete(r,j);
							}
							reset();
							//Util.enableInput();
						},
						onFailure: function(r,j){
						
							if(failure){
								failure(r,j);
							}
						},
						onException: function(r,e){
						
							if(exception){
								exception(r,e);
							}
						}
					}
				);
			}
		}else{
		
			if(!path){
				alert('Error:Ajax path has not been set.');
				hideWindow();
			}
		}
	}
} //end ajax loader class

var log = {
    html: function() { return false; },
    debug: function() { return false; },
    info: function() { return false; },
    warn: function() { return false; },
    error: function() { return false; },
    fatal: function() { return false; }
};
function LogOnLoad() { return false; }
    
