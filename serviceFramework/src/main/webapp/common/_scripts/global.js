// 
// POPUP scripts 
//for the pricing analyzer popup. prevents rollover from changing color to allow fade to complete

var ignoreOne = false; //all purpose popup
var lastSavedId = '';
function pop(sPath,sWin,mY, id) {
	var sAttr = 'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,';
	var sWinName = sWin;
		if (!sWin) {
			sWinName = 'popup';
		} 
	//modify popup size based on target
	switch (sWinName) {	
		case 'photos':
			sAttr += 'width=700,height=612';
			break;	
		//case 'profile':
		case 'plus':
			sPath += '&forecast=0&mode=UCBP&mileageFilter=1&isPopup=true';
			break;
		case 'deals':
			sPath +='&isPopup=true';
		case 'thirdparty':		
			sAttr += 'width=790,height=575';
			break;
		case 'mgmtCenter':
			sAttr += 'width=470,height=615';
			break;
		case 'estock':
			var aScreen = findScreenSize();
			sAttr += aScreen[1] >= 1024 ? 'width=900,height=870':'width=900,height=700';
			sPath += '&isPopup=true';
			break;
		case 'print':
			var aScreen = findScreenSize();
			sAttr += aScreen[1] >= 1024 ? 'width=900,height=870':'width=900,height=700';
			break;
		case 'promo':
			sAttr += 'width=350,height=250';
			break;
		case 'legal':
			sAttr += 'width=780,height=450';
			break;
		case 'lithia':
			sAttr == 'width=800,height=830';
			break;
		case 'price':		
			if(mY){ //get location of mouse click
				//set background of clicked element to fade
				if(id){
					var nme = '';
					for(var i = 1; i < 5; i++){
					  nme = 'flash'+i+'Cell'+id;			 	 
			 		 if($(nme)){
			 			 $(nme).className =  'fade';
			  		 }
					}
					ignoreOne = true;
				}
				var windowHeight = document.documentElement.clientHeight;
				var windowWidth = document.documentElement.clientWidth;
				var xLoc =  (windowWidth/2) - 410; //x location of window, half of screen, 
				var elementYLoc = mY + document.documentElement.scrollTop - (windowHeight/2); //scroll to center item on screen
				var yLoc = (windowHeight / 2) - 375; //move window so bottom is at top of item
										
				if(document.documentElement.scrollTop < 375){  //we are at top of page, move window below item so its not obscured
					yLoc += 375;      
					elementYLoc += 165;
				}
				if(yLoc < 0){  //smaller window case, need to move element further down to prevent obscure by window
					elementYLoc -= 375 - (windowHeight / 2);
				}	
				window.scrollTo(0,elementYLoc); //center the clicked element on screen				
				//if after centering we were not able to scroll all the way we are at the bottom of the screen
				if(document.documentElement.scrollTop < elementYLoc){  
					yLoc = yLoc + elementYLoc - document.documentElement.scrollTop; //move window to meet top of item
				}				
				sAttr += 'width=820,height=375,top='+yLoc+',left='+xLoc;  //create window params	
				lastSavedId = id;			
			}else{
				sAttr += 'width=820,height=375';  //modify this to make smaller			
			}
			var hiddenParams; //used for navigator page
				hiddenParams = document.getElementById('auctionDataParams');
			if(hiddenParams) {
				sPath += '&'+ hiddenParams.value;
			}	
			break;	
		case 'contact':
			sAttr += 'width=800,height=450';
			break;	
		case 'gmSmartAuction':
			sAttr = 'location=1,status=1,toolbar=1,width=800,height=600,menubar=1,resizable=1,scrollbars=1';
			break;
		default:
			sAttr += 'width=800,height=600';
	}

	 w = window.open(sPath, sWinName, sAttr);
	
}

function closeWindow(pageId,pageType) {
	if(pageId=='ingroupVehicle'&&pageType=='iframe') { 
		window.location = '/NextGen/NavigatorDefaultFrame.go';
	}
	else { 
		window.close(); 
	}
}




//
// ***** utility functions *************************
//

function stampURL(srchStr) {
	return (srchStr + '&now='+(new Date()).getTime());
}

function removeQ(str) {
	var modStr = str;
	if(str.slice(0,1) == '?') {
		modStr = str.slice(1,str.length);
	}
	return modStr;
}
function checkSlash(str) {
	var modStr = str;
	if(str.slice(0,1) != '/') {
		modStr = '/' + modStr;
	}
	return modStr;
}



function checkChars(){
	
	 var key = event.keyCode;
	
	 if((key < 48 || key > 57) && (key != 46 && key!=8)){ //only nueric values allowed
	 	return false;
	 }
	return true;
}

function checkRange(low, high, val){

	if( val <  low  || val > high ){
		return false;
	}
	return true;
}

//
//show & hide element (for element w/ different open and close links)
//
function show(sElemId) {
	// placement of onclicked link
    var oTriggerLink = document.getElementById(sElemId + 'Link');
	var aTriggerPos = findPosition(oTriggerLink); //returns x=[0] and y=[1]
	var nTriggerX = aTriggerPos[0]; var nTriggerY = aTriggerPos[1];
	
	// placement of element to show
	var oShowElem = document.getElementById(sElemId);
	var aShowElemPos = findPosition(oShowElem);
	var nShowElemX = aShowElemPos[0]; var nShowElemY = aShowElemPos[1];

	// modify properties
	oShowElem.style.position = 'absolute';
	oShowElem.style.zIndex = '300';
	oShowElem.style.top = (nTriggerY - nShowElemY) + 20 + 'px';
	oShowElem.style.left = (nTriggerX - nShowElemX) + 20 + 'px';
    oShowElem.style.display = 'block';
}

function hide(sElemId) {
	var oHideElem = document.getElementById(sElemId);
	oHideElem.style.position = 'static';
	oHideElem.style.display = 'none';
}

//
//toggle element (for elements w/ same open and close link)
//


function toggle(sElemId) {
	var oToggleElem = document.getElementById(sElemId);
	var oToggleElemVis = oToggleElem.style.display;
	oToggleElem.style.display = oToggleElemVis != 'none' ? 'none':'';
	if(document.getElementById(sElemId+'Link')) {
		swapLinkText(sElemId+'Link');
	}
}
function swapLinkText(sElemId) {
	var oLinkElem = document.getElementById(sElemId);
	var length;
	for (var i=0; oLinkElem.children[i]; i++) {
		var isVis = oLinkElem.children[i].style.display != 'none';
		oLinkElem.children[i].style.display = isVis ? 'none':'';
		length = i+1;
   	}
   	if(length==1) oLinkElem.children[0].style.display = '';
}

//
//opens system print dialog
//
function printPage() {
	window.print();  
}






//
// ***** computational functions *************************
//


//find absolute position of an element
function findPosition(oElem) {
	if (oElem.offsetParent) {	
		for (var posX = 0, posY = 0; oElem.offsetParent; oElem = oElem.offsetParent) {
			posX += oElem.offsetLeft;
			posY += oElem.offsetTop;
    	}
		return [posX, posY];
	} 
	else {
		return [oElem.x, oElem.y];
  	}
}
function findScreenSize() {
	var x; var y;
	var x = window.screen.width;
	var y = window.screen.height;
	return [x, y];	
}

//find dimensions of the viewable browser area minus chrome
function findViewportDims() {
	var x; var y;
		// non-explorer
	if (self.innerHeight) {
		x = self.innerWidth;
		y = self.innerHeight;
	}
		// explorer 6 strict
	else if (document.documentElement && document.documentElement.clientHeight)	{
		x = document.documentElement.clientWidth;
		y = document.documentElement.clientHeight;
	}
		// other explorers
	else if (document.body) {
		x = document.body.clientWidth;
		y = document.body.clientHeight;
	}
	//viewX = x;
	//viewY = y;
	return [x, y];
}

function showStockPopup(year, makeModelId, includeLowUnitCost, insert,offset){
		var params = '';
		var loc = -350;
		if(year != null ){
			params+='&year='+year;		
			year=makeModelId+''+year;
		
		}else{
			year='All'+makeModelId;
		}
		
		if(offset){
			loc = parseInt(offset);
		}
		params+='&modelGroupId='+makeModelId;
		params+='&includeLowUnitCost='+includeLowUnitCost;
		var s = function showOverview(x){
			if($('overviewFloat')){
				Element.remove('overviewFloat');
			}		
			var p = new Popup();
			new Insertion.Bottom(insert,x.responseText);
			p.noClip(true);
			p.popup_show('overviewFloat','overviewHead','','element-bottom',100,false,-350,20,$('stock'+year).id);
		
		}	
		var map = {
		path:        'OptimalInventoryOverview.go',
		msg:          'Loading...',
		options:      params,
		onSuccess: s,
		onFailure : s
		
		};
	return makeRequest(map);
}

function showOtherStockPopup(segmentId, strategyId, insert,offset){
		var params = '';
		var loc = -330;
		
		if(offset){
			loc = parseInt(offset);
		}	
		params+='&strategyId='+strategyId+'&segmentId='+segmentId;		
		var s = function showOverview(x){
			if($('overviewFloat')){
				Element.remove('overviewFloat');
			}		
			var p = new Popup();
			new Insertion.Bottom(insert,x.responseText);
			p.noClip(true);
			p.popup_show('overviewFloat','overviewHead','','element-bottom',100,false,-350,20,$('stockOther'+segmentId).id);
		
		}	
		var map = {
		path:        'StockingReportsOtherOverview.go',
		msg:          'Loading...',
		options:      params,
		onSuccess: s,
		onFailure : s
		
		};
	return makeRequest(map);
}
