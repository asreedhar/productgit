<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- need to find a way to do atomic calls to the action --%>
<%-- the columns on the right side gets messed up due to request manipulation --%>
<c:forEach items="${reportTypes}" var="t" varStatus="loopStatus">
	<c:set var="forcePageBreak" value="false" />
	<c:set var="numReports" value="${numReports}" />
	<c:if test="${not loopStatus.last}">
		<c:set var="forcePageBreak" value="true" />
	</c:if>

	<%-- good for debugging
	multipage: forcePageBreak? : ${forcePageBreak}
	
	<c:url value="/ActionPlansMultipleReportSubmit.tx" var="multiPlans">
		<c:param name="reportType" value="${t}" />
		<c:param name="daysBack" value="${daysBack}" />
		<c:param name="timePeriodMode" value="${timePeriodMode}" />
		<c:param name="forcePageBreak" value="${forcePageBreak}" />
	</c:url>
	
	url:${multiPlans}
	
	<br />
	--%>
	
	<c:import url="/ActionPlansMultipleReportSubmit.tx">
		<c:param name="reportType" value="${t.name}" />
		<c:param name="daysBack" value="${daysBack}" />
		<c:param name="timePeriodMode" value="${timePeriodMode}" />
		<c:param name="forcePageBreak" value="${forcePageBreak}" />
		
		<c:param name="thirdPartyId" value="${t.thirdPartyId != null ? t.thirdPartyId:''}"/>
		<c:param name="thirdParty" value="${t.thirdParty != null ? t.thirdParty:''}"/>
		<c:param name="endDate" value="${t.endDate != null ? t.endDate:''}"/>
	</c:import>

</c:forEach>