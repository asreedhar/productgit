<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div id="invInfo">
	<h5><fmt:message key="invInfo.header" var="_header"/>${fn:toUpperCase(_header)}</h5>
	<ol>
		<li><strong>${overallInventoryInformationBean['UnitsInStock']}</strong> Vehicles</li>
		<li><strong>${overallInventoryInformationBean['DistinctModelsInStock']}</strong> Models</li>
		<li><strong>${overallInventoryInformationBean['DaysSupply']}</strong> Days Supply</li>
		<li><strong><fmt:formatNumber type="currency" value="${overallInventoryInformationBean['TotalInventoryDollars']}" currencySymbol="$" maxFractionDigits="0"/></strong> of Inventory</li>
	</ol>
</div>