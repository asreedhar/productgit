<script type="text/javascript">
	document.title="Inventory Management Action Plans";
</script>
<c:import url="/common/googleAnalytics.jsp" />
<c:if test="${sessionScope.nextGenSession.includeAgingPlan}">
	<a href="<c:url value="/${param.enabled eq 'advertise' ? 'QuickAd':''}InventoryPlan.go"><c:param name="rangeId" value="${param.rangeId}"/><c:param name="enabled" value="${param.enabled}"/></c:url>" title="<fmt:message key="plan.pageTitle"/>"><img src="<c:url value="/common/_images/buttons/inventoryMgmtPlanOnTrans.gif"/>" class="impBtn"/></a>
</c:if>
<form action="ActionPlansReportSubmit.tx" onsubmit="javascript:printReport('selected', 'print'); return false;">
<div class="wrapper select">
	Run reports for:
	<fieldset>
	<ul>
		<li><input type="radio" id="timePeriodCurrent" name="timePeriodMode" value="CURRENT" checked="checked"/><label for="timePeriodCurrent">All Current Plans</label></li>
		<li><input type="radio" id="timePeriodDays" name="timePeriodMode" value="HISTORY"/><label for="timePeriodDays">All Plans created in the last <input type="text" name="daysBack" maxlength="2" id="daysBack" value="1" class="number" title="Enter a numeric day value" onkeypress="return checkChars();"  onfocus="$('timePeriodDays').checked = true;this.select();"/> days</label></li>
	</ul>
	</fieldset>
</div>

<div class="reports">
	<h5>SELECT ACTION PLANS / REPORTS TO REVIEW</h5>
	<div class="wrapper">
	<div class="f">
		<fieldset>
			<legend><fmt:message key="actionPlans.retailPlans"/></legend>
			<ul>
			<c:forEach items="${lRetailPlans}" var="v">
				<li><input type="checkbox" name="${v}" id="${v}"/> <label for="${v}"><a href="javascript:printReport('${v}','preview');"><fmt:message key="actionPlans.${v}Plan"/></a></label></li>
			</c:forEach>
			</ul>
		</fieldset>
		<fieldset>
			<legend><fmt:message key="actionPlans.printAdvertising"/></legend>
			<ul>
			<c:forEach items="${lPrintAdvertising}" var="v">
				<li><input type="checkbox" name="${v.id}" id="${v.id}"/> <label for="${v.name}"><a href="javascript:printReport('${v.id}','preview' );">${v.name}</a></label></li>
			</c:forEach>
			<li><input type="checkbox" name="0" id="0"/> <label for="printAdvertiserOther"><a href="javascript:printReport('0','preview');"><fmt:message key="actionPlans.msg.advertisingOtherCaption"/></a></label></li>
			<li><input type="checkbox" name="${fullAdvertising}" id="${fullAdvertising}"/> <label for=${fullAdvertising}><a href="javascript:printReport('${fullAdvertising}','preview');"><fmt:message key="actionPlans.msg.fullAdvertisingCaption"/></a></label></li>
			<li><input type="checkbox" name="${internetAdvertisement}" id="${internetAdvertisement}"/> <label for=${internetAdvertisement}><a href="javascript:printReport('${internetAdvertisement}','preview');"><fmt:message key="actionPlans.msg.internetAdvertisementCaption"/></a></label></li>
			</ul>
		</fieldset>
		<fieldset class="l">
			<legend><fmt:message key="actionPlans.retailReports"/></legend>
			<ul>
			<c:forEach items="${lRetailReports}" var="v">
				<li><input type="checkbox" name="${v}" id="${v}"/> <label for="${v}"><a href="javascript:printReport('${v}','preview');"><fmt:message key="actionPlans.${v=='certified'?'certifiedPlan':v}"/></a></label></li>
			</c:forEach>
			</ul>
		</fieldset>
	</div>	
	<div class="l">	
		<fieldset>
			<legend><fmt:message key="actionPlans.wholesalePlans"/></legend>
			<ul>
				<li><input type="checkbox" name="${auction}" id="${auction}"/> <label for="${auction}"><a href="javascript:printReport('${auction}','preview');"><fmt:message key="actionPlans.${auction}Plan"/></a></label></li>
				<c:forEach items="${lAuctions}" var="a">
					<li style="padding-left: 14px;"><input type="checkbox" value="${a.thirdPartyId}_${a.thirdParty}_${a.endDate}" name="individualAuction" id="${a.thirdParty}"/><label for="${a.thirdParty}"><a href="javascript:printReport('individualAuction','preview', '${a.thirdPartyId}','${a.endDate}');">${a.thirdParty} (${a.endDate})</a>
				</c:forEach>
			</ul>
			<hr/>
			<ul>
				<li><input type="checkbox" name="${wholesale}" id="${wholesale}"/> <label for="${wholesale}"><a href="javascript:printReport('${wholesale}','preview');"><fmt:message key="actionPlans.${wholesale}Plan"/></a></label></li>
				<c:forEach items="${lWholesales}" var="w">
					<li style="padding-left: 14px;"><input type="checkbox" value="${w.thirdPartyId}_${w.thirdParty}_${w.endDate}" name="individualWholesale" id="${w.thirdParty}"/> <label for="${w.thirdParty}"><a href="javascript:printReport('individualWholesale','preview', '${w.thirdPartyId}','${w.endDate}');">${w.thirdParty} (${w.endDate})</a>
				</c:forEach>
			</ul>
			
		</fieldset>
		<fieldset>
			<legend><fmt:message key="actionPlans.wholesaleReports"/></legend>
			<ul>
			<c:forEach items="${lWholesaleReports}" var="v">
				<li><input type="checkbox" name="${v}" id="${v}"/> <label for="${v}"><a href="javascript:printReport('${v}','preview', '','');"><fmt:message key="actionPlans.${v}"/></a></label></li>
			</c:forEach>
			</ul>
		</fieldset>
		<fieldset class="l">
			<legend><fmt:message key="actionPlans.otherPlans"/></legend>
			<ul>
			<c:forEach items="${lOtherPlans}" var="v">
				<li><input type="checkbox" name="${v}" id="${v}"/> <label for="${v}"><a href="javascript:printReport('${v}','preview', '','');"><fmt:message key="actionPlans.${v}"/></a></label></li>
			</c:forEach>
			<hr/>
			<c:if test="${sessionScope.nextGenSession.includeAgingPlan}">
				<li><a href="javascript:pop('<c:url value="/PrintInventoryPlan.go"/>','print')" class="fullLink">Print Full Inventory Report</a></li>
			</c:if>
			</ul>
		</fieldset>
	</div>
	<div class="submit"><input type="submit" value="<fmt:message key="a.runSelectedReports"/>"/></div>
	</div>
</div>
</form>
    
