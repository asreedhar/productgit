<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:importAttribute scope="request"/>

<script type="text/javascript" language="javascript">

document.title="Inventory Overview";
  
  </script>
<c:import url="/common/googleAnalytics.jsp" />
<div id="titleBtns">
	<a href="<c:url value="/InventoryPlan.go" />">
	<img src="<c:url value="/common/_images/buttons/inventoryMgmtPlanOn52.gif"/>" width="150" height="17"/>
	</a>
	<a href="<c:url value="oldIMT/TotalInventoryReportDisplayAction.go"/>">
	<img src="<c:url value="/common/_images/buttons/totalInventory_91x17.gif"/>" width="91" height="17"/>
	</a>
	<a href="javascript:pop('<c:url value="/ActionPlansReportSubmit.tx?reportType=${modelPromotionPlans}&timePeriodMode=${modelPromotionTimePeriod}"/>','print')">
	<img src="<c:url value="/common/_images/buttons/ModelPromotionPlan.gif"/>" width="122" height="17" />
	</a>
</div>

<%-- dealer prefs --%>
<c:set var="bShowStatus" value="${Preferences['ShowLotLocationStatus'] == 1 ? true : false}"/>
<c:set var="bShowVinInstead" value="${Preferences['StockOrVinPreference'] == 1 ? false : true}"/>
<c:set var="bShowListPrice" value="${Preferences['ListPricePreference'] == 1 ? true : false}"/>
<c:set var="nDaysRiskThresh" value="${Preferences['AverageDaysSupplyRedThreshold']}"/>
<c:set var="nAgeRiskThresh" value="${Preferences['AverageInventoryAgeRedThreshold']}"/>
<c:set var="nShowDaysSupply" value="${Preferences['UnitsSoldThresholdInvOverview']}"/>

<div id="fred"><iframe style="display: none; left: 0px; position: absolute; TOP: 0px" id="printProgressIFrame" src="javascript:false;" frameBorder="0" scrolling="no"></iframe><div id="printingProgressBox" STYLE="layer-background-color:#333333; visibility:hidden; z-index:10; background:#333333; position:absolute; height:120px; left:400px; top:50px; width:300px; border:1px solid #ffcc33;"><br /><div align="center"><font style="font-size:12px;color:#ffcc33;font-weight:bold">Printing . . . .</font></div><br /><div align="center"><script language="javascript" src="common/_scripts/timerbar.js"></script></div><br /><div align="center"><font style="font-size:10px;color:#ffffff;font-weight:bold">Please wait while your printable page loads...</font></div></div></div>
<script>activateMenu();</script>

	<div class="perfOverall">
		<h5><fmt:message key="performance.all.header"/> <span><fmt:message key="performance.all.range"><fmt:param><c:out value="${weeks}" default="26"/></fmt:param></fmt:message></span></h5>
		<c:import url="/PerformanceBubbleTile.go"/>
	</div>

	
<div class="sort"><%-- big fat hack here to get the reportType and activeSegment variable to refresh with the page --%>
<script language="javascript" type="text/javascript">

var IFrameObj;
var isViewDeals = false;

window.onload=function() { 
	loadPrintIframe();
 };

function enablePrintLink(){
	printFrameIsLoaded="true";
	var printCell=document.getElementById("printCell");

	if(${reportType eq 'SEGMENT'}) {
		 printCell.innerHTML = '<a onmouseover="obscureDropdown(\'show\');" onclick="printPage(\'all\')" onmouseout="obscureDropdown(\'hide\');" href="#">PRINT<img src="<c:url value="/common/_images/icons/arrowDown-menuOff.gif"/>" width="8" height="8"/></a>' +
		 	'<ul class="print" onmouseover="obscureDropdown(\'show\');" onmouseout="obscureDropdown(\'hide\');">' +
		 	'<li><a href="#" onclick="printPage(\'all\')" id="printHref">Print All Inventory</a></li>' +
		 	'<li><a href="#" onclick="printPage(${activeSegment})" id="printHref">Print Current Segment</a></li></ul>';
	}
	
	else if(isViewDeals) {
		printCell.innerHTML =
			'<a href="#" onclick="printPage()" id="printHref">' +
			'<img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage">' +
			'</a><br>';
    } else {
    	printCell.innerHTML=
    		'<a href="#" onclick="printPage(\'all\')" id="printHref">PRINT</a>';

		printCell.className="navTextoff";
	}
}

function loadPrintIframe() {
	enablePrintLink();
}

function printPage(segment) {
	var segmentDetails = {
		daysSupplyKey: '<fmt:message key="l.ds"/>', 
		daysSupplyValue: '${SegmentDetailDisplayBean['SegmentDetailReport']['DaysSupply']}',
		unitsInStockKey: '<fmt:message key="l.uis"/>',
		unitsInStockValue: '${SegmentDetailDisplayBean['SegmentDetailReport']['UnitsInStock']}',
		avgInvAgeKey: '<fmt:message key="l.aia"/>',
		avgInvAgeValue: '${SegmentDetailDisplayBean['SegmentDetailReport']['AvgInventoryAge']}',
		retailAvgGrossProfitKey: '<fmt:message key="l.ragp"/>',
		retailAvgGrossProfitValue: '<fmt:formatNumber type="currency" value="${SegmentDetailDisplayBean['SegmentDetailReport']['AGP']}" currencySymbol="$" maxFractionDigits="0"/>',
		unitsSoldKey: '<fmt:message key="l.us"/>',
		unitsSoldValue: '${SegmentDetailDisplayBean['SegmentDetailReport']['UnitsSold']}',
		avgDaysToSaleKey: '<fmt:message key="l.adts"/>',
		avgDaysToSaleValue: '${SegmentDetailDisplayBean['SegmentDetailReport']['AvgDaysToSale']}',
		noSalesKey: '<fmt:message key="l.ns"/>',
		noSalesValue: '${SegmentDetailDisplayBean['SegmentDetailReport']['NoSaleUnits']}'
	}
	if (segment != 'all')
		loadPrintIFrameOnDemand(segment, segmentDetails);
	else
		loadPrintIFrameOnDemand(segment)
}

function lightPrint() {
	/* legacy funcition do nothing */
}

function paramAdd(params,key,value) {
	var pattern = new RegExp("(\&\?)("+key+"=)[^\&]*(\&?)");
	if (params.match(pattern))
		return params = params.replace(pattern,"$1$2"+encodeURIComponent(value)+"$3"); 
	else
		return params += "&"+encodeURIComponent(key)+"="+encodeURIComponent(value);
}

function paramAdd(params, key, value) {
    var pattern = new RegExp("(\&\?)(" + key + "=)[^\&]*(\&?)");
    if (params.match(pattern))
	    return params = params.replace(pattern, "$1$2" + encodeURIComponent(value) + "$3");
    else
 	   return params += "&" + encodeURIComponent(key) + "=" + encodeURIComponent(value);
}

function loadPrintIFrameOnDemand(segment, params) {
    var IFrameDoc;
    var reportType = 'ALPHABETIC';

    reportType = '${reportType}';
    var URL = "/IMT/PrintableInventoryOverviewReportDisplayAction.go?weeks=26&reportType=" + reportType + (isNaN(segment) ? "": "&activeSegment=" + segment);    

    if (params != undefined) {
        for (key in params) {
            URL = paramAdd(URL, key, params[key]);
        }
    };

    URL = (URL == "") ? "printHolder.html": URL;

    new printUrlLoader(URL);
    return false;
}

function printUrlLoader(url) {
	this._windowName = "printWindow";
	this._w = 250;
	this._h = 150;
	this._x = (window.screen.width / 1.33) - this._w;
	this._y = (window.screen.height / 3) - this._h;
	this.url = url;
	this.loadHandler = this.delegateCurry(this, this.print);
	this.load();
}
printUrlLoader.prototype = {
	getWindowOptions: function () {
		return "width="+this._w+",height="+this._h+",top="+this._y+",left="+this._x;
	},
	getWindowName:function () {
		return this._windowName;
	},
	load: function() {
		if (!this.url) return;
		this.printWindow = window.open(this.url,this.getWindowName(),this.getWindowOptions());
		this.printWindow.focus();
		this.addPrintWindowLoadEvent();
	},
	delegateCurry: function (asThis, fn) {
		return function(e) { fn.call(asThis, new w3cEvent(e)); };
	},
	addPrintWindowLoadEvent:function () {
		if (!this.printWindow) return;
		if (this.printWindow.attachEvent) {
			this.printWindow.attachEvent('onload',this.loadHandler);
		} else if (this.printWindow.addEventListener) {
			this.printWindow.addEventListener('load',this.loadHandler,false);
		}
	},
	removePrintWindowLoadEvent:function () {
		if (!this.printWindow) return;
		if (this.printWindow.detachEvent) {
			this.printWindow.detachEvent('onload', this.loadHandler);
		} else if (this.printWindow.removeEventListener) {
			this.printWindow.removeEventListener('load',this.loadHandler,false);
		}
	},
	print:function (e) {
		if (!this.printWindow) return;
		this.removePrintWindowLoadEvent();
		this.printWindow.print();
		this.printWindow.close();
		this.printWindow = undefined;
	}
};
function w3cEvent(_e) {
	var e = _e || window.event;
	if (!e.target && e.srcElement) e.target = e.srcElement;
	if (!e.currentTarget && e.fromElement) e.currentTarget = e.fromElement;
	if (!e.relatedTarget && e.toElement) e.relatedTarget = e.toElement;
	if (!e.preventDefault) e.preventDefault = function() { this.returnValue = false; };
	if (!e.stopPropagation) e.stopPropagation = function() { this.cancelBubble = true; };
	return e;
	
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', '${googleAnalyticsAccount}', 'auto');
		ga('send', 'pageview');
	
}

</script>
	<c:if test="${bShowStatus}"><h4><fmt:message key="l.retailInvOnly"/></h4></c:if><span><fmt:message key="l.viewby"/></span>
<c:set var="nActiveSeg" value="${empty param.activeSegment ? SegmentDetailDisplayBean['SegmentDetailReport']['VehicleSegmentID']:param.activeSegment}"/>
<c:set var="sActiveSeg" value="${fn:toLowerCase(SegmentDetailDisplayBean['SegmentDetailReport']['Description'])}"/><c:if test="${empty sActiveSeg}" var="bAlphaActive"/>
<fmt:message key="l.segment" var="segmentStr"/><c:if test="${!bAlphaActive}"><c:set var="nonLinkSegText" value="${segmentStr}"/></c:if>
<fmt:message key="l.allAlpha" var="alphaStr"/><c:if test="${bAlphaActive}"><c:set var="nonLinkAlphaText" value="${alphaStr}"/></c:if>
	<c:out value="${nonLinkSegText}" escapeXml="false"><a href="<c:url value="/InventoryOverview.go"><c:param name="reportStyle" value="segment"/><c:param name="activeSegment" value="${!empty nActiveSeg ? nActiveSeg : '3'}"/></c:url>">${segmentStr}</a></c:out>
	<tt>|</tt> 
	<c:out value="${nonLinkAlphaText}" escapeXml="false"><a href="<c:url value="/InventoryOverview.go"><c:param name="reportStyle" value="alphabetic"/></c:url>">${alphaStr}</a></c:out>
	<span class="alwaysLink"><a href="<c:url value="/InventoryOverview.go"><c:param name="reportStyle" value="${not bAlphaActive ? 'segment':'alphabetic'}"/><c:param name="saveStyle" value="true"/></c:url>">Always</a> Start With This View</span>
</div>


<c:if test="${!empty sActiveSeg}"><%-- /////// [begin] SEGMENT /////////--%>
<div class="segment">
	<%-- // [begin] seg tabs --%><div class="tabs">
		<img src="<c:url value="/common/_images/bkgds/cap-ioTabs-l.gif"/>" width="25" height="34" class="lcap"/>
		<img src="<c:url value="/common/_images/bkgds/cap-ioTabs-r.gif"/>" width="25" height="34" class="rcap"/>
		<c:forEach items="${SegmentTabDisplayBeans}" var="segment" varStatus="s">
<c:set var="sSegment" value="${fn:toLowerCase(segment['Description'])}"/>
	<c:if test="${sSegment == sActiveSeg}" var="bIsOn"/>
	<c:if test="${!s.first}"><div><!-- divider --><img src="<c:url value="/common/_images/d${!bIsOn && !bSupressNextVline ?'-999':''}.gif"/>" width="1" height="25" class="vline"/></div></c:if>
		<a ${bIsOn ? 'no':''}href="?activeSegment=${segment['VehicleSegmentID']}&reportStyle=segment"<c:if test="${bIsOn}"> class="${sSegment} active"</c:if>>
	<c:if test="${bIsOn}"><img src="<c:url value="/common/_images/bkgds/cap-ioTab-active-l.gif"/>" width="4" height="40" class="lcap"/><img src="<c:url value="/common/_images/bkgds/cap-ioTab-active-r.gif"/>" width="4" height="40" class="rcap"/></c:if>
			<h4>${fn:toUpperCase(sSegment)}</h4>
	<c:if test="${!bIsOn}"><span>${segment['DaysSupply']} <fmt:message key="l.ds"/></span></c:if>
		</a>
	<c:if test="${bIsOn}" var="bSupressNextVline"/></c:forEach></div><%-- // [end] seg tabs --%>
	
	<%-- // [begin] seg details --%><div class="segmentDetails">
		<div class="numbers">
			<ul>
				<li><strong>${SegmentDetailDisplayBean['SegmentDetailReport']['DaysSupply']}</strong> <fmt:message key="l.ds"/></li>
				<li><strong>${SegmentDetailDisplayBean['SegmentDetailReport']['UnitsInStock']}</strong> <fmt:message key="l.uis"/></li>
				<li><fmt:message key="l.aia"/>: <strong>${SegmentDetailDisplayBean['SegmentDetailReport']['AvgInventoryAge']}</strong></li>
			</ul>
		</div>
		<div class="perfSegment">
			<bean:define id="reportDisplayBean" name="SegmentDetailDisplayBean" property="SegmentDetailReport" toScope="request"/>
			<c:import url="/content/analysis/_performanceBubble.jsp">
				<c:param name="perfType" value="segment"/>
			</c:import>
		</div>
	</div><%-- // [end] seg details --%>
	
</div>
<%-- // [end] SEGMENT ////////// --%>
</c:if>

<%-- /////// [begin] MODEL ///////// --%>
<div id="items">
	<div id="scroll">
	<%-- // [begin] model loop --%>
	<c:forEach items="${ModelLevelReportBlock}" var="model" varStatus="m"><c:if test="${m.count gt 0}">
		<a name="${model.performanceBubble.VehicleGroupingID}"></a>
		<div class="model">
			<div class="modelDetails">
				<h2><link:perfPlus link="${model.performanceBubble.Description}" grouping="${model.performanceBubble.VehicleGroupingID}"/>
				<link:pricingAnalyzer grouping="${model.performanceBubble.VehicleGroupingID}" product="edge"/>
				</h2>
	<c:if test="${model['AvgAgeInDays'] > nAgeRiskThresh}" var="bIsRed"/>
				<span${bIsRed ? ' class="hilite"':''}><fmt:message key="l.aia"/> ${model['AvgAgeInDays']}</span>
<c:if test="${model['performanceBubble']['UnitsSold'] >= nShowDaysSupply}">
	<c:if test="${model['performanceBubble']['DaysSupply'] > nDaysRiskThresh}" var="bIsRed"/>				
				<span${bIsRed ? ' class="hilite"':''}><fmt:message key="l.ds"/> ${model['performanceBubble']['DaysSupply']}</span>  
</c:if>				
				<a href="javascript:pop('<c:url value="oldIMT/GroupingPromotionDialogDisplayAction.go"><c:param name="groupingDescriptionId" value="${model['performanceBubble']['VehicleGroupingID']}"/></c:url>', 'promo')"><img src="<c:url value="/common/_images/buttons/ModelPromotion.gif"/>" width="100" height="17" border="0"/></a>
			</div>
			<div class="perfModel">
				<bean:define id="reportDisplayBean" name="model" property="performanceBubble" toScope="request"/>
				<c:if test="${model['performanceBubble']['Display']}">
					<c:import url="/content/analysis/_performanceBubble.jsp">
						<c:param name="perfType" value="model"/>
					</c:import>
				</c:if>
			</div>
		</div>
		<display:table requestURI="InventoryOverview.go#${model.performanceBubble.VehicleGroupingID}" id="vehicle"  name="${model.InventoryLineItems}" defaultorder="descending" defaultsort="3" cellpadding="0" cellspacing="0" class="vehicleDetails">
			<display:column title="&nbsp;" decorator="biz.firstlook.main.display.util.RowNumberDecorator" headerClass="num" class="num" />
			<display:column title="&nbsp;" sortable="false" headerClass="light" class="light">
				<c:if test="${reportDisplayBean['InventoryVehicleLightID'] > vehicle['CurrentVehicleLight']}">
					<c:set var="lightStr"><util:decodeLight id="${vehicle['CurrentVehicleLight']}"/></c:set>
					<c:choose>
						<c:when test="${!empty lightStr}">
							<img src="<c:url value="/common/_images/icons/light-13x13-${lightStr}.gif"/>" width="13" height="13" border="0" class="icon" title=""/>
						</c:when>
						<c:otherwise>
							&nbsp;
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:if test="${reportDisplayBean['InventoryVehicleLightID'] <= vehicle['CurrentVehicleLight']}">
					&nbsp;
				</c:if>
			</display:column>
			<display:column titleKey="l.age" property="AgeInDays" sortable="true" headerClass="age" class="age"/>
			<display:column titleKey="l.year" property="VehicleYear" sortable="true" headerClass="year" class="year" />
			<display:column titleKey="l.mtb" property="VehicleDescription" sortable="true" headerClass="mtb" />
			<display:column titleKey="l.color" sortable="true" headerClass="color" class="color">
		${empty vehicle['Color'] ? '&mdash;':vehicle['Color']}
			</display:column>
			<display:column titleKey="l.mileage" property="Mileage" decorator="biz.firstlook.main.display.util.MileageDecorator" sortable="true" headerClass="mileage" class="mileage" />
			<c:if test="${bShowListPrice}">
			<display:column titleKey="l.lp" sortable="true" headerClass="lp" class="lp">
				${empty vehicle['ListPrice'] ? '&mdash;':''}<fmt:formatNumber type="currency" value="${vehicle['ListPrice']}" currencySymbol="$" maxFractionDigits="0"/>
			<c:if test="${sessionScope.nextGenSession.hasPingIIUpgrade}">
				<a href="javascript:void(0)" onclick="window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="stockNumber" value="${vehicle['StockNumber']}"/><c:param name="popup" value="true"/></c:url>','PingII', 'status=1,scrollbars=1,toolbar=0,width=1009,height=860')" title="pingII">
					<html:img pageKey="img.icon.ping" styleClass="ping" />
				</a>
			</c:if>
			<c:if test="${sessionScope.nextGenSession.hasPingUpgrade && not(sessionScope.nextGenSession.hasPingIIUpgrade)}">
					<a href="javascript:pop('<c:url value="Ping.go"><c:param name="mmg" value="${vehicle.MakeModelGroupingID}"/><c:param name="year" value="${vehicle.VehicleYear}"/><c:param name="distance" value="50"/><c:param name="zip" value="${zip}"/><c:param name="search" value="Search"/></c:url>','ping')" title="<fmt:message key="title.ping"/>">
						<html:img pageKey="img.icon.ping" styleClass="ping" />
					</a>
			</c:if>
			</display:column>
			</c:if>

		 	<display:column titleKey="l.uc" property="UnitCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" headerClass="uc" class="uc" />
			<!-- if BookVsCost is negative, '<span class=neg>(BookVsCost)</span>' is returned from the decorator -->
			<display:column titleKey="l.bvc" property="BookVsCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" headerClass="bvc" class="bvc"/>
			<display:column titleKey="l.pt" sortable="true" headerClass="pt" class="pt">
		${empty vehicle['TradeOrPurchaseDesc'] || vehicle['TradeOrPurchaseDesc'] == 'N/A' ? '&mdash;': vehicle['TradeOrPurchaseDesc']}
			</display:column>
<c:set var="sStockLabel" value="${bShowVinInstead ? 'vin':'snum'}"/>
			<display:column titleKey="l.${sStockLabel}" sortable="true" headerClass="${sStockLabel}" class="${sStockLabel}">
					<c:choose><c:when test="${empty vehicle['StockNumber'] || empty vehicle['VIN']}">&mdash;</c:when><c:otherwise>
					<a id="${sStockLabel == 'snum' ? vehicle['StockNumber']:vehicle['VIN']}" href="#${sStockLabel == 'snum' ? vehicle['StockNumber']:vehicle['VIN']}" onclick="pop('<c:url value="oldIMT/EStock.go?stockNumberOrVin=${sStockLabel=='snum'?vehicle.StockNumber:vehicle.VIN}"/>','estock')">${sStockLabel == 'snum' ? vehicle['StockNumber']:vehicle['VIN']}</a>
				<c:if test="${vehicle.isAccurate == 0}">
					<img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" title="Book values may be inaccurate."/>	
				</c:if>
				</c:otherwise></c:choose>	
			</display:column>
		<c:if test="${bShowStatus}">
			<display:column titleKey="l.status" sortable="true" headerClass="status" class="status">
		${empty vehicle['InventoryStatusCode'] ? '&mdash;':vehicle['InventoryStatusCode']}
			</display:column>
		</c:if>			
		<c:if test="${showAnnualRoi}">
<display:column titleKey="l.aRoi" property="AnnualRoi" decorator="biz.firstlook.main.display.util.PercentageDecorator" sortable="true" headerClass="roi" class="roi"/>
		</c:if>
		</display:table>

	</c:if></c:forEach>
	<%-- // [end] model loop --%>
	</div>
<script language="JavaScript" type="text/javascript">
	window.onresize = function() { resizeScroll('${bAlphaActive ? 'alpha':'segment'}'); };
	resizeScroll('${bAlphaActive ? 'alpha':'segment'}');
	var bPageReload = false;
</script>
<cite><fmt:message key="overview.lastPoll"><fmt:param><fmt:formatDate value="${Preferences['LastDMSReferenceDateUsed']}"/></fmt:param></fmt:message></cite>
</div>
<%-- // [end] MODEL ////////// --%>
<script type="text/javascript" charset="utf-8">	
			if (window.location.hash) {
				window.location.replace(window.location.hash);
			};
</script>