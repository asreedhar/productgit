<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- total hack, my bad.  see WEB-INF/templates/email/exceptionReport-html.ftl --%>

<div style="margin: 0 0 0 0; background-color: white;">
<c:if test="${empty groupedExceptionReport}" >
	<div style="text-align: center;">No Inventory Transfer Exceptions to report.</div>
</c:if>
<c:forEach items="${groupedExceptionReport}" var="datafeedReports">
<div style="font: 20px arial;font-weight:bold; color: black; background-color: white; text-align:left; padding:3px;">
	${datafeedReports.key.advertiser} Inventory Transfer Exceptions
</div>
<div style="font: 12px arial; color: black; text-align: left; ;padding:5px;padding-top:13px;border-bottom:solid 1px black">
	Inclues all inventory with pricing exceptions for electronic data transfer on ${datafeedReports.key.date}
</div>
<div style="font: 12px arial; color: black; text-align: left; ;padding:5px;padding-top:13px; padding-bottom:13px;">
	Vehicles ${datafeedReports.key.reason}
</div>
<div style="background-color: #ddd;border-bottom:solid 1px black;margin-bottom:40px;">
	<c:forEach items="${datafeedReports.value}" var="vehicle">
	<div style="background-color: lightgray; font: 14px arial;border-bottom:solid 1px #bbb;border-top:solid 1px #fff;padding:5px;">
		<span style="font-weight: bold; padding-right:25px">${vehicle.VehicleYear} ${vehicle.Make} ${vehicle.OriginalModel} ${vehicle.VehicleTrim}</span>
		<span style="padding-right:25px">COLOR:  ${vehicle.BaseColor}</span>
		<span style="padding-right:25px">MILEAGE: ${vehicle.MileageRecieved}</span>
		<span style="padding-right:25px">STOCK#: ${vehicle.StockNumber}</span>
		<span style="padding-right:25px">VIN: ${vehicle.Vin}</span>
	</div>
	</c:forEach>
</div>
</c:forEach>
<img src="common/_images/logos/FL-112x24-transOn333.gif" alt="Firstlook" style="display:block;margin-left:auto;margin-right:auto;">
<div style="font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	Performance Management and Inventory Optimization
</div>
<div style="font: 12px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	Helping Dealers Become World Class Retailers
</div>
</div>
<div style="font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	First Look Help Desk: 1-877-378-5665
</div>