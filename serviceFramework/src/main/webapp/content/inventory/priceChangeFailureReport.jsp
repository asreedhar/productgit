<tiles:importAttribute/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" media="all" type="text/css" href="/NextGen/content/analysis/_styles/analysis.css"/>
<script type="text/javascript" language="javascript">
	document.title="Price Change Failure";
</script>
<c:import url="/common/googleAnalytics.jsp" />	
<div class="analyticsBody">
	<div class="filter">
	<fmt:message key="l.viewby"/>
	<span> 
<fmt:message key="l.currentWeek" var="currentWeekStr"/><c:if test="${timeFrame == '1'}"><c:set var="nonLinkCurrentWeekText" value="${currentWeekStr}"/></c:if>
<fmt:message key="l.lastTwoWeeks" var="lastTwoWeeksStr"/><c:if test="${timeFrame == '2'}"><c:set var="nonLinkLastTwoWeeksText" value="${lastTwoWeeksStr}"/></c:if>
<fmt:message key="l.currentMonth" var="currentMonthStr"/><c:if test="${timeFrame == '3'}"><c:set var="nonLinkCurrentMonthText" value="${currentMonthStr}"/></c:if>
<fmt:message key="l.previousMonth" var="priorMonthStr"/><c:if test="${timeFrame == '4'}"><c:set var="nonLinkPreviousMonthText" value="${priorMonthStr}"/></c:if>
	<c:out value="${nonLinkCurrentWeekText}" escapeXml="false"><a href="<c:url value="/PriceChangeFailureReportAction.go"><c:param name="timeframe" value="1"/></c:url>">${currentWeekStr}</a></c:out>
	<tt>|</tt> 
	<c:out value="${nonLinkLastTwoWeeksText}" escapeXml="false"><a href="<c:url value="/PriceChangeFailureReportAction.go"><c:param name="timeframe" value="2"/></c:url>">${lastTwoWeeksStr}</a></c:out>
	<tt>|</tt> 
	<c:out value="${nonLinkCurrentMonthText}" escapeXml="false"><a href="<c:url value="/PriceChangeFailureReportAction.go"><c:param name="timeframe" value="3"/></c:url>">${currentMonthStr}</a></c:out>
	<tt>|</tt> 
	<c:out value="${nonLinkPreviousMonthText}" escapeXml="false"><a href="<c:url value="/PriceChangeFailureReportAction.go"><c:param name="timeframe" value="4"/></c:url>">${priorMonthStr}</a></c:out>
	</span>
	</div>

<br />
<display:table requestURI="PriceChangeFailureReportAction.go" id="vehicle" name="priceChangeFailureBean" cellpadding="0" cellspacing="0" defaultsort="11" defaultorder="descending" class="pricingAnalyzerdata">
	<display:setProperty name="css.tr.odd" value="" />
	<display:setProperty name="css.tr.even" value="" />
	<display:setProperty name="basic.empty.showtable" value="true" />
	<display:setProperty name="basic.msg.empty_list_row">
		<tr><td colspan="{0}" class="purchasing-gridData" style="padding-left:22px"><br>There are no vehicles that match your search criteria.</td></tr>
	</display:setProperty>
	<display:column title="&nbsp;"><c:out value="${vehicle_rowNum}" /></display:column>
	<display:column title="Date Sent"  property="DateSent" sortable="true" decorator="biz.firstlook.main.display.util.DateDecorator"/>
	<display:column titleKey="l.year"  property="VehicleYear" sortable="true" />
	<display:column titleKey="l.mtb" sortable="true"><link:perfPlus link="${vehicle.Make} ${vehicle.Model} ${vehicle.VehicleTrim}" grouping="${vehicle.GroupingDescriptionID}"/><c:out value="" /></display:column>
	<display:column titleKey="l.snum"  sortable="true" class="l" headerClass="l" sortProperty="StockNumber">
		<a href="javascript:pop('<c:url value="oldIMT/EStock.go"><c:param name="stockNumberOrVin" value="${vehicle.StockNumber}"/></c:url>','estock');">${vehicle.StockNumber}</a>	
	</display:column>
	<display:column title="Repriced By"  property="RepricedBy" sortable="true" />
	<display:column title="Old Internet Price"  property="OldListPrice" sortable="true" decorator="biz.firstlook.main.display.util.MoneyDecorator" />
	<display:column title="New Internet Price"  property="NewListPrice" sortable="true" decorator="biz.firstlook.main.display.util.MoneyDecorator" />
	<display:column title="Failure Reason"  property="FailureReason" sortable="true" />
</display:table>
</div>
<%--
<div style="margin: 0 0 0 0; background-color: white;">


<img src="common/_images/logos/FL-112x24-transOn333.gif" alt="Firstlook" style="display:block;margin-left:auto;margin-right:auto;">
<div style="font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	Performance Management and Inventory Optimization
</div>
<div style="font: 12px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	Helping Dealers Become World Class Retailers
</div>
</div>
<div style="font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	First Look Help Desk: 1-877-378-5665
</div>
--%>