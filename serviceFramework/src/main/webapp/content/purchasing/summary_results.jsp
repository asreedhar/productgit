<%@ taglib uri="/WEB-INF/taglibs/firstlook.tld" prefix="firstlook" %>

<firstlook:exportStrutsForm var="theForm"/><tiles:importAttribute/>
<c:if test="${pageId=='flashSummary'}" var="isFlashSummary" scope="request"/>
<c:if test="${!showNavigator || modelContent ne 'true'}"><c:import url="/IncludeUpgradeToNavigator.go"/></c:if>

<%-- GROUP OBJECT //--%>
	
	<c:if test="${collection eq 'true'}"><c:set var="PrimaryResultsGroups" value="${optimalInventorySearchSummaryForm.groupBySearchResults.groups}" scope="page"/>
</c:if>
	<c:if test="${collection eq 'false'}"><c:set var="PrimaryResultsGroups" value="${firstlook:singletonList(optimalInventorySearchSummaryForm.groupByNodeIncrement)}" scope="page"/>
</c:if>
	
	<%--<c:set var="numberOfMarkets" value="0"/>--%>
	<c:forEach var="market" items="${theForm.searchContext.markets}">
		<c:if test="${market.suppress == false && market.marketType != 'LIVE'}"><c:set var="numberOfMarkets" value="${numberOfMarkets+1}"/></c:if>
	</c:forEach>
	<c:if test="${empty numberOfMarkets && !isFlashSummary}"><h4><fmt:message key="instruct.search.searchingNoMarkets"/></h4></c:if>
	<%-- coming from max homepage flash locate does not set the pageId properly and isFlashSummary is not present. --%>
	<c:if test="${invalidMake}"><h4><fmt:message key="instruct.search.enterMake"/></h4></c:if>
	<c:if test="${invalidModel}"><h4><fmt:message key="instruct.search.enterModel"/></h4></c:if>

<c:if test="${numberOfMarkets > 0}">
<c:if test="${modelContent ne 'true'}"><%-- //used by javascript functions	--%><input type="hidden" id="totalMarkets" name="numberOfMarkets" value="${numberOfMarkets}"/></c:if>	
	
<c:forEach items="${PrimaryResultsGroups}" var="prigroup_item" varStatus="prigroup_loop">
<%-- VAR //--%><c:set var="resultsId" value="${prigroup_item.id}" scope="request"></c:set>


<!-- begin PRIMARY LOOP //////////////// -->
<c:if test="${modelContent ne 'true'}">
<div class="group summary" id="results_${resultsId}">
</c:if>
<%-- GROUP ITEM  //--%><c:set var="item_object" value="${prigroup_item}" scope="request"/>
<%-- GROUP COUNT //--%><c:set var="item_count" value="${prigroup_loop.count}" scope="request"/>
<%-- GROUP TOTAL //--%><c:set var="total_results" value="${firstlook:numberOfNodesAtDepth(item_object,2)}" scope="request"/>
<%-- GROUP TOTAL //--%><c:set var="groupId" value="${resultsId}" scope="request"/>
	<!-- PRIMARY GROUP = header for whole group -->
	<div class="head">
		<a href="#" onclick="return confirmRemoveResultsGroup('${resultsId}','remove${resultsId}','${item_object.nodeValue.name }' );" id="remove${resultsId}" class="toggle"><span>X</span></a>
		<div class="left">

<tiles:insert attribute="primary_group"/>
		<c:if test="${total_results ne 0}"><a href="javascript:toggleAllResultsGroups(${resultsId});" class="viewAllLink"><span id="viewAll_${resultsId}"><fmt:message key="a.viewAll"/></span><span id="hideAll_${resultsId}" style="display:none;"><fmt:message key="a.hideAll"/></span></a></c:if>
<div class="select" id="dynSelect_${resultsId}"><a href="javascript:showDistanceSelect(${resultsId})" id="distanceSelectLink_${resultsId}"><c:if test="${theForm.maxDistanceFromDealerInGroup <= 1000}"><fmt:message key="l.withinMiles"><fmt:param value="${''}${theForm.maxDistanceFromDealerInGroup}"/></fmt:message></c:if><c:if test="${theForm.maxDistanceFromDealerInGroup > 1000}"><fmt:message key="l.unlimited"/></c:if></a><a href="javascript:showDistanceSelect(${resultsId})" id="distanceSelectLink_${resultsId}"><img src="<c:url value="/common/_images/bullets/caratDown-7x4-6C8196.gif"/>"/></a>
	<util:selectRange  milesMinSelected="${theForm.minMileageInGroup}" triggerRefresh="true" milesMaxSelected="${theForm.maxMileageInGroup}" uniqueId="${resultsId }"/>
							</div>
		
		</div>
					<%-- ????????????????????????????????????????????????????????????????????????????? --%>		
					<c:remove var="rankingStr"/><c:remove var="isHotList"/>
					<c:forEach items="${prigroup_item.nodeValue.searchCandidates}" var="searchCandidate" varStatus="searchCandidateStatus">
						<c:if test="${searchCandidateStatus.first}">
							<c:forEach items="${searchCandidate.annotations}" var="annotation">
								<c:choose>
									<c:when test="${annotation.name eq 'PowerZone'}"><c:set var="rankingStr" value="pz"/></c:when>
									<c:when test="${annotation.name eq 'Winners'}"><c:set var="rankingStr" value="win"/></c:when>
									<c:when test="${annotation.name eq 'GoodBets'}"><c:set var="rankingStr" value="gb"/></c:when>
									<c:when test="${annotation.name eq 'MarketPerformers'}"><c:set var="rankingStr" value="mp"/></c:when>
									<c:when test="${annotation.name eq 'Managers Choice'}"><c:set var="rankingStr" value="mc"/></c:when>
								</c:choose>		
												
							</c:forEach>
							
							<c:forEach items="${searchCandidate.criteria}" var="criterion">
								<c:forEach items="${criterion.annotations}" var="annotation">
									<c:if test="${annotation.name eq 'Hot List'}"><c:set var="isHotList" value="${annotation.name eq 'Hot List'}"/></c:if>
								</c:forEach>
							</c:forEach>
						</c:if>
					</c:forEach>
						<input type="hidden" id="selectedTrims_${resultsId }" value="<c:forEach items="${prigroup_item.nodeValue.models}" var="selectedTrims" varStatus="selectedTrimsStat">${selectedTrims.searchCandidate == null ? '' : selectedTrimsStat.index}${selectedTrims.searchCandidate != null?',':'' }</c:forEach>"/>
				
						<%-- ????????????????????????????????????????????????????????????????????????????? --%>	
		<div class="right">
			<c:if test="${theForm.searchContext.persistenceScope != 'TEMPORARY'}">
			<div class="update">
				<a href="#" onclick="return showEditOptions('edit_${resultsId}', ${resultsId});" id="edit_${resultsId}"><fmt:message key="a.edit"/></a>
			</div>
			</c:if>
			<div class="rating">
				<c:if test="${isHotList}"><span><img src="common/_images/icons/hotList-transOnBlue.gif" class="hotListIcon" /></span></c:if>			
				<c:if test="${!empty rankingStr}"><fmt:message key="insight.ranking.${rankingStr}"/></c:if>
			</div>
		</div>
	</div>
<c:if test="${!empty primary_subgroup}">
	<div class="head_sub">
<div class="submit" id="update_${resultsId}" style="display:none;">
	<input type="hidden" id="addedYears_${resultsId}" value="">
	<input type="hidden" id="total_${resultsId}" value="${total_results}">
	<util:button type="confirm"><a href="javascript:updateModelResults(${resultsId});"><fmt:message key="a.updateResults"/></a></util:button>	
</div>
<tiles:insert attribute="primary_subgroup" ignore="true"/>
	</div>
</c:if>
	<!-- /PRIMARY GROUP -->

<c:if test="${total_results ne 0}"><%-- print ONLY groups with results --%>
	<!-- SECONDARY GROUP -->

<%-- GROUP OBJECT //--%><c:set var="SecondaryResultsGroups" value="${prigroup_item.childNodes}"/>

		<c:forEach items="${SecondaryResultsGroups}" var="secgroup_item" varStatus="secgroup_loop"><c:if test="${secgroup_loop.first}"><c:set var="count" value="${secgroup_loop.index}"/></c:if><c:if test="${secgroup_item.nodeValue.suppress == false && secgroup_item.nodeValue.marketType != 'LIVE'}">
<%-- VAR //--%><c:if var="hasNoChildren" test="${fn:length(secgroup_item.childNodes)==0}" scope="page"/>
<c:set var="count" value="${count+1}"/>
<%-- VAR //--%><c:if var="isEven" test="${count mod 2 eq 0}" scope="page"/>
<%-- VAR //--%><c:remove var="groupId"/><c:set var="groupId" value="${resultsId}.${count}" scope="request"/>

	<input type="hidden" id="total_${groupId}" value="${firstlook:numberOfNodesAtDepth(secgroup_item,1)}">

		<!-- begin SECONDARY LOOP ################ -->
<span id="group_${groupId}" class="${hasNoChildren?'none':hasOpenResults?'expanded':'collapsed'}">
<%-- FUTURE ??--%><%--<c:set var="hasOpenResults" value="${prigroup_loop.count%2==0?true:false}" scope="request"/>--%>
	<div class="body${isEven?' even':' odd'}">
<%-- GROUP ITEM  //--%><c:set var="item_object" value="${secgroup_item}" scope="request"/>
<%-- GROUP COUNT //--%><c:set var="item_count" value="${count}" scope="request"/>
<tiles:insert attribute="secondary_group" ignore="true"/>
	</div>
	<!-- /SECONDARY GROUP -->

<c:set var="SortByNode" value="${item_object.id}" scope="request"/>
<c:set var="SortByDiv" value="market_${SortByNode}" scope="request"/>

<%-- VEHICLES GROUP --%>
<form id="form_${groupId}">
<span id="${SortByDiv}">
<%-- VEHICLES GROUP OBJECT //--%><c:set var="VehiclesGroups" value="${secgroup_item.childNodes}" scope="request"/>
		<tiles:insert attribute="vehicles_year_loop" ignore="true"/>
</span>
</form>
<%--/VEHICLES GROUP --%>


</span>
	<!-- end SECONDARY LOOP ################/ -->
	</c:if></c:forEach>

	</c:if>

<c:if test="${modelContent ne 'true'}">
</div>
</c:if>
<!-- end PRIMARY LOOP //////////////// -->
</c:forEach>
	</c:if>