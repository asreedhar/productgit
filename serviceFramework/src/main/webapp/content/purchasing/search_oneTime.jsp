<div id="search" class="group search onetime">
	<div class="head noBottom" id="oneTimeHead"><fmt:message key="link.url.flashSummary" var="flashUrl"/>
		<a onclick="return preventClicks(this);" href="<c:url value="${flashUrl}"/>"><fmt:message key="l.flashLocate"/> <fmt:message key="l.search"/></a>
		<h5 id="oneTimeToggle" class="contracted" onclick="toggleOneTimeSearch('oneTimeToggle','oneTimeSearchBody','oneTimeHead');"><fmt:message key="l.search"/> <fmt:message key="l.forOtherModels"/></h5>
	</div>
	<div class="body" id="oneTimeSearchBody" style="display:none;">
<form name="searchForm">
		<fieldset>
			<dl class="make">
				<dt><label for="makeSelect"><fmt:message key="l.make"/>:</label></dt>
				<dd>
					${makeDropdown}
				</dd>
			</dl>
			<dl class="model">
				<dt><label for="modelSelect"><fmt:message key="l.model"/>:</label></dt>
				<dd id="modelBox">
				<select name="model" id="modelSelect" class="model" disabled="disabled">
					<option value="">Select a make...</option>
				</select>
				</dd>
			</dl>
		</fieldset>
		<fieldset>
			<input type="checkbox" name="addToHotList" id="addToHotlistCheck" disabled="disabled" onclick="toggleHotListEdit(this,'hotListEdit_Global');" onkeypress="submitOnEnter('addSearchCandidate');"/>
			<label for="addToHotlistCheck"><fmt:message key="a.addTo"><fmt:param><fmt:message key="l.hotList"/></fmt:param></fmt:message></label>
			<ins class="hotlist" id="hotListEdit_Global">
					<fmt:message key="l.for"/>:
					<input type="radio" name="hotListDurationAllGlobal" value="always" checked="checked" id="alwaysRadioGlobal" onkeypress="submitOnEnter('addCandidateLink');"/> 
					<label for="alwaysRadioGlobal"><fmt:message key="l.always"/></label>
					<input type="radio" name="hotListDurationAllGlobal" value="days" id="hotListDurCheckAllGlobal" onkeypress="submitOnEnter('addCandidateLink');"/>
					<input type="text" name="hotListDurDaysGlobal" value="0" id="hotListDurTextGlobal" onkeypress="return Validate.checkChars();" onkeyup="submitOnEnter('addCandidateLink');" onblur="validateDays(this);" onfocus="$('hotListDurCheckAllGlobal').checked = true;" class="number" ondblclick="return showCalendarFloosan('hotListDurDaysGlobal', '%m/%d/%Y', null, true, false,'<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>');" /> 
					<label for="hotListDurTextAll"><fmt:message key="l.days"/></label>
					<img src="<c:url value="/common/_images/icons/calendar.gif"/>" class="cal" id="dateTriggerAll" onclick="return showCalendarFloosan('hotListDurDaysGlobal', '%m/%d/%Y', null, true,false,'<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>');" onkeypress="submitOnEnter('addCandidateLink');" />
			</ins>		
		</fieldset>
		<div class="submit">
			<util:button>
				<a href="#" onclick="return addSearchCandidate();" id="addCandidateLink"><fmt:message key="a.search"/></a>
			</util:button>	
		</div>
</form>
	</div>
</div>