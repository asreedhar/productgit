<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<tiles:importAttribute/>


<c:set var="searchSummary" value="${optimalInventorySearchSummaryForm.searchSummary[optimalInventorySearchSummaryForm.searchSummaryIndex]}"/>
<c:set var="searchSummaryIndex" value="${optimalInventorySearchSummaryForm.searchSummaryIndex}"/>
<c:set var="exactMatches" value="${optimalInventorySearchSummaryForm.exactMatchesLive}"/>
<c:if test="${optimalInventorySearchSummaryForm.exactMatches == null}"><c:set var="exactMatches" value="true"/></c:if>
<div id="rightNav">
	<a onclick="return preventClicks(this);" href="MarketplaceSearchSummary.go">RETURN TO MARKETPLACE SEARCH SUMMARY</a>
</div>

<div id="mainTable">
<div id="saleHeader">
 <!--${searchSummary.title}--><span class="date"><fmt:formatDate value="${searchSummary.auctionDate}" pattern="EEEE,MMMM dd, yyyy"/>  ${searchSummary.location}</span>
</div>
<div id="details">

${searchSummary.auctionName} <br/>
${searchSummary.vehiclesMatched} Possible Matches 

<br/>
<br/>
Show: <a id="potentialresults" onclick="return toggleLiveAuctionRunListMatches(this, 'contentHolder', 'true', ${searchSummaryIndex});" ${exactMatches ? 'class="disabled"' : ''} href="#">Potential Matches Only</a> | <a id="allresults" onclick="return toggleLiveAuctionRunListMatches(this, 'contentHolder', 'false', ${searchSummaryIndex});" ${!exactMatches ? 'class="disabled"' : ''} href="#">All Results</a>

<br/>
<br/>
Options and Announcements: <a id="hideAll"  class="disabled" onclick="if(this.className=='disabled')return false;  runListOptions('tr',true);" href="#">Hide All</a> | <a id="showAll" onclick="if(this.className=='disabled')return false; runListOptions('tr',false);" href="# ">Show All</a>
</div>
<div id="contentHolder">
<tiles:insert attribute="content" />
</div>
</div>