<tiles:importAttribute/><c:if test="${slice eq 'inGroupAndOnline'}" var="getInGroupAndOnlineResults"/><c:if test="${slice eq 'live'}" var="getLiveResults"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
<c:when test="${getInGroupAndOnlineResults}"><c:set var="marketType" value="IN-GROUP" scope="request"/><tiles:insert attribute="ingroup"/><c:set var="marketType" value="ONLINE" scope="request"/><tiles:insert attribute="online"/></c:when>
<c:when test="${getLiveResults}"><c:set var="marketType" value="LIVE" scope="request"/><tiles:insert attribute="live"/></c:when>
<c:otherwise><%-- get FULL PAGE --%>

<script type = 'text/javascript'>
document.title = 'Marketplace Search Summary';
</script>
<c:import url="/common/googleAnalytics.jsp" />

<div class="left">	
	<div id="inGroupAndOnline">
		<c:set var="marketType" value="IN-GROUP" scope="request"/>
		<tiles:insert attribute="ingroup"/>
		<c:set var="marketType" value="ONLINE" scope="request"/>
		<tiles:insert attribute="online"/>
	</div>
</div>
<div class="right">
	 <%--LIVE AUCTION ALERTS--%>
	<div id="liveAuctionAlerts">
		<c:set var="marketType" value="LIVE" scope="request"/>
		<tiles:insert attribute="live"/>
	</div>
</div>
	</c:otherwise>
</c:choose>
