<firstlook:exportStrutsForm var="theForm"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="left" id="markets">
	<ul id="marketElements">
		<li class="label" id="searchLabel"><fmt:message key="l.searching"/>:</li>
		<c:forEach items="${optimalInventorySearchSummaryForm.searchContext.markets}" var="market" varStatus="m"><c:if test="${market.marketType != 'LIVE'}">
		<li id="searchMarket_${m.index}" class="${market.suppress?'off':'on'}" onclick="toggleMarket('searchMarket_${m.index}');">${market.name}</li>
		</c:if></c:forEach>
	</ul>
</div>

<script type = 'text/javascript'>
var url = window.location.href;
if (url.indexOf("Flash")>-1)
	document.title = "Flash Locator Search Summary"
	else
		document.title = "FirstLook Search Engine Results"
//document.title = 'Flash Locator Search Summary';
</script>
<c:import url="/common/googleAnalytics.jsp" />

<div class="right">
	<div class="submit">
		<util:button><a href="#" onclick="return updateResults('results','distanceSelect','marketElements',['IN_GROUP','ONLINE'],'marketElements',null);">Update</a></util:button>	
	</div>
	<div id="distance">
		<!-- dynamic dropdown -->
		<c:set var="selectedOption"><c:if test="${optimalInventorySearchSummaryForm.maxDistanceFromDealerInGroup <= 1000 }"><fmt:message key="l.withinMiles"><fmt:param value="${''}${optimalInventorySearchSummaryForm.maxDistanceFromDealerInGroup}"/></fmt:message></c:if>
			<c:if test="${optimalInventorySearchSummaryForm.maxDistanceFromDealerInGroup >1000 }"><fmt:message key="l.unlimited"/></c:if>
		</c:set>
		<c:import url="/GetMarketDistanceOptions.go" var="optionsImport" scope="page"><c:param name="selectedOption" value="${optimalInventorySearchSummaryForm.maxDistanceFromDealerInGroup}"/></c:import>	
		<util:selectBox selected="${selectedOption}" id="searchDistance">
			<select id="distanceSelect">${optionsImport}</select>
		</util:selectBox>		
			<br/>
			<util:selectRange  milesMinSelected="${optimalInventorySearchSummaryForm.minMileageInGroup}" triggerRefresh="false"  milesMaxSelected="${optimalInventorySearchSummaryForm.maxMileageInGroup}" uniqueId="global"/>
		</div>
</div>
<div class="left">
	<div class="setPrefs">
		<img class="saveSuccessHidden" id="marketElementsSave" src="<c:url value="/common/_images/bullets/check-7x8-437AAA.gif"/>"/><fmt:message key="pref.alwaysStartWith"><fmt:param value="handleMarketplace('marketElements',['IN_GROUP', 'ONLINE']);"/><fmt:param><fmt:message key="pref.markets"/></fmt:param></fmt:message>
	</div>
</div>
<div class="right">
	<div class="setPrefs">
		<img class="saveSuccessHidden" id="distanceSelectSave" src="<c:url value="/common/_images/bullets/check-7x8-437AAA.gif"/>"/><fmt:message key="pref.updateDistanceAndMileage"><fmt:param value="saveSearchDistance('distanceSelect', ['IN_GROUP', 'ONLINE']);"/><fmt:param><fmt:message key="pref.distance"/></fmt:param></fmt:message>
	</div>
</div>