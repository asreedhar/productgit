<c:set var="showEdgeColumns" value="${sessionScope.nextGenSession.includeAgingPlan }"/>
<div id="overviewFloat" style="top:-100px; left:-100px; width:${showEdgeColumns?'780':'630' }px; height:113px;position:absolute;z-index:2000px;">
<iframe class="hideSelect ${isEdge?'large':'small' }" src="javascript:false;" frameBorder="0" scrolling="no"></iframe>
<div id="overviewHead" class="hdr">
	<span class="age">
		<fmt:message key="l.age"/>
	</span>
	<span class="year">
		<fmt:message key="l.year"/>
	</span>
	<span class="model">
		<fmt:message key="l.mtb"/>
	</span>
	<span class="color">
		<fmt:message key="l.color"/>
	</span>
	<span class="mileage">
		<fmt:message key="l.mileage"/>
	</span>
	<span class="cost">
		<fmt:message key="l.uc"/>
	</span>
	<c:if test="${showEdgeColumns}">
		<span class="bvc">
			<fmt:message key="l.bvc"/>
		</span>
	</c:if>
	<span class="pt">
			<fmt:message key="l.pt"/>
	</span>
	<span class="stockNum">
		<fmt:message key="l.snum.poundsign"/>
	</span>
	<c:if test="${showEdgeColumns}">
		<span class="objective">
			<fmt:message key="l.planObjective"/>
		</span>
	</c:if>
</div>
<div class="content ${showEdgeColumns?'large':'small' }">
<c:set var="count" value="0"/>
<c:forEach var="stock" items="${results}" varStatus="stat">
<c:set var="count" value="${count+1 }"/>
	<div>
		<span class="age">
			${stock.AgeInDays }
		</span>
		<span class="year">
			${stock.VehicleYear }
		</span>
		<span class="model">
			${stock.VehicleDescription }
		</span>
		<span class="color">
			${stock.Color }
		</span>
		<span class="mileage">
			<fmt:formatNumber maxFractionDigits="0" value="${stock.Mileage }"/>
		</span>
		<span class="cost">
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${stock.UnitCost }"/>			
		</span>
		<c:if test="${showEdgeColumns}">
			<span class="bvc ${stock.BookVsCost < 0?'red':'' }">
				<fmt:formatNumber type="currency" maxFractionDigits="0" value="${stock.BookVsCost }"/>	
			</span>
		</c:if>
		<span class="pt">
			${stock.TradeOrPurchaseDesc }
		</span>
		<span class="stockNum">
			<c:if test="${showEdgeColumns }"><a href="#" onclick="pop('<c:url value="oldIMT/EStock.go"><c:param name="stockNumberOrVin" value="${stock.StockNumber }"/><c:param name="fromIMP" value="false"/></c:url>','estock'); return false;"></c:if>
				${stock.StockNumber }
			<c:if test="${showEdgeColumns }"></a></c:if>		
		</span>
		<c:if test="${showEdgeColumns}">
			<span class="objective">
				${not empty stock.Strategy ? stock.Strategy:'No plan' }
			</span>
		</c:if>
	</div>
</c:forEach>
<c:if test="${count < 1 }">
<div class="center">
	<fmt:message key="equityAnalyzer.msg.emptyTableResults"/>
</div>
</c:if>
</div>
<div class="overviewFoot">
	<span class="right"><a href="#" onclick="Element.remove('overviewFloat');return false;"><fmt:message key="a.close"/></a></span>
	<c:if test="${not empty targetInv }">
		Target Inventory: <cite>${targetInv}</cite>
	</c:if>
	<c:if test="${not empty daysSupply }">
		Days Supply: <cite>${daysSupply}</cite>
	</c:if>
</div>
</div>





