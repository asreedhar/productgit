<tiles:importAttribute/>
<%-- page VARs //--%><c:if test="${slice eq 'model'}" var="getModel"/><c:if test="${slice eq 'vehicles'}" var="getVehicles"/><c:set var="pagingPosition" value="${empty param.position?requestScope.pagingPosition:param.position}" scope="page"/><c:if test="${pageId=='navigator'}" var="isNavigator" scope="request"/>
<%-- group VARs //--%><c:set var="PrimaryResultsGroups" value="${optimalInventorySearchSummaryForm.navigatorYearGrouping}" scope="page"/>
<fmt:message key="miles.minimum" var="milesMinimum"/>
<fmt:message key="miles.maximum" var="milesMaximum"/>

			<c:if test="${!getModel && !getVehicles}">		
<!-- begin RESULTS SET //////////////// -->
<div class="group fl_navigator">
	<!-- PRIMARY GROUP = header for whole group -->
	<div class="head">
	<c:if test="${optimalInventorySearchSummaryForm.navigatorHasBidList}"><a href="javascript:updateFrame('navigatorFrame','<fmt:message key="link.url.gmac.bidlist"/>');" class="bidListLink"><fmt:message key="link.text.viewBidList"/></a></c:if>
<%-- VARs //--%><c:set var="group_name" value="${optimalInventorySearchSummaryForm.navigatorSelectedMarketName}" scope="request"/>

<tiles:insert attribute="primary_group"/>

		<div class="distance">
		<label for="distanceDynSelect"><fmt:message key="l.searching"/>:</label>
<%-- VARs //--%><fmt:message key="distances.markets" var="distances"/><c:set var="distanceSelect" value="<select id=\"distanceSelect\" name=\"distanceSelect\" onchange=\"setYear();updateSearchDistance('distanceSelect', null, ['IN_GROUP', 'ONLINE'], 'modelSection', true, null, resetSelectedYear);\">"/>
	<c:forTokens items="${distances}" delims="," var="values"><c:if test="${values <= 1000}"><fmt:message var="msg" key="l.withinMiles"><fmt:param value="${values}"/></fmt:message></c:if><c:if test="${values > 1000}"><fmt:message var="msg" key="l.unlimited"/></c:if><c:set var="distanceSelect" value="${distanceSelect}<option value=\"${values }\"  ${optimalInventorySearchSummaryForm.maxDistanceFromDealerInGroup == values?'selected':'' }>${msg }</option>"/><c:if test="${optimalInventorySearchSummaryForm.maxDistanceFromDealerInGroup == values}"><c:set var="selectedValue" value="${msg}"/></c:if></c:forTokens>
	<c:set var="distanceSelect" value="${distanceSelect}</select>"/>
		<util:selectBox selected="${selectedValue}" id="searchDistance">${distanceSelect}</util:selectBox>
		<util:selectRange  milesMinSelected="${optimalInventorySearchSummaryForm.minMileageInGroup}" triggerRefresh="true" milesMaxSelected="${optimalInventorySearchSummaryForm.maxMileageInGroup}" uniqueId="navigator"/>
		</div>
	</div>
<c:if test="${!empty primary_subgroup}"><span class="head_sub">
<tiles:insert attribute="primary_subgroup" ignore="true"/>
</span></c:if>
<span id="modelSection">
			</c:if>
			<c:if test="${!getVehicles}">
<div class="body_wrap">
	<div class="body">
	<%-- group VARs //--%>
	<c:set var="group_name" value="${optimalInventorySearchSummaryForm.navigatorSelectedModel.nodeValue.name}" scope="request"/>
	<c:set var="total_results" value="${optimalInventorySearchSummaryForm.navigatorVehicleCount}" scope="request"/>
	<c:set var="groupingDescriptionId" value="${PrimaryResultsGroups.parent.parent.nodeValue.modelGroupID}" scope="request"/>
<tiles:insert attribute="secondary_group"/>
	</div>
<c:if test="${!empty secondary_subgroup}"><div class="body_sub">
<tiles:insert attribute="secondary_subgroup" ignore="true"/>
</div></c:if>	
</div>
	<div class="vert_line"></div>
<!-- begin vehicles_wrap ################ -->
<div class="vehicles_wrap" id="vehicles">
			</c:if>

<%-- group VARs //--%><c:set var="vehicles" value="${PrimaryResultsGroups.childNodes}" scope="page"/><c:set var="totalItems" value="${fn:length(vehicles)}" scope="page"/>
<c:if test="${totalItems eq 0}"><div class="left"><!-- placeholder --></div><div class="right"><!-- placeholder --></div></c:if>

<c:forEach items="${vehicles}" var="vehicle_item" varStatus="vehicle_loop"><%-- vehicle VARs //--%><c:set var="vehicle_object" value="${vehicle_item}" scope="request"/><c:set var="vehicle_count" value="${vehicle_loop.count}" scope="request"/>
<%-- paging VARs //--%><c:if test="${vehicle_loop.first || (vehicle_loop.count mod 6) eq 1}" var="startPage"/><c:if test="${vehicle_loop.last || (vehicle_loop.count mod 6) eq 0}" var="endPage"/><c:if test="${(vehicle_loop.count mod 6) eq 3}" var="endL"/><c:if test="${(vehicle_loop.count mod 6) eq 4}" var="startR"/>
<c:if test="${startPage}"><!-- begin VEHICLES PAGE ################ --><%-- paging VARs //--%><c:set var="selectedPosition" value="${empty pagingPosition?1:pagingPosition}" scope="page"/><c:set var="pageNum" value="${pageNum+1}"/><c:set var="startAtNum" value="${vehicle_loop.count}"/><c:if var="atEndOfPages" test="${vehicle_loop.count+5 ge totalItems}" scope="page"/><c:set var="endAtNum" value="${atEndOfPages?totalItems:vehicle_loop.count+5}"/><c:if var="atStartOfPages" test="${endAtNum le 6}" scope="page"/><c:if var="isActivePage" test="${(vehicle_loop.first && empty selectedPosition) || (selectedPosition ge startAtNum && selectedPosition le endAtNum)}" scope="page"/>
<span id="page${pageNum}"<c:if test="${!isActivePage}"> style="display:none;"</c:if>>
	<div class="paging">
		<c:if test="${!atStartOfPages}"><a href="javascript:getPreviousResultsPage(${pageNum});"><fmt:message key="a.previous"/></a></c:if>
${startAtNum}-${endAtNum} <fmt:message key="l.of"/> ${totalItems}
		<c:if test="${!atEndOfPages}"><a href="javascript:getNextResultsPage(${pageNum});"><fmt:message key="a.next"/></a></c:if>
	</div>
</c:if>
<c:if test="${startPage||startR}"><!-- begin LEFT/RIGHT -->
	<div class="${startR?'right':'left'}">
		<!-- begin vehicles = header table ################ -->
		<div class="vehicles">
<%-- VAR //--%><c:set var="SortByDiv" value="vehicles" scope="request"/><c:set var="SortByNode" value="${vehicle_item.parent.parent.id}" scope="request"/>
			<tiles:insert attribute="vehicles_group"/>
		</div>
		<!-- end vehicles ################ -->
		<!-- begin vehicles_sub ################ -->
		<div class="vehicles_sub"></c:if>
<%-- VAR //--%><c:if var="isSelectedItem" test="${vehicle_count eq selectedPosition}" scope="request"/>
<%-- VARs //--%><c:if var="isFirstItem" test="${startPage||startR}" scope="request"/><c:if var="isLastItem" test="${endL||endPage}" scope="request"/>
<%-- selected item VARs //--%>
<c:if test="${isSelectedItem}">
	<c:set var="frameUrl" scope="request">
		<c:choose>
			<c:when test="${!empty vehicle_item.nodeValue.inventoryId}">
				<c:url value="/ViewInGroupVehicle.go">
					<c:param name="inventoryId" value="${vehicle_item.nodeValue.inventoryId}"/>
				</c:url>
			</c:when>
			<c:otherwise>${vehicle_item.nodeValue.url}</c:otherwise>
		</c:choose>
	</c:set>
	<c:set var="theVehicle" value="${vehicle_item.nodeValue}"/>
	<c:set var="bookoutVin" value="${theVehicle.VIN}"/>
	<c:set var="bookoutMileage" value="${theVehicle.mileage}"/>
	<c:set var="bookoutMake" value="${theVehicle.model.make.name}"/>
	<c:set var="bookoutModel" value="${theVehicle.model.name}"/>
	<c:set var="bookoutVehicleYear" value="${theVehicle.modelYear.modelYear}"/>
	<c:url var="bookValuesParams" value=""><c:param name="vin" value="${bookoutVin}"/><c:param name="mileage" value="${empty bookoutMileage?0:bookoutMileage}"/><c:param name="make" value="${bookoutMake}"/><c:param name="model" value="${bookoutModel}"/><c:param name="vehicleYear" value="${bookoutVehicleYear}"/><c:param name="context" value="search"/></c:url>
	<c:set var="analyticsInputs" scope="page">
		<!-- analytics modules rely on these values -->
		<input type="hidden" id="performanceParams" value="make=${vehicle_item.nodeValue.model.make.name}&model=${vehicle_item.nodeValue.model.name}&segment=${vehicle_item.nodeValue.model.segment.name}&trim=<c:out value="${vehicle_item.nodeValue.trim.name}"/>&vin=${vehicle_item.nodeValue.VIN}&groupingDescriptionId=${vehicle_item.nodeValue.groupingDescriptionId}&mileage=${empty vehicle_item.nodeValue.mileage?0:vehicle_item.nodeValue.mileage}&year=${vehicle_item.nodeValue.modelYear.modelYear}&comingFrom=${pageId}&vehicleEntityTypeId=${vehicle_item.nodeValue.vehicleEntityTypeId}&vehicleEntityId=${vehicle_item.nodeValue.vehicleEntityId}"/><input type="hidden" id="auctionDataParams" value="makeModelGroupingId=${vehicle_item.nodeValue.makeModelGroupingId}&year=${vehicle_item.nodeValue.modelYear.modelYear}&mileage=${empty vehicle_item.nodeValue.mileage?0:vehicle_item.nodeValue.mileage}&vin=${vehicle_item.nodeValue.VIN}"/>
		<input type="hidden" id="bookValuesParams" value="${bookValuesParams}"/>
	</c:set>
</c:if>
<tiles:insert attribute="vehicles_items"/>
<c:if test="${endL||endPage}"></div>
		<!-- end vehicles_sub ################ -->
	</div>
<!-- end LEFT/RIGHT --></c:if>
<c:if test="${vehicle_loop.last && ((vehicle_loop.count mod 6) le 3) && (vehicle_loop.count mod 6) ne 0}"><div class="right"><!-- placeholder --></div></c:if>
<c:if test="${endPage}"></span>
<!-- end VEHICLES PAGE ################ --></c:if>
</c:forEach>
<%-- set in loop above for selected item--%>${analyticsInputs}
<c:if test="${!getVehicles}"></div>
<!-- end vehicles_wrap ################ -->
	<c:if test="${!getModel && !getVehicles}"></span></c:if>
</div>
<!-- end RESULTS SET //////////////// --></c:if>