<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>

<c:if test="${pageId=='flashSummary'}" var="isFlashSummary" scope="request"/>
<div id="search" class="group search flash">
	<div class="head">
		<h5><fmt:message key="l.flashLocate"/> <fmt:message key="l.search"/></h5>
	</div>
	<div class="body">
<form name="optimalInventorySearchSummaryForm" id="optimalInventorySearchSummaryForm" action="<c:url value="/FlashLocateSummary.go"/>" method="POST">
		<fieldset>
			<dl class="make">
					<dt><label for="makeInput"><fmt:message key="l.make"/>:</label></dt>
					<dd>
						<select name="makeText" id="makeTextSelect" class="make" onkeypress="submitOnEnter('runFlashLocate');" tabindex="1">
							<option value="">Please select a Make...</option>
							<c:forEach items="${makes}" var="make">
								<c:choose>
								<c:when test="${fn:toUpperCase(make.name) eq fn:toUpperCase(optimalInventorySearchSummaryForm.makeText)}">
									<option value="${make.name}" selected="selected">${make.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${make.name}">${make.name}</option>
								</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</dd>
				</dl>				
		</fieldset>
		<fieldset>
		<dl class="model">
					<dt><label for="modelInput"><fmt:message key="l.model"/>:</label></dt>
					<dd><input type="text" name="modelText" id="modelInput" onkeypress="submitOnEnter('runFlashLocate');" class="model" value="${optimalInventorySearchSummaryForm.modelText}"/></dd>
				</dl>
		</fieldset>
		<fieldset>
				<dl class="trim">
					<dt><label for="trimInput"><fmt:message key="l.trim"/>:</label></dt>
					<dd><input type="text" name="trim" id="trimInput" onkeypress="submitOnEnter('runFlashLocate');" class="trim" value="${optimalInventorySearchSummaryForm.trim}"/></dd>
				</dl>
		</fieldset>
		<fieldset class="years"><!-- start years -->
			<dl class="range">
				<dt><label for="yearBeginSelect"><fmt:message key="l.years"/>:</label></dt>
				<dd>
					<select name="yearBegin" id="yearBeginSelect" class="year" onkeypress="submitOnEnter('runFlashLocate');" onchange="matchSelects(this,$('yearEndSelect'));">
						<firstlook:yearsOptions selectedValue="${optimalInventorySearchSummaryForm.yearBegin}" />
					</select>
				</dd>
			</dl>
			<dl>
				<dt><em><label for="yearEndSelect"><fmt:message key="l.to"/></label></em></dt>
				<dd>
					<select name="yearEnd" id="yearEndSelect" class="year" onkeypress="submitOnEnter('runFlashLocate');">
						<firstlook:yearsOptions selectedValue="${optimalInventorySearchSummaryForm.yearEnd}"/>
					</select>			
				</dd>
			</dl>	
		</fieldset><!-- end years -->
		<fieldset class="markets">		
			<ul>
				<li><input type="radio" name="selectedMarket" id="inGroup" onkeypress="submitOnEnter('runFlashLocate');" value="IN_GROUP" ${optimalInventorySearchSummaryForm.selectedMarket eq "IN_GROUP"?'checked':''}/><label for="inGroupRadio"><fmt:message key="pref.inGroupOnly"/></label></li>
				<li><input type="radio" name="selectedMarket" id="online" onkeypress="submitOnEnter('runFlashLocate');" value="ONLINE" ${optimalInventorySearchSummaryForm.selectedMarket eq 'ONLINE'?'checked':''}/><label for="marketsRadio"><fmt:message key="pref.marketsOnly"/></label></li>
				<li><input type="radio" name="selectedMarket" id="inGroupAndOnline" onkeypress="submitOnEnter('runFlashLocate');" value="ALL" ${optimalInventorySearchSummaryForm.selectedMarket eq "ALL" || optimalInventorySearchSummaryForm.selectedMarket == null?'checked':''}/><label for="allRadio"><fmt:message key="pref.all"/></label></li>
			</ul>
		</fieldset>
		<div class="submit">
			<util:button>
				<a href="#" onclick="runFlashLocate();" onkeypress="submitOnEnter('runFlashLocate');" id="runFlashLocate"><fmt:message key="a.locate"/></a>
			</util:button>	
		</div>
</form>
	</div>
</div>