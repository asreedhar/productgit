//good luck.

//responsible for setting proper class name for selected year
function handleModelYearDisplay(yearObj,resultsId,state) {
	var allIsSelected = state.isAll; //ALL YEARS is selected
	var turnOnAllYears = !state.allIsActive;
	//**** sets 'all' class in overall YEARS CONTAINER
	var modelYearsObj = $('modelyears_'+resultsId);
	if(turnOnAllYears && allIsSelected) {
		Element.addClassName(modelYearsObj,'all'); //insert 'all' classname
	} else {
		Element.removeClassName(modelYearsObj,'all'); //remove 'all' classname
	}
	var yearWithNoResults = Element.hasClassName(yearObj,'none');
	var proceed = yearWithNoResults ? false:true;
	//**** transfers ALL YEARS designation to YEARS with results
	if(state.isAllYearsSearch && !yearWithNoResults) {
		handleAllYearsSearchDisplay(yearObj,resultsId,state);
		if(state.isOpt || state.isNew) {
			proceed = false; //do not toggle SELECTED YEAR - just reset it
		}
	}
	var allYearsObj = $('year_'+resultsId+'_all');	
	var selClass = yearObj.className;
	//**** reset ALL YEARS
	if(state.allIsActive) { 
		years.restore(); //????? this may not be the best interaction
		var origClass = years.original[allYearsObj.id];
		allYearsObj.className = origClass == 'all' ? origClass:origClass+'_off';
		if(state.isAllYearsSearch) {
			proceed = false;
		}
		else {
			proceed = origClass.search('_off')!= -1 || selClass == '';
		}
	}
	if(state.isAllYearsSearch && !state.allIsActive) {
		proceed = !yearWithNoResults;
	}
	if(!proceed) { 
		return; //do not need to toggle SELECTED YEAR
	}
	var actClass = state.isOpt ? 'opt':state.isNew ? 'new':'';
	//**** sets the classname for the SELECTED YEAR
	if(actClass != '') {
		var prevNotSearched = !years.original[yearObj.id] || years.original[yearObj.id] == '';
		if(allIsSelected) { 
			prevNotSearched = years.original[yearObj.id] == 'all';
			actClass = 'all'+actClass; //prepend 'all' to actClass since ALL YEARS was selected
		}
		if(prevNotSearched && !state.isOff) {
			yearObj.className = allIsSelected ? 'all':'';
		} else {
			Util.toggleClassNames(yearObj,actClass,actClass+'_off');
		}
	} else { //previously unsearched (has no class name)
		var newClass = state.isOff ? 'new':'';
		if(allIsSelected) { 
			newClass = 'all'+newClass; //prepend 'all' to actClass since ALL YEARS was selected
		}
		yearObj.className = newClass;
	}
}

function handleAllYearsSearchDisplay(yearObj,resultsId,state) { 
	if(Element.hasClassName(yearObj,'none')) {
		if(!state.isAll) { return; }
	}
	var yearsObj = Y[resultsId];	
	if(yearsObj.transferred) {
		 return;
	}
	var allYearsObj = $('year_'+resultsId+'_all');
	var allClass = flagOpt(allYearsObj) ? 'opt' : 'new';
	//transfer search designation to years with results
	var yearsWithResults = {};
	_n.times(function(i){
		var groupId = resultsId+'.'+(i+1);
		var years = getYearTotals(groupId);
		$H(years).each(function(year){
			if(!yearsWithResults[year.key]) { 
				var newClass = allClass;
				var yearObj; yearObj = $('year_'+resultsId+'_'+year.key);
				if(yearObj) {
					var alreadySet = Element.hasClassName(yearObj,allClass) || Element.hasClassName(yearObj,allClass+'_off');
					if(alreadySet || yearObj.className != '') { 
						if(yearObj.className != '') {
							newClass = flagOpt(yearObj) ? 'opt':'new';
						}
						Util.toggleClassNames(yearObj,newClass,newClass+'_off');
					} else {
						Element.addClassName(yearObj,newClass);
					}
					yearsWithResults[year.key] = year.value;
				}
			}			
		});
	});

	//reset original values to transferred values
	var modelYearsObj = $('modelyears_'+resultsId);
	var yearEls = modelYearsObj.getElementsByTagName('li');
	$A(yearEls).each(function(el,i) {
		var skipIt = flagAll(el.id); //skip ALL YEARS
		if(!skipIt) {
			var currClass = $(el.id).className;
			var origObj = Y[resultsId];
			var origClass = origObj.original[el.id];
			var tip;
			if(currClass == '') { // year has NO results //add 'none' and prepend title
				tip = 'No Vehicles Available | '+$(el.id).title;
				$(el.id).title = tip;
				origObj.original[el.id] = 'none';
				$(el.id).className = 'none';
			} else if(currClass.search('_off') != -1) {
				tip = 'No Vehicles Available | '+$(el.id).title;
				$(el.id).title = tip;
				origObj.original[el.id] += ' none';
				Element.addClassName(el.id,'none');
			} else {
				origObj.original[el.id] = currClass;
			}
		}
	});
	yearsObj.transferred = true;
}
function handleNonSearchedYears(resultsId,selYear,state) {
	if(state.isAllYearsSearch) {
		return false;
	}
	var yearObj = $('year_'+resultsId+'_'+selYear);
	var origObj;
		origObj = Y[resultsId];
	var origClass = origObj ? origObj.original[yearObj.id]:'';

	var proceed = origClass == '' || origClass == 'all' ? true:false;
	if(!proceed && origClass.search('_off') != -1) {
		proceed = true;
	}
	var hiddenObj = $('addedYears_'+resultsId); //grab added year values from hidden field
	var hiddenValue = $F(hiddenObj);
	if(state.allIsActive || (state.isAll && !state.allIsActive)) {
		hiddenValue = '';
	}
	var brandNewYear = false;
	var yearsAdded = hiddenValue == ''? []:hiddenValue.split(',');
	if(proceed) {
		brandNewYear = true;
		var addIt = state.isOff;
		if(addIt) {
			yearsAdded.push(selYear);
		} 
		else {
			var selPos;
			yearsAdded.each(function(year,i) {
				if(year == selYear) { selPos = i; }
			});
			yearsAdded.splice(selPos,1);
		}
	}
	hiddenObj.value = yearsAdded.toString();
	var isEmpty = yearsAdded.length == 0;
	handleUpdateButton(resultsId,!isEmpty);
	return brandNewYear;
}
function handleVehicleResultsDisplay(resultsId,selYear,state) {
	if(state.allIsActive) {
		return false;
	}
	if(state.isAll) {
		//open all results
		_n.times(function(index){
			var count = index+1;
			var groupId = 'group_'+resultsId+'.'+count;
			var yearCont = $(groupId);
			var els = yearCont.getElementsByTagName('div');			
			$A(els).each(function(el) {
				var id;
				id = el.id;
				if(id.indexOf('vehicles_'+resultsId+'.'+count+'_') != -1) {
					Element.show(id);
				}
			});
		});
	} else {
		//toggle year results
		_n.times(function(index){
			var count = index+1;
			var yearGrpId = 'vehicles_'+resultsId+'.'+count+'_'+selYear;
			if($(yearGrpId)) {
				Element.toggle(yearGrpId);
			}
		});	
	}
}

function handleModelYearSelection(resultsId,selYear) {
	if(!_n) { 
		_n = parseInt($F('totalMarkets'), 10);
	}
	var state = setSelectedYearState(resultsId,selYear); //returns obj with current state of 'selected year' and 'all years'
	var yearId = 'year_'+resultsId+'_'+selYear;
	var yearObj = $('year_'+resultsId+'_'+selYear); var allYearsObj = $('year_'+resultsId+'_all');
	var hasNoResults = parseInt($F('total_'+resultsId), 10) == 0;
	if(hasNoResults && state.isAllYearsSearch) {
		return; //there is nothing more to get
	}
	if(!Y[resultsId]) { //sets and stores original state of model years
		years = new Years(resultsId);
		Y[resultsId] = years;
	}
	years = Y[resultsId];
	var allYearsOff = Element.hasClassName(allYearsObj, 'allopt_off');
	if (allYearsOff){years.transferred = true;}
	handleModelYearDisplay(yearObj,resultsId,state);

	var brandNew = handleNonSearchedYears(resultsId,selYear,state);
	if(hasNoResults || (!state.isAll && state.allIsActive)) {
		return;
	}
	if(!T[resultsId]) {
		var totals = new Totals(resultsId);
		T[resultsId] = totals;
	}
	var subTotals;
	if(!brandNew || (state.isAll && !state.allIsActive)) {
		if(state.isAll) {
			subTotals = restoreOriginalTotals(resultsId);
		}
		else {
			var doAdd = state.isOff;
			subTotals = updateResultsTotals(resultsId,selYear,doAdd); //returns subtotals array in order to close 0 results groups
		}
		handleVehicleResultsDisplay(resultsId,selYear,state);
		subTotals.each(function(total,i){
			var count = i+1;
			var groupId = 'group_'+resultsId+'.'+count;
			if(total == 0) {
				$(groupId).className = 'none';
			} else {
				var isNone = Element.hasClassName(groupId,'none');
				if(isNone) {
					var results = T[resultsId];
					var mkt = results.markets[count];
					var wasExp = mkt.expanded;
					var newClass = wasExp ? 'expanded':'collapsed';
					Util.toggleClassNames(groupId,newClass,'none');
				}			
			}
		});	
	}
	handleToggleLink(resultsId);
	return;
}
function setSelectedYearState(resultsId,selYear) {
	var yearObj = $('year_'+resultsId+'_'+selYear);
	var allYearsObj = $('year_'+resultsId+'_all');
	var allYearsHidden = $F('allYears_'+resultsId);	
 	var state = { 
  		isAllYearsSearch: (allYearsHidden && allYearsHidden != 'opt_off') ? true:false,
 		isAll: selYear == 'all',
		allIsActive: Element.hasClassName(allYearsObj,'allopt') || Element.hasClassName(allYearsObj,'allnew'),
 		isOpt: flagOpt(yearObj),
 		isNew: flagNew(yearObj)
  	};
 	state.isOff = (!state.isOpt && !state.isNew) || flagOff(yearObj);
	return state;
}

var _n;
var T = {}; //holder for all totals
var Totals = Class.create();
Totals.prototype = {
    initialize: function(resultsId){
		if(!_n) { 
			_n = parseInt($F('totalMarkets'), 10);
		}
		this.total = parseInt($('total_'+resultsId).value, 10);
		if(this.total > 0) { 
			this.markets = getMarketTotals(resultsId);
		}
	}
};
var Y = {}; //holder for original years state
var years;
var Years = Class.create();
Years.prototype = {
    initialize: function(resultsId){
    	this.id = resultsId;
    	var orig = {};
 		var yearsCont = $('modelyears_'+resultsId);			
		var yearEls = yearsCont.getElementsByTagName('li');
		$A(yearEls).each(function(el,i) {
			orig[el.id] = $(el.id).className;
		});
		this.original = orig;
	},
	restore: function() {
 		var yearsCont = $('modelyears_'+this.id);			
		var yearEls = yearsCont.getElementsByTagName('li');
		$A(yearEls).each(function(el,i) {
			var obj = years;
			$(el.id).className = years.original[el.id];
		});
	}
};

function clearYearsQueues(id) {
	if(T[id]) {
		delete T[id];
	}
	if(Y[id]) {
		years = '';
		delete Y[id];
	}
}
function resetYearsQueues() {
	T = {};
	Y = {};
	_n = '';
}

function updateModelTotal(id,value) {
	$('modelTotal_'+id).innerHTML = value;
}
function updateMarketTotal(id,value) {
	$('marketTotal_'+id).innerHTML = value;
}
function getMarketTotals(id){
	var markets = {};
	_n.times(function(i) {
		var count = i+1;
		var groupId = id+'.'+count;
		var mktTotal = parseInt($F('total_'+groupId), 10);
		if(mktTotal > 0) {
			var market = { 
				expanded: Element.hasClassName('group_'+groupId,'expanded'),
				total: mktTotal,
				years: getYearTotals(groupId)
			};
		}
		markets[count] = market;
	});
	return markets;
}
function getYearTotals(id){
	var y = {};
	var form; form = $('form_'+id);
	var inputs = []; inputs = Form.getElements(form);
	$A(inputs).each(function(input) {
		y[input.name] = parseInt(input.value, 10);
	});
	return y;
}
function updateResultsTotals(resultsId,selYear,addThem) {
	var totals;
	var isAll = selYear == 'all';

	totals = T[resultsId];
	var subTotals = [];
	_n.times(function(i){
		var count = i+1;
		var groupId = resultsId+'.'+count;
		var mktObj,mktTotal,newMktTotal,yearTotal;
			mktObj = totals.markets[count];		
		if(!mktObj) {
			mktTotal = 0;
			newMktTotal = 0;
			yearTotal = 0;
		} else {
			mktTotal = mktObj.total;
			if(!isAll) {
				yearTotal = mktObj.years[selYear];
				if(!yearTotal) {
					newMktTotal = mktObj.total;
					yearTotal = 0;			
				}
			}
			else {
				newMktTotal = mktTotal;
				yearTotal = 0;
				updateMarketTotal(groupId,newMktTotal);
			}
		}
		if(yearTotal != 0) {
			newMktTotal = addThem ? mktTotal + yearTotal : mktTotal - yearTotal;
			updateMarketTotal(groupId,newMktTotal);
			mktObj.total = newMktTotal;
		}
		subTotals.push(newMktTotal);
	});
	var grandTotal = 0;
	subTotals.each(function(t){
		grandTotal += t;
	});
	totals.total = grandTotal; //total results number
	updateModelTotal(resultsId,grandTotal);
	return subTotals;
}
function restoreOriginalTotals(resultsId){
	var subTotals = [];
	var id = resultsId;
	var tObj = T[resultsId];
	var modelTotal = parseInt($('total_'+id).value, 10);
		tObj.total = modelTotal;
	var mktObj; mktObj = tObj.markets;
	updateModelTotal(id,modelTotal);
	_n.times(function(i) {
		var count = i+1;
		var mktTotal;
		if(mktObj[count]) {
			var groupId = id+'.'+count;
			mktTotal = parseInt($('total_'+groupId).value, 10);
			var m = mktObj[count];
				m.total = mktTotal;
		updateMarketTotal(groupId,mktTotal);
		} else {
			mktTotal = 0;
		}
		subTotals.push(mktTotal);
	});
	return subTotals;
}

function handleUpdateButton(resultsId,showIt) {
	var button = $('update_'+resultsId);
	if(showIt) {
		Element.show(button);
	} else {
		Element.hide(button);
	}
}
function flagOpt(obj) {
	var isOpt = Element.hasClassName(obj,'opt') || Element.hasClassName(obj,'opt_off') || Element.hasClassName(obj,'allopt') || Element.hasClassName(obj,'allopt_off');
	return isOpt;
}
function flagNew(obj) {
	var isNew = Element.hasClassName(obj,'new') || Element.hasClassName(obj,'new_off') || Element.hasClassName(obj,'allnew') || Element.hasClassName(obj,'allnew_off');
	return isNew;
}
function flagAll(obj) {
	var isAll = Element.hasClassName(obj,'all') || Element.hasClassName(obj,'allnew') || Element.hasClassName(obj,'allopt') || Element.hasClassName(obj,'allnew_off') || Element.hasClassName(obj,'allopt_off');
	return isAll;
}
function flagOff(obj) {
	var isOff = $(obj).className == 'all' || (Element.hasClassName(obj,'opt_off') || Element.hasClassName(obj,'new_off') || Element.hasClassName(obj,'allopt_off') || Element.hasClassName(obj,'allnew_off'));
	return isOff;
}

function removeFromAddedQueue(resultsId,selYear) {
	updateAddedQueue(resultsId,selYear,'remove');
}
function addToAddedQueue(resultsId,selYear) {
	updateAddedQueue(resultsId,selYear,'add');
}
function isAddedToQueue(resultsId,selYear) {
	var added = updateAddedQueue(resultsId,selYear,'return');
	return added;
}
function updateAddedQueue(resultsId,selYear,doWhat) {
	var hiddenObj = $('addedYears_'+resultsId); //grab added year values from hidden field
	var hiddenValue = $F(hiddenObj);
	if(doWhat == 'return' && hiddenValue == '') {
		return false;
	} 
	var yearsAdded = hiddenValue == ''? []:hiddenValue.split(',');
	if(doWhat == 'add') {
		yearsAdded.push(selYear);
	} 
	else {
		var addedPos;
		yearsAdded.each(function(year,i) {
			if(year == selYear) { addedPos = i; }
		});
		if(doWhat == 'return') {
			var isAdded = addedPos >= 0 ? true:false;
			return isAdded;
		} 
		else {
			yearsAdded.splice(addedPos,1);
		}	
	}
	hiddenObj.value = yearsAdded.toString();
	var isEmpty = yearsAdded.length == 0;
	handleUpdateButton(resultsId,!isEmpty);
}