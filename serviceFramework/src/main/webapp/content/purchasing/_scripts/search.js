
function updateModelResults(resultsId) {
	clearYearsQueues(resultsId);
	var inlineEl;
		inlineEl = $('distanceSelect_'+resultsId);
	var isInline;
	var inlineSelValue = inlineEl?inlineEl.value:parseValue($('distanceSelectLink_'+resultsId));
	var successAction;
	var minMiles = $('mileageMinId'+resultsId).value;
	var maxMiles = $('mileageMaxId'+resultsId).value;
	var minMilesGlobal =$('mileageMinIdglobal').value;
	var maxMilesGlobal = $('mileageMaxIdglobal').value;
	var globalDistance = $('distanceSelect').value;
	var radiusLink = 'distanceSelectLink_'+resultsId;
	var rangeLink = 'rangeSelect_'+resultsId;
	
	if(inlineEl) {
		inlineEl.disabled = true;
		isInline = true;
		inlineSelIndex = inlineEl.selectedIndex;
		inlineSelText = inlineEl.options[inlineSelIndex].text;
	
		successAction = function() {			
			if(globalDistance != inlineSelValue){
				Element.addClassName(radiusLink,'white');				
			}else{
				Element.removeClassName(radiusLink,'white');
			}
			if(minMiles!=minMilesGlobal || maxMiles!=maxMilesGlobal){
				Element.addClassName(rangeLink,'white');
			
			}else{
				Element.removeClassName(rangeLink,'white');
			}				
			var changeAction = 'updateModelResults('+resultsId+');';
			var blurAction = 'hideDistanceSelect('+resultsId+');';
			selectEl = ['<select id="distanceSelect_'+resultsId+'" onchange="'+changeAction+'" onblur="'+blurAction+'">','','</select>'];
			
			var options = $('distanceSelect').innerHTML;
			options = options.replace('selected',''); 	
		
			var index = options.indexOf(inlineSelText); //find selected element based on visible option text
			var start = options.substring(0,index-1);
			var end = options.substring(index-1,options.length);
			
			options = start+' selected '+end;
			selectEl.splice(1,1,options);
			selectEl = selectEl.join('');
			new Insertion.After('dynSelect_'+resultsId,selectEl);
			
	    	Element.hide('distanceSelect_'+resultsId);			
		}
	}else{ //mileage range was updated, not radius
	
		successAction = function() {	
			if(globalDistance != inlineSelValue){
				Element.addClassName(radiusLink,'white');
			}else{
				Element.removeClassName(radiusLink,'white');
			}
			if(minMiles!=minMilesGlobal || maxMiles!=maxMilesGlobal){
				Element.addClassName(rangeLink,'white');
			
			}else{
				Element.removeClassName(rangeLink,'white');
			}
		}
	}
  	var map = {
		ele:          'results_'+resultsId,
		path:         window.location.pathname.replace(/.go$/, 'HotlistUpdateResults.go'),
		msg:          'Updating...',
		distance:     getDistanceParams(resultsId),
		minMileage:minMiles,
		maxMileage:maxMiles,
		markets:      'marketElements',
    	marketTypes:   ['IN_GROUP','ONLINE'], 
		options:      getInlineModelYearParams(resultsId),
		selectedNode: resultsId,
		onComplete: 	successAction,
		unmodifiableMemberMarket:'true'
	};
	makeRequest(map);
}
function parseValue(el){

	var text = $(el).innerHTML;
	
	var value = text.substring(text.indexOf(' ')+1, text.lastIndexOf(' '));
	

	return value;

}
function getInlineModelYearParams(resultsId) {
	var params = [];
	var cont = $('modelyears_'+resultsId);
	var yearEls = cont.getElementsByTagName('li');
	var amp = '';
	yearEls = $A(yearEls);
	yearEls.each(function(el,i) {
		var isActive = Element.hasClassName(el.id,'new') || Element.hasClassName(el.id,'opt') || Element.hasClassName(el.id,'allopt') || Element.hasClassName(el.id,'allnew');
		var idStr = el.id;
		var iter = 0;
		if(isActive) {
			var prefixLength = ('year_'+resultsId+'_').length; //prefixed with year_[id]_
			//get year value
			var yearValue = idStr.substring(prefixLength);
			var yearText = yearValue;
			if(yearValue == 'all'){
				yearText = 'null';
			}
			if(iter == 0){
				amp='&';
			}
			iter++;
var paramYear = amp+'searchCandidateCriteria['+i+'].modelYearText='+yearText;
var paramAnnotation = 'searchCandidateCriteria['+i+'].annotationText=OneTime';
var paramExpires = 'searchCandidateCriteria['+i+'].expiresText='+false;

			var hotListExp;
			if($('hlExpiration_'+resultsId+'_'+yearValue)) {
				hotListExp = $F('hlExpiration_'+resultsId+'_'+yearValue);
				paramAnnotation = 'searchCandidateCriteria['+i+'].annotationText=HotList';
				if(hotListExp != '') {
					paramExpires = 'searchCandidateCriteria['+i+'].expiresText='+hotListExp;
				}
			}
			params.push(paramYear,paramAnnotation,paramExpires);
		}
	});
	var amp = '';
	if(params.length==0){
		amp = '&';
	}
	var models = $F('selectedTrims_'+resultsId).split(',');  //grabs the selected model trims from a hidden field associated with each model
	for(var i =0; i < models.length; i++){
		if(models[i] != ''){
			params.push(amp+'selectedModels['+i+']='+models[i]);
		}
	}
	
	var retVal = params.join('&');
	
	return retVal;
}
function getDistanceParams(resultsId) {
	var inlineEl;
		inlineEl = $('distanceSelect_'+resultsId);
	var prefValue = $F('distanceSelect');
	var inlineValue; 
	if(inlineEl) {
		inlineValue = $F(inlineEl);
		return inlineValue;
	} else {
		return prefValue;
	}
}
function showDistanceSelect(resultsId) {
	var selectEl;
		selectEl = $('distanceSelect_'+resultsId);
	var trigEl = $('dynSelect_'+resultsId);
	if(selectEl) {
		Element.toggle(trigEl);
		Element.toggle(selectEl);
		selectEl.focus();
		return;
	}
	var changeAction = 'updateModelResults('+resultsId+');';
	var blurAction = 'hideDistanceSelect('+resultsId+');';
		selectEl = ['<select id="distanceSelect_'+resultsId+'" onchange="'+changeAction+'" onblur="'+blurAction+'">','','</select>'];
	var options = $('distanceSelect').innerHTML;
	selectEl.splice(1,1,options);
	selectEl = selectEl.join('');
	new Insertion.After('dynSelect_'+resultsId,selectEl);
	Element.hide('dynSelect_'+resultsId);
	$('distanceSelect_'+resultsId).focus();
}
function hideDistanceSelect(resultsId) {
	var trigEl = $('dynSelect_'+resultsId);
	var selectEl = $('distanceSelect_'+resultsId);
	Element.toggle(trigEl);
	Element.toggle(selectEl);
}

function showUpgradeInformation(linkId) {
	var upgradeEl = $('navUpgrade');
	if(!Element.visible(upgradeEl)) {
		Element.show(upgradeEl);
	} 
	var clickedObj = $(linkId);
	var clickCoords = Util.findPosition(clickedObj);
	var elHeight = Element.getHeight(upgradeEl);
	upgradeEl.style.top = clickCoords[1]-elHeight + 'px';
}
function hideUpgradeInformation() {
	Element.hide('navUpgrade');
}



/*
*Expands or hides all markets takes in the model block id and the inner html value of the toggle link (view all or hide all)
*/
function toggleAllResultsGroups(resultsId) {
	if($('viewAll_'+resultsId)) {
		var areAllOpen = Element.visible('hideAll_'+resultsId);
		var classStr = areAllOpen ? 'collapsed':'expanded';

		Element.toggle('hideAll_'+resultsId);
		Element.toggle('viewAll_'+resultsId);
	}
	var cont = $('results_'+resultsId);
	var elements = cont.getElementsByTagName('span');
	elements = $A(elements);
	var i = 0; 
	elements.each( function(el){
		if(el.id) {
			if(el.id.indexOf('group_'+resultsId,0) != -1) {
				i += 1;
				var alreadySet = Element.hasClassName(el.id,classStr) || Element.hasClassName(el.id,'none');
				if(!alreadySet) { 
					toggleResultsGroup(resultsId+'.'+i,true);
				}
			};
		}
	});
}
function toggleResultsGroup(targetId,toggleAll){
	var el = $('group_'+targetId);
	Util.toggleClassNames(el,'collapsed','expanded');
	var splitAt = targetId.indexOf('.');
	var resultsId = targetId.substring(0,splitAt); //prefixed with 'group_' & suffixed with '_targetId'

	var t;
	//update totals obj with current expanded state
	if(!T[resultsId]) {
		t = new Totals(resultsId);
		T[resultsId] = t;
	}
	t = T[resultsId];
	var mktPos = targetId.substring(splitAt+1);
	var mkt = t.markets[mktPos];
	mkt.expanded = Element.hasClassName(el,'expanded');

	if(!toggleAll) {
		handleToggleLink(resultsId);
	}
}
function handleToggleLink(resultsId) {
	var cont = $('results_'+resultsId);
	var allAreOpen = true;
	var allAreClosed = true;
	var allAreEmpty = true;
	var elements = cont.getElementsByTagName('span');
	elements = $A(elements);
	elements.each( function(el){
		if(el.id && el.id.indexOf('group_'+resultsId,0) != -1 && !Element.hasClassName(el.id,'none')) {
			var isEmpty = Element.hasClassName(el.id,'none');
			if(isEmpty == false) allAreEmpty = false;
			var isOpen = Element.hasClassName(el.id,'expanded');
			if(isOpen == false) allAreOpen = false;
			var isClosed = Element.hasClassName(el.id,'collapsed');
			if(isClosed == false) allAreClosed = false;
		};
	});
	if(allAreOpen) { 
		Element.show('hideAll_'+resultsId);
		Element.hide('viewAll_'+resultsId); 
	}
	if(allAreClosed) { 
		Element.hide('hideAll_'+resultsId);
		Element.show('viewAll_'+resultsId); 
	}
	if(allAreEmpty) { 
		Element.hide('hideAll_'+resultsId,'viewAll_'+resultsId);
	}
}


/*
 * Validates the selection of a make and model and submits those to a SEARCH action which
 * runs an incremental search for the vehicles of that make and model.
 */
function addSearchCandidate() {

	var idx0 = $('makeSelect').value;
	var idx1 = $('modelSelect').value;

	if (idx0 != '' && idx1 !='' ) {
		var params = 'make=' + idx0 + '&model=' + idx1;
		/* Marshall the data from the one-time search div into these vars */
		if($('addToHotlistCheck').checked){
			var days = $('hotListDurTextGlobal').value;
			var date = 'false';
			if ($('alwaysRadioGlobal').checked == false) {
				date = calculateDateFromDays(days);
			}
			params += '&searchCandidateCriteria[0].modelYearText=null';    // always null
			params += '&searchCandidateCriteria[0].expiresText='+date;     // or date 'yyyy-MM-dd'
			params += '&searchCandidateCriteria[0].annotationText=HotList'; // or 'OneTime'
		}
		
		 	
		 	var s = function success(r,j){
			 	resetSearchFields();
			 	var resp = eval( '(' + r.getResponseHeader( 'X-JSON' ) + ')' ); 
			 	     if(eval(resp.updated) || !eval(resp.added)){  //remove the existing element if this is false
			 	 
			 	     	 var model  = $('results_'+resp.groupByNodeID); //store the element
			 	
			 	  		 Element.remove(model);		 	   
			 	     }  	
			 	  	
			 	 new Insertion.Top($('results'),r.responseText); //insert new element		 	    
				var numbers = resp.OptimalItems + ' Optimal Inventory searches; ' + resp.HotListItems + ' Hot List searches';
				$('totalsNumbers').innerHTML = numbers;
			
				$('totalsDate').innerHTML =Util.getFormattedDate('M j, Y h:i A');
			};
	
		
		
		var map = {
		path:         window.location.pathname.replace(/.go$/, 'AddSearchCandidate.go'),
		msg:          'Searching...',
		options:      params,
		onSuccess: s,
		onFailure : s
		
		};
	return makeRequest(map);
		
	}
	else {
		alert('You must select a make and model to search.');
	}
	return false;
}


//matches the selected index from sel1 to that of sel2
function matchSelects(sel1, sel2){	
		$(sel2).selectedIndex = $(sel1).selectedIndex;
}

//given a set number of days from today this method returns the date 
function calculateDateFromDays(days){
			
						
						var val = days;
						var DAY = 1000 * 60 * 60 * 24; //milliseconds in a day
						var today = new Date();
						var newDate = new Date();
						var milliDays = DAY * parseInt(val);
						newDate.setTime(today.getTime() + milliDays);
	
						var month = (newDate.getMonth()+1) < 10 ? '0'+(newDate.getMonth()+1):(newDate.getMonth()+1);
						var fmtDay = newDate.getDate() < 10 ? '0'+newDate.getDate():newDate.getDate();
						var str =newDate.getYear()+'-'+month+'-'+fmtDay;
					
	return str;
}
/*
 * Submits a new search distance, set of markets and their marketplaces to a SEARCH action
 * (which updates the struts-form) and returns a HTML fragment with the updated results for
 * those marketplaces.
 */
function updateResults(aDivId, distance, markets, marketTypes, options, selectedNode) {	
	resetYearsQueues();
	removeConfirm();
	var minMiles;
	var maxMiles;
	var params;
	if($('rangeSelectPopup_global')){
		$('rangeSelectPopup_global').style.display='none';
	}
	if (selectedNode != null) {
		 params = getChildrenByTagName($(options),['INPUT']);
	}	
	if (selectedNode != null) {
		var hotListed = false;
		var searchYear = false;
		var always = true;
		var p = '';
		var id = 0;
		minMiles = $('mileageMinId'+selectedNode).value;
		maxMiles = $('mileageMaxId'+selectedNode).value;
		for (var i = 0; i < params.length; i++) {
			if (params[i]) {
				id = params[i].id.substring(params[i].id.indexOf('_')+1);
				if (params[i].id.indexOf('yearOptionCheck') != -1) {
					if (params[i].checked  && params[i].disabled == false) {
						p += '&searchCandidateCriteria['+id+'].modelYearText='+params[i].value;
						searchYear = true;
					}
					else {
						searchYear = false;
					}
				}
				else if (params[i].id.indexOf('hotListCheck') != -1) {
					hotListed = params[i].checked;
					if (!hotListed && searchYear) {
						p +='&searchCandidateCriteria['+id+'].expiresText=false';
						p += '&searchCandidateCriteria['+id+'].annotationText=OneTime';
					}
				}
				else if (hotListed && params[i].id.indexOf('alwaysRadio') != -1) {
					if (params[i].checked && params[i].disabled == false) {
						p +='&searchCandidateCriteria['+id+'].expiresText=false';
						p += '&searchCandidateCriteria['+id+'].annotationText=HotList';
						always = true;
					}
					else {
						always = false;
					}
				}
				else if (hotListed && !always && params[i].id.indexOf('hotListDurText') != -1) {
				
					var str= calculateDateFromDays(params[i].value);
					if( params[i].disabled == false ){
					p += '&searchCandidateCriteria['+id+'].expiresText='+str;
					p += '&searchCandidateCriteria['+id+'].annotationText=HotList';
				}
				}
				else if (params[i].id.indexOf('modelOptionCheck') != -1) {
					if (params[i].type == 'checkbox') {
						if (params[i].checked) {
							p += '&selectedModels['+id+']=' + id;
						}
					}
					else if (params[i].type == 'hidden') {
						p += '&selectedModels['+id+']=' + id;
					}
				}
			}
		}
 	}else{ 	
 		minMiles = $('mileageMinIdglobal').value;
		maxMiles = $('mileageMaxIdglobal').value;	
 	}

	if (document.getElementById("inGroup").checked){
		marketTypes=["IN_GROUP"]
    }
    
    if (document.getElementById("online").checked){
    	marketTypes=["ONLINE"]
    }
    
   
  	var map = {
		ele:          aDivId,
		path:         selectedNode == null?window.location.pathname.replace(/.go$/, 'UpdateResults.go'):window.location.pathname.replace(/.go$/, 'HotlistUpdateResults.go'),
		msg:          'Updating...',
		distance:     $F(distance),
		markets:      markets,
		minMileage:minMiles,
		maxMileage:maxMiles,
    	marketTypes:  marketTypes, 
		options:      p,
		selectedNode: selectedNode,
		ignoreMarketSuppression:'true'
		
	};
	return makeRequest(map);
}


/*
 * Displays the search edit options popup
 */
function showEditOptions(ele, selectedNode) {

if($('confirm_float')){return false;}
	rangePop.closeLast();//purchasing.js (make sure mileage edit is closed);
	
	var f = function doAction(r,j) {
		var p = new Popup();
		var div = '<div id="float" class="" style="top:0px; left:0px;"></div>';
		if(!$('float')){
			new Insertion.After($('header'),div);
		}
        $('float').innerHTML =  r.responseText;
		p.popup_show('float','',['searchCancel','searchDone'],'element-bottom',100,false,0,0,ele);
    	var scroll = new fx.Scroll();
		var offset = ((document.documentElement.clientHeight) / 2) - (.5 * $('float').clientHeight); //center vertically on screen
		scroll.scrollTo($('float'),offset);
		
	}

	var map = {
		ele:          null,
		path:         window.location.pathname.replace(/.go$/, 'Hotlist.go'),
		msg:          'Loading...',
		selectedNode: selectedNode,
		onSuccess:    f,
		onFailure:    f
	};
	return makeRequest(map);
}


/*
 * Send a request to the server suppressing the model-group candidate.
 */
function removeResultsGroup(id){
	clearYearsQueues(id);
	Element.remove('results_'+id);
	removeConfirm();
	if (!Element.hasClassName('wrap','flashSummary')){
			var f = function doAction(r,j) {
				window.location.reload();
			}
			var map = {
				ele:         null,
				path:         window.location.pathname.replace(/.go$/,'SuppressSearchCandidate.go'),
				msg:          'Updating...',
				options:'selectedNode=' + id,
				onSuccess:f
			};		
	
		return makeRequest(map);
	}
}

//execute flash locate search
function runFlashLocate(){

    var makeText = document.getElementById("makeText").value;
    if(!makeText){
        alert('Please select a Make.');
    	Element.removeClassName('searchErrors','hide');
   		return false;
    }
    var params = 'makeText=' + makeText;
    
    var model = document.getElementById("modelText").value;
    document.getElementById("modelText").value = decodeURIComponent(model);
    if (!model && model.length == 0){
    	alert('Please enter the at least the first character of the Model.');
    	Element.removeClassName('searchErrors','hide');
   		return false;
    } else {
    	params += '&modelText=' + encodeURIComponent(model);
    }
    
    var trim = document.getElementById("trim").value;
    if (trim){
        params += '&trim=' + trim;
    }

    var yearBegin = document.getElementById("yearBegin").value;
    if (yearBegin)
    {
        params += '&yearBegin=' + yearBegin;        
    }
    var yearEnd = document.getElementById("yearEnd").value;
    if (yearEnd)
    {
        params += '&yearEnd=' + yearEnd;
    }
    
    if (document.getElementById("inGroup").checked){
        params += '&marketTypesString[0]=IN_GROUP';
    }
    
    if (document.getElementById("online").checked){
        params += '&marketTypesString[0]=ONLINE';
    }
    
    if (document.getElementById("inGroupAndOnline").checked){
        params += '&marketTypesString[0]=IN_GROUP&marketTypesString[1]=ONLINE';
    }

    params += '&flashLocateRunSearch=true';

 	var s = function success(r,j){
 	    var resp = eval( '(' + r.getResponseHeader( 'X-JSON' ) + ')' ); 
	    var numbers = resp.modelsSearched + ' Models searched';
	    $('totalsNumbers').innerHTML = numbers;
    };    

	var map = {
		ele:          'results',
		path:         '/NextGen/FlashLocateSummary.go',
		msg:          'Searching...',
		options:      params,	
		onSuccess:    s,
		ignoreMarketSuppression:'true'
	};
	
	map.onPerform = function() {
		for(var i = 0; i < document.forms.length; i++) {
			Form.disable(document.forms[i].id);
		}
		var anchors = document.getElementsByTagName("a");
		for(var j = 0; j < anchors.length; j++) {
			var anchor = anchors[j]; 
			if(anchor.className == 'viewAllLink') {
				anchor.onclick = function(){return false;};
			}
			if(anchor.id == 'runFlashLocate') {
				anchor.style.visibility = 'hidden';
			}
		} 
	}
	
	map.onComplete = function() {
		for(var i = 0; i < document.forms.length; i++) {
			Form.enable(document.forms[i].id);
		}
		var anchors = document.getElementsByTagName("a");
		for(var j = 0; j < anchors.length; j++) {
			var anchor = anchors[j]; 
			if(anchor.className == 'viewAllLink') {
				anchor.onclick = anchor.href;
			}
			if(anchor.id == 'runFlashLocate') {
				anchor.style.visibility = 'visible';
			}
		} 
	}
	return makeRequest(map);
	
}

function handleMarketplace(aId, marketTypes) {
	var params = '';
	if ($(aId)) {
		var nodes = $(aId).childNodes;
		for (var i = 0; i < nodes.length; i++) {
			if (nodes[i].id != 'searchLabel') {
				if (nodes[i].className == 'on') {
					params += '&searchContext.markets['+nodes[i].id.substring(nodes[i].id.indexOf('_')+1)+'].suppress=false';
				
				}
				else {
					params += '&searchContext.markets['+nodes[i].id.substring(nodes[i].id.indexOf('_')+1)+'].suppress=true';
				}
			}		
		}	
	}
	//method to hide/show green icon 
	var d=	function displayConfirmIcon(){

		if(Element.hasClassName($(aId+'Save'),'saveSuccessHidden')){
		
			Element.removeClassName($(aId+'Save'),'saveSuccessHidden');
			Element.addClassName($(aId+'Save'),'saveSuccess');
		}else{
			Element.removeClassName($(aId+'Save'),'saveSuccess');
			Element.addClassName($(aId+'Save'),'saveSuccessHidden');
		}
	}
	var f = function onFinish(r,j) {
	
		if($(aId+'Save')){
			var timeout = 3000;
			displayConfirmIcon();
			window.setTimeout(d,timeout);
		}
	};
	var map = {
		ele:          null,
		path:         '/NextGen/SaveMarketplaceSelections.go',
		msg:          'Saving...',
		marketTypes:  marketTypes,
		marketsParam: params,
		onSuccess:    f,
		onFailure:    f
	};

	makeRequest(map);
}

function addYearOption(yearId,makeModelId,offset){

	var yearValue = parseInt($(yearId).value);					
	var loc=-330;
	var ele = $('newYears');
	var today = new Date();
	var days = today.getDate() < 10? '0'+today.getDate():today.getDate();
	var month = today.getMonth() +1 < 10?'0'+(today.getMonth()+1):today.getMonth()+1;
	var todayStr =month+'/'+days+'/'+today.getYear();
	var innerText = ele.innerHTML;
	var id = Math.floor(Math.random()*100);
	
	if(offset){
		loc = parseInt(offset);
	}
		
		while($('hotListSelector_'+id)){		//see if element with generated id already exists; if so create a new one
			 id = Math.floor(Math.random()*100);
		}
		var title = $(yearId).options[$(yearId).selectedIndex].title;
		var numInStock = title.substring(0,title.indexOf(' '));
		var stockString =title.substring(0, title.indexOf(';')+1);
		var overUnder = title.substring(title.indexOf(';')+1,title.length);	
		if(parseInt(numInStock) > 0){	
			title = '<a href="#" id="stock'+makeModelId+yearValue+'" onclick="javascript:showStockPopup(\''+yearValue+'\',\''+makeModelId+'\',\'false\',\'float\','+loc+');return false;">'+stockString+'</a>'+overUnder;		
		}
		innerText += '<dl id="hotListSelector_'+id+'">';
		innerText += '<dt><input type="checkbox"  checked name="editYear" value="'+yearValue+'" onclick="check(this,\'hotListCheck_'+id+'\',true);toggleHotListEdit(\'hotListCheck_'+id+'\',\'hotListEdit_'+id+'\');" id="yearOptionCheck_'+id+'"/> <label for="yearOptionCheck_'+id+'"  >'+yearValue+'</label><span class="stockMessage"> '+title+'</span></dt>';
		innerText +='<dd>';
		innerText +='<input type="checkbox"  name="editHotList" value="'+yearValue+'" id="hotListCheck_'+id+'" onclick="toggleHotListEdit(this,\'hotListEdit_'+id+'\');check(this,\'yearOptionCheck_'+id+'\',false);"/> ';
		innerText +='<ins class="hotlist" id="hotListEdit_'+id+'" style="visibility:hidden;">';		
		innerText +=' for: ';
		innerText +='<input type="radio" name="hotListDuration'+yearValue+'" value="always" checked id="alwaysRadio_'+id+'"/>';
		innerText +='<label for="alwaysRadio'+id+'"> Always </label>';
		innerText +='<input type="radio" name="hotListDuration'+yearValue+'" value="days"  id="hotListDurCheck_'+id+'"/> ';
		innerText +='<input type="text" name="hotListDurDays_'+id+'" value="0" id="hotListDurText_'+id+'" class="number" onblur="validateDays(this);" onfocus="$(\'hotListDurCheck_'+id+'\').checked = true;" onkeypress="return Validate.checkChars();"  ondblclick=" return showCalendarFloosan(\'hotListDurText_'+id+'\',\'%m/%d/%Y\', null, true,false,\''+todayStr+'\');" />'; 
		innerText +='<label for="hotListDurText_'+id+'"> days</label>';
		innerText +=' <img src="common/_images/icons/calendar.gif" class="cal" id="dateTrigger'+id+'"  onclick=" return showCalendarFloosan(\'hotListDurText_'+id+'\',\'%m/%d/%Y\', null, true,false,\''+todayStr+'\');" />';
		innerText +='</ins>';
		innerText +='</dd>';
		innerText+= '</dl>';

		//remove year option from list
		for(var i = 0; i < $(yearId).options.length; i++){
		
			if($(yearId).options[i].value == yearValue){
				$(yearId).options[i] = null;
			}
		}
		
		//if last year was removed, hide the control
		if( $(yearId).options.length < 1){		
			$('searchAnotherYear').style.visibility='hidden';
			$(yearId).style.visibility='hidden';
		}
		
		//check to see if All Years is checked, if so click it to call event handler and reenable checkboxes
		if($('yearOptionCheck_0').checked){
			$('yearOptionCheck_0').click();			
		}
		ele.innerHTML = innerText;
}

function loadModels() {
	var idx = $F('makeSelect');

	if (!isNaN(idx)) {
	
	
		var focus = function(){
	
			$('modelSelect').focus();
		
		}
		var a = new AjaxLoader('Loading...');
		a.setParameters('make=' + idx);
		a.setPath('/NextGen/OptimalInventorySearchSummaryLoadModels.go');
		a.setElement(modelBox);
		a.setOnComplete(focus);
		a.performAction();
		$('addToHotlistCheck').disabled = false;
		
	}else{
	
		$('modelSelect').options.length = 0;
		var opt = document.createElement('OPTION');
		opt.text ='Select a make...';
		opt.value='';
		$('modelSelect').add(opt);
		$('modelSelect').disabled = 'disabled';
		if($('addToHotlistCheck').checked){
			$('addToHotlistCheck').click();
		}
		$('addToHotlistCheck').disabled = 'disabled';
		
		
	}
}
//resets the add search candidate form
function resetSearchFields(){

	if($('addToHotlistCheck').checked){
			
			$('addToHotlistCheck').click();
	}
	$('addToHotlistCheck').disabled = 'disabled';

	$('modelSelect').options.length = 0;
	var opt = document.createElement('OPTION');
	opt.text ='Select a make...';
	opt.value='';
	$('modelSelect').add(opt);
	$('modelSelect').disabled='disabled';
	$('makeSelect').selectedIndex = 0;
	$('alwaysRadioGlobal').checked = true;
	$('hotListDurCheckAllGlobal').checked = false;
	$('hotListDurTextGlobal').value = '0';
}


function toggleMarketBlock(open,close,parent){

	if($(open) && $(close)){	 	
	
		if( $(close).innerHTML != '' &&  $(open).innerHTML !=''){ //verify that there is content to show before toggling
					
					 	
				$(open).className = $(open).className.replace(' closed','');
				
				if($(close).className.indexOf('closed') == -1){ //make sure we dont keep appending 'closed' to class name, which will prevent the element from showing
					$(close).className =  $(close).className + ' closed';		 			
				}	
				
		}	
			
	}else{
	
		alert("ERROR (Toggle Market Block): Element does not exist.");
	}	
	
	//get the unique numeric id off of the parent element, and create the ids for the "hide all", "view all" links
	var ext = parent.substring(parent.indexOf('_'));	
	var view = 'view'+ext;
	var hide = 'hide'+ext;

	//check if we are hiding or showing
	if(checkForClosedMarkets(parent)){
		
		$(view).style.display='block';
		$(hide).style.display='none';
		
	}else{
	
		$(view).style.display='none';
		$(hide).style.display='block';
	}
}

function toggleAllMarketBlocks(id){

	if(!$(id)) return;
	var children =  $(id).children;
	var open;
	var close;
	var doWhat = checkForClosedMarkets(id);
	var reset = false;
	
	
	//iterate over the children of the body_# div element, and single out vehicles and market divs.
	//market is a sub child of the inner body div so we need to go down one more level
	for( var i = 0; i < children.length; i++ ){
      
		if(children[i].id.indexOf('vehicles') !=-1 || children[i].className.indexOf('body') !=-1){
		
			if(children[i].className.indexOf('body') !=-1){
			
			//doWhat tells us if we are opening all or closing all markets
			//reset goes to true if we found a market div and vehicles div, in which case we toggle them
				if( doWhat ){
					close = children[i].children[0];
				}else{
					open = children[i].children[0];
				}
				reset = false;
			
			}else{
			
				if( doWhat ){
					open = children[i];
				}else{
					close = children[i];
				}
				reset = true;
				
			}
			
			if(reset){
				toggleMarketBlock(open,close, id);
		
			}
		}			
	
	}
	return false;
}

//checks to see if a model had any closed vehicle groups (used for toggling all vehicles)
function checkForClosedMarkets(id){

		var children =  $(id).children;
		var child;		
		
		for( var i = 0; i < children.length; i++ ){
		
			if( children[i].id.indexOf('vehicles') !=-1 ){
			
				child = children[i];
				
				if( child.className.indexOf('closed') != -1 &&  child.innerHTML !=''){
				
					return true;				
				}
				
			}
		
		}	
		
		return false;
}

//Disables all elements that are children of element wht
//ele: the element calling the method
//wht: the outer container 
//enable: whether or not to allow the element to enable as well as disable

function disableAll(ele,wht, exceptions, enable){

	var what = $(wht);
	var flag =  false;
	var elements;

	if(ele.checked || enable){						
		//	disableLeaves(ele,what.children);		
			elements = getChildrenByTagName(what,['IMG','SELECT','INPUT']);
			for(var i = 0; i < elements.length; i++){
				for(var k=0; k < exceptions.length; k++){  //disable all except those on exception list
						if( $(exceptions[k]).id == elements[i].id ){
							flag = true;
							exceptions.splice(k,1); //if found the exception, remove it from the compare list to increase performance
							break;
						}
					}
					if( !flag){							
						elements[i].disabled = ele.checked;
					}
					flag = false;
			}									
	}		


}


//if uncheck is true, the element is only allowed to uncheck the 'checking' parameter element
function check(ele, checking, uncheck){

	if(!uncheck){
		if(ele.checked){
			$(checking).checked = true;	
		}
	}else{
	
		if(!ele.checked){
			$(checking).checked = false;
			
		}
	
	}

}
//Verifies that the number of days entered is not larger than 999
function validateDays(ele){


	if(!Validate.checkRange(0,999,parseInt(ele.value))){
	
		alert("Hotlist period must be between 0 and 999 days.");
		ele.value = '0';
		ele.select();
	}else if(isNaN(ele.value)){
	
		alert("Hotlist period must be a numeric value.");
		ele.value = '0';
		ele.select();
	}
	
	if(ele.value ==''){
		ele.value = '0';
	}

}

//shows/hides the hot list edit options
function toggleHotListEdit(check,id){

	if(check.checked){
	
		$(id).style.visibility='visible';
	
	}else{
	
		$(id).style.visibility='hidden';
	}
}

function toggleMarket(id){
	
	
	if($(id)){
	
		if($(id).className == 'on'){		
			$(id).className = 'off';		
		}else{
			$(id).className = 'on';
		}
	}

}
function submitOnEnter(id){

	if(Util.isIE){
	
		if(event.keyCode == 13){
		
			$(id).click();
		
		}
	
	}else{
		if(e.keyCode == 13){
		
			$(id).click();
		
		}		
	}



}
