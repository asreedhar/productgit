var ajax; //global ajax var
var linkClicked = false;

function preventClicks(ele){

	var timeout = function(){
		ele.title = "";	
	}
	
	if( (ajax && ajax.getWorking()) || linkClicked ){
	
		ele.title = "There is an operation in progress.  Please wait.";
		window.setTimeout(timeout,5000);
		return false;
	}else{
		linkClicked = true;
		return true;
	}


}

function getInlineModelYearParams(resultsId) {
	var params = [];
	var cont = $('modelyears_'+resultsId);
	var yearEls = cont.getElementsByTagName('li');
	yearEls = $A(yearEls);
	yearEls.each(function(el,i) {
		var isActive = Element.hasClassName(el.id,'new') || Element.hasClassName(el.id,'opt');
		var idStr = el.id;
		if(isActive) {
			var prefixLength = ('year_'+resultsId+'_').length; //prefixed with year_[id]_
			//get year value
			var yearValue = idStr.substring(prefixLength);
var paramYear = 'searchCandidateCriteria['+i+'].modelYearText='+yearValue;
var paramAnnotation = 'searchCandidateCriteria['+i+'].annotationText=OneTime';
var paramExpires = 'searchCandidateCriteria['+i+'].expiresText='+false;
			var hotListExp;
			if($('hlExpiration_'+resultsId+'_'+yearValue)) {
				hotListExp = $F('hlExpiration_'+resultsId+'_'+yearValue);
				paramAnnotation = 'searchCandidateCriteria['+i+'].annotationText=HotList';
				if(hotListExp != '') {
					paramExpires = 'searchCandidateCriteria['+i+'].expiresText='+hotListExp;
				}
			}
			params.push(paramYear,paramAnnotation,paramExpires);
		}
	});
	params = '&selectedModels[0]=0&'+params.join('&');
	return params;
}
function getDistanceParams(resultsId) {
	var inlineEl;
		inlineEl = $('distanceSelect_'+resultsId);
	var prefValue = $F('distanceSelect');
	var inlineValue; 
	if(inlineEl) {
		inlineValue = $F(inlineEl);
		return inlineValue;
	} else {
		return prefValue;
	}
}
function showDistanceSelect(resultsId) {
	var selectEl;
		selectEl = $('distanceSelect_'+resultsId);
	var trigEl = $('dynSelect_'+resultsId);
	if(selectEl) {
		Element.toggle(trigEl);
		Element.toggle(selectEl);
		selectEl.focus();
		return;
	}
	var changeAction = 'updateModelResults('+resultsId+');';
	var blurAction = 'hideDistanceSelect('+resultsId+');';
		selectEl = ['<select id="distanceSelect_'+resultsId+'" onchange="'+changeAction+'" onblur="'+blurAction+'">','','</select>'];
	var options = $('distanceSelect').innerHTML;
	selectEl.splice(1,1,options);
	selectEl = selectEl.join('');
	new Insertion.After('dynSelect_'+resultsId,selectEl);
	Element.hide('dynSelect_'+resultsId);
	$('distanceSelect_'+resultsId).focus();
}
function hideDistanceSelect(resultsId) {
	var trigEl = $('dynSelect_'+resultsId);
	var selectEl = $('distanceSelect_'+resultsId);
		Element.toggle(trigEl);
		Element.toggle(selectEl);
}

function showUpgradeInformation(linkId) {
	var upgradeEl = $('navUpgrade');
	if(!Element.visible(upgradeEl)) {
		Element.show(upgradeEl);
	} 
	var clickedObj = $(linkId);
	var clickCoords = Util.findPosition(clickedObj);
	var elHeight = Element.getHeight(upgradeEl);
	upgradeEl.style.top = clickCoords[1]-elHeight + 'px';
}
function hideUpgradeInformation() {
	Element.hide('navUpgrade');
}



/*
*Expands or hides all markets takes in the model block id and the inner html value of the toggle link (view all or hide all)
*/
function toggleAllResultsGroups(resultsId) {
	if($('viewAll_'+resultsId)) {
		var areAllOpen = Element.visible('hideAll_'+resultsId);
		var classStr = areAllOpen ? 'collapsed':'expanded';

		Element.toggle('hideAll_'+resultsId);
		Element.toggle('viewAll_'+resultsId);
	}
	var cont = $('results_'+resultsId);
	var elements = cont.getElementsByTagName('span');
	elements = $A(elements);
	var i = 0; 
	elements.each( function(el){
		if(el.id) {
			if(el.id.indexOf('group_'+resultsId,0) != -1) {
				i += 1;
				var alreadySet = Element.hasClassName(el.id,classStr) || Element.hasClassName(el.id,'none');
				if(!alreadySet) { 
					toggleResultsGroup(resultsId+'.'+i,true);
				}
			};
		}
	});
}
function toggleResultsGroup(targetId,toggleAll){
	var el = $('group_'+targetId);
	Util.toggleClassNames(el,'collapsed','expanded');
	var splitAt = targetId.indexOf('.');
	var resultsId = targetId.substring(0,splitAt); //prefixed with 'group_' & suffixed with '_targetId'

	var t;
	//update totals obj with current expanded state
	if(!T[resultsId]) {
		t = new Totals(resultsId);
		T[resultsId] = t;
	}
	t = T[resultsId];
	var mktPos = targetId.substring(splitAt+1);
	var mkt = t.markets[mktPos];
	mkt.expanded = Element.hasClassName(el,'expanded');

	if(!toggleAll) {
		handleToggleLink(resultsId);
	}
}
function handleToggleLink(resultsId) {
	var cont = $('results_'+resultsId);
	var allAreOpen = true;
	var allAreClosed = true;
	var allAreEmpty = true;
	var elements = cont.getElementsByTagName('span');
	elements = $A(elements);
	elements.each( function(el){
		if(el.id && el.id.indexOf('group_'+resultsId,0) != -1 && !Element.hasClassName(el.id,'none')) {
			var isEmpty = Element.hasClassName(el.id,'none');
			if(isEmpty == false) allAreEmpty = false;
			var isOpen = Element.hasClassName(el.id,'expanded');
			if(isOpen == false) allAreOpen = false;
			var isClosed = Element.hasClassName(el.id,'collapsed');
			if(isClosed == false) allAreClosed = false;
		};
	});
	if(allAreOpen) { 
		Element.show('hideAll_'+resultsId);
		Element.hide('viewAll_'+resultsId); 
	}
	if(allAreClosed) { 
		Element.hide('hideAll_'+resultsId);
		Element.show('viewAll_'+resultsId); 
	}
	if(allAreEmpty) { 
		Element.hide('hideAll_'+resultsId,'viewAll_'+resultsId);
	}
}

function makeParams(map) {
	var params = '';
	if (map.distance) {
		params += '&maxDistanceFromDealer=' + map.distance;
	}
	if (map.liveDistance) {
		params += '&maxDistanceLiveFromDealer=' + map.liveDistance;
	}
	if (map.markets) {
		var nodes = $(map.markets).childNodes;
		var node;
		var numMarketsSelected = 0;
		for (var i = 0; i < nodes.length; i++) {
			var market = nodes[i].id.match(/searchMarket_(\d+)/)
			if (market != null) {
				params += '&searchContext.markets['+market[1]+'].suppress=';
				if (nodes[i].className == 'on') {
					params += 'false';
					numMarketsSelected += 1;
				}
				else {
					params += 'true';
				}
			}
		}
		if (numMarketsSelected == 0) {
			map.marketsEmpty = true;
		}
	}
	if (map.marketsParam) {
		params += map.marketsParam;
	}
	if (map.marketsOrder) {
		params += map.marketsOrder;
	}
	if (map.marketTypes) {
		for (var i = 0; i < map.marketTypes.length; i++) {
			params += '&marketTypesString[' + i + ']=' + map.marketTypes[i];
		}
	}
	if (map.options) {
		params += map.options;
	}
	if (map.sortByColumn) {
		params += '&sortByColumn=' + map.sortByColumn;
	}
	if (map.sortByOrder) {
		params += '&sortByOrder=' + map.sortByOrder;
	}
	if (map.selectedNode) {
		params += '&selectedNode=' + map.selectedNode;
	}
	if (map.actionForwardSuffix) {
		params += '&actionForwardSuffix=' + map.actionForwardSuffix;
	}
	if (map.unmodifiableMemberMarket) {
		params += '&unmodifiableMemberMarket=' + map.unmodifiableMemberMarket;
	}
	if (map.ignoreMarketSuppression) {
		params += '&ignoreMarketSuppression=' + map.ignoreMarketSuppression;
	}
	if (map.minMileage) {
		params += '&minMileage=' + map.minMileage;
	}
	if (map.maxMileage) {
		params += '&maxMileage=' + map.maxMileage;
	}
	if (map.homepage) {
		params += '&memberSearchHomePageSelection=' + map.homepage;
	}
	if (map.timePeriod) {
			params += '&timePeriod=' + map.timePeriod;
	}
	if (map.exactMatches) {
		params += '&exactMatches=' + map.exactMatches;
	}
	if (map.searchSummaryIndex != null) {
		params += '&searchSummaryIndex=' + map.searchSummaryIndex;
	}
	return params;
}

function makeRequest(map) {

	if(!ajax && !linkClicked){
		ajax = new AjaxLoader();
	} else if(ajax && !ajax.getWorking() && !linkClicked) {

		ajax = new AjaxLoader();
	} else {
		return false;
	}
	var params = makeParams(map);
	if (map.marketsEmpty) {
		alert('At least one market must be selected');
		return;
	}
	ajax.setMessage(map.msg);
	ajax.setParameters(params);
	ajax.setPath(map.path);
	ajax.setElement(map.ele);
	
	if (map.onSuccess) {
		ajax.setOnSuccess(map.onSuccess);
	}
	if (map.onFailure) {
		ajax.setOnFailure(map.onFailure);
	}
	if (map.onComplete) {
		ajax.setOnComplete(map.onComplete);
	}
	if(map.setShowAt) {
		ajax.setShowAt(map.setShowAt);
	}
	if(map.onPerform) {
		ajax.setOnPerform(map.onPerform);
	}
	
	ajax.performAction();
	return false;
}


/*
 * Calls the SORT action for the current page (which updates the struts-form) and returns
 * a HTML fragment with the updated results.
 * 
 * Whosoever updated this method to take a path arg should talk to Simon as this broke
 * his functionality and if their needs are now not met he shall make amends.
 */
function updateSortByColumn(sortByColumn, sortByOrder, aDivId, params) {
	return updateSortNodeByColumn(sortByColumn, sortByOrder, aDivId, null, params);
}

/*
 * Calls the  action for the supplied node on the current page (which updates the struts-form)
 * and returns a HTML fragment with the updated results for that node.
 */
function updateSortNodeByColumn(sortByColumn, sortByOrder, aDivId, selectedNode, params) {

	var map = {
		ele:          aDivId,
		path:         window.location.pathname.replace(/.go$/, 'Sort.go'),
		msg:          'Loading...',
		sortByColumn: sortByColumn,
		sortByOrder:  sortByOrder,
		selectedNode: selectedNode,
		options: params
	};
	 makeRequest(map);
}


/*
 * Validates the selection of a make and model and submits those to a SEARCH action which
 * runs an incremental search for the vehicles of that make and model.
 */
function addSearchCandidate() {

	var idx0 = $('makeSelect').value;
	var idx1 = $('modelSelect').value;

	if (idx0 != '' && idx1 !='' ) {
		var params = 'make=' + idx0 + '&model=' + idx1;
		/* Marshall the data from the one-time search div into these vars */
		if($('addToHotlistCheck').checked){
			var days = $('hotListDurTextGlobal').value;
			var date = 'false';
			if ($('alwaysRadioGlobal').checked == false) {
				date = calculateDateFromDays(days);
			}
			params += '&searchCandidateCriteria[0].modelYearText=null';    // always null
			params += '&searchCandidateCriteria[0].expiresText='+date;     // or date 'yyyy-MM-dd'
			params += '&searchCandidateCriteria[0].annotationText=HotList'; // or 'OneTime'
		}		
		 	
		 	var s = function success(r,j){
			 	resetSearchFields();
			 	var resp = eval( '(' + r.getResponseHeader( 'X-JSON' ) + ')' ); 
			 	     if(eval(resp.updated) || !eval(resp.added)){  //remove the existing element if this is false
			 	 
			 	     	 var model  = $('results_'+resp.groupByNodeID); //store the element
			 	
			 	  		 Element.remove(model);		 	   
			 	     }  	
			 	  	
			 	 new Insertion.Top($('results'),r.responseText); //insert new element		 	    
				var numbers = resp.OptimalItems + ' Optimal Inventory searches; ' + resp.HotListItems + ' Hot List searches';
				$('totalsNumbers').innerHTML = numbers;
			
				$('totalsDate').innerHTML =Util.getFormattedDate('M j, Y h:i A');
			};		
		
		var map = {
		path:         window.location.pathname.replace(/.go$/, 'AddSearchCandidate.go'),
		msg:          'Searching...',
		options:      params,
		onSuccess: s,
		onFailure : s
		
		};
	return makeRequest(map);
		
	}
	else {
		alert('You must select a make and model to search.');
	}
	return false;
}


//matches the selected index from sel1 to that of sel2
function matchSelects(sel1, sel2){	
		$(sel2).selectedIndex = $(sel1).selectedIndex;
}

//given a set number of days from today this method returns the date 
function calculateDateFromDays(days){
			
						
						var val = days;
						var DAY = 1000 * 60 * 60 * 24; //milliseconds in a day
						var today = new Date();
						var newDate = new Date();
						var milliDays = DAY * parseInt(val);
						newDate.setTime(today.getTime() + milliDays);
	
						var month = (newDate.getMonth()+1) < 10 ? '0'+(newDate.getMonth()+1):(newDate.getMonth()+1);
						var fmtDay = newDate.getDate() < 10 ? '0'+newDate.getDate():newDate.getDate();
						var str =newDate.getYear()+'-'+month+'-'+fmtDay;
					
	return str;
}


/*
 * Submits a new search distance for the given marketplaces to a SEARCH action (which updates the struts-form)
 * and returns a HTML fragment with the updated results for those marketplaces.
 */
function updateSearchDistance(distanceElement, timePeriodElement, marketTypes, aDivId, ignoreMarketSuppression, actionForwardSuffix, onComplete) {
	var minMiles = null;
	var maxMiles = null;
	if($('wrap').className == 'navigator') {
		 minMiles = $('mileageMinIdnavigator').value;
		 maxMiles = $('mileageMaxIdnavigator').value;
	}
	var map = {
		ele:          aDivId,
		path:         window.location.pathname.replace(/.go$/, 'UpdateResults.go'),
		msg:          'Loading...',
		distance:     $F(distanceElement),
		timePeriod:   (timePeriodElement ? $F(timePeriodElement) : null),
		minMileage:minMiles,
		maxMileage:maxMiles,
		marketTypes:  marketTypes,
		ignoreMarketSuppression: ignoreMarketSuppression,
		actionForwardSuffix: actionForwardSuffix,
		onComplete:    onComplete
	};
	if($('wrap').className == 'navigator') {
		
		removeTabTitles();
	}
	return makeRequest(map);
}

/*
 * Submits a new search distance for the given marketplaces to a permament SAVE action. There is no
 * HTML returned from this fragment.
 */
function saveSearchDistance(distanceElement, marketTypes) {

	var d=	function displayConfirmIcon(){

		if(Element.hasClassName($(distanceElement+'Save'),'saveSuccessHidden')){		
			Element.removeClassName($(distanceElement+'Save'),'saveSuccessHidden');
			Element.addClassName($(distanceElement+'Save'),'saveSuccess');
		}else{
			Element.removeClassName($(distanceElement+'Save'),'saveSuccess');
			Element.addClassName($(distanceElement+'Save'),'saveSuccessHidden');
		}
	}
	var f = function onFinish(r,j) {
	
		if($(distanceElement+'Save')){
		var timeout = 3000;
			displayConfirmIcon();
			window.setTimeout(d,timeout);
		}
	};
	var map;	
	var  minMileage= $('mileageMinIdglobal')?$('mileageMinIdglobal').value:null;
	var maxMileage = $('mileageMaxIdglobal')?$('mileageMaxIdglobal').value:null;
	
	if(marketTypes.join(',').indexOf('LIVE') != -1){
		var map = {
			ele:          null,
			path:         '/NextGen/SaveDistanceFromDealer.go',
			msg:          'Saving...',
			liveDistance:     $F(distanceElement),
			maxMileage: maxMileage,
			minMileage: minMileage,
			marketTypes:  marketTypes,
			onSuccess:    f,
			onFailure:    f
		};
	
	}else{
	
		var map = {
			ele:          null,
			path:         '/NextGen/SaveDistanceFromDealer.go',
			msg:          'Saving...',
			distance:     $F(distanceElement),
			maxMileage:maxMileage,
			minMileage:minMileage,
			marketTypes:  marketTypes,
			onSuccess:    f,
			onFailure:    f
		};
	}

	
	makeRequest(map);
}

function toggleLiveAuctionRunListMatches(element, aDivId, exactMatches, searchSummaryIndex) {
	if (element.className=='disabled')
		return false;
	var pot = $('potentialresults');
	var all = $('allresults');
	var f = function () {
		 runListOptions('tr',true);
		pot.className = (pot.className == 'disabled' ? '' : 'disabled');
		all.className = (all.className == 'disabled' ? '' : 'disabled');
	};
	var map = {
		ele:                aDivId,
		path:               window.location.pathname.replace(/.go$/, 'UpdateResults.go'),
		msg:                'Loading...',
		marketTypes:        ['LIVE'],
		exactMatches:       exactMatches,
		searchSummaryIndex: searchSummaryIndex,
		onSuccess:          f,
		onFailure:          f
	};

	return makeRequest(map);
}






function removeConfirm(){

	if($('confirm_float')){
	
		Element.remove($('confirm_float'));
	}
	return false;
}
function confirmRemoveResultsGroup(id, eleId, vehName){		
		if(!$('confirm_float')){
				var div = '<div id="confirm_float"  style="top:-500px; left:-500px;"></div>';
				new Insertion.After($('header'),div);						
		}	
		
			var params = '&vehicleName='+vehName+'&id='+id;
				
			var f = function doAction(r,j) {			
					var p = new Popup();
		
					$('confirm_float').innerHTML = r.responseText;
					
						p.popup_show('confirm_float','',['confirmYes','confirmNo'],'element-bottom',100,false,0,10,$(eleId).id);
						var scroll = new fx.Scroll();
						var offset = ((document.documentElement.clientHeight) / 2) - (.5 * $('confirm_float').clientHeight); //center vertically on screen
						scroll.scrollTo($('confirm_float'),offset);
			}	
		var map = {
			ele:          'confirm_float',
			path:          'IncludeConfirmDelete.go',
			msg:          'Loading...',
			options:params,
			onSuccess:f
		};		
			return makeRequest(map);	
}


var changed = false;

function profileChanged(){
	
		changed = true;
	
	}

	

	/*
	 * Display the profile edit box.
	 */
	function showProfileEdit(id){
	
		var f = function doAction(r,j) {
			var div = '<div id="profile_float"  style="top:0px; left:0px;"></div>';
			var params = '';
			var p = new Popup();
				if(!$('profile_float ')){
					new Insertion.After($('header'),div);
				}
		        $('profile_float').innerHTML =  r.responseText;			
				Sortable.create('dragList',{ghosting:false,constraint:'vertical'})
				p.noClip(true);
				p.popup_show('profile_float','profileHead',['profileCancel','profileDone'],'element-bottom',100,false,0,10,id);
		};
	if(!linkClicked){
		var a = new AjaxLoader('Loading...');
		a.setPath( window.location.pathname.replace(/.go$/, 'LoadMemberProfile.go'))
		a.setOnSuccess(f);
		a.performAction();
	}
	}

	/*
	 * Save the user entered profiles.
	 */
	function storeProfile(){	
		
		var e = $('dragList');
		
		var marketSelections = '';
		var marketOrder = '';
		var pos = 0;
		var numMarketsSelected = 0;
		

		for (var i = 0; i < e.children.length; i++) {
			for (var j = 0;j < e.children[i].children.length; j++) {
				if (e.children[i].children[j].value) {
					var originalPositionInServerSideList = e.children[i].children[j].value;
					marketOrder += '&marketOrder[' + pos + ']=' + originalPositionInServerSideList;
					marketSelections += '&searchContext.markets['+originalPositionInServerSideList+'].suppress=';
					if (e.children[i].children[j].checked) {
						marketSelections += 'false';
						if (originalPositionInServerSideList != $F('IndexOfADESA')) {
							numMarketsSelected += 1;
						}
					}
					else {
						marketSelections += 'true';
					}
					pos++;
				}
			}
		}
		
		if (numMarketsSelected == 0) {
			alert('At least one market must be selected');
			return false;
		}
		
		var homepage = '';	
		
		if ($('marketplace').checked) {
			homepage = $('marketplace').value;
		}
		else {
			homepage = $('optimal').value;
		}
		
			//call this on success
			var s = function reloadResults(r,j){
		
				if($('marketElements')){
				
					var success = null;
					if (window.location.pathname.search(/FlashLocateSummary.go$/) >= 0) {
						success = runFlashLocate;
					}
					else {
						success = function () {
							updateResults('results', 'distanceSelect', 'marketElements',['IN_GROUP','ONLINE'],'marketElements',null);
						};
					}
			
					var a = new AjaxLoader('Updating');
					a.setPath(window.location.pathname.replace(/^(.*)\/(.*).go$/, '$1/Include$2Nav.go'));
					a.setElement('nav');
					a.setOnSuccess(success);
					a.performAction();
			
				}
				else {
				
					$('distanceSelect').value=$F('profileMarketDistance');
					$('distanceSelectRight').value=$F('profileMarketDistanceLive');
					$('searchDistance_selectText').innerHTML = $('profileMarketDistance').options[$('profileMarketDistance').selectedIndex].text;
					$('searchRightDistance_selectText').innerHTML = $('profileMarketDistanceLive').options[$('profileMarketDistanceLive').selectedIndex].text;					
							
					
					var complete = function () {
						updateSearchDistance($('distanceSelectRight'), null, ['LIVE'], 'liveAuctionAlerts', true, 'rhs', null);
					
					};
					
					updateSearchDistance($('distanceSelect'), null, ['IN_GROUP', 'ONLINE'], 'inGroupAndOnline', true, 'lhs', complete);
					
				}
			};
	
		var map = {
			ele:          null,
			path:         window.location.pathname.replace(/.go$/, 'SaveMemberProfile.go'),
			msg:          'Saving...',
			distance:     $F('profileMarketDistance'),
			liveDistance:     $F('profileMarketDistanceLive'),
			homepage:     homepage,
			maxMileage:$('mileageMaxId').value,
			minMileage:$('mileageMinId').value,
			marketsParam: marketSelections,
			marketsOrder: marketOrder,
			onSuccess:s
		};
		
		if(changed){
			return makeRequest(map);
		}
		return false;
	}



//execute flash locate search
function runFlashLocate(){

    var make = document.getElementById("makeText").value;
    
    if(make == '' || make == null){
        alert('Please enter at least one character for the make.');
    	Element.removeClassName('searchErrors','hide');
   		return;
    }
    var params = 'makeText=' + make;
    
    var model = document.getElementById("modelText").value;
    if (model){
        params += '&modelText=' + model;
    }
    
    var trim = document.getElementById("trim").value;
    if (trim){
        params += '&trim=' + trim;
    }

    var yearBegin = document.getElementById("yearBegin").value;
    if (yearBegin)
    {
        params += '&yearBegin=' + yearBegin;        
    }
    var yearEnd = document.getElementById("yearEnd").value;
    if (yearEnd)
    {
        params += '&yearEnd=' + yearEnd;
    }
    
    if (document.getElementById("inGroup").checked){
        params += '&marketTypesString[0]=IN_GROUP';
    }
    
    if (document.getElementById("online").checked){
        params += '&marketTypesString[0]=ONLINE';
    }
    
    if (document.getElementById("inGroupAndOnline").checked){
        params += '&marketTypesString[0]=IN_GROUP&marketTypesString[1]=ONLINE';
    }

    params += '&flashLocateRunSearch=true';

 	var s = function success(r,j){
 	    var resp = eval( '(' + r.getResponseHeader( 'X-JSON' ) + ')' ); 
	    var numbers = resp.modelsSearched + ' Models searched';
	    $('totalsNumbers').innerHTML = numbers;
    };    

	var map = {
		ele:          'results',
		path:         '/NextGen/FlashLocateSummary.go',
		msg:          'Searching...',
		options:      params,	
		onSuccess:    s
	};
	return makeRequest(map);
	
}

function updatePeriod(aId, aDivId) {
}

function handleMarketplace(aId, marketTypes) {
	var params = '';
	if ($(aId)) {
		var numMarketsSelected = 0;
		var nodes = $(aId).childNodes;
		for (var i = 0; i < nodes.length; i++) {
			if (nodes[i].id != 'searchLabel') {
				if (nodes[i].className == 'on') {
					params += '&searchContext.markets['+nodes[i].id.substring(nodes[i].id.indexOf('_')+1)+'].suppress=false';
					numMarketsSelected += 1;
				}
				else {
					params += '&searchContext.markets['+nodes[i].id.substring(nodes[i].id.indexOf('_')+1)+'].suppress=true';
				}
			}		
		}
		if (numMarketsSelected == 0) {
			alert('At least one market must be selected');
			return;
		}
	}
	//method to hide/show green icon 
	var d=	function displayConfirmIcon(){

		if(Element.hasClassName($(aId+'Save'),'saveSuccessHidden')){
		
			Element.removeClassName($(aId+'Save'),'saveSuccessHidden');
			Element.addClassName($(aId+'Save'),'saveSuccess');
		}else{
			Element.removeClassName($(aId+'Save'),'saveSuccess');
			Element.addClassName($(aId+'Save'),'saveSuccessHidden');
		}
	}
	var f = function onFinish(r,j) {
	
		if($(aId+'Save')){
			var timeout = 3000;
			displayConfirmIcon();
			window.setTimeout(d,timeout);
		}
	};
	var map = {
		ele:          null,
		path:         '/NextGen/SaveMarketplaceSelections.go',
		msg:          'Saving...',
		marketTypes:  marketTypes,
		marketsParam: params,
		onSuccess:    f,
		onFailure:    f
	};

	makeRequest(map);
}


function runListOptions(tag,hide){

	var tagArray = document.getElementsByTagName(tag);
	
	for( var i = 0; i < tagArray.length; i++ ){
	
		if( hide ){
		
			if( tagArray[i].className == 'show' ){
				
				tagArray[i].className = 'hide';
			}
		}else{
		
			if( tagArray[i].className == 'hide' ){
				
				tagArray[i].className = 'show';
			}
		}
	}
	
	if(hide){
	
		$('hideAll').className = 'disabled';
	
		$('showAll').className = '';
	
		
	}else{
	
		$('showAll').className = 'disabled';

		$('hideAll').className = '';
	
	}
	return false;
}


function loadModels() {
	var idx = $F('makeSelect');

	if (!isNaN(idx)) {
	
	
		var focus = function(){
	
			$('modelSelect').focus();
		
		}
		var a = new AjaxLoader('Loading...');
		a.setParameters('make=' + idx);
		a.setPath('/NextGen/OptimalInventorySearchSummaryLoadModels.go');
		a.setElement(modelBox);
		a.setOnComplete(focus);
		a.performAction();
		$('addToHotlistCheck').disabled = false;
		
	}else{
	
		$('modelSelect').options.length = 0;
		var opt = document.createElement('OPTION');
		opt.text ='Select a make...';
		opt.value='';
		$('modelSelect').add(opt);
		$('modelSelect').disabled = 'disabled';
		if($('addToHotlistCheck').checked){
			$('addToHotlistCheck').click();
		}
		$('addToHotlistCheck').disabled = 'disabled';
		
		
	}
}
//resets the add search candidate form
function resetSearchFields(){

	if($('addToHotlistCheck').checked){
			
			$('addToHotlistCheck').click();
	}
	$('addToHotlistCheck').disabled = 'disabled';

	$('modelSelect').options.length = 0;
	var opt = document.createElement('OPTION');
	opt.text ='Select a make...';
	opt.value='';
	$('modelSelect').add(opt);
	$('modelSelect').disabled='disabled';
	$('makeSelect').selectedIndex = 0;
	$('alwaysRadioGlobal').checked = true;
	$('hotListDurCheckAllGlobal').checked = false;
	$('hotListDurTextGlobal').value = '0';
}


function toggleMarketBlock(open,close,parent){

	if($(open) && $(close)){	 	
	
		if( $(close).innerHTML != '' &&  $(open).innerHTML !=''){ //verify that there is content to show before toggling
					
					 	
				$(open).className = $(open).className.replace(' closed','');
				
				if($(close).className.indexOf('closed') == -1){ //make sure we dont keep appending 'closed' to class name, which will prevent the element from showing
					$(close).className =  $(close).className + ' closed';		 			
				}	
				
		}	
			
	}else{
	
		alert("ERROR (Toggle Market Block): Element does not exist.");
	}	
	
	//get the unique numeric id off of the parent element, and create the ids for the "hide all", "view all" links
	var ext = parent.substring(parent.indexOf('_'));	
	var view = 'view'+ext;
	var hide = 'hide'+ext;

	//check if we are hiding or showing
	if(checkForClosedMarkets(parent)){
		
		$(view).style.display='block';
		$(hide).style.display='none';
		
	}else{
	
		$(view).style.display='none';
		$(hide).style.display='block';
	}
}

function toggleAllMarketBlocks(id){

	if(!$(id)) return;
	var children =  $(id).children;
	var open;
	var close;
	var doWhat = checkForClosedMarkets(id);
	var reset = false;
	
	
	//iterate over the children of the body_# div element, and single out vehicles and market divs.
	//market is a sub child of the inner body div so we need to go down one more level
	for( var i = 0; i < children.length; i++ ){
      
		if(children[i].id.indexOf('vehicles') !=-1 || children[i].className.indexOf('body') !=-1){
		
			if(children[i].className.indexOf('body') !=-1){
			
			//doWhat tells us if we are opening all or closing all markets
			//reset goes to true if we found a market div and vehicles div, in which case we toggle them
				if( doWhat ){
					close = children[i].children[0];
				}else{
					open = children[i].children[0];
				}
				reset = false;
			
			}else{
			
				if( doWhat ){
					open = children[i];
				}else{
					close = children[i];
				}
				reset = true;
				
			}
			
			if(reset){
				toggleMarketBlock(open,close, id);
		
			}
		}			
	
	}
	return false;
}

//checks to see if a model had any closed vehicle groups (used for toggling all vehicles)
function checkForClosedMarkets(id){

		var children =  $(id).children;
		var child;		
		
		for( var i = 0; i < children.length; i++ ){
		
			if( children[i].id.indexOf('vehicles') !=-1 ){
			
				child = children[i];
				
				if( child.className.indexOf('closed') != -1 &&  child.innerHTML !=''){
				
					return true;				
				}
				
			}
		
		}	
		
		return false;
}

//this method is used by the drag drop (otherwise it reverts to the 
//default value of the form element on drag)
var val;
function saveValue(id, doWhat){

	if(doWhat == 'save'){
	
		val = $(id).checked;
	}else{
	
		$(id).checked = val;
		val = null;
	}


}
//ensures that one checkbox in a group is always checked
function verifyChecked(e,what){

	var index= 0;
	var oneChecked = false;
	
	while(true){		
			
			if($(what+index)){
			
					if($(what+index).checked){
					
						oneChecked = true;
						break;
					}						
			}else{
			
				break;
			}
			index++;
		}
	
	if(!oneChecked){
	
		return false;
	
	}
	return true;
}

//Disables all elements that are children of element wht
//ele: the element calling the method
//wht: the outer container 
//enable: whether or not to allow the element to enable as well as disable

function disableAll(ele,wht, exceptions, enable){

	var what = $(wht);
	var flag =  false;
	var elements;

	if(ele.checked || enable){						
		//	disableLeaves(ele,what.children);		
			elements = getChildrenByTagName(what,['IMG','SELECT','INPUT']);
			for(var i = 0; i < elements.length; i++){
				for(var k=0; k < exceptions.length; k++){  //disable all except those on exception list
						if( $(exceptions[k]).id == elements[i].id ){
							flag = true;
							exceptions.splice(k,1); //if found the exception, remove it from the compare list to increase performance
							break;
						}
					}
					if( !flag){							
						elements[i].disabled = ele.checked;
					}
					flag = false;
			}									
	}		


}


//if uncheck is true, the element is only allowed to uncheck the 'checking' parameter element
function check(ele, checking, uncheck){

	if(!uncheck){
		if(ele.checked){
			$(checking).checked = true;	
		}
	}else{
	
		if(!ele.checked){
			$(checking).checked = false;
			
		}
	
	}

}
//Verifies that the number of days entered is not larger than 999
function validateDays(ele){


	if(!Validate.checkRange(0,999,parseInt(ele.value))){
	
		alert("Hotlist period must be between 0 and 999 days.");
		ele.value = '0';
		ele.select();
	}else if(isNaN(ele.value)){
	
		alert("Hotlist period must be a numeric value.");
		ele.value = '0';
		ele.select();
	}
	
	if(ele.value ==''){
		ele.value = '0';
	}

}

//shows/hides the hot list edit options
function toggleHotListEdit(check,id){

	if(check.checked){
	
		$(id).style.visibility='visible';
	
	}else{
	
		$(id).style.visibility='hidden';
	}
}

function toggleMarket(id){
	
	
	if($(id)){
	
		if($(id).className == 'on'){		
			$(id).className = 'off';		
		}else{
			$(id).className = 'on';
		}
	}

}
function submitOnEnter(id){


	if(Util.isIE){
	
		if(event.keyCode == 13){
		
			$(id).click();
		
		}
	
	}else{
		if(e.keyCode == 13){
		
			$(id).click();
		
		}		
	}



}

function toggleOneTimeSearch(link,content,head){

	content = $(content);
	link = $(link);
	head = $(head);
	
	if(Element.visible(content)){
		Element.hide(content);
		Element.addClassName(link,'contracted');
		Element.addClassName(head,'noBottom');
	
	}else{
		Element.show(content);
		Element.removeClassName(link,'contracted');
		Element.removeClassName(head,'noBottom');
	}

		

}


function RangeSelect(){

	var lastOpened;
	var prevent = false;
	var lastMin;
	var lastMax;
	this.show = showRangeSelectPopup; //show the popup
	this.cancel =  cancelRangeSelectPopup;
	this.save = saveRangeSelectPopup;
	this.closeLast = closeLast;
	this.preventNew = preventNew;
	this.allowNew = allowNew;
	
	function showRangeSelectPopup(id){	
		if(prevent){
			return false;
		}
		closeLast();		
		var pop = getPopup(id);
		if(!pop){
			return false;
		}
		var minMilesEl = $('mileageMinId'+id);
		var maxMilesEl = $('mileageMaxId'+id);		
		lastMin = minMilesEl.value;
		lastMax = maxMilesEl.value;		
		Element.show(pop);		
		lastOpened = id;	
		return false;
	}
	function preventNew(){
		prevent = true;
	}
	function allowNew(){
		prevent = false;
	}
	function closeLast(){		
			cancelRangeSelectPopup(lastOpened);			
	}
	function getPopup(uniqueId){	
		var id = uniqueId;	
		if(!document.getElementById(id)){	
			id = 'rangeSelectPopup_'+uniqueId;
		}
		return $(id);
	}

	function cancelRangeSelectPopup(id){	
			//put oncancel actions here
			return  hidePopup(id);			
	}
	function hidePopup(id){
		var pop = getPopup(id);	
		if(pop){
			Element.hide(pop);
		}
		return false;	
	}
	function saveRangeSelectPopup(uniqueId,doUpdate){
		var minMilesEl = $('mileageMinId'+uniqueId);
		var maxMilesEl = $('mileageMaxId'+uniqueId);
		var displayElement =$('rangeSelect_'+uniqueId);		
		displayElement.innerHTML = minMilesEl.options[minMilesEl.selectedIndex].text+' - '+maxMilesEl.options[maxMilesEl.selectedIndex].text+' mileage ';
		if(doUpdate){
			switch(uniqueId){			
				case 'navigator':
					setYear();
					updateSearchDistance('distanceSelect', null, ['IN_GROUP', 'ONLINE'], 'modelSection', true, null, resetSelectedYear);
				break;
				default:
					updateModelResults(uniqueId);	
				break;			
			}		
		}
	return hidePopup(uniqueId);	
	}
}
var rangePop = new RangeSelect();

//returns an array of all of the child elements with tagname matching each array element of tags
function getChildrenByTagName(element, tags){
	
	if(!$(element)){
		alert('Get children error: '+element+' is not a valid element.');
		return new Array();
	}
	if(!tags || tags.length == 0){
		alert('Get children error: At least one tag must be supplied.');
		return new Array();
	}
	var tagElements = new Array();
	var retArray;
	var ele = $(element);
				
	retArray = performRecursion(tagElements,ele, tags);
	
	function performRecursion(a, ele, tags){
	
			var children = ele.children;
			
			for(var i=0; i < children.length; i++){
			
				if(children[i].children.length > 0){					
					performRecursion(a,children[i], tags);
				}else{
					for(var j = 0; j < tags.length; j++){
						if(children[i] && children[i].tagName == tags[j]){							
							a.push(children[i]);							
						}
					}
				}
			
			}
			return a;			
	}
	
	return retArray;

}

function getPreviousResult(activeNum) { pageResults(activeNum,'previous'); }
function getNextResult(activeNum) { pageResults(activeNum,'next'); }
function pageResults(activeNum,whichWay) {
	var currentEl = $('result'+activeNum);
	var nextEl = whichWay=='previous' ? $('result'+(activeNum-1)) : $('result'+(activeNum+1));
	Element.toggle(currentEl);
	Element.toggle(nextEl);
}
