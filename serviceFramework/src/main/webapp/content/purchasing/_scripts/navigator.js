var analyticsRequest; //resquest object for analytic modules
var analyticsIsOpen = false;
var analyticsPlaceholder;
var mmrLastTransColClicked = 0;
var mmrColClickedState = 0;
var transTableColumns = ["Date", "Auction", "Sale Type", "Price", "Odometer"];

function updateAuctionData(){
	var pars = Form.serialize('naaaAuctionForm');
    	new Ajax.Updater(
			'naaa_results',
			'/NextGen/NAAAAuctionValues.go',
			{
				method:'post',
				parameters: stampURL(pars)
			});
}

function updateMMRAuctionData(){
	var pars = Form.serialize('mmrAuctionForm');
    	new Ajax.Updater(
			'mmr_results',
			'/IMT/MMRAuctionValues.go',
			{
				method:'post',
				parameters: stampURL(pars),
				onComplete: writeMMRTable
			});
}

function addCommasToNumber(num)
{
	if ((num == undefined) || (num < 0))
	{
		return num;
	}

	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function writeMMRTable(){
	
		
	abc=document.getElementById("MMRTrimJSON").value;
	var mmrVehSummary=jQuery.parseJSON(abc);
	
		
	if ((mmrVehSummary == null) || (mmrVehSummary == '') || (mmrVehSummary == 'null'))
    {
    	jQuery("#AuctionDataDiv").html("<div class='mmrlogowithbook'></div><div>There is no MMR data.</div>");
    }
    else
    {
    	
    

	    var salesPriceMax = "-";

	    if (mmrVehSummary.salesPrices.max > 0)
	    {
	    	salesPriceMax = "$" + addCommasToNumber(mmrVehSummary.salesPrices.max);
	    }
	    
	    var salesPriceAvg = "-";

	    if (mmrVehSummary.salesPrices.average > 0)
	    {
	    	salesPriceAvg = "$" + addCommasToNumber(mmrVehSummary.salesPrices.average);
	    }

	    var salesPriceMin = "-";

	    if (mmrVehSummary.salesPrices.min > 0)
	    {
	    	salesPriceMin = "$" + addCommasToNumber(mmrVehSummary.salesPrices.min);
	    }
	    
	    var mileageMax = "-";

	    if (mmrVehSummary.mileages.max > 0)
	    {
	    	mileageMax = addCommasToNumber(mmrVehSummary.mileages.max);
	    }
	    
	    var mileageAvg = "-";

	    if (mmrVehSummary.mileages.average > 0)
	    {
	    	mileageAvg = addCommasToNumber(mmrVehSummary.mileages.average);
	    }
	    
	    var mileageMin = "-";

	    if (mmrVehSummary.mileages.min > 0)
	    {
	    	mileageMin = addCommasToNumber(mmrVehSummary.mileages.min);
	    }
	    
    	jQuery("#abovemmr").html(salesPriceMax);
    	jQuery("#belowmmr").html(salesPriceMin);
    	jQuery("#averagemmr").html(salesPriceAvg);
    	jQuery("#abovemmrodom").html(mileageMax);
    	jQuery("#belowmmrodom").html(mileageMin);
    	jQuery("#averagemmrodom").html(mileageAvg);
    	
    	writeMMRTransactions();
    }
	
	
}


function writeMMRTransactions(){

	var abc=document.getElementById("MMRTransactionsJSON").value;
		
	var mmrVehTransactions= jQuery.parseJSON(abc);
	jQuery("#mmrprevious").unbind("click");
	jQuery("#mmrnext").unbind("click");
	
	var min=0;
	
	min=eval(jQuery("#minimiumMMRValue").val());
	
	var max = min+5;

	if(min<=0){
		min=0;
		jQuery("#mmrprevious").removeClass("mmrpagerActive");
		jQuery("#mmrprevious").addClass("mmrpagerdisabled");
	}
	else{
		jQuery("#mmrprevious").removeClass("mmrpagerdisabled");
		jQuery("#mmrprevious").addClass("mmrpagerActive");
		jQuery("#mmrprevious").click(previouspageMMRTransactions);
	}
	
	if(max>=mmrVehTransactions.transactions.length){
		max=mmrVehTransactions.transactions.length;
		jQuery("#mmrnext").removeClass("mmrpagerActive");
		jQuery("#mmrnext").addClass("mmrpagerdisabled");
	}
	else{
	
		jQuery("#mmrnext").removeClass("mmrpagerdisabled");
		jQuery("#mmrnext").addClass("mmrpagerActive");
		jQuery("#mmrnext").click(nextpageMMRTransactions);
	}
	
	jQuery("#mmrnumtrans").html(min + 1 + " - " + max + " of " + mmrVehTransactions.transactions.length);
	
	
	var table="";
	if(mmrVehTransactions.transactions.length>0){
		table="<table id='mmrtranstable' ><thead><tr class='trclass1'>";
		table+="<th id='mmrTransCol0'>Date</th>";
		table+="<th id='mmrTransCol1'>Auction</th>";
		table+="<th id='mmrTransCol2'>Sale Type</th>";
		table+="<th id='mmrTransCol3'>Price</th>";
		table+="<th id='mmrTransCol4'>Odometer</th>";
		table+="</tr></thead><tbody>";
		
		for (var i = min,j=0; i < max; j++,i++){
			table+="<tr class='trclass"+(j%2)+"'>";
			table+="<td>"+mmrVehTransactions.transactions[i].date+"</td>";
			table+="<td>"+mmrVehTransactions.transactions[i].auction+"</td>";
			table+="<td>"+mmrVehTransactions.transactions[i].salesType+"</td>";
			table+="<td>"+mmrVehTransactions.transactions[i].price+"</td>";
			table+="<td>"+mmrVehTransactions.transactions[i].mileage+"</td>";
			table+="</tr>";
		}
		
		table+="</tbody></table>";
	}
	else{
		table="No Records Found";
	}
	
	
	
	jQuery("#MMRTransactionsDiv").html(table);
	
	
    
	

}



function previouspageMMRTransactions(){
	var value=eval(jQuery("#minimiumMMRValue").val());
	value-=5;
	if(value<0)
		value=0;
	jQuery("#minimiumMMRValue").val(value);
	writeMMRTransactions();
}

function nextpageMMRTransactions(){
	var value=eval(jQuery("#minimiumMMRValue").val());
	value+=5;
	jQuery("#minimiumMMRValue").val(value);
	writeMMRTransactions();
}

function mmrDate(date)
{
	if (date.length < 9)
	{
		return "";
	}

	return date.substring(4,6) + "/" + date.substring(6,8) + "/" + date.substring(0, 4);
}

function updateMMRAuction(){
	var pars = Form.serialize('mmrCommonForm');
	new Ajax.Updater(
			'commonMMRAuction',
			'/IMT/MMRAuction.go',
			{
				method:'get',
				parameters: pars,
				onComplete: updateMMRAuctionData
			});
}

function viewCarfaxReport(baseUrl,sVIN,sReportType) {
	if (sReportType == "NONE") {
		return false;
	}
	var bIncludeInHotList = false;
    var url = baseUrl + '?vin=' + sVIN + '&reportType=' + sReportType + '&cccFlagFromTile=' + bIncludeInHotList + '&isNavigator=true';
	pop(url,'thirdparty');
}


// THE NAVIGATOR  ///////////////

var destinationUrl = '/NextGen/NavigatorDefaultFrame.go';
function updateSuccess(r){
	if (r.getResponseHeader('selectedUrl') != '/NextGen/NavigatorDefaultFrame.go' && r.getResponseHeader('selectedUrl') != '') {
		destinationUrl = r.getResponseHeader('selectedUrl');
		logClick(r.getResponseHeader('selectedVIN'),r.getResponseHeader('selectedUrl'),r.getResponseHeader('selectedMarketId'));
	}
	updateFrame('navigatorFrame',destinationUrl);
};

function handleMarketChange(){
    // get the index of the selected marketplace in the dropdown
    var navigatorMarketplaceIndex = document.getElementById("navigatorMarketplaceIndex").value;
	var params = 'navigatorMarketplaceIndex=' + navigatorMarketplaceIndex;
		var map = {
				ele:          'results',
				path:        	'/NextGen/ChangeMarket.go',
				options:      params,
				onSuccess:    updateSuccess	
			};
	removeTabTitles();
	return makeRequest(map);
}


function handleModelChange(){
    var selectedModelId = $F('modelList');
	if(selectedModelId == 'off') {
		return; 
	}
	var params = 'nodeId=' + selectedModelId;
	var map = {
		ele:          'modelSection',
		path:        '/NextGen/ChangeModel.go',
		msg:          'Searching...',
		options:      params,
		onSuccess:    updateSuccess	
	};
	removeTabTitles();
	return makeRequest(map);
}
var year;

function getVehiclesByYear(groupId) {
	var destinationUrl = '/NextGen/NavigatorDefaultFrame.go';
	if(analyticsIsOpen) { return; }
	if(!$('yearValue'+groupId)){return;}
	var selectedYearObj = $('modelyear'+groupId);
	
	year = $('yearValue'+groupId).innerHTML;
	
	var selIsCurrActive = Element.hasClassName(selectedYearObj,'active');

	if(selIsCurrActive) { return; }
	var url = '/NextGen/ChangeYear.go';
	var pars = 'nodeId='+groupId;
	var tar = $('vehicles');
	var newGroup = new Ajax.Updater(
		tar, url, {	
			method: 'get', 
			parameters: stampURL(pars), 
			onComplete: function(r) {
				//change state of tabs
				if(selectedYearObj){ //if there are results for the year and it is showing, then set it as active
					removeActiveClasses('modelyears','li');
					addAfterActiveClass('modelyears','li',selectedYearObj);
					Element.addClassName(selectedYearObj,'active');
				}
				if (r.getResponseHeader('selectedUrl')) {
					destinationUrl = r.getResponseHeader('selectedUrl');
					//This logClick is handled by ViewNavigatorAction
					//logClick(r.getResponseHeader('selectedVIN'),r.getResponseHeader('selectedUrl'),r.getResponseHeader('selectedMarketId'));				
				}
 				updateFrame('navigatorFrame',destinationUrl);
			}
	});	
}

function updateCurrentModelYearDisplay( newYear )
{
    if ($('currentModelYear'))
    {
        $('currentModelYear').innerHTML = newYear + ' ';
    }
}

function resetSelectedYear(r){ //called by distance select callback

	var itemId = findActiveByYear('modelyears','li',year); //this needs to be called because the node id's do no match when distance is  updated
	getVehiclesByYear(itemId); //reset the selected tab
	updateSuccess(r);
}

function setYear(){
	year = findYearByActive( 'modelyears','li');
}
//locate the selected year based upon the active state of the outer li
function findYearByActive( parentId,targetTag ){
	var cont = $(parentId);
	var elements = cont.getElementsByTagName(targetTag);	
	elements = $A(elements);
	var year;
	elements.each( function(el){
		if(Element.hasClassName(el, 'active')){					
				var retId = el.id.substring(el.id.indexOf('modelyear')+9);
				year = $('yearValue'+retId).innerHTML;				
		}
	});
	return year;
}
//locate the active li based upon the year
function findActiveByYear(parentId,targetTag,year){
	var cont = $(parentId);
	var elements = cont.getElementsByTagName(targetTag);	

	elements = $A(elements);
	var retId;
	elements.each( function(el){

		var numId = el.id.substring(9);
		
		var testYear = $('yearValue'+numId).innerHTML;
		
		if(parseInt(testYear) == parseInt(year)){
			
			retId = numId;
		}
	});

	return retId;

}

function updateNavigatorFrame(rowId,destinationUrl,marketType,vin,make,model,segment,trim,mmgId,year,mileage,groupingDescriptionId,marketId,vehicleEntityTypeId,vehicleEntityId) {
	
	if(analyticsIsOpen) { return; }
	//move row highlight
	removeActiveClasses('vehicles','tr');
	Element.addClassName(rowId,'active');
	//log click
	logClick(vin, destinationUrl, marketId);
	
	if(mileage==''){ //prevent dpp if no mileage
		mileage = 0;
	}
	//update frame src
	updateFrame('navigatorFrame',destinationUrl);
    // update the hidden fields for analytics
 	$('performanceParams').value = 'make='+make+'&model='+ model+'&segment='+segment+'&trim='+trim+'&groupingDescriptionId='+groupingDescriptionId+'&year='+year+'&mileage='+ mileage+'&vin='+vin+'&vehicleEntityTypeId='+vehicleEntityTypeId+'&vehicleEntityId='+vehicleEntityId;
	$('auctionDataParams').value = 'makeModelGroupingId='+mmgId+'&year='+year+'&mileage='+ mileage+'&vin='+vin;
	$('bookValuesParams').value = '?make='+make+'&model='+ model+'&vehicleYear='+year+'&mileage='+ mileage+'&vin='+vin;
}

function logClick(vin, destinationUrl, marketId) {
	//log click
	var trackingUrl = '/NextGen/LogClickAction.go';
	var logClickReq = new Ajax.Request(
		trackingUrl, 
		{
			method: 'get',
			parameters: stampURL('vin='+vin+'&url='+ escape(destinationUrl) +'&marketId='+marketId)
		});
 	return;
}

function updateFrame(which,url) {
 if(!url) url = '/NextGen/Dref.go';
 $(which).src = url;
 destinationUrl = '/NextGen/NavigatorDefaultFrame.go';
}

function loadAnalyticsModule(tabName,url) {
	var selectedTab = tabName+'Tab';
	var check;
	check = tabName == 'bookout' ? $('bookValuesParams'):(tabName == 'commonAuction' || tabName == 'naaaAuction')? $('auctionDataParams'):$('performanceParams');
	if(!check) {
		setTabTitles('A vehicle must be selected to view Analytics data.');
		return;
	}
	if(analyticsRequest) { 
		analyticsRequest = '';
	}
	var pars;
	if(tabName == 'bookout') {
		pars = $('bookValuesParams').value;
		//TradeAnalyzerForm needs the context parameter!!! do not remove!!!
		url += pars + "&context=search";
		pop(url,'thirdparty');
		return;
	}
	if(!analyticsPlaceholder) {
		analyticsPlaceholder = $('analytics').innerHTML;
	}
	$('analytics').innerHTML = analyticsPlaceholder;
	//first tab requires different active class name
	var isFirstTab = Element.hasClassName(selectedTab,'first');
	var classStr = isFirstTab?'first_active':'active';

	var doCollapse = Element.hasClassName(selectedTab,'active') || Element.hasClassName(selectedTab,'first_active');
	if(doCollapse) {
		closeAnalyticsModule();
	}
	else {
		//add bkgd behind all tabs
		if(!Element.hasClassName('tabs_analytics','open')) { Element.addClassName('tabs_analytics','open'); }
		//remove all active class names
		removeActiveClasses('tabs_analytics','li');	
		//add active class name to selected tab		
		Element.addClassName(selectedTab,classStr);
		if (tabName == 'perfSummary'){ pars = $('performanceParams').value; }
		if (tabName == 'naaaAuction'||tabName == 'commonAuction'){ pars = $('auctionDataParams').value; }

		analyticsRequest = new Ajax.Updater(
			'analytics', url, 
			{
				method: 'get', 
				parameters: stampURL(pars),
				onComplete: updateMMRAuction 
			});
		//show analytics container
		$('analytics_wrap').style.display='block';
		analyticsIsOpen = true;
	}
}


function closeAnalyticsModule() {
	if(analyticsRequest) { 
		analyticsRequest = ''; 
	}
	//remove bkgd behind all tabs
	Element.removeClassName('tabs_analytics','open'); 
	//remove all active class names
	removeActiveClasses('tabs_analytics','li');			
	//hide analytics container
	Element.hide('analytics_wrap');	
	analyticsIsOpen = false;
	$('analytics').innerHTML = analyticsPlaceholder;
}
function removeTabTitles() {
	if($('perfSummaryTab')) {
		$('perfSummaryTab').title= '';
	}
	if($('naaaAuctionTab')) {
		$('naaaAuctionTab').title= '';
	}
	if($('bookoutTab')) {
		$('bookoutTab').title= '';
	}
}
function setTabTitles(msg) {
	if($('perfSummaryTab')) {
		$('perfSummaryTab').title= msg;
	}
	if($('naaaAuctionTab')) {
		$('naaaAuctionTab').title= msg;
	}
	if($('bookoutTab')) {
		$('bookoutTab').title= msg;
	}
}
function removeActiveClasses(parentId,targetTag) {
	var cont = $(parentId);
	var elements = cont.getElementsByTagName(targetTag);	
	elements = $A(elements);
	elements.each( function(el){
		if(el.id) {
			var isFirstActive = Element.hasClassName(el.id,'first_active');
			var isActive = Element.hasClassName(el.id,'active') || isFirstActive;
			if(isFirstActive) { 
				Element.removeClassName(el.id,'first_active');	
				Element.addClassName(el.id,'first');
			}
			else { 
				Element.removeClassName(el.id,'active');
			}
		}
	});	
}

function addAfterActiveClass(parentId,targetTag,selectedEl) {
	var cont = $(parentId);
	var elements = cont.getElementsByTagName(targetTag);	
	elements = $A(elements);
	var prevElWasSelected = false;
	elements.each( function(el,i){
		if(el.id) {
			var isAfter = Element.hasClassName(el.id,'after');
			if(isAfter) { Element.removeClassName(el.id,'after'); }
			if(prevElWasSelected) {	Element.addClassName(el.id,'after'); }
			prevElWasSelected = $(selectedEl).id == el.id;
		}
	});	
}

function setHiddenField(par,id){
	$(id).value = $(par).options[$(par).selectedIndex].text;
}


// this should be more general to accomodate varying iframe configurations,
// there is an in-progress iframe.jspf for layouts that will be used with this - zF.
function setNavigatorFrameHeight(e) {
	var frameObj = document.getElementById(e.id);
	var aFramePos = findPosition(frameObj);
	var nFramePosY = aFramePos[1];
	var aViewDims = findViewportDims(); //returns x=[0] and y=[1]
	var nViewHeight = aViewDims[1];
	frameObj.style.height = nViewHeight-nFramePosY + 'px';
}

// paging functions
function getPreviousResultsPage(activeNum) { pageResults(activeNum,'previous'); }
function getNextResultsPage(activeNum) { pageResults(activeNum,'next'); }
function pageResults(activeNum,whichWay) {
	var currentEl = $('page'+activeNum);
	var nextEl = whichWay=='previous' ? $('page'+(activeNum-1)) : $('page'+(activeNum+1));
	currentEl.toggle();
	nextEl.toggle();
}