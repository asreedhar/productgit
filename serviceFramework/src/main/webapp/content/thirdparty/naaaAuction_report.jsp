<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<h4>${param.modelYear} ${vicBodyStyle} &mdash; ${param.timePeriodDescription} &mdash; ${param.areaName} <fmt:message key="l.region"/></h4>
<div class="tableContent">
	<display:table name="${report}" requestURI="/AuctionReport.go" class="thirdparty">	
		<display:setProperty name="css.tr.even" value="even"/>	  
		<display:setProperty name="basic.empty.showtable" value="true"/>
		<display:setProperty name="basic.msg.empty_list_row">
<tr class="empty"><td colspan="{0}">No data found to display.</td></tr>
		</display:setProperty>
		<display:column class="num" sortable="true" decorator="biz.firstlook.main.display.util.RowNumberDecorator" title=""/>			  
		<display:column decorator="biz.firstlook.main.display.util.ShortDateDecorator" property="saleDate" sortable="true" title="Sale Date"/>
		<display:column property="regionName" sortable="true" title="NAAA Region"/>
		<display:column property="saleTypeName" sortable="true" title="Sale Type"/>
		<display:column property="vicBodyStyle" sortable="true" title="NAAA Series"/>
	    <display:column decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" property="salePrice" title="Price"/>
	  	<display:column decorator="biz.firstlook.main.display.util.MileageDecorator" sortable="true" property="mileage" title="Mileage"/>
	  	<display:column property="engine" sortable="true" title="Engine"/>
	  	<display:column property="transmission" sortable="true" title="Transmission"/>
	</display:table>
  	 <cite>
  	 	<fmt:message key="disclaim.naaaAuctionReport"><fmt:param><fmt:formatDate value="${TODAY}" pattern="yyyy"/></fmt:param></fmt:message>
  	 </cite>
</div>