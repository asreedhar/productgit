<tiles:importAttribute/>
<c:if test="${noSeriesSelected}" var="suppressReportLink" scope="request"/><c:if test="${fn:length(vicBodyStyles) gt 1}" var="hasMultipleSeries" scope="page"/>
<div class="module" id="naaaAuction">
	<div class="head">
		<h5 class="noFloat"><fmt:message key="tile.naaa.header"/></h5>
<form name="naaaAuction" id="naaaAuctionForm">
		<input type="hidden" name="usingVIC" id="usingVIC" value="${usingVIC}" />
		<input type="hidden" name="modelYear" id="modelYear" value="${modelYear}" />
		<fieldset>
			<label for="seriesSelect"><fmt:message key="l.series"/>:</label>
				<select name="vic" id="seriesSelect"  onchange="updateAuctionData();"<c:if test="${!hasMultipleSeries}"> disabled="disabled"</c:if>>
			        <option value="0"><fmt:message key="l.selectASeries"/></option>
			    <c:forEach items="${vicBodyStyles}" var="vicBodyStyle" varStatus="i">
					<c:if test="${!hasMultipleSeries}">
						<c:set var="vic" value="${vicBodyStyle.vic}"/>
					</c:if>
			        <option value="${vicBodyStyle.vic}" <c:if test="${!hasMultipleSeries}"> selected="selected"</c:if>>${vicBodyStyle.bodyStyleDescription}</option>
			    </c:forEach>
			</select>
			<c:if test="${!hasMultipleSeries}">
				<input type="hidden" name="vic" id="vic" value="${vic}" />
			</c:if>
			<label for="areaSelect"><fmt:message key="l.region"/>:</label>
			<c:set var="areaId" value="1"/>
			<select name="areaId" id="areaSelect" class="area" onchange="setHiddenField(this,'areaDescription');updateAuctionData();">
			    <c:forEach items="${areas}" var="area" varStatus="i">
			    <c:if test="${i.index == 0}"><c:set var="areaDescription" value="${area.areaName}"/></c:if>
			        <option value="${area.areaId}">${area.areaName}</option>
			    </c:forEach>
			</select>
			<input type="hidden" name="areaDescription" value="${areaDescription}"/>
			<label for="timePeriodSelect"><fmt:message key="l.timePeriod"/>:</label>
			<select name="timePeriodId" id="timePeriodSelect" class="period" onchange="setHiddenField(this,'timePeriodDescription'); updateAuctionData();">
			    <c:forEach items="${timePeriods}" var="timePeriod" varStatus="i">
					<c:if test="${timePeriod.periodId eq defaultTimePeriodId}"><c:set var="timePeriodId" value="${timePeriod.periodId}"/><c:set var="timePeriodDescription" value="${timePeriod.description}"/></c:if>	    
			        <option value="${timePeriod.periodId}"${timePeriod.periodId eq defaultTimePeriodId?' selected="selected"':''}>${timePeriod.description}</option>
			    </c:forEach>
			    <input type="hidden" id="timePeriodDescription" name="timePeriodDescription" value="${timePeriodDescription }"/>
			</select>
		</fieldset>
		<input type="hidden" name="mileage" value="${mileage}"/>
</form>
	</div>
	<div class="body" id="naaa_results">
	    <c:import url="/NAAAAuctionValues.go"><c:param name="mileage" value="${mileage}"/><c:param name="vic" value="${vic}"/><c:param name="timePeriodDescription" value="${timePeriodDescription}"/><c:param name="areaId" value="${areaId}"/><c:param name="areaDescription" value="${areaDescription}"/><c:param name="vin" value="${vin}"/><c:param name="timePeriodId" value="${timePeriodId}"/><c:param name="usingVIC" value="${usingVIC}"/><c:param name="modelYear" value="${modelYear}"/></c:import>
	</div>
	<div class="foot">
		<a href="javascript:pop('<fmt:message key="link.url.manheim"><fmt:param value="${param.vin}"/></fmt:message>','thirdparty');"><fmt:message key="link.text.manheim"/></a>
	</div>
</div>