<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="mmrCommonForm" >
<input type="hidden" name="year" value="${param.year}">
<input type="hidden" name="vin" value="${param.vin}">
</form>
<div class="module" id="commonMMRAuction" >
		  <img src="<c:url value="/common/_images/icons/loading.gif"/>" class="loading"/>Loading MMR Data ... 
</div>


<hr style="height:1px;border:none;color:#333;background-color:#333;" />

<div class="module" id="commonNAAAAuction">
	
	    <c:import url="/NAAAAuction.go"><c:param name="year" value="${param.year}"/><c:param name="vin" value="${param.vin}"/><c:param name="mileage" value="${param.mileage}"/></c:import>
	
</div>