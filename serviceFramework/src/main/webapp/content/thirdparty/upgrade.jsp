<tiles:importAttribute/>
<div class="module">
		<div class="head">
	<img src="<c:url value="/common/_images/logos/FL-89x21-transOnE5.gif"/>" width="89" height="21" class="flLogo">	
	<h4><fmt:message key="instruct.auction.upgrade.header"/></h4>
		</div>
		<div class="body">
	<fmt:message key="instruct.auction.upgrade"/>
		</div>
	<div class="foot">
		<a href="javascript:pop('<fmt:message key="link.url.manheim"><fmt:param value="${param.vin}"/></fmt:message>','thirdparty');"><fmt:message key="link.text.manheim"/></a>
	</div>
</div>