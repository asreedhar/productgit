<c:choose>
	<c:when test="${empty inventory}">
		I HAS NO VEHICLE
	</c:when>
	<c:otherwise>
	Vin:	${inventory.vehicle.vin }
	<br>
	Trim:	${inventory.vehicle.trim }
	<br>
	Internet Price:	${inventory.listPrice}
	<br>
	Mileage:	${inventory.mileageReceived }
	<br>	
	BaseColor:	${inventory.vehicle.baseColor }
	<br>
	Age:	${inventory.age}
	<br>
	Unit Cost:	${inventory.unitCost }
	<br>
	Stock Number:	${inventory.stockNumber }
	<br>
	Certified:	${inventory.certified }
	<br>
	</c:otherwise>
</c:choose>