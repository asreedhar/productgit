
	<table>
		<%--<caption>Watch List ${fn:length(VehicleLevelReportBlock['1'])}</caption>--%>
		<tr>
			<th axis="age">Age</th>
			<th axis="risk">Risk</th>
			<th axis="vehicle">Vehicle Info</th>
			<th axis="color">Color</th>
			<th axis="mileage">Mileage</th>
			<th axis="stock">Stock #</th>
			<th axis="list">Internet Price</th>
		</tr>
<c:forEach items="${VehicleLevelReportBlock['6']}" var="vehicle">
		<c:if test="${param.inventoryId ne vehicle.inventoryId}">
			<c:set var="_select_link">
				<a href="<c:url value="/InventoryDetails.go"><c:param name="inventoryId" value="${vehicle.inventoryId}" /></c:url>" target="inventoryVehicle" />select</a>
			</c:set>
		</c:if>		
		<tr>
			<td><c:out value="${_select_link}" default="selected" escapeXml="false" /></td>
			<td>${vehicle.AgeInDays}</td>
			<td>${vehicle.CurrentVehicleLight}</td>
			<td>${vehicle.GroupingDescriptionStr}</td>
			<td>${vehicle.BaseColor}</td>
			<td>${vehicle.MileageReceived}</td>
			<td>${vehicle.StockNumber}</td>
			<td>${vehicle.ListPrice}</td>
		</tr>
</c:forEach>
	</table>