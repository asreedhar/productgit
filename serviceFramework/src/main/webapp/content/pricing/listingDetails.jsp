<c:choose>
	<c:when test="${empty listing}">
		I HAS NO INTERNET SIET VEHICLE
	</c:when>
	<c:otherwise>
		Seller Name: ${listing.sellerName}
		<br/>
		Trim: ${listing.trim}
		<br/>
		Color: ${listing.color}
		<br/>
		Mileage: ${listing.mileage}
		<br/>
		Engine: ${listing.engine}
		<br/>
		Transmission: ${listing.transmission}
		<br/>
		DriveTrain: ${listing.driveTrain}
		<br/>
		FuelType: ${listing.fuelType}
		<br/>
		Options: ${listing.options}
		<br/>
		Ad Description: ${listing.adDescription}
		<br/>
		Internet Price: ${listing.listPrice}
		<br/>
		Stock Number: ${listing.stockNumber}
		<br/>
		Vin: ${listing.vin}
		<br/>
		<a href="${listing.listingURL}"> ${listing.listingURL} </a>
		<br/>
		<a href="${listing.imageUrls}"> ${listing.imageUrls} </a>
	</c:otherwise>
</c:choose>
