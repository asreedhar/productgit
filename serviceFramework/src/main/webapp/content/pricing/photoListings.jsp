	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	
	<script>
	function getPreviousResult(activeNum) { pageResults(activeNum,'previous'); }
function getNextResult(activeNum) { pageResults(activeNum,'next'); }
function pageResults(activeNum,whichWay) {
	var currentEl = $('result'+activeNum);
	var nextEl = whichWay=='previous' ? $('result'+(activeNum-1)) : $('result'+(activeNum+1));
//	Element.toggle(currentEl);
//	Element.toggle(nextEl);
}
	</script>
		<c:choose>
			<c:when test="${empty photos or fn:length(photos) == 0 }">
			 <img src="<c:url value="/content/purchasing/_images/noVehicleImageAvailable.gif"/>" width="146" height="74"/>
			</c:when>
			<c:otherwise>
				<c:forEach items="${photos}" var="photo" varStatus="index">
					<c:set var="photoUrl">
						<c:if test="${photo.photoStatusCode == 1}">${rootURL}</c:if>${photo.photoUrl}
					</c:set>
					<div id="result${index.count}" style="${index.count != 1 ? 'display:none':'' }">
						<img src="<c:url value="${photoUrl}"/>" width="146" height="74"/>
	
						<div style="float:center;padding:8px" >
							<c:set var="appCount" value="${fn:length(photos)}"/>
							<c:choose>
								<c:when test="${appCount == 1}">
									${index.count} of ${appCount}  
								</c:when>
								<c:when test="${index.count == 1}">
									${index.count} of ${appCount}  
									<a onClick="getNextResult(${index.count})" style="cursor:hand"> Next </a>
								</c:when>
								<c:when test="${index.count == appCount}">
									<a onClick="getPreviousResult(${index.count})" style="cursor:hand" style="cursor:hand"> Previous </a>
						 			${index.count} of ${appCount}
								</c:when>
								<c:otherwise>
									<a onClick="getPreviousResult(${index.count})" style="cursor:hand" style="cursor:hand"> Previous </a>  
									${index.count} of ${appCount}  
									<a onClick="getNextResult(${index.count})" style="cursor:hand"> Next </a>
								</c:otherwise>
							</c:choose>
						</div>
		 		 </div>
    		</c:forEach>
    	 </c:otherwise>
		</c:choose>