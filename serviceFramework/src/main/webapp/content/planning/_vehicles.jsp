<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${isEmptyResults}">
	<div class="noneInGroupMsg" style="display:block;margin:20px 0 15px 0;">
		<strong>There are no vehicles in this view</strong>
	</div>
</c:if>
<c:set var="_count" value="0" />
<c:forEach items="${bucketKeySet}" var="bucket" varStatus="s">

		<c:if test="${not isDetail}">
			<h3 class="agingHeader${s.first ?  ' f' : ''}">
			<c:set var="groupTotal" value="${fn:length(VehicleLevelReportBlock[bucket])}"/>
				<strong>${groupByDescriptions[bucket]}</strong>
				<span>(<ins class="filteredValueDisplay"><strong id="ageGroup_${bucket}FilteredTotal">${groupTotal}</strong> of </ins>${fn:length(VehicleLevelReportBlock[bucket])})</span>
<c:if test="${s.first && not isPrintPage}">
	<a href="javascript:;" id="expandDetailsLink" class="CLOSED"><fmt:message key="plan.expandAllPlanDetails"/></a>
</c:if>
			</h3>
		</c:if>

	<div id="ageGroup_${bucket}" class="${fn:length(bucketKeySet) lt 1 ? 'emptyGroup':''}">
		<c:if test="${not isPrintPage}">
			<div class="noneInGroupMsg">
				<strong>There are no vehicles in this view</strong>
			</div>
		</c:if>
		<table cellpadding="0" cellspacing="0" border="0" class="aging l">
			<thead>
			<tr>
				<th class="age"><div class="age"><fmt:message key="l.age"/></div></th>
				<th class="light"><img src="<c:url value="/common/_images/d.gif"/>" width="10" height="1" /></th>		
				<th class="ymm"><img src="<c:url value="/common/_images/d.gif"/>" class="widthFix" /></th>			
				<th class="color"><div class="color"><fmt:message key="l.color"/></div></th>
				<th class="mileage"><div class="mileage"><fmt:message key="l.mileage"/></div></th>
<c:if test="${useLotPrice}">
				<th class="lotp"><fmt:message key="l.lotp"/></th>
</c:if>
<c:if test="${hasTransferPricing}">				
		<th class="transferp">
			<div class="transferp"><fmt:message key="l.transferp"/></div>
		</th>
</c:if>
				<th class="lp"><div class="lp"><fmt:message key="l.lp"/></div></th>
				<th class="uc"><div class="uc"><fmt:message key="l.uc"/></div></th>
				<th class="bookAvg"><div class="bookAvg">${empty primaryBookName ? 'Book Value': primaryBookName eq 'Kelley Blue Book' ? 'KBB' : primaryBookName}<br />${primaryBookPreference=='Average'?'Avg.':primaryBookPreference=='Extra Clean'?'E.C.':primaryBookPreference}</div></th>	
				<th class="bvc"><div class="bvc"><fmt:message key="l.bvc"/></div></th>
				<th class="bvc"><div class="bvc"><fmt:message key="l.mmr"/></div></th>
				<th class="bvc"><div class="bvc"><fmt:message key="l.mmrBvc"/></div></th>
				<th class="tp"><div class="tp">&nbsp;</div></th>	
				<th class="snum"><div class="snum">&nbsp;</div></th>		
			</tr>
			</thead>
		</table>
		<c:set var="BUCKET_TOTAL" value="0"/>
		<c:forEach items="${VehicleLevelReportBlock[bucket]}" var="_vehicle" varStatus="_j">
<c:set var="vehicle" value="${_vehicle}" scope="request" /><c:set var="iter" value="${_count + 1}" scope="request" />		
			<div id="vehicle${iter}">
			
				<c:import url="/content/planning/_vehicle.jsp">
					<c:param name="inventoryId" value="${_vehicle.inventoryId}" />
					<c:param name="isLastInGroup" value="${_j.last}" />
					<c:param name="isLast" value="${s.last and _j.last}" />
					<c:param name="ageGroup" value="ageGroup_${bucket}" />
				</c:import>

			</div>
<c:set var="_count" value="${iter}" /><c:remove var="vehicle" /><c:set var="BUCKET_TOTAL" value="${BUCKET_TOTAL +1 }"/>
		</c:forEach>
<input style="display:none;" type="hidden" value="${BUCKET_TOTAL}" id="ageGroupCount${bucket}" />
<c:remove var="BUCKET_TOTAL" />
	</div>
</c:forEach>

<input style="display:none;" type="hidden" value="${_count}" id="VEHICLES_COUNT" />
