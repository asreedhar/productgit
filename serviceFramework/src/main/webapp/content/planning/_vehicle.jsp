<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${empty isQuickAdPage}">
	<c:choose>
		<c:when test="${not empty param.page}">
			<c:set var="isQuickAdPage" value="${param.page eq 'QuickAd'}" scope="request" />
		</c:when>
		<c:otherwise>
			<c:set var="isQuickAdPage" value="${param.isQuickAdPage eq true}" scope="request" />
		</c:otherwise>	
	</c:choose>
</c:if>

<c:set var="iter" value="${iter}" scope="request" />
<c:set var="inventoryId" value="${param.inventoryId}" scope="request" />
<c:choose>
	<c:when test="${not empty singleInventory}">
		<c:set var="vehicle" value="${singleInventory}" scope="request" />
		<c:set var="iter" value="${param.iter}" scope="request"/>
		<c:set var="ageGroup" value="ageGroup_${vehicle.rangeId}" scope="request"/>
	</c:when>
	<c:otherwise>
		<c:set var="ageGroup" value="${param.ageGroup}" scope="request"/>
	</c:otherwise>
</c:choose>
<%@ include file="_includes/config_planItems.jspf" %>

<span class="${_filterClass_risk} ${_filterClass_objective} ${_filterClass_mileage}">
<input type="hidden" style="display: none;" class="classes" value="${inventoryId},${_filterClass_risk},${_filterClass_objective},${_filterClass_mileage},${ageGroup}"/>
<input type="hidden" style="display: none;" id="iter${inventoryId}" value="${iter}" />

<table cellpadding="0" cellspacing="0" border="0" id="item${iter}" class="aging${param.isLastInGroup ? ' l':''} ${_planStatusClass}">
<tfoot id="tfoot${iter}" style="${isQuickAdPage and param.enabled eq 'advertise' ? '':'display: none;'}">
		<tr>
<c:choose>
<c:when test="${useLotPrice && hasTransferPricing}">
			<td colspan="15">
</c:when>
<c:when test="${useLotPrice || hasTransferPricing}">
			<td colspan="14">
</c:when>
<c:otherwise>
			<td colspan="13">
</c:otherwise>
</c:choose>
				<c:import url="/content/planning/vehicle_strategySummary_expanded.jspf" />
				<c:if test="${isQuickAdPage}">
					<c:import url="/content/planning/_includes/advertising.jspf">
						<c:param name="level" value="vehicle"/>
						<c:param name="stockNumber" value="${vehicle.stockNumber}"/>
					</c:import>
				</c:if>
			</td>
		</tr>
	</tfoot>
	<tbody>
	<tr>
		<th class="age">
				<c:out value="${vehicle.ageInDays}" /><br />
				<img src="<c:url value="/common/_images/d.gif"/>" width="25" height="1" />
		</th>
		<th class="light">
			<img src="<c:url value="/common/_images/icons/light-9x9-${lightStr}.gif" />" width="9" height="9" class="screenOnly" />
			<span class="printOnly">${lightStr=='none' ? '&nbsp;' : fn:substring(lightStr,0,1)}</span>
		</th>
		<th class="ymm">
			<div id="vehDesc${iter}">
				<link:perfPlus link="${vehicleDescStr}" popup="true" grouping="${vehicle.groupingDescriptionId}" />	
			</div>
<img src="<c:url value="/common/_images/d.gif"/>" class="widthFix" />
		</th>
		<th class="color">
			<c:out value="${vehicle.baseColor}" default="&nbsp;" escapeXml="false" /><br />
			<img src="<c:url value="/common/_images/d.gif"/>" height="1" width="60" />
		</th>
		<th class="mileage">
			<fmt:formatNumber value="${vehicle.mileageReceived}" var="mileStr"/><c:out value="${mileStr}" default="&mdash;" escapeXml="false" /><br />
			<img src="<c:url value="/common/_images/d.gif"/>" height="1" width="60" />
		</th>
<c:if test="${useLotPrice}">				
		<th class="lotp">
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${vehicle.lotPrice}" var="lotPrice"/><c:out value="${lotPrice}" default="&mdash;" escapeXml="false" />
		</th>
</c:if>
<c:if test="${hasTransferPricing}">
<c:if test="${not isPrintPage}">
	<c:set var="_addtlAttr">onmouseover="showTooltip(this,'transferPriceDisplay${iter}', 'transferPrice')" onmouseout="this.title='';" ondblclick="showInlineTransfer(${iter});"</c:set>
</c:if>
		<th class="transferp ${hideInlineEdit and bRepriced ? 'REPRICED':''}" ${_addtlAttr}
			id="transferPrice${iter}" title="">
			<c:import url="/content/planning/_includes/transfer.jspf">
				<c:param name="level" value="vehicle" />
				<c:param name="transferPrice" value="${vehicle.transferPrice}" />
				<c:param name="isQuickAdPage" value="${isQuickAdPage}" />
			</c:import>
		</th>
</c:if>
<c:if test="${not isPrintPage}">
	<c:set var="_addtlAttr">onmouseover="showTooltip(this,'listPriceDisplay${iter}', 'listPrice')" onmouseout="this.title='';" ondblclick="showInlineReprice(${iter});"</c:set>
</c:if>
		<th class="lp ${hideInlineEdit and bRepriced ? 'REPRICED':''}" ${_addtlAttr}
			id="listPrice${iter}" title="">
			<c:import url="/content/planning/_includes/reprice.jspf">
				<c:param name="level" value="vehicle" />
				<c:param name="listPrice" value="${vehicle.listPrice}" />
				<c:param name="isQuickAdPage" value="${isQuickAdPage}" />
			</c:import>
		</th>
		<th class="uc">
			<fmt:formatNumber type="currency" maxFractionDigits="0" value="${vehicle.unitCost}" />
		</th>
		<th class="bookAvg">
			<fmt:formatNumber var="bookAvg" type="currency" 
				value="${vehicle.primaryBookValue}" maxFractionDigits="0" />
			<c:out value="${bookAvg}" default="&mdash;" escapeXml="false" />
		</th>
		<th class="bvc">
				<fmt:formatNumber type="currency" value="${vehicle.bookVsCost}" maxFractionDigits="0" var="bvc"/>
				<c:out value="${bvc}" default="&mdash;" escapeXml="false" />						
		</th>
		<th class="bvc">
			<fmt:formatNumber var="mmrValue" type="currency" 
				value="${vehicle.mmrValue}" maxFractionDigits="0" />
			<c:out value="${mmrValue eq '$0'?'&mdash;':mmrValue}" default="&mdash;" escapeXml="false" />
		</th>
		<th class="bvc">
				<fmt:formatNumber type="currency" value="${vehicle.mmrBookVsCost}" maxFractionDigits="0" var="mmrBvc"/>
				<c:out value="${mmrValue eq '$0'?'&mdash;':mmrBvc}" default="&mdash;" escapeXml="false" />						
		</th>
		<th class="tp">
				<%--${vehicle.TradeOrPurchase eq 1 ? 'PURCHASE' : 'TRADE-IN'}--%>${vehicle.tradeOrPurchase}
		</th>
		<th class="snum">
			<a name="stock${vehicle.stockNumber}" onfocus="highlightVehicle(this);" href="javascript:pop('<c:url value="oldIMT/EStock.go"><c:param name="stockNumberOrVin" value="${vehicle.stockNumber}"/><c:param name="fromIMP" value="true"/></c:url>','estock')" class="estockLink">${vehicle.stockNumber}</a>
<c:if test="${!vehicle.isAccurate}"><html:img pageKey="img.icon.inaccurate.small" titleKey="img.title.inaccurate" styleClass="inaccurate" /></c:if>
		</th>
	</tr>
	<tr>
<c:choose>
<c:when test="${useLotPrice && hasTransferPricing}">
		<td colspan="8">
</c:when>
<c:when test="${useLotPrice || hasTransferPricing}">
		<td colspan="7">
</c:when>
<c:otherwise>
		<td colspan="6">
</c:otherwise>
</c:choose>
			<c:if test="${showLotLocationAndStatus}">
				<div class="lotAndStatus">
					<c:if test="${!empty vehicle.vehicleLocation}"><fmt:message key="l.lotLocation" />: <span id="lotLocation${inventoryId}">${vehicle.vehicleLocation}</span></c:if>
					<c:if test="${!empty vehicle.inventoryStatusCodeShortDescription}"><fmt:message key="l.status" />: <span id="statusCode${inventoryId}">${vehicle.inventoryStatusCodeShortDescription}</span></c:if>
				</div>
			</c:if>
			
			<c:if test="${rangeId eq 1}">
				<c:import url="/content/planning/_includes/vehicle_insights.jspf" />&nbsp;
				<div class="watchPerf">
					<fmt:message key="l.us" />: <span>${vehicle.unitsSold}</span> <fmt:message key="l.ragp"/>: <span><fmt:formatNumber type="currency" maxFractionDigits="0">${vehicle.avgRetailGrossProfit}</fmt:formatNumber></span></div><div class="watchPerf"><fmt:message key="l.adts"/>: <span>${vehicle.avgDays2Sale}</span> <fmt:message key="l.ns"/>: <span>${vehicle.noSales}</span>
				</div>
			</c:if>
		</td>
		<td>
		<c:if test="${not isPrintPage}">
			<c:if test="${sessionScope.nextGenSession.hasPingIIUpgrade}">
			<a href="javascript:void(0)" onclick="window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="inventoryId" value="${inventoryId}"/><c:param name="popup" value="true"/></c:url>','PingII', 'status=1,scrollbars=1,toolbar=0,width=1009,height=860')" title="pingII">
				<html:img pageKey="img.icon.ping" styleClass="ping" />
			</a>
			</c:if>
			<c:if test="${sessionScope.nextGenSession.hasPingUpgrade && not(sessionScope.nextGenSession.hasPingIIUpgrade)}">
				<a href="javascript:pop('<c:url value="Ping.go"><c:param name="mmg" value="${vehicle.makeModelGroupingId}"/><c:param name="year" value="${vehicle.vehicleYear}"/><c:param name="distance" value="50"/><c:param name="zip" value="${zip}"/><c:param name="search" value="Search"/></c:url>','ping')" title="<fmt:message key="title.ping"/>">
					<html:img pageKey="img.icon.ping" styleClass="ping" />
				</a>
			</c:if>
		</c:if>
			<c:if test="${vehicle.hasSalesHistory}">
				<a href="javascript:pop('<c:url value="PricingAnalyzer.go"><c:param name="groupingDescriptionId" value="${vehicle.groupingDescriptionId}"/><c:param name="makeModelGroupingId" value="${vehicle.makeModelGroupingId}"/><c:param name="vehId" value="${inventoryId}"/><c:param name="unitCost" value="${vehicle.unitCost}"/><c:param name="year" value="${vehicle.vehicleYear}"/><c:param name="listPrice" value="${vehicle.listPrice}"/><c:param name="mileage" value="${vehicle.mileageReceived}"/><c:param name="inventoryItem" value="true"/><c:param name="inventoryId" value="${inventoryId}"/></c:url>','price',popupY,'${inventoryId}')" 
					title="<fmt:message key="title.pricingAnalyzer"/>" 
					id="pa${inventoryId}">
					<html:img pageKey="img.icon.pricingAnalyzer" styleClass="pa" />
				</a>
			</c:if>
		</td>	
		<td colspan="4" class="summary" id="summary${inventoryId}Current">
			<div class="summaryText">
				<c:import url="/content/planning/vehicle_strategySummary.jspf" />
			</div>
		</td>
		<td colspan="2" class="actions">
			<c:if test="${not isPrintPage}">
				<c:import url="/content/planning/_includes/link_openVehiclePlan.jspf">
					<c:param name="isQuickAdPage" value="${isQuickAdPage}" />
				</c:import>
			</c:if>
			<span class="inline">
				<c:choose>
					<c:when test="${isQuickAdPage}">
						<c:import url="/content/planning/_layers/advertising_description.jspf"/>
					</c:when>
					<c:otherwise>
						<c:import url="/content/planning/_includes/spiff.jspf"><c:param name="level" value="vehicle"/></c:import>
						<c:import url="/content/planning/_includes/auction.jspf"><c:param name="level" value="vehicle"/></c:import>
						<c:import url="/content/planning/_includes/wholesaler.jspf"><c:param name="level" value="vehicle"/></c:import>
					</c:otherwise>
				</c:choose>
			</span>
		</td>
	</tr>
	</tbody>
</table></span>

