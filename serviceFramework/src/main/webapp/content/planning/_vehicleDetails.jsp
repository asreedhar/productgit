<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<tiles:importAttribute/>

<c:set var="inventoryId" value="${param.inventoryId}"/>
<c:set var="AgeInDays" value="${param.AgeInDays}"/>
<c:set var="VehicleYear" value="${param.VehicleYear}"/>
<c:set var="GroupingDescriptionID" value="${param.GroupingDescriptionID}"/>
<c:set var="vehicleDescStr">${VehicleYear} ${Make} ${Model} ${VehicleTrim}</c:set>
<c:set var="BaseColor" value="${param.BaseColor}"/>
<c:set var="TradeOrPurchase" value="${param.TradeOrPurchase}"/>
<c:set var="MileageReceived" value="${param.MileageReceived}"/>
<c:set var="StockNumber" value="${param.StockNumber}"/>
<c:set var="ListPrice" value="${param.ListPrice}"/>
<c:set var="UnitCost" value="${param.UnitCost}"/>
<c:set var="PrimaryBookValue" value="${param.PrimaryBookValue}"/>
<c:set var="BookVsCost" value="${param.BookVsCost}"/>
<c:set var="EdmundsTMV" value="${param.EdmundsTMV}" />

<div id="vehicleDetails">
	<html:form action="/VehicleDetails" styleId="VehicleDetailsForm">
	
		<!-- FIRST ROW -->
		<div class="attributes">
			<a href="javascript:cancelPlan();" class="toggle">Cancel<span>X</span></a>
			<ul>
				<li class="age">
					Age: <strong>${AgeInDays}</strong>
				</li>
				<li class="vehDesc">
					<h3 id="vehicleDescription"><link:perfPlus link="${vehicleDescStr}" popup="true" grouping="${GroupingDescriptionID}" /></h3>
				</li>
				<li>
					Color: <strong><c:out value="${BaseColor}" default="&nbsp;" escapeXml="false" /></strong>
				</li>
				<li>
					Mileage: <strong><fmt:formatNumber value="${MileageReceived}" var="mileStr"/><c:out value="${mileStr}" default="&mdash;" escapeXml="false" /></strong>
				</li>
				<li class="tp">
					${TradeOrPurchase}
				</li>
				<li class="snum">
					<a href="javascript:pop('<c:url value="oldIMT/EStock.go"><c:param name="stockNumberOrVin" value="${StockNumber}"/><c:param name="fromIMP" value="true"/></c:url>','estock')" class="estockLink">
						${StockNumber}
					</a>
				</li>
				<li class="photos">
                    <c:choose>
                        <c:when test="${empty photoMgrUrl}">
                            <a href="#" class="photosLink" title="please reload the page">Photo's N/A</a>
                        </c:when>
                        <c:otherwise>
                            <a href="javascript:pop('${photoMgrUrl}','photos')" class="photosLink">${hasPhotos ? 'View/Edit':'Upload'} Photos</a>
                        </c:otherwise>
                    </c:choose>    
				</li>
			</ul>
			<%--// displayed when ESTOCK is changed during planning --%>
			<ins class="updateMsg" id="estockMsg" style="display:none;">
				<span><fmt:message key="plan.msg.unadjusted" /></span>
			</ins>
	<c:if test="${not displayTMV}">
<%--// displayed when ListPrice is updated by REPRICING --%>
<div class="updateMsg" id="adPriceMsg" style="display: none;">
	<em><fmt:message key="plan.msg.checkAdPrice" /></em>
</div>	
	
	</c:if>
		</div>
		<!-- /FIRST ROW -->

	<!-- SECOND ROW on LEFT -->
	<div class="riskFactors">
		<h5 class="<util:decodeLight id="${inventoryLight}" ignoreGreen="true"/>Med"><fmt:message key="plan.riskFactors" var="sRiskText"/>${fn:toUpperCase(sRiskText)}:</h5>
		<p id="insights"></p>
	</div>
	<!-- /SECOND ROW on LEFT -->
	
	<!-- SECOND ROW on RIGHT -->	
	<div class="prices">
		<ul>
			<li class="f">
				<fmt:message key="l.lp" />:
				<span id="listPriceDisplay">
					<fmt:formatNumber type="currency" maxFractionDigits="0" 
						value="${ListPrice}" var="lp" />
					<strong><c:out value="${lp}" default="&mdash;" escapeXml="false" /></strong>
				</span>
			</li>

		<c:if test="${displayTMV}">
			<li>
				Edmunds TMV:
				<strong>
			<c:choose>
				<c:when test="${empty vehicle.EdmundsTMV or vehicle.EdmundsTMV eq '0.00'}">
					$ &mdash;
				</c:when>
				<c:otherwise>
					<fmt:formatNumber type="currency" maxFractionDigits="0"
					value="${vehicle.EdmundsTMV}" />
				</c:otherwise>
			</c:choose>
				</strong>
			</li>
		</c:if>
			<li>
				<fmt:message key="l.uc" />:
				<strong><fmt:formatNumber type="currency" maxFractionDigits="0"
					value="${UnitCost}" /></strong>
			</li>
			<li>
				${primaryGuideBookAndCategoryDesc}:
				<fmt:formatNumber type="currency" maxFractionDigits="0"
					value="${PrimaryBookValue}" var="bookAvg" />
				<strong><c:out value="${bookAvg}" default="&mdash;" escapeXml="false" /></strong>
			</li>
			<li>
				<fmt:message key="l.bvc" />:
				<fmt:formatNumber type="currency" maxFractionDigits="0" 
					value="${BookVsCost}" var="bvc" />
				<strong><c:out value="${bvc}" default="&mdash;" escapeXml="false" /></strong>
			</li>
			<li class="specials">
				<input type="checkbox" name="certified" id="certifiedCheck" />
				<label for="certifiedCheck">
					<fmt:message key="l.certified" />
				</label>
				<input type="checkbox" name="specialFinance" id="specialFinanceCheck" />
				<label for="specialFinanceCheck">
					<fmt:message key="l.specFin" />
				</label>
			</li>
		</ul>
		</div>
		<!-- /SECOND ROW on RIGHT -->
	</html:form>
<c:choose>
<c:when test="${hasTransferPricing}">
	<div id="related" class="hasTransfer">
</c:when>
<c:otherwise>
	<div id="related">
</c:otherwise>
</c:choose>
<tiles:insert attribute="listPrice"/>
<tiles:insert attribute="service"/>
	</div>
</div>
