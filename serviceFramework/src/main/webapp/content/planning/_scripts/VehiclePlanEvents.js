

/*  EVENTS CLASS
/*--------------------------------------------------------------------------*/

var Events = Class.create();
Events.prototype = {
    initialize: function(){
    	this.objective = plan.objective;
    	this.defaultDescription = plan.defaultDescription;
    	this.N = {}; //New
    	this.D = []; //Delete
    	this.U = []; //Update
    	if(this.objective=='') {
    		this.objective = 'retail'; //cannot enter without an objective
    	}

    	if(this.objective!='sold'&&this.objective!='other') {
    		for (att in plan.A) {
    			this.U[att] = plan.A[att];
    		}
		}
    },
    current: function() {
		if(this.objective=='') return;
		if(this.objective=='sold'||this.objective=='other') return;
		var hasPlans;
		var src = plan.A;
		var tar = this.U;
		var strats = this.objective=='retail'?['advertise','lotPromote']:['auction','wholesaler'];
		strats.each(function(s) {
			var strat = s;
			var stratCont = $(strat+'Current');
			var events = src[s];
			events.each(function(e,index) {
				var html;
				var stratTar = tar[strat];
				html = buildCurrentHTML(e,s,0);
				if(html) {
					stratTar[index].html = html;
					hasPlans = true;
					stratCont.innerHTML += html; //inserts new html
				}
			});
		});
	    this.toggleNoCurrent();
    },
	updateCurrent: function(eventObj) {
		var tar;
		tar = $('event'+eventObj.id);

   		if(tar) {
   			Element.update(tar,eventObj.html);
   		}
   		else {
   			new Insertion.Top(eventObj.type+'Current',eventObj.html);
   		}
   	 this.toggleNoCurrent();
   },
   	deleteCurrent: function(eventId) {
		var tar;
		tar = $('event'+eventId);
		Element.remove(tar);
		//DE620 - need to check in IE6
		//$('currentEvents').style.height = '0px'; //prevents page from hopping around
   },
	toggleNoCurrent: function() {
		var hasReprice;
		hasReprice = $('repriceHTML')!=null;
		var showNoPlan;
		var activeObjective = this.objective;
		if(activeObjective=='retail') { 
			Element.show('retailCurrent');
			Element.hide('wholesaleCurrent');
		}
		if(activeObjective=='wholesale') { 
			Element.show('wholesaleCurrent');
			Element.hide('retailCurrent');
		}
		if(!hasReprice&&activeObjective=='retail'&&$('advertiseCurrent').innerHTML==''&&$('lotPromoteCurrent').innerHTML=='') {
				if($('retailOtherCheck').checked || $('spiffCheck').checked){
					showNoPlan == false;
					//the following line doesn't actually prevent the screen from hopping around 
					//and causes a bug for the last vehicle on the page in it's Plan when adding Events
					//$('currentEvents').style.height = '0px';  //prevents page from hopping around
				}else{			
					showNoPlan = true;
				}
		}
		if(!hasReprice&&activeObjective=='wholesale'&&$('auctionCurrent').innerHTML==''&&$('wholesalerCurrent').innerHTML=='') {
				if($('wholesaleOtherCheck').checked ){
					showNoPlan == false;
					
					//the following line doesn't actually prevent the screen from hopping around 
					//and causes a bug for the last vehicle on the page in it's Plan when adding Events
					//$('currentEvents').style.height = '0px'; //prevents page from hopping around
				}else{			
					showNoPlan = true;
				}	
		}	
		if( $('internetAuctions')){
			showNoPlan = false;
		}
		if ( !!$('noPlans') ) {
			if(!showNoPlan) {
					Element.hide('noPlans');
			}
			else {
					Element.show('noPlans');
			}
		}	
   },    
	populate: function(eventType,editObj) { // POPULATES FIELDS IN <div id="float"> WHEN EDITING AN EVENT

		if(!editObj || (this.objective=='sold'||this.objective=='other')) {
			return;
		}
		var invId = vehicle.id;
		
		//switch to save for edited events
	
		$('addOrSave').innerHTML = '<a href="javascript:saveEvent(\''+editObj.type+'\','+editObj.id+');" tabindex="50">Save</a>';
	
		//$('addMore').innerHTML = '<a href="javascript:addEventAndContinue(\''+editObj.type+'\','+editObj.id+');" tabinx="49">Add More</a>';
	
		if(this.objective=='advertise') {
			populatePrintAd(editObj,invId);
			return;
		}
		this.populateDefaults(eventType);
		$H(editObj).each(function(field){
			var objName = field.key;
			var objVal = field.value;
  	//LOT PROMOTE fields
   	if(eventType == 'lotPromote') {
		switch(objName){
			case 'strategyNotes':
				$('lotPromoteStrategyNotesText').value = objVal;
				$('lotPromoteStrategyNotesText').focus();
			break;
    	}
   	}
	//AUCTION fields
   	if(eventType == 'auction') {
 
		switch(objName){
			case 'location':
				$('auctionLocationText').value = objVal;
			break;
			case 'strategyNotes':
				$('auctionStrategyNotesText').value = objVal;
			break;
			case 'endDate':
				$('auctionDateText').value = objVal;
			break;
			case 'thirdPartyId':				
				var opts = $('auctionNameSelect').options;
		 		for(var i=0; i < opts.length; i++){
		 			if(opts[i].value == objVal){
		 				$('auctionNameSelect').selectedIndex = i;		 				
    				}
  			 	}
		 		$('auctionNameSelectLabel').className='ok';	 	
			break;
			case 'auctionName':				
			var opts = $('auctionNameSelect').options;
					var hideOther = false;
			 		for(var i=0; i < opts.length; i++){
			 			if(opts[i].text == objVal){
			 					hideOther = true;
			 			}
			 		}
			 	
			 		if(!hideOther|| editObj.thirdPartyId=='other'){			 		
			 				$('auctionNameOtherText').value = objVal;	 	
			 				$('auctionNameSelect').selectedIndex = $('auctionNameSelect').options.length -2;		
			 				$('auctionNameSelectLabel').className='ok';	
			 				$('auctionNameSelectOther').style.display = 'block';	
			 		}
			 				 	 
		break;
		default:
		break;
    	}
   	}
  	//WHOLESALER fields
   	if(eventType == 'wholesaler') {
		switch(objName){
			case 'strategyNotes':
				$('wholesalerStrategyNotesText').value = objVal;
			break;
			case 'endDate':
				$('wholesalerDateText').value = objVal;
			break;
			case 'thirdPartyId':
					var opts = $('wholesalerNameSelect').options;
			 		for(var i=0; i < opts.length; i++){
			 			if(opts[i].value == objVal){
			 				$('wholesalerNameSelect').selectedIndex = i;		 				
    					}
   					}
			$('wholesalerNameSelectLabel').className='ok';			 	 
			break;
			case 'wholesalerName':
					var opts = $('wholesalerNameSelect').options;
					var hideOther = false;
			 		for(var i=0; i < opts.length; i++){
			 			if(opts[i].text == objVal){
			 					hideOther = true;
						}
					}
			 		
			 		if(!hideOther || editObj.thirdPartyId=='other'){			 		
			 				$('wholesalerNameOtherText').value = objVal;	 	
			 				$('wholesalerNameSelect').selectedIndex = $('wholesalerNameSelect').options.length -2;		
			 				$('wholesalerNameSelectLabel').className='ok';	
			 				$('wholesalerNameSelectOther').style.display = 'block';	
				}
	
						break;
					default:
					break;
				}
			}
	
		});


    },
	populateDefaults: function(eventType) { // POPULATES FIELDS IN <div id="float"> WHEN EDITING AN EVENT
	  
		if (eventType == 'lotPromote' || eventType == 'advertise') { 
			return;
		}

    	if(eventType == 'auction') {
    		//add REQUIRED
			$('auctionNameSelectLabel').className = 'req';
    	}
    	if(eventType == 'wholesaler') {
    		//add REQUIRED
			$('wholesalerNameSelectLabel').className = 'req';
    	}

    	var fields = [];
		if(eventType=='auction'||eventType=='wholesaler'){
			fields = [eventType+'DateText'];
			fields.each(function(f){
		
				switch(f) {
					case eventType+'DateText':
						$(f).value = '';
						break;
					default:
						break;
				}				
			});
		}	

    },
   	gather: function(eventType) { //** COLLECTS FORM FIELD VALUES FROM <div id="float">
 		var cont = $('planFloat');
 		var hold = {};
		hold.plannedDate = $F('dTodayHide');

if(eventType == 'advertise') {
	return gatherPrintAd(hold);
}
		

			//handles INPUT elements
	 		var inputs = cont.getElementsByTagName('input');
	 		$A(inputs).each(function(field){
				var inputId = field.id;	var val = field.value;
				if(val) { // ONLY ADD DEFINED VALUES
					switch(inputId) {	//i.e. 'thirdParty'
					//AUCTION fields
						case 'auctionLocationText': 
								hold.location = val.stripTags();
						break;
						case 'auctionDateText': 
								hold.endDate = val.stripTags();
						break;	
					//WHOLESALER fields
						case 'wholesalerDateText': 
								hold.endDate = val.stripTags();
						break;
						default:
						break;	
					}
				}
			});
			//handles SELECT elements
			var selects = cont.getElementsByTagName('select');
			$A(selects).each( function( s ) {
				var holdField;
				var opt = s.options[s.selectedIndex];
				switch ( s.id ) {
					case 'thirdPartySelect': 
						holdField = 'thirdParty';
						break;
					case eventType+'NameSelect': 
					    holdField = eventType+'Name';
					break;
					default:
					return;
				}
				if ( opt.value == 'other' ) {				
					hold[holdField] = $( s.name + 'OtherText' ).value;
					hold['nameOther'] = $( s.name + 'OtherText' ).value;  //server expects nameOther, client expects the field name (name)
				} else {
					hold[holdField] = opt.text;
					hold.thirdPartyId = opt.value;
				}
			});
			//handles all NOTES fields et al
			var notes;
			notes = $F(eventType+'StrategyNotesText').stripTags();  
			if(notes) { // ONLY ADD DEFINED VALUE
				hold.strategyNotes = notes; //i.e. hold.strategyNotes = 'Great car!'
			}
		return hold; 
    }, 
	compare: function() { 
		if(this.objective=='retail'||this.objective=='wholesale') {
			if(plan.defaultDescription!=events.defaultDescription) S.defaultDescription = events.defaultDescription;
				var strats = this.objective=='retail'?['advertise','lotPromote']:['auction','wholesaler'];
				var src = this.U;
			strats.each(function(s) {
				var compWith = plan.A;
				src[s].each(function(event,index) {
					var eventId = event.id;
					var comp = compWith[s].detect(function(e) {
	  					return e.id == eventId;
	  				});		
	  				$H(event).each(function(e) {
	  					var hasCompVal;
	  					hasCompVal = comp[e.key];
	  					if(hasCompVal&&(event[e.key]==comp[e.key])) delete event[e.key];
	  				});
	  				var hasUpdates;
	  				hasUpdates = event.toJSONString()!='{}';
					if(hasUpdates) event.id = eventId;
					else src[s].splice(index,1);
				});
			});
		}
    },
	remove: function(eventId) {
		var i = this.getIndex(parseInt(eventId));
		this.U.splice(i,1);
		if(eventId > 0) {
			this.D.push(eventId);
		}
    },
 	getIndex: function(eventId) {
		var i;
	 	this.U.each(function(event,index) {
  			if(event.id == eventId) i = index;
  		});
 		return i;
    },     
 	getItem: function(eventId) {
		var tar = this.U;
		var item = tar.detect(function(event) {
  			return event.id == eventId;
  		});
 		return item;
    }, 
	cleanNewAndUpdated: function() {
		if(this.objective=='sold'||this.objective=='other') return;
		var strats = this.objective=='retail'?['advertise','lotPromote']:['auction','wholesaler'];
		var srcNew = this.N;
		strats.each(function(s) {
			var strat = s;
			var events = srcNew[s];
			events.each(function(e,index) {
				delete e.id;
				delete e.type;
				delete e.html;
			});
		});
		var srcUp = this.U;
		strats.each(function(s) {
			var strat = s;
			var events = srcUp[s];
			events.each(function(e,index) {
				delete e.type;
				delete e.html;
			});
		});
    }, 
	processNewItem: function(eventObj) { // ADDS UNIQUE ID
		var newEvent = eventObj;
		var stamp = parseInt((new Date()).getTime());
		newEvent.id = -stamp; // NEGATED TO INDICATE IT IS NEW
		return newEvent;
    },
    degroup: function() { //removes strategy groupings so it is easier to search by event id
		if(this.objective=='sold'||this.objective=='other') return;
    	var src = this.U;
		var hold = [];	
		var strats = ['advertise','lotPromote','auction','wholesaler'];		
		strats.each(function(s) {
			var events = src[s];
			if (events) {
				events.each(function(e,index) {
					//var id = e.id;
					var item = e;
					item.type = s; //adds type to obj so it easier to degroup later
					hold.push(item);
				});
			}
		});
		this.U = hold;
	},
	validate: function(eventType) {
    	if(eventType == 'lotPromote') {
    		return true;
    	}
     	var errors = [];
	 	if(eventType == 'advertise') {
 			errors = validatePrintAd();
 		}
	 	else {
		   	var required;		   	
	    	required = [eventType+'NameSelect'];	    	
	    	//REQUIRED fields check
			required.each(function(name) {
				var labelId; labelId = name+'Label';
				if(eventType == 'advertise') {
					labelId = name+'SelectLabel';
				}
	  	    	if($(labelId) && Element.hasClassName(labelId,'req')) { 
	  	    		errorStr = $(name+'Label').innerHTML;
	  	    		sub = '<strong>'+errorStr.slice(0,errorStr.length-1)+'</strong>';
	  	    		errors.push(sub);
	  	    	}
	    	});
	 	}
	   	var msg='';    	
	   	$(eventType+'Errors').innerHTML = ''; //reset error message field
	   	if(errors!='') {
	   	    msg = errors.length==1?'is a':'are';    	   
	   	    msg += ' required field'
	   	    msg += errors.length==1?'.':'s.';
	    	Element.show(eventType+'Errors');
	   		Element.update(eventType+'Errors','\* '+errors.join(' and ')+' '+msg);
	   		msg = '<br/>';
    		return false;
	   	}
    	return true;
    },
	regroup: function() {  //sorts events by strategy groupings
		if(this.objective=='sold'||this.objective=='other') return;
    	var src = this.U;
		var holdU = { };
		var holdN = { };
		if(this.objective=='retail') {			
			var adsUp = this.U.findAll(function(event) {
				return event.type == 'advertise'&&event.id > 0;
			});
			holdU.advertise = adsUp;			
			var adsNew = this.U.findAll(function(event) {
				return event.type == 'advertise'&&event.id < 0;
			});
			holdN.advertise = adsNew;

			var lpsUp = this.U.findAll(function(event) {
				return event.type == 'lotPromote'&&event.id > 0;
			});
			holdU.lotPromote = lpsUp;
			var lpsNew = this.U.findAll(function(event) {
				return event.type == 'lotPromote'&&event.id < 0;
			});
			holdN.lotPromote = lpsNew;	
		}
		if(this.objective=='wholesale') {
			var aucUp = this.U.findAll(function(event) {
				return event.type == 'auction'&&event.id > 0;
			});
			holdU.auction = aucUp;			
			var aucNew = this.U.findAll(function(event) {
				return event.type == 'auction'&&event.id < 0;
			});
			holdN.auction = aucNew;		
			var whoUp = this.U.findAll(function(event) {
				return event.type == 'wholesaler'&&event.id > 0;;
			});
			holdU.wholesaler = whoUp;
			var whoNew = this.U.findAll(function(event) {
				return event.type == 'wholesaler'&&event.id < 0;;
			});
			holdN.wholesaler = whoNew;		
		}
		this.U = holdU;
		this.N = holdN;
	}
}


