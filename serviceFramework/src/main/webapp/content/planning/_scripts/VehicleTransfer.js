
var Transfer = Class.create();
Transfer.prototype = {
	initialize: function(iter) {
		this.iter = parseInt(iter) || '';
		this.parentEl = 'transferPrice' + this.iter;
		this.displayEl = 'transferPriceDisplay' + this.iter;
		this.editEl = 'transferPriceEdit' + this.iter;
		this.inputEl = 'transferInput' + this.iter;
		this.formObj = null;
		this.confirmed = false;
		this.postReturnValue = false;
	},
	passedThresholdCheck: function() {
		return postReturnValue;
	},
	switchToEdit: function(display,edit) {
		Element.addClassName(this.parentEl,'OPEN');
		Element.removeClassName(this.parentEl,'CLOSED');
		this.focusInput();
	},
	switchToDisplay: function(display,edit) {
		Element.removeClassName(this.parentEl,'OPEN');
		Element.addClassName(this.parentEl,'CLOSED');
	},
	focusInput: function() {
		$(this.inputEl).focus();
	},
	reset: function() {
		if(this.formObj == null) {
			$(this.inputEl).value = $(this.inputEl).defaultValue;
		} else {
			$(this.formObj).reset();
		}
		this.confirmed = false;
	},
	validate: function(confirmed) {
		//alert('in validate function');
		this._setInputValue();
		// chars;
		if(this.inputValue == '$' || this.inputValue < 0 || (isNaN(this.inputValue) && this.inputValue.length > 0)) {
			var msg = '\"'+this.inputValue+'\" is not a valid transfer price value.';
			msg += '\nPlease try again.'
			alert(msg);
			this.reset();
			return false;
		}
	 	return true;
	},
	post: function(url) {
		this._setInputValue();
		$(this.inputEl).value = this.inputValue;
		
		var _action = url != null ? url : $(this.formObj).action;
		var _params = url != null ? Form.serialize(this.parentEl) : Form.serialize(this.formObj);
			_params += '&iter=' + this.iter;
			_params += '&confirmed=' + this.confirmed;
			_params += '&update=true';
		var _this = this;
		var _f = function (xhr) {
			this.postReturnValue = _this._handleServerResponse.call(_this, xhr);
		}
		var xhr = new Ajax.Request(
			_action, { 
				method: 'get',
				parameters: _params,
				onSuccess: _f,
				asynchronous: false
		});
	},
	moveFocus: function(visible) { 
		this._visible = visible || null;
		// tests whether there is next input and if it is visible - then moves focus if both are true
		var _nextCount = this.iter + 1;
		var _nextInput = 'transferInput' + _nextCount;
		var _nextIsVisible = false;
		var _isNoNext = false;
		var _continue = true;

		while(_continue == true) {
			_isNoNext = this._isLastInput(_nextInput);
			if(_isNoNext == true) {
				_continue = false; 
			}
			else {
				_nextIsVisible = this._isNextVisible(_nextCount);
				_continue = _nextIsVisible == false;
			}
			if(_continue == true) {
				_nextCount++;
				_nextInput = 'transferInput' + _nextCount;
			}
		}
		if(_isNoNext) {

			return;
		}
		if(_nextIsVisible){
			
			$(_nextInput).focus();
		}
	},
	_handleServerResponse: function(xhr) {
		var warning = xhr.getResponseHeader('warning');
		if(warning == "true") {
			var message = xhr.getResponseHeader('message');
			Element.addClassName(this.inputEl,'red');
			setTransferMessage(message);
			setConfirmHiddenFields('transfer', this.iter,true);
			var inputPos = Position.cumulativeOffset($(this.inputEl));
			var pop = $('confirmBox');
			Element.show(pop);
			
			pop.style.left = (inputPos[0] - 25)+'px';
			pop.style.top = inputPos[1]+'px';
			return false;
		}
		return true;
		
	},
	_validateThreshold: function(confirmed) {
		if(confirmed) {
			return true;
		}
		var priceThreshold = parseInt($F('repriceThreshold'));	
		var input = this.inputValue;

		var orig = parseInt($(this.inputEl).defaultValue);

		// *** post back here to get validation results from server *** code before release *** - tb 9/4/8
		return true;
	},	
	_isLastInput: function(nextInput) {
		return $(nextInput) == null;
	},
	_isNextVisible: function(nextCount) {	
		try{
			$('transferPriceEdit' + nextCount).focus(); 
			return true;  //if focus fails, we never get here
		}catch(e) {
			return false;
		}
	},
	_getFormElement: function() {
		var el = $(this.parentEl);
		var childEls = el.getElementsByTagName('form');
		return childEls[0];	
	},
	_setInputValue: function() {
		this.inputValue = formatPrice($F(this.inputEl));
	}
}

