
var success = 'success';


// ARRAY of all "PLAN" buttons | className == 'planLink'
var detailedPlanLinks;		
function showDetailedPlanButtons() {
	Util.toggleAll(_getDetailedPlanLinks(),'show');
}
function hideDetailedPlanButtons() {
	Util.toggleAll(_getDetailedPlanLinks(),'hide');
}
function _getDetailedPlanLinks() {
	if(!detailedPlanLinks) {	
		var els = document.links;
		var r = [];
		for(var i = 0; i < els.length; i++) {
			var _el = els[i];
			if(_el.className == 'planLink') {
				r.push(_el);
			}
		}
		detailedPlanLinks = r;
	}
	return detailedPlanLinks;
}



// ARRAY of vehicle input containers | className == 'quick___ItemInput'
var quickSpiffItemInputs, quickAdvertiseItemInputs;
var quickAuctionItemInputs, quickWholesalerItemInput;

var QuickInputs = Class.create();
QuickInputs.prototype = {
    initialize: function(){
		this.GROUPS = [ 'spiff', 'auction', 'wholesaler', 'reprice', 'tranfer' ];
		this.active = '';
		this.spiff = [];
			this._spiffClass = 'quickSpiffItemInput';
		this.auction = [];
			this._auctionClass = 'quickAuctionItemInput';
		this.wholesaler = [];
			this._wholesalerClass = 'quickWholesalerItemInput';
		this.reprice = [];
			this._repriceClass = 'quickRepriceItemInput';
		this.transfer = [];
			this._transferClass = 'quickTransferRepriceItemInput';
		this._setAll();	
    },
	//PUBLIC methods
	setActive: function(which) {
		this.active = which || '';
	},
	getActive: function(which) {
		return this.active;	
	},
	get: function(which) {
		return this[which];	
	},
	show: function(which) {
		var _tars = this[which];
		if(which == 'reprice') { 
			Util.setAllClassNames(_tars,'OPEN');
			_tars = [];
		} else if(which == this.active) {
			return;	
		} else {
			//_tars = this[which];
		}
		
		if(this.active == 'reprice') {
			this.hideAllReprice();
		} else if(this.active != '') {
			 var act = this.active;
			 var _offs = this[act];
			_tars = _tars.concat(_offs);
		}
		this.setActive(which);
//alert(_tars.length);
		Util.toggleAll(_tars);
	},
	hideActive: function() {
		var act = this.active;
		if(act == '') { 
			return;
		}
		var els = this[act];
		this._hide(act);
		this.setActive();	
	},
	
	
	showReprice: function(el) {
		var _el = el.getElementsByTagName('fieldset')[0];
		if(!Element.hasClassName(_el,'OPEN')) {
			Element.addClassName(_el,'OPEN');
		}
		return false;
	},
	hideReprice: function(iter) {
		var _el = this._getItem('reprice',iter-1);
		Element.removeClassName(_el,'OPEN');
		return false;
	},
	hideAllReprice: function() {
		this._hide('reprice');
		if(this.active == 'reprice') {
			this.setActive();
		}
		return false;
	},
	
	
	//PRIVATE methods
	_hide: function(which) {
		var _tars = this.get(which);
		if(which == 'reprice') {		
			Util.removeAllClassNames(_tars,'OPEN');
		} else {
			Util.toggleAll(_tars,'hide');
		}
	},
	_getItem: function(which,index) {
		var _tars = this.get(which);
		return _tars[index];
	},
	_setAll: function() {
		var els = $('results').getElementsByTagName('FIELDSET');
		for(var i=0; i < els.length; i++) {
			var _el = els[i];
			if(Element.hasClassName(_el,this._spiffClass)) this.spiff.push(_el);
			if(Element.hasClassName(_el,this._auctionClass)) this.auction.push(_el);
			if(Element.hasClassName(_el,this._wholesalerClass))	this.wholesaler.push(_el);
			if(Element.hasClassName(_el,this._repriceClass)) this.reprice.push(_el);
			if(Element.hasClassName(_el,this._transferClass)) this.transfer.push(_el);
		}
	}


}

var QuickPlan = Class.create();
QuickPlan.prototype = {
	initialize: function(planTypeStr) {
		this.plan = planTypeStr;
		this.classStr = 'SHOW_QUICK_'+planTypeStr.toUpperCase();
		this.id = 'quick' + planTypeStr.capitalize();
this.display = null;
this.dirty = false;
this._dirtyCount = 0;
	},
	setDirty: function(bool,count) {
		if(!bool && this._dirtyCount == 0) {
			return;
		}
		if(count == 1) {
			var _count = !bool ? -1 : 1;
			this._dirtyCount += _count;
		} else {
			var _count = !bool ? 0 : count;
			this._dirtyCount = _count;
		}
		this.dirty = this._dirtyCount > 0;//bool;
	},
	check: function(target) {
		var _tars = [];
		_tars.push(target);
		var _src = this._hashForm(this.id);
		this.apply(_src,_tars);
		this.setDirty(true,1);
	},
	getAdPublicationIds: function() {
		var pubs = $F('pubIds');
		pubs = pubs.split(',');
		return pubs;
	},
	uncheck: function(target) {
		var _tars = [];
		_tars.push(target);
		var _src = this._hashForm(this.id);
		this.reset(_tars);
		this.setDirty(false,1);
	},
	checkAll: function() {
	
		//this._setVisible();
		var _targets = this._getVisible();
		var _incr = _targets.length;
		if(this.plan == 'advertise') {
			this._checkAd(_targets,true);
			_incr = this.getAdPublicationIds().length * _incr;
		} else {
			var _src = this._hashForm(this.id);
			this.apply(_src,_targets);
			//_incr = _targets.length;
		}
		this.setDirty(true,_incr); //multiple by pubs num for ads


	},
	_checkAd: function(targets,checked) {
		var _on = checked;
		var _targets = $A(targets);
//$('debug').value = _targets;
		_targets.each(function(tar) {
			var _els = Form.getElements(tar);
				$A(_els).each(function(el) {
					if(el.type == 'checkbox' && el.checked != _on) {
						el.checked = _on;
						if(_on) {
							upAdCount(el.value);	
						}
					}
				});
			});
	},	
	uncheckAll: function() {
			
		this.setDirty(false);
		var _targets = this._getVisible();
		var _decr = _targets.length;
		if(this.plan == 'advertise') {
			this._checkAd(_targets,false);
		} else {
			this.reset(_targets);
		}

	},
	cancel: function() {
// !!! no need to do this if not dirty
		if(this.plan == 'reprice' || !this.dirty) return;
		this.setDirty(false);
		if(this.plan == 'advertise') {
			var _f = $(page.form);
			_f.reset();
		} else {
			$(this.id).reset();
			var _targets = this._getTargetIds();
			this.reset(_targets);
		}
	},
	reset: function(targets) {
		var _tars = targets;
		var display = this.display;
		var _planStr = this.plan.toUpperCase();
			_tars.each(function(tar) {
				var _f = $(tar);
					_f.reset();
				// set SPIFF display
				if(_planStr == 'SPIFF') {
						if(_f.getElementsByTagName('INS').length > 0) {
								Element.addClassName(_f,'displayMode');
								Element.removeClassName(_f,'editMode');	
						}
						if(_f.spiffNotes.value != _planStr) {
							Element.removeClassName(_f.spiffNotes,'empty');
						} else {
							Element.addClassName(_f.spiffNotes,'empty');
						}
				}
				// set WHOLESALER & AUCTION display
				if(display != null) {
					var _el = _f[display].id + 'Display';
					var _hidValue = _f[display].value;
					if(_hidValue == _planStr) {
						Element.addClassName(_el,'empty');
					}
					$(_el).innerHTML = _hidValue;
				}
			});
	},
	apply: function(src,targets) {
		var _tars = targets;
		var display = this.display;
		var _planStr = this.plan.toUpperCase();
		_tars.each(function(tar) {
			var _switch = Element.hasClassName(tar,'displayMode');
			if(_switch) {
				Element.removeClassName(tar,'displayMode');
				Element.addClassName(tar,'editMode');
			}

			var _els = Form.getElements(tar);
			$A(_els).each(function(el) {
				if(el.type == 'checkbox') {
					el.checked = true;
				}
				var _val;
				_val = src[el.name];
				if(_val) {
					el.value = _val;
				}
if(el.type == 'text') {
	if(!_val && el.value == _planStr) {
		el.value = '';
	}				
	Element.removeClassName(el,'empty');
}
				//updates corresponding display text based on designated display element
				if(display !== null && el.name == display) {
					var _ele = el.id + 'Display';					
					var _displayVal = $(_ele).innerHTML;
					if(_displayVal.strip() == _planStr) {
						Element.removeClassName(_ele,'empty');
					}
					$(_ele).innerHTML = _val;
				}
			});
			
		});
	},
	getSubmitValues: function() {

var _check = this.plan;
		var _els = this._getTargetIds();
		var _hold = [];
		_els.each(function(el) {
			var _f = $(el);
			if(_f[_check].checked) {
				var inputs = Form.serialize(el);
				var obj = inputs.parseQuery();
				_hold.push(obj);
			}
		});
		return _hold;
	},
	setDisplayElement: function(str) {
		this.display = str;
	},
	updateDisplay: function(hiddenId,val) {
		var el = hiddenId + 'Display';
		$(el).innerHTML = val;

	},
	_getVisible: function(byId,byIter) {
		var _vis;
		try {
			_vis = $A(vehicleListModel._visibleSet); //
		}
		catch(ex){
			vehicleListModel.findTotalVisible();
			_vis = $A(vehicleListModel._visibleSet);
		}
		finally {
		
		}
	
		
		var _len = _vis.length;
		var _hold = [];
		for(var i = 0; i < _len; ++i){	
			if(byId) {
				_hold.push(_vis[i].id);
			}else{
				var _val = parseInt(_vis[i].iterator);
				if(byIter) {	
					_hold.push(_val);
				} else {
					_hold.push(this.id + (_val));				
				}
			}
		}
		if(_hold == '') {
			return this._getTargetIds();
		}
		return _hold;
	},
	getVisible: function() {
		this._getVisible(false,true);
	},
	getVisibleInventoryIds: function() {
		this._getVisible(true);
	},
	_getVehiclesCount: function() {
		if($('VEHICLES_COUNT') != null) {
			return $('VEHICLES_COUNT').value;
		} else {
			alert('VEHICLES_COUNT is not set');
			return 0;
		}
	},
	_hashForm: function(el) {
		var els = Form.serialize(el);
		var obj = els.parseQuery();	
		return $H(obj);
	},
	_getTargetIds: function() {
		var len = this._getVehiclesCount();
		var str = this.id;
		var _hold = $R(1,len).collect(function(n) {
			return str + n;
		});
		return _hold;
	
	}
}
