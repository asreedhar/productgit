// 
// ***** action plan functions ******
//
var Print = Class.create();
Print.prototype = {
    initialize: function(){ 
    },
  	preview: function(which, thirdParty, date){
		var url = document.forms[0].action;
		var pars = Form.serialize( document.forms[0] ) + '&reportType=' + which + '&thirdPartyId=' + escape(thirdParty) + '&endDate=' + date ;
		pop(url+'?'+pars,'print')
    },
  	run: function(){
		var url = document.forms[0].action;
		var pars = Form.serialize(document.forms[0]);
		pop(url+'?'+pars,'print')
    }      
}
function verifySelected(){
	var ele = document.getElementsByTagName('input');
	var noneChecked = true;
	for( var i = 0; i < ele.length; i++ ){
		if(ele[i].type == "checkbox"){
			if(ele[i].checked){
				noneChecked = false;
			}	
		}
	}
	if(noneChecked){
		alert("Please select one or more reports to view.");
		return false;
	}
	return true;
}
var report = new Print();
function printReport(which,mode, thirdParty, runDate) {
	if(mode=='preview') {
		var target = $('preview');
		if(limitRange()){	
			report.preview(which, thirdParty, runDate);
		}
	}
	if(mode=='print') {
		if(verifySelected() && limitRange()){
			report.run();
		}
	}
}
function limitRange(){
	if($('timePeriodDays').checked){
		if(isNaN($F('daysBack'))){
			alert("Please enter only numeric values in the days field.");
			$('daysBack').select();
			return false;
		}
		if(parseInt($F('daysBack')) < 1 || parseInt($F('daysBack')) > 30 || $F('daysBack') == ''){
			alert("Report range must be from 1 to 30 days");
			if(parseInt($F('daysBack')) < 1){
				$('daysBack').value = '1';
			}else{
				$('daysBack').value = '30';			
			}
			$('daysBack').select();
			return false;
		}	
	}
	return true;
}
function checkChars(){
	 var key = event.keyCode;
	 if((key < 48 || key > 57) && (key != 46 && key!=8)){ //only nueric values allowed
	 	return false;
	 }
	return true;
}