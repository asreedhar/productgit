/*  GLOBALS
--------------------------------------------------------------------------*/

var page = new Page();
var isDirty = false;

var vehicle = null;
var plan;
var events = {};
var advertisers = [];
var wholesalers = [];
var auctions = [];
var curThirdParty = [];
var curEventType = null;

var S = {};
var stratStr = { advertise: 'Advertising', lotPromote: 'Lot Promote', auction: 'Auction', wholesaler: 'Wholesaler' };

var printAdManagerWindow;
var printAdManager = null;// = new PrintAdManager();
var managerOptions;// = new ManagerOptions();
var quickSpiff = null;
var activeQuickPlan = null;
var adPriceReprice;// = new AdPriceReprice();


var mX = -500;
var mY = -500;
var popupY = 0;

//has been changed on estock card flag
var bPageReload = false;


function _handleTabs(e) {
	var TAG = 'A';
	var el = Event.element(e);
	if(el.tagName != TAG) {
		el = Event.findElement(e,TAG);
	}
		var isAction = Element.hasClassName(el,'actionPlanLink');
		if(!isAction) {
		if(Element.hasClassName(el.parent,'active')) return false;
		if(Element.hasClassName(el.children[0],'none')) return false;
		} 
	var _continue = confirmDirty();
	if(_continue) {
		var _enab = getEnabledParam();
		if(_enab != '') {
			el.href += '&enabled=' + _enab;
	}
	}else {
		return false;
	}
	return true;
}
function _handleMenu(e) {
	var el = Event.element(e);
	var _act = el.tagName;
	if('A' != _act) return true;
	return confirmDirty();
}


function _handleFilters(e) {
	if(quick == null) {
		return true;
	}
	if(quick.dirty == false) return confirmDirty();

	var msg = 'You have unsaved ' + quick.plan.toUpperCase()+' updates.\r';
		msg += 'Please \'SAVE CHECKED\' or \'CANCEL\' before continuing.';

	var el = Event.element(e);
	var _act = el.tagName;
	if('A' == _act || 'LI' == _act || 'INPUT' == _act) {
		alert(msg);
		return false;
	} else {
		return true;	
	}
}

var summs;
function togSummary(e,tar) {
	if(quick != null && quick.plan == 'advertise') return false;
	var el = $(tar);
	var openIt = el.style.display == 'none';
		openIt ? el.style.display = '' : el.style.display = 'none';
	//e.title = openIt ? '':'Click to open plan details.';
	return false;
}

var allSummsExpanded = false;
function openAllExpandedSummaries() {
	togExpandedSummary('open')
}

function closeAllExpandedSummaries() {
	togExpandedSummary('close')
}

function togExpandedSummary(doWhat) {
	var openIt;
	if(doWhat != 'open' && doWhat != 'close') {
		openIt = allSummsExpanded == false;
	} else {
		openIt = doWhat == 'open';
	}
	if(openIt == allSummsExpanded) {
		return;
	}
	allSummsExpanded = openIt;
	var src = $('expandDetailsLink');
	src.innerHTML = (openIt ? 'Collapse ' : 'Expand ') + 'all plan details';
	src.className = (openIt ? 'OPEN' : 'CLOSED');	
	if(!summs) {
		summs = document.getElementById('items').getElementsByTagName('TFOOT');
	}
	for(var i = 0; i < summs.length; i++) {
		var el = $(summs[i]);
		openIt ? el.style.display = '' : el.style.display = 'none';
		/*
		var trig = el.id + 'Tog';
		if($(trig) != null) {
			$(trig).title = openIt ? '':'Click to open plan details.';
		}
		*/
	}
	return false;
}

function unregister() {
	return;
}

function register() {
	var enab = (window.location.search.parseQuery()).enabled;
	if(!enab && window.location.pathname.search('QuickAd') != -1) {
		enab = 'advertise';
	}
	initQuickPlan(enab);

	if($('menu') != null) {
	Event.observe('menu', 'click', _handleMenu);
    }
	if($('tabs') != null) {
	Event.observe('tabs', 'click', _handleTabs);
    }
	if($('advancedFilters') != null) {
		Event.observe('advancedFilters', 'click', _handleFilters);
	}
	if($('planningFilters') != null) {
		Event.observe('planningFilters', 'click', _handleFilters);
	}
	if($('expandDetailsLink') != null) {
		Event.observe('expandDetailsLink', 'click', togExpandedSummary);
	}
    //FIND BY STOCK NUMBER
    setSearchObservers();


}	

function confirmDirty() {
	var msg = '';
	if(quick != null && quick.dirty == true) {
		msg += 'You have unsaved ' + quick.plan.toUpperCase()+' updates.\r';
	}
	if($(page.planLayer) != null) {
		msg += 'Leaving this page will cause any unsaved plan for '+vehicle.description+' to be discarded.\r';
	}
	if(msg == '') {
		return true;
	} else {
		var huh = confirm(msg + 'Are you sure you want to continue?');
		return huh;
	}
}
function setFirstFocus(){
	return Try.these(
		function() { $('repriceInput1').focus() },
		function() { $('repriceInput2').focus() },
		function() { $('repriceInput3').focus() },
		function() { $('repriceInput4').focus() },
		function() { $('repriceInput5').focus() },
		function() { $('repriceInput6').focus() },
		function() { $('repriceInput7').focus() },
		function() { $('repriceInput8').focus() }
	) || false;
}

var vehicleListModel;

/* AFTER page LOAD */
Event.observe(window, 'load', function() {
	
	setFirstFocus();
	vehicleListModel = new VehicleListModel();
	vehicleListModel.loadVehicles();

	var _filterClass = (window.location.search.parseQuery()).filterClass;
	if(_filterClass) {
		$(page.content).className = _filterClass;	
	}
	register();
});



/////////////////////////////////////////////////////////
// FIND BY STOCK NUMBER

function setSearchObservers() {
	if(!$('searchStockForm')) return;
	Event.observe('searchStockForm', 'submit', findStockNumber);
	
	var togFocus = function(e) {
		var el = Event.element(e);
		Element.toggleClassName(el,'focus');
	}

	Event.observe('searchStockInput', 'focus', togFocus);
	Event.observe('searchStockInput', 'blur', togFocus);
}


var findStockNumber = function(e) {
	var f = Event.element(e);
	var url = f.action;
	var num = f.stockNumber.value.strip().toUpperCase();
	var val = 'stock'+num;

	var anchors = document.getElementsByTagName('A');
	var anchor = $A(anchors).find(function(el) {
		return el.name == val
	});
	url += '?' + Form.serialize(f);
	if(!anchor) { 
		new Ajax.Request( 
			url, { 
				method:'get',
				parameter: '',
				onComplete: function(x) {
					var rangeId = x.getResponseHeader('rangeId');					
					if(parseInt(rangeId) >= 0){			
						var _conf = confirmDirty();
						if(_conf) {
							document.location.href = document.location.pathname+'?rangeId='+rangeId+'#'+val;
						}
					}else{
						Element.update('searchStockError','No vehicles found');
						f.stockNumber.select()
					}	
				}
			}		
		);	
	} else {
	  //found on this tab
	  	try {
			anchor.focus();	   //calls highlight vehicle
		} catch(ex) {  //element is hidden, show then focus
			disableAdvancedFilters('advancedFilterGroup');
			anchor.focus();
		} finally { 
		}
		Element.scrollTo(anchor);	
		f.reset();		
	}
	return false;
}



function showTooltip(el,displayEl,fieldName) {
	if(Element.visible(displayEl)) {
		switch(fieldName) {
			case 'transferPrice':
				el.title = 'Double click here to update the transfer price';
				break;
			case 'listPrice':
				el.title = 'Double click here to reprice this vehicle';
				break;
			default:
				break;
		}
	} else {
		el.title = '';
	}
}





 

/*  Scroll to active plan function
--------------------------------------------------------------------------*/
function scrollWindowTo(id){
	var coords = findPosition($(id));  //find coordinates of the plan button clicked
	var winHeight =  document.documentElement.clientHeight;  //get height of browser window
	var planHeight = 350;  //default plan height to 350px
	var yLoc = coords[1] - ((winHeight/2) -  (document.getElementById('vehiclePlan').clientHeight/2));  //y location of plan = plan button position - .5 window height - .5 plan height
	
	window.scrollTo(0,yLoc);   //scroll window to this location

}

/**
*Validates a date entered into a text box
*Accepts calendarElement (textbox), currentDate (string mm/dd/yyyy),defaultValue (string)
*allowPast (boolean)
*/
function validateDate(calendarElement, currentDate, defaultValue, allowPast){
	var validator = /^\d{2}(\/)\d{2}\1\d{4}$/;  //regexpression date validator
	//if nothing entered don't bother to check
	if($(calendarElement).value != '' && $(calendarElement).value != null){
		if( validator.test( $(calendarElement).value ) ){
			var today = Date.parse(currentDate); //today in milliseconds
			var newDate = Date.parse( $(calendarElement).value );	//the date being entered	
			var limit = today;  //default limit to todays date (prevent dates before today from being entered)		
			$(calendarElement).value = Util.getFormattedDate('m/d/Y',$(calendarElement).value);
			//default begin and end dates are hidden fields which hold the initially populated value of the begindate and
			//enddate text fields.  
			if(calendarElement == 'beginDate'){
				if( $F('defaultBeginDate') != '' && $F('defaultBeginDate') != undefined ){
					limit = Date.parse($F('defaultBeginDate'));
					currentDate = $F('defaultBeginDate');
				}
			}else if(calendarElement == 'endDate'){
				if( $F('defaultEndDate') != '' && $F('defaultEndDate') != undefined && ($('location') && $('wholesaleType'))){	
					if(Date.parse($F('defaultEndDate')) < today){
						limit = Date.parse($F('defaultEndDate'));
						currentDate = $F('defaultEndDate');
					}	
				}else{
					limit = today;
				}
			}
			if( newDate >= limit || allowPast ){  //make sure only future dates are selected			
				 if(newDate < Date.parse('01/01/1970')){			
					alert("Date cannot be earlier than 01/01/1970");
					$(calendarElement).value = '01/01/1970';
					return false;
				}
				return true;
			}else{
				alert("Date cannot be earlier than " + currentDate);
				$(calendarElement).value = currentDate;
				return false
			}
		}
		alert("Date should use the format 'mm/dd/yyyy'");
		if(defaultValue == null){
			defaultValue = '';
		}
		$(calendarElement).value = defaultValue;
		return false;
	}
	return false;
}


function emptyAndHideFloat() {
	if($('planFloat')) { 
		Element.hide('planFloat');
		Element.update('planFloat',''); 
	}
	if($('vehicleInfo')){
		Element.remove($('vehicleInfo'));
	}
	if($('printAdvertisingManager')){
		Element.remove($('printAdvertisingManager'));
	}
	$('eventErrors').style.display='none';
}


/**
*Switches the onscreen objective
*/
function switchObjective(selected) {
	var sel = selected;
	var active = events.objective;
	if(sel == active) { 
		return; 
	}
	emptyAndHideFloat();

if($('manage')){ Element.remove('manage'); }  // remove third party manager

	events.objective = sel;	
	Element.show(sel);
	Element.hide(active);
	
	if (sel == 'sold' || sel == 'other') { 	
		if(!$('internetAdvertising')){			
			Element.hide(currentPlans);	
		}
		if(sel == 'sold'){
			$('soldAsSelect').focus();						
		}else{
			$('otherStrategyNotesText').focus();
		}
	} else {
		Element.show(currentPlans);	
	}
	events.toggleNoCurrent();
}



function locateAndSelectObjective(eventType){  //selects objective based on event type
	switch(eventType){
		case 'lotPromote':
		case 'advertise':
			$('retailApproach').click();
			break;
		case 'auction':
		case 'wholesaler':
			$('wholesaleApproach').click();
			break;
		default:
			break;
	}
}
/**
*Builds the HTML for the current plans list
*/

function buildCurrentHTML(obj,strat) {

	var id = obj.id;
	var eventTypeLink = '\''+strat+'\'';
	hdr = '<div class="hdr"><ul>';
//strategy
	hdr += '<li class="str">'+stratStr[strat]+'</li>';
//planned date
	var plannedDateVal = !obj.plannedDate?'&nbsp;':obj.plannedDate;
	hdr += '<li class="pd">'+plannedStr+'<strong>'+plannedDateVal+'</strong></li>'; 
//runUntil	
	var valEnd = '&nbsp;';
	if(strat == 'advertise'){
		valEnd = !obj.planUntilDate?valEnd:obj.planUntilDate;
	}
	else {
		valEnd = !obj.endDate?'&nbsp;':obj.endDate;
	}
	if(strat != 'lotPromote'){   
		hdr += '<li class="ru">'+dateStr+'<strong>'+valEnd+'</strong></li>'; 
	}

//edit and delete
	hdr += '<li class="upd"><a href="javascript:editEvent('+eventTypeLink+','+id+');">'+editStr+'</a>';
	hdr += '<tt>|</tt>';
	hdr += '<a href="javascript:confirmPopup(\'open\',\''+id+'\');" id="deleteLink'+id+'">'+deleteStr+'</a>';
	hdr += '<div style="display:none;" id="confirmBox'+id+'" class="confirm"><h6>Are you sure?</h6><a href="javascript:deleteEvent(\''+id+'\');"><img src="/NextGen/common/_images/buttons/confirm-16x16-ok.gif" class="ok"/></a><a href="javascript:confirmPopup(\'cancel\',\''+id+'\')"><img src="/NextGen/common/_images/buttons/confirm-16x16-cancel.gif" class="cancel"/></a></div></li>'; 
	hdr += '</ul></div>';
   	det = '<ul class="det">'; 
	var nameVal = '&nbsp;';
//outlets
	if(strat=='advertise') {
		nameVal = !obj.thirdParty?nameVal:obj.thirdParty;
	}
	if(strat=='auction'||strat=='wholesaler') {
	
	
		var nameVal = obj.name?obj.name:'';
		if(strat == 'wholesaler'){
			nameVal  = !obj.wholesalerName?nameVal:obj.wholesalerName;
		}else if(strat == 'auction'){
			nameVal = !obj.auctionName?nameVal:obj.auctionName;
		
		}	
	//alert(obj.toJSONString());
		if(strat=='auction'&&obj.location) nameVal += !obj.auctionName && !obj.name?obj.location:' ('+obj.location+')';		
	}
	det += '<li class="mo">'+nameVal+'</li>';
//notes
	var notesVal = '&nbsp;';
	
	if(strat == 'advertise'){
		notesVal = !obj.adText?notesVal:obj.adText;
		notesStr = adTextStr;
		
	}
	else {
		notesVal = !obj.strategyNotes?notesVal:obj.strategyNotes;
		notesStr = 'Notes: ';
	}
	det += '<li class="not">'+notesStr+notesVal+'</li>';
	det += '</ul>';	
	itemCont = ['<div class="event" id="event'+id+'">','','</div>'];
	itemCont[1] = hdr + det;
	eventHTML = itemCont.join('');
	return eventHTML;
}

/**Method displays a confirmation popup 
*/

function confirmPopup(doWhat,eventId){	
	if(doWhat=='clear') { timer = null; return; }
	if($('planFloat') && Element.visible('planFloat')) {
		return;
	}
	var confirmCont = $('confirmBox'+eventId);
	if(doWhat=='cancel') {
		if(confirmCont){
			Element.hide(confirmCont);
		}
		clearTimeout(timer);
		timer = null;
		return;
	}
	var deleteLink = $('deleteLink'+eventId);
	if(timer!=null){
		clearTimeout(timer);
		timer = null;
		return;
	}
	
var pos = Position.cumulativeOffset(deleteLink);
Position.clone(deleteLink, confirmCont, { setWidth: false, offsetLeft: 32, offsetTop: -5 });
Element.show(confirmCont);
	//after 10 sec, popup disappears
	timer = setTimeout("confirmPopup('cancel','"+eventId+"')", 10000);
}






//inline reprice
function toggleInput(invId,doWhat,reset) {
	var cell = $('lpCell'+invId);
	var display = $('listPrice'+invId);
	var edit = $('listPrice'+invId+'Edit');
	//remove the reprice confirm box
	if($('confirmBox')){ Element.remove($('confirmBox'));Element.removeClassName($(display.id+'Input'),'red');$(display.id+'Input').onfocus='';}

	if(doWhat == 'close' && Element.visible(edit)){
		Element.hide(edit);
		Element.show(display);
		if(reset){	
			$('listPrice'+invId+'Input').value=	formatPrice(display.innerHTML);	
		}
		//inlineRepriceOpen = false;
	}else if(!Element.visible(edit)){
		Element.show(edit);
		Element.hide(display);
		//inlineRepriceOpen = true;
	}
	cell.style.textIndent = Element.visible(edit)?'3px':'10px';
	cell.title = Element.visible(edit)?'':'Double click here to reprice this vehicle.';
}

/**
*Highlights a specific row on mouseover and removes on mouse out
*/
function highlightRow(e,eventType,vId) {
if($('plan')) return;
	if(page.planActivated==true) return;
	var state = eventType;
	var tar = $(e.id);
	if(!ignoreOne){
	removeFadingBack(vId);
	}
	state=='over' ? Element.addClassName(tar,'hi') : Element.removeClassName(tar,'hi');
	ignoreOne = false;
}

function createInputs() {
	var inputs = createInputs.arguments;
	
	for(var i = 0; i < inputs.length; i++){
		Element.toggle(inputs[i]);
	}
	Element.toggle('editDatesLink');
}

function toggleReq(e) {
	var defalt = (e.type!='select-one' && e.type!='select')?'':0;
	if(!$(e.id+'Label')) return;
	$(e.id+'Label').className=e.value==defalt?'req':'ok';
}



function updateThirdParty( s ) {
	var opt = s.options[s.selectedIndex];
	var otherText = $( s.id + 'Other' );
	switch ( opt.value ) {
		case 'other':
			otherText.style.display='block';
			$( s.id + 'Label' ).className = ( otherText.value ? 'ok' : 'req' );
			
			break;
		case '':
			$( s.id + 'Label' ).className = 'req';
			otherText.style.display='none';		
			break;
		case 'manage':
		
			if(otherText){
				otherText.className= 'hide';
			}
		
			
			s.disabled = true;
			function showWin(r,j){
			
				var p = new Popup();
				
				p.popup_show('manage','manageHeader',['manageCancelLink','manageSave'],'screen-center',100,false,0,0);
				
				managerOptions.loadOptions('manageSelect');
				managerOptions.setPopup(p);
			  
			}
			var div = '<div id="manage"  style="top:-500px; left:-500px;"></div>';
			if(!$('manage ')){
						new Insertion.After($('header'),div);
			}
			var a = new AjaxLoader('Loading...');
			a.setPath('/NextGen/ManageThirdParty.go');
			a.setElement('manage');
			a.setOnSuccess(showWin);
			a.performAction();
	
			break;
		default:
			$(s.id + 'Label').className = 'ok';
			otherText.style.display='none';						
			break;
	}
}

function updateOtherThirdParty( o, label ) {
	var textLabel = $(label);
	textLabel.className = ( o.value !='' ? 'ok' : 'req' );
}

// called by Events.populate() when editing an object
function popOpenOtherThirdParty( select, thirdPartyName ) {
	for ( var i = 0; i < select.options.length; i++ ) {
		if ( select.options[i].value == 'other' ) {
			select.selectedIndex = i;
			break;
		}
	}
	var otherText = $(select.id + 'Other');
    otherText.value = thirdPartyName;
	otherText.className = '';
	$(select.name + 'TextLabel').className = (thirdPartyName ? 'ok' : 'req');
}





function resetSummaries() {
			var foots = cont.getElementsByTagName('tfoot');
			foots = $A(foots);
			foots.each( function(foot){
	 			foot.removeAttribute('style');//)style.display = '';
				$(foot.id+'Tog').title = 'Click to open plan details'
			});	
	
}


function toggleSummary(which,event) {
	var target = $(page.el);
		if(Element.hasClassName(page.el,'SHOW_QUICK_ADVERTISE')) {
			return;
		}
	if(!which) { //all
		var cont = $('items');
		if(Element.hasClassName(cont,'SHOW_EXPANDED_SUMMARIES')) {
			Element.removeClassName(cont,'SHOW_EXPANDED_SUMMARIES');
			var foots = cont.getElementsByTagName('tfoot');
			foots = $A(foots);
			foots.each( function(foot){
	 			foot.style.display = '';
				$(foot.id+'Tog').title = 'Click to open plan details'
			});
		} else {
			Element.addClassName(cont,'SHOW_EXPANDED_SUMMARIES');
			var foots = cont.getElementsByTagName('tfoot');
			foots = $A(foots);
			foots.each( function(foot){
	 			foot.style.display = 'table-footer-group';
				$(foot.id+'Tog').title = ''
			});	
		}
		Element.toggle('collapseAll');
		Element.toggle('expandAll');
	}else { //one
 		var el = $(which);
		if(el.style.display != 'table-footer-group') {
 			el.style.display = 'table-footer-group';
 			event.title = '';
 			return;
		}
		if(el.style.display == 'table-footer-group') {
 			el.style.display = '';
 		 	event.title = 'Click to open plan details.'	
		}
	}
}

var bIsInline = false;
function submitReprice(invId,e) {
	 bIsInline = e.type!='button';
	var val = $F('listPrice'+invId+'Input');
	var origVal = formatPrice($('listPrice'+invId).innerHTML);	
	var price = validatePriceValue(val);
	
		if(price === false) {
			$('listPrice'+invId+'Input').value = '';
			return;
		}
		confirmReprice(price, invId, $('listPrice'+invId+'Input'),function(){return savePrice(price,invId);});		
}



function savePrice(price, invId){
	toggleInput(invId,'close');
	var pars = '&inventoryId='+invId+'&ListPrice='+price;
	var listPriceUpdate = new Ajax.Request(
			'/NextGen/PostVehicleReprice.go', { method: 'post', parameters: pars,
			onSuccess: function(t) {
				var newVal = '$' + t.responseText;
				//Only do an update if something actually changed.
				Element.update('listPrice'+invId,newVal);
if(!bIsInline) {
	Element.update('lastReprice',$F('dToday')); //hidden field in page header
	Element.update('listPrice',newVal);
	$('listPrice'+invId+'Input').value = formatPrice(newVal);
	$('vehReprice').value = '$';
	page.repriced=true;	
}

			}
		}); 
	return false;
}


function validatePriceValue(val) {
	var value = formatPrice(val);
	var check;	 
	if( value=='$' || isNaN(value) || value < 0) check=true;
	if(check) {
		var msg = '\"'+value+'\" is not a valid reprice value.';
		msg += '\nPlease try again.'
		alert(msg);
		return false;
	}
	return parseInt(value);
}





/*  EVENT Open-Edit-Save-Close FUNCTIONS
--------------------------------------------------------------------------*/


// EDIT ////////////////////////////////////////////////////////
function editEvent(eventType,eventId) { 
	if(Element.visible('planFloat')) return;
	var editObj = events.getItem(eventId);
	openEvent(eventType,editObj);
}



// OPEN ////////////////////////////////////////////////////////
function openEvent(eventType,editObj) {
	if($('internetAdvertisingFloat') || Element.visible('planFloat')){return;}	
	emptyAndHideFloat();
	var invId = vehicle.id;
	var trigLink = $(eventType+'EventLink');
	var url = checkSlash(trigLink.pathname);
	var pars = 'type='+eventType;
	if(eventType == 'advertise' && editObj && parseInt(editObj.id) > 0) {
		pars += '&selectedPrintAdvertiserId='+editObj.selectedPrintAdvertiserId;
		pars += '&planId='+editObj.id;
	}
	else
	{
		pars+='&inventoryId='+invId;	
	}
	if(editObj){	//select proper objective
		locateAndSelectObjective(eventType);
	}

	if(managerOptions == null) managerOptions = new ManagerOptions();
	
		var req = new Ajax.Request(
			url, { method: 'get', parameters: pars, 
				onComplete: function(xhr){
					Element.update('planFloat',xhr.responseText);
					Element.show('planFloat');

					managerOptions.setEventType(eventType); // ******************************
					switch(eventType){
						case 'advertise':				
							populatePrintAd(editObj, invId); //plan_advertising.js
							return;
							break;
						case 'lotPromote':
							$('lotPromoteStrategyNotesText').focus();
							break;
						case 'auction':					 
							curThirdParty = auctions;				
							managerOptions.setOptions(auctions);
							managerOptions.setParent('auctionNameSelect');
							managerOptions.populateParent();
							$('auctionNameSelect').focus();
							break;
						case 'wholesaler':
							curThirdParty = wholesalers;			
							managerOptions.setOptions(wholesalers);
							managerOptions.setParent('wholesalerNameSelect');
							managerOptions.populateParent();
							$('wholesalerNameSelect').focus();
						
						break;
					}
						events.populate(eventType,editObj);

				}
		});
}


function deleteEvent(eventId) { 
	if(Element.visible('planFloat')) {
		return;
	}
	confirmPopup('clear');
	events.remove(eventId);
	events.deleteCurrent(eventId);
	events.toggleNoCurrent();
}
function saveEvent(eventType,eventId) { 
	if(updateEvents(eventType,eventId)){	
		closeEvent('close');
	}
}
function addEvent(eventType, invId) { 
	var ok = updateEvents(eventType,-1); //-1 denotes a NEWLY ADDED events
	if(ok) {
		closeEvent('close');
	}
}
function addEventAndContinue(eventType,eventId) { 
	var ok = updateEvents(eventType,eventId);
	if(ok){ //-1 denotes a NEWLY ADDED events
		openEvent(eventType,null);
	}
}

function updateEvents(eventType,eventId) { 
	var ok = events.validate(eventType);
	if(ok) {

		if(eventType == 'advertise'){
			saveDefaultDescription();  //located in plan_advertising.js
		}	
	
		var saveEvent = events.gather(eventType);
	
		saveEvent.type = eventType; // ADDING EVENT TYPE
		if(eventId==-1) { // BRAND NEW EVENTS
			var stamp = parseInt((new Date()).getTime()); // RANDOM ID
			saveEvent.id = -stamp; // NEGATED TO INDICATE IT IS NEW
			saveEvent.html = buildCurrentHTML(saveEvent,eventType);
//** WRITING NEW TO events.U
			events.U.push(saveEvent);
		}
		else { // EDITED EVENTS
			saveEvent.id = eventId; // ADDING ASSIGNED ID
			saveEvent.html = buildCurrentHTML(saveEvent,eventType);
			var i = events.getIndex(eventId);
//** OVERWRITING EXISTING IN events.U
			events.U.splice(i,1,saveEvent);
		}
		events.updateCurrent(saveEvent); // UPDATING CURRENT EVENTS HTML
		
		return true;
	}	
	return false;
	
}




// CLOSE	////////////////////////////////////////////////////////
function closeEvent() { 
	emptyAndHideFloat();
	switch(managerOptions.getEventType()){
			
			case 'auction':
				auctions = managerOptions.getOptions();
			break;
			case 'wholesaler':
				wholesalers = managerOptions.getOptions();
			break;
		}
		//close manage third parties if open.
		if($('manage')){
				Element.remove('manage');
		}
	

}






//
// ***** utility functions *************************
//

function checkSlash(path) {
	var modPath = document.all?'/'+path:path;
	return modPath;
}

function getParamValue(queryStr,which) {
	var query = removeQ(queryStr);
	var queryObj = query.parseQuery(); //prototype method
	return queryObj[which];
}

function getInventoryId(idStr,prefixStr) {
	var prefixChars = prefixStr.length;
	var invId = idStr.slice(prefixChars,idStr.length);
	return invId;
}

//sets the next plan date based upon the vehicle age box
function setCalendarDate(calendarElement, ageElement, currentDate, currentAge){
	if( parseInt($F(ageElement)) >= parseInt(currentAge) && !isNaN( $F(ageElement) ) ){
		if(!checkRange(0,999,parseInt($F(ageElement))) && parseInt( currentAge ) < 999){
			alert("Vehicle age must be less than 999 days.");
			setAge(calendarElement, ageElement, currentDate, currentAge);
			$(ageElement).select();
		}
		var DAY = 1000 * 60 * 60 * 24; //milliseconds in a day
		var milliDays = parseInt($F(ageElement)) * DAY;  //convert days to milliseconds
		var milliAge = parseInt(currentAge) * DAY;
		var today = Date.parse(currentDate);  //determine the current date in milliseconds
		var difference = parseInt(milliDays) - parseInt(milliAge);		
		var returnDate = new Date();  //new date we are creating
		var formattedDate = "";  //string output date
		var month = "";  //string month
		var day = "";    //string year
		returnDate.setTime(today + difference); //set return day
		month = returnDate.getMonth() + 1;
		if(month < 10){  //for consistant formatting
			month = "0" + month;
		}	
		day = returnDate.getDate();
		if(day < 10){
			day = "0" + day;
		}
		
		formattedDate = month + "/" + day + "/" + returnDate.getYear();  //create the date string
		$(calendarElement).value = formattedDate;	 //set the text area	
	}else{
		alert("Vehicle age must be greater than or equal to " + currentAge + " days");		
		setAge(calendarElement, ageElement, currentDate, currentAge);
		$(ageElement).select();
	}
}

//Sets the vechical replan age box based upon the next plan date value
function setAge(calendarElement, ageElement, currentDate, currentAge){
	var DAY = 1000 * 60 * 60 * 24;  //milliseconds in a day
	var validator = /^\d{2}(\/)\d{2}\1\d{4}$/;  //regexpression date validator
	if( validateDate(calendarElement, currentDate) ){
		var today = Date.parse(currentDate); //today in milliseconds
		var newDate = Date.parse( $F(calendarElement) );		
		var milliDays = currentAge * DAY;//age in milliseconds
		var daysFromToday = newDate - today;
		var milliDifference = daysFromToday + milliDays;	
		var daysDiff = Math.ceil(milliDifference / DAY);
		if(daysDiff <= 999){		
			$(ageElement).value = daysDiff;		
			setCalendarDate(calendarElement, ageElement, currentDate, currentAge); 	//in case you enter '8/45/2006', recalculate date
		}else if( parseInt(currentAge) <= 999 ){
			alert("A NEXT REPLAN DATE of "+$F(calendarElement)+" would make the vehicle age over 999 days. Please revise.");
			setCalendarDate(calendarElement, ageElement, currentDate, currentAge);  //reset the calendar date to the value called for by vehicle age
			$(calendarElement).select();
		}	
	}else{	
		setCalendarDate(calendarElement, ageElement, currentDate, currentAge);  //reset the calendar date to the value called for by vehicle age
		$(calendarElement).select();
	}
}
//sets required for enter date if exit date is set
function setRequired(id, exitId){

	if($(id+'Label')){
		if($F(exitId) != '' && $F(id) =='' ){
		
			$(id+'Label').className = 'req' 
		}else{
			$(id+'Label').className = 'ok' 
		}
		
	}
}

function removeRequired(id, exitId){

	if($(id+'Label')){
		if( $F(id) != '' ){		
		
			$(id+'Label').className = 'ok';
			
		}else if($F(exitId) != ''){
		
			$(id+'Label').className = 'req';
			
		}
		
	}
}
function runFilteredReport(e){
    e.form.submit();    
}

function formatPrice(price) {	
	var indexOfPeriod = price.indexOf(".");
	if (indexOfPeriod >= 0)
	{
		price = price.substring(0, indexOfPeriod);
	}
	var indexOfComma = price.indexOf(",");
	if (indexOfComma >= 0)
	{
		price = price.substring(0, indexOfComma) + price.substring( (indexOfComma + 1), price.length );
	}
	var indexOfDollar = price.indexOf("$");
	if (indexOfDollar >= 0)
	{
		price = price.substring( (indexOfDollar + 1), price.length );
	}
	return price;
}

//limits the length of text fields
function limitChars(obj, limit){

	if( $F(obj).length > limit && (event.keyCode !=46 && event.keyCode != 8) ){
		$(obj).value = $F(obj).substring(0,limit+1);
		return false;
	}
	return true;
}

function createInternetAuctionMessage(name, datePosted){	
		var currentPlans = $('currentEvents').innerHTML;
		var newEvent = '<div class="event" id="internetAuctions">';
				newEvent+='<div class="hdr">';
				newEvent+='<ul>';
				newEvent+='<li >';
				newEvent+='<strong>';
				newEvent+='Vehicle was posted to '+name+' on '+Util.getFormattedDate('M/d/Y',datePosted);
				newEvent+='</strong>';
				newEvent+='<input type="hidden" id="newInternetAuction" value="true" style="display:none;"/>';
				newEvent+='</li>';
				newEvent+='</ul>';
				newEvent+='</div>';
				newEvent+='</div>';				
			$('currentEvents').innerHTML =  currentPlans+newEvent;			
			events.toggleNoCurrent();
}

function createGroupClosedAuctionMessage(name, datePosted, action){
	/* Replaced 1-23-2013 - kill it if it's more than a couple weeks old.
	var currentPlans = $('currentEvents').innerHTML;
	var newEvent = '<div class="event" id="internetAuctions">';
			newEvent+='<div class="hdr">';
			newEvent+='<ul>';
			newEvent+='<li >';
			newEvent+='<strong>';
			newEvent+='Vehicle was ' + action + ' ' + name + ' on '+Util.getFormattedDate('M/d/Y',datePosted);
			newEvent+='</strong>';
			newEvent+='</li>';
			newEvent+='</ul>';
			newEvent+='</div>';
			newEvent+='</div>';				
		$('currentEvents').innerHTML =  currentPlans+newEvent;			
		events.toggleNoCurrent();
	*/
	
	var currentPlans = $('currentEvents').getElementsByTagName('ul')[0]; //fragile DOM selection - improve post-cleanup
	
	if(action){
		//removes all but the first LI so change this if we want more than one update message after current plan
		var msg = (currentPlans.innerHTML.split(/<\/li>/i).shift() + '</li>');
		msg += '<li><strong>' + 'Vehicle was ' + action + ' ' + name + '</strong></li>';
	}
	
	currentPlans.innerHTML =  msg;
	events.toggleNoCurrent();
}


// for internet marketplace stuff!!!!
function submitForm() {
	var pars='?&';
	pars += Form.serialize('InternetMarketplacesForm');
	if(pars.indexOf('inlcludeAnnouncements=')==-1){
		pars+='&inlcludeAnnouncements=false';
	}
	if(pars.indexOf('exported=')==-1){
		pars+='&exported=false';
	}

	var planRequest = new Ajax.Request(
		'/NextGen/PostInternetMarketplaces.go', { method: 'get', parameters: stampURL(pars), 
		onComplete: function(xhr){ 				
			var response = xhr.getResponseHeader('GMACurl');
			if ( response=='Error')
			{
				alert( 'An error occured on your Smart Auction request\nPlease try again.  If problem persists, please contact our help desk');
			}
			else
			{
				if (pars.indexOf('activeInternetMarketplace=2') == -1) { 
					pop(response, 'gmSmartAuction');				
					createInternetAuctionMessage($('marketPlaceName').value,$('currentDate').value);
					closeEvent();
				} else {
					if (pars.indexOf('exported=on') > 0){
						createGroupClosedAuctionMessage('Closed Group Auction',$('currentDate').value, 'posted to');
						closeEvent();
					}
					if (pars.indexOf('exported=false') > 0){
						createGroupClosedAuctionMessage('Closed Group Auction',$('currentDate').value, 'removed from');
						closeEvent();
					}
				}
			}
		}
	});
}

