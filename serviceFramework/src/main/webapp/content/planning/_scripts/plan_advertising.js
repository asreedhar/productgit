

function openPrintAdManager(isQuick) {
	if(printAdManager == null){
		printAdManager = new PrintAdManager(isQuick);
	}
	if(isQuick && quick == null) {
		quick = new QuickPlan('advertise');
	}
	printAdManager.openPrintAdvertisingManager();
}



function closePrintAdManager() {
	printAdManager.closePrintAdvertisingManager();
var _doUpdate = printAdManager.updated();
	printAdManager = null; 
	if(_doUpdate && quick != null) {
		var _f = $(page.form);
       submitQuickAdPlan(_f);
	}
}





function editInternetAdvertising(invId,form){

	if($('internetAdvertisingFloat') ){Element.remove($('internetAdvertisingFloat'));}
	if(Element.visible($('planFloat'))){return;}

	var url = '/NextGen/EditInternetAdvertising.go';
	var pars = '&inventoryId='+invId;
	
	new Ajax.Request(
		url,
		{method:'post',
		parameters:pars,
		onComplete:function(xhr){					

			new Insertion.Top('internetAdvertising',xhr.responseText);	
var ia = new InternetAdvertising();
ia.populateForm($('editInternetAdvertisingForm'));									
			setupAdDescription();
		},
		onException:function(j,h){
			alert('Error: '+h.message);
		},
		onFailure:function(j){
		
			alert('Error: '+j.responseText);
		}
	});

}

function swapTabs(selectTab, tabsParent, outlet){
	var tabs = $(tabsParent).getElementsByTagName('li');  //get all tab items	
	$(outlet).removeClassName('hide'); //show tab body content
	for(var i = 0; i < tabs.length; i++){	
		Element.removeClassName(tabs[i],'active');		//reset all tabs to inactive state
		Element.removeClassName(tabs[i],'after');
		Element.removeClassName(tabs[i],'unchecked');		
		if( selectTab == tabs[i].id ){			
			Element.addClassName(tabs[i],'active');  //set the new active tab to active state					
		}else{			
			Element.addClassName($('outlet'+i),'hide');   //hide inactive tab bodies
			if ($('openSend'+i)) {
				if(!$('openSend'+i).checked ){								//set the unchecked tab style if the send box is unchecked
					Element.addClassName(tabs[i],'unchecked');
				}else{
					Element.removeClassName(tabs[i],'unchecked');
				}
			}
		}		
		if( i > 0){
			if( Element.hasClassName(tabs[i-1],'active')){ //check previous tab for active status			
				Element.addClassName(tabs[i],'after');    //add the after class for the tab after the active one
			}		
		}	
	}
}

function cancelInternetAdvertising(){
	Element.remove($('internetAdvertisingFloat'));
}
function saveInternetAdvertising(invId, form){
	var ia = new InternetAdvertising();
	ia.save();
	saveDefaultDescription('adDescriptionTextArea');
		var i =0;
		while($('send'+i)){
			$('send'+i).checked = $('openSend'+i).checked;			
			i++;
		}		
	Element.remove($('internetAdvertisingFloat'));
}

//populates the ad description text box with the default description
function setupAdDescription(link, ele){
	if(events.defaultDescription && events.defaultDescription != '') {
		$('adDescriptionTextArea').value = events.defaultDescription;
		$('adDescriptionTextArea').disabled = true;
		Element.show('changeDefaultDescriptionLink');
	}
}

//saves the ad description
function saveDefaultDescription(){
	
		events.defaultDescription = $F('adDescriptionTextArea');
	
}




function validatePrintAd(){
	var errors = [];
//check "WHERE"
	var sel = $F('selectedPrintAdvertiserId');
	var checkOther = false;
	var whereStr = $('printAdvertisersSelectLabel').innerHTML;
		whereStr = '<strong>'+whereStr.slice(0,whereStr.length-1)+'</strong>';
	if((sel==''||sel=='0') || Element.hasClassName('printAdvertisersSelectLabel','req')) {
  	    errors.push(whereStr);
		checkOther = true;
	}
	if(checkOther || sel=='other') {
		if(Element.visible('otherPrintAdvertiserText')) {
			if(!hasOtherNameChanged()) {
				if(errors.length == 0) { 
					errors.push(whereStr); 
				}
			}
			else {
				errors = [];
			}
		}
		else {		
		}
	}
	return errors;
}




//PRINT ADVERTISING
function gatherPrintAd(obj) {
	var hold = obj;
	var els = Form.getElements('PrintAdvertisingForm');			
	els.each(function(el) {

		switch(el.name) {	
			case 'otherPrintAdvertiserName': // one-off 'thirdParty'
				if($F('printAdvertisersSelect') == 'other') { 
					hold.otherPrintAdvertiserName = (el.value).stripTags();
					// NOT on form (used only by client)
					hold.thirdParty = hold.otherPrintAdvertiserName;
				}
			break;
			case 'planUntilDate':
					hold.planUntilDate = el.value;				
			break;
			case 'adPriceSelected':
				var radio = $(el.id);
				
					hold.adPriceSelected = radio.checked;				
				
			break;
			case 'showPrice':
					hold.showPrice = el.checked;
			case 'adText':
					hold.adText = (el.value).stripTags();			
			break;
			case 'selectedPrintAdvertiserId':
			 	var val = el.value;
				if(val != 'other') {
					val = parseInt(val);
				}
				hold.selectedPrintAdvertiserId = val;
				// NOT on form (used only by client)
				if(!hold.thirdParty) {
					hold.thirdParty = $(el.id).options[$(el.id).selectedIndex].text;
				}	
			break;
		}
	});

	return hold;
}
function populatePrintAd(obj, invId) {
	
	setupAdDescription();
	if(!obj)return;
$('addOrSave').innerHTML = '<a href="javascript:saveEvent(\''+obj.type+'\','+obj.id+');" tabindex="50">Save</a>';
$('addMore').innerHTML = '<a href="javascript:addEventAndContinue(\''+obj.type+'\','+obj.id+');" tabindex="49">Add More</a>';

$H(obj).each(function(field){	
			var objName =field.key;
			var objVal =field.value;	
				
		switch(objName){ 		
	 		case 'planUntilDate':
	 		
		 			$('planUntilDateText').value = objVal;	 	
		 			
		 		break;
	 		case 'adText':
	 	
		 			$('adTextTextArea').value = objVal;	 			 		
		 		break;
	 		case 'adPriceSelected':	 	
	 		
	 			if(	$('listPriceSelectedRadio') &&$('adPriceSelectedRadio')){
		 				$('listPriceSelectedRadio').checked = !eval(objVal);
		 				$('adPriceSelectedRadio').checked = eval(objVal);	 
		 		}			 			
		 		break;
	 		case 'showPrice':	 	
	
		 				$('showPriceCheck').checked = eval(objVal); 			 				
		 		break;
	 		case 'selectedPrintAdvertiserId':	 	
	 	
		 		var opts = $('printAdvertisersSelect').options;
			 		for(var i=0; i < opts.length; i++){
			 			if(opts[i].value == objVal){
			 				$('printAdvertisersSelect').selectedIndex = i;		 				
			 			}
			 		}		
			 			$('printAdvertisersSelectLabel').className='ok';	 				 			
		 		break;
	 		case 'thirdParty':	 	
	 			
	 				var opts = $('printAdvertisersSelect').options;
					var hideOther = false;
			 		for(var i=0; i < opts.length; i++){
			 			if(opts[i].text == objVal){
			 					hideOther = true;
						}
					}	
							 		
			 		if(!hideOther || objVal=='other'){			 		
			 				$('otherPrintAdvertiserText').value = objVal;	 	
			 				$('printAdvertisersSelect').selectedIndex = opts.length - 1;		
			 				$('printAdvertisersSelectLabel').className='ok';	
			 				$('otherPrintAdvertiserText').style.display = 'block';	
			 		}		
	 			 	
	 			break;
	 		default:
	 			break;		 		
 		}
});
handleOtherNameDependencies($('printAdvertisersSelect').value);
}


function toggleDefaultDescription() {
	$('adDescriptionTextArea').disabled = !$('adDescriptionTextArea').disabled;
	Element.toggle('changeDefaultDescriptionLink');
	if(!$('adDescriptionTextArea').disabled){
		$('adDescriptionTextArea').select();
	}
}
function disableDefaultDescription() {
	if($('adDescriptionTextArea').disabled) {
		return;
	}
	toggleDefaultDescription();
}


function handleOtherNameDependencies(sel) {
	var otherField = $('otherPrintAdvertiserText');

	if(sel != 'other' && !Element.visible(otherField)) {

		return true; //just continue on
	}
	
	if(sel == 'other' ) {	

		Element.hide('managePrintAdvertisersLink');
		Element.show(otherField);
		$('formActionLinks').style.visibility ='hidden';	
		$('adCountMsg').style.visibility = 'hidden';
		otherField.focus();
		return false;
	}else{	
		Element.show('managePrintAdvertisersLink');
		Element.hide(otherField);
		$('formActionLinks').style.visibility ='visible';	
	}
	handleRequiredName();
	otherField.value = $F('otherNameDefault');
	return true;
}





function _setAdText(invId,sel,adDesc,quick) {
	var url = '/NextGen/PrintAdvertiserAdBuilder.go';
	var pars = 'inventoryId='+invId;
		pars += '&selectedPrintAdvertiserId='+sel;
	var adText = new Ajax.Request(
		url, { 
			method: 'get', 
			parameters: pars, 
		onComplete: function(xhr) { 	
			var firstStr = xhr.getResponseHeader('firstText');
			var secondStr = xhr.getResponseHeader('secondText');
			var vehiclesInPlan = xhr.getResponseHeader('plannedVehicles');
			var includeAdDesc = xhr.getResponseHeader('includeAdDesc');
			var showPrice = eval(xhr.getResponseHeader('showPrice'));
			var adDescription = '';
			if(includeAdDesc) {
				adDescription = ' '+ adDesc +' ';
			}
			$('adTextTextArea').value = firstStr + adDescription + secondStr;
			$('adTextTextArea').disabled = false;
			$('addMore').down().href = $('addMore').down().temp;
			$('addOrSave').down().href = $('addOrSave').down().temp;
			if(!isQuick) {
				var _array = [vehiclesInPlan, showPrice];
				_finishBuildAd(_array);
			}
		}	
	});	
}

function _finishBuildAd(array) {
	var vehiclesInPlan = array[0];
	var showPrice = array[1];

	if($('showPriceCheck') != null) {
		$('showPriceCheck').checked = showPrice;
	}
	var vehiclesInPlan = parseInt(vehiclesInPlan)+1;
	var selName = $('printAdvertisersSelect').options[$('printAdvertisersSelect').selectedIndex].text;
	Element.update('selName',selName);
	Element.update('adCount',vehiclesInPlan+'');
	if(vehiclesInPlan > 0) {
		$('adCountMsg').style.visibility = 'visible';
	}
	else {
		$('adCountMsg').style.visibility = 'hidden';
	}
}

function buildAd(invId,sel) {

	$('adTextTextArea').disabled = true;
	$('adTextTextArea').value = "loading...";
	$('addMore').down().temp = $('addMore').down().href;
	$('addMore').down().removeAttribute("href");
	$('addOrSave').down().temp = $('addOrSave').down().href;
	$('addOrSave').down().removeAttribute("href");

	if(!sel) {
		sel = $F('selectedPrintAdvertiserId');
	}

	var ok = handleOtherNameDependencies(sel);
	if(!ok) { 
		$('adTextTextArea').value = '';	
		$('adTextTextArea').disabled = false;
		$('addMore').down().href = $('addMore').down().temp;
		$('addOrSave').down().href = $('addOrSave').down().temp;
		return; 
	}
	if(sel != 'Other') {
		$('adTextTextArea').disabled = true;
		$('adTextTextArea').value = "loading...";
	}
	if( sel == 0 || sel == '' || sel=='-----') { 
		if(Element.hasClassName('printAdvertisersSelectLabel','ok')) {
			Util.toggleClassNames('printAdvertisersSelectLabel','req','ok');			
		}
		$('adCountMsg').style.visibility = 'hidden';	
		$('showPriceCheck').checked = true;
		$('adTextTextArea').value = '';	
		$('adTextTextArea').disabled = false;
		$('addMore').down().href = $('addMore').down().temp;
		$('addOrSave').down().href = $('addOrSave').down().temp;

	} else {
		if(Element.hasClassName('printAdvertisersSelectLabel','req')) {
			$('advertiseErrors').innerHTML = '';
			Element.hide('advertiseErrors');
			Util.toggleClassNames('printAdvertisersSelectLabel','req','ok')
		}
		var adDesc = $F('adDescriptionTextArea');
		_setAdText(invId,sel,adDesc);
	}
}











function handleRequiredName() {
	if(hasOtherNameChanged()) {
		if(Element.hasClassName('printAdvertisersSelectLabel','req')) {
			$('advertiseErrors').innerHTML = '';
			Element.hide('advertiseErrors');
			Util.toggleClassNames('printAdvertisersSelectLabel','req','ok')
		}
	}
	else {
		if(Element.hasClassName('printAdvertisersSelectLabel','ok')) {
			Util.toggleClassNames('printAdvertisersSelectLabel','req','ok')
		}
	}
}

function hasOtherNameChanged() {
	var defaultStr = $F('otherNameDefault');
	var changed = defaultStr != $F('otherPrintAdvertiserText');
	return changed;
}



