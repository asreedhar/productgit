



//////////////////////////////////

function openAuctionManager() {
	createSimpleManagerOptionsPopup('quickAuctionId','auction');
}
var managerOptions = null;
function openWholesalerManager() {
	createSimpleManagerOptionsPopup('quickWholesalerId','wholesaler');
}

function createSimpleManagerOptionsPopup(parentId, eventType) {

	if(managerOptions == null) {
		managerOptions = new ManagerOptions();
	}
	managerOptions.loadOptionsFromSelect(parentId);
	managerOptions.setParent(parentId);
	managerOptions.setEventType(eventType);
	managerOptions.setSimpleOptions(true);
	managerOptions.preventEventFix(true);  //fixes events when in maximized plan mode
	function showWin(r,j){	
		var p = new Popup();		
		p.popup_show('manage','manageHeader',['manageCancelLink','manageSave'],'screen-center',100,false,0,0);
		managerOptions.loadOptions('manageSelect');
		managerOptions.setPopup(p);	  
	}
	var div = '<div id="manage"  style="top:-500px; left:-500px;"></div>';
	if(!$('manage')){
				new Insertion.After($('header'),div);
	}
	var a = new AjaxLoader('Loading...');
    a.setPath('/NextGen/ManageThirdParty.go');
	a.setElement('manage');
	a.setOnSuccess(showWin);
	a.performAction();
}


var quick = null;

// PLAN COUNTS
function updatePlanCounts(iter,incr) {
	var incr = incr || 1;
	if(Element.hasClassName('item' + iter,'due') || Element.hasClassName('item' + iter,'unplanned')) {
		$('unitsDueCount').innerHTML = parseInt($('unitsDueCount').innerHTML) - incr;
	}
	$('unitsPlannedCount').innerHTML = parseInt($('unitsPlannedCount').innerHTML) + incr;
}



/* ***********************************************
 * QUICK PLAN
 * 
 */

// runs on page load & AJAX 'content' update
function initQuickPlan(enab) {
	if($('quick') == null) return;
	if(quick == null && enab) {
		quick = new QuickPlan(enab);
		if(enab == 'auction' || enab == 'wholesaler') {
			quick.setDisplayElement('name');
		}
	}
    var isAdPage = $('quickAdvertise') != null;
	if(quick != null) {
		quick.dirty = false;
		if(quick.plan == 'advertise'){
			openAllExpandedSummaries();
		} else if (isAdPage) {
		    removeLayer('adText');
			closeAllExpandedSummaries();	
		}
		_setQuickPlanClasses(quick.plan);
	}
	
	
		return;
		}
		


function warnDirty() {

	if(quick == null) {
		return false;
	}else if(quick.dirty == false){
		return false;
	}else if(quick.plan == 'reprice'){
		return false;
	}else{
		var msg = 'You have unsaved ' + quick.plan.toUpperCase()+' updates.\r';
			msg += 'Please \'SAVE CHECKED\' or \'CANCEL\' before continuing.';
		alert(msg);
		return true;
	}
}

function enableQuickPlan(e,which,isHidden) {
	var _dirty = warnDirty();
	if(_dirty) {
		return false;
	}

	if(which == 'reprice') { //reprice icon toggles
		if(quick != null && quick.plan != 'reprice') {
			Element.removeClassName(ITEMS,quick.classStr);
		}
		toggleQuickReprice();
	} else if(which =='transfer') {
		if(quick != null && quick.plan != 'transfer') {
			Element.removeClassName(ITEMS,quick.classStr);
		}
		toggleQuickTransfer();
	} else { //other icons do nothing if already active
		if(quick != null && which == quick.plan) return false;
	}

	if(!isHidden) {
		var url = e;
		_loadQuickPlan(which,url);
		return false;
	}
	if(quick == null || which != quick.plan) {
		quick = null;
		initQuickPlan(which);
	}
	
	return false;
}

function _loadQuickPlan(which,loc) {
	var f = $(page.form);
	var url = checkSlash(loc.pathname);
	var pars = removeQ(loc.search);
	if(getFilterClass() != '') pars += '&filterClass='+getFilterClass();
	f.action = url + '?' + pars;
	f.submit();
}

function disableQuickPlan(e) {
	if(quick == null) return;
	if(quick.dirty) {
		quick.cancel();
		if(quick.plan == 'advertise') {
			if(e != 'null' && e.value == 'reset') {
				e.form.reset();
			}			
			resetAdCounts();
		}
	}
	if(quick.plan == 'advertise') {
        allSummsExpanded = true;
		closeAllExpandedSummaries();
		removeLayer('adText');
	}
	//var type = quick.plan;
	//Element.toggle('saved'+type);

	quick = null;
	$(ITEMS).className = '';
	restoreQuickReprice();
	return false;
}

/* ***********************************************
 * QUICK PLAN WIDGET
 * 
 */
var quickInputs;
var WIDGET = 'quickPlanTable';

function toggleQuickPlan() {

	var _dirty = warnDirty();
	if(_dirty) {
		return false;
	}


	var _isColl = Element.hasClassName(WIDGET,'collapsed');
	if(_isColl) {
		Element.removeClassName(WIDGET,'collapsed');
	} else {
		_closeQuickPlanWidget();
		disableQuickPlan(null);
	}
	return false;
}


function showQuickPlanOptions(objective) {

	var _dirty = warnDirty();
	if(_dirty) {
		return false;
	}

	_setQuickPlanClasses(objective);
	disableQuickPlan(null);
	return false;
}
function _closeQuickPlanWidget() {
	$(WIDGET).className = 'collapsed';
}
function _setQuickPlanClasses(type) {

	var _itemsClass, _widgetClass;
	switch(type) {	
		case 'retail':
		case 'spiff':
		case 'advertise':
		case 'reprice':
		case 'transfer':
			_widgetClass = 'SHOW_RETAIL'
			break;
		case 'wholesale':
		case 'auction':
		case 'wholesaler':
			_widgetClass = 'SHOW_WHOLESALE'
			break;
	}
	if($(WIDGET).className != _widgetClass) $(WIDGET).className = _widgetClass; 
	
	if(type == 'retail' || type == 'wholesale') return;
	
	_itemsClass = 'SHOW_QUICK_' + type.toUpperCase();

	if(Element.hasClassName(ITEMS,_itemsClass)) return false;
	$(ITEMS).className = _itemsClass;
	
}

/*
QUICK PLANS
*/

// used by SELECT els to update hidden field
function updateField(value,target) {
	$(target).value = value;
}

function checkChanges() {
	if(!quick || quick.dirty == false) {
		var _plan = 'QUICK PLANNING';
		if(quick) {
			_plan = 'QUICK '+quick.plan.toUpperCase()+' PLANNING';
		}
		alert('You have not selected any vehicles for '+_plan);
		return false;
	} else {
		return true;		
	}
}

function getFilterClass() {
	return $(page.content).className;
}
function getEnabledParam() {
	var enab;
	if(quick) {
		enab = quick.plan;
	} else if(quickInputs) {
		enab = quickInputs.getActive();
	}
	return !enab ? '':enab;
}

function submitQuickAdPlan(e,rangeId) {
	var _enab = getEnabledParam();
	var url;
	if(_enab == '') {
		url = '/NextGen/InventoryPlan.tx';
	} else {
		if(quick.dirty == true) {
			url = e.action;
		} else {
			url = '/NextGen/QuickAdInventoryPlan.tx';
		}
	}
	if(!rangeId) {
	  rangeId = (window.location.search.parseQuery()).rangeId;
    }
	url += '?rangeId='+rangeId;
	if(_enab != '')	url += '&enabled='+getEnabledParam();
    if((window.location.search.parseQuery()).statusCodeStr) {
       url += '&statusCodeStr' + (window.location.search.parseQuery()).statusCodeStr;
    }
	if(getFilterClass() != '')	url += '&filterClass='+getFilterClass();
	e.action = url;
	return true;
}

function submitQuickPlan(e,url) {
	checkChanges();
	var values = quick.getSubmitValues();
	var type = quick.plan;
	if(values != '') {
		var pars = 'json=' + values.toJSONString();
			pars += '&'+Form.serialize($(page.form));
			//pars += '&filterClass='+getFilterClass();
		var xhr = new Ajax.Updater(
			page.content, url, {
				method: 'post',
				parameters: pars,
				onComplete: function() {
					register();	
					vehicleListModel.loadVehicles();
					Element.toggle('saved'+type); 
				}
		});
	}
	return false;
}

function checkAll(which,e) {
	//$('quickAdvertise').style.cursor = 'wait';
	if(!quick) { //this will be true if user came from ad page directly
		quick = new QuickPlan(which);
	}
	if(which == 'advertise') {
		resetAdCounts();
	}
	quick.checkAll();

}

function uncheckAll(which,e) {
	if(!quick || quick.dirty == false) { //this will be true if user came from ad page directly
		return;
	}
	if(which == 'advertise') {
		resetAdCounts();
	}

	quick.uncheckAll();
}

/* vehicle-level _______ */
function handleQuickInline(e,which) {
	if(!quick) { //this will be true if user came from ad page directly
		quick = new QuickPlan(which);
	}
	var f = e.form;
	if(e.type == 'text') {
		if(f[quick.plan].checked == false) {
			f[quick.plan].checked = true;
			quick.check(f);
		} else {
			if($F('quickSpiffNotes') != '') {
				e.value = $F('quickSpiffNotes');
			}
		}
		return;	
	}
	if(e.checked) {
		quick.check(f);
	} else {
		quick.uncheck(f);
	}
}

/*
QUICK ADS
*/

/* page-level _______ */

// AD PUBLICATIONS
function openQuickAdPublications(e, rangeId) {
	if($('changePublications') != null) return; 
	if ( rangeId ) {
		e.href += '?rangeId=' + rangeId;
	}
	_openLayer(e,'changePublications');
}
function submitChangePublications(f) {
	var _f = $(f);
	_f.action += '?enabled='+getEnabledParam();
	if((window.location.search.parseQuery()).rangeId) {	
	  _f.action += '&rangeId=' + (window.location.search.parseQuery()).rangeId;
    }
    if((window.location.search.parseQuery()).statusCodeStr) {
       _f.action += '&statusCodeStr' + (window.location.search.parseQuery()).statusCodeStr;
    }	
	_f.submit();
}

// AD COUNTS
function upAdCount(id,incr) {
	var incr = incr || 1;
	$('adCount'+id+'Display').innerHTML = parseInt($('adCount'+id+'Display').innerHTML) + incr;
}
function downAdCount(id,incr) {
	var incr = incr || 1;
	$('adCount'+id+'Display').innerHTML = parseInt($('adCount'+id+'Display').innerHTML) - incr;
}


function resetAdCounts() { 
	var pubs = $F('pubIds');
		pubs = pubs.split(',');
	if(pubs == '') return; 
	pubs.each(function(id) {
		$('adCount'+id+'Display').innerHTML = parseInt($F('adCount'+id));
	});
}

/* vehicle-level _______ */
function handleAdvertiseQuickInline(e,hiddenId,hiddenId2) {
	if(e.checked) {
		if($(hiddenId2).value == 'false') {
			upAdCount(e.value);
			$(hiddenId2).value = 'true'
		}
	} else {
		if($(hiddenId2).value == 'true') {
			downAdCount(e.value);
			$(hiddenId).value = '';
			$(hiddenId2).value = 'false';
		}
	}
	quick.setDirty(true,1);
}



function openVehicleInfo(invId,e) {
	if($('vehicleInfo')){
		return false;
	}
	var url = checkSlash(e.pathname);
	var pars = 'inventoryId='+invId;
	var trigger = e.id;
	var request = new Ajax.Request(
		url, { method: 'get', parameters: pars, 
		onComplete: function(xhr){ 		
			makePopLayer('vehicleInfo',xhr.responseText,trigger);
		}
	});	
	return false;
}


function showAdDescription(iter) { 
	var parent = 'adDescription' + iter;
	//Element.addClassName(parent,'active');
	var display = 'adDescription' + iter + 'Display';
	var edit = 'adDescription' + iter + 'Edit';
	Position.clone(parent,edit, { 
		setWidth: false, 
		offsetLeft: -30, 
		offsetTop: 15 } 
	);
	$(display).style.visibility = 'hidden';
	Element.show(edit);
	$('adDescription' + iter + 'TextArea').focus();
}

function cancelAdDescriptionEdit(iter) { 
	var parent = 'adDescription' + iter;
	//Element.removeClassName(parent,'active');
	var display = 'adDescription' + iter + 'Display';
	var edit = 'adDescription' + iter + 'Edit';
	var input = 'adDescription' + iter + 'TextArea';
	$(input).value = $(input).defaultValue;
	Element.hide(edit);
	$(display).style.visibility = '';
}
function saveAdDescriptionEdit(iter) {
	//var parent = 'adDescription' + iter;
	//Element.removeClassName(parent,'active');
	var display = 'adDescription' + iter + 'Display';
	var edit = 'adDescription' + iter + 'Edit';
	var input = 'adDescription' + iter + 'TextArea';
	var adStr = $F(input);
	$('adDescription'+iter).value = adStr;
	var _ell = '...'
	var _str = adStr.length > 35 ? adStr.slice(0,34) + _ell : adStr;
	$(display).innerHTML = _str;
	Element.hide(edit);
	$(display).style.visibility = '';
}


// AD TEXT
function loadAdText(e,parentId,iter) { 
	if($('adText') != null) removeLayer('adText');//return;

    //clear any other active element [fixes DE263]
    var _lis = $('quickAdvertise' + iter).getElementsByTagName('LI');
    var _lastActive = $A(_lis).find(function(l) {
        return Element.hasClassName(l,'active');
     });
     if(_lastActive) {
        Element.removeClassName(_lastActive,'active')
     }

	var url = checkSlash(e.pathname);
	var pars = removeQ(e.search);
		pars += '&from=' + parentId;
	var req = new Ajax.Request(
		url, { 
			method: 'get', 
			parameters: pars,
			onComplete: function(xhr){
				Element.addClassName(parentId,'active');
				makePopLayer('adText',xhr.responseText,parentId);
				_handleQuickAdText(pars);
			}
		});
}

function buildQuickAd(invId,sel,iter) {
	var adDesc = $F('adDescription'+iter+'TextArea');
	_setAdText(invId,sel,adDesc,true);
}
function clearPrintAd(){
	if($('adTextTextArea')){
		$('adTextTextArea').value = '';
	}
}
function cancelAdTextEdit(parent) {
	if($('vehicleInfo') != null) removeLayer('vehicleInfo');
	Element.removeClassName(parent,'active');
	removeLayer('adText');
}
function saveAdTextEdit(value,hiddenId,check) {
	$(hiddenId).value = $F(value);
	if(!$(check).checked) {
		$(check).click();
	}
	$('cancelAdTextEditLink').click();
}
function _handleQuickAdText(pars) {
	var _textTarget = (pars.parseQuery()).saveTo;
	var iter = (pars.parseQuery()).iter;
	if($(_textTarget).value != '') {
		$('adTextTextArea').value = $F(_textTarget);
	} else {
		var inventoryId = (pars.parseQuery()).inventoryId;
		var sel = (pars.parseQuery()).selectedPrintAdvertiserId;
		buildQuickAd(inventoryId,sel,iter);
	}
}

// AD HISTORY
function openQuickAdHistory(e) {
	if($('history') != null) removeLayer('history');
	_openLayer(e,'history');
}


// REPRICE

function toggleQuickReprice(column) {
	var parentEl = $(page.el);
	var _tog = Element.hasClassName(parentEl,'REPRICE_ENABLED') || Element.hasClassName(parentEl,'REPRICE_ENABLED_ALL');
	if(_tog) {
		disableQuickReprice(column);
	} else {
		enableQuickReprice(column);
	}
}

function enableQuickReprice(column) {
	var classStr;
	var el = $(page.items);
	if(column == null) {
		classStr = 'REPRICE_ENABLED';
	}
	Element.removeClassName(el,classStr);
	Element.addClassName(el,classStr+'_ALL');
	if (page.setQuickReprice) page.setQuickReprice(true);
}

function disableQuickReprice(column) {
	var el = $(page.items);
	var classStr = 'REPRICE_ENABLED';
	Element.removeClassName(el,classStr);
	Element.removeClassName(el,classStr+'_ALL');
	if (page.setQuickReprice) page.setQuickReprice(false);
}

function restoreQuickReprice(column) {
	var el = $(page.items);
	var classStr = 'REPRICE_ENABLED';
	if(Element.hasClassName(el,classStr+'_ALL')) {
		Element.removeClassName(el,classStr+'_ALL');
	}
	if(page.isQuick()) {
		Element.removeClassName(el,'REPRICE_DISABLED');
		Element.addClassName(el,classStr);	
	}
}


// TRANSFER

function toggleQuickTransfer(column) {
	var parentEl = $(page.el);
	var _tog = Element.hasClassName(parentEl,'TRANSFER_ENABLED') || Element.hasClassName(parentEl,'TRANSFER_ENABLED_ALL');
	if(_tog) {
		disableQuickTransfer(column);
	} else {
		enableQuickTransfer(column);
	}
}

function enableQuickTransfer(column) {
	var classStr;
	var el = $(page.items);
	if(column == null) {
		classStr = 'TRANSFER_ENABLED';
	}
	Element.removeClassName(el,classStr);
	Element.addClassName(el,classStr+'_ALL');
}

function disableQuickTransfer(column) {
	var el = $(page.items);
	var classStr = 'TRANSFER_ENABLED';
	Element.removeClassName(el,classStr);
	Element.removeClassName(el,classStr+'_ALL');	
}

function restoreQuickTransfer(column) {
	var el = $(page.items);
	var classStr = 'TRANSFER_ENABLED';
	if(Element.hasClassName(el,classStr+'_ALL')) {
		Element.removeClassName(el,classStr+'_ALL');
	}
	if(page.isQuick()) {
		Element.removeClassName(el,'TRANSFER_DISABLED');
		Element.addClassName(el,classStr);	
	}
}

/*
*/



// LAYERS //////////////////////////////////////////////
function _openLayer(e,layerId) {
	var req = new Ajax.Request(
		checkSlash(e.pathname), { 
			method: 'get', 
			parameters: removeQ(e.search)+'&layerId='+layerId,  
			onComplete: function(xhr){
//alert(xhr.responseText);
var setLeft = true;
if(layerId == 'history' || layerId == 'changePublications') {
	setLeft = false;
}
	
				makePopLayer(layerId,xhr.responseText,e,setLeft);
			}
		});
}

function removeLayer(id) {
	if($(id)) Element.remove(id);
}

function makePopLayer(id,text,_posEl,setLeft) {
	var setLeft = setLeft == null ? true:setLeft;
	var _left = 0;
	var _pop = ['',text,''];
	_pop[0] = '<div id="' + id + '" style="position: absolute; display: none; z-index: 1200;">';
	_pop[2] = '</div>';
	
	if(id == 'vehicleInfo' && $('adText') != null) {
		new Insertion.Top('adText',_pop.join(''));
	} else {
		new Insertion.Before(page.items,_pop.join(''));
	}
	
	var target = $(id);
	var posEl = $(_posEl);
	var pos = Position.cumulativeOffset(posEl);

	var _top = $(posEl).getHeight();
	if(id == 'changePublications') {
		_top = _top - 75;
	}
	if(id == 'history') {
		_top = _top - 30;
	}
	if(id == 'vehicleInfo') {
		_top = -($(id).getHeight()/2);
	}
	
	if(id == 'adText' || id == 'vehicleInfo') {
		var _parentR = pos[0] + posEl.getWidth();
		var _viewR = findViewportDims()[0];
		var _diff = _viewR - _parentR;
		var _right = pos[0] + $(id).getWidth();
		if(_right > _viewR) {
			_left = _viewR - (_right + _diff);
		}
	}
	Position.clone(posEl,target, { 
		setWidth: false, setHeight: false, setLeft: setLeft, offsetLeft: _left, offsetTop: _top 
	});

	Element.show(target);
}






