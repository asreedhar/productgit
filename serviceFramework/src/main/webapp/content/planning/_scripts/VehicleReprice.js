
var Reprice = Class.create();
Reprice.prototype = {
	initialize: function(iter) {
		this.iter = parseInt(iter) || '';
		this.parentEl = 'listPrice' + this.iter;
		this.displayEl = 'listPriceDisplay' + this.iter;
		this.editEl = 'listPriceEdit' + this.iter;
		this.inputEl = 'repriceInput' + this.iter;
		this.formObj = null;
		this.confirmed = false;
	},
	switchToEdit: function(display,edit) {
		Element.addClassName(this.parentEl,'OPEN');
		Element.removeClassName(this.parentEl,'CLOSED');

/*
		if(!display || !edit) {
			display = this.displayEl;
			edit = this.editEl;
		}
		$(display).style.display = 'none';
		$(edit).style.display = 'inline';
*/
		this.focusInput();
	},
	switchToDisplay: function(display,edit) {
		Element.removeClassName(this.parentEl,'OPEN');
		Element.addClassName(this.parentEl,'CLOSED');
/*
		if(!display || !edit) {
			display = this.displayEl;
			edit = this.editEl;
		} 
		$(display).style.display = 'inline';
		$(edit).style.display = 'none';
*/
	},
	focusInput: function() {
		$(this.inputEl).focus();
	},
	reset: function() {
		if(this.formObj == null) {
	//alert('resetting to '+$(this.inputEl).defaultValue);
			$(this.inputEl).value = $(this.inputEl).defaultValue;
		} else {
			$(this.formObj).reset();
		}
		this.confirmed = false;
	},
	getRepriceConfirmation: function() {
		return $F('confirmReprice') == 'true';
	},
	validate: function(confirmed) {
		this._setInputValue();
		// chars
		if(validatePriceValue(this.inputValue) === false) {
			this.reset();
			return false;
		}
		if(confirmed == true) {
			this.confirmed = true;
			return true;
		}
		// threshold
	 	return this._validateThreshold(false);
	},
	post: function(url) {
		this._setInputValue();
		$(this.inputEl).value = this.inputValue;
		
		var _action = url != null ? url : $(this.formObj).action;
		var _params = url != null ? Form.serialize(this.parentEl) : Form.serialize(this.formObj);
			_params += '&iter=' + this.iter;
			_params += '&confirmed=' + this.confirmed;
			_params += '&update=true';
		var xhr = new Ajax.Updater(
			'vehicle'+this.iter, _action, { 
				method: 'post',
				parameters: _params,
				onComplete: function(xhr){
					//alert(xhr.responseText)
				}
		});
	},
	moveFocus: function(visible) { 
		this._visible = visible || null;
		// tests whether there is next input and if it is visible - then moves focus if both are true
		var _nextCount = this.iter + 1;
		var _nextInput = 'repriceInput' + _nextCount;
		var _nextIsVisible = false;
		var _isNoNext = false;
		var _continue = true;

		while(_continue == true) {
			_isNoNext = this._isLastInput(_nextInput);
			if(_isNoNext == true) {
				_continue = false; 
			}
			else {
				_nextIsVisible = this._isNextVisible(_nextCount);
				_continue = _nextIsVisible == false;
			}
			if(_continue == true) {
				_nextCount++;
				_nextInput = 'repriceInput' + _nextCount;
			}
		}
		if(_isNoNext) {

			return;
		}
		if(_nextIsVisible){
			
			$(_nextInput).focus();
		}
	},
	_validateThreshold: function(confirmed) {
		if(confirmed) {
			return true;
		}
		var confirm = this.getRepriceConfirmation();
		if(!confirm) {
			return true;
		}
		var priceThreshold = parseInt($F('repriceThreshold'));	
		var input = this.inputValue;

		var orig = parseInt($(this.inputEl).defaultValue);
		if(isNaN(orig)){ //detailed plan reprice
			
			var listPriceHTML = $('listPriceDisplay');
			if (listPriceHTML) {
				orig = parseFloat( formatPrice($('listPriceDisplay').innerHTML.stripTags()) );
				if(isNaN(orig)){
					orig = 0;
				}
			}
			else{
				orig = 0;
			}
		} 
		var thresh = 0.01 * priceThreshold;
		var t = orig *  thresh;
 
		if(parseInt(input) >= parseInt(orig + t) ||  parseInt(input) <= parseInt(orig - t)){
			Element.addClassName(this.inputEl,'red');
			setConfirmHiddenFields('reprice', this.iter,true);
			var inputPos = Position.cumulativeOffset($(this.inputEl))
			var pop = $('confirmBox');
			Element.show(pop);
			pop.style.left = (inputPos[0] - 25)+'px';
			pop.style.top = inputPos[1]+'px';
	
			return false;
		}
		else {
			return true;			
		}
	},	
	_isLastInput: function(nextInput) {
		return $(nextInput) == null;
	},
	_isNextVisible: function(nextCount) {	
		try{
			$('listPriceEdit' + nextCount).focus(); 
			return true;  //if focus fails, we never get here
		}catch(e) {
			return false;
		}		
	},
	_getFormElement: function() {
		var el = $(this.parentEl);
		var childEls = el.getElementsByTagName('form');
		return childEls[0];	
	},
	_setInputValue: function() {
		this.inputValue = formatPrice($F(this.inputEl));
	}
}

