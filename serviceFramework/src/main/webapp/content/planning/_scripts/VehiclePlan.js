/* CONSTANTS */

var plannedStr = 'Planned: ';
var runUntilStr = 'Run Until: ';
var runStr = 'Run: ';
var dateStr = 'Date: ';
var editStr = 'Edit';
var deleteStr = 'Delete';
var notesStr = 'Notes: ';
var adTextStr = 'Ad text: ';
var locationStr = 'Location: ';
var timer = null;



/* CONTAINER els */
var FORM = 'InventoryPlanningForm';
var ITEMS = 'items';
var CONTENT = 'content';



/*  PAGE OBJECT */
/*--------------------------------------------------------------------------*/

var Page = Class.create();
Page.prototype = {
    initialize: function(){
		this.form = 'InventoryPlanningForm';
    	this.content = 'content';
		this.el = 'items';

		this.items = 'items';
		this.confirmLayer = 'confirmBox';	
		this.manageLayer = 'manage';
		this.planLayer = 'vehiclePlan';

  		this.reset();
    },
    reset: function(){
		this.planActivated = false;
		this.repriced = false;
		this.confirmOpen = false;
		this.dirty = false;
		bPageReload = false;			
//this.reload = false;		
    },
	setDirty: function(bool) {
		this.dirty = bool;
	},
	isQuick: function() {
		var _enab = $F('quickRepriceEnabled') == 'true';
		if(!_enab) {
			_enab = $(this.items).hasClassName('REPRICE_ENABLED') || $(this.items).hasClassName('REPRICE_ENABLED_ALL'); 
		}
		return _enab;
	},
	getQuickReprice:function () {
		return this._isQuickReprice || false;
	},	
	setQuickReprice:function (bool) {
		this._isQuickReprice = !!bool;
	},
	triggerQuickRepriceSavedState:function () {
		if (this.getQuickReprice()) {
			enableQuickReprice();
		} else {
			disableQuickReprice();
		}
	},	
    removeLayers: function(){
		if($(this.planLayer) != null) {
			Element.remove(this.planLayer)
		}
		if($(this.confirmBox) != null) {
			Element.remove(this.confirmLayer)
		}
		if($(this.manageBox)){
			Element.remove(this.manageLayer);
		}
    },
    toggleShade: function(){
	    if(Element.hasClassName(this.el,'shade')) {
			Element.removeClassName(this.el,'shade');
	    } else {
			Element.addClassName(this.el,'shade');
	    }
    },
    handleDirty: function(id){ //called by estock card
    	if(bPageReload) {
     		var iterNum = $F('iter'+id);
    		if($('vehiclePlan')) { Element.show('estockMsg'); }
    		else { 
    			updateVehicle(id,iterNum,true);
    			bPageReload = false;
    		}
     	}
    }
}


/* VEHICLE CLASS ----------------------------- */
var Vehicle = Class.create();
Vehicle.prototype = {
    initialize: function(invId,iter){
		this.id = invId; //inventory id
		this.iter = iter; 
		this.description = this._getDescription();
		this.link = $('planLink' + iter);
		this.el = 'vehicle' + iter;
		this.table = 'item' + iter;
		this._origLink = null;
		this.url = '/NextGen/UpdatePlanVehicle.go';

    },
    enableButtons: function(){
		$('savePlanBtn').disabled = false;
		if($('savePlanNextBtn') != null) {
			$('savePlanNextBtn').disabled = false;
		}
    },
    disableButtons: function(){
		$('savePlanBtn').disabled = true;
		if($('savePlanNextBtn') != null) {
			$('savePlanNextBtn').disabled = true;
		}
    },
    getNextLink: function(){
    	return 'vehPlanLink' + (this.iter + 1);
    },
    toggleTable: function(){
    	Element.toggle(this.table);
    },
    toggleLoadingText: function(){
  
	    if(this._origLink == null) {
			this._origLink = (this.link).innerHTML;
			(this.link).innerHTML = 'Loading plan...';
	    } else {
			(this.link).innerHTML = this._origLink;
	    	this._origLink = null;
	    }
    },
    restorePlanLink: function(){
		(this.link).innerHTML = this._origLink;
    },
    populateInsights: function(){
		var array = this._getInsights();
    	if(array.length != 0) Element.update('insights',array[0]);
    },
    getUpdateParams: function(){
		var	params =  '&update=true';
			params += '&inventoryId=' + vehicle.id;
			params += '&iter=' + vehicle.iter;
			//params += 'overrideUpdate=' + bPageReload;
			return params;
    },
    _getDescription: function(){
		var desc = $('vehDesc'+this.iter).innerHTML;
		return desc.stripTags();
    },
    _getInsights: function(){
		var array = [];
  		var cont = $('insights'+this.id);
		if($('insights'+this.id)) {
    		var val = ($('insights'+this.id).innerHTML).stripTags();
    		val.search('/') != -1 ? array = val.split('/') : array.push(val);
		}
		return array;
    }
}



/*  PLAN CLASS */
/*--------------------------------------------------------------------------*/
var Plan = Class.create();
Plan.prototype = {
    initialize: function(obj) {
   		var activeEvents = obj;
		this.objective		= activeEvents.objective;
		this.originalObjective		= activeEvents.objective;
 		this.nextPlanDate 	= activeEvents.nextPlanDate;
		this.service		= activeEvents.service.length==0?{}:activeEvents.service[0];
		this.detail			= activeEvents.detail.length==0?{}:activeEvents.detail[0];	
		//editable events
		this.A	= { 
				advertise: 	activeEvents.advertise,
				lotPromote: activeEvents.lotPromote,
				auction:	activeEvents.auction,
				wholesaler:	activeEvents.wholesaler
		};
		this.defaultDescription = activeEvents.defaultDescription;
//all below are sent as array of one obj - only need first one
		this.spiff 			= activeEvents.spiff.length==0?{}:activeEvents.spiff[0]; 
		this.retailOther	= activeEvents.retailOther.length==0?{}:activeEvents.retailOther[0];
		this.wholesaleOther = activeEvents.wholesaleOther.length==0?{}:activeEvents.wholesaleOther[0];
		this.other			= activeEvents.other.length==0?{}:activeEvents.other[0];
		this.sold			= activeEvents.sold.length==0?{}:activeEvents.sold[0];
		this.certified		= activeEvents.certified;
		this.specialFinance	= activeEvents.specialFinance;
		this.adPrice = activeEvents.adPrice?activeEvents.adPrice:'0';
		this.isR = this.objective=='retail';
		this.isW = this.objective=='wholesale';
		this.isS = this.objective=='sold';
		this.isO = this.objective=='other';
		this.repriceConfirmed = false;
		this.hold = {};	
		this.hasTransferPricing = false;
		this.PLAN_FORMS = ['VehicleDetailsForm','VehicleRepriceForm','VehicleServiceForm','VehiclePlanForm'];
		if(document.getElementById("VehicleTransferForm") != null) {
			this.hasTransferPricing = true;
			this.PLAN_FORMS.push('VehicleTransferForm');
		}

    },
	usesTransferPricing: function() {
			return this.hasTransferPricing;
	},
	setRepriceConfirmed: function(value) {
		this.repriceConfirmed = value;
	},
	getRepriceConfirmed: function() {
		return this.repriceConfirmed;
	},
	/**
	 * Puts the form field names into an array and populates the form fields on the page from the object's internal data.
	 */
    populate: function() {  
			var src = this;
			var fields = []; 		
			var forms = this.PLAN_FORMS; 	
			forms.each(function(tar) {
				var els = $(tar).getElements();
    			els.each(function(el) {
       				fields.push(el.name)
   	    		});
			});
		var objective = this.objective;
		var serviceNotes = ''; var notes; var id;

		fields.each(function(f) {
			//fields
			switch(f) {	
				case 'adPriceEditTxt':	
					$('adPriceEditText').value=src['adPrice'];
					$('adPrice').innerHTML = Format.currency(src['adPrice']);
				break;
				case 'certified':
				case 'specialFinance':
					if(src[f]=='true') {
						$(f+'Check').checked=true;
					}	
				break;
				case 'service':
				case 'detail':
					if(serviceNotes==''&&src[f].strategyNotes) { 
						serviceNotes = src[f].strategyNotes;
					}
					id = src[f].id;
					if(id) {
						$(f+'Check').checked=true;
						$(f+'Check').value=id;
					}						
				break;	
				case 'serviceNotes':
					$('serviceNotesText').value = serviceNotes==''?'':serviceNotes;
				break;
				case 'enterDate':
					if(src['service']['enterDate']){
						$('enterDateText').value = src['service']['enterDate'];
					}
					else if (src['detail']['enterDate']){
						$('enterDateText').value = src['detail']['enterDate'];
					}
				break;
				case 'exitDate':
					if(src['service']['exitDate']){
						$('exitDateText').value = src['service']['exitDate'];
					}
					else if (src['detail']['exitDate']){
						$('exitDateText').value = src['detail']['exitDate'];
					}
				break;		
				case 'spiff':	//RETAIL field
					id = src[f].id;
					if(id) {
						$('spiffCheck').checked=true;
						$('spiffCheck').value=id;
					}
					notes = src[f].spiffNotes;
					$('spiffNotesText').value = !notes?'':notes;
				break;
				case 'retailOther':	//RETAIL field
					id = src[f].id;
					if(id) {
						$('retailOtherCheck').checked=true;
						$('retailOtherCheck').value=id;
					}
					notes = src.retailOther.retailOtherNotes;
					$('retailOtherNotesText').value = !notes?'':notes;
				break;
				case 'wholesaleOther':
					id = src[f].id;
					if(id) {
						$('wholesaleOtherCheck').checked=true;
						$('wholesaleOtherCheck').value=id;
					}
					notes = src.wholesaleOther.wholesaleOtherNotes;
					$('wholesaleOtherNotesText').value = !notes?'':notes;
				break;
				case 'nextPlanDate':
					if(src[f]){
						$(f).value = src[f];
					}
				break;
				case 'soldAs':
					$('soldAsSelect').selectedIndex = src.sold.soldAs=='retail'?1:src.sold.soldAs=='wholesale'?2:0;
					if($('soldAsSelect').selectedIndex == 0){
						$('soldAsSelectLabel').className = 'req';
					}
				break;							
				case 'soldToName': //name
				// if src.sold.name exists, populate thirdParty field with it
					if (src.sold.name) {
						var n = src.sold.name;
						$('soldToNameText').value = n; //$('thirdPartyText').value = n;
						$('soldToNameTextLabel').className = n == ''?'req':'ok';
					}
				break;
				case 'soldDate': //beginDate
					$('soldDateText').value = !src.sold.beginDate?'':src.sold.beginDate;
				break;
				case 'soldStrategyNotes': //strategy notes
					notes = src.sold.strategyNotes;	
					$('soldStrategyNotesText').value = !notes?'':notes;
				break;				
				case 'otherStrategyNotes': //strategy notes
					notes = src.other.strategyNotes;	
					$('otherStrategyNotesText').value =!notes?'':notes;
				break;
			}
			notes = '';
			id = '';
		});
    },

    gather: function() {
		var forms = this.PLAN_FORMS;	//source forms
		var fields = []; 	//fields to grab
		forms.each(function(form) {
			var els = Form.getElements(form);
			els.each(function(el) {
				fields.push(el)
			});
		});
		var isActive = false;	
		var strat = plan.objective;
		fields.each(function(f) {
			switch(f.name){

					case 'retailOther':
					case 'wholesaleOther':
					case 'spiff':		
					//current event that has been deactivated
					if(!$(f.name+'Check').checked&&f.value!=-1) {
						events.D.push(parseInt(f.value));
					}
					//brand new event
					if($(f.name+'Check').checked&&f.value==-1) {
						S.N[f.name] = { strategyNotes: '' }
					}
					//notes
					if($(f.name+'Check').checked&&$(f.name+'NotesText')) { 
						var target = f.value==-1?S.N:S.U;
						if(f.name!='spiff'&&$F(f.name+'NotesText')!='') {
								target[f.name]= { strategyNotes: $F(f.name+'NotesText').stripTags() }
								if(f.value!=-1) target[f.name].id = parseInt($F(f.name+'Check'))
						}
						if(f.name == 'spiff' && $F('spiffNotesText')!='') {
								target[f.name] = {};
								q = target[f.name];
								q.strategyNotes = $F('spiffNotesText').stripTags();
								if(f.value!=-1) q.id = parseInt($F(f.name+'Check'))
						}
					}
					events.toggleNoCurrent();
					break;
			}

if(strat == 'retail') {
//alert(f.name+' : '+f.value);
	switch(f.name) {
			case 'retailOther':
			case 'spiff':		
					//current event that has been deactivated
					if(!$(f.name+'Check').checked&&f.value!=-1) {
						events.D.push(parseInt(f.value));
					}
					//brand new event
					if($(f.name+'Check').checked&&f.value==-1) {
						S.N[f.name] = { strategyNotes: '' }
					}
					//notes
					if($(f.name+'Check').checked&&$(f.name+'NotesText')) { 
					var target = f.value==-1?S.N:S.U;
					if(f.name!='spiff'&&$F(f.name+'NotesText')!='') {
							target[f.name]= { strategyNotes: $F(f.name+'NotesText').stripTags() }
							if(f.value!=-1) target[f.name].id = parseInt($F(f.name+'Check'))
					}
					if(f.name == 'spiff' && $F('spiffNotesText')!='') {
							target[f.name] = {};
							q = target[f.name];
							q.strategyNotes = $F('spiffNotesText').stripTags();
							if(f.value!=-1) q.id = parseInt($F(f.name+'Check'))
					}
				}
	}

}
if(strat == 'sold') {
	switch(f.name) {
		case 'soldAs':
			S.sold.soldAs = f.value; 
		break;	
		case 'soldToName':
			if(f.value!='') {
				S.sold.name = f.value.stripTags(); 
			}
		break;			
		case 'soldDate':
			if(f.value!='') {
				S.sold.beginDate = f.value; 
			}
		break;
		case 'soldStrategyNotes':
			if(f.value!='') {
				S.sold.strategyNotes = f.value.stripTags();
			}
		break;
	}
}
if(strat == 'other') {
	if(f.name == 'otherStrategyNotes') {
		S.other.strategyNotes = f.value.stripTags();
	}
}

//COMMON fields
//fields
			switch(f.name) {	
		
				case 'certified':
					S.certified = $('certifiedCheck').checked ? true:false;
					break;	
				case 'specialFinance':
					S.specialFinance = $('specialFinanceCheck').checked ? true:false;
					break;
				case 'nextPlanDate':
					S.nextPlanDate = f.value
					break;
				case 'service':
				case 'detail':
						isActive = $(f.name+'Check').checked;
						var isNew = (f.value==-1);
					// get notes value
					var notes = '';
					var enterDate = '';
					var exitdate = '';
					if (isActive) {
						notes = $F((f.name=='detail'?'service':f.name)+'NotesText').stripTags();
						enterDate = $F('enterDateText');
						exitDate = $F('exitDateText');		
					}
					// update queue
					if (isNew) {
						if (isActive) {
							// new service or detail
							S.N[f.name] = { strategyNotes: notes,enterDate:enterDate,exitDate:exitDate };
						}
					}
					else {
						if (isActive) {
							// could have an update to service or detail
							S.U[f.name]= { id: parseInt(f.value), strategyNotes: notes,enterDate:enterDate,exitDate:exitDate }
					}
						else {
							// remove of service or detail
							events.D.push(parseInt(f.value));
						}
					}
					break;					
					case 'retailOther':
					case 'wholesaleOther':
					case 'spiff':		
					//current event that has been deactivated
					if(!$(f.name+'Check').checked&&f.value!=-1) {
						events.D.push(parseInt(f.value));
					}
					//brand new event
					if($(f.name+'Check').checked&&f.value==-1) {
						S.N[f.name] = { strategyNotes: '' }
					}
					//notes
					if($(f.name+'Check').checked&&$(f.name+'NotesText')) { 
					var target = f.value==-1?S.N:S.U;
					if(f.name!='spiff'&&$F(f.name+'NotesText')!='') {
							target[f.name]= { strategyNotes: $F(f.name+'NotesText').stripTags() }
							if(f.value!=-1) target[f.name].id = parseInt($F(f.name+'Check'))
					}
					if(f.name == 'spiff' && $F('spiffNotesText')!='') {
							target[f.name] = {};
							q = target[f.name];
							q.strategyNotes = $F('spiffNotesText').stripTags();
							if(f.value!=-1) q.id = parseInt($F(f.name+'Check'))
					}
				}
				break;
				default:
					break;			
			}
		});
    },
    save: function() {
		var src = $('inventoryPlan'); //source form
		
		var objective;
		objective = S.objective;
		var tar = objective=='sold'?S.sold:S.other;	//target object
	   	var fields = this.fields(src);	//fields to grab
			fields.each(function(f) {
				//fields
				switch(f) {
					case 'thirdParty':
						tar.soldAs = $F('soldAs');
						tar.thirdParty = $F('thirdParty');
						break;	
					case 'beginDate':		
						tar.beginDate = $F('beginDate');
						break;
					case 'strategyNotes':		
						tar.strategyNotes = $F('strategyNotes');
						break;
				}
			});
    },
    validate: function() {
    
				var ok = true;
				var message = 'Please correct the following error(s):\n\n';
				if($('planFloat') && !Element.empty('planFloat') || $('internetAdvertisingFloat') && !Element.empty('internetAdvertisingFloat')) {
					var type = events.objective.toUpperCase();
					
					if($('internetAdvertisingFloat') && !Element.empty('internetAdvertisingFloat')){  //check if internet ad is open
						type = 'INTERNET ADVERTISING';
					}
					message +='- Complete your open '+type+' STRATEGY before saving the plan.\n';
					ok = false;
				
				}
				
				if($('adPriceEdit') && Element.visible($('adPriceEdit'))){  //check if edit ad price is open
					message +='- Confirm your Ad Price change before saving the plan.\n';
					$('adPriceInput').select();
					ok = false;			
				}
				
		if(!ok){
			alert(message);
			return ok;
		}
		var hasObjective;
		hasObjective = plan.objective;
		if(hasObjective && (plan.objective != events.objective)) {
			var huh = confirm('Saving this plan will cause your current '+plan.objective.toUpperCase()+' plan to be discarded.\rAre you sure?');
			if(!huh) { 
				ok = false;
			}
			else {
				plan.objective=events.objective;
			}
		}
		
		errors = [];
		var fields = [];
		var forms = this.PLAN_FORMS;
		forms.each(function(tar) {
			var els = Form.getElements(tar);
			els.each(function(el) {
				fields.push(el.name);
			});
		});
		
		//alert('validating = '+fields);
		
		fields.each(function(name) {
		
			if(name == 'soldAs' && events.objective == 'sold'){
				name = 'soldAsSelect';
			}
			if(name=='soldToName' && events.objective == 'sold'){
			
				name='soldToNameText';
			}
		
			if($(name+'Label') && Element.hasClassName($(name+'Label'),'req') && name != 'enterDate') { 
				errorStr = $(name+'Label').innerHTML;
				sub = '<strong>'+errorStr.slice(0,errorStr.length-1)+'</strong>';
				errors.push(sub);
			}
				
		});
			
				if( $('enterExitErrors')){
				
				
					var msg='';			
					$('enterExitErrors').innerHTML = ''; 
					
					if( $('enterDateLabel').className == 'req' ){
						msg += '* <strong>ENTER</strong>  is a required field.</br>';
						ok = false;
					}
					if($F('enterDateText') != '' && $F('exitDateText') != ''){
						if( Date.parse($F('exitDateText') ) <  Date.parse($F('enterDateText') ) ){
						
							msg += '*<strong> EXIT</strong> date cannot be earlier than<strong> ENTER</strong> date.</br>';
							ok = false;
						}		
						
					}
							
					if($F('enterDateText') != '' || $F('exitDateText') != ''){
			
						if(	!$('serviceCheck').checked && !$('detailCheck').checked){
						
							msg += '* You must check either <strong>SERVICE</strong> or <strong>DETAIL</strong>.</br>';
							ok = false;
						
						}
					}
					if(msg != ''){
						$('enterExitErrors').innerHTML = msg;
						$('enterExitErrors').style.display = 'block';
					}		
				
			}
				
			if(msg != ''){
				$('enterExitErrors').innerHTML = msg;
				$('enterExitErrors').style.display = 'block';
			}		
	
		if( $('eventErrors')){
			var msg='';
	    	$('eventErrors').innerHTML = ''; //reset error message field
			if(errors!='') { 
	    	    msg = errors.length==1?'is a':'are';    	   
	    	    msg += ' required field'
	    	    msg += errors.length==1?'.':'s.';
		    	$('eventErrors').style.display='block';
	    		Element.update('eventErrors','\* '+errors.join(' and ')+' '+msg);
	    		msg = '<br/>';
	    	}
	    	if(errors!=''){
	    		$('eventErrors').innerHTML = $('eventErrors').innerHTML+msg;
	    		if($('soldAsSelect') && $F('soldAsSelect') == '0'){
	    			
	    			$('soldAsSelectLabel').className = 'req';
	    		}
				return false;
			}
		}
//alert('no errors = \n'+ok);
		return ok;
    },    
    fields: function(formName) {

		var els = Form.getElements(formName);
 		var hold = [];
		els.each(function(f) {
			//exclusions
			
			switch(f.name) {	
				case 'ListPrice':
				case 'approach':
				case 'savePlan':
				case 'savePlanNext':
					break;
				default:
					hold.push(f.name);
					break;
			}
		});

		return hold;
    }
}


var Send = Class.create();
Send.prototype = {
    initialize: function(id,objective,origObjective){
		this.id = id;
		this.objective = objective;
		this.originalObjective = origObjective;
		this.N;
		this.U;
		if(this.objective=='sold'||this.objective=='other') {
			this.sold = {};
			this.other = {};
		}		
		this.D;
    },
    clean: function(){
		delete S;
    }
}


//increment or decrement the value of an element
var Incrementor = Class.create();
Incrementor.prototype = {
	initialize: function( element ) {	
		this._element 	   = $(element);
		if( this._element ) {
			this._input = this._element.tagName == 'input' || this._element.tagName == 'textarea';
			this._currentValue = this._input ? parseInt(this._element.value) : parseInt(this._element.innerHTML);
		}
	},
	set: function(val) {
		if(this._element) {
			if( this._input ){
				this._element.value = val;
			}else {
				this._element.innerHTML = val;
			}
		}
	},
	add: function(val) {
		this._currentValue += parseInt(val);
		this.set( this._currentValue );
	},
	subtract: function(val) {
		this._currentValue -= parseInt(val);
		this.set( this._currentValue );
	}
	

};


