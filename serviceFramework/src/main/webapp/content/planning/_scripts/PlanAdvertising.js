


function InternetAdvertising(){
	this.advertisers = new Array();
	this.save = save;
	this.populateForm = populateForm;
	this.update = update;
	
	function save(){		//save the elements to this object
		var i = 0;
		this.advertisers = new Array();
		while($('outlet'+i)){	   //iterate over outlet divs and get all child inputs.  Push advertiser object on stack
			var elements = $A($('outlet'+i).getElementsByTagName('input'));
			elements = elements.concat($A($('outlet'+i).getElementsByTagName('textarea')));
			elements = elements.concat($A($('outlet'+i).getElementsByTagName('select')));			
			this.advertisers.push(new Advertiser(elements));
			i++;
		}		
	}
	
	function update(form){	
		if(!$(form))return;
	
		if(this.advertisers.length > 0){	//the user has edited the internet advertisement so update the send status of the existing objects
			for(var e = 0; e< this.advertisers.length; e++){
				var i=0;
				while($('advertiserId'+i)){  //user is submitting after edit, just update the send status of each venue with minimized form send check				
					if(this.advertisers[e].getId() == $('advertiserId'+i).value){
						this.advertisers[e].setSend($('send'+i).checked);		
						break;	
					}
					i++;
			}
			}
		}else{						
			var i=0;
			while($('advertiserId'+i)){  //user is only submitting, so create new advertiser objects with details
				var ad = new Advertiser(null);
				ad.setId($('advertiserId'+i).value);
				ad.setSend($('send'+i).checked);
				this.advertisers.push(ad);			
				i++;
			}
		}	
	}
				
	function populateForm(form){		//user clicked edit. make sure the send checks match the minimized form, make sure values are accurate
		var i = 0;
		while($('outlet'+i)){			
			$('openSend'+i).checked = $('send'+i).checked;		
			if(!$('openSend'+i).checked){
				if(!Element.hasClassName($('outlet'+i+'Tab'),'active')) {					
					Element.addClassName($('outlet'+i+'Tab'),'unchecked');
				}				
			}
			i++;
		}
	}
}

function Advertiser(elements){

	//member vars
	this.id = locateValue('advertiserId');
	this.send = isChecked('sendVehicle');
	
	//public methods
	this.setId = setId;
	this.setSend = setSend;
	
	this.getId = getId;
	this.getSend = getSend;
	
	function setId(val){	
		this.id = val;	
	}
	
	function setSend(val){	
		this.send = val;
	}

	function getId(val){	
		return this.id;
	}
	
	function getSend(){	
		return this.send;
	}
	
	function isChecked(fieldName){	
		var checked;
		$A(elements).each(function(e){			
			if(e.name.indexOf(fieldName) != -1){						
				checked = e.checked;
				return;
			}			
		});
		return checked;
	}
	function locateValue(fieldName){	
		var value;		
		$A(elements).each(function(e){	
			if(e.name.indexOf(fieldName) != -1){						
				value = e.value;			
			}
		});		
		return value;	
	}
}


//PRINT ADVERTISING MANAGER CLASS
function PrintAdManager(isQuick){

	var val;
	var isQuick = false || isQuick;
var isUpdated = false;

	this.adding = false; //used the let the save method know if this was an add or edit
	var request = null;  // global ajax object - used to ensure multiple clicks don't bomb out
	var MAX_ADVERTISERS = 15;
	var MANAGER_ELE    = 'printAdvertisingManager';
	var MANAGER_SELECT = 'printAdvertisersMgrSelect';
	var PARENT_SELECT  = 'printAdvertisersSelect';
	var MANAGER_FORM   = 'printAdvertisingManagerForm';
	var MANAGER_DELETE = 'printAdEditDelete';
	var MANAGER_SAVE_CANCEL = 'printAdSaveCancel';
	var MANAGER_NAME_TEXT = 'printAdvertiserNameText';
	var MANAGER_ERRORS = 'managerErrors';
	
	//expose these methods
	this.openPrintAdvertisingManager = openPrintAdvertisingManager;
	this.closePrintAdvertisingManager = closePrintAdvertisingManager;
	this.deletePrintAdvertiser = deletePrintAdvertiser;
	this.populateOptions = populateOptions;
	this.checkFirstlook = checkFirstlook;
	this.saveValue = saveValue;
	this.savePrintAdvertiser = savePrintAdvertiser;
	this.cancelPrintAdvertiser = cancelPrintAdvertiser;
	this.addPrintAdvertiser = addPrintAdvertiser;
	this.editPrintAdvertiser = editPrintAdvertiser;
	this.fixCurrentEvents = fixCurrentEvents;
	this.updated = updated;

	function updated() {
		return isUpdated;
	}
	function closePrintAdvertisingManager() {
		var prompt = true;
		if($(MANAGER_SELECT).disabled){  //if this is disabled, we know we are editing so prompt
			prompt = confirm('You have not saved your changes, are you sure you wish to close?');
		}			
		if(prompt){	
			printAdManagerWindow.popup_exit();		
			printAdManagerWindow = null;
			Element.remove(MANAGER_ELE);
		}
		
		
		
		
	}
	function saveValue(id, doWhat){  //used for drag drop which loses the checked status when dragging, this restores it
		if(doWhat == 'save'){	
			val = $(id).checked;
		}else{	
			$(id).checked = val;
			val = null;
		}
	}
	function openPrintAdvertisingManager() {
		if($(MANAGER_ELE)) {
			return false;
		}
		
if($(PARENT_SELECT) == null) {
	selectedValue = 0;
} else {
	var selectedValue = $F(PARENT_SELECT);
	selectedValue = (isNaN(selectedValue)?'0':selectedValue);
}


		var url = '/NextGen/OpenPrintAdvertisingManager.go';
		var pars = 'selectedPrintAdvertiserId='+selectedValue;
			//pars += '&quick=' + $(PARENT_SELECT) == null;
		var _request = new Ajax.Request(
			url, { method: 'get', parameters: pars, 
			onComplete: function(xhr){ 			
				new Insertion.After('wrap',xhr.responseText);		
				createPrintAdManagerPopup(null,null);			
				if(selectedValue == '0'){			//test if 'Select..' is selceted
					$(MANAGER_DELETE).style.visibility='hidden';
				}
			}
		});	
		return false;
	}
	function addPrintAdvertiser(count) {
	
		if( count >= MAX_ADVERTISERS){
			alert('You may only have '+MAX_ADVERTISERS+' print advertisers.\nPlease edit or delete to add more.');
			return;
		}
		$(MANAGER_SELECT).selectedIndex = 0;
	
		var elements = Form.getElements(MANAGER_FORM);
		var firstChecked = false;
		for(var i = 0; i < elements.length; i++){
			if(elements[i].type == 'text'){
				elements[i].value = '';
			}else if(elements[i].type == 'checkbox'){
				elements[i].checked = true;
			}else if(elements[i].type == 'radio'){
			
				if(!firstChecked){
					elements[i].checked = true;
					firstChecked = true;
				}else{
					elements[i].checked = false;
				}
				
			}
		}	
		editPrintAdvertiser(true);
	}
		function disableAll(form, doWhat){  //disable all except the select box for read only mode
			var elements = Form.getElements(form);
			var disableOptions = false;
			var OPTIONS_STRING = 'Options';
			var FIRSTLOOK_OPTIONS_ID = '5';		//this should always be 5 per backend guys, clicking the firstlook radio	causes the Options cb to be disabled
			for(var i = 0; i < elements.length; i++){			
				if(elements[i].id != MANAGER_SELECT){
					elements[i].disabled = doWhat;
					if(elements[i].type == 'radio' && elements[i].value== FIRSTLOOK_OPTIONS_ID && elements[i].checked ){
						 disableOptions = true;
						
					}
					if($(elements[i].id+'Label') && $(elements[i].id+'Label').innerHTML.indexOf(OPTIONS_STRING) != -1 && disableOptions){
								elements[i].checked = false;
								elements[i].disabled = true;
					}
					if(doWhat){		
						if($(elements[i].id+'Label') && elements[i].type=='checkbox'){
							Element.removeClassName($(elements[i].id+'Label'),'move');				
						}
					}else{
						if($(elements[i].id+'Label') && elements[i].type=='checkbox'){
							Element.addClassName($(elements[i].id+'Label'),'move');	
						}		
					}
				 }
			} //end for			
	}	
	function checkFirstlook(val){

		var OPTIONS_TEXT = 'Options';  //this should always be Options per backend guys, clicking the firstlook radio	causes the Options cb to be disabled
		var elements = Form.getElements(MANAGER_FORM);
		
		if(val){
			for(var i = 0; i < elements.length; i++){
		
				if($(elements[i].id+'Label') && elements[i].type=='checkbox'){
						if($(elements[i].id+'Label').innerHTML.indexOf(OPTIONS_TEXT) != -1){
							
						elements[i].checked = false;
						elements[i].disabled = true;
					}
				}		
			}	
		}else{
			for(var i = 0; i < elements.length; i++){
				if($(elements[i].id+'Label') && elements[i].type=='checkbox'){			
					if($(elements[i].id+'Label').innerHTML.indexOf(OPTIONS_TEXT) != -1){						
						elements[i].checked = true;
						elements[i].disabled = false;
					}
				}	
			}	
		}
	}
	
	function editPrintAdvertiser(adding) {  //handle enable/disable show/hide when in edit mode
		if($(MANAGER_SAVE_CANCEL)){
			$(MANAGER_SAVE_CANCEL).style.visibility = 'visible';
		}
		disableAll(MANAGER_FORM,false);
		if($(MANAGER_DELETE)){
			$(MANAGER_DELETE).style.visibility='hidden';
		}	
		$(MANAGER_SELECT).disabled = true;
		Sortable.create('dragList',{ghosting:false,constraint:'vertical'})
		
		if($(MANAGER_NAME_TEXT).value != ''){
			$(MANAGER_NAME_TEXT+'Label').className='ok';
			
		}else{		
			$(MANAGER_NAME_TEXT+'Label').className='req';
		}		
		$('printAdAddButton').style.display='none';
		$('dragItemsPrompt').style.visibility='visible';
		this.adding = adding;
	}
	function fixCurrentEvents(id){//convert un server saved events to others if the ad outlet has been deleted before plan save
	
		var eU = events.U;
		for(var i = 0; i <eU.length; i++){
			if(parseInt(eU[i].selectedPrintAdvertiserId) == id){
				eU[i].selectedPrintAdvertiserId = 'other';
				eU[i].otherPrintAdvertiserName = eU[i].thirdParty;
			}		
		}	
	}
	function deletePrintAdvertiser() {
		if (request != null) return;
		var numVehicles = parseInt($('adCountMgrMsg').innerHTML);
		if(numVehicles > 0){  //if there are vehicles using this ad prompt on delete
			var publicationName = $(MANAGER_SELECT).options[$(MANAGER_SELECT).selectedIndex].text;
			var confirmText = 'There';
			if (numVehicles == 1)
				confirmText += ' is ' + numVehicles + ' vehicle';
			else
				confirmText += ' are ' + numVehicles + ' vehicles';
				confirmText +=  ' for '+publicationName+' with current print advertisment plans.\n Are you sure you want to delete '+publicationName+'?';
				if(!confirm(confirmText)){
					return;
				}
		}
		var pars='?&';
		pars += Form.serialize(MANAGER_FORM);
		pars+='&deleteMe=true';
		
if(!isQuick) {
	this.fixCurrentEvents($(MANAGER_SELECT).value);
}
		if(pars.indexOf('showPrice=on') != -1){		//return true or false for show ad price
			pars = pars.replace('showPrice=on','showPrice=true');	
		}else{
			pars+='&showPrice=false';
		}

		
		var url = '/NextGen/PostPrintAdvertisingManager.go';
		var xLoc = $(MANAGER_ELE).style.left; //save window location because we create a new one
		var yLoc = $(MANAGER_ELE).style.top;
	
		request = new Ajax.Request(
			url, { method: 'post', parameters: pars, 
			onComplete: function(xhr){ 	
				request = null;
isUpdated = true;

				Element.remove($(MANAGER_ELE));
				new Insertion.After('wrap',xhr.responseText);		
				createPrintAdManagerPopup(xLoc,yLoc);
	synchOptions();
				$(MANAGER_DELETE).style.visibility='hidden';
				
			}
		});	
	}
	function cancelPrintAdvertiser() {
		if($(MANAGER_SAVE_CANCEL)){
			$(MANAGER_SAVE_CANCEL).style.visibility = 'hidden';
		}
		disableAll(MANAGER_FORM,true);
		if($(MANAGER_DELETE) && $(MANAGER_SELECT).value != '0'){
			$(MANAGER_DELETE).style.visibility='visible';
		}	
		$(MANAGER_SELECT).disabled = false;
		Sortable.destroy('dragList');
		$(MANAGER_NAME_TEXT+'Label').className='ok';
		$(MANAGER_ERRORS).style.display='none';
		$('printAdAddButton').style.display='block';
		$('dragItemsPrompt').style.visibility='hidden';
	}
	

	function populateOptions()  //populate the values on the ad manager form 
	{	
		var selectedValue = $F(MANAGER_SELECT);//document.forms['printAdvertiserForm'].selectedPrintAdvertiserId.value;
		var url = '/NextGen/OpenPrintAdvertisingManager.go';
		var pars = 'selectedPrintAdvertiserId='+selectedValue;
		var xLoc = $(MANAGER_ELE).style.left; //save window location
		var yLoc = $(MANAGER_ELE).style.top;
		var _request = new Ajax.Request(
			url, { method: 'get', parameters: pars, 
			onComplete: function(xhr){ 		
				Element.remove($(MANAGER_ELE));
				new Insertion.After('wrap',xhr.responseText);			
				createPrintAdManagerPopup(xLoc,yLoc);  //recreate the ad amanger popup			
				if(selectedValue == '0'){			
					$(MANAGER_DELETE).style.visibility='hidden';
				}else{
					$(MANAGER_DELETE).style.visibility='visible';
				}			
			}
		});	
	}
	//synch the options in the manager with those on the plan event
	function synchOptions(){
if(isQuick) {
	return;
}		
	
		 var fromOptions = $(MANAGER_SELECT).options;
		 var newOpt;
		 var option;
		 var formerSelectedValue = $(PARENT_SELECT).value;
		 var selectedText = $(PARENT_SELECT).options[$(PARENT_SELECT).selectedIndex].text == 'Other'?$('otherPrintAdvertiserText').value: $(PARENT_SELECT).options[$(PARENT_SELECT).selectedIndex].text;
		 var notFound = true;
		 
		 $(PARENT_SELECT).options.length  = 0;
		 for(var i = 0; i < fromOptions.length; i++){//iterate over options on ad manger, create new option elements, then add them to select on plan event
		 	newOpt =  document.createElement("<OPTION value='"+fromOptions[i].value+"' >");
		 	newOpt.text = fromOptions[i].text;
		 	if(newOpt.value == formerSelectedValue){
		 		newOpt.selected = true;	
		 		notFound = false;	 
		 	}
		 	$(PARENT_SELECT).add(newOpt);
		 }
		
		option = document.createElement("<OPTION value='' >"); //create separator
		option.text = '-----';
		$(PARENT_SELECT).add(option);
		option =  document.createElement("<OPTION value='other' >"); //create other, select it if the option has been removed
		option.text ='Other';
		option.selected = notFound;
		
		if(notFound){ //show other field if the option has been removed
			Element.hide('managePrintAdvertisersLink');
			Element.show($('otherPrintAdvertiserText'));
			$('formActionLinks').style.visibility ='hidden';	
			$('otherPrintAdvertiserText').value = selectedText;
		}
		$(PARENT_SELECT).add(option);		 
		 if( $(PARENT_SELECT).selectedIndex != 0){
	 		$(PARENT_SELECT+'Label').className = 'ok';
	 	 }else{
	 		$(PARENT_SELECT+'Label').className = 'req';
 		}
	}
	function getCenteredLocation(height, width) {
		var dims = Util.findViewportDims();				
		var offsetY = 0;
		if(window.pageYOffset ){
			 offsetY = window.pageYOffset;
		}else{
			 offsetY = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
		}
		var x = dims[0]/2 - width/2;
		var y = (dims[1]/2 - height/2)  + offsetY;
		return [x,y];
	}

	function createPrintAdManagerPopup(x,y){
			printAdManagerWindow = new Popup();			
var _posEl = isQuick ? 'quickAdManagerLink':'managePrintAdvertisersLink';
			printAdManagerWindow.popup_show(MANAGER_ELE,'printAdvertisingManagerHeader','','element-right',100,false,100,-350,_posEl);
			if(x == null && y == null){			
				var d = getCenteredLocation($(MANAGER_ELE).clientHeight,$(MANAGER_ELE).clientWidth);
				x = d[0];
				y = d[1];
			}	
			$(MANAGER_ELE).style.left = x;  //restore window location
			$(MANAGER_ELE).style.top = y;
				printAdManagerWindow.refresh();  //update clipping location to show falsely hidden select boxes		
			}	
	function savePrintAdvertiser(){
		// this means an ajax call has already been called, so don't do anything
		if (request != null) return;
		
		var dragItems = $('dragList').children;
		var newLocation = '0';
		var pars='?&';
		
		//form validation
		var errors = false;
		if(!Validate.checkRequired(MANAGER_NAME_TEXT)){
			if($(MANAGER_ERRORS).innerHTML != '' ){
				$(MANAGER_ERRORS).innerHTML = '';
			}
			$(MANAGER_ERRORS).innerHTML += '* <strong>'+$(MANAGER_NAME_TEXT+'Label').innerHTML.toUpperCase()+'</strong> is a required field';
			$(MANAGER_ERRORS).style.display='block';
			$(MANAGER_NAME_TEXT).focus();		
			$(MANAGER_NAME_TEXT+'Label').className='req';
			errors = true;
		}	
		if($('printAdvertiserEmailText').value != ''){
			if(!Validate.checkEMail('printAdvertiserEmailText')){
				if($(MANAGER_ERRORS).innerHTML != '' && errors){
					$(MANAGER_ERRORS).innerHTML +='</br>';
				}else{
					$(MANAGER_ERRORS).innerHTML ='';
				}
				$(MANAGER_ERRORS).innerHTML += '* <strong>'+$('printAdvertiserEmailTextLabel').innerHTML.toUpperCase()+'</strong> should be of the format "myname@mydomain.com"';
				$(MANAGER_ERRORS).style.display='block';
				$('printAdvertiserEmailText').focus();
				errors = true;
			}
		
		}
		if(errors)return;	
		pars += Form.serialize(MANAGER_FORM);
		for(var i=0; i < dragItems.length; i++){//save order of the options
			newLocation =  dragItems[i].id.substring(dragItems[i].id.indexOf('_')+1);
			pars+='&';
			pars+='adTextOptions['+newLocation+'].displayOrder='+i;		
		}	
		if(pars.indexOf('showPrice=on') != -1){		//return true or false for show ad price
			pars = pars.replace('showPrice=on','showPrice=true');	
		}else{
			pars+='&showPrice=false';
		}
		if(pars.indexOf('showQuickPlan=on') != -1){		//return true or false for show quick plan much <3 for checkboxes.
			pars = pars.replace('showQuickPlan=on','showQuickPlan=true');	
		}else{
			pars+='&showQuickPlan=false';
		}
		pars+='&selectedPrintAdvertiserId='+$(MANAGER_SELECT).value;
		var url = '/NextGen/PostPrintAdvertisingManager.go';
		var xLoc = $(MANAGER_ELE).style.left; //save window location
		var yLoc = $(MANAGER_ELE).style.top;
		
		request = new Ajax.Request(
			url, { method: 'post', parameters: pars, 
			onComplete: function(xhr){ 	
				request = null;
isUpdated = true;	
				Element.remove($(MANAGER_ELE));
				new Insertion.After('wrap',xhr.responseText);		
				createPrintAdManagerPopup(xLoc,yLoc);			
				if(this.adding){
					$(MANAGER_SELECT).selectedIndex = $(MANAGER_SELECT).options.length -1;
					this.adding = false;
				}		
				populateOptions();										
				synchOptions();								
			}
		});		
	}
}//end print ad manager class


