// resizes inventory overview scroll div to max view area
function resizeScroll(sPageType) {
	var aViewDims = findViewportDims(); //returns x=[0] and y=[1]
	var nViewHeight = aViewDims[1];
	if (nViewHeight >= 600) {
		var oScrollDiv = document.getElementById('scroll');
		var nAdjustHeightBy = sPageType == 'alpha' ? 293:366;
		if(document.all) { nAdjustHeightBy = nAdjustHeightBy - 19; }
		oScrollDiv.style.height = nViewHeight-nAdjustHeightBy + 'px';	
	}
}
function testExport( timePeriodMode, daysBack, thirdParty, endDate )
{
	var pars='timePeriodMode=' + timePeriodMode + '&daysBack=' + daysBack + '&thirdParty=' + thirdParty + '&endDate=' + endDate;
	var url = '/NextGen/ExportInServicePlan.go';
	var request = new Ajax.Request(
		 url, { method: 'get',
		  parameters: pars,
		  onComplete:function(xhr){	
			 new Insertion.Top('wrap', xhr.responseText);	
			  }		 
		   });	
}

function emailPopUp( thirdPartyId, reportType, thirdParty, endDate)
{
	var pars='thirdPartyId=' + thirdPartyId + '&reportType=' + reportType + '&thirdParty=' + thirdParty + '&endDate=' + endDate;
	var url = '/NextGen/OpenActionPlanEmail.go';
	var request = new Ajax.Request(
		 url, { method: 'get',
		  parameters: pars,
		  onComplete:function(xhr){		
		 	if($('emailPopup')){Element.remove($('emailPopup'));}
			  new Insertion.Top('wrap', xhr.responseText);			  	
			 	 var p = new Popup();
				 p.popup_show('emailPopup','emailPopupHeader','emailPopupCancelLink','screen-center',100,false,0,0);
				 Validate.toggleReq($('emailTo'));
				 Validate.toggleReq($('emailReplyTo'));
				 Validate.toggleReq($('emailSubject'));
			  }		 
		   });	
}

function errorMessageSetup(field, errorField){
	var errors = false;	
	
	if(!Validate.checkRequired(field) ){
		if($(errorField).innerHTML != ''){
			$(errorField).innerHTML+='</br>';
		}
		$(errorField).innerHTML += '*  <strong>'+$(field+'Label').innerHTML.toUpperCase()+'</strong> is a required field';
		errors = true;		
	}else{	
		if($(field).value != ''){
			if(!Validate.checkEMail(field)){
				if($(errorField).innerHTML != ''){
					$(errorField).innerHTML+='</br>';
				}
				$(errorField).innerHTML += '*  <strong>'+$(field+'Label').innerHTML.toUpperCase()+'</strong> must be of the format "myname@mydomain.com"';
				errors = true;			
			}		
		}		
	}
	return errors;
}


function validateEmailForm(){

	var errors = false;
	var messageVal  = false;
	
	//validate To
	$('emailErrors').innerHTML = '';
	if(!Validate.checkRequired('emailTo')){
		$('emailErrors').innerHTML += '*  <strong>'+$('emailToLabel').innerHTML+'</strong> is a required field';
		errors = true;		
	}else{
		var badEmails = Validate.checkEmails($('emailTo').value,',');
		for(var i = 0; i <badEmails.length; i++){
			if(i > 0){
				$('emailErrors').innerHTML +='</br>';
			}
			$('emailErrors').innerHTML += '* Address "<strong>'+badEmails[i]+'</strong>"  in the field <strong>'+$('emailToLabel').innerHTML.toUpperCase()+'</strong> should use the format "myname@mydomain.com"';	
			errors = true;		
		}
	}
	//validate reply to
	messageVal  = errorMessageSetup('emailReplyTo', 'emailErrors');
	errors = !messageVal?errors:messageVal;
	
	//validate subject
	if(!Validate.checkRequired('emailSubject')){
		if($('emailErrors').innerHTML !=''){
			$('emailErrors').innerHTML+='<br/>';
		}
		$('emailErrors').innerHTML += '* <strong>'+$('emailSubjectLabel').innerHTML.toUpperCase()+'</strong>  is a required field.';
		errors = true;		
	}
	if(errors){
		Element.show($('emailErrors'));
	}
	return errors;
}



function submitEmail()
{	
	if(!validateEmailForm()){
		var pars='?&';
		pars += Form.serialize('PrintAdvertiserEmailForm');	
		var url = '/NextGen/PostActionPlanEmail.go';		
		var request = new Ajax.Request(
		 url, { method: 'post', parameters: pars,
		 onComplete: function(xhr){ 	
		 	Element.remove($('emailPopup'));
		 } });	
		}
}


