var SelectBoxManager = Class.create();
SelectBoxManager.prototype = {
	initialize: function() {
		this.HOVER_CLASS    = 'over';
		this.INACTIVE_CLASS = 'select';
		this.ACTIVE_CLASS   = 'active';
		this.CUSTOM_CLASS   = 'custom';
		this.SELECTED_CLASS = 'selected';	
		this.ELEMENT_TAG    = 'ul';			
	},
	findItemWithClass: function(p, className) {   //accepts a parent element and class name
		if( !p ) {
			alert('Class:SelectBoxManager\nFunction:findSelected\nError: Invalid parent element');
			return false;
		}
		var c = p.childNodes;		
		for(var i = 0; i < c.length; i++){		
			if( c[i].className && Element.hasClassName(c[i],className)){
				return c[i];			
			}
		}
		return false;
	},
	resetToDefault: function( p ) {  //resets the select box to its default value (first item)
		if( !p ) {
			alert('Class:SelectBoxManager\nFunction:resetToDefault\nError: Invalid parent element');
			return false;
		}
		var k = p.childNodes;
		var ret = this.clickItem(k[0]);
			if(!ret ){
				this.hideParent(p);
			}
		return ret;
	},
	hover: function(e) {	//swap classes on hover, and hide the UL on mouse out, show it on mouse over
		if( !e ) {
			alert('Class:SelectBoxManager\nFunction:hover\nError: Invalid element');
			return false;
		}
		if( Element.hasClassName(e, this.HOVER_CLASS) ) {			
			Element.removeClassName(e,this.HOVER_CLASS);							
		}else {			
			Element.addClassName(e,this.HOVER_CLASS);				
		}		
	},
	hideParent: function(p) {	//hide the expanded UL (show only the selected item)
		if( !p ) {
			alert('Class:SelectBoxManager\nFunction:hideParent\nError: Invalid element');
			return false;
		}
		Element.removeClassName(p,this.ACTIVE_CLASS);		
		Element.addClassName(p,this.INACTIVE_CLASS);			
	},
	showParent: function(p) {  //show all items
		if( !p ) {
			alert('Class:SelectBoxManager\nFunction:showParent\nError: Invalid element');
			return false;
		}
		Element.removeClassName(p,this.INACTIVE_CLASS);
		Element.addClassName(p,this.ACTIVE_CLASS);		
	},
	swapParentClass: function(p) {  //toggle method for parent
		if( !p ) {
			alert('Class:SelectBoxManager\nFunction:showParent\nError: Invalid element');
			return false;				
		}
		if( Element.hasClassName(p,this.INACTIVE_CLASS) ) {	
			this.showParent(p);
			return false;				
		}else {
			this.hideParent(p);
			return true;				
		}
	},
	clickItem: function(e) {	//apply the selected class to an item upon click and hide the menu	
		if( !e ) {
			alert('Class:SelectBoxManager\nFunction:clickItem\nError: Invalid element');
			return false;
		}
		var p                 = e.parentNode;
		var currentlySelected = this.findItemWithClass( p, this.SELECTED_CLASS);
		var triggerAction     = this.swapParentClass( p );		
		Element.removeClassName(currentlySelected,this.SELECTED_CLASS);
		Element.addClassName(e, this.SELECTED_CLASS);		
		return triggerAction;
	}
};

/*****Begin Count Handler Class Definition******/
var VehicleListModel = Class.create();
VehicleListModel.prototype = {
	initialize: function(){
		//all vehicles
	 	this._vehicles =  new Array();
	 	//only visible vehicles
		this._visibleSet = new Array();	
		//current filter settings
		this._filters = { 
			age:new Array(), 
			risk:new Array(), 
			objective:new Array(), 
			mileage:null 
		};		
		//maps the class names applied to the vehicles to an attribute code
		this.classMap = {
			riskGroup_green          :'green',
			riskGroup_yellow         :'yellow',
			riskGroup_red            :'red',
			objectiveGroup_retail    :'retail',
			objectiveGroup_wholesale :'wholesale',
			objectiveGroup_sold      :'sold',
			objectiveGroup_other     :'other',
			mileageGroup_1			 :'1',
			mileageGroup_2           :'2',
			mileageGroup_3           :'3',
			mileageGroup_4           :'4',
			ageGroup_1				 :'1',
			ageGroup_2				 :'2',
			ageGroup_3				 :'3',
			ageGroup_4				 :'4',
			ageGroup_5				 :'5',
			ageGroup_6				 :'6'				
		};
		//maps the filter values from the drop downs to an attribute code 
		this.filterMap = {
			RISK_SHOW_GREEN          :'green',
			RISK_SHOW_YELLOW         :'yellow',
			RISK_SHOW_RED            :'red',
			OBJECTIVE_SHOW_RETAIL    :'retail',
			OBJECTIVE_SHOW_WHOLESALE :'wholesale',
			OBJECTIVE_SHOW_SOLD      :'sold',
			OBJECTIVE_SHOW_OTHER     :'other',
			MILEAGE_SHOW_RANGE_1     :'1',
			MILEAGE_SHOW_RANGE_2     :'2',
			MILEAGE_SHOW_RANGE_3     :'3',
			MILEAGE_SHOW_RANGE_4     :'4',
			AGE_SHOW_RANGE_1         :'1',
			AGE_SHOW_RANGE_2         :'2',
			AGE_SHOW_RANGE_3         :'3',
			AGE_SHOW_RANGE_4         :'4',
			AGE_SHOW_RANGE_5         :'5',
			AGE_SHOW_RANGE_6         :'6'				
		};
		//globals
		this.VEHICLE_AGE_CONTAINER      = 'ageGroup_'; //vehicle age groups common id
		this.VEHICLE_ITEM_CONTAINER     = 'vehicle';   //vehicle item common id
		this.RISK_TYPE      			= 'risk';	   //risk filter name
		this.OBJECTIVE_TYPE 			= 'objective'; //objective filter name
		this.MILEAGE_TYPE  				= 'mileage';   //mileage filter name
		this.AGE_TYPE       			= 'age';       //age filter name
		this.ALL						= '-2';        //all or general filter option (needs to be int to prevent class cast java exception)
		this.CUSTOM						= '-1';        //custom filter option
		this.EMPTY_CLASS				= 'emptyGroup';//class applied when no vehicles in an age group
		this.VEHICLE_ATTRIBUTES_CLASS   = 'classes';   //class name applied to hidden field containing vehicle attribute class name list
		this.VEHICLE_ATTRIBUTES_TYPE    = 'input';     //type of element containing the vehicle attribute classes
		this.TOTAL_VEHICLES				= 'VEHICLES_COUNT'; //element containing count of all vehicles
		this.FILTERED_TOTAL_ELEMENT		= 'tabFilteredTotal'; //total elements on tab
		this.CONTENT_ELEMENT			= 'content';
		this.SHOW_ADVANCED_FILTERS      = 'SHOW_ADVANCED_FILTERS';
		this.TOTAL_AGE_BUCKETS          = 6; //TODO: use these values to dynamically add the mileage and range filter and class mappings
		this.TOTAL_MILEAGE_BUCKETS      = 4;		
	},
	getFilterByTypeAndValue:function(type,value){ //returns a filter key 
		value = value.strip().toLowerCase();
		type = type.toLowerCase();
		if( value == this.ALL){
			return this.ALL;
		}
		if( value == this.CUSTOM){
			return this.CUSTOM;
		}				
		var t = $H(this.filterMap);				
		var filter = '';	
		t.each(function(i){
			if( i.key.toLowerCase().indexOf(type) != -1 && i.value == value){
				filter = i.key;
			}
		});		
		return filter;
	},
	getTotalVehicles:function() {
		var v = $(this.TOTAL_VEHICLES);		
		if(v){
			return v.value;
		}
		return false;
	},
	getFilterMapping: function(dropValue){ //returns the mapping value for the selected filter
		dropValue = dropValue.strip();
		if( dropValue.toLowerCase() == this.ALL ||
			dropValue.toLowerCase() == this.CUSTOM) {
			return dropValue.toLowerCase();
		}		
		return this.filterMap[dropValue];
	},
	getClassMapping: function(className){ //returns the mapping value for the selected class name	
		return this.classMap[className.strip()];
	},	
	resetAllFilters: function(){
		this._visibleSet = new Array();
		this._filters    = { age:new Array(), risk:new Array(), objective:new Array(), mileage:null };
		this.count();  //run count
	},
	resetFilter: function( which ){ 				   //clears the specified filter array
		this._visibleSet = new Array();		
		switch( which ){			
			case this.MILEAGE_TYPE:
				this._filters.mileage = null;
			break;
			default:	
				this._filters[which] = new Array();		
			break;
		}
	},
	loadVehicles: function(){	//load vehicles into model			
		var totalVehicles = this.getTotalVehicles();
		if(this._vehicles.length != totalVehicles){	//check if vehicles are already loaded	
			this._vehicles = new Array();						
			for( var j = 1; j <= totalVehicles; ++j ) {							
				this._vehicles.push( this.buildVehicle( $(this.VEHICLE_ITEM_CONTAINER+j), j ) );		
			}
		}		
		this.count(true);	//load current filters and perform count							
	},
	buildVehicle: function(e, iter){	//creates a vehicle object in the model	
		var ele        = this.getElementsByClassName(e,this.VEHICLE_ATTRIBUTES_TYPE,this.VEHICLE_ATTRIBUTES_CLASS); //hidden field containing classes (vehicle attributes)
		if(ele.length < 1){
			alert('Could not find vehicle attributes hidden field with type '+this.VEHICLE_ATTRIBUTES_TYPE+' and class '+this.VEHICLE_ATTRIBUTES_CLASS);
			return;
		}	
		var separator  = ',';	//char separating the vehicle classes					   
		var classArray = ele[0].value.split(separator);	
		//create a map representing this vehicle		
		var v = {
			id:		   classArray[0].strip(),
			risk:      this.getClassMapping(classArray[1]),
			objective: this.getClassMapping(classArray[2]),
			mileage:   this.getClassMapping(classArray[3]),
			age:       this.getClassMapping(classArray[4]),			
			iterator:  iter																		
		};	
		return v;	
	},
	checkVisible: function( element ){	//check if a vehicle object is visible based upon current filters		
		if( this._filters.age.length > 0 && !this._filters.age.include(element.age) ){		
			return false;
		}
		if( this._filters.risk.length > 0 && !this._filters.risk.include( element.risk ) ){		
			return false;
		}
		if( this._filters.objective.length > 0  && !this._filters.objective.include(element.objective )  ){		
			return false;
		}
		//include all vehicles with a mileage id less than or equal to that specified
		if( this._filters.mileage ){			
			var mileageGroup = element.mileage;			
			if( isNaN( mileageGroup ) || mileageGroup > parseInt( this._filters.mileage ) ){			
				return false;
			}
		}
		return true;
	},
	showAdvancedFilters: function() {
		if(!Element.hasClassName(this.CONTENT_ELEMENT,this.SHOW_ADVANCED_FILTERS)){
			Element.addClassName(this.CONTENT_ELEMENT,this.SHOW_ADVANCED_FILTERS);
		}
	},		
	addFilter: function(type, value ){	  //adds a new filter to the internal model		
		value = value.toString();
		type = type.toString();
		var value = this.getFilterMapping( value );			
		switch( type ){				
			case this.MILEAGE_TYPE:
				this._filters.mileage = value == this.ALL ? null : value;
				this.showAdvancedFilters();						
			break;		
			default:
				if(value && value != ''){
					if(value != this.ALL){				
						this._filters[type].push(  value );
						this.showAdvancedFilters();	
					}else{
						this.resetFilter(type);
					}
				}						
			break;
		}
	},
	isAllOrCustom:function(value) {
		if( value == this.CUSTOM || value == this.ALL ){
			return true;
		}
		return false;
	},
	loadDefaultCustomFilters: function(checks, type) {  //loads selected filters from check boxes
		var s         = null;
		var classStr  = '';
		var selectTag = '';
		for(var i = 0; i < checks.length; i++) {
			if(checks[i].checked) {
				classStr = this.getFilterByTypeAndValue(type,checks[i].value);				
				this.addFilter(type, classStr);  
				if( s == null ){ //display the custom checks
					s = new SelectBoxManager();
					selectTag = s.ELEMENT_TAG;		
					//shortcut, assume select el is 2 nodes up, if not search for it		
					if( checks[i].parentNode.parentNode.tagName.toLowerCase() == selectTag ) {
						Element.addClassName(checks[i].parentNode.parentNode,s.CUSTOM_CLASS);
					}else {
						var anc = checks[i].ancestors();
						var ff = function(f) {
							if(f.tagName.toLowerCase() == selectTag ) {
								return true;
							}
							return false;
						}
						var ele = anc.find(ff);						
						Element.addClassName(ele,s.CUSTOM_CLASS);											
					}
				}
			}		
		}		
	},
	loadDefaultFilters: function() {  //looks for values populated from backend
		//age is not always there,only replanning
		var ageValue      = $(this.AGE_TYPE+'_filter') ? $(this.AGE_TYPE+'_filter').value.strip() : '';
		if(!$(this.RISK_TYPE+'_filter'))   //safety check to make sure the filter selects were loaded
			return;		
		var riskValue     = $(this.RISK_TYPE+'_filter').value.strip();
		var form 		  = $(this.RISK_TYPE+'_filter').form;
		var objValue      = $(this.OBJECTIVE_TYPE+'_filter').value.strip();
		var mileageValue  = $(this.MILEAGE_TYPE+'_filter').value.strip();
		var commonChecks  = 'Custom';
	
		if( ageValue != '' && !this.isAllOrCustom(ageValue) ) {		
			this.addFilter(this.AGE_TYPE, this.getFilterByTypeAndValue(this.AGE_TYPE,ageValue));
		} else if(ageValue == this.CUSTOM) {		
			this.loadDefaultCustomFilters(form[this.AGE_TYPE+commonChecks], this.AGE_TYPE);
		}
		if( riskValue != '' && !this.isAllOrCustom(riskValue) ) {			
			this.addFilter(this.RISK_TYPE, this.getFilterByTypeAndValue(this.RISK_TYPE,riskValue));
		}else if( riskValue == this.CUSTOM){
			this.loadDefaultCustomFilters(form[this.RISK_TYPE+commonChecks], this.RISK_TYPE);
		}
		if( objValue != '' && !this.isAllOrCustom(objValue) ) {		
			this.addFilter(this.OBJECTIVE_TYPE, this.getFilterByTypeAndValue(this.OBJECTIVE_TYPE,objValue));
		} else if(objValue == this.OBJECTIVE_TYPE) {
			this.loadDefaultCustomFilters(form[this.OBJECTIVE_TYPE+commonChecks], this.OBJECTIVE_TYPE);
		}
		if( mileageValue != '' && !this.isAllOrCustom(objValue) )
			this.addFilter(this.MILEAGE_TYPE, this.getFilterByTypeAndValue(this.MILEAGE_TYPE,mileageValue));		
	},	
	countOneBucket: function(bucket){	//counts the number of visible vehicles in a bucket
		var id     = this.getClassMapping(bucket);
		var v      = this._visibleSet;
		var length = v.length;
		var count  = 0;		
		for( var i = 0; i < length; ++i ){
			if( v[i].age == id ){
				count++;				
			}
		}
		return count;		
	},
	countEachBucket: function(){  //counts each bucket		
		var length = this.TOTAL_AGE_BUCKETS;
		for( var i = 1; i <= length; ++i ){		
			if(!$(this.VEHICLE_AGE_CONTAINER+i)) {
				continue;
			}				
			var countDisplayElement = this.VEHICLE_AGE_CONTAINER+i+'FilteredTotal';				
			var total = this.countOneBucket( this.VEHICLE_AGE_CONTAINER+i );			
			if(!$(countDisplayElement)){
				alert('Could not find element with id: '+countDisplayElement);
			}
			Element.update(countDisplayElement , total );			
			if( total < 1 ){
				Element.addClassName(this.VEHICLE_AGE_CONTAINER+i, this.EMPTY_CLASS);					
			}else{			
				Element.removeClassName(this.VEHICLE_AGE_CONTAINER+i, this.EMPTY_CLASS);
			}			
		}	
	},	
	count: function(loadDefaults){     //count total visible and set the count total element's value
		if(loadDefaults){
			this.loadDefaultFilters();  
		} 		
		overallTotal = this.findTotalVisible();			
		Element.update(this.FILTERED_TOTAL_ELEMENT,overallTotal);	
		this.countEachBucket();		
	},	
	findTotalVisible: function() {	//check each vehicle for visibility and return total
		var v   		 = this._vehicles;  
		this._visibleSet = new Array();  //clear out old visible set					
		//place visible vehicles in an array under index 0
		var sep = v.partition(
			function(item) {
				return vehicleListModel.checkVisible(item);
			}		
		);
		this._visibleSet = sep[0];
		return this._visibleSet.length;
	},
	findVehicleByAttribute:function( key, attribute ){ //returns the vehicle object based upon inventory id, false if not found
		var v       = this._vehicles;
		var loc = function(i) {
				if(i[attribute] == key) {
					return true;
				}
				return false;
			}
		return v.find(loc);		
	},
	isVehicleVisible: function( val ){ //checks whether the specified inventory id or iterator is visible based upon filter settings
		var vehicle = this.findVehicleByAttribute( val, 'id' ); //check by id first
		if(!vehicle){
			vehicle = this.findVehicleByAttribute( val, 'iterator' );  
			if(!vehicle){
				alert('Vehicle object with id or iterator of '+val+' was not found.');
				return false;
			}
		}		
		return this.checkVisible( vehicle );
	},
	getElementsByClassName:function (parent, tag, className){//get all elements with specified class name and tag under the specified parent
	    var allEles  = parent.getElementsByTagName(tag);
	    var retEles  = new Array();
	    className    = className.replace(/\-/g, "\\-");
	    var rx       = new RegExp("(^|\\s)" + className + "(\\s|$)");
	    var e;
	    for(var i=0; i<allEles.length; i++){
	        e = allEles[i];      
	        if(rx.test(e.className)){
	            retEles.push(e);
	        }   
	    }
	    return (retEles)
	}
};
