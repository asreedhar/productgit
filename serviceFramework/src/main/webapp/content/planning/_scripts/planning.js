
/*
UPDATE content only
*/
function updateInventoryPlan(form) {
	var _f = $(form.id);
	var url = _f.action;
	var pars = Form.serialize(_f);
	var enab = getEnabledParam();
	if(enab != '')	pars += '&enabled='+enab;	
	var xhr = new Ajax.Updater(
		'content', url, {
			method: 'post',
			parameters: pars,
			onComplete: function() {
				register();
				vehicleListModel.loadVehicles()
			}
	
	});
}

/*
UPDATE single vehicle only
*/
function updateVehicle(invId,iter,overrideUpdate) {
	if (vehicle == '' || vehicle == null) {
		vehicle = new Vehicle(invId, iter);
	}
	var matchRangeId = document.location.href.match(/rangeId=\d/);
	var pars = '&overrideUpdate=' + (overrideUpdate == true);
		pars += vehicle.getUpdateParams();	
	if (matchRangeId && matchRangeId.length > 0) {
		pars += '&' + matchRangeId[0];
	};
    var xhr = new Ajax.Updater(
		vehicle.el,
		vehicle.url, { 
			method: 'get', 
			parameters: pars,
			asynchronous: false,
			onComplete: function () {
				vehicle = null;
				page.reset();
			}
		});	
}

/*
LIST PRICE editing 
	(at vehicle level)
*/

function showInlineReprice(iter) { //dblclick event
	if($(page.planLayer) != null) return;
	if(quick != null && quick.plan != 'reprice') return;
	var reprice = new Reprice(iter);
	reprice.switchToEdit();
}

function hideInlineReprice(iter) {
	var reprice = new Reprice(iter);
	reprice.reset();
	resetConfirm(reprice.inputEl);
	reprice.switchToDisplay();
}

function submitInlineReprice(iter,skipValid,url) {
	if(Element.visible('repriceThresholdMsg')) {
		skipValid = true;
	}
	var reprice = new Reprice(iter);
	if(!(reprice.validate(skipValid))) {
		if(Element.visible('confirmBox')) {
			setConfirmHiddenFields('reprice',reprice.iter,true);
		}
		reprice.focusInput();
		return false;
	}
	reprice.post(url);
	reprice.switchToDisplay();
	var newPrice = formatPrice(reprice.inputValue);
    updatePlanCounts(iter);
	Element.update(reprice.displayEl,Format.currency(newPrice));
	resetConfirm(reprice.inputEl);
	if(page.isQuick()) {
		reprice.moveFocus();
	}
	return false;
}

/*
TRANSFER PRICE editing 
	(at vehicle level)
*/

function showInlineTransfer(iter) { //dblclick event
	if($(page.planLayer) != null) return;
	if(quick != null && quick.plan != 'transfer') return;
	var transfer = new Transfer(iter);
	transfer.switchToEdit();
}

function hideInlineTransfer(iter) {
	var transfer = new Transfer(iter);
	transfer.reset();
	resetConfirm(transfer.inputEl);
	transfer.switchToDisplay();
}

function submitInlineTransfer(iter,skipValid,url) {
	if(Element.visible('repriceThresholdMsg')) {
		skipValid = true;
	}

	var transfer = new Transfer(iter);
	if(!(transfer.validate(skipValid))) {
		if(Element.visible('confirmBox')) {
			setConfirmHiddenFields('transfer', transfer.iter,true);
		}
		transfer.focusInput();
		return false;
	}
	transfer.post(url);
	if(transfer.passedThresholdCheck() == true) {
		transfer.switchToDisplay();
		Element.update(transfer.displayEl,Format.currency(formatPrice(transfer.inputValue)));
	    updatePlanCounts(iter);
		if(page.isQuick()) {
			transfer.moveFocus();
		}
	}
	return false;
}

function showAdPriceMessage(val,adEle){
	if($(adEle) == null || $('adPriceMsg') == null) return;

	var listPrice = formatPrice($('listPriceDisplay').innerHTML.stripTags());
	if(val != '$' && val != '' && $F(adEle) != listPrice){

var _msg = $('adPriceMsg');
Element.show(_msg);
Position.clone('adPrice',_msg, { setWidth: false, offsetLeft: -75, offsetTop: 14 });
		setTimeout(removeAdPriceMessage, 15000);
	}
}

//
// REPRICE CONFIRMATION

function confirmReprice(skipValid){
	var reprice = new Reprice();
	reprice.inputEl = 'vehReprice'; //override
	reprice.formObj = 'VehicleRepriceForm';
	var confirmed = skipValid === true;
	var valid = reprice.validate(confirmed);
	if(!valid) {

		if(Element.visible('confirmBox')) {
			setConfirmHiddenFields('reprice', reprice.iter,true);
		}
		reprice.focusInput();
		return false;
	}
	resetConfirm(reprice.inputEl);
	return true;
}

function handleRepriceConfirm(e,iter,url) {
	if(event.keyCode == Event.KEY_RETURN) { // ENTER
		if(validatePriceValue(e.value) === false) {
			resetConfirm($(e.id));
			e.value = e.defaultValue;
			return false;
		}	
		if(Element.visible('repriceThresholdMsg')) {
			submitInlineReprice(iter,true,url);
		} else {
			submitInlineReprice(iter,false,url);
		}
		return false;
	}
	if(event.keyCode == Event.KEY_ESC && Element.visible('confirmBox')) {
		resetConfirm($(e.id));
		return true;
	}
	if(event.keyCode == Event.KEY_TAB && e.value != e.defaultValue ) {
		if(validatePriceValue(e.value) === false || Element.visible('confirmBox')) {
			resetConfirm($(e.id));
			e.value = e.defaultValue;
			return true;
		}
		showConfirmReprice(e.id,iter);
		return false;
	}
	return true;
}


function showConfirmReprice(input,iter) {
	setConfirmHiddenFields('reprice', iter,false);
	var inputEl = $(input);
	var inputPos = Position.cumulativeOffset(inputEl);
	var pop = $('confirmBox');
	Element.show(pop);
	pop.style.left = (inputPos[0] - 25)+'px';
	pop.style.top = inputPos[1]+'px';
setTimeout(removeConfirm, 5000);
}

function cancelRepriceConfirm(goToNext) {
	var iter = $F('repriceInputIter');
	var reprice = new Reprice(iter);
	if(iter == '' && $('vehReprice') != null) {
		reprice.inputEl = 'vehReprice'; //override
		reprice.formObj = 'VehicleRepriceForm';
	}	
	reprice.reset();
	if(!goToNext) reprice.focusInput();
	resetConfirm(reprice.inputEl);
}

// TRANSFER CONFIRM

function confirmTransfer(skipValid){
	var transfer = new Transfer();
	transfer.inputEl = 'vehTransfer'; //override
	transfer.formObj = 'VehicleTransferForm';
	var confirmed = skipValid === true;
	var valid = transfer.validate(confirmed);
	if(!valid) {

		if(Element.visible('confirmBox')) {
			setConfirmHiddenFields('reprice', reprice.iter,true);
		}
		transfer.focusInput();
		return false;
	} else {
		transfer.post();
		if (transfer.passedThresholdCheck() == false) {
			return false;
		}
	}
	resetConfirm(transfer.inputEl);
	return true;
}

function handleTransferConfirm(e,iter,url) {
	if(event.keyCode == Event.KEY_RETURN) { // ENTER
		if(validatePriceValue(e.value) === false) {
			resetConfirm($(e.id));
			e.value = e.defaultValue;
			return false;
		}	
		if(Element.visible('transferThresholdMsg')) {
			submitInlineTransfer(iter,true,url);
		} else {
			submitInlineTransfer(iter,false,url);
		}
		return false;
	}
	if(event.keyCode == Event.KEY_ESC && Element.visible('confirmBox')) {
		resetConfirm($(e.id));
		return true;
	}
	if(event.keyCode == Event.KEY_TAB && e.value != e.defaultValue ) {
		if(validatePriceValue(e.value) === false || Element.visible('confirmBox')) {
			resetConfirm($(e.id));
			e.value = e.defaultValue;
			return true;
		}
		showConfirmTransfer(e.id,iter);
		return false;
	}
		return true;
}

function showConfirmTransfer(input,iter) {
	setConfirmHiddenFields('transfer', iter,false);
	var inputEl = $(input);
	var inputPos = Position.cumulativeOffset(inputEl);
	var pop = $('confirmBox');
	Element.show(pop);

	pop.style.left = (inputPos[0] - 25)+'px';
	pop.style.top = inputPos[1]+'px';
setTimeout(removeConfirm, 5000);
}

function cancelTransferConfirm(goToNext) {

	var iter = $F('transferInputIter');
	var transfer = new Transfer(iter);
	if(iter == '' && $('vehTransfer') != null) {
		transfer.inputEl = 'vehTransfer'; //override
		transfer.formObj = 'VehicleTransferForm';
	}	
	transfer.reset();
	resetConfirm(transfer.inputEl);
}



function resetConfirm(input) {
	if(!Element.visible('confirmBox')) return;
	Element.removeClassName(input,'red');
	removeConfirm();
}

function removeConfirm() {
	Element.hide('transferConfirmMsg');
	Element.hide('transferThresholdMsg');
	Element.hide('repriceConfirmMsg');
	Element.hide('repriceThresholdMsg');
	$('transferInputIter').value = '';
	$('repriceInputIter').value = '';
	Element.hide($('confirmBox'));
	clearTimeout(removeConfirm);
}

function submitRepriceConfirm(url) {
	var iter = $F('repriceInputIter');
	if(iter == '' && $('vehReprice') != null) {
		plan.setRepriceConfirmed(true);

		resetConfirm($('vehReprice'));
		return;
	}
	
	submitInlineReprice(iter,true,url);
}

function setConfirmHiddenFields(field,iter,thresh) {
	if(!thresh) {
		Element.show(field + 'ConfirmMsg');
		Element.hide(field + 'ThresholdMsg');
	} else {
		Element.show(field + 'ThresholdMsg');
		Element.hide(field + 'ConfirmMsg');
	}
	$(field + 'InputIter').value = iter;
}

function setTransferMessage(message) {
	$('transferThresholdWarning').innerHTML = message;
}



/*  VEHICLE PLAN Open-Edit-Save-Close FUNCTIONS
 *-----------------------------------------------------*/

// OPEN		////////

function openVehiclePlan(e,invId,iter) {
	
	if(page.planActivated) return; //prevents multiple requests
	page.planActivated = true;
	vehicle = new Vehicle(invId,iter);
	vehicle.toggleLoadingText();	
	var url = checkSlash(e.pathname);
	var pars = stampURL(removeQ(e.search));
	var req = new Ajax.Request(	
		url, { 
			method: 'get', 
			parameters: pars,
			onSuccess: function(xhr){ 
				vehicle.toggleLoadingText();
				Element.addClassName(vehicle.table, 'PLAN_OPEN');
		
				page.toggleShade();
				var quickRepriceState = page.getQuickReprice();
				disableQuickReprice();
				if (page.setQuickReprice) page.setQuickReprice(quickRepriceState);
			},
			onComplete: function(xhr){
				var _html = xhr.responseText;
				var obj = $('vehDesc'+ iter);
				var pos = Position.cumulativeOffset(obj);
				var target = $('box');

				Position.clone(obj,target, { setWidth: false, offsetLeft: -42, offsetTop: 15 } );
				Element.update('inner',_html);
				//document.getElementById('inner').innerHTML = _html;
				Element.show(target);

				var _json = xhr.getResponseHeader('X-JSON');

				var _planObj = eval('('+_json+')');	

				vehicle.populateInsights(); //pulled from initial display
				
				plan = new Plan(_planObj);
				plan.populate();
				events = new Events();

				events.current(); //builds HTML for editable events fields (i.e. advertise, lot promote, auction, & wholesaler)
				events.degroup();
				
				//populate lists of channels
				advertisers = _planObj.advertisers;
				wholesalers = _planObj.wholesalers;
				auctions    = _planObj.auctions;
var planBottom = target.clientHeight + Position.cumulativeOffset(target)[1];  
var windowBottom = document.documentElement.scrollHeight;
				if( planBottom >= windowBottom ){
	//plan is hanging off bottom of page, move it above item						
					if(Element.hasClassName(target,'pointer_up')) {
						Element.removeClassName(target,'pointer_up');
						Element.addClassName(target,'pointer_down');
					}
					Position.clone(obj,target, { setWidth: false, offsetLeft: -38, offsetTop: -(target.clientHeight)} );
				}else {				
					if(Element.hasClassName(target,'pointer_down')){
						Element.addClassName(target,'pointer_up');
						Element.removeClassName(target,'pointer_down');
					}

					Position.clone(obj,target, { setWidth: false, offsetLeft: -42, offsetTop: 15 } );
				}
				//center element on screen	
				var scroll = new fx.Scroll();
				var offset = ((document.documentElement.clientHeight) / 2) - (0.5 * target.clientHeight); //center vertically on screen
				scroll.scrollTo(target,offset);
				
			},
			onFailure: function() {
				page.toggleShade();	
				page.reset();
				vehicle.toggleLoadingText();
				restoreQuickReprice();
			}
		});
}

// CANCEL	////////////////////////////////////////////////////////
function cancelPlan() {
	if($('newInternetAuction')){
		updateVehicle(vehicle.id,vehicle.iter); //update vehicle to show internet auctions was posted
	}
	_removePlan(false,true);
	page.triggerQuickRepriceSavedState();
}

function _removePlan(isSaveNext,overrideUpdate){
	page.toggleShade();
	resetConfirm();
	restoreQuickReprice();
	Element.removeClassName(vehicle.table,'PLAN_OPEN');	
	page.removeLayers();
	Element.hide('box');
	var next = vehicle.getNextLink(); //need this after clearing objects
	if (page.repriced || bPageReload == true) { // checks estock card flag to reload summaries
		updateVehicle(vehicle.id, vehicle.iter, overrideUpdate);
	}
	else {
		page.reset();
	}
	vehicle = null; plan = null; events = null; // CLEAR ALL OBJECTS
	if(isSaveNext) {
		$(next).click();		
	}
}

// SAVE		////////////////////////////////////////////

function savePlanAndContinue() { 
	savePlan(true);
}
function savePlan(goToNext) {
	var reprice = false;
	var isSaveNext = goToNext || false;
	vehicle.disableButtons();
	
	var repricedConfirmed = plan.getRepriceConfirmed();
	if(!plan.validate()) {
		vehicle.enableButtons();
		return;
	}
	if(plan.usesTransferPricing() == true && $F('vehTransfer') != '$') {
		if(!confirmTransfer()) {
			vehicle.enableButtons();
			return;	
		}
	} 
	if($F('vehReprice') != '$' && !repricedConfirmed) {
		reprice = confirmReprice();
		if(reprice == false) {
			vehicle.enableButtons();
			return;	
		}
	}

	_finishSavePlan((reprice || repricedConfirmed),isSaveNext);

	if (!isSaveNext) page.triggerQuickRepriceSavedState();
}

function _finishSavePlan(repriceConfirmed,isSaveNext){
	var strats = ['retail','wholesale','sold','other'];
	var selObjective = events.objective;
	strats.each(function(s) {
		if(s != selObjective) {	
			Element.remove(s); //remove NON-relevant form fields
		}
	});	
	plan.objective = events.objective; //set the plan objective
	S = new Send(vehicle.id,plan.objective,plan.originalObjective); //create a send queue

	if(repriceConfirmed) { 
		S.reprice = formatPrice($F('vehReprice'));	
		S.repriceConfirmed = true;
	}
	if(plan.usesTransferPricing()) {
		S.transferForRetailOnly = $('transferForRetailOnly').checked ? true : false;
	}
	if(plan.objective!='sold'||plan.objective!='other') {
	//organize and determine which fields need to be sent to the server
		events.regroup();			
		events.cleanNewAndUpdated();
		//set the sent queue to the events update queue		
		S.U = events.U;
	}
	if(plan.objective=='sold'||plan.objective=='other') {			
		S.U = {};
	}
	S.D = events.D;
	S.N = events.N;
	S.defaultDescription = events.defaultDescription
	plan.gather();
	if($('internetAdvertisingMinimizedForm')){
		var ia = new InternetAdvertising();
		ia.update('internetAdvertisingMinimizedForm');	
		S.internetAdvertising = ia;
	}

	S.adPrice = Util.getValue('adPriceEditText');
	var jsonString = S.toJSONString();
	var url = removeQ($('VehiclePlanForm').action);
	var pars = '&json='+jsonString;
	pars += vehicle.getUpdateParams();
		var update = new Ajax.Updater(
			vehicle.el, url , { 
				method: 'post', 
				parameters: stampURL(pars),
				onComplete: function(xhr) {
					//alert(xhr.responseText)
				}
		});

	updatePlanCounts(vehicle.iter);
	_removePlan(isSaveNext);
}
