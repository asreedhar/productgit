
/*
STANDARD PLANNING FILTERS
*/

function handleTimeRange(arg1,arg2,arg3,sel) {  //needs empty args to fit interface

	sel = $F(sel);
	
	var container;
		container = $('timeRangeFilterOptions');
		dueSelect = $('dueTimeRangeFilterSelect');
		plannedSelect = $('plannedTimeRangeFilterSelect');
	if(sel == 0) {
		Element.hide(container);
		disableTimeRange(dueSelect);
		disableTimeRange(plannedSelect);
		return;
	} else {
		Element.show(container);
		var enabEl = sel == 2 ? dueSelect:plannedSelect;
		var disabEl = sel == 2 ? plannedSelect:dueSelect;
		Element.show(enabEl);
		enableTimeRange(enabEl);
		Element.hide(disabEl);
		disableTimeRange(disabEl);
	}
}
function disableTimeRange(el) {
	//el.setAttribute('disabled','disabled');
}
function enableTimeRange(el) {
//	el.removeAttribute('disabled');
}



/*
ADVANCED FILTERS
*/
function enableAdvancedFilters() {
	var targetEl = $('content');
	Element.addClassName(targetEl,'SHOW_ADVANCED_FILTERS');
}

function disableAdvancedFilters(parentEl) {
	var targetEl = $('content');
	
	var selects = $(parentEl).getElementsByTagName('ul');
	var s = new SelectBoxManager();
	$A(selects).each(function(select) {
		s.resetToDefault(select);
	});

	var checks = $(parentEl).getElementsByTagName('input');
	$A(checks).each(function(check,i) {
		if(check.checked) {
			check.checked = false;
		}
	});

// reset 'content' classes
	var classStr = '_SHOW_';
	removeSpecificClasses(targetEl,classStr);
	Element.removeClassName(targetEl,'SHOW_ADVANCED_FILTERS');
	vehicleListModel.resetAllFilters();
	
	

}
function applyCustomFilters(e,w) {
	var generalId        = 'Custom';
	var general_hiddenId = '_filter';
	var selectBoxType	 = 'ul';
	var count = handleCustomFilter(e,w);  //returns the number of checked check boxes
	var customFilterBox = $(w+generalId); //locate the custom filter box
	var hiddenField     = $(w+general_hiddenId); //locate the hidden field its related to
	
	var ulCandidates    = Element.siblings($(hiddenField));  //get the sibling nodes of the hidden field
	var customSelect;
	for( var i = 0; i < ulCandidates.length; i++) {      //locate the UL item within the siblings, its the custom select box
		if(ulCandidates[i].tagName.toLowerCase() == selectBoxType) {
			customSelect = ulCandidates[i];
			break;
		}
	}	
	var s = new SelectBoxManager();
	var item = s.findItemWithClass(customSelect,s.CUSTOM_CLASS);  // find the list item with the custom class
	s.removeCustomClass(item);
	if( count < 1){
		s.resetToDefault(customSelect);		//if nothing checked, reset the select box to the default value
	}
	Element.hide(customFilterBox);
}

function setCustomOptionsDisplay(type,customSelected, item) {
	var customEl;
	customEl     = item.parentNode;	
	var noCustom = !customEl;
	var s        = new SelectBoxManager();
	if(noCustom) { //mileage has no custom options
		return;
	}
	if(customSelected) {		
		s.swapParentClass(customEl); // reshow the UL (its automatically hidden on click), keep open on custom click
		Element.addClassName(customEl, s.CUSTOM_CLASS);
	}
	else {		
		Element.removeClassName(customEl, s.CUSTOM_CLASS);		
	}
}

function resetCustomFilterOptions(e,which) {
	var f = e.form;
	var checks;
	if(which == vehicleListModel.RISK_TYPE) {
		checks = f.riskCustom;
	}
	if(which == vehicleListModel.OBJECTIVE_TYPE) {
		checks = f.objectiveCustom;
	}
	if(which == vehicleListModel.AGE_TYPE) {
		checks = f.ageCustom;
	}
	$A(checks).each(function(check,i) {
		if(check.checked) {
			check.checked = false;
		}
	});
}

function selectMouseOver(e, event) {
	var s = new SelectBoxManager();
	//the checkboxes stop the onmouseover event for the UL item, this checks 
	//if the item the mouse is coming from or going to is a checkbox, if so then 
	//dont hide the selection list
	var addTarget           = event.relatedTarget ? event.relatedTarget : event.toElement;  //related target for w3c compliance, toElement for IE
	var fromTarget          = event.relatedTarget ? event.relatedTarget : event.fromElement;
	var toElementType       = '';
	var fromElementType     = '';
	var CUSTOM_ELEMENT_TYPE = 'checkbox';
	//check if the to or from elements are null
	if(addTarget) {
		toElementType = addTarget.type;
	}
	if(fromTarget) {
		fromElementType = fromTarget.type;
	}
	
	Position.prepare();	
	if(!Position.within(e, Event.pointerX(event),Event.pointerY(event))      &&  //check if mouse is over the select element (UL)
		Element.hasClassName(e,s.ACTIVE_CLASS)   &&  //make sure list is open already
		toElementType != CUSTOM_ELEMENT_TYPE     &&  
		fromElementType != CUSTOM_ELEMENT_TYPE ) {			
		s.hideParent(e);
	}
}

var _customChanged = false;

function toggleCustomChanged() {
	_customChanged = true;
}

function handleCustomFilter(e,which) {
	
	
	if(!e) var e = window.event;

	var tg = (window.event) ? e.srcElement : e.target;
	if (tg.nodeName != 'UL') return;

	var numChecked = 0;
	if (_customChanged) {
		

		var type     = which;
	// reset 'content' classes
		var targetEl = $('content');
		var classPrefix = type.toUpperCase();
		removeSpecificClasses(targetEl,classPrefix);
	
		// var f = e.form;
		var f = $('InventoryPlanningForm');
		
		var checks;
		if(which == vehicleListModel.RISK_TYPE) {
			checks = f.riskCustom;
		}
		if(which == vehicleListModel.OBJECTIVE_TYPE) {
			checks = f.objectiveCustom;
		}
		if(which == vehicleListModel.AGE_TYPE) {
			checks = f.ageCustom;
		}
			
		vehicleListModel.resetFilter(which);
		
		$A(checks).each(function(check,i) {
			if(check.checked) {
				numChecked++;
				var classStr = vehicleListModel.getFilterByTypeAndValue(which,check.value);
			
				//get count value then add it to next count value etc
				vehicleListModel.addFilter(which, classStr);   //add filters to count handler
				Element.addClassName(targetEl,classStr);
			}
		});
		vehicleListModel.count();  //perform count	
		
		_customChanged = false;
	}
	return numChecked;
	
}


function handleAdvancedFilter(item,arg2,arg3, e) {

	e = $(e);
	var type = e.name;
	var sel  = vehicleListModel.getFilterByTypeAndValue(type,e.value);
	
// custom options
	var customSelected = (sel.toLowerCase() == vehicleListModel.CUSTOM);
	setCustomOptionsDisplay(type,customSelected, item);
	
// reset 'content' classes
	var targetEl = $('content');
	
	var classPrefix = type.toUpperCase();
	removeSpecificClasses(targetEl,classPrefix);

	vehicleListModel.resetFilter(type);  //clear out the existing filters for this type
	if (customSelected) {
		vehicleListModel.count(); //resets the counts when custom is selected
		return;
	}
	vehicleListModel.addFilter(type, sel);  //add the filter to the number handler
	vehicleListModel.count();  //perform the count
	resetCustomFilterOptions(e,type);

// 'all' selected
	if(sel.toLowerCase() == vehicleListModel.ALL) {
			return;
	}
// add class
	
	Element.addClassName(targetEl,sel);

}
function removeSpecificClasses(targetEl,searchStr) {
	var targetClasses = targetEl.className;
	var classesArray = targetClasses.split(' ');
	if(classesArray.length > 1) {
		var cleanedArray = classesArray.collect(function(str) {
			if(str.search(searchStr) == -1) return str;
		});
		targetEl.className = cleanedArray.join(' ');
	}
}

function handleCustomSelect(item,type,value, hiddenId, callback){ //need a custom call back to perform action...
	
	var selectBoxManager = new SelectBoxManager();
	var execute = selectBoxManager.clickItem( item );

	if(execute){
		if( $(hiddenId) ){		
			$(hiddenId).value = value;	
			if(callback){	
								
				callback(item, type, value, hiddenId); //perform callback method method must fit interface(item, type,value, hiddenId)	
			}
		}else{
			alert('Error:Could not find hidden field, or no id specified ("'+hiddenId+'")');
		}		
	}
	
}
function handleCustomSelectHover(item){
	var selectBoxManager = new SelectBoxManager();
	selectBoxManager.hover( item );
}
/***STOCK NUMBER SEARCH***/
function highlightVehicle(e) {
	e = $(e);	
	if(e) {  	
		var SELECTED_CLASS = 'selected';
		var VEHICLE_ITEM_TAG = 'table';		
		var anc = e.ancestors();		
		var ff = function(i) {
			if(i.tagName.toLowerCase() == VEHICLE_ITEM_TAG){
				return true;
			}
			return false;
		}
		parentTable = anc.find(ff);	
		if(!Element.hasClassName(parentTable,SELECTED_CLASS)){
			Element.addClassName(parentTable,SELECTED_CLASS);
		}			
		e.onblur = function() {
			Element.removeClassName(parentTable,SELECTED_CLASS);
			e.onblur = null;
		};
	}
}

function locateStockNumber(id) {
	var COMMON = 'stock';
	var stockNumber = $(id).value.strip().toUpperCase();
	var AJAX_URL = 'SearchStockNumber.go?stockNumber=';
	var val = COMMON+stockNumber;
	var anchors = document.anchors;
	var errorEl = 'searchStockError';
	var anchor = null;
	var ff = function(a) {
		if(a.name == val){
			return true;
		}
		return false;
	}
	anchor = $A(anchors).find(ff);
	
		if(anchor) {   
		  //found on this tab
		  	try {		
				anchor.focus();	   //calls highlight vehicle
			}catch(e) {  //element is hidden, show then focus
				disableAdvancedFilters('advancedFilterGroup');
			}
			anchor.focus();	  
			Element.scrollTo(anchor);	
			$(id).value = '';	
			return;
		}  
	
	new Ajax.Request(
			AJAX_URL+stockNumber,
			{
				method:'get',
				onComplete: function(x) {
					var rangeId = x.getResponseHeader('rangeId');					
					if(parseInt(rangeId) >= 0){			
						var _conf = confirmDirty();
						if(_conf) {
							document.location.href = document.location.pathname+'?rangeId='+rangeId+'#'+val;
							$(id).value = '';	
						}
					}else{
						Element.update(errorEl,'No vehicles found');
						Element.addClassName(id,'red');
						$(id).onkeydown = function() {
							Element.removeClassName(id,'red');
							Element.update(errorEl, '');
							$(id).onkeydown = null;
						}
					}
				
				}
			}		
		);		
		
}



