

//MANAGER OPTIONS CLASS
//Used to maintain state for the update third party popup.

function ManagerOptions(){

	//private vars
	var options;
	var eventType;
	var popup;
	var parentSelect;
	var simpleOptions = false;
	var preventFix = false;
	
	//expose functions to the outside world
	this.setOptions = setOptions;
	this.getOptions = getOptions;
	this.loadOptions = loadOptions;
	this.removeOption = removeOption;
	this.addOption = addOption;
	this.save = save;
	this.setEventType = setEventType;
	this.getEventType = getEventType;
	this.setPopup = setPopup;
	this.setParent = setParent;
	this.disableParent = disableParent;
	this.populateParent = populateParent;
	this.loadOptionsFromSelect = loadOptionsFromSelect;
	this.setSimpleOptions = setSimpleOptions;
	this.preventEventFix = preventEventFix;
	
	//accepts an array of objects with fields: description and id
	function setOptions(opts){		
			options = sortOptions( opts );	
	}
	function loadOptionsFromSelect(select_id) {
		var opts = $(select_id).options;
		var length = opts.length;
		options = new Array();
		
		for(var i = 0; i < length; i++){
	
			if(i > 0){  //skip default option
				options.push({id:opts[i].value,description:opts[i].text});
			}
		}
	
	}
	//sorts an array of option maps
	function sortOptions(opts){
	
		opts.sort(function(a,b) {
			var c = a.description.toLowerCase(), d = b.description.toLowerCase();
			if (c > d) return 1;
			if (c < d) return -1;
			return 0;
		});
			
		return opts;
	}
	//sets the event type for server interaction (auction, wholesale)
	function setEventType(evt) {	
	
			eventType = evt;		
	}	
	function getEventType(){
	
		return eventType;
	}
	function getOptions(){
	
		return options;
	}
	//sets a handle to the popup management window
	function setPopup(p){
		popup = p;
		
	}
	function preventEventFix(s) {
		preventFix = s;
	}
	function setSimpleOptions(s) {	
		simpleOptions = s;
	}
	
	//loads the options into the select with id selectId
	function loadOptions(selectId) {
		var opt;
	
		for(var i = 0; i < options.length; i++){
		
				opt = document.createElement("<OPTION value='"+options[i].id+"'>");
				opt.text= options[i].description;
				$(selectId).add(opt);
				
		}
		$(selectId).selectedIndex = 0; //default to first item
	}
	
	function addOption(selectId, value, showError){
	
			var d = new Date();
			//adds a new option to select
			if( value=='' && showError ){
				alert('Option is a required field.');
				return;
			}else if( value=='' && !showError){  //submit clicked with empty value
				return;
			}			
			$(selectId).add(createOption('new_'+d.getTime(),value)); //add option to left select, time is so its unique
					
			var ordered = new Array();
			for(var i = 0; i < $(selectId).options.length; i++){  //convert select to array of option value pairs
			
					ordered.push(createMap($(selectId).options[i].value,$(selectId).options[i].text));
			}	
			ordered = sortOptions(ordered);  //order the array
			
			$(selectId).options.length = 0;  //clear the options
			
			for(var i = 0; i < ordered.length; i++){  //reset the select box
			
				$(selectId).add(createOption(ordered[i].dbId, ordered[i].description, false));
				if(value == ordered[i].description){
					$(selectId).selectedIndex = i;   //set the selected index to the newly added item
				}
			}		
	
	}
	//removes an option with the specified value from the select box
	function removeOption(selectId, value){
	
		var opts = $(selectId).options;
		
		if(value == null || value==''){
			if( opts.length == 0){
				alert('There are no options to remove.');
			}else{
				alert('Please select an option to remove.');
			}
			
			return;
		}	
		
		for(var i  = 0; i < opts.length; i++){
	
			if(opts[i].value == value ){	
				opts[i] = null; //remove option		
				if( i == opts.length ){							
					$(selectId).selectedIndex = i-1;
				}else{
					$(selectId).selectedIndex = i;
				}			
				break;				
			}
		}
		
	}
	
	//creates an object with fields dbId, description, and comments (not used) for server interaction
	function createMap(value, text){	
		
			return 	{
							dbId: value,
							description: text,			
							comments:''
						};	
	}
	
	//sets the parent select element (third party select, auction select etc...)
	function setParent(p){
	
			parentSelect = p;
	
	}	
	function disableParent(b){	
		if(b == false){		
			$(parentSelect).selectedIndex = 0;
		}	
		$(parentSelect).disabled = b;		
	}
	//private method to create a new option
	function createOption(value, text,  selected){
		
		var sel = '';
		if(selected){
			sel = 'selected';
		}
		var opt = document.createElement("<OPTION value='"+value+"' "+sel+">");
		opt.text = text;
		
		return opt;
	
	}
	//verify that the object has what it needs to work properly (for debugging purposes)
	function checkRequired(){
	
		var start = 'The following errors have occured in ManagerOptions:\n';
		var msg = start;
		
		if(parentSelect == null || !$(parentSelect)){
		
			msg+='-a parent select box was not specified.\n';
		}
		if(eventType == null){
		
			msg+='-an eventType was not specified.\n';
		}
		if(popup == null){		
		
			msg+='-a popup window object was not specified.\n';
		
		}		
		if(msg != start){
		
					alert(msg);
					return false;
		}
		return true;
	}
	function fixCurrentEvents(id){	//convert un server saved events to others if the thirdparty has been deleted before plan save
		var eU = events.U;	
		for(var i = 0; i <eU.length; i++){
			if(parseInt(eU[i].thirdPartyId) == id){
				eU[i].thirdPartyId = 'other';
				if(eU[i].type=='auction'){
					eU[i].nameOther = eU[i].auctionName;
				}else if(eU[i].type=='wholesaler'){				
						eU[i].nameOther = eU[i].wholesalerName;
				}
			}		
		}	
	
	}
	//save changes
	function save(selectId){
	
		if(!checkRequired()){
			return;
		}
		var changes  = new Object();
		changes.add = new Array();
		changes.remove = new Array();
		var selectedItem = '';
		var found = false;
	
		if( $(selectId).options.length > 0){
	
			 selectedItem = $(selectId).options[$(selectId).selectedIndex].text;
		}
	
	
		//check for newly added items and add to changes.add
		for(var i = 0; i < $(selectId).options.length; i++){
		//locate newly added items and push them on the changes add stack
			if($(selectId).options[i].value.indexOf('new_') != -1){
				changes.add.push(createMap('',$(selectId).options[i].text));
			}			
		}		
		//check for items that no longer exist and add to changes.remove
	    for(var j = 0; j < options.length; j++){
	    	for(var i = 0; i < $(selectId).options.length; i++){	    
		    		if($(selectId).options[i].value == options[j].id){		    	
		    			
							found = true;
							break;					
					}						
	    	}	    	
	    	if(!found){										
						changes.remove.push(createMap(options[j].id, options[j].description));	
						if(!preventFix)
							fixCurrentEvents(options[j].id);											
			}
		    found = false;
	    }

			//take all removed items out of stored options array ( had to do separate loop because splice causes array index to be off)
			for( var i = 0; i < changes.remove.length; i++){
			
				  for(var j = 0; j < options.length; j++){
					  if(options[j].id == changes.remove[i].dbId){
					  
						  options.splice(j,1);
						  break;
					  }
				  }
			
			}
					
		var jsonString = changes.toJSONString();		
		var pars = '&json='+ jsonString + '&eventType=' + eventType;

		var planRequest = new Ajax.Request(
			'/NextGen/SaveThirdParty.tx', { asynchronous:false, method: 'post', parameters: stampURL(pars), 
			onComplete: function(xhr){ 				
					var p = $(parentSelect).options;
					p.length = 0;  //reset parent select box to have no options
					var newOptions = eval( '(' + xhr.getResponseHeader( 'X-JSON' ) + ')' );  //get new options from server with ids					 
					options = sortOptions( options.concat(newOptions) ); //add newly added items to options array												
					populateParent(selectedItem);	
				
			}
		});
		
		//When quick planning we need to call the onChange for the drop down.
		var firstChar = eventType.charAt(0);
		firstChar = firstChar.toUpperCase();
		var updateId = 'quick'+firstChar+eventType.substring(1,eventType.length)+'Id';
		if($(updateId)) {
			document.getElementById(updateId).onchange();
		}	
		
	}
	//private method to populate the parent select
	function populateParent(selectedItem){

	    	selectedItem = selectedItem?selectedItem:'';
			var index = 0;
			$(parentSelect).options.length = 0;
			$(parentSelect).add(createOption('Unspecified','Unspecified'));
			if(!simpleOptions)
				$(parentSelect).add(createOption('','----------'));
			for(var i = 0; i < options.length; i++){
						if(options[i].description == selectedItem){
							index = simpleOptions ? i + 1 : i + 2;
						}					
						$(parentSelect).add(createOption(options[i].id,options[i].description));
			}
			if(!simpleOptions)
				$(parentSelect).add(createOption('','----------'));
			if(!simpleOptions)
				$(parentSelect).add(createOption('other','Other'));
			if(!simpleOptions)
				$(parentSelect).add(createOption('manage','Add/Manage'));
			disableParent(false); //enable the parent select
			$(parentSelect).selectedIndex = index;
			if(	$(parentSelect).value != ''){			
				$($(parentSelect).id+'Label').className = 'ok';
			}
	}
}
//END MANAGER OPTIONS CLASS
