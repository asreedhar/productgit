<tiles:importAttribute/>
<script type="text/javascript" language="javascript">
document.title = "Inventory Management Plan";
</script>
<c:import url="/common/googleAnalytics.jsp" />
<c:set var="isQuickAdPage" value="false" scope="request"/>
<c:if test="${pageId eq 'planPrint'}" var="isPrintPage" scope="request" />
<c:if test="${fn:length(bucketKeySet) eq 0}" var="isEmptyResults" scope="request" />
<c:set var="enabled" value="${param.enabled}" scope="request" />

<c:if test="${not isPrintPage}">
	<c:import url="/common/_box.jspf" />
<input style="display:none;" type="hidden" value="${quickRepriceEnabled}" id="quickRepriceEnabled" />
<input style="display:none;" type="hidden" value="${priceThreshold}" id="repriceThreshold" />
<input style="display:none;" type="hidden" value="${confirmReprice}" id="confirmReprice" />
</c:if>

<c:if test="${not isPrintPage}">
	<c:import url="/content/planning/_searchByStockNumber.jspf" />
	<c:import url="/content/planning/_plannedAndDueCounts.jspf" />
	<c:import url="/content/planning/_tabs.jspf" />
	<c:import url="/content/planning/_layers/confirmDialog.jspf" />

	<c:if test="${quickRepriceEnabled}">
		<c:set var="_itemsClassStr" value="REPRICE_ENABLED" />
	</c:if>
</c:if>

<c:if test="${not empty enabled}">
	<c:set var="_itemsClassStr" value="${quickRepriceEnabled and enabled eq 'reprice' ? 'REPRICE_ENABLED ':''}SHOW_QUICK_${fn:toUpperCase(enabled)}" />
</c:if>

<c:if test="${isPrintPage}">
<script language="javascript">
Event.observe(window, 'load', function() {
	var links = document.body.getElementsByTagName('A');
	
	// unintuitive behavior alert! this nodelist is live - so it decreases by one
	// every time we remove an element with replace(). that's why we keep hitting the [0]
	// element - basically we're popping'em off the stack.
	for(var i = links.length - 1; i >= 0; i = i - 1) {
		Element.extend(links[0]).replace(links[0].innerHTML);
	}
});
</script>
</c:if>
<c:if test="${hasTransferPricing}">
<script type="text/javascript" src="content/planning/_scripts/VehicleTransfer.js"></script>
</c:if>
<div id="items" class="${_itemsClassStr}">
	<div id="displayControls">
		<html:form action="InventoryPlan" styleId="InventoryPlanningForm">
			<html:hidden property="rangeId" value="${param.rangeId}" />
			<div id="filters">
			
				<c:if test="${showLotLocationAndStatus}">
					<c:import url="/content/dealers/longo/inventory-statusFilter.jspf">
						<c:param name="showButton" value="${not showFilter}" />
					</c:import>
					
				</c:if>	

				<c:if test="${showFilter}">
<c:import url="/content/planning/_filterByReplanningStatus.jspf" />
				</c:if>
				<c:if test="${not isEmptyResults}">
<c:import url="/content/planning/_filterByVehicleProperties.jspf" />
				</c:if>
			</div>
		</html:form>
		<c:if test="${not isEmptyResults}">
			<c:import url="/content/planning/_quickPlanning.jsp"/>
		</c:if>
	</div>
	<c:import url="/content/planning/_vehicles.jsp" />

</div>

