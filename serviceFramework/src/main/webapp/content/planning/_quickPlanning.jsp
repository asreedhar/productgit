<jsp:useBean id="params" class="java.util.HashMap"/><c:set property="rangeId" value="${rangeId}" target="${params}" />
<c:set var="_tableClassStr">
	<c:if test="${param.enabled eq 'spiff' or param.enabled eq 'reprice' or param.enabled eq 'advertise'}">SHOW_RETAIL</c:if>
	<c:if test="${param.enabled eq 'auction' or param.enabled eq 'wholesaler'}">SHOW_WHOLESALE</c:if>
</c:set>

<div id="quick">
	<table border="0" cellpadding="0" cellspacing="0" id="quickPlanTable" class="${_tableClassStr}">
		<tr>
			<th class="tl">&nbsp;</th>
			<th class="l">&nbsp;</th>
			<td><h4><a href="javascript:;" onclick="return toggleQuickPlan();">Quick Plan</a></h4></td>
			<th class="tr">&nbsp;</th>
		</tr>
		<tbody class="choices">
		<tr>
			<td class="l" colspan="2">
				<div id="quickPlanRetail">
					<div class="icons">
					
						<c:if test="${hasTransferPricing}">
						<c:set property="enabled" value="transfer" target="${params}" />
						<html:link name="params" action="InventoryPlan" 
							onclick="return enableQuickPlan(this,'transfer',true);" styleId="transferLink" title="Quick Transfer Repricing"><img src="<c:url value="/common/_images/icons/transfer-25x25.gif" />" class="icon" /></html:link>
						<tt>&bull;</tt>	
						</c:if>
						<c:set property="enabled" value="reprice" target="${params}" />
						<html:link name="params" action="InventoryPlan" 
							onclick="return enableQuickPlan(this,'reprice',true);" styleId="repriceLink" title="Quick Repricing"><img src="<c:url value="/common/_images/icons/reprice-25x25.gif" />" class="icon" /></html:link>
						<tt>&bull;</tt>			
						<c:set property="enabled" value="spiff" target="${params}" />
						<html:link name="params" action="InventoryPlan" 
							onclick="return enableQuickPlan(this,'spiff',${not isQuickAdPage});" styleId="spiffLink" title="SPIFF Quick Planning"><img src="<c:url value="/common/_images/icons/spiff-25x25.gif" />" class="icon" /></html:link>
						<tt>&bull;</tt>
						<c:set property="enabled" value="advertise" target="${params}" />
						<html:link name="params" action="QuickAdInventoryPlan" 
							onclick="return enableQuickPlan(this,'advertise',${isQuickAdPage});" styleId="advertiseLink" title="Advertising Quick Planning"><img src="<c:url value="/common/_images/icons/advertise-25x25.gif" />" class="icon" /></html:link>				
						</div>
						<div class="labels">
						<c:choose>
						<c:when test="${hasTransferPricing}">
							<span class="first">Transfer<tt>&bull;</tt></span>
							<span class="mid">Reprice<tt>&bull;</tt></span>
						</c:when>
						<c:otherwise>
							<span class="first">Reprice<tt>&bull;</tt></span>
						</c:otherwise>
						</c:choose>
							<span class="mid">SPIFF</span>
							<span class="last"><tt>&bull;</tt>Advertise</span>
						</div>
				</div>
				<div id="quickPlanWholesale">
					<div class="icons">
						<c:set property="enabled" value="auction" target="${params}" />
						<html:link name="params" action="InventoryPlan" 
							onclick="return enableQuickPlan(this,'auction',${not isQuickAdPage});" styleId="auctionLink" title="Auction Quick Planning"><img src="<c:url value="/common/_images/icons/auction-25x25.gif" />" class="icon" /></html:link>
						<tt>|</tt>
						<c:set property="enabled" value="wholesaler" target="${params}" />
						<html:link name="params" action="InventoryPlan" 
							onclick="return enableQuickPlan(this,'wholesaler',${not isQuickAdPage});" styleId="wholesalerLink" title="Wholesaler Quick Planning"><img src="<c:url value="/common/_images/icons/wholesaler-25x25.gif" />" class="icon" /></html:link>
					</div>
					<div class="labels">
						<span class="first">Auction</span>
						<span class="last"><tt>&bull;</tt>Wholesaler</span>
					</div>
				</div>
			</td>
			<td class="r" colspan="2" rowspan="2">
				<div class="objectives">
					<a href="javascript:;" onclick="return showQuickPlanOptions('retail');" class="retailLink">Retail</a>
					<a href="javascript:;" onclick="return showQuickPlanOptions('wholesale');" class="wholesaleLink">Wholesale</a>
				</div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="l">
				<div class="apply">
<c:if test="${hasTransferPricing}">
					<c:import url="/content/planning/_includes/transfer.jspf">
						<c:param name="level" value="page" />
					</c:import>
</c:if>
					<c:import url="/content/planning/_includes/reprice.jspf">
						<c:param name="level" value="page" />
					</c:import>
					<c:choose>
						<c:when test="${isQuickAdPage}">
							<c:import url="/IncludeAdvertisingQuickPlanOverview.go">
								<c:param name="level" value="page" />
							</c:import>
						</c:when>
						<c:otherwise>
							<c:import url="/content/planning/_includes/spiff.jspf">
								<c:param name="level" value="page" />
							</c:import>
							<c:import url="/content/planning/_includes/auction.jspf">
								<c:param name="level" value="page" />
							</c:import>
							<c:import url="/content/planning/_includes/wholesaler.jspf">
								<c:param name="level" value="page" />
							</c:import>
						</c:otherwise>
					</c:choose>
				</div>
			</td>
		</tr>
		</tbody>	
		<tr>
			<th class="bl">&nbsp;</th>
			<th class="l">&nbsp;</th>
			<th>&nbsp;</th>
			<th class="br">&nbsp;</th>
		</tr>
	</table>
</div>