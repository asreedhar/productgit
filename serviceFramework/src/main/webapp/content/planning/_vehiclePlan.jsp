<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:importAttribute scope="request"/>

<script type="text/javascript">
	//add data contextual to the modal here
	//note: window necessary at this point because something is breaking globals between script tags in the same JSP
	window.vehiclePlanData = {
		isClosedGroupAuction:${requestScope.isClosedGroupAuction}
	}
	//console.log('window.vehiclePlanData.isClosedGroupAuction set to: ' + window.vehiclePlanData.isClosedGroupAuction);
	//Defining func here to capture correct data and fire it at end of JSP
	function setClosedGroupAuctionStatus(){
		var currentPlans = $('currentEvents').getElementsByTagName('ul')[0], //fragile DOM selection - improve post-cleanup
		//removes all but the first LI so change this if we want more than one update message after current plan
		msg = (currentPlans.innerHTML.split(/<\/li>/i).shift() + '</li>');
		if(window.vehiclePlanData.isClosedGroupAuction) {
			msg = currentPlans.innerHTML +  '<li><strong>Vehicle has been set to Closed Group Auction</strong></li>';
			currentPlans.innerHTML = msg;
		}
	}
	window.setClosedGroupAuctionStatus = setClosedGroupAuctionStatus;
</script>

<%--CONSTANT--%><c:set var="PLAN_TYPES" value="retail,wholesale,sold,other"/>
<%--vars--%>
<c:set var="isQuickAdPage" value="${param.isQuickAdPage}" />
<c:set var="isLast" value="${param.isLast}" />
<c:set var="nInvId" value="${param.inventoryId}" scope="request"/>
<c:set var="age" value="${param.AgeInDays}" scope="request"/>
<c:set var="index" value="${param.index}" scope="request"/>

<fmt:message key="l.notes" var="sNotes" scope="request"/>

<%--<c:set var="nObjectiveId" value="${currentObjectiveId}" scope="page"/>--%>
<c:set var="active" value="${currentObjectiveId==4?'other':currentObjectiveId==3?'sold':currentObjectiveId==2?'wholesale':'retail'}" scope="request"/>
<c:if test="${!empty repriceEvents}" var="bHasReprice"/>
<%--/vars--%>
<c:set var="activeAds" value="${activeAds}" scope="request"/>
<c:set var="displayTMV" value="${displayTMV}" scope="request"/>
<c:set var="bHasInternetAd" value="${hasInternetAdvertisers}" scope="request"/>
<c:set var="adPriceValue" value="${adPriceValue}" scope="request"/>

<!-- PLANNING SPACE -->
<div id="vehiclePlan">

	<tiles:insert attribute="details"/>

	
	<div id="approach">
<input type="hidden" name="activeObjectiveType" value="${active}" id="activeObjectiveType" readonly="readonly"/>
<input type="hidden" name="defaultEndDate" value="" id="defaultEndDate"/>
<input type="hidden" name="defaultBeginDate" value="" id="defaultBeginDate"/>

	<tiles:insert attribute="current"/>
<!-- objective -->
<div class="objective">
	<h5><fmt:message key="plan.objective"/>:</h5>
	<div class="elements">
 		<c:if test="${showPlanHistory}"><a href="javascript:pop('<c:url value="/PopPlanningHistory.view"><c:param name="inventoryId" value="${inventoryId}"/></c:url>','popup')" class="historyLink"><fmt:message key="plan.clickForPlanHistory"/></a></c:if>
		<fieldset>
		<c:forEach items="${PLAN_TYPES}" var="objective">
			<input type="radio" name="approach" value="${objective}" id="${objective}Approach" onclick="switchObjective('${objective}');"${active eq objective?' checked="checked"':''}/><label for="${objective}Approach"><fmt:message key="l.${objective}"/></label>
		</c:forEach>
		</fieldset>
	</div>		
</div>
<!-- /objective -->
<!-- strategies -->
<div class="strategies">
	<!-- ???????????? --><span id="eventErrors" class="errors"></span>
	<div id="plans">
<%-- NOTE: this FLOAT cannot be nested inside a FORM --%>
<div id="planFloat" class="float" style="display:none;"></div>
		<%-- this form holds plan fields NOT explicitly associated with a plan strategy
				** those FORM elements are populated inside the above FLOAT when requested --%>
		<html:form action="/Post${isQuickAdPage ? 'QuickAd':''}VehiclePlan" onsubmit="return false;" styleId="VehiclePlanForm">
			<div class="finish">
				<label for="nextPlanDate"><fmt:message key="l.nextPlanDate"/></label>
				<input type="text" tabindex="7" name="nextPlanDate" onkeypress="if(event.keyCode ==13)return false;"id="nextPlanDate" title="MM/DD/YYYY" value="" class="date" onblur="setAge('nextPlanDate','ageAtReplanDate','<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>','${age}');"  ondblclick="return showCalendarByPointer('nextPlanDate', '%m/%d/%Y', false, true,false,'<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>');"/>
				<a href="#"><img src="<c:url value="/common/_images/icons/calendar.gif"/>" class="cal" onclick="return showCalendarByPointer('nextPlanDate', '%m/%d/%Y', false, true,false, '<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>');" /></a><br/>
<ins><fmt:message key="plan.msg.replanAtAge"><fmt:param><strong>${age}</strong></fmt:param><fmt:param><input type="text" tabindex="8" onblur="setCalendarDate('nextPlanDate','ageAtReplanDate','<fmt:formatDate value="${TODAY}" pattern="MM/dd/yyyy"/>','${age}');" value="${daysUntilReplanning + age}" id="ageAtReplanDate" name="ageAtReplanDate" onclick="this.select();" onkeypress="return checkChars();" maxlength="3" class="shortDays"/></fmt:param></fmt:message></ins>
				<div class="saveBtn" id="saveAndCloseLink">
<input type="image" src="<c:url value="/common/_images/buttons/saveAndCloseOnWhite.gif"/>" name="savePlan" id="savePlanBtn" onclick="savePlan();return false;"/>
<c:if test="${not isLast}">
<input type="image" src="<c:url value="/common/_images/buttons/saveAndNextOnWhite.gif"/>" name="savePlanNext" id="savePlanNextBtn" onclick="savePlanAndContinue();return false;"/>
</c:if>
				</div>
			</div>
			<c:forEach items="${PLAN_TYPES}" var="type"><c:set var="type" value="${type}" scope="request"/>
				<%-- THIS CONTAINER is used by switchObjective() --%>
				<span id="${type}"${type eq active ? '':' style="display:none;"'}>
					<h5><c:choose><c:when test="${type eq 'retail' or type eq 'wholesale'}"><fmt:message key="plan.newPlanStrategies"/></c:when><c:when test="${type eq 'sold'}"><fmt:message key="plan.saleInformation"/></c:when><c:when test="${type eq 'other'}">${sNotes}</c:when></c:choose>:</h5>
					<div class="elements">
				<tiles:insert attribute="types"/>
					</div>
				</span>
			</c:forEach>
		</html:form>
	</div>
</div>
<!-- /strategies -->


	</div>
	


</div>
<script type="text/javascript">
//func defined at top of JSP. Clumsy kludge for clumsy JSP fragmentation
setClosedGroupAuctionStatus();
</script>
<!-- /PLANNING SPACE -->