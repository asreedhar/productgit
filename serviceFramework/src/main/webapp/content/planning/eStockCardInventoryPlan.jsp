
<c:set var="vehicle" value="${singleInventory}" scope="request" />
<%@ include file="_includes/config_planItems.jspf" %>



<div id="planningSummarys" class="display">

<c:if test="${!isActive}">
	<!-- want to hard code state flags to prevent 'due for replanning message' for inactive inventory items -->
	<c:set var="bDueForReplanning" value="false" scope="request" />
</c:if>
<c:import url="/content/planning/vehicle_strategySummary.jspf" />


<c:if test="${!(bNeverPlanned and not hasInternetAdvertisers)}">

	<span class="nextPlanDate">
		<c:if test="${isActive}">  
			Next plan date: ${vehicle.planReminderDate}
		</c:if>
	</span>
	<a href="#" id="viewPlanHistory" onclick="javascript:Element.toggle('detail')">View plan history</a>
</c:if>

<div id="detail" style="display:none">
<c:import url="/content/planning/vehicle_strategySummary_expanded.jspf" />
</div>

</div>