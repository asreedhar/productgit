<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<%-- HISTORY POPUP --%>
<c:set var="vehicle" value="${inventoryItem.vehicle}"/>
	<div class="title">
		<h3>${vehicle.year} ${vehicle.makeModelGrouping.make} ${vehicle.makeModelGrouping.model}</h3> <h5>VIN: ${vehicle.vin} Stock#: ${inventoryItem.stockNumber}</h5>
	</div>
		<display:table  id="row" name="${planningHistory}" cellpadding="0" cellspacing="0" defaultsort="11" defaultorder="descending" class="data">
		<display:setProperty name="css.tr.odd" value="" />
		<display:setProperty name="css.tr.even" value="" />
		<display:setProperty name="basic.msg.empty_list"><p class="none">-</p></display:setProperty>
			<display:column title="End Date" sortable="false">
				<fmt:formatDate value="${row.planningEventType.id == 18?row.beginDate:row.endDate}" pattern="EEE, d MMM yyyy"/>
			</display:column>
			<display:column property="beginDate" decorator="biz.firstlook.main.display.util.DateNoTimeDecorator" title="Begin Date" sortable="false" />
			<display:column property="lastModified" decorator="biz.firstlook.main.display.util.DateNoTimeDecorator" title="Last Modified Date" sortable="false" />
			<display:column property="objective" title="Objective" sortable="false" />
			<display:column property="planningEventType.description" title="Strategy" sortable="false" />
			<display:column title="Description" sortable="false">
				<c:choose>
					<c:when test="${row.planningEventType.id == 6 || row.planningEventType.id == 7}">
							<c:out value="${!empty row.inventoryThirdParty ? row.inventoryThirdParty.name:row.notes3} - ${row.notes} ${!empty row.notes2?'-':'' } ${row.notes2}"/>
					</c:when>
					<c:when test="${row.planningEventType.id == 5}">
						<c:if test="${row.value - row.notes2 == 0}">
							Price confirmed
						</c:if>
						<c:if test="${row.value - row.notes2 != 0}">
							Repriced to <fmt:formatNumber type="currency" maxFractionDigits="0"><c:out value="${row.value}"/></fmt:formatNumber>
						</c:if>
					</c:when>
					<c:when test="${row.planningEventType.id == 11 || row.planningEventType.id == 12}">
							<fmt:formatDate value="${row.userBeginDate}" pattern="MM/dd/yyyy"/>  ${ !empty row.userEndDate?' to ':''}<fmt:formatDate value="${row.userEndDate}" pattern="MM/dd/yyyy"/> ${!empty row.notes && !empty row.userBeginDate?' - ':''}${row.notes}
					</c:when>
					<c:when test="${row.planningEventType.id == 3 }">
							<c:out value="${!empty row.inventoryThirdParty ? row.inventoryThirdParty.name:row.notes2}:  ${row.notes} "/>
					</c:when>
					<c:when test="${row.planningEventType.id == 18 }">
							<c:out value="${row.inventoryThirdParty.name}${!empty row.notes?':':'' }  ${row.notes} "/>
					</c:when>
					<c:otherwise>
							<c:out value="${row.notes} ${!empty row.notes2?' - ':''}${row.notes2}"/>
					</c:otherwise>
				</c:choose>
			</display:column>
		</display:table>
<%-- /HISTORY POPUP --%>	

	
	