<tiles:importAttribute/>
<c:set var="isQuickAdPage" value="true" scope="request"/>

<c:if test="${fn:length(bucketKeySet) eq 0}" var="isEmptyResults" scope="request" />
<c:set var="enabled" value="${empty param.enabled ? 'advertise':param.enabled}" scope="request" />

<c:import url="/common/_box.jspf" />
<input style="display:none;" type="hidden" value="${quickRepriceEnabled}" id="quickRepriceEnabled" />
<input style="display:none;" type="hidden" value="${priceThreshold}" id="repriceThreshold" />
<input style="display:none;" type="hidden" value="${confirmReprice}" id="confirmReprice" />

<c:import url="/content/planning/_searchByStockNumber.jspf" />
<c:import url="/content/planning/_plannedAndDueCounts.jspf" />
<c:import url="/content/planning/_tabs.jspf" />
<c:import url="/content/planning/_layers/confirmDialog.jspf" />

<c:if test="${not empty enabled}">
	<c:set var="_itemsClassStr" value="${quickRepriceEnabled and enabled eq 'reprice' ? 'REPRICE_ENABLED ':''}SHOW_QUICK_${fn:toUpperCase(enabled)}" />
</c:if>

<div id="items" class="${_itemsClassStr}">
	<html:form action="SaveQuickAdvertise" onsubmit="return submitQuickAdPlan(this,'${rangeId}');">
		<div id="displayControls">
		

		
			<div id="filters">
<c:if test="${showLotLocationAndStatus}">
					<c:import url="/content/dealers/longo/inventory-statusFilter.jspf">
						<c:param name="showButton" value="${not showFilter}" />
					</c:import>
</c:if>
				<c:if test="${showFilter}">
<c:import url="/content/planning/_filterByReplanningStatus.jspf" />
				</c:if>
				<c:if test="${not isEmptyResults}">
<c:import url="/content/planning/_filterByVehicleProperties.jspf" />
				</c:if>
			</div>
			<br clear="all" />
			<c:import url="/content/planning/_quickPlanning.jsp">
				<c:param name="enabled" value="${enabled}" />
			</c:import>


		</div>
		<c:import url="/content/planning/_vehicles.jsp">
			<c:param name="enabled" value="${enabled}" />
		</c:import>
	</html:form>
</div>


