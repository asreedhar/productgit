<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%--PAGE VARS //--%><c:set var="sBookName" value="${guidebookName}" scope="page"/><c:set var="sBookValueType" value="${guidebookCategoryDesc}" scope="page"/><c:set var="nBookValuePercent" value="${percentageMultiplier}" scope="page"/><c:set var="nBookCategory" value="${guidebookCategoryId}" scope="page"/><c:set var="nLastRunSearchId" value="${savedSearches}" scope="page"/>

<c:if test="${!empty savedSearches}" var="bHasSavedSearches"/>
<script language="JavaScript" type="text/javascript">

	var bPageReload = false;
	document.title="Loan Value Book Value Calculator";
</script>
<c:import url="/common/googleAnalytics.jsp" />
<form action="<c:url value="/EquityAnalyzer.go"/>" method="post" name="equityAnalyzerForm">
<div class="wrapper f">
	<dl>
		<dt><label for="guideBooksSel"><fmt:message key="l.guideBook"/></label></dt>
		<dd>
			<select name="guidebookName" id="guideBooksSel" onchange="switchSelect();">
			    <c:forEach items="${guidebookNames}" var="name" varStatus="i">
				    <c:if test="${i.first}">
				    	<c:if test="${name==sBookName}" var="bUsingPrimary"/>
				    </c:if>
				    <c:if test="${i.last}">
				    	<c:if test="${name==sBookName}" var="bUsingTertiary"/>
				    </c:if>
			        <option value="${name}"${name==sBookName?' selected="selected"':''}>${name}</option>
			    </c:forEach>
			</select>
			<select name="guidebookCategoryId" id="primaryBookSelect"${bUsingPrimary?'':' style="display:none" disabled="disabled"'}>
			    <c:forEach items="${primaryCategories}" var="category">
			        <option value="${category.id}"${category.id==nBookCategory?' selected="selected"':''}>${category.category}</option>
			    </c:forEach>
			</select>
			<select name="guidebookCategoryId" id="secondaryBookSelect"${(!bUsingPrimary && !bUsingTertiary)?'':' style="display:none" disabled="disabled"'}>
			    <c:forEach items="${secondaryCategories}" var="category">
			        <option value="${category.id}"${category.id==nBookCategory?' selected="selected"':''}>${category.category}</option>
			    </c:forEach>
			</select>
			<select name="guidebookCategoryId" id="thirdBookSelect"${(!bUsingPrimary && bUsingTertiary)?'':' style="display:none" disabled="disabled"'}>
			    <c:forEach items="${tertiaryCategories}" var="category">
			        <option value="${category.id}"${category.id==nBookCategory?' selected="selected"':''}>${category.category}</option>
			    </c:forEach>
			</select>
		</dd>
		<dt><label for="percentMultipliersInput"><fmt:message key="equityAnalyzer.percentMultiplier"/></label></dt>
		<dd>
			<html:text property="percentageMultiplier" styleId="percentMultipliersInput" value="${percentageMultiplier}" />
		</dd>
	</dl>
	<span id="multiplierErrorMsg" class="hilite"style="float:left"></span>
	<div class="b" id="runreportleftbuttondiv"><input type="submit" value="Run Report" class="buttonCSS" src="<c:url value="/common/_images/buttons/runReportOnWhite.gif"/>" onclick="return runSearch();"/></div>
</div>
<div class="wrapper l">
	<dl>
		<dt><label for="savedSearchesSel"><fmt:message key="equityAnalyzer.savedSearches"/></label></dt>
		<dd>
			<select name="savedSearches" id="savedSearchesSel">
			<c:choose>
				<c:when test="${!bHasSavedSearches}"><option value="0"><fmt:message key="equityAnalyzer.msg.noSavedSearchs"/></option></c:when>
				<c:otherwise>
					<c:forEach items="${savedSearches}" var="s"><option value="${s.id}" ${s.id == lastRunSearchId ? ' selected="selected"':''}>${s.name}</option></c:forEach>
				</c:otherwise>
			</c:choose>
			</select>
		</dd>
	</dl>
	<span id="savedMsg" class="hilite" style="float:left">${savedSearchMessage}</span>
	<c:if test="${bHasSavedSearches}"><div class="b"><input type="submit" value="Run Report" class="buttonCSS" src="<c:url value="/common/_images/buttons/runOnWhite.gif"/>" onclick="return runSavedSearch();"/></div></c:if>
</div>

<div class="hdr">
	<h4><span class="screenOnly"><fmt:message key="equityAnalyzer.pageTitle"/>: </span><span id="savedSearchStr"><fmt:message key="equityAnalyzer.msg.resultsHeader"><fmt:param value="${nBookValuePercent}"/><fmt:param value="${sBookName}"/><fmt:param value="${sBookValueType}"/></fmt:message></span></h4>
	<div class="b" id="savesearchbuttondiv"><input type="button" value="Save Search" class="buttonCSS" src="<c:url value="/common/_images/buttons/saveSearchOn333.gif"/>" onclick="return saveSearch();"/></div>
</div>
</form>

<%--HEADERS are set here and used in table--%>
<fmt:message key="equityAnalyzer.bookValue" var="sBookValueLabel"><fmt:param value="${sBookName}"/></fmt:message>
<fmt:message key="equityAnalyzer.percentBookValue" var="sPercentBookValueLabel"><fmt:param value="${nBookValuePercent}"/><fmt:param value="${sBookValueType}"/></fmt:message>
<fmt:message key="equityAnalyzer.percentBookValueLessCost" var="sPercentBookValueLessCost"><fmt:param value="${nBookValuePercent}"/><fmt:param value="${sBookValueType}"/></fmt:message>
<fmt:message key="equityAnalyzer.msg.emptyTableResults" var="sNoResults"/>

<div class = "bigtable">
<display:table requestURI="EquityAnalyzer.go" id="vehicle" name="equity" cellpadding="0" cellspacing="0" defaultsort="11" defaultorder="descending" class="pricingAnalyzerdata">
<display:setProperty name="css.tr.odd" value="" />
<display:setProperty name="css.tr.even" value="" />
<display:setProperty name="basic.msg.empty_list"><p class="none">${sNoResults}</p></display:setProperty>
	<display:column titleKey="l.age" property="AgeInDays" sortable="true" class="" headerClass="" />
	<display:column titleKey="l.year" property="VehicleYear" sortable="true" />
	<display:column titleKey="l.mtb" sortable="true" sortProperty="VehicleDescription" headerClass=""><link:perfPlus link="${vehicle.VehicleDescription}" grouping="${vehicle.VehicleGroupingID}"/></display:column>
	<display:column titleKey="l.color" property="Color" sortable="true" />
	<display:column titleKey="l.mileage" property="Mileage" decorator="biz.firstlook.main.display.util.MileageDecorator" sortable="true" />
	<display:column titleKey="l.pt" property="TradeOrPurchaseDesc" sortable="true" />
	<display:column titleKey="l.lp" property="ListPrice" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" />
 	<display:column titleKey="l.uc" property="UnitCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" />
	<display:column title="${sBookValueLabel}" headerClass="wrapped"  property="BookValue" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" />
	<display:column title="${sPercentBookValueLabel}"  headerClass="wrapped" property="AdjustedBookValue" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" />
	<display:column title="${sPercentBookValueLessCost}" headerClass="wrapped" property="AdjustedBookValueVsCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" sortable="true" />
	<display:column titleKey="l.snum" sortable="true" class="stockNumber" headerClass="l" sortProperty="StockNumber">
<a href="javascript:pop('<c:url value="oldIMT/EStock.go"><c:param name="stockNumberOrVin" value="${vehicle.StockNumber}"/></c:url>','estock');"><c:if test="${vehicle.IsAccurate == 0}"><img src="<c:url value="/common/_images/icons/inaccurateBookValues.gif"/>" width="16" height="16" border="0" class="icon" title="Book values may be inaccurate."/></c:if>${vehicle.StockNumber}</a>	
	</display:column>
</display:table>
</div>