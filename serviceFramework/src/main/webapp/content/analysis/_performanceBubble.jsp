<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<c:set var="sPerfType" value="${empty param.perfType ? 'overall':param.perfType}"/>
	<c:set var="sLightImgSuffix" value="${reportDisplayBean['InventoryVehicleLightID'] == 3 ? 'green' : reportDisplayBean['InventoryVehicleLightID'] == 2 ? 'yellow' : reportDisplayBean['InventoryVehicleLightID'] == 1 ? 'red' : 'none'}"/>	
<div class="bubble">
<img src="<c:url value="/common/_images/bkgds/cap-${sPerfType}Bubble-l.gif"/>" width="7" height="22" class="lcap"/>
<img src="<c:url value="/common/_images/bkgds/cap-${sPerfType}Bubble-r.gif"/>" width="7" height="22" class="rcap"/>
	<ul>
		<c:if test="${sPerfType == 'model'}"><li class="light"><img src="<c:url value="/common/_images/icons/light-13x13-${sLightImgSuffix}.gif"/>" width="13" height="13" border="0" class="light"/></li></c:if>
		<li class="ragp"><fmt:message key="l.ragp"/>: <strong><fmt:formatNumber type="currency" value="${reportDisplayBean['AGP']}" currencySymbol="$" maxFractionDigits="0"/></strong></li>
		<li class="us"><fmt:message key="l.us"/>: <strong>${reportDisplayBean['UnitsSold']}</strong></li>
		<li class="adts"><fmt:message key="l.adts"/>: <strong>${reportDisplayBean['AvgDaysToSale']}</strong></li>
		<li class="ns"><fmt:message key="l.ns"/>: <strong>${reportDisplayBean['NoSaleUnits']}</strong></li>
	</ul>
</div>