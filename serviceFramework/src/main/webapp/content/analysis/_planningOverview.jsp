<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="invInfo">
	<h5><fmt:message key="tile.planinfo.header" var="sHdr"/>${fn:toUpperCase(sHdr)}</h5>
	<ol>
		<li><strong>${unitsToReplan}</strong> <fmt:message key="plan.dueForPlanning"/></li>
		<li><strong>${plannedThisWeek}</strong> <fmt:message key="plan.plannedWeek"/></li>
	</ol>
</div>