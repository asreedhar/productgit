<tiles:importAttribute/>
<c:set var="edgeVersion" value="${slice=='edge' || param.edgeVersion == 'true'}"/>

<script type="text/javascript">
var pathname = window.location.pathname;
if(pathname.indexOf("/Optimal") > -1)
  document.title="Stocking Reports";
if(pathname.indexOf("MAX") > -1)
  document.title="Stocking Reports";
if(pathname.indexOf("/Stocking") > -1)
  document.title="Stocking Reports - CIA";
</script>
<c:import url="/common/googleAnalytics.jsp" />
<div class="analyticsBody">

	
		<div class="right_buttons">	
		<c:if test="${!edgeVersion }">
			<span id="printButton" >
				<util:button>
					<a href="#" onclick="$('analyticsResults').focus(); window.print();return false;"><fmt:message key="link.text.print"/></a>				
				</util:button>
			</span>
		</c:if>
			<c:if test="${(sessionScope.nextGenSession.hasBuyingPlan || hasBuyingPlan) && sessionScope.nextGenSession.programTypeCode ne 4}">
				<c:choose>
				<c:when test="${!edgeVersion }">
					<span id="buying_guide_link">
					<util:button><a href="<c:url value="oldIMT/CIABuyingGuideDisplayAction.go?priorWeek=0&from=FL"/>"><fmt:message key="link.text.buyingGuide"/></a></util:button>		
					</span>
				</c:when>
				<c:otherwise>
					<a href="<c:url value="oldIMT/CIABuyingGuideDisplayAction.go?priorWeek=0"/>"><img src="<c:url value="common/_images/buttons/buyingGuide_grey.gif"/>" /></a>
				</c:otherwise>
				</c:choose>
			</c:if>		
			<c:if test="${edgeVersion }">
					<a href="<c:url value="oldIMT/CIASummaryDisplayAction.go"/>"><img src="<c:url value="common/_images/buttons/btn_cia_150x17.gif"/>" /></a>
			</c:if>
		</div>
		<div class="filter">
			<c:if test="${sessionScope.nextGenSession.includeAgingPlan}">
				Show: 
				<span>
						<c:if test="${strategyId==1 }"><a id="optionAll" href="javascript:toggleStockingFilter(0, '${option}','${includeLowUnitCost}','${edgeVersion}');"></c:if><fmt:message key="pref.all"/><c:if test="${ strategyId==1 }"></a></c:if> <tt>|</tt> <c:if test="${empty strategyId || strategyId == 0  }"><a id="optionRetail" href="javascript:toggleStockingFilter(1, '${option }','${includeLowUnitCost}','${edgeVersion }');"></c:if><fmt:message key="pref.retailOnly"/><c:if test="${empty strategyId || strategyId == 0 }"></a></c:if>
				</span>
				<br />
			</c:if>
			Low Cost Inventory:
			<span>
				<c:choose>
					<c:when test="${includeLowUnitCost}">
						<a href="javascript:toggleStockingFilter(${strategyId}, '${option}','false','${edgeVersion}');"><fmt:message key="pref.exclude"/></a>
						<tt>|</tt>
						<fmt:message key="pref.include"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="pref.exclude"/>
						<tt>|</tt>
						<a href="javascript:toggleStockingFilter(${strategyId}, '${option}','true','${edgeVersion}');"><fmt:message key="pref.include"/></a>
					</c:otherwise>
				</c:choose>
				
			</span><br />
			[Low cost is less than $${unitCostThresholdValue} unit cost]
		</div>
	
	<div class="analyticsResults" id="analyticsResults">
	
		<h3>${sessionScope.nextGenSession.dealerNickName} - 	<fmt:message key="title.${!empty option?option:'underover' }"/></h3>
		<ul>
			<li><strong><fmt:message key="l.targetInventory"/></strong>: ${targetInv} (<fmt:message key="l.targetSupply"><fmt:param value="${daysSupply}"/></fmt:message>)</li>
			<li><strong><fmt:message key="l.totalUnitsInStock"/></strong>: ${totalStock}</li>
		</ul>
		<c:forEach items="${results}" var="segments" varStatus="segStatus">
		
		<div class="values">
			<table border="0" cellpadding="0" cellspacing="0" >
			<c:if test="${segStatus.first }">
					<thead>
						<th>
							&nbsp;
						</th>
						<th class="col_title">
							<fmt:message key="l.targetInventory"/>
						</th>
						<c:if test="${option != 'optimal' }">
							<th class="col_title">
								<fmt:message key="l.unitsInStock"/>
							</th>
							<th class="col_title">
								<fmt:message key="l.${!empty option?option:'underover' }"/>
							</th>
						</c:if>
					</thead>
				</c:if>
				<tbody>
				<c:if test="${not empty segments.ModelGroups || not empty segments.Other}">
				<tr>
					<td class="segmentTitle">
						${segments.VehicleSegmentDescription}
					</td>
						
					<td class="segmentData">
						${segments.TargetUnits }
					</td>
					<c:if test="${option != 'optimal' }">
					<td class="segmentData">
						 ${segments.UnitsInStock}
					</td>
					<td class="segmentData">
						<fmt:formatNumber type="currency" currencySymbol="" maxFractionDigits="0" value="${segments.UnitsInStock - segments.TargetUnits}"/>
					</td>
					</c:if>
				</tr>		
				
				<c:forEach items="${segments.ModelGroups}" var="modelGroups">
					<tr>
						<td  class="groupTitle">
							<link:perfPlus link="${modelGroups.Description}" grouping="${modelGroups.VehicleGroupingID}" />
						</td>
						<td  class="groupTitleData">
							${modelGroups.TargetUnits}
						</td>
						<c:if test="${option != 'optimal' }">
						<td  class="groupTitleData">
						<a href="#" id="stockAll${modelGroups.VehicleGroupingID}" onclick="showStockPopup(null, ${modelGroups.VehicleGroupingID}, ${includeLowUnitCost} ,'wrap'); return false;">${modelGroups.UnitsInStock}</a>
							
						</td>
						<td  class="groupTitleData">
							 <fmt:formatNumber type="currency" currencySymbol="" maxFractionDigits="0" value="${modelGroups.UnitsInStock - modelGroups.TargetUnits}"/>
						</td>
						</c:if>
					</tr>
					<c:forEach items="${modelGroups.Years}" var="year" varStatus="yearStat">
						<tr class="${yearStat.index%2==0?'even':'odd' }">
							<td class="category">
								<link:viewDeals link="${year.VehicleYear}" grouping="${modelGroups.VehicleGroupingID}" />
							</td>
							<td class="data">
								${year.TargetUnits }
							</td>
							<c:if test="${option != 'optimal' }">
							<td class="data">
								<a href="#" id="stock${modelGroups.VehicleGroupingID}${year.VehicleYear}" onclick="showStockPopup(${year.VehicleYear}, ${modelGroups.VehicleGroupingID}, ${includeLowUnitCost},'wrap'); return false;">${year.UnitsInStock}</a>
							</td>
							<td class="data">
								 <fmt:formatNumber type="currency" currencySymbol="" maxFractionDigits="0" value="${year.UnitsInStock - year.TargetUnits}"/>
							</td>
							</c:if>
						</tr>
						
					</c:forEach>
					</c:forEach>
						<tr>
							<c:if test="${not empty segments.Other}">
								<td class="groupTitle">
									<a href="/IMT/MarketStockingGuideRedirectionAction.go"><fmt:message key="l.otherVehicleSegment"><fmt:param>${segments.VehicleSegmentDescription}</fmt:param></fmt:message></a>
								</td>
								<td  class="groupTitleData">
									${segments.NonCoreTargetUnits}
								</td>
								<c:if test="${option != 'optimal' }">
								<td  class="groupTitleData">
								<a href="#" id="stockOther${segments.VehicleSegmentID}" onclick="showOtherStockPopup( ${segments.VehicleSegmentID},${strategyId},'wrap');return false;">${segments.Other.UnitsInStock}</a>
									
								</td>						<td  class="groupTitleData">
									 <fmt:formatNumber type="currency" currencySymbol="" maxFractionDigits="0" value="${segments.Other.UnitsInStock - segments.NonCoreTargetUnits}"/>
								</td>
							</c:if>
						</c:if>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		</c:forEach>
	</div>
</div>

