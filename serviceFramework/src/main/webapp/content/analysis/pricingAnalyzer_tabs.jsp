<c:if test="${empty param.saleTypeId || param.saleTypeId == 1}" var="bDisplayRetail" scope="request"/>
		
		<div class="hdr" >
		<h3><c:choose>
			<c:when test="${saleTypeId == 1 }"><fmt:message key="l.retail"/></c:when>
			<c:when test="${saleTypeId == 2 }"><fmt:message key="l.wholesale"/> </c:when>
		</c:choose><fmt:message key="l.pricingAnalyzerDataFor"/>: ${empty year ? '' : year} ${description} </h3>
		<c:if test="${not empty mileage}">
			<h4><fmt:message key="l.mileage"/>: <fmt:formatNumber type="number" maxFractionDigits="0" value="${mileage}"/></h4>
		</c:if>
		<c:if test="${inventoryItem}">
			
				<h4 ${not empty mileage?'class="pad"':'' }><fmt:message key="l.uc"/>: <fmt:formatNumber type="currency" maxFractionDigits="0" value="${unitCost}"/></h4>
				<h4 class="pad"><fmt:message key="l.lp"/>: <fmt:formatNumber type="currency" maxFractionDigits="0" value="${listPrice}"/></h4>
			
		</c:if>
		<c:if test="${showPingII && vehicleEntityTypeId != 3}">
		<span style="float:right; margin-right: 25px;">				
			<a href="javascript:void(0)" onclick="window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="inventoryId" value="${inventoryId}"/><c:param name="appraisalId" value="${appraisalId}"/><c:param name="vehicleEntityId" value="${vehicleEntityId}"/><c:param name="vehicleEntityTypeId" value="${vehicleEntityTypeId}"/><c:param name="popup" value="true"/></c:url>','PingII', 'status=1,scrollbars=1,toolbar=0,width=1009,height=860')" title="pingII">
			<img src="<c:url value="/common/_images/icons/pingIIWithText.gif"/>" />
			</a>
		</span>
		</c:if>
		<c:if test="${sessionScope.nextGenSession.hasPingUpgrade && !empty year && not(showPingII)}">
			<span style="float:right; margin-right: 25px;">				
				<a href="javascript:pop('<c:url value="Ping.go"><c:param name="mmg" value="${makeModelGroupingId}"/><c:param name="year" value="${year}"/><c:param name="distance" value="50"/><c:param name="zip" value="${Preferences['ZipCode']}"/><c:param name="search" value="Search"/></c:url>','ping')" title="<fmt:message key="title.ping"/>"><img src="<c:url value="/common/_images/icons/pingWithText.gif"/>" /></a>
			</span>
		</c:if>
		</div>
		<ul id="tabs">
		
			<li id="tab1" style="margin-left:15%;" class="<c:if test="${bDisplayRetail }" >active</c:if>">
				<a href="<c:url value="/PricingAnalyzer.go"><c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/><c:param name="saleTypeId" value="1"/><c:param name="unitCost" value="${unitCost}"/><c:param name="listPrice" value="${listPrice}"/><c:param name="mileage" value="${mileage}"/><c:param name="year" value="${year}"/><c:param name="inventoryItem" value="${inventoryItem }"/><c:param name="inventoryId" value="${inventoryId}"/><c:param name="appraisalId" value="${appraisalId}"/><c:param name="mmg" value="${makeModelGroupingId}"/><c:param name="year" value="${year}"/><c:param name="distance" value="50"/><c:param name="zip" value="${Preferences['ZipCode']}"/></c:url>" ><h5><em><fmt:message key="l.retail"/></em></h5></a>
			</li>
			<li id="tab2" class="<c:if test="${!bDisplayRetail }">active</c:if>">
				<a href="<c:url value="/PricingAnalyzer.go"><c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/><c:param name="saleTypeId" value="2"/><c:param name="unitCost" value="${unitCost}"/><c:param name="listPrice" value="${listPrice}"/><c:param name="mileage" value="${mileage}"/><c:param name="year" value="${year}"/><c:param name="inventoryItem" value="${inventoryItem }"/><c:param name="inventoryId" value="${inventoryId}"/><c:param name="appraisalId" value="${appraisalId}"/><c:param name="mmg" value="${makeModelGroupingId}"/><c:param name="year" value="${year}"/><c:param name="distance" value="50"/><c:param name="zip" value="${Preferences['ZipCode']}"/></c:url>"><h5><em><fmt:message key="l.wholesale"/> </em></h5></a>
			</li>
		</ul>
		
<br/>
	