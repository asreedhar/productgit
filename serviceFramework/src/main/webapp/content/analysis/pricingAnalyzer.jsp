<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<tiles:importAttribute />

<div id="dropDowns" >

	<label for="timePeriodId" style="color:#CCC">Time Period:</label>
		<html:select property="timePeriodId" value="${timePeriodId}" onchange="changeDropDown('${saleTypeId}','${groupingDescriptionId }','${makeModelGroupingId}', '${vehId}','${unitCost}','${year}', '${listPrice}', '${inventoryItem}', '${mileage}')">
		<c:forEach items="${timePeriods}" var="period" varStatus="index">
			<option <c:if test="${period['PeriodID'] == timePeriodId}">selected </c:if> value="${period['PeriodID']}">${period['Description']}</option>
		</c:forEach>
		</html:select>
	<label for="mileageRangeId" style="color:#CCC">&nbsp;Mileage Range:</label>
		<html:select property="mileageRangeId" value="${mileageRangeId}" onchange="changeDropDown('${saleTypeId}','${groupingDescriptionId }','${makeModelGroupingId}', '${vehId}','${unitCost}','${year}', '${listPrice}', '${inventoryItem}', '${mileage}')">
			<c:forEach items="${mileageRanges}" var="range" varStatus="index">
				<option <c:if test="${range['BucketDXMID'] == mileageRangeId}">selected </c:if> value="${range['BucketDXMID']}">${range['Description']}</option>
			</c:forEach>
		</html:select>
	<c:if test="${fn:length(trims) ne 0}">
	<label for="trimId" style="color:#CCC">&nbsp;Trim:</label>
		<html:select property="trimId" value="${trimId}" onchange="changeDropDown('${saleTypeId}','${groupingDescriptionId }','${makeModelGroupingId}', '${vehId}','${unitCost}','${year}', '${listPrice}', '${inventoryItem}', '${mileage}')">
			<c:forEach items="${trims}" var="trim" varStatus="index">
				<option <c:if test="${trim['TrimID'] == trimId}">selected </c:if> value="${trim['TrimID']}">${trim['Trim']}</option>
			</c:forEach>
		</html:select>
	</c:if>
</div> 

<div id="resultsIFrame">

	<c:url value="../PricingAnalyzerResultsAction.go?mileageRangeId=${mileageRangeId}&timePeriodId=${timePeriodId}&trimId=${trimId}&saleTypeId=${saleTypeId}&description=${description }&vehId=${param.vehId }&groupingDescriptionId=${groupingDescriptionId}" var="path"/>
	
	<c:import url="${path}"/>
	<%-- <iframe src="<c:import value="PricingAnalyzerResultsAction.go?mileageRangeId=${mileageRangeId}&timePeriodId=${timePeriodId}&trimId=${trimId}&saleTypeId=${saleTypeId}&description=${description }&vehId=${param.vehId }&groupingDescriptionId=${groupingDescriptionId}"/>"
		width="773"  frameborder="0" border="0" scrolling="yes" id="pricingIFrame"> 
	</iframe>--%>
	
</div>

