<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<!-- 
Page loads with a bean containing a list of tables.  The first table in the list is the summary table (the first one containing avg mileage).  The tables
are iterated over once for standard mode, and again for percentage mode.  Percentage mode remains hidden via the CSS display property until the
mode drop down is changed to "Percentage" and vice versa.

See John Caron for details

 -->
 <!-- Sets the minimum number of rows for each data table. If data is less than minimum, table is filled with blank rows-->
<c:set var="minTableSize" value="10" scope="page"/> 

<div class="main" >
<div style="color:#fc3;">

	<!-- generated tables -->
	  <div id="standard" style="display:${param.mode=='percentage'?'none':'inline' }">
		<c:forEach var="table" items="${tables}" varStatus="tableStatus">
			
			  <c:if test="${tableStatus.index < 1 }">
			  <!-- begin summary table -->
			  <div class="infobox" style="margin-bottom:10px;">
			   <display:table name="${table.rows}" style="border:1px solid #cccccc; width:100%; border-collapse:collapse;" uid="${tableStatus.index}" requestURI="/PerformancePlus.go" >			  
				
				<display:column decorator="biz.firstlook.main.display.util.MoneyDecorator" property="averageGrossProfit" title="Retail Avg. Gross Profit"/>
				<display:column property="unitsSold" title="Units Sold"/>
				<display:column property="averageDays" title="Avg. Days to Sale"/>
			   <display:column decorator="biz.firstlook.main.display.util.MileageDecorator" property="averageMileage" title="Avg. Mileage"/>
			  	<display:column property="noSales" title="No Sales"/>
			  	<display:column property="unitsInStock" title="Units In Stock"/>
			  	 </display:table>
			  	</div>
			  	<!-- end summary table -->
			  </c:if>
			
			 
			  <c:if test="${tableStatus.index > 0 }">
			  <c:if test="${tableStatus.index == 1  }">  <table width="100%" cellspacing="5" cellpadding="0"></c:if>
			  <c:if test="${ tableStatus.index % 2 != 0 }"><tr></c:if> 
				<td width="50%" valign="top">
			 	 <!-- <div style="height:100px">-->
				  <strong>${table.dimension.description}<c:if test="${tableStatus.index== 1  }"><a href="javascript:pop('<c:url value="PricingAnalyzer.go"><c:param name="groupingDescriptionId" value="${param.groupingDescriptionId}"/></c:url>','price')" title="<fmt:message key="title.pricingAnalyzer"/>" id="pa"><img src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" class="pa"/></a></c:if></strong><br/>
				 
				  <display:table name="${table.rows}" uid="${tableStatus.index}" length="10" style="height:100%;" requestURI="/PerformancePlus.go"   class="data">
					<display:setProperty name="basic.empty.showtable" value="true"/>
				  	<display:setProperty name="basic.msg.empty_list_row"  value="<tr class=\"empty\"><td style=\"color:red; text-align:center;\" colspan=\"{0}\">No data found to display.</td></tr>"/>
				  	<display:column style="font-weight:bold;width:5px;" decorator="biz.firstlook.main.display.util.RowNumberDecorator" title=""/>			  
				  <c:choose>
				  	<c:when test="${table.dimension.id == 0}">
						<display:column sortable="true" decorator="biz.firstlook.main.display.util.UnitCostDecorator" headerClass="left"  style="width:25%; " property="groupingColumn" title="Unit Cost  Range"/>
				  	</c:when>
				  	<c:when test="${table.dimension.id == 1}">
						<display:column sortable="true" style="text-align:left;" headerClass="left" property="groupingColumn" title="Trim"/>
				  	</c:when>
				  	<c:when test="${table.dimension.id == 2}">
						<display:column sortable="true"  style="text-align:left;" headerClass="left" property="groupingColumn" title="Year"/>
				  	</c:when>
				  	<c:when test="${table.dimension.id == 3}">
						<display:column sortable="true"  style="text-align:left;" headerClass="left" property="groupingColumn" title="Color"/>
				  	</c:when>
				  	<c:otherwise>
					  	<display:column sortable="true"  style="text-align:left;" headerClass="left" property="groupingColumn" title="Summary"/>
				  	</c:otherwise>
				  </c:choose>
				  <display:column sortable="true" style="color:red;" property="unitsSold" headerClass="f" class="f" title="Units<br/> Sold"/>
				 
				  <display:column sortable="true"  decorator="biz.firstlook.main.display.util.MoneyDecorator" property="averageGrossProfit" headerClass="f" class="f" title="Retail<br/> Avg.<br/> Gross<br/> Profit"/>
				  <display:column sortable="true"   property="averageDays" headerClass="f" class="f" title="Avg.<br/> Days<br/> to Sale"/>
				  <display:column sortable="true" property="noSales" headerClass="f" class="f"  title="No<br/> Sales"/>
				  <display:column sortable="true" property="unitsInStock" headerClass="f" class="f"  title="Units<br/> In<br/> Stock"/>
					<c:if test="${table.dimension.id == 2 && showAnnualROI}">
						<display:column sortable="true"  decorator="biz.firstlook.main.display.util.PercentageDecorator" headerClass="f" class="f"  property="annualROI" title="Annual<br/> ROI"/>
					</c:if>
				  <c:if test="${table.marketShareAnalysisPresent}">
				  <display:column sortable="true"  decorator="biz.firstlook.main.display.util.PercentageDecorator"  headerClass="leftBar" class="leftBar" property="percentageMarketShare" title="Local<br/> Market<br/>Share"/>
				  </c:if>
				 <display:footer>
				 <!-- Filler to make tables even -->	
				 			 		
				<%-- This used to ensure that tables on the same row were the same lenght.  It has been changed to a min and max height of 10 rows
				
					<c:choose>
				 	<c:when test="${tableStatus.index % 2 == 0 }">				
				 		 <c:if test="${fn:length(tables[tableStatus.index - 1].rows) > fn:length(tables[tableStatus.index].rows)}">
				 				<c:set var="diff" value="${fn:length(tables[tableStatus.index - 1].rows)  -  fn:length(tables[tableStatus.index].rows)}"/>				 						 		
				 		</c:if>				 	
				 	</c:when>
				 	<c:when test="${tableStatus.index % 2 != 0 }">				 	 	
				 		 <c:if test="${fn:length(tables[tableStatus.index + 1].rows) > fn:length(tables[tableStatus.index].rows)}">				 			
				 				<c:set var="diff" value="${fn:length(tables[tableStatus.index + 1].rows)  -  fn:length(tables[tableStatus.index].rows)}"/>				 				
						</c:if>				 	
				 	</c:when>
				 </c:choose>	--%>
				 <c:set var="diff" value="0"/>
				 <c:set var="numberOfRows"  value="${fn:length(tables[tableStatus.index].rows) < 1 ? 1:fn:length(tables[tableStatus.index].rows)}"/>
				 <c:if test="${ numberOfRows < minTableSize && diff < minTableSize -  numberOfRows}"><c:set var="diff" value="${minTableSize -  numberOfRows}"/></c:if>
					<c:if test="${diff >0 }">
					 <c:forEach var="i" begin="1" end="${diff}" varStatus="status">
					 				 <tr>
					 				<td colspan="9"  style="border-top: ${i==1?'1':'0' }px dashed #cccccc; background-color:white; padding:3px;" >
					 				&nbsp;
					 				</td>
					 				</tr>
					 </c:forEach>		
				</c:if>
				<c:set var="diff" value="0"/> <%--reset diff--%>
				 <!--End  Filler to make tables even -->						
				 <tr>
				 <td style="text-align:left;" colspan="2">
				 Overall
				 </td>
				 <td style="color:red;text-align:right;">
				 ${table.summary.unitsSold}
				 </td>
				 <td style="text-align:right;"  >
				  <fmt:formatNumber type="currency" maxFractionDigits="0" value="${table.summary.averageGrossProfit}" var="lp"/>
				  ${lp }
				 </td>
				  <td style="text-align:right;">
				 ${table.summary.averageDays}
				 </td>
				  <td style="text-align:right;">
				  ${table.summary.noSales}
				 </td>
				  <td style="text-align:right;">
				  ${table.summary.unitsInStock}
				 
				 </td>
				<c:if test="${table.dimension.id == 2 &&  showAnnualROI}">
					<td>&nbsp; <!-- annual ROI --></td>
				</c:if>
				  <c:if test="${table.marketShareAnalysisPresent}">
				 <td class="leftBar" >
				 <%--
				 <fmt:formatNumber type="percent" maxFractionDigits="1" value="	${table.summary.percentageMarketShare}" var="msa"/>
				  ${ msa }
				 --%>&nbsp;
					 </td>
				  </c:if>
				 </tr>
				 </display:footer>
				</display:table>
				</td>
				<c:if test="${tableStatus.index % 2 == 0}"></tr></c:if>
				<c:if test="${tableStatus.index % 4 == 0}"></table></c:if>
			
			</c:if>
		
		</c:forEach>
	 </div>
		
		
		<div id="percentage" style="display:${(param.mode!='percentage')?'none':'inline' }">
		<c:forEach var="table" items="${tables}" varStatus="tableStatus">
			
			  <c:if test="${tableStatus.index < 1 }">
			  <!-- begin summary table -->
			  <div class="infobox" style="margin-bottom:10px;">
			  <display:table name="${table.rows}" style="border:1px solid #cccccc; width:100%; border-collapse:collapse;" uid="${tableStatus.index}" requestURI="/PerformancePlus.go" >			  
				<display:column decorator="biz.firstlook.main.display.util.MoneyDecorator" property="averageGrossProfit" title="Retail Avg. Gross Profit"/>
				<display:column property="unitsSold" title="Units Sold"/>
				<display:column property="averageDays" title="Avg. Days to Sale"/>
			  	  <display:column decorator="biz.firstlook.main.display.util.MileageDecorator" property="averageMileage" title="Avg. Mileage"/>
			  	<display:column property="noSales" title="No Sales"/>
			  	<display:column property="unitsInStock" title="Units In Stock"/>
			  	 </display:table>
			  	</div>
			  	<!-- end summary table -->
			  </c:if>
			  <c:if test="${tableStatus.index > 0 }">
			  <c:if test="${tableStatus.index == 1  }">  <table width="100%" cellspacing="5" cellpadding="0"></c:if>
			  <c:if test="${ tableStatus.index % 2 != 0 }"><tr></c:if>
				<td width="50%" valign="top">
			 	
				  <strong>${table.dimension.description}<c:if test="${tableStatus.index== 1  }"><a href="javascript:pop('<c:url value="PricingAnalyzer.go"><c:param name="groupingDescriptionId" value="${param.groupingDescriptionId}"/></c:url>','price')" title="<fmt:message key="title.pricingAnalyzer"/>" id="pa"><img src="<c:url value="/common/_images/icons/pricingAnalyzer.gif"/>" class="pa"/></a></c:if></strong><br/>
				 
				  <display:table name="${table.rows}" uid="${tableStatus.index}"  length="10" requestURI="/PerformancePlus.go" class="data">
				  <display:setProperty name="basic.empty.showtable" value="true"/>
				  <display:setProperty name="basic.msg.empty_list_row"  value="<tr class=\"empty\"><td style=\"color:red; text-align:center;\" colspan=\"{0}\">No data found to display.</td></tr>"/>
				  <display:column style="font-weight:bold; width:5px;" decorator="biz.firstlook.main.display.util.RowNumberDecorator" title=""/>			  			  
				  <c:choose>
				  	<c:when test="${table.dimension.id == 0}">
						<display:column sortable="true"   decorator="biz.firstlook.main.display.util.UnitCostDecorator" style="text-align:left;" headerClass="left"   property="groupingColumn" title="Unit Cost Range"/>
				  	</c:when>
				  	<c:when test="${table.dimension.id == 1}">
						<display:column sortable="true"  property="groupingColumn" headerClass="left"  style="text-align:left;" title="Trim"/>
				  	</c:when>
				  	<c:when test="${table.dimension.id == 2}">
						<display:column sortable="true"  property="groupingColumn" headerClass="left"  style="text-align:left;" title="Year"/>
				  	</c:when>
				  	<c:when test="${table.dimension.id == 3}">
						<display:column sortable="true"  property="groupingColumn" headerClass="left"  style="text-align:left;" title="Color"/>
				  	</c:when>
				  	<c:otherwise>
					  	<display:column sortable="true" property="groupingColumn" headerClass="left"  style="text-align:left;"  title="Summary"/>
				  	</c:otherwise>
				  </c:choose>
				  <display:column sortable="true" style="color:red; "  decorator="biz.firstlook.main.display.util.PercentageDecorator" headerClass="f" class="f" property="percentageTotalRevenue" title="% of<br/>Revenue"/>
				 
				  <display:column sortable="true"  decorator="biz.firstlook.main.display.util.PercentageDecorator"  headerClass="f" class="f"  property="percentageTotalGrossMargin" title="% of<br/> Retail<br/> Gross<br/>Profit"/>
				  <display:column sortable="true"  decorator="biz.firstlook.main.display.util.PercentageDecorator"  headerClass="f" class="f"  property="percentageTotalInventoryDollars" title="% of<br/>Inventory<br/>Dollars"/>
				  <display:column sortable="true" property="noSales"  headerClass="f" class="f"  title="No<br/> Sales"/>
				  <display:column sortable="true" property="unitsInStock" headerClass="f" class="f"  title="Units<br/>In<br/>Stock"/>
				  <c:if test="${table.dimension.id == 2 && showAnnualROI}">
				  	<display:column sortable="true"  decorator="biz.firstlook.main.display.util.PercentageDecorator"  headerClass="f" class="f"  property="annualROI" title="Annual<br/>ROI"/>
				  </c:if>
				  <c:if test="${table.marketShareAnalysisPresent}">
				  <display:column sortable="true"   decorator="biz.firstlook.main.display.util.PercentageDecorator" headerClass="leftBar" class="leftBar"  property="percentageMarketShare" title="Local<br/>Market<br/>Share"/>
				  </c:if>
				 <display:footer>
				  <!-- Filler to make tables even -->				 		
				<%-- --> <c:choose>
				 	<c:when test="${tableStatus.index % 2 == 0 }">				
				 		 <c:if test="${fn:length(tables[tableStatus.index - 1].rows) > fn:length(tables[tableStatus.index].rows)}">
				 			<c:set var="diff" value="${fn:length(tables[tableStatus.index - 1].rows)  -  fn:length(tables[tableStatus.index].rows)}"/>				 					 		
				 		</c:if>				 	
				 	</c:when>
				 	<c:when test="${tableStatus.index % 2 != 0 }">				 	 	
				 		 <c:if test="${fn:length(tables[tableStatus.index + 1].rows) > fn:length(tables[tableStatus.index].rows) || fn:length(tables[tableStatus.index].rows) < 9}">				 			
				 			<c:set var="diff" value="${fn:length(tables[tableStatus.index + 1].rows)  -  fn:length(tables[tableStatus.index].rows)}"/>				 				
						</c:if>				 	
				 	</c:when>
				 </c:choose>	--%>
				  <c:set var="numberOfRows"  value="${fn:length(tables[tableStatus.index].rows) < 1 ? 1:fn:length(tables[tableStatus.index].rows)}"/>
				 <c:if test="${ numberOfRows < minTableSize && diff < minTableSize -  numberOfRows}"><c:set var="diff" value="${minTableSize -  numberOfRows}"/></c:if>
				
			
				 <c:if test="${diff > 0 }">
					 <c:forEach var="i" begin="1" end="${diff}" varStatus="status">
					 				 <tr>
					 				<td colspan="9"  style="border-top: ${i==1?'1':'0' }px dashed #cccccc; background-color:white; padding:3px;" >
					 				&nbsp;
					 				</td>
					 				</tr>
					 </c:forEach>		
				 </c:if>
				 <c:set var="diff" value="0"/>
				 <!--End  Filler to make tables even -->			
				 <tr>
				 <td style="text-align:left;" colspan="2">
				 Overall
				 </td>
				 <td style="color:red;text-align:right;">
				 ${table.summary.unitsSold}
				 </td>
				 <td style="text-align:right;">
				  <fmt:formatNumber type="currency" maxFractionDigits="0" value="${table.summary.averageGrossProfit}" var="lp"/>
				  ${lp }
				 </td>
				  <td style="text-align:right;">
				 ${table.summary.averageDays}
				 </td>
				  <td style="text-align:right;">
				  ${table.summary.noSales}
				 </td>
				  <td style="text-align:right;">
				  ${table.summary.unitsInStock}
				 
				 </td>
				<c:if test="${table.dimension.id == 2 && showAnnualROI}">
					<td>&nbsp; <!-- annual ROI --></td>
				</c:if>
				  <c:if test="${table.marketShareAnalysisPresent}">
				 <td class="leftBar">
				  <fmt:formatNumber type="percent" maxFractionDigits="1" value="	${table.summary.percentageMarketShare}" var="msa"/>
				  ${ msa }
				 </td>
				  </c:if>
				 </tr>
				 </display:footer>
				</display:table>
				
				<c:if test="${tableStatus.index % 2 == 0}"></tr></c:if>
				<c:if test="${tableStatus.index % 4 == 0}"></table></c:if>
				
			</c:if>
		
		</c:forEach>
	 </div>
		
	</div>

