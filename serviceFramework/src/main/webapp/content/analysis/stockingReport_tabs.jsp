<tiles:importAttribute/>
<c:set var="edgeVersion" value="${slice=='edge' }"/>
<div id="tabs">
	<div class="body">
		<ul class="reports" id="reportTabs">
			<li class="first" id="tab1"><a href="javascript:swapTabs('tab1','tab2','reportTabs','understocking','${edgeVersion }');"><fmt:message key="title.understocking"/></a></li>
			<li class="" id="tab2"><a href="javascript:swapTabs('tab2','tab3','reportTabs','overstocking','${edgeVersion }');" ><fmt:message key="title.overstocking"/></a></li>
			<li class=""  id="tab3"><a href="javascript:swapTabs('tab3','tab4','reportTabs','optimal','${edgeVersion }');" ><fmt:message key="title.optimal"/></a></li>
			<li class="active" id="tab4"><a href="javascript:swapTabs('tab4','','reportTabs','underover','${edgeVersion }');"><fmt:message key="title.underover"/></a></li>
		</ul>
	</div>
	<div class="body_right">

	</div>
</div>