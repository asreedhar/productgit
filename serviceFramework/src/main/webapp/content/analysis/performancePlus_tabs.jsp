<%@ page language="java" import="biz.firstlook.commons.services.insights.InsightTypeEnum,biz.firstlook.commons.services.insights.InsightAdjectivalEnum" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<!-- Since percent and standard modes just swap css properties, when changing from current to forecast or adding a mileagefilter 
the mode value would be lost.  To compensate when for example the mileage filter is clicked a hidden value is set in the form and then the form is
submitted preserving the mode state (because it is submitted as well.)
See John Caron for details
 -->
	
	<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="forcer" style="background-color:#474747;">
	<tr>
	<td>
	<div class="toprow">	
		<div class="title" >
			<c:forEach var="insight" items="${insights}">
						<c:if test="${insight.insightType.id == 7}">
										<c:choose><c:when test="${insight.insightValue == 1}"><img src="common/_images/icons/light-13x13-red.gif"  align="middle" style="margin-right:5px;"/></c:when><c:when test="${insight.insightValue == 2}"><img src="common/_images/icons/light-13x13-yellow.gif"  align="middle" style="margin-right:5px;"/></c:when><c:when test="${insight.insightValue == 3}"><img src="common/_images/icons/light-13x13-green.gif"  align="middle" style="margin-right:5px;"/></c:when></c:choose> 
						</c:if>
			
			</c:forEach> 
			${groupingDescription  }&mdash; 
			<c:choose>
						<c:when test="${!param.isForecast}">Previous ${param.weeks} Weeks</c:when>
						<c:otherwise>13  Week Forecaster</c:otherwise>
			</c:choose>
		</div>
		
		<div class="rightbuttons">
	
		
			<c:if test="${!param.isPopup}">			
			
				<img src="<c:url value="common/_images/buttons/backToPrevious_123x17_tabs.gif"/>" style="cursor:hand"  onclick="window.history.back();" width="123" height="17" />
			
			</c:if>
			<c:if test="${param.applyMileageFilter}">
				
					<img src="<c:url value="common/_images/buttons/removeHighMilageFilter_checked.gif"/>" style="cursor:hand"  onclick="document.getElementById('applyMileageFilter').value='false'; document.forms.modeAndRange.submit();"width="150" height="17" />
					
			</c:if>
			<c:if test="${!param.applyMileageFilter}">
			
				<img style="cursor:pointer;"  src="<c:url value="common/_images/buttons/applyHighMilageFilter_checked.gif"/>" onclick="document.getElementById('applyMileageFilter').value='true'; document.forms.modeAndRange.submit();" width="140" height="17" />
			</c:if>
			<a href="<c:url value="/ViewDealsAction.go">
				<c:param name="groupingDescriptionId" value="${param.groupingDescriptionId}"/>
				<c:param name="applyMileageFilter" value="${param.applyMileageFilter }"/>
				<c:param name="weeks" value="${weeks }"/>
				<c:param name="comingFrom" value="plus"/>
				<c:param name="reportType" value="0"/>
				<c:param name="isPopup" value="${param.isPopup}"/>
				<c:if test="${param.applyMileageFilter}">
					<c:param name="highMileage" value="70000"/>
				</c:if>
				<c:param name="isForecast" value="${param.isForecast }"/></c:url>">
				<img   src="<c:url value="common/_images/buttons/btn-viewDealsOnClear.gif"/>" width="69" height="17" />
				</a>
			</div>
	
	</div>
	
	<c:if test="${fn:length(insights) >1}">
		<div class="insight">
<span style="color:black; float:left;"><img src="common/_images/bullets/caratRight-red.gif" align="middle"/> Insights:</span>

			<div style="float:left;margin-left:10px;"> 
				<c:forEach var="insight" items="${insights}" varStatus="stat">
<c:if test="${stat.index <= fn:length(insights) -1 && insight.insightType.id != 7 && firstPrinted}">					
	<c:if test="${  stat.index % 4  ==  0 &&  stat.index > 0}"><br/></c:if>
</c:if>
					<c:if test="${insight.insightType.id != 7 && insight.insightType.id >= 1 && insight.insightType.id <= 8}">
						<span style="color:black">${stat.first ? '':'&bull;'}</span>${insight}
						<c:set var="firstPrinted" value="true"/>
					</c:if>
				</c:forEach>
				<c:remove var="firstPrinted"/>
			</div>
		</div>

	</c:if>
		<div id="tabs" >
		<ul style="margin-left:30%;">
			<li class="tab1 <c:if test="${!param.isForecast }">active</c:if>">
				<a href="javascript:document.getElementById('isForecast').value='false'; document.forms.modeAndRange.submit();" ><h5><em>CURRENT</em></h5></a>
			</li>
			<li class="tab2 <c:if test="${param.isForecast }">active</c:if>">
				<a href="javascript:document.getElementById('isForecast').value='true'; document.forms.modeAndRange.submit();"><h5><em>FORECAST</em></h5></a>
			</li>
		</ul>		
		</div>
		<form name="modeAndRange" action="PerformancePlus.go" method="get">		
		<div class="modebox" >
			Mode: <a href="javascript:document.getElementById('mode').value='standard';displayResults('standard');" id="standardLink"  class="<c:if test="${param.mode != 'percentage' }">disabled</c:if>">Standard</a><tt>|</tt><a href="javascript:document.getElementById('mode').value='percentage';displayResults('percentage');" id="percentageLink" class="<c:if test="${param.mode == 'percentage'}">disabled</c:if>">Percentage</a>
		</div>
		<div id="weeksdiv">
		
		<ul>
		<!-- 	<li>
				<label for="mode">Mode:</label> <select name="mode" id="mode" onchange="javascript:displayResults(this.value);">
				<option value="standard" <c:if test="${param.mode == 'standard' }">selected </c:if>>Standard</option>
				<option value="percentage" <c:if test="${param.mode == 'percentage' }">selected </c:if>>Percentage</option>
				</select>
			</li>-->
			<span style="color:#fc3; display:${param.isForecast?'none':'inline' }">
			<li>		
			<a href="javascript:document.getElementById('weeks').value='4';document.forms.modeAndRange.submit();" class="<c:if test="${param.weeks == '4' }">disabled</c:if>">4 Weeks</a>	
				<!-- <label for="weeks" >Range:</label> <select name="weeks" id="weeks" onchange="document.forms.modeAndRange.submit();">
				<option value="4"  <c:if test="${param.weeks == 4 }"> selected</c:if> >4 Weeks</option>
				<option value="8"  <c:if test="${param.weeks == 8 }"> selected</c:if>>8 Weeks</option>
				<option value="13"  <c:if test="${param.weeks == 13 }"> selected</c:if>>13 Weeks</option>
				<option value="26"  <c:if test="${param.weeks == 26 }"> selected</c:if>>26 Weeks</option>
				<option value="52"  <c:if test="${param.weeks == 52 }"> selected</c:if>>52 Weeks</option>
				</select>-->
			</li>
			<li>
				<a href="javascript:document.getElementById('weeks').value='8';document.forms.modeAndRange.submit();" class="<c:if test="${param.weeks == '8' }">disabled</c:if>">8 Weeks</a>
			</li>
			<li>
				<a href="javascript:document.getElementById('weeks').value='13';document.forms.modeAndRange.submit();" class="<c:if test="${param.weeks== '13' }">disabled</c:if>">13 Weeks</a>
			</li>
			<li>
				<a href="javascript:document.getElementById('weeks').value='26';document.forms.modeAndRange.submit();" class="<c:if test="${param.weeks == '26' || empty param.weeks}">disabled</c:if>">26 Weeks</a>
			</li>
			<li>
				<a href="javascript:document.getElementById('weeks').value='52';document.forms.modeAndRange.submit();" class="<c:if test="${param.weeks== '52' }">disabled</c:if>">52 Weeks</a>
			</li>
		</span>
		</ul>
		<input type="hidden" name="groupingDescriptionId" id="groupingDescriptionId" value="${param.groupingDescriptionId}"/> 
		<input type="hidden" name="applyMileageFilter" id="applyMileageFilter" value="${param.applyMileageFilter }"/> 		
		<input type="hidden" name="isForecast" id="isForecast" value="${param.isForecast }"/> 	
		<input type="hidden" name="weeks" id="weeks" value="${param.weeks }"/> 	
		<input type="hidden" name="mode" id="mode" value="${param.mode }"/> 	
		<input type="hidden" name="isPopup" id="isPopup" value="${isPopup }"/> 
		</div>
		<img src="<c:url value="/common/_images/d.gif"/>" class="forcer" />
		</td>
	</tr>
</table>
	
	</form>	
 	
