<tiles:importAttribute/>
<span id="perfSummary">
<c:if test="${sessionScope.nextGenSession.edgeStore}" var="isEdge"/>
<c:if test="${isEdge}"><c:if test="${!empty thirdparties}"><div class="services"><tiles:insert attribute="thirdparties" ignore="true"/></div></c:if>
<tiles:insert attribute="insights" ignore="true"/></c:if>

<div class="module"><c:forEach items="1,2" var="each" varStatus="loop"><c:if test="${loop.count eq 2}" var="getTrim"/><c:if test="${getTrim}"><span id="perfSummaryTrim" style="display:none;"></c:if>
	<div class="head">
<fmt:message key="link.url.viewDeals" var="viewDealsUrl"/>
	<a href="javascript:pop('<c:url value="${viewDealsUrl}">
								<c:param name="groupingDescriptionId" value="${param.groupingDescriptionId}"/>
								<c:param name="weeks"><c:out value="${param.weeks}" default="26"/></c:param>
								<c:if test="${getTrim}">
									<c:param name="trim" value="${trim}"/>
								</c:if>
								<c:param name="reportType" value="0"/>
								<c:param name="isPopup" value="true"/>
								<c:param name="comingFrom" value="${param.comingFrom}"/>
							</c:url>','deals')" class="viewDeals"><fmt:message key="link.text.viewDeals"/></a>
	
		<h5>${make} ${model} ${getTrim?trim:''}</h5> 
		<c:if test="${!getTrim}">
			<c:if test="${isEdge}">
				<a style="text-decoration:none" href="javascript:pop('/NextGen/PricingAnalyzer.go?groupingDescriptionId=${param.groupingDescriptionId}&vehicleEntityTypeId=${vehicleEntityTypeId}&vehicleEntityId=${vehicleEntityId}', 'price');">
					<img class="pa" src="/NextGen/common/_images/icons/pricingAnalyzer-FL.gif" />
				</a>
				<c:if test="${sessionScope.nextGenSession.hasPingIIUpgrade && vehicleEntityTypeId != 3}">
					<a style="text-decoration:none" href="javascript:void(0)" onclick="window.open('<c:url value="PingIIRedirectionAction.go"><c:param name="vehicleEntityTypeId" value="${vehicleEntityTypeId}"/><c:param name="vehicleEntityId" value="${vehicleEntityId}"/><c:param name="popup" value="true"/></c:url>','PingII', 'status=1,scrollbars=1,toolbar=0,width=1009,height=860')" title="pingII">
						<img src="<c:url value="/common/_images/icons/ping.gif"/>" />
					</a>
				</c:if>
			</c:if><a href="javascript:toggle('perfSummaryTrim');" id="perfSummaryTrimLink" class="trimToggle"><span><fmt:message key="a.showTrim"/></span><span style="display:none;"><fmt:message key="a.hideTrim"/></span></a>
		</c:if>
	</div>
	<div class="body">
<table cellpadding="0" cellspacing="0" border="0" class="data">
<thead><tr>
	<td><fmt:message key="l.ragp"/></td>
	<td><fmt:message key="l.us"/></td>
	<td><fmt:message key="l.adts"/></td>
	<td><fmt:message key="l.avgMileage"/></td>
	<td><fmt:message key="l.ns"/></td>
	<td><fmt:message key="l.uis"/></td>
</tr></thead>
<tbody><tr>
	<td><fmt:formatNumber type="currency"  value="${getTrim?perfSummaryReport.trim.averageGrossProfit:perfSummaryReport.overall.averageGrossProfit}" maxFractionDigits="0" var="ragp"/>${empty ragp?'&mdash;':ragp}</td>
	<td><fmt:formatNumber type="number" groupingUsed="true" value="${getTrim?perfSummaryReport.trim.unitsSold:perfSummaryReport.overall.unitsSold}" maxFractionDigits="0" var="us"/>${empty us?'&mdash;':us}</td>
	<td><fmt:formatNumber type="number" groupingUsed="true" value="${getTrim?perfSummaryReport.trim.averageDays:perfSummaryReport.overall.averageDays}" maxFractionDigits="0" var="adts"/>${empty adts?'&mdash;':adts}</td>
	<td><fmt:formatNumber type="number" groupingUsed="true" value="${getTrim?perfSummaryReport.trim.averageMileage:perfSummaryReport.overall.averageMileage}" maxFractionDigits="0" var="am"/>${empty am?'&mdash;':am}</td>
	<td><fmt:formatNumber type="number" groupingUsed="true" value="${getTrim?perfSummaryReport.trim.noSales:perfSummaryReport.overall.noSales}" maxFractionDigits="0" var="ns"/>${empty ns?'&mdash;':ns}</td>
	<td><fmt:formatNumber type="number" groupingUsed="true" value="${getTrim?perfSummaryReport.trim.unitsInStock:perfSummaryReport.overall.unitsInStock}" maxFractionDigits="0" var="uis"/>${empty uis?'&mdash;':uis}</td>
</tr></tbody>
</table>
	</div><c:if test="${getTrim}"></span></c:if></c:forEach>
</div>
</span>