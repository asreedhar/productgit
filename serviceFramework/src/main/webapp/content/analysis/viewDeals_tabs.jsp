<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


	<div class="toprow" style="border-bottom:1px solid #fc3; height:32px;">
		<div class="title" >
			<c:choose>
				<c:when test="${timePeriodId == 6}">
					Current Month
				</c:when>
				<c:when test="${timePeriodId == 7}">
					Prior Month
				</c:when>
				<c:otherwise>
					${weeks} <c:if test="${comingFrom !='PA'}">Week</c:if>
				</c:otherwise>
			</c:choose>
		${isForecast ? 'Forecaster':'Vehicle Sales History'} </div>
		<div class="rightbuttons">
			<c:if test="${comingFrom != 'PA' }">
				<c:choose>	
					<c:when test="${param.applyMileageFilter == false}">
						<a href="<c:url value="ViewDealsAction.go">
							<c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/>
							<c:param name="weeks" value="${param.weeks }"/>
							<c:param name="applyMileageFilter" value="${true}"/>
							<c:param name="comingFrom" value="${comingFrom }"/>
							<c:param name="trim" value="${trim}" />
							<c:param name="applyMileageFilter" value="${true}"/>
							<c:param name="isPopup" value="${isPopup}" />
							<c:param name="mode" value="${mode }"/>
							<c:param name="saleTypeId" value="${saleTypeId}"/>
							<c:param name="reportType" value="${reportType}"/>
							<c:param name="isForecast" value="${isForecast}"/></c:url>">
							<img src="<c:url value="common/_images/buttons/applyHighMilageFilter_checked.gif"/>" width="140" height="17" />
							</a>
					</c:when>
					<c:otherwise>
						<a href="<c:url value="ViewDealsAction.go">
						<c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/>
						<c:param name="weeks" value="${param.weeks }"/>
						<c:param name="comingFrom" value="${comingFrom }"/>
						<c:param name="mode" value="${mode}"/>
						<c:param name="trim" value="${trim}" />
						<c:param name="isPopup" value="${isPopup}" />
						<c:param name="saleTypeId" value="${saleTypeId}"/>
						<c:param name="isForecast" value="${isForecast}"/>
						<c:param name="applyMileageFilter" value="${false}"/>
						<c:param name="reportType" value="${reportType}"/></c:url>">
						<img src="<c:url value="common/_images/buttons/removeHighMilageFilter_checked.gif"/>" width="150" height="17" />
						</a>
					</c:otherwise>
				</c:choose>
			</c:if>
				<c:if test="${comingFrom =='PA' || comingFrom =='plus' }" >
				<a href="<c:if test="${comingFrom == 'PA' }">
					<c:url value="/PricingAnalyzer.go" >
					<c:param name="year" value="${year}"/>
					<c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/>
					<c:param name="saleTypeId" value="${saleTypeId}"/>
					<c:param name="showPingII" value="${showPingII}"/>
					<c:param name="appraisalId" value="${appraisalId}"/>
					<c:param name="inventoryId" value="${inventoryId}"/></c:url>
					</c:if>
					<c:if test="${comingFrom == 'plus' }">
					<c:url value="PerformancePlus.go">
						<c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/>
						<c:param name="applyMileageFilter" value="${param.applyMileageFilter}"/>
						<c:param name="weeks" value="${param.weeks }"/>
						<c:param name="isForecast" value="${isForecast}"/>
						<c:param name="isPopup" value="${isPopup}"/>
					</c:url>
					</c:if>">
					<img src="<c:url value="common/_images/buttons/backToPrevious_123x17_tabs.gif"/>" width="123" height="17" />
					</a>
			</c:if>
		</div>
	</div>
		


