<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

	
	<display:table id="items" requestURI="PricingAnalyzer.go?" name="${pricingAnalyzerResults}" cellpadding="0" cellspacing="0" defaultsort="1" defaultorder="descending" class="pricingAnalyzerdata" >
		<display:setProperty name="basic.msg.empty_list"><p class="none" style="color: #999;">There have been 0 sales of this vehicle in this time period.</p></display:setProperty>
		<display:column title="Year" sortable="true"  property="VehicleYear" style="text-align:center;"/>
		<display:column title="Units" sortable="true"  property="Units" style="text-align:center;"/>
		<display:column title="High Selling Price" sortable="true" property="MaxRetailPrice" decorator="biz.firstlook.main.display.util.MoneyDecorator" style="text-align:center;"/>
		<display:column title="Average Selling Price" sortable="true" property="AvgRetailPrice" decorator="biz.firstlook.main.display.util.MoneyDecorator" style="text-align:center;"/>
		<display:column title="Low Selling Price" sortable="true" property="MinRetailPrice" decorator="biz.firstlook.main.display.util.MoneyDecorator" style="text-align:center;"/>
		<display:column title="Average Unit Cost" sortable="true" property="AvgUnitCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" style="text-align:center;"/>
		<display:column title="Average Gross Profit" sortable="true" property="AvgFrontEndGross" decorator="biz.firstlook.main.display.util.MoneyDecorator" style="text-align:center;"/>
		<display:column title="Average Mileage" sortable="true" property="AvgMileage" decorator="biz.firstlook.main.display.util.MileageDecorator" style="text-align:center;"/> 
	</display:table>	
	<div class="viewDealsLink">
		<a  href="<c:url value="/ViewDealsAction.go"><c:param name="isPopup" value="${true }"/><c:param name="groupingDescriptionId" value="${groupingDescriptionId}"/><c:param name="year" value="${year}"/><c:param name="timePeriodId" value="${timePeriodId}"/><c:param name="lowMileage" value="${lowMileage}"/><c:param name="highMileage" value="${highMileage}"/><c:param name="trim" value="${trim}"/><c:param name="saleTypeId" value="${saleTypeId}"/><c:param name="reportType" value="0"/><c:param name="comingFrom" value="PA"/><c:param name="mode" value="UCBP"/><c:param name="showPingII" value="${showPingII}"/><c:param name="appraisalId" value="${appraisalId}"/><c:param name="inventoryId" value="${inventoryId}"/></c:url>"><img src="<c:url value="/common/_images/buttons/viewDealsOn555.gif"/>" width="69" height="17" class="btn" align="right"/></a>
	</div>
<script type="text/javascript">
 	highlightRow('${year}');
</script>
	