<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<div class="main" >
<!-- Spacer DIV -->

<div style="color:#fc3;">
	<h3>${ reportType eq 0 ? sessionScope.nextGenSession.dealerNickName : parentName}</h3>
	<h4>${saleType == "W" ? 'Wholesale':'Retail'} Performance Details for - ${makeModelDescription} ${trim}</h4>
	<!-- generated tables -->
	<display:table id="items" requestURI="ViewDealsAction.go?" name="${sales}" cellpadding="0" cellspacing="0" defaultsort="2" defaultorder="descending" class="data">

      <display:setProperty name="basic.msg.empty_list"><p class="none" style="color:  #999;">There have been 0 sales of this vehicle in this time period.</p></display:setProperty>

      <display:column title="&nbsp;" decorator="biz.firstlook.main.display.util.RowNumberDecorator"></display:column>

      <display:column title="Deal Date" sortable="true" property="DealDate" decorator="biz.firstlook.main.display.util.DateNoTimeDecorator" /> 

      <display:column title="Year" sortable="true" property="VehicleYear"  /> 

      <display:column title="Model" sortable="true" property="Model"  />

      <display:column title="Trim" sortable="true" > ${empty items['VehicleTrim'] ? '&nbsp;' : items['VehicleTrim']} </display:column>

      <display:column title="Body Style" sortable="true" > ${empty items['Descriptions'] ? '&nbsp;' : items['Descriptions']} </display:column>

      <display:column title="Color" sortable="true" property="BaseColor"  /> 

      <display:column title="Mileage" sortable="true" property="VehicleMileage"  decorator="biz.firstlook.main.display.util.MileageDecorator" />  

      <display:column title="Unit Cost" sortable="true" property="UnitCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" />

      <display:column title="Retail Gross Profit" sortable="true" property="FrontEndGross" decorator="biz.firstlook.main.display.util.MoneyDecorator" />

      <display:column title="Days to Sale" sortable="true" property="DaysToSale"   />

      <display:column title="Deal #" sortable="true"  > ${empty items['FinanceInsuranceDealNumber'] ? '&nbsp;' : items['FinanceInsuranceDealNumber']} </display:column>

      <display:column title="T/P" sortable="true" > ${ items['TradeOrPurchase'] == 1 ? 'P' : 'T'} </display:column>
      
      <c:if test="${not empty parentName}">
      	 <display:column title="Store" sortable="true" property="BusinessUnit"/>
      </c:if>

</display:table>


<c:if test="${comingFrom != 'PA' }">
<h4>No Sales</h4>

<display:table id="items" requestURI="ViewDealsAction.go?" name="${noSales}" cellpadding="0" cellspacing="0" defaultsort="2" defaultorder="descending" class="data">

      <display:setProperty name="basic.msg.empty_list"><p class="none" style="color:  #999;">There have been 0 no sales of this vehicle in this time period.</p></display:setProperty>

      <display:column title="&nbsp;" decorator="biz.firstlook.main.display.util.RowNumberDecorator"></display:column>

      <display:column title="Deal Date" sortable="true" property="DealDate" decorator="biz.firstlook.main.display.util.DateNoTimeDecorator" /> 

      <display:column title="Year" sortable="true" property="VehicleYear"  /> 

      <display:column title="Model" sortable="true" property="Model"  />

      <display:column title="Trim" sortable="true" > ${empty items['VehicleTrim'] ? '&nbsp;' : items['VehicleTrim']} </display:column>

      <display:column title="Body Style" sortable="true" > ${empty items['Descriptions'] ? '&nbsp;' : items['Descriptions']} </display:column>

      <display:column title="Color" sortable="true" property="BaseColor"  /> 

      <display:column title="Mileage" sortable="true" property="VehicleMileage"  decorator="biz.firstlook.main.display.util.MileageDecorator" />  

      <display:column title="Unit Cost" sortable="true" property="UnitCost" decorator="biz.firstlook.main.display.util.MoneyDecorator" />

      <display:column title="Retail Gross Profit" sortable="true" property="FrontEndGross" decorator="biz.firstlook.main.display.util.MoneyDecorator" />

      <display:column title="Days to Sale" sortable="true" property="DaysToSale"   />

      <display:column title="Deal #" sortable="true"  > ${empty items['FinanceInsuranceDealNumber'] ? '&nbsp;' : items['FinanceInsuranceDealNumber']} </display:column>

      <display:column title="T/P" sortable="true" > ${ items['TradeOrPurchase'] == 1 ? 'P' : 'T'} </display:column>

	  <c:if test="${not empty parentName}">
      	 <display:column title="Store" sortable="true" property="BusinessUnit"/>
      </c:if>
</display:table> 
</c:if>
</div>
</div>

