<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${!empty bookVsCostDisplayBeans[1]}" var="_hasTwoBooks"/>
<div id="bookVal">
	<table cellpadding="0" cellspacing="0" border="0">
		<thead>
		<tr>
			<th><h5>BOOK VALUES</h5></th>
			<td>Primary</td>
			<c:if test="${_hasTwoBooks}"><td>Secondary</td></c:if>
		</tr>
		</thead>
		<tbody class="condition">
		<tr>
			<th>&nbsp;</th>
			<td>${bookVsCostDisplayBeans[0]['ThirdPartyCategoryDescription']}</td>
			<c:if test="${_hasTwoBooks}"><td>${bookVsCostDisplayBeans[1]['ThirdPartyCategoryDescription']}</td></c:if>
		</tr>
		</tbody>
		<tbody>
		<tr>
			<th>${primaryBookName}:</th>
			<td><fmt:formatNumber type="currency" currencySymbol="$" value="${bookVsCostDisplayBeans[0]['BookValue']}" maxFractionDigits="0"/></td>
			<c:if test="${_hasTwoBooks}"><td><fmt:formatNumber type="currency" currencySymbol="$" value="${bookVsCostDisplayBeans[1]['BookValue']}" maxFractionDigits="0"/></td></c:if>
		</tr>
		<tr>
			<th><fmt:message key="l.cost"/>*:</th>
			<td><fmt:formatNumber type="currency" currencySymbol="$" value="${bookVsCostDisplayBeans[0]['UnitCost']}" maxFractionDigits="0"/></td>
			<c:if test="${_hasTwoBooks}"><td><fmt:formatNumber type="currency" currencySymbol="$" value="${bookVsCostDisplayBeans[1]['UnitCost']}" maxFractionDigits="0"/></td></c:if>
		</tr>
		<tr>
			<th><fmt:message key="l.bvc"/>:</th>
			<td><fmt:formatNumber type="currency" currencySymbol="$" value="${bookVsCostDisplayBeans[0]['BookVsCost']}" maxFractionDigits="0"/></td>
			<c:if test="${_hasTwoBooks}"><td><fmt:formatNumber type="currency" currencySymbol="$" value="${bookVsCostDisplayBeans[1]['BookVsCost']}" maxFractionDigits="0"/></td></c:if>
		</tr>		
		</tbody>
	</table>
	<cite>*Totals consider vehicles with book values</cite>
</div>