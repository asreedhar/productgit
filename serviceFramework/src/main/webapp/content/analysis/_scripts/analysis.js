function displayResults(val){
	if(val == 'standard') {
		document.getElementById('standard').style.display = 'inline';
		document.getElementById('percentage').style.display = 'none';
		document.getElementById('percentageLink').className = '';
		document.getElementById('standardLink').className = 'disabled';
	}
	else {
		document.getElementById('standard').style.display = 'none';
		document.getElementById('percentage').style.display = 'inline';
		document.getElementById('standardLink').className = '';
		document.getElementById('percentageLink').className = 'disabled';
	}
}

	
	//
// ***** Loan Value - Book Value Calculator *************************
//
function runSavedSearch() {
	document.equityAnalyzerForm.action = "/NextGen/EquityAnalyzer.go?runsavedsearch=true";
	return true;
}
function saveSearch() {
	document.equityAnalyzerForm.action = "/NextGen/SaveEquityAnalyzerSearch.go";
	if(isValidSearch(document.equityAnalyzerForm.percentageMultiplier.value)) {
		document.equityAnalyzerForm.submit();
	}
	return false;
}
function runSearch() {
	var theForm = document.equityAnalyzerForm;
	var percentValue = theForm.percentageMultiplier.value;
	return isValidSearch(percentValue);
}

function isValidSearch(percentValue) {
	if (isNaN(percentValue) || percentValue <= 0 || percentValue > 200) {
		var multiplierErrorMsgStr = ' Must be a number between 0 and 200';
		document.getElementById('multiplierErrorMsg').innerHTML = multiplierErrorMsgStr;
		return false;
	} else {
		return true;
	}
}

function switchSelect() {
		if (document.equityAnalyzerForm.guidebookName.options[0].selected) {
		document.getElementById("primaryBookSelect").style.display = "";
		document.getElementById("primaryBookSelect").removeAttribute( "disabled" );
		document.getElementById("secondaryBookSelect").style.display = "none";
		document.getElementById("secondaryBookSelect").setAttribute( "disabled", "disabled" );
		document.getElementById("thirdBookSelect").style.display = "none";
		document.getElementById("thirdBookSelect").setAttribute( "disabled", "disabled" );
	} 
	else if (document.equityAnalyzerForm.guidebookName.options[1].selected) {
		document.getElementById("secondaryBookSelect").style.display = "";
		document.getElementById("secondaryBookSelect").removeAttribute( "disabled" );
	   	document.getElementById("primaryBookSelect").style.display = "none";
		document.getElementById("primaryBookSelect").setAttribute( "disabled", "disabled" );
		document.getElementById("thirdBookSelect").style.display = "none";
		document.getElementById("thirdBookSelect").setAttribute( "disabled", "disabled" );
		
		
	}
	else  {
		document.getElementById("thirdBookSelect").style.display = "";
		document.getElementById("thirdBookSelect").removeAttribute( "disabled" );
		document.getElementById("secondaryBookSelect").style.display = "none";
		document.getElementById("secondaryBookSelect").setAttribute( "disabled", "disabled" );
		document.getElementById("primaryBookSelect").style.display = "none";
		document.getElementById("primaryBookSelect").setAttribute( "disabled", "disabled" );
	
	
	}
}
	
function highlightRow(year){

   	var table = document.getElementById("items");    
  	if(!table){ return; }
    var tbody = table.getElementsByTagName("tbody")[0];
    if(!tbody){ return; }
    var rows = tbody.getElementsByTagName("tr");
    if(!rows){ return;}
    // add event handlers so rows light up and are clickable
   
    for (i=0; i < rows.length; i++) {
        var value = rows[i].getElementsByTagName("td")[0].firstChild.nodeValue;
      
        if (value == year) {
            rows[i].style.backgroundColor = "yellow";
        }
    }

}
function changeDropDown(saleTypeId, groupingDescriptionId, makeModelGroupingId, vehId,unitCost,year, listPrice, inventoryItem, mileage)
{
    var url = "PricingAnalyzerResultsAction.go";
    mileageRange = $F('mileageRangeId');
    
	timePeriod = $F('timePeriodId');
	
	trim = $('trimId') ? $F('trimId') : '0'; //default to 0 for trim Id if there are no results

	var params = "&mileageRangeId=" + mileageRange;	
	params += "&timePeriodId=" + timePeriod
    params += "&trimId=" + trim
    params += "&saleTypeId=" + saleTypeId;
    params += "&groupingDescriptionId=" + groupingDescriptionId;
    params += "&makeModelGroupingId=" + makeModelGroupingId;
    params += "&vehId=" + vehId;
    params += "&unitCost=" + unitCost;
    params += "&year=" + year;
    params += "&listPrice=" + listPrice;
    params += "&inventoryItem=" + inventoryItem;
    params += "&mileage=" + mileage;

		new Ajax.Updater(
					$('resultsIFrame'),
					url,
					{
						method:'post',
						parameters: stampURL(params),
						onComplete:function(r,j){
						
							highlightRow(year);
						}
						
					}
				);
	
	
}
var ajax; //global ajax var
var linkClicked = false;
function preventClicks(ele){

	var timeout = function(){
		ele.title = "";	
	}
	
	if( (ajax && ajax.getWorking()) || linkClicked ){
	
		ele.title = "There is an operation in progress.  Please wait.";
		window.setTimeout(timeout,5000);
		return false;
	}else{
		linkClicked = true;
		return true;
	}


}

var loadData = function(xhr){
			if($('results')){
				$('results').innerHTML = xhr.responseText;	
				hideBuyingGuide();
			}else{
				$('content').innerHTML = xhr.responseText;	
				 //this button is located within the content that gets refreshed on tab change, 
				//therefore it does not know what version of stocking reports is being viewed after intial load
				//remove the element if this is edge version
				Element.remove('printButton'); 
			}
}

function toggleStockingFilter(type, option, includeLowUnitCost,isEdge){
		var map = {
			path:'/NextGen/StockingReportsData.go',
			msg:'Loading...',
			onComplete:loadData,
			options:'option='+option+'&strategyId='+type+'&edgeVersion='+isEdge+'&includeLowUnitCost='+includeLowUnitCost
		};
	
		makeRequest(map);
}

function swapTabs(active,after,ul,action,isEdge){
	window.focus();
	var ul = $(ul);
	var active = $(active);
	var after = $(after);
	var strat = $('optionAll')?'1':'0';
	for(var i = 0; i < ul.children.length; i++){
		Element.removeClassName(ul.children[i],'active');
		Element.removeClassName(ul.children[i],'after');	
	}
	
	Element.addClassName(active, 'active');
	Element.addClassName(after, 'after');	
	
	
	var map = {
		path:'/NextGen/StockingReportsData.go',
		msg:'Loading...',
		onComplete:loadData,
		options:'option='+action+'&strategyId='+strat+'&edgeVersion='+isEdge
	};

	makeRequest(map);
}

function makeParams(map) {
	var params = '';
	
	if (map.options) {
		params += map.options;
	}

	return params;
}

function makeRequest(map) {
	
	if(!ajax){
		ajax = new AjaxLoader();
		
	} else if(ajax && !ajax.getWorking() ) {

		ajax = new AjaxLoader();
		
	} else if(ajax) {
		ajax.reset();
		
	}
	
	var params = makeParams(map);
	if (map.marketsEmpty) {
		alert('At least one market must be selected');
		return;
	}
	ajax.setMessage(map.msg);
	ajax.setParameters(params);
	ajax.setPath(map.path);
	ajax.setElement(map.ele);
	if (map.onSuccess) {
		ajax.setOnSuccess(map.onSuccess);
	}
	if (map.onFailure) {
		ajax.setOnFailure(map.onFailure);
	}
	if (map.onComplete) {
		ajax.setOnComplete(map.onComplete);
	}
	if(map.setShowAt) {
		ajax.setShowAt(map.setShowAt);
	}
	
	ajax.performAction();
	return false;
}

//hack to hide the buying guide link when in MAX popup mode
//Tom said its ok to only hide the link in this version - JC
var hideBuyingGuide = function(){
	var url = document.location.href;	
	if( url.indexOf('MAXStockingReports.go')!= -1 ){	
		if( $('buying_guide_link') ){
			Element.remove('buying_guide_link');
		}
	}
}
Event.observe( window, 'load', hideBuyingGuide);