<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:importAttribute scope="request"/>
<c:if test="${empty productId}">
	<c:set var="productId" value="${isFL ? 'firstlook':'edge'}" scope="request" />
</c:if>

<tiles:insert definition="open.frag"/>

<c:if test="${not isFL}">
	<c:import url="/common/edge-menu-error.jsp" />
</c:if>
	<tiles:insert definition="${productId}.header.tile" />

<div id="${isFL ? 'body':'content'}">
<!-- //forces min-width for IE --><!--[if !IE 7]><table cellpadding="0" cellspacing="0" class="forcer"><tr><td><![endif]-->
	<tiles:insert attribute="content"/>

	<div class="button">
<c:choose>
<c:when test="${not isFL}">
	<a href="<c:url value="oldIMT/DealerHomeDisplayAction.go" />" target="_top"><img src="<c:url value="/common/_images/buttons/GoToHomePage-on52.gif" />" /></a>
</c:when>
<c:otherwise>
	<util:button><a href="<c:url value="/SearchHomePage.go" />" target="_top"><fmt:message key="dpp.link.text"/></a></util:button>
</c:otherwise>
</c:choose>
	</div>

<!--[if !IE 7]><img src="<c:url value="/common/_images/d.gif"/>" class="forcer"/></td></tr></table><![endif]-->
</div>
<tiles:insert attribute="footer" ignore="true"/>
<tiles:insert definition="close.frag"/>