<tiles:importAttribute scope="request"/>
<tiles:insert definition="open.frag"/>
<!-- //forces min-width for IE --><!--[if lte IE 6]><table cellpadding="0" cellspacing="0" class="forcer"><tr><td><![endif]-->
<!-- HEADER --><tiles:insert attribute="header" ignore="true"/><!-- /HEADER -->

<c:if test="${!empty nav}"><!-- PAGE NAVIGATION -->
<div id="nav"><tiles:insert attribute="nav" ignore="true"/></div>
<!-- /PAGE NAVIGATION --></c:if>

<!-- PAGE BODY -->
<div id="body">
	<c:if test="${!empty left}"><!-- LEFT-SIDE META CONTENT -->
	<span id="left"><tiles:insert attribute="left" ignore="true"/></span>
	<!-- /LEFT-SIDE META CONTENT --></c:if>
	<!-- RIGHT-SIDE META CONTENT -->
	<span id="right"><tiles:insert attribute="right" ignore="true"/>
	</span>
	<!-- /RIGHT-SIDE META CONTENT -->

	<tiles:insert attribute="search" ignore="true"/>
	<tiles:insert attribute="tabs" ignore="true"/>

	<div id="results"><tiles:insert attribute="content"/></div>	
</div>
<!-- /PAGE BODY -->

<c:if test="${!empty frame}"><div id="frame"><tiles:insert attribute="frame" ignore="true"/></div></c:if>

<!-- FOOTER --><tiles:insert attribute="footer" ignore="true"/><!-- /FOOTER -->
<!--[if lte IE 6]><img src="<c:url value="/common/_images/d.gif"/>" class="forcer"/></td></tr></table><![endif]-->
<tiles:insert definition="close.frag"/>