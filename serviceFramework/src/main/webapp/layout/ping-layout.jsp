<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>
<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<html:xhtml/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>PING Market Intelligence</title>
	</head>
	<body>
	
		
		
		<c:import url="/BucketSummary.go" />
		<c:import url="/PricingNavigator.go" />
		<hr />
		
		<c:set var="_frame_src">
			<c:url value="/InventoryDetails.go">
				<c:param name="inventoryId" value="${param.inventoryId}" />
			</c:url>
		</c:set>
		<iframe src="${_frame_src}" id="inventoryVehicle" name="inventoryVehicle"></iframe>
				
		<c:set var="_frame_src">
			<c:url value="/PhotoListings.go">
				<c:param name="inventoryId" value="${param.inventoryId}" />
			</c:url>
		</c:set>
		<iframe src="${_frame_src}" id="photoListings" name="photoListings"></iframe>
			
		<c:set var="_frame_src">
			<c:url value="/SearchMarketListings.go">
				<c:param name="year" value="${year}" />
				<c:param name="make" value="${make}" />
				<c:param name="model" value="${model}" />
				<c:param name="zip" value="${zip}" />
				<c:param name="distance" value="${distance}" />
			</c:url>
		</c:set>	
		<iframe src="${_frame_src}" id="internetListings" name="internetListings"></iframe>
		
				<c:set var="_frame_src">
			<c:url value="/ListingDetails.go">
				<c:if test="${not empty listingId}"><c:param name="listingId" value="${listingId}" /></c:if>
			</c:url>
		</c:set>	
		<iframe src="${_frame_src}" id="internetVehicle" name="internetVehicle"></iframe>



	</body>
</html>