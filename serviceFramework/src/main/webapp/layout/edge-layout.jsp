<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>
<%@ taglib tagdir="/WEB-INF/tags/link" prefix="link" %>
<html:xhtml/><tiles:importAttribute scope="request"/>
	<c:if test="${param.isPopup==true }"><c:set var="pageType" value="popup" scope="request"/></c:if>
	<tiles:insert definition="open.frag"/>

		<c:if test="${pageType ne 'popup' and not hideMenu}">
			<tiles:insert attribute="menu" ignore="true" />
		</c:if>
		<c:if test="${not hideHeader}">
			<tiles:insert attribute="header" ignore="true" />
			<c:if test="${showSnapshot}">
				<c:import url="/layout/snapshot.jspf" />
			</c:if>
		</c:if>

<%-- // NOTE: There is too much going on with the 'Action Plans' inline printing styles and page breaks, so this is the easiest (but still the worst) solution --%>
<c:if test="${pageId ne 'actionPlansPrint'}">
			<!-- //forces min-width for IE -->
			<!--[if lte IE 6]>
			<table cellpadding="0" cellspacing="0" class="forcer">
				<tr>
					<td>
			<![endif]-->
</c:if>

<tiles:insert attribute="nav" ignore="true"/>

				<div id="content">
					<tiles:insert attribute="tabs" ignore="true" />
					<tiles:insert attribute="content" />
				</div>
<c:if test="${pageId ne 'actionPlansPrint'}">				
			<!--[if lte IE 6]>
						<img src="<c:url value="/common/_images/d.gif"/>" class="forcer"/>
					</td>
				</tr>
			</table>
			<![endif]-->
		<c:if test="${!hideFooter}">
			<tiles:insert attribute="footer" ignore="true"/>
		</c:if>
<tiles:insert definition="close.frag"/>
</c:if>