<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
	body {font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;}
	table { border: solid 1px black; width:100%; border-collapse: collapse;}
	table th {border:solid 1px black; background-color: #cccccc;}
	table td {border:solid 1px black; }
	img {padding-top:25px}
	div {font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px}
	div.contact {font: 12px Verdana; padding:0px;margin-bottom:10px; width:450px}
</style>
<html>
<body>
<#include "actionplan-header.ftl">
<#list vehicleList as grouping>
	<table CELLSPACING=0>
		<tr>
			<th colspan="7" style="text-align:left; color: white;background-color: black">
				Wholesaler: ${grouping.wholesaler}-${grouping.endDate}
			</th>
		</tr>
		<#list grouping.cars as vehicle>
			<tr>
				<th>Vehicle Description</th>
				<th width="45%">Sales Price</th>
			</tr>
			<tr>
				<td rowspan="3">
					${vehicle.desc} <br />
					Vin: ${vehicle.vin} <br />
					Stock #: ${vehicle.stock} <br />
					Mileage: ${vehicle.mileage} <br />
				</td>
				<td> ${vehicle.listPrice}
				</td>
			</tr>
			<tr>
				<th>
					Notes
				</th>
			</tr>
			<tr>
				<td>
					${vehicle.notes}&nbsp;
				</td>
			</tr>
		</#list>
	</table>
</#list>
<#include "actionplan-footer.ftl">
</body>
</html>
