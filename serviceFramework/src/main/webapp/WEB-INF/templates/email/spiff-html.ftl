<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
	body {font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;}
	table { border:solid 1px black; width:100%; border-collapse: collapse;}
	table th {border:solid 1px black; background-color: #cccccc;}
	table td {border:solid 1px black; }
	img {padding-top:25px}
	div {font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px}
	div.contact {font: 12px Verdana; padding:0px;margin-bottom:10px; width:450px}
</style>
<html>
<body>
<#include "actionplan-header.ftl">
<table CELLSPACING=0>
	<#list vehicleList as vehicle>
		<tr>
			<th style="border-bottom:solid 1px black;">Vehicle Description</th>
			<th style="border-left:solid 1px black;border-bottom:solid 1px black;">Mileage</th>
			<th style="border-left:solid 1px black;border-bottom:solid 1px black;">Stock Number</th>
			<th style="border-left:solid 1px black;border-bottom:solid 1px black;">Internet Price</th>
			<th style="border-left:solid 1px black;border-bottom:solid 1px black;">Spiff Notes</th>
		</tr>
		<tr>
			<td>
				${vehicle.desc} <br />
				Vin: ${vehicle.vin} 
			</td>
			<td> 
				${vehicle.mileage}&nbsp; 
			</td>
			<td> 
				${vehicle.stock}&nbsp;
			</td>
			<td> 
				${vehicle.listPrice}&nbsp;
			</td>
			<td> 
				${vehicle.notes}&nbsp;
			</td>
		</tr>
	</#list>
</table>
<#include "actionplan-footer.ftl">
</body>
</html>
