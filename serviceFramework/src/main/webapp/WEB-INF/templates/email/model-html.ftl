<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
	body {font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;}
	table { border: solid 1px black; width:100%; border-collapse: collapse;}
	table th {border:solid 1px black; background-color: #cccccc;}
	table td {border:solid 1px black; }
	
	table table {border:0px;}
	table table th {background-color: transparent;border:0px;}
	table table td {border:0px; text-align:center;}
	
	img {padding-top:25px}
	div {font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px}
	div.contact {font: 12px Verdana; padding:0px;margin-bottom:10px; width:450px}
</style>
<html>
<body>
<#include "actionplan-header.ftl">
<table style="border: solid 1px black" CELLSPACING=0>
	<#list vehicleList as vehicle>
		<tr>
			<th>Vehicle Description</th>
			<th>Promotion Start Date</th>
			<th>Promotion End Date</th>
		</tr>
		<tr>
			<td rowspan="3">
				<b>${vehicle.desc}<b><br />
				<table style="width: 100%" CELLSPACING=0>
					<tr>
						<th>Age</th>
						<th>Year</th>
						<th>Stock</th>
					</tr>
					<#list vehicle.cars as innerVehicle>
					<tr>
						<td>${innerVehicle.age}</td>
						<td>${innerVehicle.year}</td>
						<td>${innerVehicle.stock}</td>
					</tr>
					</#list>
				</table>
			</td>
			<td> 
				${vehicle.startDate}
			</td>
			<td> 
				${vehicle.endDate}
			</td>
		</tr>
		<tr>
			<th colspan="2">
				Notes
			</th>
		</tr>
		<tr>
			<td colspan="2">
				${vehicle.notes}&nbsp;
			</td>
		</tr>
	</#list>
</table>
<#include "actionplan-footer.ftl">
</body>
</html>
