<!DOCTYPE html>
<html>
<head>
  <title>${subject}</title>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="#FFFFFF">
	<style type="text/css">
	  body { color:#222222; font-family: Verdana, Arial, Sans-Serif; font-size:10pt; }
	  h1 { color:#424242; font-size:15pt; line-height:2.2em; margin:0; }
	  h2 { margin:0; margin-bottom:5px; padding:0; color:#424242; font-size:11pt; line-height:13pt; }
	  hr { margin: 10px 0px 10px 0px; }
	  
	  table.layout { width:550px; border-collapse:collapse; margin-top:20px; margin-left:20px; }
	  table.layout td { border-color:#000000; border-style:solid; border-width:1px; font-size:10pt; }
	  table.layout td.header { height:57px; padding-left:27px; text-transform:uppercase; }
	  table.layout td.body { height:358px; padding-top:22px; padding-right:27px; padding-bottom:22px; padding-left:27px; vertical-align:top; }
	  table.layout td.footer { border-top-color:#aeaeae !important; height:32px; padding-left:27px; text-transform:uppercase; }
	  table.layout td.footer p { color:#999999; font-size:8pt; height:24px; padding-top:7px; text-align: right; margin-right: 5px; }
	  
	  table.data { margin: 10px 0px 10px 0px; width: 100%; }
	  table.data td { border-color: #999999; line-height:1.5em; border-style: solid dashed solid dashed; margin: 0px; }
	  table.data td.value, table.data td.label { border-bottom-style: none; border-left-style: none; }
	  table.data td.value { padding: 0px 20px 0px 10px; }
	  table.data td.label { padding: 0px 2px 0px 2px; color:#575757; text-align: right; }
	  table.data td.norm { text-align: left; }
	  
	  .logo { font-style:italic; }
  </style>
  <table cellpadding="0" cellspacing="0" class="layout">
    <tr>
      <td class="header">
        <h1><span class="logo">${buyerDealerName}</span> Buyer's Order</h1>
      </td>
    </tr>

    <tr>
      <td class="body" valign="top">
      
        <h2>${vehicleDescription}</h2>
        
        <table cellpadding="0" cellspacing="0" class="data">
          <tr>
          	<td class="label"><b>Stock #:</b></td>
            <td class="value">${vehicleStockNumber}</td>
          	<td class="label"><b>Transfer Price:</b></td>
            <td class="value">${transferPrice}</td>
          </tr>
          <tr>
          	<td class="label">Mileage:</td>
            <td class="value">${vehicleMileage}</td>
            <td class="label">VIN:</td>
            <td class="value"><small>${vehicleVin}</small></td>
          </tr>
          <tr>
          	<td class="label">Color:</td>
            <td class="value">${vehicleColor}</td>
            <td class="label">&nbsp;</td>
            <td class="value">&nbsp;</td>
          </tr>
          <tr>
            <td class="label norm" colspan="2">FOR IMMEDIATE PICKUP:</td>
            <td class="value" colspan="2">${transferImmediatePickup}</td>
          </tr>
          <tr>
            <td class="label norm" colspan="2">Has Inspection Book:</td>
            <td class="value" colspan="2">${includeInspectionBook}</td>
          </tr>
          <tr>
            <td class="label norm" colspan="2">Has Extra Keys:</td>
            <td class="value" colspan="2">${includeExtraKeys}</td>
          </tr>
          <tr>
            <td class="label norm" colspan="2">Has Navigation Disc</td>
            <td class="value" colspan="2">${includeNavigationDisc}</td>
          </tr>
        </table>
        
        <hr />
        
        <table cellpadding="0" cellspacing="0" class="data">
          <tr>
            <td class="label">Seller:</td>
            <td class="value">${sellerDealerName}</td>
          </tr>
          <tr>
            <td class="label">Seller Contact:</td>
            <td class="value">${sellerUserName}</td>
          </tr>
          <tr>
            <td class="label">Buyer:</td>
            <td class="value">${buyerDealerName}</td>
          </tr>
          <tr>
            <td class="label">Buyer Contact:</td>
            <td class="value">${buyerUserName}</td>
          </tr>
          <tr>
            <td class="label">Date:</td>
            <td class="value">${date}</td>
          </tr>
          <tr>
            <td class="label">Notes:</td>
            <td class="value">${note}</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="footer">
        <p>Powered by <span class="logo">FirstLook Automotive Retail Performance Solutions</span></p>
      </td>
    </tr>
  </table>
</body>
</html>
