<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
	body {font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;}
	table { border: solid 1px black; width:100%; border-collapse: collapse;}
	table th {border:solid 1px black; background-color: #cccccc;}
	table td {border:solid 1px black; }
	img {padding-top:25px}
	div {font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px}
	div.contact {font: 12px Verdana; padding:0px;margin-bottom:10px; width:450px}
</style>
<html>
<body>
<#include "actionplan-header.ftl">
<table CELLSPACING=0>
	<#list vehicleList as vehicle>
		<tr>
			<th>Vehicle Description</th>
			<th>Entered Service Dept.</th>
			<th>Days in Service</th>
			<th>Reason</th>
		</tr>
		<tr>
			<td rowspan="3">
				${vehicle.desc} <br />
				${vehicle.age} Days Old <br />
				Vin: ${vehicle.vin} <br />
				Stock #: ${vehicle.stock} <br />
				Mileage: ${vehicle.mileage} <br />
			</td>
			<td>
				${vehicle.dates}&nbsp;
			</td>
			<td>
				${vehicle.daysInService}&nbsp;
			</td>
			<td> 
				${vehicle.reason}&nbsp;
			</td>
		</tr>
		<tr>
			<th colspan="3">
				Notes
			</th>
		</tr>
		<tr>
			<td colspan="3">
				${vehicle.notes}&nbsp;
			</td>
		</tr>
	</#list>
</table>
<#include "actionplan-footer.ftl">
</body>
</html>
