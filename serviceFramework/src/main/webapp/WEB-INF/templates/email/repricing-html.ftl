<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
	body {font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;}
	table { border: solid 1px black; width:100%; border-collapse: collapse;}
	table th {border:solid 1px black; background-color: #cccccc;}
	table td {border:solid 1px black; }
	img {padding-top:25px}
	div {font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px}
	div.contact {font: 12px Verdana; padding:0px;margin-bottom:10px; width:450px}
</style>
<html>
<body>
<#include "actionplan-header.ftl">
<#list vehicleList as grouping>
	<table CELLSPACING=0>
		<tr>
			<th colspan="7" style="text-align:left; color: white;background-color: black">
				Reprice on: ${grouping.date} 
			</th>
		</tr>
		<#list grouping.cars as vehicle>
			<tr>
				<#if vehicle.showNotes ? exists>
					<th width="40%">Vehicle Description</th>
				<#else>
					<th width="60%">Vehicle Description</th>
				</#if>
				<th width="20%">Reprice Value</th>
				<th width="20%">Original Value</th>
				<#if vehicle.showNotes ? exists>
					<th width="20%">Selling Notes</th>
				</#if>
			</tr>
			<tr>
				<#if vehicle.showNotes ? exists>	
					<td width="40%">
				<#else>
					<td width="60%">
				</#if>
					${vehicle.desc} <br />
					Stock #: ${vehicle.stock} <br />
					Mileage: ${vehicle.mileage} <br />
				</td>
				<td width="20%"> 
					${vehicle.repriceVal}&nbsp;
				</td>
				<td width="20%"> 
					${vehicle.origVal}&nbsp;
				</td>
				<#if vehicle.showNotes ? exists>
					<td width="20%"> 
						<#if vehicle.notes ? exists>
							${vehicle.notes}
						</#if>
						&nbsp;
					</td>
				</#if>
			</tr>
		</#list>
	</table>
</#list>
<#include "actionplan-footer.ftl">
</body>
</html>
