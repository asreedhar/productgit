<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body style="margin: 0 0 0 0; background-color: white;">
<div style="font: 20px arial;font-weight:bold; color: black; background-color: white; text-align:left; padding:3px;">
	${advertiser} Exception Report
</div>
<div style="font: 12px arial; color: black; text-align: left; ;padding:5px;padding-top:13px;border-bottom:solid 1px black">
	Inclues all inventory with pricing exceptions for electronic data transfer on ${date}
</div>
<div style="font: 12px arial; color: black; text-align: left; ;padding:5px;padding-top:13px; padding-bottom:13px;">
	Vehicles ${reason}
</div>

<div style="background-color: #ddd;border-bottom:solid 1px black;margin-bottom:40px;">
	<#list exceptions as vehicle>
	<div style="background-color: lightgray; font: 14px arial;border-bottom:solid 1px #bbb;border-top:solid 1px #fff;padding:5px;">
		<span style="font-weight: bold; padding-right:25px">${vehicle.description}</span>
		<span style="padding-right:25px">COLOR:  ${vehicle.color}</span>
		<span style="padding-right:25px">MILEAGE: ${vehicle.mileage}</span>
		<span style="padding-right:25px">STOCK#: ${vehicle.stock}</span>
		<span style="padding-right:25px">VIN: ${vehicle.vin}</span>
	</div>
	</#list>
</div>

<img src="${flLogo}" alt="Firstlook" style="display:block;margin-left:auto;margin-right:auto;">
<div style="font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: center;padding:5px">
	Performance Management and Inventory Optimization
</div>
<div style="font: 12px arial; font-style:italic; font-weight:bold;color: black; text-align: center;padding:5px">
	Helping Dealers Become World Class Retailers
</div>
<div style="font: 10px arial; font-style:italic; font-weight:bold;color: black; text-align: left;padding:5px">
	First Look Help Desk: 1-877-378-5665
</div>