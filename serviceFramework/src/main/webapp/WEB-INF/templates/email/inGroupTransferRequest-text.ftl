
== ${sellerDealerName} Seller's Order ==

-- ${vehicleDescription} -- 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Stock Number:   ${vehicleStockNumber}
* Transfer Price: ${transferPrice}
* Color:          ${vehicleColor}
* Mileage:        ${vehicleMileage}
* VIN:            ${vehicleVin}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* For Immediate Pickup: ${transferImmediatePickup}
* Has Inspection Book:  ${includeInspectionBook}
* Has Extra Keys:       ${includeExtraKeys}
* Has Navigation Disc:  ${includeNavigationDisc}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          
* Seller:         ${sellerDealerName}
* Seller Contact: ${sellerUserName}
* Buyer:          ${buyerDealerName}
* Buyer Contact:  ${buyerUserName}
* Date:           ${date}
* Notes:          ${note}
