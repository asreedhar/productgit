<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<c:set var="_page" value="${page}" />
<c:remove var="page" />
<c:set var="isPageRequest" value="true" scope="request" />


<html:xhtml/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
	<base href="${_page.base}" />
	
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>
		<title>${_page.title}</title>
	</head>
	<body id="${_page.id}" class="${_page.type}">
	
${_page.type} = pop
		<c:set var="content" value="${_page.content}" scope="request" />
		<%@ include file="../content.jspf" %>

		
	</body>
</html>