<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><c:set var="_hold">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %><html:xhtml/>
<%-- decl [ --%>
	<%@ tag body-content="empty"%>
	<%@ attribute name="page" required="true" rtexprvalue="true" description="" %>
<%-- do [ --%>
<%-- ///
	EVERYONE GETS THESE --%>
		<script type="text/javascript" src="<c:url value="/behavior/lib/prototype.js" context="/static" />"></script>
		<script type="text/javascript" src="<c:url value="/behavior/lib/core.js" context="/static" />"></script>
		<script type="text/javascript" src="<c:url value="/behavior/lib/global.js" context="/static" />"></script>


<%--end [ --%></c:set>
<%-- print [ --%><c:out value="${_hold}" escapeXml="false" />