<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><c:set var="_hold">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %><html:xhtml/>
<%-- decl [ --%>
	<%@ tag body-content="empty"%>
	<%@ attribute name="page" required="true" rtexprvalue="true" description="" %>
<%-- do [ --%>
<%--<c:set var="_prefix">http${pageContext.request.secure?'s':''}://${pageContext.request.serverName}<c:if test="${not empty pageContext.request.serverPort}">:${pageContext.request.serverPort}</c:if></c:set>--%>
<c:set var="_view" 
	value="${not empty view ? view : not empty requestScope.view ? requestScope.view : 'edge'}" />


<link rel="stylesheet" media="all" type="text/css" 
	href="${_prefix}<c:url value="/view/base/layout.css" context="/static" />" />
<link rel="stylesheet" media="all" type="text/css" 
	href="${_prefix}<c:url value="/view/${view}/positioning.css" context="/static" />" />
<%--<link rel="stylesheet" media="all" type="text/css" 
	href="${_prefix}<c:url value="/view/${view}/skin.css" context="/static" />" />--%>

<%--end [ --%></c:set>
<%-- print [ --%><c:out value="${_hold}" escapeXml="false" />