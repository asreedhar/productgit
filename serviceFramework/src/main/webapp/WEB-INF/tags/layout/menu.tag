<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><c:set var="_hold">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib tagdir="/WEB-INF/tags/layout" prefix="layout" %><html:xhtml/>
<%-- decl [ --%>
	<%@ tag body-content="empty" %>
	<%@ attribute name="page" required="true" rtexprvalue="true" description="" %>
	<fmt:setBundle basename="UserInterface" />
	<%-- get menu groups list --%>
	<fmt:message key="nav.site" var="_navGroups" />
	<%-- get site popup list --%>
	<fmt:message key="nav.site.popups" var="_siteNavPopups" />
	<%-- get meta items list --%>
	<fmt:message key="nav.meta" var="_metaItems" />
	<%-- get meta popup list --%>
	<fmt:message key="nav.meta.popups" var="_metaNavPopups" />

	<%-- set nav list against upgrades --%>
	<c:if test="${empty sessionScope.navItems}"><layout:setUserNavItems /></c:if>
<%-- do [ --%>
	<div id="menu">
		<ul id="nav_site">
			<c:forEach items="${_navGroups}" var="_menuGroup" varStatus="i">
<%-- get child items list --%><fmt:message key="nav.site.${_menuGroup}" var="_menuItems" />
<%-- test if current page is part of group --%><c:if test="${fn:contains(_menuItems,page)}" var="_pageIsInGroup" />			
				<li class="${_menuGroup}${_pageIsInGroup ? ' current':''}">
				<%-- set group label --%><c:set var="_groupLabel" value="${fn:toUpperCase(_menuGroup)}" />
				<%-- wrap home page label with link --%><c:if test="${_menuGroup eq 'home'}"><c:set var="_groupLabel"><html:link forward="home">${fn:toUpperCase(_menuGroup)}</html:link></c:set></c:if>
				<%-- wrap redist label with link (if no upgrade) --%><c:if test="${_menuGroup eq 'redistribution'}"><c:set var="_groupLabel"><html:link forward="redistCenter">${fn:toUpperCase(_menuGroup)}</html:link></c:set></c:if>
				${_groupLabel}
					<%-- test for children ... no entry returns ???prop.entry??? --%>
					<c:if test="${not fn:contains(_menuItems,'???')}">
					<ul>
						<c:forEach items="${_menuItems}" var="_menuItem" varStatus="i">
						<%-- test for upgrades against _userNavItems --%>
						<c:if test="${fn:contains(sessionScope.navItems,_menuItem)}">
						<li>
						<%-- set item label --%><fmt:message key="nav.label.${_menuItem}" var="_itemLabel" />
								<c:choose>
									<%-- test if current page --%><c:when test="${page eq _menuItem}"><span class="current">${_itemLabel}</span></c:when>
									<c:otherwise><a href="<html:rewrite forward="${_menuItem}" />"<%-- test if popup link --%><c:if test="${fn:contains(_siteNavPopups,_menuItem)}"> class="pop_" target="${_menuItem}Win"</c:if>>${_itemLabel}</a></c:otherwise>
								</c:choose>
						</li>
						</c:if>
						</c:forEach>
					</ul>
					</c:if>
				</li>
		</c:forEach>
		</ul>
		<ul id="nav_meta">
			<c:forEach items="${_metaItems}" var="_metaItem" varStatus="i">
				<li><a href="<html:rewrite forward="${_metaItem}" />"<%-- test if popup link --%><c:if test="${fn:contains(_metaNavPopups,_metaItem)}"> class="pop_" target="${_metaItem eq 'memberProfile' ? 'profileWin':'informationWin'}"</c:if>><fmt:message key="nav.label.${_metaItem}" /></a></li>
			</c:forEach>
		</ul>
	</div>
<%--end [ --%></c:set>
<%-- print [ --%><c:out value="${_hold}" escapeXml="false" />
