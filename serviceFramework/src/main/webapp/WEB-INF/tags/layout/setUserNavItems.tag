<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %><html:xhtml/>
<%@ tag body-content="empty" %>
<fmt:setBundle basename="UserInterface" />
<%-- ///
				build user list --%>
<c:set var="_userList"><fmt:message key="nav.upgrade.all" /></c:set>
<c:if test="${sessionScope.nextGenSession.includeCIA}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.includeCIA" /></c:set>
</c:if>
<c:if test="${sessionScope.nextGenSession.includeAgingPlan}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.includeAgingPlan" /></c:set>
</c:if>
<c:if test="${sessionScope.nextGenSession.includePerformanceDashboard}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.includePerformanceDashboard" /></c:set>
</c:if>
<c:if test="${sessionScope.nextGenSession.includeAppraisal}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.includeAppraisal" /></c:set>
</c:if>
<c:if test="${sessionScope.nextGenSession.includeRedistribution}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.includeRedistribution" /></c:set>
</c:if>
<c:if test="${sessionScope.nextGenSession.includeEquityAnalyzer}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.includeEquity" /></c:set>
</c:if>
<c:if test="${sessionScope.nextGenSession.internetAdvertisersEnabled}">
	<c:set var="_userList">${_userList},<fmt:message key="nav.upgrade.internetAdvertisers" /></c:set>
</c:if>
<c:set var="navItems" value="${_userList}" scope="session" />