<%@ tag body-content="empty" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ attribute name="milesMinSelected" required="true" %> <%--selected option values--%>
<%@ attribute name="milesMaxSelected" required="true" %>
<%@ attribute name="triggerRefresh" required="true" %>
<%@ attribute name="uniqueId" required="true" %>
<fmt:message key="miles.minimum" var="milesMinimum"/>
<fmt:message key="miles.maximum" var="milesMaximum"/>

<%--Requires RangeSelect js object in purchasing.js--%>
<div id="rangeSelectPopup_${uniqueId }" class="rangeSelectPopup" style="display:none;">
<%--<iframe class="hideSelect" src="javascript:false;" frameBorder="0" scrolling="no"></iframe>--%>
<span><a href="#" onclick="return rangePop.save('${uniqueId }',${triggerRefresh });" title="Save"><img src="<c:url value="/common/_images/buttons/confirm-16x16-ok.gif"/>" class="ok"/></a> <a href="#" onclick="return rangePop.cancel('${uniqueId }');" title="Cancel"><img src="<c:url value="/common/_images/buttons/confirm-16x16-cancel.gif"/>" class="cancel"/></a></span>
	<div class="inner">
	<fmt:message key="l.from"/>: 
	<select name="mileageMin${uniqueId }" id="mileageMinId${uniqueId }" class="selectOnFloat mileage" >
	<c:forTokens items="${milesMinimum}" delims="," var="milesMin" varStatus="statusMin">
			<option value="${milesMin}" ${milesMin == milesMinSelected?'selected':'' }><fmt:formatNumber value="${milesMin }"/></option>
	</c:forTokens> 
	</select>
	<fmt:message key="l.to"/>:
	<select name="mileageMax${uniqueId }" id="mileageMaxId${uniqueId }" class="selectOnFloat mileage" >
		<c:forTokens items="${milesMaximum}" delims="," var="milesMax" varStatus="statusMax">
			<c:choose>
				<c:when test="${milesMax < 999999 }">
					<option value="${milesMax}" ${milesMax == milesMaxSelected?'selected':'' }><fmt:formatNumber value="${milesMax }"/></option>
				</c:when>
				<c:otherwise>
					<option value="${milesMax}" ${milesMax == milesMaxSelected?'selected':'' }><fmt:message key="l.unlimited"/></option>
				</c:otherwise>
			</c:choose>
		</c:forTokens>
	</select>
	</div>
</div>
<span  class="rangeSelect" onclick="rangePop.show('${uniqueId }');"><a href="javascript:void(0);" id="rangeSelect_${uniqueId}"><fmt:formatNumber value="${milesMinSelected }"/> - <c:choose><c:when test="${milesMaxSelected < 999999 }"><fmt:formatNumber value="${milesMaxSelected }"/></c:when><c:otherwise><fmt:message key="l.unlimited"/></c:otherwise></c:choose> mileage </a><img class="drop" src="<c:url value="/common/_images/bullets/caratDown-7x4-6C8196.gif"/>"/></span>