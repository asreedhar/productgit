<%@ tag body-content="empty" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="text" required="true" %>

<%--This tag requires the javascript file ui.js to work propery.--%>

<a id="${id }" href="#"  onclick="Util.hideAndShowText('${id }','${text }'<c:if test="${onchange != null }">,'${onchange }'</c:if>);">${text }</a><span id="${id }TextHolder" style="display:none"></span>


