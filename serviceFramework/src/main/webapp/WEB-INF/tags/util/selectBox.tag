<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="selected" required="true" %>

<%--This tag requires the javascript file ui.js to work propery.--%>
<%--
AUTHOR: JOHN CARON
DATE: 9/7/2006

id: the id of the dynamic select box
selected: the default selected text (text to be displayed in link)

tagBody: normal selectBox

--%>

<jsp:doBody var="select"/>

<c:set var="select" value="${fn:trim(select) }"/>
<c:set var="swapMethod">UI.swapSelect('${id }_selectParent');</c:set>

<c:choose>
	<c:when test="${fn:containsIgnoreCase(select,'onchange')}">		
		<c:set var="beforeOnChange">${ fn:substring(select,0,fn:indexOf(select,'onchange')+10) }</c:set>
		<c:set var="afterOnChange">${ fn:substring(select,fn:indexOf(select,'onchange')+10,fn:length(select)) }</c:set>		
		<c:set var="select">${beforeOnChange }${swapMethod }${afterOnChange }</c:set>		
	</c:when>
	<c:otherwise>
		<c:set var="beforeSelect">${ fn:substring(select,0,fn:indexOf(select,'select')+6) }</c:set>
		<c:set var="afterSelect">${ fn:substring(select,fn:indexOf(select,'select')+6,fn:length(select)) }</c:set>
		<c:set var="select">${beforeSelect } onchange="${swapMethod }" ${afterSelect }</c:set>		
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${fn:containsIgnoreCase(select,'onblur')}">		
		<c:set var="beforeOnBlur">${ fn:substring(select,0,fn:indexOf(select,'onblur')+8) }</c:set>
		<c:set var="afterOnBlur">${ fn:substring(select,fn:indexOf(select,'onblur')+8,fn:length(select)) }</c:set>		
		<c:set var="select">${beforeOnBlur }if($(${id }_hideSpan).className != 'hide')${swapMethod }${afterOnBlur }</c:set>		
	</c:when>
	<c:otherwise>
		<c:set var="beforeSelect">${ fn:substring(select,0,fn:indexOf(select,'select')+6) }</c:set>
		<c:set var="afterSelect">${ fn:substring(select,fn:indexOf(select,'select')+6,fn:length(select)) }</c:set>
		<c:set var="select">${beforeSelect } onblur="if($(${id }_hideSpan).className != 'hide')${swapMethod }" ${afterSelect }</c:set>		
	</c:otherwise>
</c:choose>

<div class="dynSelect" id="${id }_selectParent" onmouseup="if(this.children[0].className != 'hide'){UI.swapSelect('${id }_selectParent');}">
	
		<a href="javascript:;" id="${id }_selectText" >${selected }</a>	
	
	
	<span class="hide" id="${id }_hideSpan">
		${select }	
	</span>
	
</div>


