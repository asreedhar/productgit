<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><c:set var="_hold">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %><html:xhtml/>
<%-- decl [ --%>
	<%@ tag body-content="scriptless" dynamic-attributes="dynattrs" %>
	<%@ attribute name="tagName" required="false" rtexprvalue="false" description="" %>
	<jsp:doBody var="_body"/>
	<c:set var="_tag"><c:out value="${tagName}" default="div" /></c:set>
	<c:set var="_attrs">
		<c:forEach items="${dynattrs}" var="a"> ${a.key}="${a.value}"</c:forEach>
	</c:set>
<%-- do [ --%>
<${_tag}${not empty _attrs ? ' ':''}${_attrs}>
	${_body}
</${_tag}>
<%--end [ --%></c:set>
<c:if test="${fn:trim(_body) eq ''}"><c:set var="_hold" value="" /></c:if>
<%-- print [ --%><c:out value="${_hold}" escapeXml="false" />