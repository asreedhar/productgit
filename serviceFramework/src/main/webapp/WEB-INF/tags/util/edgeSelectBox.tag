<%@ tag body-content="scriptless" dynamic-attributes="items"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ attribute name="className" required="true" %>       <%--class name for selectbox UL --%>
<%@ attribute name="customProperty" required="false" %> <%--custom property for struts html multibox population of custom filter (ageCustom, riskCustom, etc)--%>

<%@ attribute name="change" required="false" %>         <%--javascript function object to call on change, supply object only not function call ( myFunction )* no parameters supplied here.  Should accept params of the format (item, type, value, hiddenId)--%>
<%@ attribute name="selectId" required="false" %>       <%--id supplied to the UL select element (myId)--%>
<%@ attribute name="style" required="false" %>          <%--style supplied to the UL select element (display:none;) %>

<%-- requires a dynamic attribute consisting of a list of maps with each element containing a RangeID (value) and Description (what is displayed)--%>

<jsp:doBody var="hidden"/>
<c:if test="${empty change}">
	<c:set var="change" value="false"/>
</c:if>
<%--parse out the hidden field to locate attributes--%>
<c:set var="hidden"  value = "${fn:trim(hidden)}"/>
<c:set var="property" value = "${fn:substring(hidden, fn:indexOf(hidden,'name=') + 6,fn:indexOf(hidden,'value')-2)}"/>
<c:set var="selected" value = "${fn:substring(hidden, fn:indexOf(hidden,'value=')+7,fn:indexOf(hidden,'id=')-2)}"/>
<c:set var="hiddenId" value="${fn:substring(hidden, fn:indexOf(hidden,'id=') +4, fn:indexOf(hidden,'>')-1)}"/>

<div onmouseout="handleCustomFilter(event,'${property }');">
<ul class="${className}" id="${selectId }" style="${style }" onmouseover = "selectMouseOver(this,event);" onmouseout="selectMouseOver(this,event);">	
	<c:forEach items="${items}" var="item" varStatus="stat">
		<c:forEach items="${item.value}" var="i" varStatus="stat">
		<c:if test="${i.Description ne 'Custom'}">
			<c:set var="strRangeId" value="${i.RangeID}"/>
			<%--need to make sure event originated with the LI and not the CB it holds otherwise clicking CB clicks LI--%>		
			<li onclick="if(Event.element(event) == this)handleCustomSelect(this,'${property }','${i.RangeID }','${hiddenId }', ${change });"  onmouseover="handleCustomSelectHover(this);" onmouseout="handleCustomSelectHover(this);" <c:if test="${strRangeId == selected  || empty selected && stat.index == 0}">class="selected"</c:if> ><c:if test="${!empty customProperty && !stat.first && !stat.last}"> <html:multibox property="${customProperty }" value="${fn:toUpperCase(i.RangeID) }" styleId="${customProperty }${stat.count }" styleClass="customCheck" onclick="toggleCustomChanged()" /></c:if> ${i.Description} </li>
		</c:if>
		</c:forEach>
	</c:forEach>
</ul>
</div>
<%--write the hidden field--%>
${hidden}