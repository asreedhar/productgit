<%@ tag body-content="empty" %><%@ attribute name="id" required="true" %><%@ attribute name="adjective" required="true" %><%@ attribute name="value" required="true" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="showInsight" value="true" />
<c:if test="${id eq 11 || id eq 12 || id eq 13 || id eq 14 || id eq 15}"><c:set var="showInsight" value="false" /></c:if>

<c:if test="${showInsight}">
<c:if test="${id eq 1}"><fmt:message var="arg0" key="insight.adj.insufficient${adjective eq 'INSUFFICIENT'?'':'.opp'}"/><c:set var="arg1" value="${value}"/></c:if>
<c:if test="${id eq 2}"><c:set var="arg0" value="${value}"/><fmt:message var="arg1" key="insight.adj.more${adjective eq 'MORE'?'':'.opp'}"/></c:if>
<c:if test="${id eq 3}"><c:set var="arg0" value="${value}"/></c:if>
<c:if test="${id eq 4}"><c:set var="arg0" value="${value}"/><fmt:message var="arg1" key="insight.adj.higher${adjective eq 'HIGHER'?'':'.opp'}"/></c:if>
<c:if test="${id eq 5}"><c:set var="arg0" value="${value}"/></c:if>
<c:if test="${id eq 6}"><c:set var="arg0" value="${value lt 0?-value:value}"/><fmt:message var="arg1" key="l.days${arg0 eq 1 ? '.singular':''}"/><fmt:message var="arg2" key="insight.adj.faster${adjective eq 'FASTER'?'':'.opp'}"/></c:if>
<c:if test="${id eq 11}"><c:set var="arg0" value="${value}"/></c:if>
<c:if test="${id eq 8}"><c:set var="id">${id}.${fn:toLowerCase(adjective)}</c:set></c:if>

<fmt:message key="insight.id${id}"><fmt:param value="${arg0}"/><fmt:param value="${arg1}"/><fmt:param value="${arg2}"/></fmt:message>
</c:if>