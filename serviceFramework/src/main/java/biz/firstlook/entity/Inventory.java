package biz.firstlook.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.Transient;

import biz.firstlook.commons.util.DateUtilFL;

@SuppressWarnings("serial")
@Entity
@Table( name = "Inventory" )
public class Inventory implements Serializable
{
@Id
@GeneratedValue( strategy = GenerationType.AUTO )
@Column( name = "InventoryID" )
private Integer id;

private String stockNumber;
private Integer businessUnitId;

@ManyToOne
@JoinColumn( name = "VehicleID" )
private Vehicle vehicle;

private Date inventoryReceivedDate;
private Double unitCost;
private Integer mileageReceived;
private Integer tradeOrPurchase;
private Double listPrice;
private Boolean certified;
private String vehicleLocation;
private Integer currentVehicleLight;
private Boolean specialFinance;
private Boolean listPriceLock;
private Date planReminderDate;

@Column(name="eStockCardLock")
private boolean eStockCardLock;

@Version
@Column( name="VersionNumber" )
private Integer version;

//@OneToMany(cascade=CascadeType.ALL)
//@JoinTable(
//        name="InventoryPhotos",
//        joinColumns = @JoinColumn(name="InventoryID"),
//        inverseJoinColumns = @JoinColumn(name="PhotoID")
//)
@Transient 
Set<Photo> photos;

private Boolean inventoryActive;

@Column(name="TransferPrice")
private Float transferPrice;

@Column(name="TransferForRetailOnly")
private Boolean transferForRetailOnly;

@Column(name="TransferForRetailOnlyEnabled")
private boolean transferForRetailOnlyEnabled;

public Inventory() 
{
	super();
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

public Double getListPrice()
{
	return listPrice;
}

public void setListPrice( Double listPrice )
{
	this.listPrice = listPrice;
}

public Integer getMileageReceived()
{
	return mileageReceived;
}

public void setMileageReceived( Integer mileageReceived )
{
	this.mileageReceived = mileageReceived;
}

public String getStockNumber()
{
	return stockNumber;
}

public void setStockNumber( String stockNumber )
{
	this.stockNumber = stockNumber;
}

public Integer getTradeOrPurchase()
{
	return tradeOrPurchase;
}

public void setTradeOrPurchase( Integer tradeOrPurchase )
{
	this.tradeOrPurchase = tradeOrPurchase;
}

public Double getUnitCost()
{
	return unitCost;
}

public void setUnitCost( Double unitCost )
{
	this.unitCost = unitCost;
}

public Date getInventoryReceivedDate()
{
	return inventoryReceivedDate;
}

public void setInventoryReceivedDate( Date inventoryReceivedDate )
{
	this.inventoryReceivedDate = inventoryReceivedDate;
}

public Vehicle getVehicle()
{
	return vehicle;
}

public void setVehicle( Vehicle vehicle )
{
	this.vehicle = vehicle;
}

public Integer getCurrentVehicleLight()
{
	return currentVehicleLight;
}

public void setCurrentVehicleLight( Integer currentVehicleLight )
{
	this.currentVehicleLight = currentVehicleLight;
}

public Integer getVersion()
{
	return version;
}

public void setVersion( Integer version )
{
	this.version = version;
}


public Boolean getCertified()
{
	return certified;
}

public void setCertified( Boolean certified )
{
	this.certified = certified;
	if(certified != null && certified.booleanValue()) {
		this.eStockCardLock = true;
	} else {
		this.eStockCardLock = false;
	}
}

public String getVehicleLocation()
{
	return vehicleLocation;
}

public void setVehicleLocation( String vehicleLocation )
{
	this.vehicleLocation = vehicleLocation;
}

public Boolean getSpecialFinance()
{
	return specialFinance;
}

public void setSpecialFinance( Boolean specialFinance )
{
	this.specialFinance = specialFinance;
}

public Date getPlanReminderDate()
{
	return planReminderDate;
}

public void setPlanReminderDate( Date planReminderDate )
{
	this.planReminderDate = planReminderDate;
}

public boolean isEStockCardLock() {
	return eStockCardLock;
}

/**
 * Calculates and returns the Inventory's age
 * 
 * @return Integer
 */
public Integer getAge()
{
	return DateUtilFL.calculateNumberOfCalendarDays( new Date(), inventoryReceivedDate );
}

/**
 * Determines whether an inventory has/had a current plan with
 * respect to the given date
 * 
 * @param comparisonDate
 * @return true if the inventory has a current plan, false otherwise
 */
public boolean hasCurrentPlanAsOf( Date comparisonDate )
{
    if ( getPlanReminderDate() == null || comparisonDate.compareTo( planReminderDate ) >= 0 )
    {
    	return false;
    }
    else
    {
        return true;
    }
}

public Boolean getListPriceLock() {
	return listPriceLock;
}

public void setListPriceLock(Boolean listPriceLock) {
	this.listPriceLock = listPriceLock;
}


public Boolean getInventoryActive() {
    return inventoryActive;
}

public void setInventoryActive(Boolean inventoryActive) {
    this.inventoryActive = inventoryActive;
}

public Set<Photo> getPhotos() {
    return photos;
}

public void setPhotos(Set<Photo> photos) {
    this.photos = photos;
}

public Float getTransferPrice() {
	return transferPrice;
}

public Boolean getTransferForRetailOnly() {
	return transferForRetailOnly;
}

public void setTransferForRetailOnly(Boolean transferForRetailOnly) {
	this.transferForRetailOnly = transferForRetailOnly;
}

public void setTransferPrice(Float transferPrice) {
	this.transferPrice = transferPrice;
}

public boolean isTransferForRetailOnlyEnabled() {
	return transferForRetailOnlyEnabled;
}

}
