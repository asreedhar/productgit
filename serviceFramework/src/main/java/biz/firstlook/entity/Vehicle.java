package biz.firstlook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.commons.util.ColorUtility.Color;

@SuppressWarnings("serial")
@Entity
@Table(name="Vehicle")
public class Vehicle implements Serializable
{
 
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="VehicleID")
private Integer id;

private String vin;

@Column(name="VehicleYear")
private Integer year;

//private Integer makeModelGroupingId;

@ManyToOne
@JoinColumn(name="MakeModelGroupingID")
private MakeModelGrouping makeModelGrouping;

@Column(name="VehicleTrim")
private String trim;
private String baseColor;
private String originalMake;
private String originalModel;

@Column(name="VehicleEngine")
private String engine;

private String cylinderCount;

@Column(name="VehicleDriveTrain")
private String vehicleDriveTrain;

@Column(name="VehicleTransmission")
private String vehicleTransmission;

private String fuelType;
private Integer doorCount;
private String interiorColor;
private String interiorDescription;

public Vehicle()
{
	super();
}

public Vehicle( VehicleCatalogEntry vehicleCatalogEntry )
{
	this.setVin( vehicleCatalogEntry.getVin() );
	this.setOriginalMake(vehicleCatalogEntry.getMake()); 
	this.setOriginalModel(vehicleCatalogEntry.getLine()); 
	this.setYear(vehicleCatalogEntry.getModelYear()); // not persisted
	this.setTrim(vehicleCatalogEntry.getSeries()); 
	
	GroupingDescription gd = new GroupingDescription( vehicleCatalogEntry.getGroupingDescriptionId(), vehicleCatalogEntry.getGroupingDescription() );
	MakeModelGrouping mmg = new MakeModelGrouping();
	mmg.setMake( vehicleCatalogEntry.getMake() );
	mmg.setModel( vehicleCatalogEntry.getModel() );
	mmg.setModel( vehicleCatalogEntry.getModel() );
	mmg.setId( vehicleCatalogEntry.getMakeModelGroupingId() );
	mmg.setGroupingDescription( gd );
	this.setMakeModelGrouping( mmg );

	this.setDoorCount( vehicleCatalogEntry.getDoors());
	this.setFuelType( vehicleCatalogEntry.getFuelType() );
	this.setEngine( vehicleCatalogEntry.getEngine() );
	this.setVehicleDriveTrain( vehicleCatalogEntry.getDriveTrain() );
	this.setVehicleTransmission( vehicleCatalogEntry.getTransmission() );
	this.setCylinderCount( vehicleCatalogEntry.getCylinderCount().toString() );
	this.setBaseColor(Color.UNKNOWN.toString());
	this.setInteriorColor(Color.UNKNOWN.toString());
	this.setInteriorDescription(Color.UNKNOWN.toString());
}


public String getBaseColor()
{
	setBaseColor( baseColor );
	return baseColor;
}

public void setBaseColor( String baseColor )
{
	this.baseColor = ColorUtility.formatColor( baseColor );
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

/**
public Integer getMakeModelGroupingId()
{
	return makeModelGroupingId;
}

public void setMakeModelGroupingId( Integer makeModelGroupingId )
{
	this.makeModelGroupingId = makeModelGroupingId;
}**/

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}

public String getOriginalModel()
{
	return originalModel;
}

public void setOriginalModel( String originalModel )
{
	this.originalModel = originalModel;
}

public String getOriginalMake()
{
	return originalMake;
}

public void setOriginalMake( String originalMake )
{
	this.originalMake = originalMake;
}

public String getTrim()
{
	return trim;
}

public void setTrim( String trim )
{
	this.trim = trim;
}

public MakeModelGrouping getMakeModelGrouping()
{
	return makeModelGrouping;
}

public void setMakeModelGrouping( MakeModelGrouping makeModelGrouping )
{
	this.makeModelGrouping = makeModelGrouping;
}

public String getCylinderCount()
{
	return cylinderCount;
}

public void setCylinderCount( String cylinderCount )
{
	this.cylinderCount = cylinderCount;
}

public Integer getDoorCount()
{
	return doorCount;
}

public void setDoorCount( Integer doorCount )
{
	this.doorCount = doorCount;
}

public String getVehicleDriveTrain()
{
	return vehicleDriveTrain;
}

public void setVehicleDriveTrain( String driveTrain )
{
	this.vehicleDriveTrain = driveTrain;
}

public String getEngine()
{
	return engine;
}

public void setEngine( String engine )
{
	this.engine = engine;
}

public String getFuelType()
{
	return fuelType;
}

public void setFuelType( String fuelType )
{
	this.fuelType = fuelType;
}

public String getInteriorColor()
{
	setInteriorColor( interiorColor );
	return interiorColor;
}

public void setInteriorColor( String interiorColor )
{
	this.interiorColor = ColorUtility.formatColor( interiorColor );
}

public String getInteriorDescription()
{
	return interiorDescription;
}

public void setInteriorDescription( String interiorDescription )
{
	this.interiorDescription = interiorDescription;
}

public String getVehicleTransmission()
{
	return vehicleTransmission;
}

public void setVehicleTransmission( String transmission )
{
	this.vehicleTransmission = transmission;
}



}
