package biz.firstlook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table( name = "InventoryBucketRanges" )
public class InventoryBucketRange implements Serializable
{
@Id
@Column( name = "InventoryBucketRangeId" )
private Integer id;

private Integer inventoryBucketId;
private Integer low;
private Integer high;
private Integer businessUnitId;
private Integer rangeId;
private String description;

// Lights value is BITMASKED: 
//  7 = 111 = Red,Yellow, Green
//	1 = 001 = Red
//	3 = 011 = Red, Yellow
//	4 = 100 = Green, 
//	etc...
private Integer lights;

public Integer getHigh()
{
	return high;
}

public void setHigh( Integer high )
{
	this.high = high;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

public Integer getInventoryBucketId()
{
	return inventoryBucketId;
}

public void setInventoryBucketId( Integer inventoryBucketId )
{
	this.inventoryBucketId = inventoryBucketId;
}

public Integer getLow()
{
	return low;
}

public void setLow( Integer low )
{
	this.low = low;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Integer getLights() {
	return lights;
}

public void setLights(Integer lights) {
	this.lights = lights;
}

public Integer getRangeId() {
	return rangeId;
}

public void setRangeId(Integer rangeId) {
	this.rangeId = rangeId;
}
}
