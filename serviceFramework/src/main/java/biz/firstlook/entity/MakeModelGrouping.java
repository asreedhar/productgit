package biz.firstlook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="MakeModelGrouping")
public class MakeModelGrouping implements Serializable
{

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="MakeModelGroupingID")
private Integer id;

@ManyToOne
@JoinColumn(name="GroupingDescriptionID")
private GroupingDescription groupingDescription;

private String make;
private String model;

public MakeModelGrouping()
{
	super();
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

public GroupingDescription getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( GroupingDescription groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

}
