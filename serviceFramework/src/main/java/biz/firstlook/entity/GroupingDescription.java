package biz.firstlook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="tbl_GroupingDescription")
public class GroupingDescription implements Serializable
{

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="GroupingDescriptionID")
private Integer id;

private String groupingDescription;

public GroupingDescription()
{
	super();
}


public GroupingDescription( Integer groupingDescriptionId, String groupingDescriptionString )
{
	this.groupingDescription = groupingDescriptionString;
	this.id = groupingDescriptionId;
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}



}
