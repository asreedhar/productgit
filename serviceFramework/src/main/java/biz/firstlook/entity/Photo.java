package biz.firstlook.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Photos")
public class Photo implements Serializable{

    private static final long serialVersionUID = 2389576906830448846L;
    private Integer photoId;
    private String photoUrl;
    private Boolean isPrimaryPhoto;
    private Integer photoStatusCode;

    @Id
    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }
    
    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Boolean getIsPrimaryPhoto() {
        return isPrimaryPhoto;
    }

    public void setIsPrimaryPhoto(Boolean primaryPhoto) {
        this.isPrimaryPhoto = primaryPhoto;
    }

    public Integer getPhotoStatusCode() {
        return photoStatusCode;
    }

    public void setPhotoStatusCode(Integer photoStatusCode) {
        this.photoStatusCode = photoStatusCode;
    }

    
    
    

}
