package biz.firstlook.entity;

public interface TransferPriceValidator {
	TransferPriceValidation validate(Inventory inventory, Float newTransferPrice);
}
