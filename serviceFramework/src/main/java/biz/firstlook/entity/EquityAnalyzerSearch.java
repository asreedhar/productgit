package biz.firstlook.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name="EquityAnalyzerSearches" )
public class EquityAnalyzerSearch
{
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column( name="EquityAnalyzerSearchId" )
@Basic( fetch = FetchType.EAGER)
private Integer id;

@Basic( fetch = FetchType.EAGER)
private Integer businessUnitId;

@Basic( fetch = FetchType.EAGER)
private String name;

@Basic( fetch = FetchType.EAGER)
private Integer thirdPartyCategoryId;

@Basic( fetch = FetchType.EAGER)
private Integer percentMultiplier;

public Integer getBusinessUnitId()
{
	return businessUnitId;
}
public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}
public Integer getId()
{
	return id;
}
public void setId( Integer id )
{
	this.id = id;
}
public String getName()
{
	return name;
}
public void setName( String name )
{
	this.name = name;
}
public Integer getPercentMultiplier()
{
	return percentMultiplier;
}
public void setPercentMultiplier( Integer percentMultiplier )
{
	this.percentMultiplier = percentMultiplier;
}
public Integer getThirdPartyCategoryId()
{
	return thirdPartyCategoryId;
}
public void setThirdPartyCategoryId( Integer thirdPartyCategoryId )
{
	this.thirdPartyCategoryId = thirdPartyCategoryId;
}
}
