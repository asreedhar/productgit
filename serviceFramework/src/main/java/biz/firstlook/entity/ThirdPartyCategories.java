package biz.firstlook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="ThirdPartyCategories")
public class ThirdPartyCategories implements Serializable
{
@Id
@Column( name="ThirdPartyCategoryID" )
private Integer id;

private Integer thirdPartyId;
private String category;

public String getCategory()
{
	return category;
}

public void setCategory( String category )
{
	this.category = category;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

public Integer getThirdPartyId()
{
	return thirdPartyId;
}

public void setThirdPartyId( Integer thirdPartyId )
{
	this.thirdPartyId = thirdPartyId;
}
}
