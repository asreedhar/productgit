package biz.firstlook.commons.hibernate;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.main.persist.common.IGenericDAO;

public class TransactionTemplatePersistor<E,K extends Serializable> implements Persistor<E, K> {

	protected TransactionTemplate transactionTemplate;
	protected SessionFactory sessionFactory;
	
	public TransactionTemplatePersistor(TransactionTemplate transactionTemplate, SessionFactory sessionFactory) {
		this.transactionTemplate = transactionTemplate;
		this.sessionFactory = sessionFactory;
	}
	
	public E persist(final IGenericDAO<E,K> genericHibernate, final E entity) {
		try {
			transactionTemplate.execute(new TransactionCallback() {
				public Object doInTransaction(TransactionStatus status) {
					if (status.isRollbackOnly()) {
						Logger.getLogger(getClass()).error("Not even going to try and saveOrUpdate! Is RollbackOnly ...");
						return null;
					}
					try {
						final Session session = sessionFactory.getCurrentSession();
						final Transaction transaction = session.getTransaction();
						if (!transaction.isActive()) {
							transaction.begin();
						}
						genericHibernate.saveOrUpdate(entity);
						transaction.commit();
					}
					catch (HibernateException hex) {
						Logger.getLogger(getClass()).error("Failed to saveOrUpdate! RollbackOnly ...", hex);
						status.setRollbackOnly();
					}
					return null;
				}
			});
			return entity;
		}
		catch (TransactionException tex) {
			Logger.getLogger(getClass()).error("Failed to saveOrUpdate!", tex);
			throw tex;
		}
	}
	
}
