package biz.firstlook.commons.hibernate;

import java.io.Serializable;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.main.persist.common.IGenericDAO;

/**
 * Provides the definition of a persistence Strategy pattern.
 * 
 * @author swenmouth
 *
 * @param <E> entity being persisted
 * @param <K> primary key of entity
 */
public interface Persistor<E, K extends Serializable> {

	/**
	 * Put the entity into permanent storage.
	 * @param genericHibernate the DAO for the entity
	 * @param entity the thing to be saved
	 */
	@Transactional(rollbackFor={RuntimeException.class,Exception.class})
	public E persist(IGenericDAO<E, K> genericHibernate, E entity);

}