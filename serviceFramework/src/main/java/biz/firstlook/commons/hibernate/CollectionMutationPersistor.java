package biz.firstlook.commons.hibernate;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import biz.firstlook.commons.collections.CollectionWrapper;
import biz.firstlook.main.persist.common.IGenericDAO;

/**
 * <p>Provides an implementation of the Collection interface that persists changes to an adapted
 * entity using its associated DAO. This class implements the Wrapper or Decorator pattern. Methods
 * default to calling through to the wrapped collection object. Mutator methods initiate a
 * {@link biz.firstlook.main.persist.common.IGenericDAO#saveOrUpdate(Object)} call.</p>
 * 
 * @author swenmouth
 * 
 * @param <I> Type of the collection's elements
 * @param <E> Type of the entity which is adapted into the collection
 * @param <K> Type of the entity "primary key"
 */
public class CollectionMutationPersistor<I,E,K extends Serializable> extends CollectionWrapper<I> implements Collection<I> {
	
	protected IGenericDAO<E,K> genericHibernate;
	protected E entity;
	protected Persistor<E,K> persistor;
	
	public CollectionMutationPersistor(IGenericDAO<E,K> genericHibernate, CollectionMutatorCallback<I, E> collection, E entity) {
		this(genericHibernate, collection, entity, null);
	}
	
	public CollectionMutationPersistor(IGenericDAO<E,K> genericHibernate, CollectionMutatorCallback<I, E> collection, E entity, Persistor<E,K> persistor) {
		super(collection);
		this.genericHibernate = genericHibernate;
		this.entity = entity;
		this.persistor = persistor;
	}

	public boolean add(I o) {
		boolean changed = collection.add(o);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public boolean addAll(Collection<? extends I> c) {
		boolean changed = collection.addAll(c);
		if (changed)
			saveOrUpdate();
		return changed;
	}

	public void clear() {
		collection.clear();
		saveOrUpdate();
	}

	public Iterator<I> iterator() {
		return new IteratorMutationPersistor<I,E,K>(genericHibernate, collection.iterator(), entity, persistor);
	}

	public boolean remove(Object o) {
		boolean changed = collection.remove(o);
		if (changed)
			saveOrUpdate();
		return changed;
	}

	public boolean removeAll(Collection<?> c) {
		boolean changed = collection.removeAll(c);
		if (changed)
			saveOrUpdate();
		return changed;
	}

	public boolean retainAll(Collection<?> c) {
		boolean changed = collection.retainAll(c);
		if (changed)
			saveOrUpdate();
		return changed;
	}
	
	public E getEntity() {
		return entity;
	}
	
	public void saveOrUpdate() {
		// Why do we save the entity if we're typically modifying a collection?
		if (persistor != null) {
			entity = persistor.persist(genericHibernate, entity);
			if (collection instanceof CollectionMutatorCallback) {
				CollectionMutatorCallback<I, E> collectionCallback = (CollectionMutatorCallback<I, E>) collection;
				collectionCallback.synchEntity( entity );
			}
		}
	}
	
}
