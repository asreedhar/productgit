package biz.firstlook.commons.hibernate;

import java.util.Collection;

/**
 * This callback is made to bind {@link CollectionMutationPersistor}'s Collection parameter and it's Entity parameter
 * @author bfung
 *
 * @param <I> the type of the Collection Parameter in CollectionMutationPersistor
 * @param <E> the Entity Parameter in CollectionMutationPersistor
 */
public interface CollectionMutatorCallback<I, E> extends Collection<I>{

	/**
	 * A callback made to the collection for updating the entity that was saved.
	 * @param entity
	 */
	public void synchEntity( E entity );
}
