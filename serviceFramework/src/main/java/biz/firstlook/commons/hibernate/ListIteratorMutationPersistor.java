package biz.firstlook.commons.hibernate;

import java.io.Serializable;
import java.util.ListIterator;

import biz.firstlook.commons.collections.ListIteratorWrapper;
import biz.firstlook.main.persist.common.IGenericDAO;

/**
 * <p>Provides an implementation of the ListIterator interface that persists changes to an adapted
 * entity using its associated DAO. This class implements the Wrapper or Decorator pattern. Methods
 * default to calling through to the wrapped iterator object. Mutator methods initiate a
 * {@link biz.firstlook.main.persist.common.IGenericDAO#saveOrUpdate(Object)} call.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Type of the collection's elements
 * @param <E> Type of the entity which is adapted into the collection
 * @param <K> Type of the entity "primary key" 
 */
public class ListIteratorMutationPersistor<I,E,K extends Serializable> extends ListIteratorWrapper<I> implements ListIterator<I> {

	protected E entity;
	protected IGenericDAO<E,K> genericHibernate;
	protected Persistor<E,K> persistor;
	
	public ListIteratorMutationPersistor(IGenericDAO<E, K> genericHibernate, ListIterator<I> it, E entity, Persistor<E,K> persistor) {
		super(it);
		this.entity = entity;
		this.genericHibernate = genericHibernate;
		this.persistor = persistor;
	}

	public void add(I element) {
		it.add(element);
		saveOrUpdate();
	}

	public void remove() {
		it.remove();
		saveOrUpdate();
	}

	public void set(I element) {
		it.set(element);
		saveOrUpdate();
	}

	public void saveOrUpdate() {
		if (persistor != null)
			entity = persistor.persist(genericHibernate, entity);
	}
	
}
