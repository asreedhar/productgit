package biz.firstlook.commons.hibernate;

import java.io.Serializable;
import java.util.Iterator;

import biz.firstlook.commons.collections.IteratorWrapper;
import biz.firstlook.main.persist.common.IGenericDAO;

/**
 * <p>Provides an implementation of the Iterator interface that persists changes to an adapted
 * entity using its associated DAO. This class implements the Wrapper or Decorator pattern. Methods
 * default to calling through to the wrapped iterator object. Mutator methods initiate a
 * {@link biz.firstlook.main.persist.common.IGenericDAO#saveOrUpdate(Object)} call.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Type of the collection's elements
 * @param <E> Type of the entity which is adapted into the collection
 * @param <K> Type of the entity "primary key" 
 */
public class IteratorMutationPersistor<I,E,K extends Serializable> extends IteratorWrapper<I> implements Iterator<I> {

	IGenericDAO<E,K> genericHibernate;
	E entity;
	Persistor<E,K> persistor;
	
	public IteratorMutationPersistor(IGenericDAO<E, K> genericHibernate, Iterator<I> it, E entity, Persistor<E,K> persistor) {
		super(it);
		this.genericHibernate = genericHibernate;
		this.entity = entity;
		this.persistor = persistor;
	}

	public void remove() {
		super.remove();
		saveOrUpdate();
	}

	public void saveOrUpdate() {
		if (persistor != null)
			entity = persistor.persist(genericHibernate, entity);
	}
	
}
