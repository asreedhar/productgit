package biz.firstlook.commons.hibernate;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.Session;

import biz.firstlook.main.persist.common.IGenericDAO;

/**
 * Save an entity using the standard Hibernate persistence strategy.
 * @author swenmouth
 *
 * @param <E> the type of the entity
 * @param <K> the type of the primary-key of the entity 
 */
public class ManagedPersistor<E,K extends Serializable> implements Persistor<E,K> {

	protected SessionFactory sessionFactory;
	
	public ManagedPersistor(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public E persist(IGenericDAO<E,K> genericHibernate, E entity) {
		final Session currentSession = sessionFactory.getCurrentSession();
		long ts0 = System.currentTimeMillis();
		if (currentSession.contains(entity)) {
			genericHibernate.saveOrUpdate(entity);
		}
		else {
			entity = (E) currentSession.merge(entity);
		}
		//Logger.getLogger(getClass()).debug((System.currentTimeMillis()-ts0) + "ms");
		return entity;
	}

}
