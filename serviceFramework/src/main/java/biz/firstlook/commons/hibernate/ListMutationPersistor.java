package biz.firstlook.commons.hibernate;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import biz.firstlook.commons.collections.ListWrapper;
import biz.firstlook.main.persist.common.IGenericDAO;

/**
 * <p>Provides an implementation of the List interface that persists changes to an adapted
 * entity using its associated DAO. This class implements the Wrapper or Decorator pattern. Methods
 * default to calling through to the wrapped list object. Mutator methods initiate a
 * {@link biz.firstlook.main.persist.common.IGenericDAO#saveOrUpdate(Object)} call.</p>
 * 
 * @author swenmouth
 *
 * @param <I> Type of the collection's elements
 * @param <E> Type of the entity which is adapted into the collection
 * @param <K> Type of the entity "primary key" 
 */
public class ListMutationPersistor<I,E,K extends Serializable> extends ListWrapper<I> implements List<I> {

	protected IGenericDAO<E,K> genericHibernate;
	protected E entity;
	protected Persistor<E,K> persistor;
	
	public ListMutationPersistor(IGenericDAO<E, K> genericHibernate, List<I> list, E entity) {
		this(genericHibernate, list, entity, null);
	}

	public ListMutationPersistor(IGenericDAO<E, K> genericHibernate, List<I> list, E entity, Persistor<E, K> persistor) {
		super(list);
		this.genericHibernate = genericHibernate;
		this.entity = entity;
		this.persistor = persistor;
	}

	public boolean add(I o) {
		boolean changed = getList().add(o);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public void add(int index, I element) {
		getList().add(index, element);
		saveOrUpdate();
	}

	public boolean addAll(Collection<? extends I> c) {
		boolean changed = getList().addAll(c);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public boolean addAll(int index, Collection<? extends I> c) {
		boolean changed = getList().addAll(index, c);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public void clear() {
		getList().clear();
		saveOrUpdate();
	}

	public Iterator<I> iterator() {
		return new IteratorMutationPersistor<I,E,K>(genericHibernate, getList().iterator(), entity, persistor);
	}

	public ListIterator<I> listIterator() {
		return new ListIteratorMutationPersistor<I,E,K>(genericHibernate, getList().listIterator(), entity, persistor);
	}

	public ListIterator<I> listIterator(int index) {
		return new ListIteratorMutationPersistor<I,E,K>(genericHibernate, getList().listIterator(index), entity, persistor);
	}

	public I remove(int index) {
		I element = getList().remove(index);
		saveOrUpdate();
		return element;
	}

	public boolean remove(Object o) {
		boolean changed = getList().remove(o);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public boolean removeAll(Collection<?> c) {
		boolean changed = getList().removeAll(c);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public boolean retainAll(Collection<?> c) {
		boolean changed = getList().retainAll(c);
		if (changed) {
			saveOrUpdate();
		}
		return changed;
	}

	public I set(int index, I element) {
		I replace = getList().set(index, element);
		saveOrUpdate();
		return replace;
	}

	public List<I> subList(int fromIndex, int toIndex) {
		return new ListMutationPersistor<I,E,K>(genericHibernate, getList().subList(fromIndex, toIndex), entity);
	}

	public E getEntity() {
		return entity;
	}
	
	public void saveOrUpdate() {
		if (persistor != null)
			entity = persistor.persist(genericHibernate, entity);
	}
	
}
