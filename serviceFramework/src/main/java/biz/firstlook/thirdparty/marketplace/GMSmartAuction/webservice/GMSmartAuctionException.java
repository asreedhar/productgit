/**
 * Exception.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class GMSmartAuctionException  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -286974344426763692L;
	private java.lang.String type;
    private java.lang.String description;
    private java.lang.Boolean chargeableFlag;
    private java.math.BigDecimal frameHours;
    private java.math.BigDecimal paintHours;
    private java.math.BigDecimal partCost;
    private java.math.BigDecimal metalHours;
    private java.math.BigDecimal repairHours;
    private java.math.BigDecimal total;
    private java.lang.String location;

    public GMSmartAuctionException() {
    }

    public GMSmartAuctionException(
           java.lang.String type,
           java.lang.String description,
           java.lang.Boolean chargeableFlag,
           java.math.BigDecimal frameHours,
           java.math.BigDecimal paintHours,
           java.math.BigDecimal partCost,
           java.math.BigDecimal metalHours,
           java.math.BigDecimal repairHours,
           java.math.BigDecimal total,
           java.lang.String location) {
           this.type = type;
           this.description = description;
           this.chargeableFlag = chargeableFlag;
           this.frameHours = frameHours;
           this.paintHours = paintHours;
           this.partCost = partCost;
           this.metalHours = metalHours;
           this.repairHours = repairHours;
           this.total = total;
           this.location = location;
    }


    /**
     * Gets the type value for this Exception.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this Exception.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the description value for this Exception.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Exception.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the chargeableFlag value for this Exception.
     * 
     * @return chargeableFlag
     */
    public java.lang.Boolean getChargeableFlag() {
        return chargeableFlag;
    }


    /**
     * Sets the chargeableFlag value for this Exception.
     * 
     * @param chargeableFlag
     */
    public void setChargeableFlag(java.lang.Boolean chargeableFlag) {
        this.chargeableFlag = chargeableFlag;
    }


    /**
     * Gets the frameHours value for this Exception.
     * 
     * @return frameHours
     */
    public java.math.BigDecimal getFrameHours() {
        return frameHours;
    }


    /**
     * Sets the frameHours value for this Exception.
     * 
     * @param frameHours
     */
    public void setFrameHours(java.math.BigDecimal frameHours) {
        this.frameHours = frameHours;
    }


    /**
     * Gets the paintHours value for this Exception.
     * 
     * @return paintHours
     */
    public java.math.BigDecimal getPaintHours() {
        return paintHours;
    }


    /**
     * Sets the paintHours value for this Exception.
     * 
     * @param paintHours
     */
    public void setPaintHours(java.math.BigDecimal paintHours) {
        this.paintHours = paintHours;
    }


    /**
     * Gets the partCost value for this Exception.
     * 
     * @return partCost
     */
    public java.math.BigDecimal getPartCost() {
        return partCost;
    }


    /**
     * Sets the partCost value for this Exception.
     * 
     * @param partCost
     */
    public void setPartCost(java.math.BigDecimal partCost) {
        this.partCost = partCost;
    }


    /**
     * Gets the metalHours value for this Exception.
     * 
     * @return metalHours
     */
    public java.math.BigDecimal getMetalHours() {
        return metalHours;
    }


    /**
     * Sets the metalHours value for this Exception.
     * 
     * @param metalHours
     */
    public void setMetalHours(java.math.BigDecimal metalHours) {
        this.metalHours = metalHours;
    }


    /**
     * Gets the repairHours value for this Exception.
     * 
     * @return repairHours
     */
    public java.math.BigDecimal getRepairHours() {
        return repairHours;
    }


    /**
     * Sets the repairHours value for this Exception.
     * 
     * @param repairHours
     */
    public void setRepairHours(java.math.BigDecimal repairHours) {
        this.repairHours = repairHours;
    }


    /**
     * Gets the total value for this Exception.
     * 
     * @return total
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }


    /**
     * Sets the total value for this Exception.
     * 
     * @param total
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }


    /**
     * Gets the location value for this Exception.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this Exception.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GMSmartAuctionException)) return false;
        GMSmartAuctionException other = (GMSmartAuctionException) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.chargeableFlag==null && other.getChargeableFlag()==null) || 
             (this.chargeableFlag!=null &&
              this.chargeableFlag.equals(other.getChargeableFlag()))) &&
            ((this.frameHours==null && other.getFrameHours()==null) || 
             (this.frameHours!=null &&
              this.frameHours.equals(other.getFrameHours()))) &&
            ((this.paintHours==null && other.getPaintHours()==null) || 
             (this.paintHours!=null &&
              this.paintHours.equals(other.getPaintHours()))) &&
            ((this.partCost==null && other.getPartCost()==null) || 
             (this.partCost!=null &&
              this.partCost.equals(other.getPartCost()))) &&
            ((this.metalHours==null && other.getMetalHours()==null) || 
             (this.metalHours!=null &&
              this.metalHours.equals(other.getMetalHours()))) &&
            ((this.repairHours==null && other.getRepairHours()==null) || 
             (this.repairHours!=null &&
              this.repairHours.equals(other.getRepairHours()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getChargeableFlag() != null) {
            _hashCode += getChargeableFlag().hashCode();
        }
        if (getFrameHours() != null) {
            _hashCode += getFrameHours().hashCode();
        }
        if (getPaintHours() != null) {
            _hashCode += getPaintHours().hashCode();
        }
        if (getPartCost() != null) {
            _hashCode += getPartCost().hashCode();
        }
        if (getMetalHours() != null) {
            _hashCode += getMetalHours().hashCode();
        }
        if (getRepairHours() != null) {
            _hashCode += getRepairHours().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GMSmartAuctionException.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Exception"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeableFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "ChargeableFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("frameHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "FrameHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paintHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "PaintHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "PartCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metalHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "MetalHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("repairHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "RepairHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
