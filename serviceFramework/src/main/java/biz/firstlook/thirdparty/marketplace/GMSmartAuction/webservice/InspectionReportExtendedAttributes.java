/**
 * InspectionReportExtendedAttributes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class InspectionReportExtendedAttributes  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 9210829982844133200L;
	private InspectionReportExtendedAttributesOwner owner;
    private java.lang.Boolean newPosting;
    private java.math.BigDecimal postingPrice;
    private java.lang.String titleState;
    private java.lang.String subOwner;

    public InspectionReportExtendedAttributes() {
    }

    public InspectionReportExtendedAttributes(
           InspectionReportExtendedAttributesOwner owner,
           java.lang.Boolean newPosting,
           java.math.BigDecimal postingPrice,
           java.lang.String titleState,
           java.lang.String subOwner) {
           this.owner = owner;
           this.newPosting = newPosting;
           this.postingPrice = postingPrice;
           this.titleState = titleState;
           this.subOwner = subOwner;
    }


    /**
     * Gets the owner value for this InspectionReportExtendedAttributes.
     * 
     * @return owner
     */
    public InspectionReportExtendedAttributesOwner getOwner() {
        return owner;
    }


    /**
     * Sets the owner value for this InspectionReportExtendedAttributes.
     * 
     * @param owner
     */
    public void setOwner(InspectionReportExtendedAttributesOwner owner) {
        this.owner = owner;
    }


    /**
     * Gets the newPosting value for this InspectionReportExtendedAttributes.
     * 
     * @return newPosting
     */
    public java.lang.Boolean getNewPosting() {
        return newPosting;
    }


    /**
     * Sets the newPosting value for this InspectionReportExtendedAttributes.
     * 
     * @param newPosting
     */
    public void setNewPosting(java.lang.Boolean newPosting) {
        this.newPosting = newPosting;
    }


    /**
     * Gets the postingPrice value for this InspectionReportExtendedAttributes.
     * 
     * @return postingPrice
     */
    public java.math.BigDecimal getPostingPrice() {
        return postingPrice;
    }


    /**
     * Sets the postingPrice value for this InspectionReportExtendedAttributes.
     * 
     * @param postingPrice
     */
    public void setPostingPrice(java.math.BigDecimal postingPrice) {
        this.postingPrice = postingPrice;
    }


    /**
     * Gets the titleState value for this InspectionReportExtendedAttributes.
     * 
     * @return titleState
     */
    public java.lang.String getTitleState() {
        return titleState;
    }


    /**
     * Sets the titleState value for this InspectionReportExtendedAttributes.
     * 
     * @param titleState
     */
    public void setTitleState(java.lang.String titleState) {
        this.titleState = titleState;
    }


    /**
     * Gets the subOwner value for this InspectionReportExtendedAttributes.
     * 
     * @return subOwner
     */
    public java.lang.String getSubOwner() {
        return subOwner;
    }


    /**
     * Sets the subOwner value for this InspectionReportExtendedAttributes.
     * 
     * @param subOwner
     */
    public void setSubOwner(java.lang.String subOwner) {
        this.subOwner = subOwner;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InspectionReportExtendedAttributes)) return false;
        InspectionReportExtendedAttributes other = (InspectionReportExtendedAttributes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.owner==null && other.getOwner()==null) || 
             (this.owner!=null &&
              this.owner.equals(other.getOwner()))) &&
            ((this.newPosting==null && other.getNewPosting()==null) || 
             (this.newPosting!=null &&
              this.newPosting.equals(other.getNewPosting()))) &&
            ((this.postingPrice==null && other.getPostingPrice()==null) || 
             (this.postingPrice!=null &&
              this.postingPrice.equals(other.getPostingPrice()))) &&
            ((this.titleState==null && other.getTitleState()==null) || 
             (this.titleState!=null &&
              this.titleState.equals(other.getTitleState()))) &&
            ((this.subOwner==null && other.getSubOwner()==null) || 
             (this.subOwner!=null &&
              this.subOwner.equals(other.getSubOwner())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOwner() != null) {
            _hashCode += getOwner().hashCode();
        }
        if (getNewPosting() != null) {
            _hashCode += getNewPosting().hashCode();
        }
        if (getPostingPrice() != null) {
            _hashCode += getPostingPrice().hashCode();
        }
        if (getTitleState() != null) {
            _hashCode += getTitleState().hashCode();
        }
        if (getSubOwner() != null) {
            _hashCode += getSubOwner().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InspectionReportExtendedAttributes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReportExtendedAttributes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("owner");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Owner"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReportExtendedAttributesOwner"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newPosting");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "NewPosting"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postingPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "PostingPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "TitleState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subOwner");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "SubOwner"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
