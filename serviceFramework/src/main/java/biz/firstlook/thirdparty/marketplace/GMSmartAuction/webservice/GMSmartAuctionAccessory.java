/**
 * Accessory.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class GMSmartAuctionAccessory  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5413406234091689837L;
	private java.lang.String accessoryID;
    private java.lang.String description;

    public GMSmartAuctionAccessory() {
    }

    public GMSmartAuctionAccessory(
           java.lang.String accessoryID,
           java.lang.String description) {
           this.accessoryID = accessoryID;
           this.description = description;
    }


    /**
     * Gets the accessoryID value for this Accessory.
     * 
     * @return accessoryID
     */
    public java.lang.String getAccessoryID() {
        return accessoryID;
    }


    /**
     * Sets the accessoryID value for this Accessory.
     * 
     * @param accessoryID
     */
    public void setAccessoryID(java.lang.String accessoryID) {
        this.accessoryID = accessoryID;
    }


    /**
     * Gets the description value for this Accessory.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Accessory.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GMSmartAuctionAccessory)) return false;
        GMSmartAuctionAccessory other = (GMSmartAuctionAccessory) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accessoryID==null && other.getAccessoryID()==null) || 
             (this.accessoryID!=null &&
              this.accessoryID.equals(other.getAccessoryID()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccessoryID() != null) {
            _hashCode += getAccessoryID().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GMSmartAuctionAccessory.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Accessory"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accessoryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "AccessoryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
