/**
 * Vehicle.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class GMSmartAuctionVehicle  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7643606647460860176L;
	private GMSmartAuctionAccessory[] accessory;
    private GMSmartAuctionTire[] tire;
    private java.lang.String VIN;
    private java.lang.String year;
    private java.lang.String make;
    private java.lang.String model;
    private java.lang.String series;
    private java.lang.String exteriorColor;
    private java.lang.String interiorColor;
    private java.lang.String licensePlate;
    private java.lang.String licenseState;
    private long mileage;
    private java.lang.String interiorMaterial;
    private java.lang.String engineType;
    private java.lang.String engineLiters;
    private java.lang.String engineCylinder;
    private java.lang.String transmissionType;
    private java.lang.String turboSuperCharged;
    private java.lang.Boolean smokerFlag;
    private java.lang.String comments;

    public GMSmartAuctionVehicle() {
    }

    public GMSmartAuctionVehicle(
           GMSmartAuctionAccessory[] accessory,
           GMSmartAuctionTire[] tire,
           java.lang.String VIN,
           java.lang.String year,
           java.lang.String make,
           java.lang.String model,
           java.lang.String series,
           java.lang.String exteriorColor,
           java.lang.String interiorColor,
           java.lang.String licensePlate,
           java.lang.String licenseState,
           long mileage,
           java.lang.String interiorMaterial,
           java.lang.String engineType,
           java.lang.String engineLiters,
           java.lang.String engineCylinder,
           java.lang.String transmissionType,
           java.lang.String turboSuperCharged,
           java.lang.Boolean smokerFlag,
           java.lang.String comments) {
           this.accessory = accessory;
           this.tire = tire;
           this.VIN = VIN;
           this.year = year;
           this.make = make;
           this.model = model;
           this.series = series;
           this.exteriorColor = exteriorColor;
           this.interiorColor = interiorColor;
           this.licensePlate = licensePlate;
           this.licenseState = licenseState;
           this.mileage = mileage;
           this.interiorMaterial = interiorMaterial;
           this.engineType = engineType;
           this.engineLiters = engineLiters;
           this.engineCylinder = engineCylinder;
           this.transmissionType = transmissionType;
           this.turboSuperCharged = turboSuperCharged;
           this.smokerFlag = smokerFlag;
           this.comments = comments;
    }


    /**
     * Gets the accessory value for this Vehicle.
     * 
     * @return accessory
     */
    public GMSmartAuctionAccessory[] getAccessory() {
        return accessory;
    }


    /**
     * Sets the accessory value for this Vehicle.
     * 
     * @param accessory
     */
    public void setAccessory(GMSmartAuctionAccessory[] accessory) {
        this.accessory = accessory;
    }

    public GMSmartAuctionAccessory getAccessory(int i) {
        return this.accessory[i];
    }

    public void setAccessory(int i, GMSmartAuctionAccessory _value) {
        this.accessory[i] = _value;
    }


    /**
     * Gets the tire value for this Vehicle.
     * 
     * @return tire
     */
    public GMSmartAuctionTire[] getTire() {
        return tire;
    }


    /**
     * Sets the tire value for this Vehicle.
     * 
     * @param tire
     */
    public void setTire(GMSmartAuctionTire[] tire) {
        this.tire = tire;
    }

    public GMSmartAuctionTire getTire(int i) {
        return this.tire[i];
    }

    public void setTire(int i, GMSmartAuctionTire _value) {
        this.tire[i] = _value;
    }


    /**
     * Gets the VIN value for this Vehicle.
     * 
     * @return VIN
     */
    public java.lang.String getVIN() {
        return VIN;
    }


    /**
     * Sets the VIN value for this Vehicle.
     * 
     * @param VIN
     */
    public void setVIN(java.lang.String VIN) {
        this.VIN = VIN;
    }


    /**
     * Gets the year value for this Vehicle.
     * 
     * @return year
     */
    public java.lang.String getYear() {
        return year;
    }


    /**
     * Sets the year value for this Vehicle.
     * 
     * @param year
     */
    public void setYear(java.lang.String year) {
        this.year = year;
    }


    /**
     * Gets the make value for this Vehicle.
     * 
     * @return make
     */
    public java.lang.String getMake() {
        return make;
    }


    /**
     * Sets the make value for this Vehicle.
     * 
     * @param make
     */
    public void setMake(java.lang.String make) {
        this.make = make;
    }


    /**
     * Gets the model value for this Vehicle.
     * 
     * @return model
     */
    public java.lang.String getModel() {
        return model;
    }


    /**
     * Sets the model value for this Vehicle.
     * 
     * @param model
     */
    public void setModel(java.lang.String model) {
        this.model = model;
    }


    /**
     * Gets the series value for this Vehicle.
     * 
     * @return series
     */
    public java.lang.String getSeries() {
        return series;
    }


    /**
     * Sets the series value for this Vehicle.
     * 
     * @param series
     */
    public void setSeries(java.lang.String series) {
        this.series = series;
    }


    /**
     * Gets the exteriorColor value for this Vehicle.
     * 
     * @return exteriorColor
     */
    public java.lang.String getExteriorColor() {
        return exteriorColor;
    }


    /**
     * Sets the exteriorColor value for this Vehicle.
     * 
     * @param exteriorColor
     */
    public void setExteriorColor(java.lang.String exteriorColor) {
        this.exteriorColor = exteriorColor;
    }


    /**
     * Gets the interiorColor value for this Vehicle.
     * 
     * @return interiorColor
     */
    public java.lang.String getInteriorColor() {
        return interiorColor;
    }


    /**
     * Sets the interiorColor value for this Vehicle.
     * 
     * @param interiorColor
     */
    public void setInteriorColor(java.lang.String interiorColor) {
        this.interiorColor = interiorColor;
    }


    /**
     * Gets the licensePlate value for this Vehicle.
     * 
     * @return licensePlate
     */
    public java.lang.String getLicensePlate() {
        return licensePlate;
    }


    /**
     * Sets the licensePlate value for this Vehicle.
     * 
     * @param licensePlate
     */
    public void setLicensePlate(java.lang.String licensePlate) {
        this.licensePlate = licensePlate;
    }


    /**
     * Gets the licenseState value for this Vehicle.
     * 
     * @return licenseState
     */
    public java.lang.String getLicenseState() {
        return licenseState;
    }


    /**
     * Sets the licenseState value for this Vehicle.
     * 
     * @param licenseState
     */
    public void setLicenseState(java.lang.String licenseState) {
        this.licenseState = licenseState;
    }


    /**
     * Gets the mileage value for this Vehicle.
     * 
     * @return mileage
     */
    public long getMileage() {
        return mileage;
    }


    /**
     * Sets the mileage value for this Vehicle.
     * 
     * @param mileage
     */
    public void setMileage(long mileage) {
        this.mileage = mileage;
    }


    /**
     * Gets the interiorMaterial value for this Vehicle.
     * 
     * @return interiorMaterial
     */
    public java.lang.String getInteriorMaterial() {
        return interiorMaterial;
    }


    /**
     * Sets the interiorMaterial value for this Vehicle.
     * 
     * @param interiorMaterial
     */
    public void setInteriorMaterial(java.lang.String interiorMaterial) {
        this.interiorMaterial = interiorMaterial;
    }


    /**
     * Gets the engineType value for this Vehicle.
     * 
     * @return engineType
     */
    public java.lang.String getEngineType() {
        return engineType;
    }


    /**
     * Sets the engineType value for this Vehicle.
     * 
     * @param engineType
     */
    public void setEngineType(java.lang.String engineType) {
        this.engineType = engineType;
    }


    /**
     * Gets the engineLiters value for this Vehicle.
     * 
     * @return engineLiters
     */
    public java.lang.String getEngineLiters() {
        return engineLiters;
    }


    /**
     * Sets the engineLiters value for this Vehicle.
     * 
     * @param engineLiters
     */
    public void setEngineLiters(java.lang.String engineLiters) {
        this.engineLiters = engineLiters;
    }


    /**
     * Gets the engineCylinder value for this Vehicle.
     * 
     * @return engineCylinder
     */
    public java.lang.String getEngineCylinder() {
        return engineCylinder;
    }


    /**
     * Sets the engineCylinder value for this Vehicle.
     * 
     * @param engineCylinder
     */
    public void setEngineCylinder(java.lang.String engineCylinder) {
        this.engineCylinder = engineCylinder;
    }


    /**
     * Gets the transmissionType value for this Vehicle.
     * 
     * @return transmissionType
     */
    public java.lang.String getTransmissionType() {
        return transmissionType;
    }


    /**
     * Sets the transmissionType value for this Vehicle.
     * 
     * @param transmissionType
     */
    public void setTransmissionType(java.lang.String transmissionType) {
        this.transmissionType = transmissionType;
    }


    /**
     * Gets the turboSuperCharged value for this Vehicle.
     * 
     * @return turboSuperCharged
     */
    public java.lang.String getTurboSuperCharged() {
        return turboSuperCharged;
    }


    /**
     * Sets the turboSuperCharged value for this Vehicle.
     * 
     * @param turboSuperCharged
     */
    public void setTurboSuperCharged(java.lang.String turboSuperCharged) {
        this.turboSuperCharged = turboSuperCharged;
    }


    /**
     * Gets the smokerFlag value for this Vehicle.
     * 
     * @return smokerFlag
     */
    public java.lang.Boolean getSmokerFlag() {
        return smokerFlag;
    }


    /**
     * Sets the smokerFlag value for this Vehicle.
     * 
     * @param smokerFlag
     */
    public void setSmokerFlag(java.lang.Boolean smokerFlag) {
        this.smokerFlag = smokerFlag;
    }


    /**
     * Gets the comments value for this Vehicle.
     * 
     * @return comments
     */
    public java.lang.String getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this Vehicle.
     * 
     * @param comments
     */
    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GMSmartAuctionVehicle)) return false;
        GMSmartAuctionVehicle other = (GMSmartAuctionVehicle) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accessory==null && other.getAccessory()==null) || 
             (this.accessory!=null &&
              java.util.Arrays.equals(this.accessory, other.getAccessory()))) &&
            ((this.tire==null && other.getTire()==null) || 
             (this.tire!=null &&
              java.util.Arrays.equals(this.tire, other.getTire()))) &&
            ((this.VIN==null && other.getVIN()==null) || 
             (this.VIN!=null &&
              this.VIN.equals(other.getVIN()))) &&
            ((this.year==null && other.getYear()==null) || 
             (this.year!=null &&
              this.year.equals(other.getYear()))) &&
            ((this.make==null && other.getMake()==null) || 
             (this.make!=null &&
              this.make.equals(other.getMake()))) &&
            ((this.model==null && other.getModel()==null) || 
             (this.model!=null &&
              this.model.equals(other.getModel()))) &&
            ((this.series==null && other.getSeries()==null) || 
             (this.series!=null &&
              this.series.equals(other.getSeries()))) &&
            ((this.exteriorColor==null && other.getExteriorColor()==null) || 
             (this.exteriorColor!=null &&
              this.exteriorColor.equals(other.getExteriorColor()))) &&
            ((this.interiorColor==null && other.getInteriorColor()==null) || 
             (this.interiorColor!=null &&
              this.interiorColor.equals(other.getInteriorColor()))) &&
            ((this.licensePlate==null && other.getLicensePlate()==null) || 
             (this.licensePlate!=null &&
              this.licensePlate.equals(other.getLicensePlate()))) &&
            ((this.licenseState==null && other.getLicenseState()==null) || 
             (this.licenseState!=null &&
              this.licenseState.equals(other.getLicenseState()))) &&
            this.mileage == other.getMileage() &&
            ((this.interiorMaterial==null && other.getInteriorMaterial()==null) || 
             (this.interiorMaterial!=null &&
              this.interiorMaterial.equals(other.getInteriorMaterial()))) &&
            ((this.engineType==null && other.getEngineType()==null) || 
             (this.engineType!=null &&
              this.engineType.equals(other.getEngineType()))) &&
            ((this.engineLiters==null && other.getEngineLiters()==null) || 
             (this.engineLiters!=null &&
              this.engineLiters.equals(other.getEngineLiters()))) &&
            ((this.engineCylinder==null && other.getEngineCylinder()==null) || 
             (this.engineCylinder!=null &&
              this.engineCylinder.equals(other.getEngineCylinder()))) &&
            ((this.transmissionType==null && other.getTransmissionType()==null) || 
             (this.transmissionType!=null &&
              this.transmissionType.equals(other.getTransmissionType()))) &&
            ((this.turboSuperCharged==null && other.getTurboSuperCharged()==null) || 
             (this.turboSuperCharged!=null &&
              this.turboSuperCharged.equals(other.getTurboSuperCharged()))) &&
            ((this.smokerFlag==null && other.getSmokerFlag()==null) || 
             (this.smokerFlag!=null &&
              this.smokerFlag.equals(other.getSmokerFlag()))) &&
            ((this.comments==null && other.getComments()==null) || 
             (this.comments!=null &&
              this.comments.equals(other.getComments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccessory() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccessory());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccessory(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTire() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTire());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTire(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getVIN() != null) {
            _hashCode += getVIN().hashCode();
        }
        if (getYear() != null) {
            _hashCode += getYear().hashCode();
        }
        if (getMake() != null) {
            _hashCode += getMake().hashCode();
        }
        if (getModel() != null) {
            _hashCode += getModel().hashCode();
        }
        if (getSeries() != null) {
            _hashCode += getSeries().hashCode();
        }
        if (getExteriorColor() != null) {
            _hashCode += getExteriorColor().hashCode();
        }
        if (getInteriorColor() != null) {
            _hashCode += getInteriorColor().hashCode();
        }
        if (getLicensePlate() != null) {
            _hashCode += getLicensePlate().hashCode();
        }
        if (getLicenseState() != null) {
            _hashCode += getLicenseState().hashCode();
        }
        _hashCode += new Long(getMileage()).hashCode();
        if (getInteriorMaterial() != null) {
            _hashCode += getInteriorMaterial().hashCode();
        }
        if (getEngineType() != null) {
            _hashCode += getEngineType().hashCode();
        }
        if (getEngineLiters() != null) {
            _hashCode += getEngineLiters().hashCode();
        }
        if (getEngineCylinder() != null) {
            _hashCode += getEngineCylinder().hashCode();
        }
        if (getTransmissionType() != null) {
            _hashCode += getTransmissionType().hashCode();
        }
        if (getTurboSuperCharged() != null) {
            _hashCode += getTurboSuperCharged().hashCode();
        }
        if (getSmokerFlag() != null) {
            _hashCode += getSmokerFlag().hashCode();
        }
        if (getComments() != null) {
            _hashCode += getComments().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GMSmartAuctionVehicle.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Vehicle"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accessory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Accessory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Accessory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tire");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Tire"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Tire"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "VIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("year");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Year"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("make");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Make"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("model");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Model"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("series");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Series"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exteriorColor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "ExteriorColor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interiorColor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InteriorColor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licensePlate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "LicensePlate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "LicenseState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mileage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Mileage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interiorMaterial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InteriorMaterial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "EngineType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineLiters");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "EngineLiters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineCylinder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "EngineCylinder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "TransmissionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("turboSuperCharged");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "TurboSuperCharged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("smokerFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "SmokerFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Comments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
