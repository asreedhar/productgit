/**
 * DIPostServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public interface IGMSmartAuctionSoapService extends java.rmi.Remote {
    public GMSmartAuctionResponse postInspectionReport(InspectionReportFile inspectionReportFile, java.lang.String vcDesc, java.lang.String stockNo, java.lang.String userId, java.lang.String password) throws java.rmi.RemoteException;
}
