/**
 * DIPostService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public interface IGMSmartAuctionServiceLocator extends javax.xml.rpc.Service {

/**
 * WebService for posting Inspection Reports to the GMAC VIW System
 */
    public java.lang.String getDIPostServiceSoapAddress();

    public IGMSmartAuctionSoapService getDIPostServiceSoap() throws javax.xml.rpc.ServiceException;

    public IGMSmartAuctionSoapService getDIPostServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
