package biz.firstlook.thirdparty.marketplace;



public class ThirdPartyMarketPlaceException extends Exception
{

/**
	 * 
	 */
	private static final long serialVersionUID = -3381128743146078048L;

public ThirdPartyMarketPlaceException()
{
}

public ThirdPartyMarketPlaceException( String message )
{
    super(message);
}

public ThirdPartyMarketPlaceException( String message, Throwable cause )
{
    super(message, cause);
}
}