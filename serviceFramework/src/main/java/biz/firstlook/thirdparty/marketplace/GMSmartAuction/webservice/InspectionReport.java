/**
 * InspectionReport.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class InspectionReport  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7143141633675554244L;
	private GMSmartAuctionVehicle vehicle;
    private InspectionReportImage[] image;
    private GMSmartAuctionException[] exception;
    private java.lang.String GMACAccount;
    private java.lang.String customerName;
    private java.lang.String cobuyerName;
    private java.math.BigInteger conditionGrade;
    private java.util.Date dateReturned;
    private java.lang.String dealerName;
    private java.lang.String dealerNumber;
    private java.lang.String dealerPhone;
    private java.lang.String dealerContact;
    private java.lang.String dealerAddress;
    private java.lang.String dealerAddress2;
    private java.lang.String dealerCity;
    private java.lang.String dealerState;
    private java.lang.String dealerZip;
    private java.lang.String inspectorID;
    private java.util.Date inspectionDate;
    private java.lang.String comments;
    private java.util.Date inspectionRequestDate;
    private java.math.BigDecimal chargeableTotal;
    private java.math.BigDecimal nonChargeableTotal;
    private java.math.BigDecimal total;
    private InspectionReportExtendedAttributes extendedAttributes;

    public InspectionReport() {
    }

    public InspectionReport(
           GMSmartAuctionVehicle vehicle,
           InspectionReportImage[] image,
           GMSmartAuctionException[] exception,
           java.lang.String GMACAccount,
           java.lang.String customerName,
           java.lang.String cobuyerName,
           java.math.BigInteger conditionGrade,
           java.util.Date dateReturned,
           java.lang.String dealerName,
           java.lang.String dealerNumber,
           java.lang.String dealerPhone,
           java.lang.String dealerContact,
           java.lang.String dealerAddress,
           java.lang.String dealerAddress2,
           java.lang.String dealerCity,
           java.lang.String dealerState,
           java.lang.String dealerZip,
           java.lang.String inspectorID,
           java.util.Date inspectionDate,
           java.lang.String comments,
           java.util.Date inspectionRequestDate,
           java.math.BigDecimal chargeableTotal,
           java.math.BigDecimal nonChargeableTotal,
           java.math.BigDecimal total,
           InspectionReportExtendedAttributes extendedAttributes) {
           this.vehicle = vehicle;
           this.image = image;
           this.exception = exception;
           this.GMACAccount = GMACAccount;
           this.customerName = customerName;
           this.cobuyerName = cobuyerName;
           this.conditionGrade = conditionGrade;
           this.dateReturned = dateReturned;
           this.dealerName = dealerName;
           this.dealerNumber = dealerNumber;
           this.dealerPhone = dealerPhone;
           this.dealerContact = dealerContact;
           this.dealerAddress = dealerAddress;
           this.dealerAddress2 = dealerAddress2;
           this.dealerCity = dealerCity;
           this.dealerState = dealerState;
           this.dealerZip = dealerZip;
           this.inspectorID = inspectorID;
           this.inspectionDate = inspectionDate;
           this.comments = comments;
           this.inspectionRequestDate = inspectionRequestDate;
           this.chargeableTotal = chargeableTotal;
           this.nonChargeableTotal = nonChargeableTotal;
           this.total = total;
           this.extendedAttributes = extendedAttributes;
    }


    /**
     * Gets the vehicle value for this InspectionReport.
     * 
     * @return vehicle
     */
    public GMSmartAuctionVehicle getVehicle() {
        return vehicle;
    }


    /**
     * Sets the vehicle value for this InspectionReport.
     * 
     * @param vehicle
     */
    public void setVehicle(GMSmartAuctionVehicle vehicle) {
        this.vehicle = vehicle;
    }


    /**
     * Gets the image value for this InspectionReport.
     * 
     * @return image
     */
    public InspectionReportImage[] getImage() {
        return image;
    }


    /**
     * Sets the image value for this InspectionReport.
     * 
     * @param image
     */
    public void setImage(InspectionReportImage[] image) {
        this.image = image;
    }

    public InspectionReportImage getImage(int i) {
        return this.image[i];
    }

    public void setImage(int i, InspectionReportImage _value) {
        this.image[i] = _value;
    }


    /**
     * Gets the exception value for this InspectionReport.
     * 
     * @return exception
     */
    public GMSmartAuctionException[] getException() {
        return exception;
    }


    /**
     * Sets the exception value for this InspectionReport.
     * 
     * @param exception
     */
    public void setException(GMSmartAuctionException[] exception) {
        this.exception = exception;
    }

    public GMSmartAuctionException getException(int i) {
        return this.exception[i];
    }

    public void setException(int i, GMSmartAuctionException _value) {
        this.exception[i] = _value;
    }


    /**
     * Gets the GMACAccount value for this InspectionReport.
     * 
     * @return GMACAccount
     */
    public java.lang.String getGMACAccount() {
        return GMACAccount;
    }


    /**
     * Sets the GMACAccount value for this InspectionReport.
     * 
     * @param GMACAccount
     */
    public void setGMACAccount(java.lang.String GMACAccount) {
        this.GMACAccount = GMACAccount;
    }


    /**
     * Gets the customerName value for this InspectionReport.
     * 
     * @return customerName
     */
    public java.lang.String getCustomerName() {
        return customerName;
    }


    /**
     * Sets the customerName value for this InspectionReport.
     * 
     * @param customerName
     */
    public void setCustomerName(java.lang.String customerName) {
        this.customerName = customerName;
    }


    /**
     * Gets the cobuyerName value for this InspectionReport.
     * 
     * @return cobuyerName
     */
    public java.lang.String getCobuyerName() {
        return cobuyerName;
    }


    /**
     * Sets the cobuyerName value for this InspectionReport.
     * 
     * @param cobuyerName
     */
    public void setCobuyerName(java.lang.String cobuyerName) {
        this.cobuyerName = cobuyerName;
    }


    /**
     * Gets the conditionGrade value for this InspectionReport.
     * 
     * @return conditionGrade
     */
    public java.math.BigInteger getConditionGrade() {
        return conditionGrade;
    }


    /**
     * Sets the conditionGrade value for this InspectionReport.
     * 
     * @param conditionGrade
     */
    public void setConditionGrade(java.math.BigInteger conditionGrade) {
        this.conditionGrade = conditionGrade;
    }


    /**
     * Gets the dateReturned value for this InspectionReport.
     * 
     * @return dateReturned
     */
    public java.util.Date getDateReturned() {
        return dateReturned;
    }


    /**
     * Sets the dateReturned value for this InspectionReport.
     * 
     * @param dateReturned
     */
    public void setDateReturned(java.util.Date dateReturned) {
        this.dateReturned = dateReturned;
    }


    /**
     * Gets the dealerName value for this InspectionReport.
     * 
     * @return dealerName
     */
    public java.lang.String getDealerName() {
        return dealerName;
    }


    /**
     * Sets the dealerName value for this InspectionReport.
     * 
     * @param dealerName
     */
    public void setDealerName(java.lang.String dealerName) {
        this.dealerName = dealerName;
    }


    /**
     * Gets the dealerNumber value for this InspectionReport.
     * 
     * @return dealerNumber
     */
    public java.lang.String getDealerNumber() {
        return dealerNumber;
    }


    /**
     * Sets the dealerNumber value for this InspectionReport.
     * 
     * @param dealerNumber
     */
    public void setDealerNumber(java.lang.String dealerNumber) {
        this.dealerNumber = dealerNumber;
    }


    /**
     * Gets the dealerPhone value for this InspectionReport.
     * 
     * @return dealerPhone
     */
    public java.lang.String getDealerPhone() {
        return dealerPhone;
    }


    /**
     * Sets the dealerPhone value for this InspectionReport.
     * 
     * @param dealerPhone
     */
    public void setDealerPhone(java.lang.String dealerPhone) {
        this.dealerPhone = dealerPhone;
    }


    /**
     * Gets the dealerContact value for this InspectionReport.
     * 
     * @return dealerContact
     */
    public java.lang.String getDealerContact() {
        return dealerContact;
    }


    /**
     * Sets the dealerContact value for this InspectionReport.
     * 
     * @param dealerContact
     */
    public void setDealerContact(java.lang.String dealerContact) {
        this.dealerContact = dealerContact;
    }


    /**
     * Gets the dealerAddress value for this InspectionReport.
     * 
     * @return dealerAddress
     */
    public java.lang.String getDealerAddress() {
        return dealerAddress;
    }


    /**
     * Sets the dealerAddress value for this InspectionReport.
     * 
     * @param dealerAddress
     */
    public void setDealerAddress(java.lang.String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }


    /**
     * Gets the dealerAddress2 value for this InspectionReport.
     * 
     * @return dealerAddress2
     */
    public java.lang.String getDealerAddress2() {
        return dealerAddress2;
    }


    /**
     * Sets the dealerAddress2 value for this InspectionReport.
     * 
     * @param dealerAddress2
     */
    public void setDealerAddress2(java.lang.String dealerAddress2) {
        this.dealerAddress2 = dealerAddress2;
    }


    /**
     * Gets the dealerCity value for this InspectionReport.
     * 
     * @return dealerCity
     */
    public java.lang.String getDealerCity() {
        return dealerCity;
    }


    /**
     * Sets the dealerCity value for this InspectionReport.
     * 
     * @param dealerCity
     */
    public void setDealerCity(java.lang.String dealerCity) {
        this.dealerCity = dealerCity;
    }


    /**
     * Gets the dealerState value for this InspectionReport.
     * 
     * @return dealerState
     */
    public java.lang.String getDealerState() {
        return dealerState;
    }


    /**
     * Sets the dealerState value for this InspectionReport.
     * 
     * @param dealerState
     */
    public void setDealerState(java.lang.String dealerState) {
        this.dealerState = dealerState;
    }


    /**
     * Gets the dealerZip value for this InspectionReport.
     * 
     * @return dealerZip
     */
    public java.lang.String getDealerZip() {
        return dealerZip;
    }


    /**
     * Sets the dealerZip value for this InspectionReport.
     * 
     * @param dealerZip
     */
    public void setDealerZip(java.lang.String dealerZip) {
        this.dealerZip = dealerZip;
    }


    /**
     * Gets the inspectorID value for this InspectionReport.
     * 
     * @return inspectorID
     */
    public java.lang.String getInspectorID() {
        return inspectorID;
    }


    /**
     * Sets the inspectorID value for this InspectionReport.
     * 
     * @param inspectorID
     */
    public void setInspectorID(java.lang.String inspectorID) {
        this.inspectorID = inspectorID;
    }


    /**
     * Gets the inspectionDate value for this InspectionReport.
     * 
     * @return inspectionDate
     */
    public java.util.Date getInspectionDate() {
        return inspectionDate;
    }


    /**
     * Sets the inspectionDate value for this InspectionReport.
     * 
     * @param inspectionDate
     */
    public void setInspectionDate(java.util.Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }


    /**
     * Gets the comments value for this InspectionReport.
     * 
     * @return comments
     */
    public java.lang.String getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this InspectionReport.
     * 
     * @param comments
     */
    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }


    /**
     * Gets the inspectionRequestDate value for this InspectionReport.
     * 
     * @return inspectionRequestDate
     */
    public java.util.Date getInspectionRequestDate() {
        return inspectionRequestDate;
    }


    /**
     * Sets the inspectionRequestDate value for this InspectionReport.
     * 
     * @param inspectionRequestDate
     */
    public void setInspectionRequestDate(java.util.Date inspectionRequestDate) {
        this.inspectionRequestDate = inspectionRequestDate;
    }


    /**
     * Gets the chargeableTotal value for this InspectionReport.
     * 
     * @return chargeableTotal
     */
    public java.math.BigDecimal getChargeableTotal() {
        return chargeableTotal;
    }


    /**
     * Sets the chargeableTotal value for this InspectionReport.
     * 
     * @param chargeableTotal
     */
    public void setChargeableTotal(java.math.BigDecimal chargeableTotal) {
        this.chargeableTotal = chargeableTotal;
    }


    /**
     * Gets the nonChargeableTotal value for this InspectionReport.
     * 
     * @return nonChargeableTotal
     */
    public java.math.BigDecimal getNonChargeableTotal() {
        return nonChargeableTotal;
    }


    /**
     * Sets the nonChargeableTotal value for this InspectionReport.
     * 
     * @param nonChargeableTotal
     */
    public void setNonChargeableTotal(java.math.BigDecimal nonChargeableTotal) {
        this.nonChargeableTotal = nonChargeableTotal;
    }


    /**
     * Gets the total value for this InspectionReport.
     * 
     * @return total
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }


    /**
     * Sets the total value for this InspectionReport.
     * 
     * @param total
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }


    /**
     * Gets the extendedAttributes value for this InspectionReport.
     * 
     * @return extendedAttributes
     */
    public InspectionReportExtendedAttributes getExtendedAttributes() {
        return extendedAttributes;
    }


    /**
     * Sets the extendedAttributes value for this InspectionReport.
     * 
     * @param extendedAttributes
     */
    public void setExtendedAttributes(InspectionReportExtendedAttributes extendedAttributes) {
        this.extendedAttributes = extendedAttributes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InspectionReport)) return false;
        InspectionReport other = (InspectionReport) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vehicle==null && other.getVehicle()==null) || 
             (this.vehicle!=null &&
              this.vehicle.equals(other.getVehicle()))) &&
            ((this.image==null && other.getImage()==null) || 
             (this.image!=null &&
              java.util.Arrays.equals(this.image, other.getImage()))) &&
            ((this.exception==null && other.getException()==null) || 
             (this.exception!=null &&
              java.util.Arrays.equals(this.exception, other.getException()))) &&
            ((this.GMACAccount==null && other.getGMACAccount()==null) || 
             (this.GMACAccount!=null &&
              this.GMACAccount.equals(other.getGMACAccount()))) &&
            ((this.customerName==null && other.getCustomerName()==null) || 
             (this.customerName!=null &&
              this.customerName.equals(other.getCustomerName()))) &&
            ((this.cobuyerName==null && other.getCobuyerName()==null) || 
             (this.cobuyerName!=null &&
              this.cobuyerName.equals(other.getCobuyerName()))) &&
            ((this.conditionGrade==null && other.getConditionGrade()==null) || 
             (this.conditionGrade!=null &&
              this.conditionGrade.equals(other.getConditionGrade()))) &&
            ((this.dateReturned==null && other.getDateReturned()==null) || 
             (this.dateReturned!=null &&
              this.dateReturned.equals(other.getDateReturned()))) &&
            ((this.dealerName==null && other.getDealerName()==null) || 
             (this.dealerName!=null &&
              this.dealerName.equals(other.getDealerName()))) &&
            ((this.dealerNumber==null && other.getDealerNumber()==null) || 
             (this.dealerNumber!=null &&
              this.dealerNumber.equals(other.getDealerNumber()))) &&
            ((this.dealerPhone==null && other.getDealerPhone()==null) || 
             (this.dealerPhone!=null &&
              this.dealerPhone.equals(other.getDealerPhone()))) &&
            ((this.dealerContact==null && other.getDealerContact()==null) || 
             (this.dealerContact!=null &&
              this.dealerContact.equals(other.getDealerContact()))) &&
            ((this.dealerAddress==null && other.getDealerAddress()==null) || 
             (this.dealerAddress!=null &&
              this.dealerAddress.equals(other.getDealerAddress()))) &&
            ((this.dealerAddress2==null && other.getDealerAddress2()==null) || 
             (this.dealerAddress2!=null &&
              this.dealerAddress2.equals(other.getDealerAddress2()))) &&
            ((this.dealerCity==null && other.getDealerCity()==null) || 
             (this.dealerCity!=null &&
              this.dealerCity.equals(other.getDealerCity()))) &&
            ((this.dealerState==null && other.getDealerState()==null) || 
             (this.dealerState!=null &&
              this.dealerState.equals(other.getDealerState()))) &&
            ((this.dealerZip==null && other.getDealerZip()==null) || 
             (this.dealerZip!=null &&
              this.dealerZip.equals(other.getDealerZip()))) &&
            ((this.inspectorID==null && other.getInspectorID()==null) || 
             (this.inspectorID!=null &&
              this.inspectorID.equals(other.getInspectorID()))) &&
            ((this.inspectionDate==null && other.getInspectionDate()==null) || 
             (this.inspectionDate!=null &&
              this.inspectionDate.equals(other.getInspectionDate()))) &&
            ((this.comments==null && other.getComments()==null) || 
             (this.comments!=null &&
              this.comments.equals(other.getComments()))) &&
            ((this.inspectionRequestDate==null && other.getInspectionRequestDate()==null) || 
             (this.inspectionRequestDate!=null &&
              this.inspectionRequestDate.equals(other.getInspectionRequestDate()))) &&
            ((this.chargeableTotal==null && other.getChargeableTotal()==null) || 
             (this.chargeableTotal!=null &&
              this.chargeableTotal.equals(other.getChargeableTotal()))) &&
            ((this.nonChargeableTotal==null && other.getNonChargeableTotal()==null) || 
             (this.nonChargeableTotal!=null &&
              this.nonChargeableTotal.equals(other.getNonChargeableTotal()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.extendedAttributes==null && other.getExtendedAttributes()==null) || 
             (this.extendedAttributes!=null &&
              this.extendedAttributes.equals(other.getExtendedAttributes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVehicle() != null) {
            _hashCode += getVehicle().hashCode();
        }
        if (getImage() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getImage());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getImage(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getException() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getException());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getException(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGMACAccount() != null) {
            _hashCode += getGMACAccount().hashCode();
        }
        if (getCustomerName() != null) {
            _hashCode += getCustomerName().hashCode();
        }
        if (getCobuyerName() != null) {
            _hashCode += getCobuyerName().hashCode();
        }
        if (getConditionGrade() != null) {
            _hashCode += getConditionGrade().hashCode();
        }
        if (getDateReturned() != null) {
            _hashCode += getDateReturned().hashCode();
        }
        if (getDealerName() != null) {
            _hashCode += getDealerName().hashCode();
        }
        if (getDealerNumber() != null) {
            _hashCode += getDealerNumber().hashCode();
        }
        if (getDealerPhone() != null) {
            _hashCode += getDealerPhone().hashCode();
        }
        if (getDealerContact() != null) {
            _hashCode += getDealerContact().hashCode();
        }
        if (getDealerAddress() != null) {
            _hashCode += getDealerAddress().hashCode();
        }
        if (getDealerAddress2() != null) {
            _hashCode += getDealerAddress2().hashCode();
        }
        if (getDealerCity() != null) {
            _hashCode += getDealerCity().hashCode();
        }
        if (getDealerState() != null) {
            _hashCode += getDealerState().hashCode();
        }
        if (getDealerZip() != null) {
            _hashCode += getDealerZip().hashCode();
        }
        if (getInspectorID() != null) {
            _hashCode += getInspectorID().hashCode();
        }
        if (getInspectionDate() != null) {
            _hashCode += getInspectionDate().hashCode();
        }
        if (getComments() != null) {
            _hashCode += getComments().hashCode();
        }
        if (getInspectionRequestDate() != null) {
            _hashCode += getInspectionRequestDate().hashCode();
        }
        if (getChargeableTotal() != null) {
            _hashCode += getChargeableTotal().hashCode();
        }
        if (getNonChargeableTotal() != null) {
            _hashCode += getNonChargeableTotal().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getExtendedAttributes() != null) {
            _hashCode += getExtendedAttributes().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InspectionReport.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReport"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Vehicle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Vehicle"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("image");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Image"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReportImage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exception");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Exception"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Exception"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GMACAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "GMACAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "CustomerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cobuyerName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "CobuyerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conditionGrade");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "ConditionGrade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateReturned");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DateReturned"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerContact");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerContact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerAddress2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerAddress2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerZip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DealerZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inspectorID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectorID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inspectionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Comments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inspectionRequestDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionRequestDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeableTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "ChargeableTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonChargeableTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "NonChargeableTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendedAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "ExtendedAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReportExtendedAttributes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
