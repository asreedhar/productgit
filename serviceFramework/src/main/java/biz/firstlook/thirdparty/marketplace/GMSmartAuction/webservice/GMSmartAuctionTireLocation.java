/**
 * TireLocation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class GMSmartAuctionTireLocation implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2591990241431616225L;
	private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected GMSmartAuctionTireLocation(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _FR = "FR";
    public static final java.lang.String _FL = "FL";
    public static final java.lang.String _RR = "RR";
    public static final java.lang.String _RL = "RL";
    public static final java.lang.String _SP = "SP";
    public static final java.lang.String _OT = "OT";
    public static final GMSmartAuctionTireLocation FR = new GMSmartAuctionTireLocation(_FR);
    public static final GMSmartAuctionTireLocation FL = new GMSmartAuctionTireLocation(_FL);
    public static final GMSmartAuctionTireLocation RR = new GMSmartAuctionTireLocation(_RR);
    public static final GMSmartAuctionTireLocation RL = new GMSmartAuctionTireLocation(_RL);
    public static final GMSmartAuctionTireLocation SP = new GMSmartAuctionTireLocation(_SP);
    public static final GMSmartAuctionTireLocation OT = new GMSmartAuctionTireLocation(_OT);
    public java.lang.String getValue() { return _value_;}
    public static GMSmartAuctionTireLocation fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        GMSmartAuctionTireLocation enumeration = (GMSmartAuctionTireLocation)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static GMSmartAuctionTireLocation fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GMSmartAuctionTireLocation.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "TireLocation"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
