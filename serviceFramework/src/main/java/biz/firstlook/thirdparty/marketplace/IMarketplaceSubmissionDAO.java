package biz.firstlook.thirdparty.marketplace;

import biz.firstlook.thirdparty.marketplace.entity.MarketplaceSubmission;


public interface IMarketplaceSubmissionDAO
{

public MarketplaceSubmission findActiveSubmissionByInventoryId( Integer inventoryId );
void saveOrUpdate( MarketplaceSubmission marketplaceSubmission);
}