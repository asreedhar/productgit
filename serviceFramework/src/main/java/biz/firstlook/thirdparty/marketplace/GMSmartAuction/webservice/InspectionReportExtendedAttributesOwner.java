/**
 * InspectionReportExtendedAttributesOwner.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;
public class InspectionReportExtendedAttributesOwner implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4575496414822115172L;
	private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected InspectionReportExtendedAttributesOwner(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _sfsc = "sfsc";
    public static final java.lang.String _CDMData = "CDMData";
    public static final java.lang.String _FIRSTLOOK = "FIRSTLOOK";
    public static final java.lang.String _AUCT123 = "AUCT123";
    public static final java.lang.String _AUTOUP = "AUTOUP";
    public static final java.lang.String _AAEXCHG = "AAEXCHG";
    public static final java.lang.String _DEALER = "Dealer";
    public static final java.lang.String _DLRWIRE = "DLRWIRE";
    public static final java.lang.String _DLRSPEC = "DLRSPEC";
    public static final java.lang.String _MPOWER = "MPOWER";
    public static final java.lang.String _POINT = "POINT";
    public static final java.lang.String _REYNOLDS = "REYNOLDS";
    public static final java.lang.String _ECUSTOM = "ECUSTOM";
    public static final java.lang.String _ADP = "ADP";
    public static final InspectionReportExtendedAttributesOwner sfsc = new InspectionReportExtendedAttributesOwner(_sfsc);
    public static final InspectionReportExtendedAttributesOwner CDMData = new InspectionReportExtendedAttributesOwner(_CDMData);
    public static final InspectionReportExtendedAttributesOwner FIRSTLOOK = new InspectionReportExtendedAttributesOwner(_FIRSTLOOK);
    public static final InspectionReportExtendedAttributesOwner AUCT123 = new InspectionReportExtendedAttributesOwner(_AUCT123);
    public static final InspectionReportExtendedAttributesOwner AUTOUP = new InspectionReportExtendedAttributesOwner(_AUTOUP);
    public static final InspectionReportExtendedAttributesOwner AAEXCHG = new InspectionReportExtendedAttributesOwner(_AAEXCHG);
    public static final InspectionReportExtendedAttributesOwner DEALER = new InspectionReportExtendedAttributesOwner(_DEALER);
    public static final InspectionReportExtendedAttributesOwner DLRWIRE = new InspectionReportExtendedAttributesOwner(_DLRWIRE);
    public static final InspectionReportExtendedAttributesOwner DLRSPEC = new InspectionReportExtendedAttributesOwner(_DLRSPEC);
    public static final InspectionReportExtendedAttributesOwner MPOWER = new InspectionReportExtendedAttributesOwner(_MPOWER);
    public static final InspectionReportExtendedAttributesOwner POINT = new InspectionReportExtendedAttributesOwner(_POINT);
    public static final InspectionReportExtendedAttributesOwner REYNOLDS = new InspectionReportExtendedAttributesOwner(_REYNOLDS);
    public static final InspectionReportExtendedAttributesOwner ECUSTOM = new InspectionReportExtendedAttributesOwner(_ECUSTOM);
    public static final InspectionReportExtendedAttributesOwner ADP = new InspectionReportExtendedAttributesOwner(_ADP);
    public java.lang.String getValue() { return _value_;}
    public static InspectionReportExtendedAttributesOwner fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        InspectionReportExtendedAttributesOwner enumeration = (InspectionReportExtendedAttributesOwner)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static InspectionReportExtendedAttributesOwner fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InspectionReportExtendedAttributesOwner.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReportExtendedAttributesOwner"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
