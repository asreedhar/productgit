package biz.firstlook.thirdparty.marketplace.GMSmartAuction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.Photo;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;
import biz.firstlook.thirdparty.marketplace.MarketplaceSubmissionBaseInformation;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionVehicle;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.InspectionReport;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.InspectionReportExtendedAttributes;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.InspectionReportExtendedAttributesOwner;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.InspectionReportImage;

public class GMSmartAuctionSubmissionInformation extends MarketplaceSubmissionBaseInformation
{

private String vcDesc;
private String stockNo;
private String announcments;

private boolean includePhotos;

public String getStockNo()
{
	return stockNo;
}

public void setStockNo( String stockNo )
{
	this.stockNo = stockNo;
}


public String getVcDesc()
{
	return vcDesc;
}

public void setVcDesc( String vcDesc )
{
	this.vcDesc = vcDesc;
}


public GMSmartAuctionSubmissionInformation()
{
	super();
}

public InspectionReport[] generateInspectionReports()
{
	List reportToReturn = new ArrayList();
	
	GMSmartAuctionVehicle gmSmartAuctionVehicle = new GMSmartAuctionVehicle();
	for( Inventory inventory : inventories )
	{
		InspectionReport report = new InspectionReport();
		
		// adding only the required fields for vehicle
		gmSmartAuctionVehicle.setExteriorColor( inventory.getVehicle().getBaseColor() );
		gmSmartAuctionVehicle.setMake( inventory.getVehicle().getMakeModelGrouping().getMake() );
		gmSmartAuctionVehicle.setModel( getModel( inventory.getVehicle().getMakeModelGrouping().getModel() ));
		gmSmartAuctionVehicle.setMileage( inventory.getMileageReceived().longValue() );
		gmSmartAuctionVehicle.setVIN( inventory.getVehicle().getVin() );
		gmSmartAuctionVehicle.setYear( inventory.getVehicle().getYear().toString() );
		report.setVehicle( gmSmartAuctionVehicle );

		InspectionReportExtendedAttributes extendedAttrs = new InspectionReportExtendedAttributes();
		extendedAttrs.setOwner( InspectionReportExtendedAttributesOwner.DEALER );
		report.setExtendedAttributes( extendedAttrs );
		report.setComments( announcments );
		
		if ( includePhotos )
		{
			List<InspectionReportImage> images = new ArrayList<InspectionReportImage>();
			for (String photoUrl : this.getPhotoUrls(inventory.getId()))
			{
				InspectionReportImage image = new InspectionReportImage();
				image.setFileName( photoUrl );
				images.add( image );
			}

			report.setImage( images.toArray(new InspectionReportImage[images.size()]) );
		}
		
		reportToReturn.add( report );
	}
	return (InspectionReport[])reportToReturn.toArray( new InspectionReport[ reportToReturn.size() ] );
}

private String getModel( String inModel )
{
	int lastSpace = StringUtils.lastIndexOf( inModel, ' ');
	if ( lastSpace == -1)
	{
		return inModel;
	}
	else
	{
		return inModel.substring(0, lastSpace );
	}
}

public void setAnnouncments( String announcments )
{
	this.announcments = announcments;
}

public void setIncludePhotos( boolean includePhotos )
{
	this.includePhotos = includePhotos;
}

/*
 * OK, this class is messed up.  It derives from MarketplaceSubmissionBaseInformation which has support for 
 * multiple inventory objects (base.inventories), but hides that in this subclass with the exception of the 
 * generateInspectionReports() method.  Because I do not want to perpetuate such messiness, I am adding a map
 * of inventoryid/photourls to the base class, and coding this set to accommodate multiple inventory items, 
 * so the method that does recognize the inventory list can work correctly.
 * - dh, 2011-06-30
 */
public void setPhotoUrls(int inventoryId, List<String> photoUrls)
{
	if (this.inventoryPhotoUrlsMap.containsKey(inventoryId))
	{
		this.inventoryPhotoUrlsMap.remove(inventoryId);
	}
	
	this.inventoryPhotoUrlsMap.put(inventoryId, photoUrls);
}

}
