package biz.firstlook.thirdparty.marketplace.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;

@Entity
@Table(name="Marketplace_ThirdPartyEntity")
@PrimaryKeyJoinColumn(name="MarketplaceID")
public class Marketplace_InventoryThirdParty extends InventoryThirdParty
{
/**
	 * 
	 */
	private static final long serialVersionUID = -345422065296692809L;
public static final Marketplace_InventoryThirdParty GMSMARTAUCTION = new Marketplace_InventoryThirdParty(InventoryThirdPartyEnum.INTERNET_MARKETPLACE.getId() , "GMAC Smart Auction");

public Marketplace_InventoryThirdParty()
{
	super();
}

public Marketplace_InventoryThirdParty(Integer marketplaceTypeId, String description)
{
	this.inventoryThirdPartyTypeId = marketplaceTypeId;
	this.name = description;
}

}
