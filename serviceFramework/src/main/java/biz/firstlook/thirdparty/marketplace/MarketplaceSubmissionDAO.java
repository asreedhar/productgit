package biz.firstlook.thirdparty.marketplace;

import java.util.Iterator;
import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.thirdparty.marketplace.entity.MarketplaceSubmission;

public class MarketplaceSubmissionDAO extends GenericHibernate<MarketplaceSubmission, Integer> implements IMarketplaceSubmissionDAO 
{

public MarketplaceSubmissionDAO() {
	super(MarketplaceSubmission.class);
}

public MarketplaceSubmission findActiveSubmissionByInventoryId( Integer inventoryId )
{
	MarketplaceSubmission submission = null;
	List submissions = getHibernateTemplate().find( "from biz.firstlook.thirdparty.marketplace.entity.MarketplaceSubmission where inventoryId = ? ",
													new Object[] { inventoryId } );

	// only want to return a not-expired submission
	Iterator iter = submissions.iterator();
	while ( iter.hasNext() )
	{
		submission = (MarketplaceSubmission)iter.next();
		if ( !submission.hasExpired() )
		{
			break;
		}
		submission = null;
	}
	return submission;
}

}
