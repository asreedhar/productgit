/**
 * Tire.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class GMSmartAuctionTire  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3713139062646698710L;
	private GMSmartAuctionTireLocation location;
    private java.lang.String manufacturer;
    private java.lang.String size;
    private java.lang.String tread;
    private java.lang.String comments;
    private java.math.BigDecimal damageCost;

    public GMSmartAuctionTire() {
    }

    public GMSmartAuctionTire(
           GMSmartAuctionTireLocation location,
           java.lang.String manufacturer,
           java.lang.String size,
           java.lang.String tread,
           java.lang.String comments,
           java.math.BigDecimal damageCost) {
           this.location = location;
           this.manufacturer = manufacturer;
           this.size = size;
           this.tread = tread;
           this.comments = comments;
           this.damageCost = damageCost;
    }


    /**
     * Gets the location value for this Tire.
     * 
     * @return location
     */
    public GMSmartAuctionTireLocation getLocation() {
        return location;
    }


    /**
     * Sets the location value for this Tire.
     * 
     * @param location
     */
    public void setLocation(GMSmartAuctionTireLocation location) {
        this.location = location;
    }


    /**
     * Gets the manufacturer value for this Tire.
     * 
     * @return manufacturer
     */
    public java.lang.String getManufacturer() {
        return manufacturer;
    }


    /**
     * Sets the manufacturer value for this Tire.
     * 
     * @param manufacturer
     */
    public void setManufacturer(java.lang.String manufacturer) {
        this.manufacturer = manufacturer;
    }


    /**
     * Gets the size value for this Tire.
     * 
     * @return size
     */
    public java.lang.String getSize() {
        return size;
    }


    /**
     * Sets the size value for this Tire.
     * 
     * @param size
     */
    public void setSize(java.lang.String size) {
        this.size = size;
    }


    /**
     * Gets the tread value for this Tire.
     * 
     * @return tread
     */
    public java.lang.String getTread() {
        return tread;
    }


    /**
     * Sets the tread value for this Tire.
     * 
     * @param tread
     */
    public void setTread(java.lang.String tread) {
        this.tread = tread;
    }


    /**
     * Gets the comments value for this Tire.
     * 
     * @return comments
     */
    public java.lang.String getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this Tire.
     * 
     * @param comments
     */
    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }


    /**
     * Gets the damageCost value for this Tire.
     * 
     * @return damageCost
     */
    public java.math.BigDecimal getDamageCost() {
        return damageCost;
    }


    /**
     * Sets the damageCost value for this Tire.
     * 
     * @param damageCost
     */
    public void setDamageCost(java.math.BigDecimal damageCost) {
        this.damageCost = damageCost;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GMSmartAuctionTire)) return false;
        GMSmartAuctionTire other = (GMSmartAuctionTire) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.manufacturer==null && other.getManufacturer()==null) || 
             (this.manufacturer!=null &&
              this.manufacturer.equals(other.getManufacturer()))) &&
            ((this.size==null && other.getSize()==null) || 
             (this.size!=null &&
              this.size.equals(other.getSize()))) &&
            ((this.tread==null && other.getTread()==null) || 
             (this.tread!=null &&
              this.tread.equals(other.getTread()))) &&
            ((this.comments==null && other.getComments()==null) || 
             (this.comments!=null &&
              this.comments.equals(other.getComments()))) &&
            ((this.damageCost==null && other.getDamageCost()==null) || 
             (this.damageCost!=null &&
              this.damageCost.equals(other.getDamageCost())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getManufacturer() != null) {
            _hashCode += getManufacturer().hashCode();
        }
        if (getSize() != null) {
            _hashCode += getSize().hashCode();
        }
        if (getTread() != null) {
            _hashCode += getTread().hashCode();
        }
        if (getComments() != null) {
            _hashCode += getComments().hashCode();
        }
        if (getDamageCost() != null) {
            _hashCode += getDamageCost().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GMSmartAuctionTire.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Tire"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "TireLocation"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manufacturer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Manufacturer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("size");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Size"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tread");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Tread"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "Comments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("damageCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DamageCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
