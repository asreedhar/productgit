/**
 * DIPostServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class GMSmartAuctionServiceLocator extends org.apache.axis.client.Service implements IGMSmartAuctionServiceLocator {

/**
	 * 
	 */
	private static final long serialVersionUID = -8751024242881052845L;

/**
 * WebService for posting Inspection Reports to the GMAC VIW System
 */

    public GMSmartAuctionServiceLocator() {
    }


    public GMSmartAuctionServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GMSmartAuctionServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DIPostServiceSoap
    private java.lang.String DIPostServiceSoap_address = "http://test.inspections.ally.com/GMACSAPosterService/DiPostService.asmx";

    public java.lang.String getDIPostServiceSoapAddress() {
        return DIPostServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DIPostServiceSoapWSDDServiceName = "DIPostServiceSoap";

    public java.lang.String getDIPostServiceSoapWSDDServiceName() {
        return DIPostServiceSoapWSDDServiceName;
    }

    public void setDIPostServiceSoapWSDDServiceName(java.lang.String name) {
        DIPostServiceSoapWSDDServiceName = name;
    }

    public IGMSmartAuctionSoapService getDIPostServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DIPostServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDIPostServiceSoap(endpoint);
    }

    public IGMSmartAuctionSoapService getDIPostServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            GMSmartAuctionSoapService _stub = new GMSmartAuctionSoapService(portAddress, this);
            _stub.setPortName(getDIPostServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDIPostServiceSoapEndpointAddress(java.lang.String address) {
        DIPostServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (IGMSmartAuctionSoapService.class.isAssignableFrom(serviceEndpointInterface)) {
                GMSmartAuctionSoapService _stub = new GMSmartAuctionSoapService(new java.net.URL(DIPostServiceSoap_address), this);
                _stub.setPortName(getDIPostServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DIPostServiceSoap".equals(inputPortName)) {
            return getDIPostServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.gmacinspections.com", "DIPostService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.gmacinspections.com", "DIPostServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DIPostServiceSoap".equals(portName)) {
            setDIPostServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
