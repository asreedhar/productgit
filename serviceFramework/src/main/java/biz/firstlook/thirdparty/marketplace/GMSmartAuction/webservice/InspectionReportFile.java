/**
 * InspectionReportFile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice;

public class InspectionReportFile  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6695102431710556057L;
	private InspectionReport[] inspectionReport;
    private java.util.Calendar sendDate;  // attribute
    private java.math.BigInteger totalReports;  // attribute

    public InspectionReportFile() {
    }

    public InspectionReportFile(
           InspectionReport[] inspectionReport,
           java.util.Calendar sendDate,
           java.math.BigInteger totalReports) {
           this.inspectionReport = inspectionReport;
           this.sendDate = sendDate;
           this.totalReports = totalReports;
    }


    /**
     * Gets the inspectionReport value for this InspectionReportFile.
     * 
     * @return inspectionReport
     */
    public InspectionReport[] getInspectionReport() {
        return inspectionReport;
    }


    /**
     * Sets the inspectionReport value for this InspectionReportFile.
     * 
     * @param inspectionReport
     */
    public void setInspectionReport(InspectionReport[] inspectionReport) {
        this.inspectionReport = inspectionReport;
    }

    public InspectionReport getInspectionReport(int i) {
        return this.inspectionReport[i];
    }

    public void setInspectionReport(int i, InspectionReport _value) {
        this.inspectionReport[i] = _value;
    }


    /**
     * Gets the sendDate value for this InspectionReportFile.
     * 
     * @return sendDate
     */
    public java.util.Calendar getSendDate() {
        return sendDate;
    }


    /**
     * Sets the sendDate value for this InspectionReportFile.
     * 
     * @param sendDate
     */
    public void setSendDate(java.util.Calendar sendDate) {
        this.sendDate = sendDate;
    }


    /**
     * Gets the totalReports value for this InspectionReportFile.
     * 
     * @return totalReports
     */
    public java.math.BigInteger getTotalReports() {
        return totalReports;
    }


    /**
     * Sets the totalReports value for this InspectionReportFile.
     * 
     * @param totalReports
     */
    public void setTotalReports(java.math.BigInteger totalReports) {
        this.totalReports = totalReports;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InspectionReportFile)) return false;
        InspectionReportFile other = (InspectionReportFile) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inspectionReport==null && other.getInspectionReport()==null) || 
             (this.inspectionReport!=null &&
              java.util.Arrays.equals(this.inspectionReport, other.getInspectionReport()))) &&
            ((this.sendDate==null && other.getSendDate()==null) || 
             (this.sendDate!=null &&
              this.sendDate.equals(other.getSendDate()))) &&
            ((this.totalReports==null && other.getTotalReports()==null) || 
             (this.totalReports!=null &&
              this.totalReports.equals(other.getTotalReports())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInspectionReport() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInspectionReport());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInspectionReport(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSendDate() != null) {
            _hashCode += getSendDate().hashCode();
        }
        if (getTotalReports() != null) {
            _hashCode += getTotalReports().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InspectionReportFile.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReportFile"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("sendDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "SendDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("totalReports");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TotalReports"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inspectionReport");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReport"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.gmacinspections.com", "InspectionReport"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
