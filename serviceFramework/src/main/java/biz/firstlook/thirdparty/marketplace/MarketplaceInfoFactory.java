package biz.firstlook.thirdparty.marketplace;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.module.core.impl.entity.MemberCredential;

public class MarketplaceInfoFactory
{

static private String gmPropertyName = "firstlook.GMSmartAuction.service";

public static String getGMSmartAuctionWebServiceURL()
{
	return PropertyLoader.getProperty( gmPropertyName + ".url" );
}

public static MemberCredential getGMSmartAuctionFLCredential()
{
	MemberCredential credential = new MemberCredential();
	credential.setUsername( PropertyLoader.getProperty( gmPropertyName + ".FLUsername" ));
	credential.setPassword( PropertyLoader.getProperty( gmPropertyName + ".FLPassword" ));
	return credential;
}

}
