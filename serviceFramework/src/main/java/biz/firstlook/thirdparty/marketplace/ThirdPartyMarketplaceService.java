package biz.firstlook.thirdparty.marketplace;

import java.sql.Timestamp;
import java.util.Date;

import org.omg.CORBA.portable.ApplicationException;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.entity.Inventory;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.thirdparty.marketplace.entity.MarketplaceSubmission;
import biz.firstlook.thirdparty.marketplace.entity.MarketplaceSubmissionStatus;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public abstract class ThirdPartyMarketplaceService
{

protected IMarketplaceSubmissionDAO marketplaceSubmissionDAO;
protected IInventoryDAO inventoryDAO;

public ThirdPartyMarketplaceService( IMarketplaceSubmissionDAO marketplaceSubmissionDAO )
{
	this.marketplaceSubmissionDAO = marketplaceSubmissionDAO;
}

public ThirdPartyMarketplaceService()
{
}

abstract public String submit( MarketplaceSubmissionBaseInformation baseInfo ) throws ThirdPartyMarketPlaceException, ApplicationException;

protected void persistSubmission( MarketplaceSubmissionBaseInformation submissionInformation, String response, Marketplace_InventoryThirdParty marketplace )
{
	MarketplaceSubmission submission = new MarketplaceSubmission();
	submission.setBeginEffectiveDate( new Timestamp( new Date().getTime() ) );
	submission.setDateCreated( new Timestamp( new Date().getTime() ) );
	submission.setEndEffectiveDate( new Timestamp( DateUtilFL.addDaysToDate( new Date(), 1 ).getTime() ) );
	submission.setInventoryId( submissionInformation.getFirstInventory().getId() );
	submission.setMarketPlace( marketplace );
	submission.setMemberId( submissionInformation.getMemberId() );
	submission.setMarketplaceSubmissionStatusCD( MarketplaceSubmissionStatus.SUBMITTED.getMarketplaceSubmissionStatusCD() );
	submission.setUrl( response );

	marketplaceSubmissionDAO.saveOrUpdate( submission );
}

protected void populateSubmissionInformation( MarketplaceSubmissionBaseInformation submissionInfo, Inventory inventory, Integer memberId, Marketplace_InventoryThirdParty marketplace )
{
	submissionInfo.addInventory( inventory );
	submissionInfo.setCredentials( MarketplaceInfoFactory.getGMSmartAuctionFLCredential() );
	submissionInfo.setMemberId( memberId );
	submissionInfo.setMarketplace( marketplace );
}

public void setMarketplaceSubmissionDAO( IMarketplaceSubmissionDAO marketplaceSubmissionDAO )
{
	this.marketplaceSubmissionDAO = marketplaceSubmissionDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

}
