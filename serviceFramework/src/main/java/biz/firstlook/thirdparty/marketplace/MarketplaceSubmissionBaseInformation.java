package biz.firstlook.thirdparty.marketplace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.Vehicle;
import biz.firstlook.module.core.impl.entity.MemberCredential;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;


public abstract class MarketplaceSubmissionBaseInformation
{

protected Marketplace_InventoryThirdParty marketplace;
protected List<Inventory> inventories;
protected Map<Integer, List<String>> inventoryPhotoUrlsMap = new HashMap<Integer, List<String>>();
protected MemberCredential credentials;
protected int memberId;

public int getMemberId()
{
	return memberId;
}

public void setMemberId( int memberId )
{
	this.memberId = memberId;
}

public MemberCredential getCredentials()
{
	return credentials;
}

public void setCredentials( MemberCredential credentials )
{
	this.credentials = credentials;
}

public void addInventory( Inventory vehicleInventory )
{
	if ( inventories == null )
	{
		inventories = new ArrayList<Inventory>();
	}
	inventories.add( vehicleInventory );
}

public Inventory getFirstInventory()
{
	return inventories.get(0);
}

public Vehicle getFirstVehicle()
{
	return inventories.get(0).getVehicle();
}

public Marketplace_InventoryThirdParty getMarketplace()
{
	return marketplace;
}

public void setMarketplace( Marketplace_InventoryThirdParty marketplace )
{
	this.marketplace = marketplace;
}

public List<String> getPhotoUrls(int inventoryId)
{
	if (inventoryPhotoUrlsMap.containsKey(inventoryId))
	{
		return inventoryPhotoUrlsMap.get(inventoryId);
	}
	return new ArrayList<String>();
}

	
}
