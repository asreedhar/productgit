package biz.firstlook.thirdparty.marketplace.GMSmartAuction;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;

import javax.xml.rpc.ServiceException;

import biz.firstlook.module.core.impl.entity.MemberCredential;
import biz.firstlook.thirdparty.marketplace.MarketplaceInfoFactory;
import biz.firstlook.thirdparty.marketplace.ThirdPartyMarketPlaceException;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionResponse;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionServiceLocator;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.IGMSmartAuctionSoapService;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.InspectionReport;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.InspectionReportFile;

/**
 * This class helps the IMT web application connect and retrieve values from the
 * GMSmartAuction stuff.
 */
public class GMSmartAuctionAdapter
{

private GMSmartAuctionServiceLocator serviceLocator;
private IGMSmartAuctionSoapService soapService;

public GMSmartAuctionAdapter() throws ThirdPartyMarketPlaceException
{
	super();
	String	webServiceLocation = MarketplaceInfoFactory.getGMSmartAuctionWebServiceURL();
	initalizeService( webServiceLocation );
}

// this exists so i can injet a webService location
public GMSmartAuctionAdapter(String webServiceLocation) throws ThirdPartyMarketPlaceException
{
	super();
	initalizeService(webServiceLocation);
}

private void initalizeService( String webServiceLocation ) throws ThirdPartyMarketPlaceException
{
	try
	{
		serviceLocator = new GMSmartAuctionServiceLocator();
		soapService = serviceLocator.getDIPostServiceSoap( new URL( webServiceLocation ) );
	}
	catch ( MalformedURLException e )
	{
		throw new ThirdPartyMarketPlaceException( "Bad GMSmartAuction WebService location: ", e );
	}
	catch ( ServiceException e )
	{
		throw new ThirdPartyMarketPlaceException( "GMSmartAuctionWebService Error: ", e );
	}
}

public GMSmartAuctionResponse submit( InspectionReport[] reports, String vcDesc, String stockNo, MemberCredential credentail ) throws ThirdPartyMarketPlaceException
{
	GMSmartAuctionResponse response;
	InspectionReportFile inspectionReportFile = new InspectionReportFile( reports, Calendar.getInstance(), BigInteger.valueOf( reports.length));
	try
	{
		response = soapService.postInspectionReport(inspectionReportFile, vcDesc, stockNo, credentail.getUsername(), credentail.getPassword());
	}
	catch ( RemoteException e )
	{
		throw new ThirdPartyMarketPlaceException( "Error with GMSmartAuction WebService: " + e.getMessage() );
	}
	return response;
}

}
