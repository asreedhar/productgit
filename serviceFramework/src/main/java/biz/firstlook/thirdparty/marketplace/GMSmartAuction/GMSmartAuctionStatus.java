package biz.firstlook.thirdparty.marketplace.GMSmartAuction;

public class GMSmartAuctionStatus
{

private boolean gmDealer;
private boolean posted;
private String confirmationURL;

public GMSmartAuctionStatus()
{
	super();
}

public static GMSmartAuctionStatus getDefaultNotSumbittedStatus()
{
	GMSmartAuctionStatus status = new GMSmartAuctionStatus();
	status.setGmDealer( false );
	status.setPosted( false );
	status.setConfirmationURL( "" );
	return status;
}

public String getConfirmationURL()
{
	return confirmationURL;
}
public void setConfirmationURL( String confirmationURL )
{
	this.confirmationURL = confirmationURL;
}
public boolean isPosted()
{
	return posted;
}
public void setPosted( boolean currentlySubmitted )
{
	this.posted = currentlySubmitted;
}
public boolean isGmDealer()
{
	return gmDealer;
}
public void setGmDealer( boolean gmDealer )
{
	this.gmDealer = gmDealer;
} 

}
