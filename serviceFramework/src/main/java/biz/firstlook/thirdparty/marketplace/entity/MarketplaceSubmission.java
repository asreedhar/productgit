package biz.firstlook.thirdparty.marketplace.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MarketplaceSubmission")
public class MarketplaceSubmission
{

@Id
@GeneratedValue( strategy = GenerationType.AUTO )
@Column( name = "MarketplaceSubmissionID" )
private Integer id;

@ManyToOne
@JoinColumn(name="MarketplaceID")
private Marketplace_InventoryThirdParty marketplace;

private Integer inventoryId;
private Integer memberId;
private Integer marketplaceSubmissionStatusCD;
private String url;
private Timestamp beginEffectiveDate;
private Timestamp endEffectiveDate;
private Timestamp dateCreated;

public MarketplaceSubmission()
{
	super();
}

public Timestamp getBeginEffectiveDate()
{
	return beginEffectiveDate;
}

public void setBeginEffectiveDate( Timestamp beginEffective )
{
	this.beginEffectiveDate = beginEffective;
}

public Timestamp getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Timestamp dateCreated )
{
	this.dateCreated = dateCreated;
}

public Timestamp getEndEffectiveDate()
{
	return endEffectiveDate;
}

public void setEndEffectiveDate( Timestamp endEffective )
{
	this.endEffectiveDate = endEffective;
}

public Integer getInventoryId()
{
	return inventoryId;
}

public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}

public Marketplace_InventoryThirdParty getMarketPlace()
{
	return marketplace;
}

public void setMarketPlace( Marketplace_InventoryThirdParty marketplace )
{
	this.marketplace = marketplace;
}

public Integer getId()
{
	return id;
}

public void setId( Integer marketplaceSubmissionId )
{
	this.id = marketplaceSubmissionId;
}

public Integer getMemberId()
{
	return memberId;
}

public void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}

public String getUrl()
{
	return url;
}

public void setUrl( String url )
{
	this.url = url;
}

public boolean hasExpired()
{
	Date today = new Date();
	return ( today.after( endEffectiveDate ));
}

public Integer getMarketplaceSubmissionStatusCD()
{
	return marketplaceSubmissionStatusCD;
}

public void setMarketplaceSubmissionStatusCD( Integer marketplaceSubmissionStatusCD )
{
	this.marketplaceSubmissionStatusCD = marketplaceSubmissionStatusCD;
}

}
