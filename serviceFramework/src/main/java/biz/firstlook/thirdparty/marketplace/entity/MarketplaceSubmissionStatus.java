package biz.firstlook.thirdparty.marketplace.entity;

public class MarketplaceSubmissionStatus
{

public static MarketplaceSubmissionStatus SUBMITTED = new MarketplaceSubmissionStatus(1, "Submitted");
public static MarketplaceSubmissionStatus POSTED = new MarketplaceSubmissionStatus(2, "Posted");

private int marketplaceSubmissionStatusCD;
private String description;

public MarketplaceSubmissionStatus()
{
	super();
}

public MarketplaceSubmissionStatus(int code, String description)
{
	this.marketplaceSubmissionStatusCD = code;
	this.description = description;
}

public int getMarketplaceSubmissionStatusCD()
{
	return marketplaceSubmissionStatusCD;
}

public void setMarketplaceSubmissionStatusCD( int code )
{
	this.marketplaceSubmissionStatusCD = code;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

}
