package biz.firstlook.module.pricing.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.pricing.impl.entity.PricingRisk;

public class PricingRiskDAO extends GenericHibernate<PricingRisk,Integer> implements IGenericDAO<PricingRisk,Integer> {

	public PricingRiskDAO() {
		super(PricingRisk.class);
	}

	@SuppressWarnings("unchecked")
	public List<PricingRisk> findCurrentPricingRisks(Integer businessUnitId) {
		StringBuilder hql = new StringBuilder();
		hql.append(" from");
		hql.append(" biz.firstlook.module.market.search.impl.entity.PricingRisk pr");
		hql.append(" where");
		hql.append(" pr.businessUnitId = ?");
		hql.append(" order by inventoryAge, riskLevel desc");
		return (List<PricingRisk>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { businessUnitId } );
	}
	
}
