package biz.firstlook.module.pricing.impl.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "PricingRisk")
public class PricingRisk {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer pricingRiskId;
	private Integer businessUnitId;
	private Integer inventoryId;
	private Double riskLevel;
	private Integer inventoryAge;
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	public Integer getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}
	public Integer getPricingRiskId() {
		return pricingRiskId;
	}
	public void setPricingRiskId(Integer pricingRiskId) {
		this.pricingRiskId = pricingRiskId;
	}
	public Double getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(Double riskLevel) {
		this.riskLevel = riskLevel;
	}
	public Integer getInventoryAge() {
		return inventoryAge;
	}
	public void setInventoryAge(Integer inventoryAge) {
		this.inventoryAge = inventoryAge;
	}
}
