package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.impl.AbstractMarket;

@Entity
@org.hibernate.annotations.Entity(mutable = false)
@Table(name = "AccessGroup")
public class Market extends AbstractMarket implements Serializable,
        Comparable<Market>, biz.firstlook.module.market.search.Market {

	private static final long serialVersionUID = -4245104761046941167L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "AccessGroupID")
    private Integer marketID;

    @Column(name = "CustomerFacingDescription", insertable=false, updatable=false)
    private String name;

    @Column(name = "AccessGroupTypeId")
    private Integer accessGroupTypeId;

    @Column(name = "Public_Group")
    private boolean publicMarket;

    public Market() {
        super();
    }

    public Market(Integer marketID, String name, Integer accessGroupTypeId) {
        super();
        this.marketID = marketID;
        this.name = name;
        this.accessGroupTypeId = accessGroupTypeId;
    }

    public Integer getMarketID() {
        return marketID;
    }

    public void setMarketID(Integer marketID) {
        this.marketID = marketID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAccessGroupTypeId() {
        return accessGroupTypeId;
    }

    public void setAccessGroupTypeId(Integer accessGroupTypeId) {
        this.accessGroupTypeId = accessGroupTypeId;
    }

    public boolean isPublicMarket() {
        return publicMarket;
    }

    public void setPublicMarket(boolean publicMarket) {
        this.publicMarket = publicMarket;
    }

    @Transient
    public MarketType getMarketType() {
        MarketType marketType = null;
        if (accessGroupTypeId != null) {
            switch (accessGroupTypeId) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 9:
            case 10:
                marketType = MarketType.ONLINE;
                break;
            case 6:
                marketType = MarketType.IN_GROUP;
                break;
            case 7:
                marketType = MarketType.LIVE;
                break;
            default:
                throw new IllegalStateException("Unknown MarketType: "
                        + accessGroupTypeId);
            }
        }
        return marketType;
    }

    @Transient
    public MarketAdministrator getMarketAdministrator() {
        MarketAdministrator marketAdministrator = null;
        if (accessGroupTypeId != null) {
            switch (accessGroupTypeId) {
            case 1:
                marketAdministrator = MarketAdministrator.MANUFACTURER;
                break;
            case 2:
                marketAdministrator = MarketAdministrator.ENTERPRISE;
                break;
            case 3:
                marketAdministrator = MarketAdministrator.ATC_OPEN;
                break;
            case 4:
                marketAdministrator = MarketAdministrator.GMAC;
                break;
            case 5:
                marketAdministrator = MarketAdministrator.TFS_LFS;
                break;
            case 6:
                marketAdministrator = MarketAdministrator.IN_GROUP;
                break;
            case 7:
                marketAdministrator = MarketAdministrator.LIVE;
                break;
            case 8:
                marketAdministrator = MarketAdministrator.OVE;
                break;
            case 9:
                marketAdministrator = MarketAdministrator.OVE;
                break;
            case 10:
                marketAdministrator = MarketAdministrator.AutoSales;
                break;
            default:
                throw new IllegalStateException("Unknown MarketAdminstrator: "
                        + accessGroupTypeId);
            }
        }
        return marketAdministrator;
    }

    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (obj instanceof Market) {
            Market m = (Market) obj;
            boolean lEquals = true;
            lEquals &= Functions.nullSafeEquals(getMarketID(), m.getMarketID());
            return lEquals;
        }
        return super.equals(obj);
    }

    public int compareTo(Market m1) {
        if (getAccessGroupTypeId() < 6 && m1.getAccessGroupTypeId() > 5) {
            return -1;
        }
        if (getAccessGroupTypeId() > 5 && m1.getAccessGroupTypeId() < 6) {
            return 1;
        }
        if (getAccessGroupTypeId() < 4 && m1.getAccessGroupTypeId() < 4) {
            return (Functions.nullSafeCompareTo(getAccessGroupTypeId(), m1.getAccessGroupTypeId()));
        }
        return (Functions.nullSafeCompareTo(getAccessGroupTypeId(), m1.getAccessGroupTypeId()) * -1);
    }

}
