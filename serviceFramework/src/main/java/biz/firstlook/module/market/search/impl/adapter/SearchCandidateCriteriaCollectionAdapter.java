package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.Iterator;

import biz.firstlook.commons.collections.CollectionAdapter;
import biz.firstlook.commons.hibernate.CollectionMutatorCallback;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.impl.entity.SearchCandidate;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;

/**
 * Wraps a collection of hibernate <code>SearchCandidateOption</code>s and presents
 * it as a collection of interface <code>SearchCandidateOption</code>s.
 * 
 * Changes to the collection are persisted back to the underlying data store.
 *  
 * @author swenmouth
 */
public class SearchCandidateCriteriaCollectionAdapter extends 
	CollectionAdapter<biz.firstlook.module.market.search.SearchCandidateCriteria,biz.firstlook.module.market.search.impl.entity.SearchCandidateOption> 
	implements Serializable,
	CollectionMutatorCallback<biz.firstlook.module.market.search.SearchCandidateCriteria, biz.firstlook.module.market.search.impl.entity.SearchCandidate>{

	private static final long serialVersionUID = -3812210392033417941L;
	
	private biz.firstlook.module.market.search.impl.entity.SearchCandidate searchCandidate;
	
	public SearchCandidateCriteriaCollectionAdapter(biz.firstlook.module.market.search.impl.entity.SearchCandidate searchCandidate) {
		super(searchCandidate.getSearchCandidateOptions());
		this.searchCandidate = searchCandidate;
	}
	
	public boolean add(biz.firstlook.module.market.search.SearchCandidateCriteria searchCandidateCriteria) {
		if (!(searchCandidateCriteria instanceof SearchCandidateOptionAdapter)) {
			return false;
		}
		// look for an update
		biz.firstlook.module.market.search.impl.entity.SearchCandidateOption tgt = ((SearchCandidateOptionAdapter) searchCandidateCriteria).getSearchCandidateOption();
		final Integer tgtModelYear = tgt.getModelYear();
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidateOption src : searchCandidate.getSearchCandidateOptions()) {
			Integer srcModelYear = src.getModelYear();
			if (Functions.nullSafeEquals(srcModelYear, tgtModelYear)) {
				src.setSuppress(false);
				src.setExpires(tgt.getExpires());
				src.getSearchCandidateOptionAnnotations().addAll(tgt.getSearchCandidateOptionAnnotations());
				return true;
			}
		}
		// create new row if needed
		searchCandidate.getSearchCandidateOptions().add(tgt);
		return true;
	}

	public Iterator<biz.firstlook.module.market.search.SearchCandidateCriteria> iterator() {
		return new MutableItr(collection.iterator());
	}

	public biz.firstlook.module.market.search.SearchCandidateCriteria adaptItem(biz.firstlook.module.market.search.impl.entity.SearchCandidateOption searchCandidateOption) {
		return new SearchCandidateOptionAdapter(searchCandidateOption);
	}
	
	public void synchEntity(SearchCandidate entity) {
		searchCandidate = entity;
		collection = entity.getSearchCandidateOptions();
	}

	class MutableItr extends Itr {
		biz.firstlook.module.market.search.impl.entity.SearchCandidateOption current;
		public MutableItr(Iterator<biz.firstlook.module.market.search.impl.entity.SearchCandidateOption> it) {
			super(it);
		}
		public boolean hasNext() {
			while (it.hasNext()) {
				current = it.next();
				if (!current.getSuppress())
					return true;
			}
			return false;
		}
		public void remove() {
			current.setSuppress(true);
			current.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.ONETIME);
			current.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.HOTLIST);
		}
		public biz.firstlook.module.market.search.SearchCandidateCriteria next() {
			return adaptItem(current);
		}
	}
	
}
