package biz.firstlook.module.market.search;

/**
 * A market annotated by a users preferences regarding queries against that market.
 * @author swenmouth
 */
public interface MemberMarket extends Comparable<MemberMarket>, Market {
	public Integer getMaxDistanceFromDealer();
	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer);
    public Integer getMinMileage();
    public void setMinMileage(Integer minMileage);
    public Integer getMaxMileage();
    public void setMaxMileage(Integer maxMileage);
	public Integer getTimePeriod();
	public void setTimePeriod(Integer timePeriod);
	public boolean getExactMatches();
	public void setExactMatches(boolean exactMatches);
	public boolean getSuppress();
	public void setSuppress(boolean suppress);
}
