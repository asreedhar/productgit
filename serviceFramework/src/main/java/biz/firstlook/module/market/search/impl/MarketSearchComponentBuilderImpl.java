package biz.firstlook.module.market.search.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.adapter.MarketToMemberMarketAdapter;
import biz.firstlook.module.market.search.impl.adapter.SearchCandidateAdapter;
import biz.firstlook.module.market.search.impl.adapter.SearchCandidateOptionAdapter;
import biz.firstlook.module.market.search.impl.entity.CIACategory;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOption;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

/**
 * Default implementation of the <code>MarketSearchComponentBuilder</code> class.
 * 
 * @author swenmouth
 */
public class MarketSearchComponentBuilderImpl extends
		MarketSearchComponentBuilder {

	protected MarketSearchComponentBuilderImpl() {
		super();
		MarketSearchComponentBuilderFactoryImpl.setBuilder(this);
	}

	public SearchContext newContext(SearchContext ctx, MarketSummary m) {
		return newContext(ctx, new MarketToMemberMarketAdapter(m, ctx.getMaxDistanceFromBusinessUnit(), ctx.getTimePeriod(), ctx.getExactMatches(), ctx.getMaxMileage(), ctx.getMinMileage()));
	}

	public SearchContext newContext(SearchContext ctx, MemberMarket m) {
		return newContext(ctx, Collections.singletonList(m));
	}

	public SearchContext newContext(SearchContext ctx, List<MemberMarket> m) {
		return new SimpleSearchContextImpl(ctx.getMember(), m, ctx.getPersistenceScope());
	}
	
	public SearchCandidate newSearchCandidate(Model model) {
		biz.firstlook.module.market.search.impl.entity.SearchCandidate newCandidate = new biz.firstlook.module.market.search.impl.entity.SearchCandidate(
				null,
				model.getId(),
				false,
				new ArrayList<SearchCandidateOption>(),
				new HashSet<CIACategory>());
		return new SearchCandidateAdapter(newCandidate);
	}

	public SearchCandidateCriteria newSearchCandidateCriteria(ModelYear modelYear, Date expires) {
		biz.firstlook.module.market.search.impl.entity.SearchCandidateOption newSearchCandidateCriteria = new biz.firstlook.module.market.search.impl.entity.SearchCandidateOption(
				null,
				(modelYear == null ? null : modelYear.getModelYear()),
				Functions.midnight(expires),
				false,
				new HashSet<SearchCandidateOptionAnnotationType>());
		return new SearchCandidateOptionAdapter(newSearchCandidateCriteria);
	}

}
