package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@org.hibernate.annotations.Entity(mutable=false)
@Table(name="SearchCandidateOptionAnnotationType")
public class SearchCandidateOptionAnnotationType implements Serializable {

	private static final long serialVersionUID = 3151166701165876483L;
	
	public static transient final SearchCandidateOptionAnnotationType OPTIMAL = new SearchCandidateOptionAnnotationType(1, "Optimal Plan");
	public static transient final SearchCandidateOptionAnnotationType BUYING  = new SearchCandidateOptionAnnotationType(2, "Buying Plan");
	public static transient final SearchCandidateOptionAnnotationType HOTLIST = new SearchCandidateOptionAnnotationType(3, "Hot List");
	public static transient final SearchCandidateOptionAnnotationType ONETIME = new SearchCandidateOptionAnnotationType(4, "One Time");
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer searchCandidateOptionAnnotationTypeID;
	private String description;
	
	public SearchCandidateOptionAnnotationType() {
		super();
	}
	
	public SearchCandidateOptionAnnotationType(Integer searchCandidateOptionAnnotationTypeID, String description) {
		super();
		this.searchCandidateOptionAnnotationTypeID = searchCandidateOptionAnnotationTypeID;
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getSearchCandidateOptionAnnotationTypeID() {
		return searchCandidateOptionAnnotationTypeID;
	}
	public void setSearchCandidateOptionAnnotationTypeID(Integer searchCandidateListTypeID) {
		this.searchCandidateOptionAnnotationTypeID = searchCandidateListTypeID;
	}
	
	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof SearchCandidateOptionAnnotationType) {
			SearchCandidateOptionAnnotationType t = (SearchCandidateOptionAnnotationType) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getSearchCandidateOptionAnnotationTypeID(), t.getSearchCandidateOptionAnnotationTypeID());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getSearchCandidateOptionAnnotationTypeID());
		return hashCode;
	}
	
	private Object readResolve() {
		switch (searchCandidateOptionAnnotationTypeID) {
		case 1:
			return OPTIMAL;
		case 2:
			return BUYING;
		case 3:
			return HOTLIST;
		case 4:
			return ONETIME;
		}
		return this;
	}
}
