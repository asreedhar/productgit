package biz.firstlook.module.market.search;

/**
 * <p>A meeting of people for selling and buying.</p>
 * 
 * @author swenmouth
 */
public interface Market {
	public String getName();
	public MarketType getMarketType();
	public MarketAdministrator getMarketAdministrator();
}
