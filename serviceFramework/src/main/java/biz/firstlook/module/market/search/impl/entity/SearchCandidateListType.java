package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@org.hibernate.annotations.Entity(mutable=false)
@Table(name="SearchCandidateListType")
public class SearchCandidateListType implements Serializable {
	
	private static final long serialVersionUID = 7429217545674297224L;
	
	public static transient final SearchCandidateListType HOTLIST_PERMANENT = new SearchCandidateListType(1, "Permanent Hot List");
	public static transient final SearchCandidateListType HOTLIST_CURRENT   = new SearchCandidateListType(2, "Current Hot List");
	public static transient final SearchCandidateListType HOTLIST_TEMPORARY = new SearchCandidateListType(3, "Temporary List");
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer searchCandidateListTypeID;
	private String description;
	
	public SearchCandidateListType() {
		super();
	}
	
	public SearchCandidateListType(Integer searchCandidateListTypeID, String description) {
		super();
		this.searchCandidateListTypeID = searchCandidateListTypeID;
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getSearchCandidateListTypeID() {
		return searchCandidateListTypeID;
	}
	public void setSearchCandidateListTypeID(Integer searchCandidateListTypeID) {
		this.searchCandidateListTypeID = searchCandidateListTypeID;
	}
	
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof SearchCandidateListType) {
			SearchCandidateListType t = (SearchCandidateListType) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getSearchCandidateListTypeID(), t.getSearchCandidateListTypeID());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getSearchCandidateListTypeID());
		return hashCode;
	}
}

