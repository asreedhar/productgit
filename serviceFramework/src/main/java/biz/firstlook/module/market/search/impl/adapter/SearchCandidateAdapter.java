package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import biz.firstlook.module.market.search.SearchCandidateAnnotation;
import biz.firstlook.module.market.search.impl.AbstractSearchCandidate;
import biz.firstlook.module.market.search.impl.entity.SearchCandidate;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;

/**
 * Wraps the hibernate entity <code>SearchCandidate</code> and presents it as the
 * interface <code>SearchCandidate</code>.
 * 
 * @author swenmouth
 */
public class SearchCandidateAdapter extends AbstractSearchCandidate implements Serializable, biz.firstlook.module.market.search.SearchCandidate {

	private static final long serialVersionUID = 4656210990156997396L;
	
	private SearchCandidate searchCandidate;
	private Model model;
	
	public SearchCandidateAdapter(SearchCandidate searchCandidate) {
		final VehicleService service = VehicleServiceFactory.getFactory().getService();
		this.searchCandidate = searchCandidate;
		this.model = service.getModel(searchCandidate.getModelID());
	}

	public Model getModel() {
		return model;
	}
	
	public Collection<biz.firstlook.module.market.search.SearchCandidateCriteria> getCriteria() {
		return new SearchCandidateCriteriaCollectionAdapter(searchCandidate);
	}
	
	public SearchCandidate getSearchCandidate() {
		return searchCandidate;
	}

	public Set<SearchCandidateAnnotation> getAnnotations() {
		return new CIACategorySetAdapter(searchCandidate.getSearchCandidateAnnotations());
	}
	
}
