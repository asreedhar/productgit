package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@Table(name="tbl_MemberATCAccessGroup")
public class MemberMarket implements Serializable {
	
	private static final long serialVersionUID = -6180712542667897868L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column(name="MemberATCAccessGroupID")
	private Integer memberMarketID;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="accessGroupID",nullable=false)
	private Market market;

	@Column(name="Position")
	private Integer position;
	
	@Column(name="MaxDistanceFromDealer")
	private Integer maxDistanceFromDealer;
    
    @Column(name="MinVehicleMileage")
    private Integer minMileage;
    
    @Column(name = "MaxVehicleMileage")
    private Integer maxMileage;
	
	@Column(name="Suppress")
	private Boolean suppress;

	public MemberMarket() {
		super();
	}

	public MemberMarket(Integer memberMarketID, Market market, Integer position, Integer maxDistanceFromDealer, Boolean suppress, Integer maxMileage, Integer minMileage) {
		super();
		this.memberMarketID = memberMarketID;
		this.market = market;
		this.position = position;
		this.maxDistanceFromDealer = maxDistanceFromDealer;
		this.suppress = suppress;
        this.maxMileage = maxMileage;
        this.minMileage = minMileage;
	}

	public Market getMarket() {
		return market;
	}
	public void setMarket(Market market) {
		this.market = market;
	}

	public Integer getMemberMarketID() {
		return memberMarketID;
	}
	public void setMemberMarketID(Integer memberMarketID) {
		this.memberMarketID = memberMarketID;
	}

	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getMaxDistanceFromDealer() {
		return maxDistanceFromDealer;
	}
	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
		this.maxDistanceFromDealer = maxDistanceFromDealer;
	}

	public Boolean getSuppress() {
		return suppress;
	}
	public void setSuppress(Boolean suppress) {
		this.suppress = suppress;
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof MemberMarket) {
			MemberMarket other = (MemberMarket) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getMarket(), other.getMarket());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getMarket());
		return hashCode;
	}

    public Integer getMaxMileage() {
        return maxMileage;
    }

    public void setMaxMileage(Integer maxMileage) {
        this.maxMileage = maxMileage;
    }

    public Integer getMinMileage() {
        return minMileage;
    }

    public void setMinMileage(Integer minMileage) {
        this.minMileage = minMileage;
    }
	
}
