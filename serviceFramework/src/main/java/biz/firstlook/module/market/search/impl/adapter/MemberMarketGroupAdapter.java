package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.commons.hibernate.Persistor;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.impl.AbstractMarket;
import biz.firstlook.module.market.search.impl.entity.MemberMarketList;
import biz.firstlook.module.market.search.impl.entity.dao.MemberMarketListDAO;

/**
 * Wraps a list of {@link MemberMarket}s makes it appear as a single {@link MemberMarket}.
 * 
 * More concretely, this class is used to present a multitude of "ATC Open Auctions"
 * as a single "ATC Open Auction".
 * 
 * @author swenmouth
 */
public class MemberMarketGroupAdapter extends AbstractMarket implements
		Serializable,
		biz.firstlook.module.market.search.MemberMarket,
		Iterable<biz.firstlook.module.market.search.impl.entity.MemberMarket> {
	
	private static final long serialVersionUID = 6437227726208146322L;

	private static final int DEFAULT_TIME_PERIOD = 2;
	
	private MemberMarketListDAO memberMarketListDAO;
	private MemberMarketList memberMarketList;
	private Persistor<MemberMarketList,Integer> persistor;
	
	private Collection<biz.firstlook.module.market.search.impl.entity.MemberMarket> markets;
	private MarketType marketType;
	private MarketAdministrator marketAdministrator;
	private String name;
	private boolean suppress;
	private int position;
	private Integer maxDistanceFromDealer;
	private Integer maxMileage;
	private Integer minMileage;
	private Integer timePeriod;
	private boolean exactMatches;

	public MemberMarketGroupAdapter(MemberMarketListDAO memberMarketListDAO, MemberMarketList memberMarketList, Persistor<MemberMarketList,Integer> persistor, biz.firstlook.module.market.search.impl.entity.MemberMarket memberMarket) {
		this.memberMarketListDAO = memberMarketListDAO;
		this.memberMarketList = memberMarketList;
		this.persistor = persistor;
		this.marketType = memberMarket.getMarket().getMarketType();
		this.marketAdministrator = memberMarket.getMarket().getMarketAdministrator();
		this.name = memberMarket.getMarket().getName();
		this.suppress = memberMarket.getSuppress();
		this.position = memberMarket.getPosition();
		this.maxDistanceFromDealer = memberMarket.getMaxDistanceFromDealer();
		this.timePeriod = DEFAULT_TIME_PERIOD;
		this.exactMatches = true;
        this.maxMileage = memberMarket.getMaxMileage();
        this.minMileage = memberMarket.getMinMileage();
		this.markets = new ArrayList<biz.firstlook.module.market.search.impl.entity.MemberMarket>();
		this.markets.add(memberMarket);
	}

	public MarketType getMarketType() {
		return marketType;
	}

	public MarketAdministrator getMarketAdministrator() {
		return marketAdministrator;
	}

	public String getName() {
		return name;
	}

	public Integer getMaxDistanceFromDealer() {
		return maxDistanceFromDealer;
	}

	public Integer getTimePeriod() {
		return timePeriod;
	}

	public boolean getExactMatches() {
		return exactMatches;
	}

	public boolean getSuppress() {
		return suppress;
	}

	public Integer getPosition() {
		return position;
	}
	
	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
		this.maxDistanceFromDealer = maxDistanceFromDealer;
		for (biz.firstlook.module.market.search.impl.entity.MemberMarket m : markets) {
			m.setMaxDistanceFromDealer(maxDistanceFromDealer);
		}
		memberMarketList = persistor.persist(memberMarketListDAO, memberMarketList);
		List<biz.firstlook.module.market.search.impl.entity.MemberMarket> newMarkets = new ArrayList<biz.firstlook.module.market.search.impl.entity.MemberMarket>();
		for (biz.firstlook.module.market.search.impl.entity.MemberMarket m0 : markets) {
			for (biz.firstlook.module.market.search.impl.entity.MemberMarket m1 : memberMarketList.getMemberMarkets()) {
				if (m0.equals(m1)) {
					newMarkets.add(m1);
					break;
				}
				
			}
		}
		markets = newMarkets;
	}
    
    public void setMaxMileage(Integer maxMileage) {
        this.maxMileage = maxMileage;
        for (biz.firstlook.module.market.search.impl.entity.MemberMarket m : markets) {
            m.setMaxMileage(maxMileage);
        }
        memberMarketList = persistor.persist(memberMarketListDAO, memberMarketList);
        List<biz.firstlook.module.market.search.impl.entity.MemberMarket> newMarkets = new ArrayList<biz.firstlook.module.market.search.impl.entity.MemberMarket>();
        for (biz.firstlook.module.market.search.impl.entity.MemberMarket m0 : markets) {
            for (biz.firstlook.module.market.search.impl.entity.MemberMarket m1 : memberMarketList.getMemberMarkets()) {
                if (m0.equals(m1)) {
                    newMarkets.add(m1);
                    break;
                }
            }
        }
        markets = newMarkets;
    }
    public void setMinMileage(Integer minMileage) {
        this.minMileage = minMileage;
        for (biz.firstlook.module.market.search.impl.entity.MemberMarket m : markets) {
            m.setMinMileage(minMileage);
        }
        memberMarketList = persistor.persist(memberMarketListDAO, memberMarketList);
        List<biz.firstlook.module.market.search.impl.entity.MemberMarket> newMarkets = new ArrayList<biz.firstlook.module.market.search.impl.entity.MemberMarket>();
        for (biz.firstlook.module.market.search.impl.entity.MemberMarket m0 : markets) {
            for (biz.firstlook.module.market.search.impl.entity.MemberMarket m1 : memberMarketList.getMemberMarkets()) {
                if (m0.equals(m1)) {
                    newMarkets.add(m1);
                    break;
                }
            }
        }
        markets = newMarkets;
    }
    
	public void setTimePeriod(Integer timePeriod) {
		this.timePeriod = timePeriod;
	}

	public void setExactMatches(boolean exactMatches) {
		this.exactMatches = exactMatches;
	}

	public void setSuppress(boolean suppress) {
		this.suppress = suppress;
		for (biz.firstlook.module.market.search.impl.entity.MemberMarket m : markets) {
			m.setSuppress(suppress);
		}
		memberMarketList = persistor.persist(memberMarketListDAO, memberMarketList);
	}

	public void setPosition(int position) {
		this.position = position;
		for (biz.firstlook.module.market.search.impl.entity.MemberMarket m : markets) {
			m.setPosition(position);
		}
		memberMarketList = persistor.persist(memberMarketListDAO, memberMarketList);
	}
	
	public void add(biz.firstlook.module.market.search.impl.entity.MemberMarket market) {
		markets.add(market);
	}

	public Iterator<biz.firstlook.module.market.search.impl.entity.MemberMarket> iterator() {
		return markets.iterator();
	}

	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		else if (obj instanceof MemberMarketGroupAdapter) {
			return markets.equals(((MemberMarketGroupAdapter) obj).markets);
		}
		else if (obj instanceof Market) {
			return super.equals(obj);
		}
		else if (obj instanceof biz.firstlook.module.market.search.impl.entity.MemberMarket) {
			biz.firstlook.module.market.search.impl.entity.MemberMarket m = (biz.firstlook.module.market.search.impl.entity.MemberMarket) obj;
			boolean lEquals = true;
			if (getName() == null) {
				lEquals &= (m.getMarket().getName() == null);
			}
			else {
				lEquals &= getName().equals(m.getMarket().getName());
			}
			if (getMarketType() == null) {
				lEquals &= (m.getMarket().getMarketType() == null);
			}
			else {
				lEquals &= getMarketType().equals(m.getMarket().getMarketType());
			}
			return lEquals;
		}
		else {
			return false;
		}
	}

	public int compareTo(MemberMarket memberMarket) {
		if (memberMarket instanceof MemberMarketGroupAdapter) {
			MemberMarketGroupAdapter adapter = (MemberMarketGroupAdapter) memberMarket;
			int c = 0;
			c = Functions.nullSafeCompareTo(getPosition(), adapter.getPosition(), c);
			c = Functions.nullSafeCompareTo(getName(), adapter.getName(), c);
			return c;
		}
		int c = 0;
		c = Functions.nullSafeCompareTo(getName(), memberMarket.getName(), c);
		return c;
	}

    public Integer getMaxMileage() {
        return maxMileage;
    }

    public Integer getMinMileage() {
        return minMileage;
    }
}