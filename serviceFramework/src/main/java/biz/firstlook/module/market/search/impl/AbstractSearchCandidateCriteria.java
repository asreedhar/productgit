package biz.firstlook.module.market.search.impl;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.SearchCandidateCriteria;

/**
 * Abstract implementation of <code>SearchCandidateCriteria</code> supplying
 * <code>equals</code> and <code>hashCode</code> implementations such that
 * concrete instances need not necessarily provide their own.
 *   
 * @author swenmouth
 */
public abstract class AbstractSearchCandidateCriteria implements
		SearchCandidateCriteria {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof SearchCandidateCriteria) {
			SearchCandidateCriteria s = (SearchCandidateCriteria) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getModelYear(), s.getModelYear());
			return lEquals;
		}
		return false;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getModelYear());
		return hashCode;
	}

	public int compareTo(SearchCandidateCriteria c) {
		return Functions.nullSafeCompareTo(getModelYear(), c.getModelYear());
	}
	
}
