package biz.firstlook.module.market.search;

//See dbo.AccessGroupType in ATC
public enum MarketAccessGroupTypeEnum {

	MANUFACTURER(1, "Manufacturer"), 
    ENTERPRISE(2, "Enterprise"), 
    ATC_OPEN(3,"ATC Open"), 
    GMAC(4, "GMAC"), 
    TFS(5, "TFS/LFS"), 
    IN_GROUP(6,"In Group"), 
    LIVE(7, "Live Auction"), 
    OVE(8, "OVE"),  //This is changed to "Adesa Dealer Block" in admin, but left OVE due to risk, NADA 2010 item. -bf. 
    DRIVE_AWAY(9, "Drive It Away");

    private int accessGroupId;

    private String accessGroupTypeDescription;

    MarketAccessGroupTypeEnum(int accessGroupId, String accessGroupTypeDescription) {
        this.accessGroupId = accessGroupId;
        this.accessGroupTypeDescription = accessGroupTypeDescription;
    }

    public int getAccessGroupId() {
        return accessGroupId;
    }

    public String getAccessGroupTypeDescription() {
        return accessGroupTypeDescription;
    }

}
