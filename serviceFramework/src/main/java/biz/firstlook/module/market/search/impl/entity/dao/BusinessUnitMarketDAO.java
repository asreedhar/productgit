package biz.firstlook.module.market.search.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.market.search.impl.entity.BusinessUnitMarket;

public class BusinessUnitMarketDAO extends GenericHibernate<BusinessUnitMarket,Integer> implements
		IGenericDAO<BusinessUnitMarket,Integer> {

	public BusinessUnitMarketDAO() {
		super(BusinessUnitMarket.class);
	}

	@SuppressWarnings("unchecked")
	public List<BusinessUnitMarket> findByBusinessUnitId(Integer businessUnitID) {
		StringBuffer hql = new StringBuffer();
		hql.append(" from");
		hql.append(" biz.firstlook.module.market.search.impl.entity.BusinessUnitMarket l");
		hql.append(" where");
		hql.append(" l.businessUnitID = ?");
		return (List<BusinessUnitMarket>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { businessUnitID } );
	}
	
}
