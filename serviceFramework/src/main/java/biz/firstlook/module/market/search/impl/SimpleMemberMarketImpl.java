package biz.firstlook.module.market.search.impl;

import java.io.Serializable;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;

/**
 * A simple bean implementation of <code>MemberMarket</code>.  The class is used
 * to hold changes to a persistent <code>MemberMarket</code> until the client
 * decides to commit the changes back to the persistent store. As such the class
 * <em>could</em> be abstract.
 * 
 * @see ModifiableMemberMarket
 * @author swenmouth
 */
public class SimpleMemberMarketImpl extends SimpleMarketImpl implements Comparable<MemberMarket>, Serializable, MemberMarket {

	private static final long serialVersionUID = 6523931916190013499L;
	
	Integer maxDistanceFromDealer;
    Integer maxMileage;
    Integer minMileage;
	Integer timePeriod;
	boolean exactMatches;
	boolean suppress;
	
	public SimpleMemberMarketImpl(Integer marketID, String name, MarketType marketType, MarketAdministrator marketAdministrator, Integer maxDistanceFromDealer, Integer timePeriod, boolean exactMatches, boolean suppress, Integer maxMileage, Integer minMileage) {
		super(marketID, name, marketType, marketAdministrator);
		this.maxDistanceFromDealer = maxDistanceFromDealer;
		this.timePeriod = timePeriod;
		this.exactMatches = exactMatches;
		this.suppress = suppress;
		this.maxMileage = maxMileage;
		this.minMileage = minMileage;
	}

	public boolean getExactMatches() {
		return exactMatches;
	}

	public void setExactMatches(boolean exactMatches) {
		this.exactMatches = exactMatches;
	}

	public Integer getMaxDistanceFromDealer() {
		return maxDistanceFromDealer;
	}

	public void setMaxDistanceFromDealer(Integer maxDistance) {
		this.maxDistanceFromDealer = maxDistance;
	}

	public boolean getSuppress() {
		return suppress;
	}

	public void setSuppress(boolean suppress) {
		this.suppress = suppress;
	}

	public Integer getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(Integer timePeriod) {
		this.timePeriod = timePeriod;
	}
    
	public int compareTo(MemberMarket memberMarket) {
		if (memberMarket == null)
			return 1;
		int c = 0;
		c = Functions.nullSafeCompareTo(getMarketType(), memberMarket.getMarketType(), c);
		c = Functions.nullSafeCompareTo(getName(), memberMarket.getName(), c);
		return c;
	}

    public Integer getMaxMileage() {
        return maxMileage;
    }

    public void setMaxMileage(Integer maxMileage) {
        this.maxMileage = maxMileage;
    }

    public Integer getMinMileage() {
        return minMileage;
    }

    public void setMinMileage(Integer minMileage) {
        this.minMileage = minMileage;
    }

}
