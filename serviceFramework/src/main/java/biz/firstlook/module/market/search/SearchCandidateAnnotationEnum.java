package biz.firstlook.module.market.search;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import biz.firstlook.commons.util.Functions;

public class SearchCandidateAnnotationEnum extends AbstractSearchCandidateAnnotation implements Serializable, SearchCandidateAnnotation {
	
	private static final long serialVersionUID = 6322142556514059508L;
	
	public static final SearchCandidateAnnotation POWERZONE         = new SearchCandidateAnnotationEnum("PowerZone");
	public static final SearchCandidateAnnotation WINNERS           = new SearchCandidateAnnotationEnum("Winners");
	public static final SearchCandidateAnnotation GOODBETS          = new SearchCandidateAnnotationEnum("GoodBets");
	public static final SearchCandidateAnnotation MARKET_PERFORMERS = new SearchCandidateAnnotationEnum("MarketPerformers");
	public static final SearchCandidateAnnotation MANAGERS_CHOICE   = new SearchCandidateAnnotationEnum("Managers Choice");
	
	public static final Set<SearchCandidateAnnotation> ALL;
	static {
		Set<SearchCandidateAnnotation> tmp = new HashSet<SearchCandidateAnnotation>();
		tmp.add(POWERZONE);
		tmp.add(WINNERS);
		tmp.add(GOODBETS);
		tmp.add(MARKET_PERFORMERS);
		tmp.add(MANAGERS_CHOICE);
		ALL = Collections.unmodifiableSet(tmp);
	}
	
	private String name;
	
	SearchCandidateAnnotationEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	private Object readResolve() {
		if (Functions.nullSafeEquals(POWERZONE.getName(), name)) {
			return POWERZONE;
		}
		else if (Functions.nullSafeEquals(WINNERS.getName(), name)) {
			return WINNERS;
		}
		else if (Functions.nullSafeEquals(GOODBETS.getName(), name)) {
			return GOODBETS;
		}
		else if (Functions.nullSafeEquals(MARKET_PERFORMERS.getName(), name)) {
			return MARKET_PERFORMERS;
		}
		else if (Functions.nullSafeEquals(MANAGERS_CHOICE.getName(), name)) {
			return MANAGERS_CHOICE;
		}
		return this;
	}
}
