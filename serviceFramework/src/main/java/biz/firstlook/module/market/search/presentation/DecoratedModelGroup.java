package biz.firstlook.module.market.search.presentation;

import java.util.Collection;
import java.util.List;

import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;

public interface DecoratedModelGroup extends ModelGroup {
	public Double getContribution();
	public List<DecoratedModel> getModels();
	public List<DecoratedModelYear> getModelYears();
	public Collection<SearchCandidate> getSearchCandidates();
}
