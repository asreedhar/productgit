package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import biz.firstlook.commons.collections.CollectionAdapter;
import biz.firstlook.commons.hibernate.CollectionMutatorCallback;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOption;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;

/**
 * Adapts a <code>SearchCandidateList<code> entity into a Collection of the
 * <code>SearchCandidate</code> interface.
 * 
 * @author swenmouth
 */
public class SearchCandidateListCollectionAdapter extends
	CollectionAdapter<biz.firstlook.module.market.search.SearchCandidate,biz.firstlook.module.market.search.impl.entity.SearchCandidate>
	implements Serializable,
	CollectionMutatorCallback<biz.firstlook.module.market.search.SearchCandidate, biz.firstlook.module.market.search.impl.entity.SearchCandidateList> {

	private static final long serialVersionUID = -6067955256145036710L;
	
	private biz.firstlook.module.market.search.impl.entity.SearchCandidateList searchCandidateList;
	
	public SearchCandidateListCollectionAdapter(biz.firstlook.module.market.search.impl.entity.SearchCandidateList searchCandidateList) {
		super(searchCandidateList.getSearchCandidates());
		this.searchCandidateList = searchCandidateList;
	}

	public boolean add(biz.firstlook.module.market.search.SearchCandidate element) {
		if (!(element instanceof SearchCandidateAdapter)) {
			return false;
		}
		// find matching make, line and segment
		biz.firstlook.module.market.search.impl.entity.SearchCandidate tgt = ((SearchCandidateAdapter) element).getSearchCandidate();
		biz.firstlook.module.market.search.impl.entity.SearchCandidate src = null;
		
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidate row : searchCandidateList.getSearchCandidates()) {
			if (Functions.nullSafeEquals(row.getModelID(), tgt.getModelID())) {
				src = row;
				break;
			}
		}
		// quick win: new model
		if (src == null) {
			searchCandidateList.getSearchCandidates().add(tgt);
			return true;
		}
		// un-suppress
		if (src.getSuppress()) {
			src.setSuppress(false);
		}
		// add new candidate annotations
		src.getSearchCandidateAnnotations().addAll(tgt.getSearchCandidateAnnotations());
		// extract "new" model years into a map to speed up contains query
		Map<Integer,biz.firstlook.module.market.search.impl.entity.SearchCandidateOption> modelYears = new HashMap<Integer,biz.firstlook.module.market.search.impl.entity.SearchCandidateOption>();
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidateOption option : src.getSearchCandidateOptions()) {
			modelYears.put(option.getModelYear(), option);
		}
		// search for new model-years
		boolean addOrUpdate = false;
		System.err.println(tgt.getModelID());
		debug("src", src);
		debug("tgt", tgt);
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidateOption newOption : tgt.getSearchCandidateOptions()) {
			final SearchCandidateOption oldOption = modelYears.get(newOption.getModelYear());
			if (oldOption != null) {
				if (!newOption.getSuppress()) {
					if (oldOption.getSuppress()) {
						oldOption.setSuppress(false);
					}
					oldOption.setExpires(newOption.getExpires());
					oldOption.getSearchCandidateOptionAnnotations().addAll(newOption.getSearchCandidateOptionAnnotations());
					addOrUpdate = true;
				}
				else {
					LogFactory.getLog(SearchCandidateListCollectionAdapter.class).warn("Attempt to add a suppressed option!");
				}
			}
			else {
				debug("new.", newOption);
				src.getSearchCandidateOptions().add(newOption);
				addOrUpdate = true;
			}
		}
		debug("src:end", src);
		return addOrUpdate;
	}

	private void debug(String msg, biz.firstlook.module.market.search.impl.entity.SearchCandidate src) {
		System.err.println(msg);
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidateOption option : src.getSearchCandidateOptions()) {
			debug(null, option);
		}
	}

	private void debug(String msg, biz.firstlook.module.market.search.impl.entity.SearchCandidateOption option) {
		StringBuffer sb = new StringBuffer((msg == null ? "" : msg));
		sb.append("option: [id=" + option.getSearchCandidateOptionID() + ", suppressed=" + option.getSuppress() + ", year=" + option.getModelYear() + "] ");
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType annotation : option.getSearchCandidateOptionAnnotations()) {
			sb.append(" [" + annotation.getDescription() + "] ");
		}
		System.err.println(sb.toString());
	}

	public Iterator<biz.firstlook.module.market.search.SearchCandidate> iterator() {
		return new MutableItr(collection.iterator());
	}

	public biz.firstlook.module.market.search.SearchCandidate adaptItem(biz.firstlook.module.market.search.impl.entity.SearchCandidate candidate) {
		return new SearchCandidateAdapter(candidate);
	}
	
	public void synchEntity(biz.firstlook.module.market.search.impl.entity.SearchCandidateList entity) {
		searchCandidateList = entity;
		collection = entity.getSearchCandidates();
	}

	class MutableItr extends Itr {
		biz.firstlook.module.market.search.impl.entity.SearchCandidate current;
		public MutableItr(Iterator<biz.firstlook.module.market.search.impl.entity.SearchCandidate> it) {
			super(it);
		}
		public boolean hasNext() {
			while (it.hasNext()) {
				current = it.next();
				if (!current.getSuppress())
					return true;
			}
			return false;
		}
		public biz.firstlook.module.market.search.SearchCandidate next() {
			return adaptItem(current);
		}
		public void remove() {
			for (SearchCandidateOption searchCandidateOption : current.getSearchCandidateOptions()) {
				searchCandidateOption.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.HOTLIST);
				searchCandidateOption.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.ONETIME);
				searchCandidateOption.setSuppress(true);
			}
			current.setSuppress(true);
		}
	}	
}
