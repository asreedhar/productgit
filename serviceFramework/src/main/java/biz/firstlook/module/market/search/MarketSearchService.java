package biz.firstlook.module.market.search;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.module.core.Member;

public interface MarketSearchService {
	@Transactional(rollbackFor={RuntimeException.class,Exception.class})
	public Collection<SearchCandidate> getCandidateList(Member m, PersistenceScope s);
	@Transactional(rollbackFor={RuntimeException.class,Exception.class})
	public SearchContext getContext(Member m, PersistenceScope s);
	public List<MarketSummary> summary(SearchContext ctx, Collection<SearchCandidate> candidates,Boolean isFirstTimeSearch);
	public List<MarketVehicle> search(SearchContext ctx, Collection<SearchCandidate> candidates,Boolean isFirstTimeSearch);
}
