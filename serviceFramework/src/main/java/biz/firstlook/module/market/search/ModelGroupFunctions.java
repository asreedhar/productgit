package biz.firstlook.module.market.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

public class ModelGroupFunctions {

	public static final Set<SearchCandidateAnnotation> getSearchCandidateAnnotations(
			Collection<SearchCandidate> searchCandidates,
			Set<SearchCandidateAnnotation> annotations) {
		Set<SearchCandidateAnnotation> intersection = new HashSet<SearchCandidateAnnotation>();
		for (SearchCandidate searchCandidate: searchCandidates) {
			intersection.addAll(Functions.intersection(annotations, searchCandidate.getAnnotations()));
		}
		return intersection;
	}

	public static final List<SearchCandidate> newSearchCandidates(
			List<? extends Model> models,
			Set<SearchCandidateAnnotation> annotations) {
		MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		List<SearchCandidate> newSearchCandidates = new ArrayList<SearchCandidate>();
		for (Model model : models) {
			SearchCandidate newSearchCandidate = builder.newSearchCandidate(model);
			newSearchCandidate.getAnnotations().addAll(annotations);
			newSearchCandidates.add(newSearchCandidate);
		}
		return newSearchCandidates;
	}
	
	public static final Set<SearchCandidateCriteria> union(
			Collection<SearchCandidateCriteria> srcSearchCandidateCriteria,
			Collection<SearchCandidateCriteria> dstSearchCandidateCriteria) {
		Set<SearchCandidateCriteria> all = new HashSet<SearchCandidateCriteria>();
		for (SearchCandidateCriteria src : srcSearchCandidateCriteria) {
			SearchCandidateCriteria out = src;
			for (SearchCandidateCriteria dst: dstSearchCandidateCriteria) {
				if (isNull(dst))
					continue;
				if (src.equals(dst)) {
					out = cloneSearchCandidateCriteria(src, dst.getExpires());
					out.getAnnotations().addAll(dst.getAnnotations());
					boolean isHotList = out.getAnnotations().contains(SearchCandidateCriteriaAnnotationEnum.HOTLIST);
					boolean isOneTime = out.getAnnotations().contains(SearchCandidateCriteriaAnnotationEnum.ONETIME);
					if (isOneTime && isHotList) {
						out.getAnnotations().remove(SearchCandidateCriteriaAnnotationEnum.ONETIME);
					}
					break;
				}
			}
			all.add(out);
		}
		DST: for (SearchCandidateCriteria dst: dstSearchCandidateCriteria) {
			if (isNull(dst))
				continue;
			for (SearchCandidateCriteria src : srcSearchCandidateCriteria) {
				if (dst.equals(src)) {
					continue DST;
				}
			}
			all.add(dst);
		}
		return all;
	}

	private static final boolean isNull(SearchCandidateCriteria dst) {
		final boolean nullModelYear = (dst.getModelYear() == null || dst.getModelYear().getModelYear() == null);
		final boolean nullExpires = dst.getExpires() == null;
		final boolean nullAnnotations = dst.getAnnotations().isEmpty();
		return (nullModelYear && nullExpires && nullAnnotations);
	}

	public static final Set<SearchCandidateCriteria> getSearchCandidateCriteria(
			Collection<SearchCandidate> searchCandidates,
			Collection<? extends ModelYear> modelYears,
			Set<SearchCandidateCriteriaAnnotation> annotations) {
		Set<SearchCandidateCriteria> subset = new HashSet<SearchCandidateCriteria>();
		for (SearchCandidate searchCandidate: searchCandidates) {
			for (SearchCandidateCriteria searchCandidateCriteria: searchCandidate.getCriteria()) {
				for (ModelYear modelYear : modelYears) {
					if (!searchCandidateCriteria.getModelYear().equals(modelYear)) {
						continue;
					}
					SearchCandidateCriteria newSearchCandidateCriteria = null;
					for (SearchCandidateCriteriaAnnotation searchCandidateCriteriaAnnotation: searchCandidateCriteria.getAnnotations()) {
						if (annotations.contains(searchCandidateCriteriaAnnotation)) {
							if (newSearchCandidateCriteria == null) {
								newSearchCandidateCriteria = cloneSearchCandidateCriteria(searchCandidateCriteria);
							}
							newSearchCandidateCriteria.getAnnotations().add(searchCandidateCriteriaAnnotation);
						}
					}
				}
			}
		}
		return subset;
	}
	
	public static final void setSearchCandidateCriteria(
			Collection<SearchCandidate> searchCandidates,
			Collection<SearchCandidateCriteria> searchCandidateCriteria) {
		for (SearchCandidate searchCandidate : searchCandidates) {
			searchCandidate.getCriteria().clear();
			for (SearchCandidateCriteria searchCandidateCriterion : searchCandidateCriteria) {
				searchCandidate.getCriteria().add(cloneSearchCandidateCriteria(searchCandidateCriterion));
			}
		}
	}
	
	public static final Set<ModelYear> getModelYears(Collection<SearchCandidateCriteria> searchCandidateCriteria) {
		Set<ModelYear> modelYears = new HashSet<ModelYear>();
		for (SearchCandidateCriteria searchCandidateCriterion : searchCandidateCriteria) {
			modelYears.add(searchCandidateCriterion.getModelYear());
		}
		return modelYears;
	}
	
	public static final Set<SearchCandidateCriteria> validate(List<SearchCandidateCriteria> searchCandidateCriteria) {
		final VehicleComponentBuilder vehicleBuilder = VehicleComponentBuilderFactory.getInstance().newVehicleBuilder();
		final MarketSearchComponentBuilder marketSearchBuilder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		final Set<SearchCandidateCriteria> notNull = new HashSet<SearchCandidateCriteria>();
		for (SearchCandidateCriteria searchCandidateCriterion : searchCandidateCriteria) {
			if (isNull(searchCandidateCriterion)) {
				continue;
			}
			if (searchCandidateCriterion.getAnnotations().isEmpty()) {
				searchCandidateCriterion.getAnnotations().add(SearchCandidateCriteriaAnnotationEnum.ONETIME);
			}
			if (searchCandidateCriterion.getModelYear().getModelYear() == null) {
				return Collections.singleton(cloneSearchCandidateCriteria(searchCandidateCriterion));
			}
			notNull.add(cloneSearchCandidateCriteria(searchCandidateCriterion));
		}
		if (notNull.isEmpty()) {
			SearchCandidateCriteria searchCandidateCriterion = marketSearchBuilder.newSearchCandidateCriteria(
					vehicleBuilder.buildModelYear(null),
					null);
			searchCandidateCriterion.getAnnotations().add(SearchCandidateCriteriaAnnotationEnum.ONETIME);
			notNull.add(searchCandidateCriterion);
		}
		return notNull;
	}

	public static SearchCandidateCriteria cloneSearchCandidateCriteria(SearchCandidateCriteria searchCandidateCriterion) {
		return cloneSearchCandidateCriteria(searchCandidateCriterion, searchCandidateCriterion.getExpires());
	}
	
	public static SearchCandidateCriteria cloneSearchCandidateCriteria(SearchCandidateCriteria searchCandidateCriterion, Date expires) {
		final MarketSearchComponentBuilder marketSearchBuilder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		SearchCandidateCriteria newSearchCandidateCriterion = marketSearchBuilder.newSearchCandidateCriteria(
				searchCandidateCriterion.getModelYear(),
				expires);
		newSearchCandidateCriterion.getAnnotations().addAll(searchCandidateCriterion.getAnnotations());
		return newSearchCandidateCriterion;
	}
	
}
