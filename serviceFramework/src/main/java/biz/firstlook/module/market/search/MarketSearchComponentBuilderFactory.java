package biz.firstlook.module.market.search;

public abstract class MarketSearchComponentBuilderFactory {

	protected MarketSearchComponentBuilderFactory() {
		super();
		MarketSearchComponentBuilderFactory.setFactory(this);
	}

	public abstract MarketSearchComponentBuilder newMarketSearchBuilder();
	
	private static MarketSearchComponentBuilderFactory factory;
	
	public static MarketSearchComponentBuilderFactory getInstance() {
		return factory;
	}

	public static void setFactory(MarketSearchComponentBuilderFactory factory) {
		MarketSearchComponentBuilderFactory.factory = factory;
	}
	
}
