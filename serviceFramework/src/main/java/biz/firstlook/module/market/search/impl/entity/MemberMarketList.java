package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@Table(name="tbl_MemberATCAccessGroupList")
public class MemberMarketList implements Serializable {
	
	private static final long serialVersionUID = 1839096837508408491L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column(name="memberATCAccessGroupListID")
	private Integer memberMarketListID;
	
	private Integer memberID;
	private Integer businessUnitID;
	private Integer maxDistanceFromDealer;

	@ManyToOne
    @JoinColumn(name="memberATCAccessGroupListTypeID")
	private MemberMarketListType memberMarketListType;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="memberATCAccessGroupListID",nullable=false)
	@OrderBy("position ASC")
	@org.hibernate.annotations.Cascade(value={org.hibernate.annotations.CascadeType.ALL,org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
	private List<MemberMarket> memberMarkets;
	
	public MemberMarketList() {
		super();
	}
	
	public MemberMarketList(Integer memberMarketListID, Integer memberID, Integer businessUnitID, MemberMarketListType memberMarketListType, List<MemberMarket> memberMarkets, Integer maxDistanceFromDealer) {
		super();
		this.memberMarketListID = memberMarketListID;
		this.memberID = memberID;
		this.businessUnitID = businessUnitID;
		this.memberMarketListType = memberMarketListType;
		this.memberMarkets = memberMarkets;
		this.maxDistanceFromDealer = maxDistanceFromDealer;
	}

	public Integer getBusinessUnitID() {
		return businessUnitID;
	}
	public void setBusinessUnitID(Integer businessUnitID) {
		this.businessUnitID = businessUnitID;
	}
	
	public Integer getMemberMarketListID() {
		return memberMarketListID;
	}
	public void setMemberMarketListID(Integer memberMarketListID) {
		this.memberMarketListID = memberMarketListID;
	}
	
	public Integer getMemberID() {
		return memberID;
	}
	public void setMemberID(Integer memberID) {
		this.memberID = memberID;
	}

	public MemberMarketListType getMemberMarketListType() {
		return memberMarketListType;
	}
	public void setMemberMarketListType(MemberMarketListType memberMarketListType) {
		this.memberMarketListType = memberMarketListType;
	}

	public List<MemberMarket> getMemberMarkets() {
		return memberMarkets;
	}
	public void setMemberMarkets(List<MemberMarket> memberMarkets) {
		this.memberMarkets = memberMarkets;
	}

	public Integer getMaxDistanceFromDealer() {
		return maxDistanceFromDealer;
	}
	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
		this.maxDistanceFromDealer = maxDistanceFromDealer;
	}

    public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof MemberMarketList) {
			MemberMarketList s = (MemberMarketList) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getMemberID(), s.getMemberID());
			lEquals &= Functions.nullSafeEquals(getBusinessUnitID(), s.getBusinessUnitID());
			lEquals &= Functions.nullSafeEquals(getMemberMarketListType(), s.getMemberMarketListType());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getMemberID());
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getBusinessUnitID());
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getMemberMarketListType());
		return hashCode;
	}
}
