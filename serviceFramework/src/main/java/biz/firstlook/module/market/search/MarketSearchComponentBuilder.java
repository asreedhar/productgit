package biz.firstlook.module.market.search;

import java.util.Date;
import java.util.List;

import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

public abstract class MarketSearchComponentBuilder {
	public abstract SearchCandidate newSearchCandidate(Model model);
	public abstract SearchCandidateCriteria newSearchCandidateCriteria(ModelYear modelYear, Date expires);
	public abstract SearchContext newContext(SearchContext ctx, MarketSummary m);
	public abstract SearchContext newContext(SearchContext ctx, MemberMarket m);
	public abstract SearchContext newContext(SearchContext ctx, List<MemberMarket> m);
}
