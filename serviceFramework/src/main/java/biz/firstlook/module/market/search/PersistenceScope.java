package biz.firstlook.module.market.search;

public enum PersistenceScope {
	PERMANENT, TODAY, TEMPORARY;
}
