package biz.firstlook.module.market.search.impl;

import java.io.Serializable;
import java.util.Date;

import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Trim;
import biz.firstlook.module.purchasing.vehicle.Vehicle;

/**
 * Simple (Java Bean) implementation of the <code>MarketVehicle</code> interface.
 * 
 * @author swenmouth
 */
public class SimpleMarketVehicleImpl implements Serializable, MarketVehicle {
	private static final long serialVersionUID = -116395938878299266L;
	Market market;
	Vehicle vehicle;
	Integer unitCost;
	Integer currentBid;
	Integer buyPrice;
	String location;
	String dealerName;
	String expires;
	Date inventoryReceivedDate;
	Integer distanceFromDealer;
	String lot;
	String run;
	Boolean candidateMatch;
	String consignor;
	String amsOption;
	String amsAnnouncement;
	Integer inventoryId;
	String url;
	Integer makeModelGroupingId;
	Integer groupingDescriptionId;
    Integer marketId;
    Integer vehicleEntityId;
    String stockNumber;
    Float transferPrice;
    Boolean transferForRetailOnly;
    String title;
    
	public SimpleMarketVehicleImpl(Market market, Vehicle vehicle, Integer unitCost, 
			Integer currentBid, Integer buyPrice, String location, String dealerName, 
			String expires, Date inventoryReceivedDate, Integer distanceFromDealer, 
			String lot, String run, Boolean candidateMatch, String consignor, 
			String amsOption, String amsAnnouncement, Integer inventoryId, String url, 
			Integer makeModelGroupingId, Integer groupingDescriptionId, Integer marketId, 
			Integer vehicleEntityId, String stockNumber, Float transferPrice, Boolean transferForRetailOnly,String title) {
		this.market = market;
		this.vehicle = vehicle;
		this.unitCost = unitCost;
		this.currentBid = currentBid;
		this.buyPrice = buyPrice;
		this.location = location;
		this.dealerName = dealerName;
		this.expires = expires;
		this.inventoryReceivedDate = inventoryReceivedDate;
		this.distanceFromDealer = distanceFromDealer;
		this.lot = lot;
		this.run = run;
		this.candidateMatch = candidateMatch;
		this.consignor = consignor;
		this.amsOption = amsOption;
		this.amsAnnouncement = amsAnnouncement;
		this.inventoryId = inventoryId;
		this.url = url;
		this.makeModelGroupingId = makeModelGroupingId;
		this.groupingDescriptionId = groupingDescriptionId;
        this.marketId = marketId;
        this.vehicleEntityId = vehicleEntityId;
        this.stockNumber = stockNumber;
        this.transferPrice = transferPrice;
        this.transferForRetailOnly = transferForRetailOnly;
        this.title=title;
	}
	// Market
	public String getName() {
		return market.getName();
	}
	public MarketType getMarketType() {
		return market.getMarketType();
	}
	public int getMarketTypeOrdinal(){
		return market.getMarketType().ordinal();
	}
	public MarketAdministrator getMarketAdministrator() {
		return market.getMarketAdministrator();
	}
	// Vehicle
	public String getColor() {
		return vehicle.getColor();
	}
	public Model getModel() {
		return vehicle.getModel();
	}
	public ModelYear getModelYear() {
		return vehicle.getModelYear();
	}
	public Trim getTrim() {
		return vehicle.getTrim();
	}
	public Integer getMileage() {
		return vehicle.getMileage();
	}
	public String getEngine() {
		return vehicle.getEngine();
	}
	public String getVIN() {
		return vehicle.getVIN();
	}
	public String getDriveTrain() {
		return vehicle.getDriveTrain();
	}
	// MarketVehicle
	public Integer getUnitCost() {
		//Is null when DealerPreference.DisplayUnitCostToDealerGroup is true.
		return unitCost;
	}
	public Integer getCurrentBid() {
		return currentBid;
	}
	public Date getInventoryReceivedDate() {
		return inventoryReceivedDate;
	}
	public String getExpires() {
		return expires;
	}
	public Integer getBuyPrice() {
		return buyPrice;
	}
	public String getLocation() {
		return location;
	}
	public Integer getDistanceFromDealer() {
		return distanceFromDealer;
	}
	public String getDealerName() {
		return dealerName;
	}
	public String getLot() {
		return lot;
	}
	public String getRun() {
		return run;
	}
	public Boolean getCandidateMatch() {
		return candidateMatch;
	}
	public String getAMSAnnouncement() {
		return amsAnnouncement;
	}
	public String getAMSOption() {
		return amsOption;
	}
	public String getConsignor() {
		return consignor;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	public Integer getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int compareTo(Vehicle v) {
		if (v == null)
			return 1;
		return vehicle.compareTo(v);
	}
	public Integer getMakeModelGroupingId() {
		return makeModelGroupingId;
	}
	public void setMakeModelGroupingId(Integer makeModelGroupingId) {
		this.makeModelGroupingId = makeModelGroupingId;
	}
	public Integer getGroupingDescriptionId() {
		return groupingDescriptionId;
	}
	public void setGroupingDescriptionId(Integer groupingDescriptionId) {
		this.groupingDescriptionId = groupingDescriptionId;
	}
    public Integer getMarketId() {
        return marketId;
    }
	public Float getTransferPrice() {
		return transferPrice;
	}
	public void setTransferPrice(Float transferPrice) {
		this.transferPrice = transferPrice;
	}
	public Boolean getTransferForRetailOnly() {
		return transferForRetailOnly;
	}
	public void setTransferForRetailOnly(Boolean transferForRetailOnly) {
		this.transferForRetailOnly = transferForRetailOnly;
	}
	public Integer getVehicleEntityId() {
		if(getMarketType() == MarketType.IN_GROUP){
			return inventoryId;
		}
		else return vehicleEntityId;
	}
	public Integer getVehicleEntityTypeId() {
		if(getMarketType() == MarketType.ONLINE){
			return 3;
		}
		else if(getMarketType() == MarketType.IN_GROUP){
			return 4;
		}
		return null;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
