package biz.firstlook.module.market.search.presentation.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.presentation.DecoratedModel;
import biz.firstlook.module.market.search.presentation.DecoratedModelGroup;
import biz.firstlook.module.market.search.presentation.DecoratedModelYear;
import biz.firstlook.module.purchasing.vehicle.AbstractModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;

public class DecoratedModelGroupImpl extends AbstractModelGroup implements
		Serializable, DecoratedModelGroup {
	
	private static final long serialVersionUID = 4221697663644198271L;
	
	private Double contribution;
	private ModelGroup modelGroup;
	private Collection<SearchCandidate> searchCandidates;
	private List<DecoratedModelYear> modelYears;
	private List<DecoratedModel> models;
	
	public DecoratedModelGroupImpl(Double contribution, ModelGroup modelGroup, Collection<SearchCandidate> searchCandidates, List<DecoratedModelYear> modelYears, List<DecoratedModel> models) {
		super();
		this.contribution = contribution;
		this.modelGroup = modelGroup;
		this.searchCandidates = searchCandidates;
		this.modelYears = modelYears;
		this.models = models;
	}

	public Double getContribution() {
		return contribution;
	}

	public List<DecoratedModelYear> getModelYears() {
		return modelYears;
	}

	public List<DecoratedModel> getModels() {
		return models;
	}

	public Collection<SearchCandidate> getSearchCandidates() {
		return searchCandidates;
	}

	public Integer getModelGroupID() {
		return modelGroup.getModelGroupID();
	}

	public String getName() {
		return modelGroup.getName();
	}

}
