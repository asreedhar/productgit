package biz.firstlook.module.market.search.impl;

import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;

/**
 * Spring managed implementation of the <code>MarketSearchComponentBuilderFactory</code>
 * class.
 * 
 * @author swenmouth
 *
 */
public class MarketSearchComponentBuilderFactoryImpl extends
		MarketSearchComponentBuilderFactory {

	private static MarketSearchComponentBuilder builder;
	
	protected MarketSearchComponentBuilderFactoryImpl() {
		super();
		MarketSearchComponentBuilderFactory.setFactory(this);
	}

	public MarketSearchComponentBuilder newMarketSearchBuilder() {
		return builder;
	}

	static void setBuilder(MarketSearchComponentBuilder builder) {
		MarketSearchComponentBuilderFactoryImpl.builder = builder;
	}

}
