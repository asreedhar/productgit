package biz.firstlook.module.market.search.impl;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.Market;

/**
 * Abstract implementation of <code>Market</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not
 * necessarily provide their own.
 *   
 * @author swenmouth
 */
public abstract class AbstractMarket implements Market {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof Market) {
			Market m = (Market) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getName(), m.getName());
			lEquals &= Functions.nullSafeEquals(getMarketType(), m.getMarketType());
			return lEquals;
		}
		return false;
	}
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getName());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getMarketType());
		return hashCode;
	}
}
