package biz.firstlook.module.market.search;

public enum MarketType implements Comparable<MarketType> {
	ONLINE(1), IN_GROUP(2), LIVE(3);
	int id;
	MarketType(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}
}
