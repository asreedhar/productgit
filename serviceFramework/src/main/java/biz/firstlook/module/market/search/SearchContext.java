package biz.firstlook.module.market.search;

import java.util.List;

import biz.firstlook.module.core.Member;

public interface SearchContext {
	public Member getMember();
	public PersistenceScope getPersistenceScope();
	public List<MemberMarket> getMarkets();
	public Integer getMaxDistanceFromBusinessUnit();
	public Integer getTimePeriod();
	public boolean getExactMatches();
    public Integer getMaxMileage();
    public Integer getMinMileage();
}
