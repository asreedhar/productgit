package biz.firstlook.module.market.search;

import java.util.Date;

import biz.firstlook.module.purchasing.vehicle.Trim;
import biz.firstlook.module.purchasing.vehicle.Vehicle;

public interface MarketVehicle extends Market, Vehicle {
	public Integer getUnitCost();
	public Integer getCurrentBid();
	public Integer getBuyPrice();
	public String getExpires();
	public Date getInventoryReceivedDate();
	public String getLocation();
	public String getDealerName();
	public Integer getDistanceFromDealer();
	public String getLot();
	public String getRun();
	public Boolean getCandidateMatch();
	public String getConsignor();
	public String getAMSOption();
	public String getAMSAnnouncement();
	public Trim getTrim();
	public Integer getVehicleEntityTypeId();
	public Integer getVehicleEntityId();
	public String getStockNumber();
	public Float getTransferPrice();
	public Boolean getTransferForRetailOnly();
}
