package biz.firstlook.module.market.search;

import java.util.Date;
import java.util.Set;

import biz.firstlook.module.purchasing.vehicle.ModelYear;

public interface SearchCandidateCriteria extends Comparable<SearchCandidateCriteria> {
	public ModelYear getModelYear();
	public Date getExpires();
	public Set<SearchCandidateCriteriaAnnotation> getAnnotations();
}
