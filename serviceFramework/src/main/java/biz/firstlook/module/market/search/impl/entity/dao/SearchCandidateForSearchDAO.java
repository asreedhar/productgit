package biz.firstlook.module.market.search.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateForSearch;

public class SearchCandidateForSearchDAO extends GenericHibernate<SearchCandidateForSearch, Integer> implements IGenericDAO<SearchCandidateForSearch, Integer> {

	public SearchCandidateForSearchDAO() {
		super(SearchCandidateForSearch.class);
	}
	
	
}
