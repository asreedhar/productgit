package biz.firstlook.module.market.search.impl;

import java.io.Serializable;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.MemberMarket;

/**
 * Wrapper class for an instance of <code>MemberMarket</code> that allows a client to
 * defer comitting changes to the instance until {@link #commit} is invoked.
 * 
 * @author swenmouth
 */
public class ModifiableMemberMarket extends SimpleMemberMarketImpl implements Comparable<MemberMarket>, Serializable, MemberMarket, MemberMarketWrapper {

	private static final long serialVersionUID = 1754666839396937738L;
	
	MemberMarket memberMarket;
	
	public ModifiableMemberMarket(MemberMarket memberMarket) {
		super(null,
				memberMarket.getName(),
				memberMarket.getMarketType(),
				memberMarket.getMarketAdministrator(),
				memberMarket.getMaxDistanceFromDealer(),
				memberMarket.getTimePeriod(),
				memberMarket.getExactMatches(),
				memberMarket.getSuppress(),
                memberMarket.getMaxMileage(),
                memberMarket.getMinMileage());
		this.memberMarket = memberMarket;
	}

	public MemberMarket getMemberMarket() {
		return memberMarket;
	}

	public void commit() {
		getMemberMarket().setExactMatches(getExactMatches());
		getMemberMarket().setMaxDistanceFromDealer(getMaxDistanceFromDealer());
		getMemberMarket().setSuppress(getSuppress());
		getMemberMarket().setTimePeriod(getTimePeriod());
        getMemberMarket().setMaxMileage(getMaxMileage());
        getMemberMarket().setMinMileage(getMinMileage());
	}

	public int compareTo(MemberMarket otherMemberMarket) {
		MemberMarket m0 = memberMarket;
		while (m0 instanceof MemberMarketWrapper) {
			m0 = ((MemberMarketWrapper)m0).getMemberMarket();
		}
		MemberMarket m1 = otherMemberMarket;
		while (m1 instanceof MemberMarketWrapper) {
			m1 = ((MemberMarketWrapper)m1).getMemberMarket();
		}
		return Functions.nullSafeCompareTo(m0, m1);
	}
	
}
