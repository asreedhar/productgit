package biz.firstlook.module.market.search.presentation.impl;

import java.io.Serializable;

import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.presentation.DecoratedModel;
import biz.firstlook.module.purchasing.vehicle.AbstractModel;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.Segment;

public class DecoratedModelImpl extends AbstractModel implements Serializable, DecoratedModel {
	
	private static final long serialVersionUID = -4512463246392300392L;
	
	private Model model;
	private SearchCandidate searchCandidate;
	
	public DecoratedModelImpl(Model model, SearchCandidate searchCandidate) {
		super();
		this.model = model;
		this.searchCandidate = searchCandidate;
	}

	public SearchCandidate getSearchCandidate() {
		return searchCandidate;
	}

	public Integer getId() {
		return model.getId();
	}

	public String getName() {
		return model.getName();
	}

	public Line getLine() {
		return model.getLine();
	}

	public Make getMake() {
		return model.getMake();
	}

	public ModelGroup getModelGroup() {
		return model.getModelGroup();
	}

	public Segment getSegment() {
		return model.getSegment();
	}

}
