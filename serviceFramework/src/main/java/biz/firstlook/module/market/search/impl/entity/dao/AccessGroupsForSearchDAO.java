package biz.firstlook.module.market.search.impl.entity.dao;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.market.search.impl.entity.AccessGroupsForSearch;

public class AccessGroupsForSearchDAO extends GenericHibernate<AccessGroupsForSearch, Integer> implements IGenericDAO<AccessGroupsForSearch, Integer> {

	public AccessGroupsForSearchDAO() {
		super(AccessGroupsForSearch.class);
	}

	
}
