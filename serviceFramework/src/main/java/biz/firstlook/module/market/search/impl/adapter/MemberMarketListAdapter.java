package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import biz.firstlook.commons.hibernate.Persistor;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.impl.entity.MemberMarketList;
import biz.firstlook.module.market.search.impl.entity.dao.MemberMarketListDAO;

/**
 * Wraps a {@link MemberMarketList} and presents it as a list of {@link MemberMarket}s.
 * 
 * Changes to the underlying <code>MemberMarketList</code> are committed back to its
 * data store.
 * 
 * @author swenmouth
 */
public class MemberMarketListAdapter extends
		AbstractList<biz.firstlook.module.market.search.MemberMarket> implements
		Serializable,
		List<biz.firstlook.module.market.search.MemberMarket> {

	private static final long serialVersionUID = 7396792158566420443L;
	
	private MemberMarketListDAO memberMarketListDAO;
	private MemberMarketList memberMarketList;
	private Persistor<MemberMarketList,Integer> persistor;
	private List<MemberMarketGroupAdapter> markets;

	public MemberMarketListAdapter(MemberMarketListDAO memberMarketListDAO, MemberMarketList memberMarketList, Persistor<MemberMarketList,Integer> persistor) {
		super();
		this.memberMarketListDAO = memberMarketListDAO;
		this.memberMarketList = memberMarketList;
		this.persistor = persistor;
		init();
	}

	private void init() {
		this.markets = new ArrayList<MemberMarketGroupAdapter>();
		MM: for (biz.firstlook.module.market.search.impl.entity.MemberMarket memberMarket : memberMarketList.getMemberMarkets()) {
			for (MemberMarketGroupAdapter marketGroup : markets) {
				if (marketGroup.equals(memberMarket)) {
					marketGroup.add(memberMarket);
					continue MM;
				}
			}
			markets.add(new MemberMarketGroupAdapter(memberMarketListDAO, memberMarketList, persistor, memberMarket));
		}
		Collections.sort(this.markets);
	}

	public biz.firstlook.module.market.search.MemberMarket get(int index) {
		return markets.get(index);
	}

	public int size() {
		return markets.size();
	}

	public void add(int index, MemberMarket memberMarket) {
		if (!(memberMarket instanceof MemberMarketGroupAdapter)) {
			return;
		}
		final MemberMarketGroupAdapter memberMarketGroup = (MemberMarketGroupAdapter) memberMarket;
		memberMarketGroup.setPosition(index);
		markets.add(memberMarketGroup);
		setPosition(index);
	}

	public MemberMarket remove(int index) {
		final MemberMarketGroupAdapter memberMarketGroup =  markets.remove(index);
		setPosition(index);
		return memberMarketGroup;
	}

	public MemberMarket set(int index, MemberMarket memberMarket) {
		if (!(memberMarket instanceof MemberMarketGroupAdapter)) {
			return null;
		}
		final MemberMarketGroupAdapter memberMarketGroup = markets.set(index, (MemberMarketGroupAdapter) memberMarket);
		setPosition(index);
		return memberMarketGroup;
	}

	private void setPosition(int index) {
		for (int i = index+1; i < size(); i++) {
			final MemberMarketGroupAdapter market = markets.get(i);
			System.err.println("-Market: [name=" + market.getName() + ", position=" + market.getPosition() + "]");
			market.setPosition(i);
			System.err.println("+Market: [name=" + market.getName() + ", position=" + market.getPosition() + "]");
		}
	}

}
