package biz.firstlook.module.market.search;

import biz.firstlook.commons.util.Functions;

public abstract class AbstractSearchCandidateAnnotation implements SearchCandidateAnnotation {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof SearchCandidateAnnotation) == false)
			return false;
		SearchCandidateAnnotation a = (SearchCandidateAnnotation) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getName(), a.getName());
		return lEquals;
	}
	
	public int hashCode() {
		return Functions.nullSafeHashCode(getName());
	}
	
}
