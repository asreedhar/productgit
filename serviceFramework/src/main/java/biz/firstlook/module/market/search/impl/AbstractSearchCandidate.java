package biz.firstlook.module.market.search.impl;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.SearchCandidate;

/**
 * Abstract implementation of <code>SearchCandidate</code> supplying
 * <code>equals</code> and <code>hashCode</code> implementations such that
 * concrete instances need not necessarily provide their own.
 *   
 * @author swenmouth
 */
public abstract class AbstractSearchCandidate implements SearchCandidate {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof SearchCandidate) == false)
			return false;
		SearchCandidate c = (SearchCandidate) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getModel(), c.getModel());
		return lEquals;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getModel());
		return hashCode;
	}
	
}
