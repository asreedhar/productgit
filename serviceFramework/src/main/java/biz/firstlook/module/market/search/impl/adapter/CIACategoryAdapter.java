package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;

import biz.firstlook.module.market.search.AbstractSearchCandidateAnnotation;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.SearchCandidateAnnotation;
import biz.firstlook.module.market.search.impl.entity.CIACategory;

/**
 * Wrapper that presents a {@link CIACategory} to its client as a collection of {@link Market}s.
 * 
 * @author swenmouth
 */
public class CIACategoryAdapter extends AbstractSearchCandidateAnnotation implements Serializable, SearchCandidateAnnotation {

	private static final long serialVersionUID = -5020061446361043690L;
	
	private CIACategory category;
	
	public CIACategoryAdapter(CIACategory category) {
		this.category = category;
	}

	public String getName() {
		return category.getDescription();
	}

	public CIACategory getCIACategory() {
		return category;
	}
	
}
