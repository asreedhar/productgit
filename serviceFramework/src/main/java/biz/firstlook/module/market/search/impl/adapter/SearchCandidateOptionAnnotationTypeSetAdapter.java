package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.Set;

import biz.firstlook.commons.collections.SetAdapter;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;

public class SearchCandidateOptionAnnotationTypeSetAdapter extends SetAdapter<SearchCandidateCriteriaAnnotation,SearchCandidateOptionAnnotationType> implements Serializable {

	private static final long serialVersionUID = 7498988576346501454L;

	public SearchCandidateOptionAnnotationTypeSetAdapter(Set<SearchCandidateOptionAnnotationType> types) {
		super(types);
	}

	public boolean add(SearchCandidateCriteriaAnnotation element) {
		if (element.getName().equals("Optimal Plan")) {
			return set.add(SearchCandidateOptionAnnotationType.OPTIMAL);
		}
		if (element.getName().equals("Buying Plan")) {
			return set.add(SearchCandidateOptionAnnotationType.BUYING);
		}
		if (element.getName().equals("Hot List")) {
			return set.add(SearchCandidateOptionAnnotationType.HOTLIST);
		}
		if (element.getName().equals("One Time")) {
			return set.add(SearchCandidateOptionAnnotationType.ONETIME);
		}
		return false;
	}

	public SearchCandidateCriteriaAnnotation adaptItem(SearchCandidateOptionAnnotationType type) {
		return new SearchCandidateOptionAnnotationTypeAdapter(type);
	}
	
}
