package biz.firstlook.module.market.search;

import java.util.Date;

public interface SearchSummary {
	public String getTitle();
	public Integer getVehiclesMatched();
	public Integer getCandidatesMatched();
	public String getLocation();
	public String getAuctionName();
	public Date getAuctionDate();
	public Double getDistance();
	public Double getScore();
}
