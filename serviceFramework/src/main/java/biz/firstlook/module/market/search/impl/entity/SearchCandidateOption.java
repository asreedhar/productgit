package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@Table(name="SearchCandidateOption")
public class SearchCandidateOption implements Serializable {

	private static final long serialVersionUID = 5299370978153303567L;
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer searchCandidateOptionID;
	private Integer modelYear;
	private Date expires;
	private boolean suppress;

	@ManyToMany(
		targetEntity=SearchCandidateOptionAnnotationType.class,
		fetch=FetchType.EAGER
	)
	@JoinTable(
		name="SearchCandidateOptionAnnotation",
		joinColumns={@JoinColumn(name="SearchCandidateOptionID")},
		inverseJoinColumns={@JoinColumn(name="SearchCandidateOptionAnnotationTypeID")}
	)
	private Set<SearchCandidateOptionAnnotationType> searchCandidateOptionAnnotations;
	
	public SearchCandidateOption() {
		super();
	}

	public SearchCandidateOption(Integer searchCandidateOptionID, Integer modelYear, Date expires, boolean suppress, Set<SearchCandidateOptionAnnotationType> searchCandidateOptionAnnotations) {
		super();
		this.searchCandidateOptionID = searchCandidateOptionID;
		this.modelYear = modelYear;
		this.expires = expires;
		this.suppress = suppress;
		this.searchCandidateOptionAnnotations = searchCandidateOptionAnnotations;
	}
	
	public Date getExpires() {
		return expires;
	}
	public void setExpires(Date expires) {
		this.expires = expires;
	}
	
	public Integer getModelYear() {
		return modelYear;
	}
	public void setModelYear(Integer modelYear) {
		this.modelYear = modelYear;
	}
	
	public boolean getSuppress() {
		return suppress;
	}
	public void setSuppress(boolean suppress) {
		this.suppress = suppress;
	}

	public Integer getSearchCandidateOptionID() {
		return searchCandidateOptionID;
	}
	public void setSearchCandidateOptionID(Integer searchCandidateOptionID) {
		this.searchCandidateOptionID = searchCandidateOptionID;
	}
	
	public Set<SearchCandidateOptionAnnotationType> getSearchCandidateOptionAnnotations() {
		return searchCandidateOptionAnnotations;
	}
	public void setSearchCandidateOptionAnnotations(Set<SearchCandidateOptionAnnotationType> searchCandidateOptionAnnotations) {
		this.searchCandidateOptionAnnotations = searchCandidateOptionAnnotations;
	}

	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof SearchCandidateOption) {
			SearchCandidateOption c = (SearchCandidateOption) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getModelYear(), c.getModelYear());
			return lEquals;
		}
		return false;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getModelYear());
		return hashCode;
	}
}
