package biz.firstlook.module.market.search.impl;

import java.io.Serializable;

import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;

/**
 * Wrapper class for an instance of a <code>MemberMarket</code> that always returns
 * false when {@link #getSuppress} is called.
 * 
 * @author swenmouth
 */
public class UnsuppressedMemberMarket implements Comparable<MemberMarket>, Serializable, MemberMarket, MemberMarketWrapper {

	private static final long serialVersionUID = -7764830179806929340L;
	
	MemberMarket memberMarket;

	public UnsuppressedMemberMarket(MemberMarket memberMarket) {
		this.memberMarket = memberMarket;
	}

	public boolean getSuppress() {
		return false;
	}

	public boolean getExactMatches() {
		return memberMarket.getExactMatches();
	}

	public Integer getMaxDistanceFromDealer() {
		return memberMarket.getMaxDistanceFromDealer();
	}
    
	public Integer getTimePeriod() {
		return memberMarket.getTimePeriod();
	}

	public void setExactMatches(boolean exactMatches) {
		memberMarket.setExactMatches(exactMatches);
	}

	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
		memberMarket.setMaxDistanceFromDealer(maxDistanceFromDealer);
	}

	public void setSuppress(boolean suppress) {
		memberMarket.setSuppress(suppress);
	}

	public void setTimePeriod(Integer timePeriod) {
		memberMarket.setTimePeriod(timePeriod);
	}
    
	public void setMaxMileage(Integer maxMileage) {
        memberMarket.setMaxMileage(maxMileage);
    }
    
    public void setMinMileage(Integer minMileage) {
        memberMarket.setMinMileage(minMileage);
    }

	public int compareTo(MemberMarket arg0) {
		return memberMarket.compareTo(arg0);
	}

	public MarketAdministrator getMarketAdministrator() {
		return memberMarket.getMarketAdministrator();
	}

	public MarketType getMarketType() {
		return memberMarket.getMarketType();
	}

	public String getName() {
		return memberMarket.getName();
	}

	public MemberMarket getMemberMarket() {
		return memberMarket;
	}
    
    public Integer getMaxMileage() {
        return memberMarket.getMaxMileage();
    }
    
    public Integer getMinMileage() {
        return memberMarket.getMinMileage();
    }
    
	
}
