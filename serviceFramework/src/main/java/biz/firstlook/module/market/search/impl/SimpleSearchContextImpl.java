package biz.firstlook.module.market.search.impl;

import java.io.Serializable;
import java.util.List;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchContext;

/**
 * Simple bean implementation of the <code>SearchContext</code> interface.
 * 
 * @author swenmouth
 */
public class SimpleSearchContextImpl implements Serializable, SearchContext {

	private static final long serialVersionUID = -410733729530986422L;
	
	private Member member;
	private PersistenceScope persistenceScope;
	private List<MemberMarket> markets;

	private Integer liveMaxDistanceFromBusinessUnit = null;
	private Integer otherMaxDistanceFromBusinessUnit = null;
	private Integer liveTimePeriod = null;
	private Integer otherTimePeriod = null;
	private boolean liveExactMatches = false;
	private boolean otherExactMatches = false;

	public SimpleSearchContextImpl(Member member, List<MemberMarket> markets, PersistenceScope persistenceScope) {
		super();
		this.member = member;
		this.markets = markets;
		this.persistenceScope = persistenceScope;
	}

	public Member getMember() {
		return member;
	}

	public List<MemberMarket> getMarkets() {
		return markets;
	}

	public PersistenceScope getPersistenceScope() {
		return persistenceScope;
	}

	public void setPersistenceScope(PersistenceScope persistenceScope) {
		this.persistenceScope = persistenceScope;
	}

	public Integer getMaxDistanceFromBusinessUnit() {
		if (markets.size() == 1) {
			return markets.get(0).getMaxDistanceFromDealer();
		}
		min();
		return otherMaxDistanceFromBusinessUnit;
	}
    

	public Integer getTimePeriod() {
		if (markets.size() == 1) {
			return markets.get(0).getTimePeriod();
		}
		min();
		return Functions.nullSafeMin(otherTimePeriod, liveTimePeriod);
	}

	public boolean getExactMatches() {
		if (markets.size() == 1) {
			return markets.get(0).getExactMatches();
		}
		min();
		return (otherExactMatches || liveExactMatches);
	}
    
    public Integer getMaxMileage() {
        if( markets != null && !markets.isEmpty()) {
            return markets.get(0).getMaxMileage();
        }
        return 999999;
    }

    public Integer getMinMileage() {
        if( markets != null && !markets.isEmpty()) {
            return markets.get(0).getMinMileage();
        }
        return 0;
       
    }
    
	private void min() {
		for (MemberMarket m : markets) {
			if (m.getMarketType().equals(MarketType.LIVE)) {
				liveMaxDistanceFromBusinessUnit = Functions.nullSafeMin(liveMaxDistanceFromBusinessUnit, m.getMaxDistanceFromDealer());
				liveTimePeriod = Functions.nullSafeMin(liveTimePeriod, m.getTimePeriod());
				liveExactMatches |= m.getExactMatches();
			}
			else {
				otherMaxDistanceFromBusinessUnit = Functions.nullSafeMin(otherMaxDistanceFromBusinessUnit, m.getMaxDistanceFromDealer());
				otherTimePeriod = Functions.nullSafeMin(otherTimePeriod, m.getTimePeriod());
				otherExactMatches |= m.getExactMatches();
			}
		}
	}
}
