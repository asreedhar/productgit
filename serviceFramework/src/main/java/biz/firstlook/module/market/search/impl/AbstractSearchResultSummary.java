package biz.firstlook.module.market.search.impl;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.SearchSummary;

/**
 * Abstract implementation of <code>SearchSummary</code> supplying
 * <code>equals</code> and <code>hashCode</code> implementations such that
 * concrete instances need not necessarily provide their own.
 * 
 * @author swenmouth
 */
public abstract class AbstractSearchResultSummary implements SearchSummary {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof SearchSummary) {
			SearchSummary s = (SearchSummary) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getTitle(), s.getTitle()); 
			lEquals &= Functions.nullSafeEquals(getVehiclesMatched(), s.getVehiclesMatched());
			lEquals &= Functions.nullSafeEquals(getLocation(), s.getLocation());
			lEquals &= Functions.nullSafeEquals(getAuctionName(), s.getAuctionName());
			lEquals &= Functions.nullSafeEquals(getAuctionDate(), s.getAuctionDate());
			lEquals &= Functions.nullSafeEquals(getDistance(), s.getDistance());
			lEquals &= Functions.nullSafeEquals(getScore(), s.getScore());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getTitle()); 
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getVehiclesMatched());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getLocation());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getAuctionName());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getAuctionDate());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getDistance());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getScore());
		return hashCode;
	}
	
}
