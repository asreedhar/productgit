package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@org.hibernate.annotations.Entity(mutable=false)
@Table(name="tbl_MemberATCAccessGroupListType")
public class MemberMarketListType implements Serializable {
	
	private static final long serialVersionUID = 1290540116891473711L;
	
	public static transient final MemberMarketListType MARKET_LIST_PERMANENT = new MemberMarketListType(1, "Permanent Market List");
	public static transient final MemberMarketListType MARKET_LIST_CURRENT   = new MemberMarketListType(2, "Current Market List");
	public static transient final MemberMarketListType MARKET_LIST_TEMPORARY = new MemberMarketListType(3, "Temporary Market List");
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column(name="memberATCAccessGroupListTypeID")
	private Integer memberMarketListTypeID;
	
	private String description;
	
	public MemberMarketListType() {
		super();
	}
	
	public MemberMarketListType(Integer memberMarketListTypeID, String description) {
		super();
		this.memberMarketListTypeID = memberMarketListTypeID;
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getMemberMarketListTypeID() {
		return memberMarketListTypeID;
	}
	public void setMemberMarketListTypeID(Integer searchCandidateListTypeID) {
		this.memberMarketListTypeID = searchCandidateListTypeID;
	}
	
	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof MemberMarketListType) {
			MemberMarketListType t = (MemberMarketListType) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getMemberMarketListTypeID(), t.getMemberMarketListTypeID());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getMemberMarketListTypeID());
		return hashCode;
	}
}

