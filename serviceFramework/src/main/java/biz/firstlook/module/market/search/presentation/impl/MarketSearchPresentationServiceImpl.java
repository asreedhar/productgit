package biz.firstlook.module.market.search.presentation.impl;

import java.io.Serializable;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import biz.firstlook.commons.hibernate.CollectionMutationPersistor;
import biz.firstlook.commons.sql.ResultSetUtils;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.inventory.StockLevel;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateAnnotation;
import biz.firstlook.module.market.search.SearchCandidateAnnotationEnum;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.adapter.SearchCandidateAdapter;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateList;
import biz.firstlook.module.market.search.presentation.DecoratedModel;
import biz.firstlook.module.market.search.presentation.DecoratedModelGroup;
import biz.firstlook.module.market.search.presentation.DecoratedModelYear;
import biz.firstlook.module.market.search.presentation.MarketSearchPresentationService;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;

public class MarketSearchPresentationServiceImpl extends JdbcDaoSupport implements
		Serializable, MarketSearchPresentationService {

	private static final long serialVersionUID = -646226590645212990L;

	private static final transient Logger logger = Logger.getLogger(MarketSearchPresentationServiceImpl.class);
	
	private static final String SQL_FUNCTION = "{ ? = call dbo.GetSearchCandidateListPresentationData(?) }";
	
	private static final String SQL_FUNCTION_XML = "{ ? = call dbo.GetSearchCandidatePresentationDataXML(?,?) }";

	public MarketSearchPresentationServiceImpl() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<DecoratedModelGroup> getDecoratedModelGroups(SearchContext ctx,
			Collection<SearchCandidate> searchCandidates) {
		CallableStatementCreatorFactory cscf = null;
		Map<String,Object> parameterMap = new HashMap<String,Object>();
		if (searchCandidates instanceof CollectionMutationPersistor) {
			// extract list
			final CollectionMutationPersistor collectionMutationPersistor = (CollectionMutationPersistor) searchCandidates;
			collectionMutationPersistor.saveOrUpdate();
			SearchCandidateList searchCandidateList = (SearchCandidateList) (collectionMutationPersistor).getEntity();
			if (searchCandidateList.getSearchCandidateListID() == null) {
				throw new IllegalStateException("There is no ID with which to look for search candidates!");
			}
			else {
				logger.debug("SearchCandidateListID=" + searchCandidateList.getSearchCandidateListID());
			}
			// build parameter list
			List<Object> parameterList = new ArrayList<Object>();
			parameterList.add(new SqlOutParameter("@RC", java.sql.Types.INTEGER));
			parameterList.add(new SqlParameter("@SearchCandidateListID", java.sql.Types.INTEGER));
			// build parameter map
			parameterMap.put("@SearchCandidateListID", searchCandidateList.getSearchCandidateListID());
			// prepare SQL
			cscf = new CallableStatementCreatorFactory(
					SQL_FUNCTION,
					parameterList);
			
		} else {
			//used for InGroupInventorySearch and perhaps flash locate.
			//in theory, this could replace the above block.
			String xml = buildPresentationDataQueryXml(searchCandidates);
			
			// build parameter list
			List<Object> parameterList = new ArrayList<Object>();
			parameterList.add(new SqlOutParameter("@RC", java.sql.Types.INTEGER));
			parameterList.add(new SqlParameter("@BusinessUnitID", java.sql.Types.INTEGER));
			parameterList.add(new SqlParameter("@ModelIDs", java.sql.Types.LONGVARCHAR));
			
			parameterMap.put("@BusinessUnitID", ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
			parameterMap.put("@ModelIDs", xml);
			
			cscf = new CallableStatementCreatorFactory(
					SQL_FUNCTION_XML,
					parameterList);
		}
		CallableStatementCreator csc = cscf.newCallableStatementCreator(
				parameterMap);
		// run SQL
		return (List<DecoratedModelGroup>) getJdbcTemplate().execute(
				csc,
				new DecoratedModelGroupRowMapper(searchCandidates));
	}
	
	String buildPresentationDataQueryXml(final Collection<SearchCandidate> searchCandidates) {
		DocumentBuilder builder;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.newDocument();
			Element root = doc.createElement("Models");
			for(SearchCandidate sc : searchCandidates) {
				if (sc instanceof SearchCandidateAdapter) {
					SearchCandidateAdapter sca = (SearchCandidateAdapter) sc;
					if(sca.getSearchCandidate().getSuppress()) {
						continue;
					}
				} else if (sc instanceof biz.firstlook.module.market.search.impl.entity.SearchCandidate) {
					biz.firstlook.module.market.search.impl.entity.SearchCandidate scImpl =
						(biz.firstlook.module.market.search.impl.entity.SearchCandidate) sc;
					if(scImpl.getSuppress()) {
						continue;
					}
				}
				Element modelElement = doc.createElement("Model");
				modelElement.setAttribute("ID", sc.getModel().getId().toString());
				root.appendChild(modelElement);
			}
			
			StringWriter w = new StringWriter();
			TransformerFactory.newInstance().newTransformer().transform(
					new DOMSource(root),
					new StreamResult(w));
			return w.toString();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (TransformerConfigurationException e) {
			throw new RuntimeException(e);
		} catch (TransformerException e) {
			throw new RuntimeException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new RuntimeException(e);
		}
	}
	
	static final List<Integer> ZEROS;
	static {
		final Integer ZERO = new Integer(0);
		List<Integer> tmp = new ArrayList<Integer>(25);
		for (int i = 0; i < 25; i++) {
			tmp.add(ZERO);
		}
		ZEROS = Collections.unmodifiableList(tmp);
	}
	
	class DecoratedModelGroupRowMapper implements CallableStatementCallback {
		// to build model's etc
		final VehicleComponentBuilder builder = VehicleComponentBuilderFactory.getInstance().newVehicleBuilder();
		// from search candidates
		Map<ModelGroup,List<SearchCandidate>> modelGroupSearchCandidatesMap = new HashMap<ModelGroup,List<SearchCandidate>>();
		Map<Integer,SearchCandidate> modelSearchCandidatesMap = new HashMap<Integer,SearchCandidate>();
		Map<Integer,ModelGroup> modelGroupKeyMap = new HashMap<Integer,ModelGroup>();
		Map<ModelGroup,Map<ModelYear,SearchCandidateCriteria>> modelGroupModelYearAnnotations = new HashMap<ModelGroup,Map<ModelYear,SearchCandidateCriteria>>();
		// from SQL
		Map<Integer,List<DecoratedModelYear>> modelGroupModelYearsMap = new HashMap<Integer,List<DecoratedModelYear>>();
		Map<Integer,List<Integer>> modelGroupUnitsInStock = new HashMap<Integer,List<Integer>>();
		Map<Integer,List<Integer>> modelGroupOptimalUnits = new HashMap<Integer,List<Integer>>();
		// timing
		long created = System.currentTimeMillis();
		long ctor    = 0L;
		long started = 0L;
		// constructor
		public DecoratedModelGroupRowMapper(Collection<SearchCandidate> searchCandidates) {
			super();
			for (SearchCandidate searchCandidate : searchCandidates) {
				final ModelGroup modelGroup = searchCandidate.getModel().getModelGroup();
				final Integer modelId = searchCandidate.getModel().getId();
				Functions.put(modelGroupSearchCandidatesMap, modelGroup, searchCandidate);
				modelGroupKeyMap.put(modelGroup.getModelGroupID(), modelGroup);
				if (modelSearchCandidatesMap.put(modelId, searchCandidate) != null) {
					throw new IllegalStateException("Duplicate SearchCandidate: " + searchCandidate + " " + searchCandidates);
				}
				if (modelGroupModelYearAnnotations.get(modelGroup) == null) {
					Map<ModelYear,SearchCandidateCriteria> modelYearAnnotationMap = new HashMap<ModelYear,SearchCandidateCriteria>();
					for (SearchCandidateCriteria searchCandidateCriteria : searchCandidate.getCriteria()) {
						modelYearAnnotationMap.put(
								searchCandidateCriteria.getModelYear(),
								searchCandidateCriteria);
					}
					modelGroupModelYearAnnotations.put(modelGroup, modelYearAnnotationMap);
				}
			}
			ctor = System.currentTimeMillis();
		}
		public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
			started = System.currentTimeMillis();
			cs.execute();
			extractStockLevel(cs.getResultSet());
			if (cs.getMoreResults()) {
				extractOptimalLevel(cs.getResultSet());
			}
			if (cs.getMoreResults()) {
				extractModelYear(cs.getResultSet());
			}
			if (cs.getMoreResults()) {
				List<DecoratedModelGroup> modelGroups = extractModelGroup(cs.getResultSet());
				cleanUp();
				logger.debug("Constructor: " + (ctor-created) + "ms");
				logger.debug("SQL/Marshall: " + (System.currentTimeMillis()-started) + "ms");
				return modelGroups;
			}
			throw new IllegalStateException("Missing last ResultSet");
		}
		protected void extractStockLevel(ResultSet rs) throws SQLException, DataAccessException {
			while (rs.next()) {
				final Integer groupingDescriptionID = ResultSetUtils.getInt(rs, "GroupingDescriptionID", false);
				int totalUnitsInStock = 0;
				for (int modelYear = 1990; modelYear < 2014; modelYear++) {
					final Integer unitsInStock = ResultSetUtils.getInt(
							rs,
							"ModelYear" + Integer.toString(modelYear),
							false);
					Functions.put(modelGroupUnitsInStock, groupingDescriptionID, unitsInStock);
					totalUnitsInStock += unitsInStock;
				}
				modelGroupUnitsInStock.get(groupingDescriptionID).add(0, totalUnitsInStock);
			}
		}
		protected void extractOptimalLevel(ResultSet rs) throws SQLException, DataAccessException {
			while (rs.next()) {
				Integer groupingDescriptionID = ResultSetUtils.getInt(rs, "GroupingDescriptionID", false);
				SearchCandidateAnnotation ciaCategory = null;
				switch (ResultSetUtils.getInt(rs, "CIACategoryID", false)) {
					case 1:
						ciaCategory = SearchCandidateAnnotationEnum.POWERZONE;
						break;
					case 2:
						ciaCategory = SearchCandidateAnnotationEnum.WINNERS;
						break;
					case 3:
						ciaCategory = SearchCandidateAnnotationEnum.GOODBETS;
						break;
					case 4:
						ciaCategory = SearchCandidateAnnotationEnum.MARKET_PERFORMERS;
						break;
					case 5:
						ciaCategory = SearchCandidateAnnotationEnum.MANAGERS_CHOICE;
						break;
					default:
						throw new IllegalStateException("Invalid Annotation ID");
				}
				ModelGroup modelGroup = modelGroupKeyMap.get(groupingDescriptionID);
				if (modelGroup != null) {
					List<SearchCandidate> searchCandidates = modelGroupSearchCandidatesMap.get(modelGroup);
					if (searchCandidates != null) {
						for (SearchCandidate searchCandidate : searchCandidates) {
							searchCandidate.getAnnotations().add(ciaCategory);
						}
					}
				}
				Functions.put(modelGroupOptimalUnits, groupingDescriptionID, ResultSetUtils.getInt(rs, "AllModelYears", false));
				for (int modelYear = 1990; modelYear < 2014; modelYear++) {
					final Integer optimalUnits = ResultSetUtils.getInt(
							rs,
							"ModelYear" + Integer.toString(modelYear),
							false);
					Functions.put(modelGroupOptimalUnits, groupingDescriptionID, optimalUnits);
				}
			}
		}
		protected void extractModelYear(ResultSet rs) throws SQLException, DataAccessException {
			while (rs.next()) {
				// get model group
				final Integer groupingDescriptionID = ResultSetUtils.getInt(rs, "GroupingDescriptionID", false);
				// get optimal units
				List<Integer> optimalUnitsList = modelGroupOptimalUnits.get(groupingDescriptionID);
				if (optimalUnitsList == null) {
					optimalUnitsList = ZEROS;
				}
				final Iterator<Integer> optimalUnits = optimalUnitsList.iterator();
				// get stock levels
				List<Integer> unitsInStockList = modelGroupUnitsInStock.get(groupingDescriptionID);
				if (unitsInStockList == null) {
					unitsInStockList = ZEROS;
				}
				final Iterator<Integer> unitsInStock = unitsInStockList.iterator();
				// add "all years" first
				final ModelGroup theModelGroup = modelGroupKeyMap.get(groupingDescriptionID);
				if (theModelGroup == null) {
					logger.error("ResultSet GroupingDescriptionID: " + groupingDescriptionID + " was not a search candidate " + modelGroupKeyMap);
					continue;
				}
				addModelYear(theModelGroup, null, unitsInStock.next(), optimalUnits.next());
				// do the other years
				for (int modelYear = 1990; modelYear < 2014; modelYear++) {
					final Integer modelYearUnitsInStock = unitsInStock.next();
					final Integer modelYearOptimalUnits = optimalUnits.next();
					final boolean available = ResultSetUtils.getBoolean(
							rs,
							"ModelYear" + Integer.toString(modelYear),
							false);
					if (available) {
						addModelYear(theModelGroup, modelYear, modelYearUnitsInStock, modelYearOptimalUnits);
					}
				}
			}
		}
		protected void addModelYear(ModelGroup theModelGroup, Integer modelYear, Integer modelYearUnitsInStock, Integer modelYearOptimalUnits) {
			ModelYear theModelYear = builder.buildModelYear(modelYear);
			StockLevel theStockLevel = new SimpleStockLevelImpl(null, null, null, modelYearOptimalUnits, modelYearUnitsInStock);
			Map<ModelYear, SearchCandidateCriteria> modelYearAnnotations = modelGroupModelYearAnnotations.get(theModelGroup);
			Date expires = null;
			Set<SearchCandidateCriteriaAnnotation> annotations = new HashSet<SearchCandidateCriteriaAnnotation>();
			if (modelYearAnnotations != null) {
				SearchCandidateCriteria criteria = modelYearAnnotations.get(theModelYear);
				if (criteria != null) {
					expires = criteria.getExpires();
					annotations = criteria.getAnnotations();
				}
			}
			if (modelYearOptimalUnits != null && modelYearOptimalUnits > 0) {
				annotations.add(SearchCandidateCriteriaAnnotationEnum.OPTIMAL);
			}
			DecoratedModelYear decoratedModelYear = new DecoratedModelYearImpl(
					theModelYear,
					theStockLevel,
					expires,
					annotations);
			Functions.put(modelGroupModelYearsMap, theModelGroup.getModelGroupID(), decoratedModelYear);
		}
		protected List<DecoratedModelGroup> extractModelGroup(ResultSet rs) throws SQLException, DataAccessException {
			final Map<ModelGroup,List<DecoratedModel>> modelGroupModelsMap = new LinkedHashMap<ModelGroup,List<DecoratedModel>>();
			final Map<ModelGroup,Double> modelGroupContributionMap = new HashMap<ModelGroup,Double>();
			while (rs.next()) {
				// primitives from ResultSet
				final Double contribution = ResultSetUtils.getDouble(rs, "Contribution", false);
				// build composites
				final VehicleService service = VehicleServiceFactory.getFactory().getService();
				final Integer modelId = ResultSetUtils.getInt(rs, "ModelId", false);
				final Model model = service.getModel(modelId);
				final DecoratedModel decoratedModel = new DecoratedModelImpl(model, modelSearchCandidatesMap.get(modelId));
				Functions.put(modelGroupModelsMap, model.getModelGroup(), decoratedModel);
				// store contribution
				if (!modelGroupContributionMap.containsKey(model.getModelGroup())) {
					modelGroupContributionMap.put(model.getModelGroup(), contribution);
				}
			}
			List<DecoratedModelGroup> modelGroups = new ArrayList<DecoratedModelGroup>();
			for (ModelGroup modelGroup : modelGroupModelsMap.keySet()) {
				final Double contribution = modelGroupContributionMap.get(modelGroup);
				final List<DecoratedModel> models = modelGroupModelsMap.get(modelGroup);
				final List<DecoratedModelYear> modelYears = modelGroupModelYearsMap.get(modelGroup.getModelGroupID());
				final List<SearchCandidate> searchCandidates = modelGroupSearchCandidatesMap.get(modelGroup);
				modelGroups.add(new DecoratedModelGroupImpl(contribution, modelGroup, searchCandidates, modelYears, models));
			}
			return modelGroups;
		}
		protected void cleanUp() {
			modelGroupSearchCandidatesMap.clear();
			modelSearchCandidatesMap.clear();
			modelGroupKeyMap.clear();
			modelGroupModelYearAnnotations.clear();
			modelGroupModelYearsMap.clear();
			modelGroupUnitsInStock.clear();
			modelGroupOptimalUnits.clear();
		}
	}
}
