package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.impl.AbstractSearchCandidateCriteria;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOption;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleModelYearImpl;

/**
 * Implementation of <code>SearchCandidateCriteria</code> as an adapted hibernate
 * <code>SearchCandidateOption</code>.
 * 
 * @author swenmouth
 */
public class SearchCandidateOptionAdapter extends AbstractSearchCandidateCriteria implements Serializable, biz.firstlook.module.market.search.SearchCandidateCriteria {

	private static final long serialVersionUID = -2797841128668515931L;
	
	private SearchCandidateOption searchCandidateOption;
	
	public SearchCandidateOptionAdapter(SearchCandidateOption searchCandidateCriteria) {
		this.searchCandidateOption = searchCandidateCriteria;
	}
	
	public Date getExpires() {
		return searchCandidateOption.getExpires();
	}

	public ModelYear getModelYear() {
		return new SimpleModelYearImpl(searchCandidateOption.getModelYear());
	}

	public Set<SearchCandidateCriteriaAnnotation> getAnnotations() {
		return new SearchCandidateOptionAnnotationTypeSetAdapter(searchCandidateOption.getSearchCandidateOptionAnnotations());
	}

	public SearchCandidateOption getSearchCandidateOption() {
		return searchCandidateOption;
	}

}
