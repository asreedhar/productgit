package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@Table(name="tbl_DealerATCAccessGroups")
public class BusinessUnitMarket implements Serializable {
	
	private static final long serialVersionUID = 2477932716443163394L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column(name="DealerATCAccessGroupsID")
	private Integer businessUnitMarketID;
	
	@ManyToOne
	@JoinColumn(name="accessGroupID",nullable=false)
	private Market market;

	@Column(name="BusinessUnitID")
	private Integer businessUnitID;

	public BusinessUnitMarket() {
		super();
	}

	public BusinessUnitMarket(Integer businessUnitMarketID, Integer businessUnitID, Market market) {
		super();
		this.businessUnitMarketID = businessUnitMarketID;
		this.businessUnitID = businessUnitID;
		this.market = market;
	}

	public Integer getBusinessUnitID() {
		return businessUnitID;
	}
	public void setBusinessUnitID(Integer businessUnitID) {
		this.businessUnitID = businessUnitID;
	}

	public Market getMarket() {
		return market;
	}
	public void setMarket(Market market) {
		this.market = market;
	}

	public Integer getBusinessUnitMarketID() {
		return businessUnitMarketID;
	}
	public void setBusinessUnitMarketID(Integer businessUnitMarketID) {
		this.businessUnitMarketID = businessUnitMarketID;
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof BusinessUnitMarket) {
			BusinessUnitMarket other = (BusinessUnitMarket) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getBusinessUnitID(), other.getBusinessUnitID());
			lEquals &= Functions.nullSafeEquals(getMarket(), other.getMarket());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getBusinessUnitID());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getMarket());
		return hashCode;
	}
	
}
