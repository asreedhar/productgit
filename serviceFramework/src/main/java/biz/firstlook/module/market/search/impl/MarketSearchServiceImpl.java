package biz.firstlook.module.market.search.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.hibernate.FlushMode;
import org.hibernate.Transaction;

import biz.firstlook.commons.hibernate.CollectionMutationPersistor;
import biz.firstlook.commons.hibernate.ListMutationPersistor;
import biz.firstlook.commons.hibernate.Persistor;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.core.BusinessUnitPreferences;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.market.search.MarketAccessGroupTypeEnum;
import biz.firstlook.module.market.search.MarketSearchService;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.adapter.MarketToMemberMarketAdapter;
import biz.firstlook.module.market.search.impl.adapter.MemberMarketListAdapter;
import biz.firstlook.module.market.search.impl.adapter.SearchCandidateListCollectionAdapter;
import biz.firstlook.module.market.search.impl.entity.AccessGroupsForSearch;
import biz.firstlook.module.market.search.impl.entity.BusinessUnitMarket;
import biz.firstlook.module.market.search.impl.entity.Market;
import biz.firstlook.module.market.search.impl.entity.MemberMarketList;
import biz.firstlook.module.market.search.impl.entity.MemberMarketListType;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateForSearch;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateList;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateListType;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOption;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;
import biz.firstlook.module.market.search.impl.entity.dao.AccessGroupsForSearchDAO;
import biz.firstlook.module.market.search.impl.entity.dao.BusinessUnitMarketDAO;
import biz.firstlook.module.market.search.impl.entity.dao.DefaultSearchCandidateListDAO;
import biz.firstlook.module.market.search.impl.entity.dao.MarketDAO;
import biz.firstlook.module.market.search.impl.entity.dao.MemberMarketListDAO;
import biz.firstlook.module.market.search.impl.entity.dao.SearchCandidateForSearchDAO;
import biz.firstlook.module.market.search.impl.entity.dao.SearchCandidateListDAO;
import biz.firstlook.module.market.search.impl.entity.dao.SearchResultDAO;

/**
 * Default implementation of the <code>MarketSearchService</code> interface.
 * 
 * @author swenmouth
 */
public class MarketSearchServiceImpl implements MarketSearchService {

	private static final int IN_GROUP = 141;
	private static final int ADESA    = 142;
	
	private SearchCandidateForSearchDAO searchCandidateForSearchDAO;
	private AccessGroupsForSearchDAO accessGroupsForSearchDAO;
	private SearchCandidateListDAO searchCandidateListDAO;
	private MemberMarketListDAO memberMarketListDAO;
	private BusinessUnitMarketDAO businessUnitMarketDAO;
	private MarketDAO marketDAO;
	private DefaultSearchCandidateListDAO defaultSearchCandidateListDAO;
	private SearchResultDAO searchResultDAO;

	private Persistor<
		biz.firstlook.module.market.search.impl.entity.SearchCandidateList,
		java.lang.Integer> searchCandidateListPersistor;
	
	private Persistor<
		biz.firstlook.module.market.search.impl.entity.MemberMarketList,
		java.lang.Integer> marketListPersistor;
	
	public Collection<SearchCandidate> getCandidateList(Member m, PersistenceScope s) {
		if (s == PersistenceScope.TEMPORARY) {
			// there shouldn't be any temporary lists in the DB! 
			// Sept 29, 2008 -bf.
			return new ArrayList<SearchCandidate>();
		}
		// get users list
		SearchCandidateList memberSearchCandidateList = getSearchCandidateListDAO().findByMemberId(
				m.getMemberID(),
				m.getCurrentBusinessUnit().getBusinessUnitID(),
				(s == PersistenceScope.PERMANENT ? SearchCandidateListType.HOTLIST_PERMANENT 
						: (s == PersistenceScope.TODAY ? SearchCandidateListType.HOTLIST_CURRENT 
								: SearchCandidateListType.HOTLIST_TEMPORARY)));
		
		// or merge with the default business unit list
		List<biz.firstlook.module.market.search.impl.entity.SearchCandidate> businessSearchCandidateList = getDefaultSearchCandidateListDAO().getDefaultBuyingPlan(
				m.getCurrentBusinessUnit().getBusinessUnitID());

		merge(businessSearchCandidateList, memberSearchCandidateList);

		// register the new candidates for a save
		getSearchCandidateListDAO().saveOrUpdate(memberSearchCandidateList);
		// wrap it up in a change managed collection and return it
		return new CollectionMutationPersistor<
				biz.firstlook.module.market.search.SearchCandidate,
				biz.firstlook.module.market.search.impl.entity.SearchCandidateList,
				java.lang.Integer>(
			getSearchCandidateListDAO(),
			new SearchCandidateListCollectionAdapter(memberSearchCandidateList),
			memberSearchCandidateList,
			getSearchCandidateListPersistor());
	}

	private void merge(List<biz.firstlook.module.market.search.impl.entity.SearchCandidate> src, SearchCandidateList dst) {
		// get midnight
		Date midnight = Functions.midnight(Calendar.getInstance().getTime());
		// remove all non CIA items and old one-time searches
		for (Iterator<biz.firstlook.module.market.search.impl.entity.SearchCandidate> it = dst.getSearchCandidates().iterator(); it.hasNext();) {
			biz.firstlook.module.market.search.impl.entity.SearchCandidate c = it.next();

			for( Iterator< SearchCandidateOption > opIter = c.getSearchCandidateOptions().iterator(); opIter.hasNext();) 
			{
				SearchCandidateOption o = opIter.next();
				o.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.OPTIMAL);
				o.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.BUYING);
				if (o.getExpires() != null) {
					if (o.getExpires().getTime() < midnight.getTime()) {
						o.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.HOTLIST);
						o.getSearchCandidateOptionAnnotations().remove(SearchCandidateOptionAnnotationType.ONETIME);
					}
				}
				
				// if an option has no annotations - remove it
				if (o.getSearchCandidateOptionAnnotations().isEmpty()) {
					opIter.remove();
				}
			}
			
			// if a candidate has no options - remove it
			if (c.getSearchCandidateOptions().isEmpty()) {
				it.remove();
			}
		}
		// commit the deletions to the db
		// so: we do not violate the table unique key if they are re-added in the next loop
		// as: hibernate runs inserts, updates then deletes
		// so: we'd be re-inserting the db rows we wanted deleted before they are deleted
		// so: FIXME!! Need a better merge algorithm
		commitChanges(dst);
		// add all the src candidates
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidate sc0 : src) {
			biz.firstlook.module.market.search.impl.entity.SearchCandidate sc1 = findOrCreateSearchCandidate(dst, sc0);
			for (SearchCandidateOption sco0 : sc0.getSearchCandidateOptions()) {
				SearchCandidateOption sco1 = findOrCreateSearchCandidateOption(sc1, sco0);
				for (SearchCandidateOptionAnnotationType scoa0 : sco0.getSearchCandidateOptionAnnotations()) {
					findOrCreateSearchCandidateOptionAnnotation(sco1, scoa0);
				}
				boolean isHotList = sco1.getSearchCandidateOptionAnnotations().contains(SearchCandidateOptionAnnotationType.HOTLIST);
				boolean isOneTime = sco1.getSearchCandidateOptionAnnotations().contains(SearchCandidateOptionAnnotationType.ONETIME);
				if ((isHotList || isOneTime) == false) {
					sco1.getSearchCandidateOptionAnnotations().add(SearchCandidateOptionAnnotationType.ONETIME);
				}
			}
		}
	}
	
	private void commitChanges(SearchCandidateList list) {
		Transaction tx = null;
		try {
			tx = getSearchCandidateListDAO().getSessionFactory().getCurrentSession().getTransaction();
			if (tx != null) {
				getSearchCandidateListDAO().saveOrUpdate(list);
				tx.commit();
				tx.begin();
			}
		}
		catch (Throwable t) {
			// ignore the error
		}
	}
	
	private SearchCandidateOptionAnnotationType findOrCreateSearchCandidateOptionAnnotation(SearchCandidateOption o, SearchCandidateOptionAnnotationType tgt) {
		for (SearchCandidateOptionAnnotationType src : o.getSearchCandidateOptionAnnotations()) {
			if (src.getSearchCandidateOptionAnnotationTypeID().equals(tgt.getSearchCandidateOptionAnnotationTypeID())) {
				return src;
			}
		}
		o.getSearchCandidateOptionAnnotations().add(tgt);
		return tgt;
	}

	private SearchCandidateOption findOrCreateSearchCandidateOption(biz.firstlook.module.market.search.impl.entity.SearchCandidate c, SearchCandidateOption tgt) {
		final Integer tgtModelYear = tgt.getModelYear();
		for (SearchCandidateOption src : c.getSearchCandidateOptions()) {
			final Integer srcModelYear = src.getModelYear();
			if (tgtModelYear == null && srcModelYear == null) {
				return src;
			}
			if (tgtModelYear == null && srcModelYear != null) {
				continue;
			}
			if (tgtModelYear != null && srcModelYear == null) {
				continue;
			}
			if (tgtModelYear != null && srcModelYear != null) {
				if (!tgtModelYear.equals(srcModelYear)) {
					continue;
				}
				return src;
			}
		}
		c.getSearchCandidateOptions().add(tgt);
		return tgt;
	}

	private biz.firstlook.module.market.search.impl.entity.SearchCandidate findOrCreateSearchCandidate(SearchCandidateList list, biz.firstlook.module.market.search.impl.entity.SearchCandidate tgt) {
		for (biz.firstlook.module.market.search.impl.entity.SearchCandidate src : list.getSearchCandidates()) {
			if (Functions.nullSafeEquals(tgt.getModelID(), src.getModelID())) {
				return src;
			}
		}
		if (list.getSearchCandidates().contains(tgt)) {
			throw new IllegalStateException("what the fuxk");
		}
		else {
			list.getSearchCandidates().add(tgt);
			return tgt;
		}
	}
	
	public SearchContext getContext(Member m, PersistenceScope s) {
		MemberMarketList memberMarketList = getMemberMarketListDAO().findByMemberId(
				m.getMemberID(),
				m.getCurrentBusinessUnit().getBusinessUnitID(),
				(s == PersistenceScope.PERMANENT ? 1 : (s == PersistenceScope.TODAY ? 2 : 3)));
		if (s == PersistenceScope.TEMPORARY) {
			// take todays list, fall back to the permanent list or create a new
			// list when no other exists; sorry for the bug - simon 
			final int TODAY = 2, PERMANENT = 1;
			int[] persistenceScopes = new int[] { TODAY, PERMANENT }; 
			for (int persistenceScope : persistenceScopes) {
				MemberMarketList memberMarketListScope = getMemberMarketListDAO().findByMemberId(
					m.getMemberID(),
					m.getCurrentBusinessUnit().getBusinessUnitID(),
						persistenceScope);
				if (!memberMarketListScope.getMemberMarkets().isEmpty()) {
					copyMemberMarketList(memberMarketListScope, memberMarketList);
					if (persistenceScope == PERMANENT)
						checkMemberMarketList(m, memberMarketList);
					break;
				}
			}
			if (memberMarketList.getMemberMarkets().isEmpty()) {
				memberMarketList = createMemberMarketList(m, memberMarketList);
            }
		}
		else if (memberMarketList.getMemberMarkets().isEmpty()) {
			memberMarketList = createMemberMarketList(m, memberMarketList);
		}
		else {
			checkMemberMarketList(m, memberMarketList);
		}
		getMemberMarketListDAO().saveOrUpdate(memberMarketList);
		List<MemberMarket> markets = new ListMutationPersistor<
				MemberMarket,
				biz.firstlook.module.market.search.impl.entity.MemberMarketList,
				java.lang.Integer>(
			getMemberMarketListDAO(),
			new MemberMarketListAdapter(getMemberMarketListDAO(), memberMarketList, getMarketListPersistor()),
			memberMarketList,
			getMarketListPersistor());
		return new SimpleSearchContextImpl(m, markets, s);
	}

	private void copyMemberMarketList(MemberMarketList src, MemberMarketList dst) {
		// map rows from market id to dst member market id
		Map<Integer,biz.firstlook.module.market.search.impl.entity.MemberMarket> dstMemberMarketMap = new HashMap<Integer,biz.firstlook.module.market.search.impl.entity.MemberMarket>();
		for (Iterator<biz.firstlook.module.market.search.impl.entity.MemberMarket> it = dst.getMemberMarkets().iterator(); it.hasNext();) {
			biz.firstlook.module.market.search.impl.entity.MemberMarket memberMarket = it.next();
			dstMemberMarketMap.put(memberMarket.getMarket().getMarketID(), memberMarket);
			it.remove();
		}
		// loop over src member markets using map where possible otherwise creating new entities 
		for (biz.firstlook.module.market.search.impl.entity.MemberMarket srcMemberMarket : src.getMemberMarkets()) {
			biz.firstlook.module.market.search.impl.entity.MemberMarket dstMemberMarket = dstMemberMarketMap.get(srcMemberMarket.getMarket().getMarketID());
			if (dstMemberMarket == null) {
				dstMemberMarket = new biz.firstlook.module.market.search.impl.entity.MemberMarket(
						null,
						srcMemberMarket.getMarket(),
						srcMemberMarket.getPosition(),
						srcMemberMarket.getMaxDistanceFromDealer(),
						srcMemberMarket.getSuppress(),
                        srcMemberMarket.getMaxMileage(),
                        srcMemberMarket.getMinMileage());
			}
			else {
				dstMemberMarket.setPosition(srcMemberMarket.getPosition());
				dstMemberMarket.setMaxDistanceFromDealer(srcMemberMarket.getMaxDistanceFromDealer());
				dstMemberMarket.setSuppress(srcMemberMarket.getSuppress());
			}
			dst.getMemberMarkets().add(dstMemberMarket);
		}
		dstMemberMarketMap.clear();
	}

	private void checkMemberMarketList(Member m, MemberMarketList memberMarketList) {
		final List<Market> availableMarkets = getOpenMarkets(m, memberMarketList);
		// add local markets
		final List<BusinessUnitMarket> businessUnitMarkets = getBusinessUnitMarketDAO().findByBusinessUnitId(
				m.getCurrentBusinessUnit().getBusinessUnitID());
		B: for (BusinessUnitMarket businessUnitMarket : businessUnitMarkets) {
			for (Market availableMarket : availableMarkets) {
				if (availableMarket.equals(businessUnitMarket.getMarket())) {
					continue B;
				}
			}
			availableMarkets.add(businessUnitMarket.getMarket());
		}
		// remove markets no longer associated with business unit (excluding in-group and live auctions)
		final Stack<Integer> positions = new Stack<Integer>();
		M: for (Iterator<biz.firstlook.module.market.search.impl.entity.MemberMarket> it = memberMarketList.getMemberMarkets().iterator(); it.hasNext();) {
			final biz.firstlook.module.market.search.impl.entity.MemberMarket memberMarket = it.next();
			final Integer marketID = memberMarket.getMarket().getMarketID();
			positions.push(memberMarket.getPosition());
			if (marketID.equals(IN_GROUP) || marketID.equals(ADESA)) {
				continue M;
			}
			for (Market availableMarket : availableMarkets) {
				if (availableMarket.equals(memberMarket.getMarket())) {
					continue M;
				}
			}
			it.remove();
			positions.pop();
		}
		final Integer position = Collections.max(positions)+1;
		int distanceFromDealer = m.getCurrentBusinessUnit().getBusinessUnitPreferences().getOtherDistanceFromDealer();
        int liveAuctionDistanceFromDealer = m.getCurrentBusinessUnit().getBusinessUnitPreferences().getLiveAuctionDistanceFromDealer();
		A: for (Market availableMarket : availableMarkets) {
			for (biz.firstlook.module.market.search.impl.entity.MemberMarket memberMarket : memberMarketList.getMemberMarkets()) {
				if (memberMarket.getMarket().equals(availableMarket)) {
					continue A;
				}
			}
            if( availableMarket.getMarketType() == MarketType.LIVE )
				memberMarketList.getMemberMarkets().add(new biz.firstlook.module.market.search.impl.entity.MemberMarket(null, availableMarket, position, liveAuctionDistanceFromDealer, false,  null, null));
			else
				memberMarketList.getMemberMarkets().add(new biz.firstlook.module.market.search.impl.entity.MemberMarket(null, availableMarket, position, distanceFromDealer, false, null, null));
		}
	}

	private MemberMarketList createMemberMarketList(Member m, MemberMarketList memberMarketList) {
		//Only need to create Markets for PersistentScope.TODAY if there's no permanent.  Otherwise, use the ones from PersistentScope.PERMANENT!
		MemberMarketList memberMarketListPermanent = getMemberMarketListDAO().findByMemberId(
				m.getMemberID(),
				m.getCurrentBusinessUnit().getBusinessUnitID(),
				MemberMarketListType.MARKET_LIST_PERMANENT.getMemberMarketListTypeID());
		
		if( memberMarketListPermanent != null && !memberMarketListPermanent.getMemberMarkets().isEmpty()) {
//		if( memberMarketListPermanent != null && memberMarketListPermanent.getMemberMarketListID() != null ) {
			copyMemberMarketList(memberMarketListPermanent, memberMarketList);
			return memberMarketList;
		}
		
		// get open markets
		List<Market> availableMarkets = getOpenMarkets(m, memberMarketList);
		// add local markets
		List<BusinessUnitMarket> businessUnitMarkets = getBusinessUnitMarketDAO().findByBusinessUnitId(
				m.getCurrentBusinessUnit().getBusinessUnitID());
		B: for (BusinessUnitMarket businessUnitMarket : businessUnitMarkets) {
			for (Market availableMarket : availableMarkets) {
				if (availableMarket.equals(businessUnitMarket.getMarket())) {
					continue B;
				}
			}
			availableMarkets.add(businessUnitMarket.getMarket());
		}
		// default sort order 
		Collections.sort(availableMarkets, new MarketDefaultComparator());
		// keep track of the position
		int position = 0;
		// add markets to member
		int maxDistanceFromDealer = m.getCurrentBusinessUnit().getBusinessUnitPreferences().getOtherDistanceFromDealer();
		int liveAuctionDistanceFromDealer = m.getCurrentBusinessUnit().getBusinessUnitPreferences().getLiveAuctionDistanceFromDealer();
        //int maxMileage = m.getCurrentBusinessUnit().getBusinessUnitPreferences().getMaxMileage();
        //int minMileage = m.getCurrentBusinessUnit().getBusinessUnitPreferences().getMinMileage();
        for (Market availableMarket : availableMarkets) {
			if( availableMarket.getMarketType() == MarketType.LIVE )
				memberMarketList.getMemberMarkets().add(new biz.firstlook.module.market.search.impl.entity.MemberMarket(null, availableMarket, position++, liveAuctionDistanceFromDealer, false, null, null));
			else
				memberMarketList.getMemberMarkets().add(new biz.firstlook.module.market.search.impl.entity.MemberMarket(null, availableMarket, position++, maxDistanceFromDealer, false, null, null));
		}
		// add "In Group" market
		memberMarketList.getMemberMarkets().add(new biz.firstlook.module.market.search.impl.entity.MemberMarket(null, getMarketDAO().findById(IN_GROUP), position++, maxDistanceFromDealer, false, null, null));
		// add "ADESA" markets
		memberMarketList.getMemberMarkets().add(new biz.firstlook.module.market.search.impl.entity.MemberMarket(null, getMarketDAO().findById(ADESA), position++, liveAuctionDistanceFromDealer, false, null, null));
		// return
		return memberMarketList;
	}
	
	class MarketDefaultComparator implements Comparator<Market> {
        public int compare( Market market0, Market market1 )
		{
			int market0AccessId = market0.getAccessGroupTypeId();
			int market1AccessId = market1.getAccessGroupTypeId();
            if (market0AccessId < 6 && market1AccessId > 5) {
                return -1;
            }
            if (market0AccessId > 5 && market1AccessId < 6) {
                return 1;
            }
            if (market0AccessId < 4 && market1AccessId < 4) {
                return (Functions.nullSafeCompareTo(market0AccessId,market1AccessId));
            }
            return (Functions.nullSafeCompareTo(market0AccessId, market1AccessId) * -1);
		}
        
	}
	
	private List<Market> getOpenMarkets(Member m, MemberMarketList memberMarketList) {
		BusinessUnitPreferences preferences = m.getCurrentBusinessUnit().getBusinessUnitPreferences();
		List<Market> openMarkets = new ArrayList<Market>();
		if (preferences.isATCEnabled()) {
			openMarkets.addAll(getMarketDAO().findPublicAuctions(MarketAccessGroupTypeEnum.ATC_OPEN.getAccessGroupId()));
		}
		if (preferences.isGMACEnabled()) {
			openMarkets.addAll(getMarketDAO().findPublicAuctions(MarketAccessGroupTypeEnum.GMAC.getAccessGroupId()));
		}
		if (preferences.isTFSEnabled()) {
			openMarkets.addAll(getMarketDAO().findPublicAuctions(MarketAccessGroupTypeEnum.TFS.getAccessGroupId()));
		}
		if (preferences.isOVEEnabled()) {
			openMarkets.addAll(getMarketDAO().findPublicAuctions(MarketAccessGroupTypeEnum.OVE.getAccessGroupId()));
		}
		//openMarkets.addAll(getMarketDAO().findPublicAuctions(MarketAccessGroupTypeEnum.DRIVE_AWAY.getAccessGroupId()));
		return openMarkets;
	}
	
	public List<MarketSummary> summary(SearchContext ctx, Collection<SearchCandidate> candidates,Boolean isFirstTimeSearch) {
		List<MemberMarket> liveAuctionMarkets = new ArrayList<MemberMarket>();
		List<MemberMarket> otherMarkets = new ArrayList<MemberMarket>();
		for (MemberMarket m : ctx.getMarkets()) {
			if (m.getMarketType() == MarketType.LIVE || !m.getSuppress()) {
				if (m.getMarketType() == MarketType.LIVE) {
					liveAuctionMarkets.add(m);
				}
				else {
					otherMarkets.add(m);
				}
			}
		}
		PersistenceScope s= ctx.getPersistenceScope();
		SearchCandidateListType scl=(s == PersistenceScope.PERMANENT ? SearchCandidateListType.HOTLIST_PERMANENT 
				: (s == PersistenceScope.TODAY ? SearchCandidateListType.HOTLIST_CURRENT 
						: SearchCandidateListType.HOTLIST_TEMPORARY));
		List<MarketSummary> summary = new ArrayList<MarketSummary>();
		if (!liveAuctionMarkets.isEmpty()) {
			summary.addAll(
					getSearchResultDAO().getSearchSummary(2, ctx.getExactMatches(), ctx.getTimePeriod(),ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID(),ctx.getMember().getMemberID(),null,scl.getSearchCandidateListTypeID(),liveAuctionMarkets.get(0).getMaxDistanceFromDealer()));
		}
		if (!otherMarkets.isEmpty()) {
//			summary.addAll(
//					getSearchResultDAO().getSearchSummary(
//							new SearchQueryXMLBuilder().build(
//									new SimpleSearchContextImpl(
//											ctx.getMember(),
//											otherMarkets,
//											ctx.getPersistenceScope()),
//									candidates), 1, ctx.getExactMatches(), ctx.getTimePeriod()));
		
			if(!isFirstTimeSearch){
				long time=System.currentTimeMillis();
				getSearchCandidateForSearchDAO().getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
				List<SearchCandidateForSearch> scList= new ArrayList<SearchCandidateForSearch>();
				for (SearchCandidate s1 : candidates) {
						SearchCandidateForSearch sc;
						
						if(s1.getCriteria()==null||s1.getCriteria().isEmpty()){
							sc= new SearchCandidateForSearch();
							sc.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
							sc.setMemberId(ctx.getMember().getMemberID());
							sc.setModelId(s1.getModel().getId());
							sc.setType("MakeModelGrouping");
							scList.add(sc);
	//						getSearchCandidateForSearchDAO().saveOrUpdate(sc);
						}
						Iterator<SearchCandidateCriteria> itr = s1.getCriteria().iterator();
						while ( itr.hasNext()) {
							SearchCandidateCriteria c= itr.next();
							if (c.getModelYear().getModelYear() != null) {
								sc= new SearchCandidateForSearch();
								sc.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
								sc.setMemberId(ctx.getMember().getMemberID());
								sc.setModelId(s1.getModel().getId());
								sc.setType("Year");
								sc.setValue(c.getModelYear().getModelYear().toString());
								scList.add(sc);
	//							getSearchCandidateForSearchDAO().saveOrUpdate(sc);
	
							}
							else {
								sc= new SearchCandidateForSearch();
								sc.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
								sc.setMemberId(ctx.getMember().getMemberID());
								sc.setModelId(s1.getModel().getId());
								sc.setType("MakeModelGrouping");
								scList.add(sc);
	//							getSearchCandidateForSearchDAO().saveOrUpdate(sc);
								
							}
					}
					
				}
				getSearchCandidateForSearchDAO().saveOrUpdateAll(scList);
				
				long time2=System.currentTimeMillis()-time;
				System.out.println(time2+"-----");
			}
			
			summary.addAll(getSearchResultDAO().getSearchSummaryPurchasing(1, ctx.getExactMatches(), ctx.getTimePeriod(),ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID(),ctx.getMember().getMemberID(),isFirstTimeSearch,scl.getSearchCandidateListTypeID(),otherMarkets.get(0).getMaxDistanceFromDealer()));

		}
		
		return summary;
	}
	
	public List<MarketVehicle> search(SearchContext ctx, Collection<SearchCandidate> candidates, Boolean isFirstTimeSearch) {
		List<MemberMarket> liveAuctionMarkets = new ArrayList<MemberMarket>();
		List<MemberMarket> otherMarkets = new ArrayList<MemberMarket>();
		for (MemberMarket m : ctx.getMarkets()) {
			if (m.getSuppress() == false) {
				if (m.getMarketType() == MarketType.LIVE) {
					liveAuctionMarkets.add(m);
				}
				else {
					otherMarkets.add(m);
				}
			}
		}
		PersistenceScope s= ctx.getPersistenceScope();
		SearchCandidateListType scl=(s == PersistenceScope.PERMANENT ? SearchCandidateListType.HOTLIST_PERMANENT 
				: (s == PersistenceScope.TODAY ? SearchCandidateListType.HOTLIST_CURRENT 
						: SearchCandidateListType.HOTLIST_TEMPORARY));
		
//		MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		List<MarketVehicle> results = new ArrayList<MarketVehicle>();
		if (!liveAuctionMarkets.isEmpty()) {
			//results.addAll(getSearchResultDAO().getSearchResults(new SearchQueryXMLBuilder().build(builder.newContext(ctx, liveAuctionMarkets),candidates), 2, ctx.getExactMatches(), ctx.getTimePeriod()));
			
			for (biz.firstlook.module.market.search.Market m : liveAuctionMarkets) {
				while (m instanceof MemberMarketWrapper) {
					m = ((MemberMarketWrapper) m).getMemberMarket();
				}
				if (m instanceof MarketToMemberMarketAdapter) {
					m = ((MarketToMemberMarketAdapter) m).getMarket();
					if (m instanceof MarketSummary) {
						MarketSummary ms = (MarketSummary) m;
						if (ms instanceof SimpleMarketSummaryImpl) {
							final SimpleMarketSummaryImpl sms = (SimpleMarketSummaryImpl)ms;
							
							results.addAll(getSearchResultDAO().getSearchResults(2, ctx.getExactMatches(), ctx.getTimePeriod(),ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID(),ctx.getMember().getMemberID(),sms.getSaleID(),scl.getSearchCandidateListTypeID(),ctx.getMaxDistanceFromBusinessUnit()));
							
						}
					}
				}
			}
			
			
			

		}
		if (!otherMarkets.isEmpty()) {
//			results.addAll(getSearchResultDAO().getSearchResults(new SearchQueryXMLBuilder().build(builder.newContext(ctx, otherMarkets),candidates), 1, ctx.getExactMatches(), ctx.getTimePeriod()));
			
			getAccessGroupsForSearchDAO().getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
		
			
			for (biz.firstlook.module.market.search.Market m : otherMarkets) {
				AccessGroupsForSearch ac;
				while (m instanceof MemberMarketWrapper) {
					m = ((MemberMarketWrapper) m).getMemberMarket();
				}
				if (m instanceof biz.firstlook.module.market.search.impl.entity.Market) {
					ac= new AccessGroupsForSearch();
					ac.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
					ac.setMemberId(ctx.getMember().getMemberID());
					ac.setAccessGroupId(((biz.firstlook.module.market.search.impl.entity.Market) m).getMarketID());
					getAccessGroupsForSearchDAO().saveOrUpdate(ac);
				}
				else if (m instanceof biz.firstlook.module.market.search.impl.adapter.MemberMarketGroupAdapter) {
					for (biz.firstlook.module.market.search.impl.entity.MemberMarket mm : (biz.firstlook.module.market.search.impl.adapter.MemberMarketGroupAdapter) m) {
						ac= new AccessGroupsForSearch();
						ac.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
						ac.setMemberId(ctx.getMember().getMemberID());
						ac.setAccessGroupId(mm.getMarket().getMarketID());
						getAccessGroupsForSearchDAO().saveOrUpdate(ac);
					}
				}
				}
				if(!isFirstTimeSearch){
				long time=System.currentTimeMillis();
				getSearchCandidateForSearchDAO().getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
				List<SearchCandidateForSearch> scList= new ArrayList<SearchCandidateForSearch>();
				for (SearchCandidate s1 : candidates) {
						SearchCandidateForSearch sc;
						
						if(s1.getCriteria()==null||s1.getCriteria().isEmpty()){
							sc= new SearchCandidateForSearch();
							sc.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
							sc.setMemberId(ctx.getMember().getMemberID());
							sc.setModelId(s1.getModel().getId());
							sc.setType("MakeModelGrouping");
							scList.add(sc);
	//						getSearchCandidateForSearchDAO().saveOrUpdate(sc);
						}
						Iterator<SearchCandidateCriteria> itr = s1.getCriteria().iterator();
						while ( itr.hasNext()) {
							SearchCandidateCriteria c= itr.next();
							if (c.getModelYear().getModelYear() != null) {
								sc= new SearchCandidateForSearch();
								sc.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
								sc.setMemberId(ctx.getMember().getMemberID());
								sc.setModelId(s1.getModel().getId());
								sc.setType("Year");
								sc.setValue(c.getModelYear().getModelYear().toString());
								scList.add(sc);
	//							getSearchCandidateForSearchDAO().saveOrUpdate(sc);
	
							}
							else {
								sc= new SearchCandidateForSearch();
								sc.setBusinessUnitId(ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID());
								sc.setMemberId(ctx.getMember().getMemberID());
								sc.setModelId(s1.getModel().getId());
								sc.setType("MakeModelGrouping");
								scList.add(sc);
	//							getSearchCandidateForSearchDAO().saveOrUpdate(sc);
								
							}
					}
					
				}
				getSearchCandidateForSearchDAO().saveOrUpdateAll(scList);
				
				long time2=System.currentTimeMillis()-time;
				System.out.println(time2+"-----");
			}	
			results.addAll(getSearchResultDAO().getSearchResultsPurchasing(1, ctx.getExactMatches(), ctx.getTimePeriod(),ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID(),ctx.getMember().getMemberID(),isFirstTimeSearch,scl.getSearchCandidateListTypeID(),ctx.getMaxDistanceFromBusinessUnit()));
		}
			
//			
		return results;
		
	}
	
	public SearchCandidateListDAO getSearchCandidateListDAO() {
		return searchCandidateListDAO;
	}

	public void setSearchCandidateListDAO(SearchCandidateListDAO searchListCandidateDAO) {
		this.searchCandidateListDAO = searchListCandidateDAO;
	}
	
	public SearchCandidateForSearchDAO getSearchCandidateForSearchDAO() {
		return searchCandidateForSearchDAO;
	}

	public void setSearchCandidateForSearchDAO(
			SearchCandidateForSearchDAO searchCandidateForSearchDAO) {
		this.searchCandidateForSearchDAO = searchCandidateForSearchDAO;
	}

	public AccessGroupsForSearchDAO getAccessGroupsForSearchDAO() {
		return accessGroupsForSearchDAO;
	}

	public void setAccessGroupsForSearchDAO(
			AccessGroupsForSearchDAO accessGroupsForSearchDAO) {
		this.accessGroupsForSearchDAO = accessGroupsForSearchDAO;
	}

	public DefaultSearchCandidateListDAO getDefaultSearchCandidateListDAO() {
		return defaultSearchCandidateListDAO;
	}

	public void setDefaultSearchCandidateListDAO(DefaultSearchCandidateListDAO defaultSearchCandidateListDAO) {
		this.defaultSearchCandidateListDAO = defaultSearchCandidateListDAO;
	}

	public MemberMarketListDAO getMemberMarketListDAO() {
		return memberMarketListDAO;
	}

	public void setMemberMarketListDAO(MemberMarketListDAO memberMarketListDAO) {
		this.memberMarketListDAO = memberMarketListDAO;
	}

	public BusinessUnitMarketDAO getBusinessUnitMarketDAO() {
		return businessUnitMarketDAO;
	}

	public void setBusinessUnitMarketDAO(BusinessUnitMarketDAO businessUnitMarketDAO) {
		this.businessUnitMarketDAO = businessUnitMarketDAO;
	}

	public MarketDAO getMarketDAO() {
		return marketDAO;
	}

	public void setMarketDAO(MarketDAO marketDAO) {
		this.marketDAO = marketDAO;
	}

	public SearchResultDAO getSearchResultDAO() {
		return searchResultDAO;
	}

	public void setSearchResultDAO(SearchResultDAO searchResultDAO) {
		this.searchResultDAO = searchResultDAO;
	}

	public Persistor<biz.firstlook.module.market.search.impl.entity.MemberMarketList, java.lang.Integer> getMarketListPersistor() {
		return marketListPersistor;
	}

	public void setMarketListPersistor(Persistor<biz.firstlook.module.market.search.impl.entity.MemberMarketList, java.lang.Integer> marketListPersistor) {
		this.marketListPersistor = marketListPersistor;
	}

	public Persistor<biz.firstlook.module.market.search.impl.entity.SearchCandidateList, java.lang.Integer> getSearchCandidateListPersistor() {
		return searchCandidateListPersistor;
	}

	public void setSearchCandidateListPersistor(Persistor<biz.firstlook.module.market.search.impl.entity.SearchCandidateList, java.lang.Integer> searchCandidateListPersistor) {
		this.searchCandidateListPersistor = searchCandidateListPersistor;
	}

}
