package biz.firstlook.module.market.search.impl.entity.dao;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.market.search.impl.entity.MemberMarket;
import biz.firstlook.module.market.search.impl.entity.MemberMarketList;
import biz.firstlook.module.market.search.impl.entity.MemberMarketListType;

public class MemberMarketListDAO extends GenericHibernate<MemberMarketList, Integer> implements IGenericDAO<MemberMarketList, Integer>{

	public MemberMarketListDAO() {
		super(MemberMarketList.class);
	}

	public MemberMarketList findByMemberId(Integer memberID, Integer businessUnitID, Integer memberMarketListTypeID) {
		StringBuffer hql = new StringBuffer();
		hql.append(" from");
		hql.append(" biz.firstlook.module.market.search.impl.entity.MemberMarketList l");
		hql.append(" where");
		hql.append(" l.memberID = ?");
		hql.append(" and l.businessUnitID = ?");
		hql.append(" and l.memberMarketListType.memberMarketListTypeID = ?");
		List memberMarketLists = getHibernateTemplate().find(
				hql.toString(),
				new Object[] { memberID, businessUnitID, memberMarketListTypeID } );
		if (memberMarketLists.size() > 1) {
			throw new IllegalStateException("A member should have at most one market list of a given type");
		}
		if (memberMarketLists.isEmpty()) {
			return new MemberMarketList(
					null,
					memberID,
					businessUnitID,
					(memberMarketListTypeID == 1
							? MemberMarketListType.MARKET_LIST_PERMANENT
							: (memberMarketListTypeID == 2
									? MemberMarketListType.MARKET_LIST_CURRENT
									: MemberMarketListType.MARKET_LIST_TEMPORARY)),
					new ArrayList<MemberMarket>(),
					100);
		}
		return (MemberMarketList) memberMarketLists.get(0);
	}
	
}
