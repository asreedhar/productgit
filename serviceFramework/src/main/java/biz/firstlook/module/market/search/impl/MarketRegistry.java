package biz.firstlook.module.market.search.impl;

import biz.firstlook.module.market.search.Market;

/**
 * A well-known location to lookup <code>Market</code>s.
 * 
 * @author swenmouth
 */
public abstract class MarketRegistry {
	
	public abstract Market lookupMarket(Integer marketID, String name, Integer accessGroupTypeID);
	
	private static MarketRegistry registry;
	
	public static MarketRegistry getInstance() {
		return registry;
	}
	
	public static void setInstance(MarketRegistry registry) {
		MarketRegistry.registry = registry;
	}
}
