package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.List;

import biz.firstlook.commons.collections.CollectionAdapter;
import biz.firstlook.commons.hibernate.CollectionMutatorCallback;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.impl.entity.BusinessUnitMarket;

/**
 * Wrapper that presents a collection of {@link BusinessUnitMarket}s to its client
 * as a collection of {@link Market}s.
 * 
 * @author swenmouth
 */
public class BusinessUnitMarketCollectionAdapter extends CollectionAdapter<Market,BusinessUnitMarket> 
	implements Serializable, CollectionMutatorCallback<Market, List<BusinessUnitMarket>> {

	private static final long serialVersionUID = 1683596452534284199L;

	public BusinessUnitMarketCollectionAdapter(List<BusinessUnitMarket> markets) {
		super(markets);
	}

	public Market adaptItem(BusinessUnitMarket market) {
		return market.getMarket();
	}

	public void synchEntity(List<BusinessUnitMarket> entity) {
		collection = entity;
	}
	
}
