package biz.firstlook.module.market.search.presentation;

import java.util.Date;
import java.util.Set;

import biz.firstlook.module.inventory.StockLevel;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

public interface DecoratedModelYear extends ModelYear {
	public StockLevel getStockLevel();
	public Date getExpires();
	public Set<SearchCandidateCriteriaAnnotation> getAnnotations();
}
