package biz.firstlook.module.market.search.impl.entity.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.market.search.impl.entity.SearchCandidate;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateList;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateListType;

public class SearchCandidateListDAO extends GenericHibernate<SearchCandidateList, Integer> implements IGenericDAO<SearchCandidateList, Integer> {

	public SearchCandidateListDAO() {
		super(SearchCandidateList.class);
	}

	public SearchCandidateList findByMemberId(Integer memberID, Integer businessUnitID, SearchCandidateListType searchCandidateListType) {
		StringBuffer hql = new StringBuffer();
		hql.append(" from");
		hql.append(" biz.firstlook.module.market.search.impl.entity.SearchCandidateList l");
		hql.append(" left outer join fetch l.searchCandidateListType");
		hql.append(" where");
		hql.append(" l.memberID = ?");
		hql.append(" and l.businessUnitID = ?");
		hql.append(" and l.searchCandidateListType.searchCandidateListTypeID = ?");
		List searchCandidateLists = getHibernateTemplate().find(
				hql.toString(),
				new Object[] { memberID, businessUnitID, searchCandidateListType.getSearchCandidateListTypeID() } );
		if (searchCandidateLists.size() > 1) {
			throw new IllegalStateException("A member should have at most one search candidate list of a given type");
		}
		if (searchCandidateLists.isEmpty()) {
			return new SearchCandidateList(
					null,
					Calendar.getInstance().getTime(),
					memberID,
					businessUnitID,
					searchCandidateListType,
					new ArrayList<SearchCandidate>());
		}
		return (SearchCandidateList) searchCandidateLists.get(0);
	}
	
}
