package biz.firstlook.module.market.search.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.market.search.impl.entity.Market;

public class MarketDAO extends GenericHibernate<Market,Integer> implements IGenericDAO<Market,Integer> {

	public MarketDAO() {
		super(Market.class);
	}

	@SuppressWarnings("unchecked")
	public List<Market> findPublicAuctions(Integer accessGroupTypeId) {
		StringBuffer hql = new StringBuffer();
		hql.append(" from");
		hql.append(" biz.firstlook.module.market.search.impl.entity.Market m");
		hql.append(" where");
		hql.append(" m.publicMarket = true");
		hql.append(" and m.accessGroupTypeId = ?");
		return (List<Market>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { accessGroupTypeId } );
	}
	
}
