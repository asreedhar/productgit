package biz.firstlook.module.market.search.impl.entity.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultReader;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.StatementCreatorUtils;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.commons.sql.ResultSetUtils;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.impl.MarketRegistry;
import biz.firstlook.module.market.search.impl.SimpleMarketSummaryImpl;
import biz.firstlook.module.market.search.impl.SimpleMarketVehicleImpl;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Trim;
import biz.firstlook.module.purchasing.vehicle.Vehicle;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;
import biz.firstlook.module.purchasing.vehicle.impl.builder.VehicleKey;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;

public class SearchResultDAO extends JdbcDaoSupport {

	static final String SQL_SEARCH_PURCHASING = "EXEC dbo.GetPurchasingCenterVehiclesByMakeModelGroupingXML ?, null, ?, 0";

	static final String SQL_SEARCH_AUCTION = "EXEC dbo.GetPurchasingCenterAuctionsByMakeModelGroupingXML ?, ?, ?, ?, 0";
	
	static final String SQL_SEARCH_PURCHASING_WITHOUT_XML = "EXEC dbo.GetPurchasingCenterVehicles ?,?,?,?,?, null, ?, 0";
	
	static final String SQL_SEARCH_AUCTION_WITHOUT_XML = "EXEC dbo.GetPurchasingCenterAuctions ?, ?, ?, ?,?, ?, ?, ?, 0";
	
	private static final transient Log log = LogFactory.getLog(SearchResultDAO.class);
	 
	private int queryTimeout = 90;
	
	public SearchResultDAO() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	public List<MarketSummary> getSearchSummary(String queryXML, int fixme, boolean exactMatches, Integer timePeriod) {
		if(fixme == 1){
			log.debug(SQL_SEARCH_PURCHASING + " being called from getSearchSummary() with QueryXML: " + queryXML );
			return (List<MarketSummary>) getJdbcTemplate().query(SQL_SEARCH_PURCHASING, new SearchSummaryPreparedStatementSetter(fixme, queryXML, exactMatches, timePeriod, true), new SearchSummaryRowCallbackHandler());
		}
		else {
			return (List<MarketSummary>) getJdbcTemplate().query(SQL_SEARCH_AUCTION, new SearchSummaryPreparedStatementSetter(fixme, queryXML, exactMatches, timePeriod, true),
					new SearchSummaryRowCallbackHandler(	MarketRegistry.getInstance().lookupMarket(7, "ADESA", 7)));

		}
	}

	@SuppressWarnings("unchecked")
	public List<MarketVehicle> getSearchResults(final String queryXML, int fixme, boolean exactMatches, Integer timePeriod) {
		if(fixme == 1){
			log.debug(SQL_SEARCH_PURCHASING + " being called from getSearchResults() with QueryXML: " + queryXML );
			return (List<MarketVehicle>) getJdbcTemplate().query(SQL_SEARCH_PURCHASING,  new SearchSummaryPreparedStatementSetter(fixme, queryXML, exactMatches, timePeriod, false), new SearchResultRowCallbackHandler());
		}
		else {
			return (List<MarketVehicle>) getJdbcTemplate().query(SQL_SEARCH_AUCTION, new SearchSummaryPreparedStatementSetter(fixme, queryXML, exactMatches, timePeriod, false),
					new SearchResultRowCallbackHandler(	MarketRegistry.getInstance().lookupMarket(7, "ADESA", 7)));
		}
	}
	@SuppressWarnings("unchecked")
	public List<MarketSummary> getSearchSummary(int fixme, boolean exactMatches, Integer timePeriod,Integer dealerId, Integer memberId, Integer selectedSaleId,Integer persistenceScope,Integer maxDistanceFromDealer){
		
		return (List<MarketSummary>) getJdbcTemplate().query(SQL_SEARCH_AUCTION_WITHOUT_XML, new SearchSummaryPreparedStatementSetterNew(fixme, exactMatches, timePeriod, true,memberId,dealerId,persistenceScope,selectedSaleId,maxDistanceFromDealer),
				new SearchSummaryRowCallbackHandler(	MarketRegistry.getInstance().lookupMarket(7, "ADESA", 7)));
		
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MarketSummary> getSearchSummaryPurchasing(int fixme, boolean exactMatches, Integer timePeriod,Integer dealerId, Integer memberId, boolean isFirstTimeSearch,Integer persistenceScope,Integer maxDistanceFromDealer){
		if(fixme==1){
			return (List<MarketSummary>) getJdbcTemplate().query(SQL_SEARCH_PURCHASING_WITHOUT_XML, new SearchSummaryPreparedStatementSetterNewPurchasing(fixme, exactMatches, timePeriod, true,memberId,dealerId,persistenceScope,isFirstTimeSearch,maxDistanceFromDealer),
					new SearchSummaryRowCallbackHandler());
		}
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<MarketVehicle> getSearchResults(int fixme, boolean exactMatches, Integer timePeriod,Integer dealerId, Integer memberId, Integer selectedSaleId,Integer persistenceScope,Integer maxDistanceFromDealer){
		
		return (List<MarketVehicle>) getJdbcTemplate().query(SQL_SEARCH_AUCTION_WITHOUT_XML, new SearchSummaryPreparedStatementSetterNew(fixme, exactMatches, timePeriod, false,memberId,dealerId,persistenceScope,selectedSaleId,maxDistanceFromDealer),
				new SearchResultRowCallbackHandler(	MarketRegistry.getInstance().lookupMarket(7, "ADESA", 7)));
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<MarketVehicle> getSearchResultsPurchasing(int fixme, boolean exactMatches, Integer timePeriod,Integer dealerId, Integer memberId, boolean isFirstTimeSearch,Integer persistenceScope,Integer maxDistanceFromDealer){
		if(fixme==1){
			return (List<MarketVehicle>) getJdbcTemplate().query(SQL_SEARCH_PURCHASING_WITHOUT_XML, new SearchSummaryPreparedStatementSetterNewPurchasing(fixme, exactMatches, timePeriod, false,memberId,dealerId,persistenceScope,isFirstTimeSearch,maxDistanceFromDealer),
					new SearchResultRowCallbackHandler());
		}
		return null;
		
	}
	
	class SearchResultRowCallbackHandler implements RowCallbackHandler, ResultReader {
		List<MarketVehicle> results = new ArrayList<MarketVehicle>();
		Market market;
		VehicleKey vehicleKey;
		long created = System.currentTimeMillis();
		long started = 0;
		public SearchResultRowCallbackHandler() {
			this(null);
		}
		public SearchResultRowCallbackHandler(Market market) {
			super();
			this.market = market;
			this.vehicleKey = new VehicleKey();
		}
		public void processRow(ResultSet rs) throws SQLException {
			if (started == 0) {
				started = System.currentTimeMillis();
			}
			results.add(getMarketVehicle(rs));
		}
		public List<MarketVehicle> getResults() {
			long finished = System.currentTimeMillis();
			Logger logger = Logger.getLogger(getClass());
			logger.debug("SQL: " + (started-created) + "ms");
			logger.debug("Marshall x " + results.size() + ": " + (finished-started) + "ms");
			return results;
		}
		private Market getMarket(ResultSet rs) throws SQLException {
			if (market == null) {
				return MarketRegistry.getInstance().lookupMarket(
						null,
						ResultSetUtils.getString(rs, "CustomerFacingDescription", false),
						ResultSetUtils.getInt(rs, "AccessGroupTypeId", true)); 
				
			}
			return market;
		}
		private Vehicle getVehicle(ResultSet rs) throws SQLException {
			final VehicleComponentBuilder builder = VehicleComponentBuilderFactory.getInstance().newVehicleBuilder();
			final VehicleService service = VehicleServiceFactory.getFactory().getService();
			final Model model = service.getModel(ResultSetUtils.getInt(rs, "ModelId", false));
			Trim trim = builder.buildTrim(
					(market != null)
						? ResultSetUtils.getString(rs, "Series", false)
						: ResultSetUtils.getString(rs, "Trim", false));
			ModelYear modelYear = builder.buildModelYear(ResultSetUtils.getInt(rs, "Year", true));
			return builder.buildVehicle(
					model,
					trim,
					modelYear,
					ResultSetUtils.getString(rs, "VIN", true),
					ResultSetUtils.getString(rs, "Color", false),
					ResultSetUtils.getString(rs, "Engine", true),
					ResultSetUtils.getInt(rs, "Mileage", true),
					ResultSetUtils.getString(rs, "VehicleDriveTrain", true));
		}
		private MarketVehicle getMarketVehicle(ResultSet rs) throws SQLException {
			Boolean displayUnitsCostToDealerGroup = ResultSetUtils.getBoolean( rs, "DisplayUnitCostToDealerGroup", true);
			if (displayUnitsCostToDealerGroup == null) {
				displayUnitsCostToDealerGroup = Boolean.FALSE;
			}
			return new SimpleMarketVehicleImpl(
					getMarket(rs),
					getVehicle(rs),
					displayUnitsCostToDealerGroup ? ResultSetUtils.getInt(rs, "UnitCost", true) : null,
					ResultSetUtils.getInt(rs, "CurrentBid", true),
					ResultSetUtils.getInt(rs, "BuyPrice", true),
					ResultSetUtils.getString(rs, "Location", true),
					ResultSetUtils.getString(rs, "DealerNickName", true),
					ResultSetUtils.getString(rs, "Expires", true),
					ResultSetUtils.getDate(rs, "InventoryReceivedDate", true),
					ResultSetUtils.getInt(rs, "DistanceFromDealer", true),
					ResultSetUtils.getString(rs, "Lot", true),
					ResultSetUtils.getString(rs, "RunNumber", true),
					ResultSetUtils.getBoolean(rs, "PotentialMatch", true),
					ResultSetUtils.getString(rs, "Consignor", true),
					ResultSetUtils.getString(rs, "AMSOptions", true),
					ResultSetUtils.getString(rs, "AMSAnnouncements", true),
					ResultSetUtils.getInt(rs, "InventoryID", true ),
					ResultSetUtils.getString(rs, "DetailURL", true),
					ResultSetUtils.getInt(rs, "MakeModelGroupingID", true),
					ResultSetUtils.getInt(rs, "GroupingDescriptionID", true),
                    ResultSetUtils.getInt(rs, "AccessGroupId", true),
                    ResultSetUtils.getInt(rs, "VehicleID", true),
                    ResultSetUtils.getString(rs, "StockNumber", true),
                    ResultSetUtils.getFloat(rs, "TransferPrice", true),
                    ResultSetUtils.getBoolean(rs, "TransferForRetailOnly", true),
                    ResultSetUtils.getString(rs, "Title", true));
			
		}
	}
	
	class SearchSummaryRowCallbackHandler implements RowCallbackHandler, ResultReader {
		List<MarketSummary> results = new ArrayList<MarketSummary>();
		Market market;
		long created = System.currentTimeMillis();
		long started = 0;
		public SearchSummaryRowCallbackHandler() {
			super();
			this.market = null;
		}
		public SearchSummaryRowCallbackHandler(Market market) {
			super();
			this.market = market;
		}
		public void processRow(ResultSet rs) throws SQLException {
			if (started == 0) {
				started = System.currentTimeMillis();
			}
			results.add(getMarketSummary(rs));
		}
		private MarketSummary getMarketSummary(ResultSet rs) throws SQLException {
			Market theMarket = market;
			if (theMarket == null) {
				theMarket = MarketRegistry.getInstance().lookupMarket(
						null,
						ResultSetUtils.getString(rs, "CustomerFacingDescription", false),
						ResultSetUtils.getInt(rs, "AccessGroupTypeId", false));
			}
			return new SimpleMarketSummaryImpl(
					theMarket,
					ResultSetUtils.getDate(rs, "DateHeld", true),
					ResultSetUtils.getDouble(rs, "Distance", true),
					ResultSetUtils.getString(rs, "Location", true),
					ResultSetUtils.getString(rs, "Auction", true),
					ResultSetUtils.getDouble(rs, "Score", true),
					ResultSetUtils.getString(rs, "Title", true),
					(market == null)
						? ResultSetUtils.getInt(rs, "TotalMatches", true)
						: ResultSetUtils.getInt(rs, "VehiclesMatched", true),
					ResultSetUtils.getInt(rs, "PrecisionMatches", true),
					ResultSetUtils.getInt(rs, "SaleID", true),
					ResultSetUtils.getInt(rs, "LocationID", true));
			
		}
		public List<MarketSummary> getResults() {
			long finished = System.currentTimeMillis();
			Logger logger = Logger.getLogger(getClass());
			logger.debug("SQL: " + (started-created) + "ms");
			logger.debug("Marshall: " + (finished-started) + "ms");
			return results;
		}
	}

	class SearchSummaryPreparedStatementSetter implements PreparedStatementSetter {
		public int fixme;
		public String queryXML;
		public boolean exactMatches;
		public Integer timePeriod;
		boolean isSearchSummary;
		
		public SearchSummaryPreparedStatementSetter(int fixme, String queryXML, boolean exactMatches, Integer timePeriod, boolean isSearchSummary){
			this.fixme = fixme;
			this.queryXML = queryXML;
			this.exactMatches = exactMatches;
			this.timePeriod = timePeriod;
			this.isSearchSummary = isSearchSummary;
		}
		
		public void setValues(PreparedStatement ps) throws SQLException {
			
			int resultsMode;
			StatementCreatorUtils.setParameterValue(ps, 1, java.sql.Types.VARCHAR, null, queryXML);
			if(fixme == 1){
				resultsMode = isSearchSummary ? 1 : 0;
    			StatementCreatorUtils.setParameterValue(ps, 2, java.sql.Types.INTEGER , null, new Integer(resultsMode));
			}
			else{
				resultsMode = isSearchSummary ? 0 : exactMatches ? 2 : 1;
				StatementCreatorUtils.setParameterValue(ps, 2, java.sql.Types.INTEGER , null, new Integer(resultsMode));
				
				Calendar c = Calendar.getInstance();
				Date t0 = c.getTime();
				c.add(Calendar.DAY_OF_MONTH, 7 * timePeriod);
				Date t1 = c.getTime();
				
				StatementCreatorUtils.setParameterValue(ps, 3, java.sql.Types.DATE , null, t0);
				StatementCreatorUtils.setParameterValue(ps, 4, java.sql.Types.DATE , null, t1);
			}
			
			ps.setQueryTimeout(queryTimeout);
		}
		
	}
	class SearchSummaryPreparedStatementSetterNew implements PreparedStatementSetter {
		public int fixme;
		
		public boolean exactMatches;
		public Integer timePeriod;
		boolean isSearchSummary;
		public Integer memberId;
		public Integer dealerId;
		public Integer persistenceScope;
		public Integer selectedSaleId;
		public Integer maxDistanceFromDealer;
		
		
		public SearchSummaryPreparedStatementSetterNew(int fixme,
				boolean exactMatches, Integer timePeriod,
				boolean isSearchSummary, Integer memberId, Integer dealerId,
				Integer persistenceScope, Integer selectedSaleId,Integer maxDistanceFromDealer) {
			super();
			this.fixme = fixme;
			this.maxDistanceFromDealer= maxDistanceFromDealer;
			this.exactMatches = exactMatches;
			this.timePeriod = timePeriod;
			this.isSearchSummary = isSearchSummary;
			this.memberId = memberId;
			this.dealerId = dealerId;
			this.persistenceScope = persistenceScope;
			this.selectedSaleId = selectedSaleId;
		}

	
		
		public void setValues(PreparedStatement ps) throws SQLException {
			
			int resultsMode;
			StatementCreatorUtils.setParameterValue(ps, 1, java.sql.Types.INTEGER, null, dealerId);
			StatementCreatorUtils.setParameterValue(ps, 2, java.sql.Types.INTEGER, null, memberId);
			StatementCreatorUtils.setParameterValue(ps, 3, java.sql.Types.INTEGER, null, selectedSaleId);
			StatementCreatorUtils.setParameterValue(ps, 4, java.sql.Types.INTEGER, null, persistenceScope);
			StatementCreatorUtils.setParameterValue(ps, 5, java.sql.Types.INTEGER, null, maxDistanceFromDealer);
			if(fixme == 1){
				resultsMode = isSearchSummary ? 1 : 0;
    			StatementCreatorUtils.setParameterValue(ps, 6, java.sql.Types.INTEGER , null, new Integer(resultsMode));
			}
			else{
				resultsMode = isSearchSummary ? 0 : exactMatches ? 2 : 1;
				StatementCreatorUtils.setParameterValue(ps, 6, java.sql.Types.INTEGER , null, new Integer(resultsMode));
				
				Calendar c = Calendar.getInstance();
				Date t0 = c.getTime();
				c.add(Calendar.DAY_OF_MONTH, 7 * timePeriod);
				Date t1 = c.getTime();
				
				StatementCreatorUtils.setParameterValue(ps, 7, java.sql.Types.DATE , null, t0);
				StatementCreatorUtils.setParameterValue(ps, 8, java.sql.Types.DATE , null, t1);
			}
			
			ps.setQueryTimeout(queryTimeout);
		}
		
	}
	class SearchSummaryPreparedStatementSetterNewPurchasing implements PreparedStatementSetter {
		public int fixme;
		
		public boolean exactMatches;
		public Integer timePeriod;
		boolean isSearchSummary;
		public Integer memberId;
		public Integer dealerId;
		public Integer persistenceScope;
		public boolean isFirstTimeSearch;
		public Integer maxDistanceFromDealer;
		
		
		public SearchSummaryPreparedStatementSetterNewPurchasing(int fixme,
				boolean exactMatches, Integer timePeriod,
				boolean isSearchSummary, Integer memberId, Integer dealerId,
				Integer persistenceScope, boolean isFirstTimeSearch,Integer maxDistanceFromDealer) {
			super();
			this.fixme = fixme;
			this.maxDistanceFromDealer= maxDistanceFromDealer;
			this.exactMatches = exactMatches;
			this.timePeriod = timePeriod;
			this.isSearchSummary = isSearchSummary;
			this.memberId = memberId;
			this.dealerId = dealerId;
			this.persistenceScope = persistenceScope;
			this.isFirstTimeSearch = isFirstTimeSearch;
		}

	
		
		public void setValues(PreparedStatement ps) throws SQLException {
			
			int resultsMode;
			StatementCreatorUtils.setParameterValue(ps, 1, java.sql.Types.INTEGER, null, dealerId);
			StatementCreatorUtils.setParameterValue(ps, 2, java.sql.Types.INTEGER, null, memberId);
			StatementCreatorUtils.setParameterValue(ps, 3, java.sql.Types.BIT, null, (isFirstTimeSearch?0:1));
			StatementCreatorUtils.setParameterValue(ps, 4, java.sql.Types.INTEGER, null, persistenceScope);
			StatementCreatorUtils.setParameterValue(ps, 5, java.sql.Types.INTEGER, null, maxDistanceFromDealer);
			resultsMode = isSearchSummary ? 1 : 0;
			StatementCreatorUtils.setParameterValue(ps, 6, java.sql.Types.INTEGER , null, new Integer(resultsMode));
			
			
			ps.setQueryTimeout(queryTimeout);
		}
		
	}
	public void setQueryTimeout(int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}
	
}