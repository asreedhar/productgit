package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@org.hibernate.annotations.Entity(mutable=false)
@Table(name="CIACategories")
public class CIACategory implements Serializable {

	private static final long serialVersionUID = 3627502906545869494L;
	
	public static transient final CIACategory POWERZONE = new CIACategory(1, "PowerZone");
	public static transient final CIACategory WINNERS  = new CIACategory(2, "Winners");
	public static transient final CIACategory GOODBETS = new CIACategory(3, "GoodBets");
	public static transient final CIACategory MARKET_PERFORMERS = new CIACategory(4, "MarketPerformers");
	public static transient final CIACategory MANAGERS_CHOICE = new CIACategory(5, "Managers Choice");

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer CIACategoryID;
	private String description;
	
	public CIACategory() {
		super();
	}

	public CIACategory(Integer categoryID, String description) {
		super();
		this.CIACategoryID = categoryID;
		this.description = description;
	}

	public Integer getCIACategoryID() {
		return CIACategoryID;
	}
	public void setCIACategoryID(Integer categoryID) {
		CIACategoryID = categoryID;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof CIACategory) {
			return Functions.nullSafeEquals(getCIACategoryID(), ((CIACategory) obj).getCIACategoryID());
		}
		return false;
	}
	
	public int hashCode() {
		return Functions.nullSafeHashCode(getCIACategoryID());
	}
	
}