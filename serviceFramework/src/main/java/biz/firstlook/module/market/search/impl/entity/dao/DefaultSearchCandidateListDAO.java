package biz.firstlook.module.market.search.impl.entity.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ResultReader;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.commons.sql.ResultSetUtils;
import biz.firstlook.module.market.search.impl.entity.CIACategory;
import biz.firstlook.module.market.search.impl.entity.SearchCandidate;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOption;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;

public class DefaultSearchCandidateListDAO extends JdbcDaoSupport {

	private static final String SQL_DEFAULT_BUYING_PLAN = "SELECT DISTINCT M.ModelId, D.ModelYear, D.Type, D.CIACategoryID FROM dbo.GetDefaultBuyingPlanForSearchAndAquisition(@BusinessUnitID) D JOIN MakeModelGrouping M ON M.GroupingDescriptionID = D.GroupingDescriptionID WHERE M.ModelID <> 0 ORDER BY M.ModelId, D.ModelYear";
	
	public DefaultSearchCandidateListDAO() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<SearchCandidate> getDefaultBuyingPlan(Integer businessUnitID) {
		return (List<SearchCandidate>) getJdbcTemplate().query(
				SQL_DEFAULT_BUYING_PLAN.replaceFirst("@BusinessUnitID", businessUnitID.toString()),
				new Object[] { },
				new int[] { },
				new SearchCandidateRowCallbackHandler());
	}

	class SearchCandidateRowCallbackHandler implements RowCallbackHandler, ResultReader {
		Map<Integer,SearchCandidate> searchCandidates = new HashMap<Integer,SearchCandidate>();
		public SearchCandidateRowCallbackHandler() {
			super();
		}
		public void processRow(ResultSet rs) throws SQLException {
			final Integer modelId = ResultSetUtils.getInt(rs, "ModelId", true);
			final Integer modelYear = ResultSetUtils.getInt(rs, "ModelYear", true);
			SearchCandidateOptionAnnotationType searchCandidateOptionAnnotationType = null;
			switch (ResultSetUtils.getInt(rs, "Type", true)) {
				case 1:
					searchCandidateOptionAnnotationType = SearchCandidateOptionAnnotationType.OPTIMAL;
					break;
				case 2:
					searchCandidateOptionAnnotationType = SearchCandidateOptionAnnotationType.BUYING;
					break;
				case 3:
					searchCandidateOptionAnnotationType = SearchCandidateOptionAnnotationType.HOTLIST;
					break;
				case 4:
					searchCandidateOptionAnnotationType = SearchCandidateOptionAnnotationType.ONETIME;
					break;
				default:
					throw new IllegalStateException("Invalid Annotation ID");
			}
			CIACategory ciaCategory = null;
			switch (ResultSetUtils.getInt(rs, "CIACategoryID", true)) {
				case 1:
					ciaCategory = CIACategory.POWERZONE;
					break;
				case 2:
					ciaCategory = CIACategory.WINNERS;
					break;
				case 3:
					ciaCategory = CIACategory.GOODBETS;
					break;
				case 4:
					ciaCategory = CIACategory.MARKET_PERFORMERS;
					break;
				case 5:
					ciaCategory = CIACategory.MANAGERS_CHOICE;
					break;
				default:
					throw new IllegalStateException("Invalid Annotation ID");
			}
			SearchCandidate searchCandidate = searchCandidates.get(modelId);
			if (searchCandidate == null) {
				searchCandidate = new SearchCandidate(null,modelId,false,new ArrayList<SearchCandidateOption>(), new HashSet<CIACategory>());
				searchCandidates.put(modelId, searchCandidate);
			}
			searchCandidate.getSearchCandidateAnnotations().add(ciaCategory);
			SearchCandidateOption searchCandidateOption = new SearchCandidateOption(null,modelYear,null,false,new HashSet<SearchCandidateOptionAnnotationType>());
			searchCandidateOption.getSearchCandidateOptionAnnotations().add(searchCandidateOptionAnnotationType);
			searchCandidate.getSearchCandidateOptions().add(searchCandidateOption);
		}
		public List<SearchCandidate> getResults() {
			ArrayList<SearchCandidate> l = new ArrayList<SearchCandidate>();
			for (SearchCandidate s : searchCandidates.values()) {
				l.add(s);
			}
			return l;
		}
	}

}
