package biz.firstlook.module.market.search.presentation;

import java.util.Collection;
import java.util.List;

import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchContext;

public interface MarketSearchPresentationService {
	public List<DecoratedModelGroup> getDecoratedModelGroups(
			SearchContext ctx,
			Collection<SearchCandidate> searchCandidates);
}
