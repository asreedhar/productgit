package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;

import biz.firstlook.module.market.search.AbstractSearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.impl.entity.SearchCandidateOptionAnnotationType;

/**
 * Wraps a <code>SearchCandidateOptionAnnotationType</code> and presents it as a
 * <code>SearchCandidateCriteriaAnnotation</code>.
 * 
 * @author swenmouth
 */
public class SearchCandidateOptionAnnotationTypeAdapter extends AbstractSearchCandidateCriteriaAnnotation implements Serializable, SearchCandidateCriteriaAnnotation {

	private static final long serialVersionUID = 6995313446904069321L;
	
	private SearchCandidateOptionAnnotationType searchCandidateOptionAnnotationType;
	
	public SearchCandidateOptionAnnotationTypeAdapter(SearchCandidateOptionAnnotationType searchCandidateOptionAnnotationType) {
		this.searchCandidateOptionAnnotationType = searchCandidateOptionAnnotationType;
	}

	public String getName() {
		return searchCandidateOptionAnnotationType.getDescription();
	}

}
