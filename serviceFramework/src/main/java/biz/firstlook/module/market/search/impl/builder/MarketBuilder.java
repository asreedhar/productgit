package biz.firstlook.module.market.search.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.impl.SimpleMarketImpl;

public class MarketBuilder implements FlyweightBuilder<Market,MarketKey> {

	public Market newFlyweight(MarketKey key) {
		return new SimpleMarketImpl(
			key.marketID,
			key.name,
			getMarketType(key.accessGroupTypeID),
			getMarketAdministrator(key.accessGroupTypeID));
	}
	
	static MarketType getMarketType(Integer accessGroupTypeId) {
		switch (accessGroupTypeId) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 8:
			case 9:
			case 10:
				return MarketType.ONLINE;
			case 6:
				return MarketType.IN_GROUP;
			case 7:
				return MarketType.LIVE;
			default:
					throw new IllegalStateException("Unknown MarketType: " + accessGroupTypeId);
		}
	}
	
	static MarketAdministrator getMarketAdministrator(Integer accessGroupTypeId) {
		MarketAdministrator marketAdministrator = null;
		if (accessGroupTypeId != null) {
			switch (accessGroupTypeId) {
			case 1:
				marketAdministrator = MarketAdministrator.MANUFACTURER;
				break;
			case 2:
				marketAdministrator = MarketAdministrator.ENTERPRISE;
				break;
			case 3:
				marketAdministrator = MarketAdministrator.ATC_OPEN;
				break;
			case 4:
				marketAdministrator = MarketAdministrator.GMAC;
				break;
			case 5:
				marketAdministrator = MarketAdministrator.TFS_LFS;
				break;
			case 6:
				marketAdministrator = MarketAdministrator.IN_GROUP;
				break;
			case 7:
				marketAdministrator = MarketAdministrator.LIVE;
				break;
			case 8:
				marketAdministrator = MarketAdministrator.OVE;
				break;
			case 9:
				marketAdministrator = MarketAdministrator.DRIVE_AWAY;
				break;
			case 10:
				marketAdministrator = MarketAdministrator.AutoSales;
				break;
			default:
					throw new IllegalStateException("Unknown MarketAdminstrator: " + accessGroupTypeId);
			}
		}
		return marketAdministrator;
	}
}
