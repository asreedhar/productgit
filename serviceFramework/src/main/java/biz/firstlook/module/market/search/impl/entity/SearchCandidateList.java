package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@Table(name="SearchCandidateList")
public class SearchCandidateList implements Serializable {

	private static final long serialVersionUID = -8117901404145550367L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer searchCandidateListID;
	
	private Date created;
	private Integer memberID;
	private Integer businessUnitID;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="searchCandidateListTypeID")
	private SearchCandidateListType searchCandidateListType;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="searchCandidateListID",nullable=false)
	@org.hibernate.annotations.Cascade(value={org.hibernate.annotations.CascadeType.ALL,org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
	private List<SearchCandidate> searchCandidates;
	
	public SearchCandidateList() {
		super();
	}
	
	public SearchCandidateList(Integer searchCandidateListID, Date created, Integer memberID, Integer businessUnitID, SearchCandidateListType searchCandidateListType, List<SearchCandidate> searchCandidates) {
		super();
		this.searchCandidateListID = searchCandidateListID;
		this.created = created;
		this.memberID = memberID;
		this.businessUnitID = businessUnitID;
		this.searchCandidateListType = searchCandidateListType;
		this.searchCandidates = searchCandidates;
	}

	public Integer getBusinessUnitID() {
		return businessUnitID;
	}
	public void setBusinessUnitID(Integer businessUnitID) {
		this.businessUnitID = businessUnitID;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getMemberID() {
		return memberID;
	}

	public void setMemberID(Integer memberID) {
		this.memberID = memberID;
	}
	
	public Integer getSearchCandidateListID() {
		return searchCandidateListID;
	}
	public void setSearchCandidateListID(Integer searchCandidateListID) {
		this.searchCandidateListID = searchCandidateListID;
	}
	
	public SearchCandidateListType getSearchCandidateListType() {
		return searchCandidateListType;
	}
	public void setSearchCandidateListType(
			SearchCandidateListType searchCandidateListType) {
		this.searchCandidateListType = searchCandidateListType;
	}
	
	public List<SearchCandidate> getSearchCandidates() {
		return searchCandidates;
	}
	public void setSearchCandidates(List<SearchCandidate> searchCandidates) {
		this.searchCandidates = searchCandidates;
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (obj instanceof SearchCandidateList) {
			SearchCandidateList s = (SearchCandidateList) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getMemberID(), s.getMemberID());
			lEquals &= Functions.nullSafeEquals(getBusinessUnitID(), s.getBusinessUnitID());
			lEquals &= Functions.nullSafeEquals(getSearchCandidateListType(), s.getSearchCandidateListType());
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + (getMemberID() == null ? 0 : getMemberID().hashCode());
		hashCode = 31 * hashCode + (getBusinessUnitID() == null ? 0 : getBusinessUnitID().hashCode());
		hashCode = 31 * hashCode + (getSearchCandidateListType() == null ? 0 : getSearchCandidateListType().hashCode());
		return hashCode;
	}
	
}
