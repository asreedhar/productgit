package biz.firstlook.module.market.search;

import biz.firstlook.commons.util.Functions;

public abstract class AbstractSearchCandidateCriteriaAnnotation implements SearchCandidateCriteriaAnnotation {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof SearchCandidateCriteriaAnnotation) == false)
			return false;
		SearchCandidateCriteriaAnnotation a = (SearchCandidateCriteriaAnnotation) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getName(), a.getName());
		return lEquals;
	}
	
	public int hashCode() {
		return Functions.nullSafeHashCode(getName());
	}
	
}
