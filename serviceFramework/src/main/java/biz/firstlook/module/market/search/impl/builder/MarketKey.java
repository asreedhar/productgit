package biz.firstlook.module.market.search.impl.builder;

import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.impl.AbstractMarket;

public class MarketKey extends AbstractMarket {

	Integer marketID;
	String  name;
	Integer accessGroupTypeID;

	public MarketKey() {
		init(null, null, null);
	}
	
	public MarketKey(Integer marketID, String name, Integer accessGroupTypeID) {
		init(marketID, name, accessGroupTypeID);
	}
	
	public void init(Integer marketID, String name, Integer accessGroupTypeID) {
		this.marketID = marketID;
		this.name = name;
		this.accessGroupTypeID = accessGroupTypeID;
	}

	public MarketType getMarketType() {
		return MarketBuilder.getMarketType(accessGroupTypeID);
	}

	public String getName() {
		return name;
	}

	public MarketAdministrator getMarketAdministrator() {
		return MarketBuilder.getMarketAdministrator(accessGroupTypeID);
	}

}
