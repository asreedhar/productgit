package biz.firstlook.module.market.search;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import biz.firstlook.commons.util.Functions;

public class SearchCandidateCriteriaAnnotationEnum extends AbstractSearchCandidateCriteriaAnnotation implements Serializable, SearchCandidateCriteriaAnnotation {
	
	private static final long serialVersionUID = 5779861758760631071L;
	
	public static final SearchCandidateCriteriaAnnotation OPTIMAL = new SearchCandidateCriteriaAnnotationEnum("Optimal Plan");
	public static final SearchCandidateCriteriaAnnotation BUYING  = new SearchCandidateCriteriaAnnotationEnum("Buying Plan");
	public static final SearchCandidateCriteriaAnnotation HOTLIST = new SearchCandidateCriteriaAnnotationEnum("Hot List");
	public static final SearchCandidateCriteriaAnnotation ONETIME = new SearchCandidateCriteriaAnnotationEnum("One Time");
	
	public static final Set<SearchCandidateCriteriaAnnotation> CIA;
	public static final Set<SearchCandidateCriteriaAnnotation> USER;
	public static final Set<SearchCandidateCriteriaAnnotation> ALL;
	static {
		Set<SearchCandidateCriteriaAnnotation> cia = new HashSet<SearchCandidateCriteriaAnnotation>();
		cia.add(OPTIMAL);
		cia.add(BUYING);
		CIA = Collections.unmodifiableSet(cia);
		Set<SearchCandidateCriteriaAnnotation> user = new HashSet<SearchCandidateCriteriaAnnotation>();
		user.add(OPTIMAL);
		user.add(BUYING);
		USER = Collections.unmodifiableSet(user);
		Set<SearchCandidateCriteriaAnnotation> all = new HashSet<SearchCandidateCriteriaAnnotation>();
		all.addAll(cia);
		all.addAll(user);
		ALL = Collections.unmodifiableSet(all);
	}
	
	private String name;
	
	SearchCandidateCriteriaAnnotationEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	private Object readResolve() {
		if (Functions.nullSafeEquals(OPTIMAL.getName(), name)) {
			return OPTIMAL;
		}
		else if (Functions.nullSafeEquals(BUYING.getName(), name)) {
			return BUYING;
		}
		else if (Functions.nullSafeEquals(HOTLIST.getName(), name)) {
			return HOTLIST;
		}
		else if (Functions.nullSafeEquals(ONETIME.getName(), name)) {
			return ONETIME;
		}
		return this;
	}
}
