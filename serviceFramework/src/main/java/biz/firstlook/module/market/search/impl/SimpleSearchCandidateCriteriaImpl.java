package biz.firstlook.module.market.search.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

/**
 * Simple implementation of a <code>SearchCandidateCriteria</code> instance.  This
 * class is not currently in use. If it is used the semantics of Serialization will
 * have to be considered.
 * 
 * @author swenmouth
 */
public class SimpleSearchCandidateCriteriaImpl extends AbstractSearchCandidateCriteria implements Serializable {

	private static final long serialVersionUID = 3320233942973076116L;
	
	private Date expires;
	private ModelYear modelYear;
	private Set<SearchCandidateCriteriaAnnotation> annotations;
	
	public SimpleSearchCandidateCriteriaImpl(Date expires, ModelYear modelYear, Set<SearchCandidateCriteriaAnnotation> annotations) {
		super();
		this.expires = expires;
		this.modelYear = modelYear;
		this.annotations = annotations;
	}
	
	public Date getExpires() {
		return expires;
	}
	
	public ModelYear getModelYear() {
		return modelYear;
	}

	public Set<SearchCandidateCriteriaAnnotation> getAnnotations() {
		return annotations;
	}
	
}
