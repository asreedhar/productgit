package biz.firstlook.module.market.search.impl.adapter;

import java.io.Serializable;
import java.util.Set;

import biz.firstlook.commons.collections.SetAdapter;
import biz.firstlook.module.market.search.SearchCandidateAnnotation;
import biz.firstlook.module.market.search.impl.entity.CIACategory;

/**
 * Adapts the type of a <code>Set</code> of {@link CIACategory}s to appear as
 * {@link SearchCandidateAnnotation}s.  This class implements the Adapter pattern.
 * 
 * @author swenmouth
 */
public class CIACategorySetAdapter extends SetAdapter<SearchCandidateAnnotation,CIACategory> implements Serializable {

	private static final long serialVersionUID = 1458893232407201289L;

	public CIACategorySetAdapter(Set<CIACategory> categories) {
		super(categories);
	}

	public boolean add(SearchCandidateAnnotation element) {
		if (!(element instanceof CIACategoryAdapter)) {
			return false;
		}
		return set.add(((CIACategoryAdapter) element).getCIACategory());
	}

	public SearchCandidateAnnotation adaptItem(CIACategory category) {
		return new CIACategoryAdapter(category);
	}
	
}
