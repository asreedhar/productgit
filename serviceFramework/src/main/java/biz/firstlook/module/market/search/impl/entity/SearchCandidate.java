package biz.firstlook.module.market.search.impl.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@Entity
@Table(name="SearchCandidate")
public class SearchCandidate implements Serializable {

	private static final long serialVersionUID = 9037254886142852431L;
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer searchCandidateID;
	private Integer modelID;
	private boolean suppress;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="searchCandidateID",nullable=false)
	@org.hibernate.annotations.Cascade(value={org.hibernate.annotations.CascadeType.ALL,org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
	private List<SearchCandidateOption> searchCandidateOptions;

	@ManyToMany(
		targetEntity=CIACategory.class,
		fetch=FetchType.EAGER
	)
	@JoinTable(
		name="SearchCandidateCIACategoryAnnotation",
		joinColumns={@JoinColumn(name="SearchCandidateID")},
		inverseJoinColumns={@JoinColumn(name="CIACategoryID")}
	)
	private Set<CIACategory> searchCandidateAnnotations;
	
	public SearchCandidate() {
		super();
	}

	public SearchCandidate(Integer searchCandidateID, Integer modelID, boolean suppress, List<SearchCandidateOption> searchCandidateOptions, Set<CIACategory> searchCandidateAnnotations) {
		super();
		this.searchCandidateID = searchCandidateID;
		this.modelID = modelID;
		this.suppress = suppress;
		this.searchCandidateOptions = searchCandidateOptions;
		this.searchCandidateAnnotations = searchCandidateAnnotations;
	}

	public Integer getModelID() {
		return modelID;
	}
	public void setModelID(Integer modelID) {
		this.modelID = modelID;
	}
	
	public boolean getSuppress() {
		return suppress;
	}
	public void setSuppress(boolean hidden) {
		this.suppress = hidden;
	}
	
	public Integer getSearchCandidateID() {
		return searchCandidateID;
	}
	public void setSearchCandidateID(Integer searchCandidateID) {
		this.searchCandidateID = searchCandidateID;
	}
	
	public List<SearchCandidateOption> getSearchCandidateOptions() {
		return searchCandidateOptions;
	}
	public void setSearchCandidateOptions(List<SearchCandidateOption> searchCandidateOptions) {
		this.searchCandidateOptions = searchCandidateOptions;
	}

	public Set<CIACategory> getSearchCandidateAnnotations() {
		return searchCandidateAnnotations;
	}
	public void setSearchCandidateAnnotations(Set<CIACategory> searchCandidateAnnotations) {
		this.searchCandidateAnnotations = searchCandidateAnnotations;
	}

	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof SearchCandidate) {
			SearchCandidate s = (SearchCandidate) obj;
			return Functions.nullSafeEquals(getModelID(), s.getModelID());
		}
		return false;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getModelID());
		return hashCode;
	}
}
