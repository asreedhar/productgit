package biz.firstlook.module.market.search.impl;

import java.io.Serializable;

import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;

/**
 * Simple (Java Bean) implementation of the <code>Market</code> interface.
 * 
 * @author swenmouth
 */
public class SimpleMarketImpl extends AbstractMarket implements Serializable, Market {
	private static final long serialVersionUID = -7356756162732549806L;
	private Integer marketID;
	private String name;
	private MarketType marketType;
	private MarketAdministrator marketAdministrator;
	public SimpleMarketImpl(Integer marketID, String name, MarketType marketType, MarketAdministrator marketAdministrator) {
		this.marketID = marketID;
		this.name = name;
		this.marketType = marketType;
		this.marketAdministrator = marketAdministrator;
	}
	public Integer getMarketID() {
		return marketID;
	}
	public String getName() {
		return name;
	}
	public MarketType getMarketType() {
		return marketType;
	}
	public MarketAdministrator getMarketAdministrator() {
		return marketAdministrator;
	}
	private Object readResolve() {
		return MarketRegistry.getInstance().lookupMarket(marketID, name, marketAdministrator.getAccessGroupTypeID());
	}
}
