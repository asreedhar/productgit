package biz.firstlook.module.market.search.impl;

import java.io.Serializable;
import java.util.Date;

/**
 * Simple (Java Bean) implementation of the <code>SearchResultSummary</code> interface.
 * 
 * @author swenmouth
 */
public class SimpleSearchSummaryImpl extends AbstractSearchResultSummary implements Serializable {

	private static final long serialVersionUID = -5609285307548126385L;
	
	private Date auctionDate;
	private Double distance;
	private String location;
	private String auctionName;
	private Double score;
	private String title;
	private Integer vehiclesMatched;
	private Integer candidatesMatched;
	
	public SimpleSearchSummaryImpl() {
		super();
	}
	
	public SimpleSearchSummaryImpl(Date auctionDate, Double distance, String location, String auctionName, Double score, String title, Integer vehiclesMatched, Integer candidatesMatched) {
		super();
		this.auctionDate = auctionDate;
		this.distance = distance;
		this.location = location;
		this.auctionName = auctionName;
		this.score = score;
		this.title = title;
		this.vehiclesMatched = vehiclesMatched;
		this.candidatesMatched = candidatesMatched;
	}

	public Date getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(Date auctionDate) {
		this.auctionDate = auctionDate;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAuctionName() {
		return auctionName;
	}

	public void setAuctionName(String auctionName) {
		this.auctionName = auctionName;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getVehiclesMatched() {
		return vehiclesMatched;
	}

	public void setVehiclesMatched(Integer vehiclesMatched) {
		this.vehiclesMatched = vehiclesMatched;
	}

	public Integer getCandidatesMatched() {
		return candidatesMatched;
	}

	public void setCandidatesMatched(Integer candidatesMatched) {
		this.candidatesMatched = candidatesMatched;
	}

}
