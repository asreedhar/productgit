package biz.firstlook.module.market.search;

import java.util.Collection;
import java.util.Set;

import biz.firstlook.module.purchasing.vehicle.Model;

/**
 * <p>A search candidate represents a <code>Model</code> the client either wants, needs
 * or does not want. The candidate is optionally restricted to a set of
 * <code>ModelYear</code>s.  If the <code>ModelYear</code> set is empty then the search
 * candidate is for all years.</p>
 * @author swenmouth
 */
public interface SearchCandidate {
	public Model getModel();
	public Collection<SearchCandidateCriteria> getCriteria();
	public Set<SearchCandidateAnnotation> getAnnotations();
}
