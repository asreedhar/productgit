package biz.firstlook.module.market.search.presentation.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import biz.firstlook.module.inventory.StockLevel;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.presentation.DecoratedModelYear;
import biz.firstlook.module.purchasing.vehicle.AbstractModelYear;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

public class DecoratedModelYearImpl extends AbstractModelYear implements
		Serializable, DecoratedModelYear {
	
	private static final long serialVersionUID = 1875661747943314231L;
	
	private ModelYear modelYear;
	private StockLevel stockLevel;
	private Date expires;
	private Set<SearchCandidateCriteriaAnnotation> annotations;
	
	public DecoratedModelYearImpl(ModelYear modelYear, StockLevel stockLevel, Date expires, Set<SearchCandidateCriteriaAnnotation> annotations) {
		super();
		this.modelYear = modelYear;
		this.stockLevel = stockLevel;
		this.expires = expires;
		this.annotations = annotations;
	}

	public StockLevel getStockLevel() {
		return stockLevel;
	}

	public Integer getModelYear() {
		return modelYear.getModelYear();
	}

	public Set<SearchCandidateCriteriaAnnotation> getAnnotations() {
		return annotations;
	}

	public Date getExpires() {
		return expires;
	}

}
