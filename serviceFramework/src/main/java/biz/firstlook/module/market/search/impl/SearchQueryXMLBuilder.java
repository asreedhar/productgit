package biz.firstlook.module.market.search.impl;

import java.io.StringWriter;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.adapter.MarketToMemberMarketAdapter;
import biz.firstlook.module.market.search.impl.entity.MemberMarket;

/**
 * Converts the combination of a <code>SearchContext</code> and a <code>SearchCandidate</code>
 * into an XML representation that is accepted by our stored procedures.
 * 
 * @author swenmouth
 */
public class SearchQueryXMLBuilder {

	private static final transient Log log = LogFactory.getLog( SearchQueryXMLBuilder.class );
	
	public String build(SearchContext ctx, Collection<SearchCandidate> candidates) {
		long start = System.currentTimeMillis();
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.newDocument();
			Element root = doc.createElement("Search");
			Element businessUnitElement = doc.createElement("BusinessUnit");
			businessUnitElement.setAttribute("ID", ctx.getMember().getCurrentBusinessUnit().getBusinessUnitID().toString());
			if (ctx.getMaxDistanceFromBusinessUnit() != null && ctx.getMaxDistanceFromBusinessUnit() > 0) {
				businessUnitElement.setAttribute(
						"MaxDistanceFromDealer",
						ctx.getMaxDistanceFromBusinessUnit().toString());
			}
			Integer maxVehicleMileage = ctx.getMaxMileage();
			Integer minVehicleMileage = ctx.getMinMileage();
			
			//Make sure max mileage is the higher value and min the lower in case a user decides to be cute.
			//Easier to do it here rather than disabling drop downs or something.
			if (minVehicleMileage != null && maxVehicleMileage != null ) {
			    if (minVehicleMileage > maxVehicleMileage) {
			        int temp = maxVehicleMileage;
			        maxVehicleMileage = minVehicleMileage;
			        minVehicleMileage = temp;
			    }
			}
			
			if (maxVehicleMileage != null ){
			    
			    businessUnitElement.setAttribute("MaxVehicleMileage", maxVehicleMileage.toString());
			}
			if (minVehicleMileage != null ){
			    businessUnitElement.setAttribute("MinVehicleMileage", minVehicleMileage.toString());
			}
			root.appendChild(businessUnitElement);
			addAccessGroups(doc, root, ctx);
			addAuctions(doc, root, ctx);
			addFilter(doc, root, candidates);
			StringWriter w = new StringWriter();
			TransformerFactory.newInstance().newTransformer().transform(
					new DOMSource(root),
					new StreamResult(w));
			if( log.isDebugEnabled() ) {
				log.debug( w.toString() );
				log.debug( "SearchQueryXMLBuilder.build: " + (System.currentTimeMillis() - start) + " ms" );
			}
			return w.toString();
		}
		catch (DOMException e) {
			throw new RuntimeException(e);
		}
		catch (TransformerConfigurationException e) {
			throw new RuntimeException(e);
		}
		catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
		catch (TransformerException e) {
			throw new RuntimeException(e);
		}
		catch (TransformerFactoryConfigurationError e) {
			throw new RuntimeException(e);
		}
	}
    private void addAccessGroups(Document doc, Node root, SearchContext ctx) {
		Element accessGroups = doc.createElement("AccessGroups");
		for (Market m : ctx.getMarkets()) {
			while (m instanceof MemberMarketWrapper) {
				m = ((MemberMarketWrapper) m).getMemberMarket();
			}
			if (m instanceof biz.firstlook.module.market.search.impl.entity.Market) {
				addAccessGroup(doc, accessGroups, ((biz.firstlook.module.market.search.impl.entity.Market)m).getMarketID());
			}
			else if (m instanceof biz.firstlook.module.market.search.impl.adapter.MemberMarketGroupAdapter) {
				for (MemberMarket mm : (biz.firstlook.module.market.search.impl.adapter.MemberMarketGroupAdapter) m) {
					addAccessGroup(doc, accessGroups, mm.getMarket().getMarketID());
				}
			}
		}
		root.appendChild(accessGroups);
	}
	private void addAccessGroup(Document doc, Node root, Integer marketID) {
		Element accessGroup = doc.createElement("AccessGroup");
		accessGroup.setAttribute("ID", marketID.toString());
		root.appendChild(accessGroup);
	}
	private void addAuctions(Document doc, Node root, SearchContext ctx) {
		Element auctions = doc.createElement("Auction");
		for (Market m : ctx.getMarkets()) {
			while (m instanceof MemberMarketWrapper) {
				m = ((MemberMarketWrapper) m).getMemberMarket();
			}
			if (m instanceof MarketToMemberMarketAdapter) {
				m = ((MarketToMemberMarketAdapter) m).getMarket();
				if (m instanceof MarketSummary) {
					MarketSummary ms = (MarketSummary) m;
					if (ms instanceof SimpleMarketSummaryImpl) {
						final SimpleMarketSummaryImpl sms = (SimpleMarketSummaryImpl)ms;
						
						if (sms.getLocation() != null) {
							Element auction = doc.createElement("Location");
							auction.setAttribute("ID", (sms).getLocationID().toString());
							auctions.appendChild(auction);
						}
						//Add auction date to xml
						if (sms.getAuctionDate()!= null) {
							Element auction = doc.createElement("DateHeld");
							auction.setAttribute("ID", (sms).getAuctionDate().toString());
							auctions.appendChild(auction);
						}
//						if (sms.getSaleID() != null) {
//							Element auction = doc.createElement("Sale");
//							auction.setAttribute("ID", (sms).getSaleID().toString());
//							auctions.appendChild(auction);
//						}
					}
				}
			}
		}
		root.appendChild(auctions);
	}
	private void addFilter(Document doc, Node root, Collection<SearchCandidate> candidates) {
		Element filter = doc.createElement("Filter");
		for (SearchCandidate s : candidates) {
			Element model = doc.createElement("MakeModelGrouping");
			model.setAttribute("ModelID", s.getModel().getId().toString());
			Element criteria = null;
			for (SearchCandidateCriteria c : s.getCriteria()) {
				if (c.getModelYear().getModelYear() != null) {
					if (criteria == null) {
						criteria = doc.createElement("Criteria");
						criteria.setAttribute("Type", "Year");
					}
					Element value = doc.createElement("Value");
					value.appendChild(doc.createTextNode(c.getModelYear().getModelYear().toString()));
					criteria.appendChild(value);
				}
			}
			if (criteria != null) {
				model.appendChild(criteria);
			}
			filter.appendChild(model);
		}
		root.appendChild(filter);
	}
}
