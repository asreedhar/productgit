package biz.firstlook.module.market.search.impl;

import biz.firstlook.commons.flyweight.FlyweightFactory;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.impl.builder.MarketBuilder;
import biz.firstlook.module.market.search.impl.builder.MarketKey;

/**
 * Implementation of the <code>MarketRegistry</code> interface that is backed
 * by a Spring injected <code>FlyweightFactory</code>.
 * 
 * @author swenmouth
 */
public class MarketRegistryImpl extends MarketRegistry {

	public MarketRegistryImpl() {
		super();
		MarketRegistryImpl.setInstance(this);
	}

	@Override
	public Market lookupMarket(Integer marketID, String name, Integer accessGroupTypeID) {
		return getMarketFlyweightFactory().getFlyweight(new MarketKey(marketID, name, accessGroupTypeID));
	}

	private FlyweightFactory<Market,MarketKey,MarketBuilder> marketFlyweightFactory;

	public FlyweightFactory<Market, MarketKey, MarketBuilder> getMarketFlyweightFactory() {
		return marketFlyweightFactory;
	}
	
	public void setMarketFlyweightFactory(FlyweightFactory<Market, MarketKey, MarketBuilder> marketFlyweightFactory) {
		this.marketFlyweightFactory = marketFlyweightFactory;
	}
	
}
