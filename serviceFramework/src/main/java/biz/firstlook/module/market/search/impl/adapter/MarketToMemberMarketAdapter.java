package biz.firstlook.module.market.search.impl.adapter;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;

/**
 * Wraps a {@link Market} and makes it appear as a {@link MemberMarket}.
 *  
 * @author swenmouth
 */
public class MarketToMemberMarketAdapter implements MemberMarket {
	Market  market;
	Integer maxDistanceFromDealer;
    Integer maxMileage;
    Integer minMileage;
	Integer timePeriod;
	boolean suppress;
	boolean exactMatches;
	public MarketToMemberMarketAdapter(Market market, Integer maxDistanceFromDealer, Integer timePeriod, boolean exactMatches, Integer maxMileage, Integer minMileage) {
		this.market   = market;
		this.suppress = false;
		this.maxDistanceFromDealer = maxDistanceFromDealer;
		this.timePeriod = timePeriod;
		this.exactMatches = exactMatches;
        this.maxMileage = maxMileage;
        this.minMileage = minMileage;
	}
	public Integer getMaxDistanceFromDealer() {
		return maxDistanceFromDealer;
	}
	public Integer getTimePeriod() {
		return timePeriod;
	}
	public boolean getExactMatches() {
		return exactMatches;
	}
	public void setExactMatches(boolean exactMatches) {
		this.exactMatches = exactMatches;
	}
	public boolean getSuppress() {
		return suppress;
	}
	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
		this.maxDistanceFromDealer = maxDistanceFromDealer;
	}
    
	public void setTimePeriod(Integer timePeriod) {
		this.timePeriod = timePeriod;
	}
	public void setSuppress(boolean suppress) {
		this.suppress = suppress;
	}
	public MarketType getMarketType() {
		return market.getMarketType();
	}
	public MarketAdministrator getMarketAdministrator() {
		return market.getMarketAdministrator();
	}
	public String getName() {
		return market.getName();
	}
	public Market getMarket() {
		return this.market;
	}
	public int compareTo(MemberMarket memberMarket) {
		int c = 0;
		c = Functions.nullSafeCompareTo(market.getName(), memberMarket.getName());
		return c;
	}
    public Integer getMaxMileage() {
        return maxMileage;
    }
    public void setMaxMileage(Integer maxMileage) {
        this.maxMileage = maxMileage;
    }
    public Integer getMinMileage() {
        return minMileage;
    }
    public void setMinMileage(Integer minMileage) {
        this.minMileage = minMileage;
    }
}

