package biz.firstlook.module.market.search.presentation;

import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.purchasing.vehicle.Model;

public interface DecoratedModel extends Model {
	public SearchCandidate getSearchCandidate();
}
