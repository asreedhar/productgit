package biz.firstlook.module.market.search.impl;

import biz.firstlook.module.market.search.MemberMarket;

/**
 * A <code>MemberMarketWrapper</code> is an wrapper for a <code>MemberMarket</code>
 * instance and provides access to it "unwrapped".
 * 
 * @author swenmouth
 */
public interface MemberMarketWrapper {
	public MemberMarket getMemberMarket();
}
