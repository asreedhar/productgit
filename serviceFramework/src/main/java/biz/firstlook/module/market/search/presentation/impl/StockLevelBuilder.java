package biz.firstlook.module.market.search.presentation.impl;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.inventory.StockLevel;

public class StockLevelBuilder implements FlyweightBuilder<StockLevel,SimpleStockLevelImpl> {

	public StockLevel newFlyweight(SimpleStockLevelImpl key) {
		return new SimpleStockLevelImpl(
				key.businessUnit,
				key.modelGroup,
				key.modelYear,
				key.optimalUnits,
				key.unitsInStock);
	}

}
