package biz.firstlook.module.market.search.impl;

import java.io.Serializable;
import java.util.Date;

import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MarketType;

/**
 * Simple (Java Bean) implementation of the <code>SearchSummary</code> interface.
 * 
 * @author swenmouth
 */
public class SimpleMarketSummaryImpl extends SimpleSearchSummaryImpl implements Serializable, MarketSummary {
	private static final long serialVersionUID = 5595820968206471675L;
	Market market;
	Integer saleID;
	Integer locationID;
	public SimpleMarketSummaryImpl(Market market, Date auctionDate, Double distance, String location, String auctionName, Double score, String title, Integer vehiclesMatched, Integer candidatesMatched, Integer saleID, Integer locationID) {
		super(auctionDate, distance, location, auctionName, score, title, vehiclesMatched, candidatesMatched);
		this.market = market;
		this.saleID = saleID;
		this.locationID=locationID;
	}
	public String getName() {
		return market.getName();
	}
	public MarketType getMarketType() {
		return market.getMarketType();
	}
	public Integer getSaleID() {
		return saleID;
	}
	public MarketAdministrator getMarketAdministrator() {
		return market.getMarketAdministrator();
	}
	public Integer getLocationID() {
		return locationID;
	}
}
