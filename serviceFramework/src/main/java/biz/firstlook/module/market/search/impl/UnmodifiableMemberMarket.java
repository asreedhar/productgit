package biz.firstlook.module.market.search.impl;

import java.io.Serializable;

import biz.firstlook.module.market.search.MemberMarket;

/**
 * Wrapper class for an instance of <code>MemberMarket</code> that forbids a client from
 * comitting changes to the instance when {@link #commit} is invoked.  More expressly,
 * this class stops changes to a <code>MemberMarket</code> being persisted.
 * 
 * @author swenmouth
 */
public class UnmodifiableMemberMarket extends ModifiableMemberMarket implements Comparable<MemberMarket>, Serializable, MemberMarket, MemberMarketWrapper {
	
	private static final long serialVersionUID = 925556452350844422L;

	public UnmodifiableMemberMarket(MemberMarket memberMarket) {
		super(memberMarket);
	}

	public void commit() {
		throw new UnsupportedOperationException("This class is unmodifiable!");
	}
	
}