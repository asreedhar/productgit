package biz.firstlook.module.market.search;

public enum MarketAdministrator {
	
	MANUFACTURER(1), ENTERPRISE(2), ATC_OPEN(3), GMAC(4), TFS_LFS(5), IN_GROUP(6), LIVE(7), OVE(8), DRIVE_AWAY(9), AutoSales(10);
	
	private int accessGroupTypeID;
	
	MarketAdministrator(int accessGroupTypeID) {
		this.accessGroupTypeID = accessGroupTypeID;
	}
	
	public int getAccessGroupTypeID() {
		return accessGroupTypeID;
	}
	
}
