package biz.firstlook.module.market.search.presentation.impl;

import java.io.Serializable;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.inventory.AbstractStockLevelData;
import biz.firstlook.module.inventory.StockLevel;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

public class SimpleStockLevelImpl extends AbstractStockLevelData implements Serializable, StockLevel {
	
	private static final long serialVersionUID = -3414786018209185117L;
	
	BusinessUnit businessUnit;
	ModelGroup modelGroup;
	ModelYear modelYear;
    Integer optimalUnits;
	Integer unitsInStock;
	
	public SimpleStockLevelImpl(BusinessUnit businessUnit, ModelGroup modelGroup, ModelYear modelYear, Integer optimalUnits, Integer unitsInStock) {
		this.businessUnit = businessUnit;
		this.modelGroup = modelGroup;
		this.modelYear = modelYear;
		this.optimalUnits = optimalUnits;
		this.unitsInStock = unitsInStock;
	}

	public void init(BusinessUnit businessUnit, ModelGroup modelGroup, ModelYear modelYear, Integer optimalUnits, Integer unitsInStock) {
		this.businessUnit = businessUnit;
		this.modelGroup = modelGroup;
		this.modelYear = modelYear;
		this.optimalUnits = optimalUnits;
		this.unitsInStock = unitsInStock;
	}

	public BusinessUnit getBusinessUnit() {
		return businessUnit;
	}

	public Integer getUnitsInStock() {
		return unitsInStock;
	}

	public ModelGroup getModelGroup() {
		return modelGroup;
	}

	public ModelYear getModelYear() {
		return modelYear;
	}

	public Integer getOptimalUnits() {
		return optimalUnits;
	}
    
    public void setOptimalUnits(Integer optimalUnits) {
        this.optimalUnits = optimalUnits;
    }
}
