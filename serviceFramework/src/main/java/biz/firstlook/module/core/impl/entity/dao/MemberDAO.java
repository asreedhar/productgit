package biz.firstlook.module.core.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.core.impl.entity.BusinessUnit;
import biz.firstlook.module.core.impl.entity.JobTitle;
import biz.firstlook.module.core.impl.entity.Member;

public class MemberDAO extends GenericHibernate<Member,Integer> {

	public MemberDAO() {
		super(Member.class);
	}

	@SuppressWarnings("unchecked")
	public Member findByLogin(String login) {
		// This joins against credential and you get many rows for one member. WTH?
		// List<Member> members = findByCriteria(Restrictions.eq("login", login));
		StringBuilder hql = new StringBuilder();
		hql.append(" from");
		hql.append(" biz.firstlook.module.core.impl.entity.Member m");
		hql.append(" where");
		hql.append(" m.login = ?");
		List<Member> members = (List<Member>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { login } );
		Member member = null;
		if (!members.isEmpty()) {
			if (members.size() != 1) {
				throw new IllegalStateException("Non-unique login: " + login);
			}
			else {
				member = members.get(0);
			}
		}
		return member;
	}
	
	@SuppressWarnings("unchecked")
	public List<Member> findByDealerAndJobTitle(BusinessUnit dealer, JobTitle jobTitle) {
		StringBuilder hql = new StringBuilder();
		hql.append(" from biz.firstlook.module.core.impl.entity.Member m");
		hql.append(" where");
		hql.append(" m.jobTitle.name = ?");
		hql.append(" and m.businessUnits.id = ?");
		return (List<Member>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] {jobTitle.getName(), dealer.getBusinessUnitID()} );
	}
}
