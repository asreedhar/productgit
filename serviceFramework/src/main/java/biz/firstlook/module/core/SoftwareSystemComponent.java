package biz.firstlook.module.core;

public interface SoftwareSystemComponent {

	public String getName();
	
	public String getToken();
	
}
