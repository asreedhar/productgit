package biz.firstlook.module.core.impl.entity.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.core.impl.entity.ApplicationEvent;
import biz.firstlook.module.core.impl.entity.Member;

public class ApplicationEventDAO extends
		GenericHibernate<ApplicationEvent, Integer> {

	public ApplicationEventDAO() {
		super(ApplicationEvent.class);
	}

	@SuppressWarnings("unchecked")
	public void setLoginStatus(Member member) {
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today = formatter.format(Calendar.getInstance().getTime());
		today += "T00:00:00";
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from");
		hql.append("   biz.firstlook.module.core.impl.entity.ApplicationEvent e");
		hql.append(" where");
		hql.append("   e.memberID = ? and" );
		hql.append("   createTimestamp > '" + today + "'");
		hql.append(" order by e.createTimestamp desc");
		List<ApplicationEvent> events = (List<ApplicationEvent>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { member.getMemberID() } );
		
		if (!events.isEmpty()) 
		{
			ApplicationEvent event = events.get(0);
			
			if( event.getEventType().equals(1) )
				member.setLoggedIn(true);
		}
	}
	
	public void setLoginStatus(List<Member> members)
	{
		for( Member member:members)
		{
			setLoginStatus(member);
		}
	}
}
