package biz.firstlook.module.core.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.core.impl.entity.SoftwareSystemComponent;

public class SoftwareSystemComponentDAO  extends GenericHibernate<SoftwareSystemComponent, Integer> {

	public SoftwareSystemComponentDAO() {
		super(SoftwareSystemComponent.class);
	}
	
	@SuppressWarnings("unchecked")
	public SoftwareSystemComponent findByToken(String token) {
		StringBuffer hql = new StringBuffer();
		hql.append(" select c");
		hql.append(" from");
		hql.append(" biz.firstlook.module.core.impl.entity.SoftwareSystemComponent c");
		hql.append(" where");
		hql.append(" c.token = ?");
		List<SoftwareSystemComponent> components = (List<SoftwareSystemComponent>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { token } );
		SoftwareSystemComponent component = null;
		if (!components.isEmpty()) {
			if (components.size() != 1) {
				throw new IllegalStateException(String.format("Non-unique component: {0} {1} rows", token, components.size()));
			}
			else {
				component = components.get(0);
			}
		}
		return component;
	}
	
}
