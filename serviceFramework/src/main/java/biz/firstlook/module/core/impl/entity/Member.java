package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Member")
public class Member implements Serializable, Comparable<Member> {

	private static final long serialVersionUID = -5153552560943575608L;
	
	@Transient
	private boolean loggedIn; 

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer memberID;
	
	@ManyToMany(targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnit.class)
	@JoinTable(
			name="MemberAccess",
			joinColumns=@JoinColumn(name="MemberID", referencedColumnName="MemberID"),
			inverseJoinColumns=@JoinColumn(name="BusinessUnitID", referencedColumnName="BusinessUnitID")
    )
	private Collection<BusinessUnit> businessUnits;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="memberID",nullable=false)
	private List<MemberCredential> credentials;
	
	@Basic(fetch = FetchType.EAGER)
	@Column(name="FirstName")
	private String firstName;
	
	@Basic(fetch = FetchType.EAGER)
	@Column(name="LastName")
	private String lastName;

	@Basic(fetch = FetchType.EAGER)
	@Column(name="Login")
	private String login;

	@Basic(fetch = FetchType.EAGER)
	@Column(name="MemberType")
	private Integer memberType;

	@Basic(fetch = FetchType.EAGER)
	@Column(name="SearchHomePage")
	private String searchHomePage;
	
	@Column(name="InventoryOverviewSortOrderType")
	private String inventoryOverviewSortOrderType;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="jobTitleID")
	private JobTitle jobTitle;
	
	@Basic(fetch = FetchType.EAGER)
	@Column(name="EmailAddress")
	private String emailAddress;
	
	@Basic(fetch = FetchType.EAGER)
	@Column(name="MobilePhoneNumber")
	private String mobilePhoneNumber;
	
	public Member() {
		super();
	}
	
	public boolean getLoggedIn(){
		return this.loggedIn;
	}
	
	public void setLoggedIn(boolean loggedIn){
		this.loggedIn = loggedIn;
	}

	public Collection<BusinessUnit> getBusinessUnits() {
		return businessUnits;
	}

	public void setBusinessUnits(Collection<BusinessUnit> businessUnits) {
		this.businessUnits = businessUnits;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

	public Integer getMemberType() {
		return memberType;
	}
	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}

	public String getSearchHomePage() {
		return searchHomePage;
	}
	public void setSearchHomePage(String searchHomePage) {
		this.searchHomePage = searchHomePage;
	}

	public Integer getMemberID() {
		return memberID;
	}
	public void setMemberID(Integer memberID) {
		this.memberID = memberID;
	}

	public List<MemberCredential> getCredentials() {
		return credentials;
	}
	
	public void setCredentials( List< MemberCredential > credentials ) {
		this.credentials = credentials;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof Member) {
			Member b = (Member) obj;
			boolean lEquals = true;
			if (getMemberID() == null) {
				lEquals &= (b.getMemberID() == null);
			}
			else {
				lEquals &= getMemberID().equals(b.getMemberID());
			}
			return lEquals;
		}
		return false;
	}

	public String getInventoryOverviewSortOrderType() {
		return inventoryOverviewSortOrderType;
	}

	public void setInventoryOverviewSortOrderType(String inventoryOverviewSortOrderType) {
		this.inventoryOverviewSortOrderType = inventoryOverviewSortOrderType;
	}

	public JobTitle getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(JobTitle jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public int compareTo(Member m) {
		return(this.lastName.compareToIgnoreCase(m.lastName));
	}
}
