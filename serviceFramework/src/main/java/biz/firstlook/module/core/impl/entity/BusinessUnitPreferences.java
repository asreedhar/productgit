package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@org.hibernate.annotations.Entity(mutable=false)
@Table(name="DealerPreference")
public class BusinessUnitPreferences implements Serializable, biz.firstlook.module.core.BusinessUnitPreferences {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column(name="DealerPreferenceID")
	private Integer businessUnitPreferencesID;
	
	@OneToOne(optional=false,cascade=CascadeType.ALL,fetch=FetchType.EAGER,targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnit.class)
	@JoinColumn(name="BusinessUnitID",nullable=false)
	private BusinessUnit businessUnit;
	
	@Column(name="ATCEnabled")
	private Boolean ATCEnabled;
	
	@Column(name="TFSEnabled")
	private Boolean TFSEnabled;
	
	@Column(name="GMACEnabled")
	private Boolean GMACEnabled;
	
	@Column(name="OVEEnabled")
	private Boolean OVEEnabled;
	
	@Column(name="PurchasingDistanceFromDealer")
	private int maxDistanceFromDealer;
	
	@Column(name="LiveAuctionDistanceFromDealer")
	private int maxDistanceLiveFromDealer;
	
	public BusinessUnitPreferences() {
	}
	
	public Integer getBusinessUnitPreferencesID() {
		return businessUnitPreferencesID;
	}

	public void setBusinessUnitPreferencesID(Integer businessUnitPreferencesID) {
		this.businessUnitPreferencesID = businessUnitPreferencesID;
	}

	public BusinessUnit getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(BusinessUnit businessUnit) {
		this.businessUnit = businessUnit;
	}

	public Boolean isATCEnabled() {
		return ATCEnabled;
	}
	public void setATCEnabled(Boolean enabled) {
		ATCEnabled = enabled;
	}
	
	public Boolean isGMACEnabled() {
		return GMACEnabled;
	}
	public void setGMACEnabled(Boolean enabled) {
		GMACEnabled = enabled;
	}
	
	public Boolean isTFSEnabled() {
		return TFSEnabled;
	}
	public void setTFSEnabled(Boolean enabled) {
		TFSEnabled = enabled;
	}
	
	public Boolean isOVEEnabled() {
		return OVEEnabled;
	}
	public void setOVEEnabled(Boolean enabled) {
		OVEEnabled = enabled;
	}

	public int getOtherDistanceFromDealer()
	{
		return maxDistanceFromDealer;
	}

	public void setMaxDistanceFromDealer( int distanceFromDealer )
	{
		this.maxDistanceFromDealer = distanceFromDealer;
	}

	public int getLiveAuctionDistanceFromDealer() {
		return maxDistanceLiveFromDealer;
	}

	public void setMaxDistanceLiveFromDealer(int distanceFromDealer) {
		this.maxDistanceLiveFromDealer = distanceFromDealer;
	}

}
