package biz.firstlook.module.core;

public interface BusinessUnitPreferences {

	public Boolean isATCEnabled();
	
	public Boolean isGMACEnabled();
	
	public Boolean isTFSEnabled();
	
	public Boolean isOVEEnabled();
	
	public int getOtherDistanceFromDealer();
	
	public int getLiveAuctionDistanceFromDealer();

}
