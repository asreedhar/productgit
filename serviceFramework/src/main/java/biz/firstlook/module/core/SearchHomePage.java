package biz.firstlook.module.core;

public enum SearchHomePage
{
	MARKETPLACE( "MarketPlace", "MarketplaceSearchSummary.go" ), OPTIMAL_INVENTORY( "OptimalInventory", "OptimalInventorySearchSummary.go" );
	private String name;
	private String url;

	SearchHomePage( String name, String url )
	{
		this.name = name;
		this.url = url;
	}

	public String toString()
	{
		return name;
	}

	public String getURL()
	{
		return url;
	}
	
	public static final SearchHomePage toSearchHomePage( String name )
	{
		if ( name == null || name.length() == 0 )
		{
			return null;
		}
		if ( name.equals( MARKETPLACE.toString() ) )
		{
			return MARKETPLACE;
		}
		if ( name.equals( OPTIMAL_INVENTORY.toString() ) )
		{
			return OPTIMAL_INVENTORY;
		}
		return null;
	}
}
