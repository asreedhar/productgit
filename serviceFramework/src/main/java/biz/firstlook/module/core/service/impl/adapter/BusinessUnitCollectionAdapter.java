package biz.firstlook.module.core.service.impl.adapter;

import java.util.Collection;

import biz.firstlook.commons.collections.CollectionAdapter;
import biz.firstlook.module.core.BusinessUnit;

public class BusinessUnitCollectionAdapter
	extends CollectionAdapter<biz.firstlook.module.core.BusinessUnit, biz.firstlook.module.core.impl.entity.BusinessUnit>
	implements Collection<biz.firstlook.module.core.BusinessUnit> {

	public BusinessUnitCollectionAdapter(Collection<biz.firstlook.module.core.impl.entity.BusinessUnit> a) {
		super(a);
	}

	public BusinessUnit adaptItem(biz.firstlook.module.core.impl.entity.BusinessUnit a) {
		return new BusinessUnitAdapter(a);
	}
	
}
