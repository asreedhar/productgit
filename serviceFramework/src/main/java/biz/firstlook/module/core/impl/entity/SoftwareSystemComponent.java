package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@SuppressWarnings("serial")
@Entity
@Table(name="SoftwareSystemComponent")
public class SoftwareSystemComponent implements Serializable, biz.firstlook.module.core.SoftwareSystemComponent {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer softwareSystemComponentID;

	@Column(name="Name")
	private String name;
	
	@Column(name="Token")
	private String token;

	public SoftwareSystemComponent() {
		super();
	}
	
	public SoftwareSystemComponent(Integer softwareSystemComponentID, String name, String token) {
		super();
		this.softwareSystemComponentID = softwareSystemComponentID;
		this.name = name;
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSoftwareSystemComponentID() {
		return softwareSystemComponentID;
	}

	public void setSoftwareSystemComponentID(Integer softwareSystemComponentID) {
		this.softwareSystemComponentID = softwareSystemComponentID;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof SoftwareSystemComponent) {
			SoftwareSystemComponent b = (SoftwareSystemComponent) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getSoftwareSystemComponentID(), b.getSoftwareSystemComponentID());
			return lEquals;
		}
		return false;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getSoftwareSystemComponentID());
		return hashCode;
	}
}
