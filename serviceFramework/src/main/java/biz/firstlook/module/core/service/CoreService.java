package biz.firstlook.module.core.service;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.MemberPreferences;
import biz.firstlook.module.core.SoftwareSystemComponentState;

public interface CoreService {
	public BusinessUnit findBusinessUnitByID(Integer ID);
	public Member findMemberByID(Integer ID);
	public Member findMemberByLogin(String login);
	@Transactional(rollbackFor={RuntimeException.class,Exception.class})
	public void saveMemberPreferences(MemberPreferences memberPreferences);
	@Transactional(rollbackFor={RuntimeException.class,Exception.class})
	public SoftwareSystemComponentState findOrCreateSoftwareSystemComponentState(String login, String token);
}
