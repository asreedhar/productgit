package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Collection;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ApplicationEvent")

public class ApplicationEvent implements Serializable {
	
	private static final long serialVersionUID = -5153552560943575609L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer applicationEventID;
	
	@Basic(fetch = FetchType.EAGER)
	@Column(name="EventType")
	private Integer eventType;

	@Basic(fetch = FetchType.EAGER)
	@Column(name="MemberID")
	private Integer memberID;

	@Basic(fetch = FetchType.EAGER)
	@Column(name="CreateTimestamp")
	private Date createTimestamp;

	public Integer getApplicationEventID() {
		return applicationEventID;
	}

	public void setApplicationEventID(Integer applicationEventID) {
		this.applicationEventID = applicationEventID;
	}

	public Integer getEventType() {
		return eventType;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public Integer getMemberID() {
		return memberID;
	}

	public void setMemberID(Integer memberID) {
		this.memberID = memberID;
	}

	public Date getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(Date createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

}
