package biz.firstlook.module.core;

import java.util.Collection;

public interface Member {
	public Integer getMemberID();
	public String getFirstName();
	public String getLastName();
	public String getLogin();
	public Integer getMemberType();
	public Collection<BusinessUnit> getBusinessUnits();
	public BusinessUnit getCurrentBusinessUnit();
	public void setCurrentBusinessUnit(BusinessUnit currentBusinessUnit);
	public MemberPreferences getMemberPreferences();
}
