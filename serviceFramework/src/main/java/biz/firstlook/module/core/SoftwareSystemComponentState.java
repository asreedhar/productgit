package biz.firstlook.module.core;

public interface SoftwareSystemComponentState {

	public Integer getSoftwareSystemComponentStateID();
	
	public SoftwareSystemComponent getSoftwareSystemComponent();
	
	public Member getAuthorizedMember();
	
	public BusinessUnit getDealerGroup();
	
	public BusinessUnit getDealer();
	
	public Member getMember();
	
}
