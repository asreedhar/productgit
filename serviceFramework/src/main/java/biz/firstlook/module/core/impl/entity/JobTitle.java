/**
 * 
 */
package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="JobTitle")
public class JobTitle implements Serializable {
	
	private static final long serialVersionUID = -8871408877256619028L;
	
	//these should match the database.
	public static final JobTitle USED_CAR_MANAGER = new JobTitle(13, "Used Car Manager");
	public static final JobTitle GENERAL_MANAGER = new JobTitle(7, "General Manager");
	public static final JobTitle GENERAL_SALES_MANAGER = new JobTitle(8, "General Sales Manager");


	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="JobTitleID")
	private Integer id;
	
	@Basic(fetch=FetchType.EAGER)
	private String name;
	
	//for reflection purposes only.
	public JobTitle() {
		super();
	}
	
	private JobTitle(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final JobTitle other = (JobTitle) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}