package biz.firstlook.module.core.impl.entity.dao;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.core.impl.entity.BusinessUnit;

public class BusinessUnitDAO extends GenericHibernate<BusinessUnit, Integer> {

	public BusinessUnitDAO() {
		super(BusinessUnit.class);
	}

}
