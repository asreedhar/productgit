package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import biz.firstlook.commons.util.Functions;

@SuppressWarnings("serial")
@Entity
@Table(name="SoftwareSystemComponentState")
public class SoftwareSystemComponentState implements Serializable {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer softwareSystemComponentStateID;
	
	@ManyToOne(targetEntity=biz.firstlook.module.core.impl.entity.SoftwareSystemComponent.class,optional=false)
	@JoinColumn(name="SoftwareSystemComponentID",nullable=false)
	private SoftwareSystemComponent softwareSystemComponent;
	
	@ManyToOne(targetEntity=biz.firstlook.module.core.impl.entity.Member.class,optional=false)
	@JoinColumn(name="AuthorizedMemberID",referencedColumnName="MemberID", nullable=false)
	private Member authorizedMember;
	
	@ManyToOne(targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnit.class,optional=false)
	@JoinColumn(name="DealerGroupID",nullable=false)
	private BusinessUnit dealerGroup;
	
	@ManyToOne(targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnit.class,optional=false)
	@JoinColumn(name="DealerID",nullable=true)
	private BusinessUnit dealer;
	
	@ManyToOne(targetEntity=biz.firstlook.module.core.impl.entity.Member.class,optional=true)
	@JoinColumn(name="MemberID",nullable=true)
	private Member member;
	
	public SoftwareSystemComponentState() {
		super();
	}

	public SoftwareSystemComponentState(Integer softwareSystemComponentStateId, Member authorizedMember, BusinessUnit dealerGroup, BusinessUnit dealer, Member member) {
		super();
		this.softwareSystemComponentStateID = softwareSystemComponentStateId;
		this.authorizedMember = authorizedMember;
		this.dealerGroup = dealerGroup;
		this.dealer = dealer;
		this.member = member;
	}
	
	public Member getAuthorizedMember() {
		return authorizedMember;
	}

	public void setAuthorizedMember(Member authorizedMember) {
		this.authorizedMember = authorizedMember;
	}

	public BusinessUnit getDealer() {
		return dealer;
	}

	public void setDealer(BusinessUnit dealer) {
		this.dealer = dealer;
	}

	public BusinessUnit getDealerGroup() {
		return dealerGroup;
	}

	public void setDealerGroup(BusinessUnit dealerGroup) {
		this.dealerGroup = dealerGroup;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public SoftwareSystemComponent getSoftwareSystemComponent() {
		return softwareSystemComponent;
	}

	public void setSoftwareSystemComponent(SoftwareSystemComponent softwareSystemComponent) {
		this.softwareSystemComponent = softwareSystemComponent;
	}

	public Integer getSoftwareSystemComponentStateID() {
		return softwareSystemComponentStateID;
	}

	public void setSoftwareSystemComponentStateID(Integer softwareSystemComponentStateId) {
		this.softwareSystemComponentStateID = softwareSystemComponentStateId;
	}

	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof SoftwareSystemComponent) {
			SoftwareSystemComponentState b = (SoftwareSystemComponentState) obj;
			boolean lEquals = true;
			lEquals &= Functions.nullSafeEquals(getSoftwareSystemComponentStateID(), b.getSoftwareSystemComponentStateID());
			return lEquals;
		}
		return false;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getSoftwareSystemComponentStateID());
		return hashCode;
	}
}
