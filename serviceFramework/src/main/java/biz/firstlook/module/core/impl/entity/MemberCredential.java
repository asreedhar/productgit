package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="MemberCredential")
public class MemberCredential implements Serializable
{
@Id
@GeneratedValue( strategy = GenerationType.AUTO )
private Integer memberCredentialId;
private Integer credentialTypeId;
private String username;
private String password;

public Integer getMemberCredentialId()
{
	return memberCredentialId;
}
public void setMemberCredentialId( Integer credentialId )
{
	this.memberCredentialId = credentialId;
}
public Integer getCredentialTypeId()
{
	return credentialTypeId;
}
public void setCredentialTypeId( Integer credentialTypeId )
{
	this.credentialTypeId = credentialTypeId;
}
public String getPassword()
{
	return password;
}
public void setPassword( String password )
{
	this.password = password;
}
public String getUsername()
{
	return username;
}
public void setUsername( String username )
{
	this.username = username;
}
}
