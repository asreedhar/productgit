package biz.firstlook.module.core.service.impl.adapter;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.SoftwareSystemComponent;
import biz.firstlook.module.core.SoftwareSystemComponentState;

public class SoftwareSystemComponentStateAdapter implements SoftwareSystemComponentState {

	private biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState state;
	
	public SoftwareSystemComponentStateAdapter(biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState state) {
		this.state = state;
	}

	public Member getAuthorizedMember() {
		return new MemberAdapter(state.getAuthorizedMember());
	}

	public BusinessUnit getDealer() {
		return new BusinessUnitAdapter(state.getDealer());
	}

	public BusinessUnit getDealerGroup() {
		return new BusinessUnitAdapter(state.getDealerGroup());
	}

	public Member getMember() {
		return new MemberAdapter(state.getMember());
	}

	public SoftwareSystemComponent getSoftwareSystemComponent() {
		return state.getSoftwareSystemComponent();
	}

	public Integer getSoftwareSystemComponentStateID() {
		return state.getSoftwareSystemComponentStateID();
	}

}
