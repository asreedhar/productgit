package biz.firstlook.module.core;

import java.util.Collection;

public interface BusinessUnit {
	public Integer getBusinessUnitID();
	public String getName();
	public String getShortName();
	public String getPhoneNumber();
	public Collection<Member> getMembers();
	public BusinessUnit getParent();
	public Collection<BusinessUnit> getChildren();
	public BusinessUnitPreferences getBusinessUnitPreferences();
	public String getBusinessUnitCode();
}
