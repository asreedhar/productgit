package biz.firstlook.module.core.service.impl;

import org.hibernate.SessionFactory;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.MemberPreferences;
import biz.firstlook.module.core.SoftwareSystemComponentState;
import biz.firstlook.module.core.impl.entity.dao.BusinessUnitDAO;
import biz.firstlook.module.core.impl.entity.dao.MemberDAO;
import biz.firstlook.module.core.impl.entity.dao.SoftwareSystemComponentDAO;
import biz.firstlook.module.core.impl.entity.dao.SoftwareSystemComponentStateDAO;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.module.core.service.impl.adapter.BusinessUnitAdapter;
import biz.firstlook.module.core.service.impl.adapter.MemberAdapter;
import biz.firstlook.module.core.service.impl.adapter.SoftwareSystemComponentStateAdapter;

public class CoreServiceImpl implements CoreService {

	private SessionFactory sessionFactory;
	private BusinessUnitDAO businessUnitDAO;
	private MemberDAO memberDAO;
	private SoftwareSystemComponentDAO softwareSystemComponentDAO;
	private SoftwareSystemComponentStateDAO softwareSystemComponentStateDAO;
	
	public BusinessUnit findBusinessUnitByID(Integer ID) {
		return new BusinessUnitAdapter(getBusinessUnitDAO().findById(ID));
	}

	public Member findMemberByID(Integer ID) {
		return new MemberAdapter(getMemberDAO().findById(ID));
	}

	public Member findMemberByLogin(String login) {
		return new MemberAdapter(getMemberDAO().findByLogin(login));
	}

	public void saveMemberPreferences(MemberPreferences memberPreferences) {
		if (memberPreferences instanceof MemberAdapter.MemberPreferencesImpl) {
			getMemberDAO().makePersistent(((MemberAdapter.MemberPreferencesImpl)memberPreferences).getWrappedMember());
		}
	}

	public SoftwareSystemComponentState findOrCreateSoftwareSystemComponentState(String login, String token) {
		biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState state = getSoftwareSystemComponentStateDAO().findByLoginAndToken(login, token);
		if (state == null) {
			biz.firstlook.module.core.impl.entity.Member member = getMemberDAO().findByLogin(login);
			if (member.getMemberType().intValue() == 2) {
				boolean allow = false;
				if (member.getBusinessUnits().size() == 1) {
					if (Functions.nullSafeEquals(token, "DEALER_SYSTEM_COMPONENT")) {
						allow = true;
					}
					else if (member.getBusinessUnits().size() == 1) {
						allow = true;
					}
				}
				if (allow) {
					biz.firstlook.module.core.impl.entity.BusinessUnit dealer = member.getBusinessUnits().iterator().next();
					biz.firstlook.module.core.impl.entity.SoftwareSystemComponent component = getSoftwareSystemComponentDAO().findByToken(token);
					biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState entity = new biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState();
					entity.setAuthorizedMember(member);
					entity.setSoftwareSystemComponent(component);
					entity.setDealerGroup(dealer.getParent());
					entity.setDealer(dealer);
					getSoftwareSystemComponentStateDAO().saveOrUpdate(entity);
					state = entity;
				}
			}
		}
		return new SoftwareSystemComponentStateAdapter(state);
	}

	public BusinessUnitDAO getBusinessUnitDAO() {
		return businessUnitDAO;
	}

	public void setBusinessUnitDAO(BusinessUnitDAO businessUnitDAO) {
		this.businessUnitDAO = businessUnitDAO;
	}

	public MemberDAO getMemberDAO() {
		return memberDAO;
	}

	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}

	public SoftwareSystemComponentDAO getSoftwareSystemComponentDAO() {
		return softwareSystemComponentDAO;
	}

	public void setSoftwareSystemComponentDAO(SoftwareSystemComponentDAO softwareSystemComponentDAO) {
		this.softwareSystemComponentDAO = softwareSystemComponentDAO;
	}

	public SoftwareSystemComponentStateDAO getSoftwareSystemComponentStateDAO() {
		return softwareSystemComponentStateDAO;
	}

	public void setSoftwareSystemComponentStateDAO(SoftwareSystemComponentStateDAO softwareSystemComponentStateDAO) {
		this.softwareSystemComponentStateDAO = softwareSystemComponentStateDAO;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
