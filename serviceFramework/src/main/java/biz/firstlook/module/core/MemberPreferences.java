package biz.firstlook.module.core;

public interface MemberPreferences {
	public SearchHomePage getSearchHomePage();
	public void setSearchHomePage(SearchHomePage searchHomePage);
}
