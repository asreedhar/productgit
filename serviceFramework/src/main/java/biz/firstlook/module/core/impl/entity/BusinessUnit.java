package biz.firstlook.module.core.impl.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

import biz.firstlook.module.core.Member;

@Entity
@Table(name="BusinessUnit")
@SecondaryTable(name="BusinessUnitRelationship", pkJoinColumns={@PrimaryKeyJoinColumn(name="BusinessUnitID")})
public class BusinessUnit implements Serializable {

	private static final long serialVersionUID = -8605732896652950489L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Integer businessUnitID;
	
	@Column(name="BusinessUnit")
	private String name;
	
	@Column(name="BusinessUnitShortName")
	private String shortName;
	
	@Column(name="OfficePhone")
	private String phoneNumber;

	@Column(name="BusinessUnitCode")
	private String businessUnitCode;
	
	
	@ManyToMany(targetEntity=biz.firstlook.module.core.impl.entity.Member.class,mappedBy="businessUnits")
	private Collection<Member> members;
	
	@OneToOne(targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnitPreferences.class, mappedBy="businessUnit")
	private BusinessUnitPreferences businessUnitPreferences;
	
	@ManyToOne(targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnit.class,optional=true)
	@JoinColumn(name="ParentID", table="BusinessUnitRelationship", nullable=true)
	private BusinessUnit parent;
	
	@OneToMany(targetEntity=biz.firstlook.module.core.impl.entity.BusinessUnit.class,mappedBy="parent")
	private Collection<BusinessUnit> children;
	
	public BusinessUnit() {
		super();
	}
	
	public BusinessUnit(Integer businessUnitID, String name, String shortName, Collection<Member> members) {
		super();
		this.businessUnitID = businessUnitID;
		this.name = name;
		this.shortName = shortName;
		this.members = members;
	}

	public Integer getBusinessUnitID() {
		return businessUnitID;
	}
	public void setBusinessUnitID(Integer businessUnitID) {
		this.businessUnitID = businessUnitID;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Collection<biz.firstlook.module.core.Member> getMembers() {
		return members;
	}
	public void setMembers(Collection<biz.firstlook.module.core.Member> members) {
		this.members = members;
	}
	
	public BusinessUnitPreferences getBusinessUnitPreferences() {
		return businessUnitPreferences;
	}
	public void setBusinessUnitPreferences(BusinessUnitPreferences businessUnitPreferences) {
		this.businessUnitPreferences = businessUnitPreferences;
	}

	public Collection<BusinessUnit> getChildren() {
		return children;
	}
	public void setChildren(Collection<BusinessUnit> children) {
		this.children = children;
	}

	public BusinessUnit getParent() {
		return parent;
	}
	public void setParent(BusinessUnit parent) {
		this.parent = parent;
	}

	public boolean equals(Object obj) {
		if (obj == this)
		    return true;
		if (obj instanceof BusinessUnit) {
			BusinessUnit b = (BusinessUnit) obj;
			boolean lEquals = true;
			if (getBusinessUnitID() == null) {
				lEquals &= (b.getBusinessUnitID() == null);
			}
			else {
				lEquals &= getBusinessUnitID().equals(b.getBusinessUnitID());
			}
			return lEquals;
		}
		return false;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber( String phoneNumber )
	{
		this.phoneNumber = phoneNumber;
	}
	
	public String getBusinessUnitCode()
	{
		return this.businessUnitCode;
	}	
}
