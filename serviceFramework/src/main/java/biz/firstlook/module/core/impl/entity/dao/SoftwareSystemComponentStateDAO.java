package biz.firstlook.module.core.impl.entity.dao;

import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState;

public class SoftwareSystemComponentStateDAO extends GenericHibernate<SoftwareSystemComponentState, Integer> {

	public SoftwareSystemComponentStateDAO() {
		super(SoftwareSystemComponentState.class);
	}
	
	@SuppressWarnings("unchecked")
	public SoftwareSystemComponentState findByLoginAndToken(String login, String token) {
		StringBuffer hql = new StringBuffer();
		hql.append(" select s");
		hql.append(" from");
		hql.append(" biz.firstlook.module.core.impl.entity.SoftwareSystemComponentState s");
		hql.append(" join s.softwareSystemComponent c");
		hql.append(" join s.authorizedMember m");
		hql.append(" where");
		hql.append(" m.login = ?");
		hql.append(" and c.token = ?");
		List<SoftwareSystemComponentState> stateItems = (List<SoftwareSystemComponentState>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { login, token } );
		SoftwareSystemComponentState state = null;
		if (!stateItems.isEmpty()) {
			if (stateItems.size() != 1) {
				throw new IllegalStateException(String.format("Non-unique state: [{0},{1}] {2} rows", login, token, stateItems.size()));
			}
			else {
				state = stateItems.get(0);
			}
		}
		return state;
	}
	
}
