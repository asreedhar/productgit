package biz.firstlook.module.core.service.impl.adapter;

import java.util.Collection;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.BusinessUnitPreferences;
import biz.firstlook.module.core.Member;

public class BusinessUnitAdapter implements biz.firstlook.module.core.BusinessUnit {

	private biz.firstlook.module.core.impl.entity.BusinessUnit businessUnit;
	
	public BusinessUnitAdapter(biz.firstlook.module.core.impl.entity.BusinessUnit businessUnit) {
		super();
		this.businessUnit = businessUnit;
	}

	public Integer getBusinessUnitID() {
		return businessUnit.getBusinessUnitID();
	}

	public BusinessUnitPreferences getBusinessUnitPreferences() {
		return businessUnit.getBusinessUnitPreferences();
	}

	public Collection<BusinessUnit> getChildren() {
		return new BusinessUnitCollectionAdapter(businessUnit.getChildren());
	}

	public Collection<Member> getMembers() {
		return businessUnit.getMembers();
	}

	public String getName() {
		return businessUnit.getName();
	}

	public BusinessUnit getParent() {
		return new BusinessUnitAdapter(businessUnit.getParent());
	}

	public String getPhoneNumber() {
		return businessUnit.getPhoneNumber();
	}

	public String getShortName() {
		return businessUnit.getShortName();
	}

	public biz.firstlook.module.core.impl.entity.BusinessUnit getWrappedBusinessUnit() {
		return businessUnit;
	}
	
	public String getBusinessUnitCode() {
		return businessUnit.getBusinessUnitCode();
	}
}
