package biz.firstlook.module.core.service.impl.adapter;

import java.util.Collection;

import biz.firstlook.main.enumerator.CredentialTypeEnum;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.MemberPreferences;
import biz.firstlook.module.core.SearchHomePage;
import biz.firstlook.module.core.impl.entity.MemberCredential;

public class MemberAdapter implements Member {

	private biz.firstlook.module.core.impl.entity.Member member;

	private BusinessUnit currentBusinessUnit;
	
	public MemberAdapter(biz.firstlook.module.core.impl.entity.Member member) {
		super();
		this.member = member;
	}

	public Collection<BusinessUnit> getBusinessUnits() {
		return new BusinessUnitCollectionAdapter(member.getBusinessUnits());
	}

	public BusinessUnit getCurrentBusinessUnit() {
		return currentBusinessUnit;
	}
	
	public void setCurrentBusinessUnit(BusinessUnit currentBusinessUnit) {
		this.currentBusinessUnit = currentBusinessUnit;
	}

	public String getFirstName() {
		return member.getFirstName();
	}

	public String getLastName() {
		return member.getLastName();
	}

	public String getLogin() {
		return member.getLogin();
	}

	public Integer getMemberType() {
		return member.getMemberType();
	}

	public Integer getMemberID() {
		return member.getMemberID();
	}

	public MemberPreferences getMemberPreferences() {
		return new MemberPreferencesImpl();
	}

	public boolean hasCredentialType( CredentialTypeEnum credentialType ){
	    for ( MemberCredential credential : member.getCredentials() ){
	        if ( credential.getCredentialTypeId().equals( credentialType.getCredentialTypeId() ) ){
	        	return true;
	        }	
	    }
	    return false;			
	}
	
	public class MemberPreferencesImpl implements MemberPreferences {
		public SearchHomePage getSearchHomePage() {
			return SearchHomePage.toSearchHomePage(member.getSearchHomePage());
		}
		public void setSearchHomePage(SearchHomePage searchHomePage) {
			member.setSearchHomePage(searchHomePage.toString());
		}
		public biz.firstlook.module.core.impl.entity.Member getWrappedMember() {
			return member;
		}
	}

	public biz.firstlook.module.core.impl.entity.Member getWrappedMember() {
		return member;
	}
	
	public String getInventoryOverviewSortOrderType() {
		return member.getInventoryOverviewSortOrderType();
	}

	public void setInventoryOverviewSortOrderType(String inventoryOverviewSortOrderType) {
		member.setInventoryOverviewSortOrderType(inventoryOverviewSortOrderType);
	}

}
