package biz.firstlook.module.audit.dao;

import org.hibernate.FlushMode;

import biz.firstlook.main.enumerator.LogClickEventType;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.module.audit.entity.LogClickEvent;

public class LogClickEventDAO extends GenericHibernate< LogClickEvent, Integer >
{

public LogClickEventDAO()
{
	super( LogClickEvent.class );
}

public void createLogClickEvent( LogClickEventType eventType, Integer businessUnitId, Integer memberId, Integer channelId, String vin, String url )
{
    LogClickEvent event = new LogClickEvent( eventType, businessUnitId, memberId, channelId, vin, url );
    getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode( FlushMode.ALWAYS );
    this.saveOrUpdate( event );
}

}
