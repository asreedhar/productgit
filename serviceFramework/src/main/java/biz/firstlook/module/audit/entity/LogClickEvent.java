package biz.firstlook.module.audit.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.main.enumerator.LogClickEventType;

/**
 * VehicleViewEvents record each vehicle that a user views in an
 * auction.  We need to keep track of each vehicle that is viewed
 * because, if a dealer actually purchases the vehicle, we get
 * credit for a referral. 
 * @author kkelly
 */
@SuppressWarnings("serial")
@Entity
@Table(name="FLUSAN_EventLog")
public class LogClickEvent
{
@Id
@GeneratedValue( strategy = GenerationType.AUTO )
@Column(name="FLUSAN_EventLogID")
private Integer id;

@Column(name="FLUSAN_EventTypeCode")
private Integer eventTypeCode;
private Integer businessUnitId;
private Integer memberId;
private Integer channelId;
private String vin;
private String url;
private Date dateCreated;

public LogClickEvent()
{
    super();
}

/**
 * Creates a ViewVehicleEvent with a create date of now.
 * @param eventType
 * @param businessUnitId
 * @param memberId
 * @param channelId
 * @param vin
 * @param url
 */
public LogClickEvent( LogClickEventType eventType, Integer businessUnitId, Integer memberId, Integer channelId, String vin, String url )
{
    eventTypeCode = eventType.ordinal();
    this.businessUnitId = businessUnitId;
    this.memberId = memberId;
    this.channelId = channelId;
    this.vin = vin;
    this.url = url;
    this.dateCreated = new Date();
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}
public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}
public Integer getChannelId()
{
	return channelId;
}
public void setChannelId( Integer channelId )
{
	this.channelId = channelId;
}
public Date getDateCreated()
{
	return dateCreated;
}
public void setDateCreated( Date dateCreated )
{
	this.dateCreated = dateCreated;
}
public Integer getEventTypeCode()
{
	return eventTypeCode;
}
public void setEventTypeCode( Integer eventTypeCode )
{
	this.eventTypeCode = eventTypeCode;
}
public Integer getId()
{
	return id;
}
public void setId( Integer id )
{
	this.id = id;
}
public Integer getMemberId()
{
	return memberId;
}
public void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}
public String getUrl()
{
	return url;
}
public void setUrl( String url )
{
	this.url = url;
}
public String getVin()
{
	return vin;
}
public void setVin( String vin )
{
	this.vin = vin;
}

}
