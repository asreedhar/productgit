package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.Trim;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleTrimImpl;

public class TrimBuilder implements FlyweightBuilder<Trim,String> {

	public Trim newFlyweight(String key) {
		return new SimpleTrimImpl(key);
	}

}
