package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>BodyType</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractBodyType implements BodyType {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof BodyType) == false)
			return false;
		BodyType b = (BodyType) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEqualsIgnoreCase(getName(), b.getName());
		return lEquals;
	}
	public int hashCode() {
		return Functions.nullSafeHashCode(getName());
	}
	public int compareTo(BodyType b) {
		if (b == null)
			return 1;
		return Functions.nullSafeCompareTo(getName(), b.getName());
	}
}
