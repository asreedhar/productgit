package biz.firstlook.module.purchasing.vehicle.service.impl;

import biz.firstlook.module.purchasing.vehicle.service.VehicleService;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;

public class VehicleServiceFactoryImpl extends VehicleServiceFactory {

	private VehicleService service;
	
	protected VehicleServiceFactoryImpl() {
		super();
		VehicleServiceFactory.setFactory(this);
	}

	public VehicleService getService() {
		return service;
	}

	public void setService(VehicleService service) {
		this.service = service;
	}

}
