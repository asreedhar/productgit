package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleLineImpl;

public class LineBuilder implements FlyweightBuilder<Line,NamedIdentifier> {

	public Line newFlyweight(NamedIdentifier key) {
		return new SimpleLineImpl(key.getId(), key.getName());
	}

}
