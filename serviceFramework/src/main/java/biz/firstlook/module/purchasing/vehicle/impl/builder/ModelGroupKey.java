package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;

public class ModelGroupKey {
	
	Integer groupingDescriptionID;
	String groupingDescription;

	public ModelGroupKey() {
		init(null, null);
	}
	
	public ModelGroupKey(Integer groupingDescriptionID, String groupingDescription) {
		init(groupingDescriptionID, groupingDescription);
	}
	
	public void init(Integer groupingDescriptionID, String groupingDescription) {
		this.groupingDescriptionID = groupingDescriptionID;
		this.groupingDescription = groupingDescription;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o instanceof ModelGroupKey) {
			ModelGroupKey k = (ModelGroupKey) o;
			boolean lEquals = false;
			if (groupingDescriptionID != null) {
				if (k.groupingDescriptionID != null) {
					lEquals |= groupingDescriptionID.equals(k.groupingDescriptionID);
				}
			}
			if (groupingDescription != null) {
				if (k.groupingDescription != null) {
					lEquals |= groupingDescription.equals(k.groupingDescription);
				}
			}
			return lEquals;
		}
		if (o instanceof Model) {
			final String otherGroupingDescription = ((Model) o).getModelGroup().getName();
			boolean lEquals = false;
			if (groupingDescription != null) {
				if (otherGroupingDescription != null) {
					lEquals |= groupingDescription.equals(otherGroupingDescription);
				}
			}
			return lEquals;
		}
		if (o instanceof ModelGroup) {
			final String otherGroupingDescription = ((ModelGroup) o).getName();
			boolean lEquals = false;
			if (groupingDescription != null) {
				if (otherGroupingDescription != null) {
					lEquals |= groupingDescription.equals(otherGroupingDescription);
				}
			}
			return lEquals;
		}
		return false;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + (groupingDescriptionID == null ? 0 : groupingDescriptionID.hashCode());
		hashCode = 31 * hashCode + (groupingDescription == null ? 0 : groupingDescription.hashCode());
		return hashCode;
	}
}

