package biz.firstlook.module.purchasing.vehicle.service.impl;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.service.VehicleService;

/**
 * <p>Simple timeout caching implementation of the VehicleService interface.</p>
 * 
 * @author swenmouth
 */
public class VehicleServiceCacheImpl implements VehicleService {
	
	public List<Make> getMakes() {
		return inner.getMakes();
	}

	public ModelGroup getModelGroup(Model model) {
		return inner.getModelGroup(model);
	}

	public ModelGroup getModelGroup(String make, String line, Integer segmentID) {
		return inner.getModelGroup(make, line, segmentID);
	}

	class GetModelYearByModelDataAccessor implements DataAccessor<List<ModelYear>>{
		Model model;
		public GetModelYearByModelDataAccessor(Model model) {
			this.model = model;
		}
		public List<ModelYear> get() {
			return inner.getModelYears(model);
		}
	}
	
	private TimeCache<Model,List<ModelYear>> modelYearByModelTimeCache = new TimeCache<Model,List<ModelYear>>(); 
	
	public List<ModelYear> getModelYears(Model model) {
		return modelYearByModelTimeCache.get(model, new GetModelYearByModelDataAccessor(model));
	}
	
	class GetModelYearByModelGroupDataAccessor implements DataAccessor<List<ModelYear>>{
		ModelGroup modelGroup;
		public GetModelYearByModelGroupDataAccessor(ModelGroup modelGroup) {
			this.modelGroup = modelGroup;
		}
		public List<ModelYear> get() {
			return inner.getModelYears(modelGroup);
		}
	}
	
	private TimeCache<ModelGroup,List<ModelYear>> modelYearByModelGroupTimeCache = new TimeCache<ModelGroup,List<ModelYear>>(); 
	
	public List<ModelYear> getModelYears(ModelGroup modelGroup) {
		return modelYearByModelGroupTimeCache.get(modelGroup, new GetModelYearByModelGroupDataAccessor(modelGroup));
	}

	public List<Model> getModels(Make make) {
		final List<Model> models = inner.getModels(make);
		return models;
	}

	public List<Model> getModels(String make, String line) {
		final List<Model> models = inner.getModels(make, line);
		return models;
	}

	class GetModelsByModelGroupDataAccessor implements DataAccessor<List<Model>>{
		ModelGroup modelGroup;
		public GetModelsByModelGroupDataAccessor(ModelGroup modelGroup) {
			this.modelGroup = modelGroup;
		}
		public List<Model> get() {
			return inner.getModels(modelGroup);
		}
	}
	
	private TimeCache<ModelGroup,List<Model>> modelsByModelGroupTimeCache = new TimeCache<ModelGroup,List<Model>>(); 
	
	public List<Model> getModels(ModelGroup modelGroup) {
		return modelsByModelGroupTimeCache.get(modelGroup, new GetModelsByModelGroupDataAccessor(modelGroup));
	}
	
	private Model[] cachedModels = new Model[3000];
	
	public Model getModel(Integer id) {
		if (cachedModels[id] == null) {
			cachedModels[id] = inner.getModel(id);
		}
		return cachedModels[id];
	}
	
	/*
	 * Implementation details.
	 */
	
	private VehicleService inner;
	
	public VehicleService getInner() {
		return inner;
	}
	public void setInner(VehicleService inner) {
		this.inner = inner;
	}

	interface DataAccessor<V> {
		public V get();
	}
	
	class TimeCache<K,V> {
		static final int THIRTY_MINUTES = 1000*60*30;
		Map<K,Entry> cache = new WeakHashMap<K,Entry>();
		public V get(K key, DataAccessor<V> accessor) {
			Entry e = cache.get(key);
			if (e != null) {
				if ((System.currentTimeMillis()-e.created) > THIRTY_MINUTES) {
					e = null;
				}
			}
			if (e == null) {
				e = new Entry(accessor.get());
				cache.put(key,e);
			}
			return e.value;
		}
		class Entry {
			long created;
			V value;
			public Entry(V value) {
				this.value = value;
				this.created = System.currentTimeMillis();
			}
		}
	}
}
