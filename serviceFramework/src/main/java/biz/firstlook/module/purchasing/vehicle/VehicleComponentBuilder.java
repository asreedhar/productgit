package biz.firstlook.module.purchasing.vehicle;

public abstract class VehicleComponentBuilder {

	protected VehicleComponentBuilder() {
		super();
	}

	public abstract Line buildLine(Integer id, String name);
	
	public abstract Make buildMake(Integer id, String name);
	
	public abstract Model buildModel(Integer id, String name, Make make, Line line, Segment segment, ModelGroup modelGroup);
	
	public abstract ModelGroup buildModelGroup(Integer modelGroupID, String name);
	
	public abstract ModelYear buildModelYear(Integer year);
	
	public abstract Segment buildSegment(Integer segmentID, String name);
	
	public abstract Trim buildTrim(String name);
	
	public abstract Vehicle buildVehicle(
			Model model,
			Trim trim,
			ModelYear modelYear,
			String vin,
			String color,
			String engine,
			Integer mileage,
			String driveTrain);
	
}
