package biz.firstlook.module.purchasing.vehicle;

public interface Vehicle  extends Comparable<Vehicle> {
	public Model getModel();
	public Trim getTrim();
	public ModelYear getModelYear();
	public String getColor();
	public Integer getMileage();
	public String getEngine();
	public String getVIN();
	public String getDriveTrain();
}
