package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>Vehicle</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.
 * @author swenmouth
 */
public abstract class AbstractVehicle implements Vehicle {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof Vehicle) == false)
			return false;
		Vehicle m = (Vehicle) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getModel(), m.getModel());
		lEquals &= Functions.nullSafeEquals(getModelYear(), m.getModelYear());
		lEquals &= Functions.nullSafeEquals(getTrim(), m.getTrim());
		lEquals &= Functions.nullSafeEquals(getColor(), m.getColor());
		lEquals &= Functions.nullSafeEquals(getMileage(), m.getMileage());
		lEquals &= Functions.nullSafeEquals(getEngine(), m.getEngine());
		lEquals &= Functions.nullSafeEquals(getVIN(), m.getVIN());
		lEquals &= Functions.nullSafeEquals(getDriveTrain(), m.getDriveTrain());
		return lEquals;
	}
	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getModel());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getModelYear());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getTrim());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getColor());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getMileage());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getEngine());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getVIN());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getDriveTrain());
		return hashCode;
	}
	public int compareTo(Vehicle m) {
		if (m == null)
			return 1;
		int c = 0;
		c = Functions.nullSafeCompareTo(getModel(), m.getModel(), c);
		c = Functions.nullSafeCompareTo(getModelYear(), m.getModelYear(), c);
		c = Functions.nullSafeCompareTo(getTrim(), m.getTrim(), c);
		c = Functions.nullSafeCompareTo(getMileage(), m.getMileage(), c);
		c = Functions.nullSafeCompareTo(getColor(), m.getColor(), c);
		c = Functions.nullSafeCompareTo(getEngine(), m.getEngine(), c);
		c = Functions.nullSafeCompareTo(getVIN(), m.getVIN(), c);
		c = Functions.nullSafeCompareTo(getDriveTrain(), m.getDriveTrain(), c);
		return c;
	}
}
