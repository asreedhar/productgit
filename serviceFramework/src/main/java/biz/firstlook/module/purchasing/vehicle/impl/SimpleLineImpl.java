package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractLine;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

/**
 * Simple bean implementation of the <code>Line</code> interface.
 * @author swenmouth
 */
public class SimpleLineImpl extends AbstractLine implements Serializable {
	private static final long serialVersionUID = 1074411939956692079L;
	private Integer id;
	private String name;
	/**
	 * Allocates a new <code>Line</code> with the argument name.
	 * @param name the name of the line
	 */
	public SimpleLineImpl(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildLine(id, name);
	}
}
