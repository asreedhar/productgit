package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import org.apache.commons.logging.LogFactory;

import biz.firstlook.module.purchasing.vehicle.AbstractModel;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.Segment;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

/**
 * Simple bean implementation of the <code>Model</code> interface.
 * @author swenmouth
 */
public class SimpleModelImpl extends AbstractModel implements Serializable {
	
	private static final long serialVersionUID = -6510192471961454737L;
	
	private Integer id;
	private String name; 
	private Make make;
	private Line line;
	private Segment segment;
	private ModelGroup modelGroup; 
	
	/**
	 * Allocates a new <code>Model</code> with the argument make, line and segment.
	 * @param name the name of the model
	 */
	public SimpleModelImpl(Integer id, String name, Make make, Line line, Segment segment, ModelGroup modelGroup) {
		super();
		this.id = id;
		this.name = name;
		this.make = make;
		this.line = line;
		this.segment = segment;
		this.modelGroup = modelGroup;
		if (this.modelGroup == null) {
			LogFactory.getLog(SimpleModelImpl.class).debug("null modelgroup for model");
		}
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Line getLine() {
		return line;
	}

	public Make getMake() {
		return make;
	}

	public Segment getSegment() {
		return segment;
	}

	public ModelGroup getModelGroup() {
		return modelGroup;
	}

	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildModel(id, name, make, line, segment, modelGroup);
	}
}
