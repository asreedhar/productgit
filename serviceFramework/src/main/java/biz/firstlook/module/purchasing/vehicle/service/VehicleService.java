package biz.firstlook.module.purchasing.vehicle.service;

import java.util.List;

import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

/**
 * <p>Interface to query the <em>First Look</em> vehicle catalog.</p>
 * @author swenmouth
 */
public interface VehicleService {
	/**
	 * Return a collection of all makes in the vehicle catalog.
	 * @return collection of all vehicle makes 
	 */
	public List<Make> getMakes();
	/**
	 * Return a collection of all the models for a given make in the vehicle catalog.
	 * @param the make for which we want all possible models 
	 * @return collection of all models for the given vehicle make
	 */
	public List<Model> getModels(Make make);
	/**
	 * Return a collection of all the models for the given wild-card make and line
	 * in the vehicle catalog.
	 * @param the make for which we want all possible models 
	 * @return collection of all models for the given vehicle make
	 */
	public List<Model> getModels(String make, String line);
	/**
	 * DOCUMENT ME!
	 * @param id
	 * @return
	 */
	public Model getModel(Integer id);
	/**
	 * Return a collection of all the models for a given model group in the vehicle catalog.
	 * @param modelGroup the model group for which we want all possible models 
	 * @return collection of all models for the given vehicle model group
	 */
	public List<Model> getModels(ModelGroup modelGroup);
	/**
	 * Return a collection of all the model years for a given model in the vehicle catalog.
	 * @param model the model for which we want all possible model years 
	 * @return collection of all model years for the given vehicle model
	 */
	public List<ModelYear> getModelYears(Model model);
	/**
	 * Return a collection of all the model years for a given model group in the vehicle catalog.
	 * @param modelGroup the model group for which we want all possible model years 
	 * @return collection of all model years for the given vehicle model
	 */
	public List<ModelYear> getModelYears(ModelGroup modelGroup);
	
	public ModelGroup getModelGroup(Model model);
	
	public ModelGroup getModelGroup(String make, String line, Integer segmentID);
}
