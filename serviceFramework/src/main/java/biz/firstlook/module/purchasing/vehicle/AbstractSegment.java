package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>Segment</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractSegment implements Segment {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof Segment) == false)
			return false;
		Segment s = (Segment) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getSegmentID(), s.getSegmentID());
		return lEquals;
	}
	public int hashCode() {
		return Functions.nullSafeHashCode(getSegmentID());
	}
	public int compareTo(Segment s) {
		if (s == null)
			return 1;
		return Functions.nullSafeCompareTo(getSegmentID(), s.getSegmentID());
	}
}
