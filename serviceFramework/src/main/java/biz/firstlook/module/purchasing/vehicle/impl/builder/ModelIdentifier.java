package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.Segment;

public class ModelIdentifier {
	private Integer id;
	private String name;
	private Make make;
	private Line line;
	private Segment segment;
	private ModelGroup modelGroup;
	public ModelIdentifier(Integer id, String name, Make make, Line line, Segment segment, ModelGroup modelGroup) {
		this.id = id;
		this.name = name;
		this.make = make;
		this.line = line;
		this.segment = segment;
		this.modelGroup = modelGroup;
	}
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public Line getLine() {
		return line;
	}
	public Make getMake() {
		return make;
	}
	public ModelGroup getModelGroup() {
		return modelGroup;
	}
	public Segment getSegment() {
		return segment;
	}
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof ModelIdentifier) == false)
			return false;
		ModelIdentifier m = (ModelIdentifier) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getId(), m.getId());
		return lEquals;
	}
	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getId());
		return hashCode;
	}
}
