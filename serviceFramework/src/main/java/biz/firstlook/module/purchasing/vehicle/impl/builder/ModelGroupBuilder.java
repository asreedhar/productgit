package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleModelGroupImpl;

public class ModelGroupBuilder implements FlyweightBuilder<ModelGroup,ModelGroupKey> {

	public ModelGroup newFlyweight(ModelGroupKey key) {
		return new SimpleModelGroupImpl(key.groupingDescriptionID, key.groupingDescription);
	}

}
