package biz.firstlook.module.purchasing.vehicle.service.impl;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.module.purchasing.vehicle.Model;

public class InGroupVehicleServiceImpl extends StoredProcedure {

	private final static String STORED_PROC_NAME = "dbo.GetPurchasingSisterStoreModels"; 
	
	private final static String PARAM_BUSINESS_UNIT_ID = "BusinessUnitID";
	
	private final static String RESULT_SET = "ResultSet";
	
	public InGroupVehicleServiceImpl(DataSource dataSource) {
		super(dataSource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(RESULT_SET, new VehicleServiceImpl.ModelRowMapper()));
		declareParameter(new SqlParameter(PARAM_BUSINESS_UNIT_ID, Types.INTEGER));
	}
	
	@SuppressWarnings("unchecked")
	public List<Model> getModels(Integer dealerId) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_BUSINESS_UNIT_ID, dealerId);
		
		Map<String, Object> results = execute(parameters);
		List<Model> models = (List<Model>)results.get(RESULT_SET);
		return models;
	}
}
