package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleMakeImpl;

public class MakeBuilder implements FlyweightBuilder<Make,NamedIdentifier> {

	public Make newFlyweight(NamedIdentifier key) {
		return new SimpleMakeImpl(key.getId(), key.getName());
	}

}
