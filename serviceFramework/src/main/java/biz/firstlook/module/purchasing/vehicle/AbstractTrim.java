package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>Trim</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractTrim implements Trim {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof Trim) == false)
			return false;
		Trim t = (Trim) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEqualsIgnoreCase(getName(), t.getName());
		return lEquals;
	}
	public int hashCode() {
		return Functions.nullSafeHashCode(getName());
	}
	public int compareTo(Trim t) {
		if (t == null)
			return 1;
		return Functions.nullSafeCompareTo(getName(), t.getName());
	}
}
