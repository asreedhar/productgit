package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.Vehicle;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleVehicleImpl;

public class VehicleBuilder implements FlyweightBuilder<Vehicle,VehicleKey> {

	public Vehicle newFlyweight(VehicleKey key) {
		return new SimpleVehicleImpl(
				key.getColor(),
				key.getModel(),
				key.getModelYear(),
				key.getTrim(),
				key.getMileage(),
				key.getEngine(),
				key.getVIN(), 
				key.getDriveTrain());
	}
	
}
