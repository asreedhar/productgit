package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>ModelYear</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractModelYear implements ModelYear {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof ModelYear) == false)
			return false;
		ModelYear m = (ModelYear) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getModelYear(), m.getModelYear());
		return lEquals;
	}
	public int hashCode() {
		return Functions.nullSafeHashCode(getModelYear());
	}
	public int compareTo(ModelYear m) {
		if (m == null)
			return 1;
		return Functions.nullSafeCompareTo(getModelYear(), m.getModelYear());
	}
}
