package biz.firstlook.module.purchasing.vehicle;

/**
 * <p>A category is a set of vehicles whose members are defined by a set of business-rules.</p>
 * <p>Example <code>Segment</code>'s are: Truck, Sedan, Coupe, Van, SUV, Convertible and Wagon.</p>
 * @author swenmouth
 */
public interface Segment extends Comparable<Segment>  {
	
	/**
	 * Return the ID of the segment.
	 * @return the ID of the segment.
	 */
	public Integer getSegmentID();
	
	/**
	 * Returns the name of the segment.
	 * @return the name of the segment.
	 */
	public String getName();
	
}
