package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>Line</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractLine implements Line {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof Line) == false)
			return false;
		Line l = (Line) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getId(), l.getId());
		return lEquals;
	}
	public int hashCode() {
		return Functions.nullSafeHashCode(getId());
	}
	public int compareTo(Line l) {
		if (l == null)
			return 1;
		return Functions.nullSafeCompareTo(getName(), l.getName());
	}
}
