package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractMake;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

/**
 * Simple bean implementation of the <code>Make</code> interface.
 * @author swenmouth
 */
public class SimpleMakeImpl extends AbstractMake implements Serializable {
	private static final long serialVersionUID = 1515513377613437601L;
	private Integer id;
	private String name;
	/**
	 * Allocates a new <code>Make</code> with the argument name.
	 * @param name the name of the make
	 */
	public SimpleMakeImpl(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildMake(id, name);
	}
}
