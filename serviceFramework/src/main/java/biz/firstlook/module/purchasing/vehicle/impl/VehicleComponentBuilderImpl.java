package biz.firstlook.module.purchasing.vehicle.impl;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.commons.flyweight.FlyweightFactory;
import biz.firstlook.commons.flyweight.WeakHashMapFlyweightPool;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Segment;
import biz.firstlook.module.purchasing.vehicle.Trim;
import biz.firstlook.module.purchasing.vehicle.Vehicle;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.LineBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.MakeBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.ModelBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.ModelGroupBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.ModelGroupKey;
import biz.firstlook.module.purchasing.vehicle.impl.builder.ModelIdentifier;
import biz.firstlook.module.purchasing.vehicle.impl.builder.ModelYearBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.NamedIdentifier;
import biz.firstlook.module.purchasing.vehicle.impl.builder.SegmentBuilder;
import biz.firstlook.module.purchasing.vehicle.impl.builder.TrimBuilder;

public class VehicleComponentBuilderImpl extends VehicleComponentBuilder {

	public Line buildLine(Integer id, String name) {
		return lineFlyweightFactory.getFlyweight(new NamedIdentifier(id, name));
	}

	public Make buildMake(Integer id, String name) {
		return makeFlyweightFactory.getFlyweight(new NamedIdentifier(id, name));
	}

	public Model buildModel(Integer id, String name, Make make, Line line, Segment segment, ModelGroup modelGroup) {
		return modelFlyweightFactory.getFlyweight(new ModelIdentifier(id, name, make, line, segment, modelGroup));
	}

	public ModelGroup buildModelGroup(Integer modelGroupID, String modelGroupName) {
		return modelGroupFlyweightFactory.getFlyweight(new ModelGroupKey(modelGroupID, modelGroupName));
	}

	public ModelYear buildModelYear(Integer year) {
		return modelYearFlyweightFactory.getFlyweight(year);
	}

	public Segment buildSegment(Integer segmentID, String name) {
		return segmentFlyweightFactory.getFlyweight(segmentID);
	}

	public Trim buildTrim(String name) {
		return trimFlyweightFactory.getFlyweight(name);
	}

	// FLYWEIGHT FACTORIES
	
	public Vehicle buildVehicle(Model model, Trim trim, ModelYear modelYear, String vin, String color, String engine, Integer mileage, String driveTrain) {
		return new SimpleVehicleImpl(
				colorFlyweightFactory.getFlyweight(color),
				model,
				modelYear,
				trim,
				mileage,
				engineFlyweightFactory.getFlyweight(engine),
				vin,
				driveTrainFlyweightFactory.getFlyweight(driveTrain));
	}
	
	static class IdentityFlyweightBuilder<I> implements FlyweightBuilder<I,I> {
		public I newFlyweight(I item) {
			return item;
		}
	}
	
	static FlyweightFactory<String,String,IdentityFlyweightBuilder<String>> newFlyweightFactory() {
		return new FlyweightFactory<String, String, IdentityFlyweightBuilder<String>>(
				new WeakHashMapFlyweightPool<String, String>(),
				new IdentityFlyweightBuilder<String>()
				);
	}
	
	private FlyweightFactory<String,String,IdentityFlyweightBuilder<String>> colorFlyweightFactory = newFlyweightFactory();
	private FlyweightFactory<String,String,IdentityFlyweightBuilder<String>> engineFlyweightFactory = newFlyweightFactory();
	private FlyweightFactory<String,String,IdentityFlyweightBuilder<String>> driveTrainFlyweightFactory = newFlyweightFactory();
	
	private FlyweightFactory<Line,NamedIdentifier,LineBuilder> lineFlyweightFactory;
	private FlyweightFactory<Make,NamedIdentifier,MakeBuilder> makeFlyweightFactory;
	private FlyweightFactory<Model,ModelIdentifier,ModelBuilder> modelFlyweightFactory;
	private FlyweightFactory<ModelGroup,ModelGroupKey,ModelGroupBuilder> modelGroupFlyweightFactory;
	private FlyweightFactory<ModelYear,Integer,ModelYearBuilder> modelYearFlyweightFactory;
	private FlyweightFactory<Segment,Integer,SegmentBuilder> segmentFlyweightFactory;
	private FlyweightFactory<Trim,String,TrimBuilder> trimFlyweightFactory;
	
	public FlyweightFactory<Line, NamedIdentifier, LineBuilder> getLineFlyweightFactory() {
		return lineFlyweightFactory;
	}
	public void setLineFlyweightFactory(FlyweightFactory<Line, NamedIdentifier, LineBuilder> lineFlyweightFactory) {
		this.lineFlyweightFactory = lineFlyweightFactory;
	}

	public FlyweightFactory<Make, NamedIdentifier, MakeBuilder> getMakeFlyweightFactory() {
		return makeFlyweightFactory;
	}
	public void setMakeFlyweightFactory(FlyweightFactory<Make, NamedIdentifier, MakeBuilder> makeFlyweightFactory) {
		this.makeFlyweightFactory = makeFlyweightFactory;
	}

	public FlyweightFactory<Model, ModelIdentifier, ModelBuilder> getModelFlyweightFactory() {
		return modelFlyweightFactory;
	}
	public void setModelFlyweightFactory(FlyweightFactory<Model, ModelIdentifier, ModelBuilder> modelFlyweightFactory) {
		this.modelFlyweightFactory = modelFlyweightFactory;
	}

	public FlyweightFactory<ModelGroup, ModelGroupKey, ModelGroupBuilder> getModelGroupFlyweightFactory() {
		return modelGroupFlyweightFactory;
	}
	public void setModelGroupFlyweightFactory(FlyweightFactory<ModelGroup, ModelGroupKey, ModelGroupBuilder> modelGroupFlyweightFactory) {
		this.modelGroupFlyweightFactory = modelGroupFlyweightFactory;
	}

	public FlyweightFactory<ModelYear, Integer, ModelYearBuilder> getModelYearFlyweightFactory() {
		return modelYearFlyweightFactory;
	}
	public void setModelYearFlyweightFactory(FlyweightFactory<ModelYear, Integer, ModelYearBuilder> modelYearFlyweightFactory) {
		this.modelYearFlyweightFactory = modelYearFlyweightFactory;
	}

	public FlyweightFactory<Segment, Integer, SegmentBuilder> getSegmentFlyweightFactory() {
		return segmentFlyweightFactory;
	}
	public void setSegmentFlyweightFactory(FlyweightFactory<Segment, Integer, SegmentBuilder> segmentFlyweightFactory) {
		this.segmentFlyweightFactory = segmentFlyweightFactory;
	}

	public FlyweightFactory<Trim, String, TrimBuilder> getTrimFlyweightFactory() {
		return trimFlyweightFactory;
	}
	public void setTrimFlyweightFactory(FlyweightFactory<Trim, String, TrimBuilder> trimFlyweightFactory) {
		this.trimFlyweightFactory = trimFlyweightFactory;
	}

}
