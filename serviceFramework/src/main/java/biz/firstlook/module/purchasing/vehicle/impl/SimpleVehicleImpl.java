package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractVehicle;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Trim;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

public class SimpleVehicleImpl extends AbstractVehicle implements Serializable {

	private static final long serialVersionUID = 6088184575773054846L;
	
	private String color;
	private Model model;
	private ModelYear modelYear;
	private Trim trim;
	private Integer mileage;
	private String engine;
	private String vin;
	private String driveTrain;
	
	public SimpleVehicleImpl(String color, Model model, ModelYear modelYear, Trim trim, Integer mileage, String engine, String vin, String driveTrain) {
		super();
		this.color = color;
		this.model = model;
		this.modelYear = modelYear;
		this.trim = trim;
		this.mileage = mileage;
		this.engine = engine;
		this.vin = vin;
		this.driveTrain = driveTrain;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}

	public ModelYear getModelYear() {
		return modelYear;
	}
	public void setModelYear(ModelYear modelYear) {
		this.modelYear = modelYear;
	}
	
	public Trim getTrim() {
		return trim;
	}
	public void setTrim(Trim trim) {
		this.trim = trim;
	}

	public Integer getMileage() {
		return mileage;
	}

	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getVIN() {
		return vin;
	}

	public void setVIN(String vin) {
		this.vin = vin;
	}

	public String getDriveTrain() {
		return driveTrain;
	}

	public void setDriveTrain(String driveTrain) {
		this.driveTrain = driveTrain;
	}
	
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildVehicle(model, trim, modelYear, vin, color, engine, mileage, driveTrain);
	}
}
