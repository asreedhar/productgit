package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractModelGroup;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

public class SimpleModelGroupImpl extends AbstractModelGroup implements Serializable {
	
	private static final long serialVersionUID = -129871774876122615L;
	private Integer modelGroupID;
	private String name;
	
	public SimpleModelGroupImpl(Integer id, String name) {
		super();
		this.modelGroupID = id;
		this.name = name;
	}

	public Integer getModelGroupID() {
		return modelGroupID;
	}

	public void setModelGroupID(Integer id) {
		this.modelGroupID = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return super.toString() + "[ModelGroupId=" + modelGroupID + ", name=" + name + "]";
	}
	
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildModelGroup(modelGroupID, name);
	}
}
