package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>Model</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractModel implements Model {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof Model) == false)
			return false;
		Model m = (Model) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getId(), m.getId());
		return lEquals;
	}
	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getId());
		return hashCode;
	}
	public int compareTo(Model m) {
		if (m == null)
			return 1;
		int c = 0;
		c = Functions.nullSafeCompareTo(getMake(), m.getMake(), c);
		c = Functions.nullSafeCompareTo(getName(), m.getName(), c);
		c = Functions.nullSafeCompareTo(getSegment(), m.getSegment(), c);
		return c;
	}
}
