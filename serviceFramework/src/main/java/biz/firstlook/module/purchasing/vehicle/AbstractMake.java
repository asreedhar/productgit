package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>Make</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.  
 * @author swenmouth
 */
public abstract class AbstractMake implements Make {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof Make) == false)
			return false;
		Make m = (Make) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getId(), m.getId());
		return lEquals;
	}
	public int hashCode() {
		return Functions.nullSafeHashCode(getId());
	}
	public int compareTo(Make m) {
		if (m == null)
			return 1;
		return Functions.nullSafeCompareTo(getName(), m.getName());
	}
}
