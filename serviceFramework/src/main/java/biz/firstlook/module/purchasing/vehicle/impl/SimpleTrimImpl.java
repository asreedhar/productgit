package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractTrim;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

/**
 * Simple bean implementation of the <code>Trim</code> interface.
 * @author swenmouth
 */
public class SimpleTrimImpl extends AbstractTrim implements Serializable {
	private static final long serialVersionUID = 774099887920205279L;
	private String name;
	/**
	 * Allocates a new <code>Trim</code> with the argument name.
	 * @param name the name of the trim
	 */
	public SimpleTrimImpl(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildTrim(name);
	}
}
