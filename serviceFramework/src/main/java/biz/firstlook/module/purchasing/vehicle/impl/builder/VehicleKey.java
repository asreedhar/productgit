package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.module.purchasing.vehicle.AbstractVehicle;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Trim;

public class VehicleKey extends AbstractVehicle {
	
	private Model model;
	private Trim  trim;
	private ModelYear year;
	private String  color;
	private Integer mileage;
	private String  engine;
	private String  vin;
	private String driveTrain;
	
	public void init(Model model, Trim trim, ModelYear year, String color, Integer mileage, String engine, String vin, String driveTrain) {
		this.model = model;
		this.trim = trim;
		this.year = year;
		this.color = color;
		this.mileage = mileage;
		this.engine = engine;
		this.vin = vin;
		this.driveTrain = driveTrain;
	}

	public String getColor() {
		return color;
	}

	public String getEngine() {
		return engine;
	}

	public Integer getMileage() {
		return mileage;
	}

	public Model getModel() {
		return model;
	}

	public ModelYear getModelYear() {
		return year;
	}

	public Trim getTrim() {
		return trim;
	}

	public String getVIN() {
		return vin;
	}

	public String getDriveTrain() {
		return driveTrain;
	}

	public void setDriveTrain(String driveTrain) {
		this.driveTrain = driveTrain;
	}
	
}
