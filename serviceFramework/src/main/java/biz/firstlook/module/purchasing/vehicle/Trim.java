package biz.firstlook.module.purchasing.vehicle;

/**
 * <p><em>Trim</em> (or <em>Series</em>) is a name a manufacturer applies to a subdivision of a
 * <code>{@link Line}</code> denoting price, size or weight identification and that
 * is used by the manufacturer for marketing purposes.</p> 
 * @author swenmouth
 */
public interface Trim extends Comparable<Trim>  {
	/**
	 * Returns the name of the trim.
	 * @return the name of the trim
	 */
	public String getName();
}
