package biz.firstlook.module.purchasing.vehicle.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.commons.sql.ResultSetUtils;
import biz.firstlook.module.purchasing.vehicle.Line;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Segment;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleModelGroupImpl;

/**
 * This implementation of the <code>VehicleService</code> is a simple DAO.
 * @author swenmouth
 */
public class VehicleServiceImpl extends JdbcDaoSupport implements biz.firstlook.module.purchasing.vehicle.service.VehicleService {
	
	public VehicleServiceImpl() {
		super();
	}

	// SERVICE METHODS
	
	@SuppressWarnings("unchecked")
	public List<Make> getMakes() {
		return (List<Make>) getJdbcTemplate().query(
				VehicleCatalogQueries.SelectMakes(),
				new Object[] {},
				new int[] {},
				new MakeRowMapper());
	}

	@SuppressWarnings("unchecked")
	public List<ModelYear> getModelYears(Model model) {
		return (List<ModelYear>) getJdbcTemplate().query(
				VehicleCatalogQueries.SelectModelYearsForModel(),
				new Object[] { model.getMake().getName(), model.getLine().getName(), model.getSegment().getName() },
				new int[] { java.sql.Types.VARCHAR, java.sql.Types.VARCHAR, java.sql.Types.VARCHAR },
				new ModelYearRowMapper());
	}

	@SuppressWarnings("unchecked")
	public List<ModelYear> getModelYears(ModelGroup modelGroup) {
		if (modelGroup instanceof SimpleModelGroupImpl) {
			SimpleModelGroupImpl mg = (SimpleModelGroupImpl) modelGroup;
			return (List<ModelYear>) getJdbcTemplate().query(
					VehicleCatalogQueries.SelectModelYearsForModelGroup(),
					new Object[] { mg.getModelGroupID() },
					new int[] { java.sql.Types.INTEGER },
					new ModelYearRowMapper());
		}
		return Collections.emptyList();
	}

	@SuppressWarnings("unchecked")
	public List<Model> getModels(Make make) {
		return (List<Model>) getJdbcTemplate().query(
				VehicleCatalogQueries.SelectModelsForMakeId(),
				new Object[] { make.getId() },
				new int[] { java.sql.Types.INTEGER },
				new ModelRowMapper());
	}

	@SuppressWarnings("unchecked")
	public Model getModel(Integer id) {
		List<Model> models = (List<Model>) getJdbcTemplate().query(
				VehicleCatalogQueries.SelectModelsForModelId(),
				new Object[] { id },
				new int[] { java.sql.Types.INTEGER },
				new ModelRowMapper());
		if (models.isEmpty()) {
			throw new IllegalStateException("No model for ID=" + id);
		}
		else if (models.size() > 1) {
			throw new IllegalStateException(models.size() + " models for ID=" + id);
		}
		else {
			return models.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Model> getModels(String make, String line) {
		final String safeMake = make.replaceAll("[%]", "\\%").replaceAll("[*]", "%");
		final String safeLine = line.replaceAll("[%]", "\\%").replaceAll("[*]", "%");
		return (List<Model>) getJdbcTemplate().query(
				VehicleCatalogQueries.SelectModelsByWildcardForMakeText(),
				new Object[] { safeMake, safeLine },
				new int[] { java.sql.Types.VARCHAR, java.sql.Types.VARCHAR },
				new ModelRowMapper());
	}
	
	@SuppressWarnings("unchecked")
	public List<Model> getModels(ModelGroup modelGroup) {
		if (modelGroup instanceof SimpleModelGroupImpl) {
			SimpleModelGroupImpl mg = (SimpleModelGroupImpl) modelGroup;
			return (List<Model>) getJdbcTemplate().query(
					VehicleCatalogQueries.SelectModelsForModelGroup(),
					new Object[] { mg.getModelGroupID() },
					new int[] { java.sql.Types.INTEGER },
					new ModelRowMapper());
		}
		return Collections.emptyList();
	}

	public ModelGroup getModelGroup(Model model) {
		return getModelGroup(model.getMake().getName(), model.getLine().getName(), model.getSegment().getSegmentID());
	}
	
	@SuppressWarnings("unchecked")
	public ModelGroup getModelGroup(String make, String line, Integer segmentID) {
		List<ModelGroup> modelGroups = (List<ModelGroup>) getJdbcTemplate().query(
				VehicleCatalogQueries.SelectModelGroupForModel(),
				new Object[] { make, line, segmentID },
				new int[] { java.sql.Types.VARCHAR, java.sql.Types.VARCHAR, java.sql.Types.INTEGER },
				new ModelGroupRowMapper());
		if (modelGroups.isEmpty()) {
			throw new IllegalStateException("No ModelGroup for: [Make=" + make + ", Line=" + line + ", Segment=" + segmentID + "]");
		}
		else if (modelGroups.size() > 1) {
			throw new IllegalStateException("No ModelGroup for: [Make=" + make + ", Line=" + line + ", Segment=" + segmentID + "]");
		}
		else {
			return modelGroups.get(0);
		}
	}

	// ROW MAPPERS
	
	static class MakeRowMapper extends AbstractRowMapper implements RowMapper {
		protected Object performMapping(ResultSet rs) throws SQLException {
			return buildMake(rs);
		}
		static Make buildMake(ResultSet rs) throws SQLException {
			return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildMake(
					ResultSetUtils.getInt(rs, "MakeId", false),
					ResultSetUtils.getString(rs, "Make", false));
		}
	}
	
	static class LineRowMapper extends AbstractRowMapper implements RowMapper {
		protected Object performMapping(ResultSet rs) throws SQLException {
			return buildLine(rs);
		}
		static Line buildLine(ResultSet rs) throws SQLException {
			return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildLine(
					ResultSetUtils.getInt(rs, "LineId", false),
					ResultSetUtils.getString(rs, "Line", false));
		}
	}
	
	static class SegmentRowMapper extends AbstractRowMapper implements RowMapper {
		protected Object performMapping(ResultSet rs) throws SQLException {
			return buildSegment(rs);
		}
		static Segment buildSegment(ResultSet rs) throws SQLException {
			return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildSegment(
					ResultSetUtils.getInt(rs, "SegmentId", false),
					ResultSetUtils.getString(rs, "Segment", false));
		}
	}
	
	static class ModelGroupRowMapper extends AbstractRowMapper implements RowMapper {
		protected Object performMapping(ResultSet rs) throws SQLException {
			return buildModelGroup(rs);
		}
		static ModelGroup buildModelGroup(ResultSet rs) throws SQLException {
			return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildModelGroup(
					ResultSetUtils.getInt(rs, "ModelGroupId", false),
					ResultSetUtils.getString(rs, "ModelGroup", false));
		}
	}
	
	static class ModelYearRowMapper extends AbstractRowMapper implements RowMapper {
		protected Object performMapping(ResultSet rs) throws SQLException {
			return buildModelYear(rs);
		}
		static ModelYear buildModelYear(ResultSet rs) throws SQLException {
			return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildModelYear(
					ResultSetUtils.getInt(rs, "ModelYear", false));
		}
	}
	
	static class ModelRowMapper extends AbstractRowMapper implements RowMapper {
		protected Object performMapping(ResultSet rs) throws SQLException {
			final VehicleComponentBuilder builder = VehicleComponentBuilderFactory.getInstance().newVehicleBuilder();
			return builder.buildModel(
					// model id and name
					ResultSetUtils.getInt(rs, "ModelId", false),
					ResultSetUtils.getString(rs, "Model", false),
					// model components
					MakeRowMapper.buildMake(rs),
					LineRowMapper.buildLine(rs),
					SegmentRowMapper.buildSegment(rs),
					ModelGroupRowMapper.buildModelGroup(rs));
		}
	}

	// HELPER
	
	static abstract class AbstractRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int rowNum) {
			try {
				return performMapping(rs);
			}
			catch (SQLException sql) {
				throw new IllegalStateException("Error getting columns from ResultSet!", sql);
			}
		}
		abstract protected Object performMapping(ResultSet rs) throws SQLException;
	}

}
