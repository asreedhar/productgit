package biz.firstlook.module.purchasing.vehicle;

/**
 * Identity object used to key a primitive equivalance class of group <code>Model</code>s.
 * @author swenmouth
 */
public interface ModelGroup extends Comparable<ModelGroup>  {
	public Integer getModelGroupID();
	public String getName();
}
