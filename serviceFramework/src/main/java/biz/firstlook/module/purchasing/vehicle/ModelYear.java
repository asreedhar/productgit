package biz.firstlook.module.purchasing.vehicle;

/**
 * <p>The year used to designate a discrete model, irrespective of the calendar year in which the
 * vehicle was actually produced, provided that the production period does not exceed 24 months.</p>
 * <p>Example <code>ModelYear</code>'s are 2001, 2002, 2003 etc.</p>
 * @author swenmouth
 */
public interface ModelYear extends Comparable<ModelYear>  {
	/**
	 * Return a vehicle's production year.
	 * @return a vehicle's production year.
	 */
	public Integer getModelYear();
}
