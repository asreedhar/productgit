package biz.firstlook.module.purchasing.vehicle.service.impl;

import java.text.MessageFormat;

class VehicleCatalogQueries {

	public static final String SelectMakes() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	MakeId, Make");
		sb.append(" FROM	VehicleCatalog.Firstlook.Make");
		sb.append(" WHERE 	Make <> 'UNKNOWN'");
		sb.append(" ORDER");
		sb.append(" BY		Make ASC");
		return sb.toString();
	}
	
	public static String SelectModelsForMakeId() {
		return SelectModels(VehicleCatalogQueryOperation.EQUALS);
	}
	
	public static String SelectModelsForModelId() {
		return SelectModels(VehicleCatalogQueryOperation.MODEL);
	}
	
	public static String SelectModelsByWildcardForMakeText() {
		return SelectModels(VehicleCatalogQueryOperation.LIKE);
	}
	
	public static String SelectModelsForModelGroup() {
		return SelectModels(VehicleCatalogQueryOperation.GROUP);
	}
	
	public static String SelectModelYearsForModel() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	MY.ModelYear");
		sb.append(" FROM	VehicleCatalog.Firstlook.ModelYear MY");
		sb.append(" WHERE	MY.ModelID = ?");
		sb.append(" ORDER");
		sb.append(" BY		MY.ModelYear DESC");
		return sb.toString();
	}
	
	public static String SelectModelYearsForModelGroup() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	MY.ModelYear");
		sb.append(" FROM	VehicleCatalog.Firstlook.Model MO");
		sb.append(" JOIN	VehicleCatalog.Firstlook.ModelYear MY ON MO.ModelID = MY.ModelID");
		sb.append(" JOIN	IMT.dbo.MakeModelGrouping MM ON MM.ModelID = MO.ModelID");
		sb.append(" JOIN	IMT.dbo.tbl_GroupingDescription MG ON MM.GroupingDescriptionID = MG.GroupingDescriptionID");
		sb.append(" WHERE	MO.ModelID <> 0");
		sb.append(" AND		MG.GroupingDescriptionID = ?");
		sb.append(" ORDER");
		sb.append(" BY		MY.ModelYear DESC");
		return sb.toString();
	}
	
	public static String SelectModelGroupForModel() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	MY.ModelYear");
		sb.append(" FROM	VehicleCatalog.Firstlook.Model MO");
		sb.append(" JOIN	VehicleCatalog.Firstlook.ModelYear MY ON MO.ModelID = MY.ModelID");
		sb.append(" JOIN	IMT.dbo.MakeModelGrouping MM ON MM.ModelID = MO.ModelID");
		sb.append(" JOIN	IMT.dbo.tbl_GroupingDescription MG ON MM.GroupingDescriptionID = MG.GroupingDescriptionID");
		sb.append(" WHERE	MO.ModelID <> 0");
		sb.append(" AND		MG.GroupingDescriptionID = ?");
		sb.append(" ORDER");
		sb.append(" BY		MY.ModelYear DESC");
		return sb.toString();
	}
	
	private static String SelectModels(VehicleCatalogQueryOperation op) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	MO.ModelId, MO.Model,");
		sb.append("		MA.MakeId, MA.Make,");
		sb.append("		L.LineId, L.Line,");
		sb.append("		S.SegmentId, S.Segment,");
		sb.append("		MG.GroupingDescriptionID ModelGroupId, MG.GroupingDescription ModelGroup");
		sb.append(" FROM	VehicleCatalog.Firstlook.Model MO");
		sb.append(" JOIN	VehicleCatalog.Firstlook.Line L ON MO.LineId = L.LineId");
		sb.append(" JOIN	VehicleCatalog.Firstlook.Make MA ON MA.MakeId = L.MakeId");
		sb.append(" JOIN	VehicleCatalog.Firstlook.Segment S ON MO.SegmentId = S.SegmentId");
		sb.append(" JOIN	IMT.dbo.MakeModelGrouping MM ON MM.ModelID = MO.ModelID");
		sb.append(" JOIN	IMT.dbo.tbl_GroupingDescription MG ON MM.GroupingDescriptionID = MG.GroupingDescriptionID");
		sb.append(" WHERE	MO.ModelID <> 0");
		switch(op) {
			case EQUALS: sb.append(" AND		MA.MakeID = ?"); break;
			case LIKE: sb.append(" AND		MA.Make = ? AND L.Line LIKE ?"); break;
			case GROUP: sb.append(" AND		MG.GroupingDescriptionID = ?"); break;
			case MODEL: sb.append(" AND		MO.ModelID = ?"); break;
			default:
				throw new IllegalArgumentException(
					MessageFormat.format("The op {0} is not supported!", op.name()));
		}			
		sb.append(" ORDER");
		sb.append(" BY		MA.Make ASC, MO.Model ASC, S.Segment ASC");
		return sb.toString();
	}
	
	private enum VehicleCatalogQueryOperation {
		EQUALS, LIKE, GROUP, MODEL;
	}
}
