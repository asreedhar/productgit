package biz.firstlook.module.purchasing.vehicle;

/**
 * A combination of <code>Make</code>, <code>Line</code> and <code>Segment</code>. This is
 * the First Look conceptualization of <code>Model</code> -- the VIN concept of
 * <code>Model</code> is a <code>Make</code>, <code>Line</code>, <code>Trim</code> and
 * <code>BodyType</code>. The reason for this difference is that First Look is concerned
 * with the analytics of selling vehicles which works at a coarser granularity.
 * @author swenmouth
 */
public interface Model extends Comparable<Model> {
	public Integer getId();
	public String getName();
	public Make getMake();
	public Line getLine();
	public Segment getSegment();
	public ModelGroup getModelGroup();
}
