package biz.firstlook.module.purchasing.vehicle;

/**
 * <p><em>Body type</em> means the general configuration or shape of a vehicle distiguished by
 * such characteristics as the number of doors or windows, cargo-carrying features and the
 * roofline.</p>
 * <p>Example <code>BodyType</code>s include Sedan, Hatchback and Truck.</p>
 * @author swenmouth
 */
public interface BodyType extends Comparable<BodyType> {
	/**
	 * Return the name of the body type. 
	 * @return the name of the body type
	 */
	public String getName();
}
