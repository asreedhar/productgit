package biz.firstlook.module.purchasing.vehicle;

/**
 * <p>A name a manufacturer applies to a group of vehicles or engines.</p>
 * <p>Example <code>Make</code>'s are Honda, Toyota and Ford.</p>  
 * 
 * @author swenmouth
 */
public interface Make extends Comparable<Make> {
	/**
	 * Unique identifier for the vehicle or engine.
	 * @return
	 */
	public Integer getId();
	/**
	 * Returns the name of the vehicle or engine.
	 * @return the name of the vehicle or engine
	 */
	public String getName();
}
