package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.Segment;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleSegmentImpl;

public class SegmentBuilder implements FlyweightBuilder<Segment,Integer> {

	public Segment newFlyweight(Integer key) {
		return new SimpleSegmentImpl(key);
	}

}
