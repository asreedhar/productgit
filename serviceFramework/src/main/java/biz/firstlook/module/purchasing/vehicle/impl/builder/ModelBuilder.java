package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleModelImpl;

public class ModelBuilder implements FlyweightBuilder<Model, ModelIdentifier> {

	public Model newFlyweight(ModelIdentifier key) {
		return new SimpleModelImpl(key.getId(), key.getName(), key.getMake(), key.getLine(), key.getSegment(), key.getModelGroup());
	}

}
