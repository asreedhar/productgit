package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.flyweight.FlyweightBuilder;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.impl.SimpleModelYearImpl;

public class ModelYearBuilder implements FlyweightBuilder<ModelYear,Integer> {

	public ModelYear newFlyweight(Integer key) {
		return new SimpleModelYearImpl(key);
	}

}
