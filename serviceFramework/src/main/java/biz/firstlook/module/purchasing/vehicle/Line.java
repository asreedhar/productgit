package biz.firstlook.module.purchasing.vehicle;

/**
 * <p>A name a manufacturer applies to a family of vehicles within a make which have a degree
 * of commonality in construction, such as body, chssis or cab type.</p>
 * <p>Example <code>Line</code>'s are (Honda) Civic and (Toyota) Corolla.</p>
 * @author swenmouth
 */
public interface Line extends Comparable<Line> {
	/**
	 * Return the unique identifier of the line.
	 * @return
	 */
	public Integer getId();
	/**
	 * Return the name of the line.
	 * @return the name of the line
	 */
	public String getName();
}
