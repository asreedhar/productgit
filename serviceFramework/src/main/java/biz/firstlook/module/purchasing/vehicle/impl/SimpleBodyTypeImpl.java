package biz.firstlook.module.purchasing.vehicle.impl;

import biz.firstlook.module.purchasing.vehicle.AbstractBodyType;

/**
 * Simple bean implementation of the <code>BodyType</code> interface.
 * @author swenmouth
 */
public class SimpleBodyTypeImpl extends AbstractBodyType {
	private String name;
	/**
	 * Allocates a new <code>BodyType</code> with the argument name.
	 * @param name the name of the body type
	 */
	public SimpleBodyTypeImpl(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
