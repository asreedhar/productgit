package biz.firstlook.module.purchasing.vehicle.impl;

import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

public class VehicleComponentBuilderFactoryImpl extends VehicleComponentBuilderFactory {

	private VehicleComponentBuilder builder;
	
	protected VehicleComponentBuilderFactoryImpl() {
		super();
		VehicleComponentBuilderFactory.setFactory(this);
	}

	public VehicleComponentBuilder newVehicleBuilder() {
		return builder;
	}

	public VehicleComponentBuilder getBuilder() {
		return builder;
	}

	public void setBuilder(VehicleComponentBuilder builder) {
		this.builder = builder;
	}

}
