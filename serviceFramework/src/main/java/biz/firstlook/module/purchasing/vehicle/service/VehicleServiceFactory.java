package biz.firstlook.module.purchasing.vehicle.service;

public abstract class VehicleServiceFactory {

	protected VehicleServiceFactory() {
		super();
	}

	public abstract VehicleService getService();
	
	private static VehicleServiceFactory factory;
	
	public static VehicleServiceFactory getFactory() {
		return factory;
	}
	
	public static void setFactory(VehicleServiceFactory factory) {
		VehicleServiceFactory.factory = factory;
	}

}
