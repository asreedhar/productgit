package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractModelYear;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

/**
 * Simple bean implementation of the <code>ModelYear</code> interface.
 * @author swenmouth
 */
public class SimpleModelYearImpl extends AbstractModelYear implements Serializable {
	private static final long serialVersionUID = 2143735420869728290L;
	private Integer modelYear;
	/**
	 * Allocates a new <code>ModelYear</code> with the argument year.
	 * @param modelYear the name of the year
	 */
	public SimpleModelYearImpl(Integer modelYear) {
		super();
		this.modelYear = modelYear;
	}
	public Integer getModelYear() {
		return modelYear;
	}
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildModelYear(modelYear);
	}
}
