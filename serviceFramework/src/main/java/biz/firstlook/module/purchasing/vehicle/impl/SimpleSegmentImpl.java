package biz.firstlook.module.purchasing.vehicle.impl;

import java.io.Serializable;

import biz.firstlook.module.purchasing.vehicle.AbstractSegment;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

/**
 * Simple bean implementation of the <code>Segment</code> interface.
 * @author swenmouth
 */
public class SimpleSegmentImpl extends AbstractSegment implements Serializable {
	private static final long serialVersionUID = -8505753177910256199L;
	private Integer segmentID;
	/**
	 * Allocates a new <code>Segment</code> with the argument ID.
	 * @param segmentID the ID of the segment
	 */
	public SimpleSegmentImpl(Integer segmentID) {
		this.segmentID = segmentID;
	}
	public String getName() {
		switch (getSegmentID()) {
		case 1:
			return "Unknown";
		case 2:
			return "Truck";
		case 3:
			return "Sedan";
		case 4:
			return "Coupe";
		case 5:
			return "Van";
		case 6:
			return "SUV";
		case 7:
			return "Convertible";
		case 8:
			return "Wagon";
		}
		throw new IllegalStateException("Undefined segment: " + getSegmentID());
	}
	public Integer getSegmentID() {
		return segmentID;
	}
	private Object readResolve() {
		return VehicleComponentBuilderFactory.getInstance().newVehicleBuilder().buildSegment(segmentID, getName());
	}
}
