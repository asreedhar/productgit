package biz.firstlook.module.purchasing.vehicle;

import biz.firstlook.commons.util.Functions;

/**
 * Abstract implementation of <code>ModelGroup</code> supplying <code>equals</code> and
 * <code>hashCode</code> implementations such that concrete instances need not necessarily
 * provide their own to be <code>Collection</code> safe.
 * @author swenmouth
 */
public abstract class AbstractModelGroup implements ModelGroup {
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof ModelGroup) == false)
			return false;
		ModelGroup m = (ModelGroup) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getModelGroupID(), m.getModelGroupID());
		lEquals &= Functions.nullSafeEquals(getName(), m.getName());
		return lEquals;
	}
	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getModelGroupID());
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getName());
		return hashCode;
	}
	public int compareTo(ModelGroup m) {
		if (m == null)
			return 1;
		int c = 0;
		c = Functions.nullSafeCompareTo(getModelGroupID(), m.getModelGroupID(), c);
		c = Functions.nullSafeCompareTo(getName(), m.getName(), c);
		return c;
	}
}
