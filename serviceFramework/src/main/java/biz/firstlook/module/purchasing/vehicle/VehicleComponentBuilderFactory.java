package biz.firstlook.module.purchasing.vehicle;


public abstract class VehicleComponentBuilderFactory {

	protected VehicleComponentBuilderFactory() {
		super();
	}

	public abstract VehicleComponentBuilder newVehicleBuilder();
	
	private static VehicleComponentBuilderFactory factory;
	
	public static VehicleComponentBuilderFactory getInstance() {
		return factory;
	}

	public static void setFactory(VehicleComponentBuilderFactory factory) {
		VehicleComponentBuilderFactory.factory = factory;
	}
	
}
