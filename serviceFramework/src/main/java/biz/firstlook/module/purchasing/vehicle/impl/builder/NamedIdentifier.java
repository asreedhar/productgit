package biz.firstlook.module.purchasing.vehicle.impl.builder;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.purchasing.vehicle.Line;

public class NamedIdentifier {
	private Integer id;
	private String name;
	public NamedIdentifier(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		else if (this == obj)
			return true;
		else if (obj instanceof Line) {
			Line line = (Line) obj;
			boolean lEquals = true;
			lEquals |= Functions.nullSafeEquals(id, line.getId());
			lEquals |= Functions.nullSafeEquals(id, line.getName());
			return lEquals;
		}
		else if (obj instanceof NamedIdentifier) {
			NamedIdentifier line = (NamedIdentifier) obj;
			boolean lEquals = true;
			lEquals |= Functions.nullSafeEquals(id, line.id);
			lEquals |= Functions.nullSafeEquals(id, line.name);
			return lEquals;
		}
		else {
			return false;
		}
	}
	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(id);
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(name);
		return hashCode;
	}
}
