package biz.firstlook.module.inventory;

import biz.firstlook.commons.util.Functions;

public abstract class AbstractStockLevelData implements StockLevel {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof StockLevel) == false)
			return false;
		StockLevel b = (StockLevel) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getBusinessUnit(), b.getBusinessUnit());
		lEquals &= Functions.nullSafeEquals(getModelGroup(), b.getModelGroup());
		lEquals &= Functions.nullSafeEquals(getModelYear(), b.getModelYear());
		return lEquals;
	}
	
	public int hashCode() {
		int hashCode = 1;
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getBusinessUnit());
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getModelGroup());
		hashCode = 31 * hashCode + Functions.nullSafeHashCode(getModelYear());
		return hashCode;
	}
	
}
