package biz.firstlook.module.inventory;

import biz.firstlook.commons.util.Functions;

public abstract class AbstractModelGroupContribution implements ModelGroupContribution {

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if ((obj instanceof ModelGroupContribution) == false)
			return false;
		ModelGroupContribution c = (ModelGroupContribution) obj;
		boolean lEquals = true;
		lEquals &= Functions.nullSafeEquals(getBusinessUnit(), c.getBusinessUnit());
		lEquals &= Functions.nullSafeEquals(getModelGroup(), c.getModelGroup());
		lEquals &= Functions.nullSafeEquals(getContribution(), c.getContribution());
		return lEquals;
	}

	public int hashCode() {
		int hashCode = 1;
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getBusinessUnit());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getModelGroup());
		hashCode = hashCode * 31 + Functions.nullSafeHashCode(getContribution());
		return hashCode;
	}

	public int compareTo(ModelGroupContribution modelGroupContribution) {
		int c = 0;
		final int contribution = Functions.nullSafeCompareTo(getContribution(), modelGroupContribution.getContribution(), c);
		c = (contribution != 0 ? -contribution : 0);
		c = Functions.nullSafeCompareTo(getModelGroup(), modelGroupContribution.getModelGroup(), c);
		return c;
	}
	
}
