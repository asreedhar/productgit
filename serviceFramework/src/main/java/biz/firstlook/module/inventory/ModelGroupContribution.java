package biz.firstlook.module.inventory;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;

public interface ModelGroupContribution extends Comparable<ModelGroupContribution> {
	public BusinessUnit getBusinessUnit();
	public ModelGroup getModelGroup();
	public Double getContribution();
}
