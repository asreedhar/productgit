package biz.firstlook.module.inventory.impl;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.inventory.AbstractModelGroupContribution;
import biz.firstlook.module.inventory.ModelGroupContribution;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;

public class SimpleModelGroupContribution extends AbstractModelGroupContribution implements ModelGroupContribution {

	private BusinessUnit businessUnit;
	private ModelGroup modelGroup;
	private Double contribution;
	
	public SimpleModelGroupContribution(BusinessUnit businessUnit, ModelGroup modelGroup, Double contribution) {
		this.businessUnit = businessUnit;
		this.modelGroup = modelGroup;
		this.contribution = contribution;
	}

	public BusinessUnit getBusinessUnit() {
		return businessUnit;
	}

	public Double getContribution() {
		return contribution;
	}

	public ModelGroup getModelGroup() {
		return modelGroup;
	}

}
