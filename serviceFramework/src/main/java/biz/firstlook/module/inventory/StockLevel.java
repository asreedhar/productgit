package biz.firstlook.module.inventory;

import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;

public interface StockLevel {
	public BusinessUnit getBusinessUnit();
	public ModelGroup getModelGroup();
	public ModelYear getModelYear();
	public Integer getUnitsInStock();
	public Integer getOptimalUnits();
    public void setOptimalUnits(Integer optimalUnits);
}
