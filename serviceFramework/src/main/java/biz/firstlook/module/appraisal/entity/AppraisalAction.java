package biz.firstlook.module.appraisal.entity;

public interface AppraisalAction extends Comparable<AppraisalAction>
{
	public Integer getId();
	public Integer getAppraisalId();
	public AppraisalActionType getAppraisalActionType();
}
