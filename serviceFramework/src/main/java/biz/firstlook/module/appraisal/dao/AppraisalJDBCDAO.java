package biz.firstlook.module.appraisal.dao;

import biz.firstlook.module.appraisal.entity.Appraisal;


/**
 * class defines methods use in appraisal service that are read-only
 * @author dweintrop
 *
 */
public interface AppraisalJDBCDAO
{
	public Appraisal doAppraisalLookUpByInventoryId( Integer inventoryId );
}
