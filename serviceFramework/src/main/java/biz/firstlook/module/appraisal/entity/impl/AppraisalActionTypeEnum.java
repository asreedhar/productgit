package biz.firstlook.module.appraisal.entity.impl;

import biz.firstlook.module.appraisal.entity.AppraisalActionType;

public enum AppraisalActionTypeEnum implements AppraisalActionType 
{
	PLACE_IN_RETAIL( 1, "Place in Retail Inventory" ),
	OFFER_TO_GROUP( 2, "Offer to Group (Wholesale)" ),
	WHOLESALE_OR_AUCTION( 3, "Sell to Wholesaler or Auction" ),
	NOT_TRADED_IN( 4, "Not Traded-In" ),
	DECIDE_LATER( 5, "Decide Later" ),
	AWAITING_APPRAISAL(6, "Awaiting Appraisal"),
	PURCHASE_WE_BUY( 7, "We Buy" ),
	PURCHASE_SERVICE_LANE( 8, "Service Lane" );

	private Integer Id;
	private String description;
	
	private AppraisalActionTypeEnum( Integer Id, String description )
	{
		this.Id = Id;
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}

	public Integer getId()
	{
		return Id;
	}
	
	public AppraisalActionType getAppraisalActionTypeById( int id )
	{
		AppraisalActionType toBeReturned;
		switch ( id ) 
		{
			case 1:
				toBeReturned = AppraisalActionTypeEnum.PLACE_IN_RETAIL;
				break;
			case 2:
				toBeReturned = AppraisalActionTypeEnum.OFFER_TO_GROUP;
				break;
			case 3:
				toBeReturned = AppraisalActionTypeEnum.WHOLESALE_OR_AUCTION;
				break;
			case 4:
				toBeReturned = AppraisalActionTypeEnum.NOT_TRADED_IN;
				break;
			case 5:
				toBeReturned = AppraisalActionTypeEnum.DECIDE_LATER;
				break;
			case 6:
			    toBeReturned = AppraisalActionTypeEnum.AWAITING_APPRAISAL;
			    break;
			case 7:
			    toBeReturned = AppraisalActionTypeEnum.PURCHASE_WE_BUY;
			    break;
			case 8:
			    toBeReturned = AppraisalActionTypeEnum.PURCHASE_SERVICE_LANE;
			    break;
			
			default:
				toBeReturned = null;
				break;
		}	
		return toBeReturned;
	}
	
}
