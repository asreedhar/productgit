package biz.firstlook.module.appraisal.entity.impl;

import java.util.Date;
import java.util.Map;

import biz.firstlook.module.appraisal.entity.AppraisalAction;

/**
 *	<b>This class is meant for lookups only.  Not for any transactional stuff!</b>  
 *	It is implemented as a wrapper around a map that is returned from a jdbc query using the ReportDisplayBeanRowMapper 
 * 	
 * 	@author dweintrop
 */
public class AppraisalLookUpImpl extends AbstractAppraisal
{

private Map<String, Object> appraisalData;

public AppraisalLookUpImpl( Map<String, Object> inAppaisalData )
{
	this.appraisalData = inAppaisalData;
}

public AppraisalAction getAppraisalAction()
{
	return new AppraisalActionLookUpImpl( appraisalData );
}

public Integer getBusinessUnitId()
{
	return (Integer)appraisalData.get( "BusinessUnitID" );
}

public Date getDateCrated()
{
	return (Date)appraisalData.get( "DateCreated" );
}

public Integer getId()
{
	return (Integer)appraisalData.get( "AppraisalID" );
}

}
