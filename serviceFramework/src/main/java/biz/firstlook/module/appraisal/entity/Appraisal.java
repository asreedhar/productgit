package biz.firstlook.module.appraisal.entity;

import java.util.Date;


public interface Appraisal extends Comparable<Appraisal> 
{
	public Integer getId();
	public AppraisalAction getAppraisalAction();
	public Integer getBusinessUnitId();
	public Date getDateCrated();
}
