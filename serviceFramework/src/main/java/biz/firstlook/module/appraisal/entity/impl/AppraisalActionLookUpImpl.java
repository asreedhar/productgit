package biz.firstlook.module.appraisal.entity.impl;

import java.util.Map;

import biz.firstlook.module.appraisal.entity.AppraisalActionType;

/**
 * <b>This class is meant for lookups only. Not for any transactional stuff!</b> It is implemented as a wrapper around a map that is returned
 * from a jdbc query using the ReportDisplayBeanRowMapper
 * 
 * @author dweintrop
 */
public class AppraisalActionLookUpImpl extends AbstractAppraisalAction
{

private Map< String, Object > appraisalActionData;

public AppraisalActionLookUpImpl( Map< String, Object > inAppaisalActionData )
{
	this.appraisalActionData = inAppaisalActionData;
}

public AppraisalActionType getAppraisalActionType()
{
	if ( appraisalActionData.get( "AppraisalActionTypeID" ) == null )
	{
		return null;
	}
	return AppraisalActionTypeEnum.DECIDE_LATER.getAppraisalActionTypeById( 
	              ( (Integer)appraisalActionData.get( "AppraisalActionTypeID" ) ).intValue() );
}

public Integer getAppraisalId()
{
	return (Integer)appraisalActionData.get( "AppraisalID" );
}

public Integer getId()
{
	return (Integer)appraisalActionData.get( "AppraisalActionID" );
}

}
