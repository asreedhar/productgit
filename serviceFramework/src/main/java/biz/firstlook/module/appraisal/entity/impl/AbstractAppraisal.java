package biz.firstlook.module.appraisal.entity.impl;

import biz.firstlook.module.appraisal.entity.Appraisal;

public abstract class AbstractAppraisal implements Appraisal
{

public boolean equals( Object obj )
{
	if ( obj == this )
		return true;
	if ( obj instanceof Appraisal )
	{
		Appraisal v = (Appraisal)obj;
		boolean lEquals = true;
		if ( getBusinessUnitId() == null )
		{
			lEquals &= ( v.getBusinessUnitId() == null );
		}
		else
		{
			lEquals &= getBusinessUnitId().equals( v.getBusinessUnitId() );
		}
		if ( getDateCrated() == null )
		{
			lEquals &= ( v.getDateCrated() == null );
		}
		else
		{
			lEquals &= getDateCrated().equals( v.getDateCrated() );
		}
		return lEquals;
	}
	return false;
}

public int hashCode()
{
	int hashCode = 1;
	hashCode = hashCode * 31 + ( getBusinessUnitId() == null ? 0 : getBusinessUnitId().hashCode() );
	hashCode = hashCode * 31 + ( getDateCrated() == null ? 0 : getDateCrated().hashCode() );
	return hashCode;
}

public int compareTo( Appraisal m )
{
	if ( m == null )
		return 1;
	int c = 0;
	if ( c == 0 )
	{
		if ( getBusinessUnitId() == null )
			c = 1;
		else
			c = getBusinessUnitId().compareTo( m.getBusinessUnitId() );
	}
	if ( c == 0 )
	{
		if ( getDateCrated() == null )
			c = 1;
		else
			c = getDateCrated().compareTo( m.getDateCrated() );
	}
	return c;
}

}
