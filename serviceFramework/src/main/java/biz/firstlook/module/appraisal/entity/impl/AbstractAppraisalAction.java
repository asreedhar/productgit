package biz.firstlook.module.appraisal.entity.impl;

import biz.firstlook.module.appraisal.entity.AppraisalAction;

public abstract class AbstractAppraisalAction implements AppraisalAction
{

public boolean equals( Object obj )
{
	if ( obj == this )
		return true;
	if ( obj instanceof AppraisalAction )
	{
		AppraisalAction v = (AppraisalAction)obj;
		boolean lEquals = true;
		if ( getAppraisalId() == null )
		{
			lEquals &= ( v.getAppraisalId() == null );
		}
		else
		{
			lEquals &= getAppraisalId().equals( v.getAppraisalId() );
		}
		return lEquals;
	}
	return false;
}

public int hashCode()
{
	int hashCode = 1;
	hashCode = hashCode * 31 + ( getAppraisalId() == null ? 0 : getAppraisalId().hashCode() );
	return hashCode;
}

public int compareTo( AppraisalAction m )
{
	if ( m == null )
		return 1;
	int c = 0;
	if ( c == 0 )
	{
		if ( getAppraisalId() == null )
			c = 1;
		else
			c = getAppraisalId().compareTo( m.getAppraisalId() );
	}
	return c;
}

}
