package biz.firstlook.module.appraisal.service;

import biz.firstlook.module.appraisal.entity.AppraisalActionType;

/**
 * the service through which all appraisal retrieval and persistence should take place
 * @author dweintrop
 *
 */
public interface IAppraisalService
{

/**
 * This method is used to retrieve an AppraisalActiontype record
 * 
 * @param vehicleId - vechile you're looking for @param BussinssUnitId - businessUnit you're in
 * @return found appraisal or null if not found
 */
public AppraisalActionType getAppraisalActionTypeByInventoryId( Integer inventoryId );

}
