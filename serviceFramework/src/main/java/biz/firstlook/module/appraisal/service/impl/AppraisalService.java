package biz.firstlook.module.appraisal.service.impl;

import biz.firstlook.module.appraisal.dao.AppraisalJDBCDAO;
import biz.firstlook.module.appraisal.entity.Appraisal;
import biz.firstlook.module.appraisal.entity.AppraisalActionType;
import biz.firstlook.module.appraisal.service.IAppraisalService;

public class AppraisalService implements IAppraisalService
{

private AppraisalJDBCDAO appraisalJDBCDAO;

// this is how i see the hibernate peice fitting in with the JDBC stuff - its not hooked up and not final:
// have an AppraisalHibernateDAO class live in the same package as the appraisalJDBCDAO both of which
// are injected into this service, this way the same service can handle persistent and lookup calls
// so it would have something like this: - DW
//private AppraisalHibernateDAO appraisalHibernateDAO;


public AppraisalActionType getAppraisalActionTypeByInventoryId( Integer inventoryId )
{
	Appraisal appraisal = getAppraisalJDBCDAO().doAppraisalLookUpByInventoryId( inventoryId );
	if ( appraisal == null || appraisal.getAppraisalAction() == null )
	{
		return null;
	}
	return appraisal.getAppraisalAction().getAppraisalActionType();
}

//public AppraisalHibernateDAO getAppraisalHibernateDAO()
//{
//	return appraisalHibernateDAO;
//}
//
//public void setAppraisalHibernateDAO( AppraisalHibernateDAO appraisalHibernateDAO )
//{
//	this.appraisalHibernateDAO = appraisalHibernateDAO;
//}

public AppraisalJDBCDAO getAppraisalJDBCDAO()
{
	return appraisalJDBCDAO;
}

public void setAppraisalJDBCDAO( AppraisalJDBCDAO appraisalJDBCDAO )
{
	this.appraisalJDBCDAO = appraisalJDBCDAO;
}

}
