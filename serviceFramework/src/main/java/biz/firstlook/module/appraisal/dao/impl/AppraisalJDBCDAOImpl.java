package biz.firstlook.module.appraisal.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.module.appraisal.dao.AppraisalJDBCDAO;
import biz.firstlook.module.appraisal.entity.Appraisal;
import biz.firstlook.module.appraisal.entity.impl.AppraisalLookUpImpl;
import biz.firstlook.report.reportFramework.ReportDisplayBeanRowMapper;

public class AppraisalJDBCDAOImpl extends JdbcDaoSupport implements AppraisalJDBCDAO
{

	@SuppressWarnings("unchecked")
	public Appraisal doAppraisalLookUpByInventoryId( Integer inventoryId )
	{
		StringBuilder sb = new StringBuilder("select a.*, aa.* from ");
		sb.append( " appraisals a ");
		sb.append( " join inventory i on i.vehicleId = a.vehicleId and i.businessUnitid = a.businessUnitId ");
		sb.append( " join appraisalActions aa on aa.appraisalId = a.appraisalId ");
		sb.append( " where i.inventoryId = ?" );
		sb.append( " order by aa.DateCreated desc");
		List<Map <String, Object> > results = (List<Map<String, Object> >)getJdbcTemplate().query( sb.toString(), 
		                             new Object[] { inventoryId }, new ReportDisplayBeanRowMapper( new ArrayList()));
		if ( results == null || results.isEmpty() )
		{
			return null;
		}
		return new AppraisalLookUpImpl( results.get( 0 ) );
	}

}
