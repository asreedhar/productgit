package biz.firstlook.module.appraisal.entity;

public interface AppraisalActionType
{
	public Integer getId();
	public String getDescription();
}
