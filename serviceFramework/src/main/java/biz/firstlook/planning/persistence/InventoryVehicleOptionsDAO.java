package biz.firstlook.planning.persistence;

import biz.firstlook.planning.action.InventoryVehicleInfoBean;

public interface InventoryVehicleOptionsDAO
{

InventoryVehicleInfoBean getVehicleInfo(Integer inventoryId);

InventoryVehicleInfoBean getVehicleInfo(Integer inventoryId, ThirdPartyEnumFilter... thirdPartyFitler);

}
