package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserHistory;

public class InternetAdvertiserHistoryDAO extends GenericHibernate<InternetAdvertiserHistory, Integer> 
        implements IGenericDAO<InternetAdvertiserHistory, Integer> {

    public InternetAdvertiserHistoryDAO() {
        super(InternetAdvertiserHistory.class);
    }

    @SuppressWarnings("unchecked")
    public List<InternetAdvertiserHistory> findInventoryAdvertiserHistory(
            Integer inventoryId, Integer advertiserId) {
        final StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserHistory IAH ");
        query.append("WHERE IAH.inventoryId = ? ");
        if (advertiserId != null) {
            query.append("AND IAH.advertiserId = ? ");
            // Only want to display the 5 most recent export dates for internet
            // advertising page.
            getHibernateTemplate().setMaxResults(5);
        }
        query.append("ORDER BY IAH.created DESC");

        Object[] params;
        if (advertiserId != null) {
            params = new Object[] { inventoryId, advertiserId };
        } else {
            params = new Object[] { inventoryId };
        }
        return getHibernateTemplate().find(query.toString(), params);
    }

}
