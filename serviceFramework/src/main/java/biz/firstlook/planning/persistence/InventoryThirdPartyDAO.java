package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public class InventoryThirdPartyDAO extends GenericHibernate< InventoryThirdParty, Integer > implements IInventoryThirdPartyDAO
{

public InventoryThirdPartyDAO()
{
	super( InventoryThirdParty.class );
}

@SuppressWarnings("unchecked")
public String findNameById( Integer id )
{
	StringBuilder query = new StringBuilder();
	query.append( "FROM biz.firstlook.planning.entity.thirdParty.InventoryThirdParty ITP " );
	query.append( "WHERE ITP.id = ?" );

	List results = getHibernateTemplate().find( query.toString(), id );

	if ( results != null && !results.isEmpty() )
	{
		InventoryThirdParty thirdParty = (InventoryThirdParty)results.get( 0 );
		return thirdParty.getName();
	}
	return null;
}

@SuppressWarnings("unchecked")
public Marketplace_InventoryThirdParty getMarketplace_InventoryThirdPartyByName( String marketplaceName )
{
	StringBuilder query = new StringBuilder();
	query.append( "FROM biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty marketplace " );
	query.append( "WHERE marketplace.inventoryThirdPartyTypeId = ? AND " );
	query.append( "		marketplace.name = ? " );

	List results = getHibernateTemplate().find( query.toString(),
												new Object[] { InventoryThirdPartyEnum.INTERNET_MARKETPLACE.getId(), marketplaceName } );

	if ( !results.isEmpty() )
	{
		return (Marketplace_InventoryThirdParty)results.get(0);
	}
	
	return null;
}
}
