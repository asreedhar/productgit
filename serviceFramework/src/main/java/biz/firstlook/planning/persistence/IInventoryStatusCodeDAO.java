package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.planning.entity.InventoryStatusCode;

public interface IInventoryStatusCodeDAO
{

public List<InventoryStatusCode> findAll();

}