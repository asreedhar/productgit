package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildLog;

public class InternetAdvertiserBuildLogDAO extends GenericHibernate<InternetAdvertiserBuildLog, Integer> 
implements IGenericDAO<InternetAdvertiserBuildLog, Integer> {

    public InternetAdvertiserBuildLogDAO() {
        super(InternetAdvertiserBuildLog.class);
    }
    
    @SuppressWarnings("unchecked")
    public InternetAdvertiserBuildLog findBuildLogsForExceptionReport(Integer advertiserId) {
        final StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildLog IBL ");
        query.append("WHERE IBL.advertiserId = ? ");
        query.append("ORDER BY IBL.created DESC");
        
        //Only want the most recent log.
        List<InternetAdvertiserBuildLog> results =  getHibernateTemplate().find(query.toString(), advertiserId);
        if (results != null && !results.isEmpty()){
            return results.get(0);
        }
        return null;
    }
}
