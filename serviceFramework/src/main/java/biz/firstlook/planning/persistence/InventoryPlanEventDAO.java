package biz.firstlook.planning.persistence;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;

@SuppressWarnings("unchecked")
public class InventoryPlanEventDAO extends GenericHibernate< InventoryPlanEvent, Integer > implements IInventoryPlanEventDAO
{

public InventoryPlanEventDAO()
{
	super( InventoryPlanEvent.class );
}

/**
 * returns a list of inventory aging plan events ordered from oldest to newest
 * excluding reminder date events
 * 
 * @param inventoryId
 * @return List<InventoryAgingPlanEvent>
 */
public List< InventoryPlanEvent > findPlanHistory( final Integer inventoryId )
{
	StringBuffer queryString = new StringBuffer();
	queryString.append( "from biz.firstlook.planning.entity.InventoryPlanEvent event " );
	queryString.append( "where event.inventoryId=? and event.eventTypeId != 14 ");
	queryString.append( "order by event.beginDate desc" );
	
	return getHibernateTemplate().find( queryString.toString(), inventoryId );
}

/**
 * Returns the events that were part created or modified
 * at the time the given inventory's plan was last saved
 * @param inventoryId
 * @return List< InventoryPlanEvent >
 */
public List< InventoryPlanEvent > findEventsFromLastPlan( Integer inventoryId )
{
    final StringBuffer query = new StringBuffer();
    query.append( "select * from AIP_Event where InventoryID=" );
    query.append( inventoryId );
    query.append( " and LastModified >= " );
    query.append( "(select max(LastModified) from AIP_Event where InventoryID=");
    query.append( inventoryId );
    query.append( ") and AIP_EventTypeID not in (11, 12, 14)"); // exclude service, detail, and reminder date events
    
	return (List) getHibernateTemplate().execute
	(
	    new HibernateCallback() 
	    {
	        public List doInHibernate(Session session) throws HibernateException 
	        {
	            return session.createSQLQuery( query.toString() )
	                          .addEntity( InventoryPlanEvent.class )
	                          .list();	                                                                 	
	        }	
	    }                                                             
	);	  
}

/**
 * loads all events for the given objective that have not ended
 * @param inventoryId
 * @param objectiveId
 * @return List< InventoryPlanEvent >
 */
public List< InventoryPlanEvent > findOpenEventsByObjective( Integer inventoryId, Integer objectiveId )
{
    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy");
    String today = dateFormat.format( DateUtils.truncate( new Date(), Calendar.DATE ) );
	
	final StringBuffer query = new StringBuffer();
    query.append( "select * from AIP_Event where InventoryID=" );
    query.append( inventoryId );
    query.append( " and ( EndDate is null OR EndDate > " );
    query.append( today );
    query.append( ") and AIP_EventTypeID in " );
    query.append( "(select AIP_EventTypeID from AIP_EventType where AIP_EventCategoryID=" );
    query.append( objectiveId );
    query.append( ")");
    
	return (List) getHibernateTemplate().execute
	(
	    new HibernateCallback() 
	    {
	        public List doInHibernate(Session session) throws HibernateException 
	        {
	            return session.createSQLQuery( query.toString() )
	                          .addEntity( InventoryPlanEvent.class )
	                          .list();	                                                                 	
	        }	
	    }                                                             
	);	  	
}

public InventoryPlanEvent findByInventoryIdAndCreatedDate( Integer inventoryId, Date createDate, Integer eventTypeId )
{
    final StringBuffer query = new StringBuffer();
    query.append( "from biz.firstlook.planning.entity.InventoryPlanEvent i where " );
    query.append( "i.inventoryId = ? and i.beginDate = ? and i.eventTypeId = ?" );
    
	List collection = getHibernateTemplate().find( query.toString(), new Object[] { inventoryId, createDate, eventTypeId } );
	
	if ( collection != null && !collection.isEmpty() )
	{
		InventoryPlanEvent event = (InventoryPlanEvent)collection.get( 0 );
		return event;
	}
	return null;
}


public List< InventoryPlanEvent > findEventsByThirdPartyId( Integer businessUnitid, List< InventoryThirdParty > toBeDeletedThirdParties )
{
	StringBuilder query = new StringBuilder( "from biz.firstlook.planning.entity.InventoryPlanEvent ipe ");
	query.append( "where ipe.inventoryThirdParty.id in ( " );
	for (InventoryThirdParty event : toBeDeletedThirdParties )
	{
		query.append( event.getId() + " ," );
	}
	// remove the last ,
	query.deleteCharAt( query.length() -1 );
	query.append( "  )" );
	return getHibernateTemplate().find( query.toString() );
}

public List< InventoryPlanEvent> retrievePrintAdvertiserEventsGroupedByEndDate( Integer inventoryId )
{
	StringBuffer queryString = new StringBuffer();
	queryString.append( "from biz.firstlook.planning.entity.InventoryPlanEvent event " );
	queryString.append( "where event.inventoryId=? " );
	queryString.append( "and event.endDate <  ? ");
	queryString.append( "and event.planningEventType.id =  ? ");
	queryString.append( "order by event.endDate desc, event.inventoryThirdParty.name" );
	
	return getHibernateTemplate().find( queryString.toString(), new Object[]{inventoryId, new Date(), PlanningEventTypeEnum.ADVERTISEMENT.getId() } );
}

public List< InventoryPlanEvent > findAllEventsByTypeInTimeRange( Integer businessUnitId, Date plansAfterThisDate )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select event " );
	hqlQuery.append( "from biz.firstlook.planning.entity.InventoryPlanEvent as event, " );
	hqlQuery.append( " 	  biz.firstlook.entity.Inventory as inventory " );
	hqlQuery.append( "where inventory.businessUnitId = :businessUnitId " );
	hqlQuery.append( "and event.inventoryId = inventory.id ");
	hqlQuery.append( "and event.endDate >  :plansAfterThisDate ");
	hqlQuery.append( "and event.planningEventType.id =  :planningTypeId ");
	hqlQuery.append( "and event.inventoryThirdParty is not null ");
	
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "plansAfterThisDate", plansAfterThisDate );
	query.setParameter( "businessUnitId", businessUnitId );
	query.setParameter( "planningTypeId", PlanningEventTypeEnum.ADVERTISEMENT.getId() );

	return query.list();
}

public InventoryPlanEvent findByIdandEventType( Integer inventoryId, PlanningEventTypeEnum eventType )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "from biz.firstlook.planning.entity.InventoryPlanEvent event " );
	hqlQuery.append( "where 	event.inventoryId = :inventoryId and ");
	hqlQuery.append( " 		event.eventTypeId = :eventTypeId ");
	hqlQuery.append( "order by event.beginDate desc" );
	
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "inventoryId", inventoryId );
	query.setParameter( "eventTypeId", eventType.getId() );

	List< InventoryPlanEvent > results = query.list();
	InventoryPlanEvent lastPost = null;
	if ( !results.isEmpty() )
	{
		lastPost = results.get( 0 );
	}
	
	return lastPost;
}

    public InventoryPlanEvent findSpiffEventByInvIdAndEventType(final Integer inventoryId, final PlanningEventTypeEnum eventType) {
        List<InventoryPlanEvent> events = (List<InventoryPlanEvent>) getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session)throws HibernateException, SQLException {
                Criteria crit = session.createCriteria(InventoryPlanEvent.class)
                    .add(Restrictions.eq("eventTypeId", eventType.getId()))
                    .add(Restrictions.eq("inventoryId", inventoryId))
                    .addOrder(Order.desc("created"))
                    .addOrder(Order.desc("id"));
                return crit.list();
            }
        });
        
        Date today = DateUtils.truncate(new Date(), Calendar.DATE);
        
        if(events != null) {
        	InventoryPlanEvent latestEvent = null;
        	for(InventoryPlanEvent event : events) {
        		//assumes that list is in descending order for the field "created"
        		if(latestEvent == null) { 
        			latestEvent = event;        			
        		} else {
        			if (event.getEndDate() == null) {
        				event.setEndDate(today);
        			}
        			
        			//assumes other code will update the latest event's end date.
        			if(event.getId().compareTo(latestEvent.getId()) > 0) {
        				latestEvent.setEndDate(latestEvent.getEndDate() == null ? today : latestEvent.getEndDate());
        				latestEvent = event;
        			}
        		}
        	}
        	return latestEvent; 
        }
        return null;
    }
   
}
