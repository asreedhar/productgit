package biz.firstlook.planning.persistence;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.action.InventoryVehicleInfoBean;

public class InventoryVehicleInformationStoredProc extends JdbcTemplate implements InventoryVehicleOptionsDAO
{

private static final String CALL_STRING = "{? = call GetInventoryVehicleInformationWithOptionsMultiSet( ? )}";

private static final String PARAM_RC = "RC";
private static final String PARAM_INVENTORY_ID = "inventoryID";

public InventoryVehicleInfoBean getVehicleInfo(Integer inventoryId){
	List<ThirdPartyEnumFilter> filters = new ArrayList< ThirdPartyEnumFilter >();
	for(ThirdPartyEnum tpEnum : ThirdPartyEnum.values()) {
		filters.add( new ThirdPartyEnumFilter(tpEnum) );
	}
	return getVehicleInfo(inventoryId, filters.toArray(new ThirdPartyEnumFilter[filters.size()]));
}

public InventoryVehicleInfoBean getVehicleInfo( Integer inventoryId, ThirdPartyEnumFilter... thirdPartyFitler )
{
	List<SqlParameter> declaredParams = new ArrayList< SqlParameter >();
	declaredParams.add( new SqlOutParameter(PARAM_RC, Types.INTEGER) );
	declaredParams.add( new SqlParameter(PARAM_INVENTORY_ID, Types.INTEGER) );
	
	CallableStatementCreatorFactory factory = new CallableStatementCreatorFactory(CALL_STRING, declaredParams);
	
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put( PARAM_INVENTORY_ID, inventoryId );
	CallableStatementCreator callableStatementCreator = factory.newCallableStatementCreator( parameters );
	
	InventoryVehicleInfoBean infoBean = (InventoryVehicleInfoBean)execute( callableStatementCreator, new InventoryVehicleOptionsStatementCallback(thirdPartyFitler) );	
	return infoBean;
}

private class InventoryVehicleOptionsStatementCallback implements CallableStatementCallback
{

private Map<ThirdPartyEnum, ThirdPartyEnumFilter> filters = new HashMap< ThirdPartyEnum, ThirdPartyEnumFilter >();

public InventoryVehicleOptionsStatementCallback(ThirdPartyEnumFilter... thirdPartyFitlers) {
	for(ThirdPartyEnumFilter filter : thirdPartyFitlers) {
		filters.put( filter.getThirdPartyEnum(), filter);
	}
}

public Object doInCallableStatement( CallableStatement statement ) throws SQLException, DataAccessException
{
	InventoryVehicleInfoBean infoBean = null;
	if(!statement.execute())
		throw new SQLException("Expected a ResultSet");
	
	int resultSetNum = 0;
	do {
		ResultSet rs = statement.getResultSet();
		if(rs != null ) {
			while(rs.next()) {
				if(resultSetNum == 0) {
					infoBean = new InventoryVehicleInfoBean();
					infoBean.setVehicleDescription( rs.getString( "vehicleDescription" ) );
					infoBean.setMake( rs.getString( "make" ) );
					infoBean.setYear( Integer.valueOf( rs.getInt( "year" ) ) );
					infoBean.setVin( rs.getString( "vin" ) );
					infoBean.setColor( rs.getString( "color" ) );
					infoBean.setStockNumber( rs.getString( "stockNumber" ) );
					infoBean.setMileage( Integer.valueOf( rs.getInt( "mileagereceived" ) ) );
					resultSetNum++;
				} else {
					ThirdPartyEnum tpEnum = ThirdPartyEnum.getEnumById( Integer.valueOf( rs.getInt( "ThirdPartyID" ) ) );
					ThirdPartyEnumFilter filter = filters.get( tpEnum );
					if(filter != null && filter.accept( tpEnum )) {
						infoBean.add( tpEnum, rs.getString( "Description" ), rs.getString( "OptionName" ), rs.getBoolean( "Status" ) );
					}
				}
			}
		}
	} while(statement.getMoreResults());

	return infoBean;
}
}

}
