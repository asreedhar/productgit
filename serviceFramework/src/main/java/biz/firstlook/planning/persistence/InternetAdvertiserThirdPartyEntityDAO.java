package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserThirdPartyEntity;

public class InternetAdvertiserThirdPartyEntityDAO extends GenericHibernate <InternetAdvertiserThirdPartyEntity, Integer> 
implements IGenericDAO<InternetAdvertiserThirdPartyEntity, Integer>{

    public InternetAdvertiserThirdPartyEntityDAO() {
        super(InternetAdvertiserThirdPartyEntity.class);
    }
    
    @SuppressWarnings("unchecked")
    public List<InternetAdvertiserThirdPartyEntity> retrieveAll() {
        StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserThirdPartyEntity ITE ");
        
        return getHibernateTemplate().find(query.toString());
    }
    
    @SuppressWarnings("unchecked")
    public Integer findAdvertiserIdForDatafeed(String datafeedCode) {
        StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserThirdPartyEntity ITE ");
        query.append("WHERE ITE.id.datafeedCode = ? ");
        
        List<InternetAdvertiserThirdPartyEntity> results = getHibernateTemplate().find(query.toString(), datafeedCode);
        if (results != null && !results.isEmpty()) {
            return results.get(0).getInternetAdvertiserId();
        }
        return null;
    }

}
