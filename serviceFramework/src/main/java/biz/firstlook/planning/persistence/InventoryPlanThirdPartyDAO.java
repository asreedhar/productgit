package biz.firstlook.planning.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;

public class InventoryPlanThirdPartyDAO extends GenericHibernate< InventoryThirdParty, Integer > implements IInventoryPlanThirdPartyDAO
{

public InventoryPlanThirdPartyDAO()
{
	super( InventoryThirdParty.class );
}

@SuppressWarnings("unchecked")
public List< InventoryThirdParty > findAuctionsAndWholesalers( Integer businessUnitId )
{
	StringBuffer query = new StringBuffer();
	query.append( "from biz.firstlook.planning.entity.thirdParty.InventoryThirdParty tp " );
	query.append( "where tp.businessUnitId=? and " );
	query.append( "(tp.inventoryThirdPartyTypeId=? or tp.inventoryThirdPartyTypeId=? )" );

	Integer auction = InventoryThirdPartyEnum.AUCTION.getId();
	Integer wholesale = InventoryThirdPartyEnum.WHOLESALER.getId();
	
	return getHibernateTemplate().find( query.toString(), new Object[]{ businessUnitId, wholesale, auction } );
}

@SuppressWarnings("unchecked")
public List<InventoryThirdParty> findThirdParties( List<Integer> thirdPartyIds )
{
	if ( thirdPartyIds.size() > 0 )
	{
		StringBuffer query = new StringBuffer();
		query.append( "from biz.firstlook.planning.entity.thirdParty.InventoryThirdParty tp " );
		query.append( "where " );
	
		Iterator<Integer> idIter = thirdPartyIds.iterator();
		while ( idIter.hasNext() )
		{
			query.append( "tp.id=" );
			query.append( idIter.next().toString() );
			if ( idIter.hasNext() )
			{
				query.append( " or " );
			}
		}
	
		return getHibernateTemplate().find( query.toString() );
	} 
	else
	{
		return new ArrayList<InventoryThirdParty>();
	}
}

}
