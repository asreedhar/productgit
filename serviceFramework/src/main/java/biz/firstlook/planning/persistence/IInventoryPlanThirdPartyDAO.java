package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;

public interface IInventoryPlanThirdPartyDAO
{
	public List< InventoryThirdParty > findAuctionsAndWholesalers( Integer businessUnitId );
	public List<InventoryThirdParty> findThirdParties ( List<Integer> thirdPartyIds );
}
