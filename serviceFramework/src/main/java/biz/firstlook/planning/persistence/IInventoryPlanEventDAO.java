package biz.firstlook.planning.persistence;

import java.util.Date;
import java.util.List;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;

public interface IInventoryPlanEventDAO extends IGenericDAO<InventoryPlanEvent,Integer>
{

public List< InventoryPlanEvent > findPlanHistory( final Integer inventoryId );
public List< InventoryPlanEvent > findEventsFromLastPlan( Integer inventoryId );
public List< InventoryPlanEvent > findOpenEventsByObjective( Integer inventoryId, Integer objectiveId );
public InventoryPlanEvent findByInventoryIdAndCreatedDate( Integer inventoryId, Date createDate, Integer eventTypeId );
public List<InventoryPlanEvent> findEventsByThirdPartyId( Integer businessUnitId, List< InventoryThirdParty > toBeDeletedThirdParties );
public List< InventoryPlanEvent > retrievePrintAdvertiserEventsGroupedByEndDate( Integer inventoryId );
public List< InventoryPlanEvent > findAllEventsByTypeInTimeRange( Integer businessUnitId, Date thirtyDaysAgo );
public InventoryPlanEvent findSpiffEventByInvIdAndEventType(final Integer inventoryId, final PlanningEventTypeEnum eventType);
public InventoryPlanEvent findByIdandEventType( Integer inventoryId, PlanningEventTypeEnum internet_marketplace );

}