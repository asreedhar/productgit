package biz.firstlook.planning.persistence;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public interface IInventoryThirdPartyDAO extends IGenericDAO<InventoryThirdParty, Integer>{

    public String findNameById(Integer id);
    public Marketplace_InventoryThirdParty getMarketplace_InventoryThirdPartyByName( String marketplaceName );
}
