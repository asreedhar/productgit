package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookup;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;

public class PrintAdvertiserDAO extends GenericHibernate< PrintAdvertiser_InventoryThirdParty, Integer >
{

public PrintAdvertiserDAO()
{
	super( PrintAdvertiser_InventoryThirdParty.class );
}

@SuppressWarnings( "unchecked" )
public List< PrintAdvertiser_InventoryThirdParty > retreivePrintAdvertisers( Integer businessUnitId )
{
	StringBuffer queryString = new StringBuffer();
	queryString.append( "from biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty printAdvertiser " );
	queryString.append( "where printAdvertiser.businessUnitId=? " );
	queryString.append( "and printAdvertiser.inventoryThirdPartyTypeId = ?" );

	return getHibernateTemplate().find( queryString.toString(),
										new Object[] { businessUnitId, InventoryThirdPartyEnum.PRINT_ADVERTISER.getId() } );
}

@SuppressWarnings("unchecked")
public List<PrintAdvertiser_InventoryThirdParty> retrieveQuickPrintAdvertisers(Integer businessUnitId) {
    StringBuffer queryString = new StringBuffer();
    queryString.append( "from biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty printAdvertiser " );
    queryString.append( "where printAdvertiser.businessUnitId=? " );
    queryString.append( "and printAdvertiser.quickAdvertiser = ?" );

    return getHibernateTemplate().find( queryString.toString(),
                                        new Object[] { businessUnitId, Boolean.TRUE } );
}

@SuppressWarnings( "unchecked" )
public List< PrintAdvertiserOptionLookup > retrievePrintAdvertiserTextLookupOptions()
{
	return getHibernateTemplate().find( "from biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookup options" );
}

}
