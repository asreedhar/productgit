package biz.firstlook.planning.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.planning.action.InventoryExtractDestinationEnum;


public class InventoryDestinationStatusRetriever extends JdbcDaoSupport
{

	//These can be bumped up to a super class
	protected static final String RESULTS = "@RESULTS";
	protected static final String RETURN_CODE = "@RC";
	
	//These should be kept private
	private static final String INVENTORY_ID = "@InventoryID";
	private static final String EXPORT = "@Export";
	private static final String DEALERID = "@DealerID";
	private static final String DESTINATIONNAME = "@DestinationName";

	
	@SuppressWarnings("unchecked")
	public Boolean readExportInformation( int inventoryId, InventoryExtractDestinationEnum destination )
	{
		boolean exportStatus = false;
		
		List<SqlParameter> storedProcedureParameters = new ArrayList<SqlParameter>();
	
		// Parameters of a stored proc
		storedProcedureParameters.add( new SqlReturnResultSet( InventoryDestinationStatusRetriever.RESULTS, 
				new InventoryInGroupExportRowMapper() ) );
		
		// This stores the return code from a stored proc
		storedProcedureParameters.add( new SqlOutParameter( InventoryDestinationStatusRetriever.RETURN_CODE, Types.INTEGER ) );
	
		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.INVENTORY_ID, Types.INTEGER ) );
		
		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.DESTINATIONNAME, Types.VARCHAR ) );
	
		CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{? = call Extract.InventoryDestinationStatus#Fetch (?,?)}", 
				storedProcedureParameters );
		
		Map<String, Object> storedProcParameters = new HashMap<String, Object>();
		
		storedProcParameters.put( InventoryDestinationStatusRetriever.INVENTORY_ID, inventoryId );

		storedProcParameters.put( InventoryDestinationStatusRetriever.DESTINATIONNAME, destination.getDescription());

		CallableStatementCreator csc = cscf.newCallableStatementCreator( storedProcParameters );
		
		Map results = getJdbcTemplate().call( csc, storedProcedureParameters );
		
		if ( results.containsKey( InventoryDestinationStatusRetriever.RETURN_CODE ) )
		{
			Integer returnCode = (Integer)results.get( InventoryDestinationStatusRetriever.RETURN_CODE );
	
			if ( returnCode.intValue() == 0 )
			{
				List<Boolean> status = (List<Boolean>)results.get(InventoryDestinationStatusRetriever.RESULTS);
				
				if (status != null && !status.isEmpty()){
					exportStatus = status.get(0).booleanValue();
				}				
			}
			else
			{
				//Throw some nasty errors for fun.
			}
		}
		else
		{
			//Something is seriously wrong here
		}
		
		return exportStatus;
	}
	
	@SuppressWarnings("unchecked")
	public void updateOrCreateExportInformation( int inventoryId, InventoryExtractDestinationEnum destination, boolean exportStatus )
	{
		
		List<SqlParameter> storedProcedureParameters = new ArrayList<SqlParameter>();
	
		// This stores the return code from a stored proc
		storedProcedureParameters.add( new SqlOutParameter( InventoryDestinationStatusRetriever.RETURN_CODE, Types.INTEGER ) );
	
		// Parameters of a stored proc
		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.INVENTORY_ID, Types.INTEGER ) );
		
		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.DESTINATIONNAME, Types.VARCHAR ) );
		
		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.EXPORT, Types.BIT ) );
	
		CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{? = call Extract.InventoryDestinationStatus#Upsert (?,?,?)}",
				storedProcedureParameters );
		
		Map<String, Object> storedProcParameters = new HashMap<String, Object>();
		
		storedProcParameters.put( InventoryDestinationStatusRetriever.INVENTORY_ID, inventoryId );
		
		storedProcParameters.put( InventoryDestinationStatusRetriever.DESTINATIONNAME, destination.getDescription());

		storedProcParameters.put( InventoryDestinationStatusRetriever.EXPORT, exportStatus );
	
		CallableStatementCreator csc = cscf.newCallableStatementCreator( storedProcParameters );
		
		Map results = getJdbcTemplate().call( csc, storedProcedureParameters );
		
		if ( results.containsKey( InventoryDestinationStatusRetriever.RETURN_CODE ) )
		{
			Integer returnCode = (Integer)results.get( InventoryDestinationStatusRetriever.RETURN_CODE );
	
			if ( returnCode.intValue() == 0 )
			{
				//Nothing to do
			}
			else
			{
				//Throw some nasty errors for fun.
			}
		}
		else
		{
			//Something is seriously wrong here
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean exportAllowed( int dealerId, InventoryExtractDestinationEnum destination )
	{
		boolean isExportAllowed = false;
		
		List<SqlParameter> storedProcedureParameters = new ArrayList<SqlParameter>();
	
		// Parameters of a stored proc
		storedProcedureParameters.add( new SqlReturnResultSet( InventoryDestinationStatusRetriever.RESULTS, 
				new DealerConfigurationRowMapper() ) );
		
		// This stores the return code from a stored proc
		storedProcedureParameters.add( new SqlOutParameter( InventoryDestinationStatusRetriever.RETURN_CODE, Types.INTEGER ) );
	
		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.DEALERID, Types.INTEGER ) );

		storedProcedureParameters.add( new SqlParameter( InventoryDestinationStatusRetriever.DESTINATIONNAME, Types.VARCHAR ) );

		
		CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{? = call Extract.DealerConfiguration#Exists (?,?)}", 
				storedProcedureParameters );
		
		Map<String, Object> storedProcParameters = new HashMap<String, Object>();
		
		storedProcParameters.put( InventoryDestinationStatusRetriever.DEALERID, dealerId );
		
		storedProcParameters.put( InventoryDestinationStatusRetriever.DESTINATIONNAME, destination.getDescription());
		
		CallableStatementCreator csc = cscf.newCallableStatementCreator( storedProcParameters );
		
		Map results = getJdbcTemplate().call( csc, storedProcedureParameters );
		
		if ( results.containsKey( InventoryDestinationStatusRetriever.RETURN_CODE ) )
		{
			Integer returnCode = (Integer)results.get( InventoryDestinationStatusRetriever.RETURN_CODE );
	
			if ( returnCode.intValue() == 0 )
			{
				List<Boolean> status = (List<Boolean>)results.get(InventoryDestinationStatusRetriever.RESULTS);
				
				if (status != null && !status.isEmpty()){
					isExportAllowed = ((Boolean)status.get(0)).booleanValue();
				}				
			}
			else
			{
				//Throw some nasty errors for fun.
			}
		}
		else
		{
			//Something is seriously wrong here
		}
		
		return isExportAllowed;
	}

	
	class InventoryInGroupExportRowMapper implements RowMapper
	{
		public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
		{
			Boolean result = rs.getBoolean("Export");
		
			return result;
		}
	}
	
	class DealerConfigurationRowMapper implements RowMapper
	{
		public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
		{
			Boolean result = rs.getBoolean("Exists");
		
			return result;
		}
	}

}