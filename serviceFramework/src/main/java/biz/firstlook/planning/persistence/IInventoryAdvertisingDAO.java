package biz.firstlook.planning.persistence;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.planning.entity.InventoryAdvertising;
import biz.firstlook.planning.entity.InventoryEventTypeDefaultPK;

public interface IInventoryAdvertisingDAO extends IGenericDAO<InventoryAdvertising, InventoryEventTypeDefaultPK>
{
public InventoryAdvertising load( Integer inventoryId );
}
