package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBusinessUnit;

public class InternetAdvertiserBusinessUnitDAO extends GenericHibernate<InternetAdvertiserBusinessUnit, Integer> 
        implements IGenericDAO<InternetAdvertiserBusinessUnit, Integer>{

    public InternetAdvertiserBusinessUnitDAO() {
        super(InternetAdvertiserBusinessUnit.class);
    }
    
    /**
     * @param businessUnitId
     * @return the list of advertisers that are active for the given dealership.
     */
    @SuppressWarnings("unchecked")
    public List<InternetAdvertiserBusinessUnit> findBusinessUnitAdvertisers(Integer businessUnitId) {
        StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBusinessUnit IAD ");
        query.append("WHERE IAD.id.businessUnitId = ? and (IAD.exporting = ? or IAD.incrementalExporting = ?)");

        return getHibernateTemplate().find(query.toString(), new Object[]{businessUnitId, true, true});
    }
    
    @SuppressWarnings("unchecked")
    public List<InternetAdvertiserBusinessUnit> findBusinessUnitsForAdvertiserId(Integer advertiserId) {
        StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBusinessUnit IAD ");
        query.append("WHERE IAD.id.advertiserId = ?  ");

        return getHibernateTemplate().find(query.toString(), advertiserId);
    }

}
