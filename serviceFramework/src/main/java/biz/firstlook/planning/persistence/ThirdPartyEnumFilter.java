package biz.firstlook.planning.persistence;

import biz.firstlook.commons.collections.Filter;
import biz.firstlook.main.enumerator.ThirdPartyEnum;

public class ThirdPartyEnumFilter implements Filter<ThirdPartyEnum>
{

private ThirdPartyEnum include;

public ThirdPartyEnumFilter(ThirdPartyEnum include) {
	this.include = include;
}

public boolean accept( ThirdPartyEnum element )
{
	return element.equals( include );
}

public ThirdPartyEnum getThirdPartyEnum()
{
	return include;
}

}
