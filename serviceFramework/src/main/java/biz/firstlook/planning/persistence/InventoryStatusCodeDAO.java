package biz.firstlook.planning.persistence;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.planning.entity.InventoryStatusCode;

public class InventoryStatusCodeDAO extends HibernateDaoSupport implements IInventoryStatusCodeDAO
{

public InventoryStatusCodeDAO()
{
	super();
}

@SuppressWarnings("unchecked")
public List<InventoryStatusCode> findAll()
{
	Query inventoryStatusCodeQuery = getSession().createQuery( " from biz.firstlook.planning.entity.InventoryStatusCode" );
	inventoryStatusCodeQuery.setCacheable( true );
	return inventoryStatusCodeQuery.list();
}

}
