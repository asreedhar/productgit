package biz.firstlook.planning.persistence;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.PlanningEventType;

public class PlanningEventTypeHibernateDAO extends GenericHibernate<PlanningEventType,Integer> implements IPlanningEventTypeDAO
{

public PlanningEventTypeHibernateDAO()
{
	super( PlanningEventType.class );
}

}
