package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.InventoryAdvertising;
import biz.firstlook.planning.entity.InventoryEventTypeDefaultPK;

public class InventoryAdvertisingDAO extends GenericHibernate<InventoryAdvertising, InventoryEventTypeDefaultPK> implements IInventoryAdvertisingDAO
{

public InventoryAdvertisingDAO()
{
	super(InventoryAdvertising.class);
}

@SuppressWarnings("unchecked")
public InventoryAdvertising load( Integer inventoryId )
{
    StringBuffer query = new StringBuffer();
    query.append( "from biz.firstlook.planning.entity.InventoryAdvertising i where " );
    query.append( "i.inventoryId = ?  " );
    
    List collection = getHibernateTemplate().find( query.toString(), new Object[] { inventoryId  } );
	
	if ( collection != null && !collection.isEmpty() )
	{
		InventoryAdvertising eventTypeDefault = (InventoryAdvertising)collection.get( 0 );
		return eventTypeDefault;
	}
	return null;
}
}
