package biz.firstlook.planning.persistence;

import java.util.List;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildList;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildListPk;

public class InternetAdvertiserBuildListDAO extends GenericHibernate<InternetAdvertiserBuildList, InternetAdvertiserBuildListPk> 
    implements IGenericDAO<InternetAdvertiserBuildList, InternetAdvertiserBuildListPk>{

    public InternetAdvertiserBuildListDAO() {
        super(InternetAdvertiserBuildList.class);
    }

    @SuppressWarnings("unchecked")
    public List<InternetAdvertiserBuildList> findInventoryAdvertisers(Integer id) {
        StringBuilder query = new StringBuilder();
        query.append("FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildList IAB ");
        query.append("WHERE IAB.id.inventoryId = ?");

        return getHibernateTemplate().find(query.toString(), id);
    }
    
    @SuppressWarnings("unchecked")
	public InternetAdvertiserBuildList load(Integer inventoryId, Integer advertiserId) {
        StringBuffer query = new StringBuffer();
        query.append( "FROM biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildList IAB WHERE " );
        query.append( "IAB.id.inventoryId = ? and IAB.id.advertiserId = ?  " );
        
        List collection = getHibernateTemplate().find( query.toString(), new Object[]{inventoryId, advertiserId} );
        
        if ( collection != null && !collection.isEmpty() )
        {
            InternetAdvertiserBuildList advertiser = (InternetAdvertiserBuildList)collection.get( 0 );
            return advertiser;
        }
        return null;
    }
    
}
