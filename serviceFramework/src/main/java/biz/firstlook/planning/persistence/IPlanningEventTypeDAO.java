package biz.firstlook.planning.persistence;

import biz.firstlook.main.persist.common.IGenericDAO;
import biz.firstlook.planning.entity.PlanningEventType;

public interface IPlanningEventTypeDAO extends IGenericDAO<PlanningEventType,Integer>
{
}
