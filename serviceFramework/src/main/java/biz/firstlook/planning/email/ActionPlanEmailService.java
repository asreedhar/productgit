package biz.firstlook.planning.email;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.FileSystemResource;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.report.action.ActionPlanReport;
import biz.firstlook.report.dao.IBasicIMTLookUpDAO;

public class ActionPlanEmailService
{

private IBasicIMTLookUpDAO basicIMTLookUpDAO;
private ActionPlanPdfBuilderService actionPlanPdfBuilder;
private EmailService emailService;

    public void createAndSendEmail(List<String> toAddresses, Integer thirdPartyId, Integer businessUnitId, Integer memberId, String subject,
            String userEnteredBodyText, String replyToAddress, String reportType, boolean sendHtml, Object results,
            String primaryBook, String book1Pref1, String book1Pref2, String dealerName, String reportName) {

        ActionPlanEmailBuilder emailBuilder = new ActionPlanEmailBuilder(toAddresses, subject,replyToAddress, userEnteredBodyText, reportType, 
                sendHtml, results, primaryBook, book1Pref1, book1Pref2);

        String contactInfo = getContactInfo(businessUnitId, memberId);
        emailBuilder.setContactInfo(contactInfo);
        emailBuilder.getRecipientsAndContext();

        if (!sendHtml) {
            File pdfFile = actionPlanPdfBuilder.buildPdf(emailBuilder.getPlanItems(),
                    ActionPlanReport.valueFromDescription(reportType), dealerName, reportName);
            FileSystemResource resource = new FileSystemResource(pdfFile);
            emailBuilder.putResource("pdf", resource, "application/pdf");
        }
        emailService.sendEmail(emailBuilder);
    }

    // should be private, but protected for testing purposes
    protected String getContactInfo(Integer businessUnitId, Integer memberId) {
        StringBuilder query = new StringBuilder();
        query.append(" select 	bu.BusinessUnit, bu.Address1, bu.Address2, bu.City, bu.State, bu.ZipCode, ");
        query.append(" 		m.FirstName, m.LastName, m.OfficePhoneNumber ");
        query.append(" from 	BusinessUnit bu, Member m ");
        query.append(" where	bu.businessUnitId = ? and m.memberId = ?");

        List<Map<String, Object>> results = basicIMTLookUpDAO.getResults(query.toString(), new Object[] { businessUnitId, memberId });
        Map<String, Object> row = results.get(0);
        StringBuilder emailHeader = new StringBuilder();
        emailHeader.append(row.get("FirstName") + " " + row.get("LastName")+ " - ");

        String phoneNumber = row.get("OfficePhoneNumber").toString();
        emailHeader.append("(" + phoneNumber.substring(0, 3) + ") "+ phoneNumber.substring(3, 6));
        emailHeader.append("-" + phoneNumber.substring(6, 10));

        // check to see if there is an extension
        if (phoneNumber.length() > 10) {
            emailHeader.append(" ext. " + phoneNumber.substring(10));
        }
        emailHeader.append("\n");
        emailHeader.append(row.get("BusinessUnit") + "\n");
        emailHeader.append(row.get("Address1") + " " + row.get("Address2")+ "\n");
        emailHeader.append(row.get("City") + ", " + row.get("State") + " "+ row.get("ZipCode") + "\n");

        return emailHeader.toString();
    }

    public void setBasicIMTLookUpDAO(IBasicIMTLookUpDAO basicIMTLookUpDAO) {
        this.basicIMTLookUpDAO = basicIMTLookUpDAO;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setActionPlanPdfBuilder(
            ActionPlanPdfBuilderService actionPlanPdfBuilder) {
        this.actionPlanPdfBuilder = actionPlanPdfBuilder;
    }


}


