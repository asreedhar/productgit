package biz.firstlook.planning.email;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;
import biz.firstlook.report.action.ActionPlanReport;
 
public class ActionPlanEmailBuilder implements EmailContextBuilder{
	
	private static final Logger log = Logger.getLogger(ActionPlanEmailBuilder.class);
	
	private final String imageLoc = "http://images.firstlook.biz/fldn_logo.gif";
	private List<String> recipients;
    private List< Map<String, Object> > planItems;
	private String subject;
	private String replyTo;
	private Object results;
	private String reportType;
	private String userEnteredBodyText;
    private boolean sendHtml;
	private String contactInfo;
    private String primaryBook;
    private String book1Pref1;
    private String book1Pref2;
    private Map< String, Resource > resources;
    private Map< String, String > resourceContentTypes;

	
	public ActionPlanEmailBuilder(List<String> toAddresses, String subject, String replyTo, String userEnteredBodyText, String reportType, 
            boolean sendHtml, Object results, String primaryBook, String book1Pref1, String book1Pref2) {
		super();
		this.recipients = toAddresses;
		this.subject = subject;
		this.results = results;
		this.userEnteredBodyText = userEnteredBodyText;
		this.reportType = reportType;
		this.replyTo = replyTo;
        this.primaryBook = primaryBook;
        this.book1Pref1 = book1Pref1;
        this.book1Pref2 = book1Pref2;
        this.sendHtml = sendHtml;
        this.resources = new HashMap< String, Resource >();
        this.resourceContentTypes = new HashMap< String, String >();
	}

	@SuppressWarnings("unchecked")
	public Map<String, Map< String, Object >> getRecipientsAndContext() {
		DateFormat sdf = SimpleDateFormat.getDateInstance(DateFormat.SHORT);
		Iterator itr;
		Map<String, Map< String, Object > > recipientsAndContext = new HashMap<String, Map< String, Object > >();
		
		Map<String, Object > theInfo = new HashMap< String, Object >();
		
		formatContactInfo(theInfo);
		
        theInfo.put("copyYear",Calendar.getInstance().get(Calendar.YEAR));
		theInfo.put( "bodyText", userEnteredBodyText );
		theInfo.put( "replyToAddress", replyTo);
		theInfo.put( "flLogo", imageLoc );
		
		List< Map<String, Object> > theVehicles = new ArrayList< Map<String,Object> >();
		Map<String, Object> aVehicle = null;
		List<Map<String, Object>> theGroupVehicles = null;
		StringBuilder desc = null;
		
        List singleResults = null;
        Map groupedResults = null;
        if (results instanceof Map) {
            groupedResults = (Map)results;
        } else {
            singleResults = (List) results;
        }
        
		switch (ActionPlanReport.valueFromDescription(reportType)) {
		case CERTIFIED:
		case SPECIAL_FINANCE:
			if (ActionPlanReport.valueFromDescription(reportType) == ActionPlanReport.CERTIFIED) {
				theInfo.put("reportName", "Certified");
			} else {
				theInfo.put("reportName", "Special Finance(List of SF vehicles)");
			}
			itr = singleResults.iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				HashMap vehicle = (HashMap) itr.next();
				desc = new StringBuilder();
				desc.append(vehicle.get("vehicleYear"));
				desc.append(" ");
				desc.append(vehicle.get("make"));
				desc.append(" ");
				desc.append(vehicle.get("groupingDescription"));
				desc.append(" ");
				desc.append(vehicle.get("doorCount"));
				desc.append(" ");
				desc.append(vehicle.get("color"));
				
				aVehicle.put("desc", desc.toString());
				aVehicle.put("listPrice", vehicle.get("listPrice"));
				aVehicle.put("notes", nullSafeToString( vehicle.get("notes") ) );
				aVehicle.put("vin", vehicle.get("vin"));
				aVehicle.put("stock", vehicle.get("stockNumber"));
				aVehicle.put("mileage", vehicle.get("mileageReceived"));
				aVehicle.put("objective", nullSafeToString( vehicle.get("category") ));
				aVehicle.put("otherStrat", (vehicle.get("retailStrats") != null && !vehicle.get("retailStrats").equals("")? vehicle.get("retailStrats").toString() : "No Plan" ));
				theVehicles.add(aVehicle);
			}
			break;
		case UNCHANGED:
			theInfo.put("reportName", "Unchanged Vehicle Status");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				ArrayList vehicles = (ArrayList) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					desc = new StringBuilder();
					desc.append(vehicle.get("vehicleYear"));
					desc.append(" ");
					desc.append(vehicle.get("make"));
					desc.append(" ");
					desc.append(vehicle.get("groupingDescription"));
					desc.append(" ");
					desc.append(vehicle.get("doorCount"));
					desc.append(" ");
					desc.append(vehicle.get("color"));
					groupVehicle.put("desc", desc.toString());
					groupVehicle.put("age", vehicle.get("age"));
					groupVehicle.put("mileage", vehicle.get("mileageReceived"));
					groupVehicle.put("stock", vehicle.get("stockNumber"));
					Integer risk = (Integer)vehicle.get("currentVehicleLight");
					groupVehicle.put("risk", (risk == 1 ? "Red" : (risk == 2 ? "Yellow": "Green")));
					groupVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
					groupVehicle.put("unitCost", vehicle.get("unitCost"));
					groupVehicle.put("bookValue", nullSafeToString( vehicle.get("bookoutValue") ));
                    groupVehicle.put("bookValue2", nullSafeToString( vehicle.get("secondBookoutValue") ));
					Object strategy = vehicle.get("strategy");
					Object objective = vehicle.get("objective");
					Object thirdParty = vehicle.get("thirdParty");
                    if (objective.toString().equalsIgnoreCase("sold")) {
                        groupVehicle.put("strategy", "Sold As");
                        groupVehicle.put("strategyNote", nullSafeToString( strategy ));
                    } else {
                        groupVehicle.put("strategy", nullSafeToString( strategy ));
                        groupVehicle.put("strategyNote", nullSafeToString( thirdParty ));
                    }
					groupVehicle.put("daysSincePlanned", vehicle.get("createdAge"));
					theGroupVehicles.add(groupVehicle);
				}
                aVehicle.put("primaryBook", primaryBook);
                aVehicle.put("book1Pref1", book1Pref1);
                aVehicle.put("book1Pref2", book1Pref2);
				aVehicle.put("objective", vehicle.get("objective"));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		case REPRICING_NO_NOTES:	
		case REPRICING:
			theInfo.put("reportName", "Repricing");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				ArrayList vehicles = (ArrayList) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					desc = new StringBuilder();
					desc.append(vehicle.get("vehicleYear"));
					desc.append(" ");
					desc.append(vehicle.get("make"));
					desc.append(" ");
					desc.append(vehicle.get("groupingDescription"));
					desc.append(" ");
					desc.append(vehicle.get("doorCount"));
					desc.append(" ");
					desc.append(vehicle.get("color"));
					groupVehicle.put("desc", desc.toString());
					groupVehicle.put("stock",vehicle.get("stockNumber"));
					groupVehicle.put("mileage", vehicle.get("mileageReceived"));
					groupVehicle.put("repriceVal", vehicle.get("value"));
					
					Object origVal = "0";
					
					if (vehicle.get("originalPrice") != null)
					{
						origVal = vehicle.get("originalPrice");
					}
					
					groupVehicle.put("origVal", origVal);
					if(ActionPlanReport.valueFromDescription(reportType) != ActionPlanReport.REPRICING_NO_NOTES){
						groupVehicle.put("showNotes", true);
					}
					groupVehicle.put("notes", vehicle.get("notes"));
					theGroupVehicles.add(groupVehicle);
				}
				aVehicle.put("date", format(sdf, vehicle.get("dayCreated")));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		case MODEL_PROMOTION:
			theInfo.put("reportName", "Model Promotion Plan");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				ArrayList vehicles = (ArrayList) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					groupVehicle.put("age", vehicle.get("age"));
					groupVehicle.put("year", vehicle.get("vehicleYear"));
					groupVehicle.put("stock", vehicle.get("stockNumber"));
					theGroupVehicles.add(groupVehicle);
				}
				String startDate = format(sdf, vehicle.get("promotionStartDate"));
				String endDate =  format(sdf, vehicle.get("promotionEndDate"));
				desc = new StringBuilder();
				desc.append((String) vehicle.get("make"));
				desc.append(" ");
				desc.append( vehicle.get("groupingDescription"));
				aVehicle.put("desc", desc.toString());
				aVehicle.put("startDate", startDate);
				aVehicle.put("endDate",endDate);
				aVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		case ADVERTISING:
		case ADVERTISING_INDIVIDUAL_REPORT:
			theInfo.put("reportName", "Advertising Plan");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				ArrayList vehicles = (ArrayList) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					desc = new StringBuilder();
					desc.append(vehicle.get("vehicleYear"));
					desc.append(" ");
					desc.append(vehicle.get("make"));
					desc.append(" ");
					desc.append(vehicle.get("groupingDescription"));
					desc.append(" ");
					desc.append(vehicle.get("doorCount"));
					desc.append(" ");
					desc.append(vehicle.get("color"));
					groupVehicle.put("desc", desc.toString());
					groupVehicle.put("vin", vehicle.get("vin"));
					groupVehicle.put("mileage", vehicle.get("mileageReceived"));
					groupVehicle.put("stock", vehicle.get("stockNumber"));
                    groupVehicle.put("price", nullSafeToString( vehicle.get("listPrice") ));
					groupVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
					theGroupVehicles.add(groupVehicle);
				}
                aVehicle.put("fax", nullSafeToString( vehicle.get("fax")));
				aVehicle.put("contact",  nullSafeToString( vehicle.get("email")));
				aVehicle.put("advertiser", vehicle.get("thirdParty"));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		case AUCTION_INDIVIDUAL:
		case AUCTION:
			theInfo.put("reportName", "Auction Plan");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				ArrayList vehicles = (ArrayList) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					desc = new StringBuilder();
					desc.append(vehicle.get("vehicleYear"));
					desc.append(" ");
					desc.append(vehicle.get("make"));
					desc.append(" ");
					desc.append(vehicle.get("groupingDescription"));
					desc.append(" ");
					desc.append(vehicle.get("doorCount"));
					desc.append(" ");
					desc.append(vehicle.get("color"));
					groupVehicle.put("desc", desc.toString());
					groupVehicle.put("vin", vehicle.get("vin"));
					groupVehicle.put("mileage", vehicle.get("mileageReceived"));
					groupVehicle.put("stock", vehicle.get("stockNumber"));
					groupVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
					groupVehicle.put("unitCost", vehicle.get("unitCost"));
                    groupVehicle.put("bookValue", nullSafeToString( vehicle.get("bookoutValue") ));
                    groupVehicle.put("bookValue2", nullSafeToString( vehicle.get("secondBookoutValue") ));
					groupVehicle.put("age", vehicle.get("age"));
					Integer risk = (Integer)vehicle.get("currentVehicleLight");
					groupVehicle.put("risk", (risk == 1 ? "Red" : (risk == 2 ? "Yellow": "Green")));
					theGroupVehicles.add(groupVehicle);
				}
                aVehicle.put("primaryBook", primaryBook);
                aVehicle.put("book1Pref1", book1Pref1);
                aVehicle.put("book1Pref2", book1Pref2);
                Object endDate = vehicle.get("endDate");
                aVehicle.put("endDate", format(sdf, endDate, "Unspecified"));
				aVehicle.put("auction", vehicle.get("thirdParty"));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		case WHOLESALER_INDIVIDUAL:
		case WHOLESALER:
			theInfo.put("reportName", "Wholesaler Plan");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				ArrayList vehicles = (ArrayList) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					desc = new StringBuilder();
					desc.append(vehicle.get("vehicleYear"));
					desc.append(" ");
					desc.append(vehicle.get("make"));
					desc.append(" ");
					desc.append(vehicle.get("groupingDescription"));
					desc.append(" ");
					desc.append(vehicle.get("doorCount"));
					desc.append(" ");
					desc.append(vehicle.get("color"));
					groupVehicle.put("desc", desc.toString());
					groupVehicle.put("vin", vehicle.get("vin"));
                    groupVehicle.put("listPrice", vehicle.get("listPrice"));
					groupVehicle.put("mileage", vehicle.get("mileageReceived"));
					groupVehicle.put("stock", vehicle.get("stockNumber"));
                    
					Object notes = vehicle.get("notes");
					groupVehicle.put("notes", nullSafeToString( notes ));
					theGroupVehicles.add(groupVehicle);
				}
			    Object endDate = vehicle.get("endDate");
			    aVehicle.put("endDate", format(sdf, endDate, "Unspecified"));
				aVehicle.put("wholesaler", vehicle.get("thirdParty"));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		case LOT_PROMOTE:
			theInfo.put("reportName", "Lot Promotion Plan");
			itr = singleResults.iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				HashMap vehicle = (HashMap) itr.next();
				desc = new StringBuilder();
				desc.append(vehicle.get("vehicleYear"));
				desc.append(" ");
				desc.append(vehicle.get("make"));
				desc.append(" ");
				desc.append(vehicle.get("groupingDescription"));
				desc.append(" ");
				desc.append(vehicle.get("doorCount"));
				desc.append(" ");
				desc.append(vehicle.get("color"));
				
				aVehicle.put("desc", desc.toString());
				aVehicle.put("listPrice", vehicle.get("listPrice"));
				aVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
				aVehicle.put("vin", vehicle.get("vin"));
				aVehicle.put("stock", vehicle.get("stockNumber"));
				aVehicle.put("mileage", vehicle.get("mileageReceived"));
				
				theVehicles.add(aVehicle);
			}
			break;
		case OTHER:
		case SPIFF:
			if (ActionPlanReport.valueFromDescription(reportType) == ActionPlanReport.SPIFF) {
				theInfo.put("reportName", "SPIFF Plan");
			} else {
				theInfo.put("reportName", "Other Plans");
			}
			
			itr = singleResults.iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				HashMap vehicle = (HashMap) itr.next();
				desc = new StringBuilder();
				desc.append(vehicle.get("vehicleYear"));
				desc.append(" ");
				desc.append(vehicle.get("make"));
				desc.append(" ");
				desc.append(vehicle.get("groupingDescription"));
				desc.append(" ");
				desc.append(vehicle.get("doorCount"));
				desc.append(" ");
				desc.append(vehicle.get("color"));
				
				aVehicle.put("desc", desc.toString());
				aVehicle.put("listPrice", vehicle.get("listPrice"));
				aVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
				aVehicle.put("vin", vehicle.get("vin"));
				aVehicle.put("stock", vehicle.get("stockNumber"));
				aVehicle.put("mileage", vehicle.get("mileageReceived"));
				
				theVehicles.add(aVehicle);
			}
			break;
		case WHOLESALER_BIDSHEET:
			theInfo.put("reportName", "Wholesaler Bid Sheet");
			itr = singleResults.iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				HashMap vehicle = (HashMap) itr.next();
				desc = new StringBuilder();
				desc.append(vehicle.get("vehicleYear"));
				desc.append(" ");
				desc.append(vehicle.get("make"));
				desc.append(" ");
				desc.append(vehicle.get("groupingDescription"));
				desc.append(" ");
				desc.append(vehicle.get("doorCount"));
				desc.append(" ");
				desc.append(vehicle.get("color"));
				aVehicle.put("desc", desc.toString());
				aVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
                aVehicle.put("options", nullSafeToString( vehicle.get("options") ));
				aVehicle.put("vin", vehicle.get("vin"));
				aVehicle.put("stock", vehicle.get("stockNumber"));
				aVehicle.put("mileage", vehicle.get("mileageReceived"));
                aVehicle.put("primaryBook", primaryBook);
				theVehicles.add(aVehicle);
			}
			break;
		case IN_SERVICE:
			theInfo.put("reportName", "Vehicles in Service Department");
			itr = singleResults.iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				HashMap vehicle = (HashMap) itr.next();
				desc = new StringBuilder();
				desc.append(vehicle.get("vehicleYear"));
				desc.append(" ");
				desc.append(vehicle.get("make"));
				desc.append(" ");
				desc.append(vehicle.get("groupingDescription"));
				desc.append(" ");
				desc.append(vehicle.get("doorCount"));
				desc.append(" ");
				desc.append(vehicle.get("color"));
				aVehicle.put("desc", desc.toString());
                aVehicle.put("age", vehicle.get("age"));
				aVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
				aVehicle.put("vin", vehicle.get("vin"));
				aVehicle.put("stock", vehicle.get("stockNumber"));
				aVehicle.put("mileage", vehicle.get("mileageReceived"));
				aVehicle.put("reason", vehicle.get("reason"));
				aVehicle.put("daysInService", nullSafeToString( vehicle.get("createdAge") ) );
				calculateDates((Timestamp)vehicle.get("enterDate"), (Timestamp)vehicle.get("exitDate"), aVehicle, sdf);
				theVehicles.add(aVehicle);
			}
			break;
		case INTERNET_ADVERTISEMENT:
			theInfo.put("reportName", "Internet Advertisement Plan");
			itr = groupedResults.values().iterator();
			while (itr.hasNext()) {
				aVehicle = new HashMap< String, Object >();
				theGroupVehicles = new ArrayList< Map<String,Object> >();
				List vehicles = (List) itr.next();
				Iterator vehiclesItr = vehicles.iterator();
				Map vehicle = null;
				while(vehiclesItr.hasNext()) {
					vehicle = (Map)vehiclesItr.next();
					Map<String, Object> groupVehicle = new HashMap< String, Object >();
					desc = new StringBuilder();
					desc.append(vehicle.get("vehicleYear"));
					desc.append(" ");
					desc.append(vehicle.get("make"));
					desc.append(" ");
					desc.append(vehicle.get("groupingDescription"));
					desc.append(" ");
					desc.append(vehicle.get("doorCount"));
					desc.append(" ");
					desc.append(vehicle.get("color"));
					groupVehicle.put("desc", desc.toString());
					groupVehicle.put("vin", vehicle.get("vin"));
					groupVehicle.put("mileage", vehicle.get("mileageReceived"));
					groupVehicle.put("stock", vehicle.get("stockNumber"));
                    groupVehicle.put("price", nullSafeToString( vehicle.get("listPrice") ));
					groupVehicle.put("notes", nullSafeToString( vehicle.get("notes") ));
					theGroupVehicles.add(groupVehicle);
				}
				Set<Map<String, Object>> advertisers = groupedResults.keySet();
				if(advertisers.size() != 1) {
					log.warn("Bad coding here, expected to have 1 entry as a list of internet advertisers.");
				}
				Map<String,Object> theAdvertisers = null;
				for(Map<String,Object> advertiser : advertisers) {
					theAdvertisers = advertiser;
					break; //the size of the set should always be 1
				}
				aVehicle.put("advertiser", theAdvertisers.get("thirdParty"));
				aVehicle.put("cars", theGroupVehicles);
				theVehicles.add(aVehicle);
			}
			break;
		}
        if (sendHtml) {
            theInfo.put( "vehicleList", theVehicles );
        } else {
            planItems = theVehicles;
            theInfo.put( "vehicleList", new ArrayList() );
        }
		for ( String recipient: recipients )
		{
			recipientsAndContext.put( recipient, theInfo );
		}
		
		return recipientsAndContext;
		
	}

	private String format(DateFormat sdf, Object value) {
		return format(sdf, value, "");
	}
	
	private String format(DateFormat sdf, Object value, String defaultValue) {
		String formattedValue = defaultValue;
		if (value != null) {
			try {
				formattedValue = sdf.format(value);
			}
			catch (IllegalArgumentException e) {
				// cannot be formatted
			}
		}
		return formattedValue;
	}
	
	private void formatContactInfo(Map<String, Object> theInfo) {
		String[] info = contactInfo.split("\n");
		List<String> contactInfo = new ArrayList<String>();
		for (int i = 0; i < info.length;i++ ){
			contactInfo.add(info[i]);
		}
		theInfo.put("contactInfo", contactInfo);
	}

	public void setSubject( String subject )
	{
		this.subject = subject;
	}

	public String getSubject()
	{
		return subject;
	}

	public String getTemplateName()
	{
		switch (ActionPlanReport.valueFromDescription(reportType)) {
			case ADVERTISING:
			case ADVERTISING_INDIVIDUAL_REPORT:
				return "printAdvertiser";
			case AUCTION_INDIVIDUAL:
			case AUCTION:
				return "auction";
			case CERTIFIED:
				return "special";
			case IN_SERVICE:
				return "service";
			case LOT_PROMOTE:
				return "lot";
			case MODEL_PROMOTION:
				return "model";	
			case OTHER:
			case SPIFF:
				return "spiff";
			case REPRICING_NO_NOTES:
			case REPRICING:
				return "repricing";
			case SPECIAL_FINANCE:
				return "special";
			case UNCHANGED:
				return "unchanged";
			case WHOLESALER_INDIVIDUAL:
			case WHOLESALER:
				return "wholesaler";
			case WHOLESALER_BIDSHEET:
				return "wholeBid";
			case INTERNET_ADVERTISEMENT:
				return ActionPlanReport.INTERNET_ADVERTISEMENT.getDescription();
			}
		return null;
	}

	public Map< String, EmailFormat > getEmailFormats()
	{
		Map< String, EmailFormat > emailFormats = new HashMap< String, EmailFormat >();
		for ( String emailAddress : recipients )
		{
			emailFormats.put( emailAddress, EmailFormat.HTML_MULTIPART );
		}
		return emailFormats;
	}
    
    public void putResource( String name, Resource resource, String contentType )
    {
        resources.put( name, resource );
        resourceContentTypes.put( name, contentType );
    }
    
    public Map< String, Resource > getEmbeddedResources()
    {
        return resources;
    }
    public Map< String, String > getEmbeddedResourcesContentTypes()
    {
        return resourceContentTypes;
    }

	public String getContactInfo()
	{
		return contactInfo;
	}

	public void setContactInfo( String contactInfo )
	{
		this.contactInfo = contactInfo;
	}

	public String getReplyTo()
	{
		return replyTo;
	}

	@SuppressWarnings("deprecation")
	private void calculateDates(Timestamp start, Timestamp end, Map<String, Object> vehicle, DateFormat sdf) {
		StringBuilder dateString = new StringBuilder();
		if (start != null) {
			dateString.append( sdf.format( start ) );
			if ( end != null ) {
				dateString.append( " - " + sdf.format( end ) );
			}
		} 
		vehicle.put("dates", dateString.toString() );
	}

    private static String nullSafeToString( Object obj ) {
    	if ( obj == null ) {
    		return "";
    	}
    	return obj.toString();
    }
	
    public List<Map<String, Object>> getPlanItems() {
        return planItems;
    }

    public void setPlanItems(List<Map<String, Object>> planItems) {
        this.planItems = planItems;
    }

	public List< String > getCcEmailAddresses()
	{
		return Collections.emptyList();
	}
}
