package biz.firstlook.planning.email;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;

public class ExceptionReportEmailBuilder implements EmailContextBuilder {

    private final String replyTo = "helpdesk@firstlook.biz";
    private final String imageLoc = "http://images.firstlook.biz/fldn_logo.gif";
    private String recipient;
    private String market;
    private List<Map<String, Object>> reportBuilderResults;
    private Map<String, Resource> resources;
    private Map<String, String> resourceContentTypes;

    public ExceptionReportEmailBuilder(String market,List<Map<String, Object>> reportBuilderResults) {
        super();
        this.reportBuilderResults = reportBuilderResults;
        this.market = market;
        this.recipient = getContactEmail();
        this.resources = new HashMap< String, Resource >();
        this.resourceContentTypes = new HashMap<String, String>();
    }

    public void putResource(String name, Resource resource, String contentType) {
        resources.put(name, resource);
        resourceContentTypes.put(name, contentType);
    }

    public Map<String, Map< String, Object >> getRecipientsAndContext() {
        Map<String, Map< String, Object >> recipientsAndContext = new HashMap<String, Map< String, Object >>();

        Map<String, Object> exceptionReport = new HashMap<String, Object>();
        exceptionReport.put("advertiser", market);
        Boolean sendZeroesAsNull = (Boolean) reportBuilderResults.get(0).get("SendZeroesAsNull");
        exceptionReport.put( "flLogo", imageLoc );
        exceptionReport.put("reason",(sendZeroesAsNull ? "sent with no internet price":"not sent due to an empty internet price"));
        exceptionReport.put("date", getFormattedDate());
        
        List<Map<String, Object>> theVehicles = new ArrayList<Map<String, Object>>();
        Map<String, Object> aVehicle = null;
        StringBuilder vehicleDescription = null;
        for (Map<String, Object> row : reportBuilderResults) {
            vehicleDescription = new StringBuilder();
            aVehicle = new HashMap<String, Object>();
            vehicleDescription.append(row.get("VehicleYear") + " ");
            vehicleDescription.append(row.get("Make") + " ");
            vehicleDescription.append(row.get("OriginalModel") + " ");
            vehicleDescription.append(row.get("VehicleTrim") + " ");
            aVehicle.put("description", vehicleDescription.toString());
            String color = (String) row.get("BaseColor");
            aVehicle.put("color", color.toLowerCase());
            aVehicle.put("mileage", row.get("MileageReceived"));
            aVehicle.put("stock", row.get("StockNumber"));
            aVehicle.put("vin", row.get("Vin"));

            theVehicles.add(aVehicle);
        }
        exceptionReport.put("exceptions", theVehicles);

        recipientsAndContext.put(recipient, exceptionReport);

        return recipientsAndContext;
    }

    public String getSubject() {
        return market + " Exception Report - " + getFormattedDate();
    }

    public String getReplyTo() {
        return this.replyTo;
    }

    public String getTemplateName() {
        return "exceptionReport";
    }

    public Map<String, EmailFormat> getEmailFormats() {
        Map<String, EmailFormat> emailFormats = new HashMap<String, EmailFormat>();
        emailFormats.put(recipient, EmailFormat.HTML_EXTERNAL_LINKS);
        return emailFormats;
    }

    public Map<String, Resource> getEmbeddedResources() {
        return resources;
    }

    public Map<String, String> getEmbeddedResourcesContentTypes() {
        return resourceContentTypes;
    }

    private String getFormattedDate() {
        if (reportBuilderResults != null && !reportBuilderResults.isEmpty()) {
            Date date = (Date) reportBuilderResults.get(0).get("Created");
            DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);
            String dateSent = dateFormatter.format(date);
            return dateSent;
        }
        return "No date set";
    }

    private String getContactEmail() {
        if (reportBuilderResults != null && !reportBuilderResults.isEmpty()) {
            String contactEmail = (String) reportBuilderResults.get(0).get("ContactEmail");
            return contactEmail;
        }
        return null;
    }

	public List< String > getCcEmailAddresses()
	{
		return Collections.EMPTY_LIST;
	}
}
