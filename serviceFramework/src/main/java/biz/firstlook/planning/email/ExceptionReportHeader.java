package biz.firstlook.planning.email;

import java.text.DateFormat;
import java.util.Date;

/**
 * Simple java bean holding the grouped report values
 * @author bfung
 *
 */
public class ExceptionReportHeader
{

private String advertiser;
private boolean sendZeroesAsNull;
private Date date;

public ExceptionReportHeader( String advertiser, boolean sendZeroesAsNull, Date date )
{
	super();
	this.advertiser = advertiser;
	this.sendZeroesAsNull = sendZeroesAsNull;
	this.date = date;
}

public String getReason()
{
	return ( sendZeroesAsNull ? "sent with no internet price" : "not sent due to an empty internet price" );
}

public String getAdvertiser()
{
	return advertiser;
}
public String getDate()
{
	DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);
    return dateFormatter.format(date);
}
public Boolean getSendZeroesAsNull()
{
	return sendZeroesAsNull;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( advertiser == null ) ? 0 : advertiser.hashCode() );
	result = PRIME * result + ( ( date == null ) ? 0 : date.hashCode() );
	result = PRIME * result + ( sendZeroesAsNull ? 1231 : 1237 );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final ExceptionReportHeader other = (ExceptionReportHeader)obj;
	if ( advertiser == null )
	{
		if ( other.advertiser != null )
			return false;
	}
	else if ( !advertiser.equals( other.advertiser ) )
		return false;
	if ( date == null )
	{
		if ( other.date != null )
			return false;
	}
	else if ( !date.equals( other.date ) )
		return false;
	if ( sendZeroesAsNull != other.sendZeroesAsNull )
		return false;
	return true;
}



}
