package biz.firstlook.planning.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.planning.service.InternetAdvertiserService;
import biz.firstlook.report.reportBuilders.ExceptionReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class ExceptionReportEmailService {
    
    private IReportService transactionalReportService;
    private InternetAdvertiserService internetAdvertiserService;
    private EmailService emailService;
    
    //Sends all exception report emails.
    public void createEmails() {
        List<String> datafeeds = internetAdvertiserService.retrieveAdvertiserDatafeeds();
        for(String datafeed:datafeeds) {
            List<Integer> businessUnits = internetAdvertiserService.retrieveBusinessUnitsForDatafeed(datafeed);
            sendEmails(datafeed, businessUnits);
        }
    }
    
    //Sends exception report emails for all export markets to the given business unit.
    public void createEmailsForBusinessUnit(Integer businessUnitId) {
        List<Integer> businessUnits = new ArrayList<Integer>();
        businessUnits.add(businessUnitId);
        List<String> datafeeds = internetAdvertiserService.retrieveAdvertiserDatafeeds();
        for(String datafeed:datafeeds) {
            sendEmails(datafeed, businessUnits);
        }
    }
    
//  Sends exception report emails for all export markets to the given business unit.
    public Map<ExceptionReportHeader, List> retrieveExceptionReport(Integer businessUnitId) {
    	Map<ExceptionReportHeader, List> reportByDataFeed = new LinkedHashMap<ExceptionReportHeader, List>();
        List<String> datafeeds = internetAdvertiserService.retrieveAdvertiserDatafeeds();
        for( String datafeed:datafeeds) {
	        Integer buildLogId = internetAdvertiserService.retrieveLatestBuildLogIdForDatafeed(datafeed);
	        ExceptionReportBuilder reportBuilder;
	        if(buildLogId != null) {//If null then it has never been exported for that datafeed.
	        	reportBuilder = new ExceptionReportBuilder();
	        	reportBuilder.addBusinessUnit(businessUnitId);
	        	reportBuilder.addBuildLogId(buildLogId);
	        	reportBuilder.addDatafeedCode(datafeed);
	        	List reportBuilderResults  = transactionalReportService.getResults(reportBuilder);	            
	            if( reportBuilderResults != null && !reportBuilderResults.isEmpty() )
	            {
	            	Boolean sendZeroesAsNull = (Boolean) ((Map)reportBuilderResults.get(0)).get("SendZeroesAsNull");
	            	Date date = (Date) ((Map)reportBuilderResults.get(0)).get("Created");
	            	reportByDataFeed.put( new ExceptionReportHeader( datafeed, sendZeroesAsNull, date ), reportBuilderResults );
	            }
	        }
        }
        return reportByDataFeed;
    }
    
    //Sends exception report emails for all business units for the given export market.
    public void createEmailsForDatafeed(String datafeed) {
        List<Integer> businessUnits = internetAdvertiserService.retrieveBusinessUnitsForDatafeed(datafeed);
        sendEmails(datafeed, businessUnits);
    }
    
    //Sends exception reports email for the business unit and the given export market.
    public void createEmailsForDatafeedAndBusinessUnit(String datafeed, Integer businessUnitId) {
        List<Integer> businessUnits = new ArrayList<Integer>();
        businessUnits.add(businessUnitId);
        sendEmails(datafeed, businessUnits);
    }
    
    //Sends emails.
    @SuppressWarnings("unchecked")
    public void sendEmails(String datafeedCode, List<Integer> businessUnits) {
        Integer buildLogId = internetAdvertiserService.retrieveLatestBuildLogIdForDatafeed(datafeedCode);
        ExceptionReportBuilder reportBuilder;
        if(buildLogId != null) {//If null then it has never been exported for that datafeed.
            for (Integer businessUnitId : businessUnits){
                reportBuilder = new ExceptionReportBuilder();
                reportBuilder.addBusinessUnit(businessUnitId);
                reportBuilder.addBuildLogId(buildLogId);
                reportBuilder.addDatafeedCode(datafeedCode);
                List reportBuilderResults  = transactionalReportService.getResults(reportBuilder);
                if ( reportBuilderResults != null && !reportBuilderResults.isEmpty()) {
                    ExceptionReportEmailBuilder exceptionReportEmail = new ExceptionReportEmailBuilder(datafeedCode, reportBuilderResults);
                    emailService.sendEmail(exceptionReportEmail);
                }
            }
            internetAdvertiserService.setExceptionReportSent(buildLogId);
        }
    }

    public void setTransactionalReportService(
            IReportService transactionalReportService) {
        this.transactionalReportService = transactionalReportService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setInternetAdvertiserService(InternetAdvertiserService internetAdvertiserService) {
        this.internetAdvertiserService = internetAdvertiserService;
    }

}
