package biz.firstlook.planning.email;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import biz.firstlook.report.action.ActionPlanReport;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
@SuppressWarnings("unchecked") 
public class ActionPlanPdfBuilderService extends PdfPageEventHelper{

	private static final Logger logger = Logger.getLogger( ActionPlanPdfBuilderService.class );	
	
    private final String imageLoc = "http://images.firstlook.biz/fldn_logo.gif";
    private static final Font HELVET_8_BOLD = new Font( Font.HELVETICA, 8, Font.BOLD );
    private static final Font HELVET_8_NORMAL = new Font( Font.HELVETICA, 8, Font.NORMAL);
    private static final Font HELVET_11_BOLD = new Font( Font.HELVETICA, 11, Font.BOLD );
    private static final Font HELVET_10_NORMAL = new Font( Font.HELVETICA, 10, Font.NORMAL );
    private static final Color TABLE_HEADER_COLOR = new Color( 205, 205, 205 );
    private static final Color GROUP_HEADER_COLOR = new Color( 0, 0, 0 );
    
    public Image headerImage;
    public PdfPTable headerTable;
    public PdfPTable footerTable;
    public BaseFont helv;
    public PdfTemplate tpl;
    
    public String dealerName;
    public String reportName;
    
    public ActionPlanPdfBuilderService(){
        super();
    }
    
    public ActionPlanPdfBuilderService(String dealerName, String title){
        super();
        this.dealerName = dealerName;
        this.reportName = title;
    }
    
    public File buildPdf(List<Map<String, Object>> results, ActionPlanReport reportType, String dealerName, String reportName) {
        Document doc = new Document( PageSize.A4, 20, 20, 50, 20 );
        File temp = null;
        
        try {
            temp = File.createTempFile("actionPlan", ".pdf");
            PdfWriter writer = PdfWriter.getInstance( doc,new FileOutputStream(temp));
            writer.setPageEvent(new ActionPlanPdfBuilderService(dealerName, reportName));
            
            doc.open();
            
            switch (reportType) {
            case IN_SERVICE:
                createServicePdf(doc, results);
                break;
            case SPECIAL_FINANCE:
            case CERTIFIED:
                createCertifiedSpecialPdf(doc, results);
                break;
            case ADVERTISING:
            case ADVERTISING_INDIVIDUAL_REPORT:
                createAdvertisingPdf(doc, results);
                break;
            case MODEL_PROMOTION:
                createModelPromotionPdf(doc, results);
                break;
            case LOT_PROMOTE:
            case OTHER:
            case SPIFF:
                createSpiffLotOtherPdf(doc, results, reportType);
                break;
            case AUCTION:
            case AUCTION_INDIVIDUAL:
                createAuctionPdf(doc, results);
                break;
            case WHOLESALER_BIDSHEET:
                createWholesalerBidPdf(doc, results);
                break;
            case WHOLESALER:
            case WHOLESALER_INDIVIDUAL:
                createWholesalerPdf(doc, results);
                break;
            case REPRICING_NO_NOTES:
            	createRepricePdf(doc, results, false);
            	break;
            case REPRICING:
                createRepricePdf(doc, results, true);
                break;
            case UNCHANGED:
                createUnchangePdf(doc, results);
            }
            doc.close();
        
        } catch (Exception e) {
        	logger.warn("Error generating PDF.", e);
        } 
        return temp;
    }
    
    public void onOpenDocument(PdfWriter writer, Document document) {
        try {
            // initialization of the header headerTable
            headerImage = Image.getInstance(imageLoc);
            float[] tableWidths = new float[]{75, 25};
            headerTable = new PdfPTable(tableWidths);
            Phrase p = new Phrase();
            Chunk ck = new Chunk(reportName, new Font( Font.HELVETICA, 13, Font.BOLD ));
            p.add(ck);
            p.add(Chunk.NEWLINE);
            ck = new Chunk(dealerName, new Font( Font.HELVETICA, 13, Font.BOLD ));
            p.add(ck);
            p.add(Chunk.NEWLINE);
            headerTable.getDefaultCell().setBorderWidth(0);
            headerTable.addCell(p);
            headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            headerTable.addCell(new Phrase(new Chunk(headerImage, 0, 0)));
            
            Integer year = Calendar.getInstance().get(Calendar.YEAR);
            
            footerTable = new PdfPTable(2);
            footerTable.getDefaultCell().setBorderWidth(0);
            p = new Phrase();
            ck = new Chunk("First Look Help Desk: 1-877-378-5665",  new Font( Font.HELVETICA, 8, Font.NORMAL ));
            p.add(ck);
            footerTable.addCell(p);
            footerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
            p = new Phrase();
            ck = new Chunk("\u00A9 " + year + " INCISENT Technologies, Inc. All rights reserved." ,  new Font( Font.HELVETICA, 8, Font.NORMAL ));
            p.add(ck);
            footerTable.addCell(p);
            
            tpl = writer.getDirectContent().createTemplate(100, 100);
            tpl.setBoundingBox(new Rectangle(-20, -20, 100, 100));
            helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
        }
        catch(Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte cb = writer.getDirectContent();
        
        cb.saveState();
        // write the headertable
        headerTable.setTotalWidth(document.right() - document.left());
        headerTable.writeSelectedRows(0, -1, document.left(), document.getPageSize().height() - 10, cb);

        footerTable.setTotalWidth(document.right() - document.left());
        footerTable.writeSelectedRows(0, -1, document.left(), 23, cb);
        
        String pageNum = "Page " + writer.getPageNumber() + " of ";
        float textSize = helv.getWidthPoint(pageNum, 10);
        float textBase = document.bottom() - 16;
        float adjust = helv.getWidthPoint("0", 12);
        
        cb.beginText();
        cb.setFontAndSize(helv, 8);
        cb.setTextMatrix(document.right() - textSize - adjust, textBase);
        cb.showText(pageNum);
        cb.endText();
        cb.addTemplate(tpl, document.right() - 15, textBase);
        cb.saveState();
    }
    
    public void onCloseDocument(PdfWriter writer, Document document) {
        tpl.beginText();
        tpl.setFontAndSize(helv, 9);
        tpl.setTextMatrix(0, 0);
        tpl.showText("" + (writer.getPageNumber() - 1));
        tpl.endText();
     }
    

    private void createUnchangePdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable rightColumnTable;
        
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 40, 60 };
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            cell = new PdfPCell();
            cell.setBackgroundColor(GROUP_HEADER_COLOR);
            cell.setColspan(2);
            Chunk advertiserInfo = new Chunk(new Chunk("All vehicles with " + result.get("objective") + " status for > 7 days. ", 
                    FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(255, 255,255)))); 
            cell.addElement(advertiserInfo);
            containerTable.addCell(cell);
            
            List<HashMap> cars = (List<HashMap>) result.get("cars");
            for (HashMap car:cars){
                leftColumnTable = new PdfPTable(1);
                leftColumnTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                leftColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph vehInfo = new Paragraph();
                vehInfo.add(new Chunk( nullSafeToString(car.get("desc") ), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Stock #: " + nullSafeToString(car.get("stock")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Mileage: " + formatNumberToString( car.get("mileage")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Risk: " + nullSafeToString(car.get("risk") ), HELVET_10_NORMAL));
                cell.addElement(vehInfo);
                leftColumnTable.addCell(cell);
                
                boolean hasSecondPref = false;
                tableWidths = new float[] { 20, 30, 30, 20 };
                if (result.get("book1Pref2") != null && !result.get("book1Pref2").equals("")) {
                    hasSecondPref = true;
                    tableWidths = new float[] { 20, 20, 20, 20, 20 };
                }
                
                rightColumnTable = new PdfPTable(tableWidths);
                rightColumnTable.setWidthPercentage(100);
                cell = new PdfPCell(); 
                cell.addElement(new Chunk("Unit Cost", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk(result.get("primaryBook") + " " + result.get("book1Pref1") , HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                if (hasSecondPref) {
                    cell = new PdfPCell();
                    cell.addElement(new Chunk(result.get("primaryBook") + " " + result.get("book1Pref2"), HELVET_11_BOLD));
                    cell.setBackgroundColor(TABLE_HEADER_COLOR);
                    rightColumnTable.addCell(cell);
                    
                }
                
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString(car.get("strategy")), HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("Days Since Planned", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( car.get("unitCost")))),HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("$" + formatNumberToString(Double.valueOf( nullSafeToString(  car.get("bookValue")))),HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                if (hasSecondPref) {
                    cell = new PdfPCell();
                    cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( car.get("bookValue2")))),HELVET_10_NORMAL));
                    rightColumnTable.addCell(cell);
                }
                
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString(car.get("strategyNote")), HELVET_11_BOLD));
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString( car.get("daysSincePlanned") ), HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setColspan((hasSecondPref ? 5:4));
                cell.addElement(new Chunk("Notes", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                cell = new PdfPCell();
                cell.setColspan((hasSecondPref ? 5:4));
                cell.addElement(new Chunk( nullSafeToString(car.get("notes") ), HELVET_10_NORMAL));
                cell.setBorder(Rectangle.NO_BORDER);
                rightColumnTable.addCell(cell);
                
                containerTable.addCell(leftColumnTable);
                containerTable.addCell(rightColumnTable);
            }

            doc.add(containerTable);
        }
    }

    private void createServicePdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable rightColumnTable;
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 40, 60};
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            
            leftColumnTable = new PdfPTable(1);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            leftColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            Paragraph vehInfo = new Paragraph();
            vehInfo.add(new Chunk( nullSafeToString( result.get("desc") ), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk( nullSafeToString(result.get("age") ) + " Days Old", HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Vin #: " + nullSafeToString(result.get("vin") ), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Stock #: " + nullSafeToString(result.get("stock") ), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Mileage: " + formatNumberToString( result.get("mileage")), HELVET_10_NORMAL));
            cell.addElement(vehInfo);
            leftColumnTable.addCell(cell);

            rightColumnTable = new PdfPTable(3);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Service Dates", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Days In Service", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Reason", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString( result.get("dates") ), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString(result.get("daysInService") ), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString(result.get("reason") ), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            
            
            cell = new PdfPCell();
            cell.setColspan(3);
            cell.addElement(new Chunk("Notes", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.setColspan(3);
            cell.addElement(new Chunk( nullSafeToString(result.get("notes") ), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            
            
            containerTable.addCell(leftColumnTable);
            containerTable.addCell(rightColumnTable);

            doc.add(containerTable);
        }
    }

    private void createWholesalerBidPdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable centerColumnTable;
        PdfPTable rightColumnTable;
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 40, 40, 20};
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            
            leftColumnTable = new PdfPTable(1);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            leftColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            Paragraph vehInfo = new Paragraph();
            vehInfo.add(new Chunk( nullSafeToString(result.get("desc") ), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Vin #: " + nullSafeToString(result.get("vin") ), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Stock #: " + nullSafeToString(result.get("stock")), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Mileage: " + formatNumberToString( result.get("mileage")), HELVET_10_NORMAL));
            cell.addElement(vehInfo);
            leftColumnTable.addCell(cell);
            
            centerColumnTable = new PdfPTable(1);
            cell = new PdfPCell();
            cell.addElement(new Chunk(result.get("primaryBook") + " " +"Options", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            centerColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString( result.get("options") ), HELVET_10_NORMAL));
            centerColumnTable.addCell(cell);
            
            rightColumnTable = new PdfPTable(1);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Bid", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(Chunk.NEWLINE);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk("Notes", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString( result.get("notes")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);

            
            
            
            containerTable.addCell(leftColumnTable);
            containerTable.addCell(centerColumnTable);
            containerTable.addCell(rightColumnTable);

            doc.add(containerTable);
        }
    }


    private void createWholesalerPdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable rightColumnTable;
        
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            containerTable = new PdfPTable(2);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            
            cell = new PdfPCell();
            cell.setBackgroundColor(GROUP_HEADER_COLOR);
            cell.setColspan(3);
            //Reprice on: ${grouping.date} 
            Chunk wholesalerInfo = new Chunk(new Chunk("Wholesaler: " + nullSafeToString(result.get("wholesaler")) + " - " + nullSafeToString(result.get("endDate")), 
                    FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(255, 255,255)))); 
            cell.addElement(wholesalerInfo);
            containerTable.addCell(cell);
            
            List<HashMap> cars = (List<HashMap>) result.get("cars");
            for (HashMap car:cars){
                leftColumnTable = new PdfPTable(1);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                leftColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph vehInfo = new Paragraph();
                vehInfo.add(new Chunk( nullSafeToString(car.get("desc")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Stock #: " + nullSafeToString(car.get("stock")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Mileage: " + formatNumberToString( car.get("mileage")), HELVET_10_NORMAL));
                cell.addElement(vehInfo);
                leftColumnTable.addCell(cell);
                
                rightColumnTable = new PdfPTable(1);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Sales Price", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( car.get("listPrice")))),HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("Notes", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString(car.get("notes")), HELVET_10_NORMAL));
                cell.setBorder(Rectangle.NO_BORDER);
                rightColumnTable.addCell(cell);
                
                containerTable.addCell(leftColumnTable);
                containerTable.addCell(rightColumnTable);
            }

            doc.add(containerTable);
        }
    }

    private void createAuctionPdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable rightColumnTable;
        
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 40, 60 };
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            cell = new PdfPCell();
            cell.setBackgroundColor(GROUP_HEADER_COLOR);
            cell.setColspan(2);
            Chunk advertiserInfo = new Chunk(new Chunk("Auction: " + nullSafeToString(result.get("auction")) + " - " + nullSafeToString(result.get("endDate")), 
                    FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(255, 255,255)))); 
            cell.addElement(advertiserInfo);
            containerTable.addCell(cell);
            
            List<HashMap> cars = (List<HashMap>) result.get("cars");
            for (HashMap car:cars){
                leftColumnTable = new PdfPTable(1);
                leftColumnTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                leftColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph vehInfo = new Paragraph();
                vehInfo.add(new Chunk( nullSafeToString(car.get("desc")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Vin #: " + nullSafeToString(car.get("vin")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Stock #: " + nullSafeToString(car.get("stock")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Mileage: " + formatNumberToString( car.get("mileage")), HELVET_10_NORMAL));
                cell.addElement(vehInfo);
                leftColumnTable.addCell(cell);
                
                boolean hasSecondPref = false;
                tableWidths = new float[] { 20, 30, 30, 10, 10 };
                if (result.get("book1Pref2") != null && !result.get("book1Pref2").equals("")) {
                    hasSecondPref = true;
                    tableWidths = new float[] { 20, 20, 20, 20, 10, 10 };
                }
                rightColumnTable = new PdfPTable(tableWidths);
                rightColumnTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Unit Cost", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk(result.get("primaryBook") + " " +  result.get("book1Pref1") , HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                if (hasSecondPref) {
                    cell = new PdfPCell();
                    cell.addElement(new Chunk(result.get("primaryBook") + " " + result.get("book1Pref2") , HELVET_11_BOLD));
                    cell.setBackgroundColor(TABLE_HEADER_COLOR);
                    rightColumnTable.addCell(cell);
                    
                }
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("Reserve Price", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("Age", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("Risk", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                cell = new PdfPCell();
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( car.get("unitCost")))),HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("$" + formatNumberToString(Double.valueOf( nullSafeToString( car.get("bookValue")))),HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                if (hasSecondPref) {
                    cell = new PdfPCell();
                    cell.addElement(new Chunk("$" + formatNumberToString(Double.valueOf( nullSafeToString( car.get("bookValue2")))),HELVET_10_NORMAL));
                    rightColumnTable.addCell(cell);
                }
                
                cell = new PdfPCell();
                cell.addElement(Chunk.NEWLINE);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString(car.get("age")), HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString(car.get("risk")), HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setColspan((hasSecondPref ? 6:5));
                cell.addElement(new Chunk("Notes", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                cell = new PdfPCell();
                cell.setColspan((hasSecondPref ? 6:5));
                cell.addElement(new Chunk( nullSafeToString(car.get("notes")), HELVET_10_NORMAL));
                cell.setBorder(Rectangle.NO_BORDER);
                rightColumnTable.addCell(cell);
                
                containerTable.addCell(leftColumnTable);
                containerTable.addCell(rightColumnTable);
            }

            doc.add(containerTable);
        }
    }

    private void createCertifiedSpecialPdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable rightColumnTable;
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 50, 50 };
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            
            leftColumnTable = new PdfPTable(1);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            leftColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            Paragraph vehInfo = new Paragraph();
            vehInfo.add(new Chunk( nullSafeToString(result.get("desc")), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Vin #: " + nullSafeToString(result.get("vin")) , HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Stock #: " +nullSafeToString( result.get("stock")), HELVET_10_NORMAL));
            vehInfo.add(Chunk.NEWLINE);
            vehInfo.add(new Chunk("Mileage: " + formatNumberToString( result.get("mileage")), HELVET_10_NORMAL));
            cell.addElement(vehInfo);
            leftColumnTable.addCell(cell);

            tableWidths = new float[] { 25, 50, 25};
            rightColumnTable = new PdfPTable(tableWidths);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Objective", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Other Retail Strategies", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Internet Price", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString(result.get("objective")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString(result.get("otherStrat")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( result.get("listPrice")))), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.setColspan(3);
            cell.addElement(new Chunk("Objective", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            cell = new PdfPCell();
            cell.setColspan(3);
            cell.addElement(new Chunk( nullSafeToString(result.get("notes")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            
            
            containerTable.addCell(leftColumnTable);
            containerTable.addCell(rightColumnTable);

            doc.add(containerTable);
        }
    }

    private void createRepricePdf(Document doc, List<Map<String, Object>> results, boolean showNotes) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable columnOneTable;
        PdfPTable columntwotable;
        PdfPTable columnThreeTable;
        PdfPTable columnFourTable;
        
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths;
            if(showNotes)
            	tableWidths = new float[] { 40, 20, 20, 20 };
            else 
            	tableWidths = new float[] { 60, 20, 20 };
            
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            
            cell = new PdfPCell();
            cell.setBackgroundColor(GROUP_HEADER_COLOR);
            if(showNotes)
            	cell.setColspan(4);
            else
            	cell.setColspan(3);
           
            //Reprice on: ${grouping.date} 
            Chunk advertiserInfo = new Chunk(new Chunk("Repriced on: " + nullSafeToString(result.get("date")), 
                    FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(255, 255,255)))); 
            cell.addElement(advertiserInfo);
            containerTable.addCell(cell);
            
            List<HashMap> cars = (List<HashMap>) result.get("cars");
            for (HashMap car:cars){
                columnOneTable = new PdfPTable(1);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                columnOneTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph vehInfo = new Paragraph();
                vehInfo.add(new Chunk( nullSafeToString(car.get("desc")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Stock #: " + nullSafeToString(car.get("stock")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Mileage: " + formatNumberToString( car.get("mileage")), HELVET_10_NORMAL));
                cell.addElement(vehInfo);
                columnOneTable.addCell(cell);
                
                columntwotable = new PdfPTable(1);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Reprice value", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                columntwotable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( car.get("repriceVal")))),HELVET_10_NORMAL));
                columntwotable.addCell(cell);
                
                columnThreeTable = new PdfPTable(1);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Original value", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                columnThreeTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.addElement(new Chunk("$" + formatNumberToString( car.get("origVal")),HELVET_10_NORMAL));
                columnThreeTable.addCell(cell);
                
                containerTable.addCell(columnOneTable);
                containerTable.addCell(columntwotable);
                containerTable.addCell(columnThreeTable);
                
                if(showNotes){
	                columnFourTable = new PdfPTable(1);
	                cell = new PdfPCell();
	                cell.setBackgroundColor(TABLE_HEADER_COLOR);
	                cell.addElement(new Chunk("Selling Notes", HELVET_11_BOLD));
	                columnFourTable.addCell(cell);
	                
	                cell = new PdfPCell();
	                cell.setBorder(Rectangle.NO_BORDER);
	                cell.addElement(new Chunk(nullSafeToString( car.get("notes")),HELVET_10_NORMAL));
	                columnFourTable.addCell(cell);
	                containerTable.addCell(columnFourTable);
                }   
            }

            doc.add(containerTable);
        }
    }

    private void createAdvertisingPdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable rightColumnTable;
        
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 55, 45 };
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            cell = new PdfPCell();
            cell.setBackgroundColor(GROUP_HEADER_COLOR);
            cell.setColspan(2);
            Chunk advertiserInfo = new Chunk(new Chunk(result.get("advertiser").toString() 
                    + result.get("fax").toString() 
                    + result.get("contact").toString(), 
                    FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(255, 255,255)))); 
            cell.addElement(advertiserInfo);
            containerTable.addCell(cell);
            
            List<HashMap> cars = (List<HashMap>) result.get("cars");
            for (HashMap car:cars){
                leftColumnTable = new PdfPTable(1);
                leftColumnTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                leftColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph vehInfo = new Paragraph();
                vehInfo.add(new Chunk( nullSafeToString(car.get("desc")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Vin #: " + nullSafeToString(car.get("vin")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Stock #: " + nullSafeToString(car.get("stock")), HELVET_10_NORMAL));
                vehInfo.add(Chunk.NEWLINE);
                vehInfo.add(new Chunk("Mileage: " + formatNumberToString( car.get("mileage")), HELVET_10_NORMAL));
                cell.addElement(vehInfo);
                leftColumnTable.addCell(cell);
                
                rightColumnTable = new PdfPTable(1);
                rightColumnTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.addElement(new Chunk("Internet Price", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.addElement(new Chunk("$" + formatNumberToString( Double.valueOf( nullSafeToString( car.get("price")) )),HELVET_10_NORMAL));
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk("Liner Ad Text", HELVET_11_BOLD));
                cell.setBackgroundColor(TABLE_HEADER_COLOR);
                rightColumnTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.addElement(new Chunk( nullSafeToString(car.get("notes")),HELVET_10_NORMAL));
                cell.setBorder(Rectangle.NO_BORDER);
                rightColumnTable.addCell(cell);
                
                containerTable.addCell(leftColumnTable);
                containerTable.addCell(rightColumnTable);
            }

            doc.add(containerTable);
        }
    }

    @SuppressWarnings("unchecked")
    private void createModelPromotionPdf(Document doc, List<Map<String, Object>> results) throws DocumentException {
        PdfPTable containerTable;
        PdfPTable leftColumnTable;
        PdfPTable innerTable;
        PdfPTable rightColumnTable;
        
        PdfPCell cell;
        for (Map<String,Object> result:results) {
            float[] tableWidths = new float[] { 50, 50 };
            containerTable = new PdfPTable(tableWidths);
            containerTable.setWidthPercentage(100);
            containerTable.getDefaultCell().setPadding(0);
            
            leftColumnTable = new PdfPTable(1);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Vehicle Description", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            leftColumnTable.addCell(cell);
            
            innerTable = new PdfPTable(3);
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.addElement(new Chunk( nullSafeToString(result.get("desc")), HELVET_11_BOLD));
            cell.setColspan(3);
            innerTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.addElement(new Chunk("Age", HELVET_11_BOLD));
            innerTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.addElement(new Chunk("Year", HELVET_11_BOLD));
            innerTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.addElement(new Chunk("Stock", HELVET_11_BOLD));
            innerTable.addCell(cell);
            
            List<HashMap> cars = (List<HashMap>) result.get("cars");
            for (HashMap car:cars){
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.addElement(new Chunk( nullSafeToString(car.get("age")), HELVET_10_NORMAL));
                innerTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.addElement(new Chunk( nullSafeToString(car.get("year")), HELVET_10_NORMAL));
                innerTable.addCell(cell);
                
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                cell.addElement(new Chunk( nullSafeToString(car.get("stock")), HELVET_10_NORMAL));
                innerTable.addCell(cell);
            }
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            cell.addElement(innerTable);
            leftColumnTable.addCell(cell);
            
            rightColumnTable = new PdfPTable(2);
            cell = new PdfPCell();
            cell.addElement(new Chunk("Promotion Start Date", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk("Promotion End Date", HELVET_11_BOLD));
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString(result.get("startDate")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.addElement(new Chunk( nullSafeToString(result.get("endDate")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
           
            cell = new PdfPCell();
            cell.setColspan(2);
            cell.setBackgroundColor(TABLE_HEADER_COLOR);
            cell.addElement(new Chunk("Notes", HELVET_11_BOLD));
            rightColumnTable.addCell(cell);
            
            cell = new PdfPCell();
            cell.setColspan(2);
            cell.addElement(new Chunk( nullSafeToString(result.get("notes")), HELVET_10_NORMAL));
            rightColumnTable.addCell(cell);
            
            containerTable.addCell(leftColumnTable);
            containerTable.addCell(rightColumnTable);

            doc.add(containerTable);
        }
    }
    

    private void createSpiffLotOtherPdf(Document doc, List<Map<String,Object>> results, ActionPlanReport reportType) throws DocumentException {
        float[] columnWidths = new float[] { 40, 8, 14, 8, 30 };
        PdfPTable mainTable = new PdfPTable(columnWidths);
        mainTable.setWidthPercentage(100);
        mainTable.getDefaultCell().setLeading(0,0);
        
        PdfPCell vehicleDescriptionHeader = new PdfPCell();
        vehicleDescriptionHeader.addElement(new Chunk("Vehicle Description", HELVET_8_BOLD));
        vehicleDescriptionHeader.setBackgroundColor(TABLE_HEADER_COLOR);

        PdfPCell mileageHeader = new PdfPCell();
        mileageHeader.addElement(new Chunk("Mileage", HELVET_8_BOLD));
        mileageHeader.setBackgroundColor(TABLE_HEADER_COLOR);

        PdfPCell stockNumberHeader = new PdfPCell();
        stockNumberHeader.addElement(new Chunk("Stock Number", HELVET_8_BOLD));
        stockNumberHeader.setBackgroundColor(TABLE_HEADER_COLOR);

        PdfPCell listPriceHeader = new PdfPCell();
        listPriceHeader.addElement(new Chunk("Internet Price", HELVET_8_BOLD));
        listPriceHeader.setBackgroundColor(TABLE_HEADER_COLOR);

        PdfPCell spiffNotesHeader = new PdfPCell();
        spiffNotesHeader.addElement(new Chunk((reportType == ActionPlanReport.SPIFF ? "Spiff Notes": "Notes"), HELVET_8_BOLD));
        spiffNotesHeader.setBackgroundColor(TABLE_HEADER_COLOR);

        mainTable.addCell(vehicleDescriptionHeader);
        mainTable.addCell(mileageHeader);
        mainTable.addCell(stockNumberHeader);
        mainTable.addCell(listPriceHeader);
        mainTable.addCell(spiffNotesHeader);
        
        
        for (Map<String,Object> result:results) {
        	PdfPCell vehicleInfoCell = new PdfPCell();
        	vehicleInfoCell.setLeading(0,0);
            Paragraph vehicleInfo = new Paragraph();
            vehicleInfo.add(new Chunk( nullSafeToString(result.get("desc")), HELVET_8_NORMAL));
            vehicleInfo.add(Chunk.NEWLINE);
            vehicleInfo.add(new Chunk("Vin #: " + nullSafeToString(result.get("vin")), HELVET_8_NORMAL));
            vehicleInfoCell.addElement(vehicleInfo);
            
            PdfPCell mileageCell = new PdfPCell();
            mileageCell.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);
            Paragraph mileageInfo = new Paragraph();
            mileageInfo.add(new Chunk( formatNumberToString( result.get("mileage")), HELVET_8_NORMAL));
            mileageCell.addElement(mileageInfo);

            PdfPCell stockNumberCell = new PdfPCell();
            stockNumberCell.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);
            Paragraph stockNumberInfo = new Paragraph();
            stockNumberInfo.add(new Chunk( nullSafeToString(result.get("stock")), HELVET_8_NORMAL));
            stockNumberCell.addElement(stockNumberInfo);
            
            PdfPCell listPriceCell = new PdfPCell();
            listPriceCell.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);
            Paragraph listPriceInfo = new Paragraph();
            listPriceInfo.add(new Chunk( formatNumberToString( result.get("listPrice")), HELVET_8_NORMAL));
            listPriceCell.addElement(listPriceInfo);
            
            PdfPCell spiffCell = new PdfPCell();
            spiffCell.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);
            Paragraph spiffInfo = new Paragraph();
            spiffInfo.add(new Chunk( nullSafeToString(result.get("notes")), HELVET_8_NORMAL));
            spiffCell.addElement(spiffInfo);
            
            mainTable.addCell(vehicleInfoCell);
            mainTable.addCell(mileageCell);
            mainTable.addCell(stockNumberCell);
            mainTable.addCell(listPriceCell);
            mainTable.addCell(spiffCell);            
        }
        doc.add(mainTable);
    }
    
    private static String formatNumberToString( Object number )
    {
        String formattedNum = "";
        if ( number != null && StringUtils.isNotBlank( number.toString() ) )
        {
        	formattedNum = NumberFormat.getIntegerInstance().format(number);
        }
        return formattedNum;
    }
    
    private static String nullSafeToString( Object obj ) {
    	if ( obj == null ) {
    		return "";
    	}
    	return obj.toString();
    }
    

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String title) {
        this.reportName = title;
    }

    public PdfTemplate getTpl() {
        return tpl;
    }

    public void setTpl(PdfTemplate tpl) {
        this.tpl = tpl;
    }

}
