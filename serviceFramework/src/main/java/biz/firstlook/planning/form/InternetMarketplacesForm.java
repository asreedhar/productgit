package biz.firstlook.planning.form;

import org.apache.struts.action.ActionForm;

public class InternetMarketplacesForm extends ActionForm
{

/**
	 * 
	 */
private static final long serialVersionUID = -3222206590779243346L;
private Boolean includePhotos;
private Boolean inlcludeAnnouncements;
private String announcements;
private Integer inventoryId;
private String marketplaceName;
private Boolean exported;
private String activeInternetMarketplace;

public Boolean getExported() {
	return exported;
}

public void setExported(Boolean exported) {
	this.exported = exported;
}

public String getAnnouncements()
{
	return announcements;
}
public void setAnnouncements( String announcements )
{
	this.announcements = announcements;
}
public Boolean getIncludePhotos()
{
	return includePhotos;
}
public void setIncludePhotos( Boolean includePictures )
{
	this.includePhotos = includePictures;
}
public Boolean getInlcludeAnnouncements()
{
	return inlcludeAnnouncements;
}
public void setInlcludeAnnouncements( Boolean inlcludeAnnouncements )
{
	this.inlcludeAnnouncements = inlcludeAnnouncements;
}
public Integer getInventoryId()
{
	return inventoryId;
}
public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}
public String getMarketplaceName()
{
	return marketplaceName;
}
public void setMarketplaceName( String marketplaceName )
{
	this.marketplaceName = marketplaceName;
}

public String getActiveInternetMarketplace() {
	return activeInternetMarketplace;
}

public void setActiveInternetMarketplace(String activeInternetMarketplace) {
	this.activeInternetMarketplace = activeInternetMarketplace;
}


}
