package biz.firstlook.planning.form;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.TradeOrPurchaseEnum;

/**
 * This class is a value object to ease the transition from maps to objects.
 * @author bfung
 */
public class InventoryItemPlanInfo implements Serializable {
	
	private static final long serialVersionUID = 9092920732915544122L;
	
	private Integer inventoryId;
	private Integer rangeId; //this refers to the inventory age bucket range
	private Integer ageInDays;
	private Integer currentVehicleLight;
	private String vehicleYear;
	private String make;
	private String model;
	private Integer makeModelGroupingId;
	private String groupingDescription;
	private Integer groupingDescriptionId;
	private String vehicleTrim;
	private String bodyType;
	private String baseColor;
	private Integer mileageReceived;
	private Float listPrice;
	private Float lotPrice;
	private Float unitCost;
	private TradeOrPurchaseEnum tradeOrPurchase;
	private String stockNumber;
	private Integer primaryBookValue;
	private Integer secondaryBookValue;
	private String level4Analysis;
	private Date lastPlannedDate;
	private Date planReminderDate;
	private Boolean hasSalesHistory;
	private Boolean isAccurate;
	private Integer unitsSold;
	private Integer avgRetailGrossProfit;
	private Integer avgDays2Sale;
	private Integer noSales;
	private Integer unitsInStock;
	private String inventoryStatusCodeShortDescription;
	private String vehicleLocation;
	private String adDescription;
	private Float edmundsTMV;
	private Float transferPrice;
	private Boolean transferForRetailOnly;
	private boolean transferForRetailOnlyEnabled;	
	private InventoryItemPlanSummaryInfo planSummary = new InventoryItemPlanSummaryInfo(this);
	private Float bookVsCost;
	private Integer mmrValue;
	private Float mmrBookVsCost;
	
	private String exportingAdvertisers;
	
	public Map<String, Object> getSpiff() {
		InventoryItemPlanSummaryDetailStrategy latest = getLatestDetailStrategy(PlanningEventTypeEnum.SPIFF);
		Map<String, Object> props = new HashMap<String, Object>();
		if(latest != null) {
			props.put("id", inventoryId);
			props.put("spiffNotes", latest.getNotes());
		} 
		return props;
	}
	
	public Map<String, Object> getAuction() {
		InventoryItemPlanSummaryDetailStrategy latest = getLatestDetailStrategy(PlanningEventTypeEnum.AUCTION);
		Map<String, Object> props = new HashMap<String, Object>();
		if(latest != null) {
			props.put("id", Functions.nullSafeIntValue(latest.getThirdPartyEntityId()));
			props.put("thirdParty", latest.getThirdParty());
		} 
		return props;
	}
	
	public Map<String, Object> getWholesaler() {
		InventoryItemPlanSummaryDetailStrategy latest = getLatestDetailStrategy(PlanningEventTypeEnum.WHOLESALER);
		Map<String, Object> props = new HashMap<String, Object>();
		if(latest != null) {
			props.put("id", Functions.nullSafeIntValue(latest.getThirdPartyEntityId()));
			props.put("thirdParty", latest.getThirdParty());
		} 
		return props;
	}
	
	private InventoryItemPlanSummaryDetailStrategy getLatestDetailStrategy(PlanningEventTypeEnum eventType) {
		List<InventoryItemPlanSummaryDetailStrategy> details = planSummary.getDetailedStrategies();
		InventoryItemPlanSummaryDetailStrategy latest = null;
		for(InventoryItemPlanSummaryDetailStrategy detail : details) {
			if(detail.getStrategyEnum().equals(eventType)) {
				if(latest == null 
						|| Functions.nullSafeCompareTo(latest.getBeginDate(), detail.getBeginDate()) >= 0) {
					latest = detail;
				}
			}
		}
		return latest;
	}
	
	public Map<Integer, Boolean> getActiveAds() {
	    List<InventoryItemPlanSummaryDetailStrategy> detailPlans = planSummary.getDetailedStrategies();
	    Map<Integer, Boolean> activeAds = new HashMap<Integer, Boolean>();
	    for (InventoryItemPlanSummaryDetailStrategy plan:detailPlans) {
	    	if (plan.getThirdPartyEntityId() != null ){
	    		activeAds.put( plan.getThirdPartyEntityId(), true);
	    	}
	    }
	    return activeAds;
	}
	
	//generated getters and setters
	public Integer getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}
	public Integer getRangeId() {
		return rangeId;
	}
	public void setRangeId(Integer rangeID) {
		this.rangeId = rangeID;
	}
	public Integer getAgeInDays() {
		return ageInDays;
	}
	public void setAgeInDays(Integer ageInDays) {
		this.ageInDays = ageInDays;
	}
	public Integer getCurrentVehicleLight() {
		return currentVehicleLight;
	}
	public void setCurrentVehicleLight(Integer currentVehicleLight) {
		this.currentVehicleLight = currentVehicleLight;
	}
	public String getVehicleYear() {
		return vehicleYear;
	}
	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Integer getMakeModelGroupingId() {
		return makeModelGroupingId;
	}
	public void setMakeModelGroupingId(Integer makeModelGroupingId) {
		this.makeModelGroupingId = makeModelGroupingId;
	}
	public String getGroupingDescription() {
		return groupingDescription;
	}
	public void setGroupingDescription(String groupingDescription) {
		this.groupingDescription = groupingDescription;
	}
	public Integer getGroupingDescriptionId() {
		return groupingDescriptionId;
	}
	public void setGroupingDescriptionId(Integer groupingDescriptionId) {
		this.groupingDescriptionId = groupingDescriptionId;
	}
	public String getVehicleTrim() {
		return vehicleTrim;
	}
	public void setVehicleTrim(String vehicleTrim) {
		this.vehicleTrim = vehicleTrim;
	}
	public String getBodyType() {
		return bodyType;
	}
	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}
	public String getBaseColor() {
		return baseColor;
	}
	public void setBaseColor(String baseColor) {
		this.baseColor = baseColor;
	}
	public Integer getMileageReceived() {
		return mileageReceived;
	}
	public void setMileageReceived(Integer mileageReceived) {
		this.mileageReceived = mileageReceived;
	}
	public Float getListPrice() {
		return listPrice;
	}
	public void setListPrice(Float listPrice) {
		this.listPrice = listPrice;
	}
	public Float getLotPrice() {
		return lotPrice;
	}
	public void setLotPrice(Float lotPrice) {
		this.lotPrice = lotPrice;
	}
	public Float getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(Float unitCost) {
		this.unitCost = unitCost;
	}
	public TradeOrPurchaseEnum getTradeOrPurchase() {
		return tradeOrPurchase;
	}
	public void setTradeOrPurchase(TradeOrPurchaseEnum tradeOrPurchase) {
		this.tradeOrPurchase = tradeOrPurchase;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public Integer getPrimaryBookValue() {
		return primaryBookValue;
	}
	public void setPrimaryBookValue(Integer primaryBookValue) {
		this.primaryBookValue = primaryBookValue;
	}
	public Integer getSecondaryBookValue() {
		return secondaryBookValue;
	}
	public void setSecondaryBookValue(Integer secondaryBookValue) {
		this.secondaryBookValue = secondaryBookValue;
	}
	public String getLevel4Analysis() {
		return level4Analysis;
	}
	public void setLevel4Analysis(String level4Analysis) {
		this.level4Analysis = level4Analysis;
	}
	public Date getLastPlannedDate() {
		return lastPlannedDate;
	}
	public void setLastPlannedDate(Date lastPlannedDate) {
		this.lastPlannedDate = lastPlannedDate;
	}
	public Date getPlanReminderDate() {
		return planReminderDate;
	}
	public void setPlanReminderDate(Date planReminderDate) {
		this.planReminderDate = planReminderDate;
	}
	public Boolean getHasSalesHistory() {
		return hasSalesHistory;
	}
	public void setHasSalesHistory(Boolean hasSalesHistory) {
		this.hasSalesHistory = hasSalesHistory;
	}
	public Boolean getIsAccurate() {
		return isAccurate;
	}
	public void setIsAccurate(Boolean isAccurate) {
		this.isAccurate = isAccurate;
	}
	public Integer getUnitsSold() {
		return unitsSold;
	}
	public void setUnitsSold(Integer unitsSold) {
		this.unitsSold = unitsSold;
	}
	public Integer getAvgRetailGrossProfit() {
		return avgRetailGrossProfit;
	}
	public void setAvgRetailGrossProfit(Integer avgRetailGrossProfit) {
		this.avgRetailGrossProfit = avgRetailGrossProfit;
	}
	public Integer getAvgDays2Sale() {
		return avgDays2Sale;
	}
	public void setAvgDays2Sale(Integer avgDays2Sale) {
		this.avgDays2Sale = avgDays2Sale;
	}
	public Integer getNoSales() {
		return noSales;
	}
	public void setNoSales(Integer noSales) {
		this.noSales = noSales;
	}
	public Integer getUnitsInStock() {
		return unitsInStock;
	}
	public void setUnitsInStock(Integer unitsInStock) {
		this.unitsInStock = unitsInStock;
	}
	public String getInventoryStatusCodeShortDescription() {
		return inventoryStatusCodeShortDescription;
	}
	public void setInventoryStatusCodeShortDescription(
			String inventoryStatusCodeShortDescription) {
		this.inventoryStatusCodeShortDescription = inventoryStatusCodeShortDescription;
	}
	public String getVehicleLocation() {
		return vehicleLocation;
	}
	public void setVehicleLocation(String vehicleLocation) {
		this.vehicleLocation = vehicleLocation;
	}
	public String getAdDescription() {
		return adDescription;
	}
	public void setAdDescription(String adDescription) {
		this.adDescription = adDescription;
	}
	public Float getEdmundsTMV() {
		return edmundsTMV;
	}
	public void setEdmundsTMV(Float edmundsTMV) {
		this.edmundsTMV = edmundsTMV;
	}
	public Float getTransferPrice() {
		return transferPrice;
	}
	public void setTransferPrice(Float transferPrice) {
		this.transferPrice = transferPrice;
	}
	public InventoryItemPlanSummaryInfo getPlanSummary() {
		return planSummary;
	}
	public void setPlanSummary(InventoryItemPlanSummaryInfo planSummary) {
		this.planSummary = planSummary;
	}
	public Float getBookVsCost() {
		return bookVsCost;
	}
	public void setBookVsCost(Float bookVsCost) {
		this.bookVsCost = bookVsCost;
	}
	public String getExportingAdvertisers() {
		return exportingAdvertisers;
	}
	public void setExportingAdvertisers(String exportingAdvertisers) {
		this.exportingAdvertisers = exportingAdvertisers;
	}

	public Boolean getTransferForRetailOnly() {
		if(!transferForRetailOnlyEnabled) {
			return false;
		}
		return transferForRetailOnly;
	}

	public void setTransferForRetailOnly(Boolean transferForRetailOnly) {
		if(!transferForRetailOnlyEnabled) {
			this.transferForRetailOnly = Boolean.FALSE;
		} else {
			this.transferForRetailOnly = transferForRetailOnly;
		}
	}

	public boolean isTransferForRetailOnlyEnabled() {
		return transferForRetailOnlyEnabled;
	}

	public void setTransferForRetailOnlyEnabled(boolean transferForRetailOnlyEnabled) {
		this.transferForRetailOnlyEnabled = transferForRetailOnlyEnabled;
	}
	public Integer getMmrValue() {
		return mmrValue;
	}
	public void setMmrValue(Integer mmrValue) {
		this.mmrValue = mmrValue;
	}
	public Float getMmrBookVsCost() {
		return mmrBookVsCost;
	}
	public void setMmrBookVsCost(Float mmrBookVsCost) {
		this.mmrBookVsCost = mmrBookVsCost;
	}
}
