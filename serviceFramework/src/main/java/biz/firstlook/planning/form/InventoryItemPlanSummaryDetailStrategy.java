package biz.firstlook.planning.form;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;


public class InventoryItemPlanSummaryDetailStrategy implements Serializable {
	
	private static final long serialVersionUID = -564502680335558851L;

	//reference back to parent object
	private InventoryItemPlanSummaryInfo planSummary;
	
	private Integer value;
	private Date beginDate;
	private Integer thirdPartyEntityId;
	private Integer aip_EventTypeID;
	private String notes;
	private String notes2;
	private String notes3;
	private Date endDate;
	private String thirdParty;
	private Date userBeginDate;
	private Date userEndDate;
	
	//purposely left package scoped!
	InventoryItemPlanSummaryDetailStrategy(InventoryItemPlanSummaryInfo planSummary) {
		this.planSummary = planSummary;
	}
	
	public InventoryItemPlanSummaryInfo getInventoryItemPlanSummaryInfo() {
		return planSummary;
	}
	
	void setInventoryItemPlanSummaryInfo(InventoryItemPlanSummaryInfo planSummary) {
		this.planSummary = planSummary;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Integer getThirdPartyEntityId() {
		return thirdPartyEntityId;
	}

	public void setThirdPartyEntityId(Integer thirdPartyEntityId) {
		this.thirdPartyEntityId = thirdPartyEntityId;
	}
	
	public Integer getAIP_EventTypeID() {
		return aip_EventTypeID;
	}

	public void setAIP_EventTypeID(Integer AIP_EventTypeID) {
		this.aip_EventTypeID = AIP_EventTypeID;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNotes2() {
		return notes2;
	}

	public void setNotes2(String notes2) {
		this.notes2 = notes2;
	}

	public String getNotes3() {
		return notes3;
	}

	public void setNotes3(String notes3) {
		this.notes3 = notes3;
	}

	public PlanningEventTypeEnum getStrategyEnum() {
		return PlanningEventTypeEnum.getPlanEventTypeEnum(aip_EventTypeID);
	}
	
	public String getStrategy() {
		return getStrategyEnum().getDescription();
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(String thirdParty) {
		this.thirdParty = thirdParty;
	}

	public Date getUserBeginDate() {
		return userBeginDate;
	}

	public void setUserBeginDate(Date userBeginDate) {
		this.userBeginDate = userBeginDate;
	}

	public Date getUserEndDate() {
		return userEndDate;
	}

	public void setUserEndDate(Date userEndDate) {
		this.userEndDate = userEndDate;
	}

	public boolean isActive() {
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		boolean active = true;
		switch(aip_EventTypeID) {
			case 3: 
				active = endDate == null || endDate.after(today) || today.equals(endDate);
				break;
			default:
				break;
		}
		return active;
	}

}
