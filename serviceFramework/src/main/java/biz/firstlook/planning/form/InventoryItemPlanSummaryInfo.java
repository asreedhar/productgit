package biz.firstlook.planning.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InventoryItemPlanSummaryInfo implements Serializable {
	
	private static final long serialVersionUID = -2538506494595235429L;

	//reference back to parent object
	private InventoryItemPlanInfo planInfo;
	
	private Boolean marketplacePosted;
	private Boolean neverPlanned;
	private List<String> strategies = new ArrayList<String>();
	private Boolean repriced;
	private List<InventoryItemPlanSummaryDetailStrategy> detailedStrategies = new ArrayList<InventoryItemPlanSummaryDetailStrategy>();
	private Boolean hasCurrentPlan;
	private Boolean dueForReplanning;
	private String objective;
	private Boolean noPlan;
	
	InventoryItemPlanSummaryInfo(InventoryItemPlanInfo planInfo) {
		this.planInfo = planInfo;
	}
	
	public InventoryItemPlanInfo getInventoryItemPlanInfo() {
		return planInfo;
	}
	
	public Boolean getMarketplacePosted() {
		return marketplacePosted;
	}
	public void setMarketplacePosted(Boolean marketplacePosted) {
		this.marketplacePosted = marketplacePosted;
	}
	public Boolean getNeverPlanned() {
		return neverPlanned;
	}
	public void setNeverPlanned(Boolean neverPlanned) {
		this.neverPlanned = neverPlanned;
	}
	public List<String> getStrategies() {
		return Collections.unmodifiableList(strategies);
	}
	public boolean add(String strategy) {
		return strategies.add(strategy);
	}
	public Boolean getRepriced() {
		return repriced;
	}
	public void setRepriced(Boolean repriced) {
		this.repriced = repriced;
	}
	public List<InventoryItemPlanSummaryDetailStrategy> getDetailedStrategies() {
		return Collections.unmodifiableList(detailedStrategies);
	}
	
	public InventoryItemPlanSummaryDetailStrategy newDetailStrategy() {
		InventoryItemPlanSummaryDetailStrategy child = new InventoryItemPlanSummaryDetailStrategy(this); 
		detailedStrategies.add(child);
		return child;
	}

	public Boolean getHasCurrentPlan() {
		return hasCurrentPlan;
	}
	public void setHasCurrentPlan(Boolean hasCurrentPlan) {
		this.hasCurrentPlan = hasCurrentPlan;
	}
	public Boolean getDueForReplanning() {
		return dueForReplanning;
	}
	public void setDueForReplanning(Boolean dueForReplanning) {
		this.dueForReplanning = dueForReplanning;
	}
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}
	public Boolean getNoPlan() {
		return noPlan;
	}
	public void setNoPlan(Boolean noPlan) {
		this.noPlan = noPlan;
	}
}
