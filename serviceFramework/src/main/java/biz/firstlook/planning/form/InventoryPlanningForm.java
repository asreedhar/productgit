package biz.firstlook.planning.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.QuickPlanAd;

public class InventoryPlanningForm extends ActionForm{

    private static final long serialVersionUID = -6733843349051760959L;
    
    private List<PrintAdvertiser_InventoryThirdParty> activeAdvertisers;
    private Integer[] activeAdvertiserIds;
    private List<QuickPlanAd> quickAdvertisers;
    private Map runDates;
    private String[] inventoryStatusCD;
    private int filterId;
    private int rangeId;
    private int dueDays;
    private int plannedDays;
    private String age;
    private String[] ageCustom;
    private String risk;
    private String[] riskCustom;
    private String objective;
    private String[] objectiveCustom;
    private String mileage;
    private boolean hasNoAdvertisers;
    
    public InventoryPlanningForm(){
        super();
        this.runDates = new HashMap();
        this.quickAdvertisers = new ArrayList<QuickPlanAd>();
        this.filterId = 2;
        this.dueDays = 0;
        this.rangeId = 0;
        this.plannedDays = 0;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	this.activeAdvertiserIds = null;
    	this.rangeId = 0;
    	this.runDates = new HashMap();
    	if ( this.inventoryStatusCD == null )
    		this.inventoryStatusCD = new String[0];
        this.filterId = 2;
        this.dueDays = 0;
        this.plannedDays = 0;
        this.age = "";
        this.ageCustom = new String[0];
        this.risk = "";
        this.riskCustom = new String[0];
        this.objective  = "";
        this.objectiveCustom = new String[0];
        this.mileage = "";
        
        if(quickAdvertisers != null) {
        	for(QuickPlanAd quickPlanAd : quickAdvertisers) {
        		quickPlanAd.setAdvertiserIds(null);
        	}
        }
    }

    public List<PrintAdvertiser_InventoryThirdParty> getActiveAdvertisers() {
        return activeAdvertisers;
    }

    public void setActiveAdvertisers(List<PrintAdvertiser_InventoryThirdParty> activeAdvertisers) {
        this.activeAdvertisers = activeAdvertisers;
    }

    public List<QuickPlanAd> getQuickAdvertisers() {
        return quickAdvertisers;
    }
    
    public void setQuickAdvertisers(List<QuickPlanAd> quickAdvertisers) {
        this.quickAdvertisers = quickAdvertisers;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String[] getAgeCustom() {
        return ageCustom;
    }

    public void setAgeCustom(String[] ageCustom) {
        this.ageCustom = ageCustom;
    }

    public int getDueDays() {
        return dueDays;
    }

    public void setDueDays(int dueDays) {
        this.dueDays = dueDays;
    }

    public int getFilterId() {
        return filterId;
    }

    public void setFilterId(int filterId) {
        this.filterId = filterId;
    }

    public String[] getInventoryStatusCD() {
        return inventoryStatusCD;
    }

    public void setInventoryStatusCD(String[] inventoryStatusCD) {
        this.inventoryStatusCD = inventoryStatusCD;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String[] getObjectiveCustom() {
        return objectiveCustom;
    }

    public void setObjectiveCustom(String[] objectiveCustom) {
        this.objectiveCustom = objectiveCustom;
    }

    public int getPlannedDays() {
        return plannedDays;
    }

    public void setPlannedDays(int plannedDays) {
        this.plannedDays = plannedDays;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public String[] getRiskCustom() {
        return riskCustom;
    }

    public void setRiskCustom(String[] riskCustom) {
        this.riskCustom = riskCustom;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public Integer[] getActiveAdvertiserIds() {
    	ArrayList<Integer> advertiserIds = new ArrayList<Integer>();
    	if (this.activeAdvertiserIds != null && this.activeAdvertiserIds.length > 0) {
    		for (int id : this.activeAdvertiserIds) {
    			advertiserIds.add(Integer.valueOf(id));
    		}
    	} else {
    		if (activeAdvertisers != null && !activeAdvertisers.isEmpty()) {
				for (PrintAdvertiser_InventoryThirdParty advertiser : getActiveAdvertisers()) {
					advertiserIds.add(advertiser.getId());
				}
    		}
    	}
    	if (advertiserIds.isEmpty()) {
    		return null;
    	} else {
    		return advertiserIds.toArray(new Integer[advertiserIds.size()]);
    	}
    }

    public void setActiveAdvertiserIds(Integer[] activeAdvertiserIds) {
        this.activeAdvertiserIds = activeAdvertiserIds;
    }

    public Map getRunDates() {
        return runDates;
    }

    public void setRunDates(Map runDates) {
        this.runDates = runDates;
    }
    
    public Object getRunDates(String key) {
        return runDates.get(key);
    }

    @SuppressWarnings("unchecked")
    public void setRunDates(String key, Object date) {
        this.runDates.put(key, date);
    }

    public boolean isHasNoAdvertisers() {
        return hasNoAdvertisers;
    }

    public void setHasNoAdvertisers(boolean hasNoAdvertisers) {
        this.hasNoAdvertisers = hasNoAdvertisers;
    }

	public int getRangeId() {
		return rangeId;
	}

	public void setRangeId(int rangeId) {
		//this.rangeId = rangeId;
	}
	
	public void setTab(int tabId) {
		this.rangeId = tabId;
	}
    
   

}

