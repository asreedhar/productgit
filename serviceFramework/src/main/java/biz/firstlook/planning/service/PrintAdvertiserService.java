package biz.firstlook.planning.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.struts.action.DynaActionForm;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.commons.collections.Pair;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.action.InventoryVehicleInfoBean;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserAdBuilderOption;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookup;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookupEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.persistence.InventoryThirdPartyDAO;
import biz.firstlook.planning.persistence.InventoryVehicleOptionsDAO;
import biz.firstlook.planning.persistence.PrintAdvertiserDAO;
import biz.firstlook.planning.persistence.ThirdPartyEnumFilter;
import biz.firstlook.report.dao.IBasicIMTLookUpDAO;

public class PrintAdvertiserService
{

private PrintAdvertiserDAO printAdvertiserDAO;
private InventoryThirdPartyDAO inventoryThirdPartyDAO;
private IBasicIMTLookUpDAO basicIMTLookUpDAO;
private InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO;
private IInventoryPlanEventDAO inventoryPlanEventDAO;

/**
 * this method takes in the list of printAdvertiserTextOptions and converts them into a list of printADvertiserAdBuilderOptions
 * with defaults populated (ie - sets status = true and display order)
 * @param List< PrintAdvertiserOptionLookup >
 * @return List< PrintAdvertiserAdBuilderOption >
 */
public List< PrintAdvertiserAdBuilderOption > makeDefaultListOfOptions( List< PrintAdvertiserOptionLookup > optionChoices )
{
	List< PrintAdvertiserAdBuilderOption > toBeReturned = new ArrayList<PrintAdvertiserAdBuilderOption>();
	Iterator<PrintAdvertiserOptionLookup> iter = optionChoices.iterator();
	while ( iter.hasNext() )
	{
		PrintAdvertiserOptionLookup option = iter.next();
		PrintAdvertiserAdBuilderOption newOption = new PrintAdvertiserAdBuilderOption();
		newOption.setDisplayOrder( option.getId() );
		newOption.setOption( option );
		newOption.setStatus( Boolean.TRUE );
		toBeReturned.add( newOption );
	}
	return toBeReturned;
}

//the build default list means we want all of the options to be checked
public String[] getSelectedOptionsAsArray( List<PrintAdvertiserAdBuilderOption> options )
{
	List<String> selectedOptions = new ArrayList<String>();
	for ( PrintAdvertiserAdBuilderOption option : options )
	{
		if ( option.getStatus() )
		{
			selectedOptions.add( option.getOption().getId().toString() );
		}
	}
	return (String[])selectedOptions.toArray( new String[selectedOptions.size()] );
}

public List<PrintAdvertiser_InventoryThirdParty> retreivePrintAdvertisers( Integer businessUnitId )
{
    return printAdvertiserDAO.retreivePrintAdvertisers( businessUnitId );
}

public Integer retreiveNumPlannedVehiclesByAdvertiser( PrintAdvertiser_InventoryThirdParty printAdvertiser )
{
	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy");
	String today = dateFormat.format( DateUtils.truncate( new Date(), Calendar.DATE ) );
	
	StringBuilder sb = new StringBuilder(" select distinct inventoryId from " );
	sb.append( " AIP_Event where thirdPartyEntityId = ").append(printAdvertiser.getId());
	sb.append( " and endDate >= '").append(today).append("'");
	List< Map<String, Object> > results = basicIMTLookUpDAO.getResults( sb.toString(), null );
	return results.size();
}

@Transactional(rollbackFor=Exception.class)
public void savePrintAdvertiserInventoryThirdParty( PrintAdvertiser_InventoryThirdParty thirdParty )
{
	// this is temporary for debugging
	//printAdvertiserDAO.getSessionFactory().getCurrentSession().setFlushMode( FlushMode.ALWAYS );
	printAdvertiserDAO.saveOrUpdate( thirdParty );
	//printAdvertiserDAO.getSessionFactory().getCurrentSession().flush();
}

@Transactional(rollbackFor=Exception.class)
public void deletePrintAdvertiserInventoryThirdParty( Integer businessUnitId, PrintAdvertiser_InventoryThirdParty thirdParty )
{
	// before we can delete the third party we have to update all assocaited plans to not join to 
	// this table but instead have 'other' plans
	List<InventoryThirdParty> deleteMe = new ArrayList<InventoryThirdParty>();
	deleteMe.add( thirdParty );
	List<InventoryPlanEvent> updateMe = inventoryPlanEventDAO.findEventsByThirdPartyId( businessUnitId, deleteMe );
	
	for (InventoryPlanEvent event : updateMe )
	{
		event.setNotes2( event.getInventoryThirdParty().getName() );
		event.setInventoryThirdParty( null );
	
		// update the plans
		inventoryPlanEventDAO.saveOrUpdate( event );
	}
	
	// this is temporary for debugging
	//printAdvertiserDAO.getSessionFactory().getCurrentSession().setFlushMode( FlushMode.ALWAYS );
	printAdvertiserDAO.delete( thirdParty );
	//printAdvertiserDAO.getSessionFactory().getCurrentSession().flush();
}


@SuppressWarnings("unchecked")
public PrintAdvertiser_InventoryThirdParty getThirdPartyFromForm( DynaActionForm printAdvertiserMangerForm,
                                                                  Integer businessUnitId )
{
	PrintAdvertiser_InventoryThirdParty thirdParty = null;
	
	// if there is no id passed back, that means this is a new third party
	if ( printAdvertiserMangerForm.get( "selectedPrintAdvertiserId" ) == null || 
		 (Integer)printAdvertiserMangerForm.get( "selectedPrintAdvertiserId" ) == 0 ) 
	{
		thirdParty = new PrintAdvertiser_InventoryThirdParty();
		thirdParty.setBusinessUnitId( businessUnitId );
	}
	else // retrieve third party from the DB
	{
		Integer thirdPartyId = (Integer)printAdvertiserMangerForm.get("selectedPrintAdvertiserId");
		thirdParty = printAdvertiserDAO.findById( thirdPartyId );
	}
	
	thirdParty.setContactEmail( printAdvertiserMangerForm.getString( "printAdvertiserEmail" ) );
	thirdParty.setName( printAdvertiserMangerForm.getString( "printAdvertiserName" ) );
	thirdParty.setDisplayPrice( (Boolean)printAdvertiserMangerForm.get( "showPrice" ) );
	thirdParty.setFaxNumber( printAdvertiserMangerForm.getString( "printAdvertiserFax" ) );
	thirdParty.setInventoryThirdPartyTypeId( InventoryThirdPartyEnum.PRINT_ADVERTISER.getId() );
	thirdParty.setVehicleOptionThirdPartyId( (Integer)printAdvertiserMangerForm.get( "selectedGuideBookOption" ) );
    thirdParty.setQuickAdvertiser((Boolean)printAdvertiserMangerForm.get("showQuickPlan"));
	
	List< PrintAdvertiserAdBuilderOption > options = (List< PrintAdvertiserAdBuilderOption >)printAdvertiserMangerForm.get( "adTextOptions" ); 
	options = updatedSelectedOptions( options, (String[])printAdvertiserMangerForm.get("selectedAdTextOptions"), thirdParty );
	thirdParty.setAdBuilderOptions( options );
	return thirdParty;
}

private List< PrintAdvertiserAdBuilderOption > updatedSelectedOptions( List< PrintAdvertiserAdBuilderOption > options, String[] selectedOptions, PrintAdvertiser_InventoryThirdParty thirdParty )
{
	List<String> selectedOptionsList = new ArrayList<String>(Arrays.asList( selectedOptions ) );
	for ( PrintAdvertiserAdBuilderOption option : options)
	{
		if (selectedOptionsList.contains( option.getOption().getId().toString() ))
		{
			option.setStatus( Boolean.TRUE );
		}
		else
		{
			option.setStatus( Boolean.FALSE );
		}
		
		if( thirdParty != null )
			option.setThirdParty( thirdParty );
	}
	return options;
}

/**
 *	this method builds an ad liner ad in 2 peices, the part that comes before the ad description and the part that comes after.
 *	that's why it returns the string[] 
 * @param inventoryId
 * @param adBuilderOptions
 * @return
 */

public String[] buildAd( Integer inventoryId, ThirdPartyEnum thirdPartyEnum, List< PrintAdvertiserAdBuilderOption > adBuilderOptions )
{
	InventoryVehicleInfoBean inventoryVehicleInfo = inventoryVehicleOptionsDAO.getVehicleInfo( inventoryId, new ThirdPartyEnumFilter(thirdPartyEnum) );
	PrintAdvertiserAdTextBuilder adBuilder = new SimplePrintAdvertiserAdTextBuilder();
	
	for ( PrintAdvertiserAdBuilderOption option : adBuilderOptions )
	{
		Integer optionId = option.getOption().getId(); 
		// when you get to ad description, cut the ad text in half since ad desc is added on front end
		if (optionId.equals( PrintAdvertiserOptionLookupEnum.AD_DESC.getId() ))
		{
			adBuilder.splitForAdDescription();
		} else if( option.getStatus() )	{
			PrintAdvertiserOptionLookupEnum optionEnum = PrintAdvertiserOptionLookupEnum.getPrintAdvertiserOptionLookupEnumById( optionId );
			switch(optionEnum) {
				case COLOR:
					adBuilder.appendColor(inventoryVehicleInfo.getColor());
					break;
				case MILEAGE:
					adBuilder.appendMileage( inventoryVehicleInfo.getMileage());
					break;
				case STOCK_NUM:
					adBuilder.appendStockNumber( inventoryVehicleInfo.getStockNumber() );
					break;
				case VEHICLE_DESC:
					StringBuilder vehicleDescBuff = new StringBuilder();
					
					vehicleDescBuff.append(inventoryVehicleInfo.getYear()).append(" ").append(inventoryVehicleInfo.getMake()).append(" ");
					String trim = null;
					// user chose a guide book, so go out the the third party table to get the description
					if ( !thirdPartyEnum.equals( ThirdPartyEnum.EDMUNDS ) )
					{
						trim = inventoryVehicleInfo.getThirdPartyVehicleDescription( thirdPartyEnum );
						if ( !StringUtils.isBlank( trim ))
							vehicleDescBuff.append( trim );
					}
					
					// if the user chose first look as the source, use our naming scheme
					// second check is in the case where a vehcile is not in the third party
					// this prevents getting an empty vehicle description
					if ( thirdPartyEnum.equals(ThirdPartyEnum.EDMUNDS) 
							|| StringUtils.isBlank( trim ))
					{
						if ( StringUtils.isNotBlank( inventoryVehicleInfo.getVehicleDescription() ) )
						{
							vehicleDescBuff.append( inventoryVehicleInfo.getVehicleDescription());
						}
					}
					adBuilder.appendVehicleDescription( vehicleDescBuff.toString() );
					break;
				case VIN:
					adBuilder.appendVin( inventoryVehicleInfo.getVin() );
					break;
				case OPTIONS:
					List<String> options = inventoryVehicleInfo.getOptions( thirdPartyEnum, true );
					adBuilder.appendOptions( options );
					break;
				default:
					break;
			}
		}
	}
	Pair<String, String> adTextPair = adBuilder.getPrintAdvertiserAd();
	String[] adText = new String[]{adTextPair.getLeft(), adTextPair.getRight()};
	return adText;
}

public static Boolean includeOptionInAd( List< PrintAdvertiserAdBuilderOption > adBuilderOptions, PrintAdvertiserOptionLookupEnum ad_desc )
{
	Iterator< PrintAdvertiserAdBuilderOption > iter = adBuilderOptions.iterator();
	while ( iter.hasNext() )
	{
		PrintAdvertiserAdBuilderOption option = iter.next();
		if ( option.getOption().getId().equals( ad_desc.getId() ))
		{
			return option.getStatus();
		}
	}
	return Boolean.FALSE;
}

public Map< String, List< String >> retreivePrintAdvertisingHisotry( Integer inventoryId )
{
	List< InventoryPlanEvent > rawResults = inventoryPlanEventDAO.retrievePrintAdvertiserEventsGroupedByEndDate( inventoryId );
	Map< String, List< String > > toBeReturned = new LinkedHashMap< String, List< String > >();
	Date lastDate = null;
	List< String > thirdPartyNames = null;
	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy");
	int numOfDaysIncluded = 0;
	for( InventoryPlanEvent event : rawResults )
	{
		if( !event.getEndDate().equals( lastDate ) )
		{
			// history should only display previous 3 days
			if (numOfDaysIncluded == 3 )
			{
				break;
			}
			thirdPartyNames = new ArrayList< String >();
			thirdPartyNames.add( event.getInventoryThirdParty().getName() );
			toBeReturned.put( dateFormat.format( event.getEndDate() ), thirdPartyNames );
			numOfDaysIncluded++;
		}
		else
		{
			toBeReturned.get( dateFormat.format( event.getEndDate() ) ).add( event.getInventoryThirdParty().getName() );
		}

		lastDate = event.getEndDate();
	}
	
	return toBeReturned;
}

@SuppressWarnings("unchecked")
public List< InventoryThirdParty > retriveAllInventoryThirdPartiesWithActivePlansInLast30Days( Integer businessUnitId )
{
	Date thirtyDaysAgo = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -30 );
	List<InventoryPlanEvent> allEvents = inventoryPlanEventDAO.findAllEventsByTypeInTimeRange( businessUnitId, thirtyDaysAgo );
	Set< InventoryThirdParty > thirdParties = new HashSet< InventoryThirdParty >();
	for ( InventoryPlanEvent event : allEvents )
	{
		thirdParties.add( event.getInventoryThirdParty() );
	}
	List< InventoryThirdParty > orderedThirdParties = new ArrayList< InventoryThirdParty >(thirdParties);
	Collections.sort( orderedThirdParties , new BeanComparator("name") );
	return orderedThirdParties;
}

public List<PrintAdvertiser_InventoryThirdParty> retreiveQuickPrintAdvertisers( Integer businessUnitId )
{
    return printAdvertiserDAO.retrieveQuickPrintAdvertisers( businessUnitId );
}

public List< PrintAdvertiserOptionLookup > retrievePrintAdvertiserTextLookupOptions()
{
	return printAdvertiserDAO.retrievePrintAdvertiserTextLookupOptions();
}

public PrintAdvertiser_InventoryThirdParty findPrintAdvertiserInventoryThirdPartyById( Integer id )
{
	return printAdvertiserDAO.findById( id );
}

public void setPrintAdvertiserDAO( PrintAdvertiserDAO printAdvertiserDAO )
{
	this.printAdvertiserDAO = printAdvertiserDAO;
}

public void setBasicIMTLookUpDAO( IBasicIMTLookUpDAO basicIMTLookUpDAO )
{
	this.basicIMTLookUpDAO = basicIMTLookUpDAO;
}

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}

public InventoryThirdPartyDAO getInventoryThirdPartyDAO() {
    return inventoryThirdPartyDAO;
}

public void setInventoryThirdPartyDAO(
        InventoryThirdPartyDAO inventoryThirdPartyDAO) {
    this.inventoryThirdPartyDAO = inventoryThirdPartyDAO;
}

public void setInventoryVehicleOptionsDAO( InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO )
{
	this.inventoryVehicleOptionsDAO = inventoryVehicleOptionsDAO;
}

}
