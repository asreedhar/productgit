package biz.firstlook.planning.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.commons.services.inventory.IInventoryService;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.InventoryBucketRange;
import biz.firstlook.main.commonDAOs.IInventoryBucketRangeDAO;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.module.appraisal.entity.AppraisalActionType;
import biz.firstlook.module.appraisal.entity.impl.AppraisalActionTypeEnum;
import biz.firstlook.module.appraisal.service.IAppraisalService;
import biz.firstlook.planning.entity.InventoryAdvertising;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.persistence.IInventoryAdvertisingDAO;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.persistence.IInventoryPlanThirdPartyDAO;

public class InventoryPlanningRetrievalService implements IInventoryPlanningRetrievalService
{

private IInventoryDAO inventoryDAO;
private IInventoryPlanEventDAO inventoryPlanEventDAO;
private IInventoryPlanThirdPartyDAO inventoryPlanThirdPartyDAO;
private IInventoryAdvertisingDAO inventoryAdvertisingDAO;
private IInventoryBucketRangeDAO inventoryBucketRangeDAO;
private IAppraisalService appraisalService;
private IInventoryService inventoryService;
private static final int DEFAULT_FIRSTLOOK_BUSINESS_UNIT = 100150;
/**
 * Loads an inventory item's current plan information
 */
public Map< String, Object > loadCurrentPlan( Integer inventoryId, Integer businessUnitId, Integer ageInflationForPlanning, boolean checkAppraisalHistory ) throws JSONException
{
	Map< String, Object > currentPlan = new HashMap< String, Object >();
	Inventory inventory = getInventoryDAO().findById( inventoryId );
	
	List< InventoryPlanEvent > planningHistory = getInventoryPlanEventDAO().findPlanHistory( inventoryId );
	List< InventoryPlanEvent > marketplacePostings = getMarketplacePostings( planningHistory );
	List< InventoryPlanEvent > currentEvents = extractCurrentPlanEvents( planningHistory, inventoryId, checkAppraisalHistory );
	List< InventoryPlanEvent > repriceEvents = findRepriceEvents( currentEvents );
	PlanObjectiveEnum planObjectiveEnum = determineObjective( currentEvents );
	Date nextPlanDate = findNextPlanDate( inventory.getId(),  ageInflationForPlanning);
	
	InventoryPlanEvent lastRepriceEvent = findLastRepriceDate( planningHistory );
	Date lastRepriceDate = null;
	if(lastRepriceEvent != null) {
		lastRepriceDate = lastRepriceEvent.getCreated();
	}
	String defaultAdvertisingDescription = loadDefaultDescription( inventoryId );
	List< InventoryThirdParty > thirdParties = getInventoryPlanThirdPartyDAO().findAuctionsAndWholesalers( businessUnitId );
	
	JSONObject jsonPlan = new JSONObject();
	InventoryPlanningUtilService.convertCurrentEventsToJSON( currentEvents, jsonPlan );
	jsonPlan.put( "nextPlanDate", InventoryPlanningUtilService.convertDateToString( nextPlanDate ) );
	jsonPlan.put( "objective", planObjectiveEnum.getJsonKey() );
	jsonPlan.put( "defaultDescription", defaultAdvertisingDescription );
	jsonPlan.put( "certified", inventory.getCertified().toString() );
	jsonPlan.put( "specialFinance", ( inventory.getSpecialFinance() != null ) ? inventory.getSpecialFinance().toString() : "false" );
	InventoryPlanningUtilService.convertThirdPartiesToJSON( thirdParties, jsonPlan );

	currentPlan.put( "json", jsonPlan.toString() );
	currentPlan.put( "currentObjectiveId", planObjectiveEnum.getId() );
	currentPlan.put( "daysUntilReplanning", DateUtilFL.calculateNumberOfCalendarDays( nextPlanDate, new Date() ) );
	currentPlan.put( "lastRepriceDate", lastRepriceDate );
	currentPlan.put( "showPlanHistory", !planningHistory.isEmpty() );
	currentPlan.put( "repriceEvents", repriceEvents );
	currentPlan.put( "marketplacePostings", marketplacePostings );
	currentPlan.put( "inventoryLight", inventory.getCurrentVehicleLight() );
	
	return currentPlan;
}

public List< InventoryThirdParty > loadWholesalersAndAuctions(Integer businessUnitId) {
    List< InventoryThirdParty > thirdParties = inventoryPlanThirdPartyDAO.findAuctionsAndWholesalers( businessUnitId );
    return thirdParties;
}

public Inventory findInventoryByStockNumber(String stockNum, Integer businessUnitId) {
    return inventoryDAO.findByStockNumber(stockNum, businessUnitId);
}

private List< InventoryPlanEvent > getMarketplacePostings( List< InventoryPlanEvent > planningHistory )
{
	List< InventoryPlanEvent > marketplaceEvents = new ArrayList< InventoryPlanEvent >();
	
	// get list of marketplace events
	for ( InventoryPlanEvent event : planningHistory )
	{
		if ( event.getEventTypeEnum().equals( PlanningEventTypeEnum.INTERNET_MARKETPLACE ) && 
				event.isCurrent() )
		{
			marketplaceEvents.add( event );
		}
	}
	// remove them 
	for ( InventoryPlanEvent event : marketplaceEvents )
	{
		planningHistory.remove( event );
	}
	
	// We only want to display the most recent, so just return a list with 1 element (so we don't ahve to redo the front end)
	if ( !marketplaceEvents.isEmpty() && marketplaceEvents.size() > 1 )
	{
		List< InventoryPlanEvent > justMostRecentPost = new ArrayList< InventoryPlanEvent >();
		justMostRecentPost.add( marketplaceEvents.get( 0 ) );
		return justMostRecentPost;
	}
	
	return marketplaceEvents;
}

/**
 * returns current plan events for the given strategy from a list of events
 * 
 * @param allEvents
 * @return List<InventoryPlanEvent>
 */
protected List< InventoryPlanEvent > extractCurrentPlanEvents( List< InventoryPlanEvent > allEvents, Integer inventoryId,
																boolean checkAppraisalHistory )
{
	List< InventoryPlanEvent > currentEvents = new ArrayList< InventoryPlanEvent >();

	for ( InventoryPlanEvent event : allEvents )
	{
		if ( event.isCurrent() )
		{
			currentEvents.add( event );
		}
	}

	// means has never been planned before, so check to see if this
	// vehicle has an appraisal, if so, prepopulate objective
	// with appraisal decision
	if ( checkAppraisalHistory && allEvents.isEmpty() )
	{
		AppraisalActionType action = getAppraisalService().getAppraisalActionTypeByInventoryId( inventoryId );
		if ( action != null )
		{
			InventoryPlanEvent appraisalBasedEvent = new InventoryPlanEvent();
			if ( action.equals( AppraisalActionTypeEnum.WHOLESALE_OR_AUCTION ) )
			{
				appraisalBasedEvent.setEventTypeId( Integer.valueOf( PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY.getId() ) );
				appraisalBasedEvent.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
				currentEvents.add( appraisalBasedEvent );
			}
		}
	}

	// this check says if you have ever had a plan, and don't have a current plan, 
	// then use the most recents plan objective to pre-populate the planning space
	if ( currentEvents.isEmpty() )
	{
		// don't need to sort events, retrieve in order 
		for ( InventoryPlanEvent event : allEvents )
		{
			// this means only include retail/wholesale/sold/other
			// ignore dealer/admin/reprice becuase they don't make sense for prepopultaing objectives
			if ( event.getEventTypeEnum().getObjectiveId() < 5 )
			{
				InventoryPlanEvent appraisalBasedEvent = new InventoryPlanEvent();
				switch ( event.getEventTypeEnum().getObjectiveId() )
				{
					case 1: // retail
						appraisalBasedEvent.setEventTypeId( Integer.valueOf( PlanningEventTypeEnum.RETAIL_NO_STRATEGY.getId() ) );
						break;
					case 2: // wholesale
						appraisalBasedEvent.setEventTypeId( Integer.valueOf( PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY.getId() ) );
						break;
					case 3: // sold
						appraisalBasedEvent.setEventTypeId( Integer.valueOf( PlanningEventTypeEnum.SOLD_NO_STRATEGY.getId() ) );
						break;
					case 4: // other
						appraisalBasedEvent.setEventTypeId( Integer.valueOf( PlanningEventTypeEnum.OTHER_NO_STRATEGY.getId() ) );
						break;
				}
				appraisalBasedEvent.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
				currentEvents.add( appraisalBasedEvent );
				break;
			}
		}
		
	}
	
	return currentEvents;
}

/**
 * returns a list of the reprice events that are part of the given list
 * 
 * @param events
 * @return List<InventoryPlanEvent>
 */
protected List< InventoryPlanEvent > findRepriceEvents( List< InventoryPlanEvent > events )
{
	List< InventoryPlanEvent > repriceEvents = new ArrayList< InventoryPlanEvent >();

	for ( InventoryPlanEvent event : events )
	{
		if ( event.getEventTypeEnum() == PlanningEventTypeEnum.REPRICE )
		{
			repriceEvents.add( event );
		}
	}

	return repriceEvents;
}

/**
 * finds the date on which the inventory item was last repriced
 * 
 * @param Integer -
 *            the inventory id to use to check for reprice events
 * @return Date - the last reprice date
 */
private InventoryPlanEvent findLastRepriceDate( final List< InventoryPlanEvent > planningHistory )
{
	if(planningHistory != null) {
		Collections.sort(planningHistory, new Comparator<InventoryPlanEvent>(){
			public int compare(InventoryPlanEvent o1, InventoryPlanEvent o2) {
				//reverse the list to get the latest
				return o2.getCreated().compareTo(o1.getCreated());
			}
		});
		
		for(InventoryPlanEvent event : planningHistory) {
			if(event.getEventTypeEnum().equals(PlanningEventTypeEnum.REPRICE)) {
				return event;
			}
		}
	}

	return null;
}

/**
 * Finds the current objective from a list of plan events
 * 
 * @return String - a number that describes the current objective
 */
public PlanObjectiveEnum determineObjective( List< InventoryPlanEvent > events )
{
	boolean hasRepriceEvent = false;

	// normally we would determine the objective from the first event in the list, but we have to
	// exclude service and detailer events because they don't fall into the 4 objectives that the
	// user sees - Retail, Wholesale, Sold, and Other; Also, reprice events aren't used
	// to determine the objective unless they are the only events
	for ( InventoryPlanEvent event : events )
	{
		PlanningEventTypeEnum eventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum( event.getEventTypeId() );
		if ( eventTypeEnum == PlanningEventTypeEnum.REPRICE )
		{
			hasRepriceEvent = true;
		}
		else if ( eventTypeEnum.getPlanObjectiveEnum() != PlanObjectiveEnum.DEALER &&
				  eventTypeEnum.getPlanObjectiveEnum() != PlanObjectiveEnum.MARKETPLACE )
		{
			return eventTypeEnum.getPlanObjectiveEnum();
		}
	}

	if ( hasRepriceEvent )
	{
		return PlanObjectiveEnum.RETAIL;
	}

	return PlanObjectiveEnum.UNKNOWN;
}

/**
 * loads the default advertising description for the given inventory id and event type id
 * 
 * @param inventoryId
 * @return String
 */
private String loadDefaultDescription( Integer inventoryId )
{
	String defaultDescription = "";
	InventoryAdvertising inventoryAdvertisingInfo = getInventoryAdvertisingDAO().load( inventoryId );

	if ( inventoryAdvertisingInfo != null )
	{
		defaultDescription = inventoryAdvertisingInfo.getDescription();
	}

	return defaultDescription;
}

/**
 * Typical call to find the next plan date with no age inflation.
 * Age inflation inflation allows you to 'plan for the future'. 
 * Currently used only in the IMP when you have 3/5/7 days in the future filter set
 * 
 * @param inventoryId
 * @return Date
 */
public Date findNextPlanDate( Integer inventoryId)
{
	return inventoryService.getNextPlanDate(inventoryId, 0);
}

/**
 * Gets the expiration date for the plan associated with the given inventory.
 * 
 * @param inventoryId
 * @return Date
 */
public Date findNextPlanDate( Integer inventoryId, Integer ageInflation)
{
	return inventoryService.getNextPlanDate(inventoryId, ageInflation);
}

public List<InventoryBucketRange> findBucketRange(Integer businessUnitId) {
    
	List<InventoryBucketRange> result = inventoryBucketRangeDAO.loadBuckets(businessUnitId, InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN);
	if (result == null) {
		result = inventoryBucketRangeDAO.loadBuckets(DEFAULT_FIRSTLOOK_BUSINESS_UNIT, InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN);
	}
    return result;
}


public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public IInventoryPlanEventDAO getInventoryPlanEventDAO()
{
	return inventoryPlanEventDAO;
}

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}

public IInventoryPlanThirdPartyDAO getInventoryPlanThirdPartyDAO()
{
	return inventoryPlanThirdPartyDAO;
}

public void setInventoryPlanThirdPartyDAO( IInventoryPlanThirdPartyDAO inventoryPlanThirdPartyDAO )
{
	this.inventoryPlanThirdPartyDAO = inventoryPlanThirdPartyDAO;
}

public IInventoryAdvertisingDAO getInventoryAdvertisingDAO()
{
	return inventoryAdvertisingDAO;
}

public void setInventoryAdvertisingDAO( IInventoryAdvertisingDAO inventoryEventTypeDefaultDAO )
{
	this.inventoryAdvertisingDAO = inventoryEventTypeDefaultDAO;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public IInventoryBucketRangeDAO getInventoryBucketRangeDAO()
{
	return inventoryBucketRangeDAO;
}

public void setInventoryBucketRangeDAO( IInventoryBucketRangeDAO inventoryBucketRangeDAO )
{
	this.inventoryBucketRangeDAO = inventoryBucketRangeDAO;
}

public void setInventoryService(IInventoryService inventoryService) {
	this.inventoryService = inventoryService;
}

}