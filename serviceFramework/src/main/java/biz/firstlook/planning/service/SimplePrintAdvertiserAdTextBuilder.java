package biz.firstlook.planning.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.displaytag.exception.DecoratorException;

import biz.firstlook.commons.collections.Pair;
import biz.firstlook.main.display.util.MileageDecorator;

/**
 * An implemenation of the PrintAdvertiserAdTextBuilder that provides compatibility with the existing functionality. The return type is a Pair<String,
 * String> due to the higher level class returning a String[] of hard coded 2 elements. The AD_DESC enum representing the Ad Description should
 * be brought into this class and not processed outside of here.
 * 
 * @author bfung
 * 
 */
class SimplePrintAdvertiserAdTextBuilder implements PrintAdvertiserAdTextBuilder
{

private static final Logger logger = Logger.getLogger( SimplePrintAdvertiserAdTextBuilder.class );
private static final String SPLIT_ON_AD_DESC = "SPLIT_ON_AD_DESC";

private boolean hadBeenSplit = false;
private List<String> adComponents = new ArrayList<String>();

public void splitForAdDescription()
{
	// this is only for compatibility. It would be a larger refactoring effort to pull the ad description in, due to coupling to the front end.
	if ( !hadBeenSplit )
	{
		adComponents.add(SPLIT_ON_AD_DESC);
		hadBeenSplit = true;
	}
	else
	{
		logger.warn( "Attempted to split the Ad Text again, but this Ad builder only supports splitting once." );
	}
}

public void appendVehicleDescription( String vehicleDescription )
{
	adComponents.add(vehicleDescription);
}

public void appendVin( String vin )
{
	adComponents.add(MessageFormat.format( "VIN: {0}", vin ));
}

public void appendColor( String color )
{
	adComponents.add(color);
}

public void appendMileage( Integer mileage )
{
	if ( mileage != null )
	{
		MileageDecorator mileageDecorator = new MileageDecorator();
		String mileageString = null;
		try
		{
			mileageString = MessageFormat.format( "Mileage: {0}", mileageDecorator.decorate( mileage ) );
		}
		catch ( DecoratorException e )
		{
			logger.warn( "Could not decorate mileage, using raw data." );
			mileageString = MessageFormat.format( "Mileage: {0}", mileage.toString() );
		}
		adComponents.add(mileageString);
	}
}

public void appendStockNumber( String stockNumber )
{
	adComponents.add(MessageFormat.format( "Stock #: {0}", stockNumber ) );
}

public void appendOptions( List< String > options )
{
	StringBuilder optionsString = new StringBuilder();
	Iterator< String > optionNameIter = options.iterator();
	while ( optionNameIter.hasNext() )
	{
		String optionName = optionNameIter.next();
		optionsString.append( optionName );
		if ( optionNameIter.hasNext() )
		{
			optionsString.append( ", " );
		}
	}
	adComponents.add(optionsString.toString());
}

/**
 * The caveat of this class is that the components are stored in a LinkedHashMap to preserve ordering.
 */
public Pair< String, String > getPrintAdvertiserAd()
{
	Pair< String, String > adTextPair = new Pair< String, String >();
	StringBuilder sb = new StringBuilder();
	Iterator< String > adComponentIter = adComponents.iterator();
	while ( adComponentIter.hasNext() )
	{
		String adPart = adComponentIter.next();
		if ( adPart.equals( SPLIT_ON_AD_DESC ) )
		{
			adTextPair.setLeft( sb.toString() );
			sb = new StringBuilder();
		}
		else
		{
			sb.append( adPart );
			if ( adComponentIter.hasNext() )
			{
				sb.append( " " );
			}
		}
	}
	adTextPair.setRight( sb.toString() );
	return adTextPair;
}

public String toString() {
	return adComponents.toString();
}

}
