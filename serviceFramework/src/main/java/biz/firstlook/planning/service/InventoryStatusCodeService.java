package biz.firstlook.planning.service;

import java.util.List;

import biz.firstlook.planning.entity.InventoryStatusCode;
import biz.firstlook.planning.persistence.IInventoryStatusCodeDAO;

public class InventoryStatusCodeService
{

private IInventoryStatusCodeDAO inventoryStatusCodeDAO;

public InventoryStatusCodeService()
{
	super();
}

public List<InventoryStatusCode> retrieveAll()
{
	List<InventoryStatusCode> statusCodes = getInventoryStatusCodeDAO().findAll();
	InventoryStatusCode anyStatus = new InventoryStatusCode();
	anyStatus.setInventoryStatusCD( new Integer( -1 ) );
	anyStatus.setShortDescription( "" );
	anyStatus.setDescription( "ANY" );
	statusCodes.add( 0, anyStatus );
	return statusCodes;
}

public IInventoryStatusCodeDAO getInventoryStatusCodeDAO()
{
	return inventoryStatusCodeDAO;
}

public void setInventoryStatusCodeDAO( IInventoryStatusCodeDAO inventoryStatusCodeDAO )
{
	this.inventoryStatusCodeDAO = inventoryStatusCodeDAO;
}

}
