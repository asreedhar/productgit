package biz.firstlook.planning.service;

import java.util.List;

import biz.firstlook.commons.collections.Pair;

/**
 * An interface for building Print Advertiser ads. This interface and family of classes should only be accessible from within the same package.
 * 
 * @author bfung
 * 
 */
interface PrintAdvertiserAdTextBuilder
{

void splitForAdDescription();

void appendVehicleDescription( String vehicleDescription );

void appendVin( String vin );

void appendColor( String color );

void appendMileage( Integer mileage );

void appendStockNumber( String stockNumber );

void appendOptions( List< String > options );

Pair< String, String > getPrintAdvertiserAd();

}
