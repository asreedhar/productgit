package biz.firstlook.planning.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.entity.TransferPriceValidation;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.module.core.Member;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public interface IInventoryPlanningPersistenceService {

	@Transactional(rollbackFor=Exception.class)
    public Integer savePlan(int businessUnitId, String userId, String jsonText)throws JSONException;

    public void inlineRepriceTracking(int inventoryId, Double newPrice,String userId, Boolean confirmed);
    
    @Transactional(rollbackFor=Exception.class)
    public TransferPriceValidation updateTransferPrice(Integer inventoryId, Float newPrice, Member member);

    public void saveQuickAd(List<InventoryPlanEvent> adEvents,
            List<Integer> invIds, String userId,
            PlanObjectiveEnum newObjective, PlanningEventTypeEnum eventType, Map<Integer, String> adDesc,
            PlanningResultsModeEnum resultsMode);

    public void applyRules(List<InventoryPlanEvent> events, Date reminderDate,Integer inventoryId, String userId);

    public List<Integer> saveQuickPlan(String userId, String[] events, Integer businessUnitId) throws JSONException;
    
    public void saveInternetMarketPlaceEvent(Integer inventoryId,
            String userId, String announcements, Boolean includePhotos,
            Marketplace_InventoryThirdParty marketplace);
}
