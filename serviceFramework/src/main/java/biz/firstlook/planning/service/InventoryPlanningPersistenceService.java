package biz.firstlook.planning.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.commons.enumeration.RepriceSouce;
import biz.firstlook.commons.services.inventory.IInventoryService;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.TransferPriceValidation;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.main.commonDAOs.InventoryTransferPriceStoredProcedure;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.module.core.Member;
import biz.firstlook.planning.entity.InventoryAdvertising;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiser;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;
import biz.firstlook.planning.persistence.IInventoryAdvertisingDAO;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.persistence.IInventoryThirdPartyDAO;
import biz.firstlook.planning.service.InventoryPlanningUtilService.SaveAction;
import biz.firstlook.planning.transactionFramework.ITransactionDAO;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public class InventoryPlanningPersistenceService implements IInventoryPlanningPersistenceService
{

	private static Logger logger = Logger.getLogger( InventoryPlanningPersistenceService.class );

	private IInventoryDAO inventoryDAO;
	private IInventoryThirdPartyDAO inventoryThirdPartyDAO;
	private IInventoryPlanEventDAO inventoryPlanEventDAO;
	private IInventoryAdvertisingDAO inventoryAdvertisingDAO;
	private InternetAdvertiserService internetAdvertiserService;
	private ITransactionDAO transactionDAO;
	private IInventoryService inventoryService;
	private IInventoryPlanningRetrievalService inventoryPlanningRetrievalService;
	private InventoryTransferPriceStoredProcedure inventoryTransferPriceStoredProcedure;

	/**
	 * Saves the information associated with a plan
	 * 
	 * @param businessUnitId
	 * @param userId
	 * @param jsonText
	 * @return Integer - the inventory id of the vehicle whose plan was saved
	 */
	public Integer savePlan( int businessUnitId, String userId, String jsonText ) throws JSONException
	{
		// there's a lot going on here...need to refactor
		List< Object > objectsToSave = new ArrayList< Object >();
		JSONObject jsonObject = (jsonText == null ? new JSONObject() : new JSONObject( jsonText ));
		Integer inventoryId = ( !jsonObject.optString( "id", "" ).equals( "" ) ) ? jsonObject.optInt( "id" ) : null;
		PlanObjectiveEnum objectiveToSave = PlanObjectiveEnum.getPlanObjectiveEnum( jsonObject.optString( "objective", null ) );
		PlanObjectiveEnum oldObjectiveEnum = PlanObjectiveEnum.getPlanObjectiveEnum( jsonObject.optString( "originalObjective", null ) );
		String defaultAdvertisingDescription = jsonObject.optString( "defaultDescription" );
		Date reminderDate = DateUtilFL.parseShortDateString( jsonObject.optString( "nextPlanDate" ) );
	
		// nk - we seem to be calling this with blank inv id somewhere. 
		// this precludes this action from doing anything so we are just going to return
		// The real fix is to stop blanks from being submitted. Maybe with the planning rewrite?
		if (inventoryId == null) 
			return null;
		
		// update inv by ref. Save here
		Inventory inventory = inventoryDAO.findById( inventoryId );
		// need to pull this off and hold because reprice stuff needs to record 'old price'
		Double currentInventoryPrice = inventory.getListPrice();
		updateInventory( jsonObject, inventory, userId );
		objectsToSave.add( inventory );
		
		// save the default advertising description if one is passed
		if ( defaultAdvertisingDescription != null )
		{
			InventoryAdvertising inventoryAdvertisingInfo = buildDefaultDescription( inventoryId, defaultAdvertisingDescription );
			objectsToSave.add( inventoryAdvertisingInfo );
		}
	
		List< InventoryPlanEvent > currentEvents = loadCurrentPlanEvents( inventoryId );
		List< InventoryPlanEvent > eventsToDelete = new ArrayList< InventoryPlanEvent >();
	
		if ( oldObjectiveEnum != objectiveToSave )
		{
			// the user switched objectives, so close out the previous objective's events
			// and remove them from the list of current events
			List< InventoryPlanEvent > closedEvents = closeEventsFromPreviousObjective( currentEvents, oldObjectiveEnum, userId );
			eventsToDelete.addAll( filterEventsToDelete( closedEvents ) );
	
			if ( !closedEvents.isEmpty() )
			{
				objectsToSave.addAll( closedEvents );
			}
		}
	
		List< InventoryPlanEvent > newEvents = InventoryPlanningUtilService.convertJSONToEvents(
				InventoryPlanningUtilService.extractEventsByType(jsonObject, "N"),
				currentEvents, SaveAction.NEW );
		
		List< InventoryPlanEvent > updatedEvents = InventoryPlanningUtilService.convertJSONToEvents(
				InventoryPlanningUtilService.extractEventsByType(jsonObject, "U"),
				currentEvents, SaveAction.UPDATE );
		
		List< InventoryPlanEvent > endedEvents = extractEndedPlanEvents( jsonObject, currentEvents, userId );
	
		if ( objectiveToSave == PlanObjectiveEnum.OTHER )
		{
			extractOtherEvent( jsonObject, currentEvents, newEvents, currentEvents );
		}
	
		if ( objectiveToSave == PlanObjectiveEnum.SOLD )
		{
			extractSoldEvent( jsonObject, currentEvents, newEvents, currentEvents, endedEvents );
		}
	
		if ( objectiveToSave == PlanObjectiveEnum.RETAIL && !currentEvents.isEmpty() ) {
			//spiffs are not being recorded correctly;  the user assumes that spiffs are a point in time event as opposed to a long lived entity.
			Date today = DateUtils.truncate(new Date(), Calendar.DATE);
			
			Set<InventoryPlanEvent> theEvents = new HashSet<InventoryPlanEvent>(currentEvents);
			theEvents.addAll(updatedEvents);
			
			InventoryPlanEvent latestEvent = null;
			for(InventoryPlanEvent event : theEvents) {
				if(PlanningEventTypeEnum.SPIFF.equals(event.getEventTypeEnum())) {
					if(latestEvent == null) {
						latestEvent = event;
					} else {
						if(event.getId() == null || (latestEvent.getId() != null && event.getId().compareTo(latestEvent.getId()) > 0)) {
							latestEvent.setEndDate(latestEvent.getEndDate() == null ? today : latestEvent.getEndDate());
							latestEvent = event;
						} else {
							event.setEndDate(event.getEndDate() == null ? today : event.getEndDate());
						}
					}
				}
			}
			
			// add the unmodifed events to the "updated" list so that
			// their end dates are updated to the last plan date
			updatedEvents.addAll( findUnmodifiedAdvertisingAndLotPromoteEvents( currentEvents, updatedEvents, endedEvents ) );
		}
	
		applyRules( newEvents, reminderDate, inventoryId, userId );
		applyRules( updatedEvents, reminderDate, inventoryId, userId );
	
		if ( !endedEvents.isEmpty() ) // removed ended/deleted events
		{
			eventsToDelete.addAll( filterEventsToDelete( endedEvents ) );
		}
	
		if ( onlyServiceOrDetailModifiedOrNoStrategies( newEvents, updatedEvents )
				&& !hasEventsForObjective( currentEvents, endedEvents, objectiveToSave ) )
		{
			// covers cases where there are no strategies selected;
			InventoryPlanEvent noStrategyEvent = buildNoStrategyEvent( objectiveToSave, inventoryId, userId );
			objectsToSave.add( noStrategyEvent );
		}
		//FB: 25448, when an objective comes in with no strategy, replacing a similar objective with no strategy, the old strategy
		//           must be removed and a new one created such that the vehicle is considered planned.  (this situation arises when
		//           a user simply opens a vehicle plan and clicks save (no modifications), in which the existing plan has no strategy tied to the
		//           objective (wholesale, retail... etc).
		else if (onlyServiceOrDetailModifiedOrNoStrategies( newEvents, updatedEvents ) && !hasStrategyForObjective( currentEvents, endedEvents, objectiveToSave )){
			findAndCloseNoStrategyEvent( currentEvents, objectiveToSave, eventsToDelete, objectsToSave );
			InventoryPlanEvent noStrategyEvent = buildNoStrategyEvent( objectiveToSave, inventoryId, userId );
			objectsToSave.add( noStrategyEvent );
		}		
		else
		{
			// the user selected strategies, but he could have saved a "no strategy" event
			// of the same objective earlier, so we need to close it out
			findAndCloseNoStrategyEvent( currentEvents, objectiveToSave, eventsToDelete, objectsToSave );
	
		}
	
		objectsToSave.addAll( newEvents );
		objectsToSave.addAll( updatedEvents );
		objectsToSave.addAll( endedEvents );
	
		// create a reminder date event, which is used to keep
		// track of the concept of a "plan" at a given point in time
		InventoryPlanEvent reminderDateEvent = buildReminderDateEvent( inventoryId, userId );
		objectsToSave.add( reminderDateEvent );
	
		List< InternetAdvertiser > advertisersToSave = InventoryPlanningUtilService.convertJSONToAdvertisers(
				InventoryPlanningUtilService.extractEventsByType(jsonObject,"internetAdvertising" ));
	
		// Please leave the order to save InernetAdvertisers, transactionDAO, and repriceExport.
		// HACK here to get reprice events saved correctly, due to advertisers not being part of planning object hierarchy.
		// tobeSavedRepriceEvent ID will be populated once transactionDAO finishes.
		// inside the repriceExport, it reades internetAdvertisers that were saved and populates the correct tables.
		// This is done because our inventory -> inventory plan and inventory -> inventoryReprice abstractions do not exist.
		// Feb1 13, 2007 - bf.
		internetAdvertiserService.saveAdvertisers( advertisersToSave, inventory );
		transactionDAO.execute( objectsToSave, eventsToDelete );
		
		// reprice inv
		repriceInventory(userId, jsonObject, inventory, currentInventoryPrice);

		//transferprice is handled via TransferPriceSaveAction.
		
		return inventoryId;
	}

	private void repriceInventory(String userId, JSONObject jsonObject, Inventory inventory, Double oldPrice) {
		Integer repricedValue = jsonObject.has( "reprice" ) ? jsonObject.optInt( "reprice" ) : null;
		Boolean repricedConfirmed = jsonObject.has( "repriceConfirmed" ) ? jsonObject.optBoolean("repriceConfirmed" ) : false;

		if ( repricedValue != null)
		{
			inventoryService.repriceInventory(inventory.getId(), new Date(), new Date(), oldPrice == null ? null : oldPrice.intValue(), repricedValue.intValue(), repricedConfirmed, userId, new Date(), inventory.getBusinessUnitId(), RepriceSouce.IMP);
		}
	}
	
	public TransferPriceValidation updateTransferPrice(Integer inventoryId, Float newTransferPrice, Member member) {
		Inventory inventory = inventoryDAO.findById(inventoryId);
		return updateTransferPriceInternal(inventory, newTransferPrice, member);
	}
	
	private TransferPriceValidation updateTransferPriceInternal(Inventory inventory, Float newTransferPrice, Member member) {
		TransferPriceValidation receipt = null;
		if (newTransferPrice == null) {
			receipt = new TransferPriceValidation(true, "New transfer price is null, nothing to update");				
		} else if (inventory.getTransferPrice() == null && newTransferPrice != null) {
			receipt = inventoryTransferPriceStoredProcedure.save(inventory, newTransferPrice, member);
		} else if ((inventory.getTransferPrice() != null && newTransferPrice != null) 
			&& (Math.round(inventory.getTransferPrice()) != Math.round(newTransferPrice)) ){
				receipt = inventoryTransferPriceStoredProcedure.save(inventory, newTransferPrice, member);
		} else {
			receipt = new TransferPriceValidation(true, "Same value, nothing to update");	
		}
		
		return receipt; 
	}

    @SuppressWarnings("unchecked")
    public List<Integer> saveQuickPlan(String userId, String[] events, Integer businessUnitId) throws JSONException {
        List<InventoryPlanEvent> eventsToSave = new ArrayList<InventoryPlanEvent>();
        List<InventoryPlanEvent> eventsToDelete = new ArrayList<InventoryPlanEvent>();
        List<Integer> invIds = new ArrayList<Integer>();
        List invToSave = new ArrayList();
        Date now = DateUtils.truncate(new Date(), Calendar.DATE);
        PlanningEventTypeEnum eventType = null;
        PlanObjectiveEnum newObjective = null;
        InventoryPlanEvent event = null;

        for (String eventString : events) {
            JSONObject jsonObject = new JSONObject(eventString);
            Integer invId = jsonObject.optInt("inventoryId");
            invIds.add(invId);
            Date reminderDate; 
            Inventory inv = inventoryDAO.findById(invId);
            reminderDate = inventoryPlanningRetrievalService.findNextPlanDate(invId);
            inv.setPlanReminderDate(reminderDate);
            invToSave.add(inv);
            String spiff = jsonObject.optString("spiff");
            if (spiff != null && !spiff.equalsIgnoreCase("")) {
                newObjective = PlanObjectiveEnum.RETAIL;
                eventType = PlanningEventTypeEnum.SPIFF;
                String spiffNotes = jsonObject.optString("spiffNotes");
                InventoryPlanEvent oldEvent = inventoryPlanEventDAO.findSpiffEventByInvIdAndEventType(invId, eventType);
                if (oldEvent == null) {
                    event = new InventoryPlanEvent(invId, spiffNotes, eventType.getId());
                    event.setEndDate(null);//Spiffs do not have an end date.
                } else {
                	event = InventoryPlanningUtilService.updateSpiff(oldEvent, spiffNotes);
                }
            } else { //Wholesale or auctions
                newObjective = PlanObjectiveEnum.WHOLESALE;
                InventoryThirdParty itp = null;
                Date endDate = null; 
                String name = jsonObject.optString("name");
                Integer id = jsonObject.optInt("id");
                if (id != 0) { //Unspecified are stored in the notes3 field.
                    itp = inventoryThirdPartyDAO.findById(id);
                    if (itp == null) {
                        itp = new InventoryThirdParty(id);
                        itp.setName(name);
                        itp.setBusinessUnitId(businessUnitId);
                    }
                    name = "";
                }
                DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT);
                try {
                    endDate = formatter.parse(jsonObject.optString("date"));
                } catch (ParseException e) {}//just use current date
                String auction = jsonObject.optString("auction");
                if (auction != null && !auction.equalsIgnoreCase("")) {
                    if (itp != null) {
                        itp.setInventoryThirdPartyTypeId(InventoryThirdPartyEnum.AUCTION.getId());
                    }
                    String location = jsonObject.optString("location");
                    eventType = PlanningEventTypeEnum.AUCTION;
                    event = new InventoryPlanEvent(invId, location, name, itp, eventType.getId());
                } else {
                    if (itp != null) {
                        itp.setInventoryThirdPartyTypeId(InventoryThirdPartyEnum.WHOLESALER.getId());
                    }
                    eventType = PlanningEventTypeEnum.WHOLESALER;
                    event = new InventoryPlanEvent(invId, "", name, itp, eventType.getId());
                }
                event.setEndDate(endDate);
            }
            applyRules(event, now, invId, userId);
            eventsToSave.add(event);
        }
        List expiredEvents = new ArrayList();
        expiredEvents = updateStrategyAndEvents(eventsToSave, userId,newObjective, eventType, eventsToDelete);
        eventsToSave.addAll(expiredEvents);
        List reminderEvents = new ArrayList();
        for (InventoryPlanEvent planEvent:eventsToSave) {
        	InventoryPlanEvent reminderDateEvent = buildReminderDateEvent( planEvent.getInventoryId(), userId );
        	reminderEvents.add( reminderDateEvent );
        }
        eventsToSave.addAll(reminderEvents);
        eventsToSave.addAll(invToSave);
        
        transactionDAO.execute(eventsToSave, eventsToDelete);
        
        return invIds;
    }

    @SuppressWarnings("unchecked")
	private List updateStrategyAndEvents(List<InventoryPlanEvent> planEvents,String userId, PlanObjectiveEnum newObjective,
            PlanningEventTypeEnum eventType, List<InventoryPlanEvent> eventsToDelete) {
    	List<Object> expiredEvents = new ArrayList<Object>();
        for (InventoryPlanEvent event : planEvents) {
            List<InventoryPlanEvent> currentEvents = loadCurrentPlanEvents(event.getInventoryId());
            PlanObjectiveEnum oldObjective = inventoryPlanningRetrievalService.determineObjective(currentEvents);
            findAndCloseNoStrategyEvent( currentEvents, oldObjective, eventsToDelete, expiredEvents );
            if (oldObjective != newObjective) { // Switching objectives so expire all old events.
                List expiredStrategiesForEvent = closeEventsFromPreviousObjective(currentEvents,oldObjective, userId);
                eventsToDelete.addAll(filterEventsToDelete(expiredStrategiesForEvent));
                expiredEvents.addAll(expiredStrategiesForEvent);
            } else { // Expire the events of that type (new auctions so expire old ones.)
            	for (InventoryPlanEvent closeEvent : currentEvents) {
            		InventoryPlanEvent eventToClose = null;
                	//Advertisers expire the ads of the same outlet.
                	if (closeEvent.getPlanningEventType().getId() == PlanningEventTypeEnum.ADVERTISEMENT.getId()) {
                		if (closeEvent.getInventoryThirdParty() != null && event.getInventoryThirdParty() != null && 
                					closeEvent.getInventoryThirdParty().getId() == event.getInventoryThirdParty().getId()) {
                			eventToClose = closeEvent;
                		} else if (event.getInventoryThirdParty() == null) { //This is the case where the user deselects the Advertiser
                			eventToClose = closeEvent;
                		}
                	} 
                	//Auction/wholesaler expire all other auctions and wholesalers
                	else if (closeEvent.getPlanningEventType().getId() == PlanningEventTypeEnum.AUCTION.getId() 
                			|| closeEvent.getPlanningEventType().getId() == PlanningEventTypeEnum.WHOLESALER.getId()) {
                		if (closeEvent.getPlanningEventType().getId() == eventType.getId()) {
                			eventToClose = closeEvent;
                		}
                	}
                	if (eventToClose != null) {
                		endEvent( eventToClose );
                    	eventToClose.setLastModified( new Date() );
                    	eventToClose.setLastModifiedBy( userId );
                    	if (eventToClose.getBeginDate() == eventToClose.getEndDate()) {
                    		eventsToDelete.add(eventToClose);
                    	} else {
                    		expiredEvents.add(eventToClose);
                    	}
                	}
                	
                }
            }
        }
        return expiredEvents;
    }
    
    @SuppressWarnings("unchecked")
    public void saveQuickAd(List<InventoryPlanEvent> adEvents, List<Integer> invIds, String userId,
            PlanObjectiveEnum newObjective, PlanningEventTypeEnum eventType, Map<Integer, String> adDesc, PlanningResultsModeEnum resultsMode) {
        List eventsToSave = new ArrayList();
        List<InventoryPlanEvent> eventsToDelete = new ArrayList<InventoryPlanEvent>();
        eventsToSave.addAll(adEvents);
        List expiredEvents = updateStrategyAndEvents(adEvents, userId, newObjective, eventType, eventsToDelete);
        eventsToSave.addAll(expiredEvents);
        Date reminderDate; 
        Inventory inv;
        for (Integer invId:invIds) { //We have our list of ad events to save also must update inv reminder date.
            inv = inventoryDAO.findById(invId);
            reminderDate = inventoryPlanningRetrievalService.findNextPlanDate(invId);
            inv.setPlanReminderDate(reminderDate);
            eventsToSave.add(inv);
            InventoryPlanEvent reminderDateEvent = buildReminderDateEvent( invId, userId );
            eventsToSave.add( reminderDateEvent );
            Object desc = adDesc.get(invId);
            if ( desc != null )
        	{
        		InventoryAdvertising inventoryAdvertisingInfo = buildDefaultDescription( invId, desc.toString() );
        		eventsToSave.add( inventoryAdvertisingInfo );
        	}
        }
        transactionDAO.execute(adEvents, eventsToDelete);
    }


private void updateInventory( JSONObject jsonObject, Inventory inventory, String userId )
{
	Date reminderDate = DateUtilFL.parseShortDateString( jsonObject.optString( "nextPlanDate" ) );
	Boolean isCertified = jsonObject.optBoolean( "certified" );
	Boolean isSpecialFinance = jsonObject.optBoolean( "specialFinance" );
	
	Double repricedValue = jsonObject.has( "reprice" ) ? jsonObject.optDouble("reprice" ) : null;
	
	if ( repricedValue != null && repricedValue != inventory.getListPrice() )
	{
		inventory.setListPrice( repricedValue.doubleValue() );
		inventory.setListPriceLock( Boolean.TRUE );
	}
	inventory.setPlanReminderDate( reminderDate );
	inventory.setCertified( isCertified );
	inventory.setSpecialFinance( isSpecialFinance );
	inventory.setTransferForRetailOnly(jsonObject.optBoolean("transferForRetailOnly"));
}

/**
 * Returns a list of events in the currentEvents list that do not exist in the list of updated or deleted ids
 * 
 * @param currentEvents
 * @param updatedEvents
 * @param deletedEvents
 * @return
 */
protected List< InventoryPlanEvent > findUnmodifiedAdvertisingAndLotPromoteEvents( List< InventoryPlanEvent > currentEvents,
																					List< InventoryPlanEvent > updatedEvents,
																					List< InventoryPlanEvent > deletedEvents )
{
	List< InventoryPlanEvent > unmodifiedEvents = new ArrayList< InventoryPlanEvent >();
	List< Integer > updatedIds = new ArrayList< Integer >();
	List< Integer > deletedIds = new ArrayList< Integer >();

	for ( InventoryPlanEvent event : updatedEvents )
	{
		updatedIds.add( event.getId() );
	}

	for ( InventoryPlanEvent event : deletedEvents )
	{
		deletedIds.add( event.getId() );
	}

	for ( InventoryPlanEvent event : currentEvents )
	{
		if ( !deletedIds.contains( event.getId() ) && !updatedIds.contains( event.getId() ) )
		{
			unmodifiedEvents.add( event );
		}
	}

	return unmodifiedEvents;
}

/**
 * Checks a list of events to see if contains any events that are part of the given objective
 * 
 * @return true - if there are any events that are part of the given objective, false otherwise
 */
protected boolean hasEventsForObjective( List< InventoryPlanEvent > events, List< InventoryPlanEvent > endedEvents,
											PlanObjectiveEnum objectiveEnum )
{
	List< Integer > endedEventIds = new ArrayList< Integer >();
	for ( InventoryPlanEvent event : endedEvents )
	{
		endedEventIds.add( event.getId() );
	}

	for ( InventoryPlanEvent event : events )
	{
		if ( event.getEventTypeEnum() != PlanningEventTypeEnum.REPRICE
				&& event.getEventTypeEnum().getPlanObjectiveEnum() == objectiveEnum 
				&& !endedEventIds.contains( event.getId() )
				&& event.getId() != null )
		{
			return true;
		}
	}

	return false;
}

protected boolean hasStrategyForObjective(List<InventoryPlanEvent> events,
		List<InventoryPlanEvent> endedEvents,
		PlanObjectiveEnum objectiveEnum) {
	
	List<Integer> endedEventIds = new ArrayList<Integer>();
	
	for (InventoryPlanEvent event : endedEvents) {
		endedEventIds.add(event.getId());
	}

	for (InventoryPlanEvent event : events) {
		if (event.getEventTypeEnum() != PlanningEventTypeEnum.REPRICE
				&& event.getEventTypeEnum().getPlanObjectiveEnum() == objectiveEnum
				&& (event.getEventTypeEnum() != PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY 
					&& event.getEventTypeEnum() != PlanningEventTypeEnum.RETAIL_NO_STRATEGY
					&& event.getEventTypeEnum() != PlanningEventTypeEnum.OTHER_NO_STRATEGY
					&& event.getEventTypeEnum() != PlanningEventTypeEnum.SOLD_NO_STRATEGY)
				&& !endedEventIds.contains(event.getId())
				&& event.getId() != null) {
			return true;
		}
	}

	return false;
}

    public void applyRules(List<InventoryPlanEvent> events, Date reminderDate,Integer inventoryId, String userId) {
        if (!events.isEmpty()) {
            for (InventoryPlanEvent event : events) {
                applyRules(event, reminderDate, inventoryId, userId);
            }
        }
    }

    public void applyRules(InventoryPlanEvent event, Date reminderDate,Integer inventoryId, String userId) {
        applyDateRules(event, reminderDate);
        applyAuditingRules(event, userId);
        if (event.getInventoryId() == null) {
            event.setInventoryId(inventoryId);
        }
    }

protected void applyDateRules( InventoryPlanEvent event, Date reminderDate )
{
	if ( event.getBeginDate() == null )
	{
		event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
	}

	if ( event.getEventTypeEnum() == PlanningEventTypeEnum.PROMOTION )
	{
		event.setEndDate( reminderDate );
	}
}

private boolean onlyServiceOrDetailModifiedOrNoStrategies( List< InventoryPlanEvent > newEvents, List< InventoryPlanEvent > updatedEvents )
{
	List< InventoryPlanEvent > allEvents = new ArrayList< InventoryPlanEvent >();
	allEvents.addAll( newEvents );
	allEvents.addAll( updatedEvents );

	for ( InventoryPlanEvent event : allEvents )
	{
		if ( event.getEventTypeEnum().getPlanObjectiveEnum() != PlanObjectiveEnum.DEALER 
				&& event.getEventTypeEnum() != PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY 
				&& event.getEventTypeEnum() != PlanningEventTypeEnum.RETAIL_NO_STRATEGY
				&& event.getEventTypeEnum() != PlanningEventTypeEnum.OTHER_NO_STRATEGY 
				&& event.getEventTypeEnum() != PlanningEventTypeEnum.SOLD_NO_STRATEGY
				)
		{
			return false;
		}
	}

	return true;
}

private void applyAuditingRules( InventoryPlanEvent event, String userId )
{
	if ( event.getId() == null ) // new event
	{
		event.setCreated( new Date() );
		event.setCreatedBy( userId );
	}

	event.setLastModified( new Date() );
	event.setLastModifiedBy( userId );
}

/**
 * Loads in inventory item's current plan events
 * 
 * @param inventoryId
 * @return List<InventoryPlanEvent>
 */
private List< InventoryPlanEvent > loadCurrentPlanEvents( Integer inventoryId )
{
	List< InventoryPlanEvent > planningHistory = inventoryPlanEventDAO.findPlanHistory( inventoryId );

	List< InventoryPlanEvent > currentEvents = new ArrayList< InventoryPlanEvent >();

	for ( InventoryPlanEvent event : planningHistory )
	{
		if ( event.isCurrent() )
		{
			currentEvents.add( event );
		}
	}
	return currentEvents;
}

private List< InventoryPlanEvent > filterEventsToDelete( List< InventoryPlanEvent > events )
{
	// look for events whose begin date and end date is today; we're assuming that
	// these events were not wanted
	List< InventoryPlanEvent > eventsToDelete = new ArrayList< InventoryPlanEvent >();
	Timestamp today = new Timestamp( DateUtils.truncate( new Date(), Calendar.DATE ).getTime() );

	for ( InventoryPlanEvent event : events )
	{
		if ( event.getBeginDate().compareTo( today ) == 0 && event.getEndDate().compareTo( today ) == 0 )
		{
			eventsToDelete.add( event );
		}
	}
	
	return eventsToDelete;
}

private void extractOtherEvent( JSONObject jsonPlan, List< InventoryPlanEvent > currentEvents, List< InventoryPlanEvent > newEvents,
								List< InventoryPlanEvent > updatedEvents )
{
	InventoryPlanEvent otherEvent = null;
	JSONObject otherJSONEvent = jsonPlan.optJSONObject( "other" );
	InventoryPlanEvent existingOtherEvent = getSingleEventOfType( currentEvents, PlanningEventTypeEnum.OTHER );

	if ( currentEvents.isEmpty() || existingOtherEvent == null )
	{
		otherEvent = new InventoryPlanEvent();
		otherEvent.setEventTypeId( PlanningEventTypeEnum.OTHER.getId() );
	}
	else
	{
		otherEvent = existingOtherEvent;
	}

	if ( otherJSONEvent.has( "strategyNotes" ) )
	{
		otherEvent.setNotes( otherJSONEvent.optString( "strategyNotes" ) );
	}

	if ( otherEvent.getId() != null )
	{
		updatedEvents.add( otherEvent );
	}
	else
	{
		newEvents.add( otherEvent );
	}
}

private void extractSoldEvent( JSONObject jsonPlan, List< InventoryPlanEvent > currentEvents, List< InventoryPlanEvent > newEvents,
								List< InventoryPlanEvent > updatedEvents, List< InventoryPlanEvent > endedEvents )
{
	InventoryPlanEvent soldEvent = null;
	JSONObject soldJson = jsonPlan.optJSONObject( "sold" );
	String soldAs = soldJson.optString( "soldAs" );
	PlanningEventTypeEnum soldAsEventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum( soldAs );
	InventoryPlanEvent existingSoldRetailEvent = getSingleEventOfType( currentEvents, PlanningEventTypeEnum.RETAIL );
	InventoryPlanEvent existingSoldWholesaleEvent = getSingleEventOfType( currentEvents, PlanningEventTypeEnum.WHOLESALE );

	if ( soldAsEventTypeEnum == PlanningEventTypeEnum.RETAIL && existingSoldRetailEvent != null )
	{
		soldEvent = existingSoldRetailEvent;
	}
	else if ( soldAsEventTypeEnum == PlanningEventTypeEnum.WHOLESALE && existingSoldWholesaleEvent != null )
	{
		soldEvent = existingSoldWholesaleEvent;
	}
	else
	{
		soldEvent = new InventoryPlanEvent();
		soldEvent.setEventTypeId( soldAsEventTypeEnum.getId() );

		if ( existingSoldRetailEvent != null && soldAsEventTypeEnum == PlanningEventTypeEnum.WHOLESALE )
		{
			endEvent( existingSoldRetailEvent );
			endedEvents.add( existingSoldRetailEvent );
		}
		else if ( existingSoldWholesaleEvent != null && soldAsEventTypeEnum == PlanningEventTypeEnum.RETAIL )
		{
			endEvent( existingSoldWholesaleEvent );
			endedEvents.add( existingSoldWholesaleEvent );
		}
	}

	if ( soldJson.has( "strategyNotes" ) )
	{
		soldEvent.setNotes( soldJson.optString( "strategyNotes" ) );
	}

	if ( soldJson.has( "name" ) )
	{
		soldEvent.setNotes2( soldJson.optString( "name" ) );
	}

	if ( soldJson.has( "beginDate" ) )
	{
		soldEvent.setBeginDate( InventoryPlanningUtilService.convertStringToDate( soldJson.optString( "beginDate" ) ) );
	}

	if ( soldEvent.getId() != null )
	{
		updatedEvents.add( soldEvent );
	}
	else
	{
		newEvents.add( soldEvent );
	}
}

private InventoryPlanEvent getSingleEventOfType( List< InventoryPlanEvent > currentEvents, PlanningEventTypeEnum eventTypeEnum )
{
	for ( InventoryPlanEvent event : currentEvents )
	{
		if ( event.getEventTypeEnum() == eventTypeEnum )
		{
			return event;
		}
	}

	return null;
}

private void findAndCloseNoStrategyEvent( List< InventoryPlanEvent > currentEvents, PlanObjectiveEnum objective,
											List< InventoryPlanEvent > eventsToDelete, List< Object > objectsToSave )
{
	PlanningEventTypeEnum eventTypeEnum = null;

	if ( objective == PlanObjectiveEnum.RETAIL )
	{
		eventTypeEnum = PlanningEventTypeEnum.RETAIL_NO_STRATEGY;
	}
	else if ( objective == PlanObjectiveEnum.WHOLESALE )
	{
		eventTypeEnum = PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY;
	}
	else if ( objective == PlanObjectiveEnum.SOLD )
	{
		eventTypeEnum = PlanningEventTypeEnum.SOLD_NO_STRATEGY;
	}
	else if ( objective == PlanObjectiveEnum.OTHER )
	{
		eventTypeEnum = PlanningEventTypeEnum.OTHER_NO_STRATEGY;
	}

	InventoryPlanEvent noStrategyEvent = null;

	for ( InventoryPlanEvent event : currentEvents )
	{
		if ( event.getEventTypeEnum() == eventTypeEnum )
		{
			endEvent( event );
			noStrategyEvent = event;
		}
	}

	if ( noStrategyEvent != null )
	{
		Timestamp today = new Timestamp( DateUtils.truncate( new Date(), Calendar.DATE ).getTime() );
		// if the 'no strategy' event was created and ended today, just delete it
		// (like how we handle items that are deleted from the planning space)
		if ( noStrategyEvent.getBeginDate().compareTo( today ) == 0
				&& noStrategyEvent.getEndDate().compareTo( today ) == 0
				&& DateUtils.truncate( noStrategyEvent.getCreated(), Calendar.DATE ).compareTo( today ) == 0 )
		{
			eventsToDelete.add( noStrategyEvent );
		}
		else
		{
			objectsToSave.add( noStrategyEvent );
		}
	}
}

/**
 * Builds a "no-strategy" event for the given objective, inventory id, and userId
 * 
 * @param objective
 * @param inventoryId
 * @param userId
 * @return InventoryPlanEvent
 */
protected InventoryPlanEvent buildNoStrategyEvent( PlanObjectiveEnum objective, Integer inventoryId, String userId )
{
	PlanningEventTypeEnum eventTypeEnum = null;

	if ( objective == PlanObjectiveEnum.WHOLESALE )
	{
		eventTypeEnum = PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY;
	}
	else if ( objective == PlanObjectiveEnum.SOLD )
	{
		eventTypeEnum = PlanningEventTypeEnum.SOLD_NO_STRATEGY;
	}
	else if ( objective == PlanObjectiveEnum.OTHER )
	{
		eventTypeEnum = PlanningEventTypeEnum.OTHER_NO_STRATEGY;
	}
	else
	// PlanObjectiveEnum.RETAIL by default
	{
		eventTypeEnum = PlanningEventTypeEnum.RETAIL_NO_STRATEGY;
	}

	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setInventoryId( inventoryId );
	event.setEventTypeId( eventTypeEnum.getId() );
	event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
	event.setCreated( new Date() );
	event.setCreatedBy( userId );
	event.setLastModified( new Date() );
	event.setLastModifiedBy( userId );

	return event;
}

/**
 * Builds an InventoryPlanEvent for a reminder date event; Builds 1 reminder event per day
 * 
 * @param inventoryId
 * @param referenceDate
 * @param userId
 * @return InventoryPlanEvent
 */
protected InventoryPlanEvent buildReminderDateEvent( Integer inventoryId, String userId )
{
	Date today = DateUtils.truncate( new Date(), Calendar.DATE );
	InventoryPlanEvent event = inventoryPlanEventDAO.findByInventoryIdAndCreatedDate( inventoryId, today,
																						PlanningEventTypeEnum.REMINDER_DATE.getId() );

	if ( event == null )
	{
		event = createMarkerEvent( inventoryId, PlanningEventTypeEnum.REMINDER_DATE, userId );
	}
	else
	{
		event.setLastModified( new Date() );
		event.setLastModifiedBy( userId );
	}

	return event;
}

/**
 * Closes out events from a previous objective (if any) and remove them from the list of current events
 * 
 * @param inventoryId
 * @param newObjective
 * @param userId
 */
protected List< InventoryPlanEvent > closeEventsFromPreviousObjective( List< InventoryPlanEvent > currentEvents,
																		PlanObjectiveEnum oldObjective, String userId )
{
	List< InventoryPlanEvent > closedEvents = new ArrayList< InventoryPlanEvent >();

	if ( currentEvents != null && !currentEvents.isEmpty() )
	{
		PlanningEventTypeEnum currentEventTypeEnum = null;
		for ( InventoryPlanEvent event : currentEvents )
		{
			currentEventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum( event.getEventTypeId() );
			if ( currentEventTypeEnum != PlanningEventTypeEnum.REPRICE
					&& currentEventTypeEnum != PlanningEventTypeEnum.DETAILER
					&& currentEventTypeEnum != PlanningEventTypeEnum.SERVICE_DEPARTMENT
					&& currentEventTypeEnum.getPlanObjectiveEnum() == oldObjective )
			{
				// close the event
				endEvent( event );
				event.setLastModified( new Date() );
				event.setLastModifiedBy( userId );
				closedEvents.add( event );
			}
		}
		currentEvents.removeAll( closedEvents );
	}

	return closedEvents;
}

/**
 * Builds a list of InventoryPlanEvent instances that should be ended, and ends them.
 */
protected List< InventoryPlanEvent > extractEndedPlanEvents( JSONObject planObject, List< InventoryPlanEvent > currentEvents, String userId )
{
	JSONArray endedEventIds = planObject.optJSONArray( "D" );
	List< InventoryPlanEvent > endedEvents = new ArrayList< InventoryPlanEvent >();

	InventoryPlanEvent planEvent = null;

	for ( int i = 0; i < endedEventIds.length(); i++ )
	{
		// planEvent = getInventoryPlanEventDAO().findById( endedEventIds.optInt( i ) );
		planEvent = InventoryPlanningUtilService.findEvent( currentEvents, endedEventIds.optInt( i ) );
		if ( planEvent == null ) {
			continue;
		}
		endEvent( planEvent );
		planEvent.setLastModified( new Date() );
		planEvent.setLastModifiedBy( userId );

		endedEvents.add( planEvent );
	}

	return endedEvents;
}

/**
 * Sets an InventoryPlanEvent's end date to yesterday. The date range is inclusive, so the end date is set to yesterday because the event was
 * active as of yesterday. However, it is no longer active today. (move to InventoryPlanEvent???)
 * 
 * @param event
 */
protected void endEvent( InventoryPlanEvent event )
{
	Timestamp today = new Timestamp( DateUtils.truncate( new Date(), Calendar.DATE ).getTime() );

	// DE978: we are getting a null pointer on this if statment, can't figure outhow logically it's happening
	if (event == null) {
		logger.error("DE978: event was null - it shouldn't be null!!");
		return;
	} else if ( event.getEventTypeEnum() == null ) {
		logger.error( "DE978: eventTypeEnum was null for eventId: " + event.getId() );
		return;
	} else if ( event.getEventTypeEnum().getPlanObjectiveEnum() == PlanObjectiveEnum.SOLD && event.getBeginDate() == null ) {
		logger.error( "DE978: sold event had a null begin date! this should not be possible, eventId: " + event.getId() );
		return;
	}
	
	// the begin date of a sold event can be set in the future, which is a problem
	// if we try to end the event before that date
	if ( event.getEventTypeEnum().getPlanObjectiveEnum() == PlanObjectiveEnum.SOLD && event.getBeginDate().after( today ) )
	{
		// set the begin date to the created date so that some history will exist for the car
		Date createdDate = DateUtils.truncate( event.getCreated(), Calendar.DATE );
		event.setBeginDate( createdDate );
	}
	if ( event.getBeginDate().compareTo( today ) == 0 )
	{
		event.setEndDate( today );
	}
	else
	{
		// set to yesterday
		event.setEndDate( DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), -1 ), Calendar.DATE ) );
	}
}

/**
 * saves the given description as the default for the given inventoryId and eventTypeId
 * 
 * @param inventoryId
 * @param eventTypeId
 * @param description
 */
private InventoryAdvertising buildDefaultDescription( Integer inventoryId, String description )
{
	InventoryAdvertising eventTypeDefault = inventoryAdvertisingDAO.load( inventoryId );

	if ( eventTypeDefault == null )
	{
		eventTypeDefault = new InventoryAdvertising();
		eventTypeDefault.setInventoryId( inventoryId );
	}

	eventTypeDefault.setDescription( description );
	return eventTypeDefault;
}

@SuppressWarnings("unchecked")
public void inlineRepriceTracking( int inventoryId, Double newPrice, String userId, Boolean confirmed )
{
	Inventory inventory = inventoryDAO.findById( inventoryId );
	// update and save inv
	Double currentListPrice = inventory.getListPrice();
	
	inventory.setListPrice( newPrice );
	inventory.setListPriceLock( Boolean.TRUE );
	Collection objectsToSave = new ArrayList();
	objectsToSave.add( inventory );
	transactionDAO.execute( objectsToSave );
	// create reprice event
	inventoryService.repriceInventory(inventory.getId(), new Date(), new Date(), (currentListPrice == null) ? null : currentListPrice.intValue(), newPrice.intValue(), confirmed, userId, new Date(), inventory.getBusinessUnitId(), RepriceSouce.IMP);	
}

public void saveInternetMarketPlaceEvent( Integer inventoryId, String userId, String announcements, Boolean includePhotos,
											Marketplace_InventoryThirdParty marketplace )
{
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setInventoryId( inventoryId );
	event.setEventTypeId( PlanningEventTypeEnum.INTERNET_MARKETPLACE.getId() );
	event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
	event.setEndDate( DateUtils.truncate( DateUtilFL.addDaysToDate( new Date(), 7 ), Calendar.DATE ) );
	event.setInventoryThirdParty( marketplace );
	event.setNotes( announcements );
	event.setNotes2( includePhotos.toString() );
	event.setCreated( new Date() );
	event.setCreatedBy( userId );
	event.setLastModified( new Date() );
	event.setLastModifiedBy( userId );

	Collection<InventoryPlanEvent> objectsToSave = new ArrayList<InventoryPlanEvent>();
	objectsToSave.add( event );
	transactionDAO.execute( objectsToSave );
}

/**
 * Creates an event that begins and ends today; useful for "no plan", "reminder date", and "reprice" events
 * 
 * @return
 */
protected InventoryPlanEvent createMarkerEvent( Integer inventoryId, PlanningEventTypeEnum eventTypeEnum, String userId )
{
	InventoryPlanEvent event = new InventoryPlanEvent();
	event.setInventoryId( inventoryId );
	event.setEventTypeId( eventTypeEnum.getId() );
	event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
	event.setEndDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
	event.setCreated( new Date() );
	event.setCreatedBy( userId );
	event.setLastModified( new Date() );
	event.setLastModifiedBy( userId );

	return event;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public void setTransactionDAO( ITransactionDAO transactionDAO )
{
	this.transactionDAO = transactionDAO;
}

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}

public void setInventoryAdvertisingDAO( IInventoryAdvertisingDAO inventoryEventTypeDefaultDAO )
{
	this.inventoryAdvertisingDAO = inventoryEventTypeDefaultDAO;
}

public void setInternetAdvertiserService( InternetAdvertiserService internetAdvertiserService )
{
	this.internetAdvertiserService = internetAdvertiserService;
}

public void setInventoryPlanningRetrievalService(
        IInventoryPlanningRetrievalService inventoryPlanningRetrievalService) {
    this.inventoryPlanningRetrievalService = inventoryPlanningRetrievalService;
}

public IInventoryPlanningRetrievalService getInventoryPlanningRetrievalService() {
    return inventoryPlanningRetrievalService;
}

public void setInventoryThirdPartyDAO(
        IInventoryThirdPartyDAO inventoryThirdPartyDAO) {
    this.inventoryThirdPartyDAO = inventoryThirdPartyDAO;
}

public void setInventoryService(IInventoryService inventoryService) {
	this.inventoryService = inventoryService;
}

public void setInventoryTransferPriceStoredProcedure(
		InventoryTransferPriceStoredProcedure inventoryTransferPriceStoredProcedure) {
	this.inventoryTransferPriceStoredProcedure = inventoryTransferPriceStoredProcedure;
}

}
