package biz.firstlook.planning.service;


import java.util.List;

import org.apache.axis.message.MessageElement;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.entity.Inventory;
import biz.firstlook.thirdparty.marketplace.IMarketplaceSubmissionDAO;
import biz.firstlook.thirdparty.marketplace.MarketplaceSubmissionBaseInformation;
import biz.firstlook.thirdparty.marketplace.ThirdPartyMarketPlaceException;
import biz.firstlook.thirdparty.marketplace.ThirdPartyMarketplaceService;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.GMSmartAuctionAdapter;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.GMSmartAuctionStatus;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.GMSmartAuctionSubmissionInformation;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionResponse;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.webservice.GMSmartAuctionResponseQueryString;
import biz.firstlook.thirdparty.marketplace.entity.MarketplaceSubmission;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public class GMSmartAuctionService extends ThirdPartyMarketplaceService
{

private GMSmartAuctionAdapter gmSmartAuctionAdapter;

public GMSmartAuctionService()
{
	super();
}

public GMSmartAuctionService( IMarketplaceSubmissionDAO dao )
{
	super( dao );
}

public GMSmartAuctionStatus populatedGMSmartAuctionInfo( Integer inventoryId )
{
	GMSmartAuctionStatus auctionStatus = new GMSmartAuctionStatus();
	auctionStatus.setGmDealer( true );
	MarketplaceSubmission marketplaceSubmission = marketplaceSubmissionDAO.findActiveSubmissionByInventoryId( inventoryId );

	// means vehicle has not been submitted yet
	if ( marketplaceSubmission == null )
	{
		auctionStatus.setConfirmationURL( "" );
		auctionStatus.setPosted( false );
	}
	else
	{
		auctionStatus.setConfirmationURL( marketplaceSubmission.getUrl() );
		auctionStatus.setPosted( true );
	}

	return auctionStatus;
}

@Transactional(rollbackFor=Exception.class)
public String submit( MarketplaceSubmissionBaseInformation baseInfo ) throws ThirdPartyMarketPlaceException, ApplicationException
{
	GMSmartAuctionSubmissionInformation gmInfo = (GMSmartAuctionSubmissionInformation)baseInfo;
	
	GMSmartAuctionResponse response = getGmSmartAuctionAdapter().submit( gmInfo.generateInspectionReports(), gmInfo.getVcDesc(),
																			gmInfo.getStockNo(), gmInfo.getCredentials() );
	GMSmartAuctionResponseQueryString queryResponse = response.getQueryString();

	if ( response.isValid() )
	{
		MessageElement messageElement = queryResponse.get_any()[0];
		persistSubmission( gmInfo, messageElement.toString(), baseInfo.getMarketplace() );

		// successful request!
		if ( "OK".equals( response.getMessage() ) )
		{
			return messageElement.toString();
		}
	}
	
	return "Error";
}

public GMSmartAuctionSubmissionInformation populateSubmissionInformation( Integer inventoryId, List<String> photoUrls, Integer memberId, Boolean includePhotos,
																			String announcments, Marketplace_InventoryThirdParty marketplace )
{
	Inventory inventory = inventoryDAO.findById( inventoryId );

	GMSmartAuctionSubmissionInformation submissionInfo = new GMSmartAuctionSubmissionInformation();
	submissionInfo.setStockNo( inventory.getStockNumber() );
	submissionInfo.setAnnouncments( announcments );
	submissionInfo.setIncludePhotos( (includePhotos == null) ? false : includePhotos );
	submissionInfo.setPhotoUrls(inventory.getId(), photoUrls);
	
	submissionInfo.setVcDesc( inventory.getVehicle().getMakeModelGrouping().getMake() + " " + inventory.getVehicle().getMakeModelGrouping().getModel() + 
	                          " " + inventory.getVehicle().getTrim() );
	
	populateSubmissionInformation( submissionInfo, inventory, memberId, marketplace );
	return submissionInfo;
}

public GMSmartAuctionAdapter getGmSmartAuctionAdapter()
{
	return gmSmartAuctionAdapter;
}

public void setGmSmartAuctionAdapter( GMSmartAuctionAdapter gmSmartAuctionAdapter )
{
	this.gmSmartAuctionAdapter = gmSmartAuctionAdapter;
}

}
