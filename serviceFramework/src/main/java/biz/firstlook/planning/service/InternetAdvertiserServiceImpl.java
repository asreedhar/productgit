package biz.firstlook.planning.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;

import biz.firstlook.entity.Inventory;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiser;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildList;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildListPk;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBuildLog;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserBusinessUnit;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserHistory;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiserThirdPartyEntity;
import biz.firstlook.planning.persistence.IInventoryThirdPartyDAO;
import biz.firstlook.planning.persistence.InternetAdvertiserBuildListDAO;
import biz.firstlook.planning.persistence.InternetAdvertiserBuildLogDAO;
import biz.firstlook.planning.persistence.InternetAdvertiserBusinessUnitDAO;
import biz.firstlook.planning.persistence.InternetAdvertiserHistoryDAO;
import biz.firstlook.planning.persistence.InternetAdvertiserThirdPartyEntityDAO;

public class InternetAdvertiserServiceImpl implements InternetAdvertiserService
{

private IInventoryThirdPartyDAO inventoryThirdPartyDAO;
private InternetAdvertiserBuildListDAO internetAdvertiserBuildListDAO;
private InternetAdvertiserBusinessUnitDAO internetAdvertiserBusinessUnitDAO;
private InternetAdvertiserHistoryDAO internetAdvertiserHistoryDAO;
private InternetAdvertiserThirdPartyEntityDAO internetAdvertiserThirdPartyEntityDAO;
private InternetAdvertiserBuildLogDAO internetAdvertiserBuildLogDAO;

public InternetAdvertiserServiceImpl()
{
}

public boolean internetAdvertisersEnabled( Integer businessUnitId )
{
	boolean hasActiveAdvertisers = false;
	for ( InternetAdvertiserBusinessUnit businessUnitAdvertiser : internetAdvertiserBusinessUnitDAO.findBusinessUnitAdvertisers( businessUnitId ) )
	{
		if ( businessUnitAdvertiser.getExporting() || businessUnitAdvertiser.getIncrementalExporting() )
		{
			hasActiveAdvertisers = true;
		}
	}
	return hasActiveAdvertisers;
}

/*
 * Returns a list of advertisers for a given inventoryId.
 * 
 * @see biz.firstlook.planning.service.internetAdvertiser.InternetAdvertiserService#getInventoryAdvertisers(java.lang.Integer,
 *      java.lang.Integer)
 */
public List< InternetAdvertiser > getInventoryAdvertisers( Integer inventoryId, Integer businessUnitId )
{
	List< InternetAdvertiser > inventoryAdvertisers = new ArrayList< InternetAdvertiser >();
	List< InternetAdvertiserBusinessUnit > businessUnitAdvertisers = internetAdvertiserBusinessUnitDAO.findBusinessUnitAdvertisers( businessUnitId );

	for ( InternetAdvertiserBusinessUnit advertiser : businessUnitAdvertisers )
	{
		//creates InternetAdvertisers only if they are in the BuildList table
		InternetAdvertiser internetAd = createDisplayBean( internetAdvertiserBuildListDAO.load( inventoryId,
																								advertiser.getId().getAdvertiserId() ),
															businessUnitId );
		if ( internetAd != null )
		{
			inventoryAdvertisers.add( internetAd );
		}
	}
	if ( businessUnitAdvertisers.size() != inventoryAdvertisers.size() )
	{
		addEmptyAdvertisers( businessUnitAdvertisers, inventoryAdvertisers, businessUnitId, inventoryId );
	}
	Collections.sort( inventoryAdvertisers );
	return inventoryAdvertisers;
}

/*
 * Returns the names of the advertisers currently exporting for a given inventoryId.
 * 
 * @see biz.firstlook.planning.service.internetAdvertiser.InternetAdvertiserService#getActiveInventoryAdvertisers(java.lang.Integer)
 */
public String getExportingInventoryAdvertiserNames( Integer inventoryId, Integer businessUnitId )
{
	StringBuilder exportingAdvertisers = new StringBuilder();
	List< Integer > activeAdIds = new ArrayList< Integer >();
	for ( InternetAdvertiserBusinessUnit advertiser : internetAdvertiserBusinessUnitDAO.findBusinessUnitAdvertisers( businessUnitId ) )
	{
		if ( advertiser.getExporting() )
		{
			activeAdIds.add( advertiser.getId().getAdvertiserId() );
		}
	}
	List< InternetAdvertiserBuildList > exportItems = internetAdvertiserBuildListDAO.findInventoryAdvertisers( inventoryId );
	for ( InternetAdvertiserBuildList item : exportItems )
	{
		if ( item.getExportStatusCode() != 10 && activeAdIds.contains( item.getId().getAdvertiserId() ) )
		{
			exportingAdvertisers.append( inventoryThirdPartyDAO.findNameById( item.getId().getAdvertiserId() ) );
			exportingAdvertisers.append( ", " );
		}
	}
	String returnString = exportingAdvertisers.toString();
	if ( returnString.length() > 1 )
	{
		returnString = returnString.substring( 0, returnString.length() - 2 );
	}
	return returnString;
}

// Saves the advertisers
public void saveAdvertisers( List< InternetAdvertiser > advertisersToSave, Inventory inventory )
{
	List<InternetAdvertiserBusinessUnit> advertisingPreferences = internetAdvertiserBusinessUnitDAO.findBusinessUnitAdvertisers( inventory.getBusinessUnitId() );
	
	Map<Integer, InternetAdvertiserBusinessUnit> internetAdvertiserBusinessUnitCache = new HashMap< Integer, InternetAdvertiserBusinessUnit >();
	for( InternetAdvertiserBusinessUnit advertisingPreference : advertisingPreferences )
	{
		internetAdvertiserBusinessUnitCache.put( advertisingPreference.getId().getAdvertiserId(), advertisingPreference );
	}
	
	for ( InternetAdvertiser advertiser : advertisersToSave )
	{
		InternetAdvertiserBusinessUnit internetAdvertiser = internetAdvertiserBusinessUnitCache.get( advertiser.getAdvertiserId() );
		
		//this needs a good refactoring.
		if( internetAdvertiser.getExporting() || internetAdvertiser.getIncrementalExporting() )
		{
			InternetAdvertiserBuildList advertiserToSave = new InternetAdvertiserBuildList();
			advertiserToSave.setId( new InternetAdvertiserBuildListPk( advertiser.getAdvertiserId(), inventory.getId() ) );
			// mystic bit masks
			// 11 is Export, set By User
			// 10 is Do NOT export, set by User
			// This is in the Full Export context.
			//advertiserToSave.setExportStatusCode( advertiser.isSendVehicle() ? 11 : 10 );
			
			advertiserToSave.setExportStatusCode( advertiser.isSendVehicle(), internetAdvertiser.getExporting(), internetAdvertiser.getIncrementalExporting() );
			
			//This line probably hammers the database like crazy.
			saveOrUpdateAdvertiser( advertiserToSave );
		}
	}
}

public List< String > retrieveAdvertiserDatafeeds()
{
	List< InternetAdvertiserThirdPartyEntity > datafeeds = internetAdvertiserThirdPartyEntityDAO.retrieveAll();
	List< String > datafeedCodes = new ArrayList< String >();
	for ( InternetAdvertiserThirdPartyEntity datafeed : datafeeds )
	{
		datafeedCodes.add( datafeed.getDatafeedCode() );
	}
	return datafeedCodes;
}

public Integer retrieveLatestBuildLogIdForDatafeed( String datafeedCode )
{
	Integer advertiserId = internetAdvertiserThirdPartyEntityDAO.findAdvertiserIdForDatafeed( datafeedCode );
	InternetAdvertiserBuildLog buildLog = internetAdvertiserBuildLogDAO.findBuildLogsForExceptionReport( advertiserId );
	if ( buildLog != null )
	{
		return buildLog.getAdvertiserBuildLogId();
	}
	return null;
}

public List< Integer > retrieveBusinessUnitsForDatafeed( String datafeedCode )
{
	Integer advertiserId = internetAdvertiserThirdPartyEntityDAO.findAdvertiserIdForDatafeed( datafeedCode );

	List< InternetAdvertiserBusinessUnit > advertiserBusinessUnits = internetAdvertiserBusinessUnitDAO.findBusinessUnitsForAdvertiserId( advertiserId );
	List< Integer > businessUnits = new ArrayList< Integer >();

	for ( InternetAdvertiserBusinessUnit advertiserBusiness : advertiserBusinessUnits )
	{
		businessUnits.add( advertiserBusiness.getId().getBusinessUnitId() );
	}
	return businessUnits;
}

public void setExceptionReportSent( Integer buildLogId )
{
	InternetAdvertiserBuildLog logToUpdate = internetAdvertiserBuildLogDAO.findById( buildLogId );
	if ( logToUpdate != null )
	{
		logToUpdate.setExceptionReportsSent( new Timestamp( new Date().getTime() ) );
		internetAdvertiserBuildLogDAO.getSessionFactory().getCurrentSession().setFlushMode( FlushMode.ALWAYS );
		internetAdvertiserBuildLogDAO.saveOrUpdate( logToUpdate );
		internetAdvertiserBuildLogDAO.getSessionFactory().getCurrentSession().flush();
	}
}

@SuppressWarnings( "unchecked" )
private void saveOrUpdateAdvertiser( InternetAdvertiserBuildList advertiserToSave )
{
	internetAdvertiserBuildListDAO.getSessionFactory().getCurrentSession().setFlushMode( FlushMode.ALWAYS );
	// If we are saying do not export, then we do not want to add a row to the
	// table only update ones that are there that are currently exporting.
	if ( advertiserToSave.getExportStatusCode() == 10 )
	{
		InternetAdvertiserBuildList entity = internetAdvertiserBuildListDAO.load( advertiserToSave.getId().getInventoryId(),
																					advertiserToSave.getId().getAdvertiserId() );
		if ( entity != null )
		{
			entity.setExportStatusCode( advertiserToSave.getExportStatusCode() );
			internetAdvertiserBuildListDAO.saveOrUpdate( entity );
		}
		internetAdvertiserBuildListDAO.getSessionFactory().getCurrentSession().flush();
		return;
	}
	internetAdvertiserBuildListDAO.saveOrUpdate( advertiserToSave );
	internetAdvertiserBuildListDAO.getSessionFactory().getCurrentSession().flush();
}

private InternetAdvertiser createDisplayBean( InternetAdvertiserBuildList exportItem, Integer businessUnitId )
{
	InternetAdvertiser advertiser = null;

	if ( exportItem != null )
	{
		advertiser = new InternetAdvertiser();
		advertiser.setAdvertiserId( exportItem.getId().getAdvertiserId() );
		advertiser.setInventoryId( exportItem.getId().getInventoryId() );
		advertiser.setSendVehicle( exportItem.getExportStatusCode() != 10 ? true : false );
		advertiser.setName( inventoryThirdPartyDAO.findNameById( exportItem.getId().getAdvertiserId() ) );
		advertiser.setHistory( getCreatedDates( exportItem.getId().getInventoryId(), exportItem.getId().getAdvertiserId() ) );
	}
	return advertiser;
}

private List< String > getCreatedDates( Integer inventoryId, Integer advertiserId )
{
	DateFormat dateFormatter = DateFormat.getDateInstance( DateFormat.SHORT );
	List< String > dateList = new ArrayList< String >();
	for ( InternetAdvertiserHistory adHistory : internetAdvertiserHistoryDAO.findInventoryAdvertiserHistory( inventoryId, advertiserId ) )
	{
		dateList.add( dateFormatter.format( adHistory.getCreated() ) );
	}
	return dateList;
}

private List< InternetAdvertiser > addEmptyAdvertisers( List< InternetAdvertiserBusinessUnit > businessUnitAdvertisers,
														List< InternetAdvertiser > inventoryAdvertisers, Integer businessUnitId,
														Integer inventoryId )
{

	List< InternetAdvertiser > toBeAdded = new ArrayList< InternetAdvertiser >();
	List< Integer > adIds = new ArrayList< Integer >();
	List< Integer > activeAdIds = new ArrayList< Integer >();

	for ( InternetAdvertiserBusinessUnit advertiser : businessUnitAdvertisers )
	{
		if ( advertiser.getExporting() || advertiser.getIncrementalExporting() )
		{
			adIds.add( advertiser.getId().getAdvertiserId() );
		}
	}
	for ( InternetAdvertiser advertiser : inventoryAdvertisers )
	{
		activeAdIds.add( advertiser.getAdvertiserId() );
	}
	for ( Integer adId : adIds )
	{
		if ( !activeAdIds.contains( adId ) )
		{
			InternetAdvertiser emptyAd = new InternetAdvertiser();
			emptyAd.setAdvertiserId( adId );
			emptyAd.setInventoryId( inventoryId );
			emptyAd.setSendVehicle( false );
			emptyAd.setName( inventoryThirdPartyDAO.findNameById( adId ) );
			toBeAdded.add( emptyAd );
		}
	}
	inventoryAdvertisers.addAll( toBeAdded );
	return inventoryAdvertisers;
}

public void setInternetAdvertiserBuildListDAO( InternetAdvertiserBuildListDAO internetAdvertiserBuildListDAO )
{
	this.internetAdvertiserBuildListDAO = internetAdvertiserBuildListDAO;
}

public void setInternetAdvertiserBusinessUnitDAO( InternetAdvertiserBusinessUnitDAO internetAdvertiserDealershipDAO )
{
	this.internetAdvertiserBusinessUnitDAO = internetAdvertiserDealershipDAO;
}

public void setInternetAdvertiserHistoryDAO( InternetAdvertiserHistoryDAO internetAdvertiserHistoryDAO )
{
	this.internetAdvertiserHistoryDAO = internetAdvertiserHistoryDAO;
}

public void setInventoryThirdPartyDAO( IInventoryThirdPartyDAO inventoryThirdPartyDAO )
{
	this.inventoryThirdPartyDAO = inventoryThirdPartyDAO;
}

public void setInternetAdvertiserThirdPartyEntityDAO( InternetAdvertiserThirdPartyEntityDAO internetAdvertiserThirdPartyEntityDAO )
{
	this.internetAdvertiserThirdPartyEntityDAO = internetAdvertiserThirdPartyEntityDAO;
}

public void setInternetAdvertiserBuildLogDAO( InternetAdvertiserBuildLogDAO internetAdvertiserBuildLogDAO )
{
	this.internetAdvertiserBuildLogDAO = internetAdvertiserBuildLogDAO;
}

}
