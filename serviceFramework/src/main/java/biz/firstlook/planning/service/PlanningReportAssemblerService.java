package biz.firstlook.planning.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.main.enumerator.PlanningGroupByEnum;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.main.enumerator.TradeOrPurchaseEnum;
import biz.firstlook.planning.form.InventoryItemPlanInfo;
import biz.firstlook.planning.form.InventoryItemPlanSummaryDetailStrategy;
import biz.firstlook.planning.form.InventoryItemPlanSummaryInfo;
import biz.firstlook.planning.util.InventoryPlanningUtil;
import biz.firstlook.report.reportBuilders.aip.PlanningDetailForIndividualItemReportBuilder;
import biz.firstlook.report.reportBuilders.aip.PlanningDetailReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class PlanningReportAssemblerService
{

private IReportService transactionalReportService;
private InternetAdvertiserService internetAdvertiserService;

@SuppressWarnings("unchecked")
public Map<Object, List<InventoryItemPlanInfo>> assembleReport( PlanningResultsModeEnum resultsModeEnum, PlanningGroupByEnum groupByEnum, Integer rangeId,
									Integer businessUnitId, Integer searchDays, InventoryBucketTypeEnum bucketTypeEnum, String statusCodeStr, Integer inventoryId,
									boolean showRecent, String inventoryIdsList )
{
	PlanningDetailReportBuilder planningDetailReportBuilder = new PlanningDetailReportBuilder(	businessUnitId,
			statusCodeStr,
			bucketTypeEnum.getInventoryBucketId(),
			resultsModeEnum.getId(),showRecent, inventoryIdsList,
			rangeId, searchDays, 
			new Date( new java.util.Date().getTime() ),
			inventoryId );
	
	List< Map > planningReportResults = getTransactionalReportService().getResultsFromStoredProcedure( planningDetailReportBuilder );

	addInternetAdvertisersToReport(planningReportResults, businessUnitId);
	List<InventoryItemPlanInfo> inventoryPlanItemInfoObjects = extractFromMap(planningReportResults);
	Map<Object, List<InventoryItemPlanInfo>> groupedplanningReportResults = InventoryPlanningUtil.groupReplanningDataByRange( inventoryPlanItemInfoObjects, groupByEnum );

	return groupedplanningReportResults;
}

@SuppressWarnings("unchecked")
public InventoryItemPlanInfo runReport( Integer businessUnitId, Integer inventoryId )
{
	PlanningDetailForIndividualItemReportBuilder planningDetailForIndividualItemReportBuilder = new PlanningDetailForIndividualItemReportBuilder(inventoryId,businessUnitId );
	List< Map > planningReportResults = getTransactionalReportService().getResultsFromStoredProcedure( planningDetailForIndividualItemReportBuilder );
	List<InventoryItemPlanInfo> inventoryPlanItemInfoObjects = extractFromMap(planningReportResults);
	InventoryItemPlanInfo item = !inventoryPlanItemInfoObjects.isEmpty() ? inventoryPlanItemInfoObjects.get(0) : null ;
	return item;
}

@SuppressWarnings("unchecked")
private void addInternetAdvertisersToReport(List<Map> planningResults, Integer businessUnitId) {
    if (internetAdvertiserService.internetAdvertisersEnabled(businessUnitId)) {
    for (Map planningReportResult : planningResults) {
                Integer invId = Integer.parseInt((String) planningReportResult.get("inventoryId"));
                String advertisers = internetAdvertiserService.getExportingInventoryAdvertiserNames(invId, businessUnitId);
                planningReportResult.put("exportingAdvertisers", advertisers);
            }
        }
}

@SuppressWarnings("unchecked")
private List<InventoryItemPlanInfo> extractFromMap(List<Map> planningReportResults) {
	List<InventoryItemPlanInfo> inventoryPlanItemInfoObjects = new ArrayList<InventoryItemPlanInfo>(planningReportResults.size());
	
	DateFormat stdDateFormat = new SimpleDateFormat("MM-dd-yyyy");
	for(Map result : planningReportResults) {
		InventoryItemPlanInfo itemPlan = new InventoryItemPlanInfo();
		itemPlan.setInventoryId(Functions.nullSafeInteger(result.get("inventoryId")));
		itemPlan.setRangeId(Functions.nullSafeInteger(result.get("RangeID")));
		itemPlan.setAgeInDays(Functions.nullSafeInteger(result.get("AgeInDays")));
		itemPlan.setCurrentVehicleLight(Functions.nullSafeInteger(result.get("CurrentVehicleLight")));
		itemPlan.setVehicleYear((String)result.get("VehicleYear"));
		itemPlan.setMake((String)result.get("Make"));
		itemPlan.setModel((String)result.get("Model"));
		itemPlan.setMakeModelGroupingId(Functions.nullSafeInteger(result.get("MakeModelGroupingID")));
		itemPlan.setGroupingDescription((String)result.get("GroupingDescriptionStr"));
		itemPlan.setGroupingDescriptionId(Functions.nullSafeInteger(result.get("GroupingDescriptionID")));
		itemPlan.setVehicleTrim((String)result.get("VehicleTrim"));
		itemPlan.setBodyType((String)result.get("BodyType"));
		itemPlan.setBaseColor((String)result.get("BaseColor"));
		itemPlan.setMileageReceived(Functions.nullSafeInteger(result.get("MileageReceived")));
		itemPlan.setListPrice(Functions.nullSafeFloat(result.get("ListPrice")));
		itemPlan.setLotPrice(Functions.nullSafeFloat(result.get("LotPrice")));
		itemPlan.setUnitCost(Functions.nullSafeFloat(result.get("UnitCost")));
		TradeOrPurchaseEnum tOrP = TradeOrPurchaseEnum.getTradeOrPurchaseEnum(Functions.nullSafeInteger(result.get("TradeOrPurchase")));
		itemPlan.setTradeOrPurchase(tOrP);
		itemPlan.setStockNumber((String)result.get("StockNumber"));
		itemPlan.setPrimaryBookValue(Functions.nullSafeInteger(result.get("PrimaryBookValue")));
		itemPlan.setSecondaryBookValue(Functions.nullSafeInteger(result.get("SecondaryBookValue")));
		itemPlan.setLevel4Analysis((String)result.get("Level4Analysis"));
		
		Date planReminderDate = null;
		try {
			String prd = (String)result.get("PlanReminderDate");
			if(prd != null) {
				planReminderDate = stdDateFormat.parse(prd);
			}
		} catch (ParseException pe) {
			//log it
		}
		itemPlan.setPlanReminderDate(planReminderDate);
		itemPlan.setLastPlannedDate((Date)result.get("java.util.Date:LastPlannedDate"));
		itemPlan.setHasSalesHistory(Functions.nullSafeBoolean(result.get("HasSalesHistory")));
		// nk - this is to match logic in AgingPlanInventoryItemRetriever.mapRow
		// accurate==null happens when we don't have book values. This is not = Inaccurate since
		// we can not have book values because the GuideBook has not (or has yet) to publish them.
		if (result.get("IsAccurate") == null) {
			itemPlan.setIsAccurate(Boolean.TRUE);			
		} else {
			itemPlan.setIsAccurate(Functions.nullSafeBoolean(result.get("IsAccurate")));			
		}
		itemPlan.setUnitsSold(Functions.nullSafeInteger(result.get("UnitsSold")));
		itemPlan.setAvgRetailGrossProfit(Functions.nullSafeInteger(result.get("AvgRetailGrossProfit")));
		itemPlan.setAvgDays2Sale(Functions.nullSafeInteger(result.get("AvgDays2Sale")));
		itemPlan.setNoSales(Functions.nullSafeInteger(result.get("NoSales")));
		itemPlan.setUnitsInStock(Functions.nullSafeInteger(result.get("UnitsInStock")));
		itemPlan.setInventoryStatusCodeShortDescription((String)result.get("InventoryStatusCodeShortDescription"));
		itemPlan.setVehicleLocation((String)result.get("VehicleLocation"));
		itemPlan.setAdDescription((String)result.get("AdDescription"));
		itemPlan.setEdmundsTMV(Functions.nullSafeFloat(result.get("EdmundsTMV")));
		itemPlan.setTransferPrice(Functions.nullSafeFloat(result.get("TransferPrice")));
		
		boolean transferForRetailOnly = Functions.nullSafeIntValue(result.get("TransferForRetailOnly")) == 1 ? true : false;
		itemPlan.setTransferForRetailOnly(transferForRetailOnly);
		boolean transferForRetailOnlyEnabled = Functions.nullSafeIntValue(result.get("TransferForRetailOnlyEnabled")) == 1 ? true : false;
		itemPlan.setTransferForRetailOnlyEnabled(transferForRetailOnlyEnabled);
		itemPlan.setBookVsCost(Functions.nullSafeFloat(result.get("BookVsCost")));
		itemPlan.setExportingAdvertisers((String)result.get("exportingAdvertisers"));
		itemPlan.setMmrValue((Integer)result.get("MMRValue"));
		itemPlan.setMmrBookVsCost(Functions.nullSafeFloat(result.get("MMRBookVsCost")));


		InventoryItemPlanSummaryInfo planSummary = itemPlan.getPlanSummary();
		Map ps = (Map)result.get("PlanSummary");
		planSummary.setMarketplacePosted(Functions.nullSafeBoolean(ps.get("marketplacePosted")));
		planSummary.setNeverPlanned(Functions.nullSafeBoolean(ps.get("NeverPlanned")));
		planSummary.setRepriced(Functions.nullSafeBoolean(ps.get("Repriced")));
		planSummary.setDueForReplanning(Functions.nullSafeBoolean(ps.get("DueForReplanning")));
		planSummary.setHasCurrentPlan(Functions.nullSafeBoolean(ps.get("HasCurrentPlan")));
		planSummary.setObjective((String)ps.get("objective"));
		planSummary.setNoPlan(Functions.nullSafeBoolean(ps.get("NoPlan")));
		
		for(Map ds : (List<Map>)ps.get("detailedStrategies")) {
			InventoryItemPlanSummaryDetailStrategy detailStrat = planSummary.newDetailStrategy();
			detailStrat.setValue(Functions.nullSafeInteger(ds.get("value")));
			Date detailBeginDate = null;
			try {
				String bd = (String)ds.get("beginDate");
				if(bd != null) {
					detailBeginDate = stdDateFormat.parse(bd);
				}
			} catch (ParseException pe) {
				//log it
			}
			detailStrat.setBeginDate(detailBeginDate);
			
			detailStrat.setThirdPartyEntityId(Functions.nullSafeInteger(ds.get("ThirdPartyEntityId")));
			
			detailStrat.setAIP_EventTypeID(Functions.nullSafeInteger(ds.get("AIP_EventTypeID")));
			detailStrat.setNotes((String)ds.get("notes"));
			
			Date detailEndDate = null;
			try {
				String bd = (String)ds.get("endDate");
				if(bd != null) {
					detailEndDate = stdDateFormat.parse(bd);
				}
			} catch (ParseException pe) {
				//log it
			}
			detailStrat.setEndDate(detailEndDate);
			
			detailStrat.setThirdParty((String)ds.get("ThirdParty"));
			detailStrat.setUserBeginDate((Date)ds.get("UserBeginDate"));
			detailStrat.setUserEndDate((Date)ds.get("UserEndDate"));
		}
		
		inventoryPlanItemInfoObjects.add(itemPlan);
	}
	return inventoryPlanItemInfoObjects;
}
public IReportService getTransactionalReportService()
{
	return transactionalReportService;
}

public void setTransactionalReportService( IReportService transactionalReportService )
{
	this.transactionalReportService = transactionalReportService;
}

public void setInternetAdvertiserService(
        InternetAdvertiserService internetAdvertiserService) {
    this.internetAdvertiserService = internetAdvertiserService;
}

}
