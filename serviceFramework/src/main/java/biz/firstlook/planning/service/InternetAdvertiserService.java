package biz.firstlook.planning.service;

import java.util.List;

import biz.firstlook.entity.Inventory;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiser;

public interface InternetAdvertiserService {
    
    public boolean internetAdvertisersEnabled(Integer businessUnitId);
    public List<InternetAdvertiser> getInventoryAdvertisers(Integer inventoryId, Integer businessUnitId);
    public String getExportingInventoryAdvertiserNames(Integer inventoryId, Integer businessUnitId);
    public void saveAdvertisers(List<InternetAdvertiser> advertisers, Inventory inventory);
    public List<String> retrieveAdvertiserDatafeeds();
    public Integer retrieveLatestBuildLogIdForDatafeed(String datafeedCode);
    public List<Integer> retrieveBusinessUnitsForDatafeed(String datafeedCode);
    public void setExceptionReportSent(Integer buildLogId);   
    
}
