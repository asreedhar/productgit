package biz.firstlook.planning.service;

import org.json.JSONArray;
import org.json.JSONException;

public interface IInventoryPlanningThirdPartyService
{
public JSONArray saveThirdParties( String jsonText, String eventType, Integer businessUnitId ) throws JSONException;
}
