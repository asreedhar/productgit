package biz.firstlook.planning.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.persistence.IInventoryPlanThirdPartyDAO;
import biz.firstlook.planning.transactionFramework.ITransactionDAO;

public class InventoryPlanningThirdPartyService implements IInventoryPlanningThirdPartyService 
{

private IInventoryPlanThirdPartyDAO inventoryPlanThirdPartyDAO;
private IInventoryPlanEventDAO inventoryPlanEventDAO;
private ITransactionDAO transactionDAO;

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}

public JSONArray saveThirdParties( String jsonText, String eventType, Integer businessUnitId ) throws JSONException
{
	List<InventoryThirdParty> thirdPartiesToRemove = new ArrayList<InventoryThirdParty>();
	List<InventoryThirdParty> thirdPartiesToAdd = new ArrayList<InventoryThirdParty>();
	
	JSONObject jsonObject = new JSONObject( jsonText );
	if ( jsonObject.has( "remove" ) ) {
		JSONArray toRemove = (JSONArray) jsonObject.get( "remove" );
		List<Integer> idsToRemove = new ArrayList<Integer>(toRemove.length());
		
		for ( int i = 0; i < toRemove.length(); i++ )
		{
			JSONObject jsonTp = toRemove.getJSONObject( i );
			Integer id = Functions.nullSafeInteger(jsonTp.get("dbId"));
			if (id != null)
			{
				idsToRemove.add( id );
			}
		}
		thirdPartiesToRemove.addAll( inventoryPlanThirdPartyDAO.findThirdParties( idsToRemove ) );
	}
	
	if ( jsonObject.has(  "add" ) ) {
		JSONArray toAdd = (JSONArray) jsonObject.get( "add" );
		for ( int i = 0; i < toAdd.length(); i++ )
		{
			JSONObject jsonTp = toAdd.getJSONObject( i );
			String description = (String) jsonTp.get( "description" );
			InventoryThirdPartyEnum inventoryThirdPary = InventoryThirdPartyEnum.getEnumByPlannEventType(PlanningEventTypeEnum.getPlanEventTypeEnum( eventType ));
								 
			InventoryThirdParty tp = new InventoryThirdParty();
			tp.setName( description );
			tp.setInventoryThirdPartyTypeId( inventoryThirdPary.getId() );
			tp.setBusinessUnitId( businessUnitId );
			
			thirdPartiesToAdd.add( tp );
		}
	}
	
	if ( !thirdPartiesToRemove.isEmpty() )
	{
		List<InventoryPlanEvent> updateMe = inventoryPlanEventDAO.findEventsByThirdPartyId( businessUnitId, thirdPartiesToRemove );
		for (InventoryPlanEvent event : updateMe )
		{
			event.setNotes3( event.getInventoryThirdParty().getName() );
			event.setInventoryThirdParty( null );
		}
		transactionDAO.execute( updateMe );
	}
	transactionDAO.execute( thirdPartiesToAdd, thirdPartiesToRemove );
	
	JSONArray addedTpJson = new JSONArray();
	for ( InventoryThirdParty addedTp : thirdPartiesToAdd )
	{
		addedTpJson.put( InventoryPlanningUtilService.convertThirdPartyToJSON( addedTp ) );
	}

	return addedTpJson;
}

public void setInventoryPlanThirdPartyDAO( IInventoryPlanThirdPartyDAO inventoryPlanThirdPartyDAO )
{
	this.inventoryPlanThirdPartyDAO = inventoryPlanThirdPartyDAO;
}

public void setTransactionDAO( ITransactionDAO transactionDAO )
{
	this.transactionDAO = transactionDAO;
}

}
