package biz.firstlook.planning.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiser;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;

public class InventoryPlanningUtilService
{

public enum SaveAction{ NEW, UPDATE, END };

/**
 * Converts the list of current events into a JSON object
 * 
 * @param allEvents
 * @return List<InventoryAgingPlanEvent>
 */
public static void convertCurrentEventsToJSON( List< InventoryPlanEvent > currentEvents, JSONObject jsonPlan ) throws JSONException
{	
	jsonPlan.put( "spiff", new JSONArray() );
	jsonPlan.put( "lotPromote", new JSONArray() );
	jsonPlan.put( "advertise", new JSONArray() );
	jsonPlan.put( "retailOther", new JSONArray() );
	jsonPlan.put( "reprice", new JSONArray() );
	jsonPlan.put( "auction", new JSONArray() );
	jsonPlan.put( "wholesaler", new JSONArray() );
	jsonPlan.put( "sold", new JSONArray() );
	jsonPlan.put( "wholesaleOther", new JSONArray() );
	jsonPlan.put( "other", new JSONArray() );
	jsonPlan.put( "service", new JSONArray() );
	jsonPlan.put( "detail", new JSONArray() );
		
    for ( InventoryPlanEvent event : currentEvents )
    {
    	if ( event.getEventTypeEnum() != PlanningEventTypeEnum.RETAIL_NO_STRATEGY && 
    		 event.getEventTypeEnum() != PlanningEventTypeEnum.WHOLESALE_NO_STRATEGY &&
    		 event.getEventTypeEnum() != PlanningEventTypeEnum.SOLD_NO_STRATEGY &&
    		 event.getEventTypeEnum() != PlanningEventTypeEnum.OTHER_NO_STRATEGY )
    	{
    		jsonPlan.getJSONArray( event.getJSONType() ).put( event.JSONify() );
    	}
    }	
}

public static String convertDateToString( Date date )
{
	String dateString = new String();
	
	if ( date != null )
	{
        SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );	
        dateString = dateFormat.format( date );
	}
	
	return dateString;
}

public static void convertThirdPartiesToJSON( List< InventoryThirdParty > thirdParties, JSONObject jsonPlan ) throws JSONException
{
	JSONArray wholesalers = new JSONArray();
	JSONArray auctions = new JSONArray();
	
	for ( InventoryThirdParty thirdParty : thirdParties )
	{
		JSONObject jsonThirdParty = convertThirdPartyToJSON( thirdParty );
		
		Integer eventType = thirdParty.getInventoryThirdPartyTypeId();
		switch ( InventoryThirdPartyEnum.getEnumById( eventType ) )
		{
			case AUCTION:
				auctions.put( jsonThirdParty );
				break;
			case WHOLESALER:
				wholesalers.put( jsonThirdParty );
				break;
		}
	}

	jsonPlan.put( "wholesalers", wholesalers );	
	jsonPlan.put( "auctions",    auctions );
}

public static JSONObject convertThirdPartyToJSON( InventoryThirdParty thirdParty ) throws JSONException
{
	JSONObject jsonThirdParty = new JSONObject();
	
	jsonThirdParty.put( "id", thirdParty.getId().intValue() );
	jsonThirdParty.put( "description", thirdParty.getName() );
	
	return jsonThirdParty;
}

@SuppressWarnings("unchecked")
public static List<JSONObject> extractJSONEvents( JSONObject jsonEvents ) throws JSONException
{
	List<JSONObject> newEventsJSONExtracted = new ArrayList<JSONObject>();
	
	if ( jsonEvents.length() > 0 )
	{
	    Iterator<String> keys = (Iterator<String>)jsonEvents.keys();
	    JSONObject jsonObject = null;
	    JSONArray strategyArray = null;
	    PlanningEventTypeEnum eventTypeEnum = null;
	    
	    while ( keys.hasNext() )
	    {
	        eventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum( keys.next() );
	        
			if ( eventTypeEnum.isToggledEvent() )
			{
			    jsonObject = jsonEvents.optJSONObject( eventTypeEnum.getJsonKey() );
			    jsonObject.put( "strategy", eventTypeEnum.getJsonKey() );
				newEventsJSONExtracted.add( jsonObject ); 
			}
			else
			{
	            strategyArray = jsonEvents.optJSONArray( eventTypeEnum.getJsonKey() );
	        
	            for( int i=0; i < strategyArray.length(); i++ )
	            {
	        	    jsonObject = (JSONObject) strategyArray.opt( i );
	        	    jsonObject.put( "strategy", eventTypeEnum.getJsonKey() );
	        	    newEventsJSONExtracted.add( jsonObject ); 
	            }
			}
	    }
	}
	
	return newEventsJSONExtracted;	
}

public static List<JSONObject> extractJSONInternetAdvertisers( JSONObject jsonEvents ) throws JSONException
{
    List<JSONObject> newEventsJSONExtracted = new ArrayList<JSONObject>();
    
    if ( jsonEvents != null && jsonEvents.length() > 0 )
    {
        JSONObject jsonObject = null;
        JSONArray strategyArray = null;
        strategyArray = jsonEvents.optJSONArray("advertisers");
        
        for( int i=0; i < strategyArray.length(); i++ )
        {
            jsonObject = (JSONObject) strategyArray.opt( i );
            newEventsJSONExtracted.add( jsonObject ); 
        }
        
    }
    return newEventsJSONExtracted;  
}

public static List<JSONObject> extractEventsByType( JSONObject jsonPlan, String type ) throws JSONException
{
	JSONObject jsonEvents = jsonPlan.optJSONObject( type );
    if (type.equalsIgnoreCase("internetAdvertising")){
        return extractJSONInternetAdvertisers( jsonEvents );
    }
    return extractJSONEvents( jsonEvents );
}

public static List<InventoryPlanEvent> convertJSONToEvents( List<JSONObject> jsonEvents, List<InventoryPlanEvent> currentEvents, SaveAction saveAction ) throws JSONException
{
	List<InventoryPlanEvent> events = new ArrayList<InventoryPlanEvent>();
	
	if ( !jsonEvents.isEmpty() )
    {
   	    for( JSONObject jsonObject : jsonEvents )
   	    {
		    events.add( convertJSONEventToInventoryPlanEvent( jsonObject, currentEvents, saveAction ) );
   	    }
    }

	return events;
}

public static InventoryPlanEvent convertJSONEventToInventoryPlanEvent( JSONObject jsonEvent, List<InventoryPlanEvent> currentEvents, SaveAction saveAction ) throws JSONException
{  
    String strategy = jsonEvent.optString( "strategy" );
    PlanningEventTypeEnum eventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum( strategy );
    InventoryPlanEvent event = null;
    	
    if ( saveAction == SaveAction.UPDATE ) {
        event = findEvent( currentEvents, jsonEvent.optInt( "id" ) );
    }
    if ( event == null ) {
    	event = new InventoryPlanEvent();	
    	event.setEventTypeId( eventTypeEnum.getId() );
    }
    
    switch(eventTypeEnum) {
    	case ADVERTISEMENT:
    		event.setBeginDate( DateUtils.truncate( new Date(), Calendar.DATE ) );
        	event.setEndDate( convertStringToDate( jsonEvent.getString( "planUntilDate" ) ) );
        	event.setEventTypeId( PlanningEventTypeEnum.ADVERTISEMENT.getId() );
        	
        	if ( jsonEvent.getString( "selectedPrintAdvertiserId" ).equals( "other" ) ) {
        		event.setInventoryThirdParty( null );
        		event.setNotes2( jsonEvent.getString( "otherPrintAdvertiserName" ) );
        	} else {
        		event.setInventoryThirdParty( new InventoryThirdParty() );
        		event.getInventoryThirdParty().setId( jsonEvent.getInt( "selectedPrintAdvertiserId" ) );
        	}
        	event.setNotes( jsonEvent.getString("adText") );
        	event.setNotes3( jsonEvent.getString("showPrice") );
        	if ( jsonEvent.has( "adPriceSelected" ) && jsonEvent.getString("adPriceSelected").equals( "true" ) ) {
        		event.setValue( 1d ); // 1 corresponts to true - as in use adPrice 
        	} else {
        		event.setValue( 0d ); // 0 corresponds to false - don't use adPrice
        	}
        	break;
    	case SPIFF:
    		String spiffNotes = jsonEvent.has( "strategyNotes" ) ? jsonEvent.optString( "strategyNotes" ) : "";
    		event = updateSpiff(event, spiffNotes);
    		break;
    	default:
    		if ( jsonEvent.has( "strategyNotes" ) ) {
    	        event.setNotes( jsonEvent.optString( "strategyNotes" ) );
    	    }
    	        
    	    if ( !eventTypeEnum.isToggledEvent() ) {
    	    	if ( event.getInventoryThirdParty() == null ) {
    	    		event.setInventoryThirdParty( new InventoryThirdParty() );
    	    	}
    	    	
    	    	if ( jsonEvent.has( "endDate" ) ) {
    	        	event.setEndDate( convertStringToDate( jsonEvent.optString( "endDate" ) ) );
    	        }
    	        
    	    	if ( jsonEvent.has( "thirdPartyId") && !jsonEvent.has( "thirdPartyOther" ) && !jsonEvent.has( "nameOther" ) ) {
    	    		
    	    		if ( ( eventTypeEnum.equals( PlanningEventTypeEnum.WHOLESALER ) ||
    	    				eventTypeEnum.equals( PlanningEventTypeEnum.AUCTION ) )&&
    	    				jsonEvent.optString( "thirdPartyId" ).equals( "Unspecified" )) {
    	    			jsonEvent.putOpt( "nameOther", "Unspecified" );
    	    		} else {
    	    			event.getInventoryThirdParty().setId( Integer.parseInt( jsonEvent.optString( "thirdPartyId" ) ) );
    	    			event.setNotes3( "" ); // in the case you go other 1 time plan to a managed plan
    	    		}
    	    	}

    	    	if( jsonEvent.has( "thirdParty" ) && !jsonEvent.has( "thirdPartyOther" ) ) {
    	            event.getInventoryThirdParty().setName( jsonEvent.optString( "thirdParty" ) );
    	        }
    	    	
    	    	if ( jsonEvent.has( "thirdPartyOther" ) ) {
    	    		event.setNotes3( jsonEvent.optString( "thirdPartyOther" ) );
    	    	}
    	    	
    	        if ( ( eventTypeEnum == PlanningEventTypeEnum.RETAIL ||
    			       eventTypeEnum == PlanningEventTypeEnum.WHOLESALE ||
    			       eventTypeEnum == PlanningEventTypeEnum.AUCTION ||
    			       eventTypeEnum == PlanningEventTypeEnum.WHOLESALER ) &&
    			       jsonEvent.has( "name" ) ) {
    			    event.getInventoryThirdParty().setName( jsonEvent.optString( "name" ) );
    		    }
    		
    	        if ( jsonEvent.has( "nameOther" ) ) {
    	        	event.setNotes3( jsonEvent.optString( "nameOther" ) );
    	        }
    	        
    		    if ( jsonEvent.has( "location" ) && eventTypeEnum == PlanningEventTypeEnum.AUCTION ) {
    			    event.setNotes2( jsonEvent.optString( "location", null ) );
    		    }  
    	    }
    	    
    	    if ( eventTypeEnum.equals( PlanningEventTypeEnum.SERVICE_DEPARTMENT ) || 
    	    	 eventTypeEnum.equals( PlanningEventTypeEnum.DETAILER ) ) {
    	    	if ( jsonEvent.has( "enterDate" ) ) {
    	        	event.setUserBeginDate( convertStringToDate( jsonEvent.optString( "enterDate" ) ) );
    	        }
    	    	if ( jsonEvent.has( "exitDate" ) ) {
    	        	event.setUserEndDate( convertStringToDate( jsonEvent.optString( "exitDate" ) ) );
    	        }
    	    }
    	    
    	    // if an InventoryPlanningThirdParty event was created but never set,remove it so it doesn't get saved
    	    if ( event.getInventoryThirdParty() != null && 
    	    		event.getInventoryThirdParty().getId() == null ) {
    	    	event.setInventoryThirdParty( null );
    	    }
    		break;
    }
    
    return event;
}

/**
 * Modifies the old spiff or copies the object (new object, same property values). 
 * @param oldSpiff
 * @param spiffNotes
 * @return A new InventoryPlanEvent object if the spiff is a new object, otherwise, the oldSpiff object with updated fields.
 */
static InventoryPlanEvent updateSpiff(final InventoryPlanEvent oldSpiff, String spiffNotes) {
	InventoryPlanEvent updatedSpiff = null;
	if(PlanningEventTypeEnum.SPIFF.equals(oldSpiff.getEventTypeEnum())) {
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		Date eventBeginDate = null;
		if(oldSpiff.getBeginDate() != null) {
			eventBeginDate = DateUtils.truncate(oldSpiff.getBeginDate(), Calendar.DATE);
		}
		//only make a new spiff event every day, instead of each save unless it's an actual update.
		if(eventBeginDate == null || !eventBeginDate.equals(today)) {
			updatedSpiff = new InventoryPlanEvent(oldSpiff);
			updatedSpiff.setNotes(spiffNotes);
			updatedSpiff.setVersion(null);
			updatedSpiff.setCreated(today);
			updatedSpiff.setBeginDate(today);
			updatedSpiff.setLastModified(today);
			updatedSpiff.setEndDate(null);
			
			oldSpiff.setEndDate(today);
		} else {
			oldSpiff.setNotes(spiffNotes);
			oldSpiff.setLastModified(today);
			updatedSpiff = oldSpiff;
		}
	}
	return updatedSpiff;
}

/**
 * Returns the event from the List with the given id, if it exists
 * @param event
 * @param id
 * @return
 */
public static InventoryPlanEvent findEvent( List<InventoryPlanEvent> events, Integer id )
{
    for( InventoryPlanEvent event : events )
    {
        if ( event.getId().equals( id ) )
        {
            return event;
        }
    }
    return null;
}

/**
 * converts a String to a Date in SHORT format
 */
public static Date convertStringToDate( String dateToConvert )
{
    try
    {
    	return DateFormat.getDateInstance( DateFormat.SHORT ).parse( dateToConvert );    	
    }
    catch (Exception e)
    {
        return null;	
    }
}

public static List<InternetAdvertiser> convertJSONToAdvertisers( List<JSONObject> jsonEvents)
{
    List<InternetAdvertiser> advertisers = new ArrayList<InternetAdvertiser>();
    
    if ( !jsonEvents.isEmpty() )
    {
        for( JSONObject jsonObject : jsonEvents )
        {
            if (jsonObject != null && !jsonObject.isNull("id")){
                advertisers.add( convertJSONEventToInternetAdvertiser( jsonObject ) );
            }
        }
    }

    return advertisers;
}

public static InternetAdvertiser convertJSONEventToInternetAdvertiser( JSONObject jsonEvent)
{  
    String send = jsonEvent.optString( "send" );
    String advertiserId = jsonEvent.optString("id");
    
    InternetAdvertiser advertiser = new InternetAdvertiser();
    advertiser.setAdvertiserId(Integer.parseInt(advertiserId));
    advertiser.setSendVehicle(Boolean.parseBoolean(send));
    
    return advertiser;
}

}