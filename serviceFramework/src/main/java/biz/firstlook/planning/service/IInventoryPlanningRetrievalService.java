package biz.firstlook.planning.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.InventoryBucketRange;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;

public interface IInventoryPlanningRetrievalService {
    public Map<String, Object> loadCurrentPlan(Integer inventoryId,Integer businessUnitId, Integer ageInflationForPlanning, boolean checkAppraisalHistory) throws JSONException;
    public Date findNextPlanDate(Integer inventoryId);
    public Date findNextPlanDate(Integer inventoryId, Integer ageInflation);
    public List<InventoryThirdParty> loadWholesalersAndAuctions(Integer businessUnitId);
    public Inventory findInventoryByStockNumber(String stockNum, Integer businessUnitId);
    public PlanObjectiveEnum determineObjective(List<InventoryPlanEvent> events);
    public List<InventoryBucketRange> findBucketRange(Integer businessUnitId);
}
