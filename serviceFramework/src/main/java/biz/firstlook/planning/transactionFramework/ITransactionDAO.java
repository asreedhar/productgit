package biz.firstlook.planning.transactionFramework;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

public interface ITransactionDAO
{
@Transactional(rollbackFor=Exception.class)
public void execute(final Collection<? extends Object> objs);

@Transactional(rollbackFor=Exception.class)
public void execute( Collection<? extends Object> objs, Collection<? extends Object> objsToDelete );
}
