package biz.firstlook.planning.transactionFramework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class TransactionDAO extends HibernateDaoSupport implements ITransactionDAO
{

public void execute(final Collection<? extends Object> objs) {
	execute(objs, null);
}

public void execute( final Collection<? extends Object> objs, final Collection<? extends Object> objsToDelete )
{
	final List<Object> objectsToPersist = new ArrayList<Object>();
	if(objs != null) {
		objectsToPersist.addAll(objs);
	}
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	session.setFlushMode( FlushMode.ALWAYS );

	for(Object objToPersist : objectsToPersist)
	{
		getHibernateTemplate().saveOrUpdate( objToPersist );
	}
	if (objsToDelete != null) {
		getHibernateTemplate().deleteAll( objsToDelete );
	}
}

}
