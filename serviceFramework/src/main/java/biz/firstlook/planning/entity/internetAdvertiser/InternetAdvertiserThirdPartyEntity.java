package biz.firstlook.planning.entity.internetAdvertiser;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InternetAdvertiser_ThirdPartyEntity")
public class InternetAdvertiserThirdPartyEntity {

    @Id
    private Integer internetAdvertiserId;
    private String datafeedCode;

    
    public String getDatafeedCode() {
        return datafeedCode;
    }
    public void setDatafeedCode(String datafeedCode) {
        this.datafeedCode = datafeedCode;
    }

    public Integer getInternetAdvertiserId() {
        return internetAdvertiserId;
    }
    public void setInternetAdvertiserId(Integer internetAdvertiserId) {
        this.internetAdvertiserId = internetAdvertiserId;
    }
}
