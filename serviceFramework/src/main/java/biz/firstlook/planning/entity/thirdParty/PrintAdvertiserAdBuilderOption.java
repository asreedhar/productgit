package biz.firstlook.planning.entity.thirdParty;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PrintAdvertiserTextOptionDefault")
public class PrintAdvertiserAdBuilderOption implements Serializable
{

private static final long serialVersionUID = 1780510196762813396L;

@Id
@GeneratedValue(strategy=GenerationType.AUTO )
@Column(name="PrintAdvertiserTextOptionDefaultID")
private Integer id;

@ManyToOne
@JoinColumn( name = "PrintAdvertiserTextOptionID" )
private PrintAdvertiserOptionLookup option;

@ManyToOne( cascade = CascadeType.ALL )
@JoinColumn( name = "PrintAdvertiserID")
private PrintAdvertiser_InventoryThirdParty thirdParty;

private Integer displayOrder;
private Boolean status;
public Integer getDisplayOrder()
{
	return displayOrder;
}
public void setDisplayOrder( Integer displayOrder )
{
	this.displayOrder = displayOrder;
}
public Boolean getStatus()
{
	return status;
}
public void setStatus( Boolean status )
{
	this.status = status;
}
public PrintAdvertiserOptionLookup getOption()
{
	return option;
}
public void setOption( PrintAdvertiserOptionLookup option )
{
	this.option = option;
}
public Integer getId()
{
	return id;
}
public void setId( Integer id )
{
	this.id = id;
}
public PrintAdvertiser_InventoryThirdParty getThirdParty()
{
	return thirdParty;
}
public void setThirdParty( PrintAdvertiser_InventoryThirdParty thirdParty )
{
	this.thirdParty = thirdParty;
}

}
