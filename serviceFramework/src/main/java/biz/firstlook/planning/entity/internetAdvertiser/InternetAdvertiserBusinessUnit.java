package biz.firstlook.planning.entity.internetAdvertiser;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InternetAdvertiserDealership")
public class InternetAdvertiserBusinessUnit implements Serializable {

    private static final long serialVersionUID = -5077238197367571671L;

    @Id
    private InternetAdvertiserBusinessUnitPK id;
    
    @Column(name = "InternetAdvertisersDealershipCode")
    private String dealershipCode;
    
    @Column(name = "IsLive")
    private Boolean exporting;
    
    @Column(name = "IncrementalExport")
    private Boolean incrementalExporting;

    public String getDealershipCode() {
        return dealershipCode;
    }

    public void setDealershipCode(String dealershipCode) {
        this.dealershipCode = dealershipCode;
    }

    public Boolean getExporting() {
        return exporting;
    }

    public void setExporting(Boolean exporting) {
        this.exporting = exporting;
    }

    public InternetAdvertiserBusinessUnitPK getId() {
        return id;
    }

    public void setId(InternetAdvertiserBusinessUnitPK id) {
        this.id = id;
    }

	public Boolean getIncrementalExporting() {
		return incrementalExporting;
	}

	public void setIncrementalExporting( Boolean incrementalExporting ) {
		this.incrementalExporting = incrementalExporting;
	}

}
