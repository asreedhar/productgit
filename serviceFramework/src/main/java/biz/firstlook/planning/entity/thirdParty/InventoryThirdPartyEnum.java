package biz.firstlook.planning.entity.thirdParty;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;

public enum InventoryThirdPartyEnum
{
PRINT_ADVERTISER(1, "Print Advertiser"),
AUCTION(2, "Auction" ),
WHOLESALER(3, "Wholesaler" ),
INTERNET_ADVERTISER(4, "Internet Advertiser" ),
INTERNET_MARKETPLACE(5, "Internet Marketplace");

private Integer id;
private String description;

private InventoryThirdPartyEnum( Integer id, String description )
{
	this.id = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public Integer getId()
{
	return id;
}

public static InventoryThirdPartyEnum getEnumById( Integer eventType )
{
	switch ( eventType )
	{
		case 1:
			return PRINT_ADVERTISER;
		case 2:
			return AUCTION;
		case 3:
			return WHOLESALER;
		case 4:
			return INTERNET_ADVERTISER;
	}
	return null;
}

public static InventoryThirdPartyEnum getEnumByPlannEventType( PlanningEventTypeEnum planEventTypeEnum )
{
	switch ( planEventTypeEnum )
	{
		case WHOLESALER:
			return WHOLESALER;
		case AUCTION:
			return AUCTION;
	}
	return null;
}


}
