package biz.firstlook.planning.entity.thirdParty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="ThirdPartyEntity")
@Inheritance(strategy=InheritanceType.JOINED)
public class InventoryThirdParty implements Serializable
{

private static final long serialVersionUID = -2940262475228470340L;

@Id
@GeneratedValue( strategy = GenerationType.AUTO )
@Column( name = "ThirdPartyEntityID" )
protected Integer id;

@Column(name="Name")
protected String name;

@Column(name="BusinessUnitID")
protected Integer businessUnitId;

@Column(name="ThirdPartyEntityTypeID")
protected Integer inventoryThirdPartyTypeId;

public InventoryThirdParty()
{
	super();
}

public InventoryThirdParty(Integer id)
{
    super();
    this.id = id;
}

public String getName()
{
	return name;
}

public void setName( String description )
{
	this.name = description;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Integer getInventoryThirdPartyTypeId()
{
	return inventoryThirdPartyTypeId;
}

public void setInventoryThirdPartyTypeId( Integer thirdPartyEntityTypeId )
{
	this.inventoryThirdPartyTypeId = thirdPartyEntityTypeId;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
			+ ((businessUnitId == null) ? 0 : businessUnitId.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime
			* result
			+ ((inventoryThirdPartyTypeId == null) ? 0
					: inventoryThirdPartyTypeId.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	InventoryThirdParty other = (InventoryThirdParty) obj;
	if (businessUnitId == null) {
		if (other.businessUnitId != null)
			return false;
	} else if (!businessUnitId.equals(other.businessUnitId))
		return false;
	if (id == null) {
		if (other.id != null)
			return false;
	} else if (!id.equals(other.id))
		return false;
	if (inventoryThirdPartyTypeId == null) {
		if (other.inventoryThirdPartyTypeId != null)
			return false;
	} else if (!inventoryThirdPartyTypeId
			.equals(other.inventoryThirdPartyTypeId))
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	return true;
}

}
