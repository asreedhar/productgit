package biz.firstlook.planning.entity.thirdParty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class QuickPlanAd implements Serializable {

	private static final long serialVersionUID = 264473137969549494L;
	
	private int invId;
    private String adDesc;
    private double listPrice;
    private Map<String, Object> adTexts;
    private Map hasAd;
    private Integer[] advertiserIds;
    private Integer[] previousAdvertiserIds; //depends on the this object on the form being in the session.
    
    public QuickPlanAd() {
        super();
        adTexts = new HashMap<String, Object>();
        hasAd = new HashMap();
    }
    
    public QuickPlanAd(int invId, String adDesc) {
        this.invId = invId;
        this.adDesc = adDesc;
    }
    
    public String getAdDesc() {
        return adDesc;
    }
    public void setAdDesc(String adDesc) {
        this.adDesc = adDesc;
    }
    public int getInvId() {
        return invId;
    }
    public void setInvId(int invId) {
        this.invId = invId;
    }
    public double getListPrice() {
        return listPrice;
    }
    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }
    
    /**
     * The currently selected advertiserIds
     * @return
     */
    public Integer[] getAdvertiserIds() {
        return advertiserIds;
    }

    public void setAdvertiserIds(Integer[] advertiserIds) {
    	this.previousAdvertiserIds = this.advertiserIds;
        this.advertiserIds = advertiserIds;
    }
    
    /**
     * @return an array of Advertiser Ids that were deselected
     */
    public Integer[] getDeselectedAdvertiserIds() {
    	Integer[] theIds = null;
    	if(advertiserIds == null) {
    		theIds = previousAdvertiserIds;
    	} else {
    		List<Integer> deselected = new ArrayList<Integer>();
    		Set<Integer> selectedIds = new HashSet<Integer>(Arrays.asList(advertiserIds));
    		if(previousAdvertiserIds != null) {
    			for(Integer previouslySelectedId : previousAdvertiserIds) {
    				if(!selectedIds.contains(previouslySelectedId)) {
    					deselected.add(previouslySelectedId);
    				}
    			}
    			theIds = deselected.toArray(new Integer[deselected.size()]);
    		} else {
    			theIds = new Integer[0];
    		}
    	}
    	
    	return theIds;
    }

    public Map<String, Object> getAdTexts() {
        return adTexts;
    }

    public void setAdTexts(Map<String, Object> adTexts) {
        this.adTexts = adTexts;
    }
    
    public Object getAdTexts(String key) {
        return adTexts.get(key);
    }
    
    public void setAdTexts(String key, Object text) {
        this.adTexts.put(key, text);
    }
    
    public Map<String, Object> getHasAd() {
        return hasAd;
    }

    public void setHasAd(Map hasAd) {
        this.hasAd = hasAd;
    }
    
    public Object getHasAd(String key) {
        return hasAd.get(key);
    }
    
    public void setHasAd(String key, Object text) {
        this.hasAd.put(key, text);
    }
}
