package biz.firstlook.planning.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "inventory_advertising" )
public class InventoryAdvertising implements Serializable
{

/**
	 * 
	 */
	private static final long serialVersionUID = 22440259928643430L;
@Id
private Integer inventoryId;
private String description;
private Double adPrice;

public InventoryAdvertising()
{
	super();
}

public Integer getInventoryId()
{
	return inventoryId;
}

public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}



public Double getAdPrice()
{
	return adPrice;
}



public void setAdPrice( Double adPrice )
{
	this.adPrice = adPrice;
}
}
