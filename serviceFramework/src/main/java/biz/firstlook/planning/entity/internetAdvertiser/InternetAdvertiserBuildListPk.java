package biz.firstlook.planning.entity.internetAdvertiser;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InternetAdvertiserBuildListPk implements Serializable {

    private static final long serialVersionUID = 6404893225050178398L;
    
    @Column(name="InternetAdvertiserID")
    private Integer advertiserId;
    private Integer inventoryId;
    
    public InternetAdvertiserBuildListPk() {
    }
    
    public InternetAdvertiserBuildListPk(Integer advertiserId, Integer inventoryId) {
        this.advertiserId = advertiserId;
        this.inventoryId = inventoryId;
    }

    public Integer getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(Integer advertiserId) {
        this.advertiserId = advertiserId;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof InternetAdvertiserBuildListPk)) {
            return false;
        }
        final InternetAdvertiserBuildListPk pk = (InternetAdvertiserBuildListPk) o;
        if (!(advertiserId.equals(pk.getInventoryId()))) {
            return false;
        }
        if (!(inventoryId.equals(pk.getAdvertiserId()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return advertiserId.hashCode();
    }

}
