package biz.firstlook.planning.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InventoryEventTypeDefaultPK implements Serializable
{
/**
	 * 
	 */
	private static final long serialVersionUID = 6546109429154232662L;

private Integer inventoryId;

@Column( name="AIP_EventTypeID" )
private Integer eventTypeId;

public InventoryEventTypeDefaultPK()
{
	super();
}

public Integer getInventoryId()
{
	return inventoryId;
}
public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}
public Integer getEventTypeId()
{
	return eventTypeId;
}
public void setEventTypeId( Integer eventTypeId )
{
	this.eventTypeId = eventTypeId;
}

public boolean equals(Object o)
{
    if ( o instanceof InventoryEventTypeDefaultPK )
    {
        InventoryEventTypeDefaultPK pk = (InventoryEventTypeDefaultPK) o;
    	if ( pk.getInventoryId().equals( this.getInventoryId() ) 
    		 && pk.getEventTypeId().equals( this.getEventTypeId() ) )
        {
            return true;
        }    	
    }
	 
    return false;
}

public int hashCode()
{
    int hash = this.getInventoryId() >> 4;
    hash += this.getEventTypeId();
    return hash;
}

}
