package biz.firstlook.planning.entity.internetAdvertiser;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InternetAdvertiserBusinessUnitPK implements Serializable{

    private static final long serialVersionUID = 3850157933462649409L;
    
    @Column(name="InternetAdvertiserId")
    private Integer advertiserId;
    private Integer businessUnitId;
    public Integer getAdvertiserId() {
        return advertiserId;
    }
    public void setAdvertiserId(Integer advertiserId) {
        this.advertiserId = advertiserId;
    }
    public Integer getBusinessUnitId() {
        return businessUnitId;
    }
    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

   

    

}
