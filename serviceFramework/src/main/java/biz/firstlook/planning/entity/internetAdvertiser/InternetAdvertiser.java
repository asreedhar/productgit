package biz.firstlook.planning.entity.internetAdvertiser;

import java.util.List;

public class InternetAdvertiser implements Comparable<InternetAdvertiser>{

    private String name;
    private Integer advertiserId;
    private Integer inventoryId;
    private List<String> history;
    private boolean sendVehicle;

    public InternetAdvertiser() {
        super();
    }

    public Integer getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(Integer advertiserId) {
        this.advertiserId = advertiserId;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getHistory() {
        return history;
    }

    public void setHistory(List<String> history) {
        this.history = history;
    }

    public String getLastSent() {
        if (this.history != null && !this.history.isEmpty()) {
            return this.history.get(0);
        }
        return null;
    }
    
    public String getHistoryAsString() {
    	if(this.history != null ) {
	    	StringBuilder historyString = new StringBuilder();
	    	for(String item : this.history) {
	    		historyString.append( item );
	    		historyString.append( ", " );
	    	}
	    	
	    	String returnString = historyString.toString();
	    	if( returnString.length() > 1) {
	    		returnString = returnString.substring( 0, returnString.length() - 2 );
	    	}
	    	return returnString;
    	}
    	return null;
    }

    public boolean isSendVehicle() {
        return sendVehicle;
    }

    public void setSendVehicle(boolean sendVehicle) {
        this.sendVehicle = sendVehicle;
    }

    public int compareTo(InternetAdvertiser advertiser) {
        return this.advertiserId.compareTo(advertiser.advertiserId);
     }
}
