package biz.firstlook.planning.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dbo.InventoryStatusCodes")
public class InventoryStatusCode
{
	
@Id
private int inventoryStatusCD;
private String shortDescription;
private String description;

public InventoryStatusCode()
{
	super();
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public int getInventoryStatusCD()
{
	return inventoryStatusCD;
}

public void setInventoryStatusCD( int inventoryStatusCD )
{
	this.inventoryStatusCD = inventoryStatusCD;
}

public String getShortDescription()
{
	return shortDescription;
}

public void setShortDescription( String shortDescription )
{
	this.shortDescription = shortDescription;
}

public String getDisplayDescription()
{
    return shortDescription + " - " + description;
}

}
