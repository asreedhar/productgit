package biz.firstlook.planning.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.service.InventoryPlanningUtilService;

@Entity
@Table(name = "AIP_Event")
public class InventoryPlanEvent implements Serializable {

	private static final long serialVersionUID = 4354313556745639890L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "AIP_EventID")
    private Integer id;
    @Version
    @Column(name = "VersionNumber")
    private Integer version;
    private Integer inventoryId;
    @Column(name = "AIP_EventTypeID")
    private Integer eventTypeId;
    @ManyToOne
    @JoinColumn(name = "AIP_EventTypeID", insertable = false, updatable = false)
    private PlanningEventType planningEventType;
    private Date beginDate;
    private Date endDate;
    private String notes;
    private String notes2;
    private String notes3;
    @ManyToOne
    @JoinColumn(name = "ThirdPartyEntityID")
    private InventoryThirdParty inventoryThirdParty;
    private Double value;
    private Date created = new Date();
    private String createdBy;
    private Date lastModified;
    private String lastModifiedBy;
    private Date userBeginDate;
    private Date userEndDate;

    
    public InventoryPlanEvent() {
        super();
    }

    public InventoryPlanEvent(Integer invId, String notes1, Integer eventTypeId) {
        super();
        this.eventTypeId = eventTypeId;
        this.inventoryId = invId;
        this.notes = notes1;
    }
    
    public InventoryPlanEvent(Integer invId, String notes2, String notes3, InventoryThirdParty thirdParty, Integer eventTypeId) {
        super();
        this.eventTypeId = eventTypeId;
        this.inventoryId = invId;
        this.notes2 = notes2;
        this.notes3 = notes3;
        this.inventoryThirdParty = thirdParty;
    }
    
    /**
     * Deep copy constructor
     * @param copy
     */
    public InventoryPlanEvent(InventoryPlanEvent copy) {
		this.inventoryId = copy.inventoryId;
		this.eventTypeId = copy.eventTypeId;
		this.planningEventType = copy.planningEventType;
		this.beginDate = copy.beginDate;
		this.endDate = copy.endDate;
		this.notes = copy.notes;
		this.notes2 = copy.notes2;
		this.notes3 = copy.notes3;
		this.inventoryThirdParty = copy.inventoryThirdParty;
		this.value = copy.value;
		this.created = copy.created;
		this.createdBy = copy.createdBy;
		this.lastModified = copy.lastModified;
		this.lastModifiedBy = copy.lastModifiedBy;
		this.userBeginDate = copy.userBeginDate;
		this.userEndDate = copy.userEndDate;
	}

	public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getNotes2() {
        return notes2;
    }

    public void setNotes2(String notes2) {
        this.notes2 = notes2;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    /***************************************************************************
     * public AIPThirdParty getThirdParty() { return thirdParty; }
     * 
     * public void setThirdParty( AIPThirdParty thirdParty ) { this.thirdParty =
     * thirdParty; }
     **************************************************************************/

    public PlanningEventType getPlanningEventType() {
        return planningEventType;
    }

    public void setPlanningEventType(PlanningEventType planningEventType) {
        this.planningEventType = planningEventType;
    }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    // Convenience method for display
    public String getObjective() {
        return PlanObjectiveEnum.getPlanObjectiveEnum(
                planningEventType.getObjectiveId()).getDescription();
    }

    /**
     * convienence method for getting the PlanningEventTypeEnum that corresponds
     * to this event's type
     * 
     * @return PlanningEventTypeEnum
     */
    public PlanningEventTypeEnum getEventTypeEnum() {
        return PlanningEventTypeEnum.getPlanEventTypeEnum(eventTypeId);
    }

    /**
     * Evaluates whether an event is current
     * 
     * @return true if the event is current, false otherwise
     */
    public boolean isCurrent() {
        Date today = DateUtils.truncate(new Date(), Calendar.DATE);
        PlanningEventTypeEnum eventTypeEnum = getEventTypeEnum();

        if (eventTypeEnum == PlanningEventTypeEnum.REPRICE) {
            if (today.compareTo(getBeginDate()) == 0) {
                return true;
            }
        } else if ((eventTypeEnum == PlanningEventTypeEnum.ADVERTISEMENT
                || eventTypeEnum == PlanningEventTypeEnum.PROMOTION
                || eventTypeEnum == PlanningEventTypeEnum.AUCTION
                || eventTypeEnum == PlanningEventTypeEnum.WHOLESALER || eventTypeEnum == PlanningEventTypeEnum.INTERNET_MARKETPLACE)
                && getEndDate() != null) {
            if (today.compareTo(getEndDate()) <= 0) {
                return true;
            }
        } else if (getEndDate() == null) {
            return true;
        }

        return false;
    }

    public InventoryThirdParty getInventoryThirdParty() {
        return inventoryThirdParty;
    }

    public void setInventoryThirdParty(InventoryThirdParty thirdParty) {
        this.inventoryThirdParty = thirdParty;
    }

    public Date getUserBeginDate() {
        return userBeginDate;
    }

    public void setUserBeginDate(Date userBeginDate) {
        this.userBeginDate = userBeginDate;
    }

    public Date getUserEndDate() {
        return userEndDate;
    }

    public void setUserEndDate(Date userEndDate) {
        this.userEndDate = userEndDate;
    }

    public String getNotes3() {
        return notes3;
    }

    public void setNotes3(String notes3) {
        this.notes3 = notes3;
    }

    /**
     * this method returns the InventoryPlanEvent as a JSON string - hurray for
     * OOP
     * 
     * @throws JSONException
     */
    public JSONObject JSONify() throws JSONException {
        JSONObject jsonStrategy = new JSONObject();
        jsonStrategy.put("id", id);
        PlanningEventTypeEnum eventTypeEnum = getEventTypeEnum();

        switch (eventTypeEnum) {
        case SPIFF:
            jsonStrategy.put("spiffNotes", notes);
            break;
        case PROMOTION:
            jsonStrategy.put("beginDate", InventoryPlanningUtilService
                    .convertDateToString(beginDate));
            jsonStrategy.put("endDate", InventoryPlanningUtilService
                    .convertDateToString(endDate));
            jsonStrategy.put("plannedDate", InventoryPlanningUtilService
                    .convertDateToString(created)); // not sure why this
                                                            // is here - DW
            jsonStrategy.put("strategyNotes", notes);
            break;
        case ADVERTISEMENT:
            jsonStrategy.put("thirdParty",
                    (inventoryThirdParty != null) ? inventoryThirdParty
                            .getName() : notes2);
            jsonStrategy.put("plannedDate", InventoryPlanningUtilService
                    .convertDateToString(created));
            jsonStrategy.put("planUntilDate", InventoryPlanningUtilService
                    .convertDateToString(endDate));
            jsonStrategy.put("showPrice", new Boolean(notes3));
            if(notes2==null)
            {
            	jsonStrategy.put("otherPrintAdvertiserName", "");
            }
            else
            {
            	jsonStrategy.put("otherPrintAdvertiserName", notes2);
              }
            jsonStrategy.put("selectedPrintAdvertiserId",
                    (inventoryThirdParty != null) ? inventoryThirdParty.getId()
                            : "other");
            jsonStrategy.put("adPriceSelected",
                    ((value == null || value != 1d) ? Boolean.FALSE
                            : Boolean.TRUE));
            if(notes==null)
            {
            	jsonStrategy.put("adText", "");
          }
           else
           {
            jsonStrategy.put("adText", notes);
           }
            break;
        case OTHER_RETAIL:
            jsonStrategy.put("retailOtherNotes", notes);
            break;
        case AUCTION:
            jsonStrategy.put("auctionName",
                    (inventoryThirdParty != null) ? inventoryThirdParty
                            .getName() : notes3);
            jsonStrategy.put("thirdPartyId",
                    (inventoryThirdParty != null) ? inventoryThirdParty.getId()
                            : "other");
            jsonStrategy.put("nameOther", notes3);
            jsonStrategy.put("location", notes2);
            jsonStrategy.put("endDate", InventoryPlanningUtilService
                    .convertDateToString(endDate));
            jsonStrategy.put("plannedDate", InventoryPlanningUtilService
                    .convertDateToString(created));
            jsonStrategy.put("strategyNotes", notes);
            break;
        case WHOLESALER:
            jsonStrategy.put("wholesalerName",
                    (inventoryThirdParty != null) ? inventoryThirdParty
                            .getName() : notes3);
            jsonStrategy.put("thirdPartyId",
                    (inventoryThirdParty != null) ? inventoryThirdParty.getId()
                            : "other");
            jsonStrategy.put("nameOther", notes3);
            jsonStrategy.put("endDate", InventoryPlanningUtilService
                    .convertDateToString(endDate));
            jsonStrategy.put("plannedDate", InventoryPlanningUtilService
                    .convertDateToString(created));
            jsonStrategy.put("strategyNotes", notes);
            break;
        // consolidate retail and wholesale objectives for 'sold' into one
        // JSONArray
        case RETAIL:
        	jsonStrategy.put("soldAs", "retail");
            jsonStrategy.put("beginDate", InventoryPlanningUtilService
                    .convertDateToString(beginDate));
            jsonStrategy.put("name", notes2);
            jsonStrategy.put("strategyNotes", notes);
            break;
        case WHOLESALE:
            jsonStrategy.put("soldAs", "wholesale");
            jsonStrategy.put("beginDate", InventoryPlanningUtilService
                    .convertDateToString(beginDate));
            jsonStrategy.put("name", notes2);
            jsonStrategy.put("strategyNotes", notes);
            break;
        case OTHER_WHOLESALE:
            jsonStrategy.put("wholesaleOtherNotes", notes);
            break;
        case SERVICE_DEPARTMENT:
        case DETAILER:
            jsonStrategy.put("enterDate", InventoryPlanningUtilService
                    .convertDateToString(userBeginDate));
            jsonStrategy.put("exitDate", InventoryPlanningUtilService
                    .convertDateToString(userEndDate));
        case OTHER:
            jsonStrategy.put("strategyNotes", notes);
            break;
        default:
        	jsonStrategy.put("plannedDate", InventoryPlanningUtilService
                    .convertDateToString(created));
        	break;
        }

        return jsonStrategy;
    }

    public String getJSONType() {
        PlanningEventTypeEnum eventTypeEnum = getEventTypeEnum();
        String type = eventTypeEnum.getJsonKey();
        return type;
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventTypeId == null) ? 0 : eventTypeId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((inventoryId == null) ? 0 : inventoryId.hashCode());
		result = prime
				* result
				+ ((inventoryThirdParty == null) ? 0 : inventoryThirdParty
						.hashCode());
		result = prime
				* result
				+ ((planningEventType == null) ? 0 : planningEventType
						.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InventoryPlanEvent other = (InventoryPlanEvent) obj;
		if (eventTypeId == null) {
			if (other.eventTypeId != null)
				return false;
		} else if (!eventTypeId.equals(other.eventTypeId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inventoryId == null) {
			if (other.inventoryId != null)
				return false;
		} else if (!inventoryId.equals(other.inventoryId))
			return false;
		if (inventoryThirdParty == null) {
			if (other.inventoryThirdParty != null)
				return false;
		} else if (!inventoryThirdParty.equals(other.inventoryThirdParty))
			return false;
		if (planningEventType == null) {
			if (other.planningEventType != null)
				return false;
		} else if (!planningEventType.equals(other.planningEventType))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this);
    }

}
