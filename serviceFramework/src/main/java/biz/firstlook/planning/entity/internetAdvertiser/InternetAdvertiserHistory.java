package biz.firstlook.planning.entity.internetAdvertiser;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

@Entity
@Table(name = "InternetAdvertiserBuildLog")
@SecondaryTable(name = "InternetAdvertiserBuildLogDetail", pkJoinColumns = { @PrimaryKeyJoinColumn(name = "InternetAdvertiserBuildLogID") })
public class InternetAdvertiserHistory implements Serializable {

    private static final long serialVersionUID = -7394380437467868106L;

    @Id
    @Column(name = "InternetAdvertiserBuildLogID")
    private Integer advertiserBuildLogId;
    
    @Column(name = "InternetAdvertiserID")
    private Integer advertiserId;
    
    @Column(name = "Created")
    private Timestamp created;
    
    @Column(table="InternetAdvertiserBuildLogDetail", name="InventoryID")
    private Integer inventoryId;
    
    @Column(name="ExceptionReportsSent")
    private Timestamp exceptionReportsSent;
    
    public Integer getAdvertiserBuildLogId() {
        return advertiserBuildLogId;
    }

    public void setAdvertiserBuildLogId(Integer advertiserBuildLogId) {
        this.advertiserBuildLogId = advertiserBuildLogId;
    }

    public Integer getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(Integer advertiserId) {
        this.advertiserId = advertiserId;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp createdAt) {
        this.created = createdAt;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Timestamp getExceptionReportsSent() {
        return exceptionReportsSent;
    }

    public void setExceptionReportsSent(Timestamp exceptionReportsSent) {
        this.exceptionReportsSent = exceptionReportsSent;
    }
  
}
