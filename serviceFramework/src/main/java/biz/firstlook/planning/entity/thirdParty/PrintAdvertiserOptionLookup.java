package biz.firstlook.planning.entity.thirdParty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="PrintAdvertiserTextOption")
@Inheritance(strategy=InheritanceType.JOINED)
public class PrintAdvertiserOptionLookup implements Serializable
{

@Id
@GeneratedValue( strategy = GenerationType.AUTO )
@Column( name = "PrintAdvertiserTextOptionID", insertable=false, updatable=false  )
private Integer id;

@Column(name="Description")
private String description;

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}


}
