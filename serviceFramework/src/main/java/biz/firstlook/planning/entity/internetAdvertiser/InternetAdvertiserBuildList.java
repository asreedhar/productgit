package biz.firstlook.planning.entity.internetAdvertiser;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InternetAdvertiserBuildList")
public class InternetAdvertiserBuildList implements Serializable{

    private static final long serialVersionUID = -2549776143939595143L;
    public static final int EXPORT_BY_LOAD = 1;
    public static final int FULL_EXPORT_BY_USER = 11;
    public static final int NO_EXPORT_BY_USER = 10;
    public static final int INCREMENTAL_EXPORT_BY_USER = 20;
    public static final int BOTH_EXPORT_BY_USER = 21;

    @Id
    private InternetAdvertiserBuildListPk id;
    
    @Column(name = "ExportStatusCD")
    private Integer exportStatusCode;
    
    public Integer getExportStatusCode() {
        return exportStatusCode;
    }

    public void setExportStatusCode( boolean export, boolean fullExport, boolean incrementalExport) {
    	
    	if( export )
    	{
	    	if( fullExport )
	    		this.exportStatusCode = FULL_EXPORT_BY_USER;
	    	else if( incrementalExport )
	    		this.exportStatusCode = INCREMENTAL_EXPORT_BY_USER;
	    			
	    	if( fullExport & incrementalExport )
	    		this.exportStatusCode = BOTH_EXPORT_BY_USER;
    	}
    	else
    	{
    		this.exportStatusCode = NO_EXPORT_BY_USER;
    	}
    }
    
    public void setExportStatusCode( Integer exportStatusCode ) {
        this.exportStatusCode = exportStatusCode;
    }

    public InternetAdvertiserBuildListPk getId() {
        return id;
    }

    public void setId(InternetAdvertiserBuildListPk id) {
        this.id = id;
    }
}
