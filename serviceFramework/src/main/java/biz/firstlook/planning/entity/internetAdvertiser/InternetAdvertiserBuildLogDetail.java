package biz.firstlook.planning.entity.internetAdvertiser;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="InternetAdvertiserBuildLogDetail")
public class InternetAdvertiserBuildLogDetail implements Serializable{
    
    private static final long serialVersionUID = 3821858644441331847L;
    @Id
    private Integer internetAdvertiserBuildLogId;
    private Integer inventoryId;
    private Integer listedPrice;
    private Integer SendZeroesAsNull;
    public Integer getInternetAdvertiserBuildLogId() {
        return internetAdvertiserBuildLogId;
    }
    public void setInternetAdvertiserBuildLogId(Integer internetAdvertiserBuildLogId) {
        this.internetAdvertiserBuildLogId = internetAdvertiserBuildLogId;
    }
    public Integer getInventoryId() {
        return inventoryId;
    }
    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }
    public Integer getListedPrice() {
        return listedPrice;
    }
    public void setListedPrice(Integer listedPrice) {
        this.listedPrice = listedPrice;
    }
    public Integer getSendZeroesAsNull() {
        return SendZeroesAsNull;
    }
    public void setSendZeroesAsNull(Integer sendZeroesAsNull) {
        SendZeroesAsNull = sendZeroesAsNull;
    }

}
