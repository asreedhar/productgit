package biz.firstlook.planning.entity.thirdParty;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="PrintAdvertiser_ThirdPartyEntity")
@PrimaryKeyJoinColumn(name="PrintAdvertiserID")
public class PrintAdvertiser_InventoryThirdParty extends InventoryThirdParty implements Serializable
{

private static final long serialVersionUID = -3348433361744361515L;

@Column(name="ContactEmail")
private String contactEmail;

@Column(name="FaxNumber")
private String faxNumber;

@Column(name="DisplayPrice")
private Boolean displayPrice;

@Column(name="VehicleOptionThirdPartyID")
private Integer vehicleOptionThirdPartyId;

@OneToMany(mappedBy="thirdParty", cascade = CascadeType.ALL)
@OrderBy("displayOrder")
private List<PrintAdvertiserAdBuilderOption> adBuilderOptions;

@Column(name="QuickAdvertiser")
private Boolean quickAdvertiser;

public String getContactEmail()
{
	return contactEmail;
}

public void setContactEmail( String contactEmail )
{
	this.contactEmail = contactEmail;
}

public Boolean getDisplayPrice()
{
	return displayPrice;
}

public void setDisplayPrice( Boolean displayPrice )
{
	this.displayPrice = displayPrice;
}

public String getFaxNumber()
{
	return faxNumber;
}

public void setFaxNumber( String faxNumber )
{
	this.faxNumber = faxNumber;
}

public Integer getVehicleOptionThirdPartyId()
{
	return vehicleOptionThirdPartyId;
}

public void setVehicleOptionThirdPartyId( Integer vehicleOptionThirdPartyId )
{
	this.vehicleOptionThirdPartyId = vehicleOptionThirdPartyId;
}

public List< PrintAdvertiserAdBuilderOption > getAdBuilderOptions()
{
	return adBuilderOptions;
}

public void setAdBuilderOptions( List< PrintAdvertiserAdBuilderOption > adBuilderOptions )
{
	this.adBuilderOptions = adBuilderOptions;
}

public Boolean getQuickAdvertiser() {
    return quickAdvertiser;
}

public void setQuickAdvertiser(Boolean quickAdvertiser) {
    this.quickAdvertiser = quickAdvertiser;
}

public boolean equals( Object obj ) {
	if ( obj == null || !(obj instanceof PrintAdvertiser_InventoryThirdParty) )
		return false;
	return this.getId().equals( ((PrintAdvertiser_InventoryThirdParty)obj).getId() );
}

}
