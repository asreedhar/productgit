package biz.firstlook.planning.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AIP_EventType")
public class PlanningEventType implements Serializable
{

private static final long serialVersionUID = -5179948888700745456L;

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="AIP_EventTypeID")
private Integer id;

private String description;

@Column(name="AIP_EventCategoryID")
private Integer objectiveId;

public PlanningEventType()
{
	super();
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getId()
{
	return id;
}

public void setId( Integer id )
{
	this.id = id;
}

public Integer getObjectiveId()
{
	return objectiveId;
}

public void setObjectiveId( Integer objectiveId )
{
	this.objectiveId = objectiveId;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
			+ ((description == null) ? 0 : description.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result
			+ ((objectiveId == null) ? 0 : objectiveId.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	PlanningEventType other = (PlanningEventType) obj;
	if (description == null) {
		if (other.description != null)
			return false;
	} else if (!description.equals(other.description))
		return false;
	if (id == null) {
		if (other.id != null)
			return false;
	} else if (!id.equals(other.id))
		return false;
	if (objectiveId == null) {
		if (other.objectiveId != null)
			return false;
	} else if (!objectiveId.equals(other.objectiveId))
		return false;
	return true;
}

}
