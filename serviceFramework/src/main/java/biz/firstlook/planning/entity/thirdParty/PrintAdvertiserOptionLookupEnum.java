package biz.firstlook.planning.entity.thirdParty;

public enum PrintAdvertiserOptionLookupEnum
{

VEHICLE_DESC(1, "Vehicle Description"),
VIN(2, "VIN" ),
STOCK_NUM(3, "Stock #" ),
COLOR(4, "Color" ),
MILEAGE(5, "Mileage" ),
OPTIONS(6, "Options" ),
AD_DESC(7, "Ad Description" );

private Integer id;
private String description;

private PrintAdvertiserOptionLookupEnum( Integer id, String description )
{
	this.id = id;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public Integer getId()
{
	return id;
}

public static PrintAdvertiserOptionLookupEnum getPrintAdvertiserOptionLookupEnumById( Integer id )
{
	switch (id )
	{
		case 1:
				return VEHICLE_DESC;
		case 2:
			return VIN;
		case 3:
			return STOCK_NUM;
		case 4:
			return COLOR;
		case 5:
			return MILEAGE;
		case 6:
			return OPTIONS;
		case 7:
			return AD_DESC ;
	}
	return null;
}

}
