package biz.firstlook.planning.util;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.MultiValueMap;

import biz.firstlook.main.enumerator.PlanningGroupByEnum;

public class ReplanningDataUtil
{

public ReplanningDataUtil()
{
	super();
}

/**
 * Used to fix ordering of age groups within the Replanning tab.
 * 
 * @see dbo.GetAgingInventoryPlanSummary.UDF, there is a better solution.
 * @param bucketKeySet
 * @return
 */
public static Object[] sortGroupingDescriptions( Set<Object> bucketKeySet, PlanningGroupByEnum groupByEnum )
{
	Object[] bucketKeys = bucketKeySet.toArray();
	Arrays.sort( bucketKeys, Collections.reverseOrder() ); // sort in reverse order
	if ( groupByEnum == PlanningGroupByEnum.LAST_PLANNED_DATE )
	{
		return bucketKeys;
	} 
	Object[] bucketTemp = new Object[bucketKeys.length]; // create a temp array
	if ( bucketKeys.length > 0 )
	{
		Object watchList = bucketKeys[bucketKeys.length - 1]; // store watchlist
		if ( Integer.parseInt( watchList.toString() ) == 1 )
		{ // check if this grouping is watchlist
			bucketTemp[0] = watchList; // set first location in temp to watchlist
			for ( int i = 1; i < bucketKeys.length; i++ )
			{
				bucketTemp[i] = bucketKeys[i - 1]; // bring in the rest of the keys
			}
		}
		else
		{
			bucketTemp = bucketKeys;
		}
		bucketKeys = null;
		watchList = null;
	}
	return bucketTemp;
}

/**
 * Sorts the keys that are used to group planning data and formats them for display
 * 
 * @param bucketKeySet
 * @return
 */
public static Map< Integer, String > formatGroupingDescriptions( Object[] bucketKeys, PlanningGroupByEnum groupByEnum,
																List< Map > rangeTabDisplayBeans ) throws ParseException
{ // nk - this is refactored but still feels ugly. We should be able to get grouping descriptions from buckets
	Map< Integer, String > formattedDescriptions = new HashMap< Integer, String >();
	if ( groupByEnum == PlanningGroupByEnum.AGE_BUCKET )
	{
		for ( int i = 0; i < rangeTabDisplayBeans.size(); i++ )
		{
			try {
				Integer rangeId = Integer.valueOf(rangeTabDisplayBeans.get( i ).get( "RangeID" ).toString());
				String description = rangeTabDisplayBeans.get( i ).get( "Description" ).toString();
				// key should all be range ids
				formattedDescriptions.put( rangeId, description );
			} catch (NumberFormatException nfe) {
				continue;
			}
		}
	}
	else if ( groupByEnum == PlanningGroupByEnum.LAST_PLANNED_DATE || groupByEnum == PlanningGroupByEnum.REMINDER_DATE )
	{
		for ( Object key : bucketKeys )
		{
			String sKey = key.toString();
			formattedDescriptions.put( Integer.valueOf(sKey), sKey );
		}
	}
	return formattedDescriptions;
}

}
