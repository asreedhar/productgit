package biz.firstlook.planning.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.MultiValueMap;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.PlanningGroupByEnum;
import biz.firstlook.main.enumerator.VehicleLightEnum;
import biz.firstlook.planning.form.InventoryItemPlanInfo;

public class InventoryPlanningUtil
{

public InventoryPlanningUtil()
{
	super();
}


public static boolean inventoryBucketRangeLightIncludesInventoryLight(Integer curBucketLights, Integer currentVehicleLight) {
	boolean matches = false;
	if (curBucketLights != 7) { // BITMASKED: 7 = 111 = Red,Yellow, Green
		VehicleLightEnum curInvLight = VehicleLightEnum.getLightEnum(currentVehicleLight);
		// curInvLight is non-bitmasked(i.e. 1=red, 2=yellow, 3=green) bc it can only be a single value
		switch (curInvLight) {
		case RED:
			if (curBucketLights == 1 || curBucketLights == 3|| curBucketLights == 5)
				matches = true;
			break;
		case YELLOW:
			if (curBucketLights == 2 || curBucketLights == 3|| curBucketLights == 6)
				matches = true;
			break;
		case GREEN:
			if (curBucketLights == 4 || curBucketLights == 5|| curBucketLights == 6)
				matches = true;
			break;
		}
	} else {
		matches = true;
	}
	return matches;
}


/**
 * Converts the list of vehicles to replan into a Map of lists, based on range id. The final Map is keyed by range id, and its values are lists
 * of vehicles with that range id.
 * 
 * Why it was grouping on date columns is beyond me. -bf.
 * 
 * @param vehiclesToReplan
 * @return Map
 */
public static Map<Object, List<InventoryItemPlanInfo>> groupReplanningDataByRange( List<InventoryItemPlanInfo> vehiclesToReplan, PlanningGroupByEnum groupByColumn )
{
	Map<Object, List<InventoryItemPlanInfo>> allVehiclesToReplan = new HashMap<Object, List<InventoryItemPlanInfo>>();

	if ( groupByColumn == PlanningGroupByEnum.NO_GROUPING )
	{
		allVehiclesToReplan.put( groupByColumn.getId(), vehiclesToReplan );
	}
	else
	{
		for ( InventoryItemPlanInfo vehicle : vehiclesToReplan )
		{
			List<InventoryItemPlanInfo> v = null;
			switch(groupByColumn) {
				case NO_GROUPING:
					//warning, this should've already been taken care of!
					break;
				case AGE_BUCKET:
					v = allVehiclesToReplan.get(vehicle.getRangeId());
					if(v == null) {
						v = new ArrayList<InventoryItemPlanInfo>();
					}
					v.add(vehicle);
					allVehiclesToReplan.put(vehicle.getRangeId(), v);				
					break;
				case LAST_PLANNED_DATE:
					v = allVehiclesToReplan.get(vehicle.getLastPlannedDate());
					if(v == null) {
						v = new ArrayList<InventoryItemPlanInfo>();
					}
					v.add(vehicle);
					allVehiclesToReplan.put(vehicle.getLastPlannedDate(), v);
					break;
				case REMINDER_DATE:
					v = allVehiclesToReplan.get(vehicle.getLastPlannedDate());
					if(v == null) {
						v = new ArrayList<InventoryItemPlanInfo>();
					}
					v.add(vehicle);
					allVehiclesToReplan.put(vehicle.getLastPlannedDate(), v);
					break;
				default:
					break;
			}
		}
	}

	return allVehiclesToReplan;
}

/**
 * Used to fix ordering of age groups within the Replanning tab.
 * 
 * @see dbo.GetAgingInventoryPlanSummary.UDF, there is a better solution.
 * @param bucketKeySet
 * @return
 */
public static Object[] sortGroupingDescriptions( Set<? extends Object> bucketKeySet, PlanningGroupByEnum groupByEnum )
{
	Object[] bucketKeys = bucketKeySet.toArray();

	Arrays.sort( bucketKeys, Collections.reverseOrder() ); // sort in reverse order

	if ( groupByEnum == PlanningGroupByEnum.LAST_PLANNED_DATE )
	{
		return bucketKeys;
	} 
	
	Object[] bucketTemp = new Object[bucketKeys.length]; // create a temp array

	if ( bucketKeys.length > 0 )
	{

		Object watchList = bucketKeys[bucketKeys.length - 1]; // store watchlist

		if ( Integer.parseInt( watchList.toString() ) == 1 )
		{ // check if this grouping is watchlist

			bucketTemp[0] = watchList; // set first location in temp to watchlist

			for ( int i = 1; i < bucketKeys.length; i++ )
			{

				bucketTemp[i] = bucketKeys[i - 1]; // bring in the rest of the keys

			}

		}
		else
		{

			bucketTemp = bucketKeys;

		}

		bucketKeys = null;
		watchList = null;
	}
	return bucketTemp;

}

/**
 * Sorts the keys that are used to group planning data and formats them for display
 * 
 * @param bucketKeySet
 * @return
 */
public static Map< Object, Object > formatGroupingDescriptions( Object[] bucketKeys, PlanningGroupByEnum groupByEnum,
																List< Map > rangeTabDisplayBeans ) throws ParseException
{ // nk - this is refactored but still feels ugly. We should be able to get grouping descriptions from buckets
	Map< Object, Object > formattedDescriptions = new HashMap< Object, Object >();
	if ( groupByEnum == PlanningGroupByEnum.AGE_BUCKET )
	{
		for ( int i = 0; i < rangeTabDisplayBeans.size(); i++ )
		{
			// key should all be range ids
			formattedDescriptions.put( rangeTabDisplayBeans.get( i ).get( "RangeID" ) + "", rangeTabDisplayBeans.get( i ).get( "Description" ) );
		}
	}
	else if ( groupByEnum == PlanningGroupByEnum.LAST_PLANNED_DATE || groupByEnum == PlanningGroupByEnum.REMINDER_DATE )
	{
		for ( Object key : bucketKeys )
		{
			formattedDescriptions.put( key, key );
		}
	}
	return formattedDescriptions;
}

//Pulling all the spiff events to the top level for quick spiff stuff so front end doesnt have to
//parse everything.
@SuppressWarnings("unchecked")
    public static List formatQuickPlanning(MultiValueMap groupedPlanningReportResults, Object[] bucketKeys) {
        List stockNumbers = new ArrayList<Integer>();
        for (Object bucketKey : bucketKeys) {
            List bucketRange = (List) groupedPlanningReportResults.get(bucketKey);
            Iterator itr = bucketRange.iterator();
            while (itr.hasNext()) {
                LinkedHashMap vehicle = (LinkedHashMap) itr.next();
                Map planEvents = (Map) vehicle.get("PlanSummary");
                List events = (List) planEvents.get("detailedStrategies");
                stockNumbers.add(vehicle.get("StockNumber"));
                Iterator eventsItr = events.iterator();
                HashMap<String, Object> event = null;
                while (eventsItr.hasNext()) {
                    HashMap planEvent = (HashMap) eventsItr.next();
                    PlanningEventTypeEnum eventType = PlanningEventTypeEnum.getPlanEventTypeEnum((Integer)planEvent.get("AIP_EventTypeID"));
                    switch (eventType) {
                    case SPIFF:
                        event = new HashMap<String, Object>();
                        event.put("spiffNotes", planEvent.get("notes"));
                        event.put("id", vehicle.get("inventoryId"));
                        vehicle.put("spiff", event);
                        break;
                    case AUCTION:
                        event = new HashMap<String, Object>();
                        event.put("thirdParty", planEvent.get("ThirdParty"));
                        Object auctionId = planEvent.get("ThirdPartyEntityId");
                        event.put("id", (auctionId != null ? (Integer) auctionId: 0));
                        vehicle.put("auction", event);
                        break;
                    case WHOLESALER:
                        event = new HashMap<String, Object>();
                        event.put("thirdParty", planEvent.get("ThirdParty"));
                        Object wholeId = planEvent.get("ThirdPartyEntityId");
                        event.put("id", (wholeId != null ? (Integer) wholeId: 0));
                        vehicle.put("wholesaler", event);
                        break;
                    default:
                        break;
                    }
                }
            }
        }
        return stockNumbers;
    }

}
