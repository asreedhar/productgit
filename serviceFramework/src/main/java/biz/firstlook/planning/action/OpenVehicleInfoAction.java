package biz.firstlook.planning.action;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.persistence.InventoryVehicleOptionsDAO;

public class OpenVehicleInfoAction extends NextGenAction
{

private InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer inventoryId = NextGenAction.getInt( request, "inventoryId" );
	
	InventoryVehicleInfoBean info = inventoryVehicleOptionsDAO.getVehicleInfo( inventoryId );
	
	request.setAttribute( "description", info.getYear().toString() + " " + info.getVehicleDescription() );
	request.setAttribute( "vin", info.getVin() );
	request.setAttribute( "stockNo", info.getStockNumber() );
	request.setAttribute( "color", info.getColor() );
	request.setAttribute( "mileage", info.getMileage() );
	
	ThirdPartyEnum thirdPartyEnum = ThirdPartyEnum.getEnumById( getNextGenSession( request ).getPrimaryGuideBookId() );
	
	List<String> primaryOptions = info.getOptions( thirdPartyEnum, true );
	request.setAttribute( "primaryOptions",primaryOptions );
	request.setAttribute( "primaryVehicleInformationSource", thirdPartyEnum.getDescription() );
	
	request.setAttribute( "primaryGuideBookDescription", info.getThirdPartyVehicleDescription( thirdPartyEnum ) );
	
	if ( getNextGenSession( request ).getSecondaryGuideBookId() != null && getNextGenSession( request ).getSecondaryGuideBookId().intValue() != 0  )
	{
		thirdPartyEnum = ThirdPartyEnum.getEnumById( getNextGenSession( request ).getSecondaryGuideBookId() );
		List<String> secondaryOptions = info.getOptions( thirdPartyEnum, true );
		request.setAttribute( "secondaryOptions", secondaryOptions );
		request.setAttribute( "secondaryVehicleInformationSource", thirdPartyEnum.getDescription() );
		
		request.setAttribute( "secondaryGuideBookDescription", info.getThirdPartyVehicleDescription( thirdPartyEnum ) );
	}
	else
	{
		request.setAttribute( "secondaryVehicleInformatinoSource", null );
		request.setAttribute( "secondaryOptions", Collections.EMPTY_LIST );
	}
	return mapping.findForward( "success" );
}

public void setInventoryVehicleOptionsDAO( InventoryVehicleOptionsDAO inventoryVehicleOptionsDAO )
{
	this.inventoryVehicleOptionsDAO = inventoryVehicleOptionsDAO;
}

}
