package biz.firstlook.planning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.Inventory;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.planning.form.InternetMarketplacesForm;
import biz.firstlook.planning.persistence.IInventoryThirdPartyDAO;
import biz.firstlook.planning.service.GMSmartAuctionService;
import biz.firstlook.planning.service.IInventoryPlanningPersistenceService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;
import biz.firstlook.thirdparty.marketplace.ThirdPartyMarketPlaceException;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.GMSmartAuctionSubmissionInformation;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

import com.discursive.cas.extend.client.CASFilter;

import biz.firstlook.planning.persistence.InventoryDestinationStatusRetriever;

public class InternetMarketplacesSubmitAction extends NextGenAction
{
private IInventoryDAO inventoryDAO;
private GMSmartAuctionService gmSmartAuctionService;
private IInventoryPlanningPersistenceService inventoryPlanningPersistenceService;
private IInventoryThirdPartyDAO inventoryThirdPartyDAO;
private InventoryDestinationStatusRetriever inventoryDestinationStatusRetriever;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	InternetMarketplacesForm theForm = (InternetMarketplacesForm)form;

	Integer memberId = getNextGenSession( request ).getMemberId();
	
	Integer inventoryId = theForm.getInventoryId();

	String results = null;
	
	// some info from client to indicate which tab we've posted from. Default to the GMAC tab being active
	Integer activeInternetMarketplace = new Integer(theForm.getActiveInternetMarketplace());
	
	if ( activeInternetMarketplace == null || activeInternetMarketplace.intValue() == InternetMarketplacesEnum.GMAC.getInternetMarketplaceId() ){
		//GMAC tab
		String announcements = "";
		if ( theForm.getInlcludeAnnouncements() )
		{
			announcements = theForm.getAnnouncements();
		}
	
		// note: this query needs to happen since we don't know the id of GMSmartAuction by default but need to put the marketplace on the 
		// 		 aip_event entity to be persisted to the DB
		Marketplace_InventoryThirdParty marketplace = inventoryThirdPartyDAO.getMarketplace_InventoryThirdPartyByName( 
		                                                                        Marketplace_InventoryThirdParty.GMSMARTAUCTION.getName() );
		try
		{
			List<String> photoUrls = null;
			
			if (theForm.getIncludePhotos())
			{
				biz.firstlook.module.core.BusinessUnit dealer = getCoreService().findBusinessUnitByID(getNextGenSession(request).getBusinessUnitId());
			    String businessUnitCode = dealer.getBusinessUnitCode();
	
			    Inventory inventory = getInventoryDAO().findById( inventoryId );
			    String vin = inventory.getVehicle().getVin();
			
			    photoUrls = InventoryPhotosWebServiceClient.getInstance().GetInventoryPhotoUrls(businessUnitCode, vin);			
			}
			
			GMSmartAuctionSubmissionInformation submission = gmSmartAuctionService.populateSubmissionInformation( inventoryId, photoUrls, memberId,
																					theForm.getIncludePhotos(), announcements, marketplace );
			results = gmSmartAuctionService.submit( submission );
		}
		catch ( ThirdPartyMarketPlaceException e )
		{
			results = "Error";
			e.printStackTrace();
		}
	
		// on a successful submission we need to make an AIP_Event
		if ( !results.equals( "Error" ) )
		{
			String userId = (String)request.getSession().getAttribute( CASFilter.CAS_FILTER_USER );
			inventoryPlanningPersistenceService.saveInternetMarketPlaceEvent( inventoryId, userId, announcements, theForm.getIncludePhotos(),marketplace );
		}
	
		response.setHeader( "GMACurl", results );
	}
	else if (activeInternetMarketplace.intValue() == InternetMarketplacesEnum.INGROUPCLOSEDAUCTION.getInternetMarketplaceId()){
		//InGroup closed auction tab

		// checkbox info to tell us if vehicle was selected/deselected.
		Boolean export = theForm.getExported();
		
		if (export != null) {
			
			if (export){
				inventoryDestinationStatusRetriever.updateOrCreateExportInformation(inventoryId, InventoryExtractDestinationEnum.OVEHENDRICK, true);
			}
			else {
				inventoryDestinationStatusRetriever.updateOrCreateExportInformation(inventoryId,  InventoryExtractDestinationEnum.OVEHENDRICK, false);
			}
		}
	}
	
	return null;
}

public void setGmSmartAuctionService( GMSmartAuctionService gmSmartAuctionService )
{
	this.gmSmartAuctionService = gmSmartAuctionService;
}

public void setInventoryPlanningPersistenceService( IInventoryPlanningPersistenceService inventoryPlanningPersistenceService )
{
	this.inventoryPlanningPersistenceService = inventoryPlanningPersistenceService;
}

public void setInventoryThirdPartyDAO( IInventoryThirdPartyDAO inventoryThirdPartyDAO )
{
	this.inventoryThirdPartyDAO = inventoryThirdPartyDAO;
}
public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public InventoryDestinationStatusRetriever getInventoryDestinationStatusRetriever() {
	return inventoryDestinationStatusRetriever;
}

public void setInventoryDestinationStatusRetriever(InventoryDestinationStatusRetriever inventoryDestinationStatusRetriever) {
	this.inventoryDestinationStatusRetriever = inventoryDestinationStatusRetriever;
}


}
