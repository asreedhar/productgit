package biz.firstlook.planning.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.QuickPlanAd;
import biz.firstlook.planning.form.InventoryPlanningForm;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.service.PrintAdvertiserService;

import com.discursive.cas.extend.client.CASFilter;

public class QuickAdvertisingSaveAction extends QuickPlanSaveAction {

    private PrintAdvertiserService printAdvertiserService;
    private IInventoryPlanEventDAO inventoryPlanEventDAO;

    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        InventoryPlanningForm theForm = (InventoryPlanningForm) form;
        List<InventoryPlanEvent> vehEvents = new ArrayList<InventoryPlanEvent>();
        Map<Integer, String> adDesc = new HashMap<Integer, String>();
        List<Integer> invIds = new ArrayList<Integer>();
        String userId = (String) request.getSession().getAttribute(CASFilter.CAS_FILTER_USER);
        
        final int advertisementEventTypeId = PlanningEventTypeEnum.ADVERTISEMENT.getId(); 
        
        Date now = DateUtils.truncate(new Date(), Calendar.DATE);
        for (QuickPlanAd ad : theForm.getQuickAdvertisers()) {
        	Integer[] advertiserIds = ad.getAdvertiserIds();
            if (advertiserIds != null) {
                for (Integer id : advertiserIds) {
                    String text = (String) ad.getAdTexts().get(id.toString());
                    String runDates = (String)theForm.getRunDates(id.toString());
                    Date endDate = getEndDate(runDates);
                    InventoryPlanEvent adEvent = new InventoryPlanEvent(ad.getInvId(), text, advertisementEventTypeId);
                    InventoryThirdParty itp = new InventoryThirdParty(id);
                    adEvent.setInventoryThirdParty(itp);
                    adEvent.setBeginDate(now);
                    adEvent.setEndDate(endDate);
                    adEvent.setUserBeginDate(now);
                    adEvent.setUserEndDate(endDate);
                    vehEvents.add(adEvent);
                    invIds.add(ad.getInvId());
                    setShowPrice(theForm, adEvent, id);
                }
                getInventoryPlanningPersistenceService().applyRules(vehEvents, now, ad.getInvId(), userId);
                buildAds(vehEvents, ad.getAdDesc());
            }
            
            //copied from above to preserve above behavior, but also add behavior to remove the InventoryThirdParty if the option was deselected.
            Integer[] deselectedIds = ad.getDeselectedAdvertiserIds();
            if(deselectedIds != null) {
            	for (Integer id : deselectedIds) {
            		final int inventoryId = ad.getInvId();
            		InventoryPlanEvent adEvent = inventoryPlanEventDAO.findByIdandEventType(inventoryId, PlanningEventTypeEnum.ADVERTISEMENT);
            		String text = (String) ad.getAdTexts().get(id.toString());
                    String runDates = (String)theForm.getRunDates(id.toString());
                    Date endDate = getEndDate(runDates);
                    adEvent.setNotes(text);
                    adEvent.setBeginDate(now);
                    adEvent.setEndDate(now);
                    adEvent.setUserBeginDate(now);
                    adEvent.setUserEndDate(endDate); //supposed to have ended, but user deselected!
                    vehEvents.add(adEvent);
                    invIds.add(inventoryId);
                    setShowPrice(theForm, adEvent, id);
            	}
            	getInventoryPlanningPersistenceService().applyRules(vehEvents, now, ad.getInvId(), userId);
            }
            
            if (ad.getAdDesc() != null && !ad.getAdDesc().equals("")) {
            	adDesc.put(ad.getInvId(), ad.getAdDesc());
            }

        }
        
        getInventoryPlanningPersistenceService().saveQuickAd(vehEvents, invIds, userId,
                PlanObjectiveEnum.RETAIL, PlanningEventTypeEnum.ADVERTISEMENT, adDesc,
                PlanningResultsModeEnum.getResultsModeEnumById(theForm.getFilterId()));
        
        String invIdList = createStringFromList(invIds);
        
        request.setAttribute("updatedVehicles", invIds.size());
        request.setAttribute("showRecent", Boolean.TRUE);
        request.setAttribute("inventoryIdsList", invIdList);
        
        return mapping.findForward("success");
    }

    /**
     * If a date is not specified for a Quick Ad, the default end date is the next Sunday
     * 
     * @param runDates
     * @return
     * @throws ParseException
     */
	private Date getEndDate(String runDates) throws ParseException {
		DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT);
		Date endDate  = null;
		if (runDates == null) {
			// next Sunday by default
			endDate =DateUtilFL.getWeekStartDate(DateUtilFL.addDaysToDate(new Date(), 7));; 
			endDate = formatter.parse(formatter.format(endDate));
		} else {
			endDate = formatter.parse((String) runDates);
		}
		return endDate;
	}

    /**
     * NK - For an InventoryPlan Event of type Advertise, if the notes3 is true, price is shown.
     * 		If it is null, it is not. 
     * 		This setting is based on the setting of the Print Advertiser the QuickAd applies to.
     * 
     * @param theForm
     * @param adEvent
     * @param id
     */
	private void setShowPrice(InventoryPlanningForm theForm,
			InventoryPlanEvent adEvent, Integer id) {
		for (PrintAdvertiser_InventoryThirdParty advertisers : theForm.getActiveAdvertisers()) {
			if (advertisers.getId().intValue() == id.intValue()) {
				if (advertisers.getDisplayPrice()) {
					adEvent.setNotes3("true");
					break;
				}
			}
		}
	}

    private void buildAds(List<InventoryPlanEvent> events, String adDescription) {
        Map<String, PrintAdvertiser_InventoryThirdParty> advertisers = new HashMap<String, PrintAdvertiser_InventoryThirdParty>();
        PrintAdvertiser_InventoryThirdParty advertiser;
        for (InventoryPlanEvent event : events) {
        	InventoryThirdParty itp = event.getInventoryThirdParty();
        	if(itp != null) {
	            Integer thirdPartyId = itp.getId();
	            if (advertisers.get(thirdPartyId) == null) {
	                advertiser = printAdvertiserService.findPrintAdvertiserInventoryThirdPartyById(thirdPartyId);
	                advertisers.put(thirdPartyId.toString(), advertiser);
	            } else {
	                advertiser = advertisers.get(thirdPartyId);
	            }
	            if (event.getNotes() == null || event.getNotes().equalsIgnoreCase("")) {
	                String[] autoBuiltAd = printAdvertiserService.buildAd(event
	                        .getInventoryId(), ThirdPartyEnum.getEnumById(advertiser
	                        .getVehicleOptionThirdPartyId()), advertiser
	                        .getAdBuilderOptions());
	                event.setNotes(autoBuiltAd[0] + adDescription + autoBuiltAd[1]);
	            }
        	}
        }
    }
    
    public void setPrintAdvertiserService(PrintAdvertiserService printAdvertiserService) {
        this.printAdvertiserService = printAdvertiserService;
    }

	public void setInventoryPlanEventDAO(
			IInventoryPlanEventDAO inventoryPlanEventDAO) {
		this.inventoryPlanEventDAO = inventoryPlanEventDAO;
	}

}
