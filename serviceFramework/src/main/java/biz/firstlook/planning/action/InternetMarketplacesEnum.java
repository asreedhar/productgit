package biz.firstlook.planning.action;

public enum InternetMarketplacesEnum {
	
	GMAC(1, "GMAC Smart Auction"), 
	INGROUPCLOSEDAUCTION(2, "In Group Closed Auction");
	
	private int internetMarketplaceId;
	private String description;
	
	InternetMarketplacesEnum( int internetMarketplaceId, String description )
	{
		this.internetMarketplaceId = internetMarketplaceId;
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}

	public int getInternetMarketplaceId()
	{
		return internetMarketplaceId;
	}

}