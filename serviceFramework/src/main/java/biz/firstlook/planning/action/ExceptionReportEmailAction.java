package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.email.ExceptionReportEmailService;

/*
 * This will send the emails for the exception report.  This is not really an ideal way to to this but
 * was the best that could be provided due to time constraints.
 * 
 * To send the reports this action needs to be hit.  And emails will be sent according to the parameters
 * 
 * Going to...
 * NextGen/SendExceptionReport.go 
 *  will send all exception reports for all business units.
 * 
 * NextGen/SendExceptionReport.go?businessUnitId=? 
 *  will send all exception reports to the given business units.
 * 
 * NextGen/SendExceptionReport.go?datafeedCode=? 
 *  will send the exception report for the to all business units for that datafeed.
 * 
 * NextGen/SendExceptionReport.go?datafeedCode=?&businessUnitId 
 *  will send the exception report for the given business unit to the given datafeed
 * 
 */
public class ExceptionReportEmailAction extends NextGenAction {
    protected ExceptionReportEmailService exceptionReportEmailService;

    @SuppressWarnings("unchecked")
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        String businessUnitId = request.getParameter("businessUnitId");
        String datafeedCode = request.getParameter("datafeedCode");
       
        //Send reports to all business units for all export markets
        if (businessUnitId == null && datafeedCode == null) {
            exceptionReportEmailService.createEmails();
        }
        //Send reports to all business units for a given export market.
        else if (businessUnitId == null && datafeedCode != null){
            exceptionReportEmailService.createEmailsForDatafeed(datafeedCode);
        }
        //Send reports to the given business unit for all export markets.
        else if (businessUnitId != null && datafeedCode == null) {
            exceptionReportEmailService.createEmailsForBusinessUnit(Integer.parseInt(businessUnitId));
        }
        //Send report for the given business unit for the given market.
        else{
            exceptionReportEmailService.createEmailsForDatafeedAndBusinessUnit(datafeedCode, Integer.parseInt(businessUnitId));
        }
        
        request.setAttribute("businessUnitId", businessUnitId);
        request.setAttribute("datafeedCode", datafeedCode);
        
        return mapping.findForward("success");
    }

    public void setExceptionReportEmailService(
            ExceptionReportEmailService exceptionReportEmailService) {
        this.exceptionReportEmailService = exceptionReportEmailService;
    }

}
