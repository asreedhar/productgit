package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.service.IInventoryPlanningPersistenceService;

import com.discursive.cas.extend.client.CASFilter;

public class QuickRepriceAction extends NextGenAction {
	
	private IInventoryPlanningPersistenceService inventoryPlanningPersistenceService;

    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	String valueStr = request.getParameter("ListPrice");
    	//migrating away from upper case names -bf
    	if(valueStr == null) {
    		valueStr = request.getParameter("listPrice");
    	}
    	Double value = null;
    	if (valueStr != null) {
    		try {
    			value = new Double(valueStr);
    		}
    		catch (NumberFormatException e) {
    			// ignore: use null checks
    		}
    	}
    	
    	final String invIdStr = request.getParameter("inventoryId");
    	Integer invId = null;
    	if (invIdStr != null) {
    		try {
    			invId = new Integer(invIdStr);
    		}
    		catch (NumberFormatException e) {
    			// ignore: use null checks
    		}
    	}
    	
    	if (invId != null && value != null) {
    		
	        String username = (String) request.getSession().getAttribute(
	                CASFilter.CAS_FILTER_USER);
	        
	        boolean confirmed = (request.getParameter("confirmed").equalsIgnoreCase("true") ? true : false);
	        inventoryPlanningPersistenceService.inlineRepriceTracking(invId, value, username, confirmed);
    	}
    	
        request.setAttribute("inventoryId", invId);
        
    	return mapping.findForward( "success" );
    }

    public void setInventoryPlanningPersistenceService( IInventoryPlanningPersistenceService inventoryPlanningPersistenceService )
    {
    	this.inventoryPlanningPersistenceService = inventoryPlanningPersistenceService;
    }

}
