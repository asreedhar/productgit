package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookupEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.service.PrintAdvertiserService;

public class PrintAdvertiserAdBuilderAction extends NextGenAction
{

private PrintAdvertiserService printAdvertiserService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer thirdPartyId = NextGenAction.getInt( request, "selectedPrintAdvertiserId" );
	Integer inventoryId = NextGenAction.getInt( request, "inventoryId" );

	PrintAdvertiser_InventoryThirdParty thirdParty = printAdvertiserService.findPrintAdvertiserInventoryThirdPartyById( thirdPartyId );
	String[] autoBuiltAd = printAdvertiserService.buildAd( inventoryId, ThirdPartyEnum.getEnumById(thirdParty.getVehicleOptionThirdPartyId()), thirdParty.getAdBuilderOptions() );

	//long start = System.currentTimeMillis();
	//This line takes the longest out of the 3 printAdvertiserService calls.
	Integer numPlannedVehicles = printAdvertiserService.retreiveNumPlannedVehiclesByAdvertiser( thirdParty );
	//System.out.println("retrieveNumPlanned took " + (System.currentTimeMillis() - start) + " ms.");

	Boolean includeAdDesc = PrintAdvertiserService.includeOptionInAd( thirdParty.getAdBuilderOptions(), PrintAdvertiserOptionLookupEnum.AD_DESC );
	Boolean showPrice = thirdParty.getDisplayPrice();
	
	response.setHeader( "firstText", autoBuiltAd[0] );
	response.setHeader( "secondText", autoBuiltAd[1] );
	response.setHeader( "showPrice", showPrice.toString() );
	response.setHeader( "includeAdDesc", includeAdDesc.toString() );
	response.setHeader( "plannedVehicles", numPlannedVehicles.toString() );

	return null;
}

public void setPrintAdvertiserService( PrintAdvertiserService printAdvertiserService )
{
	this.printAdvertiserService = printAdvertiserService;
}

}
