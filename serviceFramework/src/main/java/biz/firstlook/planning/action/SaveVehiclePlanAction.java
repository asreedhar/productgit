package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.service.IInventoryPlanningPersistenceService;

import com.discursive.cas.extend.client.CASFilter;

public class SaveVehiclePlanAction extends NextGenAction
{
	
private static final Logger logger = Logger.getLogger( SaveVehiclePlanAction.class );
	
private IInventoryPlanningPersistenceService inventoryPlanningPersistenceService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	String userId = (String)request.getSession().getAttribute( CASFilter.CAS_FILTER_USER );
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	String jsonText = request.getParameter( "json" );
	Integer inventoryId = getInventoryPlanningPersistenceService().savePlan( businessUnitId, userId, jsonText );	

    // have to write a String back; when I wrote the integer back, the
	// javascript saw it as a '?'
	
	//inventoryid null check
	if(inventoryId == null) {
		logger.warn("InventoryID was returned as null! " + request.getServletPath());
	} else {
		response.getWriter().write( inventoryId );
	}
	return mapping.findForward( "success" );
}

public IInventoryPlanningPersistenceService getInventoryPlanningPersistenceService()
{
	return inventoryPlanningPersistenceService;
}

public void setInventoryPlanningPersistenceService( IInventoryPlanningPersistenceService inventoryPlanningPersistenceService )
{
	this.inventoryPlanningPersistenceService = inventoryPlanningPersistenceService;
}

}
