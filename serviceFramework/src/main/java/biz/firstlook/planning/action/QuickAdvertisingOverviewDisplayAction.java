package biz.firstlook.planning.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;

@SuppressWarnings("unchecked")
public class QuickAdvertisingOverviewDisplayAction extends PrintAdvertisersDisplayAction{

    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List activeThirdParties = (List) request.getAttribute("activeThirdParties");
        List<HashMap> quickAdvertisers = retrievePlannedVehicles(activeThirdParties);
        
        Map dates = new HashMap();
        String rollDate = retrieveRollDate();
        for (HashMap advertiser:quickAdvertisers){
            Integer id = (Integer) advertiser.get("id");
            dates.put(id, rollDate);
        }
        
        boolean hasNoAdvertisers = (Boolean) request.getAttribute("hasNoAdvertisers");
        request.setAttribute("activeAdvertisers", activeThirdParties);
        request.setAttribute("rollDate", rollDate);
        request.setAttribute("quickAdvertiserOverview", quickAdvertisers);
        request.setAttribute("hasNoAdvertisers", hasNoAdvertisers);
        
        return mapping.findForward("success");
    }
    
    protected List<HashMap> retrievePlannedVehicles(List<PrintAdvertiser_InventoryThirdParty> thirdParties) {
        List<HashMap> advertisers = new ArrayList<HashMap>();
        HashMap thirdPartyInfo;
        for (PrintAdvertiser_InventoryThirdParty thirdParty : thirdParties) {
            int count = getPrintAdvertiserService().retreiveNumPlannedVehiclesByAdvertiser(thirdParty);
            thirdPartyInfo = new HashMap();
            thirdPartyInfo.put("vehicles", count);
            thirdPartyInfo.put("name", thirdParty.getName());
            thirdPartyInfo.put("id", thirdParty.getId());
            thirdPartyInfo.put("quickAd", thirdParty.getQuickAdvertiser());
            advertisers.add(thirdPartyInfo);
        }
        return advertisers;
    }

    //Default date will be set to the following sunday.
    private String retrieveRollDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_MONTH, cal.get(Calendar.WEEK_OF_MONTH)+1);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return dateFormatter.format(cal.getTime());
    }

}
