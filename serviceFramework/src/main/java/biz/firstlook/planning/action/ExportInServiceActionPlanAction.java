package biz.firstlook.planning.action;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.report.action.ActionPlanReport;
import biz.firstlook.report.action.ActionPlanReportSubmitAction;
import biz.firstlook.report.reportBuilders.ReportBuilder;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExportInServiceActionPlanAction extends ActionPlanReportSubmitAction {

	

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)	throws Exception {
		
	     response.setContentType("application/vnd.ms-excel");
	     response.setHeader("Content-Disposition","attachment;filename=export.xls");

		Integer daysBack = 1;
		try
		{
			daysBack = Integer.parseInt( request.getParameter( "daysBack" ) );
			if ( daysBack.intValue() < 1 )
				daysBack = 1;
		}
		catch ( NumberFormatException nfe )
		{ /* do nothing, default daysBack to 1, per Rally UC10; */ 	}

		String thirdParty =request.getParameter("thirdParty");
		String endDate =request.getParameter("endDate");
		
		ActionPlanReport reportType = ActionPlanReport.IN_SERVICE;
		Integer thirdPartyId = null;
		
		System.err.println("thirdPartyId="+thirdPartyId);
		System.err.println("timePeriodMode="+request.getParameter( "timePeriodMode" ));
		System.err.println("daysBack="+daysBack);
		System.err.println("endDate="+endDate);
		
        if (endDate != null) {
        	endDate = URLDecoder.decode(endDate, "UTF-8");
        }
        ActionPlanReportModeEnum actionPlanReportMode = ActionPlanReportModeEnum.valueOf( request.getParameter( "timePeriodMode" ) );

		
		ReportBuilder reportBuilder = buildReport(request, actionPlanReportMode, daysBack, reportType, thirdPartyId, endDate);
		List< Map > reportResults = getTransactionalReportService().getResults( reportBuilder );
		reportResults = buildNonGroupedResults(reportResults, request, actionPlanReportMode, daysBack, reportType);

		if(reportResults != null ) {
			System.err.println( reportResults );
		} 
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("In Service Action Plan");
		Row row = sheet.createRow(0);
		Cell cell;
		cell = row.createCell(0);   cell.setCellValue("Year");	
		cell = row.createCell(1);	cell.setCellValue("Make");	
		cell = row.createCell(2);	cell.setCellValue("Model");	
		cell = row.createCell(3);	cell.setCellValue("Style");	
		cell = row.createCell(4);	cell.setCellValue("Color");	
		cell = row.createCell(5);	cell.setCellValue("Age");	
		cell = row.createCell(6);	cell.setCellValue("VIN");	
		cell = row.createCell(7);	cell.setCellValue("Stock Number");	
		cell = row.createCell(8);	cell.setCellValue("Mileage");	
		cell = row.createCell(9);	cell.setCellValue("In Service Date");	
		cell = row.createCell(10);	cell.setCellValue("Out Service Date");	
		cell = row.createCell(11);	cell.setCellValue("Days in Service");	
		cell = row.createCell(12);	cell.setCellValue("Reason");
		
		int lineNo = 1;
		for( Map lineItem : reportResults ) {
			row = sheet.createRow(lineNo++);
			
			cell = row.createCell(0);  cell.setCellValue( (Integer)lineItem.get("vehicleYear") );
			cell = row.createCell(1);  cell.setCellValue( (String)lineItem.get("make") );
			cell = row.createCell(2);  cell.setCellValue( (String)lineItem.get("groupingDescription") );
			cell = row.createCell(3);  cell.setCellValue( (String)lineItem.get("doorCount") );
			cell = row.createCell(4);  cell.setCellValue( (String)lineItem.get("color"));	
			cell = row.createCell(5);  cell.setCellValue( (Integer)lineItem.get("age"));	
			cell = row.createCell(6);  cell.setCellValue( (String)lineItem.get("vin"));	
			cell = row.createCell(7);  cell.setCellValue( (String)lineItem.get("stockNumber"));	
			cell = row.createCell(8);  cell.setCellValue( (Integer)lineItem.get("mileageReceived"));	
			java.sql.Timestamp dtEntered = (java.sql.Timestamp)lineItem.get("enterDate");
			long timeInService = 0;
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			if( dtEntered != null ) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(dtEntered.getTime());
				
				cell = row.createCell(9);  cell.setCellValue(format.format( calendar.getTime() ) );
				
				Date today = new Date();
				long diff =  today.getTime() - calendar.getTime().getTime();
				timeInService = diff / (24 * 60 * 60 * 1000);
			} else {
				cell = row.createCell(9);  cell.setCellValue("" );
			}
			
			java.sql.Timestamp dtExit = (java.sql.Timestamp)lineItem.get("exitDate");
			if( dtExit != null ) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(dtExit.getTime());
				cell = row.createCell(10);  cell.setCellValue( format.format( calendar.getTime() ) );	
			} else {
				cell = row.createCell(10);  cell.setCellValue("");
			}
			if( timeInService == 0){
				cell = row.createCell(11); cell.setCellValue("N/A");
			}
			else {
				cell = row.createCell(11); cell.setCellValue(timeInService);
			}
			
			cell = row.createCell(12); cell.setCellValue( (String)lineItem.get("reason"));
		}
		System.err.println("Workbook Complete!");
		try{
	     ServletOutputStream out = response.getOutputStream();
	     workbook.write(out);
	     //out.flush();
	     out.close();
		} catch(Exception ex ) { System.err.println(ex.getLocalizedMessage() ); }
		
		System.err.println("Workbook Downloaded!");
		return null;
	}
}
