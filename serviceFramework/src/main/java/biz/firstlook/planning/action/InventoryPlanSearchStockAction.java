package biz.firstlook.planning.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.InventoryBucketRange;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.VehicleLightEnum;
import biz.firstlook.planning.form.InventoryPlanningForm;
import biz.firstlook.planning.service.IInventoryPlanningRetrievalService;
import biz.firstlook.planning.util.InventoryPlanningUtil;
@SuppressWarnings("unused") 

public class InventoryPlanSearchStockAction extends NextGenAction {
    
    private IInventoryPlanningRetrievalService inventoryPlanningRetrievalService;
    
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        InventoryPlanningForm theForm = (InventoryPlanningForm) form;
        Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
        String stockNum = request.getParameter("stockNumber");
        Inventory inv = inventoryPlanningRetrievalService.findInventoryByStockNumber(stockNum, businessUnitId);
        int age = -1;
        //see if vehicle was located
        if(inv != null) {        	
        	age = inv.getAge();
        }else { 
            response.addHeader( "rangeId", new Integer(age).toString() );
            return null;
        }
        Integer rangeId = -1;     
        List<InventoryBucketRange> buckets = inventoryPlanningRetrievalService.findBucketRange(businessUnitId);
        for (InventoryBucketRange bucket : buckets) {
        	// verify that the inventory item belongs in the current bucket based on lights
        	if(InventoryPlanningUtil.inventoryBucketRangeLightIncludesInventoryLight(bucket.getLights(), inv.getCurrentVehicleLight())){
        		// verify that the inventory item belongs in the current bucket based on age
        		Integer highRange = ((Integer)bucket.getHigh() != null ? (Integer)bucket.getHigh():999);
        		if (age  >= bucket.getLow() && age <= highRange){
        			rangeId = bucket.getRangeId();
        			break;
        		}
        	}
        }
        
        if (rangeId == -1) {
	        //If it is on the planning tab go all up in there.
	        if (inv.getPlanReminderDate() == null || inv.getPlanReminderDate().before(new Date())) {
	        	rangeId = 0;
	        }
        }
        
        //We are switching tabs so we need to reset the form.
        theForm.reset(mapping, request);
        
        response.addHeader( "rangeId", new Integer(rangeId).toString() );
        return null;
    }



	public void setInventoryPlanningRetrievalService(IInventoryPlanningRetrievalService inventoryPlanningRetrievalService) {
        this.inventoryPlanningRetrievalService = inventoryPlanningRetrievalService;
    }


    
}
