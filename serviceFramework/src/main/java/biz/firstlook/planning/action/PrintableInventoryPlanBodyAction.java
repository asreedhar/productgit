package biz.firstlook.planning.action;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.main.enumerator.PlanningGroupByEnum;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.planning.form.InventoryItemPlanInfo;
import biz.firstlook.planning.service.InventoryStatusCodeService;
import biz.firstlook.planning.service.PlanningReportAssemblerService;
import biz.firstlook.planning.util.ReplanningDataUtil;
import biz.firstlook.report.reportBuilders.aip.PlanningSummaryReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class PrintableInventoryPlanBodyAction extends NextGenAction
{
private IReportService transactionalReportService;
private PreferenceService preferenceService;
private InventoryStatusCodeService inventoryStatusCodeService;
private PlanningReportAssemblerService planningReportAssemblerService;

public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Short outputModeXML = 0;
	
	PlanningSummaryReportBuilder summaryReportBuilder = new PlanningSummaryReportBuilder(	getNextGenSession( request ).getBusinessUnitId(),
																							null,
																							InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN.getInventoryBucketId(),
																							new Date( new java.util.Date().getTime() ), 1, 0 );

	List< Map > rangeTabDisplayBeans = getTransactionalReportService().getResults( summaryReportBuilder );

	Map<Object, List<InventoryItemPlanInfo>> groupedPlanningReportResults = getPlanningReportAssemblerService().assembleReport(
																									PlanningResultsModeEnum.DETAIL,
																									PlanningGroupByEnum.AGE_BUCKET,
																									null,
																									getNextGenSession( request ).getBusinessUnitId(),
																									0,
																									InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN,
																									null, null ,false, null );

    Object[] bucketKeys = ReplanningDataUtil.sortGroupingDescriptions(groupedPlanningReportResults.keySet(), PlanningGroupByEnum.AGE_BUCKET );
    Map<Integer, String> groupByDescriptions = ReplanningDataUtil.formatGroupingDescriptions(bucketKeys, PlanningGroupByEnum.AGE_BUCKET, rangeTabDisplayBeans);


	request.setAttribute( "VehicleLevelReportBlock", groupedPlanningReportResults );
	request.setAttribute( "showLotLocationAndStatus", "false" );
	request.setAttribute( "bucketKeySet", bucketKeys );
	request.setAttribute( "groupByDescriptions", groupByDescriptions );

	return mapping.findForward( "success" );
}

public IReportService getTransactionalReportService()
{
	return transactionalReportService;
}

public void setTransactionalReportService( IReportService transactionalReportService )
{
	this.transactionalReportService = transactionalReportService;
}

public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public InventoryStatusCodeService getInventoryStatusCodeService()
{
	return inventoryStatusCodeService;
}

public void setInventoryStatusCodeService( InventoryStatusCodeService inventoryStatusCodeService )
{
	this.inventoryStatusCodeService = inventoryStatusCodeService;
}

public PlanningReportAssemblerService getPlanningReportAssemblerService()
{
	return planningReportAssemblerService;
}

public void setPlanningReportAssemblerService( PlanningReportAssemblerService agingInventoryReportAssemblerService )
{
	this.planningReportAssemblerService = agingInventoryReportAssemblerService;
}

}
