package biz.firstlook.planning.action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.service.PrintAdvertiserService;

public class PrintAdvertiserPlanDisplayAction extends NextGenAction
{

private PrintAdvertiserService printAdvertiserService;
private IInventoryPlanEventDAO inventoryPlanEventDAO;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	// inventoryId is populated off request or off a plan depending on if this opens as an edit existing plan or open new plan
	Integer inventoryId = null;
	
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	
	DynaActionForm printAdvertiserPlanDisplayForm = (DynaActionForm)form;
	
//	 sunday = 1.....saturday = 7
	int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
	Date planUntilDate = DateUtilFL.addDaysToDate( new Date(), 8 - dayOfWeek );
	
	// get info for current plan if the plan id is in the request
	if ( request.getParameter( "planId") != null )
	{
		Integer planId = NextGenAction.getInt( request, "planId");
		InventoryPlanEvent thePlan = inventoryPlanEventDAO.findById( planId );
		inventoryId = thePlan.getInventoryId();
		
		Integer numPlannedVehicles = 0;
		
		if ( thePlan.getInventoryThirdParty() != null )
		{
			printAdvertiserPlanDisplayForm.set("selectedPrintAdvertiserId", thePlan.getInventoryThirdParty().getId() );
			printAdvertiserPlanDisplayForm.set("otherPrintAdvertiserName", "" );
			numPlannedVehicles = printAdvertiserService.retreiveNumPlannedVehiclesByAdvertiser( (PrintAdvertiser_InventoryThirdParty)thePlan.getInventoryThirdParty() );
		}
		else
		{
			//printAdvertiserPlanDisplayForm.set("selectedPrintAdvertiserId", "other" );
			printAdvertiserPlanDisplayForm.set("otherPrintAdvertiserName", thePlan.getNotes2() );
		}
		printAdvertiserPlanDisplayForm.set("adText", thePlan.getNotes() );
		planUntilDate = thePlan.getEndDate();
		
		request.setAttribute( "plannedVehicles", numPlannedVehicles );
		
		printAdvertiserPlanDisplayForm.set( "showPrice", new Boolean( thePlan.getNotes3() ) );
		Double planValue = thePlan.getValue();
		if (planValue != null) {
			printAdvertiserPlanDisplayForm.set( "adPriceSelected", (planValue.equals( 1d ) ? Boolean.TRUE : Boolean.FALSE ) );
		} else {
			printAdvertiserPlanDisplayForm.set( "adPriceSelected", Boolean.FALSE );
		}
	}
	else	// populate plan with defaults
	{
		inventoryId = NextGenAction.getInt( request, "inventoryId");
		
		request.setAttribute( "plannedVehicles", new Integer(0) );
		printAdvertiserPlanDisplayForm.set( "otherPrintAdvertiserName", "" );
		printAdvertiserPlanDisplayForm.set( "showPrice", Boolean.TRUE );
		printAdvertiserPlanDisplayForm.set( "selectedPrintAdvertiserId", new Integer(0) );
		printAdvertiserPlanDisplayForm.set( "adText", "");
	}

	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	printAdvertiserPlanDisplayForm.set( "planUntilDate", sdf.format( planUntilDate ) );
	
	// third parties drop down
	List<PrintAdvertiser_InventoryThirdParty> thirdParties = printAdvertiserService.retreivePrintAdvertisers( businessUnitId );
	printAdvertiserPlanDisplayForm.set("printAdvertisers", thirdParties );

	
	// history
	Map<String, List<String>> weeksHistory = printAdvertiserService.retreivePrintAdvertisingHisotry( inventoryId );
	printAdvertiserPlanDisplayForm.set("advertisingHistory", weeksHistory );
	printAdvertiserPlanDisplayForm.set( "inventoryId", inventoryId );
	
	return mapping.findForward( "success" );
}

public void setPrintAdvertiserService( PrintAdvertiserService printAdvertiserService )
{
	this.printAdvertiserService = printAdvertiserService;
}

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}

}
