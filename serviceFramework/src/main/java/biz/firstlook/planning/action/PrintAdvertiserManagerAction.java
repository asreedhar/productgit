package biz.firstlook.planning.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserAdBuilderOption;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiserOptionLookup;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.service.PrintAdvertiserService;

public class PrintAdvertiserManagerAction extends NextGenAction
{

private PrintAdvertiserService printAdvertiserService;

@SuppressWarnings("unchecked")
@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	DynaActionForm printAdvertiserManagerForm = (DynaActionForm)form;
	
	Integer selectedPrintAdvertiser = NextGenAction.getInt( request, "selectedPrintAdvertiserId" );
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	
	// third parties drop down
	List<PrintAdvertiser_InventoryThirdParty> thirdParties = printAdvertiserService.retreivePrintAdvertisers( businessUnitId );
    int thirdPartyCount= thirdParties.size();
	printAdvertiserManagerForm.set("printAdvertisers", thirdParties );
	request.setAttribute("thirdPartyCount", thirdPartyCount);
	printAdvertiserManagerForm.set("selectedPrintAdvertiserId", selectedPrintAdvertiser );
	
	PrintAdvertiser_InventoryThirdParty thirdParty = null;
	
	// in the case where no 3rd party id was selected make fields blank
	if ( selectedPrintAdvertiser == 0 || selectedPrintAdvertiser == null )
	{
		printAdvertiserManagerForm.set("printAdvertiserName", "" );
		printAdvertiserManagerForm.set("printAdvertiserEmail", "" );
		printAdvertiserManagerForm.set("printAdvertiserFax", "" );
		printAdvertiserManagerForm.set("showPrice", Boolean.TRUE );
        printAdvertiserManagerForm.set("showQuickPlan", Boolean.FALSE);
		printAdvertiserManagerForm.set("selectedGuideBookOption", getNextGenSession( request ).getPrimaryGuideBookId() );
	}
	else
	{
		thirdParty = printAdvertiserService.findPrintAdvertiserInventoryThirdPartyById( selectedPrintAdvertiser );

		printAdvertiserManagerForm.set("printAdvertiserName", thirdParty.getName() );
		printAdvertiserManagerForm.set("printAdvertiserEmail", thirdParty.getContactEmail() );
		printAdvertiserManagerForm.set("printAdvertiserFax", thirdParty.getFaxNumber() );
		printAdvertiserManagerForm.set("showPrice", thirdParty.getDisplayPrice() );
        printAdvertiserManagerForm.set("showQuickPlan", thirdParty.getQuickAdvertiser());
		printAdvertiserManagerForm.set("selectedGuideBookOption", thirdParty.getVehicleOptionThirdPartyId() );
	
		// in case they want to delete this third party, pass forward number of vehilces planned for him
		Integer numPlannedVehicles = printAdvertiserService.retreiveNumPlannedVehiclesByAdvertiser( thirdParty );
		request.setAttribute( "plannedVehicles", numPlannedVehicles );

	}

	// in case where there are not 3rd party options selected, populate defaults
	if ( thirdParty == null || thirdParty.getAdBuilderOptions().isEmpty() )
	{
		List<PrintAdvertiserOptionLookup> optionChoices = printAdvertiserService.retrievePrintAdvertiserTextLookupOptions();
		List<PrintAdvertiserAdBuilderOption> options = printAdvertiserService.makeDefaultListOfOptions( optionChoices );
		String[] selectedOptions = printAdvertiserService.getSelectedOptionsAsArray( options );
		
		printAdvertiserManagerForm.set( "selectedAdTextOptions", selectedOptions );
		printAdvertiserManagerForm.set( "adTextOptions", options );
	}
	else
	{
		String[] selectedOptions = printAdvertiserService.getSelectedOptionsAsArray( thirdParty.getAdBuilderOptions() );	
		printAdvertiserManagerForm.set( "selectedAdTextOptions", selectedOptions );
		
		// this call is to force a this lazyily loaded class to be loaded so it can be set on the form
		List<PrintAdvertiserAdBuilderOption> newList = new ArrayList(thirdParty.getAdBuilderOptions() );
		Collections.sort( newList, new BeanComparator("displayOrder") );
		printAdvertiserManagerForm.set( "adTextOptions", newList );
	}
	
	Map<Integer, String> guideBookOptions = new LinkedHashMap<Integer, String>();
	Integer primaryGuideBookId = getNextGenSession( request ).getPrimaryGuideBookId();
	guideBookOptions.put( primaryGuideBookId, ThirdPartyEnum.getEnumById( primaryGuideBookId ).getDescription() );

	Integer secondaryGuideBookId = getNextGenSession( request ).getSecondaryGuideBookId();
	ThirdPartyEnum secondaryGuideBookThirdParty = ThirdPartyEnum.getEnumById( secondaryGuideBookId);
	if ( secondaryGuideBookThirdParty != null )
	{
		guideBookOptions.put( secondaryGuideBookId, secondaryGuideBookThirdParty.getDescription() );
	}
	guideBookOptions.put(new Integer(5), "First Look"); // the 5 maps to edmunds in the thirdParty table in DB

	printAdvertiserManagerForm.set( "guideBookOptions", guideBookOptions );
	
	return mapping.findForward( "success" );
}

public void setPrintAdvertiserService( PrintAdvertiserService printAdvertiserService )
{
	this.printAdvertiserService = printAdvertiserService;
}

}
