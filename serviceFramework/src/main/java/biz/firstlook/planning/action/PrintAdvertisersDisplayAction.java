package biz.firstlook.planning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.form.InventoryPlanningForm;
import biz.firstlook.planning.service.PrintAdvertiserService;

public class PrintAdvertisersDisplayAction extends NextGenAction{

    private PrintAdvertiserService printAdvertiserService;
    
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        InventoryPlanningForm theForm = (InventoryPlanningForm) form;
        Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
        List<PrintAdvertiser_InventoryThirdParty> printAdvertisers = printAdvertiserService.retreivePrintAdvertisers(businessUnitId);
        Integer[] activeAdvertiserIds = new Integer[theForm.getActiveAdvertisers().size()];
        for(int i = 0; i < theForm.getActiveAdvertisers().size(); i++){
            activeAdvertiserIds[i] = theForm.getActiveAdvertisers().get(i).getId();
        }        
        
        theForm.setActiveAdvertiserIds(activeAdvertiserIds);
        request.setAttribute("printAdvertisers", printAdvertisers);
        return mapping.findForward("success");
    }
    
    public void setPrintAdvertiserService(PrintAdvertiserService printAdvertiserService) {
        this.printAdvertiserService = printAdvertiserService;
    }

    public PrintAdvertiserService getPrintAdvertiserService() {
        return printAdvertiserService;
    }

}
