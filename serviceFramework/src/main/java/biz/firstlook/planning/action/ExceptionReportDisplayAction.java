package biz.firstlook.planning.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.planning.email.ExceptionReportHeader;

/**
 * Using a subclass to keep the reference implementation intact.
 * 
 * @author bfung
 * 
 */
public class ExceptionReportDisplayAction extends ExceptionReportEmailAction
{
@SuppressWarnings( "unchecked" )
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	// getting BusinessUnitId from session; too dangerous to expose on request. Jan 09, 2007
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();

	Map<ExceptionReportHeader, List> reportByDatafeed = exceptionReportEmailService.retrieveExceptionReport( businessUnitId );
	
	request.setAttribute( "groupedExceptionReport", reportByDatafeed );

	return mapping.findForward( "success" );
}
}
