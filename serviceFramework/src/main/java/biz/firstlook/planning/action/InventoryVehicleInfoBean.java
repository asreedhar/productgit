package biz.firstlook.planning.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.main.enumerator.ThirdPartyEnum;

public class InventoryVehicleInfoBean
{

private String vehicleDescription;

private String make;
private Integer year;
private String vin;
private String color;
private String stockNumber;
private Integer mileage;

private Map<ThirdPartyEnum, List<ThirdPartyVehicleOptionInfo>> thirdPartyVehicleOptionsInfo = new HashMap< ThirdPartyEnum, List<ThirdPartyVehicleOptionInfo>>();
private Map<ThirdPartyEnum, String> thirdPartyVehicleInfo = new HashMap<ThirdPartyEnum, String>();

private class ThirdPartyVehicleOptionInfo {

String description;
boolean selected;

public ThirdPartyVehicleOptionInfo( String description, boolean selected )
{
	super();
	this.description = description;
	this.selected = selected;
}

}

public void add(ThirdPartyEnum thirdPartyEnum, String vehicleDescription, String optionDescription, boolean optionSelected) {
	String description = thirdPartyVehicleInfo.get( thirdPartyEnum );
	if(description == null) {
		thirdPartyVehicleInfo.put( thirdPartyEnum, vehicleDescription );
	}
	
	List<ThirdPartyVehicleOptionInfo> options = thirdPartyVehicleOptionsInfo.get( thirdPartyEnum );
	if(options == null) {
		options = new ArrayList<ThirdPartyVehicleOptionInfo>();
	}
	options.add( new ThirdPartyVehicleOptionInfo(optionDescription, optionSelected) );
	thirdPartyVehicleOptionsInfo.put( thirdPartyEnum, options );
}

public String getThirdPartyVehicleDescription(ThirdPartyEnum thirdPartyEnum) {
	return thirdPartyVehicleInfo.get( thirdPartyEnum );
}

public List<String> getOptions(ThirdPartyEnum thirdPartyEnum, boolean selectedOnly) {
	List<ThirdPartyVehicleOptionInfo> options = thirdPartyVehicleOptionsInfo.get( thirdPartyEnum );
	if(options == null) {
		return Collections.emptyList();
	} else {
		List<String> optionsDescriptions = new ArrayList< String >(options.size());
		if(selectedOnly) {
			for(ThirdPartyVehicleOptionInfo opt : options) {
				if(opt.selected) {
					optionsDescriptions.add( opt.description );
				}
			}
		} else {
			for(ThirdPartyVehicleOptionInfo opt : options) {
				optionsDescriptions.add( opt.description );
			}
		}
		return Collections.unmodifiableList(optionsDescriptions);
	}
}


public String getVehicleDescription()
{
	return vehicleDescription;
}

public void setVehicleDescription( String vehicleDescription )
{
	this.vehicleDescription = vehicleDescription;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public String getColor()
{
	return color;
}

public void setColor( String color )
{
	this.color = color;
}

public String getStockNumber()
{
	return stockNumber;
}

public void setStockNumber( String stockNumber )
{
	this.stockNumber = stockNumber;
}

public Integer getMileage()
{
	return mileage;
}

public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}

}
