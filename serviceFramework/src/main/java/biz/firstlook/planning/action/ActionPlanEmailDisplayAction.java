package biz.firstlook.planning.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.report.action.ActionPlanReport;
import biz.firstlook.report.dao.IBasicIMTLookUpDAO;

public class ActionPlanEmailDisplayAction extends NextGenAction {

	IBasicIMTLookUpDAO basicIMTLookUpDAO;

	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DynaActionForm theForm = (DynaActionForm) form;
		Integer thirdPartyId = null;
		if (request.getParameter("thirdPartyId") != null && !request.getParameter("thirdPartyId").equalsIgnoreCase("")) {
			thirdPartyId = Integer.parseInt(request.getParameter("thirdPartyId"));
		}
		String thirdParty = request.getParameter("thirdParty");
		String endDate = request.getParameter("endDate");

		Integer memberId = getNextGenSession(request).getMemberId();
		String report = request.getParameter("reportType");
		ActionPlanReport reportType = null;
		if (report != null) {
			reportType = ActionPlanReport.valueFromDescription(report);
		}

		Map<String, Object> emailAddress = getEmailAddresses((thirdPartyId != null? thirdPartyId.toString():null),memberId, reportType);

		String subject = createSubject(reportType, emailAddress, request);

		request.setAttribute("subject", subject);
		request.setAttribute("emailAddresses", emailAddress);
		request.setAttribute("thirdPartyId", thirdPartyId);
		request.setAttribute("thirdParty", thirdParty);
		request.setAttribute("endDate", endDate);
        request.setAttribute("reportName", getReportName(reportType));
		request.setAttribute("reportType", reportType.getDescription());
		
		theForm.set("reportType", reportType.getDescription());
		theForm.set("reportName", getReportName(reportType));
		theForm.set("thirdParty", thirdParty);
		theForm.set("thirdPartyId", thirdPartyId);
		
		return mapping.findForward("success");
	}

	private Map<String, Object> getEmailAddresses(String thirdPartyId,
			Integer memberId, ActionPlanReport reportType) {
		Object[] pars = new Object[] { memberId };
		StringBuilder sb = new StringBuilder();
		sb.append(" select m.EmailAddress as memberEmail, tp.contactEmail as thirdPartyEmail, tpe.name ");
		sb.append(" from 	Member m, PrintAdvertiser_ThirdPartyEntity tp");
		sb.append(" 		join ThirdPartyEntity tpe on tpe.ThirdPartyEntityID = tp.PrintAdvertiserID");
		sb.append(" where 	m.MemberID = ?");
		if (reportType == ActionPlanReport.ADVERTISING_INDIVIDUAL_REPORT && thirdPartyId != null) {
			sb.append(" and tp.PrintAdvertiserID = ?");
			pars = new Object[] { memberId, thirdPartyId };
		}

		List<Map<String, Object>> toBeReturned = basicIMTLookUpDAO.getResults(sb.toString(), pars);
		if (toBeReturned.isEmpty()) {
			Map<String, Object> empty = new HashMap<String, Object>();
			empty.put("memberEmail", "");
			empty.put("thirdPartyEmail", "");
			return empty;
		}
		return toBeReturned.get(0);

	}

	private String createSubject(ActionPlanReport reportType,Map<String, Object> emailAddress, HttpServletRequest request) {
		StringBuilder subject = new StringBuilder();
		subject.append(getReportName(reportType));
		subject.append(" - ");
		subject.append(getNextGenSession(request).getDealerNickName());
		subject.append(" - for ");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		subject.append(sdf.format(new Date()));
		return subject.toString();
	}

	private String getReportName(ActionPlanReport reportType) {
		switch (reportType) {
		case ADVERTISING:
			return "Advertising Plan";
		case ADVERTISING_INDIVIDUAL_REPORT:
			return "Advertising Plan";
		case AUCTION_INDIVIDUAL:
		case AUCTION:
			return "Auction Plan";
		case CERTIFIED:
			return "Certified Vehicles";
		case IN_SERVICE:
			return "Vehicles in Service Department";
		case LOT_PROMOTE:
			return "Lot Promotion Plan";
		case MODEL_PROMOTION:
			return "Model Promotion Plan";
		case OTHER:
			return "Other Plans";
		case SPIFF:
			return "SPIFF Plan";
		case REPRICING:
		case REPRICING_NO_NOTES:
			return "Repricing Plan";
		case SPECIAL_FINANCE:
			return "Special Finance(List of SF vehicles)";
		case UNCHANGED:
			return "Unchanged Vehicle Status";
		case WHOLESALER_INDIVIDUAL:
		case WHOLESALER:
			return "Wholesaler Plan";
		case WHOLESALER_BIDSHEET:
			return "Wholesaler Bid Sheet";
		case INTERNET_ADVERTISEMENT:
			return "Internet Advertisement";
		}
		return null;
	}

	public void setBasicIMTLookUpDAO(IBasicIMTLookUpDAO basicIMTLookUpDAO) {
		this.basicIMTLookUpDAO = basicIMTLookUpDAO;
	}

}
