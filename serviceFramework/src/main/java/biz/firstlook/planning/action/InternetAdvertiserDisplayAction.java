package biz.firstlook.planning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.entity.internetAdvertiser.InternetAdvertiser;
import biz.firstlook.planning.service.InternetAdvertiserService;

public class InternetAdvertiserDisplayAction extends NextGenAction {

    private InternetAdvertiserService internetAdvertiserService;

    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        DynaActionForm theForm = (DynaActionForm) form;
        Integer inventoryId = Integer.parseInt(request.getParameter("inventoryId"));
        Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
        
        List<InternetAdvertiser> inventoryAdvertisers = internetAdvertiserService.getInventoryAdvertisers(inventoryId, businessUnitId);
        
        request.setAttribute( "type", "internetAdvertise" );
    	request.setAttribute( "inventoryId", inventoryId );
        theForm.set( "outlets", inventoryAdvertisers );

        return mapping.findForward("success");
    }

    public void setInternetAdvertiserService(
            InternetAdvertiserService internetAdvertiserService) {
        this.internetAdvertiserService = internetAdvertiserService;
    }
}
