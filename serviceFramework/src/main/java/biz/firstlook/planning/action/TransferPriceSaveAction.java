package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.entity.TransferPriceValidation;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.service.IInventoryPlanningPersistenceService;

public class TransferPriceSaveAction extends NextGenAction {

	private IInventoryPlanningPersistenceService inventoryPlanningPersistenceService;
	
	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Integer inventoryId = null;
		try {
			inventoryId = Integer.valueOf(request.getParameter("inventoryId"));
		} catch (NumberFormatException nfe) {
			throw new RuntimeException("InventoryId is missing from the request!");
		}
		
		Float price = Functions.nullSafeFloat(request.getParameter("transferPrice"));
		
		TransferPriceValidation validation = inventoryPlanningPersistenceService.updateTransferPrice(inventoryId, price, getMember(request));
		if(!validation.isPassed()) {
			response.addHeader("warning", Boolean.TRUE.toString());
			response.addHeader("message", validation.getMessage());
			return null;
		}
		
		return mapping.findForward("highlight");
	}
	
	public void setInventoryPlanningPersistenceService(
			IInventoryPlanningPersistenceService inventoryPlanningPersistenceService) {
		this.inventoryPlanningPersistenceService = inventoryPlanningPersistenceService;
	}
}
