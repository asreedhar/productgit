package biz.firstlook.planning.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.core.Member;
import biz.firstlook.entity.Inventory;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.planning.persistence.InventoryDestinationStatusRetriever;
import biz.firstlook.planning.service.IInventoryPlanningRetrievalService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;

public class OpenVehiclePlanAction extends NextGenAction
{

private IInventoryPlanningRetrievalService inventoryPlanningRetrievalService;
private PreferenceService preferenceService;
private IInventoryDAO inventoryDAO;
private InventoryDestinationStatusRetriever inventoryDestinationStatusRetriever;

public InventoryDestinationStatusRetriever getInventoryDestinationStatusRetriever() {
	return inventoryDestinationStatusRetriever;
}

public void setInventoryDestinationStatusRetriever(
		InventoryDestinationStatusRetriever inventoryDestinationStatusRetriever) {
	this.inventoryDestinationStatusRetriever = inventoryDestinationStatusRetriever;
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer inventoryId = null;
	Inventory inventory = null;
	if ( request.getParameter( "inventoryId" ) != null  )
	{
	    inventoryId = Integer.parseInt( request.getParameter( "inventoryId" ) );
	    inventory = getInventoryDAO().findById( inventoryId );
	}
	
	boolean dueForReplanning = Boolean.parseBoolean(request.getParameter("bDueForReplanning"));

	// weird JSTL encoding is messing things up for strings with spaces 
	request.setAttribute("Make", request.getParameter("Make").replace("+", " "));
	request.setAttribute("Model", request.getParameter("Model").replace("+", " "));
	request.setAttribute("VehicleTrim", request.getParameter("VehicleTrim").replace("+", " "));
	
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();	
	Integer primaryGuidebookFirstPreferenceId = getNextGenSession( request ).getPrimaryGuideBookFirstPreferenceId();
	
	Integer checkAppraisalHistory = ((Integer)(getPreferenceService().getPreferences( getNextGenSession( request ).getBusinessUnitId(),
	                                           PreferenceService.ReportsEnum.IMP )).get("CheckAppraisalHistoryForIMPPlanning"));
    
	String primaryGuideBookAndCategoryDesc = getThirdPartyAndCategoryDescription( primaryGuidebookFirstPreferenceId );
	Integer ageInflationForPlanning = 0;
	if (request.getParameter("dueDays") != null) {
		ageInflationForPlanning  = Integer.parseInt( request.getParameter( "dueDays" ) );
	}
	Map<String, Object> currentPlan = getInventoryPlanningRetrievalService().loadCurrentPlan( inventoryId, businessUnitId,ageInflationForPlanning,
																								(checkAppraisalHistory.equals(Integer.valueOf(1))) );
	 			
	request.setAttribute( "primaryGuideBookAndCategoryDesc", primaryGuideBookAndCategoryDesc );
	
	// maybe just send currentPlan back to the page???
	request.setAttribute( "lastRepriceDate", currentPlan.get( "lastRepriceDate" ) );
    request.setAttribute( "inventoryId",inventoryId);
	request.setAttribute( "currentObjectiveId", currentPlan.get( "currentObjectiveId") );
	request.setAttribute( "daysUntilReplanning", currentPlan.get( "daysUntilReplanning") );
	request.setAttribute( "showPlanHistory", currentPlan.get( "showPlanHistory") );
	request.setAttribute( "repriceEvents", currentPlan.get( "repriceEvents" ) );
	request.setAttribute( "bDueForReplanning", dueForReplanning);
	request.setAttribute( "marketplacePostings", currentPlan.get( "marketplacePostings" ) );
	request.setAttribute( "inventoryLight", currentPlan.get( "inventoryLight" ) );
    request.setAttribute( "hasInternetAdvertisers", getNextGenSession(request).isInternetAdvertisersEnabled());
    request.setAttribute( "adPriceValue", currentPlan.get("adPrice"));
    request.setAttribute( "transferPrice", inventory.getTransferPrice());
    request.setAttribute( "transferForRetailOnly", inventory.getTransferForRetailOnly());
    request.setAttribute( "transferForRetailOnlyEnabled", inventory.isTransferForRetailOnlyEnabled());
	response.addHeader( "X-JSON", (String) currentPlan.get( "json" ) );
	
    request.setAttribute("displayTMV", getNextGenSession(request).getHasEdmundsTmvUpgrade());

    BusinessUnit dealer = getCoreService().findBusinessUnitByID(getNextGenSession(request).getBusinessUnitId());

	
    boolean isExported = inventoryDestinationStatusRetriever.readExportInformation(inventoryId, InventoryExtractDestinationEnum.OVEHENDRICK);
    request.setAttribute( "isClosedGroupAuction", isExported );
    
    String businessUnitCode = dealer.getBusinessUnitCode();
    String vin = inventory.getVehicle().getVin();
    List<String> inventoryPhotoUrls = InventoryPhotosWebServiceClient.getInstance().GetInventoryPhotoUrls(businessUnitCode, vin);
    
    int memberId = getNextGenSession(request).getMemberId();
    Member member = getCoreService().findMemberByID(memberId);
    
    String photoMgrUrl = InventoryPhotosWebServiceClient.getInstance().GetPhotoManagerUrl(member.getLogin(), member.getFirstName(), member.getLastName(), businessUnitCode, inventory.getVehicle().getVin(), inventory.getStockNumber());
    request.setAttribute( "photoMgrUrl", photoMgrUrl);
    
    request.setAttribute("hasPhotos", !inventoryPhotoUrls.isEmpty());    
	
    return mapping.findForward( "success" );
}

/**
 * Builds a description of a third party plus a third party category (e.g. NADA Trade-In)
 * @param thirdPartyCategoryId
 * @return the description
 */
private String getThirdPartyAndCategoryDescription( Integer thirdPartyCategoryId )
{
	ThirdPartyCategoryEnum primaryThirdPartyCategory = ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId );
	String thirdPartyDescription = primaryThirdPartyCategory.getThirdPartyEnum().getDescription();
	String thirdPartyCategoryDescription = ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId ).getDescription();

	StringBuffer description = new StringBuffer();
	description.append( thirdPartyDescription );
	description.append( " " );
	description.append( thirdPartyCategoryDescription );
	
	return description.toString();
}


public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public IInventoryPlanningRetrievalService getInventoryPlanningRetrievalService()
{
	return inventoryPlanningRetrievalService;
}

public void setInventoryPlanningRetrievalService( IInventoryPlanningRetrievalService inventoryPlanningRetrievalService )
{
	this.inventoryPlanningRetrievalService = inventoryPlanningRetrievalService;
}

public IInventoryDAO getInventoryDAO() {
	return inventoryDAO;
}

public void setInventoryDAO(IInventoryDAO inventoryDAO) {
	this.inventoryDAO = inventoryDAO;
}
}
