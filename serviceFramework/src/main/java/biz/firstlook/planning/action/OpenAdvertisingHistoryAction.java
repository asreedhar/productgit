package biz.firstlook.planning.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class OpenAdvertisingHistoryAction extends PrintAdvertisersDisplayAction{

    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer inventoryId = Integer.parseInt(request.getParameter("inventoryId"));
        String stockNumber = request.getParameter("stockNumber");
        Map<String, List<String>> weeksHistory = getPrintAdvertiserService().retreivePrintAdvertisingHisotry( inventoryId );
        request.setAttribute("stockNumber", stockNumber);
        request.setAttribute("advertisingHistory", weeksHistory );
        return mapping.findForward("success");
    }

   
}
