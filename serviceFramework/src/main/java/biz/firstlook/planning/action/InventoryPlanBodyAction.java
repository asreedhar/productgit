package biz.firstlook.planning.action;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.main.enumerator.PlanningGroupByEnum;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.entity.InventoryStatusCode;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdPartyEnum;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.entity.thirdParty.QuickPlanAd;
import biz.firstlook.planning.form.InventoryItemPlanInfo;
import biz.firstlook.planning.form.InventoryItemPlanSummaryDetailStrategy;
import biz.firstlook.planning.form.InventoryItemPlanSummaryInfo;
import biz.firstlook.planning.form.InventoryPlanningForm;
import biz.firstlook.planning.service.IInventoryPlanningRetrievalService;
import biz.firstlook.planning.service.InventoryStatusCodeService;
import biz.firstlook.planning.service.PlanningReportAssemblerService;
import biz.firstlook.planning.service.PrintAdvertiserService;
import biz.firstlook.planning.util.ReplanningDataUtil;
import biz.firstlook.report.reportBuilders.aip.PlanningSummaryReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class InventoryPlanBodyAction extends NextGenAction {
    private PreferenceService preferenceService;
    private PrintAdvertiserService printAdvertiserService;
    private InventoryStatusCodeService inventoryStatusCodeService;
    private PlanningReportAssemblerService planningReportAssemblerService;
    private IInventoryPlanningRetrievalService inventoryPlanningRetrievalService;
    private IReportService transactionalReportService;

    @SuppressWarnings("unchecked")
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
        InventoryPlanningForm theForm = (InventoryPlanningForm) form;
        
        updateForm(request, theForm);
        
        // status code filter (developed for Longo) -- call before we reset the form!
        String statusCodeStr = null;
        if (getNextGenSession(request).isShowLotLocationAndStatus()) {
            statusCodeStr = determineStatusCodes(request, theForm);
        }
        
        Integer rangeId = null;
        try {
            rangeId = Integer.parseInt(request.getParameter("rangeId"));
        }catch (Exception e){}
        if (rangeId != null) {
        	if (rangeId != theForm.getRangeId()) {
        		theForm.reset(mapping,request);
        	} 
        	theForm.setTab(rangeId);
        } else {
        	rangeId = theForm.getRangeId();
        }
        
        int filterId = theForm.getFilterId();
        
        int dueDays = 0;
        if (filterId == PlanningResultsModeEnum.DUE_FOR_PLANNING.getId()) {
            dueDays = theForm.getDueDays();
            theForm.setPlannedDays(0);
        } else {
            dueDays = theForm.getPlannedDays();
            theForm.setDueDays(0);
        }
        
        PlanningResultsModeEnum resultsModeEnum;
        PlanningGroupByEnum groupByEnum = PlanningGroupByEnum.AGE_BUCKET;
        if (rangeId != null && rangeId != 0) {
            // request is for a date range tab
            resultsModeEnum = PlanningResultsModeEnum.DETAIL;
        } else {
            // request is for the replanning tab
            resultsModeEnum = PlanningResultsModeEnum.getResultsModeEnumById(filterId);
            if (request.getParameter("groupById") != null && Integer.parseInt(request.getParameter("groupById")) > 0) {
                int groupById = Integer.parseInt(request.getParameter("groupById"));
                groupByEnum = PlanningGroupByEnum.getGroupByEnumById(groupById);
                request.setAttribute("groupById", groupById);
            }
            request.setAttribute("showFilter", Boolean.TRUE);
        }
        PlanningSummaryReportBuilder summaryReportBuilder = new PlanningSummaryReportBuilder(
                        businessUnitId, statusCodeStr,
                        InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN.getInventoryBucketId(), 
                        new Date(new java.util.Date().getTime()), dueDays, filterId);
        List<Map> rangeTabDisplayBeans = transactionalReportService.getResults(summaryReportBuilder);

        if (resultsModeEnum == PlanningResultsModeEnum.DUE_FOR_PLANNING && request.getAttribute("updatedVehicles") != null) {
        	LinkedHashMap planningTab = (LinkedHashMap) rangeTabDisplayBeans.get(0);
        	Integer unitsInTab = (Integer) planningTab.get("Units");
        	unitsInTab += (Integer)request.getAttribute("updatedVehicles");
        	planningTab.put("Units", unitsInTab);
        }
        
        Integer inventoryId = null;
        Boolean showRecent = request.getAttribute("showRecent") != null ? (Boolean)request.getAttribute("showRecent") : false;
        String inventoryIdsList = request.getAttribute("inventoryIdsList") != null ? request.getAttribute("inventoryIdsList").toString() : null;
        Map<Object, List<InventoryItemPlanInfo>> groupedPlanningReportResults = planningReportAssemblerService.assembleReport(
                        resultsModeEnum, groupByEnum, (rangeId == 0 ? null:rangeId),
                        businessUnitId, dueDays,
                        InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN,
                        statusCodeStr, inventoryId, showRecent, 
                        inventoryIdsList);

        Object[] bucketKeys = ReplanningDataUtil.sortGroupingDescriptions(groupedPlanningReportResults.keySet(), groupByEnum);
        Map<Integer, String> groupByDescriptions = ReplanningDataUtil.formatGroupingDescriptions(bucketKeys, groupByEnum, rangeTabDisplayBeans);

        //age advanced filter
        List<Map> filterRangeList = new ArrayList<Map>();
        filterRangeList.addAll(rangeTabDisplayBeans);
        filterRangeList.remove(0);
        ArrayList ageOptionsList = new ArrayList();
        ageOptionsList.add(buildHashMap("-2","All"));
        ageOptionsList.addAll(filterRangeList);
        ageOptionsList.add(buildHashMap("-1","Custom")); 
        
        //risk advanced filter
        List<Map> riskOptionsList = new ArrayList<Map>();
        riskOptionsList.add(buildHashMap("-2","All"));
        riskOptionsList.add(buildHashMap("green","Green"));
        riskOptionsList.add(buildHashMap("yellow","Yellow"));
        riskOptionsList.add(buildHashMap("red","Red"));
        riskOptionsList.add(buildHashMap("-1","Custom"));
        
        //objective advanced filter
        List<Map> objectiveOptionsList = new ArrayList<Map>();
        objectiveOptionsList.add(buildHashMap("-2","All"));
        objectiveOptionsList.add(buildHashMap("retail","Retail"));
        objectiveOptionsList.add(buildHashMap("wholesale","Wholesale"));
        objectiveOptionsList.add(buildHashMap("sold","Sold"));
        objectiveOptionsList.add(buildHashMap("other","Other"));
        objectiveOptionsList.add(buildHashMap("-1","Custom"));
        
        //mileage advanced filter
        List<Map> mileageOptionsList = new ArrayList<Map>();            
        Map<String, Object> preferences = preferenceService.getPreferences(getNextGenSession(request).getBusinessUnitId(),PreferenceService.ReportsEnum.IMP);
        List<Integer> ranges = determineMileageRange(preferences);
        mileageOptionsList.add(buildHashMap("-2","All"));
        int count = 1;
        for(Integer mileageRange:ranges) {
        	 mileageOptionsList.add(buildHashMap(new Integer(count),"&lt; "+mileageRange));
        	 count++;
        }
        
        //plan type filter
        List<Map> planTypeFilterList = new ArrayList<Map>();
        planTypeFilterList.add(buildHashMap("2","Due for Replanning"));
        planTypeFilterList.add(buildHashMap("3","Planned"));
        planTypeFilterList.add(buildHashMap("0","All"));
        //due time filter
        List<Map> dueTimeFilterList = new ArrayList<Map>();
        dueTimeFilterList.add(buildHashMap("0","Today"));
        dueTimeFilterList.add(buildHashMap("3","Next 3 Days"));
        dueTimeFilterList.add(buildHashMap("5","Next 5 Days"));
        dueTimeFilterList.add(buildHashMap("7","This Week"));
        //planned time filter
        List<Map> plannedTimeFilterList = new ArrayList<Map>();
        plannedTimeFilterList.add(buildHashMap("0","Today"));
        plannedTimeFilterList.add(buildHashMap("7","Last 7 Days"));
       
    	Integer[] thirdPartyIds = theForm.getActiveAdvertiserIds();
        setQuickAdMode(request, thirdPartyIds, businessUnitId,groupedPlanningReportResults, bucketKeys, theForm);
         
        request.setAttribute("planTypeFilterList", planTypeFilterList);
        request.setAttribute("dueTimeFilterList",dueTimeFilterList);
        request.setAttribute("plannedTimeFilterList", plannedTimeFilterList);
        request.setAttribute("ageOptionsList",ageOptionsList);
        request.setAttribute("mileageOptionsList", mileageOptionsList);
        request.setAttribute("objectiveOptionsList", objectiveOptionsList);
        request.setAttribute("riskOptionsList", riskOptionsList);
        request.setAttribute("filterRangeList", filterRangeList);
        request.setAttribute("mileageRange", ranges);
        request.setAttribute("zip", preferences.get("ZipCode"));
        request.setAttribute("useLotPrice", preferences.get("UseLotPrice"));
        request.setAttribute("itemsToReplan", unitsToReplan(businessUnitId));
        request.setAttribute("VehicleLevelReportBlock",groupedPlanningReportResults);
        request.setAttribute("plannedLastWeek",getUnitsPlannedThisWeek(businessUnitId));
        request.setAttribute("groupByDescriptions", groupByDescriptions);
        request.setAttribute("dueDays", dueDays);
        request.setAttribute("filterId", filterId);
        request.setAttribute("bucketKeySet", bucketKeys);
        request.setAttribute("rangeId", theForm.getRangeId());
        request.setAttribute("quickRepriceEnabled", (rangeId == 0 ? true:false));
        request.setAttribute("hasInternetAdvertisers", getNextGenSession(request).isInternetAdvertisersEnabled());
        request.setAttribute("RangeTabDisplayBeans", rangeTabDisplayBeans);
        request.setAttribute("primaryBookName", ThirdPartyEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookId()).getDescription());
        request.setAttribute("primaryBookPreference", ThirdPartyCategoryEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookFirstPreferenceId()).getDescription());
        request.setAttribute("priceThreshold", preferences.get("RepriceThreshold"));
        
        boolean internetAdvertisersEnabled = getInternetAdvertiserService().internetAdvertisersEnabled(businessUnitId);
        
        Integer repriceConfirmation = (Integer)preferences.get("RepriceConfirmation");
        boolean confirmReprice = (repriceConfirmation == null || repriceConfirmation.equals(1)) ? true : false;
        request.setAttribute("confirmReprice", internetAdvertisersEnabled || confirmReprice);
        request.setAttribute("showLotLocationAndStatus", getNextGenSession( request).isShowLotLocationAndStatus());
        request.setAttribute("quickRepriceEnabled", (rangeId == 0 ? true:false));
        request.setAttribute("hasInternetAdvertisers", getNextGenSession(request).isInternetAdvertisersEnabled());
        request.setAttribute("quickPlannedVehicles", (request.getAttribute("updatedVehicles") != null ? (Integer)request.getAttribute("updatedVehicles"):null));

        List< InventoryThirdParty > thirdParties = inventoryPlanningRetrievalService.loadWholesalersAndAuctions(businessUnitId);
        
        List auctions = new ArrayList();
        List wholesalers = new ArrayList();
        
        retrieveWholesalersAndAuctions(thirdParties, auctions, wholesalers);
        
        
        request.setAttribute("auctions", auctions);
        request.setAttribute("wholesalers", wholesalers);
        
        request.setAttribute("hasTransferPricing", getNextGenSession(request).isIncludeTransferPricing());
        request.setAttribute("displayTMV", getNextGenSession(request).getHasEdmundsTmvUpgrade());
        
        return mapping.findForward("success");
    }

    //Passing around form values for ajax submits.
    private void updateForm(HttpServletRequest request, InventoryPlanningForm theForm) {
    	String risk = request.getParameter("risk");
    	if (risk != null && !risk.equals("")) {
    		theForm.setRisk(risk.toLowerCase());
    	}
        String objective = request.getParameter("objective");
        if (objective != null && !objective.equals("")) {
        	theForm.setObjective(objective.toLowerCase());
        }
        String mileage = request.getParameter("mileage");
        if (mileage != null && !mileage.equals("")) {
        	theForm.setMileage(mileage.toLowerCase());
        }
        String[] objectiveCustom = request.getParameterValues("objectiveCustom");
        if (objectiveCustom != null && !objectiveCustom.equals("")) {
        	theForm.setObjectiveCustom(objectiveCustom);
        }
        String[] riskCustom = request.getParameterValues("objectiveCustom");
        if (riskCustom != null && !riskCustom.equals("")) {
        	theForm.setRiskCustom(riskCustom);
        }
	}

	@SuppressWarnings("unchecked")
    private void retrieveWholesalersAndAuctions(List<InventoryThirdParty> thirdParties, List auctions, List wholesalers) {
        for (InventoryThirdParty tp:thirdParties) {
            switch (InventoryThirdPartyEnum.getEnumById(tp.getInventoryThirdPartyTypeId())) {
            case AUCTION:
                auctions.add(tp);
                break;
            case WHOLESALER:
                wholesalers.add(tp);
                break;
            default:
                break;
            }
        }
    }
    
   
    private String determineStatusCodes(HttpServletRequest request,InventoryPlanningForm theForm) {
        String statusCodeStr = request.getParameter("statusCodeStr");

        String[] statusCodeArr = theForm.getInventoryStatusCD();
        if ( StringUtils.isBlank( statusCodeStr ) && statusCodeArr != null ) {
            statusCodeStr = StringUtils.join(statusCodeArr, ",");
        } else if ( (statusCodeArr == null || statusCodeArr.length == 0) && StringUtils.isNotBlank( statusCodeStr ) ) { 
        	// if selected status codes were passed in on the url - set them on the form so it populates correctly
        	theForm.setInventoryStatusCD( StringUtils.split( statusCodeStr, ",") );
        }
        
        
        if (statusCodeStr == null || statusCodeStr.equals("")) {
            statusCodeStr = preferenceService.getMemberStatusCodePreferencesAsStr(getNextGenSession(request).getMemberId());
            String[] statusCodeArray = preferenceService.getMemberStatusCodePreferences(getNextGenSession(request).getMemberId());
            theForm.setInventoryStatusCD(statusCodeArray);
        }
        request.setAttribute("statusCodeStr", statusCodeStr );
        if (statusCodeStr.contains("-1") || statusCodeStr.trim().equals("")) {
            statusCodeStr = null;
        }
        List<InventoryStatusCode> statusCodes = inventoryStatusCodeService.retrieveAll();
        request.setAttribute("statusCodes", statusCodes);
        return statusCodeStr;
    }
    private List<Integer> determineMileageRange(Map<String, Object> preferences) {
        List<Integer> range = new ArrayList<Integer>();
        range.add(20000);
        range.add(50000);
        Integer highMileage = (Integer) preferences.get("HighMileage");
        if(highMileage != null) {
        	range.add(highMileage);
        
	        Integer excessive = (Integer) preferences.get("ExcessiveMileage");
	        if(excessive != null) {
		        if (excessive < 100000) {
		            range.add(excessive);
		            range.add(100000);
		        } else if (excessive > 100000) {
		            range.add(100000);
		            range.add(excessive);
		        } else {
		            range.add(excessive);
		        }
	        }
        }
        return range;
    }

    private int getUnitsPlannedThisWeek(Integer businessUnitId) {
        int unitsPlanned = 0;
        PlanningSummaryReportBuilder report = new PlanningSummaryReportBuilder(businessUnitId, new Date(new java.util.Date().getTime()));
        report.setResultsMode(PlanningResultsModeEnum.PLANNED.getId());
        report.setSearchDays(7);
        List<Map> results = transactionalReportService.getResults(report);
        if (results != null && !results.isEmpty()) {
            unitsPlanned = (Integer) results.get(0).get("ReplanUnits");
        }
        return unitsPlanned;
    }

    public int unitsToReplan(Integer businessUnitId) {
        int numberToReplan = 0;
        PlanningSummaryReportBuilder report = new PlanningSummaryReportBuilder(businessUnitId, new Date(new java.util.Date().getTime()));
        report.setResultsMode(PlanningResultsModeEnum.REPLANNING.getId());
        report.setSearchDays(7);
        List<Map> results = transactionalReportService.getResults(report);
        if (results != null && !results.isEmpty()) {
            numberToReplan = (Integer) results.get(0).get("ReplanUnits");
        }
        return numberToReplan;
    }
    @SuppressWarnings("unchecked") 
    private void setQuickAdMode(HttpServletRequest request,Integer[] thirdPartyIdsArray, Integer businessUnitId, Map<Object, List<InventoryItemPlanInfo>> groupedPlanningReportResults, Object[] bucketKeys, InventoryPlanningForm theForm) {
		
        // get all thirdParties that were selected
        List<PrintAdvertiser_InventoryThirdParty> thirdParties = printAdvertiserService.retreivePrintAdvertisers( businessUnitId );
        List<Integer> selectedThirdPartyIds = new ArrayList< Integer >();
        List<PrintAdvertiser_InventoryThirdParty> selectedThirdParties = new ArrayList< PrintAdvertiser_InventoryThirdParty >();
        
        if ( thirdPartyIdsArray != null && thirdPartyIdsArray.length > 0 ) {
	        selectedThirdPartyIds = Arrays.asList( thirdPartyIdsArray );
	        
	        // update the isQuickPlan flag on the advertisers
	        for ( PrintAdvertiser_InventoryThirdParty tp : thirdParties ) {
	        	if ( selectedThirdPartyIds.contains( tp.getId() )) {
	        		tp.setQuickAdvertiser( true );
	        		selectedThirdParties.add( tp );
	        	} else {
	        		tp.setQuickAdvertiser( false );
	        	}
	        }
        } else {
        	for ( PrintAdvertiser_InventoryThirdParty tp : thirdParties ) {
        		if ( tp.getQuickAdvertiser() ) {
        			selectedThirdPartyIds.add( tp.getId() );
        			selectedThirdParties.add( tp );
        		}
        	}
        }
        
        // if thirdparties is not null - save changes and set flags
        if (thirdParties != null && !thirdParties.isEmpty()) {
        	theForm.setHasNoAdvertisers(false);
        	request.setAttribute("hasNoAdvertisers", false);
        } else {
        	theForm.setHasNoAdvertisers(true);
        	request.setAttribute("hasNoAdvertisers", true);
        }
		
        Collections.sort(selectedThirdParties, new BeanComparator("name"));
        theForm.setActiveAdvertisers(selectedThirdParties);
        request.setAttribute("activeThirdParties", selectedThirdParties);
        request.setAttribute("quickAdPlanning", Boolean.TRUE);
        
        List<QuickPlanAd> ads = new ArrayList<QuickPlanAd>();
        for (int i = 0; i < bucketKeys.length; i++) {
            List<InventoryItemPlanInfo> bucket = groupedPlanningReportResults.get(bucketKeys[i]);
            for (InventoryItemPlanInfo vehicle : bucket) {
            	QuickPlanAd ad = new QuickPlanAd();
                ad.setInvId(vehicle.getInventoryId());
                ad.setAdDesc(vehicle.getAdDescription());
                InventoryItemPlanSummaryInfo planSummary = vehicle.getPlanSummary();
                List<InventoryItemPlanSummaryDetailStrategy> detailPlans = planSummary.getDetailedStrategies();
                Map<Integer, Object> activeAds = new HashMap<Integer, Object>();
                for (InventoryItemPlanSummaryDetailStrategy plan : detailPlans) {
                	if(plan.getThirdPartyEntityId() != null 
                			&& selectedThirdPartyIds.contains(plan.getThirdPartyEntityId())
                			&& plan.isActive()) {
                		activeAds.put(plan.getThirdPartyEntityId(), true);
                	}
                }
                
                Set<Integer> selectedThirdPartyEntityIds = activeAds.keySet();
                ad.setAdvertiserIds(selectedThirdPartyEntityIds.toArray(new Integer[selectedThirdPartyEntityIds.size()]));
                
                ad.setHasAd(activeAds);
                ads.add(ad);
            }
        }
              
        theForm.setQuickAdvertisers(ads);
    }
    
    private Map<String, Object> buildHashMap(Object range, String description) {
        Map<String, Object> h = new HashMap<String, Object>();
        h.put("RangeID", range);
        h.put("Description", description);
        return h;
    }

    public void setPreferenceService(PreferenceService preferenceService) {
        this.preferenceService = preferenceService;
    }

    public void setInventoryStatusCodeService(InventoryStatusCodeService inventoryStatusCodeService) {
        this.inventoryStatusCodeService = inventoryStatusCodeService;
    }

    public void setPlanningReportAssemblerService(PlanningReportAssemblerService agingInventoryReportAssemblerService) {
        this.planningReportAssemblerService = agingInventoryReportAssemblerService;
    }

    public void setPrintAdvertiserService(PrintAdvertiserService printAdvertiserService) {
        this.printAdvertiserService = printAdvertiserService;
    }

    public void setTransactionalReportService(IReportService transactionalReportService) {
        this.transactionalReportService = transactionalReportService;
    }

    public void setInventoryPlanningRetrievalService(
            IInventoryPlanningRetrievalService inventoryPlanningRetrievalService) {
        this.inventoryPlanningRetrievalService = inventoryPlanningRetrievalService;
    }

}
