package biz.firstlook.planning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.service.IInventoryPlanningPersistenceService;

import com.discursive.cas.extend.client.CASFilter;

@SuppressWarnings("unused") 
public class QuickPlanSaveAction extends NextGenAction{

    private IInventoryPlanningPersistenceService inventoryPlanningPersistenceService;
    
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
        String userId = (String)request.getSession().getAttribute( CASFilter.CAS_FILTER_USER );
        
        String json = request.getParameter("json"); 
        setFormAttributes(request);
        
        String[] events = formatJson(request.getParameter("json"));
        
        List<Integer> invIdList = inventoryPlanningPersistenceService.saveQuickPlan(userId, events, businessUnitId);
        
        String invIds = createStringFromList(invIdList);
        
        request.setAttribute("updatedVehicles", invIdList.size());
        request.setAttribute("showRecent", Boolean.TRUE);
        request.setAttribute("inventoryIdsList", invIds);
        
        return mapping.findForward("success");
    }
    
	private void setFormAttributes(HttpServletRequest request) {
    	request.setAttribute("risk",request.getParameter("risk"));
    	request.setAttribute("objective",request.getParameter("objective"));
    	request.setAttribute("mileage",request.getParameter("mileage"));
    	request.setAttribute("objectiveCustom",request.getParameter("objectiveCustom"));
    	request.setAttribute("riskCustom",request.getParameter("riskCustom"));
	}

	private String[] formatJson(String json) {
        json = json.substring(1, json.length()-1);
        String[] events = json.split("}");
        for (int i = 0; i < events.length; i++) {
            if (i!=0) { //Remove leading comma
                events[i] = events[i].substring(1, events[i].length());
            }
            events[i] += "}";
        }
        return events;
    }
	
	public String createStringFromList(List<Integer> invIdList) {
    	StringBuilder invIds = new StringBuilder();
        for (Integer invId:invIdList) {
        	invIds.append(invId.toString());
        	invIds.append(",");
        }
        String returnString = invIds.toString();
        if (returnString.endsWith(",")) {
        	returnString = returnString.substring(0, returnString.length()-1);
        }
        return invIds.toString();
	}
	
    public void setInventoryPlanningPersistenceService(IInventoryPlanningPersistenceService inventoryPlanningPersistenceService) {
        this.inventoryPlanningPersistenceService = inventoryPlanningPersistenceService;
    }

	public IInventoryPlanningPersistenceService getInventoryPlanningPersistenceService() {
		return inventoryPlanningPersistenceService;
	}

}
