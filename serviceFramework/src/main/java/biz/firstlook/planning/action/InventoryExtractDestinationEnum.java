package biz.firstlook.planning.action;

public enum InventoryExtractDestinationEnum {
	
	GMAC(1, "GMAC"), 
	EDMUNDS(2, "Edmunds"),
	AULTEC(3, "AulTec"),
	OVEHENDRICK(4,"OVE-Hendrick");
	
	private int destinationId;
	private String description;
	
	InventoryExtractDestinationEnum( int destinationId, String description )
	{
		this.destinationId = destinationId;
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}

	public int getDestinationId()
	{
		return destinationId;
	}

}