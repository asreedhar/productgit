package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonDAOs.InventoryHibernateDAO;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;

public class OpenPlanningHistoryAction extends NextGenAction
{

private IInventoryPlanEventDAO inventoryPlanEventDAO;
private InventoryHibernateDAO inventoryDAO;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{

	Integer inventoryId = null;
	if ( request.getParameter( "inventoryId" ) != null )
	{
		inventoryId = Integer.parseInt( request.getParameter( "inventoryId" ) );
		request.setAttribute( "inventoryItem", getInventoryDAO().findById( inventoryId ) ) ;
	}

	request.setAttribute( "planningHistory", getInventoryPlanEventDAO().findPlanHistory( inventoryId ) );
	request.setAttribute( "slice", "full" );
	return mapping.findForward( "success" );
}

public IInventoryPlanEventDAO getInventoryPlanEventDAO()
{
	return inventoryPlanEventDAO;
}

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}

public InventoryHibernateDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( InventoryHibernateDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}


}
