package biz.firstlook.planning.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.service.PrintAdvertiserService;

public class QuickAdvertisingDisplayAction extends InventoryPlanBodyAction {

    private PrintAdvertiserService printAdvertiserService;

    @SuppressWarnings("unchecked")
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Integer inventoryId = NextGenAction.getInt(request, "inventoryId");
        List quickAdvertisers = (List)request.getAttribute("quickAdvertisers");
        List planEvents = (List)request.getAttribute("planEvents");
        List adPlanEvents = retrieveAdPlanEvents(planEvents);
        
        return mapping.findForward("success");
    }
    
    @SuppressWarnings("unchecked")
    private List retrieveAdPlanEvents(List planEvents) {
        List adPlanEvents = new ArrayList();
        Iterator planEventsItr = planEvents.iterator();
        while (planEventsItr.hasNext()) {
            HashMap planEvent = (HashMap) planEventsItr.next();
            if (planEvent.containsKey("ThirdPartyEntityId")) {
                adPlanEvents.add(planEvent);
            }
        }
        return adPlanEvents;
    }
    
    public void setPrintAdvertiserService(PrintAdvertiserService printAdvertiserService) {
        this.printAdvertiserService = printAdvertiserService;
    }

}
