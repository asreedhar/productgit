package biz.firstlook.planning.action;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.planning.email.ActionPlanEmailService;
import biz.firstlook.report.action.ActionPlanReport;
import biz.firstlook.report.action.ActionPlanReportSubmitAction;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public class ActionPlanEmailSubmitAction extends ActionPlanReportSubmitAction {

    private ActionPlanEmailService actionPlanEmailService;

    @Override
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        DynaActionForm theForm = (DynaActionForm) form;
        Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
        Integer memberId = getNextGenSession(request).getMemberId();
        String dealerName = getNextGenSession(request).getDealerNickName();

        int daysBack = (Integer) theForm.get("daysBack");
        String subject = theForm.getString("subjectField");
        boolean sendHtml = (Boolean) theForm.get("sendHtml");
        String reportType = (String) theForm.get("reportType");
        String reportName = (String) theForm.get("reportName");
        String replyToAddress = theForm.getString("replyToField");
        Integer thirdPartyId = (Integer) theForm.get("thirdPartyId");
        String userEnteredBodyText = theForm.getString("bodyField");
        ActionPlanReportModeEnum reportMode = (ActionPlanReportModeEnum) theForm.get("reportMode");

        String endDate = request.getParameter("endDate");

        List<String> toAddresses = getToAddressFieldFromFrom(theForm);
        ActionPlanReport report = ActionPlanReport.valueFromDescription(reportType);

        ReportBuilder reportBuilder = buildReport(request, reportMode,daysBack, report, thirdPartyId, endDate);
        List<Map> reportResults = getTransactionalReportService().getResults(reportBuilder);
        Map<Map<String, Object>, List<Map>> groupedResults = buildGroupedResults(reportResults, request, reportMode, daysBack, report);
        if ( groupedResults == null ) {
        	reportResults = buildNonGroupedResults(reportResults, request, reportMode, daysBack, report);
        }
        String book1Pref1 = ThirdPartyCategoryEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookFirstPreferenceId()).getDescription();
        String book1Pref2 = "";
        if (getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId() != null && getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId() > 0) {
            book1Pref2 = ThirdPartyCategoryEnum.getEnumById(
                    getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId()).getDescription();
        }

        String primaryBook = ThirdPartyEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookId()).getDescription();

        actionPlanEmailService.createAndSendEmail(toAddresses, thirdPartyId,
                businessUnitId, memberId, subject, userEnteredBodyText,
                replyToAddress, reportType, sendHtml,
                (groupedResults != null ? groupedResults : reportResults),
                primaryBook, book1Pref1, book1Pref2, dealerName, reportName);

        return null;
    }

    private List<String> getToAddressFieldFromFrom(DynaActionForm printAdvertiserEmailForm) {
        String toField = printAdvertiserEmailForm.getString("toField");
        String[] toFieldArray = toField.split(",");
        return Arrays.asList(toFieldArray);
    }

    public void setActionPlanEmailService(ActionPlanEmailService printAdvertiserEmailService) {
        this.actionPlanEmailService = printAdvertiserEmailService;
    }

}
