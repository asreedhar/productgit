package biz.firstlook.planning.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.Inventory;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.main.enumerator.CredentialTypeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.impl.entity.Member;
import biz.firstlook.module.core.impl.entity.MemberCredential;
import biz.firstlook.module.core.impl.entity.dao.MemberDAO;
import biz.firstlook.planning.entity.InventoryPlanEvent;
import biz.firstlook.planning.form.InternetMarketplacesForm;
import biz.firstlook.planning.persistence.IInventoryPlanEventDAO;
import biz.firstlook.planning.persistence.InventoryDestinationStatusRetriever;
import biz.firstlook.planning.service.GMSmartAuctionService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;
import biz.firstlook.thirdparty.marketplace.GMSmartAuction.GMSmartAuctionStatus;
import biz.firstlook.thirdparty.marketplace.entity.Marketplace_InventoryThirdParty;

public class InternetMarketplacesDisplayAction extends NextGenAction
{

private GMSmartAuctionService gmSmartAuctionService;
private IInventoryPlanEventDAO inventoryPlanEventDAO;
private MemberDAO memberDAO;
private IInventoryDAO inventoryDAO;
private InventoryDestinationStatusRetriever inventoryDestinationStatusRetriever;


public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Member member = memberDAO.findById( getNextGenSession( request ).getMemberId() );
	Integer inventoryId = NextGenAction.getInt( request, "inventoryId" );
	InternetMarketplacesForm theForm = (InternetMarketplacesForm)form;
	theForm.setInventoryId( inventoryId );
	theForm.setAnnouncements( "" );
	theForm.setInlcludeAnnouncements( Boolean.TRUE );
	theForm.setIncludePhotos( Boolean.TRUE );
	theForm.setMarketplaceName( Marketplace_InventoryThirdParty.GMSMARTAUCTION.getName() );
	
	MemberCredential gmSmartAuctionCredential = getGMACCredential( member.getCredentials() );

	GMSmartAuctionStatus gmSmartAuctionStatus = GMSmartAuctionStatus.getDefaultNotSumbittedStatus();
	if ( gmSmartAuctionCredential != null && !StringUtils.isEmpty( gmSmartAuctionCredential.getUsername() ) )
	{
		gmSmartAuctionStatus = gmSmartAuctionService.populatedGMSmartAuctionInfo( inventoryId );
		InventoryPlanEvent lastEvent = inventoryPlanEventDAO.findByIdandEventType( inventoryId, PlanningEventTypeEnum.INTERNET_MARKETPLACE );
		if ( lastEvent != null )
		{
			theForm.setAnnouncements( lastEvent.getNotes() );
			theForm.setInlcludeAnnouncements( (StringUtils.isBlank( lastEvent.getNotes() )) ? Boolean.FALSE : Boolean.TRUE );
			theForm.setIncludePhotos( Boolean.valueOf( lastEvent.getNotes2() ) );
		}
	}

	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "gmDealer", gmSmartAuctionStatus.isGmDealer() );
	request.setAttribute( "isPosted", gmSmartAuctionStatus.isPosted() );
	request.setAttribute( "confirmationURL", gmSmartAuctionStatus.getConfirmationURL() );
	
	
    BusinessUnit dealer = getCoreService().findBusinessUnitByID(getNextGenSession(request).getBusinessUnitId());

    String businessUnitCode = dealer.getBusinessUnitCode();
    Inventory inventory = this.getInventoryDAO().findById(inventoryId);
    String vin = inventory.getVehicle().getVin();
//    List<String> inventoryPhotoUrls = InventoryPhotosWebServiceClient.getInstance().GetInventoryPhotoUrls(businessUnitCode, vin);

    String photoMgrUrl = InventoryPhotosWebServiceClient.getInstance().GetPhotoManagerUrl(member.getLogin(), member.getFirstName(), member.getLastName(), businessUnitCode, inventory.getVehicle().getVin(), inventory.getStockNumber());
    request.setAttribute( "photoMgrUrl", photoMgrUrl);
	
    // process in group closed auction
    boolean isExportAllowed = inventoryDestinationStatusRetriever.exportAllowed(dealer.getBusinessUnitID(), InventoryExtractDestinationEnum.OVEHENDRICK);
    
    if (isExportAllowed) {
    	theForm.setExported(inventoryDestinationStatusRetriever.readExportInformation(inventoryId, InventoryExtractDestinationEnum.OVEHENDRICK));
    }

    request.setAttribute("isExportAllowed", inventoryDestinationStatusRetriever.exportAllowed(dealer.getBusinessUnitID(), InventoryExtractDestinationEnum.OVEHENDRICK));
    
	return mapping.findForward( "success" );
}

private MemberCredential getGMACCredential( List< MemberCredential > credentials )
{
	for ( MemberCredential credential : credentials )
	{
		if ( credential.getCredentialTypeId() == CredentialTypeEnum.GMACSMARTAUCTION.getCredentialTypeId() )
		{
			return credential;
		}
	}
	return null;
}

public void setGmSmartAuctionService( GMSmartAuctionService gmSmartAuctionService )
{
	this.gmSmartAuctionService = gmSmartAuctionService;
}

public void setMemberDAO( MemberDAO memberDAO )
{
	this.memberDAO = memberDAO;
}

public void setInventoryPlanEventDAO( IInventoryPlanEventDAO inventoryPlanEventDAO )
{
	this.inventoryPlanEventDAO = inventoryPlanEventDAO;
}
public IInventoryDAO getInventoryDAO() {
	return inventoryDAO;
}
public void setInventoryDAO(IInventoryDAO inventoryDAO) {
	this.inventoryDAO = inventoryDAO;
}
public InventoryDestinationStatusRetriever getInventoryDestinationStatusRetriever() {
	return inventoryDestinationStatusRetriever;
}

public void setInventoryDestinationStatusRetriever(InventoryDestinationStatusRetriever inventoryDestinationStatusRetriever) {
	this.inventoryDestinationStatusRetriever = inventoryDestinationStatusRetriever;
}

}
