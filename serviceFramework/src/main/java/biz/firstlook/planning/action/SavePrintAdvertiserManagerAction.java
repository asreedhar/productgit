package biz.firstlook.planning.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.entity.thirdParty.PrintAdvertiser_InventoryThirdParty;
import biz.firstlook.planning.form.InventoryPlanningForm;
import biz.firstlook.planning.service.PrintAdvertiserService;

public class SavePrintAdvertiserManagerAction extends NextGenAction
{

private PrintAdvertiserService printAdvertiserService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	DynaActionForm printAdvertiserMangerForm = (DynaActionForm)form;
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	
	PrintAdvertiser_InventoryThirdParty thirdParty = printAdvertiserService.getThirdPartyFromForm( printAdvertiserMangerForm, businessUnitId );

	if (request.getParameter( "deleteMe" ) != null && request.getParameter( "deleteMe" ).equals( "true" ))
	{
		if ( thirdParty.getId() != null )
		{
			printAdvertiserService.deletePrintAdvertiserInventoryThirdParty( businessUnitId, thirdParty );
		}
		// after a delete you get forward to the printAdvertiserManagerAction which expects the selctedThirdPartyId on the request
		// this mapping has selectedThridPartyId=0 so that on redraw 'please select' is selected - DW - 11/20/06
		return mapping.findForward( "successfulDeletion" );
	}
	else
	{
		printAdvertiserService.savePrintAdvertiserInventoryThirdParty( thirdParty );
		
	}
	
	if ( request.getSession().getAttribute( "InventoryPlanningForm" ) != null ) {
		InventoryPlanningForm invForm = (InventoryPlanningForm)request.getSession().getAttribute( "InventoryPlanningForm" );
		List<Integer> thirdPartyIds = new ArrayList<Integer>();
		if( invForm.getActiveAdvertiserIds() != null ) {
			thirdPartyIds.addAll( Arrays.asList( invForm.getActiveAdvertiserIds() ) );
		}
		
		if ( thirdParty.getQuickAdvertiser() && !thirdPartyIds.contains( thirdParty.getId() )) {
			thirdPartyIds.add( thirdParty.getId() );
			invForm.setActiveAdvertiserIds( thirdPartyIds.toArray( new Integer[thirdPartyIds.size()] ) );
			invForm.getActiveAdvertisers().add( thirdParty );
		} else if ( !thirdParty.getQuickAdvertiser() && thirdPartyIds.contains( thirdParty.getId() ) ){
			thirdPartyIds.remove( thirdParty.getId() );
			invForm.setActiveAdvertiserIds( thirdPartyIds.toArray( new Integer[thirdPartyIds.size()] ) );
			invForm.getActiveAdvertisers().remove( thirdParty );
		}
	}
	
	return mapping.findForward( "success" );
}

public void setPrintAdvertiserService( PrintAdvertiserService printAdvertiserService )
{
	this.printAdvertiserService = printAdvertiserService;
}

}
