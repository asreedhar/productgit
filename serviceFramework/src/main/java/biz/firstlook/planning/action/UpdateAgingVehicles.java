package biz.firstlook.planning.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.actionFilter.NextGenSession;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.planning.form.InventoryItemPlanInfo;
import biz.firstlook.planning.service.PlanningReportAssemblerService;

public class UpdateAgingVehicles extends NextGenAction
{

private static final String INVENTORY_ID = "inventoryId";
private static final String RANGE_ID = "rangeId";

private PlanningReportAssemblerService planningReportAssemblerService; 
private PreferenceService preferenceService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer inventoryId = Functions.nullSafeInteger(request.getParameter(INVENTORY_ID));
	
	if (inventoryId == null  )
	{
		inventoryId = Functions.nullSafeInteger(request.getAttribute(INVENTORY_ID));
	}
	
	Integer rangeId = Functions.nullSafeInteger(request.getParameter(RANGE_ID));
	
	final NextGenSession nextGenSession = getNextGenSession(request);
	Map<String, Object> preferences = preferenceService.getPreferences(nextGenSession.getBusinessUnitId(),PreferenceService.ReportsEnum.IMP);
	
	if (inventoryId != null)
	{
		final Integer businessUnitId = nextGenSession.getBusinessUnitId();
		
		InventoryItemPlanInfo result = planningReportAssemblerService.runReport( businessUnitId, inventoryId );
		
		if ( result != null )
		{
			request.setAttribute("activeAds", result.getActiveAds());
			request.setAttribute( "singleInventory", result );
	        if ( StringUtils.isNotBlank( result.getExportingAdvertisers())) {
	            request.setAttribute("hasInternetAdvertisers", Boolean.TRUE);
	        }
	        if (rangeId == null) {
	        	rangeId = result.getRangeId();
	        }
		}
	}	
	
	request.setAttribute("rangeId", rangeId);
	request.setAttribute("displayTMV", nextGenSession.getHasEdmundsTmvUpgrade());
	request.setAttribute("useLotPrice", preferences.get("UseLotPrice"));
	request.setAttribute("hasTransferPricing", nextGenSession.isIncludeTransferPricing());

	
	return mapping.findForward( "success" );
}


public void setPlanningReportAssemblerService( PlanningReportAssemblerService agingInventoryReportAssemblerService )
{
	this.planningReportAssemblerService = agingInventoryReportAssemblerService;
}

public void setPreferenceService(PreferenceService preferenceService) 
{
	this.preferenceService = preferenceService;
}

}
