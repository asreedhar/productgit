package biz.firstlook.planning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.planning.service.IInventoryPlanningThirdPartyService;

public class SaveThirdPartyAction extends NextGenAction
{

private IInventoryPlanningThirdPartyService inventoryPlanningThirdPartyService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	String eventType = request.getParameter( "eventType" );
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	String jsonText = request.getParameter( "json" );
	JSONArray jsArray = getInventoryPlanningThirdPartyService().saveThirdParties( jsonText, eventType, businessUnitId );
	
	response.addHeader( "X-JSON", jsArray.toString() );

	return null;
}

public IInventoryPlanningThirdPartyService getInventoryPlanningThirdPartyService()
{
	return inventoryPlanningThirdPartyService;
}

public void setInventoryPlanningThirdPartyService( IInventoryPlanningThirdPartyService inventoryPlanningThirdPartyService )
{
	this.inventoryPlanningThirdPartyService = inventoryPlanningThirdPartyService;
}



}
