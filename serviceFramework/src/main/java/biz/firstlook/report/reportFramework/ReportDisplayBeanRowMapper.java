package biz.firstlook.report.reportFramework;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import biz.firstlook.report.postProcessorFilters.PostProcessorFilter;
import biz.firstlook.report.sql.SQLizable;

public class ReportDisplayBeanRowMapper implements ResultSetExtractor
{

private List<PostProcessorFilter> postProcessorFilters;
 
public ReportDisplayBeanRowMapper( SQLizable sqlizable )
{
	this.postProcessorFilters = sqlizable.getPostProcessorFilters();
}

public ReportDisplayBeanRowMapper( List<PostProcessorFilter> postProcessorFiltersIter )
{
	this.postProcessorFilters = postProcessorFiltersIter;
}

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	List< Map > reportDisplayBeans = new ArrayList< Map >();

	ResultSetMetaData rsMetaData = rs.getMetaData();

	int columnCount = rsMetaData.getColumnCount();

	Iterator postProcessorFiltersIter = null;
	while ( rs.next() )
	{
		Map< String, Object > rowResult = new LinkedHashMap< String, Object >();
		// 1 based for M$ SQL Server
		for ( int columnIndex = 1; columnIndex <= columnCount; columnIndex++ )
		{
			String name = rsMetaData.getColumnName( columnIndex );
			rowResult.put( name, rs.getObject( columnIndex ) );
		}

		postProcessorFiltersIter = postProcessorFilters.iterator();
		while( postProcessorFiltersIter.hasNext() )
		{
			PostProcessorFilter filter = (PostProcessorFilter)postProcessorFiltersIter.next();
			filter.filterResults( rowResult );
		}
		
		reportDisplayBeans.add( rowResult );
	}

	return reportDisplayBeans;
}


}
