package biz.firstlook.report.reportFramework;

import java.util.Map;

/**
 * Takes in two rows of a results set, compares the values in <i>column</i>, and returns a row with <i>column</i> replaced with the greater
 * value. If the reverseCompare flag is set to true, the object with lesser value is returned.
 * 
 * @author bfung
 * 
 */
public class CompareReplaceFlattener implements Flattener< Comparable >
{

private String columnToReplace;
private String columnForCompare;
private boolean reverseCompare;

public CompareReplaceFlattener( String columnToReplace, String columnForCompare )
{
	super();
	this.columnToReplace = columnToReplace;
	this.columnForCompare = columnForCompare;
	this.reverseCompare = false;
}

public CompareReplaceFlattener( String columnToReplace, String columnForCompare, boolean revserseCompare )
{
	super();
	this.columnToReplace = columnToReplace;
	this.columnForCompare = columnForCompare;
	this.reverseCompare = revserseCompare;
}

public Comparable doFlatten( final Map existingRow, final Map newRow )
{
	Comparable obj1 = (Comparable)existingRow.get( columnForCompare );
	Comparable obj2 = (Comparable)newRow.get( columnForCompare );

	if ( obj1.compareTo( obj2 ) >= 0 && !reverseCompare )
	{
		return (Comparable)existingRow.get( columnToReplace );
	}
	else
	{
		return (Comparable)newRow.get( columnToReplace );

	}
}

public String getColumn()
{
	return columnToReplace;
}

}
