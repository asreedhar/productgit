package biz.firstlook.report.reportFramework.planning;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import biz.firstlook.commons.util.XMLHelper;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.PlanningStatusEnum;
import biz.firstlook.report.reportFramework.ReportDisplayBeanXmlMapper;
import biz.firstlook.report.sql.SQLizable;

public class PlanningReportXmlMapper extends ReportDisplayBeanXmlMapper
{

private Logger logger = Logger.getLogger( PlanningReportXmlMapper.class );

public PlanningReportXmlMapper( SQLizable sqlizable )
{
	super( sqlizable );
}

@Override
public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	List< Map< String, Object > > reportDisplayBeans = new ArrayList< Map< String, Object > >();
	
	try
	{
		Document aipDocument = (Document)createXmlDocument( rs );
		if ( aipDocument != null )
		{
		    reportDisplayBeans = constructResults( aipDocument );
		}
	}
	catch ( Exception e )
	{
		logger.error( "An error occurred while trying to parse the results from the planning report stored procedure", e );
	}
	
	return reportDisplayBeans;
}

private List< Map< String, Object > > constructResults( Document aipDoc )
{
	List< Map< String, Object > > reportDisplayBeans = new ArrayList< Map< String, Object > >();
	Map< String, Object > result;

	Element root = aipDoc.getDocumentElement();
	List<Node> inventoryArrayItems = XMLHelper.getChildren( root, "Inventory" );

	for(Node inventoryNode : inventoryArrayItems)
	{
		result = new LinkedHashMap< String, Object >();
		createMapEntriesFromAttributes( result, inventoryNode );
		constructPlanInformation( result, inventoryNode, parseReminderDate( (String)result.get( "PlanReminderDate" ) ) );
		//constructPlanInformation( result, inventoryNode, (Date) result.get( "PlanReminderDate" ) ) ;

		applyPostProcessingFilters( result );

		reportDisplayBeans.add( result );
	}

	return reportDisplayBeans;
}


protected void constructPlanInformation( Map< String, Object > result, Node inventoryNode, Date reminderDate )
{
	List<Node> planEventNodes = XMLHelper.getChildren( inventoryNode, "PlanEvent" );
	Map<String, Object> planSummary = new HashMap<String, Object>();

	buildPlanSummary( planEventNodes, planSummary, reminderDate );
	
	result.put( "PlanSummary", planSummary );
}



private void buildPlanSummary( List<Node> planEventNodes, Map<String, Object> planSummary, Date reminderDate )
{
	NamedNodeMap planEventAttributes;
	PlanningEventTypeEnum eventTypeEnum;
	Integer strategy;
	Node strategyNode;
	Node statusNode;
	List<String> strategies = new ArrayList<String>();
	List<Map<String,Object>> detailedStrategies = new ArrayList<Map<String,Object>>();
	boolean repriced = false;
	String objective = null;
	PlanningStatusEnum statusEnum = null;
	boolean dueForReplanning = false;
	
	if ( reminderDate == null || ( reminderDate != null && planEventNodes.size() == 0 ) )
	{
		// vehicle has never been planned
		planSummary.put( "NeverPlanned", true ); 
		dueForReplanning = true;
	}
	else
	{
		planSummary.put( "NeverPlanned", false );
		dueForReplanning = DateUtils.truncate( new Date(), Calendar.DATE ).compareTo( reminderDate ) >= 0;
		planSummary.put( "DueForReplanning", dueForReplanning );
	}
	
	boolean noPlan = planEventNodes.size() == 1 && planEventNodes.get( 0 ).getAttributes().getLength() == 0;	
	planSummary.put( "NoPlan", noPlan );
	
	for( Node node : planEventNodes )
	{
		planEventAttributes = node.getAttributes();
		strategyNode = planEventAttributes.getNamedItem( "AIP_EventTypeID" );
		if ( strategyNode != null )
		{
			strategy = Integer.parseInt( strategyNode.getNodeValue() );
			eventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum( strategy );
			
			if ( !strategies.contains( eventTypeEnum.getDescription() ) && 
				 eventTypeEnum != PlanningEventTypeEnum.REPRICE )
			{
				strategies.add( eventTypeEnum.getDescription() );
				
				// this prevents empty parenthesis and strategy lists
				// that end with ';' from showing up on the gui
				if ( eventTypeEnum.isNoStrategyEvent() ) 
				{
					// don't show the 'green check mark' for
					// 'no strategy' events
					planSummary.put( "OverrideUpdate", true );
				}
				
				// assuming that all strategies are of the same objective,
				// but "Dealer" is not a valid objective
				if ( objective == null && eventTypeEnum.getPlanObjectiveEnum() != PlanObjectiveEnum.DEALER )
				{
				    objective = eventTypeEnum.getPlanObjectiveEnum().getDescription();
				}
			}
						
			if ( eventTypeEnum == PlanningEventTypeEnum.REPRICE )
			{
				repriced = true;
			}
			
			detailedStrategies.add( buildStrategyDetails( node, eventTypeEnum ) );
		}
		
		statusNode = planEventAttributes.getNamedItem( "Status" );
						
		// determine current or prior plan
		if ( statusEnum == null && statusNode != null && statusNode.getNodeValue() != null )
		{
	        statusEnum = PlanningStatusEnum.getStatusEnum( Integer.parseInt( statusNode.getNodeValue() ) );
		}
	}

	if ( noPlan && reminderDate != null && DateUtils.truncate( new Date(), Calendar.DATE ).compareTo( reminderDate ) < 0 )
	{
	    statusEnum = PlanningStatusEnum.CURRENT_PLAN;	
	}
	
	planSummary.put( "HasCurrentPlan", statusEnum==PlanningStatusEnum.CURRENT_PLAN?true:false );
	
	if ( repriced )
	{
		planSummary.put( "NeverPlanned", false );
		
		if ( statusEnum != null && statusEnum == PlanningStatusEnum.CURRENT_PLAN && !dueForReplanning )
		{
			// there is already a current plan for the vehicle
			planSummary.put( "DueForReplanning", false );
			
			// only override the updated checkmark if there are no other plans
			/**if ( strategies.size() == 0 )
			{
			    // causes the green "updated" checkmark to not display
				planSummary.put( "OverrideUpdate", true );
			}**/
		}
		else
		{
		    planSummary.put( "HasCurrentPlan", true );
		    planSummary.put( "DueForReplanning", true );
		}
		planSummary.put( "NoPlan", false );
	}
			
	planSummary.put( "Repriced", repriced );
	planSummary.put( "objective", objective );
	planSummary.put( "strategies", strategies );
	planSummary.put( "detailedStrategies", detailedStrategies  );
}

private Map<String, Object> buildStrategyDetails( Node node, PlanningEventTypeEnum planningEventTypeEnum )
{
	Map<String, Object> strategyData = new HashMap<String, Object>();
	
	NamedNodeMap attributes = node.getAttributes();
	
	strategyData.put( "strategy", planningEventTypeEnum.getDescription() );
	strategyData.put( "strategyEnum", planningEventTypeEnum );
	strategyData.put( "beginDate", formatSQLServerDate( attributes.getNamedItem( "BeginDate" ).getNodeValue() ) );
	
	if ( attributes.getNamedItem( "Notes" ) != null )
	{
	    strategyData.put( "notes", attributes.getNamedItem( "Notes" ).getNodeValue() );
	}
	
	if ( attributes.getNamedItem( "Notes2" ) != null )
	{
	    if ( planningEventTypeEnum == PlanningEventTypeEnum.AUCTION )
	    {
		    strategyData.put( "auctionLocationNotes", attributes.getNamedItem( "Notes2" ).getNodeValue() );
	    }
	    else
	    {
	        strategyData.put( "Notes2", attributes.getNamedItem( "Notes2" ).getNodeValue() );
	    }
	}
	
	if ( attributes.getNamedItem( "Value" ) != null )
	{
		strategyData.put( "value", attributes.getNamedItem( "Value" ).getNodeValue() );
	}
	
	if ( attributes.getNamedItem( "EndDate" ) != null )
	{
		strategyData.put( "endDate", formatSQLServerDate( attributes.getNamedItem( "EndDate" ).getNodeValue() ) );
	}
	
	if ( attributes.getNamedItem( "ThirdParty" ) != null )
	{
	    strategyData.put( "ThirdParty", attributes.getNamedItem( "ThirdParty" ).getNodeValue() );	
	} else if ( attributes.getNamedItem( "Notes3" ) != null ) 
	{
		strategyData.put( "ThirdParty", attributes.getNamedItem( "Notes3" ).getNodeValue() );	
	}
	
	if ( attributes.getNamedItem( "UserBeginDate" ) != null )
	{
		strategyData.put( "userBeginDate", formatSQLServerDate( attributes.getNamedItem( "UserBeginDate" ).getNodeValue() ) );	
	}
	
	if ( attributes.getNamedItem( "UserEndDate" ) != null )
	{
		strategyData.put( "userEndDate", formatSQLServerDate( attributes.getNamedItem( "UserEndDate" ).getNodeValue() ) );	
	}
	
	return strategyData;
}

private Date parseReminderDate( String reminderDateObj )
{  
	if ( reminderDateObj != null )
	{
	    SimpleDateFormat dateFormat = new SimpleDateFormat( "MM-dd-yyyy" );
	    
	    try
	    {
	        return dateFormat.parse( reminderDateObj );	
	    }
	    catch ( ParseException pe )
	    {
	        return null;
	    }
	}
	
	return null;
}

private Date getDateFromSQLServerDate( String sqlServerDate )
{
    SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
    try
    {
    	return dateFormat.parse( sqlServerDate );
    }
    catch (ParseException pe )
    {
    	return null;	
    }		
}

private String formatSQLServerDate( String sqlServerDate )
{
    Date reminderDate = getDateFromSQLServerDate( sqlServerDate );
    return new SimpleDateFormat( "MM-dd-yyyy" ).format( reminderDate );
}

}
