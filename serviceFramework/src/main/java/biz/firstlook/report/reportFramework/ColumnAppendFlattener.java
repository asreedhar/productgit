package biz.firstlook.report.reportFramework;

import java.util.Map;

/**
 * Takes two rows in a result set and returns a String with the values under <i>column</i> appended with a comma.
 * @author bfung
 *
 */
public class ColumnAppendFlattener implements Flattener< String >
{

private String column;
private String delimiter;

public ColumnAppendFlattener( final String column, String delimiter )
{
	super();

	this.column = column;
	this.delimiter = delimiter;
}

public String doFlatten( Map t1, Map t2 )
{
	Object t1col = t1.get( column );
	Object t2col = t2.get( column );
	
	boolean t1empty = t1col == null || t1col.toString().equals( "" );
	boolean t2empty = t2col == null || t2col.toString().equals( "" );
	
	if ( t1empty && t2empty )
	{
		return "";
	}
	else if ( t1empty )
	{
		return t2col.toString();
	}
	else if ( t2empty )
	{
		return t1col.toString();
	}
	else
	{
		return new StringBuilder().append( t1col.toString() ).append( delimiter ).append( " " ).append( t2col.toString() ).toString();
	}
}

public String getColumn()
{
	return column;
}

}
