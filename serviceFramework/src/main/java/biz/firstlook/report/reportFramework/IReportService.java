package biz.firstlook.report.reportFramework;

import java.util.List;
import java.util.Map;

import biz.firstlook.report.compositeReportBuilders.CompositeReportBuilder;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public interface IReportService
{
public List< Map > getResults( ReportBuilder reportBuilder );
public List< Map > getResultsFromStoredProcedure( ReportBuilder reportBuilder );
public List< Map > getCompositeResults( CompositeReportBuilder compositeReportBuilder );
}
