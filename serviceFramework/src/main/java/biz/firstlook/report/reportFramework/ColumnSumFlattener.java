package biz.firstlook.report.reportFramework;

import java.util.Map;


/**
 * Takes two rows in a result set and returns a String with the values under <i>column</i> summed.
 * @author dmincemoyer
 *
 */
public class ColumnSumFlattener implements Flattener{

    private String column;
    
    public ColumnSumFlattener(final String column){
        super();
        this.column = column;
        
    }
    
    public Object doFlatten(Map t1, Map t2) {
        Object t1Col = t1.get( column );
        Object t2Col = t2.get( column );
        
        boolean t1empty = t1Col == null || t1Col.toString().equals( "" );
        boolean t2empty = t2Col == null || t2Col.toString().equals( "" );
        
        if ( t1empty && t2empty )
        {
            return "";
        }
        else if ( t1empty )
        {
            return t2Col.toString();
        }
        else if ( t2empty )
        {
            return t1Col.toString();
        }
        else
        {
            Integer sum = Integer.parseInt(t1Col.toString()) + Integer.parseInt(t2Col.toString());
            return sum.toString();
        }
    }

    public String getColumn() {
        return column;
    }

}
