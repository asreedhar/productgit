package biz.firstlook.report.reportFramework;

import java.io.IOException;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.report.postProcessorFilters.PostProcessorFilter;
import biz.firstlook.report.sql.SQLizable;

public abstract class ReportDisplayBeanXmlMapper implements ResultSetExtractor
{

protected SQLizable sqlizable;

public ReportDisplayBeanXmlMapper( SQLizable sqlizable )
{
	this.sqlizable = sqlizable;
}

public abstract Object extractData( ResultSet rs ) throws SQLException, DataAccessException;

protected Object createXmlDocument( ResultSet rs ) throws SQLException, DataAccessException, ParserConfigurationException, SAXException,
		IOException
{
	String xmlStr = "";
	
	while ( rs.next() )
	{
		xmlStr += rs.getObject( 1 );
	}
    
	if ( xmlStr != null && xmlStr.trim().length() > 0 )
	{
	    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    Document xmlDoc = builder.parse( new InputSource( new StringReader( xmlStr ) ) );
	    return xmlDoc;
	}
	else
	{
	    return null;	
	}
}

public void createMapEntriesFromAttributes( Map< String, Object > rowResult, Node inventoryNode )
{	
	NamedNodeMap attributeMap = inventoryNode.getAttributes();
	Node attrNode;
	for( int i = 0; i < attributeMap.getLength(); i++ )
	{
		attrNode = attributeMap.item( i );
		if ( attrNode.getNodeName().equals( "LastPlannedDate" ) || attrNode.getNodeName().equals( "PlanReminderDate") )
		{
		    rowResult.put( attrNode.getNodeName(), DateUtilFL.formatSQLServerDate( attrNode.getNodeValue() ) );
		}
		else
		{
			rowResult.put( attrNode.getNodeName(), attrNode.getNodeValue() );
		}
		
	}
}

protected void applyPostProcessingFilters(Map< String, Object > rowResult)
{
	Iterator postProcessorFiltersIter = sqlizable.getPostProcessorFilters().iterator();
	while( postProcessorFiltersIter.hasNext() )
	{
		PostProcessorFilter filter = (PostProcessorFilter)postProcessorFiltersIter.next();
		filter.filterResults( rowResult );
	}
}

}
