package biz.firstlook.report.reportFramework;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.report.compositeReportBuilders.CompositeReportBuilder;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public class ReportService implements IReportService
{

private IReportDisplayBeanDAO reportDisplayBeanDAO;


public ReportService()
{
	super();
}

public List< Map > getResults( ReportBuilder reportBuilder)
{
	if (reportBuilder.isStoredProcedure())
	{
		return getReportDisplayBeanDAO().getResultsFromStoredProcedure( reportBuilder );
	}
	return getReportDisplayBeanDAO().getResults( reportBuilder );
}

public List< Map > getResultsFromStoredProcedure( ReportBuilder reportBuilder)
{
	return getReportDisplayBeanDAO().getResultsFromStoredProcedure( reportBuilder );
}

public List< Map > getCompositeResults( CompositeReportBuilder compositeReportBuilder )
{
	Map<String, List<Map> > subReports = new HashMap<String, List<Map> >();
	
	List<ReportBuilder> reports = compositeReportBuilder.getReports();
	Iterator reportsIter = reports.iterator();
	while ( reportsIter.hasNext() )
	{
		ReportBuilder report = (ReportBuilder)reportsIter.next();
		subReports.put( report.getReportName(), getResults( report ) );
	}
	
	List<CompositeReportBuilder> compositeReports = compositeReportBuilder.getCompositeReports();
	Iterator compositeReportsIter = compositeReports.iterator();
	while( compositeReportsIter.hasNext() )
	{
		CompositeReportBuilder compositeReport = (CompositeReportBuilder)compositeReportsIter.next();
		subReports.put( compositeReport.getCompositeReportName(), getCompositeResults( compositeReport ) );
	}
	
	return compositeReportBuilder.mergeReports( subReports );
}

public IReportDisplayBeanDAO getReportDisplayBeanDAO()
{
	return reportDisplayBeanDAO;
}


public void setReportDisplayBeanDAO( IReportDisplayBeanDAO reportDisplayBeanDAO )
{
	this.reportDisplayBeanDAO = reportDisplayBeanDAO;
}

}
