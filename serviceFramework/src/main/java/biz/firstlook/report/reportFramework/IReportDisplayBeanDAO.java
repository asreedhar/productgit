package biz.firstlook.report.reportFramework;

import java.util.List;
import java.util.Map;

import biz.firstlook.report.reportBuilders.ReportBuilder;

public interface IReportDisplayBeanDAO
{
public List< Map > getResults( ReportBuilder reportBuilder );

public List< Map > getResultsFromStoredProcedure( ReportBuilder reportBuilder );
}
