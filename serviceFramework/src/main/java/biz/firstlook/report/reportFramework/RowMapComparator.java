package biz.firstlook.report.reportFramework;

import java.util.Comparator;
import java.util.Map;

/**
 * Compares values inside a Map given the Key.
 * @author bfung
 *
 * @param <K> the Class type of the Key in the Map
 */
public class RowMapComparator<K> implements Comparator<Map>
{

private K key;

public RowMapComparator( K key )
{
	super();
	this.key = key;
}

public int compare( Map m1, Map m2 )
{
	Comparable value1 = (Comparable)m1.get( key );
	Comparable value2 = (Comparable)m2.get( key );

	return value1.compareTo( value2 );
}

}
