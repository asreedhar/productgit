package biz.firstlook.report.reportFramework;

import java.util.Map;

public interface Flattener<T>
{

public abstract T doFlatten( Map existingRow, Map newRow ); 

public abstract String getColumn();

}
