package biz.firstlook.report.reportFramework;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.report.reportBuilders.ReportBuilder;
import biz.firstlook.report.sql.SQLGenerator;

class ReportDisplayBeanDAO extends JdbcDaoSupport implements IReportDisplayBeanDAO
{
private static final Logger logger = Logger.getLogger( ReportDisplayBeanDAO.class );

@SuppressWarnings("unchecked")
public List< Map > getResults( ReportBuilder reportBuilder )
{
	SQLGenerator sqlGenerator = new SQLGenerator();
	
	String sql = null;
	try
	{
		sql = sqlGenerator.getSQL( reportBuilder );
	}
	catch ( SQLException sqlE) 
	{
		logger.debug( "Error generating SQL, probably IN clause related!", sqlE );
		List< Map > exceptionList = new ArrayList<Map>();
		exceptionList.add( new HashMap() );
		return exceptionList;
	}
	
	Object[] parameterValues = reportBuilder.getFilterValues();
	
	if ( logger.isDebugEnabled() )
	{
		StringBuffer debugMesssage = new StringBuffer();
		debugMesssage.append( "Query: " ).append( sql ).append( " " );
		debugMesssage.append( "Parameters: [ " );
		for( Object parameterValue : parameterValues )
		{
			if ( parameterValue != null )
			{
				debugMesssage.append( parameterValue.toString() );
			}
			else
			{
				debugMesssage.append( "null" );
			}	
			debugMesssage.append( " " );
		}
		debugMesssage.append( "]");
		logger.debug( debugMesssage.toString() );
	}
	List< Map > results = (List< Map >)getJdbcTemplate().query( sql, parameterValues, reportBuilder.getRowMapper() ); 
	
	return results;
}

@SuppressWarnings("unchecked")
public List<Map> getResultsFromStoredProcedure(ReportBuilder reportBuilder)
{
	List<Map> results = null;
	long ts0 = System.currentTimeMillis();
	if (reportBuilder.isCallback()) {
		if (logger.isDebugEnabled())
			logger.debug("Calling Stored Procedure: " + reportBuilder.fromClause() + " with bind variables " + reportBuilder.getParameterMap());
		CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory(
				reportBuilder.fromClause(),
				reportBuilder.getParameterList());
		CallableStatementCreator csc = cscf.newCallableStatementCreator(
				reportBuilder.getParameterMap());
		results = (List<Map>) getJdbcTemplate().execute(csc, reportBuilder.getCallableStatementCallback());
	}
	else {
		String sql = new SQLGenerator().getStoredProcedureSQL(reportBuilder);
		if (logger.isDebugEnabled())
			logger.debug("Calling Stored Procedure: " + sql);
		results = (List<Map>) getJdbcTemplate().query(sql, reportBuilder.getRowMapper());
	}
	long ts1 = System.currentTimeMillis();
	if (logger.isDebugEnabled())
		logger.debug("Finished Stored Procedure call in " + (ts1 - ts0) + "ms");
	return results;
}

}
