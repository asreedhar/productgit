package biz.firstlook.report.postProcessorFilters;

import java.util.Map;

public class StockingReportFilter implements PostProcessorFilter{

    public StockingReportFilter() {
        super();
    }
    
    public void filterResults(Map<String, Object> rowResult) {
        Integer unitsInStock = (Integer) rowResult.get("UnitsInStock");
        Integer targetSupply = (Integer) rowResult.get("TargetUnits");
        if (unitsInStock != null && unitsInStock  == 0 && unitsInStock != null && targetSupply == 0) {
            rowResult.clear();
        }
    }

}
