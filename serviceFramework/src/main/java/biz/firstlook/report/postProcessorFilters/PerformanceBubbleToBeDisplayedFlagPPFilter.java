package biz.firstlook.report.postProcessorFilters;

import java.math.BigDecimal;
import java.util.Map;

/**
 * this filter is used to determine whether or not to display a performance bubble.
 * if all the values in the bubble are 0, it shouldn't be displayed.
 * 
 * this filter adds the key 'display' with a cooresponding value of true or false (depending on whether it should be displayed)
 */
public class PerformanceBubbleToBeDisplayedFlagPPFilter implements PostProcessorFilter
{

public PerformanceBubbleToBeDisplayedFlagPPFilter()
{
	super();
}

public void filterResults( Map< String, Object > rowResult )
{
	if ( ( (BigDecimal)rowResult.get( "AGP" ) ).intValue()
			+ ( (Integer)rowResult.get( "UnitsSold" ) ).intValue()
			+ ( (Integer)rowResult.get( "AvgDaysToSale" ) ).intValue()
			+ ( (Integer)rowResult.get( "NoSaleUnits" ) ).intValue() == 0 )
	{
		rowResult.put( "Display", false );
	}
	else
	{	
		rowResult.put( "Display", true );
	}
}

}
