package biz.firstlook.report.postProcessorFilters;

import java.util.Map;

import biz.firstlook.main.enumerator.TradeOrPurchaseEnum;

public class TradeOrPurchasePPFilter implements PostProcessorFilter
{
private boolean useLongDescription = false;
	
public TradeOrPurchasePPFilter()
{
	super();
}

public TradeOrPurchasePPFilter( boolean useLongDescription )
{
    this.useLongDescription = useLongDescription;	
}

public void filterResults( Map< String, Object > rowResult )
{
	Integer tradeOrPurchaseValue;
	if( rowResult.get("TradeOrPurchase") instanceof String )
	{
		tradeOrPurchaseValue = new Integer( (String)rowResult.get("TradeOrPurchase") );
	}
	else
	{
		tradeOrPurchaseValue = (Integer)rowResult.get("TradeOrPurchase");
	}
	TradeOrPurchaseEnum tradeOrPurchase = TradeOrPurchaseEnum.getTradeOrPurchaseEnum( tradeOrPurchaseValue.intValue() );
	String tradeOrPurchaseDescription = null;
	
	if (useLongDescription)
	{
		tradeOrPurchaseDescription = tradeOrPurchase.getTradeOrPurchaseDesc();	
	}
	else
	{
		tradeOrPurchaseDescription = tradeOrPurchase.getTradeOrPurchaseShortDesc();	
	}
	
	rowResult.put( "TradeOrPurchaseDesc", tradeOrPurchaseDescription );
}

}
