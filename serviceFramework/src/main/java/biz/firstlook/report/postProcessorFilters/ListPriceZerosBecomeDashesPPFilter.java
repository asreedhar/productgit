package biz.firstlook.report.postProcessorFilters;

import java.math.BigDecimal;
import java.util.Map;

public class ListPriceZerosBecomeDashesPPFilter implements PostProcessorFilter
{

public void filterResults( Map< String, Object > rowResult )
{
	if (rowResult.get("listPrice") != null  &&
			((BigDecimal)rowResult.get("listPrice")).intValue() == 0 )
	{
		rowResult.put( "listPrice", "--");
	}
}

}
