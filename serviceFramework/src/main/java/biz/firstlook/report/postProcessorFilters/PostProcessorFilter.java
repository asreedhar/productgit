package biz.firstlook.report.postProcessorFilters;

import java.util.Map;

public interface PostProcessorFilter
{
	void filterResults( Map< String, Object > rowResult );
}
