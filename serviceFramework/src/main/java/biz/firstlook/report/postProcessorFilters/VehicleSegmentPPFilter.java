package biz.firstlook.report.postProcessorFilters;

import java.util.Map;

import biz.firstlook.main.enumerator.VehicleSegmentEnum;

public class VehicleSegmentPPFilter implements PostProcessorFilter
{

public VehicleSegmentPPFilter()
{
	super();
}

public void filterResults( Map< String, Object > rowResult )
{
	rowResult.put( "VehicleSegmentDescription", VehicleSegmentEnum.getVehicleSegmentEnum( ((Integer)rowResult.get("VehicleSegmentID")).intValue() ).getDescription() );
}

}
