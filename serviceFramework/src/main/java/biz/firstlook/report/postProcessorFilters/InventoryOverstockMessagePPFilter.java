package biz.firstlook.report.postProcessorFilters;

import java.util.Map;

public class InventoryOverstockMessagePPFilter implements PostProcessorFilter {

	public void filterResults(Map<String, Object> rowResult) 
	{
		StringBuffer message = new StringBuffer();
		Integer overstocked;
		
		if( rowResult.get("Overstocked") instanceof String )
		{
			overstocked = Integer.parseInt( (String)rowResult.get("Overstocked") );
		}
		else
		{
			overstocked = (Integer)rowResult.get("Overstocked");
		}
		
		if ( overstocked.intValue() == 1 )
		{
			message.append( rowResult.get( "VehicleYear" ) );
			message.append( " ");
			message.append( rowResult.get( "GroupingDescriptionStr" ) );
			message.append( " Overstocked Year" );
		}
		
		rowResult.put( "OverstockedMessage", message.toString() );
	}

}
