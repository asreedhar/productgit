package biz.firstlook.report.postProcessorFilters;

import java.math.BigDecimal;
import java.util.Map;

public class BookVsCostDifferencePPFilter implements PostProcessorFilter
{

public BookVsCostDifferencePPFilter()
{
	super();
}

public void filterResults( Map< String, Object > rowResult )
{	
	BigDecimal bookVsCost = null;
	BigDecimal unitCost = null;
	BigDecimal bookValue = null;
	BigDecimal mmrBookValue = null;
	BigDecimal mmrBookVsCost = null;
	
	
	if ( rowResult.get( "PrimaryBookValue" ) != null && rowResult.get( "UnitCost" ) != null )
	{
	    // inventory planning wants to use this filter, but... the 'BookValue' column is
		// called 'PrimaryBookValue in inventory planning and the book value
		// and unit cost come back as Strings - didn't want to write two filters
		// that do essentially the same thing
	    unitCost = toBigDecimal( rowResult.get( "UnitCost" ) );
	    bookValue = toBigDecimal( rowResult.get( "PrimaryBookValue" ) );
	} 
	else if ( rowResult.get( "UnitCost" ) != null && rowResult.get( "BookValue" ) != null )
	{
        unitCost = toBigDecimal( rowResult.get( "UnitCost" ) );		
		bookValue = toBigDecimal( rowResult.get( "BookValue" ) );
	}
	if(rowResult.get("MMRValue")!= null && rowResult.get( "UnitCost" ) != null )
	{
		   unitCost = toBigDecimal( rowResult.get( "UnitCost" ) );
		   mmrBookValue = toBigDecimal( rowResult.get( "MMRValue" ) );
	}
	if ( mmrBookValue != null && unitCost != null )
	{
		mmrBookVsCost = mmrBookValue.subtract( unitCost );	
	}
	rowResult.put( "MMRBookVsCost", mmrBookVsCost );
	
	if ( bookValue != null && unitCost != null )
	{
		bookVsCost = bookValue.subtract( unitCost );	
	}

	rowResult.put( "BookVsCost", bookVsCost );
}

/**
 * A fix(hack) for DB objects coming in as different data types.
 * @param object
 * @return
 */
private BigDecimal toBigDecimal( Object object )
{
	if ( object instanceof Integer )
	{
		return new BigDecimal( (Integer)object );
	}
	else if ( object instanceof String )
	{
		return new BigDecimal( (String)object );
	}
	else
	{
		return (BigDecimal)object;		
	}
}

}
