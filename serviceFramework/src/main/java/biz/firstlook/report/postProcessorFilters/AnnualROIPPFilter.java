package biz.firstlook.report.postProcessorFilters;

import java.math.BigDecimal;
import java.util.Map;

public class AnnualROIPPFilter implements PostProcessorFilter
{

public AnnualROIPPFilter()
{
	super();
}

public void filterResults( Map< String, Object > rowResult )
{
	if ( rowResult.get( "AvgFrontEndGross" ) != null && rowResult.get( "AvgUnitCost" ) != null && rowResult.get( "AvgDaysToSale" ) != null )
	{
		BigDecimal avgFrontEndGross = (BigDecimal)rowResult.get( "AvgFrontEndGross" );
		BigDecimal avgUnitCost = (BigDecimal)rowResult.get( "AvgUnitCost" );
		BigDecimal avgDaysToSale = (BigDecimal)rowResult.get( "AvgDaysToSale" );
		double annualRoi = ( avgFrontEndGross.doubleValue() / avgUnitCost.doubleValue() ) * ( 365 / avgDaysToSale.doubleValue() );
		rowResult.put( "AnnualRoi", new Double( annualRoi ) );
	}
	else
	{
		rowResult.put( "AnnualRoi", null );
	}
}

}
