package biz.firstlook.report.postProcessorFilters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class SpecialFinanceOrCertifiedReportNotesPPFilter implements PostProcessorFilter
{

public SpecialFinanceOrCertifiedReportNotesPPFilter()
{
}

public void filterResults( Map< String, Object > rowResult )
{
	String thirdPartyString = null;
	String originalNotes = (String)rowResult.get( "notes" );
	String retailStrats = (String) rowResult.get( "retailStrats" );
	
	if ( retailStrats == null || retailStrats.equals( "No Strategy" ) )
	{
		rowResult.put( "retailStrats", "" );
	}
	else
	{
		if ( retailStrats.equals("Advertisement") )
		{
			if ( rowResult.get("ThirdParty") != null )
			{
				thirdPartyString = (String)rowResult.get( "ThirdParty" );
				rowResult.put( "notes", retailStrats + " " + thirdPartyString );
			}
		}
		else if ( retailStrats.equals( "Service Department" ) )
		{
			SimpleDateFormat sdf = new SimpleDateFormat( "MM-dd-yyyy" );
			String beginDate = sdf.format( (Date) rowResult.get( "BeginDate" ) );
			rowResult.put( "notes", "Service " + beginDate );
			rowResult.put( "retailStrats", "Service" );
		}
		else
		{
			rowResult.put( "notes", retailStrats );
		}
		
		rowResult.put( "notes", rowResult.get( "notes" ) + ( ( null == originalNotes || "".equals( originalNotes ) ) ? "" : " (" + originalNotes + ")" ) );
	}
}

}

