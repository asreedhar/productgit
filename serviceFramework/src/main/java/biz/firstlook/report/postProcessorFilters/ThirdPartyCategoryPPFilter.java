 package biz.firstlook.report.postProcessorFilters;

import java.util.Map;

import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;

public class ThirdPartyCategoryPPFilter implements PostProcessorFilter
{

public ThirdPartyCategoryPPFilter()
{
	super();
}

public void filterResults( Map< String, Object > rowResult )
{
	rowResult.put( "ThirdPartyCategoryDescription", ThirdPartyCategoryEnum.getEnumById( (Integer)rowResult.get("ThirdPartyCategoryID") ).getDescription() );
}

}
