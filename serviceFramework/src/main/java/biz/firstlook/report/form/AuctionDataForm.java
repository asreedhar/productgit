package biz.firstlook.report.form;

import org.apache.struts.action.ActionForm;

public class AuctionDataForm extends ActionForm
{



/**
	 * 
	 */
	private static final long serialVersionUID = 3101820041371766054L;
private String vic;
private Integer areaId;
private Integer timePeriodId;
private Integer modelYear;
private Integer mileage;
private Boolean usingVIC;

public String getVic()
{
	return vic;
}

public void setVic( String vicBodyStyleId )
{
	this.vic = vicBodyStyleId;
}

public Integer getTimePeriodId()
{
	return timePeriodId;
}

public void setTimePeriodId( Integer timePeriodId )
{
	this.timePeriodId = timePeriodId;
}

public Integer getAreaId()
{
	return areaId;
}

public void setAreaId( Integer areaId )
{
	this.areaId = areaId;
}

public Integer getMileage()
{
	return mileage;
}

public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}

public Integer getModelYear()
{
	return modelYear;
}

public void setModelYear( Integer year )
{
	this.modelYear = year;
}

public Boolean getUsingVIC()
{
	return usingVIC;
}

public void setUsingVIC( Boolean usingVIC )
{
	this.usingVIC = usingVIC;
}


}
