package biz.firstlook.report.sql;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import biz.firstlook.main.enumerator.SQLOperationEnum;

public class SQLGenerator
{

/**
 * This method generates SQL based on fields set on this class.
 * 
 * The sql grammar used to construct the select statement will be referenced from
 * <ul>
 * <li>http://msdn.microsoft.com/library/default.asp?url=/library/en-us/odbc/htm/odbcsql_minimum_grammar.asp</li>
 * <li>http://msdn.microsoft.com/library/default.asp?url=/library/en-us/tsqlref/ts_sa-ses_154e.asp</li>
 * </ul>
 * and related pages of microsoft.com, since right now we are only ever using SQL Server database.
 * 
 * This only handles simple, non-composite beans.
 * 
 * <pre>
 *           select-statement ::=
 *          	SELECT [ALL | DISTINCT] select-list
 *          	FROM table-reference-list
 *          	[WHERE search-condition]
 *          	[order-by-clause]
 * </pre>
 * 
 * @return a full SQL select statement
 * 
 * @author bfung
 * @throws SQLException
 */
public final String getSQL( SQLizable sqlizableObject ) throws SQLException
{
	StringBuffer sql = new StringBuffer( "select " );

	if ( StringUtils.isBlank( sqlizableObject.selectFields() ) )
	{
		sql.append( "*" );
	}
	else
	{
		sql.append( sqlizableObject.selectFields() );
	}

	sql.append( " from " );

	sql.append( sqlizableObject.fromClause() );

	// search-condidtion
	Iterator sqlWhereSearchStatementsIter = sqlizableObject.getSqlWhereSearchStatementsIter();

	// this is the case where there are no where clauses needed/defined
	if ( sqlWhereSearchStatementsIter.hasNext() )
	{
		sql.append( " where " );
	}

	while ( sqlWhereSearchStatementsIter.hasNext() )
	{
		constructWhereClauses( sql, sqlWhereSearchStatementsIter, SQLOperationEnum.AND );
	}

	if ( sqlizableObject.getOrderBy() != null )
	{
		sql.append( " order by " + sqlizableObject.getOrderBy() );
		if ( sqlizableObject.isDesc() )
		{
			sql.append( " desc" );
		}
	}
	return sql.toString();
}

private void constructWhereClauses( StringBuffer sql, Iterator sqlWhereSearchStatementsIter, SQLOperationEnum concatenator )
		throws SQLException
{
	SQLWhereSearchStatement sqlWhereSearchStatement = (SQLWhereSearchStatement)sqlWhereSearchStatementsIter.next();

	SQLOperationEnum operation = sqlWhereSearchStatement.getSqlOperationEnum();
	String dbColumnName = sqlWhereSearchStatement.getDbColumnName();
	if ( operation.equals( SQLOperationEnum.IN ) )
	{
		sql.append( dbColumnName );
		sql.append( operation.toString() );
		sql.append( generateInClause( sqlWhereSearchStatement.getInClauseValues(), dbColumnName ) );
	}
	else if ( operation.equals( SQLOperationEnum.BETWEEN ) )
	{
		sql.append( dbColumnName );
		sql.append( operation.toString() );
		sql.append( generateBetweenClause( sqlWhereSearchStatement.getInClauseValues(), dbColumnName ) );
	}
	else if ( operation.equals( SQLOperationEnum.OR ) )
	{
		Set< SQLWhereSearchStatement > orClauses = sqlWhereSearchStatement.getOrClauseValues();
		Iterator orIter = orClauses.iterator();

		sql.append( "(" );
		while ( orIter.hasNext() )
		{
			constructWhereClauses( sql, orIter, SQLOperationEnum.OR );
		}
		sql.append( ")" );
	}
	else if ( operation.equals( SQLOperationEnum.IS_NULL ) )
	{
		sql.append( dbColumnName ).append( operation.toString() );
	}
	else
	{
		sql.append( dbColumnName ).append( operation ).append( "?" );
	}
	if ( sqlWhereSearchStatementsIter.hasNext() )
	{
		if ( concatenator.equals( SQLOperationEnum.AND ) )
		{
			sql.append( SQLOperationEnum.AND.toString() );
		}
		else
		{
			sql.append( SQLOperationEnum.OR.toString() );
		}
	}
}

/**
 * This method generates SQL to call a stored procedure.
 * 
 * The sql grammar used to construct the select statement will be referenced from
 * <ul>
 * <li>http://msdn.microsoft.com/library/default.asp?url=/library/en-us/odbc/htm/odbcsql_minimum_grammar.asp</li>
 * <li>http://msdn.microsoft.com/library/default.asp?url=/library/en-us/tsqlref/ts_sa-ses_154e.asp</li>
 * </ul>
 * and related pages of microsoft.com, since right now we are only ever using SQL Server database.
 * 
 * This only handles simple, non-composite beans.
 * 
 * <pre>
 *           select-statement ::=
 *          	EXEC stored-procedure-name comma-separated list of arguments
 * </pre>
 * 
 * @return a full SQL select statement
 * 
 * @author bfung
 * @throws SQLException
 */
public final String getStoredProcedureSQL( SQLizable sqlizableObject )
{
	StringBuffer sql = new StringBuffer( "EXEC " );
	sql.append( sqlizableObject.fromClause() );

	return sql.toString();
}

/**
 * @param inClauseBeanList
 *            a List of beans with properties to be used in the 'IN' clause
 * @param property
 *            the property to be used in the 'IN' clause. If the List is just a list of Strings, this parameter will be ignored.
 * @throws SQLException
 */
String generateInClause( List inClauseBeanList, String property ) throws SQLException
{
	// check for empty list - will crash query
	if ( inClauseBeanList == null || inClauseBeanList.isEmpty() )
	{
		throw new SQLException( "'IN' clause is empty." );
	}

	StringBuffer inClause = new StringBuffer();
	try
	{
		inClause.append( "( " );
		Iterator beanIter = inClauseBeanList.iterator();
		while ( beanIter.hasNext() )
		{
			Object bean = beanIter.next();
			if ( bean instanceof String )
			{
				inClause.append( "\'" ).append( bean ).append( "\'" );
			}
			else
			{
				Object propertyOnBean = PropertyUtils.getSimpleProperty( bean, property );
				if ( propertyOnBean instanceof String )
				{
					inClause.append( "\'" ).append( propertyOnBean ).append( "\'" );
				}
				else
				{
					inClause.append( propertyOnBean );
				}
			}

			if ( beanIter.hasNext() )
			{
				inClause.append( ", " );
			}
		}
		inClause.append( " )" );
	}
	catch ( Exception e )
	{
		throw new SQLException( e.getMessage() );
	}

	return inClause.toString();
}

/**
 * @param inClauseBeanList
 *            a List of beans with properties to be used in the 'BETWEEN' clause
 * @param property
 *            the property to be used in the 'BETWEEN' clause. If the List is just a list of Strings, this parameter will be ignored.
 * @throws SQLException
 */
String generateBetweenClause( List betweenClauseBeanList, String property ) throws SQLException
{
	// check for empty list - will crash query
	if ( betweenClauseBeanList == null || betweenClauseBeanList.isEmpty() )
	{
		throw new SQLException( "'BETWEEN' clause is empty." );
	}

	StringBuffer betweenClause = new StringBuffer();

	Iterator beanIter = betweenClauseBeanList.iterator();
	while ( beanIter.hasNext() )
	{
		Object bean = beanIter.next();
		if ( bean instanceof String )
		{
			betweenClause.append( "\'" ).append( bean ).append( "\'" );
		}
		else
		{
			betweenClause.append( bean );
		}
		if ( beanIter.hasNext() )
		{
			betweenClause.append( " and " );
		}
	}
	return betweenClause.toString();
}

}
