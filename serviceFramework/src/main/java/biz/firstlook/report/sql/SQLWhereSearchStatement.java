package biz.firstlook.report.sql;

import java.util.List;
import java.util.Set;

import biz.firstlook.main.enumerator.SQLOperationEnum;

public class SQLWhereSearchStatement implements Comparable< SQLWhereSearchStatement >
{

private final String dbColumnName;
private final SQLOperationEnum sqlOperationEnum;
private final Object filterValue;
private final List inClauseValues;
private final Set< SQLWhereSearchStatement > orClauseValues;

public SQLWhereSearchStatement( String dbColumnName, SQLOperationEnum sqlOperationEnum, Object filterValue )
{
	super();
	this.dbColumnName = dbColumnName;
	this.sqlOperationEnum = sqlOperationEnum;
	this.filterValue = filterValue;
	this.inClauseValues = null;
	this.orClauseValues = null;
}

public SQLWhereSearchStatement( String dbColumnName, SQLOperationEnum sqlOperationEnum, List inClauseValues )
{
	super();
	this.dbColumnName = dbColumnName;
	this.sqlOperationEnum = sqlOperationEnum;
	this.filterValue = null;
	this.inClauseValues = inClauseValues;
	this.orClauseValues = null;
}

public SQLWhereSearchStatement( String dbColumnName, SQLOperationEnum sqlOperationEnum, Set< SQLWhereSearchStatement > orClauseValues, Object filterValue )
{
	super();
	this.dbColumnName = dbColumnName;
	this.sqlOperationEnum = sqlOperationEnum;
	this.filterValue = filterValue;
	this.inClauseValues = null;
	this.orClauseValues = orClauseValues;
}

public String getDbColumnName()
{
	return dbColumnName;
}

public Object getFilterValue()
{
	return filterValue;
}

public SQLOperationEnum getSqlOperationEnum()
{
	return sqlOperationEnum;
}

public List getInClauseValues()
{
	return inClauseValues;
}

public Set< SQLWhereSearchStatement > getOrClauseValues()
{
	return orClauseValues;
}

public int compareTo( SQLWhereSearchStatement inSearchStatment )
{
	return new StringBuilder().append( dbColumnName ).append( sqlOperationEnum.toString() )
							  .append( filterValue.toString() ).toString().compareTo( 
	       new StringBuilder().append( inSearchStatment.getDbColumnName() )
	       					  .append( inSearchStatment.getSqlOperationEnum().toString() )
	       					  .append( inSearchStatment.getFilterValue().toString() ).toString()  );
}

public String toString()
{
	StringBuffer sb = new StringBuffer("{");
	sb.append("dbColumnName=").append(dbColumnName);
	sb.append(",filterValue=").append(filterValue);
	sb.append(",inClauseValues=").append(inClauseValues);
	sb.append(",orClauseValues=").append(orClauseValues);
	sb.append(",sqlOperationEnum=").append(sqlOperationEnum);
	return sb.append("}").toString();
}

}
