package biz.firstlook.report.sql;

import java.util.Iterator;
import java.util.List;

import biz.firstlook.report.postProcessorFilters.PostProcessorFilter;

public interface SQLizable
{

/**
 * The columns to return from a query. By default, the SQLGenerator uses "select * from" if the return value of this method is null, whitespace,
 * or empty string.
 * 
 * @return
 */
public abstract String selectFields();

public abstract String fromClause();

public abstract String getFilterOperation( String dbColumn );

public abstract Object[] getFilterValues();

public abstract Iterator getSqlWhereSearchStatementsIter();

public abstract List<PostProcessorFilter> getPostProcessorFilters();

public abstract String getOrderBy();

public abstract boolean isDesc();

}
