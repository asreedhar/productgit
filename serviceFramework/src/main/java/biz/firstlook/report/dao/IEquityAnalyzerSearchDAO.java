package biz.firstlook.report.dao;

import java.util.List;

import biz.firstlook.entity.EquityAnalyzerSearch;


public interface IEquityAnalyzerSearchDAO
{
public EquityAnalyzerSearch save( EquityAnalyzerSearch equityAnalyzerSearch );
public List<EquityAnalyzerSearch> loadByBusinessUnitId( final Integer businessUnitId );
public EquityAnalyzerSearch loadById( Integer equityAnalyzerSearchId );
public List<EquityAnalyzerSearch> loadByExample( final EquityAnalyzerSearch exampleInstance );
}
