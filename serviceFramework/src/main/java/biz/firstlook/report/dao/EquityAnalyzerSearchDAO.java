package biz.firstlook.report.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.entity.EquityAnalyzerSearch;


public class EquityAnalyzerSearchDAO extends HibernateDaoSupport implements IEquityAnalyzerSearchDAO
{

/**
 * Saves an equity report search
 * @param equityAnalyzerSearch - the instance to save
 * @return EquityAnalyzerSearch
 */
public EquityAnalyzerSearch save( EquityAnalyzerSearch equityAnalyzerSearch )
{
	getHibernateTemplate().setFlushMode( HibernateTemplate.FLUSH_EAGER);
    getHibernateTemplate().save( equityAnalyzerSearch );
    return null;
}

/**
 * loads all equity report searches for the given business unit id
 * @param businessUnitId
 * @return List<EquityAnalyzerSearch>
 */
public List<EquityAnalyzerSearch> loadByBusinessUnitId( final Integer businessUnitId )
{
	HibernateCallback callback = new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Criteria crit = session.createCriteria( EquityAnalyzerSearch.class )
			                .add( Expression.eq( "businessUnitId", businessUnitId ) )
			                .addOrder( Order.asc( "name" ) );
			return crit.list();
		}
	};

	return (List<EquityAnalyzerSearch>) getHibernateTemplate().execute( callback );	
}

/**
 * loads all instances of EquityAnalyzerSearch whose field's values 
 * match those of the example instance
 * @param exampleInstance
 * @return List<EquityAnalyzerSearch>
 */
public List<EquityAnalyzerSearch> loadByExample( final EquityAnalyzerSearch exampleInstance )
{
	HibernateCallback callback = new HibernateCallback() {
		public Object doInHibernate(Session session)
				throws HibernateException, SQLException {
			Criteria crit = session.createCriteria(EquityAnalyzerSearch.class);
			Example example = Example.create(exampleInstance);
			crit.add(example);
			return crit.list();
		}
	};
	return (List) getHibernateTemplate().execute(callback);	
}

/**
 * returns the instance of EquityAnalyzerSearch with the given id
 * @param equityAnalyzerSearchId
 * @return EquityAnalyzerSearch
 */
public EquityAnalyzerSearch loadById( Integer equityAnalyzerSearchId )
{
    /**EquityAnalyzerSearch equityAnalyzerSearch = (EquityAnalyzerSearch) getHibernateTemplate().load( EquityAnalyzerSearch.class, equityAnalyzerSearchId );
    equityAnalyzerSearch.getId();
    return equityAnalyzerSearch;**/
	List searches = getHibernateTemplate().find( "from biz.firstlook.entity.EquityAnalyzerSearch search where search.id = ?", equityAnalyzerSearchId );
	return (EquityAnalyzerSearch) searches.get(0);
}
}
