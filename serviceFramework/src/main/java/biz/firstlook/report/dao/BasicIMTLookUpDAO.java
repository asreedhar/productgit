package biz.firstlook.report.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.report.postProcessorFilters.PostProcessorFilter;
import biz.firstlook.report.reportFramework.ReportDisplayBeanRowMapper;

public class BasicIMTLookUpDAO  extends JdbcDaoSupport implements IBasicIMTLookUpDAO
{

@SuppressWarnings("unchecked")
public List<Map<String, Object>> getResults( String sql, Object[] parameterValues )
{
	return (List<Map<String, Object>>)getJdbcTemplate().query( sql, parameterValues, new ReportDisplayBeanRowMapper( new ArrayList<PostProcessorFilter>() ) );
}

}
