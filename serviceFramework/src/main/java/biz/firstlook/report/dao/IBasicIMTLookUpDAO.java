package biz.firstlook.report.dao;

import java.util.List;
import java.util.Map;

public interface IBasicIMTLookUpDAO
{
public List<Map<String, Object>> getResults( String sql, Object[] parameterValues );
}
