package biz.firstlook.report.reportBuilders.aip;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.report.postProcessorFilters.BookVsCostDifferencePPFilter;
import biz.firstlook.report.postProcessorFilters.TradeOrPurchasePPFilter;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public class PlanningDetailReportBuilder extends ReportBuilder {
	
	private static final String REPORT_NAME = "InventoryPlanDetail";
	
	private Integer businessUnitId;
	private String statusCodeList;
	private Integer inventoryBucketId;
	private Integer resultsMode;
	private boolean showRecent;
	private String inventoryIdsList;
	private Integer rangeId;
    private Integer searchDays;
	private Date baseDate;
	private Integer inventoryId;

	public PlanningDetailReportBuilder(
			Integer businessUnitId,
			String statusCodeList,
			Integer inventoryBucketId,
			Integer resultsMode,
			boolean showRecent,
			String inventoryIdsList,
			Integer rangeId,
            Integer searchDays,
			Date baseDate,
			Integer inventoryId) {
		this.businessUnitId = businessUnitId;
		this.statusCodeList = statusCodeList;
		this.inventoryBucketId = inventoryBucketId;
		this.resultsMode = resultsMode;
		this.showRecent = showRecent;
		this.inventoryIdsList = inventoryIdsList;
		this.rangeId = rangeId;
        this.searchDays = searchDays;
		this.baseDate = baseDate;
		this.inventoryId = inventoryId;
		addPostProcessorFilter(new TradeOrPurchasePPFilter(true));
		addPostProcessorFilter(new BookVsCostDifferencePPFilter());
		this.rowMapper = new PlanningDetailRowMapper( getPostProcessorFilters());
	}

	public String fromClause() {
		return "{ call GetAgingInventoryPlanMultiSet @BusinessUnitID=?, @StatusCodeList=?, @InventoryBucketID=?, @RangeID=?, @ResultsMode=?, @ShowRecent=?, @InventoryIdsList=?, @BaseDate=?, @SearchDays=?, @InventoryID=?, @Debug=? }";
	}

	public String getReportName() {
		return REPORT_NAME;
	}

	public boolean isStoredProcedure() {
		return true;
	}

	public boolean isCallback() {
		return true;
	}
	
	public CallableStatementCallback getCallableStatementCallback() {
		return (CallableStatementCallback) rowMapper;
	}

	public List<? extends Object> getParameterList() {
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(new SqlParameter("@BusinessUnitID",    java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@StatusCodeList",    java.sql.Types.VARCHAR));
		parameters.add(new SqlParameter("@InventoryBucketID", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@RangeID",           java.sql.Types.TINYINT));
		parameters.add(new SqlParameter("@ResultsMode",       java.sql.Types.TINYINT));
		parameters.add(new SqlParameter("@ShowRecent",       java.sql.Types.BIT));
		parameters.add(new SqlParameter("@InventoryIdsList",       java.sql.Types.VARCHAR));
		parameters.add(new SqlParameter("@BaseDate",          java.sql.Types.TIMESTAMP));
        parameters.add(new SqlParameter("@SearchDays",        Types.INTEGER));        
		parameters.add(new SqlParameter("@InventoryID",       java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@Debug",             java.sql.Types.BIT));
		return parameters;
	}

	public Map<String, ? extends Object> getParameterMap() {
		Map<String,Object> parameters = new LinkedHashMap<String,Object>();
		parameters.put("@BusinessUnitID", businessUnitId);
		parameters.put("@StatusCodeList", (statusCodeList == null ? null : statusCodeList.replaceAll("'", "")));
		parameters.put("@InventoryBucketID", inventoryBucketId);
		parameters.put("@RangeID", rangeId);
		parameters.put("@ResultsMode", resultsMode);
		parameters.put("@ShowRecent", showRecent);
		parameters.put("@InventoryIdsList", inventoryIdsList);
		parameters.put("@BaseDate", Functions.midnight(baseDate));
        parameters.put("@SearchDays", searchDays);
		parameters.put("@InventoryID", inventoryId);
		parameters.put("@Debug", false);
		return parameters;
	}
	
}
