package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.postProcessorFilters.PerformanceBubbleToBeDisplayedFlagPPFilter;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

/**
 * this report builder is used to build the overall, segment level and model level performance bubbles
 * that appear on the inventory overview (and maybe other place eventually).  the information comes from the 
 * InventorySales_A2 table.
 * 
 * if the report is of type model level, a join is performed with vehicleGrouping_D to get the model name
 * it can be access in the results set using the key 'Description'
 */
public class PerformanceBubbleReportBuilder extends ReportBuilder {

public enum PerformanceBubbleReportLevelEnum{ OVERALL_STORE, VEHICLE_SEGMENT, VEHICLE_GROUPING}

// column names
private static final String VEHICLE_GROUPING_ID = "isA2.vehicleGroupingId";
private static final String VEHICLE_SEGMENT_ID = "isA2.vehicleSegmentId";
private static final String PERIOD_ID = "isA2.periodId";
private static final String BUSINESS_UNIT_ID = "isA2.businessUnitId";
private static final String GDLIGHT_BUSINESS_UNIT_ID = "gdLight.businessUnitId";
private static final String DESCRIPTION = "vg.Description";

private final PerformanceBubbleReportLevelEnum reportLevel;

public PerformanceBubbleReportBuilder( PerformanceBubbleReportLevelEnum reportLevel )
{
	super();
	this.reportLevel = reportLevel;
	
	addPostProcessorFilter( new PerformanceBubbleToBeDisplayedFlagPPFilter() );
	
	// setting defaults for overall, if not specified
	if ( reportLevel.equals( PerformanceBubbleReportLevelEnum.VEHICLE_GROUPING )) {
		addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.NOT_EQ, new Integer( 0 )) );
		orderBy = DESCRIPTION;
	}
	else {
		addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.EQ, new Integer( 0 ) ) );
		addWhereStatement( new SQLWhereSearchStatement( VEHICLE_SEGMENT_ID, SQLOperationEnum.EQ, new Integer( 0 ) ) );
	}
}

@Override
public String selectFields()
{
	if ( reportLevel == PerformanceBubbleReportLevelEnum.VEHICLE_GROUPING) {
		return "isA2.BusinessUnitID,isA2.PeriodID,isA2.VehicleSegmentID,isA2.VehicleGroupingID,isA2.UnitsSold,isA2.AGP,isA2.TotalGrossProfit,isA2.AvgDaysToSale,isA2.NoSaleUnits,isA2.UnitsInStock,isA2.AvgInventoryAge,DS.DaysSupply,isA2.GrossProfitPct,vg.VehicleGroupingID,vg.Description,gdLight.GroupingDescriptionLightId,gdLight.GroupingDescriptionId,gdLight.BusinessUnitID,gdLight.InventoryVehicleLightID,gdLight.GridCell";
	}
	else
	{
		return "isA2.BusinessUnitID,isA2.PeriodID,isA2.VehicleSegmentID,isA2.VehicleGroupingID,isA2.UnitsSold,isA2.AGP,isA2.TotalGrossProfit,isA2.AvgDaysToSale,isA2.NoSaleUnits,isA2.UnitsInStock,isA2.AvgInventoryAge,DS.DaysSupply,isA2.GrossProfitPct";
	}
}


public String fromClause() {
	StringBuilder fromStatment = new StringBuilder( "InventorySales_A2 isA2" );
	if ( reportLevel == PerformanceBubbleReportLevelEnum.VEHICLE_GROUPING) {
		StringBuilder joinClauses = new StringBuilder();
		joinClauses.append(" join VehicleGrouping_D vg on isA2.vehicleGroupingID = vg.vehicleGroupingID");
		joinClauses.append(" left join GDLight gdLight on isA2.vehicleGroupingID = gdLight.GroupingDescriptionID");
		joinClauses.append(" and gdLight.businessUnitId = isA2.businessUnitId");
		fromStatment.append(joinClauses);
	}
	fromStatment.append(" JOIN dbo.DaysSupply_A DS ON isA2.BusinessUnitID = DS.BusinessUnitID AND isA2.VehicleSegmentID = DS.VehicleSegmentID AND DS.AllVehicleGroups = 1");
	return fromStatment.toString();
}

public void setBusinessUnitId( Integer businessUnitId ) {
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );
}

public void setPeriodId( PeriodEnum period ) {
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.EQ, period.getPeriodId() ) );
}

public void setVehicleSegmentId( Integer vehicleSegmentId ) {
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_SEGMENT_ID, SQLOperationEnum.EQ, vehicleSegmentId ) );
}

@Override
public String getReportName() {
	return "PerformanceBubbleReport";
}

}
