package biz.firstlook.report.reportBuilders;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.postProcessorFilters.TradeOrPurchasePPFilter;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class OptimalInventorySummaryReportBuilder extends ReportBuilder
{

private DateFormat dateFormatter = DateFormat.getDateInstance( DateFormat.SHORT );

private Integer businessUnitId;
private Integer groupingDescriptionId;
private Integer year;
private String date;
private boolean includeLowUnitCost = false;

public OptimalInventorySummaryReportBuilder( Integer businessUnitId, Integer groupingDescriptionId, Integer year, boolean includeLowUnitCost )
{
	super();
	this.businessUnitId = businessUnitId;
	this.groupingDescriptionId = groupingDescriptionId;
	this.year = year;
	this.date = dateFormatter.format( new Date() );
	this.includeLowUnitCost = includeLowUnitCost;
	addPostProcessorFilter( new TradeOrPurchasePPFilter() );
}

public String fromClause()
{
	StringBuilder function = new StringBuilder();
	function.append( "GetInventoryOverviewPopupDetails( " );
	function.append( businessUnitId );
	function.append( ",  " );
	function.append( groupingDescriptionId );
	function.append( ", " );
	function.append( ( year != null ? year.toString() : "null" ) );
	function.append( ", " );
	if ( includeLowUnitCost )
	{
    	//don't use the filter!
		function.append( " 0" );
	}
	else
	{
		function.append( " 1" );
	}
	function.append( ", '" );
	function.append( date );
	function.append( "')" );

	return function.toString();
}

public String getOrderBy()
{
	String orderBy = "AgeInDays DESC";
	return orderBy;
}

public String getReportName()
{
	return "OptimalInventorySummaryReport";
}

public void selectMultiple( List groupingDescriptionIds )
{
	addWhereStatement( new SQLWhereSearchStatement( "VehicleGroupingID", SQLOperationEnum.IN, groupingDescriptionIds ) );
}

}