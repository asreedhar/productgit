package biz.firstlook.report.reportBuilders.aip;

import java.sql.Date;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public class PlanningSummaryReportBuilder extends ReportBuilder {
    private Integer businessUnitId;
    private String statusCodeList;
    private int inventoryBucketId;
    private java.sql.Date baseDate;
    private Integer resultsMode;
    private Integer searchDays;
    private String reportName = "SummaryReport";
    private static final boolean isDescending = false;

    public PlanningSummaryReportBuilder(Integer businessUnitId,
            String statusCodeList, int inventoryBucketId, Date baseDate, int searchDays,
            Integer resultsMode) {
        super();
        this.businessUnitId = businessUnitId;
        
        // if a string is present, wrap it in quotes
        if (StringUtils.isNotBlank( statusCodeList) )
        	statusCodeList = "'" + statusCodeList + "'";
        this.statusCodeList = statusCodeList;
        this.inventoryBucketId = inventoryBucketId;
        this.baseDate = baseDate;
        this.searchDays = searchDays;
        this.resultsMode = resultsMode;
    }

    public PlanningSummaryReportBuilder(Integer businessUnitId, Date baseDate) {
        super();
        this.businessUnitId = businessUnitId;
        this.baseDate = baseDate;
        this.statusCodeList = null;
        this.searchDays = 0;
        this.inventoryBucketId = InventoryBucketTypeEnum.NEW_AGING_INVENTORY_PLAN
                .getInventoryBucketId();

    }

    public String fromClause() {
        return "GetAgingInventoryPlanSummary(" + businessUnitId
                + "," + statusCodeList + "," + inventoryBucketId + "," + "'"
                + baseDate + "'," + searchDays + "," + resultsMode + ")";
    }

    public String getReportName() {
        return reportName;
    }

    @Override
    public String getOrderBy() {
        return " sortOrder ";
    }

    @Override
    public boolean isDesc() {
        return isDescending;
    }

    public Integer getResultsMode() {
        return resultsMode;
    }

    public void setResultsMode(Integer resultsMode) {
        this.resultsMode = resultsMode;
    }

    public Integer getSearchDays() {
        return searchDays;
    }

    public void setSearchDays(Integer searchDays) {
        this.searchDays = searchDays;
    }

}
