package biz.firstlook.report.reportBuilders;

import biz.firstlook.report.postProcessorFilters.AnnualROIPPFilter;
import biz.firstlook.report.postProcessorFilters.TradeOrPurchasePPFilter;

public class InventoryDetailReportBuilder extends ReportBuilder {

private Integer businessUnitId;

public InventoryDetailReportBuilder( Integer businessUnitId, boolean showAnnualRoi ) {
    super();
    this.businessUnitId = businessUnitId;
    addPostProcessorFilter( new TradeOrPurchasePPFilter() );
    if ( showAnnualRoi ) {
        addPostProcessorFilter( new AnnualROIPPFilter() );
    }
}

@Override
public String fromClause() {
    return "GetInventoryOverviewDetails( " + businessUnitId + " )";
}

@Override
public String getReportName() {
    return "InventoryDetailReport";
}

}
