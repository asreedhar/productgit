package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.main.enumerator.SaleTypeEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class PricingReportBuilder extends ReportBuilder
{

private SaleTypeEnum reportType;

private static final String BUSINESS_UNIT_ID = "pricing.BusinessUnitId";
private static final String SALE_TYPE_ID = "pricing.SaleTypeID";
private static final String VEHICLE_GROUPING_ID = "VH.VehicleGroupingID";
private static final String PERIOD_ID = "pricing.PeriodID";
private static final String MILEAGE_RANGE_ID = "pricing.MileageBucketID";
private static final String TRIM_ID = "VH.TrimID";
private static final String LEVEL_ID = "VH.Level";


public PricingReportBuilder( SaleTypeEnum reportType )
{
	super();
	this.reportType = reportType;
}

@Override
public String fromClause()
{
	return "Pricing_A1 pricing JOIN VehicleHierarchy_D VH ON pricing.VehicleHierarchyID = VH.VehicleHierarchyID";
}

@Override
public String getReportName()
{
	return "PricingReport";
}

public void setBusinessUnitId( Integer businessUnitId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );	
}

public void setSaleTypeId( Integer saleTypeId )
{
	addWhereStatement( new SQLWhereSearchStatement( SALE_TYPE_ID, SQLOperationEnum.EQ, saleTypeId ) );
}

public void setVehicleGroupingId( Integer vehicleGroupingId )
{
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.EQ, vehicleGroupingId ) );
}

public void setPeriodId( Integer periodId )
{
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.EQ, periodId ) );	
}

public void setMileageRangeId( Integer mileageRangeId )
{
	addWhereStatement( new SQLWhereSearchStatement( MILEAGE_RANGE_ID, SQLOperationEnum.EQ, mileageRangeId ) );
}

public void setTrimId( Integer trimId )
{
	addWhereStatement( new SQLWhereSearchStatement( TRIM_ID, SQLOperationEnum.EQ, trimId ) );
}

public void setLevel( Integer level )
{
	addWhereStatement( new SQLWhereSearchStatement( LEVEL_ID, SQLOperationEnum.EQ, level ) );
}

}
