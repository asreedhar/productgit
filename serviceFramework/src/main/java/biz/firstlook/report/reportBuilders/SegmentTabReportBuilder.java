package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class SegmentTabReportBuilder extends ReportBuilder
{

private static final String VEHICLE_GROUPING_ID = "isA2.vehicleGroupingId";
private static final String PERIOD_ID = "isA2.periodId";
private static final String UNITS_IN_STOCK_ID = "isA2.UnitsInStock";
private static final String BUSINESS_UNIT_ID = "isA2.businessUnitId";
private static final String GROSS_PROFIT = "isA2.GrossProfitPct";


private String reportName = "SegmentTabReport";


public SegmentTabReportBuilder()
{
	super();
	
	// VehicleGroupingID = 0 (Segment Level)
	// PeriodId = 26 weeks 
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.EQ, new Integer( 0 ) ) );
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.EQ, PeriodEnum.TWENTYSIX_WEEKS.getPeriodId() ) );
	// don't show tabs for segments with 0 units in stock - SF 12876
	addWhereStatement( new SQLWhereSearchStatement( UNITS_IN_STOCK_ID, SQLOperationEnum.NOT_EQ, new Integer( 0 )) );
	
	// add the following order by clause: 'order by isA2.GrossProfitPct desc'
	orderBy = GROSS_PROFIT;
	desc = true;
}

@Override
public String selectFields()
{
	return "isA2.BusinessUnitID,isA2.PeriodID,isA2.VehicleSegmentID,isA2.VehicleGroupingID,isA2.UnitsSold,isA2.AGP,isA2.TotalGrossProfit,isA2.AvgDaysToSale,isA2.NoSaleUnits,isA2.UnitsInStock,isA2.AvgInventoryAge,DS.DaysSupply,isA2.GrossProfitPct,vs.VehicleSegmentID,vs.Description"; 
}


@Override
public String fromClause()
{
	return "InventorySales_A2 isA2 join VehicleSegment_D vs on isA2.VehicleSegmentID = vs.VehicleSegmentID JOIN dbo.DaysSupply_A DS ON vs.VehicleSegmentID = DS.VehicleSegmentID AND isA2.BusinessUnitID = DS.BusinessUnitID AND DS.AllVehicleGroups = 1"; 
}


@Override
public String getReportName()
{
	return reportName;
}


public void setBusinessUnitId( Integer businessUnitId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );	
}

}
