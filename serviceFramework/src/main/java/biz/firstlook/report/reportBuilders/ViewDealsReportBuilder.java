package biz.firstlook.report.reportBuilders;

import java.util.List;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class ViewDealsReportBuilder extends ReportBuilder
{
private final String BUSINESS_UNIT_ID = "BusinessUnitId";
private final String GROUPING_DESCRIPTION_ID = "GroupingDescriptionId";
private final String DEAL_DATE = "DealDate";
private final String SALE_DESCRIPTION = "SaleDescription";
private final String INVENTORY_TYPE = "InventoryType";
private final String VEHICLE_MILEAGE = "VehicleMileage";
private final String VEHICLE_TRIM = "VehicleTrim";
private final String DEALER_GROUP_ID = "ParentId";
private final String DAYS_TO_SALE = "DaysToSale";

public ViewDealsReportBuilder( List dateRange, List mileageRange, String saleType )
{
	addWhereStatement( new SQLWhereSearchStatement ( DEAL_DATE, SQLOperationEnum.BETWEEN, dateRange ) );
	addWhereStatement( new SQLWhereSearchStatement ( VEHICLE_MILEAGE, SQLOperationEnum.BETWEEN, mileageRange ) );
	addWhereStatement( new SQLWhereSearchStatement ( SALE_DESCRIPTION, SQLOperationEnum.EQ, saleType ) );	
	addWhereStatement( new SQLWhereSearchStatement ( INVENTORY_TYPE, SQLOperationEnum.EQ, 2 ) ); //2 is enum for used car
}

public String selectFields()
{
	StringBuilder select = new StringBuilder();
	select.append( "FrontEndGross, DealDate, FinanceInsuranceDealNumber, VehicleMileage, " );
	select.append( "BaseColor, VehicleYear, VehicleTrim, " );
	select.append( "Model, Make, Descriptions, UnitCost, ");
	select.append( "TradeOrPurchase, DaysToSale, BusinessUnit" );
	
	return select.toString();
}

public String fromClause()
{
	String from = "InventorySales";
	return from;
}

public String getReportName()
{
	return "ViewDealsReportBuilder";
}

public void setTrim( String trim )
{
	addWhereStatement( new SQLWhereSearchStatement ( VEHICLE_TRIM, SQLOperationEnum.EQ, trim ) );
}

public void setGroupingDescriptionId( Integer groupingDescId )
{
	addWhereStatement( new SQLWhereSearchStatement ( GROUPING_DESCRIPTION_ID, SQLOperationEnum.EQ, groupingDescId ) );
}

public void setInventoryType( Integer inventoryType )
{
	addWhereStatement( new SQLWhereSearchStatement ( INVENTORY_TYPE, SQLOperationEnum.EQ, inventoryType ) );
}

public void setBusinessUnitId( Integer dealerId )
{
	addWhereStatement( new SQLWhereSearchStatement ( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, dealerId ) );
}

public void setParentId( Integer parentId )
{
	addWhereStatement( new SQLWhereSearchStatement ( DEALER_GROUP_ID, SQLOperationEnum.EQ, parentId ) );
}

public void setNoSales( )
{
	addWhereStatement( new SQLWhereSearchStatement ( SALE_DESCRIPTION, SQLOperationEnum.EQ, "W" ) );
	addWhereStatement( new SQLWhereSearchStatement ( DAYS_TO_SALE, SQLOperationEnum.GTE, 30 ) );
}

}
