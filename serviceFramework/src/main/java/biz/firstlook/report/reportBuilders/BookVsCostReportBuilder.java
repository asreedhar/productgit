package biz.firstlook.report.reportBuilders;

import java.sql.SQLException;
import java.util.List;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.report.postProcessorFilters.BookVsCostDifferencePPFilter;
import biz.firstlook.report.postProcessorFilters.ThirdPartyCategoryPPFilter;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class BookVsCostReportBuilder extends ReportBuilder
{

private static final String BUSINESS_UNIT_ID = "businessUnitId";
private static final String THIRD_PARTY_CATEGORY_ID = "thirdPartyCategoryId";
private static final String INVENTORY_TYPE_ID = "InventoryTypeID";
private static final int USED_TYPE = 2;

public BookVsCostReportBuilder()
{
	super();
	addPostProcessorFilter( new ThirdPartyCategoryPPFilter() );
	addPostProcessorFilter( new BookVsCostDifferencePPFilter() );
    //Some stores send new car data, we want used here.  
    addWhereStatement( new SQLWhereSearchStatement( INVENTORY_TYPE_ID, SQLOperationEnum.EQ, USED_TYPE ) );
}

@Override
public String fromClause()
{
	return "InventoryBookout_A1";
}

public void setBusinessUnitId( Integer businessUnitId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );
}

public void setThirdPartyCategoryIds( List<ThirdPartyCategoryEnum> thirdPartyCategoryIds ) throws SQLException
{
	addWhereStatement( new SQLWhereSearchStatement( THIRD_PARTY_CATEGORY_ID, SQLOperationEnum.IN, thirdPartyCategoryIds) );
}

@Override
public String getReportName()
{
	return "BookVsCostReport";
}

}
