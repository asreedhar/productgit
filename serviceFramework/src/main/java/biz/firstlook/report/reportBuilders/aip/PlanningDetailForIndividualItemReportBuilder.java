package biz.firstlook.report.reportBuilders.aip;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.report.postProcessorFilters.BookVsCostDifferencePPFilter;
import biz.firstlook.report.postProcessorFilters.TradeOrPurchasePPFilter;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public class PlanningDetailForIndividualItemReportBuilder extends ReportBuilder
{
	private static final String REPORT_NAME = "InventoryPlanDetailForIndividualItem";
	private Integer inventoryId;
	private Integer businessUnitId;
	

	public PlanningDetailForIndividualItemReportBuilder( Integer inventoryId, Integer businessUnitId )	{
		super();
		this.inventoryId = inventoryId;
		this.businessUnitId = businessUnitId;
		addPostProcessorFilter(new TradeOrPurchasePPFilter(true));
		addPostProcessorFilter(new BookVsCostDifferencePPFilter());
		this.rowMapper = new PlanningDetailRowMapper( getPostProcessorFilters());

	}

	@Override
	public String fromClause() {
		return "{ call GetAgingInventoryPlanForSingleInventoryItemMultiSet @BusinessUnitID=?, @InventoryID=? }";
	}

	@Override
	public String getReportName() {
		return REPORT_NAME;
	}
	
	public boolean isStoredProcedure() {
		return true;
	}

	public boolean isCallback() {
		return true;
	}
	
	public CallableStatementCallback getCallableStatementCallback() {
		return (CallableStatementCallback) rowMapper;
	}

	public List<? extends Object> getParameterList() {
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(new SqlParameter("@BusinessUnitID",    java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@InventoryID",       java.sql.Types.INTEGER));
		return parameters;
	}

	public Map<String, ? extends Object> getParameterMap() {
		Map<String,Object> parameters = new LinkedHashMap<String,Object>();
		parameters.put("@BusinessUnitID", businessUnitId);
		parameters.put("@InventoryID", inventoryId);
		return parameters;
	}
	
}
