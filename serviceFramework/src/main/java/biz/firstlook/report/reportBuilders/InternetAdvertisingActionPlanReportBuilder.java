package biz.firstlook.report.reportBuilders;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

public class InternetAdvertisingActionPlanReportBuilder extends
		ReportBuilder {
	
	public static final String REPORT_HEADER_NAME = "InternetAdvertisingActionPlanReportBuilderHeader";
	public static final String REPORT_BODY_NAME = "InternetAdvertisingActionPlanReportBuilderBody";
	
	static final String REPORT_NAME = "InternetAdvertisingActionPlan";
	private int businessUnitId;
	
	public InternetAdvertisingActionPlanReportBuilder(Integer dealerId) {
		super();
		this.businessUnitId = dealerId;
		this.rowMapper = new InventoryInternetAdvertisementRowMapper();
	}
	

	@Override
	public String fromClause() {
		return "{ ? = call dbo.GetInternetAdvertisementActionPlan(?) }";
	}

	@Override
	public String getReportName() {
		return REPORT_NAME;
	}

	public boolean isStoredProcedure() {
		return true;
	}

	public boolean isCallback() {
		return true;
	}
	
	public CallableStatementCallback getCallableStatementCallback() {
		return (CallableStatementCallback) rowMapper;
	}

	public List<? extends Object> getParameterList() {
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(new SqlOutParameter("@RC", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@BusinessUnitID", java.sql.Types.INTEGER));
		return parameters;
	}

	public Map<String, ? extends Object> getParameterMap() {
		Map<String,Object> parameters = new LinkedHashMap<String,Object>();
		parameters.put("@BusinessUnitID", businessUnitId);
		return parameters;
	}
	
	class InventoryInternetAdvertisementRowMapper implements ResultSetExtractor, CallableStatementCallback {

		public Object doInCallableStatement(CallableStatement cs) 
			throws SQLException, DataAccessException {
			//do lots of lists of maps ...
			List<Map<String, Object>> internetAdvertiserProviders = new ArrayList<Map<String,Object>>();
			if(cs.execute()) {
				ResultSet rs = cs.getResultSet();
				while(rs.next()) {
					String providerName = rs.getString("Provider");
					Map<String, Object> provider = new HashMap<String, Object>();
					provider.put(providerName, providerName);
					internetAdvertiserProviders.add(provider);
				}
			} else {
				throw new DataRetrievalFailureException("Expected a result set!");
			}
			
			List<Map<String, Object>> inventoryList = new ArrayList<Map<String,Object>>();
			if(cs.getMoreResults()) {
				ResultSet rs = cs.getResultSet();
				while(rs.next()) {
					Map<String, Object> invItem = new HashMap<String, Object>();
					invItem.put("vin", rs.getString("VIN"));
					invItem.put("stockNumber", rs.getString("StockNumber"));
					invItem.put("mileageReceived", rs.getInt("MileageReceived"));
					invItem.put("vehicleYear", rs.getInt("VehicleYear"));
					invItem.put("make", rs.getString("Make"));
					invItem.put("groupingDescription", rs.getString("Model"));
					invItem.put("vehicleTrim", rs.getString("VehicleTrim"));
					invItem.put("color", rs.getString("BaseColor"));
					invItem.put("notes", rs.getString("AdvertisementText"));
					invItem.put("age", rs.getInt("AgeInDays"));
					invItem.put("listPrice", rs.getFloat("InternetPrice"));
					int doorCount = rs.getInt("DoorCount");
					String doorCountDesc = null;
					if(doorCount > 0) {
						doorCountDesc = Integer.toString(doorCount) + "DR";
					} else {
						doorCountDesc = "";
					}
					invItem.put("doorCount", doorCountDesc);
					inventoryList.add(invItem);
				}
			} else {
				throw new DataRetrievalFailureException("Expected the 2nd result set!");
			}
			Map<String, List<Map<String, Object>>> reportSections = new HashMap<String, List<Map<String,Object>>>();
			reportSections.put(REPORT_HEADER_NAME, internetAdvertiserProviders);
			reportSections.put(REPORT_BODY_NAME, inventoryList);
			
			List<Map> theReport = new ArrayList<Map>();
			theReport.add(reportSections);
			return theReport;
		}
		
		public Object extractData(ResultSet rs) throws SQLException,
				DataAccessException {
			return null; //does nothing, due to isCallback() == true
		}
	}
}
