package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class OverallInventoryInformationReportBuilder extends ReportBuilder
{

private static final String PERIOD_ID = "A.periodId";
private static final String BUSINESS_UNIT_ID = "A.businessUnitId";

@Override
public String selectFields()
{
	return "A.BusinessUnitID,A.PeriodID,A.RetailUnitsSold,A.RetailAGP,A.RetailAvgDaysToSale,A.RetailAvgMileage,A.UnitsInStock,A.NoSaleUnits,A.RetailAvgBackEnd,A.RetailTotalBackEnd,A.RetailTotalFrontEnd,A.RetailTotalSalesPrice,A.RetailTotalRevenue,A.TotalInventoryDollars,A.DistinctModelsInStock,A.AvgInventoryAge,DS.DaysSupply,A.SellThroughRate,A.WholesalePerformance,A.RetailPurchaseProfit,A.RetailTradeProfit,A.RetailPurchaseAGP,A.RetailPurchaseAvgDaysToSale,A.RetailPurchaseSellThrough,A.RetailPurchaseRecommendationsFollowed,A.RetailPurchaseAvgNoSaleLoss,A.RetailTradeAGP,A.RetailTradeAvgDaysToSale,A.RetailTradeSellThrough,A.RetailTradesAnalyzed,A.RetailTradePercentAnalyzed,A.RetailTradeAvgNoSaleLoss,A.RetailTradeAvgFlipLoss"; 
}

@Override
public String fromClause()
{
	return "InventorySales_A1 A JOIN dbo.DaysSupply_A DS ON A.BusinessUnitID = DS.BusinessUnitID AND DS.VehicleSegmentID = 0 AND DS.VehicleGroupingID = -1";
}

public void setBusinessUnitId( Integer businessUnitId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );
}

public void setPeriodId( PeriodEnum periodId )
{
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.EQ, periodId.getPeriodId() ) );
}

@Override
public String getReportName()
{
	return "OverallInventoryInformationReport";
}

}
