package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class ThirdPartyVehicleOptionsReportBuilder extends ReportBuilder{

    private final String INVENTORY_ID = "I.InventoryID";
    private final String THIRD_PARTY_ID = "TP.ThirdPartyID";
    private final String TPVO_STATUS = "TPVO.Status";
    
    private Integer thirdPartyId;
    private Integer inventoryId;
    
    public ThirdPartyVehicleOptionsReportBuilder() {
        super();
        addWhereStatement( new SQLWhereSearchStatement( TPVO_STATUS, SQLOperationEnum.EQ, 1 ) );
    }
    
    public ThirdPartyVehicleOptionsReportBuilder(Integer thirdPartyId) {
        super();
        this.thirdPartyId = thirdPartyId;
        addWhereStatement( new SQLWhereSearchStatement( TPVO_STATUS, SQLOperationEnum.EQ, 1 ) );
    }
    
    public String selectFields()
    {
        return "DISTINCT TPO.optionName as name";
    }
    
    public String fromClause() {
        StringBuilder fromClause = new StringBuilder();
    	fromClause.append(" ( ");
    	fromClause.append(" SELECT MAX(B.BookoutID) BookoutID ");
    	fromClause.append(" FROM InventoryBookouts IB ");
    	fromClause.append(" JOIN Bookouts B ON IB.BookoutID = B.BookoutID ");
    	fromClause.append(" WHERE IB.InventoryID = ").append( inventoryId );
    	fromClause.append(" AND B.ThirdPartyID = ").append( thirdPartyId );
    	fromClause.append(" ) B ");
    	fromClause.append(" JOIN BookoutThirdPartyVehicles BTPV on BTPV.BookoutID = B.BookoutID ");
        fromClause.append("JOIN ThirdPartyVehicles TPV on TPV.ThirdPartyVehicleID = BTPV.ThirdPartyVehicleID ");
        fromClause.append("JOIN ThirdPartyVehicleOptions TPVO on TPVO.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID ");
        fromClause.append("JOIN ThirdPartyOptions TPO on TPO.ThirdPartyOptionID = TPVO.ThirdPartyOptionID ");
        fromClause.append("JOIN ThirdParties TP on TP.ThirdPartyID = TPV.ThirdPartyID ");
        fromClause.append("JOIN ThirdPartyVehicleOptionValues TPVOV on TPVOV.ThirdPartyVehicleOptionID = TPVO.ThirdPartyVehicleOptionID ");
        
        return fromClause.toString();
    }

    public String getReportName() {
        return "ThirdPartyVehicleOptionsReport";
    }
    
    public void setInventoryId(Integer invId) {
    	this.inventoryId = invId;
    }

}
