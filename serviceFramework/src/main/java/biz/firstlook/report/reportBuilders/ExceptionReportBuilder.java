package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class ExceptionReportBuilder extends ReportBuilder{

    protected final String LISTED_PRICE = "IBLD.ListedPrice";
    protected final String DATAFEED_CODE = "ITPE.DatafeedCode";
    protected final String BUSINESS_UNIT_ID = "I.BusinessUnitID";
    protected final String BUILD_LOG_ID = "IBL.InternetAdvertiserBuildLogId";
    protected final String EXPORTING = "IAD.IsLive";
    //0 for listed price means it was not sent or sent as null
    protected final int EXCEPTION_CODE = 0;
    //Only want for stores actively exporting.
    protected final int IS_LIVE = 1;
    
    public ExceptionReportBuilder() {
        addWhereStatement(new SQLWhereSearchStatement(LISTED_PRICE, SQLOperationEnum.EQ, EXCEPTION_CODE));
        addWhereStatement(new SQLWhereSearchStatement(EXPORTING, SQLOperationEnum.EQ, IS_LIVE));
    }
    
    public String selectFields()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("IBL.Created, IBLD.SendZeroesAsNull, V.VehicleYear, MMG.Make, ");
        sb.append("MMG.OriginalModel, V.VehicleTrim, V.BaseColor, I.MileageReceived, ");
        sb.append("I.StockNumber, V.Vin, TPE.Name, IAD.ContactEmail");
        return sb.toString();
    }
    
    public String fromClause() {
        StringBuilder fromClause = new StringBuilder();
        fromClause.append("InternetAdvertiserBuildLog IBL ");
        fromClause.append("JOIN InternetAdvertiserBuildLogDetail IBLD ON IBL.InternetAdvertiserBuildLogID = IBLD.InternetAdvertiserBuildLogID ");
        fromClause.append("JOIN Inventory I ON IBLD.InventoryID = I.InventoryID ");
        fromClause.append("JOIN Vehicle V on I.VehicleID = V.VehicleId ");
        fromClause.append("JOIN MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID ");
        fromClause.append("JOIN dbo.InternetAdvertiserDealership IAD ON I.BusinessUnitID = IAD.BusinessUnitID ");
        fromClause.append("JOIN InternetAdvertiser_ThirdPartyEntity ITPE ON IAD.InternetAdvertiserID = ITPE.InternetAdvertiserID ");
        fromClause.append("JOIN ThirdpartyEntity TPE ON ITPE.InternetAdvertiserID = TPE.ThirdPartyEntityID ");
        
        return fromClause.toString();
    }

    public String getOrderBy()
    {   
        StringBuilder orderByStatement = new StringBuilder("IBL.Created DESC");
        return orderByStatement.toString();
    }
    
    public String getReportName() {
        return "exceptionReportBuilder";
    }
    
    public void addBusinessUnit(Integer businessUnitId) {
        addWhereStatement(new SQLWhereSearchStatement(BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId));
    }
    
    public void addDatafeedCode(String datafeedCode) {
        addWhereStatement(new SQLWhereSearchStatement(DATAFEED_CODE, SQLOperationEnum.EQ, datafeedCode));
    }
    
    public void addBuildLogId(Integer buildLogId) {
        addWhereStatement(new SQLWhereSearchStatement(BUILD_LOG_ID, SQLOperationEnum.EQ, buildLogId));
    }

}
