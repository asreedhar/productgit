package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

/**
 * this report includes days supply, units in stock and avg inv. age
 * that is displayed on the inventory overview for the selected tab only
 */
public class SegmentDetailReportBuilder extends ReportBuilder
{

//column names
private static final String VEHICLE_GROUPING_ID = "isA2.vehicleGroupingId";
private static final String VEHICLE_SEGMENT_ID = "isA2.vehicleSegmentId";
private static final String PERIOD_ID = "isA2.periodId";
private static final String BUSINESS_UNIT_ID = "isA2.businessUnitId";

public SegmentDetailReportBuilder()
{
	super();

	// default values
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.EQ, new Integer( 0 ) ) );
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.EQ, PeriodEnum.TWENTYSIX_WEEKS.getPeriodId() ) );

	// nk-dw: Now getting Vehicle Segment Description with Join rather than PostProcessor
	// postProcessorFilters.add( new VehicleSegmentPPFilter() );
}

@Override
public String selectFields()
{
	return "isA2.BusinessUnitID,isA2.PeriodID,isA2.VehicleSegmentID,isA2.VehicleGroupingID,isA2.UnitsSold,isA2.AGP,isA2.TotalGrossProfit,isA2.AvgDaysToSale,isA2.NoSaleUnits,isA2.UnitsInStock,isA2.AvgInventoryAge,DS.DaysSupply,isA2.GrossProfitPct,vs.VehicleSegmentID,vs.Description";
}

@Override
public String fromClause()
{
	return "InventorySales_A2 isA2 join VehicleSegment_D vs on isA2.VehicleSegmentID = vs.VehicleSegmentID JOIN dbo.DaysSupply_A DS ON isA2.BusinessUnitID = DS.BusinessUnitID AND isA2.VehicleSegmentID = DS.VehicleSegmentID AND DS.AllVehicleGroups = 1";
}

@Override
public String getReportName()
{
	return "SegmentDetailReport";
}

public void setBusinessUnitId( Integer businessUnitId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );	
}

public void setVehicleSegmentId( Integer vehicleSegmentId )
{
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_SEGMENT_ID, SQLOperationEnum.EQ, vehicleSegmentId ) );
}


}
