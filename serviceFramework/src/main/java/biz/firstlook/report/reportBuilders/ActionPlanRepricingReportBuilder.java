package biz.firstlook.report.reportBuilders;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class ActionPlanRepricingReportBuilder extends ReportBuilder
{

private final String EVENT_TYPE_ID = "aipEvent.AIP_EventTypeID";
private final String END_DATE = "aipEvent.EndDate";
private final String INVENTORY_ID = "aipEvent.inventoryId";
private final String CREATED_DATE = "aipEvent.created";
private final String INNER_EVENT_TYPE_ID = "E.AIP_EventTypeID";
private final String INNER_CREATED_DATE = "E.created";
private final String OWNER_TYPE = "ot.ownerTypeId";

private final Integer DEALER_OWNER_TYPE = new Integer(1);
 
private String whereClauseArgForInnerJoin;
/**
 * Holds the sql and sql parameters for actionReports (AIP).
 * 
 * @param businessUnitId
 *            dealerId
 * @param eventTypeId
 *            the PlanningEventType to retrieve from. The report types need to map to one of the PlanningEventTypes
 */
@SuppressWarnings("unchecked")
public ActionPlanRepricingReportBuilder( Map inventoryIds, ActionPlanReportModeEnum actionPlanReportMode, Integer createdDaysBack )
{
	// 5 is the enum for reprice
	ArrayList lister = new ArrayList(inventoryIds.values());
	addWhereStatement( new SQLWhereSearchStatement( EVENT_TYPE_ID, SQLOperationEnum.EQ, PlanningEventTypeEnum.REPRICE.getId() ) );
	addWhereStatement( new SQLWhereSearchStatement( INVENTORY_ID, SQLOperationEnum.IN, lister ) );
	Set< SQLWhereSearchStatement > ownerTypeOrClause = new HashSet< SQLWhereSearchStatement >();
	ownerTypeOrClause.add( new SQLWhereSearchStatement( OWNER_TYPE, SQLOperationEnum.EQ, DEALER_OWNER_TYPE ) );
	ownerTypeOrClause.add( new SQLWhereSearchStatement( OWNER_TYPE, SQLOperationEnum.IS_NULL, DEALER_OWNER_TYPE ) );
	addWhereStatement( new SQLWhereSearchStatement( OWNER_TYPE, SQLOperationEnum.OR,ownerTypeOrClause, DEALER_OWNER_TYPE ) );

	SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy"); 
	switch( actionPlanReportMode )
	{
		case CURRENT:
			Date endDate = DateUtils.truncate( new Date(), Calendar.DATE );
			Set< SQLWhereSearchStatement > orClause = new HashSet< SQLWhereSearchStatement >();
			orClause.add( new SQLWhereSearchStatement( END_DATE, SQLOperationEnum.IS_NULL, endDate ) );
			orClause.add( new SQLWhereSearchStatement( END_DATE, SQLOperationEnum.GTE, endDate ) );
			whereClauseArgForInnerJoin = "";
			break;
		case HISTORY:
			Calendar cal = DateUtils.truncate( Calendar.getInstance(), Calendar.DATE );
			cal.add( Calendar.DATE, -(createdDaysBack.intValue()) );
			addWhereStatement( new SQLWhereSearchStatement( CREATED_DATE, SQLOperationEnum.GTE, cal.getTime() ));
			whereClauseArgForInnerJoin = " AND " + INNER_CREATED_DATE + " >= '" + sdf.format( cal.getTime() ) + "'" ;
			break;
	}
}

@Override
public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	sb.append("C.inventoryId,");
	sb.append("aipEventMax.value,");
	sb.append("C.dayCreated, aipEventMax.created, C.Events, ");
	sb.append("i.stockNumber, i.mileageReceived, ");
	sb.append("convert(int, convert(float,aipEvent.Notes2)) originalPrice,");
	sb.append("datediff( dd, i.InventoryReceivedDate, aipEvent.created ) as age, ");
	sb.append( " v.vehicleYear, v.make, v.model as groupingDescription, v.vehicleTrim" );
	sb.append( ", cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, v.baseColor as color" );
	sb.append( ", pdi.Notes as notes " );
	return sb.toString();
}

@Override
public String fromClause()
{
	StringBuilder fromStatement = new StringBuilder( "AIP_Event aipEvent " );
    fromStatement.append(" JOIN AIP_Event aipEventMax on aipEventMax.inventoryId = aipEvent.inventoryId ");
	fromStatement.append(" JOIN Inventory i on aipEvent.inventoryID = i.inventoryID");
	fromStatement.append(" JOIN Vehicle v on i.vehicleId = v.vehicleId" );
	fromStatement.append(" JOIN (" +
	                     "	SELECT InventoryID, dbo.ToDate(E.created) AS DayCreated, COUNT(*) as Events, Max(E.aip_eventId) maxEventId, Min(E.AIP_eventId) minEventId    " +
 						 "	FROM AIP_Event E " +
 						 "	WHERE " + INNER_EVENT_TYPE_ID + " = " + PlanningEventTypeEnum.REPRICE.getId() + whereClauseArgForInnerJoin +
 						 " 	GROUP BY InventoryID, dbo.ToDate(E.created)) C " +
						 "	ON C.InventoryID = I.InventoryID " +
						 "	AND aipEvent.aip_eventId = C.minEventId " +
                         "   AND aipEventMax.aip_eventId = C.maxEventId");
	fromStatement.append( " JOIN MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId" );
	fromStatement.append( " LEFT JOIN [Market].Pricing.vehiclePricingDecisionInput pdi ON pdi.VehicleEntityID=I.InventoryID" );
	fromStatement.append( " LEFT JOIN [Market].Pricing.Owner o on pdi.ownerId=o.ownerId AND o.ownerEntityID=I.businessUnitID" );
	fromStatement.append( " LEFT JOIN [Market].Pricing.OwnerType ot on ot.ownerTypeId=o.ownerTypeID" );
	return fromStatement.toString();
}

@Override
public String getOrderBy()
{
	return "dayCreated desc, age desc, i.inventoryId, aipEvent.created desc";
}

@Override
public String getReportName()
{
	return "ActionPlanRepricingReportBuilder";
}

}
