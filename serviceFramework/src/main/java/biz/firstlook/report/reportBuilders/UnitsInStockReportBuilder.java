package biz.firstlook.report.reportBuilders;

public class UnitsInStockReportBuilder extends ReportBuilder
{
public static final String REPORT_NAME = "UnitsInStockReport";
private Integer businessUnitId;
private String trim;
private Integer includeDealerGroup;
private Integer inventoryType;
private String make;
private String model;

public UnitsInStockReportBuilder( Integer businessUnitId, String trim, Integer includeDealerGroup, Integer inventoryType, String make, String model )
{
	super();
	this.businessUnitId = businessUnitId;
	this.trim = trim;
	this.includeDealerGroup = includeDealerGroup;
	this.inventoryType = inventoryType;
	this.make = make;
	this.model = model;
}

@Override
public String fromClause()
{
	StringBuffer fromClause = new StringBuffer();
	fromClause.append( "GetVehicleUnitsInStock " );
	fromClause.append( businessUnitId );
	fromClause.append( "," );
	fromClause.append( inventoryType );
	fromClause.append( ",'" );
	fromClause.append( make );
	fromClause.append( "','" );
	fromClause.append( model );
	fromClause.append( "','" );
	fromClause.append( trim );
	fromClause.append( "'," );
	fromClause.append( includeDealerGroup );
	return fromClause.toString();
}

public boolean isStoredProcedure() {
	return true;
}

@Override
public String getReportName()
{
	return REPORT_NAME;
}

}
