package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;

public class AuctionAndWholeSalerActionPlanReportBuilder extends
        ActionPlanReportBuilder {
    private Integer bookPref1;
    private Integer bookPref2;
    private String innerWhereClause;

    public AuctionAndWholeSalerActionPlanReportBuilder(Integer businessUnitId,Integer primaryGuideBookFirstPreferenceId, Integer eventTypeId,
            ActionPlanReportModeEnum actionPlanReportMode,Integer createdDaysBack) {
        super(businessUnitId, primaryGuideBookFirstPreferenceId, eventTypeId,actionPlanReportMode, createdDaysBack);
    }

    public AuctionAndWholeSalerActionPlanReportBuilder(Integer businessUnitId,Integer primaryGuideBookFirstPreferenceId, Integer eventTypeId,
            ActionPlanReportModeEnum actionPlanReportMode,Integer createdDaysBack, Integer firstPrefId, Integer secondPrefId) {
        super(businessUnitId, primaryGuideBookFirstPreferenceId, eventTypeId,actionPlanReportMode, createdDaysBack);
        this.bookPref1 = firstPrefId;
        this.bookPref2 = secondPrefId;
        this.innerWhereClause = " bookoutValue.ThirdPartyCategoryID in (" + bookPref1 + ", " + bookPref2 + ") ";
    }

    public String selectFields() {
        StringBuilder sb = new StringBuilder();
        sb.append(" v.vin, v.vehicleYear, v.make, v.model as groupingDescription, v.vehicleTrim");
        sb.append(", cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, v.baseColor as color");
        sb.append(", datediff( dd, i.InventoryReceivedDate, getDate() ) as age, i.inventoryId, i.unitCost, i.stockNumber");
        sb.append(", i.mileageReceived, i.currentVehicleLight, i.inventoryActive, i.listPrice, i.InventoryReceivedDate,");
        sb.append(" aipEvent.thirdPartyEntityId as thirdPartyId, ");
        sb.append(" CASE WHEN aipEvent.thirdPartyEntityId is null "
                + "	THEN aipEvent.notes3 " + "ELSE tp.name "
                + "	END as thirdParty");
        sb.append(", aipEvent.beginDate, aipEvent.endDate, aipEvent.created, aipEvent.notes2");
        sb.append(", datediff( dd, aipEvent.created, getDate() ) as createdAge");
        sb.append(", aipEvent.notes ");
        sb.append(", CASE WHEN c.primaryValue is null ");
        sb.append(" THEN 0 ELSE c.primaryValue END as bookoutValue ");
        if (bookPref2 != null) {
            sb.append(", CASE WHEN c.secondaryValue is null ");
            sb.append(" THEN 0 ELSE c.secondaryValue END as secondBookoutValue");
        }
        return sb.toString();
    }

    public String fromClause() {
        StringBuilder fromStatement = new StringBuilder("AIP_Event aipEvent");
        fromStatement.append(" join Inventory i on aipEvent.inventoryId = i.inventoryId");
        fromStatement.append(" join Vehicle v on i.vehicleId = v.vehicleId");
        fromStatement.append(" join MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId");
        fromStatement.append(" left join ThirdPartyEntity tp on tp.ThirdPartyEntityId = aipEvent.ThirdPartyEntityId");
        fromStatement.append(" left join ( ");
        fromStatement.append(" select inventoryId, ");
        fromStatement.append(" SUM(CASE WHEN ThirdPartyCategoryID = ");
        fromStatement.append(bookPref1.toString());
        fromStatement.append(" THEN Value ELSE 0 END) PrimaryValue");
        if (bookPref2 != null) {
            fromStatement.append(", SUM(CASE WHEN ThirdPartyCategoryID = ");
            fromStatement.append(bookPref2.toString());
            fromStatement.append(" THEN Value ELSE 0 END) SecondaryValue");
        }
        fromStatement.append(" from InventoryBookoutCurrentValue bookoutValue where");
        fromStatement.append(innerWhereClause);
        fromStatement.append(" Group by InventoryId) C on C.InventoryId = I.InventoryId");
        return fromStatement.toString();
    }

    @Override
    public String getOrderBy() {
        return "notes3, thirdParty, endDate, notes2";
    }

    @Override
    public String getReportName() {
        return "WholeSalerActionPlanReport";
    }

}
