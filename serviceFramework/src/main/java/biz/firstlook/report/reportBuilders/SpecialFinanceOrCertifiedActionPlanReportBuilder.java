package biz.firstlook.report.reportBuilders;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.postProcessorFilters.SpecialFinanceOrCertifiedReportNotesPPFilter;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class SpecialFinanceOrCertifiedActionPlanReportBuilder extends ReportBuilder
{

public enum ReportTypeEnum
{
	SPECIAL_FINANCE,
	CERTIFIED
}
	
private String endDate;

public SpecialFinanceOrCertifiedActionPlanReportBuilder( Integer businessUnitId, ActionPlanReportModeEnum actionPlanReportMode, Integer createdDaysBack, ReportTypeEnum specialFinanceOrCertified )
{
	super();

	addWhereStatement( new SQLWhereSearchStatement( "i.businessUnitId", SQLOperationEnum.EQ, businessUnitId ) );
	addWhereStatement( new SQLWhereSearchStatement( "i.inventoryType", SQLOperationEnum.EQ, 2 ) );

	switch ( specialFinanceOrCertified )
	{
		case SPECIAL_FINANCE:
			addWhereStatement( new SQLWhereSearchStatement( "i.specialFinance", SQLOperationEnum.EQ, 1 ) );
			break;
		case CERTIFIED:
			addWhereStatement( new SQLWhereSearchStatement( "i.Certified", SQLOperationEnum.EQ, 1 ) );
			break;
	}
	
	switch( actionPlanReportMode )
	{
		case CURRENT:
			addWhereStatement( new SQLWhereSearchStatement( "i.inventoryActive", SQLOperationEnum.EQ, 1  ) );
			break;
		case HISTORY:
			Calendar cal = DateUtils.truncate( Calendar.getInstance(), Calendar.DATE );
			cal.add( Calendar.DATE, -( createdDaysBack.intValue() ) );
			Set< SQLWhereSearchStatement > orClause2 = new HashSet< SQLWhereSearchStatement >();
			orClause2.add( new SQLWhereSearchStatement( "i.deleteDt", SQLOperationEnum.GT, cal.getTime() ) );
			orClause2.add( new SQLWhereSearchStatement( "i.deleteDt", SQLOperationEnum.IS_NULL, null ) );
			addWhereStatement( new SQLWhereSearchStatement( "i.deleteDt", SQLOperationEnum.OR, orClause2, null ) );
			break;
	}

	SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd" );
	this.endDate = format.format( DateUtils.truncate( new Date(), Calendar.DATE ) );

	addPostProcessorFilter( new SpecialFinanceOrCertifiedReportNotesPPFilter() );
}

public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	sb.append( "v.vin, v.vehicleYear, v.make, v.model as groupingDescription, v.vehicleTrim" );
	sb.append( ", cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, v.baseColor as color" );
	sb.append( ", datediff( dd, i.InventoryReceivedDate, getDate() ) as age, i.inventoryId, i.unitCost, i.stockNumber" );
	sb.append( ", i.mileageReceived, i.currentVehicleLight, i.inventoryActive, i.listPrice" );
	sb.append( ", RE.notes, RE.description as retailStrats, RE.category, RE.ThirdParty, RE.BeginDate" );
	return sb.toString();
}

@Override
public String fromClause()
{
	StringBuilder fromStatement = new StringBuilder( "Inventory i" );
	fromStatement.append( " join Vehicle v on i.vehicleId = v.vehicleId" );
	fromStatement.append( " join MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId" );
	fromStatement.append( " left join ( ");
	fromStatement.append( " select i.inventoryId, aipEvent.notes, eventType.description, eventCategory.Description as category, tp.name as ThirdParty, aipEvent.BeginDate" );
	fromStatement.append( " from AIP_Event aipEvent " );
	fromStatement.append( " left join AIP_EventType eventType on aipEvent.AIP_EventTypeID = eventType.AIP_EventTypeID ");
	fromStatement.append( " left join ThirdPartyEntity tp on tp.ThirdPartyEntityID = aipEvent.ThirdPartyEntityID ");
	fromStatement.append( " join Inventory i on i.inventoryId = aipEvent.inventoryId ");
	fromStatement.append( "join AIP_EventCategory eventcategory on eventcategory.AIP_EventCategoryID = eventType.AIP_EventCategoryID" );
	fromStatement.append( " where (aipEvent.endDate is null or aipEvent.endDate >= ").append( endDate ).append( " ) and aipEvent.AIP_EventTypeID <> 14 ");
	fromStatement.append( " ) RE on i.inventoryId = RE.inventoryId");
	
	return fromStatement.toString();
}

@Override
public String getReportName()
{
	return "InventoryActionPlansReport";
}

}
