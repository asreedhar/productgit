package biz.firstlook.report.reportBuilders;

public class SelectedVehicleOptionsReportBuilder extends ReportBuilder
{
static final String REPORT_NAME = "SelectedVehicleOptionsReport";
private Integer inventoryId;
private Short guidebookId;

public SelectedVehicleOptionsReportBuilder( Integer inventoryId )
{
	super();
	this.inventoryId = inventoryId;
}

@Override
public String fromClause()
{
	return "GetSelectedVehicleOptions(" + inventoryId + ")";
}

@Override
public String getReportName()
{
    return REPORT_NAME;
}

public Short getGuidebookId()
{
	return guidebookId;
}

public void setGuidebookId( Short guidebookId )
{
	this.guidebookId = guidebookId;
}

public Integer getInventoryId()
{
	return inventoryId;
}

public void setInventoryId( Integer inventoryId )
{
	this.inventoryId = inventoryId;
}

}
