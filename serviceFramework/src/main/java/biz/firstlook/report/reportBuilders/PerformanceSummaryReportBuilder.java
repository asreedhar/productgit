package biz.firstlook.report.reportBuilders;

public class PerformanceSummaryReportBuilder extends ReportBuilder
{
public static final String REPORT_NAME = "PerformanceSummaryTrim";
static final Integer DEBUG_OFF = 0;

private Integer businessUnitId;
private Integer weeks;
private String make;
private String model;
private Integer includeDealerGroup;
private Integer inventoryType;
private String trim;

public PerformanceSummaryReportBuilder( Integer businessUnitId, Integer weeks, String make, String model, Integer includeDealerGroup, Integer inventoryType, String trim )
{
    super();
	this.businessUnitId = businessUnitId;
    this.weeks = weeks;
    this.make = make;
    this.model = model;
    this.includeDealerGroup = includeDealerGroup;
    this.inventoryType = inventoryType;
    this.trim = trim;
}

@Override
public String fromClause()
{
	StringBuffer fromClause = new StringBuffer();
	fromClause.append( "GetTradeAnalyzerReport " );
	fromClause.append( businessUnitId );
	fromClause.append( "," );
	fromClause.append( weeks );
	fromClause.append( ",'" );
	fromClause.append( make );
	fromClause.append( "','" );
	fromClause.append( model );
	fromClause.append( "'," );
	fromClause.append( includeDealerGroup );
	fromClause.append( "," );
	fromClause.append( inventoryType );
	fromClause.append( "," );
	
	if( trim != null )
	{
		fromClause.append( "'" );
	    fromClause.append( trim );
	    fromClause.append( "'" );
	}
	else
	{
	    fromClause.append( "null" );
	}
	
	fromClause.append( "," );
	fromClause.append( DEBUG_OFF );
	return fromClause.toString();
}

public boolean isStoredProcedure() {
	return true;
}

@Override
public String getReportName()
{
	return REPORT_NAME;
}

}
