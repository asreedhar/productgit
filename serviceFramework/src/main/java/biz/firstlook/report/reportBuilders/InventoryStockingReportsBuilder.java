package biz.firstlook.report.reportBuilders;

import java.text.DateFormat;
import java.util.Date;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.postProcessorFilters.StockingReportFilter;
import biz.firstlook.report.postProcessorFilters.VehicleSegmentPPFilter;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class InventoryStockingReportsBuilder extends ReportBuilder{
	private final static int ALL_YEARS = 0;
    private DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);

    private Integer businessUnitId;
    private Integer strategyId;
    private String date;
    private String reportName;
    private boolean includeLowUnitCost = false;
    
    public InventoryStockingReportsBuilder(Integer businessUnitId, Integer strategyId, boolean includeLowUnitCost) {
        super();
        this.businessUnitId = businessUnitId;
        this.strategyId = (strategyId != 0 ? strategyId:null);
        this.date = dateFormatter.format(new Date());
        this.includeLowUnitCost = includeLowUnitCost;
        addPostProcessorFilter( new VehicleSegmentPPFilter() );
        addPostProcessorFilter(new StockingReportFilter());
    }

    public String fromClause() {
        StringBuilder function = new StringBuilder();
        function.append("GetStockingReportCube( ");
        function.append(businessUnitId);
        function.append(", ");
        function.append(strategyId);
        function.append(", ");
        if ( includeLowUnitCost )
    	{
        	//if include low cost, don't use the filter!
    		function.append( " 0" );
    	}
    	else
    	{
    		function.append( " 1" );
    	}
        function.append(", '");
        function.append(date);
        function.append("') SR ");
        function.append("LEFT JOIN VehicleGrouping_D VG ON SR.VehicleGroupingID = VG.VehicleGroupingID");
        return function.toString();
    }

    public String getOrderBy()
    {   
        String orderBy = "SR.VehicleSegmentId ASC,SR.VehicleYear desc, SR.CIACategoryId";
        return orderBy;
    }
    
    public String getReportName() {
        return reportName;
    }

    public void selectVehicleSegment(Integer segment) {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.VehicleSegmentId", SQLOperationEnum.EQ, segment ) );
    }
    public void selectGroupingId(Integer groupingDescriptionId) {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.VehicleGroupingId", SQLOperationEnum.EQ, groupingDescriptionId ) );
    }
    public void removeGroupingId(Integer groupingDescriptionId) {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.VehicleGroupingId", SQLOperationEnum.NOT_EQ, groupingDescriptionId ) );
    }
    public void selectOthers() {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.CIACategoryId", SQLOperationEnum.EQ, 0 ) );
    }
    public void removeOthers() {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.CIACategoryId", SQLOperationEnum.NOT_EQ, 0 ) );
    }
    public void selectAllYears() {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.VehicleYear", SQLOperationEnum.EQ, ALL_YEARS ) );
    }
    public void removeAllYears() {
        addWhereStatement( new SQLWhereSearchStatement ( "SR.VehicleYear", SQLOperationEnum.NOT_EQ, ALL_YEARS ) );
    }
    public void modifyStock(boolean over) {
    	addWhereStatement( new SQLWhereSearchStatement( "(SR.UnitsInStock - SR.TargetUnits)", (over ? SQLOperationEnum.GT: SQLOperationEnum.LT), 0));
    }
}
