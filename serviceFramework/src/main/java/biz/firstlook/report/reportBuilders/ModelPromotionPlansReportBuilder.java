package biz.firstlook.report.reportBuilders;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class ModelPromotionPlansReportBuilder extends ReportBuilder
{
// beginDateColumn should be replaced with create date column when we have enough data. -bf.
private String beginDateColumn;
private String endDateColumn;

public ModelPromotionPlansReportBuilder( Integer businessUnitId, ActionPlanReportModeEnum actionPlanReportMode, Integer createdDaysBack )
{
	super();

	beginDateColumn = "gp.promotionStartDate";
	endDateColumn = "gp.promotionEndDate";

	switch ( actionPlanReportMode )
	{
		case CURRENT:
			Date endDate = DateUtils.truncate( new Date(), Calendar.DATE );
			Set< SQLWhereSearchStatement > orClause = new HashSet< SQLWhereSearchStatement >();
			orClause.add( new SQLWhereSearchStatement( endDateColumn, SQLOperationEnum.IS_NULL, endDate ) );
			orClause.add( new SQLWhereSearchStatement( endDateColumn, SQLOperationEnum.GTE, endDate ) );
			addWhereStatement( new SQLWhereSearchStatement( endDateColumn, SQLOperationEnum.OR, orClause, null ) );
			addWhereStatement( new SQLWhereSearchStatement( "i.inventoryActive", SQLOperationEnum.EQ, 1 ) );
			break;
		case HISTORY:
			Calendar cal = DateUtils.truncate( Calendar.getInstance(), Calendar.DATE );
			cal.add( Calendar.DATE, -( createdDaysBack.intValue() ) );
			addWhereStatement( new SQLWhereSearchStatement( beginDateColumn, SQLOperationEnum.GTE, cal.getTime() ) );
			break;
	}

	addWhereStatement( new SQLWhereSearchStatement( "i.businessUnitId", SQLOperationEnum.EQ, businessUnitId ) );
	addWhereStatement( new SQLWhereSearchStatement( "i.inventoryType", SQLOperationEnum.EQ, 2 ) );
}

public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	sb.append( "v.vin, v.vehicleYear, v.make, v.model as groupingDescription, v.vehicleTrim" );
	sb.append( ", cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, v.baseColor as color" );
	sb.append( ", datediff( dd, i.InventoryReceivedDate, getDate() ) as age, i.inventoryId, i.unitCost, i.stockNumber" );
	sb.append( ", i.mileageReceived, i.currentVehicleLight, i.inventoryActive, i.listPrice" );
	sb.append( ", " ).append( beginDateColumn ).append( ", " ).append( endDateColumn ).append( ", gp.notes" );
	return sb.toString();
}

public String fromClause()
{
	StringBuilder fromStatement = new StringBuilder( "InventoryOverview i" );
	fromStatement.append( " join Vehicle v on i.vehicleId = v.vehicleId" );
	fromStatement.append( " join MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId" );
	fromStatement.append( " join GroupingDescription gd on mmg.GroupingDescriptionId = gd.GroupingDescriptionId" );
	fromStatement.append( " join tbl_GroupingPromotionPlan gp on i.BusinessUnitId = gp.BusinessUnitId and gd.GroupingDescriptionId = gp.GroupingDescriptionId" );

	return fromStatement.toString();

}

@Override
public String getOrderBy()
{
	return "i.InventoryReceivedDate";
}

public String getReportName()
{
	return "ModelPromotionPlansReport";
}

}
