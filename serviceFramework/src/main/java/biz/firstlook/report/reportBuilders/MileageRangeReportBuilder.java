package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class MileageRangeReportBuilder extends ReportBuilder
{

private static final String BUCKET_DXM_ID = "Bucket_DXM.BucketDXMID";
private static final String BUCKET_TYPE_ID = "Bucket_DXM.BucketID";
private static final int MILEAGE_RANGE_BUCKET_TYPE = 1;


public MileageRangeReportBuilder()
{
	super();
	addWhereStatement( new SQLWhereSearchStatement(BUCKET_TYPE_ID, SQLOperationEnum.EQ, MILEAGE_RANGE_BUCKET_TYPE) );
}

@Override
public String fromClause()
{
	return "Bucket_DXM";
}

@Override
public String getReportName()
{
	return "MileageRangeReport";
}

public void setBucketDXMId( Integer bucketDXMId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUCKET_DXM_ID, SQLOperationEnum.EQ, bucketDXMId ) );	
}

}
