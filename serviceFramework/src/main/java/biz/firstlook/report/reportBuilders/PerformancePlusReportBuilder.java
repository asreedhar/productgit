package biz.firstlook.report.reportBuilders;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.main.enumerator.PerformancePlusDimensionEnum;
import biz.firstlook.report.reportBeans.PerformancePlusTable;
import biz.firstlook.report.reportBeans.PerformancePlusTableRow;

public class PerformancePlusReportBuilder extends ReportBuilder {

	static final String REPORT_NAME = "PerformancePlusReport";
	
	private PerformancePlusDimensionEnum dimension;
	private int businessUnitID;
	private int weeks;
	private int groupingDescriptionID;
	private int inventoryTypeID;
	private boolean applyMileageFilter;
	private boolean applyUnitCostFilter;
	private boolean isForecast;
	
	public PerformancePlusReportBuilder(PerformancePlusDimensionEnum dimension, int businessUnitID, int weeks, int groupingDescriptionID, int inventoryTypeID, boolean applyMileageFilter, boolean applyUnitCostFilter, boolean isForecast) {
		super();
		this.dimension = dimension; 
		this.businessUnitID = businessUnitID;
		this.weeks = weeks;
		this.groupingDescriptionID = groupingDescriptionID;
		this.inventoryTypeID = inventoryTypeID;
		this.applyMileageFilter = applyMileageFilter;
		this.applyUnitCostFilter = applyUnitCostFilter;
		this.isForecast = isForecast;
		this.rowMapper = new PerformancePlusVehicleDetailRowMapper();
	}

	@Override
	public String fromClause() {
		return "{ ? = call getPerformanceAnalyzerPlusMultiSet(?,?,?,?,?,?,?,?,?,?,?,?) }";
	}

	@Override
	public String getReportName() {
		return REPORT_NAME + (dimension == null ? "" : Integer.toString(dimension.getId()));
	}

	public boolean isStoredProcedure() {
		return true;
	}

	public boolean isCallback() {
		return true;
	}
	
	public CallableStatementCallback getCallableStatementCallback() {
		return (CallableStatementCallback) rowMapper;
	}

	public List<? extends Object> getParameterList() {
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(new SqlOutParameter("@RC", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@BusinessUnitID", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@GroupingDescriptionID", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@Make", java.sql.Types.VARCHAR));
		parameters.add(new SqlParameter("@Model", java.sql.Types.VARCHAR));
		parameters.add(new SqlParameter("@VehicleTrim", java.sql.Types.VARCHAR));
		parameters.add(new SqlParameter("@SegmentID", java.sql.Types.VARCHAR));
		parameters.add(new SqlParameter("@InventoryTypeID", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@FilterMileage", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@FilterUnitCost", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@Weeks", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@LastYear", java.sql.Types.INTEGER));
		parameters.add(new SqlParameter("@GroupingParameterID", java.sql.Types.INTEGER));
		return parameters;
	}

	public Map<String, ? extends Object> getParameterMap() {
		Map<String,Object> parameters = new LinkedHashMap<String,Object>();
		parameters.put("@BusinessUnitID", businessUnitID);
		parameters.put("@GroupingDescriptionID", groupingDescriptionID);
		parameters.put("@Make", null);
		parameters.put("@Model", null);
		parameters.put("@VehicleTrim", null);
		parameters.put("@SegmentID", null);
		parameters.put("@InventoryTypeID", inventoryTypeID);
		parameters.put("@FilterMileage", (applyMileageFilter ? 1 : 0));
		parameters.put("@FilterUnitCost", (applyUnitCostFilter ? 1 : 0));
		parameters.put("@Weeks", (isForecast ? 13 : weeks));
		parameters.put("@LastYear", (isForecast ? 1 : 0));
		parameters.put("@GroupingParameterID", (dimension == null ? null : dimension.getId()));
		return parameters;
	}

	/**
	 * <p>Maps the columns in a result set into a Performance Plus price-point table.</p>
	 * <p>The table is returned as the sole element of a list which is a map whose key
	 * is the dimension enumeration value. This is due to the type-restrictions of the
	 * new reporting framework which (generally) frowns upon structuring results in
	 * beans.</p>
	 * @author swenmouth
	 */
	class PerformancePlusVehicleDetailRowMapper implements ResultSetExtractor, CallableStatementCallback {

		PerformancePlusDimensionEnum dimensionAlias = dimension;
		boolean encapsulateInList = (dimension != null);
		
		@SuppressWarnings("unchecked")
		public Object doInCallableStatement(CallableStatement statement) throws SQLException, DataAccessException {
			statement.execute();
			if (dimensionAlias == null) {
				Map<PerformancePlusDimensionEnum,PerformancePlusTable> tableMap = new HashMap<PerformancePlusDimensionEnum,PerformancePlusTable>();
				dimensionAlias = PerformancePlusDimensionEnum.PRICE;
				tableMap.putAll((Map<PerformancePlusDimensionEnum,PerformancePlusTable>) extractData(statement.getResultSet()));
				if (statement.getMoreResults()) {
					dimensionAlias = PerformancePlusDimensionEnum.TRIM;
					tableMap.putAll((Map<PerformancePlusDimensionEnum,PerformancePlusTable>) extractData(statement.getResultSet()));
				}
				if (statement.getMoreResults()) {
					dimensionAlias = PerformancePlusDimensionEnum.YEAR;
					tableMap.putAll((Map<PerformancePlusDimensionEnum,PerformancePlusTable>) extractData(statement.getResultSet()));
				}
				if (statement.getMoreResults()) {
					dimensionAlias = PerformancePlusDimensionEnum.COLOR;
					tableMap.putAll((Map<PerformancePlusDimensionEnum,PerformancePlusTable>) extractData(statement.getResultSet()));
				}
				if (statement.getMoreResults()) {
					dimensionAlias = PerformancePlusDimensionEnum.ALL;
					tableMap.putAll((Map<PerformancePlusDimensionEnum,PerformancePlusTable>) extractData(statement.getResultSet()));
				}
				List<Map<PerformancePlusDimensionEnum,PerformancePlusTable>> tableMaps = new ArrayList<Map<PerformancePlusDimensionEnum,PerformancePlusTable>>();
				tableMaps.add(tableMap);
				return tableMaps;
			}
			else {
				return extractData(statement.getResultSet());
			}
		}

		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List<PerformancePlusTableRow> rows = new ArrayList<PerformancePlusTableRow>(10);
			String groupingDescription = null;
			while (rs.next()) {
				if (dimensionAlias == PerformancePlusDimensionEnum.ALL) {
					groupingDescription = rs.getString("GroupingColumn");
				}
				rows.add(new PerformancePlusTableRow(
					rs.getString("GroupingColumn"),
					rs.getInt("LowerBound"),
					rs.getInt("UpperBound"),
					rs.getInt("UnitsSold"),
					rs.getInt("AverageGrossProfit"),
					rs.getInt("TotalGrossMargin"),
					rs.getInt("AverageDays"),
					rs.getInt("AverageMileage"),
					rs.getInt("TotalDays"),
					rs.getInt("UnitsInStock"),
					rs.getInt("NoSales"),
					rs.getInt("TotalInventoryDollars"),
					rs.getInt("TotalRevenue"),
					rs.getInt("AverageBackEnd"),
					rs.getInt("MarketShareDeals"),
					rs.getDouble("AnnualROI"),
					rs.getDouble("TotalFrontEndGross"),
					rs.getDouble("TotalBackEndGross"),
					rs.getDouble("PercentageTotalGrossMargin"),
					rs.getDouble("PercentageTotalRevenue"),
					rs.getDouble("PercentageTotalInventoryDollars"),
					rs.getDouble("PercentageMarketShare")
				));
			}
			PerformancePlusTable table = new PerformancePlusTable(dimensionAlias, (dimensionAlias == PerformancePlusDimensionEnum.YEAR), rows, groupingDescription);
			Map<PerformancePlusDimensionEnum,PerformancePlusTable> tableMap = new HashMap<PerformancePlusDimensionEnum,PerformancePlusTable>();
			tableMap.put(table.getDimension(), table);
			if (encapsulateInList) {
				List<Map<PerformancePlusDimensionEnum,PerformancePlusTable>> tableMaps = new ArrayList<Map<PerformancePlusDimensionEnum,PerformancePlusTable>>();
				tableMaps.add(tableMap);
				return tableMaps;
			}
			else {
				return tableMap;
			}
		}
	}
}
