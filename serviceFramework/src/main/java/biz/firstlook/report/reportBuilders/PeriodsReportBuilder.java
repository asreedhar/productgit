package biz.firstlook.report.reportBuilders;

import java.util.List;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class PeriodsReportBuilder extends ReportBuilder
{

private static final String PERIOD_ID = "periodId";

public PeriodsReportBuilder()
{
	super();
}

@Override
public String fromClause()
{
	return "Period_D";
}

@Override
public String getReportName()
{
	return "PeriodsReport";
}

public void setPeriodIds( List< PeriodEnum > periodIds )
{
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.IN, periodIds ) );
}

public void setPeriodId( Integer periodId )
{
	addWhereStatement( new SQLWhereSearchStatement( PERIOD_ID, SQLOperationEnum.EQ, periodId ) );
}

}
