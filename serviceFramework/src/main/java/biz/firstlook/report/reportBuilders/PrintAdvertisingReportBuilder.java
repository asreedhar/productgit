package biz.firstlook.report.reportBuilders;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.postProcessorFilters.ListPriceZerosBecomeDashesPPFilter;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class PrintAdvertisingReportBuilder extends ReportBuilder
{

public PrintAdvertisingReportBuilder( Integer businessUnitId, Integer printAdvertiserId, ActionPlanReportModeEnum actionPlanReportMode,
                                      Integer createdDaysBack )
{
	addWhereStatement( new SQLWhereSearchStatement("aipEvent.AIP_EventTypeID", SQLOperationEnum.EQ, PlanningEventTypeEnum.ADVERTISEMENT.getId() ));
	addWhereStatement( new SQLWhereSearchStatement("i.businessUnitId", SQLOperationEnum.EQ, businessUnitId) );
	addWhereStatement( new SQLWhereSearchStatement("i.inventoryActive", SQLOperationEnum.EQ, new Integer(1)) );
	addWhereStatement( new SQLWhereSearchStatement("i.inventoryType", SQLOperationEnum.EQ, new Integer(2)) );
	
	if ( printAdvertiserId != null && printAdvertiserId == 0)
	{
		// for other (one time use) third parties, the thridPartyEntity id is null, but gets passed back from .jsp as 0
		addWhereStatement( new SQLWhereSearchStatement("aipEvent.thirdPartyEntityId", SQLOperationEnum.IS_NULL, null) );
	}
	else if ( printAdvertiserId != null )
	{
		// if printAdvertiserId exists, means report for specific dealer, if no printADvertiser
		// then you want to display all third parties
		addWhereStatement( new SQLWhereSearchStatement("aipEvent.thirdPartyEntityId", SQLOperationEnum.EQ, printAdvertiserId) );
	}
	
	switch( actionPlanReportMode )
	{
		case CURRENT:
			Date today = DateUtils.truncate( new Date(), Calendar.DATE );
			addWhereStatement( new SQLWhereSearchStatement("aipEvent.EndDate", SQLOperationEnum.GTE, today ) );
			break;
		case HISTORY:
			Calendar cal = DateUtils.truncate( Calendar.getInstance(), Calendar.DATE );
			cal.add( Calendar.DATE, -( createdDaysBack.intValue() ) );
			addWhereStatement( new SQLWhereSearchStatement( "aipEvent.created", SQLOperationEnum.GTE, cal.getTime() ) );
			break;
	}

	addPostProcessorFilter( new ListPriceZerosBecomeDashesPPFilter() );
	
}

public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	sb.append(" v.vin, v.vehicleYear, v.make, v.model as groupingDescription, ");
	sb.append(" v.vehicleTrim, cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, ");
	sb.append(" v.baseColor as color, i.stockNumber, i.mileageReceived, i.inventoryActive," );
	sb.append(" aipEvent.endDate, aipEvent.notes," );
	sb.append(" CASE WHEN aipEvent.thirdPartyEntityId is null " +
			  "			THEN aipEvent.notes2 " +
			  "			ELSE tp.name " +
			  "	END as thirdParty," );
	sb.append(" CASE WHEN aipEvent.notes3 = 'true' " +
			"			THEN CASE WHEN aipEvent.value = 1 and ia.AdPrice is not null " );
	sb.append(" 					THEN ia.AdPrice " +
			"						WHEN i.listPrice <> 0 THEN i.listPrice " +
			"					END " +
			"			ELSE null " +
			"	 END as listPrice, " );
	sb.append( "pa_tp.contactEmail as email, pa_tp.faxNumber as fax, tp.thirdPartyEntityID as thirdPartyId" );
	return sb.toString();
}

@Override
public String fromClause()
{
	StringBuilder sb = new StringBuilder();
	sb.append("	AIP_Event aipEvent ");
	sb.append("	join Inventory i on aipEvent.inventoryId = i.inventoryId ");
	sb.append("	join Vehicle v on i.vehicleId = v.vehicleId ");
	sb.append("	join MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId ");
	sb.append("	left join ThirdPartyEntity tp on tp.ThirdPartyEntityId = aipEvent.ThirdPartyEntityId ");
	sb.append("	left join PrintAdvertiser_ThirdPartyEntity pa_tp on pa_tp.printAdvertiserId = tp.ThirdPartyEntityId");
	sb.append("	join dealerPreference dp on dp.businessUnitId = i.businessUnitId ");
	sb.append("	left join inventory_Advertising ia on ia.inventoryId = i.inventoryId ");
	return sb.toString();
}

@Override
public String getReportName()
{
	return "PrintAdvertisingReportBuilder";
}

}
