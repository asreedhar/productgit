package biz.firstlook.report.reportBuilders;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

/**
 * This report builder is used for the following reports:
 * <ul>
 * <li>advertising</li>
 * <li>auction</li>
 * <li>lot promotion</li>
 * <li>service department</li>
 * <li>spiff</li>
 * <li>wholesaler</li>
 * </ul>
 * 
 * @author bfung
 * 
 */
public class ActionPlanReportBuilder extends ReportBuilder
{

protected final String EVENT_TYPE_ID = "aipEvent.AIP_EventTypeID";
protected final String END_DATE = "aipEvent.EndDate";
protected final String BUSINESS_UNIT_ID = "i.businessUnitId";
protected final String INVENTORY_TYPE = "i.inventoryType";
protected final String INVENTORY_ACTIVE = "i.inventoryActive";
protected final String LAST_MODIFIED_DATE = "aipEvent.lastModified";
protected final String BOOKOUT_PREFERENCE_ID = "bookout.thirdPartyCategoryId";

// this is a field becuase it set in the constructor but use in the fromClause
private Integer primaryGuideBookFirstPreferenceId;
private boolean makeDistinct = false;

/**
 * Holds the sql and sql parameters for actionReports (AIP).
 * 
 * @param businessUnitId
 *            dealerId
 * @param eventTypeId
 *            the PlanningEventType to retrieve from. The report types need to map to one of the PlanningEventTypes
 */
public ActionPlanReportBuilder( Integer businessUnitId, Integer primaryGuideBookFirstPreferenceId, Integer eventTypeId, ActionPlanReportModeEnum actionPlanReportMode, Integer createdDaysBack )
{
	this.primaryGuideBookFirstPreferenceId = primaryGuideBookFirstPreferenceId; 
	
	// 2 is the enum for used car;
	addWhereStatement( new SQLWhereSearchStatement( INVENTORY_TYPE, SQLOperationEnum.EQ, 2 ) );
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );
	addWhereStatement( new SQLWhereSearchStatement( EVENT_TYPE_ID, SQLOperationEnum.EQ, eventTypeId ) );
	
	switch( actionPlanReportMode )
	{
		case CURRENT:
			addWhereStatement( new SQLWhereSearchStatement ( INVENTORY_ACTIVE, SQLOperationEnum.EQ, 1) );
			Date endDate = DateUtils.truncate( new Date(), Calendar.DATE );
			SQLWhereSearchStatement endDateIsNull = new SQLWhereSearchStatement( END_DATE, SQLOperationEnum.IS_NULL, endDate );
			if ( eventTypeId.intValue() !=  PlanningEventTypeEnum.REPRICE.getId() )
			{
				if(eventTypeId.equals(PlanningEventTypeEnum.SPIFF.getId())) {
					addWhereStatement(endDateIsNull);
				} else {
					Set< SQLWhereSearchStatement > orClause = new HashSet< SQLWhereSearchStatement >();
					orClause.add( endDateIsNull );
					orClause.add( new SQLWhereSearchStatement( END_DATE, SQLOperationEnum.GTE, endDate ) );
					addWhereStatement( new SQLWhereSearchStatement( END_DATE, SQLOperationEnum.OR, orClause, endDate ) );
				}
			}
			break;
		case HISTORY:
			Calendar cal = DateUtils.truncate( Calendar.getInstance(), Calendar.DATE );
			cal.add( Calendar.DATE, -( createdDaysBack.intValue() ) );
			addWhereStatement( new SQLWhereSearchStatement( LAST_MODIFIED_DATE, SQLOperationEnum.GTE, cal.getTime() ) );
			break;
	}

}

public ActionPlanReportBuilder( Integer businessUnitId, Integer primaryGuideBookFirstPreferenceId, Integer eventTypeId, ActionPlanReportModeEnum actionPlanReportMode, Integer createdDaysBack, boolean makeDistinct )
{
	this(businessUnitId, primaryGuideBookFirstPreferenceId, eventTypeId, actionPlanReportMode, createdDaysBack);
	this.makeDistinct = makeDistinct;
}

@Override
/**
 * MakeDistinct is currently only used by repricing action plan. 
 * It results in only distinct result, one per inventory item being returned
 */
public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	if (makeDistinct) {
		sb.append(" distinct ");
	}
	sb.append( " v.vin, v.vehicleYear, v.make, v.model as groupingDescription, v.vehicleTrim" );
	sb.append( ", cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, v.baseColor as color" );
	sb.append( ", datediff( dd, i.InventoryReceivedDate, getDate() ) as age, i.inventoryId, i.unitCost, i.stockNumber" );
	sb.append( ", i.mileageReceived, i.currentVehicleLight, i.inventoryActive, i.listPrice, i.InventoryReceivedDate" );
	if (!makeDistinct) {
		// for repricing. these fields are what cause dups
		sb.append( ", aipEvent.beginDate, aipEvent.endDate, aipEvent.created, aipEvent.notes2, aipEvent.notes3 ");
	}
	sb.append(", datediff( dd, aipEvent.created, getDate() ) as createdAge" );
	sb.append( ", aipEvent.notes, tp.name as thirdParty" );
	sb.append( ", bookout.value as bookoutValue" );
	return sb.toString();
}

@Override
public String fromClause()
{
	StringBuilder fromStatement = new StringBuilder( "AIP_Event aipEvent" );
	fromStatement.append( " join Inventory i on aipEvent.inventoryId = i.inventoryId" );
	fromStatement.append( " join Vehicle v on i.vehicleId = v.vehicleId" );
	fromStatement.append( " join MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId" );
	fromStatement.append( " left join ThirdPartyEntity tp on tp.ThirdPartyEntityId = aipEvent.ThirdPartyEntityId");
	fromStatement.append( " left join InventoryBookoutCurrentValue bookout" );
	fromStatement.append( " on i.inventoryId = bookout.inventoryId and bookout.thirdPartyCategoryId = ").append( primaryGuideBookFirstPreferenceId.toString() );
	return fromStatement.toString();
}

@Override
public String getOrderBy()
{	
	StringBuilder orderByStatement = new StringBuilder( "i.InventoryReceivedDate, v.model, v.vehicleYear, v.vehicleTrim");
	if (!makeDistinct) {
		orderByStatement.append(", aipEvent.created, aipEvent.beginDate");
	}
	return orderByStatement.toString();
}

@Override
public String getReportName()
{
	return "ActionPlanReportBuilder";
}

}
