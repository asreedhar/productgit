package biz.firstlook.report.reportBuilders;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class UnchangedActionPlanReportBuilder extends ReportBuilder
{

private final String BUSINESS_UNIT_ID = "i.businessUnitId";
private final String CREATED_DATE = "aipEvent.created";
private final String INVENTORY_TYPE = "i.inventoryType";
private final String INVENTORY_ACTIVE = "i.inventoryActive";
private final String AIP_EVENT_CATEGORY_ID = "aipEventType.AIP_EventCategoryId";

// this exists becuase this value is set in the constructor but used in fromClause
private Integer book1Pref1Id;
private Integer book1Pref2Id;
private String innerWhereClause;

public UnchangedActionPlanReportBuilder( Integer businessUnitId, Integer primaryGuideBookFirstPreferenceId, Integer primaryGuideBookSecondPreferenceId )
{
	super();
	this.book1Pref1Id = primaryGuideBookFirstPreferenceId;
    this.book1Pref2Id = primaryGuideBookSecondPreferenceId;
    this.innerWhereClause = " bookoutValue.ThirdPartyCategoryID in (" + book1Pref1Id + ", " + book1Pref2Id + ") ";
	addWhereStatement( new SQLWhereSearchStatement( INVENTORY_TYPE, SQLOperationEnum.EQ, 2 ) );
	addWhereStatement( new SQLWhereSearchStatement( INVENTORY_ACTIVE, SQLOperationEnum.EQ, 1 ) );

	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );

	Date createdDate = DateUtilFL.addDaysToDate( DateUtils.truncate( new Date(), Calendar.DATE ), -7 );
	addWhereStatement( new SQLWhereSearchStatement( CREATED_DATE, SQLOperationEnum.LT, createdDate ) );
	
	List<String> inClause = new ArrayList<String>();
	inClause.add( new Integer( PlanObjectiveEnum.WHOLESALE.getId() ).toString() );
	inClause.add( new Integer( PlanObjectiveEnum.SOLD.getId() ).toString() );
	inClause.add( new Integer( PlanObjectiveEnum.OTHER.getId() ).toString() );
	addWhereStatement( new SQLWhereSearchStatement( AIP_EVENT_CATEGORY_ID, SQLOperationEnum.IN, inClause) );
}


@Override
public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	sb.append( "v.vehicleYear, v.make, v.model as groupingDescription, v.vehicleTrim" );
	sb.append( ", cast( v.DoorCount as varchar(1) ) + 'DR' as doorCount, v.baseColor as color" );
	sb.append( ", datediff( dd, i.InventoryReceivedDate, getDate() ) as age, i.inventoryId, i.unitCost, i.stockNumber" );
	sb.append( ", i.mileageReceived, i.currentVehicleLight, i.listPrice" );
	sb.append( ", aipEvent.created, datediff( dd, aipEvent.created, getDate() ) as createdAge" );
	sb.append( ", aipEvent.notes, aipEvent.notes2, tp.name as thirdParty" );
	sb.append( ", aipEventType.description as strategy, aipEventCategory.description as objective " );
    sb.append( ", c.primaryValue as bookoutValue ");
    if (book1Pref2Id != null) {
        sb.append(", c.secondaryValue as secondBookoutValue");
    }
	return sb.toString();
}

@Override
public String fromClause()
{
	StringBuilder fromStatement = new StringBuilder( "AIP_Event aipEvent" );
	fromStatement.append( " join AIP_EventType aipEventType on aipEvent.AIP_EventTypeID = aipEventType.AIP_EventTypeID" );
	fromStatement.append( " join AIP_EventCategory aipEventCategory on aipEventType.AIP_EventCategoryID = aipEventCategory.AIP_EventCategoryID" );
	fromStatement.append( " join Inventory i on aipEvent.inventoryId = i.inventoryId" );
	fromStatement.append( " join Vehicle v on i.vehicleId = v.vehicleId" );
	fromStatement.append( " join MakeModelGrouping mmg on v.MakeModelGroupingId = mmg.MakeModelGroupingId" );
	fromStatement.append( " left join ThirdPartyEntity tp on tp.ThirdPartyEntityId = aipEvent.ThirdPartyEntityId" );
    fromStatement.append(" left join ( ");
    fromStatement.append(" select inventoryId, ");
    fromStatement.append(" SUM(CASE WHEN ThirdPartyCategoryID = ");
    fromStatement.append(book1Pref1Id.toString());
    fromStatement.append(" THEN Value ELSE 0 END) PrimaryValue "); 
    if (book1Pref2Id != null) {
        fromStatement.append(", SUM(CASE WHEN ThirdPartyCategoryID = ");
        fromStatement.append(book1Pref2Id.toString());
        fromStatement.append(" THEN Value ELSE 0 END) SecondaryValue");
    }
    fromStatement.append(" from InventoryBookoutCurrentValue bookoutValue where");
    fromStatement.append(innerWhereClause);
    fromStatement.append(" Group by InventoryId) C on C.InventoryId = I.InventoryId");
	return fromStatement.toString();
}

@Override
public String getReportName()
{
	return "UnchangedStatusReport";
}

}
