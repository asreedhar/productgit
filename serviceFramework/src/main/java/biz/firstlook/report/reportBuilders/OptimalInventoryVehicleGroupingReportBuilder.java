package biz.firstlook.report.reportBuilders;


public class OptimalInventoryVehicleGroupingReportBuilder extends ReportBuilder {

    private Integer businessUnitId;
    private Integer groupingDescriptionId;
    private Integer year;
    private boolean includeLowUnitCost = false;
    
    public OptimalInventoryVehicleGroupingReportBuilder(Integer businessUnitId, Integer groupingDescriptionId, Integer year, Boolean includeLowUnitCost) {
        super();
        this.businessUnitId = businessUnitId;
        this.groupingDescriptionId = groupingDescriptionId;
        this.year = year;
    }

    public String fromClause() {
        StringBuilder function = new StringBuilder();
        function.append("GetVehicleGroupingYearFacts( ");
        function.append(businessUnitId);
        function.append(", ");
        function.append(groupingDescriptionId);
        function.append(", ");
        function.append((year != null ? year.toString() : "null"));
        function.append(", ");
        if ( includeLowUnitCost )
    	{
        	//if include low cost, don't use the filter!
    		function.append( " 0" );
    	}
    	else
    	{
    		function.append( " 1" );
    	}
        function.append(")");

        return function.toString();
    }

    public String getReportName() {
        return "OptimalInventorySummaryReport";
    }

}
