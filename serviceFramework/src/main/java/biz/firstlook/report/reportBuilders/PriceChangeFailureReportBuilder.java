package biz.firstlook.report.reportBuilders;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;



public class PriceChangeFailureReportBuilder extends ReportBuilder {
	private static final String BUSINESS_UNIT_ID = "businessUnitId";
	private static final String REPRICE_EXPORT_STATUS_ID = "re.RepriceExportStatusID";
	private static final String DATE_SENT = "re.DateSent";
	
	
	@Override
	public String selectFields()
	{
		return "re.FailureReason, re.DateSent, rh.originalPrice, rh.NewPrice, rh.Created, rh.CreatedBy, inv.StockNumber, v.Make, v.Model, v.VehicleYear, v.VehicleTrim, mmg.GroupingDescriptionID";
	}
	
	@Override
	public String fromClause() {
	    StringBuilder fromClause = new StringBuilder();
        fromClause.append("dbo.RepriceExport re ");
        fromClause.append("JOIN dbo.RepriceEvent rh ON re.RepriceEventID = rh.RepriceEventID ");
        fromClause.append("JOIN dbo.Inventory inv ON inv.InventoryID = rh.InventoryID ");
        fromClause.append("JOIN dbo.Vehicle v ON v.VehicleID = inv.VehicleID ");
        fromClause.append("JOIN dbo.MakeModelGrouping mmg ON mmg.MakeModelGroupingID = v.MakeModelGroupingID ");
        
        return fromClause.toString();
	}

	@Override
	public String getReportName() {
		return "PriceChangeFailureReport";
	}

	public void setBusinessUnitId( Integer businessUnitId )
	{
		addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );
	}
	
	public void setRepriceExportStatusId( Integer repriceExportStatusId )
	{
		addWhereStatement( new SQLWhereSearchStatement( REPRICE_EXPORT_STATUS_ID, SQLOperationEnum.EQ, repriceExportStatusId ) );
	}
	
	public void setDates( Date start, Date end )
	{
		
		String startString = DateFormat.getDateInstance().format(start);
		String endString = DateFormat.getDateInstance().format(end);
		List<String> dates = new ArrayList <String>();
		
		dates.add(startString);
		dates.add(endString);
		addWhereStatement( new SQLWhereSearchStatement( DATE_SENT, SQLOperationEnum.BETWEEN, dates) );
	}
}
