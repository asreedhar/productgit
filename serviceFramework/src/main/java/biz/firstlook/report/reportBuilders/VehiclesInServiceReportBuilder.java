package biz.firstlook.report.reportBuilders;

import java.util.HashSet;
import java.util.Set;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class VehiclesInServiceReportBuilder extends ActionPlanReportBuilder
{

public VehiclesInServiceReportBuilder( Integer businessUnitId, Integer primaryGuideBookFirstPreferenceId, Integer eventTypeId,
										ActionPlanReportModeEnum actionPlanReportMode, Integer createdDaysBack )
{
	super( businessUnitId, primaryGuideBookFirstPreferenceId, eventTypeId, actionPlanReportMode, createdDaysBack );

	//overwrite EVENT_TYPE_ID in the map
	Set< SQLWhereSearchStatement > orClause2 = new HashSet< SQLWhereSearchStatement >();
	orClause2.add( new SQLWhereSearchStatement( EVENT_TYPE_ID, SQLOperationEnum.EQ, PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() ) );
	orClause2.add( new SQLWhereSearchStatement( EVENT_TYPE_ID, SQLOperationEnum.EQ, PlanningEventTypeEnum.DETAILER.getId() ) );
	addWhereStatement( new SQLWhereSearchStatement( EVENT_TYPE_ID, SQLOperationEnum.OR, orClause2, null ) );
}

public String selectFields()
{
	StringBuilder sb = new StringBuilder();
	sb.append( " v.vin, " );
	sb.append( " v.vehicleYear, ");
	sb.append( " v.make, ");
	sb.append( " v.model as groupingDescription, ");
	sb.append( " CAST( v.DoorCount as varchar(1) ) + 'DR' as doorCount, ");
	sb.append( " v.baseColor as color, " );
	sb.append( " DATEDIFF( dd, i.InventoryReceivedDate, getDate() ) as age, ");
	sb.append( " i.inventoryId, ");
	sb.append( " i.unitCost, ");
	sb.append( " i.stockNumber, ");
	sb.append( " i.mileageReceived, ");
	sb.append( " i.currentVehicleLight, ");
	sb.append( " i.inventoryActive, ");
	sb.append( " i.listPrice, ");
	sb.append( " i.InventoryReceivedDate, ");
	sb.append( " CASE WHEN aipEvent.userBeginDate is null THEN null " );
	sb.append( " 	  WHEN aipEvent.userBeginDate <= getDate() AND aipEvent.userEndDate IS NULL THEN datediff( dd, aipEvent.userBeginDate, getDate() ) ");
	sb.append( " 	  WHEN aipEvent.userBeginDate <= getDate() AND aipEvent.userEndDate IS NOT NULL THEN datediff( dd, aipEvent.userBeginDate, aipEvent.userEndDate ) + 1 ");
	sb.append( " 	  ELSE null ");
	sb.append( " END AS createdAge, ");
	sb.append( " aipEvent.userBeginDate as enterDate, " );
	sb.append( " aipEvent.userEndDate as exitDate, " );
	sb.append( " aipEvent.notes, ");
	sb.append( " tp.name as thirdParty, ");
	sb.append( " aipEvent.created, ");
	sb.append( " CASE " ).append( EVENT_TYPE_ID );
	sb.append(" WHEN " ).append( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getId() ).append( " THEN \'" ).append( PlanningEventTypeEnum.SERVICE_DEPARTMENT.getDescription() ).append( "\'");
	sb.append(" WHEN " ).append( PlanningEventTypeEnum.DETAILER.getId() ).append( " THEN \'" ).append( PlanningEventTypeEnum.DETAILER.getDescription() ).append( "\'");
	sb.append( " END AS reason, ");
	sb.append( " bookout.value as bookoutValue, " );
	sb.append( " CASE WHEN aipEvent.userBeginDate is null THEN aipEvent.beginDate ELSE aipEvent.userBeginDate END AS orderByDate " );
	return sb.toString();
}

public String getOrderBy()
{
	StringBuilder sb = new StringBuilder();
	sb.append( " CASE WHEN aipEvent.userBeginDate > getDate() THEN 2 " );
	sb.append( " ELSE 1 END ASC, " );
	sb.append( " orderByDate, " );
	sb.append( super.getOrderBy() );
	return sb.toString();
}

}
