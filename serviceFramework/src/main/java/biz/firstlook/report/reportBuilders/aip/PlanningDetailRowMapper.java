package biz.firstlook.report.reportBuilders.aip;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.ResultSetExtractor;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.main.enumerator.PlanObjectiveEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.main.enumerator.PlanningStatusEnum;
import biz.firstlook.report.postProcessorFilters.PostProcessorFilter;

class PlanningDetailRowMapper implements ResultSetExtractor, CallableStatementCallback {

private List<PostProcessorFilter> postProcessorFilters;

public PlanningDetailRowMapper( List<PostProcessorFilter> filters) {
	this.postProcessorFilters = filters;
}

public Object doInCallableStatement(CallableStatement stmt) throws SQLException, DataAccessException {
	List<Map<String,Object>> inventoryMap = new ArrayList<Map<String,Object>>();
	stmt.execute();
	ResultSet rs = stmt.getResultSet();
	ResultSetMetaData metaData = rs.getMetaData();
	SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
	
	// get inventory items due for replanning
	while (rs.next()) {
		Map<String,Object> result = new LinkedHashMap<String,Object>();
		for (int i = 1; i <= metaData.getColumnCount(); i++) {
			int columnType = metaData.getColumnType(i);
			String columnName = metaData.getColumnName(i);
			if (columnType == java.sql.Types.DATE || columnType == java.sql.Types.TIMESTAMP) {
				Date dateObject = (Date) rs.getObject(i);
				if (!rs.wasNull()) { 
					result.put(columnName, sdf.format(dateObject));
					result.put("java.util.Date:" + columnName, dateObject);
				}
				else {
					result.put(columnName, null);
					result.put("java.util.Date:" + columnName, null);
				}
			}
			else {
                if (columnName.equals("InventoryID")) {
                    result.put("inventoryId", rs.getString(i));
                }
                else if(columnName.equals("MMRValue")){
                	result.put(columnName, rs.getInt(i));
                }
                else {
                    result.put(columnName, rs.getString(i));
                }
			}
		}
		inventoryMap.add(result);
	}
	
	Map<String,List<Map<String,Object>>> eventMap = new HashMap<String,List<Map<String,Object>>>();
	
	// gets all events based on inventory items
	if (stmt.getMoreResults()) {
		rs = stmt.getResultSet();
		while (rs.next()) {
			Map<String, Object> strategyData = new HashMap<String, Object>();
			int strategy = rs.getInt("AIP_EventTypeID");
			PlanningEventTypeEnum planningEventTypeEnum = PlanningEventTypeEnum.getPlanEventTypeEnum(strategy);
			strategyData.put("AIP_EventTypeID", strategy);
			strategyData.put("strategy", planningEventTypeEnum.getDescription());
			strategyData.put("strategyEnum", planningEventTypeEnum);
			Date beginDate = (Date) rs.getObject("BeginDate");
			if (rs.wasNull()) {
				strategyData.put("beginDate", "");
			}
			else {
				strategyData.put("beginDate", sdf.format(beginDate));
			}
			String notes = rs.getString("Notes");
			if (!rs.wasNull()) {
				strategyData.put("notes", notes);
			}
			String notes2 = rs.getString("Notes2");
			if (!rs.wasNull()) {
				if (planningEventTypeEnum == PlanningEventTypeEnum.AUCTION) {
					strategyData.put("auctionLocationNotes", notes2);
				}
				else {
					strategyData.put("Notes2", notes2);
				}
			}
			String value = rs.getString("Value");
			if (!rs.wasNull()) {
				strategyData.put("value", value);
			}
			Date endDate = (Date) rs.getObject("EndDate");
			if (!rs.wasNull()) {
				strategyData.put("endDate", sdf.format(endDate));
			}
			String thirdParty = rs.getString("ThirdParty");
			boolean thirdPartyWasNull = rs.wasNull(); 
			String notes3 = rs.getString("Notes3");
			boolean notes3WasNull = rs.wasNull();
			if (!thirdPartyWasNull) {
				strategyData.put("ThirdParty", thirdParty);
                strategyData.put("ThirdPartyEntityId", rs.getInt("ThirdPartyEntityID"));
			}
			else if (!notes3WasNull && !planningEventTypeEnum.equals( PlanningEventTypeEnum.ADVERTISEMENT )) {
				strategyData.put("ThirdParty", notes3);
			}
			else if (!notes3WasNull && planningEventTypeEnum.equals( PlanningEventTypeEnum.ADVERTISEMENT )) {
				strategyData.put("ThirdParty", notes2);
			}
			Date userBeginDate = (Date) rs.getObject("UserBeginDate");
			if (!rs.wasNull()) {
				strategyData.put("userBeginDate", sdf.format(userBeginDate));
			}
			Date userEndDate = (Date) rs.getObject("UserEndDate");
			if (!rs.wasNull()) {
				strategyData.put("userEndDate", sdf.format(userEndDate));
			}
			// removed later on
			int status = rs.getInt("Status");
			strategyData.put("Status", status);
			// map for later merging
			String inventoryID = rs.getString("InventoryID"); 
			Functions.put(eventMap, inventoryID, strategyData);
		}
	}
	for (Map<String,Object> inventoryItem : inventoryMap) {
		String inventoryID = (String) inventoryItem.get("inventoryId");
		Date reminderDate = (Date) inventoryItem.remove("java.util.Date:PlanReminderDate");
		Map<String, Object> planSummary = new HashMap<String, Object>();
		List<Map<String, Object>> planEventNodes = eventMap.get(inventoryID);
		if (planEventNodes == null) {
			planEventNodes = Collections.emptyList();
		}
		buildPlanSummary(planEventNodes, planSummary, reminderDate);
		inventoryItem.put("PlanSummary", planSummary);
		applyPostProcessingFilters(inventoryItem);
	}
	return inventoryMap;
}

public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
	return null;
}

private void buildPlanSummary(List<Map<String,Object>> planEventNodes, Map<String, Object> planSummary, Date reminderDate) {
	PlanningEventTypeEnum eventTypeEnum;
	Integer status;
	List<String> strategies = new ArrayList<String>();
	boolean repriced = false;
	boolean marketplacePosted = false;
	String objective = null;
	PlanningStatusEnum statusEnum = null;
	boolean dueForReplanning = false;

	
	if (reminderDate == null) {
		// vehicle has never been planned
		planSummary.put("NeverPlanned", true);
		dueForReplanning = true;
	}
	else {
		planSummary.put("NeverPlanned", false);
		dueForReplanning = DateUtils.truncate(new Date(), Calendar.DATE).compareTo(reminderDate) >= 0;
		planSummary.put("DueForReplanning", dueForReplanning);
	}

	// this is to handle the case where you make a plan then delete a plan in the same day
	// you still have a next plan date stored in the DB so don't want to go back to never planned.
	boolean noPlan = (reminderDate != null && planEventNodes.isEmpty() );
	planSummary.put("NoPlan", noPlan);

	for (Map<String,Object> node : planEventNodes) {
		
		eventTypeEnum = (PlanningEventTypeEnum) node.get("strategyEnum");

		if (!strategies.contains(eventTypeEnum.getDescription()) && eventTypeEnum != PlanningEventTypeEnum.REPRICE) {
			strategies.add(eventTypeEnum.getDescription());

			// this prevents empty parenthesis and strategy lists
			// that end with ';' from showing up on the gui
			if (eventTypeEnum.isNoStrategyEvent()) {
				// don't show the 'green check mark' for
				// 'no strategy' events
				planSummary.put("OverrideUpdate", true);
			}

			// all stategies are assumed to be of the same objective, BUT if for some reason it is not
			// let's keep updating the objective with the newest stragety.
			// but "Dealer" is not a valid objective
			if (eventTypeEnum.getPlanObjectiveEnum() != PlanObjectiveEnum.DEALER && eventTypeEnum.getPlanObjectiveEnum() != PlanObjectiveEnum.MARKETPLACE ) {
				objective = eventTypeEnum.getPlanObjectiveEnum().getDescription();
			}
		}

		if (eventTypeEnum == PlanningEventTypeEnum.REPRICE) {
			repriced = true;
			planSummary.put("NoPlan", false);
		}
		
		if (eventTypeEnum == PlanningEventTypeEnum.INTERNET_MARKETPLACE) {
			marketplacePosted = true;
			planSummary.put("NoPlan", false);
		}

		status = (Integer) node.remove("Status");

		// determine current or prior plan
		if (statusEnum == null && status != null) {
			statusEnum = PlanningStatusEnum.getStatusEnum(status);
		}
	}

	if (noPlan && reminderDate != null && DateUtils.truncate(new Date(), Calendar.DATE).compareTo(reminderDate) < 0) {
		statusEnum = PlanningStatusEnum.CURRENT_PLAN;
	}

	planSummary.put("HasCurrentPlan", statusEnum == PlanningStatusEnum.CURRENT_PLAN ? true : false);

	if (repriced) {
		planSummary.put("NeverPlanned", false);

		// nk - FB: 2126. If we have a plan with a end date in the past but with a reminder date in the future
		//				  it is not yet due for replannning. Only once the reminder day has passed.
		if (statusEnum != null && ((statusEnum == PlanningStatusEnum.CURRENT_PLAN) || (DateUtils.truncate(new Date(), Calendar.DATE).compareTo(reminderDate) < 0))  && !dueForReplanning) {
			// there is already a current plan for the vehicle
			planSummary.put("DueForReplanning", false);
		}
		else {
			planSummary.put("DueForReplanning", true);
		}
		planSummary.put("NoPlan", false);
	}

	planSummary.put("Repriced", repriced);
	planSummary.put("marketplacePosted", marketplacePosted);
	planSummary.put("objective", objective);
	planSummary.put("strategies", strategies);
	planSummary.put("detailedStrategies", planEventNodes);
}


protected void applyPostProcessingFilters(Map<String,Object> rowResult) {
	for (PostProcessorFilter filter : postProcessorFilters ) {
		filter.filterResults(rowResult);
	}
}
}