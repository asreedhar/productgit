package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class TrimReportBuilder extends ReportBuilder
{

private static final String BUSINESS_UNIT_ID = "BusinessUnitId";
private static final String SALE_TYPE_ID = "pricing.SaleTypeID";
private static final String VEHICLE_GROUPING_ID = "VehicleGroupingID";
private static final String TRIM_ID = "TrimID";

public TrimReportBuilder()
{
	super();
}

@Override
public String fromClause()
{
	return "Pricing_A1 pricing JOIN VehicleHierarchy_D VH ON pricing.VehicleHierarchyID = VH.VehicleHierarchyID";
}

@Override
public String getReportName()
{
	return "TrimReport";
}

public void setBusinessUnitId( Integer businessUnitId )
{
	addWhereStatement( new SQLWhereSearchStatement( BUSINESS_UNIT_ID, SQLOperationEnum.EQ, businessUnitId ) );	
}

public void setSaleTypeId( Integer saleTypeId )
{
	addWhereStatement( new SQLWhereSearchStatement( SALE_TYPE_ID, SQLOperationEnum.EQ, saleTypeId ) );
}

public void setVehicleGroupingId( Integer vehicleGroupingId )
{
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.EQ, vehicleGroupingId ) );
}

public void setTrimId( Integer trimId )
{
	addWhereStatement( new SQLWhereSearchStatement( TRIM_ID, SQLOperationEnum.EQ, trimId ) );
}

}
