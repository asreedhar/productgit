package biz.firstlook.report.reportBuilders.aip;
import biz.firstlook.report.postProcessorFilters.TradeOrPurchasePPFilter;
import biz.firstlook.report.reportBuilders.ReportBuilder;
public class EquityAnalyzerReportBuilderforMMR extends ReportBuilder
{
private Integer businessUnitId;
private Integer percentageMultiplier;

public EquityAnalyzerReportBuilderforMMR( Integer businessUnitId, Integer percentageMultiplier )
{
	super();
	this.businessUnitId = businessUnitId;
	this.percentageMultiplier = percentageMultiplier;
	addPostProcessorFilter( new TradeOrPurchasePPFilter() );	
}

@Override
public String fromClause() {
	return "GetEquityAnalyzerDetailsforMMR( " + businessUnitId.toString()+ "," + percentageMultiplier.toString() + " )";
}

@Override
public String getReportName() {
	return "EquityAnalyzerReportforMMR";
}
}
