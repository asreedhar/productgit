package biz.firstlook.report.reportBuilders;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

public class VehicleGroupingReportBuilder extends ReportBuilder
{

private static final String VEHICLE_GROUPING_ID = "VehicleGroupingID";

/**
 * Currently this only returns the VehicleDescription and MakeModelGrouping. 
 *
 */
public VehicleGroupingReportBuilder()
{
	super();
}

@Override
public String fromClause()
{
	StringBuilder fromStatement = new StringBuilder( "VehicleHierarchy_D vd" );
	fromStatement.append( " join MakeModelGrouping mmg on vd.VehicleGroupingID = mmg.GroupingDescriptionId" );
	return fromStatement.toString();
}

@Override
public String getReportName()
{
	return "VehicleGroupingReport";
}

public void setVehicleGroupingId( Integer vehicleGroupingId )
{
	addWhereStatement( new SQLWhereSearchStatement( VEHICLE_GROUPING_ID, SQLOperationEnum.EQ, vehicleGroupingId ) );
}

@Override
public String selectFields() {
	StringBuilder sb = new StringBuilder();
	sb.append( " vd.Description, mmg.MakeModelGroupingID " );
	return sb.toString();
}

}
