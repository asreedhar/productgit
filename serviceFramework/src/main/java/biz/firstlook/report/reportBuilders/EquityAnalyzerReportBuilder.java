package biz.firstlook.report.reportBuilders;

import biz.firstlook.report.postProcessorFilters.TradeOrPurchasePPFilter;

public class EquityAnalyzerReportBuilder extends ReportBuilder
{

private Integer businessUnitId;
private Integer thirdPartyCategory;
private Integer percentageMultiplier;

public EquityAnalyzerReportBuilder( Integer businessUnitId, Integer thirdPartyCategory, Integer percentageMultiplier )
{
	super();
	this.businessUnitId = businessUnitId;
	this.thirdPartyCategory = thirdPartyCategory;
	this.percentageMultiplier = percentageMultiplier;
	addPostProcessorFilter( new TradeOrPurchasePPFilter() );	
}

@Override
public String fromClause()
{
	return "GetEquityAnalyzerDetails( " + businessUnitId.toString() + "," + thirdPartyCategory.toString() + "," + percentageMultiplier.toString() + " )";
}

@Override
public String getReportName()
{
	return "EquityAnalyzerReport";
}
}
