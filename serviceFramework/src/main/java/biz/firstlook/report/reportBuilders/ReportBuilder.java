package biz.firstlook.report.reportBuilders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;

import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.report.postProcessorFilters.PostProcessorFilter;
import biz.firstlook.report.reportFramework.ReportDisplayBeanRowMapper;
import biz.firstlook.report.sql.SQLWhereSearchStatement;
import biz.firstlook.report.sql.SQLizable;

public abstract class ReportBuilder implements SQLizable
{

private Map<String, SQLWhereSearchStatement> sqlWhereSearchStatements;
private List<PostProcessorFilter> postProcessorFilters;
protected String orderBy;
protected boolean desc;
protected ResultSetExtractor rowMapper;

public ReportBuilder()
{
	super();
	sqlWhereSearchStatements = new TreeMap<String, SQLWhereSearchStatement>();
	postProcessorFilters = new ArrayList<PostProcessorFilter>();
	
	// optional order by clause
	orderBy = null;
	desc = false;
	
	// this is the default row mapper to use
	// if a specific row mapper is needed, set it in the concrete class's constructor
	// MH - 05/30/2006
	rowMapper = new ReportDisplayBeanRowMapper( this );
}

public String selectFields()
{
	return "";
}

public abstract String fromClause();

public abstract String getReportName();

public final String getFilterOperation( String dbColumn )
{
	return sqlWhereSearchStatements.get( dbColumn ).getSqlOperationEnum().toString();
}

protected final void addPostProcessorFilter( PostProcessorFilter postProcessorFilter )
{
	postProcessorFilters.add( postProcessorFilter );
}

public final void addWhereStatement( SQLWhereSearchStatement sqlWhereSearchStatement )
{
	sqlWhereSearchStatements.put( sqlWhereSearchStatement.getDbColumnName(), sqlWhereSearchStatement );
}

public final Iterator getSqlWhereSearchStatementsIter()
{
	return sqlWhereSearchStatements.values().iterator();
}

public final Object[] getFilterValues()
{
	List<Object> filterValuesWithoutInClause = getFilterValues( sqlWhereSearchStatements.values() );
	return filterValuesWithoutInClause.toArray();
}

private List<Object> getFilterValues( Collection<SQLWhereSearchStatement> sqlWhereSearchStatements )
{
	List<Object> filterValuesWithoutInClause = new ArrayList<Object>();
	for( SQLWhereSearchStatement sqlWhereSearchStatement : sqlWhereSearchStatements )
	{
		if( sqlWhereSearchStatement.getSqlOperationEnum().equals( SQLOperationEnum.OR ) )
		{
			//inside "or clause", so recurse
			filterValuesWithoutInClause.addAll( getFilterValues( sqlWhereSearchStatement.getOrClauseValues() ) );
		}
		else if ( !sqlWhereSearchStatement.getSqlOperationEnum().equals( SQLOperationEnum.IN )
				&& !sqlWhereSearchStatement.getSqlOperationEnum().equals( SQLOperationEnum.IS_NULL )
				&& !sqlWhereSearchStatement.getSqlOperationEnum().equals( SQLOperationEnum.BETWEEN ) )
		{
			filterValuesWithoutInClause.add( sqlWhereSearchStatement.getFilterValue() );
		}
	}
	return filterValuesWithoutInClause;
}

public final List<PostProcessorFilter> getPostProcessorFilters()
{
	return postProcessorFilters;
}

public boolean isDesc()
{
	return desc;
}

public String getOrderBy()
{
	return orderBy;
}

public ResultSetExtractor getRowMapper()
{
	return rowMapper;
}

public boolean isStoredProcedure() {
	return false;
}

public boolean isCallback() {
	return false;
}

public List<? extends Object> getParameterList() {
	return Collections.singletonList(new SqlOutParameter("@RC", java.sql.Types.INTEGER));
}

public Map<String,? extends Object> getParameterMap() {
	return Collections.emptyMap();
}

public CallableStatementCallback getCallableStatementCallback() {
	return null;
}

}