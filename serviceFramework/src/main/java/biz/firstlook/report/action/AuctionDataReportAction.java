package biz.firstlook.report.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.AuctionReport;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.service.AuctionDataService;

public class AuctionDataReportAction extends NextGenAction
{
private AuctionDataService auctionDataService;

public AuctionDataReportAction()
{
	super();
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer areaId = Integer.valueOf( request.getParameter( "areaId" ) );
	Integer periodId = Integer.valueOf( request.getParameter( "timePeriod" ) );
	String vic = request.getParameter( "vic" ); 
	Boolean usingVIC = Boolean.valueOf( request.getParameter( "usingVIC" ) );
	Integer modelYear = NextGenAction.getInt( request, "modelYear" );
	
    List< AuctionReport > report = auctionDataService.retrieveAuctionReport(periodId, vic, areaId, usingVIC, modelYear );
 
    if ( report != null &&! report.isEmpty()) 
    {
    	   String sBStyle = (String) report.get( 0  ).getVicBodyStyle();
    	   String rName =  (String) report.get(  0  ).getRegionName();
    	   request.setAttribute( "vicBodyStyle", sBStyle );
    	   request.setAttribute( "regionName", rName );
    }
    request.setAttribute( "report", report );
    return mapping.findForward( "success" );
}

public AuctionDataService getAuctionDataService()
{
	return auctionDataService;
}

public void setAuctionDataService( AuctionDataService auctionDataService )
{
	this.auctionDataService = auctionDataService;
}

}
