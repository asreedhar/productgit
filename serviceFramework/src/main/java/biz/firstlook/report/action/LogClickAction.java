package biz.firstlook.report.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.LogClickEventType;
import biz.firstlook.module.audit.dao.LogClickEventDAO;

public class LogClickAction extends NextGenAction
{

private LogClickEventDAO logClickEventDAO;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
    Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
    Integer memberId = getNextGenSession( request ).getMemberId();   
    Integer marketId = Integer.parseInt(request.getParameter("marketId"));
    //Integer marketType = Integer.parseInt( request.getParameter( "marketType" ) );
    //PurchasingCenterChannelEnum purchasingCenterChannel = findPurchasingCenterChannel( marketType );
    String vin = request.getParameter( "vin" );
    String url = request.getParameter( "url" );
    
	getLogClickEventDAO().createLogClickEvent( LogClickEventType.SEARCH_AND_ACQUISITION, businessUnitId, memberId, marketId, vin, url );
    
    return mapping.findForward( "NavigatorSuccess" );
}
//Commenting out old channel stuff for now, can probably remove but not sure yet.
//public PurchasingCenterChannelEnum findPurchasingCenterChannel( Integer marketType )
//{
//    if ( marketType.equals( MarketType.IN_GROUP.ordinal() ) )
//    	return PurchasingCenterChannelEnum.IN_GROUP;
//    
//    return PurchasingCenterChannelEnum.ONLINE;
//}

public LogClickEventDAO getLogClickEventDAO()
{
	return logClickEventDAO;
}

public void setLogClickEventDAO( LogClickEventDAO logClickEventDAO )
{
	this.logClickEventDAO = logClickEventDAO;
}

}
