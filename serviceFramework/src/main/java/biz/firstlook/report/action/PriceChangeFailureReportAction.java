package biz.firstlook.report.action;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.report.reportBuilders.PriceChangeFailureReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class PriceChangeFailureReportAction extends NextGenAction {

	private IReportService transactionalReportService;
	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Calendar rightNow = Calendar.getInstance();
		Date start = null;
		Date end = null;
		
		if ( request.getParameter( "timeframe" ) != null)
		{
			if(request.getParameter( "timeframe" ).equals( "1" )){
				//current week
				int firstDayOfWeek = rightNow.getFirstDayOfWeek()- rightNow.get(Calendar.DAY_OF_WEEK);
				rightNow.add(Calendar.DAY_OF_MONTH, firstDayOfWeek);
				start = rightNow.getTime();
				rightNow.add(Calendar.DAY_OF_MONTH, 7);
				end = rightNow.getTime();
				
				request.setAttribute("timeFrame", 1);
			}
			else if(request.getParameter( "timeframe" ).equals( "2" )){
				//last 2 weeks
				rightNow.add(Calendar.DAY_OF_MONTH, -14);
				start = rightNow.getTime();
				rightNow.add(Calendar.DAY_OF_MONTH, 15);
				end = rightNow.getTime();
			
				request.setAttribute("timeFrame", 2);
			}
			else if(request.getParameter( "timeframe" ).equals( "3" )){
				//current month
				rightNow.set(Calendar.DAY_OF_MONTH, 1);
				start = rightNow.getTime();
				int lastDay = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
				rightNow.set(Calendar.DAY_OF_MONTH, lastDay);			
				end = rightNow.getTime();				
	
				request.setAttribute("timeFrame", 3);
			}
			else if(request.getParameter( "timeframe" ).equals( "4" )){
				//last month
				rightNow.set(Calendar.DAY_OF_MONTH, 1);
				rightNow.add(Calendar.MONTH, -1);
				start = rightNow.getTime();
				int lastDay = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
				rightNow.set(Calendar.DAY_OF_MONTH, lastDay);			
				end = rightNow.getTime();
				
				request.setAttribute("timeFrame", 4);	
			}
		}
		else{
			int firstDayOfWeek = rightNow.getFirstDayOfWeek()- rightNow.get(Calendar.DAY_OF_WEEK);
			rightNow.add(Calendar.DAY_OF_MONTH, firstDayOfWeek);
			start = rightNow.getTime();
			rightNow.add(Calendar.DAY_OF_MONTH, 7);
			end = rightNow.getTime();
			request.setAttribute("timeFrame", 1);
		}
		PriceChangeFailureReportBuilder report = new PriceChangeFailureReportBuilder();
		report.setBusinessUnitId( getNextGenSession(request).getBusinessUnitId());
		report.setRepriceExportStatusId(RepriceEventStatus.Failure.getId());
		report.setDates(start, end);

		List< Map > reportDisplayBeans = getTransactionalReportService().getResults( report );
		
		request.setAttribute( "priceChangeFailureBean", reportDisplayBeans );

		return mapping.findForward( "success" );
	}

	public IReportService getTransactionalReportService() {
		return transactionalReportService;
	}

	public void setTransactionalReportService(
			IReportService transactionalReportService) {
		this.transactionalReportService = transactionalReportService;
	}
}
