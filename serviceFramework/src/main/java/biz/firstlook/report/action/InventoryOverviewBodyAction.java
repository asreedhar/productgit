package biz.firstlook.report.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.report.compositeReportBuilders.IOSegmentDetailCompositeReportBuilder;
import biz.firstlook.report.compositeReportBuilders.InventoryOverviewBodyCompositeReportBuilder;
import biz.firstlook.report.compositeReportBuilders.InventoryOverviewBodyCompositeReportBuilder.InventoryOverviewBodyReportEnum;
import biz.firstlook.report.reportBuilders.SegmentTabReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

/**
 * this action builds the main part (or body if you will) of inventory overview.
 * it makes the segment tabs, the segment level information as well as the model
 * level line item
 */
public class InventoryOverviewBodyAction extends NextGenAction
{

private IReportService reportService;
private PreferenceService preferenceService;

public InventoryOverviewBodyAction()
{
	super();
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	InventoryOverviewBodyReportEnum reportType = InventoryOverviewBodyReportEnum.ALPHABETIC;
	if ( request.getParameter( "reportStyle" ) != null  && request.getParameter( "reportStyle" ).equalsIgnoreCase( InventoryOverviewBodyReportEnum.SEGMENT.toString() ) )
	{
		reportType = InventoryOverviewBodyReportEnum.SEGMENT;
	}
	else if(request.getParameter( "reportStyle" ) == null){
		Integer sortBy = getPreferenceService().getMemberInventoryOverviewSortOrderType(getNextGenSession( request ).getMemberId());
		if(sortBy == InventoryOverviewBodyReportEnum.SEGMENT.ordinal()){
			reportType = InventoryOverviewBodyReportEnum.SEGMENT;
		}
	}
	
	
	Integer activeSegment = null;
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	// if the request was for the alphabetic version, don't compute the tabs!
	if ( reportType != InventoryOverviewBodyReportEnum.ALPHABETIC )
	{
		// build the report for just the tabs
		SegmentTabReportBuilder segmentTabsReportBuilder = new SegmentTabReportBuilder();
		segmentTabsReportBuilder.setBusinessUnitId( getNextGenSession( request ).getBusinessUnitId() );

		List< Map > segmentTabDisplayBeans = getReportService().getResults( segmentTabsReportBuilder );

		// get the active tab either from the url, or the highest contributing %
		// from the first report
		if ( request.getParameter( "activeSegment" ) != null )
		{
			activeSegment = Integer.parseInt( request.getParameter( "activeSegment" ) );
		}
		else
		{
			Map highestContributingSegment = (Map)segmentTabDisplayBeans.get( 0 );
			activeSegment = (Integer) highestContributingSegment.get( "VehicleSegmentID" );
		}

		// build the report for the segment level details (the info under the
		// tabs, like units in stock and performance bubble)
		IOSegmentDetailCompositeReportBuilder segmentDetailReportBuilder = new IOSegmentDetailCompositeReportBuilder(	businessUnitId,
																														activeSegment );

		List< Map > segmentDetailDisplayBeans = getReportService().getCompositeResults( segmentDetailReportBuilder );

		request.setAttribute("activeSegment", activeSegment);
		request.setAttribute( "SegmentTabDisplayBeans", segmentTabDisplayBeans );
		request.setAttribute( "SegmentDetailDisplayBean", segmentDetailDisplayBeans.get( 0 ) );
	}
	
	// determine whether or not to display the annual ROI for this dealership
	boolean showAnnualRoi = getNextGenSession( request ).showAnnualRoi();
	request.setAttribute( "showAnnualRoi", showAnnualRoi );

	// build the report for the model level items
	InventoryOverviewBodyCompositeReportBuilder inventoryOverviewBodyCompositeReportBuilder = new InventoryOverviewBodyCompositeReportBuilder(	reportType,
	                                                                                                                                          	businessUnitId,																																			PeriodEnum.TWENTYSIX_WEEKS,
																																				activeSegment,
																																				showAnnualRoi );


	List< Map > inventoryDetailReportResults = getReportService().getCompositeResults( inventoryOverviewBodyCompositeReportBuilder );

	request.setAttribute( "ModelLevelReportBlock", inventoryDetailReportResults );

	request.setAttribute( "Preferences", getPreferenceService().getPreferences( getNextGenSession( request ).getBusinessUnitId(),
																				PreferenceService.ReportsEnum.INVENTORY_OVERVIEW ) );
	request.setAttribute( "zip", getNextGenSession( request ).getZipCode());
	
	// need to propagate the report style to the JSP for the print link (probably a temporary thing)
	request.setAttribute( "reportType", reportType);
	
	request.setAttribute( "modelPromotionPlans", ActionPlanReport.MODEL_PROMOTION.getDescription() );
	request.setAttribute( "modelPromotionTimePeriod", ActionPlanReportModeEnum.CURRENT.toString() );
	
	//save reportstyle if necessary
	if ( request.getParameter( "saveStyle" ) != null  && reportType == InventoryOverviewBodyReportEnum.ALPHABETIC)
		getPreferenceService().setMemberInventoryOverviewSortOrderType(InventoryOverviewBodyReportEnum.ALPHABETIC.ordinal(), getNextGenSession( request ).getMemberId());
	else if(request.getParameter( "saveStyle" ) != null)
		getPreferenceService().setMemberInventoryOverviewSortOrderType(InventoryOverviewBodyReportEnum.SEGMENT.ordinal(), getNextGenSession( request ).getMemberId());
	
	return mapping.findForward( "success" );

}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

}
