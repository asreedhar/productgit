package biz.firstlook.report.action;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.report.reportBuilders.OverallInventoryInformationReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class OverallInventoryInformationTileAction extends NextGenAction
{

private IReportService reportService;

@SuppressWarnings("unchecked")
@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{

	OverallInventoryInformationReportBuilder report = new OverallInventoryInformationReportBuilder();

	report.setBusinessUnitId( getNextGenSession(request).getBusinessUnitId());
	report.setPeriodId( PeriodEnum.TWENTYSIX_WEEKS);

	List< Map > reportDisplayBeans = getReportService().getResults( report );
	
	if (reportDisplayBeans.isEmpty()) {
		request.setAttribute( "overallInventoryInformationBean", Collections.emptyMap() );
	}
	else {
		request.setAttribute( "overallInventoryInformationBean", reportDisplayBeans.get( 0 ) );
	}

	return mapping.findForward( "success" );
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

}
