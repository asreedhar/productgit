package biz.firstlook.report.action;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.PlanningResultsModeEnum;
import biz.firstlook.report.reportBuilders.aip.PlanningSummaryReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class PlanningOverviewTileAction extends NextGenAction{

    private IReportService transactionalReportService;
    
    public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
        int plannedThisWeek = 0;
        int unitsToReplan = 0;
        
        PlanningSummaryReportBuilder report = new PlanningSummaryReportBuilder(businessUnitId, new Date( new java.util.Date().getTime() ));
        report.setResultsMode(PlanningResultsModeEnum.REPLANNING.getId()); 
        List< Map > results = transactionalReportService.getResults(report);
        if (results != null && !results.isEmpty()) {
            unitsToReplan = (Integer) results.get(0).get("ReplanUnits");
        }
        
        report.setResultsMode(PlanningResultsModeEnum.PLANNED.getId());
        report.setSearchDays(7);
        results = transactionalReportService.getResults(report);
        if (results != null && !results.isEmpty()) {
            plannedThisWeek = (Integer) results.get(0).get("ReplanUnits");
        }
        
        request.setAttribute("unitsToReplan", unitsToReplan);
        request.setAttribute("plannedThisWeek", plannedThisWeek);
        
        return mapping.findForward("success");
    }

    public void setTransactionalReportService(IReportService transactionalReportService) {
        this.transactionalReportService = transactionalReportService;
    }
}
