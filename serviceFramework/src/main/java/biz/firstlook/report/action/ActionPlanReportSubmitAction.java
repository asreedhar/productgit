package biz.firstlook.report.action;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.actionFilter.NextGenSession;
import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.SQLOperationEnum;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.report.reportBuilders.ActionPlanReportBuilder;
import biz.firstlook.report.reportBuilders.ActionPlanRepricingReportBuilder;
import biz.firstlook.report.reportBuilders.AuctionAndWholeSalerActionPlanReportBuilder;
import biz.firstlook.report.reportBuilders.InternetAdvertisingActionPlanReportBuilder;
import biz.firstlook.report.reportBuilders.ModelPromotionPlansReportBuilder;
import biz.firstlook.report.reportBuilders.PrintAdvertisingReportBuilder;
import biz.firstlook.report.reportBuilders.ReportBuilder;
import biz.firstlook.report.reportBuilders.SpecialFinanceOrCertifiedActionPlanReportBuilder;
import biz.firstlook.report.reportBuilders.ThirdPartyVehicleOptionsReportBuilder;
import biz.firstlook.report.reportBuilders.UnchangedActionPlanReportBuilder;
import biz.firstlook.report.reportBuilders.VehiclesInServiceReportBuilder;
import biz.firstlook.report.reportFramework.ColumnAppendFlattener;
import biz.firstlook.report.reportFramework.CompareReplaceFlattener;
import biz.firstlook.report.reportFramework.Flattener;
import biz.firstlook.report.reportFramework.IReportService;
import biz.firstlook.report.reportFramework.RowMapComparator;
import biz.firstlook.report.sql.SQLWhereSearchStatement;

/**
 * For Print all functionality, the variables that get put into the request are request scoped, so null are set on lists.
 * 
 * @author bfung
 * 
 */
@SuppressWarnings("unchecked")
public class ActionPlanReportSubmitAction extends NextGenAction
{

private IReportService transactionalReportService;

@SuppressWarnings("deprecation")
@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
    DynaActionForm theForm = (DynaActionForm) form;
	// pass through forcePageBreak
	request.setAttribute( "forcePageBreak", request.getParameter( "forcePageBreak" ) );

	Integer daysBack = 1;
	try
	{
		daysBack = Integer.parseInt( request.getParameter( "daysBack" ) );
		if ( daysBack.intValue() < 1 )
			daysBack = 1;
	}
	catch ( NumberFormatException nfe )
	{
		// do nothing, default daysBack to 1, per Rally UC10;
	}

	ActionPlanReportModeEnum actionPlanReportMode = ActionPlanReportModeEnum.valueOf( request.getParameter( "timePeriodMode" ) );
	String individualReport = request.getParameter( "reportType" );
	ActionPlanReport reportType = null;

	// need to "dispatch action" here
	if ( individualReport != null )
	{
		reportType = ActionPlanReport.valueFromDescription( request.getParameter( "reportType" ) );
	}
	else
	{
	    //    subroutine to pass on valid reports to print to fullPrint page.
        List< Map > reportTypes = new ArrayList< Map >();
        Enumeration paramNames = request.getParameterNames();
        while ( paramNames.hasMoreElements() )
        {
            String paramName = (String)paramNames.nextElement();
            // check that the reportTypes are valid and add to list.
            ActionPlanReport actionPlanReport = ActionPlanReport.valueFromDescription( paramName );
			if ( actionPlanReport != null )
            {
                if (actionPlanReport == ActionPlanReport.AUCTION_INDIVIDUAL) {
                    formatIndividualParams("individualAuction", request, reportTypes);
                }
                else if (actionPlanReport == ActionPlanReport.WHOLESALER_INDIVIDUAL) {
                    formatIndividualParams("individualWholesale", request, reportTypes);
                }
                else {
                    Map itemToAdd = new HashMap();
                    itemToAdd.put("name", paramName);
                    reportTypes.add( itemToAdd );
                }
                
            }
        }

		request.setAttribute( "reportTypes", reportTypes );
		request.setAttribute( "daysBack", daysBack );
		request.setAttribute( "timePeriodMode", actionPlanReportMode.toString() );
		request.setAttribute( "numReports", Integer.valueOf( reportTypes.size() ) ); 
		
		return mapping.findForward( "fullPrint" );
	}
    
    String forward = "preview";
    if (reportType == ActionPlanReport.MODEL_PROMOTION) {
        forward = "preview.modelPromotion";
    }
    if (reportType == ActionPlanReport.WHOLESALER_BIDSHEET) {
        forward = "preview.wholesaler";
    }
    if (reportType == ActionPlanReport.REPRICING || reportType == ActionPlanReport.REPRICING_NO_NOTES) {
        forward = "preview.repricing";
    }
    
    Integer thirdPartyId = null;
    if (reportType == ActionPlanReport.ADVERTISING_INDIVIDUAL_REPORT) {
    	try {
    		thirdPartyId = Integer.parseInt(request.getParameter("reportType"));
    	} catch (Exception nfe) {
    		//do nothing.
    	}
    }
    String endDate = null;
    if (reportType == ActionPlanReport.AUCTION_INDIVIDUAL || reportType == ActionPlanReport.WHOLESALER_INDIVIDUAL) {
    	try {
    		thirdPartyId = Integer.parseInt(request.getParameter("thirdPartyId"));
    	} catch (Exception nfe) {
    		//do nothing.
    	}
    	
        endDate = request.getParameter("endDate");
        if (endDate != null) {
        	endDate = URLDecoder.decode(endDate, "UTF-8");
        }
    }
    ReportBuilder reportBuilder = buildReport(request, actionPlanReportMode, daysBack, reportType, thirdPartyId, endDate);
//    List< Map > reportResults = helper.getResults( reportBuilder,  transactionalReportService);
    List< Map > reportResults = transactionalReportService.getResults( reportBuilder);
    Map< Map< String, Object >, List< Map > > groupedResults = buildGroupedResults(reportResults, request, actionPlanReportMode, daysBack, reportType);

	if ( groupedResults == null )
	{
		reportResults = buildNonGroupedResults(reportResults, request, actionPlanReportMode, daysBack, reportType);
		request.setAttribute( "reportResults", reportResults );
		request.setAttribute( "groupedResults", null ); // erase the pagescope variable
	}
	else
	{
		request.setAttribute( "groupedResults", groupedResults );
		request.setAttribute( "groupedResultsSize", groupedResults.size() );
		request.setAttribute( "reportResults", null ); // erase the pagescope variable
	}
    if (actionPlanReportMode == ActionPlanReportModeEnum.HISTORY) {
        Date start = new Date();
        start.setHours(start.getHours() - (daysBack * 24));
        request.setAttribute("past", Boolean.TRUE);
        request.setAttribute("start", start);
    }
   
	theForm.set("daysBack", daysBack);
    theForm.set("reportMode", actionPlanReportMode);
    //theForm.set("reportResults", reportResults);
    
    request.setAttribute("primaryBookPreference", ThirdPartyCategoryEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookFirstPreferenceId()).getDescription());
    if (getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId()!= null && getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId() > 0) {
        request.setAttribute("secondaryBookPreference", ThirdPartyCategoryEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId()).getDescription());
    }
    request.setAttribute("primaryBookName", ThirdPartyEnum.getEnumById(getNextGenSession(request).getPrimaryGuideBookId()).getDescription());
    Date now = new Date();
    request.setAttribute("endDate", endDate);
    request.setAttribute("thirdPartyId", thirdPartyId);
    request.setAttribute("date", now);
    request.setAttribute( "reportType", reportType.getDescription() );

	return mapping.findForward( forward );
}

private void retrieveVehicleOptions(List<Map> reportResults, HttpServletRequest request) {
    ThirdPartyVehicleOptionsReportBuilder optionsBuilder = new ThirdPartyVehicleOptionsReportBuilder(getNextGenSession(request).getPrimaryGuideBookId());
    for (Map result:reportResults) {
        Integer invId = (Integer) result.get("inventoryId");
        optionsBuilder.setInventoryId(invId);
        List< Map > optionsList = getTransactionalReportService().getResults( optionsBuilder );
        Iterator optionsItr = optionsList.iterator();
        
        StringBuilder options = new StringBuilder();
        while (optionsItr.hasNext()) {
            Map test = (Map) optionsItr.next();
            options.append((String) test.get("name"));
            if (optionsItr.hasNext()) {
                options.append(", ");
            }
        }
        result.put("options",options.toString());
    }
}

private void formatIndividualParams(String reportName, HttpServletRequest request, List<Map> reportTypes) {
    String[] pars = request.getParameterValues(reportName);
    String[] thirdParties = new String[pars.length];
    String[] endDates = new String[pars.length];
    for (int i = 0; i < pars.length;i++) {
        String auction = pars[i];
        String[] parValues = auction.split("_");
        
        Map itemToAdd = new HashMap();
        itemToAdd.put("name", reportName);
        itemToAdd.put("thirdPartyId", parValues[0]);
        itemToAdd.put("thirdParty", parValues[1]);
        itemToAdd.put("endDate", parValues[2]);
        reportTypes.add(itemToAdd);
    } 
    request.setAttribute((reportName.equalsIgnoreCase("individualAuction") ? "auctionThirdParties":"wholeThirdParties"), thirdParties);
    request.setAttribute((reportName.equalsIgnoreCase("individualAuction") ? "auctionEndDates":"wholeEndDates"), endDates);
}

private Map< Map< String, Object >, List< Map >> getRepricingHistory( Integer daysBack, ActionPlanReportModeEnum actionPlanReportMode,
																		List< Map > reportResults )
{
	// get inv ids
	Map inventoryIds = new HashMap();
	for ( int i = 0; i < reportResults.size(); i++ )
	{
		String inventoryIdString = ( (Map)reportResults.get( i ) ).get( "inventoryId" ) + "";
		// using a hashmap because it will only store unique keys and reportResults is not unique
		inventoryIds.put( inventoryIdString, inventoryIdString );
	}
	if (inventoryIds.isEmpty()) {
		return new HashMap< Map< String, Object >, List< Map >>();
	}
	ActionPlanRepricingReportBuilder repricingReportBuilder = new ActionPlanRepricingReportBuilder( inventoryIds,
																									actionPlanReportMode,
																									daysBack );
	List< Map > repricingResults = getTransactionalReportService().getResults( repricingReportBuilder );

	// remove reprice events when old price = new price - should be done in sql but was getting data truncation errors and whatnot
	List< Map > trimmedRepricingResults = removedNonPriceChangingConfermations( repricingResults );
    //trimmedRepricingResults = flatten( trimmedRepricingResults, "inventoryId" );
	
	// group repricing arrays by date created
	Map< Map< String, Object >, List< Map >> groupedByDateResults = toGroupedResults( trimmedRepricingResults, new String[] { "NA" },
																						new String[] { "dayCreated" } );
	return groupedByDateResults;

}


protected ReportBuilder buildReport(HttpServletRequest request, ActionPlanReportModeEnum actionPlanReportMode, Integer daysBack, ActionPlanReport reportType, 
        Integer thirdPartyId, String endDate) {
    final NextGenSession nextGenSession = getNextGenSession( request );
	final Integer businessUnitId = nextGenSession.getBusinessUnitId();
	final Integer primaryGuideBookFirstPreferenceId = nextGenSession.getPrimaryGuideBookFirstPreferenceId();
	final Integer primaryGuideBookSecondPreferenceId = nextGenSession.getPrimaryGuideBookSecondPreferenceId();
	final String noEndDateText = "No date specified.";
	if (endDate == null) {
		endDate = noEndDateText;
	}
	ReportBuilder reportBuilder = null;
	switch ( reportType )
    {
        case SPECIAL_FINANCE:
            reportBuilder = new SpecialFinanceOrCertifiedActionPlanReportBuilder(   businessUnitId,
                                                                                    actionPlanReportMode,
                                                                                    daysBack,
                                                                                    SpecialFinanceOrCertifiedActionPlanReportBuilder.ReportTypeEnum.SPECIAL_FINANCE );
            break;
        case CERTIFIED:
            reportBuilder = new SpecialFinanceOrCertifiedActionPlanReportBuilder(   businessUnitId,
                                                                        actionPlanReportMode,
                                                                        daysBack,
                                                                        SpecialFinanceOrCertifiedActionPlanReportBuilder.ReportTypeEnum.CERTIFIED );
            break;
        case UNCHANGED:
            reportBuilder = new UnchangedActionPlanReportBuilder(   businessUnitId,
                                                                    primaryGuideBookFirstPreferenceId,
                                                                    primaryGuideBookSecondPreferenceId);
            break;
        case REPRICING_NO_NOTES:
        case REPRICING:
            // Since reprice is not totally a planning even, for all current we just want the last 5 days.
            if ( (reportType == ActionPlanReport.REPRICING || reportType == ActionPlanReport.REPRICING_NO_NOTES) && actionPlanReportMode == ActionPlanReportModeEnum.CURRENT ) {
                daysBack = 5;
                actionPlanReportMode = ActionPlanReportModeEnum.HISTORY;
            }
            reportBuilder = new ActionPlanReportBuilder(    businessUnitId,
                                                            primaryGuideBookFirstPreferenceId,
                                                            reportType.getPlanningEventTypeEnum().getId(),
                                                            actionPlanReportMode,
                                                            daysBack,
                                                            false );
            break;
        case MODEL_PROMOTION:
            reportBuilder = new ModelPromotionPlansReportBuilder(   businessUnitId,
                                                                    actionPlanReportMode,
                                                                    daysBack );
            break;
        case ADVERTISING:
            reportBuilder = new PrintAdvertisingReportBuilder ( businessUnitId, 
                                                                null, actionPlanReportMode, daysBack ); 
            break;
        case ADVERTISING_INDIVIDUAL_REPORT:
                // for advertsing reportType is passed back as the third party ID this guy is associated with
                reportBuilder = new PrintAdvertisingReportBuilder ( businessUnitId, 
                                                                    thirdPartyId, actionPlanReportMode, daysBack ); 
            break;
        case IN_SERVICE:
            // this is a nice little hack since Detailer (In Service type of thing) event was introduced after all the reports were written.
            reportBuilder = new VehiclesInServiceReportBuilder( businessUnitId,
                                                                primaryGuideBookFirstPreferenceId,
                                                                reportType.getPlanningEventTypeEnum().getId(),
                                                                actionPlanReportMode,
                                                                daysBack );
            break;
        case AUCTION:
        case WHOLESALER:
            reportBuilder = new AuctionAndWholeSalerActionPlanReportBuilder(    businessUnitId,
                                                            primaryGuideBookFirstPreferenceId,
                                                            reportType.getPlanningEventTypeEnum().getId(),
                                                            actionPlanReportMode,
                                                            daysBack, primaryGuideBookFirstPreferenceId,
                                                            primaryGuideBookSecondPreferenceId);
            break;
        case WHOLESALER_INDIVIDUAL:
            reportBuilder = new AuctionAndWholeSalerActionPlanReportBuilder(businessUnitId,
                                                            primaryGuideBookFirstPreferenceId,
                                                            reportType.getPlanningEventTypeEnum().getId(),
                                                            ActionPlanReportModeEnum.CURRENT,
                                                            daysBack, primaryGuideBookFirstPreferenceId,
                                                            primaryGuideBookSecondPreferenceId);
            if(thirdPartyId.equals(-1)) {
            	reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.notes3", SQLOperationEnum.EQ, "Unspecified"));
            } else {
            	reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.thirdPartyEntityId", SQLOperationEnum.EQ, thirdPartyId));
            }
            if (endDate.equalsIgnoreCase(noEndDateText)) {
                reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.endDate", SQLOperationEnum.IS_NULL, null));
            } else {
                reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.endDate", SQLOperationEnum.EQ, endDate));
            }
        break;
        case AUCTION_INDIVIDUAL:
            reportBuilder = new AuctionAndWholeSalerActionPlanReportBuilder(businessUnitId,
                                                            primaryGuideBookFirstPreferenceId,
                                                            reportType.getPlanningEventTypeEnum().getId(),
                                                            ActionPlanReportModeEnum.CURRENT,
                                                            daysBack, primaryGuideBookFirstPreferenceId,
                                                            primaryGuideBookSecondPreferenceId);
            if(thirdPartyId.equals(-1)) {
            	reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.notes3", SQLOperationEnum.EQ, "Unspecified"));
            } else {
            	reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.thirdPartyEntityId", SQLOperationEnum.EQ, thirdPartyId));
            }
            if (endDate.equalsIgnoreCase(noEndDateText)) {
                reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.endDate", SQLOperationEnum.IS_NULL, null));
            } else {
                reportBuilder.addWhereStatement(new SQLWhereSearchStatement("aipEvent.endDate", SQLOperationEnum.EQ, endDate));
            }
        break;
        case INTERNET_ADVERTISEMENT:
        	reportBuilder = new InternetAdvertisingActionPlanReportBuilder(businessUnitId);
        break;
        default:
            reportBuilder = new ActionPlanReportBuilder(    businessUnitId,
                                                            primaryGuideBookFirstPreferenceId,
                                                            reportType.getPlanningEventTypeEnum().getId(),
                                                            actionPlanReportMode,
                                                            daysBack );
            break;
    }
    
    return reportBuilder;
}

protected List<Map> buildNonGroupedResults(List<Map> reportResults, HttpServletRequest request, ActionPlanReportModeEnum actionPlanReportMode, 
                                           Integer daysBack, ActionPlanReport reportType) {
    switch ( reportType ) {
        case CERTIFIED: // certified and special_finance reports behave identically for now
        case SPECIAL_FINANCE:
            reportResults = flatten( reportResults, "inventoryId", new ColumnAppendFlattener( "retailStrats", "," ),
                                     new ColumnAppendFlattener( "notes", ";" ));
            break;
        case OTHER:
        case LOT_PROMOTE:
            reportResults = flatten( reportResults, "inventoryId", new ColumnAppendFlattener( "notes", "," ) );
            // groupedResults = toGroupedResults( reportResults, new String[] { "" },
            // new String[] { "vin" } );
            // reportResults = null;// erase the pagescope variable
            break;
        case IN_SERVICE:
            reportResults = flatten( reportResults, "inventoryId", new ColumnAppendFlattener( "reason", "," ) );
            break;
    }
    return reportResults;
}

protected Map< Map< String, Object >, List< Map >> buildGroupedResults(List<Map> reportResults, HttpServletRequest request, ActionPlanReportModeEnum actionPlanReportMode, 
        Integer daysBack, ActionPlanReport reportType) {
    Map< Map< String, Object >, List< Map > > groupedResults = null;
    switch ( reportType )
    {

        case UNCHANGED:
            reportResults = flatten( reportResults, "inventoryId", new CompareReplaceFlattener( "strategy", "createdAge", true ),
                                        new CompareReplaceFlattener( "objective", "createdAge", true ),
                                        new CompareReplaceFlattener( "notes", "createdAge", true ),
                                        new CompareReplaceFlattener( "createdAge", "createdAge" ) );

            // need to sort the report after replacing values and before grouping it by Objective age
            Collections.sort( reportResults, new ReverseComparator( new RowMapComparator< String >( "createdAge" ) ) );

            groupedResults = toGroupedResults( reportResults, new String[] { "No Category" }, new String[] { "objective" } );
            reportResults = null;// erase the pagescope variable
            break;
        case MODEL_PROMOTION:
            groupedResults = toGroupedResults( reportResults, new String[] { "Vehicle make not specified", "Vehicle model not specified" },
                                                new String[] { "make", "groupingDescription" } );
            reportResults = null;// erase the pagescope variable
            break;
        case AUCTION_INDIVIDUAL:
        case AUCTION:
            groupedResults = toGroupedResults( reportResults, new String[] { "Auction Name not Specified.", "No date specified."}, 
                                               new String[] { "thirdParty", "endDate" } );
            reportResults = null;// erase the pagescope variable
            break;
        case ADVERTISING_INDIVIDUAL_REPORT:
        case ADVERTISING:
            groupedResults = toGroupedResults( reportResults, new String[] { "No Advertising Channel Specified." },
                                                new String[] { "thirdParty" } );
            reportResults = null;// erase the pagescope variable
            break;
        case WHOLESALER_INDIVIDUAL:
        case WHOLESALER:
            groupedResults = toGroupedResults( reportResults, new String[] { "Wholesaler Name not Specified.", "No date specified." },
                                                new String[] { "thirdParty", "endDate" } );
            reportResults = null;// erase the pagescope variable
            break;
        case REPRICING_NO_NOTES:
        case REPRICING:
        	if ( (reportType == ActionPlanReport.REPRICING || reportType == ActionPlanReport.REPRICING_NO_NOTES) && actionPlanReportMode == ActionPlanReportModeEnum.CURRENT ) {
                daysBack = 5;
                actionPlanReportMode = ActionPlanReportModeEnum.HISTORY;
            }
            groupedResults = getRepricingHistory( daysBack, actionPlanReportMode, reportResults );
            reportResults = null;
            break;
        case WHOLESALER_BIDSHEET:
            retrieveVehicleOptions(reportResults, request);
            groupedResults = null;
            break;
        case INTERNET_ADVERTISEMENT:
        	if(reportResults.size() > 0) {
        		Map<String, List<Map>> reportSections = reportResults.get(0);
        		if(reportSections != null) {
        			List<Map> headers = reportSections.get(InternetAdvertisingActionPlanReportBuilder.REPORT_HEADER_NAME); 
        			if(headers != null) {
        				StringBuilder headerName = new StringBuilder();
        				Iterator<Map> iter = headers.iterator();
        				while(iter.hasNext()) {
        					Map<String,Object> provider = iter.next();
        					for(String providerName : provider.keySet()) {
	        					headerName.append(providerName);
        					}
        					if(iter.hasNext()) {
        						headerName.append(", ");
        					}
        				}
        				String[] group = new String[1];
        				group[0] = headerName.toString();
        				Map<String, Object> groupHeader = new HashMap<String, Object>();
        				groupHeader.put("thirdParty", headerName.toString());
    	        		groupedResults = new HashMap<Map<String,Object>, List<Map>>();
    	        		groupedResults.put(groupHeader, ((List<Map>)reportSections.get(InternetAdvertisingActionPlanReportBuilder.REPORT_BODY_NAME)));
    	        		reportResults = null;
        			}
        		}
        	}
        	break;
        default:
            groupedResults = null;// erase the pagescope variable
            break;
    }
    return groupedResults;
}


private List< Map > removedNonPriceChangingConfermations( List< Map > reportResults )
{
	List< Map > nonRepriceEvents = new ArrayList< Map >();
	for ( Map row : reportResults )
	{
		String oldPrice="0.00";
		
		if (row.get("originalPrice") != null)
		{
			oldPrice = row.get( "originalPrice" ).toString() + ".00";
		}
		String newPrice = row.get( "value" ).toString();
		if ( oldPrice.equals( newPrice ) )
		{
			nonRepriceEvents.add( row );
		}
	}
	
	for ( Map nonRepriceEvent : nonRepriceEvents )
	{
		// update the Events field to reflect the fact that this event is being removed
		for ( Map row : reportResults )
		{
			if ( row.get( "inventoryId" ).toString().equals( nonRepriceEvent.get("inventoryId").toString() ))
			{
				row.put( "Events", ((Integer)row.get( "Events" )) - 1 );
			}
		}
		reportResults.remove( nonRepriceEvent );
	}
	return reportResults;
}

/**
 * This "function" takes a list of results, groups them by the key and flattens the column field, so where there was <i>x</i> rows per key,
 * there would be only 1 row per key.
 * 
 * This is a good function to go into a ReportUtils or some sort of ResultSet transformer.
 * 
 * @param reportResults
 * @param key
 * @param column
 * @return
 */
private List< Map > flatten( List< Map > reportResults, String key, Flattener... flatteners )
{
	// Ex: inventoryId - list of inventory + retail strats
	Map< Object, Map > groupedResults = new LinkedHashMap< Object, Map >();

	// group the duplicate rows by key
	for ( Map row : reportResults )
	{
		Object groupKey = row.get( key );
		if ( groupedResults.containsKey( groupKey ) )
		{
			// flattens happens here.

			// if already a row, retrieve that row
			Map existingRow = groupedResults.get( groupKey );

			for ( Flattener flattener : flatteners )
			{
				// update the row with it's existing data and "flatten" it with the new data
				existingRow.put( flattener.getColumn(), ( flattener.doFlatten( existingRow, row ) ) );
			}

			// put it back into the groupedResults
			groupedResults.put( groupKey, existingRow );
		}
		else
		{
			groupedResults.put( groupKey, row );
		}
	}

	return new ArrayList< Map >( groupedResults.values() );
}

/**
 * Using an map as the multikey for a multikey-list map.
 * 
 * @param reportResults
 * @param defaultGroupDescription
 * @param group
 * @return
 */
protected Map< Map< String, Object >, List< Map >> toGroupedResults( List< Map > reportResults, String[] defaultGroupDescription, String[] group )
{
	Map< Map< String, Object >, List< Map >> groupedResults = new LinkedHashMap< Map< String, Object >, List< Map >>();

	// for each row in the results
	for ( Map row : reportResults )
	{
		// need to keep the order of insertion
		Map< String, Object > multiKey = new LinkedHashMap< String, Object >();
		for ( int i = 0; i < group.length; i++ )
		{
			String column = group[i];
			if ( row.containsKey( column ) && row.get( column ) != null )
			{
				multiKey.put( column, row.get( column ) );
			}
			else
			{
				multiKey.put( column, defaultGroupDescription[i] );
			}
		}

		List< Map > rowsInGroup = null;
		if ( groupedResults.containsKey( multiKey ) )
		{
			rowsInGroup = groupedResults.get( multiKey );
		}
		else
		{
			rowsInGroup = new ArrayList< Map >();
		}
		rowsInGroup.add( row );
		groupedResults.put( multiKey, rowsInGroup );
	}
	return groupedResults;
}

public IReportService getTransactionalReportService()
{
	return transactionalReportService;
}

public void setTransactionalReportService( IReportService transactionalReportService )
{
	this.transactionalReportService = transactionalReportService;
}

}