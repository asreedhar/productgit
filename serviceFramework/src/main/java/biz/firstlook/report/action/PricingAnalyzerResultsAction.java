package biz.firstlook.report.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.enumerator.SaleTypeEnum;
import biz.firstlook.report.reportBuilders.MileageRangeReportBuilder;
import biz.firstlook.report.reportBuilders.PricingReportBuilder;
import biz.firstlook.report.reportBuilders.TrimReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class PricingAnalyzerResultsAction extends NextGenAction
{

private IReportService reportService;
private PreferenceService preferenceService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	int groupingDescriptionId = Integer.parseInt( request.getParameter( "groupingDescriptionId" ) );
	int saleTypeId = Integer.parseInt( request.getParameter( "saleTypeId" ) );
	int mileageRangeId = Integer.parseInt( request.getParameter( "mileageRangeId" ) );
	int timePeriodId = Integer.parseInt( request.getParameter( "timePeriodId" ) );
	int trimId = Integer.parseInt( request.getParameter( "trimId" ) );

	PricingReportBuilder pricingReportBuilder = new PricingReportBuilder( SaleTypeEnum.getEnumFromSaleTypeId( saleTypeId ) );
	pricingReportBuilder.setBusinessUnitId( getNextGenSession( request ).getBusinessUnitId() );
	pricingReportBuilder.setMileageRangeId( mileageRangeId );
	pricingReportBuilder.setPeriodId( timePeriodId );
	pricingReportBuilder.setSaleTypeId( saleTypeId );
	pricingReportBuilder.setTrimId( trimId );
	pricingReportBuilder.setVehicleGroupingId( groupingDescriptionId );
	List< Map > resultBeans = getReportService().getResults( pricingReportBuilder );

	setViewDealsStartAndEndMileage( mileageRangeId, request );
	setViewDealsTrim( trimId, groupingDescriptionId, request );
	
	request.setAttribute( "year", request.getParameter( "year" ) );
	request.setAttribute( "mileageRangeId", mileageRangeId );  
	request.setAttribute( "timePeriodId", timePeriodId );
	request.setAttribute( "trimId", trimId );
	request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
	request.setAttribute( "saleTypeId", saleTypeId );
	request.setAttribute( "pricingAnalyzerResults", resultBeans );

	return mapping.findForward( "success" );
}


public void setViewDealsStartAndEndMileage( int mileageRangeId, HttpServletRequest request )
{
	MileageRangeReportBuilder mileageRangeReportBuilder = new MileageRangeReportBuilder();
	mileageRangeReportBuilder.setBucketDXMId( mileageRangeId );
	List< Map > mileageRange = getReportService().getResults( mileageRangeReportBuilder );
	
	int lowMileage = (Integer)mileageRange.get( 0 ).get("LowRange");
	int highMileage = (Integer)mileageRange.get( 0 ).get("HighRange");
	
	request.setAttribute( "lowMileage", lowMileage );
	request.setAttribute( "highMileage", highMileage );
	
}

private void setViewDealsTrim( int trimId, int groupingDescriptionId, HttpServletRequest request )
{
	String trim = "ALL";
	
	TrimReportBuilder trimReportBuilder = new TrimReportBuilder();
	trimReportBuilder.setBusinessUnitId( getNextGenSession( request ).getBusinessUnitId() );
	trimReportBuilder.setSaleTypeId( SaleTypeEnum.RETAIL.getSaleTypeId() );
	trimReportBuilder.setVehicleGroupingId( groupingDescriptionId );
	trimReportBuilder.setTrimId( trimId );
	List< Map > trims = getReportService().getResults( trimReportBuilder );
	
	if ( !trims.isEmpty() ) 
	{
		trim = (String)trims.get( 0 ).get( "Trim" );
	}
	
	request.setAttribute( "trim", trim );
	
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}


public PreferenceService getPreferenceService() {
	return preferenceService;
}


public void setPreferenceService(PreferenceService preferenceService) {
	this.preferenceService = preferenceService;
}

}
