package biz.firstlook.report.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.GroupingDescription;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.persist.common.hibernate.GroupingDescriptionDAO;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.report.reportBuilders.PeriodsReportBuilder;
import biz.firstlook.report.reportBuilders.ViewDealsReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class ViewDealsAction extends NextGenAction
{

private IReportService reportService;
private IReportService transactionalReportService;
private CoreService coreService;
private PreferenceService preferenceService;
private GroupingDescriptionDAO groupingDescriptionDAO;

private final static int USED_CAR_DEALERSHIP = 0;
private final static int USED_CAR_DEALER_GROUP = 1;

public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
    Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
    
	List dateRange = getDateRange( request );
	List mileageRange = getMileageRange( request, businessUnitId );
	String saleType = getSaleType( request );

	int groupingDescriptionId = Integer.parseInt( request.getParameter( "groupingDescriptionId" ));
	
	ViewDealsReportBuilder viewDealsRB = new ViewDealsReportBuilder( dateRange, mileageRange, saleType );
	int reportType = Integer.parseInt( request.getParameter( "reportType" ) );
	switch ( reportType )
	{
		case USED_CAR_DEALERSHIP:
			viewDealsRB.setGroupingDescriptionId( groupingDescriptionId );
			viewDealsRB.setBusinessUnitId( businessUnitId );
			request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
			break;
		case USED_CAR_DEALER_GROUP:
			viewDealsRB.setGroupingDescriptionId( groupingDescriptionId );
			viewDealsRB.setParentId( getNextGenSession( request ).getParentId() );

			BusinessUnit bu = coreService.findBusinessUnitByID( getNextGenSession( request ).getParentId() );
			request.setAttribute( "parentName", bu.getName() );

			request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
			break;
		default:
			break;

	}
	if ( request.getParameter( "trim" ) != null
			&& !request.getParameter( "trim" ).equals( "" ) && !request.getParameter( "trim" ).equals( "ALL" ) )
	{
		viewDealsRB.setTrim( request.getParameter( "trim" ) );
		request.setAttribute( "trim", request.getParameter( "trim" ) );
	}

	List< Map > viewDealsResults = getTransactionalReportService().getResults( viewDealsRB );

	viewDealsRB.setNoSales();
	List< Map > viewDealsNoSalesResults = getTransactionalReportService().getResults( viewDealsRB );

	String makeModelDescription = "";
	if ( viewDealsResults.size() > 0 ) {
		makeModelDescription = viewDealsResults.get(0).get("Make") + " " + viewDealsResults.get(0).get("Model");
	} else {
		makeModelDescription = retrieveMakeAndModelByReference( groupingDescriptionId );
	}
	
	request.setAttribute("makeModelDescription", makeModelDescription);
	request.setAttribute( "reportType", reportType );
	request.setAttribute( "sales", viewDealsResults );
	request.setAttribute( "noSales", viewDealsNoSalesResults );
	request.setAttribute( "saleType", saleType );
	request.setAttribute( "mode", request.getParameter( "mode" ) );
	
	setComingFromAttributes(request);
	
	return mapping.findForward( "success" );
}

private String retrieveMakeAndModelByReference( int groupingDescriptionId ) {
	GroupingDescription gd = groupingDescriptionDAO.findById(groupingDescriptionId);
	return ( gd != null) ? gd.getGroupingDescription() : "";
}

private void setComingFromAttributes( HttpServletRequest request ){
	String comingFrom = request.getParameter( "comingFrom" );
		
	if(comingFrom != null && comingFrom.equalsIgnoreCase("PA")){
		if(request.getParameter( "showPingII" ) != null){
			//to show pingII icon when going back to previous page
			request.setAttribute( "showPingII", request.getParameter( "showPingII" ) );
		}
		if(request.getParameter( "appraisalId" ) != null){
			request.setAttribute( "appraisalId", request.getParameter( "appraisalId" ) );
		}
		if(request.getParameter( "inventoryId" ) != null){
			request.setAttribute( "inventoryId", request.getParameter( "inventoryId" ) );
		}
	}
	request.setAttribute( "comingFrom", comingFrom);	
}


private String getSaleType( HttpServletRequest request )
{
	String saleType = "R";
	if ( request.getParameter( "saleTypeId" ) != null && request.getParameter( "saleTypeId" ).equals( "2" ) )
	{
		saleType = "W";
	}
	request.setAttribute( "saleTypeId", request.getParameter( "saleTypeId" ) );
	return saleType;
}

private List getDateRange( HttpServletRequest request )
{
	Calendar cal = Calendar.getInstance();
	Date startDate, endDate;
	if ( request.getParameter( "isForecast" ) != null
			&& request.getParameter( "isForecast" ) != "" && Boolean.parseBoolean( request.getParameter( "isForecast" ) ) )
	{
		// forecasting
		cal.add( Calendar.WEEK_OF_YEAR, -52 );
		startDate = DateUtils.truncate( new Date( cal.getTimeInMillis() ), Calendar.DATE );
		cal.add( Calendar.WEEK_OF_YEAR, +13 );
		endDate = DateUtils.truncate( new Date( cal.getTimeInMillis() ), Calendar.DATE );
		request.setAttribute( "isForecast", Boolean.TRUE );
		request.setAttribute( "weeks", 13 );
	}
	else if ( request.getParameter( "comingFrom" ) != null && request.getParameter( "comingFrom" ).equalsIgnoreCase( "PA" ) )
	{
		 PeriodsReportBuilder periodReportBuilder = new PeriodsReportBuilder();
		 periodReportBuilder.setPeriodId( Integer.parseInt( request.getParameter( "timePeriodId" ) ) );
		 List< Map > periodsDisplayBeans = getReportService().getResults( periodReportBuilder );
		 startDate = (Date)periodsDisplayBeans.get( 0 ).get( "BeginDate" );
		 endDate = (Date)periodsDisplayBeans.get( 0 ).get( "EndDate" );
		 request.setAttribute( "year", request.getParameter( "year" ) );
		 request.setAttribute( "weeks", periodsDisplayBeans.get( 0 ).get( "Description" ) );
		 request.setAttribute( "timePeriodId", Integer.parseInt( request.getParameter( "timePeriodId" ) ) );
	}
	else
	{
		int weeks = Integer.parseInt( request.getParameter( "weeks" ) );
		cal.add( Calendar.WEEK_OF_YEAR, -weeks );
		startDate = DateUtils.truncate( new Date( cal.getTimeInMillis() ), Calendar.DATE );
		endDate = DateUtils.truncate( new Date(), Calendar.DATE );
		request.setAttribute( "weeks", weeks );
	}
	List< String > dateRange = new ArrayList< String >();
	dateRange.add( new Timestamp( startDate.getTime() ).toString() );
	dateRange.add( new Timestamp( endDate.getTime() ).toString() );
	return dateRange;
}

private List getMileageRange( HttpServletRequest request, Integer businessUnitId )
{
	List< Integer > mileageRange = new ArrayList< Integer >();
	Integer lowMileage = -1;
	Integer highMileage = Integer.MAX_VALUE;
	if ( request.getParameter( "lowMileage" ) != null )
	{
		lowMileage = Integer.parseInt( request.getParameter( "lowMileage" ) );
	}
    if ( request.getParameter( "highMileage" ) != null )
    {
        highMileage = Integer.parseInt( request.getParameter( "highMileage" ) );
    }
	if ( request.getParameter( "applyMileageFilter" ) != null && request.getParameter( "applyMileageFilter" ).equalsIgnoreCase("true"))
	{
        highMileage =  (Integer) preferenceService.getPreferences(businessUnitId,
                PreferenceService.ReportsEnum.VIEW_DEALS).get("HighMileageThreshold");
	}
	mileageRange.add( lowMileage );
	mileageRange.add( highMileage );
	request.setAttribute( "highMileage", highMileage );
	return mileageRange;
}

public CoreService getCoreService()
{
	return coreService;
}

public void setCoreService( CoreService coreService )
{
	this.coreService = coreService;
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public IReportService getTransactionalReportService()
{
	return transactionalReportService;
}

public void setTransactionalReportService( IReportService transactionalReportService )
{
	this.transactionalReportService = transactionalReportService;
}

public void setGroupingDescriptionDAO(
		GroupingDescriptionDAO groupingDescriptionDAO) {
	this.groupingDescriptionDAO = groupingDescriptionDAO;
}
}
