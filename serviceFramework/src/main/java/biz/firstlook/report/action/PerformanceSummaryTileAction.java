package biz.firstlook.report.action;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightService;
import biz.firstlook.commons.services.insights.InsightServiceException;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.InventoryTypeEnum;
import biz.firstlook.main.enumerator.PerformancePlusDimensionEnum;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.report.reportBeans.PerformancePlusTable;
import biz.firstlook.report.reportBeans.PerformancePlusTableRow;
import biz.firstlook.report.reportBuilders.PerformancePlusReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;

public class PerformanceSummaryTileAction extends NextGenAction
{
private InsightService insightService;
private IReportService reportService;
private CoreService coreService;
final static Integer NUMBER_OF_WEEKS = 26;

public PerformanceSummaryTileAction()
{
	super();
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	String make = request.getParameter( "make" ).toUpperCase();
	String model = request.getParameter( "model" ).toUpperCase();
	String trim = request.getParameter( "trim" );
	String year = request.getParameter( "year" );
	String mileage = request.getParameter( "mileage" );
	String groupingDescriptionId = request.getParameter( "groupingDescriptionId" );
	String vin = request.getParameter( "vin" );
	String vehicleEntityTypeId = request.getParameter("vehicleEntityTypeId");
	String vehicleEntityId = request.getParameter("vehicleEntityId");
	
	if ( trim != null )
	{
		trim.toUpperCase();
	}
	
	// build parameter object for service call for insights
	InsightParameters params = new InsightParameters(
			businessUnitId,
			Integer.parseInt(groupingDescriptionId),
			InventoryTypeEnum.USED.ordinal(),
			NUMBER_OF_WEEKS,
			trim,
			Integer.parseInt(year),
			Integer.parseInt(mileage));
	// get the insights (if any)
	List<Insight> insights = Collections.emptyList();
	try {
		insights = getInsightService().getInsights(params);
	}
	catch (InsightServiceException ise) {
		Logger.getLogger(PerformanceSummaryTileAction.class).error("Failed to get Insights", ise);
	}
	
	if ( !insights.isEmpty() )
	{
		request.setAttribute( "insights", insights );	
	
		if (InsightUtils.containsTrafficLight(insights)) {
			request.setAttribute("lightId", InsightUtils.getTrafficLight(insights).getInsightValue());
		}
		
		request.setAttribute( "weeks", NUMBER_OF_WEEKS );
	}
	PerformancePlusReportBuilder trimReportBuilder = new PerformancePlusReportBuilder(PerformancePlusDimensionEnum.TRIM, businessUnitId, 
	                                                                                  NUMBER_OF_WEEKS, Integer.parseInt( groupingDescriptionId ),
	                                                                                  2, false, false, false);
	

	PerformancePlusReportBuilder overallReportBuilder = new PerformancePlusReportBuilder(PerformancePlusDimensionEnum.ALL, businessUnitId, 
	                                                                                  NUMBER_OF_WEEKS, Integer.parseInt( groupingDescriptionId ),
	                                                                                  2, false, false, false);
	
//
//	PerformanceSummaryCompositeReportBuilder trimReportBuilder = new PerformanceSummaryCompositeReportBuilder(	businessUnitId,
//	                                                                                                          	InventoryTypeEnum.USED.ordinal(),
//	                                                                                                      	NUMBER_OF_WEEKS,
//	                                                                                                      	make,
//	                                                                                                      	line,
//	                                                                                                      	trim,
//	                                                                                                      	includeDealerGroup);
//
//	
//	PerformanceSummaryCompositeReportBuilder overallReportBuilder = new PerformanceSummaryCompositeReportBuilder(	businessUnitId,
//	                                                                                                             	InventoryTypeEnum.USED.ordinal(),
//																												NUMBER_OF_WEEKS,
//																												make,
//																												line,
//																												null,
//																												includeDealerGroup);
//
	List< Map > trimReport = getReportService().getResults( trimReportBuilder );
	
	//List<Map<PerformancePlusDimensionEnum,PerformancePlusTable>>
	List< Map > overallReport = getReportService().getResults( overallReportBuilder );

	Map< String, PerformancePlusTableRow > result = new HashMap< String, PerformancePlusTableRow >();
	
	if ( overallReport.size() > 0 ) {
		//<PerformancePlusDimensionEnum,PerformancePlusTable>
		Map overallReportRow = overallReport.get( 0 );
		if(overallReportRow != null ) {
			PerformancePlusTable table = (PerformancePlusTable)overallReportRow.get(PerformancePlusDimensionEnum.ALL);
			if(table != null) {
				List<PerformancePlusTableRow> rows = table.getRows();
				if(rows != null && !rows.isEmpty()) {
					PerformancePlusTableRow row = rows.get(0);
					result.put( "overall", row);
				}
			}
		}
	}
	
	PerformancePlusTableRow theRowIWant = null;
	if ( trimReport.size() > 0 )
	{
		for (PerformancePlusTableRow aRow : ((PerformancePlusTable)trimReport.get( 0 ).get( PerformancePlusDimensionEnum.TRIM )).getRows() )
		{
			if (aRow.getGroupingColumn().equalsIgnoreCase( trim ))
			{
				theRowIWant = aRow;
				break;
			}
		}
	    result.put( "trim", theRowIWant );
	}
	
	Integer memberId = getNextGenSession( request ).getMemberId();
	
	Member member = getCoreService().findMemberByID( memberId );
	
	try {
		request.setAttribute( "hasAutocheck", AutoCheckService.getInstance().hasAccount(businessUnitId, member.getLogin()) );
	} catch (AutoCheckException ace) {
		//prevent rest of page blowing up, what to do for autocheck error???
	} 
	
	try {
		if (CarfaxService.getInstance().hasAccount(businessUnitId, member.getLogin())) {
			request.setAttribute( "hasCarfax" , true);
			CarfaxReportTO carfaxTO = CarfaxService.getInstance().getCarfaxReport(businessUnitId, member.getLogin(), vin);
			if (carfaxTO == null) {
				request.setAttribute( "reportAvailable" , false);
			} else {
				request.setAttribute( "reportAvailable" , true);
				request.setAttribute( "reportType" , carfaxTO.getReportType());
			}
		}
	} catch (CarfaxException ce) {
		request.setAttribute("hasCarfaxError", true);
		request.setAttribute("problem", ce.getResponseCode());
	}
	request.setAttribute( "perfSummaryReport", result );
	request.setAttribute( "make", make );
	request.setAttribute( "model", model );
	request.setAttribute( "trim", trim );
	request.setAttribute( "vin", vin );
	request.setAttribute( "year", year);
	request.setAttribute( "vehicleEntityTypeId", vehicleEntityTypeId);
	request.setAttribute( "vehicleEntityId", vehicleEntityId);
	request.setAttribute( "dealerId" , businessUnitId );
	
	return mapping.findForward( "success" );
}

public InsightService getInsightService() {
	return insightService;
}

public void setInsightService(InsightService insightService) {
	this.insightService = insightService;
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

public CoreService getCoreService()
{
	return coreService;
}

public void setCoreService( CoreService coreService )
{
	this.coreService = coreService;
}

}
