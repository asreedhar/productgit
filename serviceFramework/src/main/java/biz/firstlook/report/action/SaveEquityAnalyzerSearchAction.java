package biz.firstlook.report.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.report.service.EquityAnalyzerSearchService;

public class SaveEquityAnalyzerSearchAction extends NextGenAction
{
private EquityAnalyzerSearchService equityAnalyzerSearchService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();

	DynaActionForm equityAnalyzerForm = (DynaActionForm)form;
	String guidebookName = (String)equityAnalyzerForm.get( "guidebookName" );
	Integer percentageMultiplier = (Integer)equityAnalyzerForm.get( "percentageMultiplier" );
	Integer thirdPartyCategoryId = (Integer)equityAnalyzerForm.get( "guidebookCategoryId" );

	getEquityAnalyzerSearchService().saveSearch( businessUnitId, guidebookName, thirdPartyCategoryId, percentageMultiplier );

	String savedSearchMessage = buildSavedSearchMessage( percentageMultiplier, guidebookName, thirdPartyCategoryId );
	request.setAttribute( "savedSearchMessage", savedSearchMessage );

	return mapping.findForward( "success" );
}

public String buildSavedSearchMessage( Integer percentageMultiplier, String guidebookName, Integer thirdPartyCategoryId )
{
	StringBuffer message = new StringBuffer();
	message.append( percentageMultiplier );
	message.append( "% of " );
	message.append( guidebookName );
	message.append( " " );
	message.append( ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId ).getDescription() );
	message.append( " <i>Saved</i>" );

	return message.toString();
}

public EquityAnalyzerSearchService getEquityAnalyzerSearchService()
{
	return equityAnalyzerSearchService;
}

public void setEquityAnalyzerSearchService( EquityAnalyzerSearchService equityAnalyzerSearchService )
{
	this.equityAnalyzerSearchService = equityAnalyzerSearchService;
}

}
