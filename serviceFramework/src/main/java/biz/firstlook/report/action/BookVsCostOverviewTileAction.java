package biz.firstlook.report.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.report.reportBuilders.BookVsCostReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class BookVsCostOverviewTileAction extends NextGenAction
{

private IReportService reportService;

@SuppressWarnings("unchecked")
@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{	
	Integer primaryGuideBookPref1Integer = getNextGenSession( request ).getPrimaryGuideBookFirstPreferenceId();
	Integer primaryGuideBookPref2Integer = getNextGenSession( request ).getPrimaryGuideBookSecondPreferenceId();
	String primaryGuideBookName = null;
	
	List< ThirdPartyCategoryEnum > thirdPartyCategoryIds = new ArrayList< ThirdPartyCategoryEnum >();
	if ( primaryGuideBookPref1Integer != null )
	{
		ThirdPartyCategoryEnum primaryGuideBookPref1 = ThirdPartyCategoryEnum.getEnumById( primaryGuideBookPref1Integer.intValue() );
		thirdPartyCategoryIds.add( primaryGuideBookPref1 );
		primaryGuideBookName = primaryGuideBookPref1.getThirdPartyEnum().getDescription();
	}
	if ( primaryGuideBookPref2Integer != null )
	{
		ThirdPartyCategoryEnum primaryGuideBookPref2 = ThirdPartyCategoryEnum.getEnumById( getNextGenSession( request ).getPrimaryGuideBookSecondPreferenceId().intValue() );
		if ( primaryGuideBookPref2 != null )
		{
			thirdPartyCategoryIds.add( primaryGuideBookPref2 );
		}
	}
	
	
	BookVsCostReportBuilder report = new BookVsCostReportBuilder();

	report.setBusinessUnitId( getNextGenSession(request).getBusinessUnitId() );
	report.setThirdPartyCategoryIds( thirdPartyCategoryIds );

	List< Map > reportDisplayBeans = getReportService().getResults( report );
	
	//Correct order if necessary. 
	if (!reportDisplayBeans.isEmpty())
	{
		if ( !((Integer)reportDisplayBeans.get( 0 ).get( "ThirdPartyCategoryID" )).equals( primaryGuideBookPref1Integer ) )
		{
			Collections.reverse( reportDisplayBeans );
		}
	}
	
	request.setAttribute( "bookVsCostDisplayBeans", reportDisplayBeans );
	request.setAttribute( "primaryBookName",  primaryGuideBookName );

	return mapping.findForward( "success" );
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

}
