package biz.firstlook.report.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.entity.EquityAnalyzerSearch;
import biz.firstlook.entity.ThirdPartyCategories;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.ThirdPartyCategoriesService;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.main.enumerator.ThirdPartyEnum;
import biz.firstlook.report.reportBuilders.EquityAnalyzerReportBuilder;
import biz.firstlook.report.reportBuilders.aip.EquityAnalyzerReportBuilderforMMR;
import biz.firstlook.report.reportFramework.IReportService;
import biz.firstlook.report.service.EquityAnalyzerSearchService;

public class EquityAnalyzerAction extends NextGenAction
{
private IReportService reportService;
private ThirdPartyCategoriesService thirdPartyCategoriesService;
private EquityAnalyzerSearchService equityAnalyzerSearchService;

public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();

	// fake a struts DispatchAction
	String runSavedSearch = request.getParameter( "runsavedsearch" );

	ActionForward forward = mapping.findForward( "success" );

	if ( runSavedSearch != null && runSavedSearch.equals( "true" ) )
	{
		forward = savedSearch( mapping, form, request, businessUnitId );
	}
	else
	{
		forward = search( mapping, form, request, businessUnitId );
	}

	boolean hasKBBTradeInUpgrade = getNextGenSession( request ).isIncludeKBBTradeIn();
	
	List< List<ThirdPartyCategories> > thirdPartyCategories = getThirdPartyCategoriesService().getThirdPartyCategories( businessUnitId );
	List< String > guidebookNames = formatGuidebookNames( thirdPartyCategories );
	List< ThirdPartyCategories > primaryCategories = (List< ThirdPartyCategories >)thirdPartyCategories.get( 0 );
	List< ThirdPartyCategories > secondaryCategories = (List< ThirdPartyCategories >)thirdPartyCategories.get( 1 );
	List< ThirdPartyCategories > tertiaryCategories = (List< ThirdPartyCategories >)thirdPartyCategories.get( 2 );
	
	updateGuideBookCategoriesList( primaryCategories, secondaryCategories,hasKBBTradeInUpgrade );
	
	List< EquityAnalyzerSearch > savedSearches = getEquityAnalyzerSearchService().findByBusinessUnitId( businessUnitId );

	// request.setAttribute( "equityAnalyzerForm", form );
	request.setAttribute( "guidebookNames", guidebookNames );
	request.setAttribute( "primaryCategories", primaryCategories );
	request.setAttribute( "secondaryCategories", secondaryCategories );
	request.setAttribute( "tertiaryCategories", tertiaryCategories );
	request.setAttribute( "savedSearches", savedSearches );

	return forward;
}

private void updateGuideBookCategoriesList( List< ThirdPartyCategories > primaryCategories, List< ThirdPartyCategories > secondaryCategories, boolean hasKBBTradeInUpgrade )
{
	List< ThirdPartyCategories > toBeRemoved = new ArrayList< ThirdPartyCategories >();
	for( ThirdPartyCategories tpc : primaryCategories )
	{
		if( ( tpc.getId().intValue() == ThirdPartyCategoryEnum.KBB_PRIVATE_PARTY.getThirdPartyCategoryId() ) ||
				( !hasKBBTradeInUpgrade && tpc.getId().intValue() == ThirdPartyCategoryEnum.KBB_TRADE_IN.getThirdPartyCategoryId() ) )
		{
			toBeRemoved.add( tpc );
		}
	}
	primaryCategories.removeAll( toBeRemoved );
	toBeRemoved = new ArrayList< ThirdPartyCategories >();
	for( ThirdPartyCategories tpc : secondaryCategories )
	{
		if (( tpc.getId().intValue() == ThirdPartyCategoryEnum.KBB_PRIVATE_PARTY.getThirdPartyCategoryId() )||
		( !hasKBBTradeInUpgrade && tpc.getId().intValue() == ThirdPartyCategoryEnum.KBB_TRADE_IN.getThirdPartyCategoryId() ) )
		{
			toBeRemoved.add( tpc );
		}
	}
	secondaryCategories.removeAll( toBeRemoved );
}

public List< String > formatGuidebookNames( List< List<ThirdPartyCategories> > thirdPartyCategories )
	{
	List<String> guideBooks = new ArrayList< String >();

	for ( List<ThirdPartyCategories> guideBookThirdPartyCategories : thirdPartyCategories )
	{
		if ( guideBookThirdPartyCategories != null && guideBookThirdPartyCategories.size() > 0 )
		{
			int thirdPartyId = ( guideBookThirdPartyCategories.get( 0 ) ).getThirdPartyId();
			guideBooks.add( ThirdPartyEnum.getEnumById( thirdPartyId ).getDescription() );
		}
	}
	return guideBooks;
}

private ActionForward search( ActionMapping mapping, ActionForm form, HttpServletRequest request, Integer businessUnitId )
{
	DynaActionForm equityAnalyzerForm = (DynaActionForm)form;

    Integer percentageMultiplier = (Integer)equityAnalyzerForm.get( "percentageMultiplier" );
	if ( percentageMultiplier == null )
	{
		percentageMultiplier = 100;
	}

	Integer thirdPartyCategoryId = (Integer)equityAnalyzerForm.get( "guidebookCategoryId" );
	if ( thirdPartyCategoryId == null )
	{
		thirdPartyCategoryId = getNextGenSession( request ).getPrimaryGuideBookFirstPreferenceId();
	}

	String guidebookName = (String)equityAnalyzerForm.get( "guidebookName" );
	if ( guidebookName == null || guidebookName.trim().length() == 0 )
	{
		guidebookName = ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId ).getThirdPartyEnum().getDescription();
	}
	
	return runReport( mapping, form, request, businessUnitId, thirdPartyCategoryId, percentageMultiplier );
}

private ActionForward savedSearch( ActionMapping mapping, ActionForm form, HttpServletRequest request, Integer businessUnitId )
{
	DynaActionForm equityAnalyzerForm = (DynaActionForm)form;
	Integer equityAnalyzerSearchId = (Integer)equityAnalyzerForm.get( "savedSearches" );
	if ( equityAnalyzerSearchId == null || equityAnalyzerSearchId == 0)
	{
		return mapping.findForward( "failure" );
	}
		
	EquityAnalyzerSearch equityAnalyzerSearch = getEquityAnalyzerSearchService().findById( equityAnalyzerSearchId );
	Integer percentageMultiplier = equityAnalyzerSearch.getPercentMultiplier();
	Integer thirdPartyCategoryId = equityAnalyzerSearch.getThirdPartyCategoryId();

	request.setAttribute( "lastRunSearchId", equityAnalyzerSearchId );
	
	return runReport( mapping, form, request, businessUnitId, thirdPartyCategoryId, percentageMultiplier );
}

@SuppressWarnings("unchecked")
private ActionForward runReport( ActionMapping mapping, ActionForm form, HttpServletRequest request, Integer businessUnitId,
								Integer thirdPartyCategoryId, Integer percentageMultiplier )
{
	List< Map > results = new ArrayList<Map>();
	
	if(thirdPartyCategoryId==ThirdPartyCategoryEnum.MMR_AVERAGE.getThirdPartyCategoryId())
	{
		results = getReportService().getResults(new EquityAnalyzerReportBuilderforMMR(	businessUnitId,percentageMultiplier ));
	}
	else
	{
	    results = getReportService().getResults(new EquityAnalyzerReportBuilder(businessUnitId,thirdPartyCategoryId,percentageMultiplier ) );
	}

	String guidebookName = ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId ).getThirdPartyEnum().getDescription();
	String guidebookCategory = ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId ).getDescription();

	request.setAttribute( "equity", results );
	request.setAttribute( "guidebookName", guidebookName );
	request.setAttribute( "guidebookCategoryDesc", guidebookCategory );
	request.setAttribute( "guidebookCategoryId", thirdPartyCategoryId );
	request.setAttribute( "percentageMultiplier", percentageMultiplier );

    clearForm(form);
    
	return mapping.findForward( "success" );
}

private void clearForm(ActionForm form) {
    DynaActionForm equityAnalyzerForm = (DynaActionForm)form;
    equityAnalyzerForm.set("percentageMultiplier", null);
    equityAnalyzerForm.set( "guidebookCategoryId", null );
    equityAnalyzerForm.set( "guidebookName", null );
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

public ThirdPartyCategoriesService getThirdPartyCategoriesService()
{
	return thirdPartyCategoriesService;
}

public void setThirdPartyCategoriesService( ThirdPartyCategoriesService thirdPartyCategoriesService )
{
	this.thirdPartyCategoriesService = thirdPartyCategoriesService;
}

public EquityAnalyzerSearchService getEquityAnalyzerSearchService()
{
	return equityAnalyzerSearchService;
}

public void setEquityAnalyzerSearchService( EquityAnalyzerSearchService equityAnalyzerSearchService )
{
	this.equityAnalyzerSearchService = equityAnalyzerSearchService;
}
}
