package biz.firstlook.report.action;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.enumerator.ActionPlanReportModeEnum;
import biz.firstlook.main.enumerator.PlanningEventTypeEnum;
import biz.firstlook.planning.entity.thirdParty.InventoryThirdParty;
import biz.firstlook.planning.service.PrintAdvertiserService;
import biz.firstlook.report.reportBuilders.AuctionAndWholeSalerActionPlanReportBuilder;
import biz.firstlook.report.reportBuilders.ReportBuilder;

public class ActionPlanReportDisplayAction extends ActionPlanReportSubmitAction
{

private PrintAdvertiserService printAdvertiserService;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();

	// Retail Plans
	request.setAttribute( "lRetailPlans", new String[] { ActionPlanReport.SPIFF.getDescription(),
			ActionPlanReport.LOT_PROMOTE.getDescription(), ActionPlanReport.MODEL_PROMOTION.getDescription() } );

	// print advertising plans
	List< InventoryThirdParty > activePrintAdvertiserThirdParties = printAdvertiserService.retriveAllInventoryThirdPartiesWithActivePlansInLast30Days( businessUnitId );
	request.setAttribute( "lPrintAdvertising", activePrintAdvertiserThirdParties.toArray() );
	request.setAttribute( "fullAdvertising", ActionPlanReport.ADVERTISING.getDescription() );
	
	// Retail Reports
	request.setAttribute( "lRetailReports", new String[] { ActionPlanReport.REPRICING.getDescription(),
			ActionPlanReport.SPECIAL_FINANCE.getDescription(), ActionPlanReport.CERTIFIED.getDescription() } );

	//	All Auction Plans
	request.setAttribute( "auction", new String (ActionPlanReport.AUCTION.getDescription()) );

	//All Wholesale Plans
	request.setAttribute( "wholesale", new String (ActionPlanReport.WHOLESALER.getDescription() ) );
	
	// Wholesale Reports
	request.setAttribute( "lWholesaleReports", new String[] { ActionPlanReport.WHOLESALER_BIDSHEET.getDescription() } );

	// Other
	request.setAttribute( "lOtherPlans", new String[] { ActionPlanReport.IN_SERVICE.getDescription(),
			ActionPlanReport.UNCHANGED.getDescription(), ActionPlanReport.OTHER.getDescription() } );

	// the radio buttons
	// request.setAttribute( "lTimePeriodMode", new String[] { ActionPlanReportMode.CURRENT.toString(), ActionPlanReportMode.HISTORY.toString()
	// } );
	// need to find a good way so that the input="text" box is also taken care of on ActionPlanReportMode.HISTORY

	ReportBuilder reportBuilder = new AuctionAndWholeSalerActionPlanReportBuilder(	getNextGenSession( request ).getBusinessUnitId(),
        	getNextGenSession( request ).getPrimaryGuideBookFirstPreferenceId(),
        	PlanningEventTypeEnum.AUCTION.getId(),
        	ActionPlanReportModeEnum.CURRENT ,1,getNextGenSession(request).getPrimaryGuideBookFirstPreferenceId(),
            getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId() );
	List< Map > reportResults = getTransactionalReportService().getResults( reportBuilder );
	LinkedHashMap auctionResults = (LinkedHashMap) toGroupedResults( reportResults, new String[] { "-1", "Auction Name not Specified.", "No date specified."}, new String[] { "thirdPartyId", "thirdParty", "endDate" } );
	Set allAuctions = auctionResults.keySet();
	formatSetDates(allAuctions);
	request.setAttribute("lAuctions", allAuctions);
    
	reportBuilder = new AuctionAndWholeSalerActionPlanReportBuilder(getNextGenSession( request ).getBusinessUnitId(),
        	getNextGenSession( request ).getPrimaryGuideBookFirstPreferenceId(),
        	PlanningEventTypeEnum.WHOLESALER.getId(),
        	ActionPlanReportModeEnum.CURRENT ,1,getNextGenSession(request).getPrimaryGuideBookFirstPreferenceId(),
            getNextGenSession(request).getPrimaryGuideBookSecondPreferenceId() );
	reportResults = getTransactionalReportService().getResults( reportBuilder );
	LinkedHashMap wholeResults = (LinkedHashMap) toGroupedResults( reportResults, new String[] { "-1", "Wholesaler Name not Specified.", "No date specified."}, new String[] { "thirdPartyId","thirdParty", "endDate" } );
	Set allWhole = wholeResults.keySet();
	formatSetDates(allWhole);
	request.setAttribute("lWholesales", allWhole);
	
	// Internet Advertisement
	request.setAttribute(ActionPlanReport.INTERNET_ADVERTISEMENT.getDescription(), ActionPlanReport.INTERNET_ADVERTISEMENT.getDescription());
	
	return mapping.findForward( "success" );
}

@SuppressWarnings("unchecked")
private void formatSetDates(Set keySet) {
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyy");
	
	Iterator setItr = keySet.iterator();
	
	while (setItr.hasNext()) {
		LinkedHashMap key = (LinkedHashMap) setItr.next();
		Object date = key.get("endDate");
		String auctionDate;
		if (date instanceof Timestamp) {
			Timestamp realDate = (Timestamp) date;
			auctionDate = sdf.format(realDate);
			
		} else {
			auctionDate = date.toString();
		}
		key.put("endDate", auctionDate);
	}
}

public void setPrintAdvertiserService( PrintAdvertiserService printAdvertiserService )
{
	this.printAdvertiserService = printAdvertiserService;
}



}
