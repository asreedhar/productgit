package biz.firstlook.report.action;

import biz.firstlook.main.enumerator.PlanningEventTypeEnum;

/**
 * An Enumeration of possible Action Plans and Reports with mappings to {@link biz.firstlook.main.enumerator.PlanningEventTypeEnum}. Some
 * ActionPlanReports will not have a PlanningEventTypeEnum due to the nature of the reports as being based on Inventory instead of an
 * InventoryPlanningEvent.
 * 
 */
public enum ActionPlanReport
{
	SPIFF( PlanningEventTypeEnum.SPIFF, "spiff" ), 
	LOT_PROMOTE( PlanningEventTypeEnum.PROMOTION, "lotPromotion" ), 
	ADVERTISING( PlanningEventTypeEnum.ADVERTISEMENT, "advertising" ), 
	MODEL_PROMOTION( null, "modelPromotion" ), 
	REPRICING( PlanningEventTypeEnum.REPRICE,"repricing" ), 
	SPECIAL_FINANCE( null,"specialFinance" ), 
	AUCTION( PlanningEventTypeEnum.AUCTION, "auction" ), 
	WHOLESALER( PlanningEventTypeEnum.WHOLESALER,"wholesaler" ), 
	WHOLESALER_BIDSHEET( PlanningEventTypeEnum.WHOLESALER, "wholesalerBidSheet" ), 
	IN_SERVICE( PlanningEventTypeEnum.SERVICE_DEPARTMENT,"vehiclesInService" ), 
	UNCHANGED( null ,"unchangedStatus" ), 
	CERTIFIED( null, "certified" ), 
	OTHER( PlanningEventTypeEnum.OTHER, "other" ), 
	ADVERTISING_INDIVIDUAL_REPORT( PlanningEventTypeEnum.ADVERTISEMENT, "individualAdvertising"), 
	WHOLESALER_INDIVIDUAL( PlanningEventTypeEnum.WHOLESALER, "individualWholesale"),
	AUCTION_INDIVIDUAL( PlanningEventTypeEnum.AUCTION, "individualAuction"),
	REPRICING_NO_NOTES( PlanningEventTypeEnum.REPRICE,"repricingNoNotes" ),
	INTERNET_ADVERTISEMENT( null, "internetAdvertisement");
	

	private PlanningEventTypeEnum planningEventTypeEnum;
	private String description;
	
	ActionPlanReport( PlanningEventTypeEnum planningEventTypeEnum, String description )
	{
		this.planningEventTypeEnum = planningEventTypeEnum;
		this.description = description;
	}

	public String getDescription()
	{
		return description;
	}

	public PlanningEventTypeEnum getPlanningEventTypeEnum()
	{
		return planningEventTypeEnum;
	}
	
	public static ActionPlanReport valueFromDescription( String description )
	{
		for (ActionPlanReport report : ActionPlanReport.values()) {
			if (report.description.equals(description))
				return report;
			
// THIS LINE WAS CAUSING WHOLESALEBID SHEETS DESCRIPTION TO MATCH JUST WHOLESALE DESCRIPTION
// CAUSING PROBLEMS, NOT SURE WHY THIS WAS EVER PUT IN
//			if (report.description.equals(description.substring(1, description.length())))
//				return report;
		}
		// this little bit is to handle the fact that advertising reports come
		// in based on
		// the id of the inventoryThirdParty they're associated with
		try{ Integer.parseInt( description ); }
		catch( NumberFormatException nfe )
		{
			return null;
		}
		return ActionPlanReport.ADVERTISING_INDIVIDUAL_REPORT;
	}

}