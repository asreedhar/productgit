package biz.firstlook.report.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.AuctionData;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.report.form.AuctionDataForm;
import biz.firstlook.service.AuctionDataService;
import biz.firstlook.service.IAuctionDataService;

public class AuctionDataValuesTileAction extends NextGenAction
{
private IAuctionDataService auctionDataService;

public AuctionDataValuesTileAction()
{
	super();
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	AuctionDataForm auctionDataForm = (AuctionDataForm)form;
	
	AuctionData auctionData = auctionDataService.retrieveAuctionData( auctionDataForm.getVic(),
												auctionDataForm.getAreaId(),
												auctionDataForm.getTimePeriodId(),
												auctionDataForm.getMileage(),
												auctionDataForm.getUsingVIC(),
												auctionDataForm.getModelYear() );

	request.setAttribute( "auctionData", auctionData );
	return mapping.findForward( "success" );
}

public void setAuctionDataService( AuctionDataService auctionDataService )
{
	this.auctionDataService = auctionDataService;
}

}
