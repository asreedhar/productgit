package biz.firstlook.report.action;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.report.reportBuilders.PerformanceBubbleReportBuilder;
import biz.firstlook.report.reportBuilders.PerformanceBubbleReportBuilder.PerformanceBubbleReportLevelEnum;
import biz.firstlook.report.reportFramework.IReportService;

public class PerformanceBubbleTileAction extends NextGenAction
{

private IReportService reportService;

/**
 * for now this action only displays an overall report.  there are comments in the justDoIt method for how
 * to make it more flexible to include vehicle and segment level (don't worry it should be pretty easy)
 */
@SuppressWarnings("unchecked")
@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	// to make this report display a different bubble (Segment level/model level) all you would need to do is look in the request
	// to see what values are present. so if there is a segmentId in the request and no modelId, you know the request is for 
	// a segment level, if there is no segment level and no model level, then you know its overall.  for now we're only doing
	// overall becuase there is/was no need for the other two.  work with the front end person to figure out how those values
	// will get passed back adn what they'll be called.
	PerformanceBubbleReportBuilder report = new PerformanceBubbleReportBuilder( PerformanceBubbleReportLevelEnum.OVERALL_STORE );

	report.setBusinessUnitId( (Integer) getNextGenSession(request).getBusinessUnitId() );
	report.setPeriodId( PeriodEnum.TWENTYSIX_WEEKS );

	List< Map > reportDisplayBeans = getReportService().getResults( report );

	if (reportDisplayBeans.isEmpty()) {
		request.setAttribute( "reportDisplayBean", Collections.emptyMap() );
	}
	else {
		request.setAttribute( "reportDisplayBean", reportDisplayBeans.get( 0 ) );
	}

	return mapping.findForward( "success" );
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}


}
