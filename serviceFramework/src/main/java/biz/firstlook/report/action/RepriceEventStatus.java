package biz.firstlook.report.action;

public enum RepriceEventStatus
{

InQueue(1), InProgress(2), Successful(3), Failure(4);

private Integer id;

private RepriceEventStatus( Integer id )
{
	this.id = id;
}

public Integer getId()
{
	return id;
}

public static RepriceEventStatus valueOf( final Integer id )
{
	for( RepriceEventStatus status : RepriceEventStatus.values() )
	{
		if( status.getId().equals( id ) )
			return status;
	}
	return null;
}

}
