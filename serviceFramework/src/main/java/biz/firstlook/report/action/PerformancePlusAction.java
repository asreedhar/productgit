package biz.firstlook.report.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightService;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.enumerator.PerformancePlusDimensionEnum;
import biz.firstlook.report.reportBeans.PerformancePlusTable;
import biz.firstlook.report.reportBuilders.PerformancePlusReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

/**
 * <p>Takes the HTTP request parameters</p>
 * <dl>
 * <dt>groupingDescriptionId</dt><dd>The vehicle grouping ID.</dd>
 * <dt>weeks</dt><dd>The number of weeks the report data-set is to encompass.</dd>
 * <dt>isForecast</dt><dd>Whether they want a 13 week "forecast".</dd>
 * <dt>applyMileageFilter</dt><dd>Whether the report data-set is restricted to vehicles with mileage below the dealer specified high-threshold.</dd>
 * </dl>
 * <p>The parameters <code>weeks</code> and <code>isForecast</code> are mutually exclusive; if <code>isForecast</code>
 * is <em>true</em> then weeks is ignored and a 13 week forecast is made.</p>
 * @author swenmouth
 */
public class PerformancePlusAction extends NextGenAction {

	private static final Logger logger = Logger.getLogger( PerformancePlusAction.class );
	
	private InsightService insightService;
	private IReportService reportService;
	
	public PerformancePlusAction() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DynaActionForm theForm = (DynaActionForm) form;
		// get the report parameters from the form
		Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
		Integer groupingDescriptionId = getIntegerFromDynaActionForm(theForm, "groupingDescriptionId", 0);
		// defaults to 26
		int defaultNumOfWeeks = 26; 
		Integer weeks = getIntegerFromDynaActionForm(theForm, "weeks", defaultNumOfWeeks);
		// if the weeks param was specified but with no value it will get 0. This is an inavlid # weeks.
		if (weeks.intValue() == 0) { 
			weeks = defaultNumOfWeeks;
		}
		Boolean isForecast = getBooleanFromDynaActionForm(theForm, "isForecast", false);
		Boolean applyMileageFilter = getBooleanFromDynaActionForm(theForm, "applyMileageFilter", false);
		Boolean applyUnitCostFilter = getBooleanFromDynaActionForm(theForm, "applyUnitCostFilter", false);
		Boolean isPopUpWindow = getBooleanFromDynaActionForm(theForm, "isPopup", false);
		// create performance plus "report builder" and execute
		PerformancePlusReportBuilder performancePlusReportBuilder = new PerformancePlusReportBuilder(null, businessUnitId, weeks, groupingDescriptionId, 2, applyMileageFilter, applyUnitCostFilter, isForecast);
		List<Map> performancePlusTableList = getReportService().getResults(performancePlusReportBuilder);
		Map<PerformancePlusDimensionEnum,PerformancePlusTable> tableMap = new HashMap<PerformancePlusDimensionEnum,PerformancePlusTable>();
		for (Map<PerformancePlusDimensionEnum, PerformancePlusTable> performancePlusTableMap : performancePlusTableList) {
			for (Map.Entry<PerformancePlusDimensionEnum, PerformancePlusTable> performancePlusTableEntry : performancePlusTableMap.entrySet()) {
				tableMap.put(performancePlusTableEntry.getKey(), performancePlusTableEntry.getValue());
			}
		}
		List<PerformancePlusTable> tables = new ArrayList<PerformancePlusTable>(5);
		String groupingDescription = "";
		if (!tableMap.isEmpty()) {
			tables.add(tableMap.get(PerformancePlusDimensionEnum.ALL));
			tables.add(tableMap.get(PerformancePlusDimensionEnum.YEAR));
			tables.add(tableMap.get(PerformancePlusDimensionEnum.TRIM));
			tables.add(tableMap.get(PerformancePlusDimensionEnum.COLOR));
			tables.add(tableMap.get(PerformancePlusDimensionEnum.PRICE));
			groupingDescription = tableMap.get(PerformancePlusDimensionEnum.ALL).getGroupingDescription();
		}
		// make service call for insights
		List<Insight> insights = getInsightService().getInsights(new InsightParameters(businessUnitId, groupingDescriptionId, 2, weeks));
		// add the results to the form
		request.setAttribute("groupingDescription", groupingDescription);
		request.setAttribute("weeks", weeks);
		request.setAttribute("tables", tables);
		request.setAttribute("insights", insights);
		request.setAttribute("showAnnualROI", getNextGenSession(request).showAnnualRoi());
		request.setAttribute("showBackButton", !isPopUpWindow);
		request.setAttribute("isPopUpWindow", isPopUpWindow );
		if (isPopUpWindow) {
			return mapping.findForward("success.pop");
		}
		else {
			return mapping.findForward("success.std");
		}
	}

	public IReportService getReportService() {
		return reportService;
	}

	public void setReportService(IReportService reportService) {
		this.reportService = reportService;
	}

	public InsightService getInsightService() {
		return insightService;
	}

	public void setInsightService(InsightService insightService) {
		this.insightService = insightService;
	}

	private int getIntegerFromDynaActionForm(DynaActionForm form, String property, int def) {
		int i = def;
		try {
			i = (Integer) form.get(property);
		}
		catch (Exception e) {
			logger.debug("HTTP request missing parameter '" + property + "'");
		}
		return i;
	}

	private boolean getBooleanFromDynaActionForm(DynaActionForm form, String property, boolean def) {
		boolean b = def;
		try {
			b = (Boolean) form.get(property);
		}
		catch (Exception e) {
			logger.debug("HTTP request missing parameter '" + property + "'");
		}
		return b;
	}
}
