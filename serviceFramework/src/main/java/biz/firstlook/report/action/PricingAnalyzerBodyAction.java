package biz.firstlook.report.action;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.main.enumerator.MileageRangeEnum;
import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.main.enumerator.SaleTypeEnum;
import biz.firstlook.report.reportBuilders.MileageRangeReportBuilder;
import biz.firstlook.report.reportBuilders.PeriodsReportBuilder;
import biz.firstlook.report.reportBuilders.TrimReportBuilder;
import biz.firstlook.report.reportBuilders.VehicleGroupingReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class PricingAnalyzerBodyAction extends NextGenAction
{

private IReportService reportService;
private PreferenceService preferenceService;

//hack for ping II integration
private DataSource dataSource;

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	int saleTypeId;
	int timePeriodId;
	int mileageRangeId;
	int trimId;

	try
	{
		saleTypeId = Integer.parseInt( request.getParameter( "saleTypeId" ) );
	}
	catch ( NumberFormatException nfe )
	{
		// default should be retail
		saleTypeId = 1;
	}

	try
	{
		timePeriodId = Integer.parseInt( request.getParameter( "timePeriodId" ) );
		mileageRangeId = Integer.parseInt( request.getParameter( "mileageRangeId" ) );
		trimId = Integer.parseInt( request.getParameter( "trimId" ) );
	}
	catch ( NumberFormatException nfe )
	{
		// default should be 26, all, all
		timePeriodId = PeriodEnum.TWENTYSIX_WEEKS.getPeriodId();
		mileageRangeId = MileageRangeEnum.ALL.getMileageRangeId();
		trimId = 0;
	}

	int groupingDescriptionId = Integer.parseInt( request.getParameter( "groupingDescriptionId" ) );
	MileageRangeReportBuilder mileageRangeReportBuilder = new MileageRangeReportBuilder();
	List< Map > mileageRangeDisplayBeans = getReportService().getResults( mileageRangeReportBuilder );

	PeriodsReportBuilder periodReportBuilder = new PeriodsReportBuilder();
	List< Map > periodsDisplayBeans = getReportService().getResults( periodReportBuilder );
	TrimReportBuilder trimReportBuilder = new TrimReportBuilder();
	trimReportBuilder.setBusinessUnitId( getNextGenSession( request ).getBusinessUnitId() );
	trimReportBuilder.setSaleTypeId( SaleTypeEnum.RETAIL.getSaleTypeId() );
	trimReportBuilder.setVehicleGroupingId( groupingDescriptionId );
	List< Map > trims = getReportService().getResults( trimReportBuilder );
	List< Map > trimDisplayBeans = determineUniqueTrims( trims );

	// Set the display description and if necessary, the data for ping
	addVehicleDescription( request, groupingDescriptionId );
	
	//logic to prevent inactive vehicles from the estock card launching PingII
	if(getNextGenSession( request ).getHasPingIIUpgrade() && (requestParameterHasValue(request, "showPingII" ) && request.getParameter("showPingII").equalsIgnoreCase("false"))){
		request.setAttribute("showPingII", Boolean.FALSE);
	}
	else if(getNextGenSession( request ).getHasPingIIUpgrade() && 
			(requestParameterHasValue(request, "appraisalId") || requestParameterHasValue(request, "inventoryId")
			|| 	(requestParameterHasValue(request, "vehicleEntityId") && requestParameterHasValue(request, "vehicleEntityTypeId"))	
			|| (requestParameterHasValue(request, "showPingII" ) && request.getParameter("showPingII").equalsIgnoreCase("true")) )){
		request.setAttribute("showPingII", Boolean.TRUE);
	}
	
	if(requestParameterHasValue(request,"inventoryId")){
		Integer inventoryId = Integer.parseInt(request.getParameter("inventoryId"));
		request.setAttribute("inventoryId", inventoryId);
	}
	
	if ( (request.getParameter( "inventoryItem" ) != null 
			&& request.getParameter( "inventoryItem" ).equalsIgnoreCase( "true" ))
		|| requestParameterHasValue(request, "stockNumber"))
	{
		setInventoryInfo( request );
	}
	else if(requestParameterHasValue(request, "appraisalId")){
		Integer appraisalId = Integer.parseInt(request.getParameter("appraisalId"));
		request.setAttribute("appraisalId", appraisalId);
	}
	else if((requestParameterHasValue(request, "vehicleEntityId") && requestParameterHasValue(request, "vehicleEntityTypeId"))){
		Integer vehicleEntityId = Integer.parseInt(request.getParameter("vehicleEntityId"));
		Integer vehicleEntityTypeId = Integer.parseInt(request.getParameter("vehicleEntityTypeId"));
		request.setAttribute("vehicleEntityId", vehicleEntityId);
		request.setAttribute("vehicleEntityTypeId", vehicleEntityTypeId);
	}
	
	//procedural code, must follow call to setInventoryInfo
	if ( getNextGenSession( request ).getHasPingUpgrade() )	
	{
		request.setAttribute( "showPing", Boolean.TRUE );
		request.setAttribute( "year", request.getParameter( "year" ) );
		//When coming from TA/TM
		request.setAttribute( "mileage", request.getAttribute("mileage") );
		request.setAttribute( "Preferences", getPreferenceService().getPreferences( getNextGenSession( request ).getBusinessUnitId(),
																					PreferenceService.ReportsEnum.PING ) );
	}
	
	request.setAttribute( "timePeriodId", timePeriodId );
	request.setAttribute( "mileageRangeId", mileageRangeId );
	request.setAttribute( "trimId", trimId );
	request.setAttribute( "saleTypeId", saleTypeId );
	request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
	request.setAttribute( "mileageRanges", mileageRangeDisplayBeans );
	request.setAttribute( "timePeriods", periodsDisplayBeans );
	request.setAttribute( "trims", trimDisplayBeans );

	return mapping.findForward( "success" );
}

private boolean requestParameterHasValue(HttpServletRequest request, String parameter){
	return request.getParameter(parameter) != null && !request.getParameter(parameter).equals("");
}

private void addVehicleDescription( HttpServletRequest request, int groupingDescriptionId )
{
	VehicleGroupingReportBuilder vehicleHierarchyReportBuilder = new VehicleGroupingReportBuilder();
	vehicleHierarchyReportBuilder.setVehicleGroupingId( groupingDescriptionId );
	List< Map > descriptionAndMakeModelGrouping = getReportService().getResults( vehicleHierarchyReportBuilder );
	if ( !descriptionAndMakeModelGrouping.isEmpty() )
	{ // Only one description and mmgId for a VehicleGrouping
		Map firstElement = descriptionAndMakeModelGrouping.get( 0 );
		request.setAttribute( "description", (String)firstElement.get( "Description" ) );
		//if there is a make model grouping sent from trade manager or trade analyzer, use that
		if(getNextGenSession( request ).getHasPingUpgrade() && request.getParameter( "makeModelGroupingId" ) != null){
			request.setAttribute("makeModelGroupingId",Integer.parseInt( request.getParameter( "makeModelGroupingId" ) ));
		}
		else {
			request.setAttribute( "makeModelGroupingId", firstElement.get( "MakeModelGroupingID" ).toString() );
		}
	}
}

private List< Map > determineUniqueTrims( List< Map > trims )
{
	List< Map > uniqueTrims = new ArrayList< Map >();
	List< Integer > trimIds = new ArrayList< Integer >();
	Iterator trimIter = trims.iterator();
	Map trimMap;
	Integer trimId;
	while ( trimIter.hasNext() )
	{
		trimMap = (Map)trimIter.next();
		trimId = (Integer)trimMap.get( "TrimID" );
		if ( !trimIds.contains( trimId ) )
		{
			trimIds.add( trimId );
			uniqueTrims.add( trimMap );
		}
	}
	return uniqueTrims;
}

@SuppressWarnings("unchecked")
private void setInventoryInfo( HttpServletRequest request )
{
	try
	{
		Double unitCost = null;
		Double listPrice = null;
		Double mileage = null;
		String year = request.getParameter( "year" );
		
		if(requestParameterHasValue(request, "stockNumber")) {
			StringBuilder sql = new StringBuilder("SELECT UnitCost, ListPrice, MileageReceived AS Mileage");
			sql.append(" FROM dbo.Inventory");
			sql.append(" WHERE BusinessUnitID = ? ");
			sql.append(" AND StockNumber = ? ");
			sql.append(" AND InventoryActive = 1 ");
			sql.append(" AND InventoryType = 2 ");
			
			int dealerId = getNextGenSession(request).getBusinessUnitId();
			String stockNumber = request.getParameter("stockNumber");
			
			JdbcTemplate db = new JdbcTemplate(dataSource);
			List<Double[]> records = (List<Double[]>)db.query(sql.toString(), 
					new Object[] {dealerId, stockNumber}, 
					new RowMapper()
			{
				public Object mapRow(ResultSet rs, int index)
						throws SQLException {
					Double[] record = new Double[3];
					record[0] = rs.getDouble("UnitCost");
					record[1] = rs.getDouble("ListPrice");
					record[2] = rs.getDouble("Mileage");
					return record;
				}
			});
			
			if(records != null && !records.isEmpty() && records.size() == 1) {
				Double[] record = records.get(0);
				unitCost = record[0];
				listPrice = record[1];
				mileage = record[2];
			}
			
		} else {
			unitCost = Double.parseDouble( request.getParameter( "unitCost" ) );
			listPrice = Double.parseDouble( request.getParameter( "listPrice" ) );
			mileage = Double.parseDouble( request.getParameter( "mileage" ) );
		}
		
		request.setAttribute( "unitCost", unitCost.intValue() );
		request.setAttribute( "listPrice", listPrice );
		request.setAttribute( "mileage", mileage );
		request.setAttribute( "year", year );
		request.setAttribute( "inventoryItem", Boolean.TRUE );
	}
	catch ( NumberFormatException nfe )
	{
		// Then dont show the inventory info
		request.setAttribute( "inventoryItem", Boolean.FALSE );
	}
}

public IReportService getReportService()
{
	return reportService;
}

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
}

}
