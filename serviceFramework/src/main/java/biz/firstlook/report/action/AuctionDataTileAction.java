package biz.firstlook.report.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.entity.Area;
import biz.firstlook.entity.Period;
import biz.firstlook.entity.VICBodyStyle;
import biz.firstlook.entity.Vehicle;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.service.IAuctionDataService;

public class AuctionDataTileAction extends NextGenAction
{							
private IAuctionDataService auctionDataService;
private IVehicleCatalogService vehicleCatalogService;
private PreferenceService preferenceService;

public AuctionDataTileAction()
{
	super();
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	Integer year = Integer.valueOf( request.getParameter( "year" ) );
	Integer mileage = Integer.valueOf( request.getParameter( "mileage" ) );
	Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
	String vin = request.getParameter( "vin" );
	
	Boolean usingVICs = Boolean.TRUE;
	List< VICBodyStyle > vicBodyStyles = auctionDataService.retrieveVICBodyStyles( vin );
	
	
	// if no VIC was found - pass down makemodelGroupingID
	if ( vicBodyStyles.isEmpty() )
	{
		usingVICs = Boolean.FALSE;
		VICBodyStyle bodyStyle = null;
		VehicleCatalogEntry vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( vin );
		if (vcEntry != null)
		{	
			Vehicle vehicle = new Vehicle( vcEntry );
			bodyStyle = new VICBodyStyle( vehicle.getMakeModelGrouping().getId(), vehicle.getMakeModelGrouping().getGroupingDescription().getGroupingDescription() );
			vicBodyStyles.add( bodyStyle );
		}
	}
		
	List< Area > areas = auctionDataService.retrieveAllAreas();
	List< Period > timePeriods = auctionDataService.retrieveAllTimePeriods();
	
	Integer auctionAreaIdPreference = Integer.valueOf(1); //Look at DealerPreference for defaults.
	Integer auctionTimePeriodId = Integer.valueOf(12); //4weeks
	Map<String, Object> auctionPreferences = getPreferenceService().getPreferences( businessUnitId, PreferenceService.ReportsEnum.AUCTION_DATA );
	if ( auctionPreferences != null ) {
	    auctionAreaIdPreference = (Integer)auctionPreferences.get( "AuctionAreaId" );
	    auctionTimePeriodId = (Integer)auctionPreferences.get( "AuctionTimePeriodId" );
	}

	request.setAttribute( "usingVIC", usingVICs );
	request.setAttribute( "auctionAreaIdPreference", auctionAreaIdPreference );
	request.setAttribute( "defaultTimePeriodId", auctionTimePeriodId );
	request.setAttribute( "areas", areas );
	request.setAttribute( "timePeriods", timePeriods );
	request.setAttribute( "vicBodyStyles", vicBodyStyles );
	request.setAttribute( "modelYear", year );
	request.setAttribute( "mileage", mileage );
	
	if ( vicBodyStyles.size() == 1 )
	{
		request.setAttribute( "noSeriesSelected", "false" );
	}
	else
	{
		// more than one series
		request.setAttribute( "noSeriesSelected", "true" );	
	}

	return mapping.findForward( "success" );
}

public void setAuctionDataService( IAuctionDataService auctionDataService )
{
	this.auctionDataService = auctionDataService;
}

public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}
