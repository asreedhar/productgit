package biz.firstlook.report.service;

import java.util.List;

import biz.firstlook.entity.EquityAnalyzerSearch;
import biz.firstlook.main.enumerator.ThirdPartyCategoryEnum;
import biz.firstlook.report.dao.IEquityAnalyzerSearchDAO;

public class EquityAnalyzerSearchService
{
private IEquityAnalyzerSearchDAO equityAnalyzerSearchDAO;

/**
 * Saves an equity report search
 * @param businessUnitId
 * @param guidebookName
 * @param thirdPartyCategoryId
 * @param percentMultiplier
 */
public void saveSearch( Integer businessUnitId, String guidebookName, Integer thirdPartyCategoryId, Integer percentMultiplier )
{			
	EquityAnalyzerSearch searchToSave = new EquityAnalyzerSearch();
    searchToSave.setBusinessUnitId( businessUnitId );
    searchToSave.setThirdPartyCategoryId( thirdPartyCategoryId );
    searchToSave.setPercentMultiplier( percentMultiplier );
    
    String categoryDescription = ThirdPartyCategoryEnum.getEnumById( thirdPartyCategoryId ).getDescription();
    searchToSave.setName( guidebookName + "_" + categoryDescription + "_" + percentMultiplier + "%" );
    
    if ( getEquityAnalyzerSearchDAO().loadByExample( searchToSave ).size() == 0 ) 
    {
        getEquityAnalyzerSearchDAO().save( searchToSave );
    }
}

/**
 * loads all equity report searches for the given business unit id
 * @param businessUnitId
 * @return List<EquityAnalyzerSearch>
 */
public List<EquityAnalyzerSearch> findByBusinessUnitId( Integer businessUnitId )
{
    return getEquityAnalyzerSearchDAO().loadByBusinessUnitId( businessUnitId );	
}

/**
 * returns the instance of EquityAnalyzerSearch with the given id
 * @param equityAnalyzerSearchId
 * @return EquityAnalyzerSearch
 */
public EquityAnalyzerSearch findById( Integer equityAnalyzerSearchId )
{
    return getEquityAnalyzerSearchDAO().loadById( equityAnalyzerSearchId );
}

public IEquityAnalyzerSearchDAO getEquityAnalyzerSearchDAO()
{
	return equityAnalyzerSearchDAO;
}

public void setEquityAnalyzerSearchDAO( IEquityAnalyzerSearchDAO equityAnalyzerSearchDAO )
{
	this.equityAnalyzerSearchDAO = equityAnalyzerSearchDAO;
}

}
