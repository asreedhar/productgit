package biz.firstlook.report.compositeReportBuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.report.reportBuilders.InventoryDetailReportBuilder;
import biz.firstlook.report.reportBuilders.PerformanceBubbleReportBuilder;
import biz.firstlook.report.reportBuilders.PerformanceBubbleReportBuilder.PerformanceBubbleReportLevelEnum;

public class InventoryOverviewBodyCompositeReportBuilder extends CompositeReportBuilder {

public enum InventoryOverviewBodyReportEnum{ ALPHABETIC, SEGMENT  }

public InventoryOverviewBodyCompositeReportBuilder( InventoryOverviewBodyReportEnum reportType,
                                                    Integer businessUnitId, PeriodEnum period, Integer vehicleSegmentId, boolean showAnnualRoi ) {
	super();
	
	InventoryDetailReportBuilder inventoryDetailReportBuilder;
	PerformanceBubbleReportBuilder performanceBubbleReportBuilder;

	inventoryDetailReportBuilder = new InventoryDetailReportBuilder( businessUnitId, showAnnualRoi );
	performanceBubbleReportBuilder = new PerformanceBubbleReportBuilder( PerformanceBubbleReportLevelEnum.VEHICLE_GROUPING );
	performanceBubbleReportBuilder.setBusinessUnitId( businessUnitId );
	performanceBubbleReportBuilder.setPeriodId( period );
	
	// leave out this where clause for alphabetic version becuase you want all the results
	if ( reportType.equals( InventoryOverviewBodyReportEnum.SEGMENT ) ) {
		performanceBubbleReportBuilder.setVehicleSegmentId( vehicleSegmentId );
	}
	
	addReport( inventoryDetailReportBuilder );
	addReport( performanceBubbleReportBuilder );
}

@Override
public String getCompositeReportName() {
	return "InventoryOverviewBodyCompositeReport";
}


@SuppressWarnings("unchecked")
@Override
public List< Map > mergeReports( Map< String, List< Map >> subReports ) {
	List< Map > inventoryDetailReportResults = subReports.get("InventoryDetailReport");
	List< Map > perfomanceBubbleReportRestults = subReports.get("PerformanceBubbleReport"); 
	
	if ( subReports.size() == 0 || inventoryDetailReportResults == null 
			|| perfomanceBubbleReportRestults == null )
	{
		List<Map> emptyList = new ArrayList<Map>();
		emptyList.add( new HashMap() );
		return emptyList;
	}
	
	List< Map > mergedList = new ArrayList< Map >();

	Iterator inventoryDetailResultsIter = inventoryDetailReportResults.iterator();
	Map<Object, List<Map>> IOModelLevelResultsByMMG = new HashMap<Object, List<Map>>();
	while ( inventoryDetailResultsIter.hasNext() ) {
		Map row = (Map)inventoryDetailResultsIter.next();
		Object id = row.get("VehicleGroupingID");
		
		List<Map> result = IOModelLevelResultsByMMG.get(id);
		if(result == null) {
			result = new ArrayList<Map>();
		}
		result.add(row);
		IOModelLevelResultsByMMG.put( id, result );
	}

	Iterator bubbleIter = perfomanceBubbleReportRestults.iterator();
	while ( bubbleIter.hasNext() )
	{
		Map performanceBubble = (Map)bubbleIter.next();
		Map addMe = new HashMap();
		if ( IOModelLevelResultsByMMG.get( performanceBubble.get( "VehicleGroupingID" ) ) != null )
		{
			addMe.put( "performanceBubble", performanceBubble );
			addMe.put( "InventoryLineItems", (List< Map >)IOModelLevelResultsByMMG.get( performanceBubble.get( "VehicleGroupingID" ) ) );
			addMe.put( "AvgAgeInDays", getAvgAgeInDays((List< Map >)IOModelLevelResultsByMMG.get( performanceBubble.get( "VehicleGroupingID" ) ) ) );
			mergedList.add( addMe );
		}
	}

	return mergedList;
}
 
// protected for testing purposes
protected static int getAvgAgeInDays( List< Map > inventoryDetailReportResults ) {
	if( inventoryDetailReportResults == null || inventoryDetailReportResults.isEmpty() ) {
		return 0;
	}
	
	int total = 0;
	
	try	{
		Iterator iter = inventoryDetailReportResults.iterator();
		while ( iter.hasNext() )
		{
			Map element = (Map)iter.next();
			total += (Integer)element.get("AgeInDays");
		}
	} catch (ClassCastException e )	{
		total = 0; // if there is no age for a vehicle, return avg age of 0;
	}
	return total/(inventoryDetailReportResults.size());
}

}
