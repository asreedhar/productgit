package biz.firstlook.report.compositeReportBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import biz.firstlook.report.reportBuilders.ReportBuilder;

public abstract class CompositeReportBuilder
{

private List<ReportBuilder> reports;
private List<CompositeReportBuilder> compositeReports;

public CompositeReportBuilder()
{
	reports = new ArrayList< ReportBuilder >();
	compositeReports = new ArrayList< CompositeReportBuilder >();
}

public abstract String getCompositeReportName();

public abstract List<Map> mergeReports( Map< String, List< Map >> subReports );

public List< CompositeReportBuilder > getCompositeReports()
{
	return compositeReports;
}
public List< ReportBuilder > getReports()
{
	return reports;
}

public void addReport( ReportBuilder report )
{
	reports.add( report );
}

public void addCompositeReport( CompositeReportBuilder compostieReport )
{
	compositeReports.add( compostieReport );
}


}
