package biz.firstlook.report.compositeReportBuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.main.enumerator.PeriodEnum;
import biz.firstlook.report.reportBuilders.PerformanceBubbleReportBuilder;
import biz.firstlook.report.reportBuilders.SegmentDetailReportBuilder;
import biz.firstlook.report.reportBuilders.PerformanceBubbleReportBuilder.PerformanceBubbleReportLevelEnum;

/**
 * the mergeReports method of this class returns a list containing 3 reports.
 * the entries with keys segmentDetailReport and PerformanceBubbleReport have values
 * of type Map becuae both of these reports will only return 1 row, so a list is unneccessary.
 *  the key contributionTabsReport has a value of type list because it returns multiple rows.
 */
public class IOSegmentDetailCompositeReportBuilder extends CompositeReportBuilder
{

public IOSegmentDetailCompositeReportBuilder( Integer businessUnitId, Integer segmentId )
{
	super();
	
	SegmentDetailReportBuilder segmentDetailReportBuilder = new SegmentDetailReportBuilder();
	segmentDetailReportBuilder.setBusinessUnitId( businessUnitId );
	segmentDetailReportBuilder.setVehicleSegmentId( segmentId );

	PerformanceBubbleReportBuilder performanceBubbleReportBuilder = 
						new PerformanceBubbleReportBuilder( PerformanceBubbleReportLevelEnum.VEHICLE_SEGMENT );
	performanceBubbleReportBuilder.setBusinessUnitId( businessUnitId );
	performanceBubbleReportBuilder.setPeriodId( PeriodEnum.TWENTYSIX_WEEKS );
	performanceBubbleReportBuilder.setVehicleSegmentId( segmentId );
	
	addReport( segmentDetailReportBuilder );
	addReport( performanceBubbleReportBuilder );
}

@Override
public String getCompositeReportName()
{
	return "IOSegmentTabsCompositeREportBuilder";
}

@SuppressWarnings("unchecked")
@Override
public List< Map > mergeReports( Map< String, List< Map >> subReports )
{
	List< Map > segmentDetailReport = subReports.get("SegmentDetailReport");
	List< Map > perfomanceBubbleReportRestults = subReports.get("PerformanceBubbleReport"); 
	
	if ( subReports.size() == 0 
			|| segmentDetailReport == null || segmentDetailReport.isEmpty()  
			|| perfomanceBubbleReportRestults == null || perfomanceBubbleReportRestults.isEmpty())
	{
		List<Map> emptyList = new ArrayList<Map>();
		emptyList.add( new HashMap() );
		return emptyList;
	}
	
	Map threeReports = new HashMap();
	threeReports.put( "SegmentDetailReport", segmentDetailReport.get( 0 ) );
	threeReports.put( "PerformanceBubbleReport", perfomanceBubbleReportRestults.get( 0 ) );
	
	List< Map > mergedList = new ArrayList< Map >();
	mergedList.add( threeReports );
	
	return mergedList;
}

}
