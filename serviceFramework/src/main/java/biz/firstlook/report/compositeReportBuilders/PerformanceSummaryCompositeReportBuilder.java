package biz.firstlook.report.compositeReportBuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.report.reportBuilders.PerformanceSummaryReportBuilder;
import biz.firstlook.report.reportBuilders.UnitsInStockReportBuilder;

public class PerformanceSummaryCompositeReportBuilder extends CompositeReportBuilder
{
static final String COMPOSITE_REPORT_NAME = "PerformanceSummaryCompositeReport";

public PerformanceSummaryCompositeReportBuilder( Integer businessUnitId, Integer inventoryTypeId, Integer weeks, String make, String model, 
												String trim, Integer includeDealerGroup )
{
	addReport( new PerformanceSummaryReportBuilder( businessUnitId, weeks, make, model, includeDealerGroup, inventoryTypeId, trim ) );
	addReport( new UnitsInStockReportBuilder( businessUnitId, trim, includeDealerGroup, inventoryTypeId, make, model ) );
}

@Override
public String getCompositeReportName()
{
	return COMPOSITE_REPORT_NAME;
}

@Override
public List< Map > mergeReports( Map< String, List< Map >> subReports )
{
	Map< String, Object > report = new HashMap< String, Object >();

	for ( String reportName : subReports.keySet() )
	{
		if ( subReports.get( reportName ).size() > 0 )
		{
			if ( reportName.equals( PerformanceSummaryReportBuilder.REPORT_NAME ) || reportName.equals( UnitsInStockReportBuilder.REPORT_NAME ) )
			{
				report.putAll( subReports.get( reportName ).get( 0 ) );
			}
		}
	}

	List< Map > results = new ArrayList< Map >();
	results.add( report );
	return results;
}

}
