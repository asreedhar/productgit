package biz.firstlook.report.reportBeans;

import java.io.Serializable;
import java.util.List;

import biz.firstlook.main.enumerator.PerformancePlusDimensionEnum;

/**
 * <p>The PerformancePlusTable represents a result-set enumeration and data-set for a <em>Performance Plus</em> report.</p>
 * <p>This is a <em>read-only</em> bean whose purpose is to give a simple and manageable structure to the display template.</p> 
 * @author swenmouth
 */
@SuppressWarnings("serial")
public class PerformancePlusTable implements Serializable {
	private PerformancePlusDimensionEnum dimension;
	private boolean isMarketShareAnalysisPresent;
	private List<PerformancePlusTableRow> rows;
	private String groupingDescription;
	/**
	 * Initializes a newly created PerformancePlusTable from the constructor arguments. 
	 * @param dimension the aggregation dimension the table reports upon 
	 * @param isMarketShareAnalysisPresent whether the dimension has market share specified for each row
	 * @param rows the data-set representing the body of the table
	 * @param groupingDescription the name of the vehicle grouping
	 */
	public PerformancePlusTable(PerformancePlusDimensionEnum dimension, boolean isMarketShareAnalysisPresent, List<PerformancePlusTableRow> rows, String groupingDescription) {
		super();
		this.dimension = dimension;
		this.isMarketShareAnalysisPresent = isMarketShareAnalysisPresent;
		this.rows = rows;
		this.groupingDescription = groupingDescription;
	}
	public PerformancePlusDimensionEnum getDimension() {
		return dimension;
	}
	public boolean isMarketShareAnalysisPresent() {
		return isMarketShareAnalysisPresent;
	}
	public List<PerformancePlusTableRow> getRows() {
		return rows;
	}
	public String getGroupingDescription() {
		return groupingDescription;
	}
	/**
	 * Initializes and returns a new PerformancePlusTableRow which contains
	 * the summary data for the table. 
	 * @return summary PerformancePlusTableRow 
	 */
	public PerformancePlusTableRow getSummary() {
		// zero tally variables
		int unitsSold = 0;
		int averageGrossProfit = 0;
		int totalGrossMargin = 0;
		int averageDays = 0;
		int averageMileage = 0;
		int totalDays = 0;
		int unitsInStock = 0;
		int noSales = 0;
		int totalInventoryDollars = 0;
		int totalRevenue = 0;
		int averageBackEnd = 0;
		int marketShareDeals = 0;
		double annualROI = 0.0d;
		double totalFrontEndGross = 0.0d;
		double totalBackEndGross = 0.0d;
		double percentageTotalGrossMargin = 0.0d;
		double percentageTotalRevenue = 0.0d;
		double percentageTotalInventoryDollars = 0.0d;
		double percentageMarketShare = 0.0d;
		// tally
		for (PerformancePlusTableRow row : rows) {
			unitsSold += row.getUnitsSold();
			averageGrossProfit += row.getAverageGrossProfit();
			totalGrossMargin += row.getTotalGrossMargin();
			averageDays += row.getAverageDays();
			averageMileage += row.getAverageMileage();
			totalDays += row.getTotalDays();
			unitsInStock += row.getUnitsInStock();
			noSales += row.getNoSales();
			totalInventoryDollars += row.getTotalInventoryDollars();
			totalRevenue += row.getTotalRevenue();
			averageBackEnd += row.getAverageBackEnd();
			marketShareDeals += row.getMarketShareDeals();
			annualROI += row.getAnnualROI();
			totalFrontEndGross += row.getTotalFrontEndGross();
			totalBackEndGross += row.getTotalBackEndGross();
			percentageTotalGrossMargin += row.getPercentageTotalGrossMargin();
			percentageTotalRevenue += row.getPercentageTotalRevenue();
			percentageTotalInventoryDollars += row.getPercentageTotalInventoryDollars();
			percentageMarketShare += row.getPercentageMarketShare();
		}
		// create new row
		return new PerformancePlusTableRow(
				"Summary",
				0,
				1000000,
				unitsSold,
				divide(totalFrontEndGross, unitsSold),
				new Double(totalFrontEndGross).intValue(),
				divide(totalDays, unitsSold),
				averageMileage,
				totalDays,
				unitsInStock,
				noSales,
				totalInventoryDollars,
				totalRevenue,
				averageBackEnd,
				marketShareDeals,
				annualROI,
				totalFrontEndGross,
				totalBackEndGross,
				percentageTotalGrossMargin,
				percentageTotalRevenue,
				percentageTotalInventoryDollars,
				percentageMarketShare
		);
	}
	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append("dimension=").append(dimension).append(", ");
		sb.append("isMarketShareAnalysisPresent=").append(isMarketShareAnalysisPresent).append(", ");
		sb.append("rows=").append(rows);
		return sb.append("}").toString();
	}
	private int divide(int numerator, int denominator) {
		if (denominator > 0) {
			return (int) Math.round(((double)numerator)/((double)denominator));
		}
		return 0;
	}
	private int divide(double numerator, int denominator) {
		if (denominator > 0) {
			return (int) Math.round(numerator/((double)denominator));
		}
		return 0;
	}
}
