package biz.firstlook.report.reportBeans;

import java.io.Serializable;

/**
 * <p>A PerformancePlusTableRow represents a slice of the Performance Plus aggregated table for a given dimension.</p>
 * <p>The data is represented by the abstract class <code>Number</code> so the class can hold both standard and
 * percentage representations.</p>
 * <p>This is a <em>read-only</bean> whose purpose is to make the display template clean and manageable.</p>
 * @author swenmouth
 */
@SuppressWarnings("serial")
public class PerformancePlusTableRow implements Serializable {
	
	private String groupingColumn;
	private int lowerBound;
	private int upperBound;
	private int unitsSold;
	private int averageGrossProfit;
	private int totalGrossMargin;
	private int averageDays;
	private int averageMileage;
	private int totalDays;
	private int unitsInStock;
	private int noSales;
	private int totalInventoryDollars;
	private int totalRevenue;
	private int averageBackEnd;
	private int marketShareDeals;
	private double annualROI;
	private double totalFrontEndGross;
	private double totalBackEndGross;
	private double percentageTotalGrossMargin;
	private double percentageTotalRevenue;
	private double percentageTotalInventoryDollars;
	private double percentageMarketShare;
	
	public PerformancePlusTableRow(String groupingColumn, int lowerBound, int upperBound, int unitsSold, int averageGrossProfit, int totalGrossMargin, int averageDays, int averageMileage, int totalDays, int unitsInStock, int noSales, int totalInventoryDollars, int totalRevenue, int averageBackEnd, int marketShareDeals, double annualROI, double totalFrontEndGross, double totalBackEndGross, double percentageTotalGrossMargin, double percentageTotalRevenue, double percentageTotalInventoryDollars, double percentageMarketShare) {
		super();
		this.groupingColumn = groupingColumn;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.unitsSold = unitsSold;
		this.averageGrossProfit = averageGrossProfit;
		this.totalGrossMargin = totalGrossMargin;
		this.averageDays = averageDays;
		this.averageMileage = averageMileage;
		this.totalDays = totalDays;
		this.unitsInStock = unitsInStock;
		this.noSales = noSales;
		this.totalInventoryDollars = totalInventoryDollars;
		this.totalRevenue = totalRevenue;
		this.averageBackEnd = averageBackEnd;
		this.marketShareDeals = marketShareDeals;
		this.annualROI = annualROI;
		this.totalFrontEndGross = totalFrontEndGross;
		this.totalBackEndGross = totalBackEndGross;
		this.percentageTotalGrossMargin = percentageTotalGrossMargin;
		this.percentageTotalRevenue = percentageTotalRevenue;
		this.percentageTotalInventoryDollars = percentageTotalInventoryDollars;
		this.percentageMarketShare = percentageMarketShare;
	}

	public double getAnnualROI() {
		return annualROI;
	}

	public int getAverageBackEnd() {
		return averageBackEnd;
	}

	public int getAverageDays() {
		return averageDays;
	}

	public int getAverageGrossProfit() {
		return averageGrossProfit;
	}

	public int getAverageMileage() {
		return averageMileage;
	}

	public String getGroupingColumn() {
		return groupingColumn;
	}

	public int getLowerBound() {
		return lowerBound;
	}

	public int getMarketShareDeals() {
		return marketShareDeals;
	}

	public int getNoSales() {
		return noSales;
	}

	public double getPercentageMarketShare() {
		return percentageMarketShare;
	}

	public double getPercentageTotalGrossMargin() {
		return percentageTotalGrossMargin;
	}

	public double getPercentageTotalInventoryDollars() {
		return percentageTotalInventoryDollars;
	}

	public double getPercentageTotalRevenue() {
		return percentageTotalRevenue;
	}

	public double getTotalBackEndGross() {
		return totalBackEndGross;
	}

	public int getTotalDays() {
		return totalDays;
	}

	public double getTotalFrontEndGross() {
		return totalFrontEndGross;
	}

	public int getTotalGrossMargin() {
		return totalGrossMargin;
	}

	public int getTotalInventoryDollars() {
		return totalInventoryDollars;
	}

	public int getTotalRevenue() {
		return totalRevenue;
	}

	public int getUnitsInStock() {
		return unitsInStock;
	}

	public int getUnitsSold() {
		return unitsSold;
	}

	public int getUpperBound() {
		return upperBound;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer("{");
		sb.append("groupingColumn=").append(groupingColumn).append(",");
		sb.append("lowerBound=").append(lowerBound).append(",");
		sb.append("upperBound=").append(upperBound).append(",");
		sb.append("unitsSold=").append(unitsSold).append(",");
		sb.append("averageGrossProfit=").append(averageGrossProfit).append(",");
		sb.append("totalGrossMargin=").append(totalGrossMargin).append(",");
		sb.append("averageDays=").append(averageDays).append(",");
		sb.append("averageMileage=").append(averageMileage).append(",");
		sb.append("totalDays=").append(totalDays).append(",");
		sb.append("unitsInStock=").append(unitsInStock).append(",");
		sb.append("noSales=").append(noSales).append(",");
		sb.append("totalInventoryDollars=").append(totalInventoryDollars).append(",");
		sb.append("totalRevenue=").append(totalRevenue).append(",");
		sb.append("averageBackEnd=").append(averageBackEnd).append(",");
		sb.append("marketShareDeals=").append(marketShareDeals).append(",");
		sb.append("annualROI=").append(annualROI).append(",");
		sb.append("totalFrontEndGross=").append(totalFrontEndGross).append(",");
		sb.append("totalBackEndGross=").append(totalBackEndGross).append(",");
		sb.append("percentageTotalGrossMargin=").append(percentageTotalGrossMargin).append(",");
		sb.append("percentageTotalRevenue=").append(percentageTotalRevenue).append(",");
		sb.append("percentageTotalInventoryDollars=").append(percentageTotalInventoryDollars).append(",");
		sb.append("percentageMarketShare=").append(percentageMarketShare).append("}");
		return sb.append("}").toString();
	}
}

