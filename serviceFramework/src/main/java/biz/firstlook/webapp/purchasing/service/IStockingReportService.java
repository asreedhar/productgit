package biz.firstlook.webapp.purchasing.service;

import java.util.List;
import java.util.Map;

public interface IStockingReportService {

    public Integer retrieveDaySupply(Integer businessUnitId);
    public List<String> retrieveGroupingIds(List<Map> reportResults);
    public List<Map> filterOthers(List<Map> segments, boolean modifyStock);
    public List addOthers(List<Map> segments, List<Map> others);
    public List createResults(List<Map> segments, List<Map> modelGroups, List<Map> years, List<Map> others, Boolean modifyStock);
    public boolean hasBuyingPlan(Integer businessUnitId);
}
