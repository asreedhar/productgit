package biz.firstlook.webapp.purchasing.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class OptimalInventorySearchSummaryLoadMakesAction extends NextGenAction {

	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		theForm.setMakes(VehicleServiceFactory.getFactory().getService().getMakes());
		response.addHeader("X-JSON", JSONAdapter(theForm.getMakes()));
		return mapping.findForward("success");
	}

	protected String JSONAdapter(List<Make> makes) {
		JSONObject json = new JSONObject();
		JSONArray jsonMakes = new JSONArray();
		for (Make m : makes) {
			try {
				JSONObject jsonMake = new JSONObject();
				jsonMake.put("name", m.getName());
				jsonMakes.put(jsonMake);
			}
			catch (JSONException npe) {
				// is a null pointer exception if name is null
			}
		}
		return json.toString();
	}
	
}
