package biz.firstlook.webapp.purchasing.util;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.module.market.search.MarketVehicle;

public class MarketVehicleStockNumberNodeVisitor extends
		AbstractMarketVehicleNodeVisitor {

	private final String stockNumber;
	
	public MarketVehicleStockNumberNodeVisitor(String stockNumber) {
		if(StringUtils.isBlank(stockNumber)) {
			throw new IllegalArgumentException("Searching for a null or whitespace stock number is not permitted!");
		}
		this.stockNumber = stockNumber;
	}
	
	@Override
	protected boolean isItemFound(MarketVehicle marketVehicle) {
		if(marketVehicle == null) {
			return false;
		}
		return stockNumber.equalsIgnoreCase(marketVehicle.getStockNumber());
	}

}
