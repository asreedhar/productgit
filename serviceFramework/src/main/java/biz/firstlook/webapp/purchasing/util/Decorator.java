package biz.firstlook.webapp.purchasing.util;

public interface Decorator {
	public Object decorate(Object o);
}
