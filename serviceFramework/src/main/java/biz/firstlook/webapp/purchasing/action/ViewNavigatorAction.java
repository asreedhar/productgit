package biz.firstlook.webapp.purchasing.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.main.enumerator.LogClickEventType;
import biz.firstlook.module.audit.dao.LogClickEventDAO;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.impl.SimpleMarketVehicleImpl;
import biz.firstlook.module.market.search.presentation.DecoratedModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupByNode;
import biz.firstlook.webapp.purchasing.util.MarketDecorator.DecoratedMarket;

public class ViewNavigatorAction extends OptimalInventorySearchSummaryAction {
	
	private LogClickEventDAO logClickEventDAO;

	public ViewNavigatorAction() {
		super();
	}

	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		GroupByNode currentYearNode = null;
		GroupByNode node = theForm.getGroupBySearchResults().get(theForm.getNodeId());
		GroupByNode currentModelNode = null;
		GroupByNode currentMarketNode = null;
		
		// Need the stocking info on the navigator page.
		String selectedNodeText = request.getParameter("selectedNode");
		if (!Functions.isNullOrEmpty(selectedNodeText)) {
			theForm.setGroupByNodeIncrement(theForm.getGroupBySearchResults().get(
									Integer.parseInt(selectedNodeText)));
		}
		// user clicked a year tab from the navigator page - pull this out into
		// its own action
		int pagingPosition = 1;
		Object nodeValue = node.getNodeValue();
		if (nodeValue instanceof ModelYear) {
			currentYearNode = node;
		}
		// user clicked a 'view' link on Search Summary
		else if (nodeValue instanceof MarketVehicle) {
			currentYearNode = node.getParent(); // get the vehicle's node
			int vehicleIndex = 1 + currentYearNode.getChildNodes().indexOf(node);
			pagingPosition = vehicleIndex > 0 ? vehicleIndex : pagingPosition;
		}
		else if (nodeValue instanceof DecoratedModelGroup) {
			currentYearNode = node.getChildNodes().get(
					theForm.getNavigatorMarketplaceIndex()).getChildNodes().get(0);
		}
		request.setAttribute("pagingPosition", pagingPosition);

		currentMarketNode = currentYearNode.getParent();
		currentModelNode = currentMarketNode.getParent();
		
		if(theForm.getGroupByNodeIncrement() == null ) {
			theForm.setGroupByNodeIncrement(currentModelNode);
		}

		handleCounts(currentYearNode, theForm);

		theForm.setNavigatorHasBidList(doesMarketHaveBidList(currentMarketNode));

		List<MemberMarket> markets = getSearchContext(request, theForm, theForm.getPersistenceScope()).getMarkets();
		
		theForm.setNavigatorMarkets(markets);
		theForm.setNavigatorMarketplaceIndex(markets.indexOf(currentMarketNode.getNodeValue()));
		theForm.setNavigatorYearGrouping(currentYearNode);
		theForm.setNavigatorSelectedModel(currentModelNode);

		if (theForm.getSortByColumn() != null) {
			sort(request, theForm);
			tidy(theForm);
		}

		logNavigatorClickEvent(theForm, response);
		
		request.setAttribute("includeAuctionData", getNextGenSession(request).isIncludeAuctionData());
		request.setAttribute("hasTransferPricing", getNextGenSession(request).isIncludeTransferPricing());

		request.setAttribute("marketName", theForm.getNavigatorMarkets().get(
				theForm.getNavigatorMarketplaceIndex()).getName());

		return mapping.findForward("success");
	}

	/**
	 * Either use Aspects to do logging, or use a better abstract class
	 * structure where logging in built in and the NavigatorChange*Actions don't
	 * need to worry about this.
	 * 
	 * @param theForm
	 * @param response
	 */
	private void logNavigatorClickEvent(OptimalInventorySearchSummaryForm theForm, HttpServletResponse response) {
		setDefaultVehicleOnResponse(
				theForm.getNavigatorYearGrouping(),
				theForm.getNodeId(),
				response);
		SimpleMarketVehicleImpl defaultVehicle = getDefaultVehicle(
				theForm.getNavigatorYearGrouping(),
				theForm.getNodeId());
		getLogClickEventDAO().createLogClickEvent(
				LogClickEventType.SEARCH_AND_ACQUISITION,
				theForm.getSearchContext().getMember().getCurrentBusinessUnit().getBusinessUnitID(),
				theForm.getSearchContext().getMember().getMemberID(),
				defaultVehicle.getMarketType().getId(),
				defaultVehicle.getVIN(),
				getVehicleURL(defaultVehicle));
	}

	public boolean doesMarketHaveBidList(GroupByNode node) {
		if (node == null || node.getNodeValue() == null) {
			return false;
		}

		DecoratedMarket market = (DecoratedMarket) node.getNodeValue();
		if (market.getMarketAdministrator().equals(MarketAdministrator.GMAC))
			return true;

		return false;
	}

	/**
	 * Returns the total number of vehicles encapsulated in the given set of
	 * year nodes
	 * 
	 * @param yearNodes
	 * @return
	 */
	public Integer getVehicleCount(List<GroupByNode> yearNodes) {
		// the total is in a seperate jsp fragment so it is
		// easier to compute the total here
		Integer vehicleCount = 0;
		for (GroupByNode node : yearNodes) {
			vehicleCount += node.getChildNodes().size();
		}
		return vehicleCount;
	}

	/**
	 * Takes care of getting the counts for each year as well as the total
	 * number of results returned.
	 * 
	 * @param currentYearNode
	 * @param theForm
	 */
	public void handleCounts(GroupByNode currentYearNode,
			OptimalInventorySearchSummaryForm theForm) {
		if (currentYearNode != null) {
			ModelYear currentModelYear = (ModelYear) currentYearNode.getNodeValue();
			List<GroupByNode> yearNodes = currentYearNode.getParent().getChildNodes();
			theForm.setCurrentModelYear(currentModelYear);
			theForm.setNavigatorYearNodes(yearNodes);
			theForm.setNavigatorVehicleCount(getVehicleCount(yearNodes));
		}
		else {
			theForm.setNavigatorVehicleCount(0);
		}
	}

	private SimpleMarketVehicleImpl getDefaultVehicle(GroupByNode grouping, Integer nodeId) {
		SimpleMarketVehicleImpl defaultVehicle = (SimpleMarketVehicleImpl) grouping.getChildNodes().get(0).getNodeValue();
		if (nodeId != null) {
			List<GroupByNode> children = grouping.getChildNodes();
			for (GroupByNode child : children) {
				if (child.getId().equals(nodeId)) {
					defaultVehicle = (SimpleMarketVehicleImpl) child.getNodeValue();
				}
			}
		}
		return defaultVehicle;
	}

	/**
	 * This method should be private, but scope relaxed due to same day bugfix!
	 * 
	 * @param yearGrouping
	 * @param nodeId
	 * @param response
	 */
	protected void setDefaultVehicleOnResponse(GroupByNode yearGrouping, Integer nodeId, HttpServletResponse response) {
		SimpleMarketVehicleImpl defaultVehicle = null;
		if (yearGrouping != null) {
			defaultVehicle = getDefaultVehicle(yearGrouping, nodeId);
			response.addHeader("selectedVin", defaultVehicle.getVIN());
			response.addHeader("selectedMarketId", Integer.toString(defaultVehicle.getMarketType().getId()));
		}
		response.addHeader("selectedUrl", getVehicleURL(defaultVehicle));
	}

	private String getVehicleURL(SimpleMarketVehicleImpl defaultVehicle) {
		String defaultUrl = "/NextGen/NavigatorDefaultFrame.go";
		if (defaultVehicle != null) {
			if (defaultVehicle.getUrl() != null) {
				defaultUrl = defaultVehicle.getUrl();
			}
			else {
				defaultUrl = "/NextGen/ViewInGroupVehicle.go?inventoryId="
						+ defaultVehicle.getInventoryId();
			}
		}
		return defaultUrl;
	}

	public LogClickEventDAO getLogClickEventDAO() {
		return this.logClickEventDAO;
	}
	
	public void setLogClickEventDAO(LogClickEventDAO logClickEventDAO) {
		this.logClickEventDAO = logClickEventDAO;
	}

}
