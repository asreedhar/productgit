package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.ModelGroupFunctions;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.presentation.DecoratedModelGroup;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupBy;
import biz.firstlook.webapp.purchasing.util.GroupByNode;

public class OptimalInventorySearchSummaryAddSearchCandidateAction extends OptimalInventorySearchSummaryAction {

	protected void search(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IntrospectionException {
		if (theForm.getMake() == null || theForm.getMakes().isEmpty()) {
			return;
		}
		if (theForm.getModel() == null || theForm.getModels().isEmpty()) {
			return;
		}
		// create the new search candidate
		MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		Model model = getModel(theForm);
		SearchCandidate newSearchCandidate = builder.newSearchCandidate(model);
		Collection<SearchCandidate> searchCandidates = null;
		// find an equivalent model group
		GroupByNode modelGroupNode = getModelGroupNode(model.getModelGroup(), theForm.getGroupBySearchResults());
		Integer modelGroupID = null;
		Boolean updated = Boolean.FALSE;
		Boolean added   = Boolean.FALSE;
		// clean user input
		Set<SearchCandidateCriteria> userSearchCandidateCriteria = ModelGroupFunctions.validate(theForm.getSearchCandidateCriteria());
		// if we have a matching model group then go search for the model selected
		if (modelGroupNode != null) {
			DecoratedModelGroup decoratedModelGroup = (DecoratedModelGroup) modelGroupNode.getNodeValue();
			modelGroupID = decoratedModelGroup.getModelGroupID();
			SearchCandidate theSearchCandidate = null;
			for (SearchCandidate searchCandidate : decoratedModelGroup.getSearchCandidates()) {
				if (searchCandidate.getModel().equals(newSearchCandidate.getModel())) {
					theSearchCandidate = searchCandidate;
					break;
				}
			}
			// take the union of the annotations (user + CIA)
			userSearchCandidateCriteria = ModelGroupFunctions.union(
					ModelGroupFunctions.getSearchCandidateCriteria(
							decoratedModelGroup.getSearchCandidates(),
							ModelGroupFunctions.getModelYears(userSearchCandidateCriteria),
							SearchCandidateCriteriaAnnotationEnum.ALL),
					userSearchCandidateCriteria);
			if (theSearchCandidate == null) {
				// the model is new to the model group so add it to the list of search candidates
				getSearchCandidates(request, theForm).add(newSearchCandidate);
				// add it to the partial list
				decoratedModelGroup.getSearchCandidates().add(newSearchCandidate);
				// note we added a model
				added = Boolean.TRUE;
			}
			// apply the union of all criteria to the model group search candidates
			ModelGroupFunctions.setSearchCandidateCriteria(
					decoratedModelGroup.getSearchCandidates(),
					userSearchCandidateCriteria);
			updated = Boolean.TRUE;
			// assign the search candidate list
			searchCandidates = decoratedModelGroup.getSearchCandidates();
		}
		else {
			// the model belongs in a new model group
			modelGroupID = newSearchCandidate.getModel().getModelGroup().getModelGroupID();
			// assign the search candidate list
			searchCandidates = Collections.singleton(newSearchCandidate);
			// append the users criteria to the new search candidate
			ModelGroupFunctions.setSearchCandidateCriteria(
					searchCandidates,
					userSearchCandidateCriteria);
			// add the new search candidate to the list
			getSearchCandidates(request, theForm).add(newSearchCandidate);
			// note we added something
			added = Boolean.TRUE;
		}
		// remove vehicles of the updated/added model(s)
		for (Iterator<MarketVehicle> it = theForm.getSearchResults().iterator(); it.hasNext();) {
			MarketVehicle vehicle = it.next();
			for (SearchCandidate searchCandidate : searchCandidates) {
				if (vehicle.getModel().equals(searchCandidate.getModel())) {
					it.remove();
					break;
				}
			}
		}
		// run an incremental search and append the data
		theForm.getSearchResults().addAll(getSearchResults(theForm, getSearchContext(request, theForm), searchCandidates));
		// add JSON text so the AJAX updater knows what to do
		theForm.setJsonObject(summaryJSON(theForm, modelGroupID, updated, added));
	}

	private JSONObject summaryJSON(OptimalInventorySearchSummaryForm theForm, Integer modelGroupID, Boolean updated, Boolean added) {
		JSONObject json = summaryJSON(theForm);
		jsonPut(json, "modelGroupID", modelGroupID == null ? "null" : modelGroupID);
		jsonPut(json, "updated", updated.toString());
		jsonPut(json, "added", added.toString());
		return json;
	}

	protected void tidy(OptimalInventorySearchSummaryForm theForm) {
		theForm.setGroupByNodeIncrement(
				getModelGroupNode(
						getModel(theForm).getModelGroup(), 
						theForm.getGroupBySearchResults()));
		jsonPut(theForm.getJsonObject(), "groupByNodeID", theForm.getGroupByNodeIncrement().getId());
	}

	protected GroupByNode getModelGroupNode(ModelGroup modelGroup, GroupBy groupBy) {
		for (GroupByNode n : groupBy.getGroupByNode().getChildNodes()) {
			if (n.getNodeValue().equals(modelGroup)) {
				return n;
			}
		}
		return null;
	}
	
	protected Model getModel(OptimalInventorySearchSummaryForm theForm) {
		return theForm.getModels().get(theForm.getModel());
	}

	protected void jsonPut(JSONObject obj, String key, Object value) {
		try {
			obj.put(key, value);
		}
		catch (JSONException npe) {
			// null pointer exception
		}
	}
	
}
