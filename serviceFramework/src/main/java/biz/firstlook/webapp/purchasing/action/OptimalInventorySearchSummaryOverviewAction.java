package biz.firstlook.webapp.purchasing.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.report.reportBuilders.InventoryStockingReportsBuilder;
import biz.firstlook.report.reportBuilders.OptimalInventorySummaryReportBuilder;
import biz.firstlook.report.reportBuilders.OptimalInventoryVehicleGroupingReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;

public class OptimalInventorySearchSummaryOverviewAction extends AbstractLowerUnitCostFilterAction {
    
    private IReportService reportService;
    
    public ActionForward filterIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, Integer businessUnitId, Boolean includeLowUnitCost) throws Exception {
        String groupingIdPar = request.getParameter("modelGroupId");
        String yearPar = request.getParameter("year");
        
        Integer groupingId = (groupingIdPar != null ? Integer.parseInt(groupingIdPar):null);
        Integer year = (yearPar != null ? Integer.parseInt(yearPar):null);
        
        OptimalInventorySummaryReportBuilder inventoryDetail = new OptimalInventorySummaryReportBuilder(businessUnitId, groupingId, year, includeLowUnitCost); 
        List results = reportService.getResults(inventoryDetail);
        
        if ( year != null ) {
            OptimalInventoryVehicleGroupingReportBuilder stockingInfo = new OptimalInventoryVehicleGroupingReportBuilder(businessUnitId, groupingId, year, includeLowUnitCost);
            List<Map> stockingInfoResults = reportService.getResults( stockingInfo );
            if ( stockingInfoResults != null && !stockingInfoResults.isEmpty()) {
                request.setAttribute("daysSupply", stockingInfoResults.get(0).get("DaysSupply"));
            }
        }
        else {
            InventoryStockingReportsBuilder stockingInfo = new InventoryStockingReportsBuilder(businessUnitId, 0, includeLowUnitCost);
            stockingInfo.selectGroupingId(groupingId);
            stockingInfo.selectAllYears();
            List<Map> stockingInfoResults = reportService.getResults( stockingInfo );
            if ( stockingInfoResults != null && !stockingInfoResults.isEmpty()) {
                request.setAttribute("targetInv", stockingInfoResults.get(0).get("TargetUnits"));
            }
        }
        
        request.setAttribute("results", results);
        return mapping.findForward("success");
    }

    public void setReportService(IReportService reportService) {
        this.reportService = reportService;
    }
    
}
