package biz.firstlook.webapp.purchasing.util;

import java.io.Serializable;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.impl.AbstractSearchCandidateCriteria;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;

public class InputSearchCandidateCriteriaImpl extends AbstractSearchCandidateCriteria implements Serializable, SearchCandidateCriteria {
	
	private static final long serialVersionUID = 5816234849500953274L;

	public InputSearchCandidateCriteriaImpl() {
		super();
	}

	public Set<SearchCandidateCriteriaAnnotation> getAnnotations() {
		if (annotationText != null && annotationText.equals("HotList")) {
			SearchCandidateCriteriaAnnotation a = SearchCandidateCriteriaAnnotationEnum.HOTLIST;
			return Collections.singleton(a);
		}
		if (annotationText != null && annotationText.equals("OneTime")) {
			SearchCandidateCriteriaAnnotation a = SearchCandidateCriteriaAnnotationEnum.ONETIME;
			return Collections.singleton(a);
		}
		System.err.println("Unrecognized Annotation: " + annotationText);
		return Collections.emptySet();
	}

	public Date getExpires() {
		if (expiresText == null || expiresText.length() == 0 || expiresText.equalsIgnoreCase("false")) {
			if (annotationText != null && annotationText.equals("OneTime")) {
				return Functions.midnight(1);
			}
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient(false);
		Date expires = sdf.parse(expiresText, new ParsePosition(0));
		if (expires == null) {
			System.err.println("Bad Expires-Date Format: " + expiresText);
		}
		return expires;
	}

	public ModelYear getModelYear() {
		final VehicleComponentBuilder builder = VehicleComponentBuilderFactory.getInstance().newVehicleBuilder();
		if (modelYearText == null || modelYearText.length() == 0) {
			return builder.buildModelYear(null);
		}
		try {
			return builder.buildModelYear(new Integer(modelYearText));
		}
		catch (NumberFormatException nfe) {
			System.err.println("Non-Integer ModelYear: " + modelYearText);
			return builder.buildModelYear(null);
		}
	}

	private String modelYearText;
	private String expiresText;
	private String annotationText;

	public String getExpiresText() {
		return expiresText;
	}
	public void setExpiresText(String expiresText) {
		this.expiresText = expiresText;
	}

	public String getModelYearText() {
		return modelYearText;
	}
	public void setModelYearText(String modelYearText) {
		this.modelYearText = modelYearText;
	}

	public String getAnnotationText() {
		return annotationText;
	}
	public void setAnnotationText(String annotationText) {
		this.annotationText = annotationText;
	}
	
}
