package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.service.impl.InGroupVehicleServiceImpl;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupBy;
import biz.firstlook.webapp.purchasing.util.GroupByNode;
import biz.firstlook.webapp.purchasing.util.GroupByNodeVisitor;
import biz.firstlook.webapp.purchasing.util.MarketVehicleStockNumberNodeVisitor;
import biz.firstlook.webapp.purchasing.util.MarketVehicleVinNodeVisitor;

/**
 * This class is used as a webservice call to return whether or not the client should forward to the navigator or if the search returned no results.
 * @author bfung
 *
 */
public class InGroupInventorySearchAction extends OptimalInventorySearchSummaryAction {
	
	private InGroupVehicleServiceImpl inGroupVehicleServiceImpl;
	
	private static final String NAVIGATOR = "/ViewNavigator.go";
	
	private static final String SEARCH_HOME = "/SearchHomePage.go";
	
	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//setup params
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm)form;
		
		String vinOrStock = request.getParameter("stockNumberOrVin");
		search(request, theForm, vinOrStock);

		ActionForward forward = mapping.findForward("notfound");
		request.setAttribute("forwardTo", SEARCH_HOME);
		if(theForm.getNodeId() != null) {
			forward = mapping.findForward("success");
			request.setAttribute("forwardTo", NAVIGATOR);
		}

		return forward;
	}
	
	@Override
	protected SearchContext getSearchContext(HttpServletRequest request,
			OptimalInventorySearchSummaryForm theForm, PersistenceScope scope) {
		Set<MarketType> marketTypes = new HashSet<MarketType>();
		marketTypes.add(MarketType.IN_GROUP);
		theForm.setMarketTypes(marketTypes);
		
		Integer unlimitedDistanceFromDealer = Integer.valueOf(99999);
		
		SearchContext ctx = super.getSearchContext(request, theForm, PersistenceScope.TEMPORARY);
		
		List<MemberMarket> memberMarkets = ctx.getMarkets();
		for(MemberMarket memberMarket : memberMarkets) {
			if(MarketType.IN_GROUP.equals(memberMarket.getMarketType())) {
				memberMarket.setSuppress(Boolean.FALSE.booleanValue());
				memberMarket.setMaxDistanceFromDealer(unlimitedDistanceFromDealer);
			} else {
				memberMarket.setSuppress(Boolean.TRUE.booleanValue());
			}
		}
		theForm.setUnmodifiableMemberMarket(Boolean.TRUE);
		theForm.setHaveProcessedSearchContext(Boolean.TRUE);
		return ctx;
	}
	
	/**
	 * Override the parent definition of candidates to pull from the text fields
	 * on the form, not from my candidate list derived from my stores purchasing
	 * list.
	 */
	@Override
	protected Collection<SearchCandidate> getSearchCandidates(HttpServletRequest request, OptimalInventorySearchSummaryForm form) {
		if (!form.getFormHasFlashLocateSearchCandidates()) {
			Integer dealerId = getNextGenSession(request).getBusinessUnitId();
			List<Model> models = inGroupVehicleServiceImpl.getModels(dealerId);
			Collection<SearchCandidate> searchCandidates = getMarketSearchService().getCandidateList(getMember(request, form), PersistenceScope.TEMPORARY);
			MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();

			// build list of SearchCandidateCriteria (same for every model)
			List<SearchCandidateCriteria> searchCandidateCriteria = buildSearchCandidateCriteria(form, builder);

			// build list of SearchCandidates
			// the hashSet is there to ensure unique Models from the days when models were Strings
			for (Model model : new HashSet<Model>(models)) {
				SearchCandidate searchCandidate = builder.newSearchCandidate(model);
				for (SearchCandidateCriteria c : searchCandidateCriteria) {
					SearchCandidateCriteria newSearchCandidateCriteria = builder.newSearchCandidateCriteria(c.getModelYear(), c.getExpires());
					newSearchCandidateCriteria.getAnnotations().addAll(c.getAnnotations());
					searchCandidate.getCriteria().add(newSearchCandidateCriteria);
				}
				searchCandidates.add(searchCandidate);
			}

			// i do not believe we have to force call this as that's what #add
			// does ...
			// CollectionMutationPersistor collectionMutationPersistor =
			// (CollectionMutationPersistor)searchCandidates;
			// collectionMutationPersistor.saveOrUpdate();

			form.setTotalSearch(Boolean.FALSE);
			form.setSearchCandidates(searchCandidates);
			form.setFormHasFlashLocateSearchCandidates(true);
		}
		return form.getSearchCandidates();
	}
	
	/**
	 * Builds a list of SearchCandidateCriteria (basically represents the years to search)
	 */
	protected List<SearchCandidateCriteria> buildSearchCandidateCriteria(OptimalInventorySearchSummaryForm theForm, MarketSearchComponentBuilder builder) {
		List<SearchCandidateCriteria> searchCandidateCriteria = new ArrayList<SearchCandidateCriteria>();
		//all years

		SearchCandidateCriteria allYearsCriteria = builder.newSearchCandidateCriteria(null, null);
		allYearsCriteria.getAnnotations().add(SearchCandidateCriteriaAnnotationEnum.ONETIME);
		searchCandidateCriteria.add(allYearsCriteria);
		return searchCandidateCriteria;
	}

	protected void search(HttpServletRequest request,
			OptimalInventorySearchSummaryForm theForm, String vinOrStock)
			throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, IntrospectionException {
		search(request, theForm);
		GroupByNode node = getNode(vinOrStock, theForm);
		if(node != null) {
			theForm.setNodeId(node.getId());
		} else {
			theForm.setNodeId(null); //just making sure this is nulled out since form lives in session.
		}
	}

	@Override
	protected void search(HttpServletRequest request,
			OptimalInventorySearchSummaryForm theForm)
			throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, IntrospectionException {
		super.search(request, theForm);
		group(request, theForm);
		empty(request, theForm);
		
	}

	private GroupByNode getNode(String vinOrStock, OptimalInventorySearchSummaryForm theForm) {
		GroupByNode node = null;
		GroupByNodeVisitor searchVisitor = null;
		GroupBy groupBy = theForm.getGroupBySearchResults();
		GroupByNode root = groupBy.getGroupByNode();
		if(StringUtils.isNotBlank(vinOrStock)) {
			try {
				VinUtility.validate(vinOrStock);
				//search on vin
				searchVisitor = new MarketVehicleVinNodeVisitor(vinOrStock);
			} catch (InvalidVinException e) {
				//not a vin
				searchVisitor = new MarketVehicleStockNumberNodeVisitor(vinOrStock);
			}
			
			searchVisitor.visit(root);
			node = searchVisitor.getNode();
		}
		return node;
	}

	public void setInGroupVehicleServiceImpl(
			InGroupVehicleServiceImpl inGroupVehicleServiceImpl) {
		this.inGroupVehicleServiceImpl = inGroupVehicleServiceImpl;
	}
}
