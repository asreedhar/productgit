package biz.firstlook.webapp.purchasing.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
  *	Cache bean methods for quick reflection.
  *	
  *	@author Simon Wenmouth
  */
public class BeanMethodCache
{
	private static Map<Class,BeanMethodCache> map = new HashMap<Class,BeanMethodCache>();

	public static BeanMethodCache getInstance(Class clazz) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		BeanMethodCache instance = (BeanMethodCache) map.get(clazz);
		if (instance == null) {
			instance = new BeanMethodCache(clazz);
			map.put(clazz, instance);
		}
		return instance;
	}

	private Class clazz;
	private Map<String,Method> getters;
	private Map<String,Method> setters;

	private BeanMethodCache(Class clazz) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		this.clazz = clazz;
		cache();
	}

	/**
	  *	@return the zero argument getter for the argument property.
	  */
	public Method getter(String property) {
		return (Method) getters.get(property);
	}

	/**
	  *	@return the JavaBean argument setter for the argument property.
	  */
	public Method setter(String property) {
		return (Method) setters.get(property);
	}

	/**
	  *	@return true if the class has both a JavaBean getter and setter for the argument property, otherwise false.
	  */
	public boolean isBeanProperty(String property) {
		return (getters.get(property) != null && setters.get(property) != null);
	}

	/**
	  *	@return an array of the beans properties with getter methods.
	  */
	public String[] getters() {
		return (String[]) getters.keySet().toArray(new String[0]);
	}

	/**
	  *	@return an array of the beans properties with setter methods.
	  */
	public String[] setters() {
		return (String[]) setters.keySet().toArray(new String[0]);
	}

	/**
	  *	@return an array of the beans properties with both getter and setter methods.
	  */
	public String[] properties() {
		Set<String> keys = getters.keySet();
		keys.retainAll(setters.keySet());
		return (String[]) keys.toArray(new String[0]);
	}

	/**
	  *	Executes the argument getter on the supplied instance.
	  */
	public Object get(Object instance, String property) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		return getter(property).invoke(instance, new Object[0]);
	}

	/**
	  *	Executes the argument setter on the supplied instance with no conversion of the value.
	  */
	public void set(Object instance, String property, Object value) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		setter(property).invoke(instance, new Object[] { value });
	}

	/**
	  *	Cache the bean Method instances for quick reflection.
	  */
	private void cache() throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		// initialize getter/setter maps
		getters = new HashMap<String,Method>();
		setters = new HashMap<String,Method>();
		// add data to the maps
		BeanInfo beanInfo = Introspector.getBeanInfo(clazz, Introspector.USE_ALL_BEANINFO);
		PropertyDescriptor[] properties = beanInfo.getPropertyDescriptors();
		for (int i = 0; i < properties.length; i++) {
			PropertyDescriptor property = properties[i];
			String name = property.getName();
			Method method;
			if ((method = property.getReadMethod()) != null) {
				getters.put(name, method);
			}
			if ((method = property.getWriteMethod()) != null) {
				setters.put(name, method);
			}
		}
	}
}

