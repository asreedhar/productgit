package biz.firstlook.webapp.purchasing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.market.search.presentation.DecoratedModelYear;
import biz.firstlook.module.market.search.presentation.impl.DecoratedModelGroupImpl;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupByNode;

public class OptimalInventorySearchSummarySelectNodeAction extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		theForm.setGroupByNodeIncrement(theForm.getGroupBySearchResults().get(theForm.getSelectedNode()));
		
        getOptimalGroupingStock(theForm.getGroupByNodeIncrement());
        
        return mapping.findForward("success");
	}
    
    private void getOptimalGroupingStock(GroupByNode node) {
        int optimalStockLevel = 0;
        DecoratedModelGroupImpl modelGroup = (DecoratedModelGroupImpl) node.getNodeValue();
        for(DecoratedModelYear modelYear : modelGroup.getModelYears()) {
            if (modelYear.getModelYear() != null) {
                optimalStockLevel += modelYear.getStockLevel().getOptimalUnits();
            }
        }
        modelGroup.getModelYears().get(0).getStockLevel().setOptimalUnits(optimalStockLevel);
        
    }
    
}
