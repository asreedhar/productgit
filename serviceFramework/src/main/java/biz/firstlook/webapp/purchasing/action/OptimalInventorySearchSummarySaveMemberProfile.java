package biz.firstlook.webapp.purchasing.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.SearchHomePage;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class OptimalInventorySearchSummarySaveMemberProfile extends SearchBaseAction {

	private static final Set<MarketType> MARKET_TYPES = Collections.emptySet();
	
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		// force update of all markets
		theForm.setMarketTypes(MARKET_TYPES);
		theForm.setIgnoreMarketSuppression(Boolean.FALSE);
		theForm.setUnmodifiableMemberMarket(Boolean.FALSE);
		// get the search contexts (unmodified)
		SearchContext ctxToday = getSearchContext(request, theForm, PersistenceScope.TODAY);
		SearchContext ctxPerm = getSearchContext(request, theForm, PersistenceScope.PERMANENT);
        SearchContext ctxTemp = getSearchContext(request, theForm, PersistenceScope.TEMPORARY);
		// set the max distance from dealer here as the MarketTypes set is empty
		// which maintains the ListMutationPersistor for later transformations
		setMaxDistanceFromDealer(ctxToday.getMarkets(), theForm.getMaxDistanceFromDealer(), theForm.getMaxDistanceLiveFromDealer());
		setMaxDistanceFromDealer(ctxPerm.getMarkets(), theForm.getMaxDistanceFromDealer(), theForm.getMaxDistanceLiveFromDealer());
        setMaxDistanceFromDealer(ctxTemp.getMarkets(), theForm.getMaxDistanceFromDealer(), theForm.getMaxDistanceLiveFromDealer());
		// set the max and min mileages
        setMileageRange(ctxToday.getMarkets(), theForm.getMaxMileage(), theForm.getMinMileage());
        setMileageRange(ctxPerm.getMarkets(), theForm.getMaxMileage(), theForm.getMinMileage());
        setMileageRange(ctxTemp.getMarkets(), theForm.getMaxMileage(), theForm.getMinMileage());
		// save market selections
		saveMarkets(request, ctxToday, ctxPerm);
        saveMarkets(request, ctxPerm, ctxTemp);
        saveMarkets(request, ctxPerm, ctxToday);
		// save market ordering
		List<String> newMemberMarketOrder = theForm.getMarketOrder();
		saveMarketOrder(ctxToday, newMemberMarketOrder);
		saveMarketOrder(ctxPerm, newMemberMarketOrder);
        saveMarketOrder(ctxTemp, newMemberMarketOrder);
		// save member home page
		final Member member = theForm.getMember();
		member.getMemberPreferences().setSearchHomePage(SearchHomePage.toSearchHomePage(theForm.getMemberSearchHomePageSelection()));
		getCoreService().saveMemberPreferences(member.getMemberPreferences());
		// put the updated search context on the form as we've changed the object outside the modifiable adapter
		theForm.resetSearchContext(ctxToday);
		// do nothing
		return null;
	}

	private void setMaxDistanceFromDealer(List<MemberMarket> memberMarkets, Integer maxDistanceFromDealer, Integer maxLiveDistanceFromDealer) {
		if (maxDistanceFromDealer != null) {
			for (MemberMarket memberMarket : memberMarkets) {
                if (memberMarket.getMarketType() == MarketType.LIVE && maxLiveDistanceFromDealer != null) {
                    memberMarket.setMaxDistanceFromDealer(maxLiveDistanceFromDealer);
                }
                else {
                    memberMarket.setMaxDistanceFromDealer(maxDistanceFromDealer);
                }
			}
		}
    }
    private void setMileageRange(List<MemberMarket> memberMarkets, Integer maxMileage, Integer minMileage) {
        if (maxMileage != null && minMileage != null) {
            for (MemberMarket memberMarket : memberMarkets) {
                memberMarket.setMaxMileage(maxMileage);
                memberMarket.setMinMileage(minMileage);
            }
        }
    }
	
	protected void saveDistanceAndMileageForDealer(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
        getSearchContext(request, theForm, PersistenceScope.TODAY);
        getSearchContext(request, theForm, PersistenceScope.PERMANENT);
	}

	protected void saveMarkets(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
        SearchContext ctxToday = getSearchContext(request, theForm, PersistenceScope.TODAY);
        SearchContext ctxPerm = getSearchContext(request, theForm, PersistenceScope.PERMANENT);
        SearchContext ctxTemp = getSearchContext(request, theForm, PersistenceScope.TEMPORARY);
        saveMarkets(request, ctxToday, ctxPerm);
        saveMarkets(request, ctxPerm, ctxTemp);
        saveMarkets(request, ctxPerm, ctxToday);
	}
	
	protected void saveMarkets(HttpServletRequest request, SearchContext ctxTemp, SearchContext ctxPerm) {
		for (MemberMarket mt : ctxTemp.getMarkets()) {
			for (MemberMarket mp : ctxPerm.getMarkets()) {
				if (mt.equals(mp)) {
					mp.setSuppress(mt.getSuppress());
				}
			}
		}
	}
	
	protected void saveMarketOrder(SearchContext ctx, List<String> newMemberMarketOrder) {
		List<MemberMarket> memberMarkets = ctx.getMarkets();
		List<MemberMarket> newMemberMarkets = new ArrayList<MemberMarket>();
		for (String index : newMemberMarketOrder) {
			newMemberMarkets.add(memberMarkets.get(Integer.parseInt(index)));
		}
		memberMarkets.clear();
		memberMarkets.addAll(newMemberMarkets);
	}

}
