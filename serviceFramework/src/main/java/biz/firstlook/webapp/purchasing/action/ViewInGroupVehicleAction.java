package biz.firstlook.webapp.purchasing.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.entity.Inventory;
import biz.firstlook.entity.Photo;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.actionFilter.NextGenSession;
import biz.firstlook.main.commonDAOs.IInventoryDAO;
import biz.firstlook.main.commonServices.PreferenceService;
import biz.firstlook.module.core.impl.entity.BusinessUnit;
import biz.firstlook.module.core.impl.entity.JobTitle;
import biz.firstlook.module.core.impl.entity.Member;
import biz.firstlook.module.core.impl.entity.dao.BusinessUnitDAO;
import biz.firstlook.module.core.impl.entity.dao.MemberDAO;
import biz.firstlook.module.core.impl.entity.dao.ApplicationEventDAO;
import biz.firstlook.report.reportBuilders.SelectedVehicleOptionsReportBuilder;
import biz.firstlook.report.reportFramework.IReportService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;

/**
 * Action for loading data for the In-Group vehicle detail page for Search and Acqusition.
 * 
 * @author kkelly
 * 
 */
public class ViewInGroupVehicleAction extends NextGenAction
{
private IInventoryDAO inventoryDAO;
private BusinessUnitDAO businessUnitDAO;
private IReportService transactionalReportService;
private PreferenceService preferenceService;
private MemberDAO memberDAO;
private ApplicationEventDAO applicationEventDAO;
private java.util.Date date;
 

public ViewInGroupVehicleAction()
{
	super();
}

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	DynaActionForm inGroupVehicleForm = (DynaActionForm)form;
	Integer inventoryId = (Integer)inGroupVehicleForm.get( "inventoryId" );
	Inventory inventory = getInventoryDAO().findById( inventoryId );
	BusinessUnit owningBusinessUnit = getBusinessUnitDAO().findById( inventory.getBusinessUnitId() );
	List<Member> contactList = memberDAO.findByDealerAndJobTitle(owningBusinessUnit, JobTitle.USED_CAR_MANAGER);
	Collections.sort(contactList);
	List<Member> generalManagers = memberDAO.findByDealerAndJobTitle(owningBusinessUnit, JobTitle.GENERAL_MANAGER);
	Collections.sort(generalManagers);
	contactList.addAll(generalManagers);
	List<Member> generalSalesManagers = memberDAO.findByDealerAndJobTitle(owningBusinessUnit, JobTitle.GENERAL_SALES_MANAGER);
	Collections.sort(generalSalesManagers);
	contactList.addAll(generalSalesManagers);
	
	applicationEventDAO.setLoginStatus(contactList);

	SelectedVehicleOptionsReportBuilder reportBuilder = new SelectedVehicleOptionsReportBuilder( inventoryId );
	List< Map > options = getTransactionalReportService().getResults( reportBuilder );

	Map< String, Object > preferences = getPreferenceService().getPreferences( inventory.getBusinessUnitId(),
																				PreferenceService.ReportsEnum.SEARCH_AND_ACQUISITION );
	biz.firstlook.module.core.BusinessUnit dealer = getCoreService().findBusinessUnitByID(inventory.getBusinessUnitId());

    String businessUnitCode = dealer.getBusinessUnitCode();
    String vin = inventory.getVehicle().getVin();
    List<String> inventoryPhotoUrls = InventoryPhotosWebServiceClient.getInstance().GetInventoryPhotoUrls(businessUnitCode, vin);	

    date = new java.util.Date();
    
    final NextGenSession nextGenSession = getNextGenSession(request);
    
	Member member = memberDAO.findById(nextGenSession.getMemberId());    
    String loggedInUser =  String.format("%s %s", member.getFirstName(), member.getLastName());
    String userEmailAddress = member.getEmailAddress();
    String formatedTransferPrice = "";
    
    Float transferPrice = inventory.getTransferPrice();
    if (transferPrice != null) {
    	formatedTransferPrice = new DecimalFormat("#0").format(transferPrice);
    }
    
    int currentDealerId = inventory.getBusinessUnitId();
    
	try {
		Map<String,String> properties = CarfaxService.getInstance().getCarfaxReportProperties(
				currentDealerId,
				member.getLogin(),
				vin,
				VehicleEntityType.INVENTORY);
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (CarfaxException ce) {
		request.setAttribute("hasCarfaxError", true);
		request.setAttribute("problem", ce.getResponseCode());
	}

	try {
		Map<String,String>	properties = AutoCheckService.getInstance().getAutoCheckReportProperties(
			currentDealerId,
			member.getLogin());
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (AutoCheckException ae) {
		request.setAttribute("hasAutoCheckError", true);
		request.setAttribute("autoCheckProblem", ae.getResponseCode());
	}
	
	request.setAttribute("photos", inventoryPhotoUrls);
	request.setAttribute( "inventory", inventory );
	request.setAttribute( "businessUnit", owningBusinessUnit );
	request.setAttribute( "options", options );
	request.setAttribute("contacts", contactList);
	request.setAttribute("date", date.toString());
	request.setAttribute("loggedInUser", loggedInUser);
	request.setAttribute("userEmailAddress", userEmailAddress);
	request.setAttribute("formatedTransferPrice", formatedTransferPrice);

	if ( preferences != null && preferences.get( "DisplayUnitCostToDealerGroup" ) != null )
	{
		Integer displayUnitCostFlag = (Integer)preferences.get( "DisplayUnitCostToDealerGroup" );
		request.setAttribute( "displayUnitCost", displayUnitCostFlag == 1 );
	}
	
	request.setAttribute("hasTransferPricing", nextGenSession.isIncludeTransferPricing());	

	return mapping.findForward( "success" );
}

class PrimaryPhotoComparator implements Comparator<Photo> {
    public int compare(Photo p0, Photo p1) {
    	Boolean b0 = p0.getIsPrimaryPhoto();
    	Boolean b1 = p1.getIsPrimaryPhoto();
        if (b0 == null) {
            if (b1 == null) {
                return 0;
            }
            return 1;
        }
        else {
            if (b1 == null) {
                return -1;
            }
            return b1.compareTo(b0);
        }
    }
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public BusinessUnitDAO getBusinessUnitDAO()
{
	return businessUnitDAO;
}

public void setBusinessUnitDAO( BusinessUnitDAO businessUnitDAO )
{
	this.businessUnitDAO = businessUnitDAO;
}

public IReportService getTransactionalReportService()
{
	return transactionalReportService;
}

public void setTransactionalReportService( IReportService transactionalReportService )
{
	this.transactionalReportService = transactionalReportService;
}

public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public void setMemberDAO(MemberDAO memberDAO) {
	this.memberDAO = memberDAO;
}

public void setApplicationEventDAO(ApplicationEventDAO applicationEventDAO) {
	this.applicationEventDAO = applicationEventDAO;
}

}
