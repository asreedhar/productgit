package biz.firstlook.webapp.purchasing.util;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.logging.LogFactory;

@SuppressWarnings("serial")
public class DereferenceReflectionComparator extends ReflectionComparator {

	private String dereferenceProperty;
	
	public DereferenceReflectionComparator(String dereferenceProperty, String[] sortOrderNames, String[][] beanProperties, int[][] beanPropertiesOrder) {
		super(sortOrderNames, beanProperties, beanPropertiesOrder);
		this.dereferenceProperty = dereferenceProperty;
	}

	public int compare(Object o1, Object o2) {
		int j = 0;
		try {
			BeanMethodCache cache1 = BeanMethodCache.getInstance(o1.getClass());
			BeanMethodCache cache2 = BeanMethodCache.getInstance(o2.getClass());
			
			Object d1 = cache1.get(o1, dereferenceProperty);
			Object d2 = cache2.get(o2, dereferenceProperty);
			
			j = super.compare(d1, d2);
		}
		catch (IllegalAccessException e) {
			LogFactory.getLog(DereferenceReflectionComparator.class).warn(e.getMessage() + " caused by " + e.getCause());
		}
		catch (InvocationTargetException e) {
			LogFactory.getLog(DereferenceReflectionComparator.class).warn(e.getMessage() + " caused by " + e.getCause());
		}
		catch (IntrospectionException e) {
			LogFactory.getLog(DereferenceReflectionComparator.class).warn(e.getMessage() + " caused by " + e.getCause());
		}
		return j;
	}
}
