package biz.firstlook.webapp.purchasing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.core.SearchHomePage;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class OptimalInventorySearchSummaryLoadMemberProfile extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		// home page
		SearchHomePage searchHomePage = getMember(request, theForm).getMemberPreferences().getSearchHomePage();
		if (searchHomePage == SearchHomePage.OPTIMAL_INVENTORY) {
			theForm.setMemberSearchHomePageSelection(SearchHomePage.OPTIMAL_INVENTORY.toString());			
		} else {
				theForm.setMemberSearchHomePageSelection(SearchHomePage.MARKETPLACE.toString());
		}
		initForm(request, theForm);
		// render the JSP
		return mapping.findForward("success");
	}

}
