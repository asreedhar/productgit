package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupByNode;

public class NavigatorChangeMarketplaceAction extends ViewNavigatorAction 
{

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
    OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm)form;
    Integer selectedMarketplaceIndex = theForm.getNavigatorMarketplaceIndex();
    GroupByNode selectedModel = theForm.getNavigatorSelectedModel(); 
    
    MemberMarket selectedMarket = theForm.getNavigatorMarkets().get( selectedMarketplaceIndex );
    GroupByNode currentMarketplaceNode = null;
    for ( GroupByNode node : selectedModel.getChildNodes() )
    {
        if ( node.getNodeValue().equals( selectedMarket ) )
        {
            currentMarketplaceNode = node;
            break;
        }
    }
    
    //The selected marketplace is not in our current results to search again.
    if (currentMarketplaceNode == null) {
        currentMarketplaceNode = updateSearchResults(theForm, request, selectedMarket);
    }
    
	theForm.setNavigatorYearNodes( currentMarketplaceNode.getChildNodes() );
	theForm.setNavigatorVehicleCount( getVehicleCount( currentMarketplaceNode.getChildNodes() ) );
	
	if ( !currentMarketplaceNode.getChildNodes().isEmpty() )
	{
	    theForm.setNavigatorYearGrouping( currentMarketplaceNode.getChildNodes().get( 0 ) );
	    // seems like this is duplicating the line above???
	    theForm.setCurrentModelYear( (ModelYear) currentMarketplaceNode.getChildNodes().get( 0 ).getNodeValue() );
	}
	else
	{
		theForm.setNavigatorYearGrouping( null );
		theForm.setCurrentModelYear( null );
	}
	
	//logNavigatorClickEvent( theForm, response );
	//the above line produces double logging - no time this bugfix round to fix.
	setDefaultVehicleOnResponse( theForm.getNavigatorYearGrouping(), theForm.getNodeId(), response );
	theForm.setNavigatorHasBidList( doesMarketHaveBidList( currentMarketplaceNode ) );
	
	request.setAttribute("marketName", theForm.getNavigatorMarkets().get(theForm.getNavigatorMarketplaceIndex()).getName());
	
	return mapping.findForward( "success" );
}

protected void tidy(OptimalInventorySearchSummaryForm theForm) {
    super.tidy( theForm );
}

private GroupByNode updateSearchResults(OptimalInventorySearchSummaryForm theForm, HttpServletRequest request, MemberMarket selectedMarket) 
    throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IntrospectionException {
    theForm.setUnmodifiableMemberMarket( true );
    search(request, theForm);
    group(request, theForm);
    empty(request, theForm);
    sort(request, theForm);
    tidy(theForm);  
    
    GroupByNode currentYearNode = theForm.getGroupBySearchResults().get( theForm.getNodeId() ).getParent(); // get the vehicle's node
    GroupByNode currentMarketplaceNode = currentYearNode.getParent();
    GroupByNode currentModelNode = currentMarketplaceNode.getParent();
    theForm.setNavigatorSelectedModel( currentModelNode );
    
    for ( GroupByNode node : currentModelNode.getChildNodes() )
    {
        if ( node.getNodeValue().equals( selectedMarket ) )
        {
            currentMarketplaceNode = node;
            break;
        }
    }
    
    return currentMarketplaceNode;
}

}
