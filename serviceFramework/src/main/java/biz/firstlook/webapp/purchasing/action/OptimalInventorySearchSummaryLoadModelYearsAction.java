package biz.firstlook.webapp.purchasing.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class OptimalInventorySearchSummaryLoadModelYearsAction extends NextGenAction {
	
	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		if (theForm.getModel() == null || theForm.getModels().isEmpty()) {
			return mapping.findForward("failure");
		}
		theForm.setModelYears(VehicleServiceFactory.getFactory().getService().getModelYears(theForm.getModels().get(theForm.getModel())));
		response.addHeader("X-JSON", JSONAdapter(theForm.getModelYears()));
		return mapping.findForward("success");
	}

	protected String JSONAdapter(List<ModelYear> modelYears) {
		JSONObject json = new JSONObject();
		JSONArray jsonModelYears = new JSONArray();
		for (ModelYear m : modelYears) {
			try {
				JSONObject jsonModelYear = new JSONObject();
				jsonModelYear.put("year", m.getModelYear());
				jsonModelYears.put(jsonModelYear);
			}
			catch (JSONException npe) {
				// is a null pointer exception if name is null
			}
		}
		return json.toString();
	}
	
}
