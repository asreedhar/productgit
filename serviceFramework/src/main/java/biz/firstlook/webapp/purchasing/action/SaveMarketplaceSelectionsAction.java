package biz.firstlook.webapp.purchasing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class SaveMarketplaceSelectionsAction extends OptimalInventorySearchSummarySaveMemberProfile {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		saveMarkets(request, theForm);
		return null;
	}

}
