package biz.firstlook.webapp.purchasing.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.module.market.search.impl.entity.CIACategory;
import biz.firstlook.report.dao.IBasicIMTLookUpDAO;

public class StockingReportService implements IStockingReportService {

    private IBasicIMTLookUpDAO basicIMTLookUpDAO;
    
    public boolean hasBuyingPlan(Integer businessUnitId) {
        StringBuilder query = new StringBuilder();
        query.append("select * from CIAGroupingItems as item, ");
        query.append("CIAGroupingItemDetails as itemDetail, ");
        query.append("CIABodyTypeDetails as typeDetail, ");
        query.append("CIASummary as summary ");
        query.append("where summary.businessUnitID = ");
        query.append(businessUnitId.toString());
        query.append("and summary.CiaSummaryStatusID = 2 ");
        query.append("and itemDetail.ciaGroupingItemDetailTypeId = 3 ");
        query.append("and summary.ciaSummaryId = typeDetail.ciaSummaryId ");
        query.append("and item.ciaBodyTypeDetailId = typeDetail.ciaBodyTypeDetailId ");
        query.append("and item.ciaGroupingItemId = itemDetail.ciaGroupingItemId ");
        
        List<Map<String,Object>> results = basicIMTLookUpDAO.getResults(query.toString(), null);
        
        if (results != null && !results.isEmpty()) {
            return true;
        }
        return false;
    }
    
    public Integer retrieveDaySupply(Integer businessUnitId) {
        Integer daysSupply = null;
        StringBuilder query = new StringBuilder();
        query.append("select TargetDaysSupply FROM tbl_CIAPreferences ");
        query.append("where BusinessUnitId = ");
        query.append(businessUnitId);
        
        List<Map<String,Object>> results = basicIMTLookUpDAO.getResults(query.toString(), null);
        
        if ( results != null && !results.isEmpty()){
            daysSupply = (Integer) results.get(0).get("TargetDaysSupply");
        }
       
        return daysSupply;
    }
    
    public List<Map> filterOthers(List<Map> segments, boolean modifyStock) {
        int i = 0;
        for (Map segment:segments) {
            Integer target = (Integer) segment.get("NonCoreTargetUnits");
            Map other = (Map) segment.get("Other");
            if (other != null) {
                Object unitsInStock = other.get("UnitsInStock");
                Integer otherStock = (unitsInStock == null ? 0:Integer.parseInt(unitsInStock.toString()));
                int diff = otherStock - target;
                if (modifyStock && diff <= 0) {
                    segments.get(i).remove("Other");
                }
                if (!modifyStock && diff >= 0) {
                    segments.get(i).remove("Other");
                }
                i++;
            }
        }
        return segments;
    }
    
    @SuppressWarnings("unchecked")
    public List addOthers(List<Map> segments, List<Map> others) {
        Iterator segmentItr = segments.iterator();
        Iterator otherItr;
        int i=0;
        while (segmentItr.hasNext()) {
            Map segment = (Map)segmentItr.next();
            otherItr = others.iterator();
            while (otherItr.hasNext()) {
                Map other = (Map)otherItr.next();
                if (segment.get("VehicleSegmentID").equals(other.get("VehicleSegmentID"))) {
                    segments.get(i).put("Other", other);
                }
            }
            i++;
        }
        return segments;
    }
    
    @SuppressWarnings("unchecked")
    public List createResults(List<Map> segments, List<Map> modelGroups, List<Map> years, List<Map> others, Boolean modifyStock) {
        Iterator modelItr = modelGroups.iterator();
        Iterator yearItr;
        
        int i = 0;
        while (modelItr.hasNext()) {
            Map modelGrouping = (Map) modelItr.next();
            if( modelGrouping != null && !modelGrouping.isEmpty()) {
	            Integer groupingDescriptionId = (Integer) modelGrouping.get("VehicleGroupingID");
	            Integer ciaCategoryId = (Integer) modelGrouping.get("CIACategoryID");
	            List<Map> groupingYears = new ArrayList<Map>();
	            yearItr = years.iterator();
	            
	            int noYearLevelTargetUnits = 0; //go through each year to see sum of target inventory
	            while (yearItr.hasNext()) {
	                Map year = (Map) yearItr.next();
	                if (groupingDescriptionId != null && groupingDescriptionId.equals(year.get("VehicleGroupingID"))) {
	                	if( ciaCategoryId.equals( CIACategory.POWERZONE.getCIACategoryID()) 
	                			|| ciaCategoryId.equals( CIACategory.WINNERS.getCIACategoryID() )) {
	                		Integer yearLevelTargetUnits = (Integer)year.get( "TargetUnits" );
	                		noYearLevelTargetUnits += yearLevelTargetUnits.intValue();
	                		groupingYears.add(year);
	                	}
	                }
	            }
	            Integer modelStocking = ((Integer)modelGrouping.get( "UnitsInStock" ));
	            modelStocking = modelStocking - ((Integer)modelGrouping.get( "TargetUnits" ));
	            //if target inventory for all years is 0, don't return a the year lines.
	            if( noYearLevelTargetUnits == 0 && modelStocking == 0) {
	            	groupingYears.clear();
	            }
	           
	            //filtering the Model Groups out based on if you want overstock or understocked vehicles
	            boolean includeModelGroup = true;
	            if(modifyStock != null){
	            	//looking for overstocked vehicles but the model group says understocked or vice versa
	            	if((modifyStock && modelStocking < 0) || (!modifyStock && modelStocking > 0)){
	            		//there are no vehicle years under this model that need to be included in the report
	            		if(groupingYears.isEmpty()){
	            			modelItr.remove();
	            			includeModelGroup = false;
	            		}
	            	}
	            }
	            if(includeModelGroup){
	            	modelGroups.get(i).put("Years", groupingYears);
	            	i++;
	            }
            }
            else{
            	i++;
            }
        }
        
        Iterator segmentItr = segments.iterator();
        i = 0;
        while (segmentItr.hasNext() ){
            Map segment = (Map) segmentItr.next();
            Integer segmentId = (Integer) segment.get("VehicleSegmentID");
            List<Map> segmentGroupings = new ArrayList<Map>();
            modelItr = modelGroups.iterator();
            while (modelItr.hasNext()) {
                Map modelGrouping = (Map) modelItr.next();
                if (segmentId != null && segmentId.equals(modelGrouping.get("VehicleSegmentID"))) {
                    segmentGroupings.add(modelGrouping);
                }
            }
            segments.get(i).put("ModelGroups", segmentGroupings);
            i++;
        }
        return segments;
    }

    public List<String> retrieveGroupingIds(List<Map> reportResults) {
        List<String> groupingIds = new ArrayList<String>();
        for(Map result:reportResults) {
            groupingIds.add(result.get("VehicleGroupingID").toString());
        }
        return groupingIds;
    }
    
    public void setBasicIMTLookUpDAO( IBasicIMTLookUpDAO basicIMTLookUpDAO )
    {
        this.basicIMTLookUpDAO = basicIMTLookUpDAO;
    }
}
