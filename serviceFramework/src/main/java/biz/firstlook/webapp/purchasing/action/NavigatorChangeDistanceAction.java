package biz.firstlook.webapp.purchasing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupByNode;

public class NavigatorChangeDistanceAction extends OptimalInventorySearchSummaryAction
{

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
	GroupByNode selectedModelNode = theForm.getNavigatorSelectedModel();
	theForm.setUnmodifiableMemberMarket( true );

	search(request, theForm);
	group(request, theForm);
	empty(request, theForm);
	sort(request, theForm);
	tidy(theForm);	
		
	// locate the selected model in the new search results and set its node id on the form
	// (the 'change model' action will take it from here)
	for( GroupByNode node : theForm.getGroupBySearchResults().getGroups() )
	{
	    if ( ((ModelGroup)node.getNodeValue()).equals(selectedModelNode.getNodeValue()) )
	    {
	        theForm.setNodeId( node.getId() );
	        break;
	    }
	}
	
	request.setAttribute("marketName", theForm.getNavigatorMarkets().get(theForm.getNavigatorMarketplaceIndex()).getName());
	
	request.setAttribute( "isNavigator", "true" );
	return mapping.findForward( "success" );
}

protected void tidy(OptimalInventorySearchSummaryForm theForm) {
	super.tidy( theForm );
}

}
