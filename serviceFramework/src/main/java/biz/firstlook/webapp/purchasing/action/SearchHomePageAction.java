package biz.firstlook.webapp.purchasing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.SearchHomePage;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class SearchHomePageAction extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		theForm.reset();
		Member member = getMember(request, theForm);
		if (member.getMemberPreferences().getSearchHomePage() == null) {
			request.setAttribute("forwardTo", SearchHomePage.OPTIMAL_INVENTORY.getURL() );
		}
		else if (member.getMemberPreferences().getSearchHomePage() == SearchHomePage.MARKETPLACE) {
			request.setAttribute("forwardTo", SearchHomePage.MARKETPLACE.getURL() );
		}
		else {
			request.setAttribute("forwardTo", SearchHomePage.OPTIMAL_INVENTORY.getURL() );
		}
		theForm.setMember(null); //what?
		request.setAttribute( "scheme", HttpServletRequestHelper.getScheme(request) );
		request.setAttribute( "context", "NextGen" );
		return mapping.findForward( "success" );
	}
}
