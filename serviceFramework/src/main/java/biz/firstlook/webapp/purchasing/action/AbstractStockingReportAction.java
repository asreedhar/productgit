package biz.firstlook.webapp.purchasing.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.report.reportFramework.IReportService;
import biz.firstlook.webapp.purchasing.service.IStockingReportService;

public abstract class AbstractStockingReportAction extends AbstractLowerUnitCostFilterAction
{

private final static Log log = LogFactory.getLog(AbstractStockingReportAction.class);
	
protected IStockingReportService stockingReportService;
protected IReportService reportService;

protected final static int ALL_INVENTORY_STRATEGY = 0;
protected final static int RETAIL_INVENTORY_STRATEGY = 1;

@Override
public final ActionForward filterIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response,
										Integer businessUnitId, Boolean includeLowUnitCost ) throws Exception
{
	Integer strategyId = Integer.valueOf(RETAIL_INVENTORY_STRATEGY); 
	if ( request.getParameter( "strategyId" ) != null )	{
		try{
			strategyId = Integer.parseInt( request.getParameter( "strategyId" ) );
		} catch (NumberFormatException e) {
			log.warn("Invalid strategyId parameter specified, defaulting to Retail Inventory.");
		}
	}

	return reportIt( mapping, request, businessUnitId, strategyId, includeLowUnitCost );
}

protected abstract ActionForward reportIt( ActionMapping mapping, HttpServletRequest request, Integer businessUnitId, Integer strategyId,
											Boolean includeUnitCostThreshold ) throws Exception;

public void setReportService( IReportService reportService )
{
	this.reportService = reportService;
}

public void setStockingReportService( IStockingReportService stockingReportService )
{
	this.stockingReportService = stockingReportService;
}
}
