package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilder;
import biz.firstlook.module.purchasing.vehicle.VehicleComponentBuilderFactory;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class FlashLocateAction extends OptimalInventorySearchSummaryAction {

	/**
	 * The flash locate action always submits the markets which means this
	 * method returns null to indicate the clients of this action behave as
	 * intended.
	 * 
	 * @return null
	 */
	protected Set<MarketType> getMarketTypes() {
		return null;
	}

	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		
		// keep track of whether or not we have run a search
		boolean haveRunSearch = false;
		
		if (!mapping.getPath().contains("UpdateResults") && !theForm.isFlashLocateRunSearch()) {
			// user clicked on a Flash Locate link
			form.reset(mapping, request);
			theForm.setFlashLocateNotAjax(true);
		}
		else if (theForm.validateFlashLocate(mapping, request)) {
			//run the search if the form validates, otherwise we skip.
			super.justDoIt(mapping, form, request, response);
			haveRunSearch = true;
		}
		
		List<Make> makes = VehicleServiceFactory.getFactory().getService().getMakes();
		request.setAttribute("makes", makes);
		
		// if we have not run a search make sure we've got the search context
		// otherwise the page will not render the market places
		if (!haveRunSearch) {
			getSearchContext(request, theForm);
		}
		
		// by default render a partial page
		String forward = "success";
		
		// unless it is explicitly not an ajax request
		if (theForm.isFlashLocateNotAjax()) {
			forward = "successNotAjax";
		}
		
		Iterator<MemberMarket> ir=theForm.getSearchContext().getMarkets().iterator();
		
		while(ir.hasNext()){
			MemberMarket market= ir.next();
			if(!market.getName().equalsIgnoreCase("IN GROUP")){
				market.setSuppress(true);
			}
			else {
				market.setSuppress(false);
			}
			
		}
		
		
		

		return mapping.findForward(forward);
	}

	protected JSONObject summaryJSON(OptimalInventorySearchSummaryForm theForm) {
		try {
			JSONObject json = super.summaryJSON(theForm);
			if (json == null) {
				json = new JSONObject();
			}
			json.put("modelsSearched", theForm.getNumberOfModelsSearched());
			return json;
		} catch (JSONException npe) {
			// ignore
		}
		return null;
	}

	/**
	 * Search as my parent does, flag we've flash located and filter out any
	 * unwanted trims.
	 */
	protected void search(HttpServletRequest request, OptimalInventorySearchSummaryForm form) throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, IntrospectionException {
		super.search(request, form);

		if (form.getSelectedNode() != null && form.getSelectedNode() > 0) {
			form.setFormHasFlashLocateSearchCandidates(true);
		}

		filterTrim(form);
	}

	/**
	 * Filters out vehicles whose trim does not match the trim supplied by the
	 * form
	 */
	protected void filterTrim(OptimalInventorySearchSummaryForm theForm) {
		final String trim = theForm.getTrim();

		if (trim == null || trim.trim().length() == 0)
			return;

		for (Iterator<MarketVehicle> it = theForm.getSearchResults().iterator(); it.hasNext();) {
			MarketVehicle marketVehicle = it.next();

			// indexOf is case sensitive - so make everything uppercase
			final String upperCaseName = marketVehicle.getTrim().getName().toUpperCase();

			if (upperCaseName != null && upperCaseName.length() > 0 && upperCaseName.lastIndexOf(trim.toUpperCase()) != -1) {
				continue;
			} else {
				it.remove();
			}
		}
	}

	/**
	 * Override the parent definition of candidates to pull from the text fields
	 * on the form, not from my candidate list derived from my stores purchasing
	 * list.
	 */
	protected Collection<SearchCandidate> getSearchCandidates(HttpServletRequest request, OptimalInventorySearchSummaryForm form) {
		if (!form.getFormHasFlashLocateSearchCandidates()) {
			String modelToSearch = form.getModelText() + "%"; // wildcard the
																// model
			List<Model> models = VehicleServiceFactory.getFactory().getService().getModels(form.getMakeText(), modelToSearch);
			Collection<SearchCandidate> searchCandidates = getMarketSearchService().getCandidateList(getMember(request, form), PersistenceScope.TEMPORARY);
			MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();

			// build list of SearchCandidateCriteria (same for every model)
			List<SearchCandidateCriteria> searchCandidateCriteria = buildSearchCandidateCriteria(form, builder);

			// build list of SearchCandidates
			// the hashSet is there to ensure unique Models from the days when models were Strings
			Set<Model> uniqueModels = new HashSet<Model>(models); 
			for (Model model : uniqueModels) {
				SearchCandidate searchCandidate = builder.newSearchCandidate(model);
				for (SearchCandidateCriteria c : searchCandidateCriteria) {
					SearchCandidateCriteria newSearchCandidateCriteria = builder.newSearchCandidateCriteria(c.getModelYear(), c.getExpires());
					newSearchCandidateCriteria.getAnnotations().addAll(c.getAnnotations());
					searchCandidate.getCriteria().add(newSearchCandidateCriteria);
				}
				searchCandidates.add(searchCandidate);
			}

			// i do not believe we have to force call this as that's what #add
			// does ...
			// CollectionMutationPersistor collectionMutationPersistor =
			// (CollectionMutationPersistor)searchCandidates;
			// collectionMutationPersistor.saveOrUpdate();
			form.setTotalSearch(false);
			form.setSearchCandidates(searchCandidates);
			form.setFormHasFlashLocateSearchCandidates(true);
		}
		return form.getSearchCandidates();
	}

	/**
	 * Builds a list of SearchCandidateCriteria (basically represents the years to search)
	 */
	protected List<SearchCandidateCriteria> buildSearchCandidateCriteria(OptimalInventorySearchSummaryForm theForm, MarketSearchComponentBuilder builder) {
		List<SearchCandidateCriteria> searchCandidateCriteria = new ArrayList<SearchCandidateCriteria>();

		// a zero stands for all years
		if (theForm.getYearBegin().equals("0") || theForm.getYearEnd().equals("0")) {
			SearchCandidateCriteria allYearsCriteria = builder.newSearchCandidateCriteria(null, null);
			allYearsCriteria.getAnnotations().add(SearchCandidateCriteriaAnnotationEnum.ONETIME);
			searchCandidateCriteria.add(allYearsCriteria);
			return searchCandidateCriteria;
		}

		VehicleComponentBuilder vehicleBuilder = VehicleComponentBuilderFactory.getInstance().newVehicleBuilder();

		int minYear = Math.min(Integer.parseInt(theForm.getYearBegin()), Integer.parseInt(theForm.getYearEnd()));
		int maxYear = Math.max(Integer.parseInt(theForm.getYearBegin()), Integer.parseInt(theForm.getYearEnd()));

		for (int lowYear = minYear; lowYear < maxYear + 1; lowYear++) {
			ModelYear modelYear = vehicleBuilder.buildModelYear(lowYear);
			SearchCandidateCriteria aCriteria = builder.newSearchCandidateCriteria(modelYear, null);
			aCriteria.getAnnotations().add(SearchCandidateCriteriaAnnotationEnum.ONETIME);
			searchCandidateCriteria.add(aCriteria);
		}

		return searchCandidateCriteria;
	}

	protected SearchContext getSearchContext(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		return getSearchContext(request, theForm, PersistenceScope.TEMPORARY);
	}

}
