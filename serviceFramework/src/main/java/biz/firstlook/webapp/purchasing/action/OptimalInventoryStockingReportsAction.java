package biz.firstlook.webapp.purchasing.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.report.reportBuilders.InventoryStockingReportsBuilder;
import biz.firstlook.report.reportFramework.ColumnAppendFlattener;
import biz.firstlook.report.reportFramework.ColumnSumFlattener;
import biz.firstlook.report.reportFramework.Flattener;

public class OptimalInventoryStockingReportsAction extends AbstractStockingReportAction {
    
    @SuppressWarnings("unchecked")
    public ActionForward reportIt(ActionMapping mapping, HttpServletRequest request, Integer businessUnitId, Integer strategyId, Boolean includeLowUnitCost ) throws Exception {
        String option = request.getParameter( "option" );
        Boolean modifyStock = setStockFilter(option);
        
        InventoryStockingReportsBuilder reportBuilder = new InventoryStockingReportsBuilder(businessUnitId, strategyId, includeLowUnitCost);
        reportBuilder.selectGroupingId(0);
        List<Map> segments = reportService.getResults(reportBuilder);
        if (segments.get(0).get("TargetUnits") != null) {
            Collections.sort(segments, new ReverseComparator(new BeanComparator("TargetUnits")));
        }
        else if (segments.get(0).get("UnitsInStock") != null){
            Collections.sort(segments, new ReverseComparator(new BeanComparator("UnitsInStock")));
        }
        
        reportBuilder = new InventoryStockingReportsBuilder(businessUnitId, strategyId, includeLowUnitCost);
        reportBuilder.selectAllYears();
        reportBuilder.removeOthers();
        List<Map> modelGroups = reportService.getResults(reportBuilder);
        
        reportBuilder = new InventoryStockingReportsBuilder(businessUnitId, strategyId, includeLowUnitCost);
        reportBuilder.removeAllYears();
        if (modifyStock != null){
            reportBuilder.modifyStock(modifyStock);
        }
        List<Map> years = reportService.getResults(reportBuilder);
        
        reportBuilder = new InventoryStockingReportsBuilder(businessUnitId, strategyId, includeLowUnitCost);
        reportBuilder.selectAllYears();
        reportBuilder.selectOthers();
        if (modifyStock != null){
            reportBuilder.modifyStock(modifyStock);
        }
        List<Map> others = reportService.getResults(reportBuilder);
        
        if ( segments != null && !segments.isEmpty()) {
            request.setAttribute("totalStock", segments.get(0).get("UnitsInStock"));
            request.setAttribute("targetInv", segments.get(0).get("TargetUnits"));
            //Remove the total inventory level segment.
            segments.remove(0);
        }
        
        others = flatten(others, "VehicleSegmentID",new ColumnAppendFlattener( "VehicleGroupingID", "," ),
                new ColumnSumFlattener( "UnitsInStock"));
        segments = stockingReportService.addOthers(segments, others);
        if (modifyStock != null){
            segments = stockingReportService.filterOthers(segments, modifyStock);
        }
        List results = stockingReportService.createResults(segments, modelGroups, years, others, modifyStock);
        
        Integer daysSupply = stockingReportService.retrieveDaySupply(businessUnitId);
        
        request.setAttribute("hasBuyingPlan", stockingReportService.hasBuyingPlan(businessUnitId));
        request.setAttribute("daysSupply", daysSupply);
        request.setAttribute("option", option);
        request.setAttribute("strategyId", strategyId);
        request.setAttribute("results", results);
        return mapping.findForward("success");
    }
    
    private Boolean setStockFilter(String option){
        Boolean modifyStock = null;
        if (option != null && (option.equals("understocking") || option.equals("overstocking"))) {
            modifyStock = (option.equalsIgnoreCase("understocking")? false: true);
        }
        return modifyStock;
    }
    
    //HAX! Copied from ActionPlanReportSubmitAction.java.  Benson was right this really should be moved
    //to some Report utils.
    @SuppressWarnings("unchecked")
    private List< Map > flatten( List< Map > reportResults, String key, Flattener... flatteners )
    {
        // Ex: inventoryId - list of inventory + retail strats
        Map< Object, Map > groupedResults = new LinkedHashMap< Object, Map >();

        // group the duplicate rows by key
        for ( Map row : reportResults )
        {
            Object groupKey = row.get( key );
            if ( groupedResults.containsKey( groupKey ) )
            {
                // flattens happens here.

                // if already a row, retrieve that row
                Map existingRow = groupedResults.get( groupKey );

                for ( Flattener flattener : flatteners )
                {
                    // update the row with it's existing data and "flatten" it with the new data
                    existingRow.put( flattener.getColumn(), ( flattener.doFlatten( existingRow, row ) ) );
                }

                // put it back into the groupedResults
                groupedResults.put( groupKey, existingRow );
            }
            else
            {
                groupedResults.put( groupKey, row );
            }
        }
        return new ArrayList< Map >( groupedResults.values() );
    }
}
