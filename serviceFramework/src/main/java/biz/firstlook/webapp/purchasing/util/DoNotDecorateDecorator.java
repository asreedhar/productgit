package biz.firstlook.webapp.purchasing.util;

import java.io.Serializable;

public class DoNotDecorateDecorator implements Serializable, Decorator {

	private static final long serialVersionUID = -2734303006516509370L;

	public Object decorate(Object o) {
		return o;
	}

}
