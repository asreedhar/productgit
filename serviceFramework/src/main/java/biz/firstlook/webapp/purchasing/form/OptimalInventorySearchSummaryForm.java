package biz.firstlook.webapp.purchasing.form;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.json.JSONObject;

import biz.firstlook.commons.collections.ListProxyAdapter;
import biz.firstlook.commons.collections.MemorySensitiveList;
import biz.firstlook.commons.collections.WrappedList;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotation;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.ModifiableMemberMarket;
import biz.firstlook.module.purchasing.vehicle.Make;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.webapp.purchasing.action.SearchBaseAction;
import biz.firstlook.webapp.purchasing.util.GroupBy;
import biz.firstlook.webapp.purchasing.util.GroupByNode;
import biz.firstlook.webapp.purchasing.util.InputList;
import biz.firstlook.webapp.purchasing.util.InputSearchCandidateCriteriaImpl;

public class OptimalInventorySearchSummaryForm extends ActionForm implements Serializable {

	private static final long serialVersionUID = -5874273340547078663L;
	
	public static final List<Make> EMPTY_MAKES = Collections.emptyList();
	public static final List<Model> EMPTY_MODELS = Collections.emptyList();
	public static final List<ModelYear> EMPTY_MODEL_YEARS = Collections.emptyList();
	
	

	
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		// well. this is to stop the leaf nodes refering to the entries in the
		// search result list. as such they should become available for garbage
		// collection when their memory sensitive list is turned into a soft
		// copy. if you do not know what i've just said leave the code alone. sbw
		if (groupBySearchResults != null) {
			groupBySearchResults.setProxyReferent(searchResults);
		}
		if (groupBySearchSummary != null) {
			groupBySearchSummary.setProxyReferent(searchSummary);
		}

		if ( getSearchResults() == null ) {
			List<MarketVehicle> sr = Collections.emptyList(); 
			setSearchResults( sr );
		}
		
		// Yes, this is ugly.  Flash Locate needs to use this form because
		// it links to the Navigator which uses this form, and the Navigator
		// needs to be in synch with Flash Locate when it is opened from
		// Flash Locate.
		if ( mapping.getPath().indexOf( "FlashLocate" ) > 0 ){
			if ( !mapping.getPath().endsWith( "UpdateResults" )  && !mapping.getPath().endsWith( "Hotlist" ) ){
			    makeText = "";
			    modelText = "";
			    trim = "";
			    formHasFlashLocateSearchCandidates = false;
			}
			
			if ( mapping.getPath().indexOf( "Hotlist" ) < 0 && mapping.getPath().indexOf( "Sort" ) < 0)
			{
			    setGroupBySearchResults( null );
			    getSearchResults().clear();
			    setSearchCandidateCriteria( new InputList<SearchCandidateCriteria>(new InputSearchCandidateCriteriaImpl()) );
			}
			flashLocateNotAjax = false;
			flashLocateRunSearch = false;
		}

		// clean up search candidates from the flash locator so that other pages
		// don't see that there are already search candidates use them 
		if ( mapping.getPath().indexOf( "FlashLocate" ) == -1 && formHasFlashLocateSearchCandidates ){
			setSearchCandidates( null );
		    formHasFlashLocateSearchCandidates = false;
		}
		
		this.maxDistanceFromDealer = null;
		this.maxDistanceLiveFromDealer = null;
        this.maxMileage = null;
        this.minMileage = null;
		this.marketTypes = new HashSet<MarketType>();
		if (this.searchCandidateCriteria == null) {
			this.searchCandidateCriteria = new InputList<SearchCandidateCriteria>(new InputSearchCandidateCriteriaImpl());
		}
		else {
			this.searchCandidateCriteria.clear(); 
		}
		if (this.marketOrder == null) {
			this.marketOrder = new InputList<String>(new String());
		}
		else {
			this.marketOrder.clear(); 
		}
		if (this.selectedModels == null) {
			this.selectedModels = new InputList<String>(new String());
		}
		else {
			this.selectedModels.clear(); 
		}
		if (mapping.getPath().endsWith("UpdateResults")) {
			resetSearchResults();
		}
		
		if (mapping.getPath().indexOf( "LiveAuctionRunList" ) > 0) {
			this.searchSummaryIndex = null;
		}
		
		if (getSearchContext() != null) {
			resetSearchContext(getSearchContext());
		}
		this.permanentSearchContext = null;
		this.sortByColumn = null;
		this.sortByOrder  = null;
		this.actionForwardSuffix = "";
		this.unmodifiableMemberMarket = Boolean.FALSE;
		this.selectedNode   = null;
		this.ignoreMarketSuppression = Boolean.FALSE;
		this.timePeriod = null;
		this.exactMatches = null;
		this.jsonObject = null;
		this.suppressSearch = Boolean.FALSE;
		this.haveProcessedSearchContext = Boolean.FALSE;
		this.memberSearchHomePageSelection = null;
	}

	@SuppressWarnings("unchecked")
	public void resetSearchContext(SearchContext theSearchContext) {
		List<MemberMarket> markets = theSearchContext.getMarkets();
		while (markets instanceof WrappedList) {
			markets = ((WrappedList<MemberMarket>) markets).getList();
		}
		final MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		setSearchContext(builder.newContext(theSearchContext, new ModifiableListProxyAdapter(markets)));
	}

	class ModifiableListProxyAdapter extends ListProxyAdapter<MemberMarket,MemberMarket> implements List<MemberMarket>, WrappedList<MemberMarket> {
		public ModifiableListProxyAdapter(List<MemberMarket> list) {
			super(list);
		}
		public MemberMarket adaptItem(MemberMarket memberMarket) {
			return new ModifiableMemberMarket(memberMarket);
		}
	}
	
	public void reset() {
		this.make = null;
		this.model = null;
		this.makes = EMPTY_MAKES;
		this.models = EMPTY_MODELS;
		this.modelYears = EMPTY_MODEL_YEARS;
		this.member = null;
		this.searchCandidates = null;
		this.searchContext = null;
		this.permanentSearchContext = null;
		resetSearchResults();
		this.searchSummary = null;
	}

	private Integer make;
	private Integer model;
	
	private List<Make> makes = EMPTY_MAKES;
	private List<Model> models = EMPTY_MODELS;
	private List<ModelYear> modelYears = EMPTY_MODEL_YEARS;
	
	public Integer getMake() {
		return make;
	}

	public void setMake(Integer make) {
		this.make = make;
	}

	public Integer getModel() {
		return model;
	}

	public void setModel(Integer model) {
		this.model = model;
	}

	public List<Make> getMakes() {
		return makes;
	}

	public void setMakes(List<Make> makes) {
		this.makes = makes;
	}

	public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}

	public List<ModelYear> getModelYears() {
		return modelYears;
	}

	public void setModelYears(List<ModelYear> modelYears) {
		this.modelYears = modelYears;
	} 

	private Member member;
	private SearchContext searchContext;
	private SearchContext permanentSearchContext;
	private Collection<SearchCandidate> searchCandidates;
	private List<MarketSummary> searchSummary;
	private List<MarketVehicle> searchResults = new ArrayList<MarketVehicle>();
	private GroupBy groupBySearchSummary;
	private GroupBy groupBySearchResults;
	private GroupByNode groupByNodeIncrement;
	private Integer searchSummaryIndex;
	private Integer flashLocatorHideUnitCostDays;

	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}

	public Collection<SearchCandidate> getSearchCandidates() {
		return searchCandidates;
	}
	public void setSearchCandidates(Collection<SearchCandidate> searchCandidates) {
		this.searchCandidates = searchCandidates;
	}

	public SearchContext getSearchContext() {
		return searchContext;
	}
	public void setSearchContext(SearchContext searchContext) {
		this.searchContext = searchContext;
	}
	public String getCurrentMarketSuppression() {
		StringBuffer sb = new StringBuffer();
		sb.append("persistenceScopeString=").append(getSearchContext().getPersistenceScope());
		int index = -1;
		for (MemberMarket market : getSearchContext().getMarkets()) {
			++index;
			sb.append("&searchContext.markets[").append(index).append("].suppress=").append(market.getSuppress());
		}
		return sb.toString();
	}

	public SearchContext getPermanentSearchContext() {
		return permanentSearchContext;
	}

	public void setPermanentSearchContext(SearchContext permanentSearchContext) {
		this.permanentSearchContext = permanentSearchContext;
	}

	public List<MarketSummary> getSearchSummary() {
		return searchSummary;
	}
	public void setSearchSummary(List<MarketSummary> searchSummary) {
		this.searchSummary = searchSummary;
	}

	public List<MarketVehicle> getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(List<MarketVehicle> searchResults) {
		this.searchResults = searchResults;
	}
	private void resetSearchResults() {
		if (searchResults != null && searchResults instanceof MemorySensitiveList<?>) {
			SearchBaseAction.pool.release((MemorySensitiveList<MarketVehicle>)searchResults);
		}
		searchResults = new ArrayList<MarketVehicle>();
	}

	public GroupBy getGroupBySearchSummary() {
		return groupBySearchSummary;
	}
	public void setGroupBySearchSummary(GroupBy groupBySearchSummary) {
		this.groupBySearchSummary = groupBySearchSummary;
	}

	public GroupBy getGroupBySearchResults() {
		return groupBySearchResults;
	}
	public void setGroupBySearchResults(GroupBy groupBySearchResults) {
		this.groupBySearchResults = groupBySearchResults;
	}

	public GroupByNode getGroupByNodeIncrement() {
		return groupByNodeIncrement;
	}
	public void setGroupByNodeIncrement(GroupByNode groupByNodeIncrement) {
		this.groupByNodeIncrement = groupByNodeIncrement;
	}

	public boolean getGroupByNodeIncrementIsMarketVehicle() {
		return (groupByNodeIncrement.getNodeValue() instanceof MarketVehicle);
	}
	
	public Integer getSearchSummaryIndex() {
		return searchSummaryIndex;
	}
	public void setSearchSummaryIndex(Integer saleID) {
		this.searchSummaryIndex = saleID;
	}
	
	public Integer getFlashLocatorHideUnitCostDays() {
		return flashLocatorHideUnitCostDays;
	}
	public void setFlashLocatorHideUnitCostDays(Integer flashLocatorHideUnitCostDays) {
		this.flashLocatorHideUnitCostDays = flashLocatorHideUnitCostDays;
	}

	public GroupByNode getGroupBySearchSummary(MarketType marketType) {
		for (GroupByNode n : getGroupBySearchSummary().getGroupByNode().getChildNodes()) {
			if (marketType.equals(n.getNodeValue())) {
				return n;
			}
		}
		return null;
	}

	public GroupByNode getGroupBySearchSummaryInGroup() {
		return getGroupBySearchSummary(MarketType.IN_GROUP);
	}
	
	public GroupByNode getGroupBySearchSummaryOnline() {
		return getGroupBySearchSummary(MarketType.ONLINE);
	}
	
	public GroupByNode getGroupBySearchSummaryLive() {
		return getGroupBySearchSummary(MarketType.LIVE);
	}
	
	public int getNumberOfHotListSearchCandidates() {
		return totalModelGroupCriteria(SearchCandidateCriteriaAnnotationEnum.HOTLIST); 
	}
	
	public int getNumberOfOptimalSearchCandidates() {
		return totalModelGroupCriteria(SearchCandidateCriteriaAnnotationEnum.OPTIMAL);
	}

	private int totalModelGroupCriteria(final SearchCandidateCriteriaAnnotation annotation) {
		final Collection<SearchCandidate> searchCandidates = getSearchCandidates();
		final Map<ModelGroup,Integer> modelGroupCounts = new HashMap<ModelGroup,Integer>();
		for (SearchCandidate searchCandidate : searchCandidates) {
			final ModelGroup modelGroup = searchCandidate.getModel().getModelGroup();
			Integer count = modelGroupCounts.get(modelGroup);
			if (count == null) {
				count = 0;
				for (SearchCandidateCriteria searchCandidateCriteria : searchCandidate.getCriteria()) {
					if (searchCandidateCriteria.getAnnotations().contains(annotation)) {
						count += 1;
					}
				}
				modelGroupCounts.put(modelGroup, count);
			}
		}
		int total = 0;
		for (Integer subtotal : modelGroupCounts.values()) {
			total += subtotal;
		}
		return total;
	}
		
	private JSONObject jsonObject;
	private Integer dealerOrgID = 0;
	private Integer maxDistanceFromDealer;
    private Integer maxDistanceLiveFromDealer;
	private Integer timePeriod;
	private Boolean exactMatches;
	private Set<MarketType> marketTypes;
	private List<String> marketOrder;
	private String actionForwardSuffix;
	private String memberSearchHomePageSelection;
	private Boolean ignoreMarketSuppression = Boolean.FALSE;
	private Boolean suppressSearch = Boolean.FALSE;
	private Boolean haveProcessedSearchContext = Boolean.FALSE;
    private Integer maxMileage;
    private Integer minMileage;
    private PersistenceScope persistenceScope;
    private boolean isTotalSearch;
    

    
	public boolean isTotalSearch() {
		return isTotalSearch;
	}

	public void setTotalSearch(boolean isTotalSearch) {
		this.isTotalSearch = isTotalSearch;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public Integer getMaxDistanceFromDealer() {
		return maxDistanceFromDealer;
	}
	public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
		this.maxDistanceFromDealer = maxDistanceFromDealer;
	}
	
	public Integer getDealerOrgID() 
	{
		return this.dealerOrgID;
	}
	
	public void setDealerOrgID(Integer dealerID) 
	{
		//set this only if this is the first time.
		if (dealerOrgID == 0)
		{
			this.dealerOrgID = dealerID;
		}
	}
	
	public void forceSetDealerOrgID(Integer dealerID)
	{
		this.dealerOrgID = dealerID;
	}
	

	public Integer getMaxDistanceLiveFromDealer() {
        return maxDistanceLiveFromDealer;
    }

    public void setMaxDistanceLiveFromDealer(Integer maxDistanceLiveFromDealer) {
        this.maxDistanceLiveFromDealer = maxDistanceLiveFromDealer;
    }
    
    public Integer getMaxMileage() {
        return maxMileage;
    }

    public void setMaxMileage(Integer maxMileage) {
        this.maxMileage = maxMileage;
    }
     
    public Integer getMinMileage() {
        return minMileage;
    }

    public void setMinMileage(Integer minMileage) {
       this.minMileage = minMileage;
    }

    public Integer getTimePeriod() {
		return timePeriod;
	}
	public void setTimePeriod(Integer timePeriod) {
		this.timePeriod = timePeriod;
	}

	public Boolean getExactMatches() {
		return exactMatches;
	}
	public void setExactMatches(Boolean exactMatches) {
		this.exactMatches = exactMatches;
	}

	public Integer getMaxDistanceFromDealer(MarketType marketType) {
		for (MemberMarket m : getSearchContext().getMarkets()) {
			if (m.getMarketType().equals(marketType)) {
				return m.getMaxDistanceFromDealer();
			}
		}
		return 100;
	}

	public Integer getMaxDistanceFromDealerInGroup() {
		return getMaxDistanceFromDealer(MarketType.IN_GROUP);
	}
	
	public Integer getMaxDistanceFromDealerOnline() {
		return getMaxDistanceFromDealer(MarketType.ONLINE);
	}
	
	public Integer getMaxDistanceFromDealerLive() {
        return getMaxDistanceFromDealer(MarketType.LIVE);
	}
    
    public Integer getMaxMileage(MarketType marketType) {
        for (MemberMarket m : getSearchContext().getMarkets()) {
            if (m.getMarketType().equals(marketType)) {
                if (m.getMaxMileage() != null) {
                    return m.getMaxMileage();
                }
            }
        }
        return 999999;
    }

    public Integer getMinMileageInGroup() {
        return getMinMileage(MarketType.IN_GROUP);
    }
    
    public Integer getMinMileage(MarketType marketType) {
        for (MemberMarket m : getSearchContext().getMarkets()) {
            if (m.getMarketType().equals(marketType)) {
                if(m.getMinMileage() != null) {
                    return m.getMinMileage();
                }
            }
        }
        return 0;
    }

    public Integer getMaxMileageInGroup() {
        return getMaxMileage(MarketType.IN_GROUP);
    }
	
	public Integer getTimePeriod(MarketType marketType) {
		for (MemberMarket m : getSearchContext().getMarkets()) {
			if (m.getMarketType().equals(marketType)) {
				return m.getTimePeriod();
			}
		}
		return 1;
	}
	
	public Integer getTimePeriodInGroup() {
		return getTimePeriod(MarketType.IN_GROUP);
	}
	
	public Integer getTimePeriodOnline() {
		return getTimePeriod(MarketType.ONLINE);
	}
	
	public Integer getTimePeriodLive() {
		return getTimePeriod(MarketType.LIVE);
	}

	public boolean getExactMatches(MarketType marketType) {
		for (MemberMarket m : getSearchContext().getMarkets()) {
			if (m.getMarketType().equals(marketType)) {
				return m.getExactMatches();
			}
		}
		return true;
	}
	
	public boolean getExactMatchesInGroup() {
		return getExactMatches(MarketType.IN_GROUP);
	}
	
	public boolean getExactMatchesOnline() {
		return getExactMatches(MarketType.ONLINE);
	}
	
	public boolean getExactMatchesLive() {
		return getExactMatches(MarketType.LIVE);
	}
	
	public Set<MarketType> getMarketTypes() {
		return marketTypes;
	}
	public void setMarketTypes(Set<MarketType> marketTypes) {
		this.marketTypes = marketTypes;
	}
	
	public Collection<String> getMarketTypesString() {
		return Collections.emptyList();
	}
	public void setMarketTypesString(int idx, String marketType) {
		if (marketType.equals("IN_GROUP")) {
			this.marketTypes.add(MarketType.IN_GROUP);
		}
		if (marketType.equals("LIVE")) {
			this.marketTypes.add(MarketType.LIVE);
		}
		if (marketType.equals("ONLINE")) {
			this.marketTypes.add(MarketType.ONLINE);
		}
		if (marketType.equals("ALL")) {
			this.marketTypes.add(MarketType.ONLINE);
			this.marketTypes.add(MarketType.IN_GROUP);
		}
	}
	
	public PersistenceScope getPersistenceScope() {
		return persistenceScope;
	}
	public void setPersistenceScope(PersistenceScope persistenceScope) {
		this.persistenceScope = persistenceScope;
	}

	public String getPersistenceScopeString() {
		return String.valueOf(persistenceScope);
	}
	public void setPersistenceScopeString(String persistenceScopeString) {
		this.persistenceScope = PersistenceScope.valueOf(persistenceScopeString);
	}

	public List<String> getMarketOrder() {
		return marketOrder;
	}
	public void setMarketOrder(List<String> marketOrder) {
		this.marketOrder = marketOrder;
	}

	public String getMemberSearchHomePageSelection() {
		return memberSearchHomePageSelection;
	}
	public void setMemberSearchHomePageSelection(String memberSearchHomePageSelection) {
		this.memberSearchHomePageSelection = memberSearchHomePageSelection;
	}

	public Boolean getIgnoreMarketSuppression() {
		return ignoreMarketSuppression;
	}

	public void setIgnoreMarketSuppression(Boolean ignoreMarketSuppression) {
		this.ignoreMarketSuppression = ignoreMarketSuppression;
	}

	public Boolean getHaveProcessedSearchContext() {
		return haveProcessedSearchContext;
	}

	public void setHaveProcessedSearchContext(Boolean haveProcessedSearchContext) {
		this.haveProcessedSearchContext = haveProcessedSearchContext;
	}

	public String getActionForwardSuffix() {
		return actionForwardSuffix;
	}
	public void setActionForwardSuffix(String actionForwardSuffix) {
		this.actionForwardSuffix = actionForwardSuffix;
	}
	
	private String  sortByColumn;
	private String  sortByOrder;
	private Integer selectedNode;
	
	public String getSortByColumn() {
		return sortByColumn;
	}
	public void setSortByColumn(String sortByColumn) {
		this.sortByColumn = sortByColumn;
	}

	public String getSortByOrder() {
		return sortByOrder;
	}
	public void setSortByOrder(String sortByOrder) {
		this.sortByOrder = sortByOrder;
	}

	public Integer getSelectedNode() {
		return selectedNode;
	}
	public void setSelectedNode(Integer sortByNode) {
		this.selectedNode = sortByNode;
	}

	private Integer selectedModelGroup;

	public Integer getSelectedModelGroup() {
		return selectedModelGroup;
	}
	public void setSelectedModelGroup(Integer selectedModelGroup) {
		this.selectedModelGroup = selectedModelGroup;
	}
	
	private List<SearchCandidateCriteria> searchCandidateCriteria;
	private List<String> selectedModels;

	public List<SearchCandidateCriteria> getSearchCandidateCriteria() {
		return searchCandidateCriteria;
	}
	public void setSearchCandidateCriteria(List<SearchCandidateCriteria> searchCandidateCriteria) {
		this.searchCandidateCriteria = searchCandidateCriteria;
	}

	public List<String> getSelectedModels() {
		return selectedModels;
	}

	public void setSelectedModels(List<String> selectedModels) {
		this.selectedModels = selectedModels;
	}

	private GroupByNode navigatorYearGrouping;

	public GroupByNode getNavigatorYearGrouping()
	{
		return navigatorYearGrouping;
	}

	public void setNavigatorYearGrouping( GroupByNode navigatorYearGrouping )
	{
		this.navigatorYearGrouping = navigatorYearGrouping;
	}
	
	private Integer navigatorVehicleCount;

	public Integer getNavigatorVehicleCount()
	{
		return navigatorVehicleCount;
	}

	public void setNavigatorVehicleCount( Integer navigatorVehicleCount )
	{
		this.navigatorVehicleCount = navigatorVehicleCount;
	}
	
	private List<GroupByNode> navigatorYearNodes;

	public List<GroupByNode> getNavigatorYearNodes()
	{
		return navigatorYearNodes;
	}

	public void setNavigatorYearNodes( List<GroupByNode> navigatorYearNodes )
	{
		this.navigatorYearNodes = navigatorYearNodes;
	}
	
    private ModelYear currentModelYear;

	public ModelYear getCurrentModelYear()
	{
		return currentModelYear;
	}

	public void setCurrentModelYear( ModelYear currentModelYear )
	{
		this.currentModelYear = currentModelYear;
	}
	
	private Integer nodeId;

	public Integer getNodeId()
	{
		return nodeId;
	}

	public void setNodeId( Integer yearNodeId )
	{
		this.nodeId = yearNodeId;
	}
	
	private GroupByNode selectedVehicleGroup;

	public GroupByNode getSelectedVehicleGroup()
	{
		return selectedVehicleGroup;
	}

	public void setSelectedVehicleGroup( GroupByNode selectedVehicleGroup )
	{
		this.selectedVehicleGroup = selectedVehicleGroup;
	}
	
    /**	 
     * navigator marketplace index is the position of the initially
	 * selected marketplace in the marketplace dropdown
	 */
	private Integer navigatorMarketplaceIndex;

	public Integer getNavigatorMarketplaceIndex()
	{
		return navigatorMarketplaceIndex;
	}

	public void setNavigatorMarketplaceIndex( Integer navigatorMarketplace )
	{
		this.navigatorMarketplaceIndex = navigatorMarketplace;
	}
	
	private GroupByNode navigatorSelectedModel;

	public GroupByNode getNavigatorSelectedModel()
	{
		return navigatorSelectedModel;
	}

	public void setNavigatorSelectedModel( GroupByNode selectedModel )
	{
		this.navigatorSelectedModel = selectedModel;
	}
	
	private Boolean unmodifiableMemberMarket = Boolean.FALSE;

	public Boolean getUnmodifiableMemberMarket() {
		return unmodifiableMemberMarket;
	}
	public void setUnmodifiableMemberMarket(Boolean doNotUpdateMemberMarket) {
		this.unmodifiableMemberMarket = doNotUpdateMemberMarket;
	}
	
	private Integer navigatorDistance;
	
	public Integer getNavigatorDistance()
	{
		return navigatorDistance;
	}

	public void setNavigatorDistance( Integer navigatorDistance )
	{
		this.navigatorDistance = navigatorDistance;
	}
	
	/**
	 * Convienence method for deriving the selected market name
	 * from the list of markets and the selected market position
	 * @return
	 */
	public String getNavigatorSelectedMarketName()
	{
		return getNavigatorMarkets().get( getNavigatorMarketplaceIndex() ).getName();	
	}

	/**
	 * Convienence method for deriving the selected market
	 * from the list of markets
	 * @return
	 */
	public MemberMarket getNavigatorSelectedMarket()
	{
	    return getNavigatorMarkets().get( getNavigatorMarketplaceIndex() );	
	}
	
	private List<MemberMarket> navigatorMarkets;
	
	public List<MemberMarket> getNavigatorMarkets()
	{
	    return navigatorMarkets;
	}
	
	public void setNavigatorMarkets( List<MemberMarket> navigatorMarkets )
	{
		this.navigatorMarkets = navigatorMarkets;
	}
	
	private boolean navigatorHasBidList;

	public boolean getNavigatorHasBidList()
	{
		return navigatorHasBidList;
	}

	public void setNavigatorHasBidList( boolean navigatorHasBidList )
	{
		this.navigatorHasBidList = navigatorHasBidList;
	}

	public Boolean getSuppressSearch()
	{
		return suppressSearch;
	}

	public void setSuppressSearch( Boolean suppressSearch )
	{
		this.suppressSearch = suppressSearch;
	}
	
	// *************** 
	// fields for the flash locator page;  The flash locator's fields are 
	// on this form because the flash locator has to link to the Navigator
	// page and the Navigator page uses this form.  This is a big candidate
	// for refactoring!!!!
	// ***************
	private String makeText;
	private String modelText;
	private String trim;
	private String yearBegin;
	private String yearEnd;
	private String selectedMarket;
	private boolean formHasFlashLocateSearchCandidates;
	private boolean flashLocateNotAjax;
	private boolean flashLocateRunSearch;
	final static String IN_GROUP = "IN_GROUP";
	final static String ONLINE = "ONLINE";
	final static String ALL = "ALL";
	
	/* Begin: Not used??? -bf August 7, 2009 */
	//extension to flash locate, search by vin or stock number in-group only.
	//private String vin;
	//private String stockNumber;
	/* End: Not used??? -bf August 7, 2009 */

	public String getMakeText()
	{
		return makeText;
	}
	
	public void setMakeText( String makeText )
	{
		this.makeText = makeText;
	}

	public String getModelText()
	{
		String decoded;
		try {
			decoded = URLDecoder.decode(modelText, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			decoded = modelText;
		}
		return decoded;
	}

	public void setModelText( String modelText )
	{
		this.modelText = modelText;
	}

	public String getSelectedMarket()
	{
		return selectedMarket;
	}

	public void setSelectedMarket( String selectedMarket )
	{
		this.selectedMarket = selectedMarket;

		if ( selectedMarket.equals( IN_GROUP ) || selectedMarket.equals( ONLINE ) )
		{
			setMarketTypesString( 0, selectedMarket );
		}
		else if ( selectedMarket.equals( ALL ) )
		{
			setMarketTypesString( 0, IN_GROUP );
			setMarketTypesString( 1, ONLINE );
		}
		else
		{
			// default to all
			setSelectedMarket( ALL );
			setMarketTypesString( 0, IN_GROUP );
			setMarketTypesString( 1, ONLINE );
		}
	}

	public String getTrim()
	{
		return trim;
	}

	public void setTrim( String trim )
	{
		this.trim = trim;
	}

	public String getYearBegin()
	{
		return yearBegin;
	}

	public void setYearBegin( String yearBegin )
	{
		this.yearBegin = yearBegin;
	}

	public String getYearEnd()
	{
		return yearEnd;
	}

	public void setYearEnd( String yearEnd )
	{
		this.yearEnd = yearEnd;
	}
	
	/**
	 * seems like we need to do validation here, rather
	 * than using Struts validation because
	 * we have one form that is used for two different
	 * UIs (the home page and the flash locate page)
	 * 
	 * @param mapping
	 * @param request
	 * @return
	 */
	public boolean validateFlashLocate(ActionMapping mapping, HttpServletRequest request)
	{
		boolean result = false;
		if (makeText != null && cleanText(makeText).length() > 0) {
			result = true;
		} else {
			request.setAttribute("invalidMake", true);
		}
		
		if(modelText != null && cleanText(modelText).length() > 0) {
			result &= true;
		} else {
			result = false;
			request.setAttribute("invalidModel", true);
		}
		
		return result;
	}
	
	/**
	 * Strip out all non-word characters, where a word character is [a-zA-Z_0-9].
	 * @param text
	 * @return
	 */
	private static String cleanText(String text) {
		if(text != null) {
			String clean = text.trim();
			try {
				return URLDecoder.decode(clean, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				Pattern p = Pattern.compile("\\W");
				Matcher m = p.matcher(clean);
				return m.replaceAll("");
			}
		}
		return text;
	}

	public boolean getFormHasFlashLocateSearchCandidates()
	{
		return formHasFlashLocateSearchCandidates;
	}

	public void setFormHasFlashLocateSearchCandidates(boolean formHasFlashLocateSearchCandidates)
	{
		this.formHasFlashLocateSearchCandidates = formHasFlashLocateSearchCandidates;
	}	
	
	public boolean isFlashLocateNotAjax()
	{
		return flashLocateNotAjax;
	}
	
	public void setFlashLocateNotAjax( boolean flashLocateFromDealerHomepage )
	{
		this.flashLocateNotAjax = flashLocateFromDealerHomepage;
	}

	public boolean isFlashLocateRunSearch()
	{
		return flashLocateRunSearch;
	}
	
	public void setFlashLocateRunSearch( boolean flashLocateNoSearch )
	{
		this.flashLocateRunSearch = flashLocateNoSearch;
	}

	/**
	 * Convenience method for getting the number of models searched
	 * for the flash locator
	 * 
	 * @return int the number of models searched
	 */
	public int getNumberOfModelsSearched()
	{
		int numberOfModelsSearched = 0;

		if ( getSearchCandidates() != null && isFlashLocateRunSearch() )
		{
		    for( Object searchCandidate : getSearchCandidates() )
		    {
		        if ( searchCandidate == null )
		        	break;
		        numberOfModelsSearched++;
		    }
		}
		
		return numberOfModelsSearched;
	}
}