package biz.firstlook.webapp.purchasing.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupByNode;

/**
 * Pulls up search results for a model for the current marketplace.
 * 
 * @author kkelly
 * 
 */
public class NavigatorChangeModelAction extends ViewNavigatorAction
{

@Override
public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm)form;
	GroupByNode currentYearNode = null;

	// get new model's node
	GroupByNode currentModelNode = theForm.getGroupBySearchResults().get( theForm.getNodeId() );
	
	// get the currently selected market
	List<GroupByNode> modelMarkets = currentModelNode.getChildNodes();
	GroupByNode currentMarketNode = null;
	for ( GroupByNode marketNode : modelMarkets ) 
	{		
		if ( marketNode.getNodeValue().equals( theForm.getNavigatorSelectedMarket() ) )
		{
		    currentMarketNode = marketNode;
		    break;
		}
	}
	
	if ( !currentMarketNode.getChildNodes().isEmpty() )
	{
		// default to the first year
		currentYearNode = currentMarketNode.getChildNodes().get( 0 );
        
	}
	else
	{
		theForm.setNavigatorYearNodes( new ArrayList<GroupByNode>() );
	}

	handleCounts( currentYearNode, theForm );

	theForm.setNavigatorYearGrouping( currentYearNode );
	theForm.setNavigatorSelectedModel( currentModelNode );
    
    //logNavigatorClickEvent( theForm, response );
	//the above line produces double logging - no time this bugfix round to fix.
	setDefaultVehicleOnResponse( theForm.getNavigatorYearGrouping(), theForm.getNodeId(), response );
	request.setAttribute( "isNavigator", "true" );

	request.setAttribute("marketName", theForm.getNavigatorMarkets().get(theForm.getNavigatorMarketplaceIndex()).getName());
	
	return mapping.findForward( "success" );
}

}
