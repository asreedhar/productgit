package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import biz.firstlook.commons.collections.Filter;
import biz.firstlook.commons.collections.FilteredList;
import biz.firstlook.commons.collections.ListAdapter;
import biz.firstlook.commons.collections.ListProxyAdapter;
import biz.firstlook.commons.collections.MemorySensitiveList;
import biz.firstlook.commons.collections.MemorySensitiveListPool;
import biz.firstlook.commons.collections.WrappedList;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.actionFilter.NextGenSession;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;
import biz.firstlook.module.market.search.MarketSearchService;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.MemberMarketWrapper;
import biz.firstlook.module.market.search.impl.ModifiableMemberMarket;
import biz.firstlook.module.market.search.impl.UnmodifiableMemberMarket;
import biz.firstlook.module.market.search.impl.UnsuppressedMemberMarket;
import biz.firstlook.module.market.search.presentation.MarketSearchPresentationService;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public abstract class SearchBaseAction extends NextGenAction {

	private static final Logger logger = Logger.getLogger(SearchBaseAction.class);

	public static final MemorySensitiveListPool<MarketVehicle> pool = new MemorySensitiveListPool<MarketVehicle>(
			"MarketVehicle",
			PropertyLoader.getIntProperty(
					"firstlook.memorysensitivelistpool.capacity.marketvehicle",
					50));
	
	private CoreService coreService;

	private MarketSearchService marketSearchService;
	
	private MarketSearchPresentationService marketSearchPresentationService;
	
	public SearchBaseAction() {
		super();
	}

	protected void summary(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		theForm.setSearchSummary(getSearchSummary(request, theForm));
	}

	protected void search(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm)
			throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, IntrospectionException {
		List<MarketVehicle> r = getSearchResults(request, theForm);
		try {
			r = pool.newInstance(r);
		}
		catch (IOException e) {
			logger.error(e);
		}
		finally {
			List<MarketVehicle> x = theForm.getSearchResults();
			if (x != null && x instanceof MemorySensitiveList) {
				pool.release((MemorySensitiveList<MarketVehicle>)x);
			}
			theForm.setSearchResults(r);
		}
	}

	protected List<MarketSummary> getSearchSummary(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		SearchContext ctx = getSearchContext(request, theForm);
		List<MarketSummary> summary = null;
		if (!theForm.getMarketTypes().isEmpty()) {
			summary = new ArrayList<MarketSummary>();
			if (theForm.getSearchSummary() != null) {
				for (MarketSummary ms : theForm.getSearchSummary()) {
					if (!theForm.getMarketTypes().contains(ms.getMarketType())) {
						summary.add(ms);
					}
				}
			}
			summary.addAll(getMarketSearchService().summary(ctx, getSearchCandidates(request, theForm),theForm.isTotalSearch()));
		}
		else {
			summary = getMarketSearchService().summary(ctx, getSearchCandidates(request, theForm),theForm.isTotalSearch());
		}
		return summary;
	}

	protected List<MarketVehicle> getSearchResults(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		return getSearchResults(
				theForm,
				getSearchContext(request, theForm),
				getSearchCandidates(request, theForm));
	}

	/**
	 * Returns a list of MarketVehicles.  Does not set them onto the form as searchResults.
	 * @param theForm
	 * @param ctx
	 * @param searchCandidates
	 * @return
	 */
	protected List<MarketVehicle> getSearchResults(OptimalInventorySearchSummaryForm theForm, SearchContext ctx, Collection<SearchCandidate> searchCandidates) {
		long ts0 = System.currentTimeMillis();
		List<MarketVehicle> results = null;
		if (!theForm.getMarketTypes().isEmpty()) {
			results = new ArrayList<MarketVehicle>();
			if (theForm.getSearchResults() != null) {
				for (MarketVehicle mv : theForm.getSearchResults()) {
					if (!theForm.getMarketTypes().contains(mv.getMarketType())) {
						results.add(mv);
					}
				}
			}
			results.addAll(getMarketSearchService().search(ctx, searchCandidates,theForm.isTotalSearch()));
		}
		else {
			results = getMarketSearchService().search(ctx, searchCandidates,theForm.isTotalSearch());
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getSearchResults: " + (System.currentTimeMillis()-ts0) + "ms");
		}
		return results;
	}

	protected SearchContext getSearchContext(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		return getSearchContext(request, theForm, PersistenceScope.TODAY);
	}

	protected SearchContext getSearchContext(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm, PersistenceScope scope) {
		long ts0 = System.currentTimeMillis();
		final MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		// take a cached search-context or get one from the service method
		SearchContext ctx = theForm.getSearchContext();
		if (ctx == null || ctx.getPersistenceScope() != scope || scope == PersistenceScope.PERMANENT) {
			ctx = getMarketSearchService().getContext(getMember(request, theForm), scope);
			theForm.setHaveProcessedSearchContext(Boolean.FALSE);
			theForm.setTotalSearch(true);
		}
		else{
			theForm.setTotalSearch(false);
		}
		// short circuit if have been here before
		if (theForm.getHaveProcessedSearchContext()) {
			return ctx;
		}
		// unwrap markets to modifiable (or until there is no more unwrapping)
		if (ctx.getMarkets() instanceof WrappedList) {
			ctx = builder.newContext(ctx, unwrapMemberMarketList(ctx.getMarkets()));
		}
		// commit updates to modifiable's if permitted
		final Boolean unmodifiableMemberMarket = theForm.getUnmodifiableMemberMarket();
		final Boolean ignoreMarketSuppression  = theForm.getIgnoreMarketSuppression();
		if (ctx.getMarkets() instanceof ListAdapter) {
			ListAdapter<MemberMarket,MemberMarket> markets = (ListAdapter<MemberMarket,MemberMarket>) ctx.getMarkets();
			if (!unmodifiableMemberMarket) {
				for (MemberMarket memberMarket : markets) {
					commitModifiableMemberMarket(memberMarket);
				}
				if (!ignoreMarketSuppression) {
					// not unmodifiable nor ignoring suppression so expose "real" market list
					ctx = builder.newContext(ctx, markets.getList());
				}
			}
		}
		// wrap unmodifiable un-adapted market lists
		if (unmodifiableMemberMarket) {
			ctx = builder.newContext(ctx, new UnmodifiableListProxyAdapter(ctx.getMarkets()));
		}
		// wrap to ignore-suppression
		if (ignoreMarketSuppression) {
			ctx = builder.newContext(ctx, new UnsuppressedListProxyAdapter(ctx.getMarkets()));
		}
		// sub-list of markets?
		final Set<MarketType> marketTypes = theForm.getMarketTypes();
		if (!marketTypes.isEmpty()) {
			for (MemberMarket m : ctx.getMarkets()) {
				if (marketTypes.contains(m.getMarketType())) {
					if (theForm.getMaxDistanceFromDealer() != null) {
						m.setMaxDistanceFromDealer(theForm.getMaxDistanceFromDealer());
					}
                    if ( m.getMarketType()== MarketType.LIVE && theForm.getMaxDistanceFromDealerLive() != null) {
                        m.setMaxDistanceFromDealer(theForm.getMaxDistanceFromDealerLive());
                    }
					if (theForm.getTimePeriod() != null) {
						m.setTimePeriod(theForm.getTimePeriod());
					}
					if (theForm.getExactMatches() != null) {
						m.setExactMatches(theForm.getExactMatches());
					}
                    if (theForm.getMaxMileage() != null) {
                        m.setMaxMileage(theForm.getMaxMileage());
                    }
                    if (theForm.getMinMileage() != null) {
                        m.setMinMileage(theForm.getMinMileage());
                    }
					commitModifiableMemberMarket(unwrapMemberMarket(m));
				}
			}
			List<MemberMarket> markets = new FilteredList<MemberMarket>(
					ctx.getMarkets(),
					new Filter<MemberMarket>() {
						public boolean accept(MemberMarket memberMarket) {
							return marketTypes.contains(memberMarket.getMarketType());
					}});
			ctx = builder.newContext(ctx, markets);
		}
		// put the search context on the page
		if (scope != PersistenceScope.PERMANENT) {
			theForm.setSearchContext(ctx);
		}
		theForm.setHaveProcessedSearchContext(Boolean.TRUE);
		logger.debug("getSearchContext: " + (System.currentTimeMillis()-ts0) + "ms");
		return ctx;
	}

	private void commitModifiableMemberMarket(MemberMarket m) {
		if (m instanceof UnmodifiableMemberMarket) {
			return;
		}
		if (m instanceof ModifiableMemberMarket) {
			((ModifiableMemberMarket) m).commit();
		}
	}

	@SuppressWarnings("unchecked")
	protected List<MemberMarket> unwrapMemberMarketList(List<MemberMarket> markets) {
		UNWRAP: while (markets instanceof WrappedList) {
			if (markets instanceof ListAdapter) {
				for (MemberMarket memberMarket : markets) {
					if (memberMarket instanceof ModifiableMemberMarket) {
						break UNWRAP;
					}
					break;
				}
			}
			markets = ((WrappedList<MemberMarket>) markets).getList();
		}
		return markets;
	}

	protected MemberMarket unwrapMemberMarket(MemberMarket memberMarket) {
		UNWRAP: while (memberMarket instanceof MemberMarketWrapper) {
			if (memberMarket instanceof UnmodifiableMemberMarket) {
				break UNWRAP;
			}
			if (memberMarket instanceof ModifiableMemberMarket) {
				break UNWRAP;
			}
			memberMarket = ((MemberMarketWrapper) memberMarket).getMemberMarket();
		}
		return memberMarket;
	}
	
	class UnmodifiableListProxyAdapter extends ListProxyAdapter<MemberMarket,MemberMarket> implements List<MemberMarket>, WrappedList<MemberMarket> {
		public UnmodifiableListProxyAdapter(List<MemberMarket> list) {
			super(list);
		}
		public MemberMarket adaptItem(MemberMarket memberMarket) {
			return new UnmodifiableMemberMarket(memberMarket);
		}
	}
	
	class UnsuppressedListProxyAdapter extends ListProxyAdapter<MemberMarket,MemberMarket> implements List<MemberMarket>, WrappedList<MemberMarket> {
		public UnsuppressedListProxyAdapter(List<MemberMarket> list) {
			super(list);
		}
		public MemberMarket adaptItem(MemberMarket memberMarket) {
			return new UnsuppressedMemberMarket(memberMarket);
		}
	}
	
	/**
	 * Finds the search candidates and sets them onto the form.
	 * @param request
	 * @param theForm
	 * @return
	 */
	
	
	protected Collection<SearchCandidate> getSearchCandidates
		(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) 
	{
		long start = System.currentTimeMillis();
		//get the dealer id the form was created with and the current dealer id
		Integer orgFormDealerID = theForm.getDealerOrgID();
		Integer currentDealerID = getBusinessUnitIdFromCookie(request);
		
		Collection<SearchCandidate> candidates = theForm.getSearchCandidates();		
		
		if (candidates == null || !orgFormDealerID.equals(currentDealerID)) 
		{
			candidates = getMarketSearchService().getCandidateList(
					getMember(request, theForm), PersistenceScope.TODAY);
			theForm.setSearchCandidates(candidates);
			theForm.forceSetDealerOrgID(currentDealerID);
			theForm.setTotalSearch(true);
		}
		else{
			theForm.setTotalSearch(false);
		}
		logger.debug( "getSearchCandidates: " + (System.currentTimeMillis() - start) + "ms");
		return candidates;
	}

	protected Member getMember(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		Member member = theForm.getMember();
		final Integer businessUnitId = getNextGenSession(request).getBusinessUnitId();
		// special check for admin like users that can come in and out as diff business units
		if (member == null || (!member.getCurrentBusinessUnit().getBusinessUnitID().equals(businessUnitId))) {
			member = getCoreService().findMemberByID(getNextGenSession(request).getMemberId());
			setCurrentBusinessUnitOnMember(member, businessUnitId);
			theForm.reset();
			theForm.setMember(member);
		} 
		if (theForm.getFlashLocatorHideUnitCostDays() == null) {
			NextGenSession nextGenSession = (NextGenSession)request.getSession().getAttribute(NextGenSession.NEXT_GEN_SESSION);
			theForm.setFlashLocatorHideUnitCostDays(nextGenSession.getFlashLocatorHideUnitCostDays());
		}
		return member;
	}

	protected void setCurrentBusinessUnitOnMember(Member member, final Integer businessUnitId) {
		for (BusinessUnit businessUnit : member.getBusinessUnits()) {
			if (businessUnit.getBusinessUnitID().equals(businessUnitId)) {
				member.setCurrentBusinessUnit(businessUnit);
				break;
			}
		}
		if (member.getCurrentBusinessUnit() == null) {
			logger.warn("The member does not belong to business unit: "
					+ businessUnitId);
			member.setCurrentBusinessUnit(getCoreService().findBusinessUnitByID(businessUnitId));
		}
	}
	
	protected void initForm(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		SearchContext searchContext = theForm.getPermanentSearchContext();
		if(searchContext == null) {
			searchContext = getSearchContext(request, theForm, PersistenceScope.PERMANENT);
		}
		for (MemberMarket m : searchContext.getMarkets()) {
			switch (m.getMarketType()) {
				case IN_GROUP:
					theForm.setMaxDistanceFromDealer(m.getMaxDistanceFromDealer());
	                theForm.setMaxMileage(m.getMaxMileage());
	                theForm.setMinMileage(m.getMinMileage());	
	                break;
				case LIVE:
					theForm.setMaxDistanceLiveFromDealer(m.getMaxDistanceFromDealer());
					break;
				default:
					break;
			}
		}
		// max distance from dealer
        if (theForm.getMaxMileage() == null ) {
            theForm.setMaxMileage(999999);
        }
        if (theForm.getFlashLocatorHideUnitCostDays() == null) {
			NextGenSession nextGenSession = (NextGenSession)request.getSession().getAttribute(NextGenSession.NEXT_GEN_SESSION);
			theForm.setFlashLocatorHideUnitCostDays(nextGenSession.getFlashLocatorHideUnitCostDays());
		}
        //used for member profile.
		theForm.setPermanentSearchContext(searchContext);		
	}

	public CoreService getCoreService() {
		return coreService;
	}

	public void setCoreService(CoreService coreService) {
		this.coreService = coreService;
	}

	public MarketSearchService getMarketSearchService() {
		return marketSearchService;
	}

	public void setMarketSearchService(MarketSearchService marketSearchService) {
		this.marketSearchService = marketSearchService;
	}

	public MarketSearchPresentationService getMarketSearchPresentationService() {
		return marketSearchPresentationService;
	}
	public void setMarketSearchPresentationService(MarketSearchPresentationService marketSearchPresentationService) {
		this.marketSearchPresentationService = marketSearchPresentationService;
	}

}
