/**
  * $Id: ReflectionComparator.java,v 1.5 2007/10/12 16:52:44 nkeen Exp $
  */

package biz.firstlook.webapp.purchasing.util;

import java.beans.IntrospectionException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.logging.LogFactory;

/**
  *	Implementation of the Comparator interface which is driven by reflection
  *	and three arrays. The argument arrays passed to the constructor as follows.
  *	<ol>
  *	<li>a two-dimensional array where the first index represents the primary sort order
  *	    (e.g. sort by headline) and the values of the second index are JavaBean properties
  *	    whose values are the subject of comparison.</li>
  *	<li>a two-dimensional array where the first index represents the primary sort order
  *	    (e.g. sort by headline) and the values of the second index are 1 or -1 (ASC or DESC)
  *	    to give the sort order direction.</li>
  *	<li>a one-dimensional array whose values are the names of the sort orders (the first index
  *	    of the last two items.</li>
  *	</ol>
  *	<p>
  *	Due to the nature of the constructor arguments i expect comparators to extend from this object
  *	as passing multi-dimensional arrays in a Properties instance is not particularly nice.
  *	
  *	@author Simon Wenmouth
  *	@version $Revision: 1.5 $
  */
@SuppressWarnings("serial")
public class ReflectionComparator implements Serializable, Comparator
{
	protected String[][] beanProperties;
	protected int[][]    beanPropertiesOrder;
	protected String[]   sortOrderNames;
	protected OrderBy    ordering;
	protected int        expression;

	/**
	  *	@param sortOrderNames a one-dimensional array of sort order names
	  *	@param beanProperties a two-dimensional array of strings the first index being the
	  *		sort order and the second the bean properties whose values the comparison is
	  *		made upon
	  *	@param beanPropertiesOrder a two-dimensional array of ints whose first index is the
	  *		sort order and the second index the ASC or DESC for the corresponding bean
	  *		property
	  */
	public ReflectionComparator(String[] sortOrderNames, String[][] beanProperties, int[][] beanPropertiesOrder) {
		this.sortOrderNames      = sortOrderNames;
		this.beanProperties      = beanProperties;
		this.beanPropertiesOrder = beanPropertiesOrder;
	}

	public ReflectionComparator initialise(String expr, OrderBy order) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		return initialise(lookup(expr), order);
	}

	public ReflectionComparator initialise(int expr, OrderBy order) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		this.expression = (expr >= 0 && expr < beanProperties.length) ? expr : 0;
		this.ordering   = (order == OrderBy.ASC) ? OrderBy.ASC : OrderBy.DESC;
		return this;
	}

	public OrderBy ordering() {
		return this.ordering;
	}

	public String expression() {
		return lookup(this.expression);
	}

	public void reverse() {
		this.ordering = (this.ordering == OrderBy.ASC) ? OrderBy.DESC : OrderBy.ASC;
	}

	@SuppressWarnings("unchecked")
	public int compare(Object o1, Object o2) {
		if (o1 == null && o2 != null) {
			return 1;
		}
		if (o1 != null && o2 == null) {
			return -1;
		}
		if (o1 == null && o2 == null) {
			return 0;
		}
		int i = 0, j = 0;
		try {
			BeanMethodCache cache1 = BeanMethodCache.getInstance(o1.getClass());
			BeanMethodCache cache2 = BeanMethodCache.getInstance(o2.getClass());

			for (; i < beanProperties[expression].length && j == 0; i++) {

				Comparable c1 = (Comparable) cache1.get(o1, beanProperties[expression][i]);
				Comparable c2 = (Comparable) cache2.get(o2, beanProperties[expression][i]);

				if (c1 == null && c2 != null) {
					j = 1;
				}
				else if (c1 != null && c2 == null) {
					j = -1;
				}
				else if (c1 != null && c2 != null) {
					if (c1 instanceof String && c2 instanceof String) {
						j = ((String)c1).compareToIgnoreCase((String)c2);
					}
					else {
						j = c1.compareTo(c2);
					}
				}

				j *= beanPropertiesOrder[expression][i] * ordering.intValue();
			}
		}
		catch (IllegalAccessException e) {
			LogFactory.getLog(ReflectionComparator.class).warn(e.getMessage() + " caused by " + e.getCause());
		}
		catch (InvocationTargetException e) {
			LogFactory.getLog(ReflectionComparator.class).warn(e.getMessage() + " caused by " + e.getCause());
		}
		catch (IntrospectionException e) {
			LogFactory.getLog(ReflectionComparator.class).warn(e.getMessage() + " caused by " + e.getCause());
		}
		return j;
	}

	public boolean equals(Object obj) {
		ReflectionComparator cmp = (ReflectionComparator) obj;
		return (compareTo(this.expression, cmp.expression) == 0)
			? true
			: ((compareTo(this.ordering.intValue(), cmp.ordering.intValue()) == 0) ? true : false);
	}

	public int lookup(String sortOrderName) {
		int expr = 0;
		for (int i = 0; i < sortOrderNames.length; i++) {
			if (sortOrderNames[i].equals(sortOrderName)) {
				expr = i;
				break;
			}
		}
		return expr;
	}

	public String lookup(int sortOrderId) {
		return (sortOrderId >= 0 && sortOrderId < sortOrderNames.length)
			? sortOrderNames[sortOrderId]
			: sortOrderNames[0];
	}

	private int compareTo(int i1, int i2) {
		return (i1 == i2) ? 0 : ((i1 > i2) ? 1 : -1);
	}

}

