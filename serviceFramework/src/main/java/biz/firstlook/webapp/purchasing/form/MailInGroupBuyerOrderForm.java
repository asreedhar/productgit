package biz.firstlook.webapp.purchasing.form;

import org.apache.struts.action.*;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

public class MailInGroupBuyerOrderForm extends ActionForm implements Serializable {

	private static final long serialVersionUID = 4569674030664924647L; //for Serializable
	
	private String date;
	private String sellingDealerName;
	private String contactName;
	private String contactEmail;
	private String buyer;
	private String  senderName;
	private String senderEmailAddress;
	private String transferPrice;
	private String vehicle;
	private String vin;
	private String stockNumber;
	private int    mileage;
	private String color;
	private boolean  sendToSelf;
	private boolean  immediatePickup;
	private boolean  includeInspectionBook;
	private boolean  includeExtraKeys;
	private boolean  includeNavigationDisc;
	private String ccEmailAddress;
	private int inventoryId;
	private String emailBuyerNotes;
	
	public void setDate(String _date) {
		this.date = _date;
	}
	public String getDate() {
		return date;
	}
	
	public void setSellingDealerName(String _sellingDealerName) {
		this.sellingDealerName = _sellingDealerName;
	}
	public String getSellingDealerName() {
		return sellingDealerName;
	}
	
	public void setContactEmail(String _contactEmail) {
		this.contactEmail = _contactEmail;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	
	public void setBuyer(String _buyer) {
		this.buyer = _buyer;
	}
	public String getBuyer() {
		return buyer;
	}
	
	public void setSenderName(String _senderName) {
		this.senderName = _senderName;
	}
	public String getSenderName() {
		return senderName;
	}
	
	public void setTransferPrice(String _transferPrice) {
		this.transferPrice = _transferPrice;
	}
	public String getTransferPrice() {
		return transferPrice;
	}
	
	public void setVehicle(String _vehicle) {
		this.vehicle = _vehicle;
	}
	public String getVehicle() {
		return vehicle;
	}
	
	public void setVin(String _vin) {
		this.vin = _vin;
	}
	public String getVin() {
		return vin;
	}
	
	public void setStockNumber(String _stockNumber) {
		this.stockNumber = _stockNumber;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	
	public void setMileage(int _Mileage) {
		this.mileage = _Mileage;
	}
	public int getMileage() {
		return mileage;
	}
	
	public void setColor(String _color) {
		this.color = _color;
	}
	public String getColor() {
		return color;
	}
	
	public void setContactName(String _contactName)	{
		this.contactName = _contactName;
	}
	public String getContactName() {
		return contactName;
	}
	
	public void setSendToSelf(boolean _sendToSelf) {
		this.sendToSelf = _sendToSelf;
	}
	public boolean getSendToSelf() {
		return sendToSelf;
	}
	
	public boolean isImmediatePickup() {
		return immediatePickup;
	}
	public void setImmediatePickup(boolean immediatePickup) {
		this.immediatePickup = immediatePickup;
	}
	
	public boolean isIncludeInspectionBook() {
		return includeInspectionBook;
	}
	public void setIncludeInspectionBook(boolean includeInspectionBook) {
		this.includeInspectionBook = includeInspectionBook;
	}
	
	public boolean isIncludeExtraKeys() {
		return includeExtraKeys;
	}
	public void setIncludeExtraKeys(boolean includeExtraKeys) {
		this.includeExtraKeys = includeExtraKeys;
	}
	
	public boolean isIncludeNavigationDisc() {
		return includeNavigationDisc;
	}
	public void setIncludeNavigationDisc(boolean includeNavigationDisc) {
		this.includeNavigationDisc = includeNavigationDisc;
	}
	
	public void setCcEmailAddress(String _ccEmailAddress) {
		this.ccEmailAddress = _ccEmailAddress;
	}
	public String getCcEmailAddress() {
		return ccEmailAddress;
	}	

	public void setEmailBuyerNotes(String _emailBuyerNotes) {
		this.emailBuyerNotes = _emailBuyerNotes;
	}
	public String getEmailBuyerNotes() {
		return emailBuyerNotes;
	}	
	
	public void setSenderEmailAddress(String _senderEmailAddress) {
		this.senderEmailAddress = _senderEmailAddress;
	}
	public String getSenderEmailAddress() {
		return senderEmailAddress;
	}
	
	public void setInventoryId(int _inventoryId) {
		this.inventoryId = _inventoryId;
	}
	public int getInventoryId() {
		return inventoryId;
	}
	
	//Behavior
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		date = null;
		sellingDealerName = null;
		contactName = null;
		buyer = null;
		senderName = null;
		transferPrice = null;
		vehicle = null;
		vin = null;
		stockNumber = null;
		mileage = 0;
		color = null;
		sendToSelf = false;
		immediatePickup = false;
		includeExtraKeys = false;
		includeInspectionBook = false;
		includeNavigationDisc = false;
		senderEmailAddress = null;
		inventoryId = 0;
		emailBuyerNotes = null;
		ccEmailAddress = null;
	}
}
