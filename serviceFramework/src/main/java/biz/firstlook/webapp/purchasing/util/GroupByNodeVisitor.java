package biz.firstlook.webapp.purchasing.util;

public interface GroupByNodeVisitor {
	public void visit(GroupByNode node);
	
	public GroupByNode getNode();
}
