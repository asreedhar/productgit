package biz.firstlook.webapp.purchasing.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.report.reportBuilders.InventoryStockingReportsBuilder;
import biz.firstlook.report.reportBuilders.OptimalInventorySummaryReportBuilder;

public class StockingReportOtherOverviewAction extends AbstractStockingReportAction {

    public ActionForward reportIt(ActionMapping mapping, HttpServletRequest request, Integer businessUnitId, Integer strategyId, Boolean includeLowUnitCost) throws Exception {
        Integer segmentId = 0;
        if (request.getParameter("segmentId") != null) {
            segmentId = Integer.parseInt(request.getParameter("segmentId"));
        }
        
        InventoryStockingReportsBuilder othersBuilder = new InventoryStockingReportsBuilder(businessUnitId, strategyId, includeLowUnitCost);
        othersBuilder.selectAllYears();
        othersBuilder.selectOthers();
        othersBuilder.selectVehicleSegment(segmentId);
        
        List<String> groupingIds = stockingReportService.retrieveGroupingIds(reportService.getResults(othersBuilder)); 
        OptimalInventorySummaryReportBuilder inventoryDetail = new OptimalInventorySummaryReportBuilder(businessUnitId, null, null, includeLowUnitCost);
        inventoryDetail.selectMultiple(groupingIds);
        List results = reportService.getResults(inventoryDetail);
        
        request.setAttribute("results", results);
        return mapping.findForward("success");
    }
}
