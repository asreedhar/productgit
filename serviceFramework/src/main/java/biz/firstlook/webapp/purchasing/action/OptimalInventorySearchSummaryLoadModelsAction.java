package biz.firstlook.webapp.purchasing.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.service.VehicleServiceFactory;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;

public class OptimalInventorySearchSummaryLoadModelsAction extends NextGenAction {

	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		if (theForm.getMake() == null || theForm.getMakes().isEmpty()) {
			return mapping.findForward("failure");
		}
		theForm.setModelYears(OptimalInventorySearchSummaryForm.EMPTY_MODEL_YEARS);
		theForm.setModels(VehicleServiceFactory.getFactory().getService().getModels(theForm.getMakes().get(theForm.getMake())));
		response.addHeader("X-JSON", JSONAdapter(theForm.getModels()));
		return mapping.findForward("success");
	}

	protected String JSONAdapter(List<Model> models) {
		JSONObject json = new JSONObject();
		JSONArray jsonModels = new JSONArray();
		for (Model m : models) {
			try {
				JSONObject jsonModel = new JSONObject();
				jsonModel.put("make", m.getMake().getName());
				jsonModel.put("line", m.getLine().getName());
				jsonModel.put("segmentID", m.getSegment().getSegmentID());
				jsonModels.put(jsonModel);
			}
			catch (JSONException npe) {
				// is a null pointer exception if name is null
			}
		}
		return json.toString();
	}
	
}
