package biz.firstlook.webapp.purchasing.util;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketAdministrator;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.impl.AbstractMarket;

public class MarketDecorator implements Serializable, Decorator {

	private static final long serialVersionUID = -3310684189939494370L;
	
	private List<MemberMarket> memberMarkets;
	
	public MarketDecorator(List<MemberMarket> memberMarkets) {
		this.memberMarkets = memberMarkets;
	}

	public Object decorate(Object o) {
		if (o instanceof Market) {
			Market m = (Market) o;
			for (MemberMarket mm : memberMarkets) {
				if (m.getName().equals(mm.getName()) && m.getMarketType().equals(mm.getMarketType())) {
					return new DecoratedMarket(m, mm);
				}
			}
			Logger.getLogger(getClass()).warn("No match for: " + m.getName());
		}
		return o;
	}

	public class DecoratedMarket extends AbstractMarket implements Serializable, Market, MemberMarket {
		private static final long serialVersionUID = 4893930420077783856L;
		Market market;
		MemberMarket memberMarket;
		public DecoratedMarket(Market market, MemberMarket memberMarket) {
			this.market = market;
			this.memberMarket = memberMarket;
		}
		public MarketType getMarketType() {
			return market.getMarketType();
		}
		public MarketAdministrator getMarketAdministrator() {
			return market.getMarketAdministrator();
		}
		public String getName() {
			return market.getName();
		}
		public Integer getMaxDistanceFromDealer() {
			return memberMarket.getMaxDistanceFromDealer();
		}
		public Integer getTimePeriod() {
			return memberMarket.getTimePeriod();
		}
		public boolean getExactMatches() {
			return memberMarket.getExactMatches();
		}
		public boolean getSuppress() {
			return memberMarket.getSuppress();
		}
        public Integer getMaxMileage() {
            return memberMarket.getMaxMileage();
        }
        public Integer getMinMileage() {
            return memberMarket.getMinMileage();
        }
		public void setMaxDistanceFromDealer(Integer maxDistanceFromDealer) {
			// ignore
		}
		public void setTimePeriod(Integer timePeriod) {
			// ignore
		}
		public void setSuppress(boolean suppress) {
			// ignore
		}
		public void setExactMatches(boolean exactMatches) {
			// ignore
		}
        public void setMaxMileage(Integer maxMileage) {
            // ignore
        }
        public void setMinMileage(Integer minMileage) {
            // ignore
        }
		public int compareTo(MemberMarket otherMemberMarket) {
			if (otherMemberMarket instanceof DecoratedMarket) {
				return Functions.nullSafeCompareTo(memberMarket, ((DecoratedMarket)otherMemberMarket).memberMarket);
			}
			return Functions.nullSafeCompareTo(memberMarket, otherMemberMarket);
		}
	}
}
