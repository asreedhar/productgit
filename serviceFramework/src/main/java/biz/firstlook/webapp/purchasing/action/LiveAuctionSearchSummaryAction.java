package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.OrderBy;
import biz.firstlook.webapp.purchasing.util.ReflectionComparator;

public class LiveAuctionSearchSummaryAction extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		if (theForm.getSortByColumn() == null) {
			summary(request, theForm);
		}
		sort(theForm);
		return mapping.findForward("success");
	}

	@SuppressWarnings("unchecked")
	protected void sort(OptimalInventorySearchSummaryForm theForm) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		String sortByColumn = theForm.getSortByColumn();
		String sortByOrder = theForm.getSortByOrder();	
		if (sortByColumn == null) {
			sortByColumn = "Score";
		}
		OrderBy orderBy = OrderBy.ASC;
		if (sortByOrder == null || sortByOrder.equals("DESC")) {
			orderBy = OrderBy.DESC;
		}
		Collections.sort(
				theForm.getSearchSummary(),
				new ReflectionComparator(
						CMP_NAMES_SUMMARY,
						CMP_PROPERTIES_SUMMARY,
						CMP_ORDER_SUMMARY).initialise(sortByColumn, orderBy));
	}

	static final String[][] CMP_PROPERTIES_SUMMARY = new String[][] {
		{ "title", "vehiclesMatched", "auctionName", "location", "auctionDate", "distance", "score" },
		{ "vehiclesMatched", "title", "auctionName", "location", "auctionDate", "distance", "score" },
		{ "auctionName", "title", "vehiclesMatched", "location", "auctionDate", "distance", "score" },
		{ "location", "title", "vehiclesMatched", "auctionName", "auctionDate", "distance", "score" },
		{ "auctionDate", "title", "vehiclesMatched", "auctionName", "location", "distance", "score" },
		{ "distance", "title", "vehiclesMatched", "auctionName", "location", "auctionDate", "score" },
		{ "score", "auctionName", "title", "vehiclesMatched", "location", "auctionDate", "distance" }
	};
	static final String[] CMP_NAMES_SUMMARY = new String[] {
		"Seller",
		"Matches",
		"Auction",
		"Location",
		"Date",
		"Distance",
		"Score"
	};
	static final int[][] CMP_ORDER_SUMMARY = new int[][] {
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, -1, 1, 1, 1, 1, 1 },
	};
	
}
