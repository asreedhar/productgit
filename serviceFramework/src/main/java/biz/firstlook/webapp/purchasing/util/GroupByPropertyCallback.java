package biz.firstlook.webapp.purchasing.util;

public interface GroupByPropertyCallback<O,P> {
	public P getProperty(O obj);
}
