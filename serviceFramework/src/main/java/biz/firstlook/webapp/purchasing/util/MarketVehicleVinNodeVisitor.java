package biz.firstlook.webapp.purchasing.util;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.module.market.search.MarketVehicle;

public class MarketVehicleVinNodeVisitor extends
		AbstractMarketVehicleNodeVisitor {

	private final String vin;
	
	public MarketVehicleVinNodeVisitor(String vin) {
		if(StringUtils.isBlank(vin)) {
			throw new IllegalArgumentException("Searching for a null or whitespace stock number is not permitted!");
		}
		this.vin = vin;
	}
	
	@Override
	protected boolean isItemFound(MarketVehicle marketVehicle) {
		if(marketVehicle == null) {
			return false;
		}
		return vin.equalsIgnoreCase(marketVehicle.getVIN());
	}

}
