package biz.firstlook.webapp.purchasing.util;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.HashMap;
import java.util.Map;

public class InputList<I> extends AbstractList<I> implements Serializable {

	private static final long serialVersionUID = 5324535555177358044L;
	
	Map<Integer,I> map;
	I i;
	
	public InputList(I i) {
		super();
		this.map = new HashMap<Integer,I>();
		this.i = i;
	}

	@SuppressWarnings("unchecked")
	public I get(int index) {
		I element = map.get(index);
		if (element == null) {
			try {
				element = (I)i.getClass().newInstance();
				map.put(index, element);
			}
			catch (InstantiationException e) {
				e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return element;
	}

	public I set(int index, I element) {
		return map.put(index, element);
	}

	public int size() {
		return map.size();
	}

	public void clear() {
		map.clear();
	}

}
