package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.ModelGroupFunctions;
import biz.firstlook.module.market.search.PersistenceScope;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchCandidateAnnotationEnum;
import biz.firstlook.module.market.search.SearchCandidateCriteria;
import biz.firstlook.module.market.search.SearchCandidateCriteriaAnnotationEnum;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.module.market.search.impl.adapter.MarketToMemberMarketAdapter;
import biz.firstlook.module.market.search.presentation.DecoratedModelGroup;
import biz.firstlook.module.purchasing.vehicle.Model;
import biz.firstlook.module.purchasing.vehicle.ModelGroup;
import biz.firstlook.module.purchasing.vehicle.ModelYear;
import biz.firstlook.module.purchasing.vehicle.Vehicle;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.Decorator;
import biz.firstlook.webapp.purchasing.util.DereferenceReflectionComparator;
import biz.firstlook.webapp.purchasing.util.DoNotDecorateDecorator;
import biz.firstlook.webapp.purchasing.util.GroupBy;
import biz.firstlook.webapp.purchasing.util.GroupByNode;
import biz.firstlook.webapp.purchasing.util.GroupByPropertyCallback;
import biz.firstlook.webapp.purchasing.util.MarketDecorator;
import biz.firstlook.webapp.purchasing.util.OrderBy;

public class OptimalInventorySearchSummaryAction extends SearchBaseAction {
	
	/**
	 * A static collection of the market types the OptimalInventorySearchSummaryAction
	 * will search and display.
	 * @see #getMarketTypes()
	 */
	private static final transient Set<MarketType> MARKET_TYPES;
	static {
		Set<MarketType> tmp = new HashSet<MarketType>();
		tmp.add(MarketType.IN_GROUP);
		tmp.add(MarketType.ONLINE);
		MARKET_TYPES = Collections.unmodifiableSet(tmp);
	}
	
	/**
	 * Every link into the action should have passed in the market types. However, this did
	 * not happen (rushed out the door). So we have had to fudge it by hard-coding the
	 * values depending on the action. The OISS page only does in-group and online market
	 * types so this method passes a set containing those two constants.
	 * @return set of market types for the action
	 */
	protected Set<MarketType> getMarketTypes() {
		return MARKET_TYPES;
	}
	
	
	
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		
		//set the original deal id - if the form already has one it will not be set.
		theForm.setDealerOrgID(getBusinessUnitIdFromCookie(request));		
		
		if (getMarketTypes() != null) {
			theForm.setMarketTypes(getMarketTypes());
		}
        
		String market = request.getParameter("market");
        if (market != null) {
            MarketType type = (market.equalsIgnoreCase("group") ? MarketType.IN_GROUP: MarketType.ONLINE);
            if (theForm.getSearchContext() == null) {
                theForm.setSearchContext(getMarketSearchService().getContext(getMember(request, theForm), PersistenceScope.TODAY));
            }
            filterResults(theForm, type);
        }

		if (!theForm.getSuppressSearch()) {
			if (theForm.getSortByColumn() == null) {
				search(request, theForm);
				group(request, theForm);
				empty(request, theForm);
			}
			else {
				theForm.setGroupByNodeIncrement(theForm.getGroupBySearchResults().get(theForm.getSelectedNode()));
			}
			sort(request, theForm);
			if (theForm.getSortByColumn() == null) {
				tidy(theForm);
			}
		}
		
		if (theForm.getJsonObject() == null) {
			theForm.setJsonObject(summaryJSON(theForm));
		}
		if (theForm.getJsonObject() != null) {
			response.addHeader("X-JSON", theForm.getJsonObject().toString());
		}
		
		request.setAttribute("showNavigator", getNextGenSession(request).getHasNavigator());
		request.setAttribute("hasTransferPricing", getNextGenSession(request).isIncludeTransferPricing());
		
		return mapping.findForward("success");
	}

    protected void search(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IntrospectionException {
		if (theForm.getSelectedNode() != null && theForm.getSelectedNode() > 0) {
			GroupBy searchResults = theForm.getGroupBySearchResults();
			GroupByNode selectedResult = searchResults.get(theForm.getSelectedNode());
			if (selectedResult.getNodeValue() instanceof DecoratedModelGroup) {
				DecoratedModelGroup decoratedModelGroup = (DecoratedModelGroup) selectedResult.getNodeValue();
				final List<? extends Model> models = decoratedModelGroup.getModels();
				final Set<SearchCandidateCriteria> userSearchCandidateCriteria = ModelGroupFunctions.validate(theForm.getSearchCandidateCriteria());
				final Set<ModelYear> modelYears = ModelGroupFunctions.getModelYears(userSearchCandidateCriteria);
				final Collection<SearchCandidate> searchCandidates = decoratedModelGroup.getSearchCandidates();
				// new candidates(models) with .annotations = all current annotations
				List<SearchCandidate> newSearchCandidates = ModelGroupFunctions.newSearchCandidates(
						Functions.subList(models, Functions.parseInt(theForm.getSelectedModels())),
						ModelGroupFunctions.getSearchCandidateAnnotations(searchCandidates, SearchCandidateAnnotationEnum.ALL));
				// criteria for group = CIA criteria + USER criteria
				Set<SearchCandidateCriteria> newSearchCandidateCriteria = ModelGroupFunctions.union(
						ModelGroupFunctions.getSearchCandidateCriteria(searchCandidates, modelYears, SearchCandidateCriteriaAnnotationEnum.CIA),
						userSearchCandidateCriteria);
				// update new candidates with new criteria
				ModelGroupFunctions.setSearchCandidateCriteria(newSearchCandidates, newSearchCandidateCriteria);
				// get rid of the old candidates and add the new ones
				Collection<SearchCandidate> allSearchCandidates = getSearchCandidates(request, theForm);
				allSearchCandidates.removeAll(searchCandidates);
				allSearchCandidates.addAll(newSearchCandidates);
				// remember modelgroup for tidy
				theForm.setSelectedModelGroup(decoratedModelGroup.getModelGroupID());
			}
		}
		super.search(request, theForm);
	}

	@SuppressWarnings("unchecked")
	protected void group(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws NoSuchMethodException {
		final Collection<SearchCandidate> searchCandidates = getSearchCandidates(request, theForm);
		final SearchContext ctx = getSearchContext(request, theForm);
		final List<DecoratedModelGroup> decoratedModelGroups = getMarketSearchPresentationService().getDecoratedModelGroups(ctx, searchCandidates);
		Decorator doNotDecorate = new DoNotDecorateDecorator();
		GroupBy groupByResults = new GroupBy(
				new GroupByPropertyCallback[] {
						new ModelGroupCallback(),
						new MemberMarketCallback(ctx.getMaxDistanceFromBusinessUnit(), ctx.getTimePeriod(), ctx.getExactMatches(), ctx.getMaxMileage(), ctx.getMinMileage()),
						new ModelYearCallback()
				},
				new Decorator[] {
					doNotDecorate,
					new MarketDecorator(theForm.getSearchContext().getMarkets()),
					doNotDecorate,
					doNotDecorate,
					doNotDecorate
				},
				theForm.getSearchResults().size());
		GroupByNode root = groupByResults.getGroupByNode();
		for (DecoratedModelGroup decoratedModelGroup : decoratedModelGroups) {
			root.appendChild(groupByResults.createNode(root, decoratedModelGroup));
		}
		groupByResults.group(theForm.getSearchResults());
		theForm.setGroupBySearchResults(groupByResults);
	}

	class ModelGroupCallback implements Serializable, GroupByPropertyCallback<Vehicle,ModelGroup> {
		private static final long serialVersionUID = -1788725683107216691L;
		public ModelGroup getProperty(Vehicle vehicle) {
			return vehicle.getModel().getModelGroup();
		}
	}
	
	class MemberMarketCallback implements Serializable, GroupByPropertyCallback<MarketVehicle,MemberMarket> {
		private static final long serialVersionUID = -2877537927563534988L;
		Integer maxDistanceFromDealer;
		Integer timePeriod;
		Boolean exactMatches;
        Integer maxMileage;
        Integer minMileage;
		public MemberMarketCallback(Integer maxDistanceFromDealer, Integer timePeriod, Boolean exactMatches, Integer maxMileage, Integer minMileage) {
			this.maxDistanceFromDealer = maxDistanceFromDealer;
			this.timePeriod = timePeriod;
			this.exactMatches = exactMatches;
            this.maxMileage = maxMileage;
            this.minMileage = minMileage;
		}
		public MemberMarket getProperty(MarketVehicle marketVehicle) {
			return new MarketToMemberMarketAdapter(marketVehicle, maxDistanceFromDealer, timePeriod, exactMatches, maxMileage, minMileage);
		}
	}

	class ModelYearCallback implements Serializable, GroupByPropertyCallback<Vehicle,ModelYear> {
		private static final long serialVersionUID = -8982024350205847031L;
		public ModelYear getProperty(Vehicle vehicle) {
			return vehicle.getModelYear();
		}
	}
	
	protected void sort(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		String sortByColumn = theForm.getSortByColumn();
		if (sortByColumn == null) {
			sortByColumn = "Trim";
		}
		String sortByOrder = theForm.getSortByOrder();
		OrderBy orderBy = OrderBy.ASC;
		if (sortByOrder == null || sortByOrder.equals("DESC")) {
			orderBy = OrderBy.DESC;
		}
		theForm.getGroupBySearchResults().sort(new Comparator[] {
				new ModelGroupComparator(),
				new MarketComparator(),
				new DereferenceReflectionComparator(
						"nodeValue",
						new String[] { "ModelYear" },
						new String[][] { { "modelYear" } },
						new int[][] { { 1 } }).initialise("ModelYear", OrderBy.DESC),
				new DereferenceReflectionComparator(
						"nodeValue",
						CMP_NAMES_VEHICLE,
						CMP_PROPERTIES_VEHICLE,
						CMP_ORDER_VEHICLE).initialise(sortByColumn, orderBy) },
				theForm.getSelectedNode());
	}

	protected void empty(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		// cache document and root node
		GroupBy groupBy = theForm.getGroupBySearchResults();
		GroupByNode root = groupBy.getGroupByNode();
		// create default children for empty top level nodes
		MarketDecorator marketDecorator = new MarketDecorator(theForm.getSearchContext().getMarkets());
		List<MemberMarket> markets = getSearchContext(request, theForm).getMarkets();
		List<GroupByNode> marketNodes = new ArrayList<GroupByNode>();
		for (MemberMarket market : markets) {
			if (!market.getSuppress()) {
				marketNodes.add(groupBy.createNode(root, marketDecorator.decorate(market)));
			}
		}
		// add empty market nodes
		SC: for (SearchCandidate searchCandidate : theForm.getSearchCandidates()) {
			final ModelGroup modelGroup = searchCandidate.getModel().getModelGroup();
			for (GroupByNode n : root.getChildNodes()) {
				if (n.getNodeValue().equals(modelGroup)) {
					// add empty markets
					MM: for (GroupByNode marketNode : marketNodes) {
						for (GroupByNode c : n.getChildNodes()) {
							if (c.getNodeValue().equals(marketNode.getNodeValue())) {
								continue MM;
							}
						}
						n.appendChild(marketNode);
					}
					continue SC;
				}
			}
			Logger.getLogger(getClass()).warn("Missing ModelGroup: " + modelGroup.getName());
		}
	}
	
	protected void tidy(OptimalInventorySearchSummaryForm theForm) {
		if (theForm.getSelectedNode() != null && theForm.getSelectedNode() > 0) {
			theForm.setGroupByNodeIncrement(
					getModelGroupNode(
							theForm.getSelectedModelGroup(),
							theForm.getGroupBySearchResults()));
		}
	}

    private void filterResults(OptimalInventorySearchSummaryForm theForm, MarketType type) {
        for (MemberMarket m: theForm.getSearchContext().getMarkets()) {
            if (m.getMarketType().equals(type)) {
                m.setSuppress(false);
            }
            else {
                m.setSuppress(true);
            }
        }
    }
    
	protected GroupByNode getModelGroupNode(Integer modelGroupID, GroupBy groupBy) {
		for (GroupByNode n : groupBy.getGroupByNode().getChildNodes()) {
			if (n.getNodeValue() instanceof ModelGroup) {
				ModelGroup modelGroup = (ModelGroup) n.getNodeValue();
				if (modelGroup.getModelGroupID().equals(modelGroupID)) {
					return n;
				}
			}
		}
		return null;
	}
	
	protected JSONObject summaryJSON(OptimalInventorySearchSummaryForm theForm) {
		try {
			JSONObject json = new JSONObject();
			json.put("HotListItems", theForm.getNumberOfHotListSearchCandidates());
			json.put("OptimalItems", theForm.getNumberOfOptimalSearchCandidates());
			return json;
		}
		catch (JSONException npe) {
			// ignore
		}
		return null;
	}
	
	class ModelGroupComparator implements Comparator<GroupByNode> {
		public int compare(GroupByNode modelGroup0, GroupByNode modelGroup1) {
			Object nodeValue1 = modelGroup1.getNodeValue();
			Object nodeValue0 = modelGroup0.getNodeValue();
			if ((nodeValue0 instanceof DecoratedModelGroup) && (nodeValue1 instanceof DecoratedModelGroup)) { 
				DecoratedModelGroup decoratedModelGroup0 = (DecoratedModelGroup) nodeValue0;
				DecoratedModelGroup decoratedModelGroup1 = (DecoratedModelGroup) nodeValue1;
				final int c = Functions.nullSafeCompareTo(decoratedModelGroup0.getContribution(), decoratedModelGroup1.getContribution());
				return (c == 0 ? 0 : -c);
			}
			else {
				if (nodeValue0 instanceof ModelGroup) 
					Logger.getLogger(getClass()).warn("Not decorated: " + ((ModelGroup)nodeValue0).getName());
				if (nodeValue1 instanceof ModelGroup) 
					Logger.getLogger(getClass()).warn("Not decorated: " + ((ModelGroup)nodeValue1).getName());
				return 0;
			}
		}
	}
	
	class MarketComparator implements Comparator<GroupByNode> {
		public int compare(GroupByNode market0, GroupByNode market1) {
			boolean isMemberMarket0 = (market0.getNodeValue() instanceof MemberMarket);
			boolean isMemberMarket1 = (market1.getNodeValue() instanceof MemberMarket);
			if (isMemberMarket0 && isMemberMarket1) {
				MemberMarket memberMarket0 = (MemberMarket) market0.getNodeValue();
				MemberMarket memberMarket1 = (MemberMarket) market1.getNodeValue();
				return Functions.nullSafeCompareTo(memberMarket0, memberMarket1);
			}
			else {
				if (!isMemberMarket0) {
					System.err.println(market0.getNodeValue() + " is not a MemberMarket (0)");
					GroupByNode p = market0.getParent();
					while (p != null) {
						p = p.getParent();
						if (p != null) {
							System.err.println("parent: " + p.getNodeValue());
						}
					}
				}
				if (!isMemberMarket1) {
					System.err.println(market1.getNodeValue() + " is not a MemberMarket (1)");
					GroupByNode p = market1.getParent();
					while (p != null) {
						p = p.getParent();
						if (p != null) {
							System.err.println("parent: " + p.getNodeValue());
						}
					}
				}
				return 0;
			}
		}
	}
	
	static final String[][] CMP_PROPERTIES_VEHICLE = new String[][] {
		{ "modelYear", "model", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "color", "trim", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "mileage", "trim", "color", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "dealerName", "trim", "color", "mileage", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "distanceFromDealer", "trim", "color", "mileage", "dealerName", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "unitCost", "transferPrice", "transferForRetailOnly", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "transferPrice", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "transferForRetailOnly", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "currentBid", "buyPrice", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "currentBid", "buyPrice", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "inventoryReceivedDate", "expires" },
		{ "modelYear", "model", "inventoryReceivedDate", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "expires" },
		{ "modelYear", "model", "expires", "trim", "color", "mileage", "dealerName", "distanceFromDealer", "unitCost", "transferPrice", "transferForRetailOnly", "currentBid", "buyPrice", "inventoryReceivedDate" }
	};
	static final String[] CMP_NAMES_VEHICLE = new String[] {
		"Trim",
		"Color",
		"Mileage",
		"Seller",
		"Distance",
		"UnitCost",
		"TransferPrice",
		"TransferForRetailOnly",
		"Bid",
		"DaysInInventory",
		"Expires"
	};
	static final int[][] CMP_ORDER_VEHICLE = new int[][] {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
	};
	
}
