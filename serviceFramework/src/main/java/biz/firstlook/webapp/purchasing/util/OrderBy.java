package biz.firstlook.webapp.purchasing.util;

public enum OrderBy {
	ASC (1, "ASC"), DESC(-1, "DESC");
	private int i;
	private String s;
	private OrderBy(int i, String s) {
		this.i = i;
		this.s = s;
	}
	public int intValue() {
		return i;
	}
	public String sqlValue() {
		return s;
	}
}
