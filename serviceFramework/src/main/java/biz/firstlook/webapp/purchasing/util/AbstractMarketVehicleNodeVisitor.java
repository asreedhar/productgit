package biz.firstlook.webapp.purchasing.util;

import java.util.Collection;
import java.util.List;

import biz.firstlook.module.market.search.MarketVehicle;

/**
 * This visitor performs a depth-first search of the GroupByNode tree structure to find a match of a MarketVehicle, returning the GroupByNode.
 * 
 * This class is also a template class for subclasses to check the condition of returning a MarketVehicle. 
 * @author bfung
 *
 */
public abstract class AbstractMarketVehicleNodeVisitor implements GroupByNodeVisitor {

	private GroupByNode node = null;

	public void visit(GroupByNode node) {
		List<GroupByNode> children = node.getChildNodes();
		if(children.isEmpty()) {
			Object val = node.getNodeValue();
			if (val instanceof Collection) {
				for(Object v : (Collection<?>)val) {
					if(v instanceof MarketVehicle) {
						MarketVehicle mv = (MarketVehicle)v;
						if(isItemFound(mv)) {
							this.node = node;
							break;
						}
					}
				}
			} else if (val instanceof MarketVehicle){
				MarketVehicle mv = (MarketVehicle)val;
				if(isItemFound(mv)) {
					this.node = node;
				}
			} 
		} else {
			for(GroupByNode child : children) {
				child.accept(this);
				if(this.node != null) {
					break;
				}
			}
		}
	}
	
	protected abstract boolean isItemFound(MarketVehicle marketVehicle);
	
	public GroupByNode getNode() {
		return node;
	}
}
