package biz.firstlook.webapp.purchasing.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class GroupByNode implements Serializable {
	private static final long serialVersionUID = -8717578392734049273L;
	Integer i;
	GroupByNode p;
	List<GroupByNode> c;
	Object v;
	transient List list;
	int listIndex;
	public GroupByNode (Integer i, GroupByNode p, Object v) {
		this.i = i;
		this.p = p;
		this.v = v;
		this.list = null;
		this.listIndex = -1;
		this.c = new ArrayList<GroupByNode>();
	}
	public GroupByNode (Integer i, GroupByNode p, List list, int listIndex) {
		this.i = i;
		this.p = p;
		this.v = null;
		this.list = list;
		this.listIndex = listIndex;
		this.c = new ArrayList<GroupByNode>();
	}
	public Integer getId() {
		return i;
	}
	public GroupByNode appendChild(GroupByNode n) {
		c.add(n);
		return n;
	}
	public List<GroupByNode> getChildNodes() {
		return c;
	}
	public Object getNodeValue() {
		return (v == null)
			? ((list != null && listIndex >= 0 && listIndex < list.size()) ? list.get(listIndex) : null)
			: v;
	}
	public GroupByNode getParent(){
		return p;
	}
	public String getPath() {
		Stack<Integer> stack = new Stack<Integer>();
		GroupByNode n = this;
		while (n.p != null) {
			stack.push(n.p.c.indexOf(n)+1);
			n = n.p;
		}
		StringBuffer sb = new StringBuffer();
		while (!stack.isEmpty()) {
			sb.append(stack.pop()).append(".");
		}
		return sb.deleteCharAt(sb.length()-1).toString();
	}
	public static final int numberOfNodesAtDepth(GroupByNode n, int depth) {
		if (n == null) {
			return 0;
		}
		if (depth == 0) {
			return n.c.size(); 
		}
		int num = 0;
		for (GroupByNode c : n.getChildNodes()) {
			num += numberOfNodesAtDepth(c, depth-1);
		}
		return num;
	}
	public void accept(GroupByNodeVisitor visitor) {
		visitor.visit(this);
	}
}
