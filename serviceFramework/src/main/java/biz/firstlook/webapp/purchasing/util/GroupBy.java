package biz.firstlook.webapp.purchasing.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.commons.util.Functions;

public class GroupBy implements Serializable {
	private static final long serialVersionUID = -166964402075712412L;
	private GroupByPropertyCallback<Object,Object>[] callbacks;
	private Decorator[] decorators;
	private GroupByNode rt;
	private List<GroupByNode> index;
	public GroupBy(GroupByPropertyCallback<Object,Object>[] callbacks, Decorator[] decorators, int size) {
		this.callbacks = callbacks;
		this.decorators = decorators;
		this.index = new ArrayList<GroupByNode>(size);
		this.rt = createNode(null, null);
	}
	public GroupByNode get(Integer groupByNodeID) {
		return index.get(groupByNodeID);
	}
	public Collection<GroupByNode> getGroups() {
		return rt.getChildNodes();
	}
	public GroupByNode getGroupByNode() {
		return rt;
	}
	public GroupByNode createNode(GroupByNode p, Object v) {
		GroupByNode n = new GroupByNode(index.size(), p, v);
		index.add(n);
		return n;
	}
	public GroupByNode createNode(GroupByNode p, List l, int i) {
		GroupByNode n = new GroupByNode(index.size(), p, l, i);
		index.add(n);
		return n;
	}
	public void group(List<? extends Object> objects) {
		long start = System.currentTimeMillis();
		long[] timing = new long[decorators.length];
		Arrays.fill(timing, 0L);
		for (int x = 0; x < objects.size(); x++) {
			Object o = objects.get(x);
			GroupByNode r = rt;
			EQ: for (int i = 0; i < callbacks.length; i++) {
				Object v = callbacks[i].getProperty(o);
				for (GroupByNode n : r.getChildNodes()) {
					if (Functions.nullSafeEquals(n.getNodeValue(), v)) {
						r = n;
						continue EQ;
					}
				}
				long ts0 = System.currentTimeMillis();
				final Object decoratedValue = decorators[i].decorate(v);
				long ts1 = System.currentTimeMillis();
				timing[i] += (ts1-ts0);
				r = r.appendChild(createNode(r, decoratedValue));
				v = null;
			}
			r.appendChild(createNode(r,objects,x));
		}
		boolean showGroupByDebug = false;
		Logger logger = Logger.getLogger(getClass());
		for (int i = 0; i < timing.length; i++) {
			if (timing[i] > 0) {
				logger.debug(decorators[i].getClass() + "=" + timing[i] + "ms");
				showGroupByDebug = true;
			}
		}
		if (showGroupByDebug) {
			logger.debug("GroupBy: " + (System.currentTimeMillis()-start) + "ms");
		}
	}
	public void sort(Comparator[] comparators) {
		sort(comparators, rt, 0);
	}
	public void sort(Comparator[] comparators, Integer groupByNodeID) {
		sort(comparators, (groupByNodeID == null || groupByNodeID >= index.size() ? rt : index.get(groupByNodeID)));
	}
	public void sort(Comparator[] comparators, GroupByNode n) {
		GroupByNode p = n.p;
		int depth = 0;
		while (p != null) {
			depth++;
			p = p.p;
		}
		sort(comparators, n, depth);
	}
	@SuppressWarnings("unchecked")
	public void sort(Comparator[] comparators, GroupByNode n, int depth) {
		if (depth >= 0 && comparators.length > depth) {
			Collections.sort(n.getChildNodes(), comparators[depth]);
			for (GroupByNode m : n.getChildNodes()) {
				sort(comparators, m, depth+1);
			}
		}
	}
	public void setProxyReferent(List list) {
		setProxyReferent(list, rt);
	}
	public void setProxyReferent(List list, GroupByNode node) {
		if (node.listIndex != -1) {
			node.list = list;
		}
		for (GroupByNode c : node.getChildNodes()) {
			setProxyReferent(list, c);
		}
	}
}
