package biz.firstlook.webapp.purchasing.action;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.presentation.DecoratedModelGroup;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.GroupBy;
import biz.firstlook.webapp.purchasing.util.GroupByNode;

public class OptimalInventorySearchSummarySuppressSearchCandidateAction extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		if (theForm.getSelectedNode() == null || theForm.getSelectedNode() < 1) {
			return null;
		}
		if (theForm.getGroupBySearchResults() == null) {
			return null;
		}
		if (theForm.getSelectedNode() >= theForm.getGroupBySearchResults().getGroupByNode().getChildNodes().size()+1) {
			return null;
		}
		GroupBy searchResults = theForm.getGroupBySearchResults();
		GroupByNode selectedResult = searchResults.get(theForm.getSelectedNode());
		DecoratedModelGroup decoratedModelGroup = (DecoratedModelGroup) selectedResult.getNodeValue();
		Collection<SearchCandidate> suppressSearchCandidates = decoratedModelGroup.getSearchCandidates();
		Collection<SearchCandidate> allSearchCandidates = theForm.getSearchCandidates();
		allSearchCandidates.removeAll(suppressSearchCandidates);
		searchResults.getGroupByNode().getChildNodes().remove(selectedResult);
		return null;
	}

}
