package biz.firstlook.webapp.purchasing.action;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.market.search.MarketSearchComponentBuilder;
import biz.firstlook.module.market.search.MarketSearchComponentBuilderFactory;
import biz.firstlook.module.market.search.MarketSummary;
import biz.firstlook.module.market.search.MarketVehicle;
import biz.firstlook.module.market.search.SearchCandidate;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.OrderBy;
import biz.firstlook.webapp.purchasing.util.ReflectionComparator;

public class LiveAuctionRunListAction extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		if (theForm.getSortByColumn() == null) {
			search(request, theForm);
		}
		sort(request, theForm);
		return mapping.findForward("success");
	}

	protected void search(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IntrospectionException {
		if (getMarketSummary(theForm) != null) {
			super.search(request, theForm);
		}
		else {
			if (theForm.getSearchResults() != null) {
				theForm.getSearchResults().clear();
			}
		}
	}
	
	protected SearchContext getSearchContext(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		final MarketSearchComponentBuilder builder = MarketSearchComponentBuilderFactory.getInstance().newMarketSearchBuilder();
		return builder.newContext(super.getSearchContext(request,theForm), getMarketSummary(theForm));
	}
	
	protected List<MarketVehicle> getSearchResults(OptimalInventorySearchSummaryForm theForm, SearchContext ctx, Collection<SearchCandidate> searchCandidates) {
		return getMarketSearchService().search(ctx, searchCandidates,theForm.isTotalSearch());
	}
	
	protected MarketSummary getMarketSummary(OptimalInventorySearchSummaryForm theForm) {
		MarketSummary marketSummary = null;
		if (theForm.getSearchSummaryIndex() != null && theForm.getSearchSummaryIndex() >= 0) {
			// this is if user is search from the Live Auction search Summary
			marketSummary = theForm.getSearchSummary().get(theForm.getSearchSummaryIndex());
		}
		else if (theForm.getSelectedNode() != null) {
			// this is if user is search from the Market Place search Summary
			marketSummary = (MarketSummary)theForm.getGroupBySearchSummary().get(theForm.getSelectedNode()).getNodeValue();
			theForm.setSearchSummaryIndex(theForm.getSearchSummary().indexOf(marketSummary));
		}
		return marketSummary;
	}
	
	@SuppressWarnings("unchecked")
	protected void sort(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws IllegalAccessException, InvocationTargetException, IntrospectionException {
		String sortByColumn = theForm.getSortByColumn();
		if (sortByColumn == null) {
			sortByColumn = "Lot";
		}
		String sortByOrder = theForm.getSortByOrder();
		OrderBy orderBy = OrderBy.DESC;
		if (sortByOrder == null || sortByOrder.equals("ASC")) {
			orderBy = OrderBy.ASC;
		}
		Collections.sort(
				theForm.getSearchResults(),
				new ReflectionComparator(
						CMP_NAMES_AUCTION,
						CMP_PROPERTIES_AUCTION,
						CMP_ORDER_AUCTION).initialise(sortByColumn, orderBy));
	}

	//DE1073 - added trim to sorting properties; the FE shows model + trim in one field -bf.
	static final String[][] CMP_PROPERTIES_AUCTION = new String[][] {
		{ "lot", "run", "modelYear", "model", "trim", "engine", "mileage", "candidateMatch", "VIN" },
		{ "run", "lot", "modelYear", "model", "trim", "engine", "mileage", "candidateMatch", "VIN" },
		{ "modelYear", "lot", "run", "model", "trim", "engine", "mileage", "candidateMatch", "VIN" },
		{ "model", "trim", "lot", "run", "modelYear", "engine", "mileage", "candidateMatch", "VIN" },
		{ "engine", "lot", "run", "modelYear", "model", "trim", "mileage", "candidateMatch", "VIN" },
		{ "mileage", "lot", "run", "modelYear", "model", "trim", "engine", "candidateMatch", "VIN" },
		{ "candidateMatch", "lot", "run", "modelYear", "model", "trim", "engine", "mileage", "VIN" },
		{ "VIN", "lot", "run", "modelYear", "model", "trim", "engine", "mileage", "candidateMatch" },
	};
	static final String[] CMP_NAMES_AUCTION = new String[] {
		"Lot",
		"Run",
		"Year",
		"Model",
		"Engine",
		"Mileage",
		"CandidateMatch",
		"VIN"
	};
	static final int[][] CMP_ORDER_AUCTION = new int[][] {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	};
	
}

