package biz.firstlook.webapp.purchasing.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.email.EmailService.EmailFormat;
import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.actionFilter.NextGenSession;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.webapp.purchasing.form.MailInGroupBuyerOrderForm;

public class MailInGroupBuyerOrder extends NextGenAction {

	private CoreService coreService;
	private EmailService emailService;
	
	public CoreService getCoreService() {
		return coreService;
	}

	public void setCoreService(CoreService coreService) {
		this.coreService = coreService;
	}

	public EmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Override
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		NextGenSession session = super.getNextGenSession(request);
		
		BusinessUnit dealer = getCoreService().findBusinessUnitByID(session.getBusinessUnitId());

		Member member = getMember(request);
		
		MailInGroupBuyerOrderForm mailForm = (MailInGroupBuyerOrderForm) form;
		
		final String from = mailForm.getSenderEmailAddress();
		final String to = mailForm.getContactEmail();
		final String cc = mailForm.getCcEmailAddress();
		
		TransferContentBuilder content = new TransferContentBuilder();
		
		content.setReplyTo(from);
		content.to(to);
		if (cc != null && cc.length() >= "a@b.c".length()) { 
			content.cc(cc);
		}
		if (mailForm.getSendToSelf()) {
			content.cc(from);
		}
		
		content.setBuyerDealerName(dealer.getName());
		content.setBuyerUserName(member.getFirstName() + " " + member.getLastName());
		content.setDate(mailForm.getDate());
		content.setNote(mailForm.getEmailBuyerNotes());
		content.setSellerDealerName(mailForm.getSellingDealerName());
		content.setSellerUserName(mailForm.getContactName());
		content.setTransferImmediatePickup(mailForm.isImmediatePickup() ? "Yes" : "No");
		content.setTransferPrice(mailForm.getTransferPrice());
		content.setIncludeInspectionBook(mailForm.isIncludeInspectionBook() ? "Yes" : "No");
		content.setIncludeExtraKeys(mailForm.isIncludeExtraKeys() ? "Yes" : "No");
		content.setIncludeNavigationDisc(mailForm.isIncludeNavigationDisc() ? "Yes" : "No");
		content.setVehicleColor(mailForm.getColor());
		content.setVehicleDescription(mailForm.getVehicle());
		content.setVehicleMileage(Integer.toString(mailForm.getMileage()));
		content.setVehicleStockNumber(mailForm.getStockNumber());
		content.setVehicleVin(mailForm.getVin());
		
		getEmailService().sendEmail(content);
		
		ActionRedirect redirect = new ActionRedirect(mapping.findForward("none"));
		
		redirect.addParameter("inventoryId", mailForm.getInventoryId());
		
		return redirect;
	}

	class TransferContentBuilder implements EmailContextBuilder {
		
		public List<String> getCcEmailAddresses() {
			return cc;
		}
		
		public Map<String, EmailFormat> getEmailFormats() {
			Map<String, EmailFormat> formats = new HashMap<String, EmailFormat>();
			for (String recipient: to) {
				formats.put(recipient, EmailFormat.HTML_MULTIPART);
			}
			return formats;
		}

		public Map<String, Resource> getEmbeddedResources() {
			return Collections.emptyMap();
		}

		public Map<String, String> getEmbeddedResourcesContentTypes() {
			return Collections.emptyMap();
		}

		public Map<String, Map<String, Object>> getRecipientsAndContext() {
			Map<String, Object> content = new HashMap<String, Object>();
			
			content.put("sellerDealerName", getSellerDealerName());
			content.put("sellerUserName", getSellerUserName());
			
			content.put("buyerDealerName", getBuyerDealerName());
			content.put("buyerUserName", getBuyerUserName());
			
			content.put("vehicleDescription", getVehicleDescription());
			content.put("vehicleColor", getVehicleColor());
			content.put("vehicleMileage", getVehicleMileage());
			content.put("vehicleVin", getVehicleVin());
			content.put("vehicleStockNumber", getVehicleStockNumber());
			
			content.put("transferImmediatePickup", getTransferImmediatePickup());
			content.put("transferPrice", getTransferPrice());
			
			content.put("includeInspectionBook", getIncludeInspectionBook());
			content.put("includeExtraKeys", getIncludeExtraKeys());
			content.put("includeNavigationDisc", getIncludeNavigationDisc());
			
			content.put("subject", getSubject());
			content.put("date", getDate());
			content.put("note", getNote());
			
			Map<String, Map<String, Object>> merge = new HashMap<String, Map<String, Object>>();
			for (String recipient: to) {
				merge.put(recipient, content);
			}
			return merge;
		}

		public String getSubject() {
			return String.format("Seller's Order for %s - Stock # %s", vehicleDescription, vehicleStockNumber);
		}

		public String getTemplateName() {
			return "inGroupTransferRequest";
		}

		public TransferContentBuilder() {
			super();
			to = new ArrayList<String>();
			cc = new ArrayList<String>();
		}
		
		private List<String> to, cc;

		private String replyTo;
		
		public void to(String to) {
			this.to.add(to);
		}
		
		public void cc(String cc) {
			this.cc.add(cc);
		}

		public String getReplyTo() {
			return this.replyTo;
		}
		public void setReplyTo(String replyTo) {
			this.replyTo = replyTo;
		}
		
		private String	sellerDealerName,
				sellerUserName,
				buyerDealerName,
				buyerUserName,
				vehicleDescription,
				vehicleColor,
				vehicleMileage,
				vehicleVin,
				vehicleStockNumber,
				transferImmediatePickup,
				transferPrice,
				includeInspectionBook,
				includeExtraKeys,
				includeNavigationDisc,
				date,
				note;

		public String getSellerDealerName() {
			return sellerDealerName;
		}
		public void setSellerDealerName(String sellerDealerName) {
			this.sellerDealerName = sellerDealerName;
		}

		public String getSellerUserName() {
			return sellerUserName;
		}
		public void setSellerUserName(String sellerUserName) {
			this.sellerUserName = sellerUserName;
		}

		public String getBuyerDealerName() {
			return buyerDealerName;
		}
		public void setBuyerDealerName(String buyerDealerName) {
			this.buyerDealerName = buyerDealerName;
		}

		public String getBuyerUserName() {
			return buyerUserName;
		}
		public void setBuyerUserName(String buyerUserName) {
			this.buyerUserName = buyerUserName;
		}

		public String getVehicleDescription() {
			return vehicleDescription;
		}
		public void setVehicleDescription(String vehicleDescription) {
			this.vehicleDescription = vehicleDescription;
		}

		public String getVehicleColor() {
			return vehicleColor;
		}
		public void setVehicleColor(String vehicleColor) {
			this.vehicleColor = vehicleColor;
		}

		public String getVehicleMileage() {
			return vehicleMileage;
		}
		public void setVehicleMileage(String vehicleMileage) {
			this.vehicleMileage = vehicleMileage;
		}

		public String getVehicleVin() {
			return vehicleVin;
		}
		public void setVehicleVin(String vehicleVin) {
			this.vehicleVin = vehicleVin;
		}

		public String getVehicleStockNumber() {
			return vehicleStockNumber;
		}
		public void setVehicleStockNumber(String vehicleStockNumber) {
			this.vehicleStockNumber = vehicleStockNumber;
		}

		public String getTransferImmediatePickup() {
			return transferImmediatePickup;
		}
		public void setTransferImmediatePickup(String transferImmediatePickup) {
			this.transferImmediatePickup = transferImmediatePickup;
		}

		public String getTransferPrice() {
			return transferPrice;
		}
		public void setTransferPrice(String transferPrice) {
			this.transferPrice = transferPrice;
		}
		
		public String getIncludeInspectionBook() {
			return includeInspectionBook;
		}
		public void setIncludeInspectionBook(String includeInspectionBook) {
			this.includeInspectionBook = includeInspectionBook;
		}

		public String getIncludeExtraKeys() {
			return includeExtraKeys;
		}
		public void setIncludeExtraKeys(String includeExtraKeys) {
			this.includeExtraKeys = includeExtraKeys;
		}

		public String getIncludeNavigationDisc() {
			return includeNavigationDisc;
		}
		public void setIncludeNavigationDisc(String includeNavigationDisc) {
			this.includeNavigationDisc = includeNavigationDisc;
		}

		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}

		public String getNote() {
			return note;
		}
		public void setNote(String note) {
			this.note = note;
		}
	}
}
