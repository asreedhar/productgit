package biz.firstlook.webapp.purchasing.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.market.search.Market;
import biz.firstlook.module.market.search.MarketType;
import biz.firstlook.module.market.search.MemberMarket;
import biz.firstlook.module.market.search.SearchContext;
import biz.firstlook.webapp.purchasing.form.OptimalInventorySearchSummaryForm;
import biz.firstlook.webapp.purchasing.util.Decorator;
import biz.firstlook.webapp.purchasing.util.DoNotDecorateDecorator;
import biz.firstlook.webapp.purchasing.util.GroupBy;
import biz.firstlook.webapp.purchasing.util.GroupByNode;
import biz.firstlook.webapp.purchasing.util.GroupByPropertyCallback;

public class MarketplaceSearchSummaryAction extends SearchBaseAction {

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OptimalInventorySearchSummaryForm theForm = (OptimalInventorySearchSummaryForm) form;
		
		//theForm.reset();
		
		if (theForm.getMarketTypes().isEmpty()) {
			theForm.setIgnoreMarketSuppression(true);
		}
		
		summary(request, theForm);
		group(request, theForm);
		empty(request, theForm);
		if (theForm.getActionForwardSuffix().length() > 0) {
			return mapping.findForward("success." + theForm.getActionForwardSuffix());
		}
		else {
			return mapping.findForward("success");
		}
	}

	@SuppressWarnings("unchecked")
	protected void group(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) throws NoSuchMethodException {
		GroupBy groupByResults = new GroupBy(
				new GroupByPropertyCallback[] {
						new MarketTypeCallback(),
						new MarketNameCallback()
				},
				new Decorator[] {
						new DoNotDecorateDecorator(),
						new DoNotDecorateDecorator() },
				theForm.getSearchSummary().size());

		
		System.out.println("Search Summary Size = " + theForm.getSearchSummary().size() );
		//This is here to sort the online auction results according to the member profile order.
		//Done as a bit of a hack due to time constraints as this was not an original requirement.
		GroupByNode root = groupByResults.getGroupByNode();
		GroupByNode online = root.appendChild(groupByResults.createNode(root, MarketType.ONLINE));
		SearchContext ctx = getSearchContext(request, theForm);
		for (MemberMarket memberMarket : ctx.getMarkets()) {
			if (memberMarket.getMarketType() == MarketType.ONLINE) {
				online.appendChild(groupByResults.createNode(online, memberMarket.getName()));
				System.out.println(theForm.getDealerOrgID() +"  Market Name = " + memberMarket.getName() );
			}
		}
		groupByResults.group(theForm.getSearchSummary());
		theForm.setGroupBySearchSummary(groupByResults);
	}
	
	class MarketTypeCallback implements Serializable, GroupByPropertyCallback<Market,MarketType> {
		private static final long serialVersionUID = 22136956994197539L;
		public MarketType getProperty(Market market) {
			return market.getMarketType();
		}
	}
	
	class MarketNameCallback implements Serializable, GroupByPropertyCallback<Market,String> {
		private static final long serialVersionUID = 5873490567242661990L;
		public String getProperty(Market market) {
			return market.getName();
		}
	}
	
	protected void empty(HttpServletRequest request, OptimalInventorySearchSummaryForm theForm) {
		// cache document and root node
		GroupBy groupBy = theForm.getGroupBySearchSummary();
		GroupByNode root = groupBy.getGroupByNode();
		// cache enum
		List<MarketType> marketTypes = new ArrayList<MarketType>();
		marketTypes.add(MarketType.ONLINE);
		marketTypes.add(MarketType.IN_GROUP);
		marketTypes.add(MarketType.LIVE);
		// add empty top level nodes
		MT: for (MarketType marketType: marketTypes) {
			for (GroupByNode n : root.getChildNodes()) {
				if (n.getNodeValue().equals(marketType)) {
					continue MT;
				}
			}
			root.appendChild(groupBy.createNode(root, marketType));
		}
		// add missing in-group and online markets
		MK: for (Market market : getSearchContext(request, theForm).getMarkets()) {
			if (market.getMarketType() == MarketType.LIVE) {
				continue MK;
			}
			final String name = market.getName();
			for (GroupByNode mt : root.getChildNodes()) {
				MarketType marketType = (MarketType) mt.getNodeValue();
				if (market.getMarketType().equals(marketType)) {
					for (GroupByNode mk : mt.getChildNodes()) {
						if (mk.getNodeValue().equals(name)) {
							continue MK;
						}
					}
					mt.appendChild(groupBy.createNode(mt, name));
				}
			}
		}
	}

}
