package biz.firstlook.webapp.purchasing.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.main.actionFilter.NextGenAction;
import biz.firstlook.main.commonDAOs.IPreferencesDAO;

/**
 * An action class that binds a request parameter to switch UnitCostThresholds (upper and lower) on and off
 * @author bfung
 *
 */
public abstract class AbstractLowerUnitCostFilterAction extends NextGenAction {

	protected static final String INCLUDE_LOW_UNIT_COST = "includeLowUnitCost";
	
	protected IPreferencesDAO preferencesDAO;
	
	@Override
	public final ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
			throws Exception {
        Integer businessUnitId = getNextGenSession( request ).getBusinessUnitId();
        
        Map<String, Object> pref = preferencesDAO.getPreferences( businessUnitId );
        Integer lowerUnitCostThresholdValue = (Integer)pref.get( "UnitCostThresholdLower" );
        request.setAttribute( "unitCostThresholdValue", lowerUnitCostThresholdValue );
        
        Boolean includeLowUnitCost = getUnitCostThreshPref(request);
        String includeLowUnitCostParam = request.getParameter( INCLUDE_LOW_UNIT_COST );
        if( includeLowUnitCostParam != null ) {
        	includeLowUnitCost = Boolean.valueOf( includeLowUnitCostParam );
        	request.getSession(false).setAttribute( INCLUDE_LOW_UNIT_COST, includeLowUnitCost.booleanValue() );
        }
        request.setAttribute( INCLUDE_LOW_UNIT_COST, includeLowUnitCost.booleanValue() );
        
		return filterIt( mapping, form, request, response, businessUnitId, includeLowUnitCost );
	}
	
	protected abstract ActionForward filterIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response,
									Integer businessUnitId, Boolean includeLowUnitCost ) throws Exception;

	private Boolean getUnitCostThreshPref( HttpServletRequest request )	{
    	Boolean useUnitCostThresh = Boolean.FALSE;
    	//check session
    	HttpSession session = request.getSession( false );
    	Boolean sessPref = (Boolean)session.getAttribute( INCLUDE_LOW_UNIT_COST );
    	if( sessPref != null ) {
    		useUnitCostThresh = sessPref;
    	} else {
    		session.setAttribute( INCLUDE_LOW_UNIT_COST, useUnitCostThresh );
    	}
		return useUnitCostThresh;
	}

	public void setPreferencesDAO( IPreferencesDAO preferencesDAO )	{
		this.preferencesDAO = preferencesDAO;
	}
}
