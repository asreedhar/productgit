package biz.firstlook.webapp.interfaces;


public interface Vehicle {
	
	public Integer getAge();
	public String getVin();

}
