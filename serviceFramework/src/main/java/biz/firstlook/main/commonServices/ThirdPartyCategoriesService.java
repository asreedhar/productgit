package biz.firstlook.main.commonServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import biz.firstlook.entity.ThirdPartyCategories;
import biz.firstlook.main.commonDAOs.IThirdPartyCategoriesDAO;
import biz.firstlook.main.commonServices.PreferenceService.ReportsEnum;

public class ThirdPartyCategoriesService
{
private IThirdPartyCategoriesDAO thirdPartyCategoriesDAO;
private PreferenceService preferenceService;

/**
 * Returns a List of Lists that contains the third party categories for the given business unit.
 * The first List in the List contains the primary categories, and the second List in the List
 * contains the secondary categories, if the dealer has a second book.
 * @param businessUnitId
 * @return List
 */
public List<List<ThirdPartyCategories>> getThirdPartyCategories( Integer businessUnitId )
{
	Map< String, Object > guideBookIdPreferences = getPreferenceService().getPreferences( businessUnitId, ReportsEnum.EQUITY );
	List< Integer > guideBookIds = getGuideBookIds( guideBookIdPreferences );
	List< ThirdPartyCategories > thirdPartyCategories = getThirdPartyCategoriesDAO().findAllByThirdPartyId( guideBookIds );
	List<List<ThirdPartyCategories>> thirdPartyCategoriesByThirdParty = splitListByThirdPartyId( thirdPartyCategories, guideBookIds );
	
	return thirdPartyCategoriesByThirdParty;
}

/**
 * Converts a Map of guidebook dealer preferences to an array of the guidebook ids that 
 * correspond to the preferences
 */
private List<Integer> getGuideBookIds( Map< String, Object > guideBookIdPreferences )
{
	List< Integer > preferencesList = new ArrayList< Integer >();
	preferencesList.add( (Integer)guideBookIdPreferences.get( "GuideBookID" ) );

	if ( guideBookIdPreferences.get( "GuideBook2Id" ) != null && (Integer)guideBookIdPreferences.get( "GuideBook2Id" ) != 0 )
	{
		preferencesList.add( (Integer)guideBookIdPreferences.get( "GuideBook2Id" ) );
	}
	preferencesList.add( (Integer)guideBookIdPreferences.get( "GuideBook3Id" ) );
	return preferencesList;
}

/**
 * Creates a List of Lists where the first List is the primary guidebook categories and the second
 * list is the secondary guidebook categories.
 */
private List<List<ThirdPartyCategories>> splitListByThirdPartyId( List< ThirdPartyCategories > thirdPartyCategories, List< Integer > guideBookIds )
{	
	List<ThirdPartyCategories> primaryThirdPartyCategories = new ArrayList<ThirdPartyCategories>();
	List<ThirdPartyCategories> secondaryThirdPartyCategories = new ArrayList<ThirdPartyCategories>();
	List<ThirdPartyCategories> tertiaryThirdPartyCategories = new ArrayList<ThirdPartyCategories>();
	
			
	for ( ThirdPartyCategories thirdPartyCategory : thirdPartyCategories )
	{
	    if ( thirdPartyCategory.getThirdPartyId().equals( guideBookIds.get( 0 ) ) )
	    {
	    	primaryThirdPartyCategories.add( thirdPartyCategory );	
	    }
	    else if ( thirdPartyCategory.getThirdPartyId().equals( guideBookIds.get( 1 ) ) )
	    {
	    	secondaryThirdPartyCategories.add( thirdPartyCategory );
	    }
	    else
	    {
	    	tertiaryThirdPartyCategories.add( thirdPartyCategory );
	    }
	}
	
	List<List<ThirdPartyCategories>> splitThirdPartyCategories = new ArrayList<List<ThirdPartyCategories>>();
	splitThirdPartyCategories.add( primaryThirdPartyCategories );
	splitThirdPartyCategories.add( secondaryThirdPartyCategories );
	splitThirdPartyCategories.add( tertiaryThirdPartyCategories );
	
	return splitThirdPartyCategories;	
}

public PreferenceService getPreferenceService()
{
	return preferenceService;
}

public void setPreferenceService( PreferenceService preferenceService )
{
	this.preferenceService = preferenceService;
}

public IThirdPartyCategoriesDAO getThirdPartyCategoriesDAO()
{
	return thirdPartyCategoriesDAO;
}

public void setThirdPartyCategoriesDAO( IThirdPartyCategoriesDAO thirdPartyCategoriesDAO )
{
	this.thirdPartyCategoriesDAO = thirdPartyCategoriesDAO;
}

}
