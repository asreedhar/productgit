package biz.firstlook.main.commonServices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.main.commonDAOs.IPreferencesDAO;

/**
 * this class is used to get preferences that are needed for a given report, but not worthy of being 
 * 		put into the session.
 * 
 * this class has an inner Enum type called ReportsEnum that just lists all reports and is used for 
 * 		the switch statement when trying to figure out what preferences are needed.
 */
public class PreferenceService
{

public enum ReportsEnum { INVENTORY_OVERVIEW, EQUITY, IMP, SEARCH_AND_ACQUISITION, AUCTION_DATA, PING, VIEW_DEALS }

private IPreferencesDAO preferencesDAO;

private Map<String, Object> allPreferences;

private Map<String, Object> thirdPartyasMMR;

private List< String > statusCodePreferences;

private Integer inventoryOverviewSortOrderType;

public PreferenceService()
{
	super();
	allPreferences = null;
	statusCodePreferences = null;
}

private void init( Integer businessUnitId )
{
	allPreferences = getPreferencesDAO().getPreferences( businessUnitId );
	allPreferences.put( "businessUnitId", businessUnitId );
	thirdPartyasMMR = getPreferencesDAO().fetchMMR();
}

private void initMemberPreferences( Integer memberId )
{
	statusCodePreferences = getPreferencesDAO().getMemberStatusCodePreferences( memberId );
	inventoryOverviewSortOrderType = getPreferencesDAO().getMemberInventoryOverviewSortOrderType( memberId );
}

public Map<String, Object> getPreferences( Integer businessUnitId, ReportsEnum reportEnum )
{ 	if ( allPreferences == null || allPreferences.get("businessUnitId") != businessUnitId )	{
		init( businessUnitId );
	}
		
	Map<String, Object> toBeReturned = new HashMap<String, Object>();
	switch (reportEnum)
	{
		case INVENTORY_OVERVIEW:
			toBeReturned.put("ShowLotLocationStatus", allPreferences.get("ShowLotLocationStatus"));
			toBeReturned.put("ListPricePreference", allPreferences.get("ListPricePreference"));
			toBeReturned.put("AverageInventoryAgeRedThreshold", allPreferences.get("AverageInventoryAgeRedThreshold"));
			toBeReturned.put("AverageDaysSupplyRedThreshold", allPreferences.get("AverageDaysSupplyRedThreshold"));
			toBeReturned.put("StockOrVinPreference", allPreferences.get("StockOrVinPreference"));
			toBeReturned.put("LastDMSReferenceDateUsed", allPreferences.get("LastDMSReferenceDateUsed"));
			toBeReturned.put("UnitsSoldThresholdInvOverview", allPreferences.get("UnitsSoldThresholdInvOverview"));
			toBeReturned.put("ZipCode", allPreferences.get("ZipCode"));
			break;
		case EQUITY:
			toBeReturned.put("GuideBookID", allPreferences.get("GuideBookID"));
			toBeReturned.put("GuideBook2Id", allPreferences.get("GuideBook2Id"));
			toBeReturned.put("GuideBook3Id", thirdPartyasMMR.get("ThirdPartyID"));
			break;
		case IMP:
			toBeReturned.put("CheckAppraisalHistoryForIMPPlanning", allPreferences.get("CheckAppraisalHistoryForIMPPlanning") );
			toBeReturned.put("ZipCode", allPreferences.get("ZipCode"));
			Object useLotPrice = allPreferences.get("UseLotPrice");
			if (useLotPrice != null && ((Integer)useLotPrice).intValue() == 1) {
				toBeReturned.put("UseLotPrice", Boolean.TRUE);				
			} else {
				toBeReturned.put("UseLotPrice", Boolean.FALSE);				
			}
            toBeReturned.put("HighMileage", allPreferences.get("HighMileageThreshold"));
            toBeReturned.put("ExcessiveMileage", allPreferences.get("ExcessiveMileageThreshold"));
            toBeReturned.put("RepriceConfirmation", allPreferences.get("RepriceConfirmation"));
            toBeReturned.put("RepriceThreshold", allPreferences.get("RepricePercentChangeThreshold"));
            break;
		case SEARCH_AND_ACQUISITION:
			toBeReturned.put("DisplayUnitCostToDealerGroup", allPreferences.get("DisplayUnitCostToDealerGroup") );
            break;
		case AUCTION_DATA:
			toBeReturned.put("AuctionAreaId", allPreferences.get("AuctionAreaId"));
			toBeReturned.put("AuctionTimePeriodId", allPreferences.get("AuctionTimePeriodId") );
            break;
		case PING:
			toBeReturned.put("ZipCode", allPreferences.get("ZipCode"));
            break;
        case VIEW_DEALS:
            toBeReturned.put("HighMileageThreshold", allPreferences.get("HighMileageThreshold"));
            break;
		default:
	}
	return toBeReturned;
}

public String[] getMemberStatusCodePreferences( Integer memberId )
{
	initMemberPreferences( memberId );
	String[] statusCodeArray = (String[])statusCodePreferences.toArray(new String[statusCodePreferences.size()]);
		
	return statusCodeArray;
}

public String getMemberStatusCodePreferencesAsStr( Integer memberId )
{
	String statusCodeStr = "";
	initMemberPreferences( memberId );
	statusCodeStr += StringUtils.join( statusCodePreferences.iterator(), ',' );
	return statusCodeStr;
}

public IPreferencesDAO getPreferencesDAO()
{
	return preferencesDAO;
}

public void setPreferencesDAO( IPreferencesDAO preferencesDAO )
{
	this.preferencesDAO = preferencesDAO;
}

public Integer getMemberInventoryOverviewSortOrderType(Integer memberId) {
	initMemberPreferences( memberId );
	return inventoryOverviewSortOrderType;
}

public void setMemberInventoryOverviewSortOrderType(Integer sortBy, Integer memberId){
	getPreferencesDAO().setMemberInventoryOverviewSortOrderType( sortBy, memberId );
}


}
