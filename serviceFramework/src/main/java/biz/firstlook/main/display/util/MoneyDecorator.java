package biz.firstlook.main.display.util;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class MoneyDecorator implements ColumnDecorator
{

public static String MONEY_FORMAT = "$#,###,###,###,##0";

public String decorate( Object object ) throws DecoratorException
{
	try
	{
		String s = formatMoney( object );
		return s;
	}
	catch ( Exception e )
	{
		throw new DecoratorException( MoneyDecorator.class, e.getMessage() );
	}
}

public static String formatMoney( Object object ) throws DecoratorException
{
	if ( object == null )
	{
		return "&mdash;";
	}
	else
	{
		try
		{
			if ( object instanceof String )
			{
				Number number = new Double( (String)object );
				object = number;
			}
		}
		catch ( NumberFormatException nfe )
		{
			return "&mdash;";
		}

		if ( object instanceof Number )
		{
			Number number = (Number)object;
			DecimalFormat decimalFormat = new DecimalFormat( MONEY_FORMAT );
			String toBeReturned;
			if ( number.doubleValue() >= 0 )
			{
				toBeReturned = decimalFormat.format( number.doubleValue() );
			}
			else
			{
				toBeReturned = "<span class=\"neg\"> (" + decimalFormat.format( Math.abs( number.doubleValue()) ) + ")</span>";
			}
			return toBeReturned;
		}
		else
		{
			throw new RuntimeException( "MoneyDecorator expects a Number object." );
		}
	}
}

}
