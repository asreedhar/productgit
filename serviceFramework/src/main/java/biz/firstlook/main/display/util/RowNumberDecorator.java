package biz.firstlook.main.display.util;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class RowNumberDecorator implements ColumnDecorator
{
int rowNumber;
	
public RowNumberDecorator()
{
	super();
}

public String decorate( Object arg0 ) throws DecoratorException
{
	return "" + ++rowNumber ;
}

}
