package biz.firstlook.main.display.util;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class PercentageDecorator implements ColumnDecorator
{

private static DecimalFormat PERCENTAGE_FORMAT = new DecimalFormat( "####.#%" );

public PercentageDecorator()
{
	super();
}

public String decorate( Object object ) throws DecoratorException
{
	if ( object == null )
	{
		return "N/A";
	}
	else if( object instanceof Double )
	{
		Double number = (Double)object;
		if( number.doubleValue() == Integer.MIN_VALUE )
		{
			return "N/A";
		}
		else
		{
			String retString = PERCENTAGE_FORMAT.format( object );
			if(number < 0) {
				
				number = -number;				
				retString = "<span style=\"color:red;\">("+PERCENTAGE_FORMAT.format( number )+")</span>";
				
			}
			return retString;
		}
	}
	else
	{
		return "N/A";
	}
}

}
