package biz.firstlook.main.display.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class DateNoTimeDecorator implements DisplaytagColumnDecorator
{

private static final SimpleDateFormat dateFormater = new SimpleDateFormat("EEE, d MMM yyyy");

public String decorate( Object columnValue, PageContext pageContext, MediaTypeEnum mediaTypeEnum ) throws DecoratorException
{
	try
	{
		String s = formatDate( columnValue );
		return s;
	}
	catch ( Exception e )
	{
		throw new DecoratorException( DateDecorator.class, e.getMessage() );
	}	
}

public static String formatDate( Object object ) throws DecoratorException
{
	if ( object == null )
	{
		return "&mdash;";
	}
	else
	{
		if ( object instanceof Date )
		{
			return dateFormater.format(object);
		} else {
			throw new RuntimeException( "DateDecorator expects a Date object." );
		}
	}
}

}
