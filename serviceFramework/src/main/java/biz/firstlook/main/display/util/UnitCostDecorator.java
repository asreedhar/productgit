package biz.firstlook.main.display.util;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

/**
 * Takes a String object representing a numeric range of values and returns a 
 * formatted string.
 * @author jcaron
 *
 */

public class UnitCostDecorator implements ColumnDecorator {
	
	public static String MONEY_FORMAT = "$#,###,###,###,##0";
	
	public UnitCostDecorator() {
		super();
	}

	public String decorate(Object obj) throws DecoratorException {
		
		if(obj == null) {
			
			return "&mdash;";
			
		}else if(obj instanceof String) {
			
			DecimalFormat decimalFormat = new DecimalFormat( MONEY_FORMAT );
			
			String element = (String)obj;
			
			String first = element.substring(0,element.indexOf("-"));
			String last = element.substring(element.indexOf("-") + 1);
			
			try {
					double dblFirst = Double.parseDouble(first);
					double dblLast = Double.parseDouble(last);
					
					if(dblFirst < 1) {
						
					
						return "Under "+decimalFormat.format(dblLast);
						
					}else if(dblLast == 10000000) {
						
						return "Over "+decimalFormat.format(dblFirst);
						
					}else {				
						
						return decimalFormat.format(dblFirst)+"-"+decimalFormat.format(dblLast);
					}
			}catch(NumberFormatException e) {
				return "&mdash;";
			}
				
			
			
		}else {
			
			return "&mdash;";
		}
		
		
		
		
		
	}

}
