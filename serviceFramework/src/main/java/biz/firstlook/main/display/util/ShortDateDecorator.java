package biz.firstlook.main.display.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class ShortDateDecorator implements ColumnDecorator
{

private static final SimpleDateFormat dateFormater = new SimpleDateFormat("MM/dd/yy");

public String decorate( Object object ) throws DecoratorException
{
	try
	{
		String s = formatDate( object );
		return s;
	}
	catch ( Exception e )
	{
		throw new DecoratorException( DateDecorator.class, e.getMessage() );
	}
}

public static String formatDate( Object object ) throws DecoratorException
{
	if ( object == null )
	{
		return "&mdash;";
	}
	else
	{
		if ( object instanceof Date )
		{
			return dateFormater.format(object);
		} else {
			throw new RuntimeException( "DateDecorator expects a Date object." );
		}
	}
}

}
