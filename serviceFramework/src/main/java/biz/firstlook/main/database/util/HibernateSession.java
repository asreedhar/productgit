package biz.firstlook.main.database.util;

import java.sql.Connection;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class HibernateSession
{
private static final String ROLLBACKONLYFLAG = "RollBackOnlyFlag";
private final static Log log = LogFactory.getLog( HibernateSession.class );

private static SessionFactory m_SessionFactory;

public final static void setRollbackOnly( boolean flag )
{
	if ( !TransactionSynchronizationManager.hasResource( ROLLBACKONLYFLAG ) )
	{
		TransactionSynchronizationManager.bindResource( ROLLBACKONLYFLAG, Boolean.valueOf( flag ) );
	}
}

public final static void setRollbackOnly()
{
	setRollbackOnly( true );
}

public final static boolean getRollbackOnly()
{
	boolean rollbackOnly = false;

	if ( TransactionSynchronizationManager.hasResource( ROLLBACKONLYFLAG ) )
	{
		rollbackOnly = ( (Boolean)TransactionSynchronizationManager.getResource( ROLLBACKONLYFLAG ) ).booleanValue();
	}

	return rollbackOnly;
}

public final static Session getSession() throws HibernateException
{
	if ( !isSessionBoundToThread() )
	{
		SessionHolder sessionHolder = new SessionHolder( getSessionFactory().openSession() );
		sessionHolder.setTransaction( sessionHolder.getSession().beginTransaction() );
		TransactionSynchronizationManager.bindResource( getSessionFactory(), sessionHolder );
		TransactionSynchronizationManager.initSynchronization();
	}
	
	reconnectSession();
	
	return SessionFactoryUtils.getSession( getSessionFactory(), false );
}

public static boolean isSessionBoundToThread()
{
	return TransactionSynchronizationManager.hasResource( getSessionFactory() );
}

public static void initSyncIfNecessary()
{
	if ( !TransactionSynchronizationManager.isSynchronizationActive() )
	{
		TransactionSynchronizationManager.initSynchronization();
	}
}

protected static void reconnectSession() throws HibernateException
{
	if ( SessionFactoryUtils.getSession( getSessionFactory(), false ) != null
			&& !SessionFactoryUtils.getSession( getSessionFactory(), false ).isConnected() )
	{
		Connection connection = SessionFactoryUtils.getSession( getSessionFactory(), false ).connection();
		SessionFactoryUtils.getSession( getSessionFactory(), false ).reconnect( connection );
	}
}

private static void disconnectSession() throws HibernateException
{
	if ( isSessionBoundToThread()
			&& SessionFactoryUtils.getSession( getSessionFactory(), false ) != null
			&& SessionFactoryUtils.getSession( getSessionFactory(), false ).isConnected() )
	{
		SessionFactoryUtils.getSession( getSessionFactory(), false ).disconnect();
	}
}

protected static void doBeginTransaction() throws HibernateException
{
	SessionHolder sessionHolder = (SessionHolder)TransactionSynchronizationManager.getResource( getSessionFactory() );
	if ( sessionHolder.getTransaction() == null )
	{
		sessionHolder.setTransaction( sessionHolder.getSession().beginTransaction() );
	}
}

protected final static Transaction getTransaction()
{
	if ( isSessionBoundToThread() )
	{
		return ( (SessionHolder)TransactionSynchronizationManager.getResource( getSessionFactory() ) ).getTransaction();
	}

	return null;
}

public static void doTransactionRollback()
{
	try
	{
		if ( getTransaction() != null && !getTransaction().wasCommitted() && !getTransaction().wasRolledBack() )
		{
			getTransaction().rollback();
		}

		if ( !getRollbackOnly() )
		{
			doCloseSession();
		}
	}
	catch ( HibernateException rollbackException )
	{
		log.error( "error during Hibernate Transaction Rollback, SKIPPING", rollbackException );
	}
}

public static void doTransactionCommit() throws HibernateException
{
	if ( getTransaction() != null && !getTransaction().wasCommitted() && !getTransaction().wasRolledBack() )
	{
		getTransaction().commit();
	}

	disconnectSession();
}

public static void resetThreadSession()
{
	if ( isSessionBoundToThread() )
	{
		TransactionSynchronizationManager.unbindResource( getSessionFactory() );
		TransactionSynchronizationManager.clearSynchronization();
	}
}

private static void doCloseSession() throws HibernateException
{
	if ( isSessionBoundToThread() )
	{
		SessionFactoryUtils.getSession( getSessionFactory(), false ).close();
	}
}

final public static void closeSession()
{
	try
	{
		try
		{
			doTransactionCommit();
		}
		catch ( HibernateException e )
		{
			log.error( "error during Hibernate COMMIT, SKIPPING", e );
			doTransactionRollback();
		}

		doCloseSession();
	}
	catch ( HibernateException closeException )
	{
		log.error( "error during Hibernate Session Close, SKIPPING", closeException );
	}
	finally
	{
		resetThreadSession();
	}
}

public static void restoreSession( Session session ) throws HibernateException
{
	if ( isSessionBoundToThread() )
	{
		log.warn( "Closing dirty Hibernate Session "
				+ SessionFactoryUtils.getSession( getSessionFactory(), false ) + " found in Thread " + Thread.currentThread() );
		doCloseSession();
		resetThreadSession();
	}

	TransactionSynchronizationManager.bindResource( getSessionFactory(), new SessionHolder( session ) );
	TransactionSynchronizationManager.initSynchronization();
	reconnectSession();
	doBeginTransaction();
}

public static SessionFactory getSessionFactory()
{
	return m_SessionFactory;
}

public static void setSessionFactory( SessionFactory sessionFactory )
{
	m_SessionFactory = sessionFactory;
}
}