package biz.firstlook.main.commonDAOs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.main.actionFilter.NextGenSession;
import biz.firstlook.main.enumerator.DealerUpgradeEnum;
import biz.firstlook.main.enumerator.RoleEnum;

/**
 * this DAO is used to populate a @link biz.firstlook.main.actionFilter.NextGenSession bean.
 * this class has an inner class mapper that maps results from the query to fields on the bean, it also handles 
 * conversions from 0/1 in the DB to true/false
 */
public class NextGenSessionDAO extends JdbcDaoSupport implements INextGenSessionDAO
{

public NextGenSessionDAO()
{
	super();
}

/**
 * @param userName the user name from CAS
 * @param businessUnitId the dealership the user is logged into
 * @throws NoNextGenSessionDataException if either the userName or businessUnitId are null
 */
public NextGenSession getNextGenSession( String userName, Integer businessUnitId )
{
	if (userName == null || businessUnitId == null) {
		throw new NoNextGenSessionDataException();
	}
	
	Object[] parameterValues = { (Object)businessUnitId, (Object)userName };

	String sql = ""
			+ " Select 	BusinessUnitID, MemberID, RoleID, BookOutPreferenceId, BookOutPreferenceSecondId, UpgradeStartDate, UpgradeEndDate, "
			+ " 		GuideBook2BookOutPreferenceId, GuideBook2SecondBookOutPreferenceId, BusinessUnitShortName, "
			+ " 		DealerUpgradeCD, LithiaStore, ShowLotLocationStatus, bucketJumperAIPApproach, aipRunDayOfWeek, ParentId, FlashLocatorHideUnitCostDays, ProgramType_CD, ZipCode," 
			+ " 		GuideBookId, GuideBook2Id, IncludeTransferPricing, ShowInTransitInventoryForm" 
			+ " from dbo.GetBusinessUnitMemberPreference( ?, ?) ";	
	return (NextGenSession)getJdbcTemplate().query( sql, parameterValues, new RowMapper() );
}

public Integer isFromMax( final String sessionId )
{
	try
	{
		return getJdbcTemplate().queryForInt( "select BusinessUnitId from HalSessions where HalSessionID = ?", new Object[]{sessionId} );
	}
	catch( IncorrectResultSizeDataAccessException ex )
	{
		logger.debug( "No halsession" );
		return null;
	}
}

/**
 * inner class that maps the results form the query to our display bean
 */
class RowMapper implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	NextGenSession nextGenSession = new NextGenSession();

	Set< Integer > roles = new HashSet< Integer >();
	Set< Integer > upgrades = new HashSet< Integer >();

	boolean firstTimeThrough = true;

	while ( rs.next() ) {
		if ( firstTimeThrough ) {
			// all values that will be the same for each result row are
			// initialized inside this if block
			nextGenSession.setBusinessUnitId( rs.getInt( "BusinessUnitID" ) );
			nextGenSession.setMemberId( rs.getInt( "MemberID" ) );
			nextGenSession.setPrimaryGuideBookId( rs.getInt( "GuideBookId" ) );
			nextGenSession.setPrimaryGuideBookFirstPreferenceId( rs.getInt( "BookOutPreferenceId" ) );
			nextGenSession.setPrimaryGuideBookSecondPreferenceId( rs.getInt( "BookOutPreferenceSecondId" ) );
			nextGenSession.setSecondaryGuideBookId( rs.getInt( "GuideBook2Id" ) );
			nextGenSession.setSecondaryGuideBookFirstPreferenceId( rs.getInt( "GuideBook2BookOutPreferenceId" ) );
			nextGenSession.setSecondaryGuideBookSecondPreferenceId( rs.getInt( "GuideBook2SecondBookOutPreferenceId" ) );
			nextGenSession.setDealerNickName( rs.getString( "BusinessUnitShortName" ) );
			nextGenSession.setLithiaStore( rs.getBoolean( "LithiaStore" ) );
			nextGenSession.setShowLotLocationAndStatus( rs.getBoolean( "ShowLotLocationStatus" ) );
			nextGenSession.setAipRunDayOfWeek( new Integer( rs.getInt( "aipRunDayOfWeek") ) );
			nextGenSession.setBucketJumperAIPApproach( rs.getBoolean( "bucketJumperAIPApproach") );
			nextGenSession.setFlashLocatorHideUnitCostDays( rs.getInt( "FlashLocatorHideUnitCostDays") );
			nextGenSession.setParentId( rs.getInt( "ParentId" ) );
			nextGenSession.setProgramTypeCode( rs.getInt( "ProgramType_CD" ) );
			nextGenSession.setZipCode( rs.getString("ZipCode" ) );
			nextGenSession.setIncludeTransferPricing(rs.getBoolean("IncludeTransferPricing"));
			nextGenSession.setShowInTransitInventoryForm(rs.getBoolean("ShowInTransitInventoryForm"));
			firstTimeThrough = false;
		}
		
		Integer dealerUpgradeCD = (Integer)rs.getObject( "DealerUpgradeCD" );
		if ( dealerUpgradeCD != null ) {
			DealerUpgradeEnum dealerUpgrade = DealerUpgradeEnum.valueOfId(dealerUpgradeCD);
    		
			Date today = DateUtils.truncate(Calendar.getInstance().getTime(), Calendar.DATE);
			Date startDate = rs.getDate( "UpgradeStartDate" ); 
			Date endDate = rs.getDate( "UpgradeEndDate" );
			Integer roleId = (Integer)rs.getObject( "RoleID" );
			
			switch(dealerUpgrade) {
				case MAKE_A_DEAL:
				case MARKET_STOCKING_GUIDE:	
				case PING:
				case PINGII:
					if ( startDate == null || startDate.before( today ) ) {
	    				if (  endDate == null || endDate.after( today ) ) {
	    					roles.add( roleId );
	    					upgrades.add( dealerUpgradeCD );
	    				}
	    			}
					break;
				case JDPOWER_USED_CAR_MARKET_DATA:
				case EDMUNDS_TMV:
					//if the End date is filled in, the Upgrade is in Demo mode.  Demo ends after the End Date.
					//if the End date is not filled in, the customer has purchased the Upgrade
	    			if(startDate != null && today.after(startDate)) {
	    				if(endDate == null || (today.before(endDate) || today.equals(endDate))) {
	    					roles.add( roleId );
	    					upgrades.add( dealerUpgradeCD );
	    				}
	    			}
					break;
				default:
					roles.add( roleId );
					upgrades.add( dealerUpgradeCD );
					break;
			}
		}
	}

	// you got no data back from the stored procedure
	if (firstTimeThrough) {
		throw new NoNextGenSessionDataException();
	}
	
	// Setting upgrade preferences
	// note: need to create Shorts because the column in the db is of type tinyInt so Short's in java
	nextGenSession.setIncludeAgingPlan( upgrades.contains( DealerUpgradeEnum.AIP.getDealerUpgradeId() ) );
	nextGenSession.setIncludeCIA( upgrades.contains( DealerUpgradeEnum.CIA.getDealerUpgradeId()) );
	nextGenSession.setIncludeRedistribution( upgrades.contains( DealerUpgradeEnum.REDISTRIBUTION.getDealerUpgradeId() ) );
	nextGenSession.setIncludePerformanceDashboard( upgrades.contains(  DealerUpgradeEnum.PERFORMANCE_DASHBOARD.getDealerUpgradeId() ) );
	nextGenSession.setIncludeAppraisal( upgrades.contains( DealerUpgradeEnum.TRADE_ANALYZER.getDealerUpgradeId() ) );
	nextGenSession.setIncludeAnnualRoi( upgrades.contains( DealerUpgradeEnum.ANNUAL_ROI.getDealerUpgradeId() ) );
	nextGenSession.setIncludeEquityAnalyzer( upgrades.contains( DealerUpgradeEnum.EQUITY_ANALYZER.getDealerUpgradeId()) );
	nextGenSession.setIncludeAuctionData( upgrades.contains( DealerUpgradeEnum.AUCTION_DATA.getDealerUpgradeId() ) );
	nextGenSession.setIncludeNavigatorVIPAndDealersResource( upgrades.contains( DealerUpgradeEnum.NAVIGATOR_VIP_DEALERS_RESOURCE.getDealerUpgradeId() ) );
	nextGenSession.setPingUpgrade( upgrades.contains( DealerUpgradeEnum.PING.getDealerUpgradeId() ) );
	nextGenSession.setIncludeKBBTradeIn( upgrades.contains( DealerUpgradeEnum.KBB_TRADE_IN.getDealerUpgradeId() ) );
	nextGenSession.setIncludeJDPowerUCMData( upgrades.contains( DealerUpgradeEnum.JDPOWER_USED_CAR_MARKET_DATA.getDealerUpgradeId() ) );
	nextGenSession.setPingIIUpgrade(upgrades.contains(DealerUpgradeEnum.PINGII.getDealerUpgradeId()));
	nextGenSession.setEdmundsTmvUpgrade(upgrades.contains(DealerUpgradeEnum.EDMUNDS_TMV.getDealerUpgradeId()));
	nextGenSession.setIncludeMakeADeal(upgrades.contains(DealerUpgradeEnum.MAKE_A_DEAL.getDealerUpgradeId()));
	nextGenSession.setIncludeMarketStockingGuide(upgrades.contains(DealerUpgradeEnum.MARKET_STOCKING_GUIDE.getDealerUpgradeId()));
	nextGenSession.setIncludeNADAValues(upgrades.contains(DealerUpgradeEnum.NADA_VALUES.getDealerUpgradeId()));

	// set permissions based on member role

	// this logic is used to figure out what the nav bar will include.
	// this one reducedToolsMenu variable is replacing 4 variables from old IMT:
	// 				-reducedToolsMenu	
	// 				-IncludeREportsMenu
	// 				-IncludeRedistributionMenu
	// 				-IncludePrintButton
	// sorry this logic is pretty ugly, it comes from the following old IMT statement:
	// if ( member.getMemberRoleTester().isUsedAppraiser() || member.getMemberRoleTester().isUsedNoAccess() )
	// where isUsedNoAccess is defined as: !(Role.USED_APPRAISER ) && !( Role.USED_STANDARD ) && !( Role.USED_MANAGER )
	if ( roles.contains( RoleEnum.USED_APPRAISER.getRoleId() )
			|| !( roles.contains( RoleEnum.USED_APPRAISER.getRoleId() ) 
					|| roles.contains( RoleEnum.USED_STANDARD.getRoleId() ) 
					|| roles.contains( RoleEnum.USED_MANAGER.getRoleId() ) ) )
	{
		nextGenSession.setReducedToolsMenu( true );
	}
	else
	{
		nextGenSession.setReducedToolsMenu( false );
	}

	return nextGenSession;
}

}

}
