package biz.firstlook.main.commonDAOs;

import biz.firstlook.main.actionFilter.NextGenSession;

public interface INextGenSessionDAO
{
	public NextGenSession getNextGenSession( String userName, Integer businessUnitId );
	
	/**
	 * @param sessionId
	 * @return BusinessUnitId accessing MAX
	 */
	public Integer isFromMax( String sessionId );
}
