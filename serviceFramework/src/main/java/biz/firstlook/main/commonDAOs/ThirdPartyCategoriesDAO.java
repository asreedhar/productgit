package biz.firstlook.main.commonDAOs;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.entity.ThirdPartyCategories;


public class ThirdPartyCategoriesDAO extends HibernateDaoSupport implements IThirdPartyCategoriesDAO
{

@SuppressWarnings("unchecked")
public List< ThirdPartyCategories > findAllByThirdPartyId( final List< Integer > thirdPartyIds )
{
	HibernateCallback callback = new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Criteria crit = session.createCriteria( ThirdPartyCategories.class )
			                .add( Expression.in( "thirdPartyId", thirdPartyIds ) )
			                .addOrder( Order.asc( "thirdPartyId" ) )
			                .addOrder( Order.asc( "id" ) );
			return crit.list();
		}
	};

	return (List< ThirdPartyCategories >)getHibernateTemplate().execute( callback );
}

}
