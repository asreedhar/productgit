package biz.firstlook.main.commonDAOs;

import java.util.List;
import java.util.Map;

public interface IPreferencesDAO
{
public Map<String, Object> getPreferences( Integer businessUnitId );

public List< String > getMemberStatusCodePreferences( Integer memberId );

public Integer getMemberInventoryOverviewSortOrderType(Integer memberId);

public void setMemberInventoryOverviewSortOrderType(Integer sortBy, Integer memberId);

public Map<String, Object> fetchMMR();

}
