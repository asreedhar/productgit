package biz.firstlook.main.commonDAOs;

import java.util.List;

import biz.firstlook.entity.InventoryBucketRange;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;

public interface IInventoryBucketRangeDAO
{
public List<InventoryBucketRange> loadBuckets( Integer businessUnitId, InventoryBucketTypeEnum inventoryBucketType );

public InventoryBucketRange loadBucketForInventoryAge( Integer age, Integer businessUnitId, InventoryBucketTypeEnum inventoryBucketType );
}
