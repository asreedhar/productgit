package biz.firstlook.main.commonDAOs;

public class NoNextGenSessionDataException extends RuntimeException {

	private static final long serialVersionUID = 1196772634926937271L;

	public NoNextGenSessionDataException() {
		// TODO Auto-generated constructor stub
	}

	public NoNextGenSessionDataException(String message) {
		super(message);
	}

	public NoNextGenSessionDataException(Throwable cause) {
		super(cause);
	}

	public NoNextGenSessionDataException(String message, Throwable cause) {
		super(message, cause);
	}

}
