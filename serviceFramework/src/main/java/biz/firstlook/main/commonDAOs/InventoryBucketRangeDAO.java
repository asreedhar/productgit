package biz.firstlook.main.commonDAOs;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.entity.InventoryBucketRange;
import biz.firstlook.main.enumerator.InventoryBucketTypeEnum;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;

public class InventoryBucketRangeDAO  extends GenericHibernate< InventoryBucketRange, Integer > implements IInventoryBucketRangeDAO
{

public InventoryBucketRangeDAO()
{
	super( InventoryBucketRange.class );
}



public List<InventoryBucketRange> loadBuckets( Integer businessUnitId, InventoryBucketTypeEnum inventoryBucketType )
{	
    final StringBuffer queryString = new StringBuffer();
    queryString.append( "select * from GetInventoryBucketRanges(" );
    queryString.append( businessUnitId );
    queryString.append( ", " );
    queryString.append( inventoryBucketType.getInventoryBucketId() );
    queryString.append( ")" );
    
    List bucketRanges = (List) getHibernateTemplate().execute(
        new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {
	            return session.createSQLQuery( queryString.toString() )
	                          .addEntity( InventoryBucketRange.class )
	                          .list();	                                                                 	
	        }	
        }                                                             
    );
    
   return bucketRanges;
}



/**
 * Loads the bucket range for the given age for the given business unit
 * id and inventory bucket id
 * @param age
 * @param businessUnitId
 * @param inventoryBucketType
 * @return InventoryBucketRange
 */
public InventoryBucketRange loadBucketForInventoryAge( Integer age, Integer businessUnitId, InventoryBucketTypeEnum inventoryBucketType )
{	
    final StringBuffer queryString = new StringBuffer();
    queryString.append( "select * from GetInventoryBucketRanges(" );
    queryString.append( businessUnitId );
    queryString.append( ", " );
    queryString.append( inventoryBucketType.getInventoryBucketId() );
    queryString.append( ")" );
	queryString.append( " where ");
	queryString.append( age );
	queryString.append( " between low and isNull(high, 999)" );
    
    List bucketRanges = (List) getHibernateTemplate().execute(
        new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {
	            return session.createSQLQuery( queryString.toString() )
	                          .addEntity( InventoryBucketRange.class )
	                          .list();	                                                                 	
	        }	
        }                                                             
    );
    
    if ( bucketRanges != null && bucketRanges.size() > 0 )
    {
        return (InventoryBucketRange) bucketRanges.get( 0 );
    }
    
    return null;
}

}
