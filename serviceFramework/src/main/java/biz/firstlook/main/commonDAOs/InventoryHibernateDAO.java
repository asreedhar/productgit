package biz.firstlook.main.commonDAOs;

import java.sql.SQLException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.entity.Inventory;
import biz.firstlook.main.persist.common.hibernate.GenericHibernate;

public class InventoryHibernateDAO extends GenericHibernate<Inventory, Integer>
        implements IInventoryDAO {

    public InventoryHibernateDAO() {
        super(Inventory.class);
    }

    public Inventory findByStockNumber(final String stockNum, final Integer businessUnitId) {
        
    	Inventory inv = (Inventory) getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session)throws HibernateException, SQLException {
                Criteria crit = session.createCriteria(Inventory.class).add(Restrictions.eq("stockNumber", stockNum))
                .add(Restrictions.eq("businessUnitId", businessUnitId))
                .add(Restrictions.eq("inventoryActive", true)); 
            return crit.uniqueResult();
            }
        });
        return inv;
    }

}
