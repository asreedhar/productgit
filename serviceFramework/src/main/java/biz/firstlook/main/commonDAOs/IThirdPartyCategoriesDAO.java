package biz.firstlook.main.commonDAOs;

import java.util.List;

import biz.firstlook.entity.ThirdPartyCategories;


public interface IThirdPartyCategoriesDAO
{
public List<ThirdPartyCategories> findAllByThirdPartyId( List<Integer> thirdPartyIds );
}
