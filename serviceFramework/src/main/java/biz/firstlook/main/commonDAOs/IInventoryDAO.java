package biz.firstlook.main.commonDAOs;

import biz.firstlook.entity.Inventory;
import biz.firstlook.main.persist.common.IGenericDAO;

public interface IInventoryDAO extends IGenericDAO<Inventory, Integer> {
    
    public Inventory findByStockNumber(String stockNum, Integer businessUnitId);
}
