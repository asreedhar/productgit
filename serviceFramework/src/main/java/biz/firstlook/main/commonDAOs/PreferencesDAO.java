package biz.firstlook.main.commonDAOs;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * this class retrieves ALL preferences that are needed by the app that are not being stored in the session
 * the 1 get method returns a map of <columnId, value>.  the @link biz.firstlook.main.commonServices.PreferenceService
 * then figures out what specific
 * preferences are needed for a given report
 */
public class PreferencesDAO extends JdbcDaoSupport implements IPreferencesDAO
{

public PreferencesDAO()
{
	super();
}

@SuppressWarnings("unchecked")
public Map<String, Object> getPreferences( Integer businessUnitId )
{
	StringBuilder sql = new StringBuilder();
	sql.append("SELECT dp.ShowLotLocationStatus, dp.ListPricePreference,");
	sql.append("dp.AverageInventoryAgeRedThreshold, dp.AverageDaysSupplyRedThreshold,");
	sql.append("dp.StockOrVinPreference, df.LastDMSReferenceDateUsed,");
	sql.append("dp.UnitsSoldThresholdInvOverview, dp.GuideBookID, dp.UseLotPrice, ");
	sql.append("dp.RepricePercentChangeThreshold, dp.RepriceConfirmation, dp.GuideBook2Id,");
	sql.append("dp.CheckAppraisalHistoryForIMPPlanning,");
	sql.append("dp.UnitCostThresholdUpper, dp.UnitCostThresholdLower,");
	sql.append("dp.DisplayUnitCostToDealerGroup, dp.AuctionAreaId,");
	sql.append("dp.AuctionTimePeriodId, bu.ZipCode,");
	sql.append("dr.HighMileageThreshold, dr.ExcessiveMileageThreshold");
	sql.append(" FROM DealerPreference dp");
	sql.append(" JOIN DealerFacts df on dp.BusinessUnitID = df.BusinessUnitID");
	sql.append(" JOIN BusinessUnit bu on dp.BusinessUnitID = bu.BusinessUnitID");
	sql.append(" JOIN DealerRisk dr on dp.BusinessUnitID = dr.BusinessUnitID");
	sql.append(" WHERE dp.BusinessUnitID = ?");
	
	Object[] parameterValues = { (Object)businessUnitId };
	return (Map<String, Object>)getJdbcTemplate().query( sql.toString(), parameterValues, new PreferenceRowMapper() );
}

@SuppressWarnings("unchecked")
public List< String > getMemberStatusCodePreferences( Integer memberId )
{
	Object[] parameterValues = { (Object)memberId };

	String sql = ""
			+ " Select 	InventoryStatusCD"
			+ " from 	map_MemberToInvStatusFilter filter" 
			+ "	where	memberId = ?";

	return (List< String >)getJdbcTemplate().query( sql, parameterValues, new StatusCodeRowMapper() );
}

public Integer getMemberInventoryOverviewSortOrderType(Integer memberId){
	Object[] parameterValues = { (Object)memberId };

	String sql = ""
			+ " Select 	InventoryOverviewSortOrderType"
			+ " from 	Member" 
			+ "	where	memberId = ?";

	return (Integer) getJdbcTemplate().query( sql, parameterValues, new SortByRowMapper() );
}

public void setMemberInventoryOverviewSortOrderType(Integer sortBy, Integer memberId){
	Object[] parameterValues = { sortBy, (Object)memberId };

	String sql = ""
			+ " update 	Member"
			+ " set InventoryOverviewSortOrderType = ?" 
			+ "	where	memberId = ?";

	getJdbcTemplate().update(sql, parameterValues);

}

/**
 * inner class that maps the results form the query to our display bean
 */
class PreferenceRowMapper implements ResultSetExtractor
{

public PreferenceRowMapper( )
{
}

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	Map<String, Object> preferencesDisplayBeans = new HashMap<String, Object>();

	ResultSetMetaData rsMetaData = rs.getMetaData();

	int columnCount = rsMetaData.getColumnCount();

	while ( rs.next() )
	{
		// 1 based for M$ SQL Server
		for ( int columnIndex = 1; columnIndex <= columnCount; columnIndex++ )
		{
			String name = rsMetaData.getColumnName( columnIndex );
			preferencesDisplayBeans.put( name, rs.getObject( columnIndex ) );
		}
	}

	return preferencesDisplayBeans;
}
}

class StatusCodeRowMapper implements ResultSetExtractor
{

public StatusCodeRowMapper( )
{
}

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	List< String > statusCodeDisplayBeans = new ArrayList< String >();

	while ( rs.next() )
	{
		statusCodeDisplayBeans.add( rs.getString(1) );
	}

	return statusCodeDisplayBeans;
}
}

class SortByRowMapper implements ResultSetExtractor
{

public SortByRowMapper( )
{
}
public Integer extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	Integer sortByBean = new Integer(0);
	while ( rs.next() )
	{	
		sortByBean = rs.getInt(1);
	}
	return sortByBean;
}
}

@SuppressWarnings("unchecked")
public Map<String, Object> fetchMMR() {
	StringBuilder sql = new StringBuilder();
	sql.append("select thirdparties.ThirdPartyID, thirdparties.Description from ThirdParties thirdparties");
	sql.append(" where thirdparties.ThirdPartyID = 6");
	return (Map<String, Object>)getJdbcTemplate().query( sql.toString(), new PreferenceRowMapper());
}

}
