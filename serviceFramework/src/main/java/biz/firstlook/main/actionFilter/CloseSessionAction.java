package biz.firstlook.main.actionFilter;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.AnyPredicate;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;

public class CloseSessionAction extends Action
{

private static final Logger logger = Logger.getLogger( NextGenAction.class );

private static AnyPredicate arePopups;
private static final EqualPredicate areJavascriptRefs = new EqualPredicate( "/IMT/javascript" ); 

static {
	List<Predicate> popups = new ArrayList<Predicate>(); 
	popups.add( new EqualPredicate("/IMT/EStock.go") );
	popups.add( new EqualPredicate("/IMT/PopUpPlusDisplayAction.go") );
	popups.add( new EqualPredicate("/ucbp/TileManagementCenter.go") );
	arePopups = new AnyPredicate(popups.toArray(new Predicate[popups.size()]));
}

public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{
	String destination = parseDestination(request);
	if ( destination != null && !areJavascriptRefs.evaluate(destination) )
	{
		if( !arePopups.evaluate(destination))
		{
			HttpSession httpSession = request.getSession( false );
			if ( httpSession == null )
			{
				logger.error( "the HttpSession was null while trying to log out of NextGen." );
			}
			else
			{
				if ( logger.isDebugEnabled() )
					logger.debug( "Invalidating user session ID: " + httpSession.getId() + " on " + InetAddress.getLocalHost() );
	
				httpSession.invalidate();
			}
		}
		 
		if ( logger.isDebugEnabled() )
		{
			logger.debug( "Redirecting User to: " + destination );
		}
		
		request.setAttribute( "scheme", HttpServletRequestHelper.getScheme(request) );
		request.setAttribute( "context", "IMT" );
		request.setAttribute( "forwardTo", destination );
		return mapping.findForward( "success" );
	}
	return mapping.findForward( "error" );
}

private String parseDestination(HttpServletRequest request) {
	
	String destination = request.getQueryString();

	// remove the 'forwardTo' Prefix fromt he querystring
	destination = destination.split( "forwardTo=" )[1];
	// remove the ticket= argument added by cas if there is one (it will always be the last param)
	destination = destination.split( "ticket=" )[0];
	
	//	hacky, but urlrewrite regex always appends a &.
	// try parsing forwardTo=/DealerHomeDisplayAction.go&&ticket=ST-299-YkPyxFvOpQautT7xdOPrB5SgvHjxF9ICkIn-20 (beta testing)
	while( destination.endsWith( "&" ) ) {
		destination = destination.substring( 0, destination.length() - 1 );
	}
	int amp = destination.indexOf('&');
	if (amp >= 0)
	{
		destination = destination.substring( 0, amp ) + "?" + destination.substring( amp+1, destination.length() );
		
	}
	if( destination.startsWith("/") ) {
		destination = destination.substring(1);
	} else if (destination.startsWith("%2F")) {
		destination = destination.substring(3);
	}
	logger.debug(destination);
	return destination;
}

}
