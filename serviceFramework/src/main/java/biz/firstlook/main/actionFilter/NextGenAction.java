package biz.firstlook.main.actionFilter;

import java.net.InetAddress;
import java.util.IllegalFormatException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.commons.util.SystemErrorBean;
import biz.firstlook.commons.util.SystemErrorUtil;
import biz.firstlook.main.commonDAOs.INextGenSessionDAO;
import biz.firstlook.main.commonDAOs.NoNextGenSessionDataException;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.SoftwareSystemComponentState;
import biz.firstlook.module.core.service.CoreService;
import biz.firstlook.planning.service.InternetAdvertiserService;

import com.discursive.cas.extend.client.CASFilter;

/**
 * this class serves the same purpose as baseAction + secureBaseAction from old IMT its takes in a request and adds our session variable to the
 * request (if its not already there) all nextGen Actions should extend this class.
 * 
 * Note: you know a user is logged in when a request gets here because it is a CAS protected page
 */
public abstract class NextGenAction extends Action
{

private static final String DEALER_SYSTEM_COMPONENT = "DEALER_SYSTEM_COMPONENT";

public static final String HAL_SESSION_ID = "HALSESSIONID";

protected final static String GOOGLE_ANALYTICS_ACCOUNT = "google.analytics.account";
protected final static String GOOGLE_ANALYTICS_ACCOUNT_NO = PropertyLoader.getProperty(GOOGLE_ANALYTICS_ACCOUNT);

private static final Logger logger = Logger.getLogger( NextGenAction.class );
private INextGenSessionDAO nextGenSessionDAO;
private CoreService coreService;
private InternetAdvertiserService internetAdvertiserService;

public NextGenAction()
{
	super();
}

public abstract ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception; // DatabaseException, ApplicationException;

public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws Exception
{	
	request.setAttribute("googleAnalyticsAccount", GOOGLE_ANALYTICS_ACCOUNT_NO);
	long requestStart = System.currentTimeMillis();
	ActionForward forward = null;

	// check to see if session already exists, if not, create new session
	HttpSession session = request.getSession();

	if ( logger.isDebugEnabled() )
		logger.debug( "User's NextGen sessionID: " + session.getId() + " on " + InetAddress.getLocalHost() );

	NextGenSession nextGenSession = (NextGenSession)session.getAttribute( NextGenSession.NEXT_GEN_SESSION );
	Integer businessUnitId = getBusinessUnitIdFromCookie( request );
	if ( nextGenSession == null || nextGenSession.getBusinessUnitId() == null || !nextGenSession.getBusinessUnitId().equals( businessUnitId ))
	{
		try {
			populateNextGenSession( request );
		}
		catch (NoNextGenSessionDataException e) {
			BusinessUnitFilter.redirectToLogin(request, response, businessUnitId);
			return null;
		}
	}

	try
	{
		forward = justDoIt( mapping, form, request, response );
		baseActionLogging( session, requestStart );
	}
	catch ( Throwable th )
	{
		// log error to the DB SytemError table
		SystemErrorBean systemError = null;
		if ( nextGenSession != null )
		{
			systemError = new SystemErrorBean(th, nextGenSession.getBusinessUnitId(), nextGenSession.getMemberId(),
			                                  "ProgramTypeID: " + nextGenSession.getProgramTypeCode(),
			                                  session.getId() + " on " + InetAddress.getLocalHost(),
			                                  nextGenSession.getHalSessionId(), request.getRequestURI() );
		} else
		{
			systemError = new SystemErrorBean(th, null, null, null, session.getId() + " on " + InetAddress.getLocalHost(),
			                                  null, request.getRequestURI() );
		}
		
		Integer systemErrorId = new Integer(0);
		
		try {
			// save the exception in the fault database by making the call to the web service
			systemErrorId = SystemErrorUtil.save(systemError, request);
		}
		catch (Throwable t) {
			// ignore
		}
		
		request.setAttribute( "SystemErrorId", "FL" + systemErrorId );
		request.setAttribute( "UncaughtException", th );
		logger.error( systemError.toString() );		
		response.setStatus(500);
		forward = mapping.findForward( "error" );
	}

	request.setAttribute( "actionPath", request.getServletPath().substring( 1 ) );
	response.addHeader( "Cache-Control", "no-cache" );
	response.addHeader( "Pragma", "no-cache" );
	
	return forward;
}

private void baseActionLogging( HttpSession session, long requestStart )
{
	if ( logger.isDebugEnabled() )
	{
		long responseStart = System.currentTimeMillis();
		String msg = this.getClass().getSimpleName() + " took " + ( responseStart - requestStart ) + "ms ";

		msg += " username: " + (String)session.getAttribute( CASFilter.CAS_FILTER_USER );

		logger.debug( msg );
	}
}

private void populateNextGenSession( HttpServletRequest request )
{
	HttpSession session = request.getSession();

	String username = (String)session.getAttribute( CASFilter.CAS_FILTER_USER );
	Integer businessUnitId = getBusinessUnitIdFromCookie( request );
	if (businessUnitId == null) {
		SoftwareSystemComponentState state = getCoreService().findOrCreateSoftwareSystemComponentState(username, DEALER_SYSTEM_COMPONENT);
		if (state != null) {
			if (state.getDealer() != null) {
				businessUnitId = state.getDealer().getBusinessUnitID();
			}
		}
	}
	
	NextGenSession nextGenSession = getNextGenSessionDAO().getNextGenSession( username, businessUnitId );
	nextGenSession.setInternetAdvertisersEnabled( internetAdvertiserService.internetAdvertisersEnabled( businessUnitId ) );
	
	String halSessionId = request.getParameter( HAL_SESSION_ID );
	if( halSessionId == null ) {
		//check the cookie
		halSessionId = getPropertyFromCookie( request, "_session_id" );
	}
	Integer businessUnitIdFromHal = getNextGenSessionDAO().isFromMax( halSessionId );
	if( businessUnitIdFromHal != null ) {
		nextGenSession.setHalSessionId( halSessionId );
	}
	
	request.getSession().setAttribute( NextGenSession.NEXT_GEN_SESSION, nextGenSession );
}

protected Member getMember(HttpServletRequest request) {
	Member member = null;
	NextGenSession nSession = getNextGenSession(request);
	if(nSession != null) {
		member = getCoreService().findMemberByID(nSession.getMemberId());
		SoftwareSystemComponentState state = getCoreService().findOrCreateSoftwareSystemComponentState(member.getLogin(), DEALER_SYSTEM_COMPONENT);
		if (state != null) {
			BusinessUnit dealer = state.getDealer(); 
			if(dealer == null) {
				member.setCurrentBusinessUnit(state.getDealerGroup());
			} else {
				member.setCurrentBusinessUnit(dealer);
			}
		}
	}
	return member;	
}

protected Integer getBusinessUnitIdFromCookie( HttpServletRequest request )
{
	String businessUnitIdFromCookie = getPropertyFromCookie( request, "dealerId" );
	
	Integer businessUnitId = null;
	
	try{
		businessUnitId = Integer.parseInt( businessUnitIdFromCookie );
	} catch( NumberFormatException nfe ) {
		logger.error( " no cookies set for this session!" );
	}
	
	return businessUnitId;
}

private String getPropertyFromCookie( HttpServletRequest request, String property )
{
	Cookie[] cookies = request.getCookies();
	if ( cookies == null )
	{
		logger.error( " no cookies set on this request!!!!! " );
		return null;
	}
	for( Cookie cookie : cookies)
	{
		if( cookie.getName().equals( property ) )
			return cookie.getValue();
	}
	
	return null;
}

protected NextGenSession getNextGenSession( HttpServletRequest request )
{
	HttpSession session = request.getSession(false);
	
	if ( session == null )
		return null;

	return (NextGenSession) session.getAttribute( NextGenSession.NEXT_GEN_SESSION );
}

/**
 * <p><em>DO NOT USE THIS METHOD</em></p>
 * <p>Try and parse an int from a named request-parameter failing over to look at a similarly
 * named request-attribute. If an integer was not found either as a request-parameter or request-
 * attribute a runtime exception is thrown.</p>
 * <p>This method is very dangerous and should not be used. Either manual or struts validation
 * should be used to protect the action code from missing values. Additionally, you <em>should
 * know</em> where the value is coming from so the fail-over logic is entirely ridiculous.<p>
 * @param request the request from which the request-parameter or session-attribute is taken
 * @param parameterName the name (or key) of the resource 
 * @return an integer value from the first available resource
 * @throws RuntimeException if an integer value was not found on the request or session
 * @throws IllegalArgumentException if request or parameterName are null
 * @deprecated since 07/11/2007 by sbw as it's god-awful and bad-code all round. and no, i did not write it originally. 
 */
public static int getInt( HttpServletRequest request, String parameterName )
{
	// validate arguments
	if (request == null) {
		throw new IllegalArgumentException("Parameter 'request' cannot be null.");
	}
	if (parameterName == null) {
		throw new IllegalArgumentException("Parameter 'parameterName' cannot be null.");
	}
	// extract variables
	final String parameter = request.getParameter(parameterName);
	final Object attribute = request.getAttribute(parameterName);
	// try request parameter
	Integer integer = Functions.nullSafeInteger(parameter);
	// try request attribute
	if (integer == null) {
		integer = Functions.nullSafeInteger(attribute);
	}
	// throw an exception if neither produced an integer
	if (integer == null) {
		String msg = "DE981: Neither request-parameter nor request-attribute were integers";
		try {
			msg = String.format("DE981: Neither the request-parameter '%s' nor the request-attribute '%s' for the key '%s' are integers", parameter, attribute, parameterName);
		}
		catch (IllegalFormatException ex) {
			// ignore
		}
		throw new RuntimeException(msg); // is there a better exception type to throw?
	}
	// else return the int value
	else {
		return integer.intValue();
	}
}

public INextGenSessionDAO getNextGenSessionDAO()
{
	return nextGenSessionDAO;
}

public void setNextGenSessionDAO( INextGenSessionDAO nextGenSessionDAO )
{
	this.nextGenSessionDAO = nextGenSessionDAO;
}

public CoreService getCoreService() {
	return coreService;
}
public void setCoreService(CoreService coreService) {
	this.coreService = coreService;
}

public InternetAdvertiserService getInternetAdvertiserService() {
	return internetAdvertiserService;
}
public void setInternetAdvertiserService(InternetAdvertiserService internetAdvertiserService) {
	this.internetAdvertiserService = internetAdvertiserService;
}

}
