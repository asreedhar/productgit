package biz.firstlook.main.actionFilter;

import java.io.Serializable;

import biz.firstlook.main.enumerator.ProgramTypeEnum;

/**
 * this class is a bean that hold dealer and businessUnitId that is used all
 * over the site and is always stored in session
 */
public class NextGenSession implements Serializable {

    private static final long serialVersionUID = -3767752466628069614L;
    public static final String NEXT_GEN_SESSION = "nextGenSession";

    // general info
    private Integer memberId;
    private String dealerNickName;
    private Integer businessUnitId;
    private boolean lithiaStore;
    private boolean includePerformanceManagementCenter;
    private boolean showLotLocationAndStatus;
    private Integer aipRunDayOfWeek;
    private Boolean bucketJumperAIPApproach;
    private Integer parentId;
    private Integer flashLocatorHideUnitCostDays;
    private Integer programTypeCode; // indicates VIP, Insight, Dealers
                                        // Resource...
    // guide book preferences
    private Integer primaryGuideBookId;
    private Integer primaryGuideBookFirstPreferenceId;
    private Integer primaryGuideBookSecondPreferenceId;
    private Integer secondaryGuideBookId;
    private Integer secondaryGuideBookFirstPreferenceId;
    private Integer secondaryGuideBookSecondPreferenceId;

    // nav bar prefences and upgrades:
    private boolean reducedToolsMenu;
    private boolean includeAgingPlan;
    private boolean includeCIA;
    private boolean includeRedistribution;
    private boolean includePerformanceDashboard;
    private boolean includeAppraisal;
    private boolean includeAnnualRoi;
    private boolean includeEquityAnalyzer;
    private boolean includeAuctionData;
    private boolean includeKBBTradeIn;
    private boolean includeNADAValues;
    private boolean includeNavigatorVIPAndDealersResource;
    private boolean edmundsTmvUpgrade;
    private boolean pingUpgrade;
    private boolean internetAdvertisersEnabled;
    private boolean includeJDPowerUCMData;
    private boolean pingIIUpgrade;
    private boolean includeMakeADeal;
    private boolean includeMarketStockingGuide;
    private boolean showInTransitInventoryForm;
    
    private boolean includeTransferPricing;
    
    private String halSessionId;
	private String zipCode;

    public NextGenSession() {
        super();
    }

    public Integer getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getPrimaryGuideBookFirstPreferenceId() {
        return primaryGuideBookFirstPreferenceId;
    }

    public void setPrimaryGuideBookFirstPreferenceId(
            Integer primaryGuideBookFirstPreferenceId) {
        this.primaryGuideBookFirstPreferenceId = primaryGuideBookFirstPreferenceId;
    }

    public Integer getPrimaryGuideBookSecondPreferenceId() {
        return primaryGuideBookSecondPreferenceId;
    }

    public void setPrimaryGuideBookSecondPreferenceId(
            Integer primaryGuideBookSecondPreferenceId) {
        this.primaryGuideBookSecondPreferenceId = primaryGuideBookSecondPreferenceId;
    }

    public Integer getSecondaryGuideBookFirstPreferenceId() {
        return secondaryGuideBookFirstPreferenceId;
    }

    public void setSecondaryGuideBookFirstPreferenceId(
            Integer secondaryGuideBookFirstPreferenceId) {
        this.secondaryGuideBookFirstPreferenceId = secondaryGuideBookFirstPreferenceId;
    }

    public Integer getSecondaryGuideBookSecondPreferenceId() {
        return secondaryGuideBookSecondPreferenceId;
    }

    public void setSecondaryGuideBookSecondPreferenceId(
            Integer secondaryGuideBookSecondPreferenceId) {
        this.secondaryGuideBookSecondPreferenceId = secondaryGuideBookSecondPreferenceId;
    }

    public String getDealerNickName() {
        return dealerNickName;
    }

    public void setDealerNickName(String dealerNickName) {
        this.dealerNickName = dealerNickName;
    }

    public boolean isReducedToolsMenu() {
        return reducedToolsMenu;
    }

    public void setReducedToolsMenu(boolean reducedToolsMenu) {
        this.reducedToolsMenu = reducedToolsMenu;
    }

    public boolean isIncludeAgingPlan() {
        return includeAgingPlan;
    }

    public void setIncludeAgingPlan(boolean includeAgingPlan) {
        this.includeAgingPlan = includeAgingPlan;
    }

    public boolean isIncludeCIA() {
        return includeCIA;
    }

    public void setIncludeCIA(boolean includeCIA) {
        this.includeCIA = includeCIA;
    }

    public boolean isIncludeRedistribution() {
        return includeRedistribution;
    }

    public void setIncludeRedistribution(boolean includeRedistribution) {
        this.includeRedistribution = includeRedistribution;
    }

    public boolean isIncludePerformanceDashboard() {
        return includePerformanceDashboard;
    }

    public void setIncludePerformanceDashboard(
            boolean includePerformanceDashboard) {
        this.includePerformanceDashboard = includePerformanceDashboard;
    }

    public boolean isIncludeAppraisal() {
        return includeAppraisal;
    }

    public void setIncludeAppraisal(boolean includeAppraisal) {
        this.includeAppraisal = includeAppraisal;
    }

    public boolean isLithiaStore() {
        return lithiaStore;
    }

    public void setLithiaStore(boolean lithiaStore) {
        this.lithiaStore = lithiaStore;
    }

    public boolean isIncludePerformanceManagementCenter() {
		return includePerformanceManagementCenter;
	}

	public void setIncludePerformanceManagementCenter(boolean includePerformanceManagementCenter) {
		this.includePerformanceManagementCenter = includePerformanceManagementCenter;
	}

	public boolean showAnnualRoi() {
        boolean showAnnualRoi = false;
        if (isLithiaStore() || isIncludeAnnualRoi()) {
            showAnnualRoi = true;
        }
        return showAnnualRoi;
    }

    public boolean isIncludeAnnualRoi() {
        return includeAnnualRoi;
    }

    public void setIncludeAnnualRoi(boolean includeAnnualRoi) {
        this.includeAnnualRoi = includeAnnualRoi;
    }

    public boolean isShowLotLocationAndStatus() {
        return showLotLocationAndStatus;
    }

    public void setShowLotLocationAndStatus(boolean showLotLocationAndStatus) {
        this.showLotLocationAndStatus = showLotLocationAndStatus;
    }

    public Integer getAipRunDayOfWeek() {
        return aipRunDayOfWeek;
    }

    public void setAipRunDayOfWeek(Integer aipRunDayOfWeek) {
        this.aipRunDayOfWeek = aipRunDayOfWeek;
    }

    public Boolean getBucketJumperAIPApproach() {
        return bucketJumperAIPApproach;
    }

    public void setBucketJumperAIPApproach(Boolean bucketJumperAIPApproach) {
        this.bucketJumperAIPApproach = bucketJumperAIPApproach;
    }

    public boolean isIncludeEquityAnalyzer() {
        return includeEquityAnalyzer;
    }

    public void setIncludeEquityAnalyzer(boolean includeEquityAnalyzer) {
        this.includeEquityAnalyzer = includeEquityAnalyzer;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getFlashLocatorHideUnitCostDays() {
        return flashLocatorHideUnitCostDays;
    }

    public void setFlashLocatorHideUnitCostDays(
            Integer flashLocatorHideUnitCostDays) {
        this.flashLocatorHideUnitCostDays = flashLocatorHideUnitCostDays;
    }

    public boolean isIncludeAuctionData() {
        return includeAuctionData;
    }

    public void setIncludeAuctionData(boolean includeAuctionData) {
        this.includeAuctionData = includeAuctionData;
    }

    public Integer getProgramTypeCode() {
        return programTypeCode;
    }

    public void setProgramTypeCode(Integer programTypeCode) {
        this.programTypeCode = programTypeCode;
    }

    public boolean isVIPStore() {
        return ProgramTypeEnum.VIP.equals(ProgramTypeEnum
                .getByProgramTypeCode(getProgramTypeCode()));
    }

    public boolean isEdgeStore() {
        return ProgramTypeEnum.EDGE.equals(ProgramTypeEnum
                .getByProgramTypeCode(getProgramTypeCode()));
    }

    public boolean isFLStore() {
        return ProgramTypeEnum.FIRSTLOOK.equals(ProgramTypeEnum
                .getByProgramTypeCode(getProgramTypeCode()));
    }

    public boolean getHasBuyingPlan() {
        return isIncludeCIA();
    }

    public boolean isDealersResourceStore() {
        return ProgramTypeEnum.DEALERS_RESOURCE.equals(ProgramTypeEnum
                .getByProgramTypeCode(getProgramTypeCode()));
    }

    public boolean isIncludeNavigatorVIPAndDealersResource() {
        return includeNavigatorVIPAndDealersResource;
    }

    public void setIncludeNavigatorVIPAndDealersResource(
            boolean includeNavigatorVIPAndDealersResource) {
        this.includeNavigatorVIPAndDealersResource = includeNavigatorVIPAndDealersResource;
    }

    public boolean getHasNavigator()
    {   //Yes there is a lot here.  Due to odd requirements about vip upgrades.  VIP stores have flusan,
        //but the navigator is an upgrade. However, if they get purchasing center(CIA) upgrade they 
        //have full access to the flusan.
        return isEdgeStore() || isFLStore() || isIncludeNavigatorVIPAndDealersResource() || isIncludeCIA(); 
    }

    public boolean getHasPingUpgrade() {
        return pingUpgrade;
    }

    public void setPingUpgrade(boolean pingUpgrade) {
        this.pingUpgrade = pingUpgrade;
    }

    public Integer getPrimaryGuideBookId() {
        return primaryGuideBookId;
    }

    public void setPrimaryGuideBookId(Integer primaryGuideBookId) {
        this.primaryGuideBookId = primaryGuideBookId;
    }

    public Integer getSecondaryGuideBookId() {
        return secondaryGuideBookId;
    }

    public void setSecondaryGuideBookId(Integer secondaryGuideBookId) {
        this.secondaryGuideBookId = secondaryGuideBookId;
    }

    public boolean isInternetAdvertisersEnabled() {
        return internetAdvertisersEnabled;
    }

    public void setInternetAdvertisersEnabled(boolean internetAdvertisersEnabled) {
        this.internetAdvertisersEnabled = internetAdvertisersEnabled;
    }

    public boolean isIncludeJDPowerUCMData() {
		return includeJDPowerUCMData;
	}

	public void setIncludeJDPowerUCMData( boolean includeJDPowerUCMData ) {
		this.includeJDPowerUCMData = includeJDPowerUCMData;
	}

	public boolean isIncludeMakeADeal() {
		return includeMakeADeal;
	}

	public void setIncludeMakeADeal(boolean includeMakeADeal) {
		this.includeMakeADeal = includeMakeADeal;
	}

	public boolean isIncludeMarketStockingGuide() {
		return includeMarketStockingGuide;
	}

	public void setIncludeMarketStockingGuide(boolean includeMarketStockingGuide) {
		this.includeMarketStockingGuide = includeMarketStockingGuide;
	}
	
	public boolean isShowInTransitInventoryForm() {
		return showInTransitInventoryForm;
	}

	public void setShowInTransitInventoryForm(boolean showInTransitInventoryForm) {
		this.showInTransitInventoryForm = showInTransitInventoryForm;
	}

public String getHalSessionId()
{
	return halSessionId;
}
/**
 * Keep this package scoped, no one should be setting this!
 * @param halSessionId
 */
void setHalSessionId( String halSessionId )
{
	this.halSessionId = halSessionId;
}

public boolean isIncludeNADAValues() {
	return includeNADAValues;
}

public void setIncludeNADAValues(boolean includeNADAValues) {
	this.includeNADAValues = includeNADAValues;
}

public boolean isIncludeKBBTradeIn()
{
	return includeKBBTradeIn;
}

public void setIncludeKBBTradeIn( boolean includeKBBTradeIn )
{
	this.includeKBBTradeIn = includeKBBTradeIn;
}

public String getZipCode() {
	return zipCode;
}

public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}

public boolean getHasPingIIUpgrade() {
	return pingIIUpgrade;
}

public void setPingIIUpgrade(boolean pingIIUpgrade) {
	this.pingIIUpgrade = pingIIUpgrade;
}

public void setEdmundsTmvUpgrade(boolean edmundsTmvUpgrade) {
	this.edmundsTmvUpgrade = edmundsTmvUpgrade;
}

public boolean getHasEdmundsTmvUpgrade() {
	return this.edmundsTmvUpgrade;
}

public boolean isIncludeTransferPricing() {
	return includeTransferPricing;
}

public void setIncludeTransferPricing(boolean includeTransferPricing) {
	this.includeTransferPricing = includeTransferPricing;
}




}
