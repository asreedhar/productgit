package biz.firstlook.main.actionFilter;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import biz.firstlook.commons.filter.RequestWithCookie;
import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;
import biz.firstlook.commons.util.CookieHelper;
import biz.firstlook.module.core.BusinessUnit;
import biz.firstlook.module.core.Member;
import biz.firstlook.module.core.SoftwareSystemComponentState;
import biz.firstlook.module.core.service.CoreService;

import com.discursive.cas.extend.client.CASFilter;

/**
 * Filter to ensure we have a dealerId cookie for subsequent filters to consume.  When there is
 * no such cookie present we check the session for a login which we use as a map to business
 * units. When a single business unit results we create a cookie and add it to the response and
 * wrap the request such that it is available to subsequent consumers. When zero or many business
 * units result from the mapping we redirect the user to the IMT login action.
 * @author swenmouth
 */
public class BusinessUnitFilter implements Filter {

	private CoreService coreService;
	
	public void destroy() {
		coreService = null;
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request   = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpSession session = request.getSession(false);
		// flow control variables
		boolean success = true;
		boolean insertOrUpdateCookie = false;
		// get member
		String login = (String) (session == null ? null : session.getAttribute(CASFilter.CAS_FILTER_USER));
		Member member = null;
		if (login != null) {
			try {
				member = coreService.findMemberByLogin(login);
				response.addCookie(CookieHelper.createTrueSightMemberCookie(member.getLogin()));
			}
			catch (Throwable t) {
				Logger.getLogger(BusinessUnitFilter.class).error(t);
				success = false;
			}
		}
		else {
			success = false;
		}
		// check business unit id from cookie
		Integer businessUnitID = getBusinessUnitId(request);
		if (success) {
			// when the business unit is not valid go find a valid one ...
			if (!isValidBusinessUnitForMember(member, businessUnitID)) {
				success = false;
				insertOrUpdateCookie = true;
				try {
					SoftwareSystemComponentState state = coreService.findOrCreateSoftwareSystemComponentState(login, "DEALER_SYSTEM_COMPONENT");
					if (state != null) {
						if (state.getDealer() != null) {
							businessUnitID = state.getDealer().getBusinessUnitID();
							success = true;
						}
					}
					if (businessUnitID == null) {
						Collection<BusinessUnit> businessUnits = member.getBusinessUnits();
						if (businessUnits.size() == 1) {
							businessUnitID = businessUnits.iterator().next().getBusinessUnitID();
							success = true;
						}
					}
				}
				catch (Throwable t) {
					Logger.getLogger(BusinessUnitFilter.class).error(t);
				}
			}
		}
		// end game logic
		if (success) {
			// change cookie where needed
			if (insertOrUpdateCookie) {
				Cookie cookie = new Cookie("dealerId", businessUnitID.toString());
				cookie.setPath("/");
				response.addCookie(cookie);
				request = new RequestWithCookie(request, cookie);
				
				BusinessUnit dealer = null;
				for(BusinessUnit businessUnit: member.getBusinessUnits()) {
					if(businessUnit.getBusinessUnitID().equals(businessUnitID)) {
						dealer = businessUnit;
						break;
					}
				}
				if(dealer != null) {
					response.addCookie(CookieHelper.createTrueSightDealerCookie(dealer.getName()));
					if(dealer.getParent() != null) {
						response.addCookie(CookieHelper.createTrueSightDealerGroupCookie(dealer.getParent().getName()));
					}
				}
			}
			// allow request to continue
			chain.doFilter(request, response);
		}
		else {
			redirectToLogin(request, response, businessUnitID);
		}
	}

	public static void redirectToLogin(HttpServletRequest request, HttpServletResponse response, Integer businessUnitID) throws IOException {
		// delete an erroneous cookie from the browser
		if (businessUnitID != null) {
			Cookie cookie = new Cookie("dealerId", businessUnitID.toString());
			cookie.setPath("/");
			cookie.setMaxAge(0);
			response.addCookie(cookie);
		}
		// redirect to login action  
		StringBuilder sb = new StringBuilder();
		sb.append(HttpServletRequestHelper.getScheme(request));
		sb.append("://");
		sb.append(HttpServletRequestHelper.getHost(request));
		sb.append("/IMT/LoginAction.go");
		response.sendRedirect(response.encodeRedirectURL(sb.toString()));
	}

	private boolean isValidBusinessUnitForMember(Member member, Integer businessUnitID) {
		boolean isValidBusinessUnitForMember = (member.getMemberType().intValue() == 1 && businessUnitID != null);
		if (businessUnitID != null) {
			for (BusinessUnit businessUnit : member.getBusinessUnits()) {
				if (businessUnit.getBusinessUnitID().equals(businessUnitID)) {
					isValidBusinessUnitForMember = true;
					break;
				}
			}
		}
		return isValidBusinessUnitForMember;
	}

	public void init(FilterConfig config) throws ServletException {
		try {
			coreService = (CoreService) WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext()).getBean("coreService");
		}
		catch (Throwable t) {
			throw new ServletException("Could not configure CoreService!!!", t);
		}
	}

	private Integer getBusinessUnitId(HttpServletRequest request) {
		try {
			return Integer.parseInt(getPropertyFromCookie(request, "dealerId"));
		}
		catch (Throwable t) {
			// ignore it
		}
		return null;
	}

	private String getPropertyFromCookie(HttpServletRequest request, String property) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null ) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(property))
					return cookie.getValue();
			}
		}
		return null;
	}	
}
