package biz.firstlook.main.actionFilter;

import java.io.IOException;
import java.util.ConcurrentModificationException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.transaction.TransactionRolledbackException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import biz.firstlook.main.database.util.HibernateSession;

public class ApplicationTransactionFilter extends OncePerRequestFilter implements Filter, HttpSessionListener
{
public static final String DEFAULT_SESSION_FACTORY_BEAN_NAME = "sessionFactory";
private String sessionFactoryBeanName = DEFAULT_SESSION_FACTORY_BEAN_NAME;

private final static Log log = LogFactory.getLog( ApplicationTransactionFilter.class );
private final static String SESSION = Session.class.getName();

private boolean m_CloseSession = true;
private boolean m_OpenNewSession = true;
private boolean m_NeedsHibernateSession = true;

private static class SessionWrapper
{
private final transient Session m_Session;
private boolean m_IsAllocated = false;

public SessionWrapper( Session session )
{
	m_Session = session;
}

public Session getSession()
{
	try
	{
		if ( m_IsAllocated )
		{
			throw new ConcurrentModificationException( "Hibernate Session is not reentrant" );
		}

		return m_Session;
	}
	finally
	{
		m_IsAllocated = true;
	}
}

public boolean isSessionOpened()
{
	return m_Session.isOpen();
}

public void resetIsAllocated()
{
	m_IsAllocated = false;
}
}

/**
 * Look up the SessionFactory that this filter should use.
 * <p>
 * Default implementation looks for a bean with the specified name in Spring's
 * root application context.
 * 
 * @return the SessionFactory to use
 * @see #getSessionFactoryBeanName
 */
protected SessionFactory lookupSessionFactory()
{
	if ( logger.isDebugEnabled() )
	{
		logger.debug( "Using SessionFactory '" + getSessionFactoryBeanName() + "' for ApplicationTransactionFilter" );
	}
	WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext( getServletContext() );
	return (SessionFactory)wac.getBean( getSessionFactoryBeanName(), SessionFactory.class );
}

protected void doFilterInternal( HttpServletRequest request, HttpServletResponse response, FilterChain chain ) throws ServletException,
		IOException
{
	try
	{
		restoreSession( request.getSession( false ) );

		chain.doFilter( request, response );

		if ( HibernateSession.getRollbackOnly() )
		{
			throw new TransactionRolledbackException( "Rollback Only Flag is true" );
		}

		HibernateSession.doTransactionCommit();
	}
	catch ( ServletException e )
	{
		manageException( ( e.getRootCause() != null ) ? e.getRootCause() : e );
	}
	catch ( Exception e )
	{
		manageException( ( e.getCause() != null ) ? e.getCause() : e );
	}
	finally
	{
		if ( isNeedsHibernateSession() )
		{
			if ( !isCloseSession() )
			{
				HibernateSession.resetThreadSession();
			}
			else
			{
				HibernateSession.closeSession();
			}

			if ( request.getSession( false ) != null && request.getSession( false ).getAttribute( SESSION ) != null )
			{
				( (SessionWrapper)request.getSession( false ).getAttribute( SESSION ) ).resetIsAllocated();
			}
		}
	}
}

private void manageException( Throwable throwable ) throws ServletException
{
	HibernateSession.doTransactionRollback();

	log.error( throwable.getMessage(), throwable );
	if ( !HibernateSession.getRollbackOnly() )
	{
		throw new ServletException( throwable.getMessage(), throwable );
	}
}

private void restoreSession( HttpSession httpSession ) throws HibernateException
{
	if ( httpSession != null )
	{
		SessionWrapper sessionWrapper = (SessionWrapper)httpSession.getAttribute( SESSION );
		HibernateSession.setSessionFactory( lookupSessionFactory() );

		// If a new Hibernate session must be opened
		if ( isOpenNewSession() )
		{
			// Then check to see if one already exists in the http session and
			// close it
			if ( sessionWrapper != null && sessionWrapper.isSessionOpened() )
			{
				HibernateSession.restoreSession( sessionWrapper.getSession() );
				HibernateSession.closeSession();
			}
			// If the new hibernate session is not be discarded at the end of
			// this action,
			// then put it in the http session after opening it
			if ( !isCloseSession() )
			{
				httpSession.setAttribute( SESSION, new SessionWrapper( HibernateSession.getSession() ) );
			}
			// Otherwise, just open it without setting it in the http session
			else
			{
				HibernateSession.getSession();
			}
		}
		// Otherwise, just reconnect the existing hibernate session (if it
		// exists) or
		// create a new one if it doesn't
		else if ( isNeedsHibernateSession() )
		{
			if ( sessionWrapper != null && sessionWrapper.isSessionOpened() )
			{
				HibernateSession.restoreSession( sessionWrapper.getSession() );
			}
			else
			{
				httpSession.setAttribute( SESSION, new SessionWrapper( HibernateSession.getSession() ) );
			}
		}
		// If this servlet does not need access to a Hibernate session, it will
		// simply clean up any existing ones
		else
		{
			if ( sessionWrapper != null && sessionWrapper.isSessionOpened() )
			{
				HibernateSession.restoreSession( sessionWrapper.getSession() );
				HibernateSession.closeSession();
			}
		}
	}
}

public void sessionCreated( HttpSessionEvent sessionEvent )
{
}

public void sessionDestroyed( HttpSessionEvent sessionEvent )
{
	HttpSession session = sessionEvent.getSession();

	destroySession( session );
}

public static void destroySession( HttpSession session )
{
	if ( session != null && session.getAttribute( SESSION ) != null )
	{
		SessionWrapper sessionWrapper = (SessionWrapper)session.getAttribute( SESSION );

		try
		{
			HibernateSession.restoreSession( sessionWrapper.getSession() );
		}
		catch ( Exception e )
		{
			log.warn( "cannot restore HttpSession " + session + ", " + e.getMessage() );
		}
	}

	HibernateSession.closeSession();

	if ( session != null )
	{
		try
		{
			session.removeAttribute( SESSION );
		}
		catch ( Exception e )
		{
			log.warn( "cannot remove Session " + session + ", " + e.getMessage() );
		}
	}
}

private boolean isCloseSession()
{
	return m_CloseSession;
}

public void setCloseSession( boolean closeSession )
{
	m_CloseSession = closeSession;
}

private boolean isOpenNewSession()
{
	return m_OpenNewSession;
}

public void setOpenNewSession( boolean openNewSession )
{
	m_OpenNewSession = openNewSession;
}

protected boolean shouldNotFilter( HttpServletRequest request ) throws ServletException
{
	return HibernateSession.isSessionBoundToThread();
}

public String getSessionFactoryBeanName()
{
	return sessionFactoryBeanName;
}

public void setSessionFactoryBeanName( String sessionFactoryBeanName )
{
	this.sessionFactoryBeanName = sessionFactoryBeanName;
}

public boolean isNeedsHibernateSession()
{
	return m_NeedsHibernateSession;
}

public void setNeedsHibernateSession( boolean needsHibernateSession )
{
	m_NeedsHibernateSession = needsHibernateSession;
}

}