package biz.firstlook.main.enumerator;

public enum ThirdPartyCategoryEnum
{
	NADA_RETAIL( 1, "Retail", ThirdPartyEnum.NADA),
	NADA_TRADE_IN( 2, "Trade-In", ThirdPartyEnum.NADA),
	NADA_LOAN( 3, "Loan", ThirdPartyEnum.NADA),
	BLACKBOOK_EXTRACLEAN( 4, "Extra Clean", ThirdPartyEnum.BLACKBOOK ),
	BLACKBOOK_CLEAN( 5, "Clean", ThirdPartyEnum.BLACKBOOK ),
	BLACKBOOK_AVERAGE( 6, "Average", ThirdPartyEnum.BLACKBOOK ),
	BLACKBOOK_ROUGH( 7, "Rough", ThirdPartyEnum.BLACKBOOK ),
	KBB_WHOLESALE( 8, "Lending Value", ThirdPartyEnum.KBB ),
	KBB_RETAIL( 9, "Retail", ThirdPartyEnum.KBB ),
	GALVES_MARKET_READY( 10, "Market Ready", ThirdPartyEnum.GALVES ),
	KBB_TRADE_IN(11, "Trade-In", ThirdPartyEnum.KBB),
	KBB_PRIVATE_PARTY(12, "Trade-In", ThirdPartyEnum.KBB),
	GALVES_TRADE_IN(13, "Trade-In", ThirdPartyEnum.GALVES),
	BLACKBOOK_FINANCE_ADVANCE(14, "Finance Advance", ThirdPartyEnum.BLACKBOOK),
	NADA_AVERAGE_TRADE_IN( 15, "Average Trade-In", ThirdPartyEnum.NADA),
	NADA_ROUGH_TRADE_IN( 16, "Rough Trade-In", ThirdPartyEnum.NADA),
	KBB_TRADE_IN_RANGELOW(17, "Trade-In + RangeLow", ThirdPartyEnum.KBB),
	KBB_TRADE_IN_RANGEHIGH(18, "Trade-In + RangeHigh", ThirdPartyEnum.KBB),
	MMR_AVERAGE(19,"Average",ThirdPartyEnum.MMR);

	
	private final int thirdPartyCategoryId;
	private final String category;
	private final ThirdPartyEnum thirdPartyEnum;
	
	ThirdPartyCategoryEnum(int thirdPartyCategoryId, String category, ThirdPartyEnum thirdPartyEnum )
	{
		this.thirdPartyCategoryId = thirdPartyCategoryId;
		this.category = category;
		this.thirdPartyEnum = thirdPartyEnum;
	}
	
	public String getDescription()
	{
		return category;
	}
	
	public static ThirdPartyCategoryEnum getEnumById( int thirdPartyCategoryId )
	{
		switch( thirdPartyCategoryId )
		{
			case 1: return NADA_RETAIL;
			case 2: return NADA_TRADE_IN;
			case 3: return NADA_LOAN;
			case 4: return BLACKBOOK_EXTRACLEAN;
			case 5: return BLACKBOOK_CLEAN;
			case 6: return BLACKBOOK_AVERAGE;
			case 7: return BLACKBOOK_ROUGH;
			case 8: return KBB_WHOLESALE;
			case 9: return KBB_RETAIL;
			case 10: return GALVES_MARKET_READY;
			case 11: return KBB_TRADE_IN;
			case 12: return KBB_PRIVATE_PARTY;
			case 13: return GALVES_TRADE_IN;
			case 14: return BLACKBOOK_FINANCE_ADVANCE;
			case 15: return NADA_AVERAGE_TRADE_IN;
			case 16: return NADA_ROUGH_TRADE_IN;
			case 17: return KBB_TRADE_IN_RANGELOW;
			case 18: return KBB_TRADE_IN_RANGEHIGH;
			case 19: return MMR_AVERAGE;
			default: return null;
		}
	}

	public int getThirdPartyCategoryId()
	{
		return thirdPartyCategoryId;
	}

	public ThirdPartyEnum getThirdPartyEnum()
	{
		return thirdPartyEnum;
	}

	public String getCategory()
	{
		return category;
	}
}
