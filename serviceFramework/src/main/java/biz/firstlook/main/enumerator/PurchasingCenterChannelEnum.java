package biz.firstlook.main.enumerator;

public enum PurchasingCenterChannelEnum
{
ONLINE ( 1, "Online" ),
IN_GROUP( 2, "In Group" );

private Integer channelId;
private String channelName;

PurchasingCenterChannelEnum( Integer channelId, String channelName )
{
    this.channelId = channelId;
    this.channelName = channelName;
}

public Integer getChannelId()
{
	return channelId;
}

public void setChannelId( Integer channelId )
{
	this.channelId = channelId;
}

public String getChannelName()
{
	return channelName;
}

public void setChannelName( String channelName )
{
	this.channelName = channelName;
}
}
