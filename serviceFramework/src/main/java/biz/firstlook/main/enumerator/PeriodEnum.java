package biz.firstlook.main.enumerator;

public enum PeriodEnum
{
	ALL( new Integer( 0 ), "All" ), 
	FOUR_WEEKS( new Integer( 1 ), "Four Weeks" ),
	EIGHT_WEEKS( new Integer( 2 ), "Eight Weeks" ),
	THIRTEEN_WEEKS( new Integer( 3 ), "Thirteen Weeks" ),
	TWENTYSIX_WEEKS( new Integer( 4 ), "Twenty-six Weeks" ),
	FIFTYTWO_WEEKS( new Integer( 5 ), "Fifty-two Weeks" ),
	CURRENT_MONTH( new Integer( 6 ), "Current Month" ),
	PRIOR_MONTH( new Integer( 7 ), "Prior Month" );

	private Integer periodId;
	private String description;

	PeriodEnum( Integer periodId, String description )
	{
		this.periodId = periodId;
		this.description = description;
	}

	public Integer getPeriodId()
	{
		return this.periodId;
	}

	public String getDescription()
	{
		return this.description;
	}

	public static PeriodEnum getEnumFromWeeks( int weeks )
	{
		switch ( weeks )
		{
			case 0:
				return ALL;
			case 4:
				return FOUR_WEEKS;
			case 8:
				return EIGHT_WEEKS;
			case 13:
				return THIRTEEN_WEEKS;
			case 26:
				return TWENTYSIX_WEEKS;
			case 52:
				return FIFTYTWO_WEEKS;

			default:
				return ALL;
		}
	}

}
