package biz.firstlook.main.enumerator;

public enum SQLOperationEnum
{
	EQ( " = " ), LT( " < " ), LTE( " <= " ), GT( " > " ), GTE( " >= " ), NOT_EQ( " <> " ), IN( " in " ), AND( " and " ), OR( " or " ), BETWEEN( " between " ), IS_NULL( " is null" );

	private final String sqlSymbol;

	SQLOperationEnum( String symbol )
	{
		this.sqlSymbol = symbol;
	}

	public String toString()
	{
		return sqlSymbol;
	}

}
