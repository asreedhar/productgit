package biz.firstlook.main.enumerator;

public enum LevelEnum
{
ALL( new Integer( 4 ), "All" ), 
SPECIFIC( new Integer( 5 ), "Specific" );

private Integer levelId;
private String description;

LevelEnum( Integer levelId, String description )
{
	this.levelId = levelId;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getLevelId()
{
	return levelId;
}

public void setlevelId( Integer levelId )
{
	this.levelId = levelId;
}

}
