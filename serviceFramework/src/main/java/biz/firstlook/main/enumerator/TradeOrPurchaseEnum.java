package biz.firstlook.main.enumerator;

public enum TradeOrPurchaseEnum
{

PURCHASE( 1, "Purchase", "P" ),
TRADE( 2, "Trade", "T" ),
UNKNOWN( 3, "Unknown", "N/A" );

private final int tradeOrPurchaseCD;
private final String tradeOrPurchaseDesc;
private final String tradeOrPurchaseShortDesc;

TradeOrPurchaseEnum( int tradeOrPurchaseCD, String tradeOrPurchaseDesc, String tradeOrPurchaseShortDesc )
{
	this.tradeOrPurchaseCD = tradeOrPurchaseCD;
	this.tradeOrPurchaseDesc = tradeOrPurchaseDesc;
	this.tradeOrPurchaseShortDesc = tradeOrPurchaseShortDesc;
}

public int getTradeOrPurchaseCD()
{
	return tradeOrPurchaseCD;
}

public String getTradeOrPurchaseDesc()
{
	return tradeOrPurchaseDesc;
}

public static TradeOrPurchaseEnum getTradeOrPurchaseEnum( int tradeOrPurchaseCD )
{
	switch( tradeOrPurchaseCD )
	{
		case 1 : return PURCHASE;
		case 2 : return TRADE;
		default : return UNKNOWN;
	}
}

public String getTradeOrPurchaseShortDesc()
{
	return tradeOrPurchaseShortDesc;
}

}
