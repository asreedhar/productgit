package biz.firstlook.main.enumerator;

public enum PlanObjectiveEnum
{
	UNKNOWN( 0, "-", "" ), 
	RETAIL( 1, "Retail", "retail" ),
	WHOLESALE( 2, "Wholesale", "wholesale" ), 
	SOLD( 3, "Sold", "sold" ),
	OTHER( 4, "Other", "other" ),
	DEALER( 5, "Dealer", "dealer" ),
	ADMIN( 6, "Admin", "" ), 
	MARKETPLACE( 7, "Marketplace", "marketplace");

	private int id;
	private String description;
	private String jsonKey;

	PlanObjectiveEnum( int id, String description, String jsonKey )
	{
		this.id = id;
		this.description = description;
		this.jsonKey = jsonKey;
	}

	public int getId()
	{
		return id;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public String getJsonKey()
	{
	    return jsonKey;
	}

	public static PlanObjectiveEnum getPlanObjectiveEnum( int id )
	{
		switch ( id )
		{
			case 1:
				return RETAIL;
			case 2:
				return WHOLESALE;
			case 3:
				return SOLD;
			case 4:
				return OTHER;
			case 5:
				return DEALER;
			case 6:
				return ADMIN;
			default:
				return UNKNOWN;
		}
	}

    public static PlanObjectiveEnum getPlanObjectiveEnum( String jsonKey )
    {
    	if ( jsonKey == null) {
    		return UNKNOWN;
    	} else if ( jsonKey.equals( "retail" ) )
    	{
    		return RETAIL;
    	}
    	else if ( jsonKey.equals( "wholesale" ) )
    	{
    		return WHOLESALE;
    	}
    	else if ( jsonKey.equals( "sold" ) )
    	{
    		return SOLD;
    	}
    	else if ( jsonKey.equals( "other" ) )
    	{
    		return OTHER;
    	}
    	else if ( jsonKey.equals( "dealer" ) )
        {
        	return DEALER;
        }
        else
        {
        	return UNKNOWN;
        }  	
    }
}
