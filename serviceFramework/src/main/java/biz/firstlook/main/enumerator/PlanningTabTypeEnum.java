package biz.firstlook.main.enumerator;

public enum PlanningTabTypeEnum {

	REPLANNING( 1, "Replanning" ),
	WATCH_LIST( 2, "Watch List" ),
	TIME_BUCKET( 3, "Time Bucket" );
	
	private int agingPlanTabId;
	private String description;
	
	PlanningTabTypeEnum( int agingPlanTabId, String description )
	{
		this.agingPlanTabId = agingPlanTabId;
		this.description = description;
	}

	public static PlanningTabTypeEnum getEnumById( int tabId )
	{
		switch( tabId )
		{
			case 1: return REPLANNING;
			case 2: return WATCH_LIST;
			case 3: return TIME_BUCKET;
			default: return REPLANNING;
		}
	}	
	
	public String getDescription()
	{
		return description;
	}

	public int getAgingPlanTabId()
	{
		return agingPlanTabId;
	}
}
