package biz.firstlook.main.enumerator;

public enum PlanningEventTypeEnum
{
SPIFF( 1, PlanObjectiveEnum.RETAIL, "SPIFF", "spiff", true ), 
PROMOTION( 2, PlanObjectiveEnum.RETAIL, "Promotion", "lotPromote", false ), 
ADVERTISEMENT( 3, PlanObjectiveEnum.RETAIL, "Advertisement", "advertise", false ), 
OTHER_RETAIL( 4, PlanObjectiveEnum.RETAIL, "Other", "retailOther", true ), 
REPRICE( 5, PlanObjectiveEnum.RETAIL, "Reprice", "reprice", false ), 
AUCTION( 6, PlanObjectiveEnum.WHOLESALE, "Auction", "auction", false ),
WHOLESALER( 7, PlanObjectiveEnum.WHOLESALE, "Wholesaler", "wholesaler", false ),
RETAIL( 8, PlanObjectiveEnum.SOLD, "Retail", "retail", false ),
WHOLESALE( 9, PlanObjectiveEnum.SOLD, "Wholesale", "wholesale", false ), 
OTHER( 10, PlanObjectiveEnum.OTHER, "Other", "other", false ),
SERVICE_DEPARTMENT( 11, PlanObjectiveEnum.DEALER, "Service Department", "service", true ),
DETAILER( 12, PlanObjectiveEnum.DEALER, "Detailer", "detail", true ),
OTHER_WHOLESALE( 13, PlanObjectiveEnum.WHOLESALE, "Other", "wholesaleOther", true ),
REMINDER_DATE( 14, PlanObjectiveEnum.ADMIN, "Reminder Date", "", false ),
RETAIL_NO_STRATEGY( 15, PlanObjectiveEnum.RETAIL, "Retail:No Strategy", "retailNoStrategy", false ),
WHOLESALE_NO_STRATEGY( 16, PlanObjectiveEnum.WHOLESALE, "Wholesale:No Strategy", "wholesaleNoStrategy", false ),
SOLD_NO_STRATEGY( 17, PlanObjectiveEnum.SOLD, "No Plan", "0", false ), //JSON comes back with a '0' if no strategy is selected
INTERNET_MARKETPLACE( 18, PlanObjectiveEnum.MARKETPLACE, "Internet Marketplace", "marketplace", false),
// note: other no strategy does not exist in DB!!!! can we get ride of it?
OTHER_NO_STRATEGY( 19, PlanObjectiveEnum.OTHER, "No Plan", "otherNoStrategy", false );

private int id;
private PlanObjectiveEnum objectiveEnum;
private String description;
private String jsonKey;
private boolean isToggledEvent;

PlanningEventTypeEnum( int id, PlanObjectiveEnum objectiveEnum, String description, String jsonKey, boolean isToggledEvent )
{
	this.id = id;
	this.objectiveEnum = objectiveEnum;
	this.description = description;
	this.jsonKey = jsonKey;
	this.isToggledEvent = isToggledEvent;
}

public int getId()
{
	return id;
}

public PlanObjectiveEnum getPlanObjectiveEnum()
{
    return objectiveEnum;	
}

public int getObjectiveId()
{
	return objectiveEnum.getId();
}

public String getDescription()
{
	return description;
}

public String getJsonKey()
{
	//overrides, but don't modify internal state in case of regression! -bf.
	switch (this) {
	    case SPIFF:
	        return "spiff";
	    case OTHER_RETAIL:
	    	return "retailOther";
	    case RETAIL:
	    case WHOLESALE:
	        return "sold";
    }
    return jsonKey;	
}

public boolean isToggledEvent()
{
    return isToggledEvent;
}

public static PlanningEventTypeEnum getPlanEventTypeEnum( int planEventTypeId )
{
	switch ( planEventTypeId )
	{
		case 1:
			return SPIFF;
		case 2:
			return PROMOTION;
		case 3:
			return ADVERTISEMENT;
		case 4:
			return OTHER_RETAIL;
		case 5:
			return REPRICE;
		case 6:
			return AUCTION;
		case 7:
			return WHOLESALER;
		case 8:
			return RETAIL;
		case 9:
			return WHOLESALE;
		case 10:
			return OTHER;
		case 11:
			return SERVICE_DEPARTMENT;
		case 12:
			return DETAILER;
		case 13:
			return OTHER_WHOLESALE;
		case 14:
			return REMINDER_DATE;
		case 15:
			return RETAIL_NO_STRATEGY;
		case 16:
			return WHOLESALE_NO_STRATEGY;
		case 17:
			return SOLD_NO_STRATEGY;
		case 18:
			return INTERNET_MARKETPLACE;
		case 19:
			return OTHER_NO_STRATEGY;
		default:
			return OTHER;
	}
}

public static PlanningEventTypeEnum getPlanEventTypeEnum( String jsonKey )
{
    // should we throw an exception if the jsonKey doesn't
	// match any of the possibilities?
	
	if ( jsonKey.equals( "advertise" ) )
	{
	    return ADVERTISEMENT;	
	} 
	else if ( jsonKey.equals( "lotPromote" ) )
	{
	    return PROMOTION;	
	}
	else if ( jsonKey.equals( "auction" ) )
	{
	    return AUCTION;	
	}
	else if ( jsonKey.equals( "wholesaler" ) )
	{
		return WHOLESALER;
	}
	else if ( jsonKey.equals( "spiff" ) )
	{
		return SPIFF;
	}
	else if ( jsonKey.equals( "other" ) )
	{
		return OTHER;
	}
	else if ( jsonKey.equals( "retail" ) )
	{
		return RETAIL;
	}
	else if ( jsonKey.equals( "wholesale" ) )
	{
		return WHOLESALE;
	}
	else if ( jsonKey.equals( "wholesaleOther" ) )
	{
		return OTHER_WHOLESALE;
	}
	else if ( jsonKey.equals( "retailOther" ) )
	{
		return OTHER_RETAIL;
	}
	else if ( jsonKey.equals( "service" ) )
	{
		return SERVICE_DEPARTMENT;
	}
	else if ( jsonKey.equals( "detail" ) )
	{
		return DETAILER;
	}
	else if ( jsonKey.equals( "0" ) )
	{
	    return SOLD_NO_STRATEGY;
	}
	
	return null;
}

public boolean isNoStrategyEvent()
{
    switch ( id )
    {
    	case 15:
    	case 16:
    	case 17:
    	case 18:
    		return true;
    	default:
    		return false;
    }
}

}
