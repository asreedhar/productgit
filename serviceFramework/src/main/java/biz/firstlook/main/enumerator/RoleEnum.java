package biz.firstlook.main.enumerator;

public enum RoleEnum
{
    USED_NOACCESS( new Integer(1), "U", "NOACCESS", "No Access"),
    USED_APPRAISER( new Integer(2), "U", "APPRAISE", "Appraiser"),
    USED_STANDARD( new Integer(3), "U", "STANDARD", "Standard"),
    USED_MANAGER( new Integer(4), "U", "MANAGER", "Manager"),
    NEW_NOACCESS( new Integer(5), "N", "NOACCESS", "No Access"),
    NEW_STANDARD( new Integer(6), "N", "STANDARD", "Standard"),
    ADMIN_NONE( new Integer(7), "A", "NONE", "None" ),
    ADMIN_FULL( new Integer(8), "A", "FULL", "Full" );
    
    private Integer roleId;
    private String type;
    private String code;
    private String name;
   
    RoleEnum( Integer roleId, String type, String code, String name) 
    {
        this.roleId = roleId;
        this.type = type;
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public String getType() {
        return type;
    }

}

