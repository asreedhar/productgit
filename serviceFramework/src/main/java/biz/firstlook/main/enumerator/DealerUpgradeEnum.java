package biz.firstlook.main.enumerator;

public enum DealerUpgradeEnum
{
	TRADE_ANALYZER( Integer.valueOf( 1 ), "Trade Analyzer" ),
	AIP( Integer.valueOf( 2 ), "Aging Inventory Plan" ),
	CIA( Integer.valueOf( 3 ), "Purchasing Center" ),
	REDISTRIBUTION( Integer.valueOf( 4 ), "Redistribution" ),
	AUCTION_DATA( Integer.valueOf( 5 ), "Auction Data" ),
	//WINDOW_STICKER( Integer.valueOf( 6 ), "Window Sticker" ),
	PERFORMANCE_DASHBOARD( Integer.valueOf( 7 ), "Performance Dashboard" ),
	MARKET_DATA( Integer.valueOf( 8 ), "Market Data" ),
	ANNUAL_ROI( Integer.valueOf( 9 ), "Annual ROI" ),
	APPRAISAL_LOCKOUT(Integer.valueOf(10), "Appraisal Lockout"),
	EQUITY_ANALYZER( Integer.valueOf ( 11 ), "Equity Analyzer"),
	PLATINUM_PACKAGE(Integer.valueOf(12), "Platinum Package"),
	NAVIGATOR_VIP_DEALERS_RESOURCE( Integer.valueOf ( 13 ), "Navigator for VIP and Dealers Resource"),
	PING( Integer.valueOf ( 14 ), "PING"),
	MAX(Integer.valueOf(15), "MAX"),
	MAX_TEST(Integer.valueOf(16), "MAX-Text"),
	KBB_TRADE_IN( Integer.valueOf ( 17 ), "KBB Trade-In Values"),
	JDPOWER_USED_CAR_MARKET_DATA( Integer.valueOf( 18 ), "JDPower Used Car Market Data"),
	PINGII (Integer.valueOf( 19 ), "PING II"),
	EDMUNDS_TMV(Integer.valueOf( 20 ), "Edmunds TMV"),
	MAKE_A_DEAL(Integer.valueOf(21), "Make-A-Deal"),
	MARKET_STOCKING_GUIDE(Integer.valueOf(22), "Market Stocking Guide"),
	MARKETING(Integer.valueOf(23), "Marketing"),
	ADVERTISING(Integer.valueOf(24), "Advertising"),
	WEBSITE_PDF(Integer.valueOf(25), "Website PDF"),
	WEBSITE_PDF_MARKET_LISTINGS(Integer.valueOf(26), "Website PDF Market Listings"),
	FIRSTLOOK_3_0(Integer.valueOf(27), "Firstlook 3.0"),
	NADA_VALUES(Integer.valueOf(28),"NADA Values");
	                          
	private Integer dealerUpgradeId;
	private String dealerUpgradeDESC;
	
	DealerUpgradeEnum( Integer dealerUpgradeId, String dealerUpgradeDESC )
	{
		this.dealerUpgradeId = dealerUpgradeId;
		this.dealerUpgradeDESC = dealerUpgradeDESC;
	}

	public String getDealerUpgradeDESC()
	{
		return dealerUpgradeDESC;
	}

	public Integer getDealerUpgradeId()
	{
		return dealerUpgradeId;
	}
	
	public static DealerUpgradeEnum valueOfId(int id) {
		for(DealerUpgradeEnum e : DealerUpgradeEnum.values()) {
			if(e.getDealerUpgradeId().equals(id)) {
				return e;
			}
		}
		return null;
	}
}
