package biz.firstlook.main.enumerator;

public enum VehicleSegmentEnum
{

	UNKNOWN( 1, "Unknown" ),
	TRUCK( 2, "Truck" ),
	SEDAN( 3, "Sedan" ),
	COUPE( 4, "Coupe" ),
	VAN( 5, "Van" ),
	SUV( 6, "SUV" ),
	CONVERTIBLE( 7, "Convertible" ),
	WAGON( 8, "Wagon" );
	
	private int vehicleSegmentId;
	private String description;
	
	VehicleSegmentEnum( int vehicleSegmentId, String description )
	{
		this.vehicleSegmentId = vehicleSegmentId;
		this.description = description;
	}

	public static VehicleSegmentEnum getVehicleSegmentEnum( int vehicleSegmentId )
	{
		switch( vehicleSegmentId )
		{
			case 2 : return TRUCK;
			case 3 : return SEDAN;
			case 4 : return COUPE;
			case 5 : return VAN;
			case 6 : return SUV;
			case 7 : return CONVERTIBLE;
			case 8 : return WAGON;
			default : return UNKNOWN;
		}
	}
	
	public String getDescription()
	{
		return description;
	}

	public int getVehicleSegmentId()
	{
		return vehicleSegmentId;
	}

	
	
}
