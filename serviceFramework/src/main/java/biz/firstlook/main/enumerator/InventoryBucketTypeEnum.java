package biz.firstlook.main.enumerator;

public enum InventoryBucketTypeEnum 
{
	AGING_INVENTORY_PLAN_USED( 1, "Aging Inventory Plan (Used)" ),
	TOTAL_INVENTORY_REPORT_NEW( 2, "Total Inventory Report (New)" ),
	TOTAL_INVENTORY_REPORT_USED( 3, "Total Inventory Report (Used)" ),
    NEW_AGING_INVENTORY_PLAN( 4, "New Aging Inventory Plan" );
	//MOD AP might need new bucket type for pricing?
	
	private int inventoryBucketId;
	private String description;
	
	InventoryBucketTypeEnum( int inventoryBucketId, String description )
	{
	    this.inventoryBucketId = inventoryBucketId;
	    this.description = description;
	}
	
	public int getInventoryBucketId()
	{
	    return inventoryBucketId;
	}
	
	public String getDescription()
	{
	    return description;
	}
}
