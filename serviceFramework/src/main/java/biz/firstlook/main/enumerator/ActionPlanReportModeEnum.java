package biz.firstlook.main.enumerator;

public enum ActionPlanReportModeEnum
{
	CURRENT(1, "Current"), 
	HISTORY(2, "History");
	
	
	private int actionPlanReportModeId;
	private String description;
	
	ActionPlanReportModeEnum( int actionPlanReportModeId, String description )
	{
		this.actionPlanReportModeId = actionPlanReportModeId;
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}

	public int getActionPlanReportModeId()
	{
		return actionPlanReportModeId;
	}

	
	
}
