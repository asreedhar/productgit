package biz.firstlook.main.enumerator;

public enum MileageRangeEnum
{

ALL( new Integer( 9 ), "All" ), 
UNDER_TEN_THOUSAND( new Integer( 1 ), "0-10000" ),
TEN_TO_TWENTY_THOUSAND( new Integer( 2 ), "10000-19999" ),
TWENTY_TO_THIRTY_THOUSAND( new Integer( 3 ), "20000-29999" ),
THIRTY_TO_FORTY_THOUSAND( new Integer( 4 ), "30000-39999" ),
FORTY_TO_FIFTY_THOUSAND( new Integer( 5 ), "40000-49999" ),
FIFTY_TO_SIXTY_THOUSAND( new Integer( 6 ), "50000-59999" ),
SIXTY_TO_SEVENTY_THOUSAND( new Integer( 7 ), "60000-69999" ),
OVER_SEVENTY_THOUSAND( new Integer( 8 ), "70000+" );

private Integer mileageRangeId;
private String description;

MileageRangeEnum( Integer mileageRangeId, String description )
{
	this.mileageRangeId = mileageRangeId;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getMileageRangeId()
{
	return mileageRangeId;
}

public void setMileageRangeId( Integer mileageRangeId )
{
	this.mileageRangeId = mileageRangeId;
}


}
