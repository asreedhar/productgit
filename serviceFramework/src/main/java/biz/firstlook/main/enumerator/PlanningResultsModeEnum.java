package biz.firstlook.main.enumerator;

public enum PlanningResultsModeEnum
{
	DETAIL( 0, "Detail", 0 ), 
	REPLANNING( 1, "Replanning", 0 ), 
	DUE_FOR_PLANNING( 2, "Due for planning", 7 ),
	PLANNED( 3, "Planned", 0 );

	private int id;
	private String description;
	private int inventoryPlanningAgeInflation;

	PlanningResultsModeEnum( int id, String description, int inventoryPlanningAgeInflation )
	{
		this.id = id;
		this.description = description;
		this.inventoryPlanningAgeInflation = inventoryPlanningAgeInflation;
	}

	public String getDescription()
	{
		return description;
	}

	public int getId()
	{
		return id;
	}
	
	public static PlanningResultsModeEnum getResultsModeEnumById( Integer id )
	{
		switch ( id )
		{
		    case 0: return DETAIL;
		    case 1: return REPLANNING;
            case 2: return DUE_FOR_PLANNING;
            case 3: return PLANNED;
		    default: return DETAIL;
		}		
	}

	public int getInventoryPlanningAgeInflation()
	{
		return inventoryPlanningAgeInflation;
	}
}
