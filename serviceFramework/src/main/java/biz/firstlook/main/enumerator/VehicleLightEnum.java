package biz.firstlook.main.enumerator;

public enum VehicleLightEnum
{
	NONE( new Integer( 0), "None" ), 
	RED( new Integer( 1), "Red" ), 
	YELLOW( new Integer( 2 ), "Yellow" ),
	GREEN( new Integer( 3 ), "Green" );

	private Integer id;
	private String description;

	VehicleLightEnum( Integer id, String description )
	{
		this.id = id;
		this.description = description;
	}

	public Integer getId()
	{
		return this.id;
	}

	public String getDescription()
	{
		return this.description;
	}

	
	public static VehicleLightEnum getLightEnum( int lightId )
	{
		switch ( lightId )
		{
			case 1:
				return RED;
			case 2:
				return YELLOW;
			case 3:
				return GREEN;
			default:
				return NONE;
		}
	}

}
