package biz.firstlook.main.enumerator;

import java.io.Serializable;

/**
 * <p>Enumeration for the Performance Plus report aggregation dimensions.</p>
 * @author swenmouth
 */
public enum PerformancePlusDimensionEnum implements Serializable {

	PRICE ( 0, "Price Range Analysis" ),
	TRIM ( 1, "Trim Analysis" ),
	YEAR ( 2, "Year Analysis" ),
	COLOR ( 3, "Color Analysis" ),
	ALL ( 4, "Summary Line Analysis" );
	
	private int id;
	private String description;
	PerformancePlusDimensionEnum(int id, String description) {
		this.id = id;
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
	public int getId() {
		return id;
	}
}
