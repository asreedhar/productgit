package biz.firstlook.main.enumerator;

public enum ProgramTypeEnum
{
UNKNOWN (0),
VIP (1),
EDGE (2),
DEALERS_RESOURCE (3),
FIRSTLOOK(4);

private Integer programTypeCode;

ProgramTypeEnum( int programTypeCode )
{
    this.programTypeCode = programTypeCode;    	
}

public Integer getProgramTypeCode()
{
	return programTypeCode;
}

public void setProgramTypeCode( Integer programTypeCode )
{
	this.programTypeCode = programTypeCode;
}

public static ProgramTypeEnum getByProgramTypeCode( Integer programTypeCode )
{
    switch ( programTypeCode )
    {
    	case 1: return VIP;
    	case 2: return EDGE;
    	case 3: return DEALERS_RESOURCE;
        case 4: return FIRSTLOOK;
    	default: return UNKNOWN;
    }
}

}
