package biz.firstlook.main.enumerator;

public enum PlanningGroupByEnum
{
	AGE_BUCKET( 1, "Age Bucket", "RangeID" ), 
	REMINDER_DATE( 2, "Reminder Date", "PlanReminderDate" ),
	LAST_PLANNED_DATE( 3, "Last Planned Date", "LastPlannedDate" ),
	NO_GROUPING( 4, "No Grouping", "NoGrouping");

	private int id;
	private String description;
	private String columnName;

	PlanningGroupByEnum( int id, String description, String columnName )
	{
		this.id = id;
		this.description = description;
		this.columnName = columnName;
	}

	public static PlanningGroupByEnum getGroupByEnumById( int id )
	{
	    switch ( id )
	    {
	    	case 1: return AGE_BUCKET;
	    	case 2: return REMINDER_DATE;
	    	case 3: return LAST_PLANNED_DATE;
	    	case 4: return NO_GROUPING;
	    	default: return AGE_BUCKET;
	    }
	}
	
	public String getDescription()
	{
		return description;
	}

	public int getId()
	{
		return id;
	}

	public String getColumnName()
	{
		return columnName;
	}
}
