package biz.firstlook.main.enumerator;

public enum PlanningStatusEnum 
{
    NO_PLAN ( 0 ),
    CURRENT_PLAN ( 1 ),
    EXPIRED_PLAN ( 2 );
	
	private int id;
    
    PlanningStatusEnum( int id )
    {
        this.id = id;
    }
    
    public static PlanningStatusEnum getStatusEnum ( int statusId )
    {
		switch( statusId )
		{
		    case 0 : return NO_PLAN;
		    case 1 : return CURRENT_PLAN;
		    case 2 : return EXPIRED_PLAN;
			default : return NO_PLAN;
		}    	
    }

	public int getId() 
	{
		return id;
	}
}
