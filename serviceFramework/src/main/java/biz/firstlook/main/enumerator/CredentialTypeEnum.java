package biz.firstlook.main.enumerator;

public enum CredentialTypeEnum
{
GMACSMARTAUCTION(3, "GMAC Smart Auction");

private int credentialTypeId;
private String name;

CredentialTypeEnum( int credentialTypeId, String name )
{
    this.credentialTypeId = credentialTypeId;
    this.name = name;
}

public int getCredentialTypeId()
{
	return credentialTypeId;
}

public String getName()
{
	return name;
}
}
