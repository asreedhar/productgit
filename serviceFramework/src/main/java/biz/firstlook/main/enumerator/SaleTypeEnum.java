package biz.firstlook.main.enumerator;

public enum SaleTypeEnum
{

	RETAIL( new Integer( 1 ), "Retail" ), 
	WHOLESALE( new Integer( 2 ), "Wholesale" );

	private Integer saleTypeId;
	private String description;

	SaleTypeEnum( Integer saleTypeId, String description )
	{
		this.saleTypeId = saleTypeId;
		this.description = description;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public Integer getSaleTypeId()
	{
		return saleTypeId;
	}

	public void setSaleTypeId( Integer saleTypeId )
	{
		this.saleTypeId = saleTypeId;
	}
	
	public static SaleTypeEnum getEnumFromSaleTypeId( int saleTypeId )
	{
		switch ( saleTypeId )
		{
			case 1:
				return RETAIL;
			case 2:
				return WHOLESALE;
			default:
				return RETAIL;
		}
	}

}
