package biz.firstlook.main.persist.common;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface IGenericDAO <T, ID extends Serializable>
{
T findById(ID id);

List<T> findByExample(T exampleInstance, String... excludeProperty);

@Transactional(readOnly=false)
T makePersistent(T entity);

@Transactional(readOnly=false)
void saveOrUpdate(T entity);

}  
