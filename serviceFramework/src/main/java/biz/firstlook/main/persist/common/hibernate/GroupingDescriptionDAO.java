package biz.firstlook.main.persist.common.hibernate;

import biz.firstlook.entity.GroupingDescription;

public class GroupingDescriptionDAO extends GenericHibernate<GroupingDescription, Integer> {

	public GroupingDescriptionDAO() {
		super(GroupingDescription.class);
	}
	
}
