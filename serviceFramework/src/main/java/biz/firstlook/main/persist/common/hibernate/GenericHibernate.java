package biz.firstlook.main.persist.common.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.main.persist.common.IGenericDAO;

public abstract class GenericHibernate<T, ID extends Serializable> extends
		HibernateDaoSupport implements IGenericDAO<T, ID> {

	private Class persistentClass;

	public GenericHibernate(Class persistentClass) {
		this.persistentClass = persistentClass;
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		return findById(id, false);
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getHibernateTemplate().load(getPersistentClass(), id,
					org.hibernate.LockMode.UPGRADE);
		} else {
			entity = (T) getHibernateTemplate().load(getPersistentClass(), id);
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByExample(final T exampleInstance,
			final String... excludeProperty) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				// Using Hibernate, more difficult with EntityManager and EJB-QL
				Criteria crit = session.createCriteria(getPersistentClass());
				Example example = Example.create(exampleInstance);
				for (String exclude : excludeProperty) {
					example.excludeProperty(exclude);
				}
				crit.add(example);
				return crit.list();
			}

		};
		return (List) getHibernateTemplate().execute(callback);
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(final Criterion... criterion) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(getPersistentClass());
				for (Criterion c : criterion) {
					crit.add(c);
				}
				return crit.list();
			}
		};
		return (List) getHibernateTemplate().execute(callback);
	}

	@SuppressWarnings("unchecked")
	public T makePersistent(T entity) {
		return (T) getHibernateTemplate().merge(entity);
	}

	public void saveOrUpdate(T entity) {
		//Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		//session.setFlushMode( FlushMode.ALWAYS );
		getHibernateTemplate().saveOrUpdate(entity);
	}

	public void delete(T entity) {
		//Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		//session.setFlushMode( FlushMode.ALWAYS );
		getHibernateTemplate().delete(entity);
	}
	
	public void saveOrUpdateAll(List<T> entityList ){
		getHibernateTemplate().saveOrUpdateAll(entityList);
	}
	
	public Class getPersistentClass() {
		return persistentClass;
	}
}
