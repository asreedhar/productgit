package biz.firstlook.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.config.ActionConfig;
import org.apache.struts.config.ModuleConfig;

@SuppressWarnings("serial")
public class ExportStrutsForm extends TagSupport {
	
	private static final String THE_FORM = "theForm";

	private String var;
	
	public String getVar() {
		return var;
	}
	public void setVar(String exportName) {
		this.var = exportName;
	}

	public void release() {
		super.release();
		this.var = THE_FORM;
	}
	
	public int doEndTag() throws JspException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) pageContext.getRequest();
		while (httpServletRequest instanceof HttpServletRequestWrapper) {
			httpServletRequest = (HttpServletRequest) ((HttpServletRequestWrapper) httpServletRequest).getRequest();
		}
		final ActionConfig actionConfig = getActionConfig(
				httpServletRequest.getContextPath(),
				httpServletRequest.getRequestURI());
		if (actionConfig == null) {
			return EVAL_PAGE;
		}
		final String formName = getFormName();
		final String attrName = actionConfig.getAttribute();
		if (attrName == null) {
			return EVAL_PAGE;
		}
		ActionForm form = null;
		int attempt = 0;
		TRY: while (true) {
			switch (attempt++) {
			case 0:
				form = (ActionForm) httpServletRequest.getAttribute(attrName);
				break;
			case 1:
				form = (ActionForm) httpServletRequest.getSession().getAttribute(attrName);
				break;
			default:
				break TRY;
			}
			if (form != null)
				break;
		}
		pageContext.setAttribute(formName, form);
		return EVAL_PAGE;
	}
	
	private String getFormName() {
		String formName = getVar(); 
		if (getVar() == null) {
			formName = THE_FORM;
		}
		return formName;
	}

	private ActionConfig getActionConfig(String contextPath, String requestURI) {
		ModuleConfig moduleConfig = (ModuleConfig) pageContext.getRequest().getAttribute(Globals.MODULE_KEY);
		if (moduleConfig == null) {
			moduleConfig = (ModuleConfig) pageContext.getServletContext().getAttribute(Globals.MODULE_KEY);
		}
		if (moduleConfig == null) {
			return null;
		}
		final String action = requestURI.substring(
				contextPath.length() + 1,
				requestURI.length() - ".go".length());
		return (ActionConfig) moduleConfig.findActionConfig("/" + action);
	}

}
