package biz.firstlook.taglib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Functions {

	public static final <T> List<T> singletonList(T o) {
		return Collections.singletonList(o);
	}

	public static final <T> List<T> reverse(List<T> list) {
		List<T> copy = new ArrayList<T>(list.size());
		copy.addAll(list);
		Collections.reverse(copy);
		return copy;
	}
	
	public static final <T extends Comparable<? super T>> List<T> reverseOrder(List<T> list) {
		if (list == null) {
			return Collections.emptyList();
		}
		List<T> copy = new ArrayList<T>(list.size());
		copy.addAll(list);
		Collections.sort(copy, Collections.reverseOrder());
		return copy;
	}
	
	public static final <T extends Comparable<? super T>> List<T> sort(List<T> list) {
		if (list == null) {
			return Collections.emptyList();
		}
		List<T> copy = new ArrayList<T>(list.size());
		copy.addAll(list);
		Collections.sort(copy);
		return copy;
	}

	public static final int daysFromNow(Date date) {
		if (date == null)
			return 0;
		final long dayAsMillis = 1000*60*60*24;
		return new Long((date.getTime()-System.currentTimeMillis())/dayAsMillis).intValue();
	}
	
	public static final int daysFromMidnight(Date date) {
		if (date == null)
			return 0;
		final double dayAsMillis = 1000*60*60*24;		//changed to double due to rounding errors.
		final int days =(int) Math.ceil(((date.getTime()-System.currentTimeMillis())/dayAsMillis));
		return days;
	}
	
	public static final int abs(int a) {
		return Math.abs(a);
	}
	
}
