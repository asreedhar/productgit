/**
 * 
 */
package biz.firstlook.taglib;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class YearsOptionsTag extends TagSupport implements Serializable
{

private static final long serialVersionUID = 2388825378352647649L;

public final static int NUMBER_OF_YEARS = 20;

public final static String ALL_VALUE = "0";
public final static String ALL_LABEL = "ALL";
public final static String SELECTED = " selected";

private String requestParamName;
private String beanName;
private String property;
private String selectedValue;

public YearsOptionsTag()
{
    super();
}

protected String createOptionTag( String label )
{
    return createOptionTag(label, label);
}

protected String createOptionTag( String label, String value )
{
    StringBuffer sb = new StringBuffer();

    sb.append("<option value=\"" + value + "\"");
    if ( isSelected(value) )
    {
        sb.append(SELECTED);
    }

    sb.append(">" + label + "</option>\n");

    return sb.toString();
}

public int doStartTag() throws JspException
{
    setSelectedValueFromRequestOrBean();

    String optionTags = getOptionTags();

    try
    {
        pageContext.getOut().write(optionTags);
    } catch (Exception e)
    {
        e.printStackTrace();
    }

    return SKIP_BODY;
}

/**
 * 
 * @return String
 */
public String getBeanName()
{
    return beanName;
}

protected String getOptionTags()
{
    StringBuffer sb = new StringBuffer();

    sb.append(createOptionTag(ALL_LABEL, ALL_VALUE));

    GregorianCalendar calendar = new GregorianCalendar();
    int startingYear = calendar.get(Calendar.YEAR) + 1;

    for (int i = 0; i < NUMBER_OF_YEARS; i++)
    {
        int currentYear = startingYear - i;
        sb.append(createOptionTag(String.valueOf(currentYear)));
    }

    return sb.toString();
}

/**
 * 
 * @return String
 */
public String getProperty()
{
    return property;
}

/**
 * 
 * @return String
 */
public String getRequestParamName()
{
    return requestParamName;
}

protected String getSelectedValue()
{
    return selectedValue;
}

protected boolean isSelected( String value )
{
    return selectedValue.equals(value);
}

/**
 * 
 * @param newBeanName
 *            String
 */
public void setBeanName( String newBeanName )
{
    beanName = newBeanName;
}

/**
 * 
 * @param newProperty
 *            String
 */
public void setProperty( String newProperty )
{
    property = newProperty;
}

/**
 * 
 * @param newRequestParamName
 *            String
 */
public void setRequestParamName( String newRequestParamName )
{
    requestParamName = newRequestParamName;
}
 public void setSelectedValue(String val ) {
	 
	 selectedValue = val;
	 
	 
 }
protected void setSelectedValueFromRequestOrBean()
{
    if ( getRequestParamName() != null )
    {
        selectedValue = pageContext.getRequest().getParameter(
                getRequestParamName());
    } else if ( getBeanName() != null )
    {
        Object bean = pageContext.findAttribute(getBeanName());

        if ( getProperty() != null )
        {
            String methodName = "get"
                    + getProperty().substring(0, 1).toUpperCase()
                    + getProperty().substring(1);
            try
            {
                Method method = bean.getClass().getMethod(methodName, new Class[] {});
                selectedValue = method.invoke(bean, new Object[] {}).toString();
            } catch (Exception e)
            {
                throw new RuntimeException(
                        "Error attempting to access method: "
                                + bean.getClass().getName() + "." + methodName
                                + " in YearsOptionsTag", e);
            }
        } else
        {
            selectedValue = bean.toString();
        }
    }

    selectedValue = (selectedValue == null) ? "" : selectedValue;
}
}