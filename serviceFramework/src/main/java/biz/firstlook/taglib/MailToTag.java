package biz.firstlook.taglib;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang.StringUtils;

public class MailToTag extends AbstractHtmlBodyTagSupport {
	
	private static final long serialVersionUID = -1674986024023216216L;

	/**
	 * A comma seperated values of email addresses
	 */ 
	private String recipients = null;
	
	/**
	 * Optional subject line
	 */
	private String subject = null;
	
	/**
	 * A comma seperated values of email address to carbon copy to (cc).
	 */
	private String carbonCopy = null;
	
	/**
	 * A comma seperated values of email address to blind carbon copy to (bcc).
	 */
	private String blindCarbonCopy = null;
	
	/**
	 * The body of the email.  Please follow the convention as the normal mailTo tag (use %0A for new line)
	 */
	private String body = null;
	
	private StringBuilder openTag = null;
	private static String CLOSE_TAG = "</a>";
	
	@Override
	public int doStartTag() throws JspException {
		openTag = new StringBuilder();
		openTag.append("<a href=").append(getQuote()).append("mailto:");
		if(StringUtils.isNotBlank(recipients)) {
			openTag.append(recipients);
		}
		Map<String, String> queryString = new HashMap<String, String>();
		if(StringUtils.isNotBlank(subject)) {
			queryString.put("subject", subject);
		}
		if(StringUtils.isNotBlank(carbonCopy)) {
			queryString.put("carbonCopy", carbonCopy);
		}
		if(StringUtils.isNotBlank(blindCarbonCopy)) {
			queryString.put("blindCarbonCopy", blindCarbonCopy);
		}
		if(StringUtils.isNotBlank(body)) {
			queryString.put("body", body);
		}
		if(!queryString.isEmpty()) {
			openTag.append("?");
		}
		Iterator<String> queryIter = queryString.keySet().iterator();
		while(queryIter.hasNext()) {
			String key = queryIter.next();
			openTag.append(key);
			openTag.append("=");
			openTag.append(queryString.get(key));
			if(queryIter.hasNext()) {
				openTag.append("&");
			}
		}
		openTag.append(getQuote()).append(">");
		try {
			JspWriter writer = pageContext.getOut(); 
			writer.write(openTag.toString());
		} catch (Exception e) {
			throw new JspTagException("MailToTag: " + e.getMessage());
		}
		return EVAL_BODY_BUFFERED;
	}

	@Override
	public int doAfterBody() throws JspException {
		try {
			if(bodyContent != null) {
				JspWriter out = getPreviousOut();
				out.print(bodyContent.getString());
				bodyContent.clear();
			}
		} catch(Exception e) {
			throw new JspTagException("MailToTag: " + e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		try {
			pageContext.getOut().write(CLOSE_TAG);
		} catch (Exception e) {
			throw new JspTagException("MailToTag: " + e.getMessage());
		}
		return EVAL_PAGE;
	}
	
	@Override
	public void reset() {
		recipients = null;
		subject = null;
		carbonCopy = null;
		blindCarbonCopy = null;
		body = null;
		openTag = null;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCarbonCopy() {
		return carbonCopy;
	}

	public void setCarbonCopy(String carbonCopy) {
		this.carbonCopy = carbonCopy;
	}

	public String getBlindCarbonCopy() {
		return blindCarbonCopy;
	}

	public void setBlindCarbonCopy(String blindCarbonCopy) {
		this.blindCarbonCopy = blindCarbonCopy;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
