package biz.firstlook.taglib;

import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * This class holds the standard html attributes.  
 * Subclasses that produce tags with html output should extend this tag to gain all the standard properties of a html tag.
 * @author bfung
 * @see <a href="http://www.w3schools.com/tags/ref_standardattributes.asp">http://www.w3schools.com/tags/ref_standardattributes.asp</a>
 */
public abstract class AbstractHtmlBodyTagSupport extends BodyTagSupport {
	
	/**
	 * The class of the element. 
	 */
	private String cssClass = null;
	
	//the id property is provided by TagSupport class
	
	/**
	 * An inline style definition.
	 */
	private String style = null;
	
	/**
	 * A text to display in a tool tip.
	 */
	private String title = null;
	
	/**
	 * Helps with tags and quote escapes.  By default, child tags should use double quotes unless this flag is set true.
	 */
	private boolean useSingleQuote = false;

	protected String getQuote() {
		return useSingleQuote ? "'" : "\"";
	}
	
	public boolean isUseSingleQuote() {
		return useSingleQuote;
	}

	public void setUseSingleQuote(boolean useSingleQuote) {
		this.useSingleQuote = useSingleQuote;
	}
	
	@Override
	public final void release() {
		reset();
		cssClass = null;
		style = null;
		title = null;
		useSingleQuote = false;
		super.release();
	}
	
	/**
	 * A method for subclasses to implement in lieu of the release method due to state held in the parent class.
	 */
	protected abstract void reset();

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
