package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_CIAPreferences")
public class CiaPreference {
	
	@Id
	@Column(name="BusinessUnitID")
	private Integer id;
	private Integer powerZoneGroupingThreshold = Integer.valueOf(10);
	private Integer bucketAllocationMinimumThreshold = Integer.valueOf(5);
	private Integer targetDaysSupply = Integer.valueOf(45);
	private Integer marketPerformersDisplayThreshold = Integer.valueOf(3);
	private Integer marketPerformersInStockThreshold = Integer.valueOf(3);
	private Integer marketPerformersUnitsThreshold = Integer.valueOf(6);
	private Double marketPerformersZipCodeThreshold = Double.valueOf(0.80d);
	private Integer ciaStoreTargetInventoryBasisPeriodId = Integer.valueOf(1); // 13/13
	private Integer ciaCoreModelDeterminationBasisPeriodId = Integer.valueOf(5); // 26/0
	private Integer ciaPowerzoneModelTargetInventoryBasisPeriodId = Integer.valueOf(1); // 13/13
	private Integer ciaCoreModelYearAllocationBasisPeriodId = Integer.valueOf(5); // 26/0
	private Integer gdLightProcessorTimePeriodId = Integer.valueOf(5); // 26/0
	private Integer salesHistoryDisplayTimePeriodId = Integer.valueOf(5); // 26/0
	
	public CiaPreference() {
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBucketAllocationMinimumThreshold() {
		return bucketAllocationMinimumThreshold;
	}

	public void setBucketAllocationMinimumThreshold(
			Integer bucketAllocationMinimumThreshold) {
		this.bucketAllocationMinimumThreshold = bucketAllocationMinimumThreshold;
	}

	public Integer getCiaCoreModelDeterminationBasisPeriodId()	{
		return ciaCoreModelDeterminationBasisPeriodId;
	}

	public void setCiaCoreModelDeterminationBasisPeriodId( Integer ciaCoreModelDeterminationBasisPeriodId )	{
		this.ciaCoreModelDeterminationBasisPeriodId = ciaCoreModelDeterminationBasisPeriodId;
	}

	public Integer getCiaCoreModelYearAllocationBasisPeriodId()	{
		return ciaCoreModelYearAllocationBasisPeriodId;
	}

	public void setCiaCoreModelYearAllocationBasisPeriodId( Integer ciaCoreModelYearAllocationBasisPeriodId ){
		this.ciaCoreModelYearAllocationBasisPeriodId = ciaCoreModelYearAllocationBasisPeriodId;
	}

	public Integer getCiaPowerzoneModelTargetInventoryBasisPeriodId(){
		return ciaPowerzoneModelTargetInventoryBasisPeriodId;
	}

	public void setCiaPowerzoneModelTargetInventoryBasisPeriodId( Integer ciaPowerzoneModelTargetInventoryBasisPeriodId ){
		this.ciaPowerzoneModelTargetInventoryBasisPeriodId = ciaPowerzoneModelTargetInventoryBasisPeriodId;
	}

	public Integer getCiaStoreTargetInventoryBasisPeriodId(){
		return ciaStoreTargetInventoryBasisPeriodId;
	}
	
	public void setCiaStoreTargetInventoryBasisPeriodId( Integer ciaStoreTargetInventoryBasisPeriodId ){
		this.ciaStoreTargetInventoryBasisPeriodId = ciaStoreTargetInventoryBasisPeriodId;
	}

	public Integer getGdLightProcessorTimePeriodId() {
		return gdLightProcessorTimePeriodId;
	}

	public void setGdLightProcessorTimePeriodId(Integer gdLightProcessorTimePeriodId) {
		this.gdLightProcessorTimePeriodId = gdLightProcessorTimePeriodId;
	}

	public Integer getMarketPerformersDisplayThreshold() {
		return marketPerformersDisplayThreshold;
	}

	public void setMarketPerformersDisplayThreshold(
			Integer marketPerformersDisplayThreshold) {
		this.marketPerformersDisplayThreshold = marketPerformersDisplayThreshold;
	}

	public Integer getMarketPerformersUnitsThreshold() {
		return marketPerformersUnitsThreshold;
	}

	public void setMarketPerformersUnitsThreshold(
			Integer marketPerformersUnitsThreshold) {
		this.marketPerformersUnitsThreshold = marketPerformersUnitsThreshold;
	}

	public Double getMarketPerformersZipCodeThreshold() {
		return marketPerformersZipCodeThreshold;
	}

	public void setMarketPerformersZipCodeThreshold(
			Double marketPerformersZipCodeThreshold) {
		this.marketPerformersZipCodeThreshold = marketPerformersZipCodeThreshold;
	}

	public Integer getMarketPerformersInStockThreshold() {
		return marketPerformersInStockThreshold;
	}

	public void setMarketPerformersInStockThreshold(
			Integer marketPerformersInStockThreshold) {
		this.marketPerformersInStockThreshold = marketPerformersInStockThreshold;
	}

	public Integer getPowerZoneGroupingThreshold() {
		return powerZoneGroupingThreshold;
	}

	public void setPowerZoneGroupingThreshold(Integer powerZoneGroupingThreshold) {
		this.powerZoneGroupingThreshold = powerZoneGroupingThreshold;
	}

	public Integer getSalesHistoryDisplayTimePeriodId() {
		return salesHistoryDisplayTimePeriodId;
	}

	public void setSalesHistoryDisplayTimePeriodId(
			Integer salesHistoryDisplayTimePeriodId) {
		this.salesHistoryDisplayTimePeriodId = salesHistoryDisplayTimePeriodId;
	}

	public Integer getTargetDaysSupply() {
		return targetDaysSupply;
	}

	public void setTargetDaysSupply(Integer targetDaysSupply) {
		this.targetDaysSupply = targetDaysSupply;
	}	
}
