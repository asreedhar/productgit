package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DealerGridThresholds")
public class DealerGridThresholds {

	@Id
	@Column(name="DealerGridThresholdsId")
	private Integer id;
	
	@Column(name="BusinessUnitID")
	private Integer dealerId;

	private Integer firstValuationThreshold;
    private Integer secondValuationThreshold;
    private Integer thirdValuationThreshold;
    private Integer fourthValuationThreshold;
    private Integer firstDealThreshold;
	private Integer secondDealThreshold;
    private Integer thirdDealThreshold;
    private Integer fourthDealThreshold;
	
	public DealerGridThresholds() {}

	public Integer getDealerId() {
		return dealerId;
	}

	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}

	public Integer getFirstDealThreshold() {
		return firstDealThreshold;
	}

	public void setFirstDealThreshold(Integer firstDealThreshold) {
		this.firstDealThreshold = firstDealThreshold;
	}

	public Integer getFirstValuationThreshold() {
		return firstValuationThreshold;
	}

	public void setFirstValuationThreshold(Integer firstValuationThreshold) {
		this.firstValuationThreshold = firstValuationThreshold;
	}

	public Integer getFourthDealThreshold() {
		return fourthDealThreshold;
	}

	public void setFourthDealThreshold(Integer fourthDealThreshold) {
		this.fourthDealThreshold = fourthDealThreshold;
	}

	public Integer getFourthValuationThreshold() {
		return fourthValuationThreshold;
	}

	public void setFourthValuationThreshold(Integer fourthValuationThreshold) {
		this.fourthValuationThreshold = fourthValuationThreshold;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSecondDealThreshold() {
		return secondDealThreshold;
	}

	public void setSecondDealThreshold(Integer secondDealThreshold) {
		this.secondDealThreshold = secondDealThreshold;
	}

	public Integer getSecondValuationThreshold() {
		return secondValuationThreshold;
	}

	public void setSecondValuationThreshold(Integer secondValuationThreshold) {
		this.secondValuationThreshold = secondValuationThreshold;
	}

	public Integer getThirdDealThreshold() {
		return thirdDealThreshold;
	}

	public void setThirdDealThreshold(Integer thirdDealThreshold) {
		this.thirdDealThreshold = thirdDealThreshold;
	}

	public Integer getThirdValuationThreshold() {
		return thirdValuationThreshold;
	}

	public void setThirdValuationThreshold(Integer thirdValuationThreshold) {
		this.thirdValuationThreshold = thirdValuationThreshold;
	}

}
