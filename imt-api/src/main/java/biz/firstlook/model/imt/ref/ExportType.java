package biz.firstlook.model.imt.ref;

public enum ExportType {

	EBIZ("eBiz"),
	SIS("SIS"),
	REY_DIRECT("ReyDirect");
	
	private String name;
	
	
	private ExportType(String formFieldName) {
		this.name = formFieldName;
	}
	
	public static ExportType getExportTypeByName(String name ){
		ExportType toBeReturned = null;
		if ( EBIZ.name.equals(name)) {
			toBeReturned = EBIZ;
		} else if ( SIS.name.equals(name)) {
			toBeReturned = SIS;
		} else if ( REY_DIRECT.name.equals(name)) {
			toBeReturned = REY_DIRECT;
		}  
		return toBeReturned;
	}
	
}
