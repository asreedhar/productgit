package biz.firstlook.model.imt.ref;


public enum BusinessUnitType {
	None,
	Corporate,
	Region,
	DealerGroup,
	Dealer,
	Firstlook;
}
