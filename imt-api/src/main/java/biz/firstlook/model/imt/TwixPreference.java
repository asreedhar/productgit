package biz.firstlook.model.imt;

// Transient
// Just a simple data bean to hold data. 
public class TwixPreference {

	private Integer id;
	private Boolean twixEnabled = Boolean.FALSE;
	private String twixURL = "";
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getTwixEnabled() {
		return twixEnabled;
	}
	public void setTwixEnabled(Boolean twixEnabled) {
		this.twixEnabled = twixEnabled;
	}
	public String getTwixURL() {
		return twixURL;
	}
	public void setTwixURL(String twixURL) {
		this.twixURL = twixURL;
	}
	
}
