package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_DealerATCAccessGroups")
public class ATCAccessGroup implements Serializable{
    
    private static final long serialVersionUID = -1210269127013601712L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="DealerATCAccessGroupsId")
    private Integer id;
    private Integer businessUnitId;
    private Integer accessGroupId;
    
    public Integer getAccessGroupId() {
        return accessGroupId;
    }
    public void setAccessGroupId(Integer accessGroupId) {
        this.accessGroupId = accessGroupId;
    }
    public Integer getBusinessUnitId() {
        return businessUnitId;
    }
    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

}
