package biz.firstlook.model.imt.ref;

public enum Day {
	NONE,
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
}
