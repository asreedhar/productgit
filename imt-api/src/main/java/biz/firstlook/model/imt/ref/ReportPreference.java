package biz.firstlook.model.imt.ref;

public enum ReportPreference {
	DASHBOARD,
	CAP,
	TAP,
	CBG,
	TMG,
	ACTIVE_REDISTRIBUTION,
	STORETRADE,
	GROUPTRADE;

	public int getFactor() {
		int ord = ordinal();
		int factor = (int) Math.pow( 2.0, ord );
		return factor;
	}
}
