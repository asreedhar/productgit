package biz.firstlook.model.imt;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;
import org.hibernate.annotations.Where;

import biz.firstlook.model.imt.compare.MemberComparator;
import biz.firstlook.model.imt.ref.BusinessUnitType;
import biz.firstlook.model.imt.ref.JdPowerRegion;

@Entity
@DiscriminatorValue(value="4")
@SecondaryTable(name="DealerPreference_JDPowerSettings", 
                pkJoinColumns={
					@PrimaryKeyJoinColumn(name="BusinessUnitId")
				})
public class Dealer extends BusinessUnit implements Serializable {

	private static final long serialVersionUID = 7106666197082972804L;

	@OneToOne(mappedBy="dealer")
	private DealerPreference preference;

	@OneToOne(mappedBy="dealer",cascade=CascadeType.ALL)
	private BusinessUnitMarketDealer businessUnitMarketDealer;
	
	@OneToOne()
	@JoinColumn(name="PowerRegionID", unique=true, nullable=true, updatable=true, table="DealerPreference_JDPowerSettings")
	private JdPowerRegion powerRegion;
	
	@ManyToMany(
		targetEntity=Franchise.class
	)
	@JoinTable(
        name="DealerFranchise",
        joinColumns={@JoinColumn(name="BusinessUnitID")},
        inverseJoinColumns={@JoinColumn(name="FranchiseID")}
    )
	private Set<Franchise> franchises;
	
	@ManyToMany
	@JoinTable(
			name="BusinessUnitRelationship",
			joinColumns=@JoinColumn(name="BusinessUnitID"),
			inverseJoinColumns=@JoinColumn(name="ParentID")
	)
	private Set<DealerGroup> dealerGroups;

	@OneToMany
	@JoinColumn(name="BusinessUnitID")
	@Where( clause="active = 1")
	private Set<DealerUpgrade> dealerUpgrades;
    
    @OneToMany
    @JoinColumn(name="BusinessUnitID")
    private Set<ATCAccessGroup> accessGroups;
    
	@ManyToMany(
			targetEntity=Member.class
		)
	@JoinTable(
        name="MemberAccess",
        joinColumns={@JoinColumn(name="BusinessUnitID")},
        inverseJoinColumns={@JoinColumn(name="MemberID")}
    )
    @Sort( type=SortType.COMPARATOR, comparator=MemberComparator.class)
	private SortedSet<Member> members;	

	@OneToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private DealerRisk risk;
	
	@OneToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private CiaPreference ciaPreferences;
	
	@OneToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private DealerAuctionPreference auctionPreference;
//    
//	@OneToMany
//	@JoinColumn(name="BusinessUnitID")
	@OneToMany(mappedBy="dealer")
	@Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private Set<DMSExportPreference> dmsExportPreferences;
	
	@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "BusinessUnitID", referencedColumnName = "BusinessUnitID", insertable = false, updatable = false)
    @OrderBy("transferPriceUnitCostValue")
	private Set<DealerTransferPriceUnitCostRule> dealerTransferPriceUnitCostRules = Collections.emptySet();
	
	@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "BusinessUnitID", referencedColumnName = "BusinessUnitID", insertable = false, updatable = false)
	private Set<DealerTransferAllowRetailOnlyRule> dealerTransferAllowRetailOnlyRules = Collections.emptySet();
	
	@OneToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private DealerPreferenceKBBConsumerTool kbbConsumerToolPreferences;
	
	@ManyToOne
	@JoinTable(schema="dbo",name="DealerSalesChannel",
		joinColumns = @JoinColumn(name="BusinessUnitID", unique=true),
		inverseJoinColumns = @JoinColumn(name="SalesChannelID")
	)
	private SalesChannel salesChannel;

	public Dealer() {
		setType(BusinessUnitType.Dealer);
	}

	public DealerPreference getPreference() {
		return preference;
	}

	public void setPreference(DealerPreference preference) {
		this.preference = preference;
	}

	public Set<Franchise> getFranchises() {
		return franchises;
	}

	public void setFranchises(Set<Franchise> franchises) {
		this.franchises = franchises;
	}

	public DealerGroup getDealerGroup() {
		DealerGroup group = null;
		if( dealerGroups != null && !dealerGroups.isEmpty() ) {
			group = (DealerGroup) dealerGroups.toArray()[0];
		} 
		return group;
	}

	public void setDealerGroup(DealerGroup dealerGroup) {
		if( dealerGroups == null ) {
			dealerGroups = new HashSet<DealerGroup>();
		}
		this.dealerGroups.clear();
		this.dealerGroups.add( dealerGroup );
	}

	public SortedSet<Member> getMembers() {
		return members;
	}

	public void setMembers(SortedSet<Member> members) {
		this.members = members;
	}

	public DealerRisk getRisk() {
		return risk;
	}

	public void setRisk(DealerRisk risk) {
		this.risk = risk;
	}

	public CiaPreference getCiaPreferences() {
		return ciaPreferences;
	}

	public void setCiaPreferences(CiaPreference ciaPreferences) {
		this.ciaPreferences = ciaPreferences;
	}
	
	public boolean eligibleForSubscription( Set<DealerUpgradeCode> upgrades )
	{
		boolean eligible = true;
		boolean hasIt = false;
		Iterator<DealerUpgradeCode> requiredUpgradesIter = upgrades.iterator();
		while ( requiredUpgradesIter.hasNext() )
		{
			DealerUpgradeCode neededUpgrade = (DealerUpgradeCode)requiredUpgradesIter.next();
			Iterator<DealerUpgrade> currentUpgragesIter = dealerUpgrades.iterator();
			hasIt = false;
			while ( currentUpgragesIter.hasNext() )
			{
				DealerUpgrade upgrade = (DealerUpgrade)currentUpgragesIter.next();
				if ( upgrade.getDealerUpgradeCD().equals( neededUpgrade.getId() ))
				{
					hasIt = true;
				}
			}
				
			if ( !hasIt ) 
			{
				eligible = false;
				break;
			}
			
		}
		return eligible;
	}

	public Set< DealerUpgrade > getDealerUpgrade()
	{
		return dealerUpgrades;
	}

	public void setDealerUpgrade( Set< DealerUpgrade > dealerUpgrades )
	{
		this.dealerUpgrades = dealerUpgrades;
	}

	public DealerAuctionPreference getAuctionPreference() {
		return auctionPreference;
	}

	public void setAuctionPreference(DealerAuctionPreference auctionPreference) {
		this.auctionPreference = auctionPreference;
	}

	public BusinessUnitMarketDealer getBusinessUnitMarketDealer() {
		return businessUnitMarketDealer;
	}

	public void setBusinessUnitMarketDealer(
			BusinessUnitMarketDealer businessUnitMarketDealer) {
		this.businessUnitMarketDealer = businessUnitMarketDealer;
	}

    public Set<ATCAccessGroup> getAccessGroups() {
        return accessGroups;
    }

    public void setAccessGroups(Set<ATCAccessGroup> accessGroups) {
        this.accessGroups = accessGroups;
    }

	public JdPowerRegion getJdPowerRegion() {
		return powerRegion;
	}

	public void setJdPowerRegion( JdPowerRegion powerRegion ) {
		this.powerRegion = powerRegion;
	}

	public Set<DMSExportPreference> getDmsExportPreferences() {
		return dmsExportPreferences;
	}

	public void setDmsExportPreferences(
			Set<DMSExportPreference> dmsExportPreferences) {
		this.dmsExportPreferences = dmsExportPreferences;
	}

	public Boolean getGoLive() {
		if(preference.getGoLiveDate() != null){
			return true;
		}
		return false;
	}

	public void setGoLive(Boolean goLive) {
		if(goLive && preference.getGoLiveDate() == null){
			Date date = new java.util.Date();
			preference.setGoLiveDate(date);
		}
		else if(!goLive){
			preference.setGoLiveDate(null);
		}
	}

	/**
	 * Work around for hibernate bug.  When loading this collection, hibernate throws an error about
	 * not being able to get the Dealer.id property by reflection on the TransferPriceUnitCostValue object.
	 * @return
	 */
	public Set<DealerTransferPriceUnitCostRule> getDealerTransferPriceUnitCostRules() {
		return dealerTransferPriceUnitCostRules;
	}

	public Set<DealerTransferAllowRetailOnlyRule> getDealerTransferAllowRetailOnlyRules() {
		return dealerTransferAllowRetailOnlyRules;
	}
	
	public DealerPreferenceKBBConsumerTool getKBBConsumerToolPreferences() {
		return kbbConsumerToolPreferences == null ?
			new DealerPreferenceKBBConsumerTool() :
			kbbConsumerToolPreferences;
	}

	public void setKBBConsumerToolPreferences(DealerPreferenceKBBConsumerTool kbbConsumerToolPreferences) {
		this.kbbConsumerToolPreferences = kbbConsumerToolPreferences;
	}
	
	public SalesChannel getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(SalesChannel salesChannel) {
		this.salesChannel = salesChannel;
	}
}
