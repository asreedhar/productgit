package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

@Entity
@Table(name = "Dealers")
public class MarketMapping {

    @Id
    @Column(name = "DealerNumber", insertable = false, nullable = false, updatable = false)
    private Integer id;

    @Column(name = "DealerName", insertable = false, length = 100, nullable = false, updatable = false)
    private String name;

    @Column(name = "DealerAddress", insertable = false, length = 100, nullable = true, updatable = false)
    private String address;

    @Column(name = "DealerCity", insertable = false, length = 100, nullable = true, updatable = false)
    private String city;

    @Column(name = "DealerCounty", insertable = false, length = 100, nullable = true, updatable = false)
    private String county;
    
    @Column(name = "DealerState", insertable = false, length = 20, nullable = true, updatable = false)
    private String state;

    @Column(name = "DealerZipCode", insertable = false, length = 11, nullable = true, updatable = false)
    private String zipCode;

    @Column(name = "DealerSourceID", insertable = false, nullable = false, updatable = false)
    private Integer sourceId;

    public Integer getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String getAddress() {
	return address;
    }

    public String getCity() {
	return city;
    }
    
    public String getCounty() {
	return county;
    }

    public String getState() {
	return state;
    }

    public String getZipCode() {
	return zipCode;
    }

    public Integer getSourceId() {
	return sourceId;
    }

    @Override
    public String toString() {
	return ReflectionToStringBuilder.toString(this);
    }
}
