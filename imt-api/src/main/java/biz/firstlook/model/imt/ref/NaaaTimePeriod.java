package biz.firstlook.model.imt.ref;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="dbo.PeriodDates")
public class NaaaTimePeriod implements DatabaseEnum {
    
    @Id
    @Column(name="PeriodID")
    private Integer id;
    
    @Column(name="Description")
    private String description;
    
    @Column(name="Rank")
    private Integer rank;
    
    protected NaaaTimePeriod(){
	super();
    }
    
    public Integer getId() {
        return id;
    }
    
    public String getDescription() {
        return description;
    }
    
    /**
     * The order to display this enum
     */
    public int ordinal(){
	return rank.intValue();
    }
}
