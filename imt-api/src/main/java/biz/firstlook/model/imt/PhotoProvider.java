package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PhotoProvider_ThirdPartyEntity")
public class PhotoProvider implements Serializable {
	
	private static final long serialVersionUID = -7259881006899215446L;
	@Id
	Integer photoProviderId;
	String datafeedCode;
	Integer photoStorageCode;
	
	public String getDatafeedCode() {
		return datafeedCode;
	}
	public void setDatafeedCode(String datafeedCode) {
		this.datafeedCode = datafeedCode;
	}
	public Integer getPhotoProviderId() {
		return photoProviderId;
	}
	public void setPhotoProviderId(Integer photoProviderId) {
		this.photoProviderId = photoProviderId;
	}
	public Integer getPhotoStorageCode() {
		return photoStorageCode;
	}
	public void setPhotoStorageCode(Integer photoStorageCode) {
		this.photoStorageCode = photoStorageCode;
	}
}
