package biz.firstlook.model.imt;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue(value="1")
public class Corporation extends BusinessUnit implements Serializable {

	private static final long serialVersionUID = -8937764938144533908L;

	@ManyToMany
	@JoinTable(
			name="BusinessUnitRelationship",
			joinColumns=@JoinColumn(name="ParentID"),
			inverseJoinColumns=@JoinColumn(name="BusinessUnitID")
	)
	public Set<DealerGroup> groups;
	
	public Corporation() {}

	public Set<DealerGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<DealerGroup> groups) {
		this.groups = groups;
	}
	
	

}
