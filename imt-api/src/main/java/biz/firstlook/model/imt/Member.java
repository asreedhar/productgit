package biz.firstlook.model.imt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.hibernate.annotations.Cascade;

import biz.firstlook.model.imt.ref.LoginStatus;
import biz.firstlook.model.imt.ref.MemberType;
import biz.firstlook.model.imt.ref.NewRole;
import biz.firstlook.model.imt.ref.ReportingMethod;
import biz.firstlook.model.imt.ref.UsedRole;
import biz.firstlook.model.imt.ref.UserRole;

@Entity
@Table(name="Member")
public class Member {
	
private static final String HASH_ALGORITHM = "SHA";
private static Logger logger = Logger.getLogger( Member.class );

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MemberID")
	private Integer id;

	private String login;
	private String password;
	private String salutation;
	private String firstName;
	private String preferredFirstName;
	private String middleInitial;
	private String lastName;
	private String officePhoneNumber;
	private String officePhoneExtension;
	private String officeFaxNumber;
	private String mobilePhoneNumber;
	private String pagerNumber;
	private String emailAddress;
	private String reportMethod;
	
	@Transient
	private ReportingMethod reportingMethod;
	
	private String notes;
	private Integer dashboardRowDisplay;
	
	@Column(name="LoginStatus")
	private LoginStatus loginStatus;
	
	@Column(name="MemberType")
	private MemberType memberType;
	
	@Temporal(TemporalType.DATE)
	private Date createDate;
	
	@Column(name="userRoleCD")
	private UserRole userRole;
	
	@Transient
	private UsedRole usedRole;
	@Transient
	private NewRole newRole;
	
	private Integer defaultDealerGroupId;
	
	@Transient
	private String defaultDealerGroupName;
	

	private Boolean active = true;

	@Column(name="sms_address")
	private String smsAddress;
	
	@ManyToOne
	@JoinColumn(name="JobTitleID")
	private JobTitle jobTitle;
	
	@OneToMany
	@JoinColumn(name="MemberID")
	@Cascade({org.hibernate.annotations.CascadeType.DELETE})
	private Set<MemberCredential> memberCredentials;
	
	@ManyToMany(
			targetEntity=Dealer.class
		)
	@JoinTable(
        name="MemberAccess",
        joinColumns={@JoinColumn(name="MemberID")},
        inverseJoinColumns={@JoinColumn(name="BusinessUnitID")}
    )
	private Set<Dealer> dealers;
	
	@ManyToMany(
			targetEntity=Role.class
		)
	@JoinTable(
        name="MemberRole",
        joinColumns={@JoinColumn(name="MemberID")},
        inverseJoinColumns={@JoinColumn(name="RoleID")}
    )
	private Set<Role> roles;
	
	@Column(name="InventoryOverviewSortOrderType")
	private Integer inventoryOverviewSortOrderType = 0; 

	private Integer activeInventoryLaunchTool = 0; 
	
	@Column(name="GroupHomePageID")
	private Integer groupHomePageId;

	public Member() {
		roles = new HashSet<Role>();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getDashboardRowDisplay() {
		return dashboardRowDisplay;
	}

	public void setDashboardRowDisplay(Integer dashboardRowDisplay) {
		this.dashboardRowDisplay = dashboardRowDisplay;
	}

	public Integer getDefaultDealerGroupId() {
		return defaultDealerGroupId;
	}

	public void setDefaultDealerGroupId(Integer defaultDealerGroupId) {
		this.defaultDealerGroupId = defaultDealerGroupId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public JobTitle getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(JobTitle jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public LoginStatus getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(LoginStatus loginStatus) {
		this.loginStatus = loginStatus;
	}

	public MemberType getMemberType() {
		return memberType;
	}

	public void setMemberType(MemberType memberType) {
		this.memberType = memberType;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getOfficeFaxNumber() {
		return officeFaxNumber;
	}

	public void setOfficeFaxNumber(String officeFaxNumber) {
		this.officeFaxNumber = officeFaxNumber;
	}

	public String getOfficePhoneExtension() {
		return officePhoneExtension;
	}

	public void setOfficePhoneExtension(String officePhoneExtension) {
		this.officePhoneExtension = officePhoneExtension;
	}

	public String getOfficePhoneNumber() {
		return officePhoneNumber;
	}

	public void setOfficePhoneNumber(String officePhoneNumber) {
		this.officePhoneNumber = officePhoneNumber;
	}

	public String getPagerNumber() {
		return pagerNumber;
	}

	public void setPagerNumber(String pagerNumber) {
		this.pagerNumber = pagerNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if ( password != null)
		{
			if (this.password != null && this.password.equals(password)) {
				// don't set the same password
				return;
			}

			MessageDigest digester = null;
			try
			{
				digester = MessageDigest.getInstance( HASH_ALGORITHM );
			}
			catch ( NoSuchAlgorithmException e )
			{
				logger.debug( "MISSING HASHING ALGORITHM: " + HASH_ALGORITHM );
			}
			this.password = new String( Hex.encodeHex( digester.digest( password.getBytes() ) ) );
			this.loginStatus = LoginStatus.NEW;
		} 
	}

	public String getPreferredFirstName() {
		return preferredFirstName;
	}

	public void setPreferredFirstName(String preferredFirstName) {
		this.preferredFirstName = preferredFirstName;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSmsAddress() {
		return smsAddress;
	}

	public void setSmsAddress(String smsAddress) {
		this.smsAddress = smsAddress;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public void setUserRole(String userRoleStr) {
		if (userRoleStr != null) {
			if (userRoleStr.equalsIgnoreCase(UserRole.ALL.name())) {
				setUserRole(UserRole.ALL);
			} else if (userRoleStr.equalsIgnoreCase(UserRole.NEW.name())) {
				setUserRole(UserRole.NEW);
			} else if (userRoleStr.equalsIgnoreCase(UserRole.USED.name())) {
				setUserRole(UserRole.USED);
			} else {
				setUserRole(UserRole.NONE);
			}
		} else {
			setUserRole(UserRole.NONE);
		}
	}
	
	public Set<Dealer> getDealers() {
		return dealers;
	}

	public void setDealers(Set<Dealer> dealers) {
		this.dealers = dealers;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public NewRole getNewRole() {
		if (newRole == null) {
			for (Role role : roles) {
				switch (role.getRoleId()) {
					case 5:
						setNewRole(NewRole.NO_ACCESS);
						break;
					case 6:
						setNewRole(NewRole.STANDARD);
						break;
				}
				if (this.newRole != null) {
					break;
				}
			}
			//	if new role was not matched then set NO_ACCESS as default
			if(newRole == null){
				setNewRole(NewRole.NO_ACCESS);
			}
		}
		
		return newRole;
	}

	public void setNewRole(NewRole newRole) {
		this.newRole = newRole;
	}
	
	public void setNewRole(String newRoleStr) {
		if (newRoleStr != null) {
			if (newRoleStr.equalsIgnoreCase(NewRole.STANDARD.name())) {
				setNewRole(NewRole.STANDARD);
			} else {
				setNewRole(NewRole.NO_ACCESS);
			}
		} else {
			setNewRole(NewRole.NO_ACCESS);
		}
	}
	

	public UsedRole getUsedRole() {
		if (usedRole == null) {
			for (Role role : roles) {
				switch (role.getRoleId()) {
					case 1:
						setUsedRole(UsedRole.NO_ACCESS);
						break;
					case 2:
						setUsedRole(UsedRole.APPRAISER);
						break;
					case 3:
						setUsedRole(UsedRole.STANDARD);
						break;
					case 4:
						setUsedRole(UsedRole.MANAGER);
						break;
				}
				if (this.usedRole != null) {
					break;
				}
			}
		}
		return usedRole;
	}

	
	public void setUsedRole(String usedRoleStr) {
		if (usedRoleStr != null) {
			if (usedRoleStr.equalsIgnoreCase(UsedRole.APPRAISER.name())) {
				setUsedRole(UsedRole.APPRAISER);
			}else if (usedRoleStr.equalsIgnoreCase(UsedRole.MANAGER.name())) {
				setUsedRole(UsedRole.MANAGER);
			}else if (usedRoleStr.equalsIgnoreCase(UsedRole.STANDARD.name())) {
				setUsedRole(UsedRole.STANDARD);
			} else {
				setUsedRole(UsedRole.NO_ACCESS);
			}
		} else {
			setUsedRole(UsedRole.NO_ACCESS);
		}
	}
	
	public void setUsedRole(UsedRole usedRole) {
		this.usedRole = usedRole;
	}

	public ReportingMethod getReportingMethod() {
		if (reportMethod != null) {
			if (reportMethod.equalsIgnoreCase("Fax")) {
				this.reportingMethod = ReportingMethod.Fax;
			} else if (reportMethod.equalsIgnoreCase("Email")) {
				this.reportingMethod = ReportingMethod.Email;
			}
		} else {
			this.reportingMethod = ReportingMethod.Fax;
		}
		return this.reportingMethod;
	}

	public void setReportingMethod(ReportingMethod reportMethodEnum) {
		this.reportingMethod = reportMethodEnum;
		this.reportMethod = reportingMethod.name();
	}

	public String getReportMethod() {
		return reportMethod;
	}

	public void setReportMethod(String reportMethod) {
		this.reportMethod = reportMethod;
		if (reportMethod != null) {
			if (reportMethod.equalsIgnoreCase(ReportingMethod.Fax.name())) {
				this.reportingMethod = ReportingMethod.Fax;
			} else if (reportMethod.equalsIgnoreCase(ReportingMethod.Email.name())) {
				this.reportingMethod = ReportingMethod.Email;
			}
		} else {
			this.reportingMethod = ReportingMethod.Fax;
		}
	}
	public Integer getInventoryOverviewSortOrderType() {
		return inventoryOverviewSortOrderType;
	}

	public void setInventoryOverviewSortOrderType(Integer inventoryOverviewSortOrderType) {
		this.inventoryOverviewSortOrderType = inventoryOverviewSortOrderType;
	}

	public Integer getActiveInventoryLaunchTool() {
		return activeInventoryLaunchTool;
	}

	public void setActiveInventoryLaunchTool(Integer activeInventoryLaunchTool) {
		this.activeInventoryLaunchTool = activeInventoryLaunchTool;
	}

	public String getDefaultDealerGroupName() {
		return defaultDealerGroupName;
	}

	public void setDefaultDealerGroupName(String defaultDealerGroupName) {
		this.defaultDealerGroupName = defaultDealerGroupName;
	}

	public Integer getGroupHomePageId() {
		return groupHomePageId;
	}

	public void setGroupHomePageId(Integer groupHomePageId) {
		this.groupHomePageId = groupHomePageId;
	}


	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	
	public Set<MemberCredential> getMemberCredentials() {
		return memberCredentials;
	}

	public void setMemberCredentials(Set<MemberCredential> memberCredentials) {
		this.memberCredentials = memberCredentials;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Member other = (Member) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
}
