package biz.firstlook.model.imt;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity	     
@Table(name="KBBArchiveDatasetOverride")
public class KBBArchiveDatasetOverride {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KBBArchiveDatasetOverrideID")
	private Integer id;
	private Integer businessUnitId;
	private Integer memberId;
	private Date startDate;
	private Date endDate;
	
	public KBBArchiveDatasetOverride()
	{
		this.startDate = Calendar.getInstance().getTime();
		Calendar midnightTonight = Calendar.getInstance();
		midnightTonight.set(Calendar.HOUR_OF_DAY, 0);
		midnightTonight.set(Calendar.MINUTE, 0);
		midnightTonight.set(Calendar.SECOND, 0);
		midnightTonight.add(Calendar.DAY_OF_MONTH, 1);
		this.endDate = midnightTonight.getTime();
	}
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberWhoSavedOverride) {
		this.memberId = memberWhoSavedOverride;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
}
