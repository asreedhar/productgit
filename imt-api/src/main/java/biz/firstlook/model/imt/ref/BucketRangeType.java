package biz.firstlook.model.imt.ref;

public enum BucketRangeType 
{
	AGING_INVENTORY_PLAN_USED( 1, "Aging Inventory Plan (Used)" ),
	TOTAL_INVENTORY_REPORT_NEW( 2, "Total Inventory Report (New)" ),
	TOTAL_INVENTORY_REPORT_USED( 3, "Total Inventory Report (Used)" ),
    NEW_AGING_INVENTORY_PLAN( 4, "New Aging Inventory Plan" ),
    AGING_INVENTORY_SHORT_LEASH( 5, "Aging Inventory - Short Leash" ),
    DEALER_GROUP_COMMAND_CENTER_INV_BREAKDOWN( 6, "Dealer Group Command Center Inventory Breakdown" ),
	PINGII_PRICING_AGE(7, "PING II Pricing Age "),
	PINGII_MILEAGE_ADJUSTMENT(8, "PING II Mileage Adjustment");
	
	private int inventoryBucketId;
	private String description;
	
	BucketRangeType( int inventoryBucketId, String description )
	{
	    this.inventoryBucketId = inventoryBucketId;
	    this.description = description;
	}
	
	public int getInventoryBucketId()
	{
	    return inventoryBucketId;
	}
	
	public String getDescription()
	{
	    return description;
	}
}
