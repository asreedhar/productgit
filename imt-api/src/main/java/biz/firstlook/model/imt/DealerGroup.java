package biz.firstlook.model.imt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value="3")
public class DealerGroup extends BusinessUnit implements Serializable {

	private static final long serialVersionUID = 3880452852569673430L;

	@OneToOne(mappedBy="group")
	private DealerGroupPreference preference;

	@ManyToMany
	@JoinTable(
			name="BusinessUnitRelationship",
			joinColumns=@JoinColumn(name="ParentID"),
			inverseJoinColumns=@JoinColumn(name="BusinessUnitID")
	)
	public Set<Dealer> dealers;
	
	@ManyToMany
	@JoinTable(
			name="BusinessUnitRelationship",
			joinColumns=@JoinColumn(name="BusinessUnitID"),
			inverseJoinColumns=@JoinColumn(name="ParentID")
	)
	private Set<Corporation> corporations;

	
	public DealerGroup() {
		super();
	}
	
	/**
	 * Finds the Dealer in the DealerGroup by Dealer ID
	 * @param id the Dealer's ID
	 * @return the Dealer who has the specified id, or null if the the DealerID is not part of this DealerGroup.
	 */
	public Dealer getDealer(Integer id) {
		for(Dealer dealer : dealers) {
			if(dealer.getId().equals(id)) {
				return dealer;
			}
		}
		return null;
	}

	public Set<Dealer> getDealers() {
		return dealers;
	}

	public void setDealers(Set<Dealer> dealers) {
		this.dealers = dealers;
	}
	
	/**
	 * Query method on the DealerGroup object.
	 * This is a read-only collection.
	 * 
	 * @return an unmodifiable Collection of {@link Member}s
	 */
	public Collection<Member> getMembers() {
		//hide the fact that this is a Set.  We want unique Members.
		Set<Member> members = new HashSet<Member>();
		for( Dealer dealer : dealers ) {
			for( Member member : dealer.getMembers() ) {
				members.add( member );
			}
		}
		return Collections.unmodifiableCollection( members );
	}
	
	/**
	 * Adds a {@link Member} to all {@link Dealer}s of this DealerGroup.
	 * @param member
	 * @return a Collection of error messages.  If the collection is empty, there were no errors.
	 */
	public Collection<String> add(Member member) {
		List<String> errors = new ArrayList<String>();
		for(Dealer dealer : dealers) {
			Collection<Member> members = dealer.getMembers();
			if( !members.contains( member ) ) { 
				try {
					members.add( member );
				} catch(Exception e) {
					StringBuilder errorMsg = new StringBuilder();
					errorMsg.append( e ).append(" ");
					errorMsg.append( dealer.getId() );
					errors.add( errorMsg.toString() );
				}
			}
		}
		return errors;
	}
	
	public void remove( Member member ) {
		for(Dealer dealer : dealers) {
			Collection<Member> members = dealer.getMembers();
			members.remove( member );
		}
	}

	public DealerGroupPreference getPreference() {
		return preference;
	}

	public void setPreference(DealerGroupPreference preference) {
		this.preference = preference;
	}	
	
	public Corporation getCorporation() {
		Corporation group = null;
		if( corporations != null && !corporations.isEmpty() ) {
			group = (Corporation) corporations.toArray()[0];
		} 
		return group;
	}

	public void setCorporation(Corporation corporation) {
		if( corporations == null ) {
			corporations = new HashSet<Corporation>();
		}
		this.corporations.clear();
		this.corporations.add( corporation );
	}
}
