package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GlobalParam")
public class GlobalParam implements Serializable {

	private static final long serialVersionUID = -8507497574449827104L;
	
	public static final String PARAM_EBIZID = "eBizId";
	public static final String PARAM_EBIZAUTH = "eBizAuth";
	public static final String PARAM_SIS = "SIS_URL";
	public static final String PARAM_REY_DIRECT = "Reynolds_Direct_URL";

	@Id @GeneratedValue
	private Integer id;
	private String name;
	private String value;
	
	protected GlobalParam() {
		super();
	}
	
	public GlobalParam( String name ) {
		this.name = name;
	}
	
	public GlobalParam( String name, String value ) {
		this.name = name;
		setValue( value );
	}
	
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}
	
	public void setValue( String value ) {
		this.value = value;
	}
}
