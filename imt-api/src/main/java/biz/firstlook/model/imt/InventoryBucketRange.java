package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InventoryBucketRanges")
public class InventoryBucketRange implements Serializable {

	private static final long serialVersionUID = -7385259975807305748L;

	public InventoryBucketRange() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "InventoryBucketRangeID")
	private Integer id;
	private Integer sortOrder;
	private Integer inventoryBucketID;
	private Integer rangeID;
	private String description;
	private Integer low;
	private Integer high;
	private Integer lights;
	private Integer businessUnitID;
	private String longDescription;
	private Integer value1;
	private Integer value2;
	private Integer value3;
	
	private transient int numberOfDaysInBucket;

	public Integer getBusinessUnitID() {
		return businessUnitID;
	}

	public void setBusinessUnitID(Integer businessUnitID) {
		this.businessUnitID = businessUnitID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getHigh() {
		return high;
	}

	public void setHigh(Integer high) {
		this.high = high;
	}

	public Integer getInventoryBucketID() {
		return inventoryBucketID;
	}

	public void setInventoryBucketID(Integer inventoryBucketID) {
		this.inventoryBucketID = inventoryBucketID;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLights() {
		return lights;
	}

	public void setLights(Integer lights) {
		this.lights = lights;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public Integer getLow() {
		return low;
	}

	public void setLow(Integer low) {
		this.low = low;
	}

	public Integer getRangeID() {
		return rangeID;
	}

	public void setRangeID(Integer rangeID) {
		this.rangeID = rangeID;
	}

	public int getNumberOfDaysInBucket() {
		return numberOfDaysInBucket;
	}

	public void setNumberOfDaysInBucket(int numberOfDaysInBucket) {
		this.numberOfDaysInBucket = numberOfDaysInBucket;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getValue1() {
		return value1;
	}

	public void setValue1(Integer value1) {
		this.value1 = value1;
	}

	public Integer getValue2() {
		return value2;
	}

	public void setValue2(Integer value2) {
		this.value2 = value2;
	}

	public Integer getValue3() {
		return value3;
	}

	public void setValue3(Integer value3) {
		this.value3 = value3;
	}
}
