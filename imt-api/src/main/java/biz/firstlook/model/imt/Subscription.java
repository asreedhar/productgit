package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Subscriptions")
public class Subscription {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SubscriptionID")
	private Integer id;
	
	private Integer subscriberId;
	
	@ManyToOne
	@JoinColumn(name="SubscriberTypeID")
	private SubscriberType subscriberType;

	@ManyToOne
	@JoinColumn(name="SubscriptionTypeID")
	private SubscriptionType subscriptionType;

	@ManyToOne
	@JoinColumn(name="SubscriptionDeliveryTypeID")
	private SubscriptionDeliveryType subscriptionDeliveryType;
	
	private Integer businessUnitId;
	
	@ManyToOne
	@JoinColumn(name="SubscriptionFrequencyDetailID")
	private SubscriptionFrequencyDetail subscriptionFrequencyDetail;

	public Subscription() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(Integer subscriberId) {
		this.subscriberId = subscriberId;
	}

	public SubscriberType getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(SubscriberType subscriberType) {
		this.subscriberType = subscriberType;
	}

	public SubscriptionDeliveryType getSubscriptionDeliveryType() {
		return subscriptionDeliveryType;
	}

	public void setSubscriptionDeliveryType(
			SubscriptionDeliveryType subscriptionDeliveryType) {
		this.subscriptionDeliveryType = subscriptionDeliveryType;
	}

	public SubscriptionFrequencyDetail getSubscriptionFrequencyDetail() {
		return subscriptionFrequencyDetail;
	}

	public void setSubscriptionFrequencyDetail(
			SubscriptionFrequencyDetail subscriptionFrequencyDetail) {
		this.subscriptionFrequencyDetail = subscriptionFrequencyDetail;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	
	

	
	
}
