package biz.firstlook.model.imt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "InternetAdvertiserDealership")
@IdClass(InternetAdvertiserDealershipPK.class)
public class InternetAdvertiserDealership {

    @Id
    @Column(name = "InternetAdvertiserId")
    private Integer thirdPartyEntityId;
    @Id
    private Integer businessUnitId;
    @Column(name = "InternetAdvertisersDealershipCode")
    private String dealerCode;
    private String notes;
    private String contactFirstName;
    private String contactLastName;
    private String contactPhoneNumber;
    private String contactEmail;
    private Boolean active;
    @Column(name = "IsLive")
    private Boolean exporting;
    private Boolean status;
    @Temporal(TemporalType.TIMESTAMP)
    private Date verifiedDate;
    private String verifiedBy;
    private Boolean incrementalExport;
    private Integer exportClientId;
    private String exportGuid;
    
    public Integer getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public Integer getThirdPartyEntityId() {
        return thirdPartyEntityId;
    }

    public void setThirdPartyEntityId(Integer thirdPartyEntityId) {
        this.thirdPartyEntityId = thirdPartyEntityId;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getExporting() {
        return exporting;
    }

    public void setExporting(Boolean exporting) {
        this.exporting = exporting;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public Date getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(Date verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

	public Boolean getIncrementalExport() {
		return incrementalExport;
	}

	public void setIncrementalExport(Boolean incrementalExport) {
		this.incrementalExport = incrementalExport;
	}

	public Integer getExportClientId() {
		return exportClientId;
	}

	public void setExportClientId(Integer exportClientId) {
		this.exportClientId = exportClientId;
	}

	public String getExportGuid() {
		return exportGuid;
	}

	public void setExportGuid(String exportGuid) {
		this.exportGuid = exportGuid;
	}

}
