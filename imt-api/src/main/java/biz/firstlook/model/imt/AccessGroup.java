package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AccessGroup")
public class AccessGroup implements Serializable{
    
    private static final long serialVersionUID = -3576791058401536677L;
    
    @Id
    @Column(name="AccessGroupID")
    private Integer id;
    @Column(name="AccessGroupDescr")
    private String name;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String customerFacingDescription) {
        this.name = customerFacingDescription;
    }

}
