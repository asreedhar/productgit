package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class BusinessUnitCredentialPK implements Serializable {
	private static final long serialVersionUID = -6258740934444832576L;
	
	private Integer businessUnitId;
	private Integer credentialTypeId;
	
	
	public Integer getCredentialTypeId() {
		return credentialTypeId;
	}
	public void setCredentialTypeId(Integer credentialTypeID) {
		this.credentialTypeId = credentialTypeID;
	}
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitID) {
		this.businessUnitId = businessUnitID;
	}
	
	@Override
	public boolean equals(Object o) {
		if( o instanceof BusinessUnitCredentialPK ) {
			BusinessUnitCredentialPK pk = (BusinessUnitCredentialPK) o;
			if( pk.getBusinessUnitId().equals( this.getBusinessUnitId() ) &&
			    pk.getCredentialTypeId().equals( this.getCredentialTypeId() ) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = this.getBusinessUnitId().hashCode() << 4;
		hash += this.getCredentialTypeId().hashCode();
		return hash;
	}

	@Override
	public String toString() {
		return "DealerUpgradePK: " + getBusinessUnitId() + ", " + getCredentialTypeId();
	}		
}