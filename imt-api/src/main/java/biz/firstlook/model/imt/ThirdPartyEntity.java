package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ThirdPartyEntity")
public class ThirdPartyEntity implements Serializable {

    private static final long serialVersionUID = 7526975452341456715L;
    @Id
    private Integer thirdPartyEntityId;
    private Integer BusinessUnitId;
    @Column(name="ThirdPartyEntityTypeID")
    private Integer typeId;
    private String name;
    

    public Integer getBusinessUnitId() {
        return BusinessUnitId;
    }

    public void setBusinessUnitId(Integer businessUnitId) {
        BusinessUnitId = businessUnitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getThirdPartyEntityId() {
        return thirdPartyEntityId;
    }

    public void setThirdPartyEntityId(Integer thirdPartyEntityId) {
        this.thirdPartyEntityId = thirdPartyEntityId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

}
