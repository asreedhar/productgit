package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TargetType")
public class TargetType {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TargetTypeCD")
	private Integer code;
	
	@Column(name="TargetTypeName")
	private String name;
	
	public TargetType() {}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
