package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
@Entity
@Table(name = "PhotoProviderDealership")
@IdClass(PhotoProviderDealershipPK.class)
public class PhotoProviderDealership implements Serializable {

	private static final long serialVersionUID = -7571846288209667264L;

	@Id
	private Integer photoProviderId;
	
	@Id
	private Integer businessUnitId;
	private String photoProvidersDealershipCode;
	@Column(name = "PhotoStorageCode_Override")
	private Integer photoStorageCodeOverride;
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	
	public void setBusinessUnitId(Integer businessUnitID) {
		this.businessUnitId = businessUnitID;
	}
	
	public String getPhotoProvidersDealershipCode() {
		return photoProvidersDealershipCode;
	}
	
	public void setPhotoProvidersDealershipCode(String photoProviderDealershipCode) {
		this.photoProvidersDealershipCode = photoProviderDealershipCode;
	}
	
	public Integer getPhotoProviderId() {
		return photoProviderId;
	}
	
	public void setPhotoProviderId(Integer photoProviderID) {
		this.photoProviderId = photoProviderID;
	}
	
	public Integer getPhotoStorageCodeOverride() {
		return photoStorageCodeOverride;
	}
	
	public void setPhotoStorageCodeOverride(Integer photoStorageCode_Override) {
		this.photoStorageCodeOverride = photoStorageCode_Override;
	}
}
