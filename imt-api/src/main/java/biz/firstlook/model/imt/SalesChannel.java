package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="SalesChannel")
public class SalesChannel implements Serializable {
	
	private static final long serialVersionUID = 2476176643796892363L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SalesChannelID")
	private Integer id;
	
	@Column(name="Name", length=50)
	private String name;

	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
