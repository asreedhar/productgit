package biz.firstlook.model.imt.ref;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="dbo.Area_D")
public class NaaaRegion implements DatabaseEnum {
    @Id
    @Column(name="AreaID")
    private Integer id;
    
    @Column(name="AreaName")
    private String description;
    
    protected NaaaRegion(){
	super();
    }
    
    public Integer getId() {
        return id;
    }
    
    public String getDescription() {
        return description;
    }
}
