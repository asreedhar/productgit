package biz.firstlook.model.imt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="DealerTarget")
public class DealerTarget 	{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DealerTargetId")
	private Integer id;
	
	private Integer businessUnitId;
	
	@ManyToOne
	@JoinColumn(name="TargetCD")
	private Target target;
	
	@Temporal(TemporalType.DATE)
	@Column(name="StartDT")
	private Date start;
	
	@Column(name="TargetValue")
	private Integer value;
	private Boolean active;
	
	public DealerTarget() {}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}


	

}
