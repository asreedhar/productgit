package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="TransferPriceInventoryAgeRange")
public class TransferPriceInventoryAgeRange implements Serializable, Comparable<TransferPriceInventoryAgeRange> {

	static final long serialVersionUID = -7650052027864728924L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="TransferPriceInventoryAgeRangeID")
	private Integer id;
	
	private Integer low;
	private Integer high;
	
	@Column(insertable=false, updatable=false)
	private String description;
	
	/**
	 * Constructor for reflection purposes only.
	 */
	public TransferPriceInventoryAgeRange() {
		super();
	}
	
	/**
	 * Constructor for making a new range.
	 * @param low
	 * @param high
	 */
	public TransferPriceInventoryAgeRange(Integer low, Integer high) {
		setLow(low);
		setHigh(high);
	}

	public int getId() {
		return id;
	}

	public int getLow() {
		return low;
	}
	
	protected void setLow(Integer low) {
		if(low == null) {
			this.low = Integer.valueOf(0);
		} else {
			this.low = low;
		}
	}

	public int getHigh() {
		return high;
	}
	
	protected void setHigh(Integer high) {
		if(high == null) {
			this.high = Integer.MAX_VALUE;
		} else {
			this.high = high;
		}
	}
	
	public String getDescription() {
		return description;
	}
	
	public int compareTo(TransferPriceInventoryAgeRange o) {
		if(o == null) {
			return 1;
		} 
		
		if(this.low < o.low) {
			return -1; 
		} else if (this.low > o.low) {
			return 1;
		} else {
			if(this.high < o.high) {
				return -1;
			} else if (this.high > o.high) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + high;
		result = prime * result + low;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TransferPriceInventoryAgeRange other = (TransferPriceInventoryAgeRange) obj;
		if (high != other.high)
			return false;
		if (low != other.low)
			return false;
		return true;
	}
}
