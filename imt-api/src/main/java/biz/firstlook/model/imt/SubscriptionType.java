package biz.firstlook.model.imt;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="SubscriptionTypes")
public class SubscriptionType {
	
	@Id
	@Column(name="SubscriptionTypeID")
	private Integer id;

	private String description;
	private String notes;
	
	private Boolean userSelectable;
	
	// nk - 03-11-08
	// Both subscriptionDeliveryTypes and subscriptionFrequencyDetails contain all those
	// deliveryType and Frequency values that are possible for this subscription type.
	// A single instance of a subscription needs to have a single 'selected' deliveryType and Freq
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
	           name="SubscriptionTypeSubscriptionDeliveryType",
	           joinColumns=@JoinColumn(name="SubscriptionTypeID"),
	           inverseJoinColumns=@JoinColumn(name="SubscriptionDeliveryTypeID")
	)
	private List<SubscriptionDeliveryType> subscriptionDeliveryTypes;
	
	@ManyToMany
	@JoinTable(
	           name="SubscriptionTypeSubscriptionFrequencyDetail",
	           joinColumns=@JoinColumn(name="SubscriptionTypeID"),
	           inverseJoinColumns=@JoinColumn(name="SubscriptionFrequencyDetailID")
	)
	private List<SubscriptionFrequencyDetail> subscriptionFrequencyDetails;

	@ManyToMany
	@JoinTable(
	           name="SubscriptionTypeDealerUpgrade",
	           joinColumns=@JoinColumn(name="SubscriptionTypeID"),
	           inverseJoinColumns=@JoinColumn(name="DealerUpgradeCD")
	)
	private List<DealerUpgradeCode> dealerUpgradeCodes;
	
	public SubscriptionType() {}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<DealerUpgradeCode> getDealerUpgradeCodes() {
		return dealerUpgradeCodes;
	}

	public void setDealerUpgradeCodes(List<DealerUpgradeCode> dealerUpgradeCodes) {
		this.dealerUpgradeCodes = dealerUpgradeCodes;
	}

	public List<SubscriptionFrequencyDetail> getSubscriptionFrequencyDetails() {
		return subscriptionFrequencyDetails;
	}

	public void setSubscriptionFrequencyDetails(
			List<SubscriptionFrequencyDetail> subscriptionFrequencyDetails) {
		this.subscriptionFrequencyDetails = subscriptionFrequencyDetails;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getUserSelectable() {
		return userSelectable;
	}

	public void setUserSelectable(Boolean userSelectable) {
		this.userSelectable = userSelectable;
	}

	public List<SubscriptionDeliveryType> getSubscriptionDeliveryTypes() {
		return subscriptionDeliveryTypes;
	}

	public void setSubscriptionDeliveryTypes(
			List<SubscriptionDeliveryType> subscriptionDeliveryTypes) {
		this.subscriptionDeliveryTypes = subscriptionDeliveryTypes;
	}

	
	
}
