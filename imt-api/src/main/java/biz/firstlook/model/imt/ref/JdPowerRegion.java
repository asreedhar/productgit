package biz.firstlook.model.imt.ref;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="dbo.JDPower_PowerRegion")
public class JdPowerRegion implements DatabaseEnum, Serializable
{

private static final long serialVersionUID = 7392526929757614375L;

@Id
@Column(name="PowerRegionID")
private Integer powerRegionId;

@Column(name="Name")
private String name;

public String getDescription()
{
	return name;
}

public Integer getId()
{
	return powerRegionId;
}

public String getName()
{
	return name;
}

public void setName( String name )
{
	this.name = name;
}

public Integer getPowerRegionId() {
	return powerRegionId;
}

public void setPowerRegionId( Integer id )
{
	this.powerRegionId = id;
}

@Override
public int hashCode()
{
	final int prime = 31;
	int result = 1;
	result = prime * result + ( ( powerRegionId == null ) ? 0 : powerRegionId.hashCode() );
	result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final JdPowerRegion other = (JdPowerRegion)obj;
	if ( powerRegionId == null )
	{
		if ( other.powerRegionId != null )
			return false;
	}
	else if ( !powerRegionId.equals( other.powerRegionId ) )
		return false;
	if ( name == null )
	{
		if ( other.name != null )
			return false;
	}
	else if ( !name.equals( other.name ) )
		return false;
	return true;
}

}
