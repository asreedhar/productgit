package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DealerGridValues")
public class DealerGridValues {

	@Id
	@Column(name="DealerGridValuesId")
	private Integer id;
	
	@Column(name="BusinessUnitID")
	private Integer dealerId;

	private Integer lightValue;
	private Integer typeValue;
	private Integer indexKey;
	
	public DealerGridValues() {}

	public Integer getDealerId() {
		return dealerId;
	}

	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexKey() {
		return indexKey;
	}

	public void setIndexKey(Integer indexKey) {
		this.indexKey = indexKey;
	}

	public Integer getLightValue() {
		return lightValue;
	}

	public void setLightValue(Integer lightValue) {
		this.lightValue = lightValue;
	}

	public Integer getTypeValue() {
		return typeValue;
	}

	public void setTypeValue(Integer typeValue) {
		this.typeValue = typeValue;
	}

}
