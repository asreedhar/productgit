package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Target")
public class Target {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TargetCD")
	private Integer code;
	
	@Column(name="TargetName")
	private String name;
	
	@Column(name="TargetMin")
	private Integer min;
	
	@Column(name="TargetMax")
	private Integer max;
	
	@ManyToOne
	@JoinColumn(name="SectionCD")
	private TargetSection section;

	@ManyToOne
	@JoinColumn(name="TargetTypeCD")
	private TargetType type;
	
	public Target() {}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TargetSection getSection() {
		return section;
	}

	public void setSection(TargetSection section) {
		this.section = section;
	}

	public TargetType getType() {
		return type;
	}

	public void setType(TargetType type) {
		this.type = type;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	
	
	
}
