package biz.firstlook.model.imt.ref;


public enum ReportingMethod {
	Email,
	Fax;
}
