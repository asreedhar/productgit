package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="DMSExportBusinessUnitPreference")
public class DMSExportPreference {

	@Id
	@Column(name="DMSExportBusinessUnitPreferenceID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
    @OneToOne
    @JoinColumn(name = "BusinessUnitID")
    private Dealer dealer;
    
	private Integer dmsExportId;
	private boolean isActive = false;
	private String clientSystemId;
	@Column(name="DMSIPAddress")
	private String ipAddress;
	@Column(name="DMSDialUpPhone1")
	private String dialUpPhone1;
	@Column(name="DMSDialUpPhone2")
	private String dialUpPhone2;
	@Column(name="DMSLogin")
	private String login;
	@Column(name="DMSPassword")
	private String password;
	private String accountLogon;
	private String financeLogon;
	private Integer areaNumber;
	private Integer storeNumber;
	private Integer branchNumber;
	@Column(name="DMSExportFrequencyId")
	private Integer frequencyId;
	@Column(name="DMSExportDailyHourOfDay")
	private Integer dailyExportHourOfDay;
	private String dealerName;
	private String dealerPhoneOffice;
	private String dealerPhoneCell;
	private String dealerEmail;
	private String reynoldsDealerNo;
	
	@ManyToOne
	@JoinColumn(name="DMSExportInternetPriceMappingID")
	private DMSExportPriceMapping internetPricingMapping;

	@ManyToOne
	@JoinColumn(name="DMSExportLotPriceMappingID")
	private DMSExportPriceMapping lotPriceMapping;


	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getDmsExportId() {
		return dmsExportId;
	}

	public void setDmsExportId(Integer dmsExportID) {
		this.dmsExportId = dmsExportID;
	}

	public String getClientSystemId() {
		return clientSystemId;
	}

	public void setClientSystemId(String clientSystemId) {
		this.clientSystemId = clientSystemId;
	}


	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getDialUpPhone1() {
		return dialUpPhone1;
	}

	public void setDialUpPhone1(String dmsDisalUpPhone1) {
		this.dialUpPhone1 = dmsDisalUpPhone1;
	}

	public String getDialUpPhone2() {
		return dialUpPhone2;
	}

	public void setDialUpPhone2(String dmsDisalUpPhone2) {
		this.dialUpPhone2 = dmsDisalUpPhone2;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String dmsLogin) {
		this.login = dmsLogin;
	}

	public Integer getStoreNumber() {
		return storeNumber;
	}

	public void setStoreNumber(Integer storeNumber) {
		this.storeNumber = storeNumber;
	}

	public Integer getBranchNumber() {
		return branchNumber;
	}

	public void setBranchNumber(Integer branchNumber) {
		this.branchNumber = branchNumber;
	}

	public Integer getFrequencyId() {
		return frequencyId;
	}

	public void setFrequencyId(Integer dmsExportFrequencyId) {
		this.frequencyId = dmsExportFrequencyId;
	}

	public Integer getDailyExportHourOfDay() {
		return dailyExportHourOfDay;
	}

	public void setDailyExportHourOfDay(Integer dmsExportDailyHourOfDay) {
		this.dailyExportHourOfDay = dmsExportDailyHourOfDay;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerPhoneOffice() {
		return dealerPhoneOffice;
	}

	public void setDealerPhoneOffice(String dealerPhoneOffice) {
		this.dealerPhoneOffice = dealerPhoneOffice;
	}

	public String getDealerPhoneCell() {
		return dealerPhoneCell;
	}

	public void setDealerPhoneCell(String dealerPhoneCell) {
		this.dealerPhoneCell = dealerPhoneCell;
	}

	public String getDealerEmail() {
		return dealerEmail;
	}

	public void setDealerEmail(String dealerEmail) {
		this.dealerEmail = dealerEmail;
	}

	public DMSExportPriceMapping getInternetPricingMapping() {
		return internetPricingMapping;
	}

	public void setInternetPricingMapping(
			DMSExportPriceMapping internetPricingMapping) {
		this.internetPricingMapping = internetPricingMapping;
	}

	public DMSExportPriceMapping getLotPriceMapping() {
		return lotPriceMapping;
	}

	public void setLotPriceMapping(DMSExportPriceMapping lotPriceMapping) {
		this.lotPriceMapping = lotPriceMapping;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountLogon() {
		return accountLogon;
	}

	public void setAccountLogon(String accountLogon) {
		this.accountLogon = accountLogon;
	}

	public String getFinanceLogon() {
		return financeLogon;
	}

	public void setFinanceLogon(String finanaceLogon) {
		this.financeLogon = finanaceLogon;
	}

	public Integer getAreaNumber() {
		return areaNumber;
	}

	public void setAreaNumber(Integer areaNumber) {
		this.areaNumber = areaNumber;
	}

	public String getReynoldsDealerNo() {
		return reynoldsDealerNo;
	}

	public void setReynoldsDealerNo(String reynoldsDealerNo) {
		this.reynoldsDealerNo = reynoldsDealerNo;
	}
	
}
