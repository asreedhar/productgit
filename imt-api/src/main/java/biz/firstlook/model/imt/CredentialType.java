package biz.firstlook.model.imt;


public enum CredentialType {
	
	GMAC(3, "GMAC"),
	APPRAISAL_LOCK_OUT(4, "appraisalLockOut"),
	WAVIS(5, "WAVIS"),
	AUTOBASE(6, "autoBase"),
	DEALERSOCKET(7, "dealerSocket"),
	DEALERPEAK(8, "dealerPeak"),
	LOWBOOKSALES(9, "lowBookSales"),
	HIGHERGEAR(10, "higherGear"),
	REYNOLDS(11, "reynolds");
	
	private Integer id;
	private String name;
	
	private CredentialType(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public CredentialType[] getAllValues() {
		return CredentialType.values();
	}
	public static String getName(Integer id)
	{
		String toReturn = null;	
		CredentialType[] ct = values();
		for(CredentialType temp:ct){
			if(temp.id==id.intValue()){
				toReturn =temp.name;
				break;
			}	
		}
		return toReturn;
	}
}
