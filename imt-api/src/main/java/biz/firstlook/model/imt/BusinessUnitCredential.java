package biz.firstlook.model.imt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="BusinessUnitCredential")
@IdClass(BusinessUnitCredentialPK.class)
public class BusinessUnitCredential { 
	
	@Id
	private Integer businessUnitId;

	// this is because hibernate is incapable of handling enums properly at this time
	// needs to be handled explicitly in dao
	@Id
	private Integer credentialTypeId;
	
	@Column(nullable=true)
	private String userName;
	
	private String password;
	
	private String reynoldsDealerNumber;
	private Integer reynoldsStoreNumber;
	private Integer reynoldsAreaNumber;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updatedDate")
	private Date updatedDate;
	
	public BusinessUnitCredential() {
	}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getCredentialTypeId() {
		return credentialTypeId;
	}

	public void setCredentialTypeId(Integer credentialTypeId) {
		this.credentialTypeId = credentialTypeId;
	}

	public Integer getReynoldsAreaNumber() {
		return reynoldsAreaNumber;
	}

	public void setReynoldsAreaNumber(Integer reynoldsAreaNumber) {
		this.reynoldsAreaNumber = reynoldsAreaNumber;
	}

	public String getReynoldsDealerNumber() {
		return reynoldsDealerNumber;
	}

	public void setReynoldsDealerNumber(String reynoldsDealerNumber) {
		this.reynoldsDealerNumber = reynoldsDealerNumber;
	}

	public Integer getReynoldsStoreNumber() {
		return reynoldsStoreNumber;
	}

	public void setReynoldsStoreNumber(Integer reynoldsStoreNumber) {
		this.reynoldsStoreNumber = reynoldsStoreNumber;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
