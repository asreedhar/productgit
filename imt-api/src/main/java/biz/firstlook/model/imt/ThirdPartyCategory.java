package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ThirdPartyCategories")
public class ThirdPartyCategory {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ThirdPartyCategoryID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn( name = "ThirdPartyID" )
	private ThirdParty thirdParty;
	
	private String category;
	
	public ThirdPartyCategory() {}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ThirdParty getThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(ThirdParty thirdParty) {
		this.thirdParty = thirdParty;
	}
	
	public String getName() {
		return thirdParty.getName() + " - " + category;
	}

	
	
}
