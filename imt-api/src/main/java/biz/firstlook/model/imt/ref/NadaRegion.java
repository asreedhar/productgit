package biz.firstlook.model.imt.ref;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="dbo.NADARegions")
public class NadaRegion implements DatabaseEnum {

    @Id
    @Column(name="NADARegionCode")
    private Integer id;
    
    @Column(name="NADARegionName")
    private String description;
    
    protected NadaRegion(){
	super();
    }
	
    public Integer getId() {
        return id;
    }
    
    public String getDescription() {
        return description;
    }

}
