package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SubscriptionFrequency")
public class SubscriptionFrequency {

	@Id
	@Column(name="SubscriptionFrequencyID")
	private Integer id;
	private String description;
	
	public SubscriptionFrequency() {}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
