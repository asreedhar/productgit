package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lu_DealerUpgrade")
public class DealerUpgradeCode implements Serializable {

	private static final long serialVersionUID = -1360248776188669160L;
	
	public static final DealerUpgradeCode APPRAISL_LOCKOUT = new DealerUpgradeCode( 10, "Appraisal Lockout"); 
	public static final DealerUpgradeCode PURCHASING = new DealerUpgradeCode( 3, "Purchasing");
	public static final DealerUpgradeCode MAX = new DealerUpgradeCode(15, "MAX");
	public static final DealerUpgradeCode MAX_TEST = new DealerUpgradeCode(16, "MAX-Test");
	public static final DealerUpgradeCode JDPOWER_USED_MARKET_DATA = new DealerUpgradeCode( 18, "JDPower Used Car Market Data");
	public static final DealerUpgradeCode PING2 = new DealerUpgradeCode( 19, "PING II");
	public static final DealerUpgradeCode FIRSTLOOK_3_0 = new DealerUpgradeCode (27, "Firstlook 3.0");
	public static final DealerUpgradeCode NADA_VALUES= new DealerUpgradeCode(28,"NADA Values");
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "DealerUpgradeCD")
	private Integer id;

	@Column(name = "DealerUpgradeDESC")
	private String description;

	public DealerUpgradeCode() {
	}
	
	public DealerUpgradeCode( Integer id, String description) {
		this.id = id;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final DealerUpgradeCode other = (DealerUpgradeCode) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
