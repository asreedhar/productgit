package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DealerPreference_KBBConsumerTool")
public class DealerPreferenceKBBConsumerTool implements Serializable
{
	private static final long serialVersionUID = -1395879777104383355L;
	
	@Id
    @Column(name = "BusinessUnitID")
    private Integer businessUnitId;
	
	@Column(name="ShowTradeIn")
	private Boolean showTradeIn = true;

	@Column(name="ShowRetail")
	private Boolean showRetail = false;



	public Integer getBusinessUnitId()
	{
		return businessUnitId;
	}
	
	public void setBusinessUnitId(Integer businessUnitId)
	{
		this.businessUnitId = businessUnitId;
	}
	
	public Boolean getShowTradeIn()
	{
		return showTradeIn;
	}
	
	public void setShowTradein(Boolean showTradeIn)
	{
		if(showTradeIn == null)
		{
			this.showTradeIn = false;
		}
		else
		{
			this.showTradeIn = showTradeIn;
		}
	}
	
	public Boolean getShowRetail()
	{
		return showRetail;
	}
	
	public void setShowRetail(Boolean showRetail)
	{
		if(showRetail == null)
		{
			this.showRetail = false;
		}
		else
		{
			this.showRetail = showRetail;
		}
	}
}
