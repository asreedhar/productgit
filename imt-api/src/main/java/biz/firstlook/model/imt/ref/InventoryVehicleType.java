package biz.firstlook.model.imt.ref;

public enum InventoryVehicleType {
	NONE,
	NEW,
	USED;
}
