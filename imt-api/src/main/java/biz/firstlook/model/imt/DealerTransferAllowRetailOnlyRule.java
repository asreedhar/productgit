package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="DealerTransferAllowRetailOnlyRule")
public class DealerTransferAllowRetailOnlyRule implements Serializable {

	private static final long serialVersionUID = -7602363716216969406L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DealerTransferAllowRetailOnlyRuleID")
	private Integer id;
	
	@Column(name="BusinessUnitID")
	private Integer businessUnitId;
	
	@ManyToOne
	@JoinColumn(name="TransferPriceInventoryAgeRangeID")
	private TransferPriceInventoryAgeRange transferPriceInventoryAgeRange;
	
	/**
	 * For reflection purposes only.
	 */
	public DealerTransferAllowRetailOnlyRule() {
		super();
	}

	public DealerTransferAllowRetailOnlyRule(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	
	public Integer getId() {
		return id;
	}
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public TransferPriceInventoryAgeRange getTransferPriceInventoryAgeRange() {
		return transferPriceInventoryAgeRange;
	}

	public void setTransferPriceInventoryAgeRange(
			TransferPriceInventoryAgeRange transferPriceInventoryAgeRange) {
		this.transferPriceInventoryAgeRange = transferPriceInventoryAgeRange;
	}


}
