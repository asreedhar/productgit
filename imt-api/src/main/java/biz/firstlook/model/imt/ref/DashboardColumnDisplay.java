package biz.firstlook.model.imt.ref;

public enum DashboardColumnDisplay {
	NONE,
	AVERAGE_MILEAGE,
	UNITS_IN_STOCK;
}
