package biz.firstlook.model.imt;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionOfElements;

@Entity
@Table(name="FirstlookRegion")
public class FirstLookRegion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="FirstLookRegionID")
	private Integer id;

	@Column(name="FirstLookRegionName")
	private String name;
	
	@CollectionOfElements
    @JoinTable(
            name="FirstLookRegionToZip",
            joinColumns = @JoinColumn(name="FirstLookRegionID")
    )
    @Column(name="ZipCode", nullable=false)
	private Set<String> zipCodes;
	
	public FirstLookRegion() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getZipCodes() {
		return zipCodes;
	}

	public void setZipCodes(Set<String> zipCodes) {
		this.zipCodes = zipCodes;
	}
	
	
	
}
