package biz.firstlook.model.imt;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DealerRisk")
public class DealerRisk {

	@Id
	private Integer businessUnitId;
	private Integer riskLevelNumberOfWeeks;
	private Integer riskLevelDealsThreshold;
	private Integer riskLevelNumberOfContributors;
	private Integer redLightNoSaleThreshold;
	private Integer redLightGrossProfitThreshold;
	private Integer greenLightNoSaleThreshold;
	private Integer greenLightGrossProfitThreshold;
	private Integer greenLightMarginThreshold;
	private Integer greenLightDaysPercentage;
	private Integer highMileagePerYearThreshold;
	private Integer highMileageThreshold;
	private Integer riskLevelYearInitialTimePeriod;
	private Integer riskLevelYearSecondaryTimePeriod;
	private Integer riskLevelYearRollOverMonth;
	private Integer redLightTarget;
	private Integer yellowLightTarget;
	private Integer greenLightTarget;
	private Integer excessiveMileageThreshold;
	//	 new columns
	private int highAgeThreshold;
	private int excessiveAgeThreshold;
	private int excessiveMileagePerYearThreshold;

	
	public DealerRisk() {}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getGreenLightDaysPercentage() {
		return greenLightDaysPercentage;
	}

	public void setGreenLightDaysPercentage(Integer greenLightDaysPercentage) {
		this.greenLightDaysPercentage = greenLightDaysPercentage;
	}

	public Integer getGreenLightGrossProfitThreshold() {
		return greenLightGrossProfitThreshold;
	}

	public void setGreenLightGrossProfitThreshold(
			Integer greenLightGrossProfitThreshold) {
		this.greenLightGrossProfitThreshold = greenLightGrossProfitThreshold;
	}

	public Integer getGreenLightMarginThreshold() {
		return greenLightMarginThreshold;
	}

	public void setGreenLightMarginThreshold(Integer greenLightMarginThreshold) {
		this.greenLightMarginThreshold = greenLightMarginThreshold;
	}

	public Integer getGreenLightNoSaleThreshold() {
		return greenLightNoSaleThreshold;
	}

	public void setGreenLightNoSaleThreshold(Integer greenLightNoSaleThreshold) {
		this.greenLightNoSaleThreshold = greenLightNoSaleThreshold;
	}

	public Integer getGreenLightTarget() {
		return greenLightTarget;
	}

	public void setGreenLightTarget(Integer greenLightTarget) {
		this.greenLightTarget = greenLightTarget;
	}

	public Integer getHighMileageThreshold() {
		return highMileageThreshold;
	}

	public void setHighMileageThreshold(Integer highMileageThreshold) {
		this.highMileageThreshold = highMileageThreshold;
	}

	public Integer getExcessiveMileageThreshold() {
		return excessiveMileageThreshold;
	}

	public void setExcessiveMileageThreshold(Integer excessiveMileageThreshold) {
		this.excessiveMileageThreshold = excessiveMileageThreshold;
	}

	public Integer getHighMileagePerYearThreshold() {
		return highMileagePerYearThreshold;
	}

	public void setHighMileagePerYearThreshold(Integer highMileagePerYearThreshold) {
		this.highMileagePerYearThreshold = highMileagePerYearThreshold;
	}

	public Integer getRedLightGrossProfitThreshold() {
		return redLightGrossProfitThreshold;
	}

	public void setRedLightGrossProfitThreshold(Integer redLightGrossProfitThreshold) {
		this.redLightGrossProfitThreshold = redLightGrossProfitThreshold;
	}

	public Integer getRedLightNoSaleThreshold() {
		return redLightNoSaleThreshold;
	}

	public void setRedLightNoSaleThreshold(Integer redLightNoSaleThreshold) {
		this.redLightNoSaleThreshold = redLightNoSaleThreshold;
	}

	public Integer getRedLightTarget() {
		return redLightTarget;
	}

	public void setRedLightTarget(Integer redLightTarget) {
		this.redLightTarget = redLightTarget;
	}

	public Integer getRiskLevelDealsThreshold() {
		return riskLevelDealsThreshold;
	}

	public void setRiskLevelDealsThreshold(Integer riskLevelDealsThreshold) {
		this.riskLevelDealsThreshold = riskLevelDealsThreshold;
	}

	public Integer getRiskLevelNumberOfContributors() {
		return riskLevelNumberOfContributors;
	}

	public void setRiskLevelNumberOfContributors(
			Integer riskLevelNumberOfContributors) {
		this.riskLevelNumberOfContributors = riskLevelNumberOfContributors;
	}

	public Integer getRiskLevelNumberOfWeeks() {
		return riskLevelNumberOfWeeks;
	}

	public void setRiskLevelNumberOfWeeks(Integer riskLevelNumberOfWeeks) {
		this.riskLevelNumberOfWeeks = riskLevelNumberOfWeeks;
	}

	public Integer getRiskLevelYearInitialTimePeriod() {
		return riskLevelYearInitialTimePeriod;
	}

	public void setRiskLevelYearInitialTimePeriod(
			Integer riskLevelYearInitialTimePeriod) {
		this.riskLevelYearInitialTimePeriod = riskLevelYearInitialTimePeriod;
	}

	public Integer getRiskLevelYearSecondaryTimePeriod() {
		return riskLevelYearSecondaryTimePeriod;
	}

	public void setRiskLevelYearSecondaryTimePeriod(
			Integer riskLevelYearSecondaryTimePeriod) {
		this.riskLevelYearSecondaryTimePeriod = riskLevelYearSecondaryTimePeriod;
	}

	public Integer getRiskLevelYearRollOverMonth() {
		return riskLevelYearRollOverMonth;
	}

	public void setRiskLevelYearRollOverMonth(Integer riskLevelYearRollOverMonth) {
		this.riskLevelYearRollOverMonth = riskLevelYearRollOverMonth;
	}

	public Integer getYellowLightTarget() {
		return yellowLightTarget;
	}

	public void setYellowLightTarget(Integer yellowLightTarget) {
		this.yellowLightTarget = yellowLightTarget;
	}

	public int getExcessiveAgeThreshold() {
		return excessiveAgeThreshold;
	}

	public void setExcessiveAgeThreshold(int excessiveAgeThreshold) {
		this.excessiveAgeThreshold = excessiveAgeThreshold;
	}

	public int getExcessiveMileagePerYearThreshold() {
		return excessiveMileagePerYearThreshold;
	}

	public void setExcessiveMileagePerYearThreshold(
			int excessiveMileagePerYearThreshold) {
		this.excessiveMileagePerYearThreshold = excessiveMileagePerYearThreshold;
	}

	public int getHighAgeThreshold() {
		return highAgeThreshold;
	}

	public void setHighAgeThreshold(int highAgeThreshold) {
		this.highAgeThreshold = highAgeThreshold;
	}
	
	
}
