package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InternetAdvertiserDealershipPK implements Serializable{

    private static final long serialVersionUID = -3495891254926529025L;
    @Column(name = "InternetAdvertiserId")
    private Integer thirdPartyEntityId;
    private Integer businessUnitId;
    
    public InternetAdvertiserDealershipPK(){}
    
    public Integer getBusinessUnitId() {
        return businessUnitId;
    }
    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }
    public Integer getThirdPartyEntityId() {
        return thirdPartyEntityId;
    }
    public void setThirdPartyEntityId(Integer thirdPartyEntityId) {
        this.thirdPartyEntityId = thirdPartyEntityId;
    }

    @Override
    public boolean equals(Object o) {
        if( o instanceof InternetAdvertiserDealershipPK ) {
            InternetAdvertiserDealershipPK pk = (InternetAdvertiserDealershipPK) o;
            if( pk.getBusinessUnitId().equals( this.getBusinessUnitId() ) &&
                pk.getThirdPartyEntityId().equals( this.getThirdPartyEntityId() ) ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = this.getBusinessUnitId().hashCode() << 4;
        hash += this.getThirdPartyEntityId().hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return "InternetAdvertiserDealershipPK: " + getBusinessUnitId() + ", " + getThirdPartyEntityId();
    }
}
