package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="TransferPriceUnitCostValue")
public class TransferPriceUnitCostValue implements Comparable<TransferPriceUnitCostValue>, Serializable {
	
	static final long serialVersionUID = -6579360996421584524L;
	
	@Id
	@Column(name="TransferPriceUnitCostValueID")
	private Integer id;

	@Column(name="Description")
	private String description;

	public Integer getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public int compareTo(TransferPriceUnitCostValue o) {
		if (id == null) {
			if (o.id == null) {
				return 0;
			} else {
				return -1;
			}
		} else {
			return id.compareTo(o.id);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TransferPriceUnitCostValue other = (TransferPriceUnitCostValue) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
}
