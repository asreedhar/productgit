package biz.firstlook.model.imt;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="DealerUpgrade")
@IdClass(DealerUpgradePK.class)
public class DealerUpgrade {
	
	@Id
	private Integer businessUnitId;
	
	@Id
	private Integer dealerUpgradeCD;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	private Boolean active;
	
	public DealerUpgrade() {}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getDealerUpgradeCD() {
		return dealerUpgradeCD;
	}

	public void setDealerUpgradeCD(Integer dealerUpgradeCD) {
		this.dealerUpgradeCD = dealerUpgradeCD;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	
}

