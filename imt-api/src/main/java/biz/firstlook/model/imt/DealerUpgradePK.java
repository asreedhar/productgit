package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class DealerUpgradePK implements Serializable {

	private static final long serialVersionUID = -262669639469021169L;
	private Integer businessUnitId;
	private Integer dealerUpgradeCD;
	
	public DealerUpgradePK() {}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getDealerUpgradeCD() {
		return dealerUpgradeCD;
	}

	public void setDealerUpgradeCD(Integer dealerUpgradeCD) {
		this.dealerUpgradeCD = dealerUpgradeCD;
	}

	@Override
	public boolean equals(Object o) {
		if( o instanceof DealerUpgradePK ) {
			DealerUpgradePK pk = (DealerUpgradePK) o;
			if( pk.getBusinessUnitId().equals( this.getBusinessUnitId() ) &&
			    pk.getDealerUpgradeCD().equals( this.getDealerUpgradeCD() ) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = this.getBusinessUnitId().hashCode() << 4;
		hash += this.getDealerUpgradeCD().hashCode();
		return hash;
	}

	@Override
	public String toString() {
		return "DealerUpgradePK: " + getBusinessUnitId() + ", " + getDealerUpgradeCD();
	}
	
	
}
