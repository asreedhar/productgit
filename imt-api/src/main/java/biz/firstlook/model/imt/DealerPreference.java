package biz.firstlook.model.imt;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;

import biz.firstlook.model.imt.ref.DashboardColumnDisplay;
import biz.firstlook.model.imt.ref.Day;
import biz.firstlook.model.imt.ref.ProgramType;
import biz.firstlook.model.imt.ref.TrendingView;

@Entity
@org.hibernate.annotations.Entity(
		dynamicUpdate = true )
@Table(name = "DealerPreference")
public class DealerPreference implements Serializable {

	private static final long serialVersionUID = 3146296003370953229L;
	
	private static final String APPRAISAL_FORM_DISCLAIMER = "The owner of this vehicle herby affirms that it has not been damaged by flood or had frame damage.";
    private static final String APPRAISAL_FORM_MEMO = "Trade in value for purchase of a vehicle.";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DealerPreferenceID")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "BusinessUnitID")
    private Dealer dealer;

    private String customHomePageMessage;

    private DashboardColumnDisplay dashboardColumnDisplayPreference = DashboardColumnDisplay.UNITS_IN_STOCK;

    private Integer checkAppraisalHistoryForIMPPlanning = Integer.valueOf(1);

    private Integer unitsSoldThreshold4Wks = Integer.valueOf(0);
    private Integer unitsSoldThreshold8Wks = Integer.valueOf(1);
    private Integer unitsSoldThreshold12Wks = Integer.valueOf(1);
    private Integer unitsSoldThreshold13Wks = Integer.valueOf(1);
    private Integer unitsSoldThreshold26Wks = Integer.valueOf(2);
    private Integer unitsSoldThreshold52Wks = Integer.valueOf(4);

    private TrendingView defaultTrendingView = TrendingView.TOP_SELLER;
    private Integer defaultTrendingWeeks = Integer.valueOf(13);
    private Integer defaultForecastingWeeks = Integer.valueOf(13);

    private Integer nadaRegionCode;

    @Temporal(TemporalType.DATE)
    private Date inceptionDate;

    private Integer agingInventoryTrackingDisplayPref = Integer.valueOf(1);
    private Float packAmount = Float.valueOf(0.0f);
    private Integer stockOrVinPreference = Integer.valueOf(1);
    private Date maxSalesHistoryDate;
    private Integer unitCostOrListPricePreference;
    private Boolean listPricePreference = Boolean.TRUE;

    private Integer ageBandTarget1 = Integer.valueOf(0);
    private Integer ageBandTarget2 = Integer.valueOf(5);
    private Integer ageBandTarget3 = Integer.valueOf(15);
    private Integer ageBandTarget4 = Integer.valueOf(20);
    private Integer ageBandTarget5 = Integer.valueOf(60);
    private Integer ageBandTarget6 = Integer.valueOf(0);

    private Integer daysSupply12WeekWeight = Integer.valueOf(100);
    private Integer daysSupply26WeekWeight = Integer.valueOf(0);

    private Boolean bookOut = Boolean.TRUE;

    // This is the craziest model - First Guide Book Preferences
    private Integer guideBookId = Integer.valueOf(2); // NADA by default
    private Integer bookOutPreferenceId = Integer.valueOf(1);
    private Integer bookOutPreferenceSecondId;

    // This is the craziest model - Second Guide Book Preferences
    private Integer guideBook2Id;
    private Integer guideBook2BookOutPreferenceId;
    private Integer guideBook2SecondBookOutPreferenceId;

    // I'm ignoring this one
    private Integer bookOutSecondPreferenceId;

    private Float unitCostThreshold = Float.valueOf(0f);
    private Float unitCostThresholdLower = Float.valueOf(4000.0f);
    private Float unitCostThresholdUpper = Float.valueOf(1000000.0f);

    private Boolean unitCostUpdateOnSale = Boolean.TRUE;
    private Integer unwindDaysThreshold = Integer.valueOf(45);
    private Integer auctionAreaId = Integer.valueOf(1); // defaults to national
    private Integer auctionTimePeriodId = Integer.valueOf(11); //2weeks - need properties file
    private Boolean showLotLocationStatus = Boolean.FALSE;
    private Boolean showInactiveAppraisals = Boolean.FALSE;
    private String populateClassName; // What is this??
    @Column(name = "RunDayOfWeek")
    private Day runDay = Day.SUNDAY;
    private Integer feGrossProfitThreshold = Integer.valueOf(-1000000);
    private Integer marginPercentile = Integer.valueOf(25);
    private Integer daysToSalePercentile = Integer.valueOf(90);
    @Column(name = "ProgramType_CD")
    private ProgramType programType = ProgramType.INSIGHT;
    private Integer sellThroughRate = Integer.valueOf(90);
    private Boolean includeBackEndInValuation = Boolean.TRUE;
    private Integer unitsSoldThresholdInvOverview = Integer.valueOf(6);
    private Integer ciaTargetDaysSupply;
    private Integer ciaMarketPerformerInStockThreshold;
    @Temporal(TemporalType.DATE)
    private Date goLiveDate;
    private Float vehicleSaleThresholdForCoreMarketPenetration;
    
    private Boolean applyPriorAgingNotes = Boolean.TRUE;
    private Integer averageInventoryAgeRedThreshold = Integer.valueOf(30);
    private Integer averageDaysSupplyRedThreshold = Integer.valueOf(50);
    private Integer purchasingDistanceFromDealer = Integer.valueOf(1000);
    private Integer liveAuctionDistanceFromDealer = Integer.valueOf(1000);
    private Boolean displayUnitCostToDealerGroup = Boolean.TRUE;
    private Integer searchInactiveInventoryDaysBackThreshold = Integer
	    .valueOf(90);
    private Boolean calculateAverageBookValue = Boolean.TRUE;
    private Day aipRunDayOfWeek = Day.SUNDAY;
    private Integer showroomDaysFilter = Integer.valueOf(3);
    
    private Integer tradeManagerDaysFilter = Integer.valueOf(10);
    private Boolean bucketJumperAIPApproach = Boolean.TRUE;
    private Integer appraisalRequirementLevel = Integer.valueOf(0);
    private Boolean requireNameOnAppraisals = false;
    private Boolean requireEstReconCostOnAppraisals = false;
    private Boolean requireReconNotesOnAppraisals = false;
    private Integer flashLocatorHideUnitCostDays = Integer.valueOf(0);
    
    private Boolean atcEnabled = Boolean.TRUE;
    private Boolean tfsEnabled = Boolean.FALSE;
    private Boolean gmacEnabled = Boolean.FALSE;
    private Boolean oveEnabled = Boolean.FALSE;
        
    private Integer applyDefaultPlanToGreenLightsInIMP = Integer.valueOf(0);
    private Integer repricePercentChangeThreshold = Integer.valueOf(10);
    private Boolean repriceConfirmation = Boolean.TRUE;
    @Column(name = "InternetAdvertiser_SendZeroesAsNull")
    private Boolean sendZeroesAsNull = Boolean.FALSE;
    
    private Boolean advertisingStatus = Boolean.FALSE;
    
    public Boolean getAdvertisingStatus() {
		return advertisingStatus;
	}

	public void setAdvertisingStatus(Boolean advertisingStatus) {
		this.advertisingStatus = advertisingStatus;
	}

	//BlackBookMobileOptInFlag
    @Column(name="BlackBookMobileOptInFlag" ,nullable=true)
    private Boolean blackBookMobileOptInFlag=null;

    @Column(name="BlackBookOptInDate" ,nullable=true)
    private Date blackBookOptInDate=null;
    
    @Column(name="BlackBookOptOutDate" ,nullable=true)
    private Date blackBookOptOutDate=null;
    
    public Date getBlackBookOptInDate() {
		return blackBookOptInDate;
	}

	public void setBlackBookOptInDate(Date blackBookOptInDate) {
		this.blackBookOptInDate = blackBookOptInDate;
	}

	public Date getBlackBookOptOutDate() {
		return blackBookOptOutDate;
	}

	public void setBlackBookOptOutDate(Date blackBookOptOutDate) {
		this.blackBookOptOutDate = blackBookOptOutDate;
	}
	
	//------30154----//
    @Column(name="GalvesMobileOptInFlag" ,nullable=true)
    private Boolean galvesMobileOptInFlag=null;

    @Column(name="GalvesOptInDate" ,nullable=true)
    private Date galvesOptInDate=null;
    
    public Boolean getGalvesMobileOptInFlag() {
		return galvesMobileOptInFlag;
	}

	public void setGalvesMobileOptInFlag(Boolean galvesMobileOptInFlag) {
		Boolean temp = galvesMobileOptInFlag;
		if((this.galvesMobileOptInFlag!=galvesMobileOptInFlag)&&(galvesMobileOptInFlag))
			galvesOptInDate= new Date();
		if((this.galvesMobileOptInFlag!=null)&&(this.galvesMobileOptInFlag!=galvesMobileOptInFlag)&&(!galvesMobileOptInFlag))
			galvesOptOutDate=new Date();
		if(this.galvesMobileOptInFlag==null&&(!temp))
			temp=null;
		
		this.galvesMobileOptInFlag = temp;
		
		temp=null;
	}

	public Date getGalvesOptInDate() {
		return galvesOptInDate;
	}

	public void setGalvesOptInDate(Date galvesOptInDate) {
		this.galvesOptInDate = galvesOptInDate;
	}

	public Date getGalvesOptOutDate() {
		return galvesOptOutDate;
	}

	public void setGalvesOptOutDate(Date galvesOptOutDate) {
		this.galvesOptOutDate = galvesOptOutDate;
	}

	@Column(name="GalvesOptOutDate" ,nullable=true)
    private Date galvesOptOutDate=null;
	//----30154-----//
	
	

	private Integer kbbAppraisalBookoutDatasetPreference = Integer.valueOf(0); // newest
    // book
    private Integer kbbInventoryBookoutDatasetPreference = Integer.valueOf(1); // current
    // book
    private Integer kbbInventoryDefaultCondition = Integer.valueOf(1); // EXCELLENT

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "BusinessUnitID", referencedColumnName = "BusinessUnitID", insertable = false, updatable = false)
    @Where(clause = "startDate < getDate() AND endDate > getDate()")
    private List<KBBArchiveDatasetOverride> kbbArchiveDataSetOverride;

    // Appraisal Form preferences.
    private Integer appraisalFormValidDate = Integer.valueOf(14);
    private Integer appraisalFormValidMileage = Integer.valueOf(150);
    private String appraisalFormDisclaimer = APPRAISAL_FORM_DISCLAIMER;
    private String appraisalFormMemo = APPRAISAL_FORM_MEMO;
    @Transient
    private Integer appraisalFormShowPhotos = Integer.valueOf(1);
    private Integer appraisalFormShowOptions = Integer.valueOf(1);
    @Transient
    private Integer showAppraisalForm = Integer.valueOf(1);
    private Integer showCheckOnAppraisalForm = Integer.valueOf(1);

    private Integer groupAppraisalSearchWeeks = Integer.valueOf(4);
    private Boolean showAppraisalValueGroup = Boolean.FALSE;
    private Boolean showAppraisalFormOfferGroup = Boolean.FALSE;

    private Integer redistributionNumTopDealers = Integer.valueOf(10);
    private Integer redistributionDealerDistance = Integer.valueOf(500);
    private Integer redistributionRoi = Integer.valueOf(0);
    private Integer redistributionUnderstock = Integer.valueOf(1);

    private Boolean useLotPrice = Boolean.FALSE;

    @Transient
    private Boolean twixEnabled = Boolean.FALSE;
    private String twixURL = null;

	private Integer defaultLithiaCarCenterMemberId = null;
	private Integer searchAppraisalDaysBackThreshold = Integer.valueOf(90);
	private Integer appraisalLookBackPeriod = Integer.valueOf(45);
	private Integer appraisalLookForwardPeriod = Integer.valueOf(3);
	
    private Boolean excludeWholesaleFromDaysSupply = Boolean.FALSE;
    
	@Transient
	private transient boolean primaryBookChanged = false;
	
	private Boolean showInTransitInventoryForm = Boolean.FALSE;
 
    /**
     * Default constructor needed for fl-admin reflection magic?
     */
    public DealerPreference() {
    }

    /**
     * Constructs a new DealerPreference entity for the given Dealer. Defaults
     * settings are populated here.
     * 
     * @param dealer
     */
    public DealerPreference(Dealer dealer) {
	this.dealer = dealer;
    }

    public Integer getAgeBandTarget1() {
	return ageBandTarget1;
    }

    public void setAgeBandTarget1(Integer ageBandTarget1) {
	this.ageBandTarget1 = ageBandTarget1;
    }

    public Integer getAgeBandTarget2() {
	return ageBandTarget2;
    }

    public void setAgeBandTarget2(Integer ageBandTarget2) {
	this.ageBandTarget2 = ageBandTarget2;
    }

    public Integer getAgeBandTarget3() {
	return ageBandTarget3;
    }

    public void setAgeBandTarget3(Integer ageBandTarget3) {
	this.ageBandTarget3 = ageBandTarget3;
    }

    public Integer getAgeBandTarget4() {
	return ageBandTarget4;
    }

    public void setAgeBandTarget4(Integer ageBandTarget4) {
	this.ageBandTarget4 = ageBandTarget4;
    }

    public Integer getAgeBandTarget5() {
	return ageBandTarget5;
    }

    public void setAgeBandTarget5(Integer ageBandTarget5) {
	this.ageBandTarget5 = ageBandTarget5;
    }

    public Integer getAgeBandTarget6() {
	return ageBandTarget6;
    }

    public void setAgeBandTarget6(Integer ageBandTarget6) {
	this.ageBandTarget6 = ageBandTarget6;
    }

    public Integer getAgingInventoryTrackingDisplayPref() {
	return agingInventoryTrackingDisplayPref;
    }

    public void setAgingInventoryTrackingDisplayPref(
	    Integer agingInventoryTrackingDisplayPref) {
	this.agingInventoryTrackingDisplayPref = agingInventoryTrackingDisplayPref;
    }

    public Boolean getApplyPriorAgingNotes() {
	return applyPriorAgingNotes;
    }

    public void setApplyPriorAgingNotes(Boolean applyPriorAgingNotes) {
	this.applyPriorAgingNotes = applyPriorAgingNotes;
    }

    public Boolean getAtcEnabled() {
	return atcEnabled;
    }

    public void setAtcEnabled(Boolean atcEnabled) {
	this.atcEnabled = atcEnabled;
    }

    public Integer getAuctionAreaId() {
	return auctionAreaId;
    }

    public void setAuctionAreaId(Integer auctionAreaId) {
	this.auctionAreaId = auctionAreaId;
    }

    public Integer getAuctionTimePeriodId() {
        return auctionTimePeriodId;
    }

    public void setAuctionTimePeriodId(Integer auctionTimePeriodId) {
        this.auctionTimePeriodId = auctionTimePeriodId;
    }

    public Integer getAverageDaysSupplyRedThreshold() {
	return averageDaysSupplyRedThreshold;
    }

    public void setAverageDaysSupplyRedThreshold(
	    Integer averageDaysSupplyRedThreshold) {
	this.averageDaysSupplyRedThreshold = averageDaysSupplyRedThreshold;
    }

    public Integer getAverageInventoryAgeRedThreshold() {
	return averageInventoryAgeRedThreshold;
    }

    public void setAverageInventoryAgeRedThreshold(
	    Integer averageInventoryAgeRedThreshold) {
	this.averageInventoryAgeRedThreshold = averageInventoryAgeRedThreshold;
    }

    public Boolean getBookOut() {
	return bookOut;
    }

    public void setBookOut(Boolean bookOut) {
	this.bookOut = bookOut;
    }

    public Integer getBookOutSecondPreferenceId() {
	return bookOutSecondPreferenceId;
    }

    public void setBookOutSecondPreferenceId(Integer bookOutSecondPreferenceId) {
	this.bookOutSecondPreferenceId = bookOutSecondPreferenceId;
    }

    public Boolean getCalculateAverageBookValue() {
	return calculateAverageBookValue;
    }

    public void setCalculateAverageBookValue(Boolean calculateAverageBookValue) {
	this.calculateAverageBookValue = calculateAverageBookValue;
    }

    public Integer getCiaMarketPerformerInStockThreshold() {
	return ciaMarketPerformerInStockThreshold;
    }

    public void setCiaMarketPerformerInStockThreshold(
	    Integer ciaMarketPerformerInStockThreshold) {
	this.ciaMarketPerformerInStockThreshold = ciaMarketPerformerInStockThreshold;
    }

    public Integer getCiaTargetDaysSupply() {
	return ciaTargetDaysSupply;
    }

    public void setCiaTargetDaysSupply(Integer ciaTargetDaysSupply) {
	this.ciaTargetDaysSupply = ciaTargetDaysSupply;
    }

    public String getCustomHomePageMessage() {
	return customHomePageMessage;
    }

    public void setCustomHomePageMessage(String customHomePageMessage) {
	this.customHomePageMessage = customHomePageMessage;
    }

    public DashboardColumnDisplay getDashboardColumnDisplayPreference() {
	return dashboardColumnDisplayPreference;
    }

    public void setDashboardColumnDisplayPreference(
	    DashboardColumnDisplay dashboardColumnDisplayPreference) {
	this.dashboardColumnDisplayPreference = dashboardColumnDisplayPreference;
    }

    public Integer getDaysSupply12WeekWeight() {
	return daysSupply12WeekWeight;
    }

    public void setDaysSupply12WeekWeight(Integer daysSupply12WeekWeight) {
	this.daysSupply12WeekWeight = daysSupply12WeekWeight;
    }

    public Integer getDaysSupply26WeekWeight() {
	return daysSupply26WeekWeight;
    }

    public void setDaysSupply26WeekWeight(Integer daysSupply26WeekWeight) {
	this.daysSupply26WeekWeight = daysSupply26WeekWeight;
    }

    public Integer getDaysToSalePercentile() {
	return daysToSalePercentile;
    }

    public void setDaysToSalePercentile(Integer daysToSalePercentile) {
	this.daysToSalePercentile = daysToSalePercentile;
    }

    public Integer getDefaultForecastingWeeks() {
	return defaultForecastingWeeks;
    }

    public void setDefaultForecastingWeeks(Integer defaultForecastingWeeks) {
	this.defaultForecastingWeeks = defaultForecastingWeeks;
    }

    public TrendingView getDefaultTrendingView() {
	return defaultTrendingView;
    }

    public void setDefaultTrendingView(TrendingView defaultTrendingView) {
	this.defaultTrendingView = defaultTrendingView;
    }

    public Integer getDefaultTrendingWeeks() {
	return defaultTrendingWeeks;
    }

    public void setDefaultTrendingWeeks(Integer defaultTrendingWeeks) {
	this.defaultTrendingWeeks = defaultTrendingWeeks;
    }

    public Boolean getDisplayUnitCostToDealerGroup() {
	return displayUnitCostToDealerGroup;
    }

    public void setDisplayUnitCostToDealerGroup(
	    Boolean displayUnitCostToDealerGroup) {
	this.displayUnitCostToDealerGroup = displayUnitCostToDealerGroup;
    }

    public Integer getFeGrossProfitThreshold() {
	return feGrossProfitThreshold;
    }

    public void setFeGrossProfitThreshold(Integer feGrossProfitThreshold) {
	this.feGrossProfitThreshold = feGrossProfitThreshold;
    }

    public Date getGoLiveDate() {
	return goLiveDate;
    }

    public void setGoLiveDate(Date goLiveDate) {
	this.goLiveDate = goLiveDate;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getInceptionDate() {
	return inceptionDate;
    }

    public void setInceptionDate(Date inceptionDate) {
	this.inceptionDate = inceptionDate;
    }

    public Boolean getIncludeBackEndInValuation() {
	return includeBackEndInValuation;
    }

    public void setIncludeBackEndInValuation(Boolean includeBackEndInValuation) {
	this.includeBackEndInValuation = includeBackEndInValuation;
    }

    public Boolean getListPricePreference() {
	return listPricePreference;
    }

    public void setListPricePreference(Boolean listPricePreference) {
	this.listPricePreference = listPricePreference;
    }

    public Integer getMarginPercentile() {
	return marginPercentile;
    }

    public void setMarginPercentile(Integer marginPercentile) {
	this.marginPercentile = marginPercentile;
    }

    public Date getMaxSalesHistoryDate() {
	return maxSalesHistoryDate;
    }

    public void setMaxSalesHistoryDate(Date maxSalesHistoryDate) {
	this.maxSalesHistoryDate = maxSalesHistoryDate;
    }

    public Integer getNadaRegionCode() {
	return nadaRegionCode;
    }

    public void setNadaRegionCode(Integer nadaRegionCode) {
	this.nadaRegionCode = nadaRegionCode;
    }

    public Float getPackAmount() {
	return packAmount;
    }

    public void setPackAmount(Float packAmount) {
	this.packAmount = packAmount;
    }

    public String getPopulateClassName() {
	return populateClassName;
    }

    public void setPopulateClassName(String populateClassName) {
	this.populateClassName = populateClassName;
    }

    public ProgramType getProgramType() {
	return programType;
    }

    public void setProgramType(ProgramType programType) {
	this.programType = programType;
    }

    public Integer getPurchasingDistanceFromDealer() {
	return purchasingDistanceFromDealer;
    }

    public void setPurchasingDistanceFromDealer(
	    Integer purchasingDistanceFromDealer) {
	this.purchasingDistanceFromDealer = purchasingDistanceFromDealer;
    }

    public Integer getLiveAuctionDistanceFromDealer() {
	return liveAuctionDistanceFromDealer;
    }

    public void setLiveAuctionDistanceFromDealer(
	    Integer liveAuctionDistanceFromDealer) {
	this.liveAuctionDistanceFromDealer = liveAuctionDistanceFromDealer;
    }

    public Day getRunDay() {
	return runDay;
    }

    public void setRunDay(Day runDay) {
	this.runDay = runDay;
    }

    public Integer getSearchInactiveInventoryDaysBackThreshold() {
	return searchInactiveInventoryDaysBackThreshold;
    }

    public void setSearchInactiveInventoryDaysBackThreshold(
	    Integer searchInactiveInventoryDaysBackThreshold) {
	this.searchInactiveInventoryDaysBackThreshold = searchInactiveInventoryDaysBackThreshold;
    }

    public Integer getSellThroughRate() {
	return sellThroughRate;
    }

    public void setSellThroughRate(Integer sellThroughRate) {
	this.sellThroughRate = sellThroughRate;
    }

    public Boolean getShowLotLocationStatus() {
	return showLotLocationStatus;
    }

    public void setShowLotLocationStatus(Boolean showLotLocationStatus) {
	this.showLotLocationStatus = showLotLocationStatus;
    }

    public Integer getStockOrVinPreference() {
	return stockOrVinPreference;
    }

    public void setStockOrVinPreference(Integer stockOrVinPreference) {
	this.stockOrVinPreference = stockOrVinPreference;
    }

    public Integer getUnitCostOrListPricePreference() {
	return unitCostOrListPricePreference;
    }

    public void setUnitCostOrListPricePreference(
	    Integer unitCostOrListPricePreference) {
	this.unitCostOrListPricePreference = unitCostOrListPricePreference;
    }

    public Float getUnitCostThreshold() {
	return unitCostThreshold;
    }

    public void setUnitCostThreshold(Float unitCostThreshold) {
	this.unitCostThreshold = unitCostThreshold;
    }

    public Float getUnitCostThresholdLower() {
	return unitCostThresholdLower;
    }

    public void setUnitCostThresholdLower(Float unitCostThresholdLower) {
	this.unitCostThresholdLower = unitCostThresholdLower;
    }

    public Float getUnitCostThresholdUpper() {
	return unitCostThresholdUpper;
    }

    public void setUnitCostThresholdUpper(Float unitCostThresholdUpper) {
	this.unitCostThresholdUpper = unitCostThresholdUpper;
    }

    public Boolean getUnitCostUpdateOnSale() {
	return unitCostUpdateOnSale;
    }

    public void setUnitCostUpdateOnSale(Boolean unitCostUpdateOnSale) {
	this.unitCostUpdateOnSale = unitCostUpdateOnSale;
    }

    public Integer getUnitsSoldThreshold12Wks() {
	return unitsSoldThreshold12Wks;
    }

    public void setUnitsSoldThreshold12Wks(Integer unitsSoldThreshold12Wks) {
	this.unitsSoldThreshold12Wks = unitsSoldThreshold12Wks;
    }

    public Integer getUnitsSoldThreshold13Wks() {
	return unitsSoldThreshold13Wks;
    }

    public void setUnitsSoldThreshold13Wks(Integer unitsSoldThreshold13Wks) {
	this.unitsSoldThreshold13Wks = unitsSoldThreshold13Wks;
    }

    public Integer getUnitsSoldThreshold26Wks() {
	return unitsSoldThreshold26Wks;
    }

    public void setUnitsSoldThreshold26Wks(Integer unitsSoldThreshold26Wks) {
	this.unitsSoldThreshold26Wks = unitsSoldThreshold26Wks;
    }

    public Integer getUnitsSoldThreshold4Wks() {
	return unitsSoldThreshold4Wks;
    }

    public void setUnitsSoldThreshold4Wks(Integer unitsSoldThreshold4Wks) {
	this.unitsSoldThreshold4Wks = unitsSoldThreshold4Wks;
    }

    public Integer getUnitsSoldThreshold52Wks() {
	return unitsSoldThreshold52Wks;
    }

    public void setUnitsSoldThreshold52Wks(Integer unitsSoldThreshold52Wks) {
	this.unitsSoldThreshold52Wks = unitsSoldThreshold52Wks;
    }

    public Integer getUnitsSoldThreshold8Wks() {
	return unitsSoldThreshold8Wks;
    }

    public void setUnitsSoldThreshold8Wks(Integer unitsSoldThreshold8Wks) {
	this.unitsSoldThreshold8Wks = unitsSoldThreshold8Wks;
    }

    public Integer getUnitsSoldThresholdInvOverview() {
	return unitsSoldThresholdInvOverview;
    }

    public void setUnitsSoldThresholdInvOverview(
	    Integer unitsSoldThresholdInvOverview) {
	this.unitsSoldThresholdInvOverview = unitsSoldThresholdInvOverview;
    }

    public Integer getUnwindDaysThreshold() {
	return unwindDaysThreshold;
    }

    public void setUnwindDaysThreshold(Integer unwindsDaysThreshold) {
	this.unwindDaysThreshold = unwindsDaysThreshold;
    }

    public Float getVehicleSaleThresholdForCoreMarketPenetration() {
	return vehicleSaleThresholdForCoreMarketPenetration;
    }

    public void setVehicleSaleThresholdForCoreMarketPenetration(
	    Float vehicleSaleThresholdForCoreMarketPenetration) {
	this.vehicleSaleThresholdForCoreMarketPenetration = vehicleSaleThresholdForCoreMarketPenetration;
    }

    public Day getAipRunDayOfWeek() {
	return aipRunDayOfWeek;
    }

    public void setAipRunDayOfWeek(Day aipRunDayOfWeek) {
	this.aipRunDayOfWeek = aipRunDayOfWeek;
    }

    public Integer getBookOutPreferenceId() {
	return bookOutPreferenceId;
    }

    public void setBookOutPreferenceId(Integer bookOutPreferenceId) {
    	if(!this.bookOutPreferenceId.equals(bookOutPreferenceId)) {
    		primaryBookChanged = true;
    	}
    	this.bookOutPreferenceId = bookOutPreferenceId;
    }

    public Integer getBookOutPreferenceSecondId() {
	return bookOutPreferenceSecondId;
    }

    public void setBookOutPreferenceSecondId(Integer bookOutPreferenceSecondId) {
	this.bookOutPreferenceSecondId = bookOutPreferenceSecondId;
    }

    public Integer getGuideBook2BookOutPreferenceId() {
	return guideBook2BookOutPreferenceId;
    }

    public void setGuideBook2BookOutPreferenceId(
	    Integer guideBook2BookOutPreferenceId) {
	this.guideBook2BookOutPreferenceId = guideBook2BookOutPreferenceId;
    }

    public Integer getGuideBook2Id() {
	return guideBook2Id;
    }

    public void setGuideBook2Id(Integer guideBook2Id) {
	this.guideBook2Id = guideBook2Id;
    }

    public Integer getGuideBook2SecondBookOutPreferenceId() {
	return guideBook2SecondBookOutPreferenceId;
    }

    public void setGuideBook2SecondBookOutPreferenceId(
	    Integer guideBook2SecondBookOutPreferenceId) {
	this.guideBook2SecondBookOutPreferenceId = guideBook2SecondBookOutPreferenceId;
    }

    public Integer getGuideBookId() {
	return guideBookId;
    }

    public void setGuideBookId(Integer guideBookId) {
    	if(!this.guideBookId.equals(guideBookId)) {
    		primaryBookChanged = true;
    	}
	this.guideBookId = guideBookId;
    }

    public Integer getShowroomDaysFilter() {
	return showroomDaysFilter;
    }

    public void setShowroomDaysFilter(Integer showroomDaysFilter) {
	this.showroomDaysFilter = showroomDaysFilter;
    }

    public Boolean getBucketJumperAIPApproach() {
	return bucketJumperAIPApproach;
    }

    public void setBucketJumperAIPApproach(Boolean bucketJumperAIPApproach) {
	this.bucketJumperAIPApproach = bucketJumperAIPApproach;
    }

    public Boolean getGmacEnabled() {
	return gmacEnabled;
    }

    public void setGmacEnabled(Boolean gmacEnabled) {
	this.gmacEnabled = gmacEnabled;
    }

    public Boolean getTfsEnabled() {
	return tfsEnabled;
    }

    public void setTfsEnabled(Boolean tfsEnabled) {
	this.tfsEnabled = tfsEnabled;
    }
    
    public Boolean getOveEnabled() {
    	return oveEnabled;
    }
    
    public void setOveEnabled(Boolean oveEnabled) {
    	this.oveEnabled = oveEnabled;
    }

    public Integer getTradeManagerDaysFilter() {
	return tradeManagerDaysFilter;
    }

    public void setTradeManagerDaysFilter(Integer tradeManagerDaysFilter) {
	this.tradeManagerDaysFilter = tradeManagerDaysFilter;
    }

    public Integer getAppraisalRequirementLevel() {
	return appraisalRequirementLevel;
    }

    public void setAppraisalRequirementLevel(Integer appraisalRequirementLevel) {
	this.appraisalRequirementLevel = appraisalRequirementLevel;
    }

    public Boolean getRequireNameOnAppraisals() {
    	return requireNameOnAppraisals;
    }

    public void setRequireNameOnAppraisals(Boolean requireNameOnAppraisals) {
    	this.requireNameOnAppraisals = requireNameOnAppraisals;
    }

	public Boolean getRequireEstReconCostOnAppraisals() {
		return requireEstReconCostOnAppraisals;
	}

	public void setRequireEstReconCostOnAppraisals( Boolean requireEstReconCostOnAppraisals ) {
		this.requireEstReconCostOnAppraisals = requireEstReconCostOnAppraisals;
	}

	public Boolean getRequireReconNotesOnAppraisals() {
		return requireReconNotesOnAppraisals;
	}

	public void setRequireReconNotesOnAppraisals( Boolean requireReconNotesOnAppraisals ) {
		this.requireReconNotesOnAppraisals = requireReconNotesOnAppraisals;
	}

    
    public Integer getFlashLocatorHideUnitCostDays() {
	return flashLocatorHideUnitCostDays;
    }

    public void setFlashLocatorHideUnitCostDays(
	    Integer flashLocatorHideUnitCostDays) {
	this.flashLocatorHideUnitCostDays = flashLocatorHideUnitCostDays;
    }

    public Integer getCheckAppraisalHistoryForIMPPlanning() {
	return checkAppraisalHistoryForIMPPlanning;
    }

    public void setCheckAppraisalHistoryForIMPPlanning(
	    Integer checkAppraisalHistoryForIMPPlanning) {
	this.checkAppraisalHistoryForIMPPlanning = checkAppraisalHistoryForIMPPlanning;
    }

    public String getAppraisalFormDisclaimer() {
	return appraisalFormDisclaimer;
    }

    public void setAppraisalFormDisclaimer(String appraisalFormDisclaimer) {
	this.appraisalFormDisclaimer = appraisalFormDisclaimer;
    }

    public String getAppraisalFormMemo() {
	return appraisalFormMemo;
    }

    public void setAppraisalFormMemo(String appraisalFormMemo) {
	this.appraisalFormMemo = appraisalFormMemo;
    }

    public Integer getAppraisalFormValidDate() {
	return appraisalFormValidDate;
    }

    public void setAppraisalFormValidDate(Integer appraisalFormValidDate) {
	this.appraisalFormValidDate = appraisalFormValidDate;
    }

    public Integer getAppraisalFormValidMileage() {
	return appraisalFormValidMileage;
    }

    public void setAppraisalFormValidMileage(Integer appraisalFormValidMileage) {
	this.appraisalFormValidMileage = appraisalFormValidMileage;
    }

    public Integer getApplyDefaultPlanToGreenLightsInIMP() {
	return applyDefaultPlanToGreenLightsInIMP;
    }

    public void setApplyDefaultPlanToGreenLightsInIMP(
	    Integer applyDefaultPlanToGreenLightsInIMP) {
	this.applyDefaultPlanToGreenLightsInIMP = applyDefaultPlanToGreenLightsInIMP;
    }

    public Integer getAppraisalFormShowOptions() {
	return appraisalFormShowOptions;
    }

    public void setAppraisalFormShowOptions(Integer appraisalFormShowOptions) {
	this.appraisalFormShowOptions = appraisalFormShowOptions;
    }

    public Integer getAppraisalFormShowPhotos() {
        //-------FB:29754---------//
    	//return appraisalFormShowPhotos;
    	return 1;
    }

    public void setAppraisalFormShowPhotos(Integer appraisalFormShowPhotos) {
        //-------FB:29754---------//
    	//this.appraisalFormShowPhotos = appraisalFormShowPhotos;
    	this.appraisalFormShowPhotos = 1;
    }

    public Integer getShowAppraisalForm() {
    //-------FB:29754---------//
	// return showAppraisalForm;
	   return 1;
    }

    public void setShowAppraisalForm(Integer showAppraisalForm) {
    //-------FB:29754---------//
	// this.showAppraisalForm = showAppraisalForm;
       this.showAppraisalForm = 1;
    }

    public Boolean getRepriceConfirmation() {
	return repriceConfirmation;
    }

    public void setRepriceConfirmation(Boolean repriceConfrimation) {
	this.repriceConfirmation = repriceConfrimation;
    }

    public Integer getRepricePercentChangeThreshold() {
	return repricePercentChangeThreshold;
    }

    public void setRepricePercentChangeThreshold(
	    Integer repricePercentChangeThreshold) {
	this.repricePercentChangeThreshold = repricePercentChangeThreshold;
    }

    public Boolean getSendZeroesAsNull() {
	return sendZeroesAsNull;
    }

    public void setSendZeroesAsNull(Boolean sendZeroesAsNull) {
	this.sendZeroesAsNull = sendZeroesAsNull;
    }

    public Integer getKbbAppraisalBookoutDatasetPreference() {
	return kbbAppraisalBookoutDatasetPreference;
    }

    public void setKbbAppraisalBookoutDatasetPreference(
	    Integer appraisalBookoutDatasetPreference) {
	kbbAppraisalBookoutDatasetPreference = appraisalBookoutDatasetPreference;
    }

    public Integer getKbbInventoryBookoutDatasetPreference() {
	return kbbInventoryBookoutDatasetPreference;
    }

    public void setKbbInventoryBookoutDatasetPreference(
	    Integer inventoryBookoutDatasetPreference) {
	kbbInventoryBookoutDatasetPreference = inventoryBookoutDatasetPreference;
    }

    public Integer getKbbInventoryDefaultCondition() {
	return kbbInventoryDefaultCondition;
    }

    public void setKbbInventoryDefaultCondition(
	    Integer kbbInventoryDefaultCondition) {
	this.kbbInventoryDefaultCondition = kbbInventoryDefaultCondition;
    }

    public Boolean getKbbOverrideEnabled() {
	if (kbbArchiveDataSetOverride == null
		|| kbbArchiveDataSetOverride.isEmpty())
	    return Boolean.FALSE;
	if (kbbArchiveDataSetOverride.get(0).getEndDate().after(
		Calendar.getInstance().getTime()))
	    return Boolean.TRUE;
	return Boolean.FALSE;
    }

    public void setKbbOverrideEnabled(Boolean inFlag) {
	// this says - if check box is checked and we don't already have an
	// override saved - save an override
	if (inFlag && this.kbbArchiveDataSetOverride.isEmpty()) {
	    KBBArchiveDatasetOverride override = new KBBArchiveDatasetOverride();
	    override.setBusinessUnitId(dealer.getId());
	    override.setMemberId(100000);
	    this.kbbArchiveDataSetOverride.add(override);
	}
	if (!inFlag && !this.kbbArchiveDataSetOverride.isEmpty()) {
	    this.kbbArchiveDataSetOverride.get(0).setEndDate(
		    Calendar.getInstance().getTime());
	}
    }

    public List<KBBArchiveDatasetOverride> getKbbArchiveDataSetOverride() {
	return kbbArchiveDataSetOverride;
    }

    public void setKbbArchiveDataSetOverride(
	    List<KBBArchiveDatasetOverride> kbbArchiveDataSetOverride) {
	this.kbbArchiveDataSetOverride = kbbArchiveDataSetOverride;
    }

    public Integer getGroupAppraisalSearchWeeks() {
	return groupAppraisalSearchWeeks;
    }

    public void setGroupAppraisalSearchWeeks(Integer groupAppraisalSearchWeeks) {
	this.groupAppraisalSearchWeeks = groupAppraisalSearchWeeks;
    }

    public Boolean getShowAppraisalValueGroup() {
	return showAppraisalValueGroup;
    }

    public void setShowAppraisalValueGroup(Boolean showAAppraisalValueGroup) {
	this.showAppraisalValueGroup = showAAppraisalValueGroup;
    }

    public Boolean getShowAppraisalFormOfferGroup() {
	return showAppraisalFormOfferGroup;
    }

    public void setShowAppraisalFormOfferGroup(
	    Boolean showAppraisalFormOfferGroup) {
	this.showAppraisalFormOfferGroup = showAppraisalFormOfferGroup;
    }

    public Integer getRedistributionDealerDistance() {
	return redistributionDealerDistance;
    }

    public void setRedistributionDealerDistance(
	    Integer redistributionDealerDistance) {
	this.redistributionDealerDistance = redistributionDealerDistance;
    }

    public Integer getRedistributionNumTopDealers() {
	return redistributionNumTopDealers;
    }

    public void setRedistributionNumTopDealers(
	    Integer redistributionNumTopDealers) {
	this.redistributionNumTopDealers = redistributionNumTopDealers;
    }

    public Integer getRedistributionRoi() {
	return redistributionRoi;
    }

    public void setRedistributionRoi(Integer redistributionRoi) {
	this.redistributionRoi = redistributionRoi;
    }

    public Integer getRedistributionUnderstock() {
	return redistributionUnderstock;
    }

    public void setRedistributionUnderstock(Integer redistributionUnderstock) {
	this.redistributionUnderstock = redistributionUnderstock;
    }

	public Integer getShowCheckOnAppraisalForm() {
		return showCheckOnAppraisalForm;
	}

	public void setShowCheckOnAppraisalForm(Integer showCheckOnAppraisalForm) {
		this.showCheckOnAppraisalForm = showCheckOnAppraisalForm;
	}
  
	public Boolean getShowInactiveAppraisals() {
		return showInactiveAppraisals;
	}

	public void setShowInactiveAppraisals(Boolean showInactiveAppraisals) {
		this.showInactiveAppraisals = showInactiveAppraisals;
	}

	public Integer getDefaultLithiaCarCenterMemberId() {
		return defaultLithiaCarCenterMemberId;
	}

	public void setDefaultLithiaCarCenterMemberId( Integer defaultLithiaCarCenterMemberID ) {
		if (defaultLithiaCarCenterMemberID == null || defaultLithiaCarCenterMemberID.intValue() == 0) {
			this.defaultLithiaCarCenterMemberId = null;
		} else {
			defaultLithiaCarCenterMemberId = defaultLithiaCarCenterMemberID;
		}
	}

	public Boolean getTwixEnabled() {
		if (twixURL == null || twixURL.trim().length() == 0) {
			twixEnabled = Boolean.FALSE;
		} else {
			twixEnabled = Boolean.TRUE;			
		}
		return twixEnabled;
	}

	public void setExcludeWholesaleFromDaysSupply(Boolean excludeWholesaleFromDaysSupply) {
		this.excludeWholesaleFromDaysSupply = excludeWholesaleFromDaysSupply;
	}

	public Boolean getExcludeWholesaleFromDaysSupply() {
		return excludeWholesaleFromDaysSupply;
	}
/*
	public Boolean isExcludeWholesaleFromDaysSupply() {
		return excludeWholesaleFromDaysSupply;
	}
*/
	public void setTwixEnabled(Boolean twixEnabled) {
		this.twixEnabled = twixEnabled;
	}

	public String getTwixURL() {
		return twixURL;
	}

	public void setTwixURL(String twixURL) {
		if (!twixEnabled) {
			this.twixURL = null;
		} else {
			this.twixURL = twixURL;
		}
	}

	public Boolean getUseLotPrice() {
		return useLotPrice;
	}

	public void setUseLotPrice(Boolean useLotPrice) {
		this.useLotPrice = useLotPrice;
	}

	public Integer getSearchAppraisalDaysBackThreshold() {
		return searchAppraisalDaysBackThreshold;
	}

	public void setSearchAppraisalDaysBackThreshold(
			Integer searchAppraisalDaysBackThreshold) {
		this.searchAppraisalDaysBackThreshold = searchAppraisalDaysBackThreshold;
	}

	public Integer getAppraisalLookBackPeriod() {
		return appraisalLookBackPeriod;
	}

	public void setAppraisalLookBackPeriod(Integer appraisalLookBackPeriod) {
		this.appraisalLookBackPeriod = appraisalLookBackPeriod;
	}

	public Integer getAppraisalLookForwardPeriod() {
		return appraisalLookForwardPeriod;
	}

	public void setAppraisalLookForwardPeriod(Integer appraisalLookForwardPeriod) {
		this.appraisalLookForwardPeriod = appraisalLookForwardPeriod;
	}	

	public boolean isPrimaryBookChanged() {
		return primaryBookChanged;
	}
	
	public void resetPrimaryBookChangedFlag() {
		primaryBookChanged = false;
	}
	
	public Boolean getShowInTransitInventoryForm() {
		return showInTransitInventoryForm;
	}
	
	

	public Boolean getBlackBookMobileOptInFlag() {
		return blackBookMobileOptInFlag;
	}

	public void setBlackBookMobileOptInFlag(Boolean blackBookMobileOptInFlag) {
		Boolean temp = blackBookMobileOptInFlag;
		if((this.blackBookMobileOptInFlag!=blackBookMobileOptInFlag)&&(blackBookMobileOptInFlag))
			blackBookOptInDate= new Date();
		if((this.blackBookMobileOptInFlag!=null)&&(this.blackBookMobileOptInFlag!=blackBookMobileOptInFlag)&&(!blackBookMobileOptInFlag))
			blackBookOptOutDate=new Date();
		if(this.blackBookMobileOptInFlag==null&&(!temp))
			temp=null;
		
		this.blackBookMobileOptInFlag = temp;
		
		temp=null;
		
		
	}

	public void setShowInTransitInventoryForm(Boolean showInTransitInventoryForm) {
		this.showInTransitInventoryForm = showInTransitInventoryForm;
	}

}
