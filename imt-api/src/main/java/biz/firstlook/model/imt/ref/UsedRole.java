package biz.firstlook.model.imt.ref;


public enum UsedRole {
	NO_ACCESS,
	APPRAISER,
	STANDARD,
	MANAGER;
}
