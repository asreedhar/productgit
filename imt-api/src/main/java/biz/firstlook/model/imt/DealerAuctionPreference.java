package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DealerAuctionPreference")
public class DealerAuctionPreference implements Serializable {

	private static final long serialVersionUID = 1474962916722419161L;

	@Id
	@Column(name="BusinessUnitID")
	private Integer id;

	private Boolean auctionSearchEnabled = Boolean.FALSE;
	private Integer maxMilesAway = Integer.valueOf(100);
	private Integer maxDaysAhead = Integer.valueOf(30);
	
	public DealerAuctionPreference() {}

	public Boolean getAuctionSearchEnabled() {
		return auctionSearchEnabled;
	}

	public void setAuctionSearchEnabled(Boolean auctionSearchEnabled) {
		this.auctionSearchEnabled = auctionSearchEnabled;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMaxDaysAhead() {
		return maxDaysAhead;
	}

	public void setMaxDaysAhead(Integer maxDaysAhead) {
		this.maxDaysAhead = maxDaysAhead;
	}

	public Integer getMaxMilesAway() {
		return maxMilesAway;
	}

	public void setMaxMilesAway(Integer maxMilesAway) {
		this.maxMilesAway = maxMilesAway;
	}

	
}
