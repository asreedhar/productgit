package biz.firstlook.model.imt;

public enum ThirdPartyEnum
{
	BLACKBOOK( 1, "Black Book" ),
	NADA( 2, "NADA" ),
	KBB( 3, "Kelley Blue Book"),
	GALVES( 4, "Galves" ),
	EDMUNDS( 5, "Edmunds" ),
	MMR(6, "MMR");
	
	private int thirdPartyId;
	private String description;
	
	ThirdPartyEnum( int thirdPartyId, String description )
	{
		this.thirdPartyId = thirdPartyId;
		this.description = description;
	}

	public static ThirdPartyEnum getEnumById( int thirdPartyId )
	{
		switch( thirdPartyId )
		{
			case 1: return BLACKBOOK;
			case 2: return NADA;
			case 3: return KBB;
			case 4: return GALVES;
			case 5: return EDMUNDS;
			case 6: return MMR;
			default: return null;
		}
	}

	public String getDescription()
	{
		return description;
	}

	public int getThirdPartyId()
	{
		return thirdPartyId;
	}
	
}
