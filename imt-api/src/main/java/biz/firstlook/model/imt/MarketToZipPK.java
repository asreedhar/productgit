package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class MarketToZipPK implements Serializable {

	private static final long serialVersionUID = -8742784996770913858L;

	private String zipcode;
	private Integer businessUnitId;
	private Integer ring;
	
	public MarketToZipPK() {}

	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}

	public Integer getRing() {
		return ring;
	}

	public void setRing(Integer ring) {
		this.ring = ring;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	@Override
	public boolean equals(Object o) {
		if( o instanceof MarketToZipPK ) {
			MarketToZipPK pk = (MarketToZipPK) o;
			if( pk.getRing().equals( this.getRing() )  &&
				pk.getBusinessUnitId().equals( this.getBusinessUnitId() ) &&
				pk.getZipcode().equals( this.getZipcode() ) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = this.getRing().hashCode() << 8;
		hash += this.getBusinessUnitId().hashCode() << 4;
		hash += this.getZipcode().hashCode();
		return hash;
	}

	@Override
	public String toString() {
		return "MarketToZipPK: " + this.getRing() + ", " + this.getBusinessUnitId() + ", " + this.getZipcode();
	}
	
	
	
}
