package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.model.imt.ref.InventoryVehicleType;

@Entity
@Table(name="TargetSection")
public class TargetSection {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SectionCD")
	private Integer code;
	
	@Column(name="SectionName")
	private String name;
	
	@Column(name="InventoryVehicleTypeCD")
	private InventoryVehicleType type;
	
	public TargetSection() {}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InventoryVehicleType getType() {
		return type;
	}

	public void setType(InventoryVehicleType type) {
		this.type = type;
	}
	
	
	
	
}
