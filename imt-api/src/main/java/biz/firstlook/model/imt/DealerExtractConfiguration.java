package biz.firstlook.model.imt;

import java.io.Serializable;
import java.util.Date;

public class DealerExtractConfiguration implements Serializable {

	private static final long serialVersionUID = -56081784226342134L;
	
	private Integer dealerId;
	private String destinationName;
	private String externalIdentifier;
	private Date startDate;
	private Date endDate;
	private boolean active;
	private Date updateDate;
	private String updateUser;
	private boolean editable = true;
	private String editRuleMessage;
	
	public DealerExtractConfiguration(Integer dealerId, String destinationName) {
		super();
		this.dealerId = dealerId;
		this.destinationName = destinationName;
	}
	
	public DealerExtractConfiguration(Integer dealerId, String destinationName, boolean isEditable, String editRuleMessage) {
		super();
		this.dealerId = dealerId;
		this.destinationName = destinationName;
		this.editable = isEditable;
		this.editRuleMessage = editRuleMessage;
	}
	
	public Integer getDealerId() {
		return dealerId;
	}
	
	public String getDestinationName() {
		return destinationName;
	}
	
	public boolean isEditable() {
		return editable;
	}
	
	public String getEditRuleMessage() {
		return editRuleMessage;
	}
	
	public String getExternalIdentifier() {
		return externalIdentifier;
	}
	public void setExternalIdentifier(String externalIdentifier) {
		this.externalIdentifier = externalIdentifier;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result
				+ ((dealerId == null) ? 0 : dealerId.hashCode());
		result = prime * result
				+ ((destinationName == null) ? 0 : destinationName.hashCode());
		result = prime * result + (editable ? 1231 : 1237);
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime
				* result
				+ ((externalIdentifier == null) ? 0 : externalIdentifier
						.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result
				+ ((updateDate == null) ? 0 : updateDate.hashCode());
		result = prime * result
				+ ((updateUser == null) ? 0 : updateUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DealerExtractConfiguration other = (DealerExtractConfiguration) obj;
		if (active != other.active)
			return false;
		if (dealerId == null) {
			if (other.dealerId != null)
				return false;
		} else if (!dealerId.equals(other.dealerId))
			return false;
		if (destinationName == null) {
			if (other.destinationName != null)
				return false;
		} else if (!destinationName.equals(other.destinationName))
			return false;
		if (editable != other.editable)
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (externalIdentifier == null) {
			if (other.externalIdentifier != null)
				return false;
		} else if (!externalIdentifier.equals(other.externalIdentifier))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (updateDate == null) {
			if (other.updateDate != null)
				return false;
		} else if (!updateDate.equals(other.updateDate))
			return false;
		if (updateUser == null) {
			if (other.updateUser != null)
				return false;
		} else if (!updateUser.equals(other.updateUser))
			return false;
		return true;
	}
}
