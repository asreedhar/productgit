package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import biz.firstlook.common.model.Identifiable;

@Entity
@Table(name = "map_BusinessUnitToMarketDealer")
public class BusinessUnitMarketDealer implements Identifiable<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "BusinessUnitID")
    private Dealer dealer;

    private Integer dealerNumber;

    /**
     * Needed because the framework expects the bean and action form to have the same properties.
     */
    private transient String selectedState;

    /**
     * Needed because the framework expects the bean and action form to have the same properties.
     */
    private transient String selectedCounty;
    
    /**
     * Needed because the framework expects the bean and action form to have the same properties.
     */
    private transient String marketDealerName;

    public BusinessUnitMarketDealer() {
    }

    public Dealer getDealer() {
	return dealer;
    }

    public void setDealer(Dealer dealer) {
	this.dealer = dealer;
    }

    public Integer getDealerNumber() {
	return dealerNumber;
    }

    public void setDealerNumber(Integer dealerNumber) {
	this.dealerNumber = dealerNumber;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getSelectedState() {
	return selectedState;
    }

    public void setSelectedState(String selectedState) {
	this.selectedState = selectedState;
    }

    public String getSelectedCounty() {
	return selectedCounty;
    }

    public void setSelectedCounty(String selectedCounty) {
	this.selectedCounty = selectedCounty;
    }

    public String getMarketDealerName() {
        return marketDealerName;
    }

    public void setMarketDealerName(String marketDealerName) {
        this.marketDealerName = marketDealerName;
    }
}
