package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Ok. So, DealerPreferencePricing is managed MANUALLY as a separate entity. It is NOT managed from the Dealer Entity
 * using hibernate. This is for 1 very simple yet crappy reason. DealerPreferencePricing values roll up from the default
 * firstlook dealer (100150). If a dealership doesn't have values, they use the default ones. If they change the default values
 * then dealer specific values are written to the table. Hibernate starts freaking with this because the firstlook default dealer (100150)
 * is NOT in the DB as a standard dealer.  
 * 
 * @author nkeen
 *
 */
@Entity
@Table(name = "DealerPreference_Pricing")
public class DealerPreferencePricing implements Serializable {


	private static final long serialVersionUID = 1535628451907784093L;
	
    @Id
    @Column(name = "BusinessUnitID")
    private Integer id;

	//Ping II Settings
    private Integer pingII_DefaultSearchRadius = null;
    private Integer pingII_MaxSearchRadius = null;
	private Boolean pingII_SupressSellerName = null;
	private Boolean pingII_ExcludeNoPriceFromCalc = null;
	private Double pingII_ExcludeLowPriceOutliersMultiplier = null;
	private Double pingII_ExcludeHighPriceOutliersMultiplier = null;
	private Integer pingII_GuideBookId = null;
	@Column(name = "PackageID")
	private Integer pingII_PackageId = null;
	@Column(name = "MatchCertifiedByDefault")
	private Boolean pingII_MatchCertifiedByDefault = null;
	@Column(name = "SuppressTrimMatchStatus")
	private Boolean pingII_SuppressTrimMatchStatus = null;
	@Column(name = "IsNewPing")
	 private Boolean pingII_NewPing = null;
	 private String pingII_MarketListing;
	 private Integer  pingII_RedRange1Value1;
	 private Integer  pingII_RedRange1Value2;
	 private Integer  pingII_YellowRange1Value1;
	 private Integer  pingII_YellowRange1Value2;
	 private Integer  pingII_YellowRange2Value1;
	 private Integer  pingII_YellowRange2Value2;
	 private Integer  pingII_GreenRange1Value1;
	 private Integer  pingII_GreenRange1Value2;
	 
    public Integer getPingII_RedRange1Value1() {
		return pingII_RedRange1Value1;
	}

	public void setPingII_RedRange1Value1(Integer pingII_RedRange1Value1) {
		this.pingII_RedRange1Value1 = pingII_RedRange1Value1;
	}

	public Integer getPingII_RedRange1Value2() {
		return pingII_RedRange1Value2;
	}

	public void setPingII_RedRange1Value2(Integer pingII_RedRange1Value2) {
		this.pingII_RedRange1Value2 = pingII_RedRange1Value2;
	}

	public Integer getPingII_YellowRange1Value1() {
		return pingII_YellowRange1Value1;
	}

	public void setPingII_YellowRange1Value1(Integer pingII_YellowRange1Value1) {
		this.pingII_YellowRange1Value1 = pingII_YellowRange1Value1;
	}

	public Integer getPingII_YellowRange1Value2() {
		return pingII_YellowRange1Value2;
	}

	public void setPingII_YellowRange1Value2(Integer pingII_YellowRange1Value2) {
		this.pingII_YellowRange1Value2 = pingII_YellowRange1Value2;
	}

	public Integer getPingII_YellowRange2Value1() {
		return pingII_YellowRange2Value1;
	}

	public void setPingII_YellowRange2Value1(Integer pingII_YellowRange2Value1) {
		this.pingII_YellowRange2Value1 = pingII_YellowRange2Value1;
	}

	public Integer getPingII_YellowRange2Value2() {
		return pingII_YellowRange2Value2;
	}

	public void setPingII_YellowRange2Value2(Integer pingII_YellowRange2Value2) {
		this.pingII_YellowRange2Value2 = pingII_YellowRange2Value2;
	}

	public Integer getPingII_GreenRange1Value1() {
		return pingII_GreenRange1Value1;
	}

	public void setPingII_GreenRange1Value1(Integer pingII_GreenRange1Value1) {
		this.pingII_GreenRange1Value1 = pingII_GreenRange1Value1;
	}

	public Integer getPingII_GreenRange1Value2() {
		return pingII_GreenRange1Value2;
	}

	public void setPingII_GreenRange1Value2(Integer pingII_GreenRange1Value2) {
		this.pingII_GreenRange1Value2 = pingII_GreenRange1Value2;
	}

	public DealerPreferencePricing() {
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getPingII_ExcludeHighPriceOutliersMultiplier() {
		return pingII_ExcludeHighPriceOutliersMultiplier;
	}

	public void setPingII_ExcludeHighPriceOutliersMultiplier(
			Double pingII_ExcludeHighPriceOutliersMultiplier) {
		this.pingII_ExcludeHighPriceOutliersMultiplier = pingII_ExcludeHighPriceOutliersMultiplier;
	}

	public Double getPingII_ExcludeLowPriceOutliersMultiplier() {
		return pingII_ExcludeLowPriceOutliersMultiplier;
	}

	public void setPingII_ExcludeLowPriceOutliersMultiplier(
			Double pingII_ExcludeLowPriceOutliersMultiplier) {
		this.pingII_ExcludeLowPriceOutliersMultiplier = pingII_ExcludeLowPriceOutliersMultiplier;
	}

	public Boolean getPingII_ExcludeNoPriceFromCalc() {
		return pingII_ExcludeNoPriceFromCalc;
	}

	public void setPingII_ExcludeNoPriceFromCalc(
			Boolean pingII_ExcludeNoPriceFromCalc) {
		this.pingII_ExcludeNoPriceFromCalc = pingII_ExcludeNoPriceFromCalc;
	}

	public Integer getPingII_DefaultSearchRadius() {
		return pingII_DefaultSearchRadius;
	}

	public void setPingII_DefaultSearchRadius(Integer pingII_DefaultSearchRadius) {
		this.pingII_DefaultSearchRadius = pingII_DefaultSearchRadius;
	}

	public Integer getPingII_MaxSearchRadius() {
		return pingII_MaxSearchRadius;
	}

	public void setPingII_MaxSearchRadius(Integer pingII_MaxSearchRadius) {
		this.pingII_MaxSearchRadius = pingII_MaxSearchRadius;
	}

	public Boolean getPingII_SupressSellerName() {
		return pingII_SupressSellerName;
	}

	public void setPingII_SupressSellerName(Boolean pingII_SupressSellerName) {
		this.pingII_SupressSellerName = pingII_SupressSellerName;
	}

	public Integer getPingII_GuideBookId() {
		return pingII_GuideBookId;
	}

	public void setPingII_GuideBookId(Integer pingII_GuideBookId) {
		this.pingII_GuideBookId = pingII_GuideBookId;
	}

	public Integer getPingII_PackageId() {
		return pingII_PackageId;
	}

	public void setPingII_PackageId(Integer pingII_PackageId) {
		this.pingII_PackageId = pingII_PackageId;
	}

	public Boolean getPingII_MatchCertifiedByDefault() {
		return pingII_MatchCertifiedByDefault;
	}

	public void setPingII_MatchCertifiedByDefault(
			Boolean pingII_MatchCertifiedByDefault) {
		this.pingII_MatchCertifiedByDefault = pingII_MatchCertifiedByDefault;
	}

	public Boolean getPingII_SuppressTrimMatchStatus() {
		return pingII_SuppressTrimMatchStatus;
	}

	public void setPingII_SuppressTrimMatchStatus(
			Boolean pingII_SuppressTrimMatchStatus) {
		this.pingII_SuppressTrimMatchStatus = pingII_SuppressTrimMatchStatus;
	}

	public Boolean getPingII_NewPing() {
		return pingII_NewPing;
	}

	public void setPingII_NewPing(Boolean pingII_NewPing) {
		this.pingII_NewPing = pingII_NewPing;
	}

	public String getPingII_MarketListing() {
		return pingII_MarketListing;
	}

	public void setPingII_MarketListing(String pingII_MarketListing) {
		this.pingII_MarketListing = pingII_MarketListing;
	}
	
}
