package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="JobTitle")
public class JobTitle {

	public static final int LITHIA_CAR_CENTER_ID = 24;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="JobTitleID")
	private Integer id;
	private String name;
	
	public JobTitle() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}
