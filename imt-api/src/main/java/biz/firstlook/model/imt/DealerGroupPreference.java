package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="DealerGroupPreference")
public class DealerGroupPreference implements Serializable {

	private static final long serialVersionUID = 343613839030349881L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DealerGroupPreferenceID")
	private Integer id;

	@OneToOne
	@JoinColumn(name="BusinessUnitID")
	private DealerGroup group;	
	
	private Integer agingPolicy = 60;
	private Integer agingPolicyAdjustment = 0;
	private Boolean includeRedistribution = false;
	private Integer inventoryExchangeAgeLimit = 45;
	private Boolean includeRoundTable = false;
	private Boolean includeCIA = false;
	private Boolean includeRedLightAgingPlan = false;
	private Integer pricePointDealsThreshold = 8;
	
	private Boolean lithiaStore = false;
	
	
	private Boolean includePerformanceManagementCenter = false;


    private Boolean showAppraisalFormOffer;
    private Boolean showAppraisalValue;
    
    private Boolean includeTransferPricing = Boolean.FALSE;

    private String carSearchTitle; 
    private Boolean showCarSearch = Boolean.FALSE;
    private Boolean excludeWholesaleFromDaysSupply = Boolean.FALSE;
  
    //------ case 30018-----//
    private Boolean inventoryBookoutTransfer = Boolean.FALSE;
	
	public Boolean getInventoryBookoutTransfer() {
		return inventoryBookoutTransfer;
	}

	public void setInventoryBookoutTransfer(Boolean inventoryBookoutTransfer) {
		this.inventoryBookoutTransfer = inventoryBookoutTransfer;
	}
	//------ case 30018-----//
	
	public DealerGroupPreference() {}

	public Integer getAgingPolicy() {
		return agingPolicy;
	}

	public void setAgingPolicy(Integer agingPolicy) {
		this.agingPolicy = agingPolicy;
	}

	public Integer getAgingPolicyAdjustment() {
		return agingPolicyAdjustment;
	}

	public void setAgingPolicyAdjustment(Integer agingPolicyAdjustment) {
		this.agingPolicyAdjustment = agingPolicyAdjustment;
	}

	public DealerGroup getGroup() {
		return group;
	}

	public void setGroup(DealerGroup group) {
		this.group = group;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIncludeCIA() {
		return includeCIA;
	}

	public void setIncludeCIA(Boolean includeCIA) {
		this.includeCIA = includeCIA;
	}

	public Boolean getIncludeRedistribution() {
		return includeRedistribution;
	}

	public void setIncludeRedistribution(Boolean includeRedistribution) {
		this.includeRedistribution = includeRedistribution;
	}

	public Boolean getIncludeRedLightAgingPlan() {
		return includeRedLightAgingPlan;
	}

	public void setIncludeRedLightAgingPlan(Boolean includeRedLightAgingPlan) {
		this.includeRedLightAgingPlan = includeRedLightAgingPlan;
	}

	public Boolean getIncludeRoundTable() {
		return includeRoundTable;
	}

	public void setIncludeRoundTable(Boolean includeRoundTable) {
		this.includeRoundTable = includeRoundTable;
	}

	public Integer getInventoryExchangeAgeLimit() {
		return inventoryExchangeAgeLimit;
	}

	public void setInventoryExchangeAgeLimit(Integer inventoryExchangeAgeLimit) {
		this.inventoryExchangeAgeLimit = inventoryExchangeAgeLimit;
	}

	public Boolean getLithiaStore() {
		return lithiaStore;
	}

	public void setLithiaStore(Boolean lithiaStore) {
		this.lithiaStore = lithiaStore;
	}

	public Integer getPricePointDealsThreshold() {
		return pricePointDealsThreshold;
	}

	public void setPricePointDealsThreshold(Integer pricePointDealsThreshold) {
		this.pricePointDealsThreshold = pricePointDealsThreshold;
	}

	public Boolean getShowAppraisalFormOffer() {
        return showAppraisalFormOffer;
    }

    public void setShowAppraisalFormOffer(Boolean showAppraisalFormOffer) {
        this.showAppraisalFormOffer = showAppraisalFormOffer;
    }

    public Boolean getShowAppraisalValue() {
        return showAppraisalValue;
    }

    public void setShowAppraisalValue(Boolean showAppraisalValue) {
        this.showAppraisalValue = showAppraisalValue;
    }

	public Boolean getIncludePerformanceManagementCenter() {
		return includePerformanceManagementCenter;
	}

	public void setIncludePerformanceManagementCenter(Boolean includePerformanceManagementCenter) {
		this.includePerformanceManagementCenter = includePerformanceManagementCenter;
	}

	public Boolean getIncludeTransferPricing() {
		return includeTransferPricing;
	}

	public void setIncludeTransferPricing( Boolean includeTransferPricing )	{
		this.includeTransferPricing = includeTransferPricing;
	}
	
	public String getCarSearchTitle()
	{
	    return carSearchTitle;
	}

	public void setCarSearchTitle( String carSearchTitle )
	{
		this.carSearchTitle = carSearchTitle;
	}

	public Boolean getShowCarSearch()
	{
	    return showCarSearch;
	}
	
	public Boolean isShowCarSearch()
	{
	    return showCarSearch;
	}

	public void setShowCarSearch( Boolean showCarSearch )
	{
		this.showCarSearch = showCarSearch;
	}
	
	public Boolean getExcludeWholesaleFromDaysSupply()
	{
	    return excludeWholesaleFromDaysSupply;
	}
	
	public Boolean isExcludeWholesaleFromDaysSupply()
	{
	    return excludeWholesaleFromDaysSupply;
	}

	public void setExcludeWholesaleFromDaysSupply( Boolean excludeWholesaleFromDaysSupply )
	{
		this.excludeWholesaleFromDaysSupply = excludeWholesaleFromDaysSupply;
	}

}
