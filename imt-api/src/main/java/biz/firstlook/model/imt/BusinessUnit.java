package biz.firstlook.model.imt;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.common.model.Identifiable;
import biz.firstlook.model.imt.ref.BusinessUnitType;
 
@Entity
@Table(name="BusinessUnit")
@DiscriminatorColumn(name="BusinessUnitTypeID", discriminatorType=DiscriminatorType.INTEGER)
@javax.persistence.DiscriminatorValue("0")
public abstract class BusinessUnit implements Serializable, Identifiable<Integer> {
	
	private static final long serialVersionUID = -5562960432303828292L;
	public static int MAX_SHORT_NAME_LEN = 30;
	public static int MAX_NAME_LEN = 40;
	public static int MAX_CODE_LEN = 20;
	public static int MAX_ADDRESS1_LEN = 35;
	public static int MAX_ADDRESS2_LEN = 35;
	public static int MAX_ZIP_LEN = 10;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="BusinessUnitID")
	protected Integer id;
	
	@Column(name="BusinessUnitTypeID", insertable=false, updatable=false)
	private BusinessUnitType type;
	
	@Column(name="BusinessUnit")
	private String name;
	
	@Column(name="BusinessUnitShortName")
	private String shortName;
	
	@Column(name="BusinessUnitCode")
	private String code;
	
	@Column( name ="Address1" )
	private String street1;

	@Column( name ="Address2" )
	private String street2;

	@Column( name ="City" )
	private String city;
	
	@Column( name ="State" )
	private String state;

	@Column( name ="ZipCode" )
	private String zip;
	
	@Column(name="OfficePhone")
	private String phone;
	
	@Column(name="OfficeFax")
	private String fax;
	
	@Column(name="Active")
	private Boolean active;
	
	private BigDecimal latitude;
	
	private BigDecimal longitude;

	public BusinessUnit() {
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public BusinessUnitType getType() {
		return type;
	}

	public void setType(BusinessUnitType type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public java.lang.String getState()
	{
		if(state!=null)
			return state.toUpperCase();
		else
			return state;
	}

	public void setState( String newState )
	{
		if(newState!=null)
			state=newState.toUpperCase();
		else
			state = newState;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	
	
}
