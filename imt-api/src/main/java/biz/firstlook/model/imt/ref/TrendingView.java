package biz.firstlook.model.imt.ref;

public enum TrendingView {
	NONE,
	TOP_SELLER,
	FASTEST_SELLER,
	MOST_PROFITABLE;
}
