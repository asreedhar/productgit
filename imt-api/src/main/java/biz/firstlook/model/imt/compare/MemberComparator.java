package biz.firstlook.model.imt.compare;

import java.util.Comparator;

import biz.firstlook.model.imt.Member;

public class MemberComparator implements Comparator<Member> {

    public int compare(Member m1, Member m2) {

	int compare = m1.getLastName().compareTo(m2.getLastName());

	if (compare == 0) {
	    compare = m1.getFirstName().compareTo(m2.getFirstName());
	}

	if (compare == 0) {
	    compare = m1.getId().compareTo(m2.getId());
	}

	return compare;

    }

}
