package biz.firstlook.model.imt;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Franchise")
public class Franchise {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "FranchiseID")
	private Integer id;

	@Column(name = "FranchiseDescription")
	private String description;

	@ManyToMany(
			targetEntity = Dealer.class
	)
	@JoinTable(
			name = "DealerFranchise", 
			joinColumns = { @JoinColumn(name = "FranchiseID") }, 
			inverseJoinColumns = { @JoinColumn(name = "BusinessUnitID") }
	)
	private Set<Dealer> dealers;

	public Franchise() {
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Dealer> getDealers() {
		return dealers;
	}

	public void setDealers(Set<Dealer> dealers) {
		this.dealers = dealers;
	}

	@Override
	public boolean equals(Object obj) {
		if( obj instanceof Franchise) {
			getId().equals( ((Franchise) obj).getId() );
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getId().hashCode();
	}

}
