package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PhotoProviderDealershipPK implements Serializable{

	private static final long serialVersionUID = -3495891254926529027L;
	 
	private Integer photoProviderId;
    private Integer businessUnitId;
    
    public PhotoProviderDealershipPK(){}
    
    public Integer getBusinessUnitId() {
        return businessUnitId;
    }
    public void setBusinessUnitId(Integer businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public Integer getPhotoProviderId() {
		return photoProviderId;
	}

	public void setPhotoProviderId(Integer photoProviderId) {
		this.photoProviderId = photoProviderId;
	}

    @Override
    public boolean equals(Object o) {
        if( o instanceof PhotoProviderDealershipPK ) {
        	PhotoProviderDealershipPK pk = (PhotoProviderDealershipPK) o;
            if( pk.getBusinessUnitId().equals( this.getBusinessUnitId() ) &&
                pk.getPhotoProviderId().equals( this.getPhotoProviderId() ) ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = this.getBusinessUnitId().hashCode() << 4;
        hash += this.getPhotoProviderId().hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return "PhotoProviderDealershipPK: " + getBusinessUnitId() + ", " + getPhotoProviderId();
    }
}

