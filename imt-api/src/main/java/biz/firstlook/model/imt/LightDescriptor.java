package biz.firstlook.model.imt;

public class LightDescriptor
{

public final static int POWERZONE = 1;
public final static int WINNERS = 2;
public final static int GOOD_BETS = 3;
public final static int MARKET_PERFORMER_OR_OTHER_GREEN = 4;
public final static int MARKET_PERFORMER_OR_MANAGERS_CHOICE = 5;
public final static int MANAGERS_CHOICE = 6;

public static int RED_LIGHT = 1;
public static int YELLOW_LIGHT = 2;
public static int GREEN_LIGHT = 3;

public static LightDescriptor RED_LIGHT_MC = new LightDescriptor(
        1, RED_LIGHT, MANAGERS_CHOICE, "RED-MANAGER'S-CHOICE");
public static LightDescriptor YELLOW_LIGHT_MC_MP = new LightDescriptor(
        2, YELLOW_LIGHT, MARKET_PERFORMER_OR_MANAGERS_CHOICE,
        "YELLOW-MANAGER'S-CHOICE");
public static LightDescriptor YELLOW_LIGHT_POWERZONE = new LightDescriptor(
        3, YELLOW_LIGHT, POWERZONE, "YELLOW-POWERZONE");
public static LightDescriptor GREEN_LIGHT_MP_OG = new LightDescriptor(
        4, GREEN_LIGHT, MARKET_PERFORMER_OR_OTHER_GREEN,
        "GREEN-MANAGER'S-CHOICE-OTHER-GREEN");
public static LightDescriptor GREEN_LIGHT_GOOD_BETS = new LightDescriptor(
        5, GREEN_LIGHT, GOOD_BETS, "GREEN-GOOD-BETS");
public static LightDescriptor GREEN_LIGHT_WINNER = new LightDescriptor(
        6, GREEN_LIGHT, WINNERS, "GREEN-WINNER");
public static LightDescriptor GREEN_LIGHT_POWERZONE = new LightDescriptor(
        7, GREEN_LIGHT, POWERZONE, "GREEN-POWERZONE");

private int id;
private int ciaTypeValue;
private int lightValue;
private String description;

public LightDescriptor()
{
}

public LightDescriptor( int id, String description )
{
    setId(id);
    setDescription(description);
}

public LightDescriptor( int id, int lightValue, int ciaType,
        String description )
{
    setId(id);
    setLightValue(lightValue);
    setCiaTypeValue(ciaType);
    setDescription(description);
}

public String getDescription()
{
    return description;
}

public int getLightValue()
{
    return lightValue;
}

public void setDescription( String string )
{
    description = string;
}

public int getCiaTypeValue()
{
    return ciaTypeValue;
}

public int getId()
{
    return id;
}

public void setId( int i )
{
    id = i;
}

public void setCiaTypeValue( int i )
{
    ciaTypeValue = i;
}

public void setLightValue( int i )
{
    lightValue = i;
}

}
