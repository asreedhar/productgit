package biz.firstlook.model.imt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.codec.binary.Base64;

@Entity
@Table(name = "MemberCredential")
public class MemberCredential {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MemberCredentialID")
	private Integer id;
	
	private Integer memberId;
	
	private Integer credentialTypeId;
	private String username;
	private String password;
	
	public Integer getCredentialTypeId() {
		return credentialTypeId;
	}

	public void setCredentialTypeId(Integer credentialTypeId) {
		this.credentialTypeId = credentialTypeId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPassword() {
		if ( password == null )
		{
			return null;
		}
		return new String(Base64.decodeBase64(password.getBytes()));
	}

	public void setPassword(String password) {
		if (password != null) {
			this.password = new String(Base64.encodeBase64(password.getBytes()));
		} else 
		{
			this.password = null;
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}



}
