package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.common.model.Identifiable;

@Entity
@Table(name="BusinessUnitRelationship")
public class BusinessUnitRelationship implements Serializable, Identifiable<Integer> {

	private static final long serialVersionUID = 8056691789542068307L;

	@Id
//	@GenericGenerator(name="mine", strategy="asisgned")
	@Column(name="BusinessUnitId")
	private Integer id;
	
	private Integer parentId;

	
	public BusinessUnitRelationship() {}
	
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
