package biz.firstlook.model.imt.ref;

public enum ProgramType {
	NONE,
	VIP,
	INSIGHT,
	DEALERS_RESOURCES,
    FIRSTLOOK
}
