package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import biz.firstlook.model.imt.ref.WindowStickerBuyersGuideTemplate;
import biz.firstlook.model.imt.ref.WindowStickerTemplate;

/**
 * Class encapsulating the settings for the WindowSticker set of functionality.
 * Default windowStickerTemplate is None and buyersGuideTemplate is AS_IS_NO_WARRANTY
 * 
 * see DealerPreferenceWindowSticker.hbm.xml for O/R mapping.
 * 
 * @author bfung
 */
@Entity
@Table(schema="dbo", name="DealerPreference_WindowSticker")
public class DealerPreferenceWindowSticker implements Serializable {

	private static final long serialVersionUID = 4816073476453753620L;

	@Id
	@Column(name="BusinessUnitID")
	private Integer dealerId;
	
	@Column(name="WindowStickerTemplateId")
	private Integer windowStickerTemplateId = WindowStickerTemplate.NONE.getId();
	
	@Column(name="WindowStickerBuyersGuideTemplateId")
	private Integer buyersGuideTemplateId = WindowStickerBuyersGuideTemplate.AS_IS_NO_WARRANTY.getId();
	// should lotprice be in this class? need to find what the lotprice field represents  
	// (in admin, mapped to window sticker price)
	
	private transient boolean newEntity = false;
	
	/**
	 * Reflection purposes only.
	 */
	public DealerPreferenceWindowSticker() {
	}
	
	public DealerPreferenceWindowSticker(Integer dealerId) {
		this.dealerId = dealerId;
	}
	
	public WindowStickerTemplate getWindowStickerTemplate() {
		return WindowStickerTemplate.getById(windowStickerTemplateId);
	}
	
	public void setWindowStickerTemplate(WindowStickerTemplate template) {
		setWindowStickerTemplateId(template.getId());
	}
	
	public WindowStickerBuyersGuideTemplate getBuyersGuideTemplate() {
		return WindowStickerBuyersGuideTemplate.getById(buyersGuideTemplateId);
	}
	
	public void setBuyersGuideTemplate(WindowStickerBuyersGuideTemplate template) {
		setBuyersGuideTemplateId(template.getId());
	}
	
	/*
	 * Setters below used mainly for O/R mapping, using reflection.
	 */
	
	public Integer getDealerId() {
		return dealerId;
	}

	public void setDealerId(Integer dealerId) {
		this.dealerId = dealerId;
	}

	public Integer getWindowStickerTemplateId() {
		return windowStickerTemplateId;
	}

	public void setWindowStickerTemplateId(Integer windowStickerTemplateId) {
		this.windowStickerTemplateId = windowStickerTemplateId;
	}
	
	public Integer getBuyersGuideTemplateId() {
		return buyersGuideTemplateId;
	}

	public void setBuyersGuideTemplateId(Integer buyersGuideTemplateId) {
		this.buyersGuideTemplateId = buyersGuideTemplateId;
	}

	public boolean isNewEntity() {
		return newEntity;
	}

	public void setNewEntity(boolean newEntity) {
		this.newEntity = newEntity;
	}
}
