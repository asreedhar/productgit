package biz.firstlook.model.imt.ref;

public interface DatabaseEnum {
    Integer getId();
    String getDescription();
}
