package biz.firstlook.model.imt.ref;


public enum MemberType {
	NONE,
	ADMIN,
	USER,
	ACCOUNT_REP,
	SALESPERSON;
	
}
