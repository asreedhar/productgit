package biz.firstlook.model.imt;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="dbo", name="DealerTransferPriceUnitCostRule")
public class DealerTransferPriceUnitCostRule implements Serializable {
	
	private static final long serialVersionUID = 3200954981508222256L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="DealerTransferPriceUnitCostRuleID")
	private Integer id;
	
	@Column(name="BusinessUnitID")
	private Integer businessUnitId;
	
	@ManyToOne
	@JoinColumn(name="TransferPriceInventoryAgeRangeID")
	private TransferPriceInventoryAgeRange transferPriceInventoryAgeRange;
	
	@ManyToOne
	@JoinColumn(name="TransferPriceUnitCostValueID")
	private TransferPriceUnitCostValue transferPriceUnitCostValue;

	/**
	 * For hibernate reflection uses only.
	 */
	public DealerTransferPriceUnitCostRule() {
		super();
	}
	
	public DealerTransferPriceUnitCostRule(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	
	public Integer getId() {
		return id;
	}
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public TransferPriceInventoryAgeRange getTransferPriceInventoryAgeRange() {
		return transferPriceInventoryAgeRange;
	}
	
	public void setTransferPriceInventoryAgeRange(
			TransferPriceInventoryAgeRange transferPriceInventoryAgeRange) {
		this.transferPriceInventoryAgeRange = transferPriceInventoryAgeRange;
	}

	public TransferPriceUnitCostValue getTransferPriceUnitCostValue() {
		return transferPriceUnitCostValue;
	}

	public void setTransferPriceUnitCostValue(
			TransferPriceUnitCostValue transferPriceUnitCostValue) {
		this.transferPriceUnitCostValue = transferPriceUnitCostValue;
	}
}
