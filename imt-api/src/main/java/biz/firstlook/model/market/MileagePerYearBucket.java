package biz.firstlook.model.market;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Pricing.MileagePerYearBucket")

public class MileagePerYearBucket {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MileagePerYearBucketID")
	private Integer id;
	@Column(name = "LowMileage")
	private Integer lowMileage;
	@Column(name = "HighMileage")
	private Integer highMileage;
	public Integer getHighMileage() {
		return highMileage;
	}
	public void setHighMileage(Integer highMileage) {
		this.highMileage = highMileage;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getLowMileage() {
		return lowMileage;
	}
	public void setLowMileage(Integer lowMileage) {
		this.lowMileage = lowMileage;
	}
	
}
