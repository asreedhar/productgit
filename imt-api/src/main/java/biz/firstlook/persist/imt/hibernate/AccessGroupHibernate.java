package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.AccessGroup;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.AccessGroupDao;

public class AccessGroupHibernate extends GenericHibernate<AccessGroup,Integer> implements AccessGroupDao{

    public AccessGroupHibernate() {
        super();
    }

}
