package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DMSExportPriceMapping;
import biz.firstlook.persist.common.GenericDAO;

public interface DMSExportPriceMappingDao extends GenericDAO<DMSExportPriceMapping, Integer> {

}
