package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.DealerUpgradePK;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerUpgradeDao;

public class DealerUpgradeHibernate extends GenericHibernate<DealerUpgrade,DealerUpgradePK> implements
		DealerUpgradeDao {

	public DealerUpgradeHibernate() {
		super();
	}
	
	public DealerUpgrade findByBusinessUnitAndUpgradeType(final Integer businessUnitId, final Integer upgradeCode)
	{
		DealerUpgrade upgrade = (DealerUpgrade) getHibernateTemplate().execute( new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( DealerUpgrade.class )
				    .add( Restrictions.eq( "businessUnitId", businessUnitId) )
				    .add( Restrictions.eq( "dealerUpgradeCD", upgradeCode ) );
				return crit.uniqueResult();
			}
		});
		
		return upgrade;
	}
	
}
