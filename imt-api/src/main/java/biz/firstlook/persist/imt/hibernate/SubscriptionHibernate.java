package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.Subscription;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SubscriptionDao;

public class SubscriptionHibernate extends GenericHibernate<Subscription,Integer> implements
		SubscriptionDao {

	public SubscriptionHibernate() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Subscription> findByMemberIdAndSubscriptionTypeId(final Integer memberId, final Integer subscriptionTypeId) {
		List<Subscription> subscriptions = (List<Subscription>) getHibernateTemplate().execute( new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( Subscription.class )
				    .add( Restrictions.eq( "subscriberId", memberId) )
				    .createCriteria("subscriptionType")
				    	.add( Restrictions.eq( "id", subscriptionTypeId ) );
				return new ArrayList<Subscription>( crit.list() );
			}
		});
		return subscriptions;
	}

	@SuppressWarnings("unchecked")
	public List<Subscription> findBy(final Integer subscriptionTypeId, final Integer detailId, final Integer subscriberTypeId) {
		List<Subscription> subscriptions = (List<Subscription>) getHibernateTemplate().execute( new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( Subscription.class );
				crit.createCriteria("subscriptionType")
				    	.add( Restrictions.eq( "id", subscriptionTypeId ) );
				crit.createCriteria("subscriberType")
		    	.add( Restrictions.eq( "id", subscriberTypeId ) );
				crit.createCriteria("subscriptionFrequencyDetail")
		    	.add( Restrictions.eq( "id", detailId ) );
				return new ArrayList<Subscription>( crit.list() );
			}
		});
		return subscriptions;
	}

}
