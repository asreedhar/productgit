package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.SubscriptionFrequencyDetail;
import biz.firstlook.persist.common.GenericDAO;

public interface SubscriptionFrequencyDetailDao extends GenericDAO<SubscriptionFrequencyDetail, Integer> {
	

}
