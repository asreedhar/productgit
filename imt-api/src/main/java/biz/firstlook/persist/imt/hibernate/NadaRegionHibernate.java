package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.ref.NadaRegion;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.NadaRegionDao;

public class NadaRegionHibernate extends GenericHibernate<NadaRegion,Integer> implements NadaRegionDao {

	public NadaRegionHibernate() {
		super();
	}
	
}
