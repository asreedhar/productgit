package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.ref.DatabaseEnum;

public interface NaaaAuctionDao {
    List<DatabaseEnum> getTimePeriods();
    
    List<DatabaseEnum> getRegions();
}
