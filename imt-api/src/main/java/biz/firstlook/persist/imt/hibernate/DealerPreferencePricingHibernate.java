package biz.firstlook.persist.imt.hibernate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import biz.firstlook.model.imt.DealerPreferencePricing;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerPreferencePricingDao;

public class DealerPreferencePricingHibernate extends
		GenericHibernate<DealerPreferencePricing, Integer> implements
		DealerPreferencePricingDao {

	// parent dealership for all
	private static final int FIRSTLOOK_BUSINESS_UNIT = 100150;

	public DealerPreferencePricingHibernate() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	public DealerPreferencePricing findByDealerId(Integer dealerId) {
		DealerPreferencePricing result = null;
		List<DealerPreferencePricing> results = getHibernateTemplate().find( "from biz.firstlook.model.imt.DealerPreferencePricing dpp where dpp.id = ?",
                new Object[] { dealerId } );
		if (results != null && !results.isEmpty()) {
			result = results.get(0);
		}
		return result;
	}
	
	public DealerPreferencePricing findDefaultSystemDealerPreference() {
		DealerPreferencePricing systemPref = new DealerPreferencePricing();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = getSession().connection().prepareStatement("select * from DealerPreference_Pricing where businessUnitId = " + FIRSTLOOK_BUSINESS_UNIT);
			rs = ps.executeQuery();
			rs.next();
			systemPref.setPingII_ExcludeHighPriceOutliersMultiplier(rs.getDouble("PingII_ExcludeHighPriceOutliersMultiplier"));
			systemPref.setPingII_ExcludeLowPriceOutliersMultiplier(rs.getDouble("PingII_ExcludeLowPriceOutliersMultiplier"));
			systemPref.setPingII_ExcludeNoPriceFromCalc(rs.getBoolean("PingII_ExcludeNoPriceFromCalc"));
			systemPref.setPingII_DefaultSearchRadius(rs.getInt("PingII_DefaultSearchRadius"));
			systemPref.setPingII_MaxSearchRadius(rs.getInt("PingII_MaxSearchRadius"));
			systemPref.setPingII_SupressSellerName(rs.getBoolean("PingII_SupressSellerName"));
			systemPref.setPingII_PackageId(rs.getInt("PackageID"));
			systemPref.setPingII_MatchCertifiedByDefault(rs.getBoolean("MatchCertifiedByDefault"));
			systemPref.setPingII_MatchCertifiedByDefault(rs.getBoolean("SuppressTrimMatchStatus"));
			systemPref.setPingII_NewPing(new Boolean(rs.getBoolean("IsNewPing")));
			systemPref.setPingII_MarketListing(rs.getString("PingII_MarketListing"));
			systemPref.setPingII_RedRange1Value1(rs.getInt("PingII_RedRange1Value1"));
			systemPref.setPingII_RedRange1Value2(rs.getInt("PingII_RedRange1Value2"));
			systemPref.setPingII_YellowRange1Value1(rs.getInt("PingII_YellowRange1Value1"));
			systemPref.setPingII_YellowRange1Value2(rs.getInt("PingII_YellowRange1Value2"));
			systemPref.setPingII_YellowRange2Value1(rs.getInt("PingII_YellowRange2Value1"));
			systemPref.setPingII_YellowRange2Value2(rs.getInt("PingII_YellowRange2Value2"));
			systemPref.setPingII_GreenRange1Value1(rs.getInt("PingII_GreenRange1Value1"));
			systemPref.setPingII_GreenRange1Value2(rs.getInt("PingII_GreenRange1Value2"));
		} catch (Exception e) {
			e.printStackTrace();
			systemPref.setPingII_ExcludeHighPriceOutliersMultiplier(5.0d);
			systemPref.setPingII_ExcludeLowPriceOutliersMultiplier(.25d);
			systemPref.setPingII_ExcludeNoPriceFromCalc(true);
			systemPref.setPingII_MaxSearchRadius(25);
			systemPref.setPingII_DefaultSearchRadius(150);
			systemPref.setPingII_SupressSellerName(true);
			systemPref.setPingII_PackageId(1);
			systemPref.setPingII_MatchCertifiedByDefault(false);
			systemPref.setPingII_SuppressTrimMatchStatus(false);
			systemPref.setPingII_NewPing(false);
			systemPref.setPingII_RedRange1Value1(108);
			systemPref.setPingII_RedRange1Value2(92);
			systemPref.setPingII_YellowRange1Value1(104);
			systemPref.setPingII_YellowRange1Value2(107);
			systemPref.setPingII_YellowRange2Value1(92);
			systemPref.setPingII_YellowRange2Value2(95);
			systemPref.setPingII_GreenRange1Value1(96);
			systemPref.setPingII_GreenRange1Value2(103);
			
		} finally {
			if (rs != null) {
				try{
					rs.close();
					ps.close();
				}catch(Exception e){ e.printStackTrace();}
			}
		}
		return systemPref;
	}

}
