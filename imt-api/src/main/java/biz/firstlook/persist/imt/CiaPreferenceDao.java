package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.CiaPreference;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.common.GenericDAO;

public interface CiaPreferenceDao extends GenericDAO<CiaPreference,Integer> {

	public CiaPreference create(Dealer dealer);
}
