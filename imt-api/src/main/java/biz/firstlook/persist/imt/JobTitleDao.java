package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.JobTitle;
import biz.firstlook.persist.common.GenericDAO;

public interface JobTitleDao extends GenericDAO<JobTitle,Integer> {
}
