package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.BusinessUnitCredential;
import biz.firstlook.persist.common.GenericDAO;

public interface BusinessUnitCredentialDao extends GenericDAO<BusinessUnitCredential,Integer> {
	public List<BusinessUnitCredential> findByBusinessUnitId(Integer id);
}
