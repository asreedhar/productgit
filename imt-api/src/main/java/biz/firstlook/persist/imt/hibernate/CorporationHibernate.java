package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.Corporation;
import biz.firstlook.persist.imt.CorporationDao;

public class CorporationHibernate extends BusinessUnitHibernate<Corporation>
	implements CorporationDao {

    public List<Corporation> search(String searchStr) {
	Criterion criterion = Restrictions.ilike("name", searchStr,
		MatchMode.ANYWHERE);
	return findByCriteria(criterion);
    }
}
