package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.Member;
import biz.firstlook.persist.common.SearchableDAO;

public interface MemberDao extends SearchableDAO<Member,Integer> {
	public List<Member> search( String searchStr );
	public Member byLogin(String login);
	public List<Member> findLithiaCarCenterMembers();
}
