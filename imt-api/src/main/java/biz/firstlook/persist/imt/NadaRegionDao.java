package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.ref.NadaRegion;
import biz.firstlook.persist.common.GenericDAO;

public interface NadaRegionDao extends GenericDAO<NadaRegion, Integer> {

}