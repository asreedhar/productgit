package biz.firstlook.persist.imt.direct.jdbc;


import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.persist.imt.direct.PingIIDirect;

public class PingIIDirectImpl extends JdbcTemplate implements PingIIDirect{

	private int queryTimeout = 90;
	private static final String CALL_STRING = "{call pricing.SetupDefaultDealerSearch( ? )}";
	
	public void setupDefaultDealerSearch(Integer businessUnitId) {
		if(businessUnitId != null){
			// build parameter list
			List<Object> parameterList = new ArrayList<Object>();
			parameterList.add(new SqlParameter("@DealerID", java.sql.Types.INTEGER));
			// build parameter map
			Map<String,Object> parameterMap = new HashMap<String,Object>();
			parameterMap.put("@DealerID", businessUnitId);
			
			// prepare SQL
			CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory(
					CALL_STRING,
					parameterList);
			CallableStatementCreator csc = cscf.newCallableStatementCreator(
					parameterMap);
			
			execute(csc, new SetupDefaultDealerSearchCallback());
		}
	}	

	public void setQueryTimeout(int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}
	
private class SetupDefaultDealerSearchCallback implements CallableStatementCallback {

	public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
		cs.setQueryTimeout(queryTimeout);
		return cs.execute();
	}
}





	
}
