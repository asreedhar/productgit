package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.Target;
import biz.firstlook.persist.common.GenericDAO;

public interface TargetDao extends GenericDAO<Target,Integer> {	
}
