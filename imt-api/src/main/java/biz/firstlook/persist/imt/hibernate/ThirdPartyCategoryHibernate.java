package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.ThirdPartyCategory;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.ThirdPartyCategoryDao;

public class ThirdPartyCategoryHibernate extends GenericHibernate<ThirdPartyCategory,Integer> implements
		ThirdPartyCategoryDao {

	public ThirdPartyCategoryHibernate() {
		super();
	}

}
