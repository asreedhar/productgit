package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.AccessGroup;
import biz.firstlook.persist.common.GenericDAO;

public interface AccessGroupDao extends GenericDAO<AccessGroup,Integer>{

}
