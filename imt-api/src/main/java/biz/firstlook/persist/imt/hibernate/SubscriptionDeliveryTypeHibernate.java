
package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.SubscriptionDeliveryType;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SubscriptionDeliveryTypeDao;

public class SubscriptionDeliveryTypeHibernate extends GenericHibernate<SubscriptionDeliveryType,Integer> implements
		SubscriptionDeliveryTypeDao {

	public SubscriptionDeliveryTypeHibernate() {
		super();
	}

}
