package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.MemberCredential;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.MemberCredentialDao;

public class MemberCredentialHibernate extends GenericHibernate<MemberCredential,Integer> implements
		MemberCredentialDao {

	public MemberCredentialHibernate() {
		super();
	}

	public List<MemberCredential> findByMemberId(Integer id) {
		Criterion criterion = Restrictions.eq( "memberId", id);
		return findByCriteria(criterion);
	}

}
