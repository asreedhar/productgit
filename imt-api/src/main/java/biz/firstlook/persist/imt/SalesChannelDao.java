package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.SalesChannel;
import biz.firstlook.persist.common.GenericDAO;

public interface SalesChannelDao extends GenericDAO<SalesChannel, Integer> {
	
}
