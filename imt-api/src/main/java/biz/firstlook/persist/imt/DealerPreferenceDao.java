package biz.firstlook.persist.imt;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreference;
import biz.firstlook.model.imt.DealerPreferenceWindowSticker;
import biz.firstlook.persist.common.GenericDAO;


public interface DealerPreferenceDao extends GenericDAO<DealerPreference,Integer>
{

	public DealerPreference create(Dealer dealer);
	public DealerPreference findByDealerId(Integer dealerId);
	public void populateNadaRegionCode(Dealer dealer,DealerPreference preference);
	
	public DealerPreferenceWindowSticker findWindowStickerPrefs(Integer dealerId);
	
	@Transactional(rollbackFor=Exception.class)
	public void save(DealerPreferenceWindowSticker dealerPreferenceWindowSticker);
}
