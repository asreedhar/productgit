package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerUpgradeCode;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerUpgradeCodeDao extends GenericDAO<DealerUpgradeCode,Integer> {
}
