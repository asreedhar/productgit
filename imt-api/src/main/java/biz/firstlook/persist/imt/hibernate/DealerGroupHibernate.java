package biz.firstlook.persist.imt.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.BusinessUnitRelationship;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerGroup;
import biz.firstlook.model.imt.DealerGroupPreference;
import biz.firstlook.persist.imt.BusinessUnitRelationshipDao;
import biz.firstlook.persist.imt.DealerGroupDao;
import biz.firstlook.persist.imt.DealerGroupPreferenceDao;
import biz.firstlook.persist.imt.direct.jdbc.DealerTransferPriceCreateStoredProcedure;

public class DealerGroupHibernate extends BusinessUnitHibernate<DealerGroup>
		implements DealerGroupDao {

	private static final int FIRSTLOOK_BUSINESSUNIT_ID = 100150;

	private DealerGroupPreferenceDao dealerGroupPreferenceDao;
	private BusinessUnitRelationshipDao businessUnitRelationshipDao;
	private DealerTransferPriceCreateStoredProcedure dealerTransferPriceCreateStoreProcedure;

	public List<DealerGroup> search(String searchStr) {
		Criterion criterion = Restrictions.ilike("name", searchStr,
				MatchMode.ANYWHERE);
		return findByCriteria(criterion);
	}

	@Override
	public void postInsert(DealerGroup dealerGroup) {
		if ((dealerGroup != null) && dealerGroup.getId() != null) {
			// set up dealer group prefs
			DealerGroupPreference pref = new DealerGroupPreference();
			pref.setGroup(dealerGroup);
			dealerGroup.setPreference(pref);
			dealerGroupPreferenceDao.makePersistent(pref);

			// set up a parent relationship with firstlook.
			// required for all dealer groups
			BusinessUnitRelationship parentRelationshipWithFirstLook = new BusinessUnitRelationship();
			parentRelationshipWithFirstLook.setId(dealerGroup.getId());
			parentRelationshipWithFirstLook.setParentId(FIRSTLOOK_BUSINESSUNIT_ID);
			businessUnitRelationshipDao	.makePersistent(parentRelationshipWithFirstLook);
			
			processTransferPricePreference(dealerGroup);
		}
	}
	
	@Override
	public void postUpdate(DealerGroup dealerGroup) {
		if(dealerGroup != null && dealerGroup.getId() != null) {
			processTransferPricePreference(dealerGroup);
		}
	}
	
	/**
	 * This really belongs on DealerGroup, when a Dealer is added to the DealerGroup.
	 * 
	 * This method is an ugly hack to cascade save the Dealers since we won't have DealerIDs 
	 * until the group is saved, hence, the method could not be on the DealerGroup class.
	 * 
	 * @param dealerGroup
	 */
	private void processTransferPricePreference(DealerGroup dealerGroup) {
		DealerGroupPreference pref = dealerGroup.getPreference();
		if(pref != null && pref.getIncludeTransferPricing()) {
			List<Dealer> dealers = new ArrayList<Dealer>(dealerGroup.getDealers());
			
			for(Dealer dealer : dealers) {
				if(	dealer.getDealerTransferAllowRetailOnlyRules().isEmpty() && 
					dealer.getDealerTransferPriceUnitCostRules().isEmpty()) {
					
					dealerTransferPriceCreateStoreProcedure.execute(dealer);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public boolean hasTransferPrice(Integer dealerGroupId) {
		List includeTransferPrices = getHibernateTemplate()
				.find(
						"select g.preference.includeTransferPricing from biz.firstlook.model.imt.DealerGroup g "
								+ "where g.id = ?", dealerGroupId);

		if (includeTransferPrices != null && !includeTransferPrices.isEmpty()) {
			return (Boolean) includeTransferPrices.get(0);
		} else {
			return false;
		}
	}

	public DealerGroupPreferenceDao getDealerGroupPreferenceDao() {
		return dealerGroupPreferenceDao;
	}

	public void setDealerGroupPreferenceDao(
			DealerGroupPreferenceDao dealerGroupPreferenceDao) {
		this.dealerGroupPreferenceDao = dealerGroupPreferenceDao;
	}

	public BusinessUnitRelationshipDao getBusinessUnitRelationshipDao() {
		return businessUnitRelationshipDao;
	}

	public void setBusinessUnitRelationshipDao(
			BusinessUnitRelationshipDao businessUnitRelationshipDao) {
		this.businessUnitRelationshipDao = businessUnitRelationshipDao;
	}

	public void setDealerTransferPriceCreateStoreProcedure(
			DealerTransferPriceCreateStoredProcedure dealerTransferPriceCreateStoreProcedure) {
		this.dealerTransferPriceCreateStoreProcedure = dealerTransferPriceCreateStoreProcedure;
	}
}
