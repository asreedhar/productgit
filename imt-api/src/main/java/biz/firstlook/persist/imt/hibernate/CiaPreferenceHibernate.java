package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.CiaPreference;
import biz.firstlook.model.imt.Dealer;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.CiaPreferenceDao;

public class CiaPreferenceHibernate extends GenericHibernate<CiaPreference,Integer> implements
		CiaPreferenceDao {

	public CiaPreferenceHibernate() {
		super();
	}

	public CiaPreference create(Dealer dealer) {
		assert( dealer != null );
		CiaPreference preference = new CiaPreference();
		preference.setId(dealer.getId());
		makePersistent( preference );
		return preference;
	}
}
