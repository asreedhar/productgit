package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.PhotoProviderDealership;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.PhotoProviderDealershipDao;


public class PhotoProviderDealershipHibernate extends GenericHibernate<PhotoProviderDealership, Integer> implements
        PhotoProviderDealershipDao {

    public PhotoProviderDealershipHibernate() {
        super();

    }

	public List<PhotoProviderDealership> findByBusinessUnitId(final int businessUnitId) {
		@SuppressWarnings("unchecked")
		List<PhotoProviderDealership> pd = (List<PhotoProviderDealership>) getHibernateTemplate()
				.execute(new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						Criteria crit = session.createCriteria(
								PhotoProviderDealership.class).add(
								Restrictions.eq("businessUnitId",businessUnitId));
						return crit.list();
					}
				});

		return pd;
	}

}
