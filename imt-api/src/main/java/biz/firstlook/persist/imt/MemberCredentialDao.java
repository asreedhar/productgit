package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.MemberCredential;
import biz.firstlook.persist.common.GenericDAO;

public interface MemberCredentialDao extends GenericDAO<MemberCredential, Integer> {
	public List<MemberCredential> findByMemberId(Integer id);
}
