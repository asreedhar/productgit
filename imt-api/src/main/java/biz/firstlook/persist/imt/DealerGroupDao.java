package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerGroup;

public interface DealerGroupDao extends BusinessUnitDao<DealerGroup> {
	
	boolean hasTransferPrice(Integer dealerGroupId);
}
