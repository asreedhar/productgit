package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.model.imt.ref.DatabaseEnum;
import biz.firstlook.persist.imt.NaaaAuctionDao;

public class NaaaAuctionHibernate extends HibernateDaoSupport implements NaaaAuctionDao {

    @SuppressWarnings("unchecked")
    public List<DatabaseEnum> getRegions() {
	return getHibernateTemplate().find("FROM NaaaRegion");
    }

    @SuppressWarnings("unchecked")
    public List<DatabaseEnum> getTimePeriods() {
	return getHibernateTemplate().find("FROM NaaaTimePeriod ORDER BY rank");
    }
}
