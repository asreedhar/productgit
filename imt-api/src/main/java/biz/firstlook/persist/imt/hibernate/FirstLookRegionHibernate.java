package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.FirstLookRegion;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.FirstLookRegionDao;

public class FirstLookRegionHibernate extends GenericHibernate<FirstLookRegion,Integer> implements
		FirstLookRegionDao {

	public FirstLookRegionHibernate() {
		super();
	}

}
