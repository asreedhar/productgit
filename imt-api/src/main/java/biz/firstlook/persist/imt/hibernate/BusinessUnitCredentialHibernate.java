package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.BusinessUnitCredential;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.BusinessUnitCredentialDao;

public class BusinessUnitCredentialHibernate extends GenericHibernate<BusinessUnitCredential,Integer> implements
		BusinessUnitCredentialDao {
 
	public BusinessUnitCredentialHibernate() {
		super();
	}

	@SuppressWarnings("unchecked")
    public List<BusinessUnitCredential> findByBusinessUnitId(final Integer id) {
        List<BusinessUnitCredential> businessUnitCredential = (List<BusinessUnitCredential>) getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria crit = session.createCriteria( BusinessUnitCredential.class)
                    .add( Restrictions.eq("businessUnitId", id))
                    .addOrder(Order.desc("updatedDate"));
                return new ArrayList<BusinessUnitCredential>( crit.list() );  
            }
        });
        return businessUnitCredential;
    }
}
