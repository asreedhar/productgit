package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.PhotoProvider;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.PhotoProviderDao;

public class PhotoProviderHibernate extends GenericHibernate<PhotoProvider, Integer> implements
		PhotoProviderDao {

	PhotoProviderHibernate(){
		super();
	}

}
