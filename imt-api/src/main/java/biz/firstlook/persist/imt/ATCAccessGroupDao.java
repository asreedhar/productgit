package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.ATCAccessGroup;
import biz.firstlook.persist.common.GenericDAO;

public interface ATCAccessGroupDao extends GenericDAO<ATCAccessGroup,Integer>{

    List<ATCAccessGroup> findByBusinessUnitId(Integer businessUnitId); 
}
