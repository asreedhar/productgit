package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.TransferPriceUnitCostValue;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.TransferPriceUnitCostValueDao;

public class TransferPriceUnitCostValueHibernate extends GenericHibernate<TransferPriceUnitCostValue, Integer>
		implements TransferPriceUnitCostValueDao {

}
