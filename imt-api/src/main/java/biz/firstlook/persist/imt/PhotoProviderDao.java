package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.PhotoProvider;
import biz.firstlook.persist.common.GenericDAO;

public interface PhotoProviderDao extends GenericDAO<PhotoProvider, Integer> {

}
