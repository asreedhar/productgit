package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.FirstLookRegion;
import biz.firstlook.persist.common.GenericDAO;

public interface FirstLookRegionDao extends GenericDAO<FirstLookRegion,Integer> {
}
