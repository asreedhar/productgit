package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.DealerPreferenceKBBConsumerTool;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerPreferenceKBBConsumerToolDao;

public class DealerPreferenceKBBConsumerToolHibernate extends
GenericHibernate<DealerPreferenceKBBConsumerTool, Integer> implements
DealerPreferenceKBBConsumerToolDao{

}
