package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import biz.firstlook.model.imt.SubscriptionFrequencyDetail;
import biz.firstlook.model.imt.SubscriptionType;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SubscriptionTypeDao;

public class SubscriptionTypeHibernate extends GenericHibernate<SubscriptionType,Integer> implements
		SubscriptionTypeDao {

	public SubscriptionTypeHibernate() {
		super();
	}

	public List<SubscriptionFrequencyDetail> getSubscriptionFrequenciesForSubscriptionType(Integer subscriptionTypeId) {
		SubscriptionType type = super.findById(subscriptionTypeId);
		return type.getSubscriptionFrequencyDetails();
	}

}
