package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.Franchise;
import biz.firstlook.persist.common.GenericDAO;

public interface FranchiseDao extends GenericDAO<Franchise,Integer> {
}
