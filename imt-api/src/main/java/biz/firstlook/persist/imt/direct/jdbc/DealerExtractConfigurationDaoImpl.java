package biz.firstlook.persist.imt.direct.jdbc;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.model.imt.DealerExtractConfiguration;
import biz.firstlook.persist.imt.DealerExtractConfigurationDao;

public class DealerExtractConfigurationDaoImpl implements
		DealerExtractConfigurationDao {
	
	private DataSource dataSource;

	@SuppressWarnings("unchecked")
	public List<DealerExtractConfiguration> findBy(Integer dealerId) {
		CallableStatementCreatorFactory factory = new CallableStatementCreatorFactory("{call Extract.DealerConfigurationCollection#Fetch (?)}");
		factory.addParameter(new SqlParameter("DealerID", Types.INTEGER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("DealerID", dealerId);
		
		CallableStatementCreator csc = factory.newCallableStatementCreator(params);
		
		JdbcTemplate template = new JdbcTemplate(dataSource);
		
		List<DealerExtractConfiguration> configs = 
			(List<DealerExtractConfiguration>)template.execute(csc, new CallableStatementCallback(){
			public Object doInCallableStatement(CallableStatement cs)
					throws SQLException, DataAccessException {
				ResultSet rs = cs.executeQuery();
				List<DealerExtractConfiguration> configs = new ArrayList<DealerExtractConfiguration>();
				while(rs.next()) {
					configs.add(map(rs));
				}
				return configs;
			}				
		});
		
		return configs;
	}

	public DealerExtractConfiguration findBy(Integer dealerId, String destinationName) {
		List<DealerExtractConfiguration> configs = findBy(dealerId);
		DealerExtractConfiguration theOne = null;
		for(DealerExtractConfiguration config : configs) {
			if(config.getDestinationName().equals(destinationName)) {
				theOne = config;
				break;
			}
		}
			
		return theOne;
	}

	@SuppressWarnings("unchecked")
	public List<String> findDestinations() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<String> destinations = (List<String>)
			jdbcTemplate.query("SELECT [Name] FROM Extract.Destination ORDER BY [Name]", new RowMapper(){
			public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
				return rs.getString("Name");
			}
		});
		
		return destinations;
	}

	public DealerExtractConfiguration save(DealerExtractConfiguration config) {
		Integer dealerId = config.getDealerId();  
		String destinationName = config.getDestinationName(); 
		
		if(exists(dealerId, destinationName)) {
			update(config);
		} else {
			insert(config);
		}
		return config;
	}
	
	private boolean exists(Integer dealerId, String destinationName) {
		CallableStatementCreatorFactory factory = 
			new CallableStatementCreatorFactory("{call Extract.DealerConfiguration#Exists (?, ?)}");
		
		factory.addParameter(new SqlParameter("DealerID", Types.INTEGER));
		factory.addParameter(new SqlParameter("DestinationName", Types.VARCHAR));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("DealerID", dealerId);
		params.put("DestinationName", destinationName);
		
		CallableStatementCreator csc = factory.newCallableStatementCreator(params);
		
		JdbcTemplate template = new JdbcTemplate(dataSource);
		
		Boolean exists = (Boolean)template.execute(csc, new CallableStatementCallback(){
			public Object doInCallableStatement(CallableStatement cs)
					throws SQLException, DataAccessException {
				ResultSet rs = cs.executeQuery();
				boolean exists = false;
				
				while(rs.next()) {
					exists = rs.getBoolean("Exists");
					break;
				}
				
				return exists;
			}				
		});
		
		return exists;
	}
	
	private void insert(DealerExtractConfiguration config) {
		insertOrUpdate("{call Extract.DealerConfiguration#Insert (?, ?, ?, ?, ?, ?, ?)}", config);
	}
	
	private void update(DealerExtractConfiguration config) {
		insertOrUpdate("{call Extract.DealerConfiguration#Update (?, ?, ?, ?, ?, ?, ?)}", config);
	}
	
	private void insertOrUpdate(String callString, DealerExtractConfiguration config) {
		CallableStatementCreatorFactory factory = new CallableStatementCreatorFactory(callString);

		factory.addParameter(new SqlParameter("DealerID", Types.INTEGER));
		factory.addParameter(new SqlParameter("DestinationName", Types.VARCHAR));
		factory.addParameter(new SqlParameter("ExternalIdentifier", Types.VARCHAR));
		factory.addParameter(new SqlParameter("StartDate", Types.DATE));
		factory.addParameter(new SqlParameter("EndDate", Types.DATE));
		factory.addParameter(new SqlParameter("Active", Types.BIT));
		factory.addParameter(new SqlParameter("InsertUser", Types.VARCHAR));
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("DealerID", config.getDealerId());
		params.put("DestinationName", config.getDestinationName());
		params.put("ExternalIdentifier", config.getExternalIdentifier());
		params.put("StartDate", config.getStartDate());
		params.put("EndDate", config.getEndDate());
		params.put("Active", config.isActive());
		params.put("InsertUser", config.getUpdateUser());
		
		CallableStatementCreator csc = factory.newCallableStatementCreator(params);
		
		JdbcTemplate template = new JdbcTemplate(dataSource);
		
		template.execute(csc, new CallableStatementCallback(){
			public Object doInCallableStatement(CallableStatement cs)
					throws SQLException, DataAccessException {
				int rc = cs.executeUpdate();
				return rc;
			}				
		});
	}
	
	private static DealerExtractConfiguration map(ResultSet rs) throws SQLException {
		Integer businessUnitId = rs.getInt("BusinessUnitID");
		String destinationName = rs.getString("DestinationName");
		Boolean editable = rs.getBoolean("Editable");
		if(editable == null) {
			editable = Boolean.FALSE;
		}
		
		DealerExtractConfiguration config = new DealerExtractConfiguration(
				businessUnitId, destinationName, 
				editable, rs.getString("EditRuleMessage"));
		config.setExternalIdentifier(rs.getString("ExternalIdentifier"));
		config.setStartDate(rs.getDate("StartDate"));
		config.setEndDate(rs.getDate("EndDate"));
		config.setActive(rs.getBoolean("Active"));
		config.setUpdateDate(rs.getDate("UpdateDate"));
		config.setUpdateUser(rs.getString("UpdateUser"));
		return config;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
