package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.MarketToZip;
import biz.firstlook.model.imt.MarketToZipPK;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.MarketToZipDao;

public class MarketToZipHibernate extends GenericHibernate<MarketToZip,MarketToZipPK> implements
		MarketToZipDao {

	public MarketToZipHibernate() {
		super();
	}
	
}
