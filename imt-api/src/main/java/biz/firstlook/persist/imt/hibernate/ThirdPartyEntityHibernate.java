package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import biz.firstlook.model.imt.ThirdPartyEntity;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.ThirdPartyEntityDao;

public class ThirdPartyEntityHibernate extends
        GenericHibernate<ThirdPartyEntity, Integer> implements
        ThirdPartyEntityDao {

	private static final int INTERNET_ADVERTISER_THIRD_PARTY_ENTITY_TYPE_ID = 4;
	private static final int DMS_EXPORT_THIRD_PARTY_ENTITY_TYPE_ID = 7;
	
    public ThirdPartyEntityHibernate() {
        super();
    }

	@SuppressWarnings("unchecked")
	public List<ThirdPartyEntity> findByDMSExportThirdPartyEntity() {
		return (List<ThirdPartyEntity>)getSession().createCriteria(ThirdPartyEntity.class).add( 
				Expression.eq("typeId", DMS_EXPORT_THIRD_PARTY_ENTITY_TYPE_ID)).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<ThirdPartyEntity> findAllInternetAdvertisers() {
		Criteria criteria = getSession().createCriteria(ThirdPartyEntity.class);
		criteria.add(Expression.eq("typeId", INTERNET_ADVERTISER_THIRD_PARTY_ENTITY_TYPE_ID));
		criteria.addOrder(Order.asc("name"));
		return (List<ThirdPartyEntity>)criteria.list();
	}
}
