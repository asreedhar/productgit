package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.DealerGroupPreference;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerGroupPreferenceDao;

public class DealerGroupPreferenceHibernate extends GenericHibernate<DealerGroupPreference,Integer> implements
		DealerGroupPreferenceDao {

	public DealerGroupPreferenceHibernate() {
		super();
	}


}
