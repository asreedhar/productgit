package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.Dealer;

public interface DealerDao extends BusinessUnitDao<Dealer> {
}
