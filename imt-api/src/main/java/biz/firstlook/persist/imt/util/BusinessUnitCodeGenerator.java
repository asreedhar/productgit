package biz.firstlook.persist.imt.util;

import java.text.NumberFormat;
import java.util.StringTokenizer;

public class BusinessUnitCodeGenerator {

    private static final int BUSINESSUNIT_CODEPREFIX_SIZE = 8;
    private static final int BUSINESSUNIT_CODESUFFIX_SIZE = 2; 

    public String getCodePrefix(String name) {
	StringTokenizer tokenizer = new StringTokenizer(name.toUpperCase(),
		"<>&\'\" ", false);
	StringBuffer prefix = new StringBuffer();
	while (tokenizer.hasMoreTokens()) {
	    prefix.append(tokenizer.nextToken());
	}

	String checkedPrefix;
	int length = prefix.length();
	if (length < BUSINESSUNIT_CODEPREFIX_SIZE) {
	    for (int index = 0; index < (BUSINESSUNIT_CODEPREFIX_SIZE - length); index++) {
		prefix.append("0");
	    }
	    checkedPrefix = prefix.toString();
	} else {
	    checkedPrefix = prefix.substring(0, BUSINESSUNIT_CODEPREFIX_SIZE);
	}

	return checkedPrefix;
    }

    public String getNextCodeSuffix( String lastCode ) {
	int start = lastCode.length() - BUSINESSUNIT_CODESUFFIX_SIZE;
	int end = start + BUSINESSUNIT_CODESUFFIX_SIZE;
	String codeSuffix = lastCode.substring( start, end );
	int codeSuffixNumber = 0;
	try {
	    codeSuffixNumber = Integer.parseInt(codeSuffix);
	} catch (NumberFormatException nfe) {
	    throw new RuntimeException( "Not A BusinessUnitCode!: " + lastCode, nfe);
	}

	codeSuffixNumber++;

	NumberFormat format = NumberFormat.getInstance();
	format.setMaximumIntegerDigits(BUSINESSUNIT_CODESUFFIX_SIZE);
	format.setMinimumIntegerDigits(BUSINESSUNIT_CODESUFFIX_SIZE);
	format.setGroupingUsed(false);
	String newCodeSuffix = format.format(codeSuffixNumber);

	return newCodeSuffix;
    }
}
