package biz.firstlook.persist.imt.hibernate;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.BusinessUnit;
import biz.firstlook.model.imt.JobTitle;
import biz.firstlook.model.imt.Member;
import biz.firstlook.model.imt.Role;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerGroupDao;
import biz.firstlook.persist.imt.MemberDao;

public class MemberHibernate extends GenericHibernate<Member, Integer>
		implements MemberDao {

	private DealerGroupDao dealerGroupDao;
	
	public MemberHibernate() {
		super();
	}

	public List<Member> search(String searchStr) {
		Criterion criterion = Restrictions.or(Restrictions.ilike("firstName",
				searchStr, MatchMode.ANYWHERE), Restrictions.ilike("lastName",
				searchStr, MatchMode.ANYWHERE));
		List<Member> members =  findByCriteria(criterion);
		for (Member member : members) {
			if (member.getDefaultDealerGroupId() != null && member.getDefaultDealerGroupId().intValue() > 0) {
				BusinessUnit businessUnit = (BusinessUnit)dealerGroupDao.findById(member.getDefaultDealerGroupId());
				member.setDefaultDealerGroupName(businessUnit.getShortName());
			}
		}
		
		return members;
	}

	public Member byLogin(String login) {
		Criterion criterion = Restrictions.eq("login", login);
		return findUniqueByCriteria(criterion);
	}

	@Override
	public Member makePersistent(Member entity) {
		setUsedRole(entity);
		setNewRole(entity);
		return super.makePersistent(entity);
	}

	private void setNewRole(Member entity) {
		switch (entity.getNewRole()) {
		case STANDARD:
			if (!containsRole(entity.getRoles(), Role.NEW_STANDARD)) {
				entity.getRoles().add(Role.NEW_STANDARD); 
			}
			removeOtherRoles(entity.getRoles(), Role.NEW_STANDARD);
			break;
		default:
			// No access
			if (!containsRole(entity.getRoles(), Role.NEW_NOACCESS)) {
				entity.getRoles().add(Role.NEW_NOACCESS); 
			}
			removeOtherRoles(entity.getRoles(), Role.NEW_NOACCESS);
		}
	}

	private void setUsedRole(Member entity) {
		switch (entity.getUsedRole()) {
			case APPRAISER:
				if (!containsRole(entity.getRoles(), Role.USED_APPRAISER)) {
					entity.getRoles().add(Role.USED_APPRAISER); 
				}
				removeOtherRoles(entity.getRoles(), Role.USED_APPRAISER);
				break;
			case MANAGER:
				if (!containsRole(entity.getRoles(), Role.USED_MANAGER)) {
					entity.getRoles().add(Role.USED_MANAGER);
				}
				removeOtherRoles(entity.getRoles(), Role.USED_MANAGER);
				break;
			case STANDARD:
				if (!containsRole(entity.getRoles(), Role.USED_STANDARD)) {
					entity.getRoles().add(Role.USED_STANDARD);
				}
				removeOtherRoles(entity.getRoles(), Role.USED_STANDARD);
				break;
			default:
				// No access
				if (!containsRole(entity.getRoles(), Role.USED_NOACCESS)) {
					entity.getRoles().add(Role.USED_NOACCESS);
				}
				removeOtherRoles(entity.getRoles(), Role.USED_NOACCESS);
			}
	}
	
	private boolean containsRole(Collection<Role> roles, Role role) {
		boolean result = false;
		for (Role existingRole: roles) {
			if (existingRole.getRoleId().intValue() == role.getRoleId().intValue()) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	private void removeOtherRoles(Collection<Role> roles, Role role) {
		Iterator<Role> iter = roles.iterator();
		while (iter.hasNext()){
			Role existingRole = iter.next();
			if (existingRole.getRoleId().intValue() != role.getRoleId().intValue() && existingRole.getType().equalsIgnoreCase(role.getType())){
				iter.remove();
			}
		}
	}
	

	public List<Member> findLithiaCarCenterMembers() {
		Criterion criterion = Restrictions.eq("jobTitle.id", JobTitle.LITHIA_CAR_CENTER_ID);
		List<Member> members =  findByCriteria(criterion);
		return members;
	}
	
	public void setDealerGroupDao(DealerGroupDao dealerGroupDao) {
		this.dealerGroupDao = dealerGroupDao;
	}

}
