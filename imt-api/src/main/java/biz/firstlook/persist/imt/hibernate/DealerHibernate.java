package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreference;
import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.DealerUpgradeCode;
import biz.firstlook.model.imt.ref.ProgramType;
import biz.firstlook.persist.imt.CiaPreferenceDao;
import biz.firstlook.persist.imt.DealerAuctionPreferenceDao;
import biz.firstlook.persist.imt.DealerDao;
import biz.firstlook.persist.imt.DealerPreferenceDao;
import biz.firstlook.persist.imt.DealerUpgradeDao;
import biz.firstlook.persist.imt.ThirdPartyEntityDao;

public class DealerHibernate extends BusinessUnitHibernate<Dealer> implements
		DealerDao {
 
	private static Logger log = Logger.getLogger(DealerHibernate.class);

	private CiaPreferenceDao ciaPreferenceDao;

	private DealerPreferenceDao dealerPreferenceDao;

	private DealerUpgradeDao dealerUpgradeDao;

	private DealerAuctionPreferenceDao dealerAuctionPreferenceDao;

	private ThirdPartyEntityDao thirdPartyEntityDao;

	public List<Dealer> search(String searchStr) {
		Criterion criterion = Restrictions.or(Restrictions.or(Restrictions
				.ilike("name", searchStr, MatchMode.ANYWHERE), Restrictions
				.ilike("code", searchStr, MatchMode.ANYWHERE)), Restrictions
				.ilike("shortName", searchStr, MatchMode.ANYWHERE));

		return findByCriteria(criterion);
	}

	@Override
	public void postInsert(Dealer result) {
		// Create a Dealer Preference Object
		log.debug("Creating a Dealer Preference Object");
		dealerPreferenceDao.create(result);

		// Create a CiaPreference Object
		ciaPreferenceDao.create(result);

		// Create DealerAuctionPreference Object
		dealerAuctionPreferenceDao.create(result);
	}

	@Override
	public void prePersist(Dealer entity) {
		// Catching a bumd with a null back reference.
		if (entity.getBusinessUnitMarketDealer() != null) {
			entity.getBusinessUnitMarketDealer().setDealer(entity);
		}
	}

	@Override
	public void preUpdate(Dealer entity) {
		// any dealer with a prgram type of FIRSTLOOK needs the CIA Purchasing
		// Upgrade turned on by default
		DealerPreference preference = entity.getPreference();
		if (preference != null
				&& preference.getProgramType() == ProgramType.FIRSTLOOK) {
			boolean hasPurchasing = false;
			for (DealerUpgrade upgrade : entity.getDealerUpgrade()) {
				if (upgrade.getDealerUpgradeCD().intValue() == DealerUpgradeCode.PURCHASING
						.getId().intValue()) {
					hasPurchasing = true;
					break;
				}
			}
			if (!hasPurchasing) {
				DealerUpgrade dealerUpgrade = new DealerUpgrade();
				dealerUpgrade.setActive(true);
				dealerUpgrade.setBusinessUnitId(entity.getId());
				dealerUpgrade.setDealerUpgradeCD(DealerUpgradeCode.PURCHASING
						.getId());
				dealerUpgradeDao.makePersistent(dealerUpgrade);
				// entity.getDealerUpgrade().add(dealerUpgrade);
			}

		}
		
		// always do this check/update
		dealerPreferenceDao.populateNadaRegionCode(entity, preference);
		
		// if they didn't check the box, don't persist
		if (!(preference.getTwixEnabled()) || preference.getTwixURL().trim().length() == 0) {
			preference.setTwixURL(null);
			preference.setTwixEnabled(Boolean.FALSE);
		}
	}

	public DealerPreferenceDao getDealerPreferenceDao() {
		return dealerPreferenceDao;
	}

	public void setDealerPreferenceDao(DealerPreferenceDao dealerPreferenceDao) {
		this.dealerPreferenceDao = dealerPreferenceDao;
	}

	public DealerAuctionPreferenceDao getDealerAuctionPreferenceDao() {
		return dealerAuctionPreferenceDao;
	}

	public void setDealerAuctionPreferenceDao(
			DealerAuctionPreferenceDao dealerAuctionPreferenceDao) {
		this.dealerAuctionPreferenceDao = dealerAuctionPreferenceDao;
	}

	public CiaPreferenceDao getCiaPreferenceDao() {
		return ciaPreferenceDao;
	}

	public void setCiaPreferenceDao(CiaPreferenceDao ciaPreferenceDao) {
		this.ciaPreferenceDao = ciaPreferenceDao;
	}

	public ThirdPartyEntityDao getThirdPartyEntityDao() {
		return thirdPartyEntityDao;
	}

	public void setThirdPartyEntityDao(ThirdPartyEntityDao thirdPartyEntityDao) {
		this.thirdPartyEntityDao = thirdPartyEntityDao;
	}

	public void setDealerUpgradeDao(DealerUpgradeDao dealerUpgradeDao) {
		this.dealerUpgradeDao = dealerUpgradeDao;
	}
}
