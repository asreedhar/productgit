package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.DealerUpgradeCode;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerUpgradeCodeDao;

public class DealerUpgradeCodeHibernate extends GenericHibernate<DealerUpgradeCode,Integer> implements
		DealerUpgradeCodeDao {

	public DealerUpgradeCodeHibernate() {
		super();
	}
	
}
