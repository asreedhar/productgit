package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.GlobalParam;
import biz.firstlook.model.imt.Subscription;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.GlobalParamDao;

public class GlobalParamHibernate extends GenericHibernate<GlobalParam, Integer> implements
		GlobalParamDao {

	public GlobalParamHibernate() {
		super();
	}

	public GlobalParam forName( final String name ) {
		GlobalParam globalParam = (GlobalParam)getHibernateTemplate().execute( new HibernateCallback(){
			@SuppressWarnings("unchecked")
			public Object doInHibernate( Session session ) throws HibernateException, SQLException
			{
				Query query = session.createQuery( "from GlobalParam gp where gp.name = :name" );
				query.setParameter( "name", name );
				Object obj = null;
				try {
					obj = query.uniqueResult();
				} catch (NonUniqueResultException ex) {
					List<GlobalParam> list = query.list();
					logger.warn( "Expected only 1 result in GlobalParam." + name + " but got " + list.size() + " results.  Returning first from list.");
					if( list.size() > 0 )
						obj = list.get( 0 );
				}
				return obj; 
			}
		} );
		return globalParam;
	}

	@SuppressWarnings("unchecked")
	public List<GlobalParam> all() {
		return getHibernateTemplate().loadAll(GlobalParam.class);
	}

	@SuppressWarnings("unchecked")
	public List<GlobalParam> findByNameLike(final String name) {
		List<GlobalParam> params = (List<GlobalParam>) getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria crit = session.createCriteria( GlobalParam.class)
                	.add(Restrictions.like("name", name));
                return new ArrayList<Subscription>( crit.list() );  
            }
        });
        return params;
    }
	
	 

	
}
