package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.SubscriberType;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SubscriberTypeDao;

public class SubscriberTypeHibernate extends GenericHibernate<SubscriberType,Integer> implements
		SubscriberTypeDao {

	public SubscriberTypeHibernate() {
		super();
	}

}
