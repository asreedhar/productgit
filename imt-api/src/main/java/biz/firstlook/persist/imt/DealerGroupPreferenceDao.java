package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerGroupPreference;
import biz.firstlook.persist.common.GenericDAO;


public interface DealerGroupPreferenceDao extends GenericDAO<DealerGroupPreference, Integer>
{

}
