package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.BusinessUnit;
import biz.firstlook.persist.common.LifeCycleDAO;
import biz.firstlook.persist.common.SearchableDAO;

public interface BusinessUnitDao<E extends BusinessUnit> extends
	LifeCycleDAO<E, Integer>, SearchableDAO<E, Integer> {
    public BusinessUnit findLikeCode(String code);
    
    public List<E> search(String searchStr);
}
