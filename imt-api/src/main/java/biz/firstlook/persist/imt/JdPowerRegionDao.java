package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.ref.JdPowerRegion;
import biz.firstlook.persist.common.GenericDAO;

public interface JdPowerRegionDao extends GenericDAO<JdPowerRegion,Integer>
{

}
