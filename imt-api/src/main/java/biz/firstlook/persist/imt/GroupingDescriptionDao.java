package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.GroupingDescription;
import biz.firstlook.persist.common.GenericDAO;

public interface GroupingDescriptionDao extends GenericDAO<GroupingDescription, Integer> {

}
