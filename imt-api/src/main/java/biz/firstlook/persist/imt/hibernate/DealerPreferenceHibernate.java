package biz.firstlook.persist.imt.hibernate;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerPreference;
import biz.firstlook.model.imt.DealerPreferenceWindowSticker;
import biz.firstlook.model.imt.ref.WindowStickerTemplate;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerPreferenceDao;
import biz.firstlook.persist.imt.direct.ZipCodeToNADARegion;

public class DealerPreferenceHibernate extends
		GenericHibernate<DealerPreference, Integer> implements
		DealerPreferenceDao {

	private ZipCodeToNADARegion zipCodeToNadaRegion;

	public DealerPreferenceHibernate() {
		super();
	}

	@Override
	public DealerPreference makePersistent(DealerPreference entity) {
		return super.makePersistent(entity);
	}

	public DealerPreference create(Dealer dealer) {
		assert (dealer != null);
		assert (dealer.getZip() != null);
		DealerPreference preference = new DealerPreference(dealer);
		populateNadaRegionCode(dealer, preference);
		makePersistent(preference);
		return preference;
	}

	// TODO: This can throw a bad error, we need to figure out what happens when
	// a zip isn't in the database
	public void populateNadaRegionCode(Dealer dealer,
			DealerPreference preference) {
		preference.setNadaRegionCode(zipCodeToNadaRegion
				.getNADARegionCode(Integer.parseInt(dealer.getZip())));
	}

	public ZipCodeToNADARegion getZipCodeToNadaRegion() {
		return zipCodeToNadaRegion;
	}

	public void setZipCodeToNadaRegion(ZipCodeToNADARegion zipCodeToNadaRegion) {
		this.zipCodeToNadaRegion = zipCodeToNadaRegion;
	}

	public DealerPreference findByDealerId(Integer dealerId) {
		Criteria criteria = getSession().createCriteria(DealerPreference.class);
		criteria.add(Restrictions.eq("dealer.id", dealerId));
		return (DealerPreference)criteria.uniqueResult();
	}

	public DealerPreferenceWindowSticker findWindowStickerPrefs(Integer dealerId) {
		DealerPreferenceWindowSticker stickerPref = (DealerPreferenceWindowSticker)
			getHibernateTemplate().get(DealerPreferenceWindowSticker.class, dealerId);
		
		if(stickerPref == null) {
			stickerPref = new DealerPreferenceWindowSticker(dealerId);
			stickerPref.setNewEntity(Boolean.TRUE);
		}
		
		return stickerPref;
	}

	/**
	 * No need to call find beforehand.  This method will retrieve from database and save, copying properties of the passed in object.
	 */
	public void save(final DealerPreferenceWindowSticker dealerPreferenceWindowSticker) {
		final DealerPreferenceWindowSticker stickerPref = 
			findWindowStickerPrefs(dealerPreferenceWindowSticker.getDealerId());
		
		stickerPref.setWindowStickerTemplateId(dealerPreferenceWindowSticker.getWindowStickerTemplateId());
		stickerPref.setBuyersGuideTemplateId(dealerPreferenceWindowSticker.getBuyersGuideTemplateId());
		
		//there is no WindowStickerTemplateId of 0 in the database!
		//the user cannot have a buyer's guide w/o a window sticker as it exists today.
		if(!stickerPref.getWindowStickerTemplate().equals(WindowStickerTemplate.NONE)) {
			getHibernateTemplate().save(stickerPref);
		} else if (!stickerPref.isNewEntity()){ //delete only if we had a row or else hibernate gets upset.
			getHibernateTemplate().delete(stickerPref);
		}
	}
	

}
