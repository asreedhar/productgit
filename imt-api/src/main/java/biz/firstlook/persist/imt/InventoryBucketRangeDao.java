package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.InventoryBucketRange;
import biz.firstlook.model.imt.ref.BucketRangeType;
import biz.firstlook.persist.common.GenericDAO;

public interface InventoryBucketRangeDao extends GenericDAO<InventoryBucketRange, Integer>
{

public List<InventoryBucketRange> findByBusinessUnitIdAndBucketRangeType(
		final Integer businessUnitId, BucketRangeType invBucketTypeId);

}
