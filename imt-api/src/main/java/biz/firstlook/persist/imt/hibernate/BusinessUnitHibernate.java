package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.BusinessUnit;
import biz.firstlook.persist.common.hibernate.LifeCycleHibernate;
import biz.firstlook.persist.imt.BusinessUnitDao;
import biz.firstlook.persist.imt.util.BusinessUnitCodeGenerator;

public abstract class BusinessUnitHibernate<E extends BusinessUnit> extends
	LifeCycleHibernate<E, Integer> implements BusinessUnitDao<E> {

    private static final Log log = LogFactory
	    .getLog(BusinessUnitHibernate.class);

    private BusinessUnitCodeGenerator codeGenerator;

    public BusinessUnitHibernate() {
	super();
	codeGenerator = new BusinessUnitCodeGenerator();
    }

    @SuppressWarnings("unchecked")
    public BusinessUnit findLikeCode(String code) {
	final Criterion likeClause = Restrictions.ilike("code", code,
		MatchMode.START);
	List<BusinessUnit> list = (List<BusinessUnit>) getHibernateTemplate()
		.execute(new HibernateCallback() {
		    public Object doInHibernate(Session session)
			    throws HibernateException, SQLException {
			Criteria criteria = session
				.createCriteria(BusinessUnit.class);
			criteria.add(likeClause);
			criteria.addOrder(Order.desc("code"));
			criteria.setMaxResults(1);
			return criteria.list();
		    }
		});
	if (list.isEmpty())
	    return null;
	else
	    return list.get(0);
    }

    /**
         * Generates the BusinessUnitCode for every BusinessUnit
         */
    @Override
    public void preInsert(E entity) {
	if (entity != null && StringUtils.isBlank(entity.getCode())) {
	    if (log.isDebugEnabled())
		log.debug("Generating a BusinessUnitCode for: " + entity);
	    entity.setCode(calculateCode(entity.getName()));
	}
    }

    /**
         * Calculates the BusinessUnitCode. This algorithm will fail when there
         * are more than 99 businessunits with the same 8 letters as the name.
         * 
         * @param name
         *                the name of the BusinessUnit
         * @return the BusinessUnitCode
         */
    protected String calculateCode(String name) {
	StringBuilder businessUnitCode = new StringBuilder();
	businessUnitCode.append(codeGenerator.getCodePrefix(name));
	BusinessUnit bu = findLikeCode(businessUnitCode.toString());
	if (bu != null) {
	    businessUnitCode.append(codeGenerator.getNextCodeSuffix(bu
		    .getCode()));
	} else {
	    businessUnitCode.append("01");
	}

	return businessUnitCode.toString();
    }
}
