package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.ThirdPartyEntity;
import biz.firstlook.persist.common.GenericDAO;

public interface ThirdPartyEntityDao extends
        GenericDAO <ThirdPartyEntity, Integer>{

	List<ThirdPartyEntity> findByDMSExportThirdPartyEntity();
	
	List<ThirdPartyEntity> findAllInternetAdvertisers();
	
}
