package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.DealerTarget;
import biz.firstlook.model.imt.Target;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerTargetDao extends GenericDAO<DealerTarget,Integer> {
	public List<DealerTarget> retrieveActive(Integer dealerId);
	public DealerTarget retrieveActive(Integer dealerId, Target target);
}
