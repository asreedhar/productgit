package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.ThirdParty;
import biz.firstlook.persist.common.GenericDAO;

public interface ThirdPartyDao extends GenericDAO<ThirdParty,Integer> {	
	
	public List<ThirdParty> getBookOutThirdPartyForBusinessUnit(Integer businessUnitId);
}
