package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.DealerGridValues;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerGridValuesDao extends GenericDAO<DealerGridValues,Integer> {
	public List<DealerGridValues> findByDealerId(Integer dealerId);
}
