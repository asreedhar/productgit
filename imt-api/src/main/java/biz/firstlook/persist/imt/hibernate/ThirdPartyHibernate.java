package biz.firstlook.persist.imt.hibernate;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.model.imt.ThirdParty;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.ThirdPartyDao;

public class ThirdPartyHibernate extends GenericHibernate<ThirdParty,Integer> implements
		ThirdPartyDao {

	@Override
	public List<ThirdParty> findAll() {
		List<ThirdParty> result = new ArrayList<ThirdParty>();
		List<ThirdParty> parties = super.findAll();
		for (ThirdParty tp : parties) {
			if (!tp.getName().equalsIgnoreCase("Edmunds")&&!tp.getName().equalsIgnoreCase("Manheim")) {
				result.add(tp);
			}
		}
		return result;
	}
		
	@SuppressWarnings("unchecked")
	public List<ThirdParty> getBookOutThirdPartyForBusinessUnit(Integer businessUnitId) {
		return (List<ThirdParty>)getHibernateTemplate().find("SELECT tp from ThirdParty as tp, " +
				"biz.firstlook.model.imt.DealerPreference as dp " +
				"where ( (dp.guideBookId = tp.id) OR (dp.guideBook2Id = tp.id)  ) AND dp.dealer.id = ? ", new Integer[] {businessUnitId});
		
	}

	public ThirdPartyHibernate() {
		super();
	}




}
