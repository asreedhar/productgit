package biz.firstlook.persist.imt.hibernate;

import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.DealerGridValues;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerGridValuesDao;

public class DealerGridValuesHibernate extends GenericHibernate<DealerGridValues, Integer> implements
		DealerGridValuesDao {

	public DealerGridValuesHibernate() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<DealerGridValues> findByDealerId(Integer dealerId) {
		Criterion criterion = Restrictions.eq( "dealerId", dealerId );
		List<DealerGridValues> values = findByCriteria(criterion);
		Collections.sort( values, new BeanComparator("indexKey") );
		return values;
	}
	
}
