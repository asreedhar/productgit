package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.SubscriberType;
import biz.firstlook.persist.common.GenericDAO;

public interface SubscriberTypeDao extends GenericDAO<SubscriberType, Integer> {

}
