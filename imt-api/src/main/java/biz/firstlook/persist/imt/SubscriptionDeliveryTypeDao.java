package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.SubscriptionDeliveryType;
import biz.firstlook.persist.common.GenericDAO;

public interface SubscriptionDeliveryTypeDao extends GenericDAO<SubscriptionDeliveryType, Integer> {
	
	
}
