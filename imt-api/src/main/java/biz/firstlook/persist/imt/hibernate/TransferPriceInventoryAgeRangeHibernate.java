package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.TransferPriceInventoryAgeRange;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.TransferPriceInventoryAgeRangeDao;

public class TransferPriceInventoryAgeRangeHibernate extends GenericHibernate<TransferPriceInventoryAgeRange, Integer> implements TransferPriceInventoryAgeRangeDao {
	
}
