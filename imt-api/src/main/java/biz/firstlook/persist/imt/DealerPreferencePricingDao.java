package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerPreferencePricing;
import biz.firstlook.persist.common.GenericDAO;


public interface DealerPreferencePricingDao extends GenericDAO<DealerPreferencePricing,Integer>
{

	public DealerPreferencePricing findDefaultSystemDealerPreference();
	public DealerPreferencePricing findByDealerId(Integer dealerId);
}
