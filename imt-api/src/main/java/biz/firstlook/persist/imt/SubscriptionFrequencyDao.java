package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.SubscriptionFrequency;
import biz.firstlook.persist.common.GenericDAO;

public interface SubscriptionFrequencyDao extends GenericDAO<SubscriptionFrequency, Integer> {

}
