package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.MarketMapping;

public interface MarketMappingDao {
    public List<String> findStates();
    
    public List<String> findCounties(String state);
    
    public List<MarketMapping> find(String state, String county);
    
    public MarketMapping find(Integer id);
}
