package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.TransferPriceInventoryAgeRange;
import biz.firstlook.persist.common.GenericDAO;

public interface TransferPriceInventoryAgeRangeDao extends GenericDAO<TransferPriceInventoryAgeRange, Integer>{

}