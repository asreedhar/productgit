package biz.firstlook.persist.imt.direct.jdbc;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.model.imt.Dealer;

public class DealerTransferPriceCreateStoredProcedure extends StoredProcedure {

	private static final String STORED_PROC_NAME = "DealerTransferPriceDefault#Create";

	private static final String PARAM_BUSINESS_UNIT_ID = "DealerID";

	public DealerTransferPriceCreateStoredProcedure(DataSource dataSource) {
		super(dataSource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlParameter(PARAM_BUSINESS_UNIT_ID, Types.INTEGER));
	}

	public void execute(Dealer dealer) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_BUSINESS_UNIT_ID, dealer.getId());
		execute(parameters);
	}
}
