package biz.firstlook.persist.imt.direct.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;

import biz.firstlook.persist.imt.direct.BusinessUnitDirect;

public class BusinessUnitDirectImpl extends JdbcTemplate implements BusinessUnitDirect {

	private String updateParentSQL;
	
	public void setParent(Integer businessUnitId, Integer parentId) {
		update( updateParentSQL, new Object[] { parentId, businessUnitId } );	
		
	}

	public String getUpdateParentSQL() {
		return updateParentSQL;
	}

	public void setUpdateParentSQL(String updateParentSQL) {
		this.updateParentSQL = updateParentSQL;
	}
}
