package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.SalesChannel;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SalesChannelDao;

public class SalesChannelHibernate extends
		GenericHibernate<SalesChannel, Integer> implements SalesChannelDao {
}
