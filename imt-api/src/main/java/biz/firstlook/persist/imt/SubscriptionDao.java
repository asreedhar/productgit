package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.Subscription;
import biz.firstlook.persist.common.GenericDAO;

public interface SubscriptionDao extends GenericDAO<Subscription, Integer> {

	List<Subscription> findByMemberIdAndSubscriptionTypeId(Integer memberId, Integer id);

	List<Subscription> findBy(Integer subscriptionTypeId, Integer detailId, Integer subscriberTypeId);
}
