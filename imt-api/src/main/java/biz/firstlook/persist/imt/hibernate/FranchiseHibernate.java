package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.Franchise;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.FranchiseDao;

public class FranchiseHibernate extends GenericHibernate<Franchise,Integer> implements
		FranchiseDao {

	public FranchiseHibernate() {
		super();
	}
}
