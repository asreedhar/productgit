package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.PhotoProviderDealership;
import biz.firstlook.persist.common.GenericDAO;

public interface PhotoProviderDealershipDao extends
GenericDAO<PhotoProviderDealership, Integer> {
	List<PhotoProviderDealership> findByBusinessUnitId(int businessUnitId);
}
