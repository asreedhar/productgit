package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.InternetAdvertiserDealership;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.InternetAdvertiserDealershipDao;

public class InternetAdvertiserDealershipHibernate extends
        GenericHibernate<InternetAdvertiserDealership, Integer> implements
        InternetAdvertiserDealershipDao {

    public InternetAdvertiserDealershipHibernate() {
        super();

    }

}
