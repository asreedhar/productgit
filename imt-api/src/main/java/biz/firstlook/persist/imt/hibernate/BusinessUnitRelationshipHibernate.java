package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.BusinessUnitRelationship;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.BusinessUnitRelationshipDao;

public class BusinessUnitRelationshipHibernate extends GenericHibernate<BusinessUnitRelationship,Integer> implements
		BusinessUnitRelationshipDao {

	public BusinessUnitRelationshipHibernate() {
		super();
	}

}
