package biz.firstlook.persist.imt.direct.jdbc;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class GuidebookCorrectionForMaxStoredProcedure extends StoredProcedure {

	private static final String STORED_PROC_NAME = "GuidebookCorrectionForMax";

	private static final String PARAM_BUSINESS_UNIT_ID = "BusinessUnitID";

	public GuidebookCorrectionForMaxStoredProcedure(DataSource dataSource) {
		super(dataSource, STORED_PROC_NAME);
		setFunction(false);
		// no meaningful return value
		// declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET, new
		// InGroupAppraisalsResultSetExtractor()));
		declareParameter(new SqlParameter(PARAM_BUSINESS_UNIT_ID, Types.INTEGER));
		// compile(); //do not compile, let db server take care of optimization.
	}

	public void execute(Integer businessUnitId) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_BUSINESS_UNIT_ID, businessUnitId);

		execute(parameters);
	}

}
