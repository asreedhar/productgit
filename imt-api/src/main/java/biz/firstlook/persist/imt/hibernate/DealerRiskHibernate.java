package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.DealerRisk;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerRiskDao;

public class DealerRiskHibernate extends GenericHibernate<DealerRisk,Integer> implements
		DealerRiskDao {

	public DealerRiskHibernate() {
		super();
	}	
}
