package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.ATCAccessGroup;
import biz.firstlook.model.imt.Subscription;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.ATCAccessGroupDao;

public class ATCAccessGroupHibernate extends GenericHibernate<ATCAccessGroup,Integer> implements ATCAccessGroupDao{

    public ATCAccessGroupHibernate() {
        super();
    }

    @SuppressWarnings("unchecked")
    public List<ATCAccessGroup> findByBusinessUnitId(final Integer businessUnitId) {
        List<ATCAccessGroup> accessGroups = (List<ATCAccessGroup>) getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria crit = session.createCriteria( ATCAccessGroup.class)
                    .add( Restrictions.eq("businessUnitId", businessUnitId));
                return new ArrayList<Subscription>( crit.list() );  
            }
        });
        return accessGroups;
    }
}
