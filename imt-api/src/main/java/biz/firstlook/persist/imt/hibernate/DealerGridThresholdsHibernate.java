package biz.firstlook.persist.imt.hibernate;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import biz.firstlook.model.imt.DealerGridThresholds;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerGridThresholdsDao;

public class DealerGridThresholdsHibernate extends GenericHibernate<DealerGridThresholds, Integer> implements
		DealerGridThresholdsDao {

	public DealerGridThresholdsHibernate() {
		super();
	}

	public DealerGridThresholds findByDealerId(Integer dealerId) {
		Criterion criterion = Restrictions.eq( "dealerId", dealerId );
		return findUniqueByCriteria(criterion);
	}
	
}
