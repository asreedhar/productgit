package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.Target;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.TargetDao;

public class TargetHibernate extends GenericHibernate<Target,Integer> implements
		TargetDao {

	public TargetHibernate() {
		super();
	}

}
