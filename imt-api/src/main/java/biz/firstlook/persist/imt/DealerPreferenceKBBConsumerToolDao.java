package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerPreferenceKBBConsumerTool;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerPreferenceKBBConsumerToolDao extends GenericDAO<DealerPreferenceKBBConsumerTool,Integer> {

}
