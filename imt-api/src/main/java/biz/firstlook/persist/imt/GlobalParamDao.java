package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.GlobalParam;
import biz.firstlook.persist.common.GenericDAO;

public interface GlobalParamDao extends GenericDAO<GlobalParam,Integer>{
	
	public GlobalParam forName( String name );
	public List<GlobalParam> all();
	public List<GlobalParam> findByNameLike(String name);

}
