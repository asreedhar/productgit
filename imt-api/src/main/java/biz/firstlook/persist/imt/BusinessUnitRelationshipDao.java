package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.BusinessUnitRelationship;
import biz.firstlook.persist.common.GenericDAO;

public interface BusinessUnitRelationshipDao extends GenericDAO<BusinessUnitRelationship,Integer> {
//	public List<BusinessUnitRelationship> findByBusinessUnitId(Integer id);
}
