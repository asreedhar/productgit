package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.DMSExportPriceMapping;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DMSExportPriceMappingDao;

public class DMSExportPriceMappingHibernate  extends GenericHibernate<DMSExportPriceMapping,Integer> implements DMSExportPriceMappingDao {

}
