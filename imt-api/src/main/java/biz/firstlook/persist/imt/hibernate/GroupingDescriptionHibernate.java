package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.GroupingDescription;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.GroupingDescriptionDao;

public class GroupingDescriptionHibernate extends GenericHibernate<GroupingDescription,Integer> implements
		GroupingDescriptionDao {

	public GroupingDescriptionHibernate() {
		super();
	}

}
