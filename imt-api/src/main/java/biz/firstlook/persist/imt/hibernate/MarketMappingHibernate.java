package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.model.imt.MarketMapping;
import biz.firstlook.persist.imt.MarketMappingDao;

public class MarketMappingHibernate extends HibernateDaoSupport implements
	MarketMappingDao {

    public MarketMappingHibernate() {
	super();
    }

    @SuppressWarnings("unchecked")
    public List<String> findCounties(final String state) {
	StringBuilder sb = new StringBuilder();
	sb.append("SELECT county FROM MarketMapping");
	sb.append(" WHERE state = ?");
	sb.append(" AND county != null AND county != ''");
	sb.append(" GROUP BY county");
	sb.append(" ORDER BY county");
	return getHibernateTemplate().find(sb.toString(), state);
    }

    @SuppressWarnings("unchecked")
    public List<String> findStates() {
	StringBuilder sb = new StringBuilder();
	sb.append("SELECT state FROM MarketMapping");
	sb.append(" WHERE state != null AND state != ''");
	sb.append(" GROUP BY state");
	sb.append(" ORDER BY state");
	return getHibernateTemplate().find(sb.toString());
    }

    @SuppressWarnings("unchecked")
    public List<MarketMapping> find(String state, String county) {
	StringBuilder sb = new StringBuilder();
	sb.append("FROM MarketMapping");
	sb.append(" WHERE state = ? AND county = ?");
	sb.append(" ORDER BY name");
	return getHibernateTemplate().find(sb.toString(), new String[] {state, county});
    }

    public MarketMapping find(Integer id) {
	return (MarketMapping)getHibernateTemplate().get(MarketMapping.class, id);
    }
}
