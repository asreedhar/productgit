package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.Corporation;

public interface CorporationDao extends BusinessUnitDao<Corporation> {
}
