package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.InternetAdvertiserDealership;
import biz.firstlook.persist.common.GenericDAO;

public interface InternetAdvertiserDealershipDao extends
        GenericDAO<InternetAdvertiserDealership, Integer> {

}
