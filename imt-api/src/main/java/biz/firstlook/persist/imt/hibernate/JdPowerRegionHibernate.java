package biz.firstlook.persist.imt.hibernate;

import java.util.List;

import biz.firstlook.model.imt.ref.JdPowerRegion;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.JdPowerRegionDao;

public class JdPowerRegionHibernate extends GenericHibernate<JdPowerRegion,Integer> implements JdPowerRegionDao
{

@SuppressWarnings("unchecked")
@Override
public List< JdPowerRegion > findAll()
{
	return (List< JdPowerRegion >)getHibernateTemplate().find( "from JdPowerRegion order by name" );
}

}
