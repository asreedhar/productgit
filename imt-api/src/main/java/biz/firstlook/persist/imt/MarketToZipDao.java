package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.MarketToZip;
import biz.firstlook.model.imt.MarketToZipPK;
import biz.firstlook.persist.common.GenericDAO;

public interface MarketToZipDao extends GenericDAO<MarketToZip,MarketToZipPK> {
}
