package biz.firstlook.persist.imt.direct.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import biz.firstlook.persist.imt.direct.ZipCodeToNADARegion;

public class ZipCodeToNADARegionDirectImpl extends JdbcTemplate implements ZipCodeToNADARegion
{
private String zipCodeToNadaParentSQL;

@SuppressWarnings("unchecked")
public Integer getNADARegionCode(Integer zipCode) {
	List< Integer > nadaRegionCode =  (List< Integer >)query( zipCodeToNadaParentSQL, new Object[] { zipCode }, new Mapper() );
	if( !nadaRegionCode.isEmpty() ) {
	    return nadaRegionCode.get( 0 );
	} else {
	    return null;
	}
}

class Mapper implements ResultSetExtractor
{

	public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
	{
		List<Integer> nadaRegionCode = new ArrayList<Integer>();
		while( rs.next() )
		{
			 nadaRegionCode.add( (Integer)rs.getObject( "NADARegionCode" ) );
		}
		return nadaRegionCode;
	}


}

public String getZipCodeToNadaParentSQL()
{
	return zipCodeToNadaParentSQL;
}

public void setZipCodeToNadaParentSQL( String zipToNadaParentSQL )
{
	this.zipCodeToNadaParentSQL = zipToNadaParentSQL;
}


}
