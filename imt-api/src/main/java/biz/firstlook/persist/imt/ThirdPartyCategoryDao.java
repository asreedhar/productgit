package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.ThirdPartyCategory;
import biz.firstlook.persist.common.GenericDAO;

public interface ThirdPartyCategoryDao extends GenericDAO<ThirdPartyCategory,Integer> {	
}
