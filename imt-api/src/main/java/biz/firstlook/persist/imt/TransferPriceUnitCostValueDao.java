package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.TransferPriceUnitCostValue;
import biz.firstlook.persist.common.GenericDAO;

public interface TransferPriceUnitCostValueDao extends GenericDAO<TransferPriceUnitCostValue, Integer> {

}
