package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerGridThresholds;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerGridThresholdsDao extends GenericDAO<DealerGridThresholds,Integer> {
	public DealerGridThresholds findByDealerId(Integer dealerId);
}
