package biz.firstlook.persist.imt.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.model.imt.DealerTarget;
import biz.firstlook.model.imt.Target;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerTargetDao;

public class DealerTargetHibernate extends GenericHibernate<DealerTarget,Integer> implements
		DealerTargetDao {

	public DealerTargetHibernate() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<DealerTarget> retrieveActive(final Integer dealerId) {
		List execute = (List) getHibernateTemplate().execute( new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( DealerTarget.class )
				    .add( Restrictions.eq( "businessUnitId", dealerId) )
				    .add( Restrictions.eq( "active", true ) );
				return crit.list();
			}
		});
		return new ArrayList<DealerTarget>(execute);
	}

	public DealerTarget retrieveActive(final Integer dealerId, final Target target) {
		DealerTarget execute = (DealerTarget) getHibernateTemplate().execute( new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( DealerTarget.class )
				    .add( Restrictions.eq( "businessUnitId", dealerId) )
				    .add( Restrictions.eq( "active", true ) )
				    .createCriteria("target")
				    	.add( Restrictions.eq( "code", target.getCode() ) );
				return crit.uniqueResult();
			}
		});
		return execute;
	}
	
}
