package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.SubscriptionFrequencyDetail;
import biz.firstlook.model.imt.SubscriptionType;
import biz.firstlook.persist.common.GenericDAO;

public interface SubscriptionTypeDao extends GenericDAO<SubscriptionType, Integer> {

	public List<SubscriptionFrequencyDetail> getSubscriptionFrequenciesForSubscriptionType(Integer subscriptionTypeId);
	
}
