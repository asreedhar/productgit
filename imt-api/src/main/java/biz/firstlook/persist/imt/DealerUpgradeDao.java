package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerUpgrade;
import biz.firstlook.model.imt.DealerUpgradePK;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerUpgradeDao extends GenericDAO<DealerUpgrade,DealerUpgradePK> {
	
	public DealerUpgrade findByBusinessUnitAndUpgradeType(Integer businessUnitId, Integer upgradeCode);
}
