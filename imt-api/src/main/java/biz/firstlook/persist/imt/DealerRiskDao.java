package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.DealerRisk;
import biz.firstlook.persist.common.GenericDAO;

public interface DealerRiskDao extends GenericDAO<DealerRisk,Integer> {
}
