package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.JobTitle;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.JobTitleDao;

public class JobTitleHibernate extends GenericHibernate<JobTitle,Integer> implements
		JobTitleDao {

	public JobTitleHibernate() {
		super();
	}

}
