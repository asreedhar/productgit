package biz.firstlook.persist.imt.direct;

public interface ZipCodeToNADARegion
{
	public Integer getNADARegionCode(Integer zipCode);
}
