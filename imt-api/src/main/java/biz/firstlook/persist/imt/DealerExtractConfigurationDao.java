package biz.firstlook.persist.imt;

import java.util.List;

import biz.firstlook.model.imt.DealerExtractConfiguration;

public interface DealerExtractConfigurationDao {
	
	public List<String> findDestinations();
	
	public List<DealerExtractConfiguration> findBy(Integer dealerId);
	
	public DealerExtractConfiguration findBy(Integer dealerId, String destinationName);
	
	public DealerExtractConfiguration save(DealerExtractConfiguration config);
}
