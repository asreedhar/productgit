package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.SubscriptionFrequency;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SubscriptionFrequencyDao;

public class SubscriptionFrequencyHibernate extends GenericHibernate<SubscriptionFrequency,Integer> implements
		SubscriptionFrequencyDao {

	public SubscriptionFrequencyHibernate() {
		super();
	}
	
}
