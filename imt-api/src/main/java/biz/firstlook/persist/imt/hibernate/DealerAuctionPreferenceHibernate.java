package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerAuctionPreference;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.DealerAuctionPreferenceDao;

public class DealerAuctionPreferenceHibernate extends
		GenericHibernate<DealerAuctionPreference, Integer> implements
		DealerAuctionPreferenceDao {

	public DealerAuctionPreferenceHibernate() {
		super();
	}

	public DealerAuctionPreference create( Dealer dealer ) {
		assert( dealer != null );
		assert( dealer.getId() != null );
		DealerAuctionPreference preference = new DealerAuctionPreference();
		preference.setId( dealer.getId() );
		makePersistent( preference );
		return preference;
	}

}
