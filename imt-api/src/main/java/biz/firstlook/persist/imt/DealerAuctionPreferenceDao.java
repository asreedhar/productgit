package biz.firstlook.persist.imt;

import biz.firstlook.model.imt.Dealer;
import biz.firstlook.model.imt.DealerAuctionPreference;
import biz.firstlook.persist.common.GenericDAO;


public interface DealerAuctionPreferenceDao extends GenericDAO<DealerAuctionPreference,Integer>
{

	public DealerAuctionPreference create(Dealer dealer);

}
