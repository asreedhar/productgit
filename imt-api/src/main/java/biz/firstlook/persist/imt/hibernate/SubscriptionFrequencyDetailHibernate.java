package biz.firstlook.persist.imt.hibernate;

import biz.firstlook.model.imt.SubscriptionFrequencyDetail;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.imt.SubscriptionFrequencyDetailDao;

public class SubscriptionFrequencyDetailHibernate extends GenericHibernate<SubscriptionFrequencyDetail,Integer> implements
		SubscriptionFrequencyDetailDao {

	public SubscriptionFrequencyDetailHibernate() {
		super();
	}
	
}
