package biz.firstlook.persist.market.hibernate;

import java.util.List;

import biz.firstlook.model.market.MileagePerYearBucket;
import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.persist.market.MileagePerYearBucketDao;


public class MileagePerYearBucketHibernate extends GenericHibernate<MileagePerYearBucket, Integer> implements MileagePerYearBucketDao {

	@Override
	public List<MileagePerYearBucket> findAll() {
		List<MileagePerYearBucket> result = super.findAll();;
		return result;
	}

	public MileagePerYearBucketHibernate() {
		super();
	}
	
}
