package biz.firstlook.persist.market;

import biz.firstlook.model.market.MileagePerYearBucket;
import biz.firstlook.persist.common.GenericDAO;

public interface MileagePerYearBucketDao extends GenericDAO<MileagePerYearBucket,Integer>{

}
