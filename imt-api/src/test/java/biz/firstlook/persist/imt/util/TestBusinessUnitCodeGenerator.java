package biz.firstlook.persist.imt.util;

import junit.framework.TestCase;

public class TestBusinessUnitCodeGenerator extends TestCase {

    private BusinessUnitCodeGenerator codeGenerator = new BusinessUnitCodeGenerator();

    public TestBusinessUnitCodeGenerator(String arg1) {
	super(arg1);
    }

    public void testGetCodePrefix() {
	String alphabet = "abcdefghijklmnopqrstuvwxyz";
	String eightChars = alphabet.substring(0, 8);

	// need eight character prefixes
	assertEquals(eightChars.toUpperCase(), codeGenerator
		.getCodePrefix(alphabet));
	assertEquals(eightChars.toUpperCase(), codeGenerator
		.getCodePrefix(eightChars));
    }

    public void testGetCodePrefixLessThanEightChars() {
	String name = "name";
	assertEquals("NAME0000", codeGenerator.getCodePrefix(name));
    }

    public void testGetCodePrefixWithSpacesInName() {
	assertEquals("ABCDEFGF", codeGenerator.getCodePrefix("a bc def  gfsd"));
	assertEquals("ABC00000", codeGenerator.getCodePrefix(" a b c "));
    }

    public void testGetNextCodeSuffix() {
	assertEquals("12", codeGenerator.getNextCodeSuffix("PQRS11"));
	assertEquals("03", codeGenerator.getNextCodeSuffix("PQRS02"));
	assertEquals("57", codeGenerator.getNextCodeSuffix("PQRS123456"));
	assertEquals("23", codeGenerator.getNextCodeSuffix("PQRS000122"));
	assertEquals("23", codeGenerator.getNextCodeSuffix("000122"));
    }

    public void testNotValidCode() {
	String nonCode = "AS";
	try {
	    String suffix = codeGenerator.getNextCodeSuffix(nonCode);
	    fail("A suffix was generated for an invalid BusinessUnitCode! : " + suffix);
	} catch (Exception e) {
	    assertEquals("Not A BusinessUnitCode!: " + nonCode, e.getMessage());
	}
    }

    public void testIllegalXMLCharacter() {
	assertEquals("SLMOTORS", codeGenerator.getCodePrefix("S & L Motors"));
	assertEquals("SLMOTORS", codeGenerator.getCodePrefix("S >< L Motors"));
	assertEquals("SLMOTORS", codeGenerator.getCodePrefix("S \"L\" Motors"));
	assertEquals("SLMOTORS", codeGenerator.getCodePrefix("S\'\"L Motors"));
	assertEquals("SLMOTORS", codeGenerator.getCodePrefix("\"S\'&L>Mot>ors"));
    }
}
