# README #
---------------------

## Getting code

### New copy

If you are pulling code for the first time, or just want to start fresh getting code is as simple as
`git clone https://<BITBUCKET_USERNAME>@bitbucket.org/max-digital/productline-git.git`

*N.B.* The full, personalized url can be found in the top right of the Overview page for a given repository.

I have removed notes related to migrating an existing repo to reduce laziness.
---------------------

## Working with forks

If you are using a forked workflow, the following may be useful to you.

### Updating your fork

You will need to update your fork on a regular basis. Bitbucket makes this easy to do in the web UI. 
In this case, F is your fork and O the original.  So, you would do the following:

1. Go to your forked repository F.
2. Click the Compare button.
3. Click Incoming to see what changes were made on the source O. If the system finds there were changes on O not in F, it provides instructions for merging them. 

### Submitting Pull Requests

Once development of a feature/bugfix is ready to be merged back to the main repository you will need to submit a pull request. 

Before submitting a pull request, make sure your fork is up to date. In the BitBucket Web Interface, if there are any outstanding differences, there is a badge on the right which says "This fork is _N_ commit(s) behind max-digital/productline. *Sync now*."

When your repository is current with the base repository you may [submit a pull request](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests). (Relevant parts quoted below)


> Request the destination repo owner pull your changes
> To create a pull request, do the following:
>
> 1. Navigate to the repository with the changes.
> 2. Press the Pull Request button.
> 3. The system displays the request form.
> 4. Complete the form as appropriate for your request.
> 5. Press Create pull request.
> 6. The system opens your latest request on the Pull Request page of the original repository.
> 7. To see the list of all the pull requests against this repository, click Pull Requests in the navigation bar.
> 8. The system also sent a notification email to your account Inbox.
> 9. Go ahead and open your account inbox avatar > Inbox.
> 10. Locate and review the pull request email (it should be near the top).       
> 11. Adjust the Source branch if necessary.
> 12. Adjust the Destination repository or branch if necessary.
> 13. Enter a Title and Description for your pull request.
> 14. Press Create pull request.



### Contact ###

If you have any problems or questions relating to bitbucket/mercurial usage please contact Travis Huber <thuber@maxdigital.com>