package biz.firstlook.cia.persistence;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.transact.persist.model.Inventory;

public class MockInventoryRetriever
{

private List inventories = new ArrayList();

public void add( Inventory inventory )
{
	inventories.add( inventory );
}

public int retrieveInventoryCount( Integer dealerId, Integer classTypeId, Integer unitCostStart, Integer unitCostStop, Integer inventoryType )
		throws PersistenceException
{
	return inventories.size();
}

public int retrieveInventoryCount( Integer dealerId, Integer classTypeId, Integer inventoryType ) throws PersistenceException
{
	return inventories.size();
}

public int retrieveCurrentUnitsInStock( int dealerId ) throws PersistenceException
{
	return inventories.size();
}

public List retrieveRetailVehicleSalesByGroupingDescriptionIds( Integer dealerId, List groupingDescriptionIds,
																CompositeBasisPeriod basisPeriod, Integer lowerUnitCostThreshold,
																Integer upperUnitCostThreshold, Integer feGrossProfitThreshold )
{
	return inventories;
}

public int retrieveCountOfRetailVehicleSalesByGroupingDescriptionIds( Integer dealerId, List groupingDescriptionIds,
																		CompositeBasisPeriod basisPeriod, Integer lowerUnitCostThreshold,
																		Integer upperUnitCostThreshold )
{
	return inventories.size();
}

public int retrieveCountOfRetailVehicleSalesByGroupingDescriptionIds( Integer dealerId, List groupingDescriptionIds,
																		CompositeBasisPeriod basisPeriod, Integer lowerUnitCostThreshold,
																		Integer upperUnitCostThreshold, Integer feGrossProfitThreshold )
{
	return inventories.size();
}

}
