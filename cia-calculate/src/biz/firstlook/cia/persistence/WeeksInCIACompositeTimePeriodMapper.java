/**
 * 
 */
package biz.firstlook.cia.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

class WeeksInCIACompositeTimePeriodMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
{
    return new Integer( rs.getInt( "Weeks" ) );
}
}