package biz.firstlook.cia.persistence;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import biz.firstlook.cia.db.CIADatabaseUtil;
import biz.firstlook.cia.model.InventoryOverstocking;

import com.firstlook.data.RuntimeDatabaseException;

public class InventoryOverstockingPersistence implements IInventoryOverstockingPersistence
{

public InventoryOverstockingPersistence()
{
	super();
}

public List findByInventoryIdAndStatus( Integer inventoryId, Integer statusId )
{
	Session session = null;
	try
	{
		session = CIADatabaseUtil.instance().retrieveSession();
		Criteria crit = session.createCriteria( InventoryOverstocking.class );
		crit.add( Expression.eq( "inventoryId", inventoryId ) );
		crit.add( Expression.eq( "statusId", statusId ) );
		return crit.list();
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving Inventory Overstocking by inventory id and status" + e );
	}
	finally
	{
		CIADatabaseUtil.instance().closeSession( session );
	}
}

public void save( InventoryOverstocking inventoryOverstocking )
{
	try
	{
		CIADatabaseUtil.instance().save( inventoryOverstocking );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error saving Inventory Overstocking" + e );
	}
}

}