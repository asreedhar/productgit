package biz.firstlook.cia.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.cia.model.CIABodyTypeButton;
import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

public class CIABodyTypeDetailContributionDaysSupplyRetriever extends StoredProcedureTemplate
{

public List retrieveCIABodyTypeDetails( int businessUnitId, boolean useNoSales, Timestamp basisPeriodDate, int ciaSummaryId )
{

	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, businessUnitId) );
	parameters.add( new SqlParameterWithValue( "@includeNoSales", Types.BOOLEAN, useNoSales) );
	parameters.add( new SqlParameterWithValue( "@basisPeriodDate", Types.TIMESTAMP, basisPeriodDate) );
	parameters.add( new SqlParameterWithValue( "@ciaSummaryId", Types.INTEGER, ciaSummaryId) );
	
	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetDealerSegmentContribution (?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new CIABodyTypeDetailContributionDaysSupplyMapper() );

	LinkedList<CIABodyTypeButton> resultsList = (LinkedList<CIABodyTypeButton>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList();
	
}

private class CIABodyTypeDetailContributionDaysSupplyMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
			CIABodyTypeButton ciaBodyTypeButton = new CIABodyTypeButton();
			ciaBodyTypeButton.setSegmentId( new Integer( rs.getInt( "SegmentId" ) ) );
			ciaBodyTypeButton.setPercentContribution( new Double( rs.getDouble( "DealerContribution" ) ) );
			ciaBodyTypeButton.setDaysSupply( new Integer( rs.getInt( "DaysSupply" ) ) );
			return ciaBodyTypeButton;
	}

}

}
