package biz.firstlook.cia.persistence;

public class BucketronRowResult
{
private String description;
private int allocationUnits;

public BucketronRowResult()
{
	
}

public BucketronRowResult( String description, int units )
{
	this.description = description;
	allocationUnits = units;
}

public int getAllocationUnits()
{
	return allocationUnits;
}

public void setAllocationUnits( int allocationUnits )
{
	this.allocationUnits = allocationUnits;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}
}
