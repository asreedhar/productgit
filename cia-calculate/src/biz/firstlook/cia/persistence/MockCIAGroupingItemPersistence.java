package biz.firstlook.cia.persistence;

import java.util.Collection;
import java.util.List;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.transact.persist.model.GroupingDescription;

public class MockCIAGroupingItemPersistence implements ICIAGroupingItemDAO
{

public MockCIAGroupingItemPersistence()
{
	super();
}

public CIAGroupingItem findByCIAGroupingItemId( Integer CIAGroupingItemId )
{
	CIAGroupingItem ciaGroupingItem = new CIAGroupingItem();
	ciaGroupingItem.setGroupingDescription( new GroupingDescription( new Integer( 1 ), "Honda Civic"));
	return ciaGroupingItem;
}

public List<CIAGroupingItem> findBy( Integer ciaSummaryId, Integer segmentId, Integer type )
{
	return null;
}

public List<CIAGroupingItem> findCoreGroupingsByDealerIdAndStatus( Integer dealerId, Integer ciaSummaryStatusId )
{
	return null;
}

public void update( CIAGroupingItem ciaGroupingItem )
{
}

public void saveOrUpdate( CIAGroupingItem ciaGroupingItem )
{
}

public List findBy( Integer ciaSummaryId, Integer segmentId, Integer ciaCategory, Integer groupingDescriptionId )
{
	return null;
}

public List findGroupingItemsWithPlans( Integer ciaSummaryId, Integer segmentId )
{
	return null;
}

public List findBy( Integer ciaSummaryId, Integer segmentId )
{
	return null;
}

public List findCoreGroupingsByCIASummaryIdAndSegment( Integer ciaSummaryId, Integer segmentId )
{
	return null;
}

public List findBy( Integer ciaSummaryId )
{
	return null;
}

public Collection findDistinctCIAGroupingItemsWithRecommendationsByCIACategory( Integer businessUnitId, Integer ciaSummaryStatusId, Integer ciaCategoryId )
{
    return null;
}

public CIAGroupingItem findNonLazyByCIAGroupingItemId( Integer CIAGroupingItemId )
{
	return null;
}

public CIAGroupingItem findByGroupingDescriptionSegmentAndCIACategory( Integer segmentId, Integer ciaCategoryId, Integer groupingDescriptionId, Integer dealerId )
{
	// TODO Auto-generated method stub
	return null;
}

public CIAGroupingItem findByGroupingDescription( Integer groupingDescriptionId, Integer dealerId )
{
	return null;
}

}
