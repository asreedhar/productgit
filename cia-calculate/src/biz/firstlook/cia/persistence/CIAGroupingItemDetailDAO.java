package biz.firstlook.cia.persistence;

import java.util.Collection;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;

public class CIAGroupingItemDetailDAO extends HibernateDaoSupport implements ICIAGroupingItemDetailDAO
{

public CIAGroupingItemDetail findForDealerByGroupingDescriptionAndDetailValue( Integer businessUnitId, Integer ciaSummaryStatusId,
																				Integer groupingDescriptionId, String detailValue )
{
	StringBuffer query = new StringBuffer();
	query.append( "select detail " );
	query.append( " from CIASummary summary " );
	query.append( " join summary.ciaBodyTypeDetails btd " );
	query.append( " join btd.ciaGroupingItems item " );
	query.append( " join item.ciaGroupingItemDetails detail " );
	query.append( " where summary.businessUnitId = ? " );
	query.append( " and summary.status.statusId = ? " );
	query.append( " and item.groupingDescription.groupingDescriptionId = ? " );
	query.append( " and detail.detailLevelValue = ? " );
	query.append( " and detail.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = ? " );

	CIAGroupingItemDetail detail = null;

	Collection collection = getHibernateTemplate().find(
															query.toString(),
															new Object[] {
																	businessUnitId,
																	ciaSummaryStatusId,
																	groupingDescriptionId,
																	detailValue,
																	CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL.getCiaGroupingItemDetailTypeId() } );
	if ( collection != null && !collection.isEmpty() )
	{
		return (CIAGroupingItemDetail)collection.toArray()[0];
	}

	return detail;
}

}
