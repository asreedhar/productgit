package biz.firstlook.cia.persistence;

import java.util.List;

import biz.firstlook.cia.model.InventoryOverstocking;

public class MockInventoryOverstockingPersistence implements IInventoryOverstockingPersistence
{

public MockInventoryOverstockingPersistence()
{
	super();
}

public List findByInventoryIdAndStatus( Integer inventoryId, Integer statusId )
{
	return null;
}

public List findByDealerIdNotInactive( Integer dealerId )
{
	return null;
}

public void update( InventoryOverstocking inventoryOverstocking )
{

}

public void save( InventoryOverstocking inventoryOverstocking )
{

}

}
