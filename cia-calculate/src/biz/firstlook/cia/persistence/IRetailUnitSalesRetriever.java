package biz.firstlook.cia.persistence;

import java.util.Date;
import java.util.List;

public interface IRetailUnitSalesRetriever
{

public List calculateGroupingDescriptionTotalRevenueByClassType(
        Integer classTypeId, Integer dealerId, Date startDate, Date endDate )
        throws PersistenceException;

public List calculateGroupingDescriptionUnitsSoldByClassType(
        Integer classTypeId, Integer dealerId, Date startDate, Date endDate )
        throws PersistenceException;

public List calculateProfitableGroupingDescriptionUnitsSoldByClassType(
        Integer classTypeId, Integer dealerId, Date startDate, Date endDate )
        throws PersistenceException;

public double calculateTotalRevenueByDealer( Integer dealerId, Date startDate,
        Date endDate ) throws PersistenceException;

public int calculateUnitsSoldForDealer( Integer dealerId, Date startDate,
        Date endDate ) throws PersistenceException;

}
