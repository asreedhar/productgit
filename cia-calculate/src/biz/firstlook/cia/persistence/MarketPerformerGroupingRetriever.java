package biz.firstlook.cia.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.cia.model.MarketPerformerGrouping;

public class MarketPerformerGroupingRetriever extends StoredProcedure
{

private static final String STORED_PROC_NAME = "GetMarketPerformers";

private static final String PARAM_RESULT_SET = "ResultSet";
private static final String PARAM_RC = "RC";

private static final String PARAM_BUSINESSUNIT_ID = "BusinessUnitID";

public MarketPerformerGroupingRetriever( DataSource dataSource )
{
	super( dataSource, STORED_PROC_NAME );
	setFunction( true );
	declareParameter( new SqlReturnResultSet( PARAM_RESULT_SET, new MarketPerformerGroupingRetrieverRowMapper() ) );
	declareParameter( new SqlOutParameter( PARAM_RC, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_BUSINESSUNIT_ID, Types.INTEGER ) );
	compile();
}

public List retrieveMarketPerformerGroupings( Integer businessUnitId )
{
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put( PARAM_BUSINESSUNIT_ID, businessUnitId );
	
	Map results = execute( parameters );
	
	return (List)results.get( PARAM_RESULT_SET );
}

public class MarketPerformerGroupingRetrieverRowMapper implements RowMapper
{

public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
{
	MarketPerformerGrouping marketPerformerGrouping = new MarketPerformerGrouping();
	marketPerformerGrouping.setGroupingDescriptionId( rs.getInt( "GroupingDescriptionId" ) );
	marketPerformerGrouping.setDealerPenetration( rs.getDouble( "DealerPenetration" ) );
	marketPerformerGrouping.setSegmentId( rs.getInt( "SegmentId" ) );
	marketPerformerGrouping.setInBrand( rs.getInt( "InBrand" ) );
	marketPerformerGrouping.setLight( rs.getInt( "Light" ) );
	marketPerformerGrouping.setMarketShare( rs.getDouble( "MarketShare" ) );
	return marketPerformerGrouping;
}

}

}