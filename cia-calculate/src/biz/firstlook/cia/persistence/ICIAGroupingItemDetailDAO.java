package biz.firstlook.cia.persistence;

import biz.firstlook.cia.model.CIAGroupingItemDetail;

public interface ICIAGroupingItemDetailDAO
{

public CIAGroupingItemDetail findForDealerByGroupingDescriptionAndDetailValue( Integer businessUnitId, Integer ciaSummaryStatusId,
                                                                              Integer groupingDescriptionId, String detailValue );

}