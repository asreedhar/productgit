package biz.firstlook.cia.persistence;

import java.util.List;

import org.hibernate.Hibernate;
import biz.firstlook.cia.db.CIADatabaseUtil;

import com.firstlook.data.DatabaseException;

public class CIAPowerZonePricePointPersistence
{

public CIAPowerZonePricePointPersistence()
{
}

public List findBy( int ciaPowerZoneItemId ) throws DatabaseException
{

    try
    {
        List pricePoints = CIADatabaseUtil.instance().find(
                "from biz.firstlook.cia.model.CIAPowerZonePricePoint "
                        + "where ciaPowerZoneItemId = ?",
                new Integer(ciaPowerZoneItemId), Hibernate.INTEGER);
        return pricePoints;
    } catch (Exception e)
    {
        throw new DatabaseException(
                "Error retrieving price points by cia power zone item id: ", e);
    }
}

}
