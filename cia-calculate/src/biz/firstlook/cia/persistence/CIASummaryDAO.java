package biz.firstlook.cia.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;

public class CIASummaryDAO extends HibernateDaoSupport implements ICIASummaryDAO
{

public CIASummary retrieveCIASummary( Integer dealerId, Status status )
{
	CIASummary ciaSummary = null;
//TODO:  we would need to know the sort order already if this is going to be a List - KDL
	List list = getHibernateTemplate().find(
												"from biz.firstlook.cia.model.CIASummary as ciaSummary where ciaSummary.businessUnitId = ? and ciaSummary.status.statusId = ? ",
												new Object[] { dealerId, status.getStatusId() } );
	if ( list != null && !list.isEmpty())
	{
		ciaSummary = (CIASummary)list.get( 0 );
	}

	return ciaSummary;
}

public void save( CIASummary ciaSummary )
{
	getHibernateTemplate().save( ciaSummary );
}

public void update( CIASummary ciaSummary )
{
	getHibernateTemplate().update( ciaSummary );
}

public Integer retrieveCIASummaryId( Integer dealerId, Status status )
{
	List list = getHibernateTemplate().find(
											"select ciaSummary.ciaSummaryId from biz.firstlook.cia.model.CIASummary as ciaSummary where ciaSummary.businessUnitId = ? and ciaSummary.status.statusId = ? ",
											new Object[] { dealerId, status.getStatusId() } );
	if(list == null || list.isEmpty() )
	{
		return null;
	}
	return (Integer)list.get(0);
}

}
