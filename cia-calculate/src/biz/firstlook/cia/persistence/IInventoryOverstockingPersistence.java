package biz.firstlook.cia.persistence;

import java.util.List;

import biz.firstlook.cia.model.InventoryOverstocking;

public interface IInventoryOverstockingPersistence
{
public List findByInventoryIdAndStatus( Integer inventoryId,
        Integer statusId );

public void save( InventoryOverstocking inventoryOverstocking );
}