package biz.firstlook.cia.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

public class CoreMarketPenetrationRetriever extends StoredProcedure
{

private static final String STORED_PROC_NAME = "GetCoreMarketPenetration";

private static final String PARAM_RESULT_SET = "ResultSet";
private static final String PARAM_RC = "RC";

private static final String PARAM_BUSINESSUNIT_ID = "BusinessUnitID";
private static final String PARAM_BASIS_PERIOD_DATE = "BaseDate";
private static final String PARAM_VEHICLE_SALE_THRESHOLD = "MarketZipcodeThreshold";

public CoreMarketPenetrationRetriever( DataSource dataSource )
{
	super( dataSource, STORED_PROC_NAME );
	setFunction( true );
	declareParameter( new SqlReturnResultSet( PARAM_RESULT_SET, new CoreMarketPenetrationResultSetExtractor() ) );
	declareParameter( new SqlOutParameter( PARAM_RC, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_BUSINESSUNIT_ID, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_BASIS_PERIOD_DATE, Types.DATE ) );
	declareParameter( new SqlParameter( PARAM_VEHICLE_SALE_THRESHOLD, Types.DOUBLE ) );
	compile();
}

public Map<Integer,Double> retrieveCoreMarketPenetration( Integer businessUnitId, Date basisPeriodDate, Double vehicleSaleThreshold )
{
	Map< String, Object > inParams = new HashMap< String, Object >( 1 );
	inParams.put( PARAM_BUSINESSUNIT_ID, businessUnitId );
	inParams.put( PARAM_BASIS_PERIOD_DATE, new Timestamp( basisPeriodDate.getTime() ) );
	inParams.put( PARAM_VEHICLE_SALE_THRESHOLD, vehicleSaleThreshold );

	Map out = execute( inParams );
	
	return (Map<Integer,Double>)out.get( PARAM_RESULT_SET );
}

private class CoreMarketPenetrationResultSetExtractor implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	Map< Integer, Double > results = new HashMap< Integer, Double >();
	Integer groupingDescId = null;
	Double marketPenetration = null;

	while ( rs.next() )
	{
		groupingDescId = new Integer( rs.getInt( "GroupingDescriptionId" ) );
		marketPenetration = new Double( rs.getDouble( "CoreMarketPenetration" ) );

		results.put( groupingDescId, marketPenetration );
	}

	return results;
}

}

}
