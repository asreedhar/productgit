package biz.firstlook.cia.persistence;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import biz.firstlook.cia.db.CIADatabaseUtil;
import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.Status;
import biz.firstlook.commons.sql.RuntimeDatabaseException;

public class CIABodyTypeDetailPersistence
{

public CIABodyTypeDetail findBySegmentAndSummaryId( Integer segmentId, Integer summaryId )
{
	try
	{
		List results = CIADatabaseUtil.instance().find(
														"from biz.firstlook.cia.model.CIABodyTypeDetail "
																+ " where segmentId = ?"
																+ " and ciaSummaryId = ?", new Object[] { segmentId, summaryId },
														new Type[] { Hibernate.INTEGER, Hibernate.INTEGER } );

		if ( results != null && !results.isEmpty() )
		{
			return (CIABodyTypeDetail)results.get( 0 );
		}
		else
		{
			return null;
		}
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving CIABodyTypeDetail by display body type id and summary id: " + e );
	}
}

public CIABodyTypeDetail findByDealerIdStatusAndSegment( Integer dealerId, Status status, Integer segmentId )
{
	try
	{
		List results = CIADatabaseUtil.instance().find(
														"select detail from biz.firstlook.cia.model.CIABodyTypeDetail detail, biz.firstlook.cia.model.CIASummary summary "
																+ " where detail.segment.segmentId = ?"
																+ " and detail.ciaSummaryId = summary.ciaSummaryId"
																+ " and summary.businessUnitId = ?"
																+ " and summary.status.statusId = ?", 
																new Object[] { segmentId, dealerId, status.getStatusId() },
														new Type[] { Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.INTEGER } );

		if ( results != null && !results.isEmpty() )
		{
			return (CIABodyTypeDetail)results.get( 0 );
		}
		else
		{
			return null;
		}
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving CIABodyTypeDetail by display body type id, dealer id and status: " + e );
	}
}


}
