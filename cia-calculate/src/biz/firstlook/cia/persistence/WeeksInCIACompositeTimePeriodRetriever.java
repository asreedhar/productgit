package biz.firstlook.cia.persistence;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

public class WeeksInCIACompositeTimePeriodRetriever
{

private DataSource dataSource;
public static final int SUCCESS_RETURN_CODE = 0;
public static final String ResultSet = "ResultSet";
public static final String ReturnCode = "@RC";

public Map call( Integer ciaCompositeTimePeriodID, Integer isForecast )
{
    Map storedProcParameters = new HashMap();
    storedProcParameters.put( "@CiaCompositeTimePeriodID", ciaCompositeTimePeriodID );
    storedProcParameters.put( "@IsForecast", isForecast );
    return call( storedProcParameters );
}

private Map call( Map sprocParameterValues ) throws DataAccessException
{
    List storedProcedureParameters = new ArrayList();
    /*
     * This is a set of return values. Typically when doing a prepared statment,
     * you get a ResultSet object. When getting Out parameters from a sproc, you
     * get a ResultSet object AND the return code, and that is why this is
     * needed. THIS MUST BE ADDED FIRST!
     */
    storedProcedureParameters.add( new SqlReturnResultSet( WeeksInCIACompositeTimePeriodRetriever.ResultSet,
                                                           new WeeksInCIACompositeTimePeriodMapper() ) );

    // This stores the return code from a stored proc
    storedProcedureParameters.add( new SqlOutParameter( "@RC", Types.INTEGER ) );

    // Parameters of a stored proc
    storedProcedureParameters.add( new SqlParameter( "@CiaCompositeTimePeriodID", Types.INTEGER ) );
    storedProcedureParameters.add( new SqlParameter( "@IsForecast", Types.INTEGER ) );

    // the statement to execute
    CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{? = call GetWeeksInCIACompositeTimePeriod (?, ?)}",
                                                                                storedProcedureParameters );
    CallableStatementCreator csc = cscf.newCallableStatementCreator( sprocParameterValues );
    JdbcTemplate jdbcTemplate = new JdbcTemplate( dataSource );
    return jdbcTemplate.call( csc, storedProcedureParameters );
}

public DataSource getDataSource()
{
    return dataSource;
}

public void setDataSource( DataSource dataSource )
{
    this.dataSource = dataSource;
}

}
