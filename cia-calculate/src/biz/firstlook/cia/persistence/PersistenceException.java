package biz.firstlook.cia.persistence;

public class PersistenceException extends Exception
{

private static final long serialVersionUID = 1507088475472315345L;

public PersistenceException()
{
    super();
}

public PersistenceException( String arg0 )
{
    super(arg0);
}

public PersistenceException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public PersistenceException( Throwable arg0 )
{
    super(arg0);
}

}
