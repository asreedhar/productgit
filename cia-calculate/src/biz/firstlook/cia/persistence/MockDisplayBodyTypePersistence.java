package biz.firstlook.cia.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.persistence.ISegmentDAO;

class MockSegmentPersistence implements ISegmentDAO
{

private Map<Integer, Segment> returnTypes;

public MockSegmentPersistence()
{
	super();
	returnTypes = new HashMap<Integer, Segment>();
	
	Segment dbt1 = new Segment();
	dbt1.setSegmentId( new Integer( 1 ) );
	Segment dbt2 = new Segment();
	dbt2.setSegmentId( new Integer( 2 ) );
	Segment dbt3 = new Segment();
	dbt3.setSegmentId( new Integer( 3 ) );
	Segment dbt4 = new Segment();
	dbt4.setSegmentId( new Integer( 4 ) );
	Segment dbt5 = new Segment();
	dbt5.setSegmentId( new Integer( 5 ) );
	Segment dbt6 = new Segment();
	dbt6.setSegmentId( new Integer( 6 ) );
	Segment dbt7 = new Segment();
	dbt7.setSegmentId( new Integer( 7 ) );
	Segment dbt8 = new Segment();
	dbt8.setSegmentId( new Integer( 8 ) );
	
	returnTypes.put( dbt1.getSegmentId(), dbt1 );
	returnTypes.put( dbt2.getSegmentId(), dbt2 );
	returnTypes.put( dbt3.getSegmentId(), dbt3 );
	returnTypes.put( dbt4.getSegmentId(), dbt4 );
	returnTypes.put( dbt5.getSegmentId(), dbt5 );
	returnTypes.put( dbt6.getSegmentId(), dbt6 );
	returnTypes.put( dbt7.getSegmentId(), dbt7 );
	returnTypes.put( dbt8.getSegmentId(), dbt8 );
}

public List<Segment> findAll()
{
	List<Segment> allSegments = new ArrayList<Segment>();
	allSegments.addAll( returnTypes.values() );
	return allSegments;
}

public Segment findById( Integer segmentId )
{
	return (Segment)returnTypes.get( segmentId );
}

}
