package biz.firstlook.cia.persistence;

import org.springframework.dao.DataAccessException;

import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;

public interface ICIASummaryDAO
{

public abstract CIASummary retrieveCIASummary( Integer dealerId, Status status );

public abstract void save( CIASummary ciaSummary ) throws DataAccessException;

public abstract void update( CIASummary ciaSummary ) throws DataAccessException;

public abstract Integer retrieveCIASummaryId( Integer integer, Status status );

}