package biz.firstlook.cia.persistence;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.Status;

import com.firstlook.data.RuntimeDatabaseException;

public class CIAGroupingItemDAO extends HibernateDaoSupport implements ICIAGroupingItemDAO
{

public CIAGroupingItemDAO()
{
	super();
}

public CIAGroupingItem findByCIAGroupingItemId( Integer CIAGroupingItemId )
{
	try
	{
		return (CIAGroupingItem)getHibernateTemplate().load( CIAGroupingItem.class, CIAGroupingItemId );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving Grouping Item by summary id: ", e );
	}
}

public CIAGroupingItem findNonLazyByCIAGroupingItemId( final Integer CIAGroupingItemId )
{
	return (CIAGroupingItem)getHibernateTemplate().execute( new HibernateCallback()
	{

		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Criteria criteria = session.createCriteria( CIAGroupingItem.class );
			criteria.add( Expression.eq( "ciaGroupingItemId", CIAGroupingItemId ) );
			criteria.setFetchMode( "ciaGroupingItemDetails", FetchMode.JOIN );
			List list = criteria.list();
			if ( list != null && !list.isEmpty() )
			{
				return (CIAGroupingItem)list.get( 0 );
			}
			else
			{
				return null;
			}
		}

	} );
}

@SuppressWarnings("unchecked")
public List<CIAGroupingItem> findBy( Integer ciaSummaryId, Integer bodyTypeId, Integer ciaCategoryId )
{
	try
	{
		return getHibernateTemplate().find(
											"select cgi from biz.firstlook.cia.model.CIAGroupingItem cgi, biz.firstlook.cia.model.CIABodyTypeDetail btd"
													+ " where btd.ciaSummaryId = ?" + " and btd.segment.segmentId = ?"
													+ " and cgi.ciaCategory.ciaCategoryId = ?"
													+ " and cgi.ciaBodyTypeDetail.ciaBodyTypeDetailId = btd.ciaBodyTypeDetailId",
											new Object[] { ciaSummaryId, bodyTypeId, ciaCategoryId } );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving Grouping Item by summary id, bodyTypeId and ciaCategoryId: ", e );
	}
}

public List<CIAGroupingItem> findCoreGroupingsByDealerIdAndStatus( final Integer dealerId, final Integer ciaSummaryStatusId )
{
	Integer[] ciaCategories = new Integer[] { CIACategory.POWERZONE.getCiaCategoryId(), CIACategory.WINNERS.getCiaCategoryId(),
			CIACategory.GOODBETS.getCiaCategoryId() };
	final String ciaCategoryStr = StringUtils.join( ciaCategories, "," );

	return (List<CIAGroupingItem>) getHibernateTemplate().execute( new HibernateCallback()
	{

		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( "select item from biz.firstlook.cia.model.CIAGroupingItem item, "
					+ "biz.firstlook.cia.model.CIABodyTypeDetail detail, " + "biz.firstlook.cia.model.CIASummary summary "
					+ " where detail.ciaSummaryId = summary.ciaSummaryId" + " and summary.businessUnitId = " + dealerId
					+ " and summary.status.statusId = " + ciaSummaryStatusId
					+ " and detail.ciaBodyTypeDetailId = item.ciaBodyTypeDetail.ciaBodyTypeDetailId "
					+ " and item.ciaCategory.ciaCategoryId IN (" + ciaCategoryStr + ")" );
			return query.list();
		}

	} );
}

public void update( CIAGroupingItem ciaGroupingItem )
{
	try
	{
		getHibernateTemplate().update( ciaGroupingItem );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Problem updating ciaGroupingItem", e );
	}
}

public void saveOrUpdate( CIAGroupingItem ciaGroupingItem )
{
	try
	{
		getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
		getHibernateTemplate().saveOrUpdate( ciaGroupingItem );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Problem updating ciaGroupingItem", e );
	}
}

public CIAGroupingItem findByGroupingDescriptionSegmentAndCIACategory( Integer segmentId, Integer ciaCategoryId,
																				Integer groupingDescriptionId, Integer dealerId )
{
	try
	{
		List results = getHibernateTemplate().find(
													"select cgi from biz.firstlook.cia.model.CIAGroupingItem cgi, biz.firstlook.cia.model.CIABodyTypeDetail btd,"
															+ " biz.firstlook.cia.model.CIASummary summary"
															+ " where summary.businessUnitId = ?" + " and summary.status.statusId = ?"
															+ " and btd.segment.segmentId = ?"
															+ " and cgi.ciaBodyTypeDetail.ciaBodyTypeDetailId = btd.ciaBodyTypeDetailId"
															+ " and cgi.ciaBodyTypeDetail.ciaSummaryId = summary.ciaSummaryId"
															+ " and cgi.ciaCategory.ciaCategoryId = ? "
															+ " and cgi.groupingDescription.groupingDescriptionId = ?",
													new Object[] { dealerId, Status.CURRENT.getStatusId(), segmentId, ciaCategoryId,
															groupingDescriptionId } );
		if ( results != null && !results.isEmpty() )
		{
			return (CIAGroupingItem)results.get( 0 );
		}
		else
		{
			return null;
		}
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving Grouping Item by segmentId, ciaCategoryid, and GroupingdescriptionID ", e );
	}
}

public CIAGroupingItem findByGroupingDescription( Integer groupingDescriptionId, Integer dealerId )
{
	try
	{
		List results = getHibernateTemplate().find(
													"select cgi from biz.firstlook.cia.model.CIAGroupingItem cgi, biz.firstlook.cia.model.CIABodyTypeDetail btd,"
															+ " biz.firstlook.cia.model.CIASummary summary"
															+ " where summary.businessUnitId = ?" + " and summary.status.statusId = ?"
															+ " and cgi.ciaBodyTypeDetail.ciaBodyTypeDetailId = btd.ciaBodyTypeDetailId"
															+ " and cgi.ciaBodyTypeDetail.ciaSummaryId = summary.ciaSummaryId"
															+ " and cgi.groupingDescription.groupingDescriptionId = ?",
													new Object[] { dealerId, Status.CURRENT.getStatusId(), groupingDescriptionId } );
		if ( results != null && !results.isEmpty() )
		{
			return (CIAGroupingItem)results.get( 0 );
		}
		else
		{
			return null;
		}
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error retrieving Grouping Item by groupingdescriptionID ", e );
	}
}

public Collection findDistinctCIAGroupingItemsWithRecommendationsByCIACategory( Integer businessUnitId, Integer ciaSummaryStatusId,
																				Integer ciaCategoryId )
{
	Collection items = getHibernateTemplate().find(
													" select distinct item from CIAGroupingItem as item, CIAGroupingItemDetail as itemDetail,"
															+ " CIABodyTypeDetail as typeDetail, CIASummary as summary where"
															+ " summary.businessUnitId = ? and summary.status = ?"
															+ " and itemDetail.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = ?"
															+ " and summary.ciaSummaryId = typeDetail.ciaSummaryId"
															+ " and item.ciaBodyTypeDetail.ciaBodyTypeDetailId = typeDetail.ciaBodyTypeDetailId"
															+ " and item.ciaGroupingItemId = itemDetail.ciaGroupingItem.ciaGroupingItemId"
															+ " and item.ciaCategory.ciaCategoryId = ?" + " order by item.valuation",
													new Object[] { businessUnitId, ciaSummaryStatusId,
															new Integer( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE ), ciaCategoryId } );

	return items;
}

}