package biz.firstlook.cia.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockRetailUnitSalesRetriever implements IRetailUnitSalesRetriever
{

private List unitSales = new ArrayList();
private double totalRevenue;
private int unitsSold;

public MockRetailUnitSalesRetriever()
{
}

public void add( Object[] sales )
{
    unitSales.add(sales);
}

public List calculateGroupingDescriptionTotalRevenueByClassType(
        Integer classTypeId, Integer dealerId, Date startDate, Date endDate )
        throws PersistenceException
{
    return unitSales;
}

public double calculateTotalRevenueByDealer( Integer dealerId, Date startDate,
        Date endDate ) throws PersistenceException
{
    return totalRevenue;
}

public void setTotalRevenue( double d )
{
    totalRevenue = d;
}

public List calculateGroupingDescriptionUnitsSoldByClassType(
        Integer classTypeId, Integer dealerId, Date startDate, Date endDate )
        throws PersistenceException
{
    return unitSales;
}

public List calculateProfitableGroupingDescriptionUnitsSoldByClassType(
        Integer classTypeId, Integer dealerId, Date startDate, Date endDate )
        throws PersistenceException
{
    return unitSales;
}

public int calculateUnitsSoldForDealer( Integer dealerId, Date startDate,
        Date endDate ) throws PersistenceException
{
    return unitsSold;
}

public void setUnitsSold( int unitsSold )
{
    this.unitsSold = unitsSold;
}
}
