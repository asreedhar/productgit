package biz.firstlook.cia.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

public class BucketronExecutor
{

//TODO: put this in a more generic class handling sproc calls.
public static final int SUCCESS_RETURN_CODE = 0;

public static final String Result_Set = "Result_Set";
public static final String Return_Code = "@RC";
private List<SqlParameter> storedProcedureParameters;
private CallableStatementCreatorFactory cscf;

public BucketronExecutor()
{
	storedProcedureParameters = new ArrayList<SqlParameter>();

	/*
	 * This is a set of return values. Typically when doing a prepared statment,
	 * you get a ResultSet object. When getting Out parameters from a sproc, you
	 * get a ResultSet object AND the return code, and that is why this is
	 * needed.  THIS MUST BE ADDED FIRST!
	 */
	storedProcedureParameters.add( new SqlReturnResultSet( BucketronExecutor.Result_Set, new BucketronRowMapper() ) );
	
	// This stores the return code from a stored proc
	storedProcedureParameters.add( new SqlOutParameter( "@RC", Types.INTEGER ) );

	// Parameters of a stored proc
	storedProcedureParameters.add( new SqlParameter( "@BusinessUnitID", Types.INTEGER ) );
	storedProcedureParameters.add( new SqlParameter( "@GroupingDescriptionID", Types.INTEGER ) );
	storedProcedureParameters.add( new SqlParameter( "@TargetUnits", Types.INTEGER ) );
	storedProcedureParameters.add( new SqlParameter( "@BaseDate", Types.DATE ) );

	//the statement to execute
	cscf = new CallableStatementCreatorFactory( "{? = call GetCIABucketedAllocationByYear (?,?,?,?)}", storedProcedureParameters );
}

public Map call( JdbcTemplate jdbcTemplate, Map sprocParameterValues ) throws DataAccessException
{
	CallableStatementCreator csc = cscf.newCallableStatementCreator( sprocParameterValues );
	return jdbcTemplate.call( csc, storedProcedureParameters );
}

class BucketronRowMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
{
	BucketronRowResult bucket = new BucketronRowResult();
	bucket.setDescription( rs.getString( "Description" ) );
	bucket.setAllocationUnits( rs.getInt( "AllocatedUnits" ) );
	return bucket;
}
}

}
