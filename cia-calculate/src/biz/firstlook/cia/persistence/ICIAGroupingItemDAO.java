package biz.firstlook.cia.persistence;

import java.util.Collection;
import java.util.List;

import biz.firstlook.cia.model.CIAGroupingItem;

public interface ICIAGroupingItemDAO 
{
public abstract CIAGroupingItem findByCIAGroupingItemId( Integer CIAGroupingItemId );

public abstract List<CIAGroupingItem> findBy( Integer ciaSummaryId, Integer segmentId, Integer ciaCategory );

public CIAGroupingItem findByGroupingDescriptionSegmentAndCIACategory( Integer segmentId, Integer ciaCategoryId, Integer groupingDescriptionId, Integer dealerId );

public CIAGroupingItem findByGroupingDescription( Integer groupingDescriptionId, Integer dealerId );

public List<CIAGroupingItem> findCoreGroupingsByDealerIdAndStatus( Integer dealerId, Integer ciaSummaryStatusId );

public abstract void update( CIAGroupingItem ciaGroupingItem );

public abstract void saveOrUpdate( CIAGroupingItem ciaGroupingItem );

public Collection findDistinctCIAGroupingItemsWithRecommendationsByCIACategory( Integer businessUnitId, Integer ciaSummaryStatusId, Integer ciaCategoryId );

public CIAGroupingItem findNonLazyByCIAGroupingItemId( Integer CIAGroupingItemId );
}