package biz.firstlook.cia.model;

import java.io.Serializable;

public class CIAGroupingItemDetailType implements Serializable
{

private static final long serialVersionUID = -3731916730571372471L;
public static final int NO_PREFERENCE_TYPE = 0;
public static final int PREFER_TYPE = 1;
public static final int AVOID_TYPE = 2;
public static final int BUY_TYPE = 3;
public static final int OPTIMAL_STOCKING_LEVEL_TYPE = 4;

public static final CIAGroupingItemDetailType NO_PREFERENCE = new CIAGroupingItemDetailType( NO_PREFERENCE_TYPE, "No Preference" );
public static final CIAGroupingItemDetailType PREFER = new CIAGroupingItemDetailType( PREFER_TYPE, "Prefer" );
public static final CIAGroupingItemDetailType AVOID = new CIAGroupingItemDetailType( AVOID_TYPE, "Avoid" );
public static final CIAGroupingItemDetailType BUY = new CIAGroupingItemDetailType( BUY_TYPE, "Buy" );
public static final CIAGroupingItemDetailType OPTIMAL_STOCKING_LEVEL = new CIAGroupingItemDetailType(	OPTIMAL_STOCKING_LEVEL_TYPE,
																										"OptimalStockingLevel" );

private Integer ciaGroupingItemDetailTypeId;
private String description;

public CIAGroupingItemDetailType()
{
	super();
}

public CIAGroupingItemDetailType( int ciaGroupingItemDetailTypeId, String description )
{
	this.ciaGroupingItemDetailTypeId = new Integer( ciaGroupingItemDetailTypeId );
	this.description = description;
}

public Integer getCiaGroupingItemDetailTypeId()
{
	return ciaGroupingItemDetailTypeId;
}

public void setCiaGroupingItemDetailTypeId( Integer ciaGroupingItemDetailTypeId )
{
	this.ciaGroupingItemDetailTypeId = ciaGroupingItemDetailTypeId;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public static CIAGroupingItemDetailType constructTypeFromDescription( String description )
{
	if( description.equalsIgnoreCase( PREFER.getDescription() ) )
	{
		return PREFER;
	}
	if( description.equalsIgnoreCase( AVOID.getDescription() ) )
	{
		return AVOID;
	}
	if( description.equalsIgnoreCase( NO_PREFERENCE.getDescription() ) )
	{
		return NO_PREFERENCE;
	}
	if( description.equalsIgnoreCase( OPTIMAL_STOCKING_LEVEL.getDescription() ) )
	{
		return OPTIMAL_STOCKING_LEVEL;
	}
	if( description.equalsIgnoreCase( BUY.getDescription() ) )
	{
		return BUY;
	}
	return null;

}

public boolean equals( Object obj )
{
	if ( obj instanceof CIAGroupingItemDetailType )
	{
		CIAGroupingItemDetailType otherCIAGroupingItemDetailType = (CIAGroupingItemDetailType)obj;
		return otherCIAGroupingItemDetailType.getCiaGroupingItemDetailTypeId().intValue() == this.ciaGroupingItemDetailTypeId.intValue();
	}
	
	return false;
}

}
