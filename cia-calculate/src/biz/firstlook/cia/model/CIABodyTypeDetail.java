package biz.firstlook.cia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.transact.persist.model.Segment;

public class CIABodyTypeDetail implements ITargetUnits, Serializable, Comparable<CIABodyTypeDetail>
{

private static final long serialVersionUID = -4490514430943078789L;
private Integer ciaBodyTypeDetailId;
private Segment segment;
private Integer nonCoreTargetUnits;
private Integer displaySortOrder;
private Integer ciaSummaryId;
private Set<CIAGroupingItem> ciaGroupingItems;

private transient double valuation;

public CIABodyTypeDetail()
{
	super();
	nonCoreTargetUnits = new Integer( 0 );
	ciaGroupingItems = new HashSet<CIAGroupingItem>();
}

public Integer getCiaBodyTypeDetailId()
{
	return ciaBodyTypeDetailId;
}

public void setCiaBodyTypeDetailId( Integer ciaBodyTypeDetailId )
{
	this.ciaBodyTypeDetailId = ciaBodyTypeDetailId;
}

public Integer getNonCoreTargetUnits()
{
	return nonCoreTargetUnits;
}

public void setNonCoreTargetUnits( Integer nonCoreTargetUnits )
{
	this.nonCoreTargetUnits = nonCoreTargetUnits;
}

public Integer getDisplaySortOrder()
{
	return displaySortOrder;
}

public void setDisplaySortOrder( Integer displaySortOrder )
{
	this.displaySortOrder = displaySortOrder;
}

public Segment getSegment()
{
	return segment;
}

public void setSegment( Segment segment )
{
	this.segment = segment;
}

public Set<CIAGroupingItem> getCiaGroupingItems()
{
	return ciaGroupingItems;
}

public void setCiaGroupingItems( Set<CIAGroupingItem> ciaGroupingItems )
{
	this.ciaGroupingItems = ciaGroupingItems;
}

public double getValuation()
{
	return valuation;
}

public void setValuation( double valuation )
{
	this.valuation = valuation;
}

public void setTotalTargetUnits( int targetUnits )
{
	nonCoreTargetUnits = targetUnits;
}

public int getTotalTargetUnits()
{
	return nonCoreTargetUnits;
}

public List getCiaGroupingItemsByCIACategory( int ciaCategoryId )
{
	List<CIAGroupingItem> ciaCategoryItems = new ArrayList<CIAGroupingItem>();
	Iterator ciaGroupingItemsIter  = getCiaGroupingItems().iterator();
	CIAGroupingItem item = null;
	while( ciaGroupingItemsIter.hasNext() )
	{
		item = (CIAGroupingItem)ciaGroupingItemsIter.next();
		if( item.getCiaCategory().getCiaCategoryId().intValue() == ciaCategoryId )
		{
			ciaCategoryItems.add( item );
		}
	}
	Collections.sort(ciaCategoryItems);
	
	return null;
}

public Integer getCiaSummaryId()
{
	return ciaSummaryId;
}

public void setCiaSummaryId( Integer ciaSummaryId )
{
	this.ciaSummaryId = ciaSummaryId;
}

public int compareTo(CIABodyTypeDetail other) {
	return Functions.nullSafeCompareTo(getDisplaySortOrder(), other.getDisplaySortOrder());
}

}
