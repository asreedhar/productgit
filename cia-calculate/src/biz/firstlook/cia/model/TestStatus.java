package biz.firstlook.cia.model;

import junit.framework.TestCase;

public class TestStatus extends TestCase
{

private Status status;

public TestStatus( String name )
{
    super(name);
}

public void setUp()
{
    status = new Status();
}

public void testGetNextStatusCurrent()
{
    status = Status.CURRENT;

    assertEquals(Status.PRIOR, status.getNextStatus());
}

public void testGetNextStatusPrior()
{
    status = Status.PRIOR;

    assertEquals(Status.INACTIVE, status.getNextStatus());
}

public void testGetNextStatusInActive()
{
    status = Status.INACTIVE;

    assertEquals(Status.INACTIVE, status.getNextStatus());
}

}
