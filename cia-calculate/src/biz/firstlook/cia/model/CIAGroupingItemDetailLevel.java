package biz.firstlook.cia.model;

import java.io.Serializable;

public class CIAGroupingItemDetailLevel implements Serializable
{
private static final long serialVersionUID = 5849312388986006938L;
public static final int COLOR_DETAIL_LEVEL = 1;
public static final int YEAR_DETAIL_LEVEL = 2;
public static final int TRIM_DETAIL_LEVEL = 3;
public static final int UNITCOST_DETAIL_LEVEL = 4;
public static final int GROUPING_DETAIL_LEVEL = 5;

public static final CIAGroupingItemDetailLevel COLOR = new CIAGroupingItemDetailLevel( COLOR_DETAIL_LEVEL, "Color" );
public static final CIAGroupingItemDetailLevel YEAR = new CIAGroupingItemDetailLevel( YEAR_DETAIL_LEVEL, "Year" );
public static final CIAGroupingItemDetailLevel TRIM = new CIAGroupingItemDetailLevel( TRIM_DETAIL_LEVEL, "Trim" );
public static final CIAGroupingItemDetailLevel UNIT_COST = new CIAGroupingItemDetailLevel( UNITCOST_DETAIL_LEVEL, "UnitCost" );
public static final CIAGroupingItemDetailLevel GROUPING = new CIAGroupingItemDetailLevel( GROUPING_DETAIL_LEVEL, "Grouping" );

private Integer ciaGroupingItemDetailLevelId;
private String description;

public CIAGroupingItemDetailLevel()
{
	super();
}

public CIAGroupingItemDetailLevel( int ciaGroupingItemDetailLevelId, String description )
{
	this.ciaGroupingItemDetailLevelId = new Integer( ciaGroupingItemDetailLevelId );
	this.description = description;
}

public Integer getCiaGroupingItemDetailLevelId()
{
	return ciaGroupingItemDetailLevelId;
}

public void setCiaGroupingItemDetailLevelId( Integer groupingItemDetailLevelId )
{
	ciaGroupingItemDetailLevelId = groupingItemDetailLevelId;
}

public String getDescription()
{
	return description;
}

public void setDescription( String description )
{
	this.description = description;
}

public static CIAGroupingItemDetailLevel constructLevelFromDescription( String desc )
{
	if ( desc.equalsIgnoreCase( COLOR.getDescription() ) )
	{
		return COLOR;
	}
	else if ( desc.equalsIgnoreCase( YEAR.getDescription() ) )
	{
		return YEAR;
	}
	else if ( desc.equalsIgnoreCase( TRIM.getDescription() ) )
	{
		return TRIM;
	}
	else if ( desc.equalsIgnoreCase( UNIT_COST.getDescription() ) )
	{
		return UNIT_COST;
	}
	else if ( desc.equalsIgnoreCase( GROUPING.getDescription() ) )
	{
		return GROUPING;
	}
	else
	{

		return null;
	}

}

public static String getDescriptionFromID( int id )
{
	if ( id == COLOR.ciaGroupingItemDetailLevelId.intValue() )
	{
		return COLOR.getDescription();
	}
	if ( id == YEAR.ciaGroupingItemDetailLevelId.intValue() )
	{
		return YEAR.getDescription();
	}
	if ( id == TRIM.ciaGroupingItemDetailLevelId.intValue() )
	{
		return TRIM.getDescription();
	}
	if ( id == UNIT_COST.ciaGroupingItemDetailLevelId.intValue() )
	{
		return UNIT_COST.getDescription();
	}
	if ( id == GROUPING.ciaGroupingItemDetailLevelId.intValue() )
	{
		return GROUPING.getDescription();
	}

	return "Unknown DescriptionID";

}

public boolean equals( Object obj )
{
	if ( obj instanceof CIAGroupingItemDetailLevel )
	{
		CIAGroupingItemDetailLevel otherCIAGroupingItemDetailLevel = (CIAGroupingItemDetailLevel)obj;
		return otherCIAGroupingItemDetailLevel.getCiaGroupingItemDetailLevelId().intValue() == this.ciaGroupingItemDetailLevelId.intValue();
	}

	return false;
}

}
