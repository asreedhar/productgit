package biz.firstlook.cia.model;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ColorItem
{
private String color;
private Integer unitsSold;
private Integer averageDaysToSale;
private Integer averageGrossProfit;
private Integer noSales;
private Integer unitsInStock;

public Integer getAverageDaysToSale()
{
    return averageDaysToSale;
}

public Integer getAverageGrossProfit()
{
    return averageGrossProfit;
}

public Integer getNoSales()
{
    return noSales;
}

public Integer getUnitsInStock()
{
    return unitsInStock;
}

public Integer getUnitsSold()
{
    return unitsSold;
}

public void setAverageDaysToSale( Integer integer )
{
    averageDaysToSale = integer;
}

public void setAverageGrossProfit( Integer int1 )
{
    averageGrossProfit = int1;
}

public void setNoSales( Integer integer )
{
    noSales = integer;
}

public void setUnitsInStock( Integer integer )
{
    unitsInStock = integer;
}

public void setUnitsSold( Integer integer )
{
    unitsSold = integer;
}

public String getColor()
{
    return color;
}

public void setColor( String string )
{
    color = string;
}

public String toString()
{
    return ToStringBuilder.reflectionToString(this);
}

}
