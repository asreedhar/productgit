package biz.firstlook.cia.model;

public class MarketPerformerGrouping
{

private int groupingDescriptionId;
private int segmentId;
private double marketShare;
private double dealerPenetration;
private int light;
private int inBrand;

public MarketPerformerGrouping()
{
}

public MarketPerformerGrouping( int groupingDescriptionId, int light,
        int inBrand, double marketShare, double dealerPenetration )
{
    setGroupingDescriptionId(groupingDescriptionId);
    setLight(light);
    setInBrand(inBrand);
    setMarketShare(marketShare);
    setDealerPenetration(dealerPenetration);
}

public double getDealerPenetration()
{
    return dealerPenetration;
}

public int getSegmentId()
{
    return segmentId;
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public int getInBrand()
{
    return inBrand;
}

public int getLight()
{
    return light;
}

public double getMarketShare()
{
    return marketShare;
}

public void setDealerPenetration( double d )
{
    dealerPenetration = d;
}

public void setSegmentId( int i )
{
    segmentId = i;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public void setInBrand( int i )
{
    inBrand = i;
}

public void setLight( int i )
{
    light = i;
}

public void setMarketShare( double d )
{
    marketShare = d;
}

}
