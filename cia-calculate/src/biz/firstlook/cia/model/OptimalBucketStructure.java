package biz.firstlook.cia.model;

import org.apache.commons.lang.math.IntRange;

public class OptimalBucketStructure
{

private double standardDeviation;
private double meanPercentage;
private double medianUnitCost;
private double bucketSize;
private int numOfBuckets;
private double[] rangeCounts;
private IntRange[] actualRanges;

public OptimalBucketStructure()
{
    super();
}

public double getMedianUnitCost()
{
    return medianUnitCost;
}

public double getStandardDeviation()
{
    return standardDeviation;
}

public void setMedianUnitCost( double d )
{
    medianUnitCost = d;
}

public void setStandardDeviation( double d )
{
    standardDeviation = d;
}

public double getBucketSize()
{
    return bucketSize;
}

public int getNumOfBuckets()
{
    return numOfBuckets;
}

public void setBucketSize( double i )
{
    bucketSize = i;
}

public void setNumOfBuckets( int i )
{
    numOfBuckets = i;
}

public double getMeanPercentage()
{
    return meanPercentage;
}

public double[] getRangeCounts()
{
    return rangeCounts;
}

public void setMeanPercentage( double d )
{
    meanPercentage = d;
}

public void setRangeCounts( double[] ds )
{
    rangeCounts = ds;
}

public IntRange[] getActualRanges()
{
    return actualRanges;
}

public void setActualRanges( IntRange[] ranges )
{
    this.actualRanges = ranges;
}

}
