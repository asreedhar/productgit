package biz.firstlook.cia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;

public class CIASummary implements Serializable
{

private static final long serialVersionUID = -8981920358899178926L;
private Integer ciaSummaryId;
private Integer businessUnitId;
private Status status;
private Date dateCreated;

// unidirectional one-to-many mapping.
private Set<CIABodyTypeDetail> ciaBodyTypeDetails;

public CIASummary()
{
}

public CIASummary( int businessUnitId )
{
	this.businessUnitId = new Integer( businessUnitId );
	ciaSummaryId = null;
	status = Status.CURRENT;
	dateCreated = new Date();
	
	ciaBodyTypeDetails = new HashSet<CIABodyTypeDetail>();
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Set<CIABodyTypeDetail> getCiaBodyTypeDetails()
{
	return ciaBodyTypeDetails;
}

public void setCiaBodyTypeDetails( Set<CIABodyTypeDetail> ciaBodyTypeDetails )
{
	this.ciaBodyTypeDetails = ciaBodyTypeDetails;
}

public Integer getCiaSummaryId()
{
	return ciaSummaryId;
}

public void setCiaSummaryId( Integer ciaSummaryId )
{
	this.ciaSummaryId = ciaSummaryId;
}

public Date getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Date dateCreated )
{
	this.dateCreated = dateCreated;
}

public Status getStatus()
{
	return status;
}

public void setStatus( Status status )
{
	this.status = status;
}

public boolean isEmpty()
{
	return ( ciaSummaryId == null ) || ( businessUnitId == null ) || ( status == null );
}

public String toString()
{
	return ToStringBuilder.reflectionToString( this );
}

}
