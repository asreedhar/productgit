package biz.firstlook.cia.model;

import java.io.Serializable;

import biz.firstlook.commons.util.Functions;

public class CIACategory implements Serializable, Comparable<CIACategory>
{

private static final long serialVersionUID = -7568636171308980575L;
public final static int POWERZONE_CATEGORY = 1;
public final static int WINNERS_CATEGORY = 2;
public final static int GOODBETS_CATEGORY = 3;
public final static int MARKETPERFORMERS_CATEGORY = 4;
public final static int MANAGERSCHOICE_CATEGORY = 5;
public final static int NON_POWERZONE_CATEGORY = 6;

public final static CIACategory POWERZONE = new CIACategory( POWERZONE_CATEGORY, "Power Zone" );
public final static CIACategory WINNERS = new CIACategory( WINNERS_CATEGORY, "Winners" );
public final static CIACategory GOODBETS = new CIACategory( GOODBETS_CATEGORY, "Good Bets" );
public final static CIACategory MARKET_PERFORMERS = new CIACategory( MARKETPERFORMERS_CATEGORY, "Market Performers" );
public final static CIACategory MANAGERS_CHOICE = new CIACategory( MANAGERSCHOICE_CATEGORY, "Managers Choice" );

private Integer ciaCategoryId;
private String ciaCategoryDescription;

public CIACategory()
{
	super();
}

public CIACategory( int ciaCategoryId, String ciaCategoryDescription )
{
	this.ciaCategoryId = new Integer( ciaCategoryId );
	this.ciaCategoryDescription = ciaCategoryDescription;
}

public String getCiaCategoryDescription()
{
	return ciaCategoryDescription;
}

public Integer getCiaCategoryId()
{
	return ciaCategoryId;
}

public void setCiaCategoryDescription( String string )
{
	ciaCategoryDescription = string;
}

public void setCiaCategoryId( Integer integer )
{
	ciaCategoryId = integer;
}

public static CIACategory retrieveCIACategory( int ciaCategoryId )
{
	CIACategory ciaCategory = null;
	switch ( ciaCategoryId )
	{
		case CIACategory.POWERZONE_CATEGORY:
			ciaCategory = CIACategory.POWERZONE;
			break;
		case CIACategory.WINNERS_CATEGORY:
			ciaCategory = CIACategory.WINNERS;
			break;
		case CIACategory.GOODBETS_CATEGORY:
			ciaCategory = CIACategory.GOODBETS;
			break;
		case CIACategory.MARKETPERFORMERS_CATEGORY:
			ciaCategory = CIACategory.MARKET_PERFORMERS;
			break;
		case CIACategory.MANAGERSCHOICE_CATEGORY:
			ciaCategory = CIACategory.MANAGERS_CHOICE;
			break;
		default:
			ciaCategory = null;
			break;
	}
	return ciaCategory;
}

public int compareTo(CIACategory other) {
	return Functions.nullSafeCompareTo(getCiaCategoryId(), other.getCiaCategoryId());
}

}
