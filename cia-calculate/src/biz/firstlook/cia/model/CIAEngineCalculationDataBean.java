package biz.firstlook.cia.model;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;

//Formerly GroupingValuationAndUnitsSold
public class CIAEngineCalculationDataBean implements ITargetUnits
{

private Integer groupingDescriptionId;
private double valuation;
private int retailUnitsSold;
private int wholesaleUnitsSold;
private Integer segmentId;
private int ciaCategoryId;
private int totalTargetUnits;
private double marketShare;

private transient List bucketronRowResults;
private transient List retailSaleList;

public Integer getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public Integer getUnitsSold()
{
    return new Integer(retailUnitsSold + wholesaleUnitsSold);
}

public double getValuation()
{
    return valuation;
}

public void setGroupingDescriptionId( Integer integer )
{
    groupingDescriptionId = integer;
}

public void setValuation( double double1 )
{
    valuation = double1;
}

public int getCiaCategoryId()
{
    return ciaCategoryId;
}

public void setCiaCategoryId( int i )
{
    ciaCategoryId = i;
}

public double getAverageValuation()
{
    return valuation / getUnitsSold().intValue();
}

public boolean equals( Object obj )
{
    if ( obj instanceof CIAEngineCalculationDataBean )
    {
        EqualsBuilder equals = new EqualsBuilder();
        equals.append(this.groupingDescriptionId,
                ((CIAEngineCalculationDataBean) obj).groupingDescriptionId);
        return equals.isEquals();
    } else
    {
        return false;
    }
}

public int hashCode()
{
    return new HashCodeBuilder().append(groupingDescriptionId).toHashCode();
}

public Integer getSegmentId()
{
    return segmentId;
}

public void setSegmentId( Integer integer )
{
    segmentId = integer;
}

public boolean determineIsCore()
{
    switch (ciaCategoryId)
    {
        case LightAndCIATypeDescriptor.GOOD_BETS:
            return true;
        case LightAndCIATypeDescriptor.POWERZONE:
            return true;
        case LightAndCIATypeDescriptor.WINNERS:
            return true;
        default:
            return false;

    }
}

public int getRetailUnitsSold()
{
    return retailUnitsSold;
}

public int getWholesaleUnitsSold()
{
    return wholesaleUnitsSold;
}

public void setRetailUnitsSold( int i )
{
    retailUnitsSold = i;
}

public void setWholesaleUnitsSold( int i )
{
    wholesaleUnitsSold = i;
}

public int getTotalTargetUnits()
{
    return totalTargetUnits;
}

public void setTotalTargetUnits( int i )
{
    totalTargetUnits = i;
}

public double getMarketShare()
{
    return marketShare;
}

public void setMarketShare( double d )
{
    marketShare = d;
}

public List getRetailSaleList()
{
    return retailSaleList;
}

public void setRetailSaleList( List list )
{
    retailSaleList = list;
}

public List getBucketronRowResults()
{
	return bucketronRowResults;
}

public void setBucketronRowResults( List bucketronRowResults )
{
	this.bucketronRowResults = bucketronRowResults;
}

}
