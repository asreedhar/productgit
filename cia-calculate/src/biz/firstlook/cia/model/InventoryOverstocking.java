package biz.firstlook.cia.model;



public class InventoryOverstocking
{

private Integer ciaInventoryOverstockingId;
private Integer inventoryId;
private Integer ciaSummaryId;

public InventoryOverstocking()
{
    super();
}

public Integer getInventoryId()
{
    return inventoryId;
}

public Integer getCiaInventoryOverstockingId()
{
    return ciaInventoryOverstockingId;
}

public void setInventoryId( Integer integer )
{
    inventoryId = integer;
}

public void setCiaInventoryOverstockingId( Integer integer )
{
    ciaInventoryOverstockingId = integer;
}

public Integer getCiaSummaryId()
{
    return ciaSummaryId;
}

public void setCiaSummaryId( Integer integer )
{
    ciaSummaryId = integer;
}
}
