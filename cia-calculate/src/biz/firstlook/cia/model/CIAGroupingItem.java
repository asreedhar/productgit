package biz.firstlook.cia.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.transact.persist.model.GroupingDescription;

public class CIAGroupingItem implements Serializable, Comparable<CIAGroupingItem>
{
private static final long serialVersionUID = -1676823124614273660L;
private Integer ciaGroupingItemId;
private CIACategory ciaCategory;
private GroupingDescription groupingDescription;
private Double marketShare;
private Double valuation;
private Double marketPenetration;
private String notes;
private CIABodyTypeDetail ciaBodyTypeDetail;

private Set<CIAGroupingItemDetail> ciaGroupingItemDetails;

private transient Integer segmentId;

public CIAGroupingItem()
{
	super();
	ciaGroupingItemDetails = new HashSet<CIAGroupingItemDetail>();
}

public CIACategory getCiaCategory()
{
	return ciaCategory;
}

public void setCiaCategory( CIACategory ciaCategory )
{
	this.ciaCategory = ciaCategory;
}

public Set<CIAGroupingItemDetail> getCiaGroupingItemDetails()
{
	return ciaGroupingItemDetails;
}

public void setCiaGroupingItemDetails( Set<CIAGroupingItemDetail> ciaGroupingItemDetails )
{
	this.ciaGroupingItemDetails = ciaGroupingItemDetails;
}

public void addCiaGroupingItemDetail( CIAGroupingItemDetail detail )
{
	this.ciaGroupingItemDetails.add( detail );
}

public Integer getCiaGroupingItemId()
{
	return ciaGroupingItemId;
}

public void setCiaGroupingItemId( Integer ciaGroupingItemId )
{
	this.ciaGroupingItemId = ciaGroupingItemId;
}

public Double getMarketPenetration()
{
	return marketPenetration;
}

public void setMarketPenetration( Double marketPenetration )
{
	this.marketPenetration = marketPenetration;
}

public Double getMarketShare()
{
	return marketShare;
}

public void setMarketShare( Double marketShare )
{
	this.marketShare = marketShare;
}

public String getNotes()
{
	return notes;
}

public void setNotes( String notes )
{
	this.notes = notes;
}

public Double getValuation()
{
	return valuation;
}

public void setValuation( Double valuation )
{
	this.valuation = valuation;
}

public Integer getSegmentId()
{
	return segmentId;
}

public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}

public GroupingDescription getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( GroupingDescription groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public CIABodyTypeDetail getCiaBodyTypeDetail()
{
	return ciaBodyTypeDetail;
}

public void setCiaBodyTypeDetail( CIABodyTypeDetail ciaBodyTypeDetail )
{
	this.ciaBodyTypeDetail = ciaBodyTypeDetail;
}

public int compareTo(CIAGroupingItem other) {
	int c = 0;
	if (other == null) {
		c = 1;
	}
	else {
		c = Functions.nullSafeCompareTo(getCiaBodyTypeDetail(), other.getCiaBodyTypeDetail(), c);
		c = Functions.nullSafeCompareTo(getCiaCategory(), other.getCiaCategory(), c);
	}
	return c;
}

}