package biz.firstlook.cia.model;

import java.util.List;

public class GroupingAndPreferenceHolder
{

private String groupingDescription;
private List groupingPreferences;

public GroupingAndPreferenceHolder()
{
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public List getGroupingPreferences()
{
    return groupingPreferences;
}

public void setGroupingDescription( String string )
{
    groupingDescription = string;
}

public void setGroupingPreferences( List list )
{
    groupingPreferences = list;
}

}
