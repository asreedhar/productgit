package biz.firstlook.cia.model;

import java.io.Serializable;
import java.util.Date;

public class CIAGroupingItemDetail implements Serializable
{

private static final long serialVersionUID = 4870387945791636771L;
private Integer ciaGroupingItemDetailId;
private CIAGroupingItem ciaGroupingItem;
private CIAGroupingItemDetailType ciaGroupingItemDetailType;
private CIAGroupingItemDetailLevel ciaGroupingItemDetailLevel;
private String detailLevelValue;
private Integer lowRange;
private Integer highRange;
private Integer units;
private Date dateCreated;
private transient int understocked;
private transient int unitsInStock;

public CIAGroupingItemDetail()
{
	super();
	dateCreated = new Date();
}

public CIAGroupingItem getCiaGroupingItem()
{
	return ciaGroupingItem;
}

public void setCiaGroupingItem( CIAGroupingItem ciaGroupingItem )
{
	this.ciaGroupingItem = ciaGroupingItem;
}

public Integer getCiaGroupingItemDetailId()
{
	return ciaGroupingItemDetailId;
}

public void setCiaGroupingItemDetailId( Integer ciaGroupingItemDetailId )
{
	this.ciaGroupingItemDetailId = ciaGroupingItemDetailId;
}

public Date getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( Date dateCreated )
{
	this.dateCreated = dateCreated;
}

public String getDetailLevelValue()
{
	return detailLevelValue;
}

public void setDetailLevelValue( String detailLevelValue )
{
	this.detailLevelValue = detailLevelValue;
}

public Integer getHighRange()
{
	return highRange;
}

public void setHighRange( Integer highRange )
{
	this.highRange = highRange;
}

public Integer getLowRange()
{
	return lowRange;
}

public void setLowRange( Integer lowRange )
{
	this.lowRange = lowRange;
}

public Integer getUnits()
{
	return units;
}

public void setUnits( Integer units )
{
	this.units = units;
}

public CIAGroupingItemDetailLevel getCiaGroupingItemDetailLevel()
{
	return ciaGroupingItemDetailLevel;
}

public void setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel ciaGroupingItemDetailLevel )
{
	this.ciaGroupingItemDetailLevel = ciaGroupingItemDetailLevel;
}

public CIAGroupingItemDetailType getCiaGroupingItemDetailType()
{
	return ciaGroupingItemDetailType;
}

public void setCiaGroupingItemDetailType( CIAGroupingItemDetailType ciaGroupingItemDetailType )
{
	this.ciaGroupingItemDetailType = ciaGroupingItemDetailType;
}

public int getUnderstocked()
{
	return understocked;
}

public void setUnderstocked( int understocked )
{
	this.understocked = understocked;
}

public boolean isHasUnderstocked()
{
	return this.understocked > 0;
}

public boolean isHasOverstocked()
{
	return this.understocked < 0;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public void setUnitsInStock( int unitsInStock )
{
	this.unitsInStock = unitsInStock;
}

public String getUnitCostRangeString()
{
	Integer lowerRange = getLowRange();
	Integer upperRange = getHighRange();
	if ( lowerRange == null || lowerRange.intValue() < 0 )
	{
		return ( "Under $" + upperRange );
	}
	else if ( upperRange.intValue() == Integer.MAX_VALUE )
	{
		if ( lowerRange.intValue() == Integer.MIN_VALUE )
		{
			return "Model ";
		}
		else
		{
			return ( "Over $" + lowerRange );
		}
	}
	else
	{
		return ( "$" + lowerRange + " - $" + upperRange );
	}
}

}
