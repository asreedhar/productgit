package biz.firstlook.cia.model;

public class CIABodyTypeButton
{

private Integer segmentId;
private Double percentContribution;
private Integer daysSupply;
private Integer totalTargetUnits;
private String segment;

public CIABodyTypeButton()
{
	super();
}

public Integer getDaysSupply()
{
	return daysSupply;
}

public void setDaysSupply( Integer daysSupply )
{
	this.daysSupply = daysSupply;
}

public Integer getSegmentId()
{
	return segmentId;
}

public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}

public Double getPercentContribution()
{
	return percentContribution;
}

public void setPercentContribution( Double percentContribution )
{
	this.percentContribution = percentContribution;
}

public Integer getTotalTargetUnits()
{
	return totalTargetUnits;
}

public void setTotalTargetUnits( Integer totalTargetUnits )
{
	this.totalTargetUnits = totalTargetUnits;
}

public String getSegment()
{
    return segment;
}

public void setSegment( String segment )
{
    this.segment = segment;
}

}
