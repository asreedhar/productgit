package biz.firstlook.cia.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Status implements Serializable
{

private static final long serialVersionUID = 4918490583678546428L;
private static final int PENDING_VAL = 1;
private static final int CURRENT_VAL = 2;
private static final int PRIOR_VAL = 3;
private static final int INACTIVE_VAL = 4;

public static final Status PENDING = new Status( "Pending", PENDING_VAL );
public static final Status CURRENT = new Status( "Current", CURRENT_VAL );
public static final Status PRIOR = new Status( "Prior", PRIOR_VAL );
public static final Status INACTIVE = new Status( "Inactive", INACTIVE_VAL );

private Integer statusId;
private String statusDescription;


Status()
{
}

Status( String statusDescription, int statusId )
{
	this.statusId = Integer.valueOf( statusId );
	this.statusDescription = statusDescription;
	
}

public String getStatusDescription()
{
	return statusDescription;
}

public void setStatusDescription( String string )
{
	statusDescription = string;
}

public Integer getStatusId()
{
	return statusId;
}

public void setStatusId( Integer i )
{
	statusId = i;
}

public Status getNextStatus()
{
	if ( statusId.intValue() == Status.CURRENT_VAL )
	{
		return Status.PRIOR;
	}
	else if ( statusId.intValue() == Status.PRIOR_VAL )
	{
		return Status.INACTIVE;
	}
	else
	{
		return Status.INACTIVE;
	}
}

public String toString()
{
	return ToStringBuilder.reflectionToString( this );
}

}