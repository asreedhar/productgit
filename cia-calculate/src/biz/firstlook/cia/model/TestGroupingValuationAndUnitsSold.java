package biz.firstlook.cia.model;

import junit.framework.TestCase;

public class TestGroupingValuationAndUnitsSold extends TestCase
{

public TestGroupingValuationAndUnitsSold()
{
	super();
}

public void testAverageValuation()
{
	double valuation = 20000.0;
	int unitsSold = 10;

	CIAEngineCalculationDataBean gvus = new CIAEngineCalculationDataBean();
	gvus.setValuation( valuation );
	gvus.setRetailUnitsSold( unitsSold );

	assertEquals( "The average valuation should be 2000", 2000, gvus.getAverageValuation(), 0 );
}

}
