package biz.firstlook.cia;

public class CIAException extends Exception
{

private static final long serialVersionUID = -2329863973000024840L;

public CIAException()
{
    super();
}

public CIAException( String message )
{
    super(message);
}

public CIAException( String message, Throwable cause )
{
    super(message, cause);
}

public CIAException( Throwable cause )
{
    super(cause);
}

}
