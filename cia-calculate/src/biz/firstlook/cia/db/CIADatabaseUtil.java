package biz.firstlook.cia.db;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.Type;

import org.apache.log4j.Logger;

import com.firstlook.data.DatabaseEnum;
import com.firstlook.data.DatabaseException;
import com.firstlook.data.factory.hibernate.SessionFactory;
import com.firstlook.data.hibernate.HibernateManager;
import com.firstlook.data.hibernate.HibernateUtil;
import com.firstlook.data.hibernate.IHibernateSessionFactory;

public class CIADatabaseUtil implements IHibernateSessionFactory
{
private static Logger logger = Logger.getLogger(CIADatabaseUtil.class);

private static CIADatabaseUtil instance;

private CIADatabaseUtil()
{
    super();
}

public static synchronized CIADatabaseUtil instance()
{
    if ( instance == null )
    {
        instance = new CIADatabaseUtil();

        try
        {
            Configuration cfg = (new Configuration()).configure(Thread
                    .currentThread().getContextClassLoader().getResource(
                            "cia.hibernate.cfg.xml"));
            HibernateManager.getInstance().setConfiguration(DatabaseEnum.CIA,
                    cfg);
        } catch (Exception e)
        {
            logger
                    .fatal(
                            "There has been a serious problem trying to configure the hibernate mappings for cia.",
                            e);
        }

    }
    return instance;
}

public CIADatabaseUtil getInstance()
{
    return instance();
}

public Session retrieveSession() throws HibernateException, DatabaseException
{
    return SessionFactory.createSession(DatabaseEnum.CIA);
}

public void update( Object object ) throws HibernateException, SQLException,
        DatabaseException
{
    HibernateUtil.update(DatabaseEnum.CIA, object);
}

public void save( Object object ) throws HibernateException, SQLException,
        DatabaseException
{
    HibernateUtil.save(DatabaseEnum.CIA, object);
}

public void saveOrUpdate( Object object ) throws HibernateException,
        SQLException, DatabaseException
{
    HibernateUtil.saveOrUpdate(DatabaseEnum.CIA, object);
}

public void delete( Object object ) throws HibernateException, SQLException,
        DatabaseException
{
    HibernateUtil.delete(DatabaseEnum.CIA, object);
}

public List find( String query, Object[] params, Type[] types )
        throws HibernateException, SQLException, DatabaseException
{
    return HibernateUtil.find(DatabaseEnum.CIA, query, params, types);
}

public List find( String query, Object param, Type type )
        throws HibernateException, SQLException, DatabaseException
{
    return HibernateUtil.find(DatabaseEnum.CIA, query, param, type);
}

public List find( String query ) throws HibernateException, SQLException,
        DatabaseException
{
    return HibernateUtil.find(DatabaseEnum.CIA, query);
}

public Object load( Class clazz, Object identifier ) throws HibernateException,
        DatabaseException
{
    return HibernateUtil.load(DatabaseEnum.CIA, clazz, identifier);
}

public void rollback( Transaction tx )
{
    try
    {
        tx.rollback();
    } catch (Exception e)
    {
    }
}

public void closeSession( Session session )
{
    HibernateUtil.closeSession(session);
}

}
