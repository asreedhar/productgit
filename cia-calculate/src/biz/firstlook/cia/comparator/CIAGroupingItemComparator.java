package biz.firstlook.cia.comparator;

import java.util.Comparator;

import biz.firstlook.cia.model.CIAGroupingItem;

public class CIAGroupingItemComparator implements Comparator
{

public CIAGroupingItemComparator()
{
	super();
}

public int compare( Object obj1, Object obj2 )
{
	CIAGroupingItem item1 = (CIAGroupingItem)obj1;
	CIAGroupingItem item2 = (CIAGroupingItem)obj2;

	int returnVal = ( item1.getCiaCategory().getCiaCategoryId().intValue() < item2.getCiaCategory().getCiaCategoryId().intValue() ? -1
			: ( item1.getCiaCategory().getCiaCategoryId().intValue() == item2.getCiaCategory().getCiaCategoryId().intValue() ? 0 : 1 ) );
	if ( returnVal == 0 )
	{
			if ( item1.getValuation().doubleValue() < item2.getValuation().doubleValue() )
			{
				returnVal = 1;
			}
			else if ( item1.getValuation().doubleValue() == item2.getValuation().doubleValue() )
			{
				returnVal = 0;
			}
			else
			{
				returnVal = -1;
			}
	}
	return returnVal;
}
}
