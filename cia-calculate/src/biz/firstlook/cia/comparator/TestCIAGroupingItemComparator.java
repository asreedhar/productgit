package biz.firstlook.cia.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;

public class TestCIAGroupingItemComparator extends TestCase
{

private CIAGroupingItemComparator comparator;
private List items;

public TestCIAGroupingItemComparator( String name )
{
	super( name );
}

public void setUp()
{
	comparator = new CIAGroupingItemComparator();

	items = new ArrayList();

	CIAGroupingItem pzItem = new CIAGroupingItem();
	pzItem.setValuation( new Double( 1000.43 ) );
	pzItem.setCiaCategory( CIACategory.POWERZONE );
	CIAGroupingItem pzItem2 = new CIAGroupingItem();
	pzItem2.setValuation( new Double( 14444 ) );
	pzItem2.setCiaCategory( CIACategory.POWERZONE );

	CIAGroupingItem winnerItem = new CIAGroupingItem();
	winnerItem.setValuation( new Double( 25000 ) );
	winnerItem.setCiaCategory( CIACategory.WINNERS );

	items.add( winnerItem );
	items.add( pzItem );
	items.add( pzItem2 );
}

public void testCompare()
{
	Collections.sort( items, comparator );
	CIAGroupingItem item1 = (CIAGroupingItem)items.get( 0 );
	CIAGroupingItem item2 = (CIAGroupingItem)items.get( 1 );
	CIAGroupingItem item3 = (CIAGroupingItem)items.get( 2 );

	assertEquals( 14444, item1.getValuation().doubleValue(), 0 );
	assertEquals( 1000.43, item2.getValuation().doubleValue(), 0 );
	assertEquals( 25000, item3.getValuation().doubleValue(), 0 );
}

}
