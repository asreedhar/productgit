package biz.firstlook.cia.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import biz.firstlook.cia.CIAException;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.BucketronRowResult;
import biz.firstlook.cia.persistence.CoreMarketPenetrationRetriever;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.persistence.IGroupingDescriptionDAO;

public class CIASummaryService
{

private ICIASummaryDAO ciaSummaryDAO;
private IGroupingDescriptionDAO groupingDescriptionDAO;
private CoreMarketPenetrationRetriever coreMarketPenetrationRetriever;

private static Logger logger = Logger.getLogger( CIASummaryService.class );

public void save( CIAContext ciaContext ) throws CIAException
{
	logger.info( "Begin call to GetCoreMarketPenetration");
	Map<Integer,Double> marketPenetrationMap = coreMarketPenetrationRetriever.retrieveCoreMarketPenetration( ciaContext.getDealerId(), new Date(), ciaContext.getCoreMarketPenetrationSalesThreshold() );
	logger.info( "End call to GetCoreMarketPenetration");
	
	List<CIAEngineCalculationDataBean> groupingValuations = new ArrayList<CIAEngineCalculationDataBean>();
	groupingValuations.addAll( ciaContext.getPowerzoneModelCalculationBeans() );
	groupingValuations.addAll( ciaContext.getWinnerModelCalculationBeans() );
	groupingValuations.addAll( ciaContext.getGoodBetModelCalculationBeans() );
	groupingValuations.addAll( ciaContext.getMarketPerformerModelCalculationBeans() );
	groupingValuations.addAll( ciaContext.getManagersChoiceModelCalculationBeans() );

	Map<Integer,CIABodyTypeDetail> ciaBodyTypeDetails = transformCalculationDataBeansToCiaData( ciaContext.getCiaBodyTypeDetails(), groupingValuations, marketPenetrationMap );

	CIASummary ciaSummary = new CIASummary( ciaContext.getDealerId().intValue() );
	ciaSummary.getCiaBodyTypeDetails().addAll( ciaBodyTypeDetails.values() );

	try
	{
		CIASummary staleCurrentCiaSummary = getCiaSummaryDAO().retrieveCIASummary( ciaContext.getDealerId(), Status.CURRENT );
		CIASummary stalePriorCiaSummary = getCiaSummaryDAO().retrieveCIASummary( ciaContext.getDealerId(), Status.PRIOR );

		// TODO: These last 3 calls should be in a transaction. -BF September 6,
		// 2005
		
		//need to evaluate & update each object independently since 
		//they are mutually exclusive
		if (staleCurrentCiaSummary != null) 
		{
			staleCurrentCiaSummary.setStatus( staleCurrentCiaSummary.getStatus().getNextStatus() );
			getCiaSummaryDAO().update( staleCurrentCiaSummary );
		}
		if (stalePriorCiaSummary != null)
		{
			stalePriorCiaSummary.setStatus( stalePriorCiaSummary.getStatus().getNextStatus() );
			getCiaSummaryDAO().update( stalePriorCiaSummary );
		}
			
		getCiaSummaryDAO().save( ciaSummary );
	}
	catch ( Exception e )
	{
		throw new CIAException( "Problem saving ciasummary", e );
	}
}

private Map<Integer,CIABodyTypeDetail> transformCalculationDataBeansToCiaData( Map<Integer,CIABodyTypeDetail> ciaBodyTypeDetails, List<CIAEngineCalculationDataBean> ciaGroupingDescriptionlCalculationDataBeans, Map<Integer,Double> marketPenetrationMap )
{
	List<CIAGroupingItem> ciaGroupingItems = new ArrayList<CIAGroupingItem>();

	Iterator<CIAEngineCalculationDataBean> calculationDataBeanIter = ciaGroupingDescriptionlCalculationDataBeans.iterator();
	while ( calculationDataBeanIter.hasNext() )
	{
		CIAEngineCalculationDataBean calculationDataBean = calculationDataBeanIter.next();

		CIAGroupingItem ciaGroupingItem = new CIAGroupingItem();
		ciaGroupingItem.setCiaCategory( CIACategory.retrieveCIACategory( calculationDataBean.getCiaCategoryId() ) );

		String groupingDescriptionText = getGroupingDescriptionDAO().findGroupingDescriptionStringById( calculationDataBean.getGroupingDescriptionId() );
		ciaGroupingItem.setGroupingDescription( new GroupingDescription(	calculationDataBean.getGroupingDescriptionId(),
																			groupingDescriptionText ) );

		ciaGroupingItem.setMarketShare( new Double( calculationDataBean.getMarketShare() ) );
		Double marketPenetration = new Double(0);
		if( marketPenetrationMap.get( calculationDataBean.getGroupingDescriptionId() ) != null )
		{
			marketPenetration = marketPenetrationMap.get( calculationDataBean.getGroupingDescriptionId() );
		}
		ciaGroupingItem.setMarketPenetration( marketPenetration );
		ciaGroupingItem.setValuation( new Double( calculationDataBean.getValuation() ) );

		Set<CIAGroupingItemDetail> ciaGroupingItemDetails = constructCIAGroupingItemDetails( calculationDataBean, ciaGroupingItem );
		ciaGroupingItem.setCiaGroupingItemDetails( ciaGroupingItemDetails );

		// setting transient field for later processing
		ciaGroupingItem.setSegmentId( calculationDataBean.getSegmentId() );
		ciaGroupingItems.add( ciaGroupingItem );

		// sort the grouping items into their respective bodyTypes
		if ( ciaBodyTypeDetails.containsKey( ciaGroupingItem.getSegmentId() ) )
		{
			CIABodyTypeDetail ciaBodyTypeDetail = (CIABodyTypeDetail)ciaBodyTypeDetails.get( ciaGroupingItem.getSegmentId() );
			ciaGroupingItem.setCiaBodyTypeDetail( ciaBodyTypeDetail );
			ciaBodyTypeDetail.getCiaGroupingItems().add( ciaGroupingItem );
		}
	}

	return ciaBodyTypeDetails;
}

private Set<CIAGroupingItemDetail> constructCIAGroupingItemDetails( CIAEngineCalculationDataBean calculationDataBean, CIAGroupingItem ciaGroupingItem )
{
	Set<CIAGroupingItemDetail> ciaGroupingItemDetails = new HashSet<CIAGroupingItemDetail>();

	List bucketedRecommendations = calculationDataBean.getBucketronRowResults();

	if ( bucketedRecommendations == null )
	{
		CIAGroupingItemDetail ciaGroupingItemDetail = new CIAGroupingItemDetail();
		ciaGroupingItemDetail.setCiaGroupingItem( ciaGroupingItem );
		ciaGroupingItemDetail.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.GROUPING );
		ciaGroupingItemDetail.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL );
		ciaGroupingItemDetail.setDetailLevelValue( calculationDataBean.getGroupingDescriptionId().toString() );
		ciaGroupingItemDetail.setUnits( new Integer( calculationDataBean.getTotalTargetUnits() ) );
		ciaGroupingItemDetails.add( ciaGroupingItemDetail );
	}
	else
	{
		Iterator bucketRecommendationsIter = bucketedRecommendations.iterator();
		while ( bucketRecommendationsIter.hasNext() )
		{
			BucketronRowResult recommendation = (BucketronRowResult)bucketRecommendationsIter.next();

			CIAGroupingItemDetail ciaGroupingItemDetail = new CIAGroupingItemDetail();
			ciaGroupingItemDetail.setCiaGroupingItem( ciaGroupingItem );
			ciaGroupingItemDetail.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
			ciaGroupingItemDetail.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL );
			ciaGroupingItemDetail.setDetailLevelValue( recommendation.getDescription() );
			ciaGroupingItemDetail.setUnits( new Integer( recommendation.getAllocationUnits() ) );

			ciaGroupingItemDetails.add( ciaGroupingItemDetail );
		}
	}
	return ciaGroupingItemDetails;
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public IGroupingDescriptionDAO getGroupingDescriptionDAO()
{
	return groupingDescriptionDAO;
}

public void setGroupingDescriptionDAO( IGroupingDescriptionDAO groupingDescriptionDAO )
{
	this.groupingDescriptionDAO = groupingDescriptionDAO;
}

public void setCoreMarketPenetrationRetriever( CoreMarketPenetrationRetriever coreMarketPenetrationRetriever )
{
	this.coreMarketPenetrationRetriever = coreMarketPenetrationRetriever;
}

}