package biz.firstlook.cia.service;

import java.util.List;

import biz.firstlook.cia.CIAException;
import biz.firstlook.cia.persistence.CIAPowerZonePricePointPersistence;

public class CIAPowerZonePricePointService
{

public CIAPowerZonePricePointService()
{
}

public List retrievePricePoint( int ciaPowerZoneItemId ) throws CIAException
{
    try
    {
        CIAPowerZonePricePointPersistence persistence = new CIAPowerZonePricePointPersistence();
        List pricePoints = persistence.findBy(ciaPowerZoneItemId);

        return pricePoints;
    } catch (Exception e)
    {
        throw new CIAException(
                "Error retrieving pricepoints by cia powerzone item id: ", e);
    }
}
}
