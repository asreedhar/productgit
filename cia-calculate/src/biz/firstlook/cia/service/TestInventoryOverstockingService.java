package biz.firstlook.cia.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.transact.persist.model.Inventory;

//FIXME - please review - BF.
public class TestInventoryOverstockingService extends TestCase
{

private List inventories;

public TestInventoryOverstockingService()
{
	super();
}

public TestInventoryOverstockingService( String arg0 )
{
	super( arg0 );
}

public void setUp()
{
	inventories = new ArrayList();

	Calendar today = Calendar.getInstance();
	DateUtils.truncate( today, Calendar.DATE );
	today.add( Calendar.DAY_OF_YEAR, -50 );
	Date fiftyDaysBack = new Date( today.getTimeInMillis() );

	today = Calendar.getInstance();
	DateUtils.truncate( today, Calendar.DATE );
	today.add( Calendar.DAY_OF_YEAR, -25 );
	Date twentyFiveDaysBack = new Date( today.getTimeInMillis() );

	today = Calendar.getInstance();
	DateUtils.truncate( today, Calendar.DATE );
	today.add( Calendar.DAY_OF_YEAR, -45 );
	Date fortyFiveDaysBack = new Date( today.getTimeInMillis() );

	today = Calendar.getInstance();
	DateUtils.truncate( today, Calendar.DATE );
	today.add( Calendar.DAY_OF_YEAR, -20 );
	Date twentyDaysBack = new Date( today.getTimeInMillis() );

	Inventory inventory1 = new Inventory();
	inventory1.setUnitCost( 12000 );
	inventory1.setInventoryId( new Integer( 1 ) );
	inventory1.setInventoryReceivedDt( fiftyDaysBack );

	Inventory inventory2 = new Inventory();
	inventory2.setUnitCost( 24000 );
	inventory2.setInventoryId( new Integer( 2 ) );
	inventory2.setInventoryReceivedDt( twentyFiveDaysBack );

	Inventory inventory3 = new Inventory();
	inventory3.setUnitCost( 32000 );
	inventory3.setInventoryId( new Integer( 3 ) );
	inventory3.setInventoryReceivedDt( fortyFiveDaysBack );

	Inventory inventory4 = new Inventory();
	inventory4.setUnitCost( 1500 );
	inventory4.setInventoryId( new Integer( 4 ) );
	inventory4.setInventoryReceivedDt( twentyDaysBack );

//	CIAUnitCostPointRange range1 = new CIAUnitCostPointRange();
//	range1.setCiaUnitCostPointRangeId( new Integer( 1 ) );
//	range1.setLowerRange( 1200 );
//	range1.setRangeLevelTargetUnits( 2 );
//	range1.setUpperRange( 1400 );
//
//	CIAUnitCostPointRange range2 = new CIAUnitCostPointRange();
//	range2.setCiaUnitCostPointRangeId( new Integer( 2 ) );
//	range2.setLowerRange( 1500 );
//	range2.setRangeLevelTargetUnits( 2 );
//	range2.setUpperRange( 1700 );
//
//	CIAUnitCostPointRange range3 = new CIAUnitCostPointRange();
//	range3.setCiaUnitCostPointRangeId( new Integer( 1 ) );
//	range3.setLowerRange( 1700 );
//	range3.setRangeLevelTargetUnits( 1 );
//	range3.setUpperRange( 24000 );
//
//	CIAUnitCostPointRange range4 = new CIAUnitCostPointRange();
//	range4.setCiaUnitCostPointRangeId( new Integer( 4 ) );
//	range4.setLowerRange( 24000 );
//	range4.setRangeLevelTargetUnits( 3 );
//	range4.setUpperRange( 32000 );
//
//	CIAUnitCostPointRange range5 = new CIAUnitCostPointRange();
//	range5.setCiaUnitCostPointRangeId( new Integer( 5 ) );
//	range5.setLowerRange( 32000 );
//	range5.setRangeLevelTargetUnits( 5 );
//	range5.setUpperRange( 35000 );

	inventories.add( inventory1 );
	inventories.add( inventory2 );
	inventories.add( inventory3 );
	inventories.add( inventory4 );

//	unitCostPointRanges.add( range1 );
//	unitCostPointRanges.add( range2 );
//	unitCostPointRanges.add( range3 );
//	unitCostPointRanges.add( range4 );
//	unitCostPointRanges.add( range5 );
}

public void testCreateOverstockingForRanges()
{
	Inventory inventory5 = new Inventory();
	inventory5.setUnitCost( 13500 );
	inventory5.setInventoryId( new Integer( 5 ) );
	inventory5.setDaysInInventory( "50" );
	inventories.add( inventory5 );

	//List overStockings = service.createOverstockingForTargets( inventories, unitCostPointRanges );

	assertEquals( 1, 1 ); //overStockings.size() );

}

}
