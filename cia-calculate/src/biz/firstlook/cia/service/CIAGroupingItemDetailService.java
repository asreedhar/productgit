package biz.firstlook.cia.service;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.persistence.ICIAGroupingItemDetailDAO;

public class CIAGroupingItemDetailService implements ICIAGroupingItemDetailService
{

private ICIAGroupingItemDetailDAO ciaGroupingItemDetailDAO;

public CIAGroupingItemDetailService()
{
	super();
}

public CIAGroupingItemDetail findForDealerByGroupingDescriptionAndDetailValue( Integer businessUnitId, Integer ciaSummaryStatusId,
                                                                              Integer groupingDescriptionId, String detailValue )
{
    return getCiaGroupingItemDetailDAO().findForDealerByGroupingDescriptionAndDetailValue( businessUnitId, ciaSummaryStatusId,
                                                                                           groupingDescriptionId, detailValue );
}

public ICIAGroupingItemDetailDAO getCiaGroupingItemDetailDAO()
{
	return ciaGroupingItemDetailDAO;
}

public void setCiaGroupingItemDetailDAO( ICIAGroupingItemDetailDAO groupingItemDetailDAO )
{
	this.ciaGroupingItemDetailDAO = groupingItemDetailDAO;
}

}
