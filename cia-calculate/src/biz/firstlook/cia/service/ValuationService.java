package biz.firstlook.cia.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.calculator.ValuationCalculator;
import biz.firstlook.cia.calculator.ValuationDeal;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.commons.date.BasisPeriod;
import biz.firstlook.commons.util.ILogger;
import biz.firstlook.commons.util.PropertyFinder;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.DealerValuation;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.VehicleSale;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;
import biz.firstlook.transact.persist.persistence.IValuationDAO;
import biz.firstlook.transact.persist.persistence.VehicleSaleDAO;

import com.firstlook.data.DatabaseException;

public class ValuationService
{
private ValuationCalculator valuationCalculator;
private IValuationDAO valuationDAO;
private ILogger logger;
private VehicleSaleDAO vehicleSaleDAO;
private IInventoryDAO inventoryDAO;
private DealerPreferenceDAO dealerPreferenceDAO;

public List retrieveRetailValuationDeals( int dealerId, BasisPeriod basisPeriod )
{
	DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId( new Integer( dealerId ) );
	try
	{
		List sales = vehicleSaleDAO.findRetailSalesBy( new Integer( dealerId ), new Timestamp(basisPeriod.getStartDate().getTime()), new Timestamp(basisPeriod.getEndDate().getTime()),
														Inventory.USED_CAR, dealerPreference.getUnitCostThresholdLower(),
														dealerPreference.getUnitCostThresholdUpper() );

		List<ValuationDeal> deals = new ArrayList<ValuationDeal>();
		Iterator salesIterator = sales.iterator();
		VehicleSale sale;
		Inventory inventory;
		while ( salesIterator.hasNext() )
		{
			sale = (VehicleSale)salesIterator.next();
			inventory = inventoryDAO.findBy( new Integer( sale.getInventoryId() ) );
			ValuationDeal deal = new ValuationDeal();
			deal.setInventoryId( inventory.getInventoryId().intValue() );
			deal.setFeGrossProfit( sale.getFrontEndGross() );
			deal.setUnitCost( inventory.getUnitCost() );
			deal.setSalePrice( sale.getSalePrice() );
			deal.setDaysToSale( daysToSale( inventory.getInventoryReceivedDt(), sale.getDealDate() ) );
			deals.add( deal );
		}
		return deals;
	}
	catch ( DatabaseException ex )
	{
		// todo: probably should throw a runtime exception of our own
		throw new RuntimeException( "Error retrieving data for valuation deals.", ex );
	}
}

double daysToSale( Date start, Date end )
{
	long startTime = moveDateToTwelveAM( start );
	long endTime = moveDateToTwelveAM( end );

	double daysToSale = ( endTime - startTime ) / ( 1000 * 60 * 60 * 24 );
	if ( daysToSale <= 0 )
	{
		daysToSale = 1;
	}
	return daysToSale;
}

private long moveDateToTwelveAM( Date date )
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( date );
	calendar.set( Calendar.HOUR_OF_DAY, 0 );
	calendar.set( Calendar.MINUTE, 00 );
	calendar.set( Calendar.SECOND, 00 );
	calendar.set( Calendar.MILLISECOND, 000 );
	return calendar.getTime().getTime() + ( calendar.get( Calendar.ZONE_OFFSET ) + calendar.get( Calendar.DST_OFFSET ) );
}

public DealerValuation determineRateAndTime( int dealerId, BasisPeriod basisPeriod )
{
	DealerPreference dealerPreference = dealerPreferenceDAO.findByBusinessUnitId( new Integer( dealerId ) );

	List deals = retrieveRetailValuationDeals( dealerId, basisPeriod );
	DealerValuation dealerValuation = null;
	double discountRate = 0;
	double timeHorizon = 0;
	if ( deals.size() > 0 )
	{
		discountRate = valuationCalculator.calculateDiscountRate( deals, dealerPreference.getMarginPercentile().intValue() );
		timeHorizon = valuationCalculator.calculateTimeHorizon( deals, dealerPreference.getDaysToSalePercentile().intValue() );
		dealerValuation = new DealerValuation( new Date(), timeHorizon, discountRate, new Integer( dealerId ) );
	}
	else
	{
		discountRate = 0; // KL - for logging only, we will no longer create a
		// DealerValuation record when there are no sales.
		timeHorizon = 0;
	}
	logger.log( "Number of vehicles in the set retrieved=" + deals.size(),
				getLogLevel( "DiscountRateTimeHorizonProcessor.NumberOfVehiclesInSet" ) );
	logger.log( "Begin date for the set=" + basisPeriod.getStartDate().toString(),
				getLogLevel( "DiscountRateTimeHorizonProcessor.BeginDateForSet" ) );
	logger.log( "End date for the set=" + basisPeriod.getEndDate().toString(), getLogLevel( "DiscountRateTimeHorizonProcessor.EndDateForSet" ) );
	logger.log( "Margin Percentile Rank for Valuation used on the run=" + dealerPreference.getMarginPercentile().intValue(),
				getLogLevel( "DiscountRateTimeHorizonProcessor.MarginPercentileRankForValuation" ) );
	logger.log( "Margin at the Margin Percentile Rank for Valuation used on the run=" + discountRate,
				getLogLevel( "DiscountRateTimeHorizonProcessor.MarginAtTheMarginPercentileRank" ) );
	logger.log( "Days to Sale Percentile Rank for Valuation used on the run=" + dealerPreference.getDaysToSalePercentile().intValue(),
				getLogLevel( "DiscountRateTimeHorizonProcessor.DaysToSalePercentileRankForValuation" ) );
	logger.log( "Days to Sale at the Days to Sale Percentile Rank for Valuation used on the run=" + timeHorizon,
				getLogLevel( "DiscountRateTimeHorizonProcessor.DaysToSaleAtTheDaysToSalePercentileRank" ) );
	return dealerValuation;
}

public double determineValuation( DealerValuation dealerValuation, ValuationDeal deal, Boolean includeBackEndInValuation )
{
	double discountRate = dealerValuation.getDiscountRate();
	double timeHorizon = dealerValuation.getTimeHorizon();

	return valuationCalculator.calculateValuation( deal, discountRate, timeHorizon, includeBackEndInValuation );
}

public Collection retrieveValuationGroupingDescriptionUnitsSold( Integer dealerId, CompositeBasisPeriod basisPeriod, Integer unitCostLower,
																Integer unitCostUpper )
{
	Map groupingValuationAndUnitsSolds = new HashMap();
	List vehicleSales = new ArrayList();
	vehicleSales.addAll( getValuationDAO().retrieveGroupingUnitsSoldValuationBy( dealerId, Inventory.USED_CAR,
																					new Timestamp(basisPeriod.getPriorBasisPeriod().getStartDate().getTime()),
																					new Timestamp(basisPeriod.getPriorBasisPeriod().getEndDate().getTime()),
																					unitCostLower, unitCostUpper ) );

	if ( basisPeriod.isComposite() )
	{

		vehicleSales.addAll( getValuationDAO().retrieveGroupingUnitsSoldValuationBy( dealerId, Inventory.USED_CAR,
																						new Timestamp(basisPeriod.getForecastBasisPeriod().getStartDate().getTime()),
																						new Timestamp(basisPeriod.getForecastBasisPeriod().getEndDate().getTime()),
																						unitCostLower, unitCostUpper ) );
	}

	populateGroupingValuationAndUnitsSold( groupingValuationAndUnitsSolds, vehicleSales );
	return groupingValuationAndUnitsSolds.values();
}

void populateGroupingValuationAndUnitsSold( Map groupingValuationAndUnitsSolds, List allVehicleSales )
{
	Iterator allVehicleSalesIter = allVehicleSales.iterator();
	while ( allVehicleSalesIter.hasNext() )
	{
		Object[] values = (Object[])allVehicleSalesIter.next();
		Integer groupingDescriptionId = (Integer)values[0];
		Integer segmentId = (Integer)values[1];
		String retailOrWholesale = (String)values[2];
		double valuation = ( (Double)values[3] ).doubleValue();
		int unitsSold = ( (Integer)values[4] ).intValue();

		CIAEngineCalculationDataBean groupingValuationAndUnitsSold = new CIAEngineCalculationDataBean();
		groupingValuationAndUnitsSold.setGroupingDescriptionId( groupingDescriptionId );
		groupingValuationAndUnitsSold.setSegmentId( segmentId );

		if ( groupingValuationAndUnitsSolds.containsKey( groupingDescriptionId ) )
		{
			CIAEngineCalculationDataBean returnedGroupingValuesAndUnitsSold = (CIAEngineCalculationDataBean)groupingValuationAndUnitsSolds.get( groupingDescriptionId );
			valuation += returnedGroupingValuesAndUnitsSold.getValuation();
			if ( retailOrWholesale.equalsIgnoreCase( "R" ) )
			{
				groupingValuationAndUnitsSold.setWholesaleUnitsSold( returnedGroupingValuesAndUnitsSold.getWholesaleUnitsSold() );
				unitsSold += returnedGroupingValuesAndUnitsSold.getRetailUnitsSold();
			}
			else
			{
				groupingValuationAndUnitsSold.setRetailUnitsSold( returnedGroupingValuesAndUnitsSold.getRetailUnitsSold() );
				unitsSold += returnedGroupingValuesAndUnitsSold.getWholesaleUnitsSold();
			}
		}

		groupingValuationAndUnitsSold.setValuation( valuation );
		if ( retailOrWholesale.equalsIgnoreCase( "R" ) )
		{
			groupingValuationAndUnitsSold.setRetailUnitsSold( unitsSold );
		}
		else
		{
			groupingValuationAndUnitsSold.setWholesaleUnitsSold( unitsSold );
		}

		groupingValuationAndUnitsSolds.put( groupingDescriptionId, groupingValuationAndUnitsSold );
	}
}

protected int getLogLevel( String propertyName )
{
	PropertyFinder finder = new PropertyFinder( "Commons" );

	return Integer.parseInt( finder.getProperty( propertyName ) );
}

public IValuationDAO getValuationDAO()
{
	return valuationDAO;
}

public void setValuationDAO( IValuationDAO valuationDAO )
{
	this.valuationDAO = valuationDAO;
}

public VehicleSaleDAO getVehicleSaleDAO()
{
	return vehicleSaleDAO;
}

public void setVehicleSaleDAO( VehicleSaleDAO vehicleSaleDAO )
{
	this.vehicleSaleDAO = vehicleSaleDAO;
}

public ILogger getLogger()
{
	return logger;
}

public void setLogger( ILogger logger )
{
	this.logger = logger;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public ValuationCalculator getValuationCalculator()
{
	return valuationCalculator;
}

public void setValuationCalculator( ValuationCalculator calculator )
{
	this.valuationCalculator = calculator;
}

public void setDealerPreferenceDAO( DealerPreferenceDAO dealerPreferenceDAO )
{
	this.dealerPreferenceDAO = dealerPreferenceDAO;
}

}
