package biz.firstlook.cia.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import biz.firstlook.cia.calculator.BasisPeriod;
import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.persistence.BucketronExecutor;
import biz.firstlook.cia.persistence.WeeksInCIACompositeTimePeriodRetriever;
import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;
import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.persistence.CIAPreferencesDAO;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.ICIACompositeTimePeriodDAO;
import biz.firstlook.transact.persist.persistence.IVehicleSaleDAO;

import com.firstlook.data.DatabaseException;

public class BasisPeriodService 
{

private CIAPreferencesDAO ciaPreferencesDAO;
private DealerPreferenceDAO dealerPreferenceDAO;
private ICIACompositeTimePeriodDAO ciaCompositeTimePeriodDAO;
private IVehicleSaleDAO vehicleSaleDAO;
private WeeksInCIACompositeTimePeriodRetriever weeksInCIACompositeTimePeriodRetriever;

public BasisPeriodService()
{
}

private CompositeBasisPeriod convertToBasisPeriod( CIACompositeTimePeriod ciaCompositeTimePeriod, Integer dealerId )
{
	BasisPeriod forecastBP = new BasisPeriod( ciaCompositeTimePeriod.getForecastTimePeriod().getDays(), ciaCompositeTimePeriod.getForecastTimePeriod().isForecast() );

	double retailUnitSales = retrieveRetailUnitSales( dealerId, forecastBP );
	forecastBP.setTotalUnitsSold( (int)retailUnitSales );
	BasisPeriod priorBP = new BasisPeriod( ciaCompositeTimePeriod.getPriorTimePeriod().getDays(), ciaCompositeTimePeriod.getPriorTimePeriod().isForecast() );

	retailUnitSales = retrieveRetailUnitSales( dealerId, priorBP );
	priorBP.setTotalUnitsSold( (int)retailUnitSales );

	CompositeBasisPeriod compositeBasisPeriod = new CompositeBasisPeriod( forecastBP, priorBP );

	return compositeBasisPeriod;
}

private double retrieveRetailUnitSales( Integer dealerId, BasisPeriod bp )
{
	double retailUnitSales = 0.0;
	try
	{

		DealerPreference dealerPreference = getDealerPreferenceDAO().findByBusinessUnitId( dealerId );
		retailUnitSales = getVehicleSaleDAO().findCountAllRetailSalesUsingProfitabilityThreshold( dealerId, new Timestamp(bp.getStartDate().getTime()), new Timestamp(bp.getEndDate().getTime()),
																							Inventory.USED_CAR,
																							dealerPreference.getUnitCostThresholdLower(),
																							dealerPreference.getUnitCostThresholdUpper(),
																							dealerPreference.getFeGrossProfitThreshold() );
	}
	catch ( DatabaseException e )
	{
		throw new RuntimeDatabaseException( "Error retrieving RetailUnitSales", e );
	}
	return retailUnitSales;
}


public int calculateNumberOfWeeksInBasisPeriodByType( Integer dealerId, TimePeriod timePeriod, int isForecast )
{
    int result = 0;
    Integer timePeriodId = getCIACompositeTimePeriodForDealer( dealerId, timePeriod ).getCiaCompositeTimePeriodId();
    Map results = weeksInCIACompositeTimePeriodRetriever.call(timePeriodId, new Integer(isForecast));
    Integer returnCode = (Integer)results.get( WeeksInCIACompositeTimePeriodRetriever.ReturnCode );
    if ( returnCode.intValue() == BucketronExecutor.SUCCESS_RETURN_CODE )
    {
        result = ((Integer)((List)results.get( WeeksInCIACompositeTimePeriodRetriever.ResultSet )).get(0)).intValue();
    }
    return result;
}


public CompositeBasisPeriod retrieveCompositeBasisPeriodByCIAPreference( Integer dealerId, TimePeriod timePeriodEnum )
{
	CIACompositeTimePeriod timePeriod = getCIACompositeTimePeriodForDealer( dealerId, timePeriodEnum );
	return convertToBasisPeriod( timePeriod, dealerId );
}

/**
 * @param dealerId
 * @param ciaPreferenceType
 * @return
 *
 **/
private CIACompositeTimePeriod getCIACompositeTimePeriodForDealer( Integer dealerId, TimePeriod timePeriodEnum )
{
    CIAPreferences ciaPreference = getCiaPreferencesDAO().findByBusinessUnitId( dealerId );

	CIACompositeTimePeriod timePeriod = null;

	if ( ciaPreference != null )
	{
		switch ( timePeriodEnum )
		{
			case CIA_STORE_TARGET_INVENTORY:
				timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( ciaPreference.getCiaStoreTargetInventoryBasisPeriodId() ) );
				break;
			case CIA_CORE_MODEL_DETERMINATION:
				timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( ciaPreference.getCiaCoreModelDeterminationBasisPeriodId() ) );
				break;
			case CIA_POWERZONE_MODEL_TARGET_INVENTORY:
				timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( ciaPreference.getCiaPowerzoneModelTargetInventoryBasisPeriodId() ) );
				break;
			case CIA_CORE_MODEL_YEAR_ALLOCATION:
				timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( ciaPreference.getCiaCoreModelYearAllocationBasisPeriodId() ) );
				break;
			case GD_LIGHT_PROCESSOR:
				timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( ciaPreference.getGdLightProcessorTimePeriodId() ) );
				break;
			default:
				timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( ciaPreference.getSalesHistoryDisplayTimePeriodId() ) );
				break;
		}
	}
	else
	{
		timePeriod = ciaCompositeTimePeriodDAO.findById( new Integer( CIAPreferences.TWENTYSIX_ZERO_TIME_PERIOD ) );
	}
    return timePeriod;
}

public IVehicleSaleDAO getVehicleSaleDAO()
{
	return vehicleSaleDAO;
}

public void setVehicleSaleDAO( IVehicleSaleDAO vehicleSaleDAO )
{
	this.vehicleSaleDAO = vehicleSaleDAO;
}

public ICIACompositeTimePeriodDAO getCiaCompositeTimePeriodDAO()
{
    return ciaCompositeTimePeriodDAO;
}

public void setCiaCompositeTimePeriodDAO( ICIACompositeTimePeriodDAO ciaCompositeTimePeriodDAO )
{
    this.ciaCompositeTimePeriodDAO = ciaCompositeTimePeriodDAO;
}

public DealerPreferenceDAO getDealerPreferenceDAO()
{
	return dealerPreferenceDAO;
}

public void setDealerPreferenceDAO( DealerPreferenceDAO dealerPreferenceDAO )
{
	this.dealerPreferenceDAO = dealerPreferenceDAO;
}

public CIAPreferencesDAO getCiaPreferencesDAO()
{
	return ciaPreferencesDAO;
}

public void setCiaPreferencesDAO( CIAPreferencesDAO ciaPreferencesDAO )
{
	this.ciaPreferencesDAO = ciaPreferencesDAO;
}

public WeeksInCIACompositeTimePeriodRetriever getWeeksInCIACompositeTimePeriodRetriever()
{
    return weeksInCIACompositeTimePeriodRetriever;
}

public void setWeeksInCIACompositeTimePeriodRetriever( WeeksInCIACompositeTimePeriodRetriever weeksInCIACompositeTimePeriodRetriever )
{
    this.weeksInCIACompositeTimePeriodRetriever = weeksInCIACompositeTimePeriodRetriever;
}

}