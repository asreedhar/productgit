package biz.firstlook.cia.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.persistence.ICIAGroupingItemDAO;

public class CIAGroupingItemService
{

private ICIAGroupingItemDAO ciaGroupingItemDAO;

public CIAGroupingItemService()
{

}

public CIAGroupingItem retrieveByCIAGroupingItemId( Integer CIAGroupingItemId )
{
	return getCiaGroupingItemDAO().findByCIAGroupingItemId( CIAGroupingItemId );
}

public List retrieveBy( Integer summaryId, Integer segmentId, Integer ciaCategoryId )
{
	return getCiaGroupingItemDAO().findBy( summaryId, segmentId, ciaCategoryId );
}

public void update( CIAGroupingItem ciaGroupingItem )
{
	getCiaGroupingItemDAO().update( ciaGroupingItem );
}

public void updateNotesOnCIAGroupingItems( Collection notes )
{
    Iterator notesIter = notes.iterator();
    while ( notesIter.hasNext() )
    {
        CIAGroupingItem ciaGroupingItem = (CIAGroupingItem)notesIter.next();
        CIAGroupingItem returnedCIAGroupingItem = retrieveByCIAGroupingItemId( ciaGroupingItem.getCiaGroupingItemId() );
        returnedCIAGroupingItem.setNotes( ciaGroupingItem.getNotes() );
        update( returnedCIAGroupingItem );
    }
}

public void saveOrUpdate( CIAGroupingItem ciaGroupingItem )
{
	ciaGroupingItemDAO.saveOrUpdate( ciaGroupingItem );
}

public List<CIAGroupingItem> retrieveCoreGroupingsByDealerIdAndStatus( Integer dealerId, Integer ciaSummaryStatusId )
{
	return getCiaGroupingItemDAO().findCoreGroupingsByDealerIdAndStatus( dealerId, ciaSummaryStatusId );
}

public ICIAGroupingItemDAO getCiaGroupingItemDAO()
{
	return ciaGroupingItemDAO;
}

public void setCiaGroupingItemDAO( ICIAGroupingItemDAO ciaGroupingItemDAO )
{
	this.ciaGroupingItemDAO = ciaGroupingItemDAO;
}

public void save( Collection ciaGroupingItems )
{
    CIAGroupingItem item = null;
    Iterator listIter = ciaGroupingItems.iterator();
    while ( listIter.hasNext() )
    {
        item = (CIAGroupingItem)listIter.next();
        saveOrUpdate( item );
    }

}

public Collection findDistinctCIAGroupingItemsWithRecommendationsByCIACategory( Integer businessUnitId, Integer ciaSummaryStatusId,
                                                                               Integer ciaCategoryId )
{
    return getCiaGroupingItemDAO().findDistinctCIAGroupingItemsWithRecommendationsByCIACategory( businessUnitId, ciaSummaryStatusId,
                                                                                                 ciaCategoryId );
}

}
