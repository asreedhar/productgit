package biz.firstlook.cia.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.commons.util.MockLogger;

public class TestValuationService extends TestCase
{
private ValuationService service;

public TestValuationService( String name )
{
    super( name );
}

public void setUp()
{
    service = new ValuationService();
    service.setLogger( new MockLogger() );
}

public void testDaysToSale()
{
    Calendar cal = Calendar.getInstance();

    cal.set( Calendar.MONTH, 2 );
    cal.set( Calendar.DAY_OF_MONTH, 5 );
    cal.set( Calendar.YEAR, 1976 );
    Date start = cal.getTime();

    cal.set( Calendar.MONTH, 2 );
    cal.set( Calendar.DAY_OF_MONTH, 15 );
    cal.set( Calendar.YEAR, 1978 );
    Date end = cal.getTime();

    double expected = 740;
    double actual = service.daysToSale( start, end );
    assertEquals( expected, actual, .00 );
}

public void testNegativeDaysToSale()
{
    Calendar cal = Calendar.getInstance();

    cal.set( Calendar.MONTH, 2 );
    cal.set( Calendar.DAY_OF_MONTH, 5 );
    cal.set( Calendar.YEAR, 1978 );
    Date start = cal.getTime();

    cal.set( Calendar.MONTH, 2 );
    cal.set( Calendar.DAY_OF_MONTH, 4 );
    cal.set( Calendar.YEAR, 1978 );
    Date end = cal.getTime();

    double expected = 1;
    double actual = service.daysToSale( start, end );
    assertEquals( expected, actual, .01 );
}

public void testPopulateGroupingValuationAndUnitsSoldMultiple()
{
    Map groupings = new HashMap();
    List allVehicleSales = new ArrayList();

    Integer groupingDescriptionId = new Integer( 1 );
    Integer segmentId = new Integer( 1 );

    Object[] priorArray = new Object[] { groupingDescriptionId, segmentId, "R", new Double( 100 ), new Integer( 5 ) };
    Object[] forecastArray = new Object[] { groupingDescriptionId, segmentId, "W", new Double( 50 ), new Integer( 3 ) };

    allVehicleSales.add( priorArray );
    allVehicleSales.add( forecastArray );

    service.populateGroupingValuationAndUnitsSold( groupings, allVehicleSales );

    CIAEngineCalculationDataBean groupingValuationUnitsSold = (CIAEngineCalculationDataBean)groupings.get( groupingDescriptionId );

    assertEquals( 150, groupingValuationUnitsSold.getValuation(), 0 );
    assertEquals( 8, groupingValuationUnitsSold.getUnitsSold().intValue() );
    assertEquals( 5, groupingValuationUnitsSold.getRetailUnitsSold() );
    assertEquals( 3, groupingValuationUnitsSold.getWholesaleUnitsSold() );
}

public void testPopulateGroupingValuationAndUnitsSold()
{
    Map groupings = new HashMap();
    List allVehicleSales = new ArrayList();

    Integer groupingDescriptionId = new Integer( 1 );
    // segment Id is not used for anything
    Integer segmentId = new Integer( 1 );

    // Each vehicleSale result array: GD, SegId, R|W, Valutation, Units Sold
    Object[] retailSale1 = new Object[] { groupingDescriptionId, segmentId, "R", new Double( 100 ), new Integer( 5 ) };
    Object[] wholeSale1 = new Object[] { groupingDescriptionId, segmentId, "W", new Double( 100 ), new Integer( 6 ) };

    Object[] retailSale2 = new Object[] { groupingDescriptionId, segmentId, "R", new Double( 100 ), new Integer( 5 ) };
    Object[] wholeSale2 = new Object[] { groupingDescriptionId, segmentId, "W", new Double( 100 ), new Integer( 6 ) };

    // add all to arg
    allVehicleSales.add( retailSale1 );
    allVehicleSales.add( retailSale2 );
    allVehicleSales.add( wholeSale1 );
    allVehicleSales.add( wholeSale2 );

    service.populateGroupingValuationAndUnitsSold( groupings, allVehicleSales );

    CIAEngineCalculationDataBean groupingValuationUnitsSold = (CIAEngineCalculationDataBean)groupings.get( groupingDescriptionId );
    // units sold are kept in separate fields based on W or R
    assertEquals(groupingValuationUnitsSold.getRetailUnitsSold(), 10);
    assertEquals(groupingValuationUnitsSold.getWholesaleUnitsSold(), 12);
    assertEquals(groupingValuationUnitsSold.getUnitsSold().intValue(), 22);
    
    // valuation is a total of all W and R for that grouping desc   
    assertEquals("Valuation should be total of all retail and all wholesale", groupingValuationUnitsSold.getValuation(), new Double(400).doubleValue(), 0.0);
}

}