package biz.firstlook.cia.service;

import biz.firstlook.cia.model.CIAGroupingItemDetail;

public interface ICIAGroupingItemDetailService
{

public CIAGroupingItemDetail findForDealerByGroupingDescriptionAndDetailValue( Integer businessUnitId, Integer ciaSummaryStatusId,
                                                                              Integer groupingDescriptionId, String detailValue );

}