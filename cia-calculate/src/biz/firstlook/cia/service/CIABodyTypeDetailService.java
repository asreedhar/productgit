package biz.firstlook.cia.service;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;

import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.persistence.CIABodyTypeDetailContributionDaysSupplyRetriever;
import biz.firstlook.cia.persistence.CIABodyTypeDetailPersistence;

public class CIABodyTypeDetailService
{

private CIABodyTypeDetailPersistence persistence;
private CIABodyTypeDetailContributionDaysSupplyRetriever ciaBodyTypeDetailContributionDaysSupplyRetriever;

public CIABodyTypeDetailService()
{
	persistence = new CIABodyTypeDetailPersistence();
}

public CIABodyTypeDetail retrieveBySegmentAndSummaryId( Integer segmentId, Integer summaryId )
{
	CIABodyTypeDetail ciaBodyTypeDetail;

	ciaBodyTypeDetail = persistence.findBySegmentAndSummaryId( segmentId, summaryId );
	if ( ciaBodyTypeDetail == null )
	{
		ciaBodyTypeDetail = new CIABodyTypeDetail();
	}
	return ciaBodyTypeDetail;
}

public List retrieveCIABodyTypeDetailOrderedByContribution( int dealerId, int ciaSummaryId )
{

	List ciaBodyTypeDetailButtons = ciaBodyTypeDetailContributionDaysSupplyRetriever.retrieveCIABodyTypeDetails( dealerId, true, new Timestamp(new Date().getTime()), ciaSummaryId );

	ReverseComparator comparator = new ReverseComparator( new BeanComparator( "percentContribution" ) );
	Collections.sort( ciaBodyTypeDetailButtons, comparator );

	return ciaBodyTypeDetailButtons;
}

public int findRankingInCiaOrderedBodyTypeList( List ciaBodyTypeDetailsOrderedByContribution, int segmentId )
{
	Iterator it = ciaBodyTypeDetailsOrderedByContribution.iterator();
	int count = 0;
	CIABodyTypeDetail bodyType;
	
	while ( it.hasNext() )
	{
		bodyType = (CIABodyTypeDetail)it.next();
		count++;

		if ( bodyType.getSegment().getSegmentId().intValue() == segmentId )
			break;
	}
	
	return count;
}

public void setCiaBodyTypeDetailContributionDaysSupplyRetriever(
		CIABodyTypeDetailContributionDaysSupplyRetriever ciaBodyTypeDetailContributionDaysSupplyRetriever) {
	this.ciaBodyTypeDetailContributionDaysSupplyRetriever = ciaBodyTypeDetailContributionDaysSupplyRetriever;
}

}
