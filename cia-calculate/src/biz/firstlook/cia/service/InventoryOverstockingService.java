package biz.firstlook.cia.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.log4j.Logger;

import biz.firstlook.cia.calculator.CIAGroupingItemDetailCalculator;
import biz.firstlook.cia.context.CIAContext;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.InventoryOverstocking;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.IInventoryOverstockingPersistence;
import biz.firstlook.cia.persistence.InventoryOverstockingPersistence;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;
import biz.firstlook.transact.persist.service.vehicle.VehicleDAO;

public class InventoryOverstockingService
{

private IInventoryOverstockingPersistence persistence;
private static Logger logger = Logger.getLogger( InventoryOverstockingService.class );
private CIAGroupingItemService ciaGroupingItemService;
private CIAGroupingItemDetailService ciaGroupingItemDetailService;
private VehicleDAO vehicleDAO;
private IInventoryDAO inventoryDAO;


public InventoryOverstockingService( IInventoryOverstockingPersistence persistence )
{
	this.persistence = persistence;
}

public InventoryOverstockingService()
{
	this.persistence = new InventoryOverstockingPersistence();
}

public void generateInventoryOverstockings( CIAContext context, Integer ciaSummaryId )
{
	Integer dealerId = context.getDealerId();
	logger.info( " Run for Dealer Id: " + dealerId );
	List<CIAGroupingItem> ciaGroupingItems = getCiaGroupingItemService().retrieveCoreGroupingsByDealerIdAndStatus( dealerId, Status.CURRENT.getStatusId() );

	int overStockingsCreated = createOverStockings( dealerId, ciaGroupingItems, ciaSummaryId );
	logger.info( " Number of over stockings created: " + overStockingsCreated );
}

public int createOverStockings( Integer dealerId, List<CIAGroupingItem> groupingItems, Integer ciaSummaryId )
{
	Iterator<CIAGroupingItem> groupingItemIter = groupingItems.iterator();
	CIAGroupingItem groupingItem = null;
	List<Inventory> inventories = null;
	List<CIAGroupingItemDetail> targets = null;
	List<InventoryOverstocking> overStockings = new ArrayList<InventoryOverstocking>();

	while ( groupingItemIter.hasNext() )
	{
		groupingItem = groupingItemIter.next();
		inventories = inventoryDAO.findActiveByDealerIdAndGroupingDescriptionId(
																							dealerId,
																							groupingItem.getGroupingDescription().getGroupingDescriptionId() );
		targets = CIAGroupingItemDetailCalculator.filterDetailsByType(
														CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL.getCiaGroupingItemDetailTypeId().intValue(),
														groupingItem.getCiaGroupingItemDetails() );

		BeanComparator inventoryComparator = new BeanComparator( "daysInInventoryLongValue" );
		ReverseComparator reverseInventoryComparator = new ReverseComparator( inventoryComparator );
		Collections.sort( inventories, reverseInventoryComparator );

		if ( targets != null && !targets.isEmpty() )
		{
			overStockings.addAll( createOverstockingForTargets( inventories, targets, ciaSummaryId ) );
		}

	}
	return overStockings.size();
}

private List<InventoryOverstocking> calculateOverstockings( List<Inventory> inventories, int targetUnitsForModel, Integer ciaSummaryId )
{
	List<InventoryOverstocking> overStockings = new ArrayList<InventoryOverstocking>();
	if ( inventories.size() > targetUnitsForModel )
	{
		InventoryOverstocking inventoryOverstocking = null;
		Inventory inventory = null;
		int numberOverstocked = inventories.size() - targetUnitsForModel;
		for ( int i = 0; i < numberOverstocked; i++ )
		{
			inventory = inventories.get( i );
			long daysInInventory = inventory.getDaysInInventoryLongValue();
			if ( daysInInventory >= 30 )
			{
				inventoryOverstocking = new InventoryOverstocking();
				inventoryOverstocking.setInventoryId( inventory.getInventoryId() );
				inventoryOverstocking.setCiaSummaryId( ciaSummaryId );
				persistence.save( inventoryOverstocking );
				overStockings.add( inventoryOverstocking );
			}
		}
	}
	return overStockings;
}

protected List<InventoryOverstocking> createOverstockingForTargets( List<Inventory> inventories, List<CIAGroupingItemDetail> targets, Integer ciaSummaryId )
{
	List<InventoryOverstocking> overStockings = new ArrayList<InventoryOverstocking>();
	Iterator<CIAGroupingItemDetail> targetsIter = targets.iterator();
	CIAGroupingItemDetail target;

	while ( targetsIter.hasNext() )
	{
		target = targetsIter.next();

		overStockings.addAll( createOverstockingPerTarget( inventories, target, ciaSummaryId ) );
	}

	return overStockings;
}

private List<InventoryOverstocking> createOverstockingPerTarget( List<Inventory> inventories, CIAGroupingItemDetail target, Integer ciaSummaryId )
{
	List<Inventory> inventoriesInTarget = new ArrayList<Inventory>();
	Iterator<Inventory> inventoryIter = inventories.iterator();
	Inventory inventory = null;
	Vehicle vehicle = null;
	Integer vehicleYear;
	String vehicleYearStr;
	while ( inventoryIter.hasNext() )
	{
		inventory = inventoryIter.next();
		vehicle = vehicleDAO.findByPk( new Integer( inventory.getVehicleId() ) );
		vehicleYear = vehicle.getVehicleYear();
		if ( vehicleYear != null )
		{
			vehicleYearStr = vehicleYear.toString();
			if ( vehicleYearStr.equals( target.getDetailLevelValue() ) )
			{
				inventoriesInTarget.add( inventory );
			}
		}
	}

	return calculateOverstockings( inventoriesInTarget, target.getUnits().intValue(), ciaSummaryId );
}

public CIAGroupingItemService getCiaGroupingItemService()
{
	return ciaGroupingItemService;
}

public void setCiaGroupingItemService( CIAGroupingItemService ciaGroupingItemService )
{
	this.ciaGroupingItemService = ciaGroupingItemService;
}

public CIAGroupingItemDetailService getCiaGroupingItemDetailService()
{
	return ciaGroupingItemDetailService;
}

public void setCiaGroupingItemDetailService( CIAGroupingItemDetailService ciaGroupingItemDetailService )
{
	this.ciaGroupingItemDetailService = ciaGroupingItemDetailService;
}

public VehicleDAO getVehicleDAO()
{
	return vehicleDAO;
}

public void setVehicleDAO( VehicleDAO vehicleDAO )
{
	this.vehicleDAO = vehicleDAO;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

}
