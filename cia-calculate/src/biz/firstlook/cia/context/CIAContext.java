package biz.firstlook.cia.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import biz.firstlook.cia.calculator.CompositeBasisPeriod;
import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.commons.dealergrid.DealerGrid;

public class CIAContext
{

private ApplicationContext springApplicationContext;

private Integer dealerId;

private CompositeBasisPeriod ciaStoreTargetInventoryBasisPeriod;
private CompositeBasisPeriod ciaCoreModelDeterminationBasisPeriod;
private CompositeBasisPeriod ciaPowerzoneModelTargetInventoryBasisPeriod;
private CompositeBasisPeriod ciaCoreModelYearAllocationBasisPeriod;

private double totalStoreValuation;
private double nonCoreValuation;
private DealerGrid grid;
private Integer lowerUnitCostThreshold;
private Integer upperUnitCostThreshold;
private Integer unitCostBucketCreationThreshold;
private Integer bucketAllocationMinimumThreshold;
private Integer feGrossProfitThreshold;
private int marketPerformersDisplayThreshold;
private int sellThroughRate;
private int totalTargetInventory;
private int nonCoreUnits;
private int targetDaysSupply;
private boolean marketDataUpgrade;
private List<CIAEngineCalculationDataBean> powerzoneModelCalculationBeans;
private List<CIAEngineCalculationDataBean> winnerModelCalculationBeans;
private List<CIAEngineCalculationDataBean> goodBetModelCalculationBeans;
private List<CIAEngineCalculationDataBean> nonCoreModelCalculationBeans;
private List<CIAEngineCalculationDataBean> marketPerformerModelCalculationBeans;
private List<CIAEngineCalculationDataBean> managersChoiceModelCalculationBeans;
private Map<Integer,CIABodyTypeDetail> ciaBodyTypeDetails;

private Double coreMarketPenetrationSalesThreshold;

public CIAContext()
{
	powerzoneModelCalculationBeans = new ArrayList<CIAEngineCalculationDataBean>();
	winnerModelCalculationBeans = new ArrayList<CIAEngineCalculationDataBean>();
	goodBetModelCalculationBeans = new ArrayList<CIAEngineCalculationDataBean>();
	marketPerformerModelCalculationBeans = new ArrayList<CIAEngineCalculationDataBean>();
	managersChoiceModelCalculationBeans = new ArrayList<CIAEngineCalculationDataBean>();
	nonCoreModelCalculationBeans = new ArrayList<CIAEngineCalculationDataBean>();
}

public Integer getDealerId()
{
	return dealerId;
}

public void setDealerId( Integer integer )
{
	dealerId = integer;
}

public DealerGrid getGrid()
{
	return grid;
}

public void setGrid( DealerGrid grid )
{
	this.grid = grid;
}

public Integer getLowerUnitCostThreshold()
{
	return lowerUnitCostThreshold;
}

public Integer getUpperUnitCostThreshold()
{
	return upperUnitCostThreshold;
}

public void setLowerUnitCostThreshold( Integer integer )
{
	lowerUnitCostThreshold = integer;
}

public void setUpperUnitCostThreshold( Integer integer )
{
	upperUnitCostThreshold = integer;
}

public int getSellThroughRate()
{
	return sellThroughRate;
}

public void setSellThroughRate( int integer )
{
	sellThroughRate = integer;
}

public int getTotalTargetInventory()
{
	return totalTargetInventory;
}

public void setTotalTargetInventory( int i )
{
	totalTargetInventory = i;
}

public double getTotalStoreValuation()
{
	return totalStoreValuation;
}

public void setTotalStoreValuation( double d )
{
	totalStoreValuation = d;
}

public void setTargetDaysSupply( int integer )
{
	targetDaysSupply = integer;
}

public int getTargetDaysSupply()
{
	return targetDaysSupply;
}

public List<CIAEngineCalculationDataBean> getPowerzoneModelCalculationBeans()
{
	return powerzoneModelCalculationBeans;
}

public void setPowerzoneModelCalculationBeans( List<CIAEngineCalculationDataBean> list )
{
	powerzoneModelCalculationBeans = list;
}

public List<CIAEngineCalculationDataBean> getGoodBetModelCalculationBeans()
{
	return goodBetModelCalculationBeans;
}

public List<CIAEngineCalculationDataBean> getManagersChoiceModelCalculationBeans()
{
	return managersChoiceModelCalculationBeans;
}

public List<CIAEngineCalculationDataBean> getMarketPerformerModelCalculationBeans()
{
	return marketPerformerModelCalculationBeans;
}

public List<CIAEngineCalculationDataBean> getWinnerModelCalculationBeans()
{
	return winnerModelCalculationBeans;
}

public void setGoodBetModelCalculationBeans( List<CIAEngineCalculationDataBean> list )
{
	goodBetModelCalculationBeans = list;
}

public void setManagersChoiceModelCalculationBeans( List<CIAEngineCalculationDataBean> list )
{
	managersChoiceModelCalculationBeans = list;
}

public void setMarketPerformerModelCalculationBeans( List<CIAEngineCalculationDataBean> list )
{
	marketPerformerModelCalculationBeans = list;
}

public void setWinnerModelCalculationBeans( List<CIAEngineCalculationDataBean> list )
{
	winnerModelCalculationBeans = list;
}

public Integer getBucketAllocationMinimumThreshold()
{
	return bucketAllocationMinimumThreshold;
}

public Integer getUnitCostBucketCreationThreshold()
{
	return unitCostBucketCreationThreshold;
}

public void setBucketAllocationMinimumThreshold( Integer integer )
{
	bucketAllocationMinimumThreshold = integer;
}

public void setUnitCostBucketCreationThreshold( Integer integer )
{
	unitCostBucketCreationThreshold = integer;
}

public List<CIAEngineCalculationDataBean> getCoreModelCalculationBeans()
{
	List<CIAEngineCalculationDataBean> coreGroupingValuations = new ArrayList<CIAEngineCalculationDataBean>();
	coreGroupingValuations.addAll( powerzoneModelCalculationBeans );
	coreGroupingValuations.addAll( winnerModelCalculationBeans );
	coreGroupingValuations.addAll( goodBetModelCalculationBeans );

	return coreGroupingValuations;
}

public List<CIAEngineCalculationDataBean> getNonCoreModelCalculationBeans()
{
	return nonCoreModelCalculationBeans;
}

public double getNonCoreValuation()
{
	return nonCoreValuation;
}

public void setNonCoreModelCalculationBeans( List<CIAEngineCalculationDataBean> list )
{
	nonCoreModelCalculationBeans = list;
}

public int getNonCoreUnits()
{
	return nonCoreUnits;
}

public void setNonCoreUnits( int integer )
{
	nonCoreUnits = integer;
}

public void setNonCoreValuation( double double1 )
{
	nonCoreValuation = double1;
}

public int getMarketPerformersDisplayThreshold()
{
	return marketPerformersDisplayThreshold;
}

public void setMarketPerformersDisplayThreshold( int i )
{
	marketPerformersDisplayThreshold = i;
}

public Map<Integer,CIABodyTypeDetail> getCiaBodyTypeDetails()
{
	return ciaBodyTypeDetails;
}

public void setCiaBodyTypeDetails( Map<Integer,CIABodyTypeDetail> map )
{
	ciaBodyTypeDetails = map;
}

public Integer getFeGrossProfitThreshold()
{
	return feGrossProfitThreshold;
}

public void setFeGrossProfitThreshold( Integer integer )
{
	feGrossProfitThreshold = integer;
}

public boolean hasMarketDataUpgrade()
{
	return marketDataUpgrade;
}

public void setMarketDataUpgrade( boolean b )
{
	marketDataUpgrade = b;
}

public ApplicationContext getSpringApplicationContext()
{
	return springApplicationContext;
}

public void setSpringApplicationContext( ApplicationContext springApplicationContext )
{
	this.springApplicationContext = springApplicationContext;
}

public CompositeBasisPeriod getCiaCoreModelYearAllocationBasisPeriod()
{
	return ciaCoreModelYearAllocationBasisPeriod;
}

public void setCiaCoreModelYearAllocationBasisPeriod( CompositeBasisPeriod ciaCoreModelYearAllocationBasisPeriod )
{
	this.ciaCoreModelYearAllocationBasisPeriod = ciaCoreModelYearAllocationBasisPeriod;
}

public CompositeBasisPeriod getCiaCoreModelDeterminationBasisPeriod()
{
	return ciaCoreModelDeterminationBasisPeriod;
}

public void setCiaCoreModelDeterminationBasisPeriod( CompositeBasisPeriod ciaPowerzoneModelDeterminationBasisPeriod )
{
	this.ciaCoreModelDeterminationBasisPeriod = ciaPowerzoneModelDeterminationBasisPeriod;
}

public CompositeBasisPeriod getCiaPowerzoneModelTargetInventoryBasisPeriod()
{
	return ciaPowerzoneModelTargetInventoryBasisPeriod;
}

public void setCiaPowerzoneModelTargetInventoryBasisPeriod( CompositeBasisPeriod ciaPowerzoneModelTargetInventoryBasisPeriod )
{
	this.ciaPowerzoneModelTargetInventoryBasisPeriod = ciaPowerzoneModelTargetInventoryBasisPeriod;
}

public CompositeBasisPeriod getCiaStoreTargetInventoryBasisPeriod()
{
	return ciaStoreTargetInventoryBasisPeriod;
}

public void setCiaStoreTargetInventoryBasisPeriod( CompositeBasisPeriod ciaStoreTargetInventoryBasisPeriod )
{
	this.ciaStoreTargetInventoryBasisPeriod = ciaStoreTargetInventoryBasisPeriod;
}

public void setCoreMarketPenetrationSalesThreshold( Double vehicleSaleThresholdForCoreMarketPenetration )
{
	this.coreMarketPenetrationSalesThreshold = vehicleSaleThresholdForCoreMarketPenetration;
}

public Double getCoreMarketPenetrationSalesThreshold()
{
	return coreMarketPenetrationSalesThreshold;
}

}