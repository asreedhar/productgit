package biz.firstlook.cia.calculator;

public class BucketException extends Exception
{

private static final long serialVersionUID = 4918052312908204162L;

public BucketException()
{
    super();
}

public BucketException( String message )
{
    super(message);
}

public BucketException( String message, Throwable cause )
{
    super(message, cause);
}

public BucketException( Throwable cause )
{
    super(cause);
}

}
