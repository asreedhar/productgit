package biz.firstlook.cia.calculator;

import junit.framework.TestCase;
import biz.firstlook.commons.util.MockLogger;

public class TestValuationCalculator extends TestCase
{

private ValuationCalculator calculator;

public TestValuationCalculator( String arg0 )
{
    super(arg0);
}

public void setUp()
{
    calculator = new ValuationCalculator(new MockLogger());
}

public void testCalculateValuationNoBackend()
{
    ValuationDeal deal = new ValuationDeal(0, 10, 1000, 2100);
    double discountRate = 0.1025;
    int timeHorizon = 20;

    double valuation = calculator.calculateValuation(deal, discountRate,
            timeHorizon, new Boolean(false));

    assertEquals("Valuation should be 1000", 1000, valuation, 0);
}

public void testCalculateValuationIncludeBackend()
{
    ValuationDeal deal = new ValuationDeal(0, 10, 1000, 2100);
    deal.setBackendGrossProfit(500);
    double discountRate = 0.1025;
    int timeHorizon = 20;

    double valuation = calculator.calculateValuation(deal, discountRate,
            timeHorizon, new Boolean(true));

    assertEquals("Valuation should be 1500", 1500, valuation, 0);
}

public void testCalculateValuationTimeHorizonZero()
{
    ValuationDeal deal = new ValuationDeal(0, 10, 1000, 2100);

    double discountRate = 0.1025;
    int timeHorizon = 0;

    double valuation = calculator.calculateValuation(deal, discountRate,
            timeHorizon, new Boolean(false));

    assertEquals("Valuation should be 0", 0, valuation, 0);

}

public void testCalculateValuationDiscountRateNegative()
{
    ValuationDeal deal = new ValuationDeal(0, 10, 1000, 2100);

    double discountRate = -1;
    int timeHorizon = 20;

    double valuation = calculator.calculateValuation(deal, discountRate,
            timeHorizon, new Boolean(false));

    assertEquals("Valuation should be 0", 0, valuation, 0);
}

public void testValidateDiscountRateZero()
{
    double discountRate = calculator.validateDiscountRate(0);

    assertEquals(0, discountRate, 0);
}

public void testValidateDiscountRateNegative()
{
    double discountRate = calculator.validateDiscountRate(-.0006);

    assertEquals(0, discountRate, 0);
}

public void testValidateDiscountRatePositive()
{
    double discountRate = calculator.validateDiscountRate(.0011);

    assertEquals(discountRate, discountRate, 0);
}

}
