package biz.firstlook.cia.calculator;

public class BasisPeriod extends biz.firstlook.commons.date.BasisPeriod
{

private int totalUnitsSold;

public BasisPeriod()
{
    super();
}

public BasisPeriod( int days, int totalUnitsSold, boolean forecast )
{
    setDays(days);
    setTotalUnitsSold(totalUnitsSold);
    setForecast(forecast);
}

public BasisPeriod( int days, boolean forecast )
{
    super(days, forecast);
}

public int getTotalUnitsSold()
{
    return totalUnitsSold;
}

public void setTotalUnitsSold( int i )
{
    totalUnitsSold = i;
}

}
