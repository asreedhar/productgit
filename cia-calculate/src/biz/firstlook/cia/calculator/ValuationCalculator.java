package biz.firstlook.cia.calculator;

import java.util.Collection;

import biz.firstlook.commons.util.ILogger;

public class ValuationCalculator
{

private DiscountRateCalculator discountRateCalculator;
private TimeHorizonCalculator timeHorizonCalculator;

public ValuationCalculator( ILogger logger )
{
	discountRateCalculator = new DiscountRateCalculator( logger );
	timeHorizonCalculator = new TimeHorizonCalculator( logger );
}

public double calculateDiscountRate( Collection valuationDeals, int percentileMargin )
{
	double discountRate = discountRateCalculator.calculate( valuationDeals, percentileMargin );
	return validateDiscountRate( discountRate );
}

public double calculateTimeHorizon( Collection valuationDeals, int percentileDaysToSale )
{
	return timeHorizonCalculator.calculate( valuationDeals, percentileDaysToSale );
}

public double calculateValuation( ValuationDeal deal, double discountRate, double timeHorizon, Boolean includeBackEndInValuation )
{
	double daysToSale = deal.getDaysToSale();
	double salePrice = deal.getSalePrice();
	double unitCost = deal.getUnitCost();
	double backendGrossProfit = deal.getBackendGrossProfit();

	if ( ( timeHorizon <= 0 ) || ( discountRate < 0 ) )
	{
		return 0;
	}

	double valuation;
	valuation = ( salePrice / ( Math.pow( ( 1.0 + discountRate ), ( daysToSale / timeHorizon ) ) ) ) - unitCost;

	if ( includeBackEndInValuation.booleanValue() )
	{
		valuation += backendGrossProfit;
	}

	return valuation;
}

double validateDiscountRate( double discountRate )
{
	if ( discountRate < 0 )
	{
		return 0.0;
	}
	else
	{
		return discountRate;
	}
}

}
