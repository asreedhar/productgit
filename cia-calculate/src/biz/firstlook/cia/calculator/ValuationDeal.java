package biz.firstlook.cia.calculator;

public class ValuationDeal
{
private double feGrossProfit;
private double daysToSale;
private double unitCost;
private double salePrice;
private double backendGrossProfit;
private transient int inventoryId;

public ValuationDeal()
{
}

public ValuationDeal( double grossProfit, double daysToSale, double unitCost,
        double salePrice )
{
    this.feGrossProfit = grossProfit;
    this.daysToSale = daysToSale;
    this.unitCost = unitCost;
    this.salePrice = salePrice;
}

public double getDaysToSale()
{
    return daysToSale;
}

public double getFeGrossProfit()
{
    return feGrossProfit;
}

public double getSalePrice()
{
    return salePrice;
}

public double getUnitCost()
{
    return unitCost;
}

public void setDaysToSale( double d )
{
    daysToSale = d;
}

public void setFeGrossProfit( double d )
{
    feGrossProfit = d;
}

public void setSalePrice( double d )
{
    salePrice = d;
}

public void setUnitCost( double d )
{
    unitCost = d;
}

public double getBackendGrossProfit()
{
    return backendGrossProfit;
}

public void setBackendGrossProfit( double d )
{
    backendGrossProfit = d;
}

public int getInventoryId()
{
    return inventoryId;
}

public void setInventoryId( int i )
{
    inventoryId = i;
}

}
