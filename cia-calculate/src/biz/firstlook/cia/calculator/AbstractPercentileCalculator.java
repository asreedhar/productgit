package biz.firstlook.cia.calculator;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.math.stat.StatUtils;

import biz.firstlook.commons.util.ILogger;
import biz.firstlook.commons.util.PropertyFinder;

public abstract class AbstractPercentileCalculator
{
// noah test
protected ILogger logger;

public double calculate( Collection valuationDeals, int percentile )
{
    Iterator valuationDealsIter = valuationDeals.iterator();
    if ( valuationDeals.size() == 0 )
    {
        return 0;
    }
    double[] values = new double[valuationDeals.size()];
    for (int i = 0; valuationDealsIter.hasNext(); i++)
    {
        ValuationDeal valuationDeal = (ValuationDeal) valuationDealsIter.next();
        values[i] = createValue(valuationDeal);
    }
    return calculatePercentile(values, percentile);
}

double calculatePercentile( double[] values, double percentile )
{
    return StatUtils.percentile(values, percentile);
}

public abstract double createValue( ValuationDeal deal );

protected int getLogLevel( String propertyName )
{
    PropertyFinder finder = new PropertyFinder("Commons");

    return Integer.parseInt(finder.getProperty(propertyName));
}

}
