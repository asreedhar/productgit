package biz.firstlook.cia.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.apache.commons.math.stat.univariate.moment.Mean;
import org.apache.commons.math.stat.univariate.moment.StandardDeviation;
import org.apache.commons.math.stat.univariate.rank.Min;

import biz.firstlook.cia.model.OptimalBucketStructure;
import biz.firstlook.commons.util.BaseScriptJob;

public abstract class Bucketizer
{

protected BaseScriptJob loggingEngine;

public static final int FITTY_TOO_WEEK = 52;
public static final int LEAP_YEAR_DAYS = 366;

public static final int[] BUCKET_SIZES =
 { 2000, 3000 };
public static final int[] NUM_OF_BUCKETS =
 { 6, 8, 10 };

public Bucketizer()
{
    super();
}

public Bucketizer( BaseScriptJob loggingEngine )
{
    this.loggingEngine = loggingEngine;
}

public IntRange[] retrieveBuckets( double[] doublePoints,
        OptimalBucketStructure structure, int lowerUnitCostThreshold )
        throws BucketException
{
    double[] doubleBucketSize = toDoubleArray(BUCKET_SIZES);

    Arrays.sort(doublePoints);
    
    DoubleRange[] tempBuckets = createBuckets(doublePoints, doubleBucketSize,
            NUM_OF_BUCKETS, structure, lowerUnitCostThreshold);

    double[] rangeCount = calculateRangeCounts(doublePoints, tempBuckets);
    IntRange[] optimalBuckets = transformBuckets(tempBuckets);
    structure.setRangeCounts(rangeCount);

    return optimalBuckets;
}

private IntRange[] transformBuckets( DoubleRange[] doubleBuckets )
{
    IntRange[] buckets = new IntRange[doubleBuckets.length];
    for (int i = 0; i < doubleBuckets.length; i++)
    {
        buckets[i] = new IntRange(doubleBuckets[i].getMinimumInteger(),
                doubleBuckets[i].getMaximumInteger());
    }

    return buckets;
}

private double[] toDoubleArray( int[] intArray )
{
    double[] doubleArray = new double[intArray.length];

    for (int i = 0; i < intArray.length; i++)
    {
        doubleArray[i] = (double) intArray[i];
    }

    return doubleArray;
}

private DoubleRange[] createBuckets( double[] points, double[] bucketSizes,
        int[] numBuckets, OptimalBucketStructure structure,
        int lowerUnitCostThreshold ) throws BucketException
{
    Min min = new Min();
    if ( min.evaluate(points) < 0.0 )
    {
        throw new BucketException(
                "Unrealistic points data supplied, negative number");
    }
    double unroundedMedian = calculateMedian(points);
    double median = roundNearestThousand(unroundedMedian);
    structure.setMedianUnitCost(median);

    double[] meanUnitsInBuckets = new double[bucketSizes.length
            * numBuckets.length];
    double[] standardDeviation = new double[bucketSizes.length
            * numBuckets.length];
    double[] stdDevOverMeanValues = new double[bucketSizes.length
            * numBuckets.length];
    Map bucketParamMap = new HashMap();

    int paramsIndex = 0;
    for (int i = 0; i < bucketSizes.length; i++)
    {
        double bucketSize = bucketSizes[i];
        for (int j = 0; j < numBuckets.length; j++)
        {
            int numOfBuckets = numBuckets[j];

            BucketParameters bucketParameters = new BucketParameters();
            bucketParameters.setNumberOfBuckets(numOfBuckets);
            bucketParameters.setBucketSize(bucketSize);
            bucketParamMap.put(new Integer(paramsIndex), bucketParameters);

            DoubleRange[] ranges = createRanges(median, bucketSize,
                    numOfBuckets, lowerUnitCostThreshold);

            double[] rangeCount = calculateRangeCounts(points, ranges);

            Mean meanie = new Mean();
            double meanUnits = meanie.evaluate(rangeCount);

            StandardDeviation stdDev = new StandardDeviation();
            double std = stdDev.evaluate(rangeCount);

            double stdDevOverMean = calculateStdDevOverMean(meanUnits, std);

            stdDevOverMeanValues[paramsIndex] = stdDevOverMean;
            meanUnitsInBuckets[paramsIndex] = meanUnits;
            standardDeviation[paramsIndex] = std;
            paramsIndex++;
        }
    }

    int indexMin = indexOfMin(stdDevOverMeanValues);

    BucketParameters params = (BucketParameters) bucketParamMap
            .get(new Integer(indexMin));
    double bucketSize = params.getBucketSize();
    structure.setBucketSize(bucketSize);
    int numberOfBuckets = params.getNumberOfBuckets();
    structure.setNumOfBuckets(numberOfBuckets);
    structure.setMeanPercentage(meanUnitsInBuckets[indexMin]);
    structure.setStandardDeviation(standardDeviation[indexMin]);

    return createRanges(median, bucketSize, numberOfBuckets,
            lowerUnitCostThreshold);

}

protected abstract double calculateMedian( double[] points );

int indexOfMin( double[] stdDevOverMeanValues )
{
    Min minimum = new Min();
    double minO = minimum.evaluate(stdDevOverMeanValues);

    int indexMinO = ArrayUtils.indexOf(stdDevOverMeanValues, minO);
    return indexMinO;
}

double calculateStdDevOverMean( double meanUnitsInBuckets,
        double standardDeviation )
{
    if ( meanUnitsInBuckets == 0 )
    {
        return 0;
    } else
    {
        return (standardDeviation / meanUnitsInBuckets);
    }
}

DoubleRange[] createRanges( double median, double bucketSize, int numBuckets,
        int lowerUnitCostThreshold )
{
    List rangesList = new ArrayList();

    double upperBound = firstBucketUpperBound(
            numberBucketsBelowMedian(numBuckets), bucketSize, median);
    double lowerBound = upperBound - bucketSize;

    if ( lowerBound > lowerUnitCostThreshold )
    {
        lowerBound = lowerUnitCostThreshold;
    }

    for (int i = 0; i < numBuckets; i++)
    {
        if ( i == numBuckets - 1 )
        {
            addRange(rangesList, lowerBound, Double.MAX_VALUE,
                    lowerUnitCostThreshold);
        } else
        {
            addRange(rangesList, lowerBound, upperBound, lowerUnitCostThreshold);
        }
        upperBound += bucketSize;
        lowerBound = upperBound - bucketSize;
    }

    return (DoubleRange[]) rangesList
            .toArray(new DoubleRange[rangesList.size()]);
}

protected abstract void addRange( List ranges, double lowerBound,
        double upperBound, int lowerUnitCostThreshold );

int numberBucketsBelowMedian( int numBuckets )
{
    return (int) (numBuckets) / 2;
}

double firstBucketUpperBound( int numBucketsBelowMedian, double bucketSize,
        double medianUnitCost )
{
    return medianUnitCost - ((numBucketsBelowMedian - 1) * bucketSize);
}

double roundNearestThousand( double median )
{
    if ( !Double.isNaN(median) && !Double.isInfinite(median) )
    {
        return Math.round(median / 1000.0) * 1000.0;
    } else
    {
        return median;
    }
}

double[] calculateRangeCounts( double[] points, DoubleRange[] ranges )
{
    double[] rangeCount = new double[ranges.length];
    Map unitCosts = new MultiValueMap();

    for (int i = 0; i < points.length; i++)
    {
        double point = points[i];

        for (int j = 0; j < ranges.length; j++)
        {
            DoubleRange range = ranges[j];
            if ( point >= range.getMinimumDouble()
                    && point < range.getMaximumDouble() )
            {
                rangeCount[j] = ++rangeCount[j];
                unitCosts.put(new Integer(j), new Double(point));
                break;
            }
        }
    }
    return rangeCount;
}

private class BucketParameters
{
private double bucketSize;
private int numberOfBuckets;

public BucketParameters()
{
}

public double getBucketSize()
{
    return bucketSize;
}

public int getNumberOfBuckets()
{
    return numberOfBuckets;
}

public void setBucketSize( double size )
{
    bucketSize = size;
}

public void setNumberOfBuckets( int number )
{
    numberOfBuckets = number;
}
}

}