package biz.firstlook.cia.calculator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;

public class CIAGroupingItemDetailCalculator
{

public static final int HIGH_PRICE_POINT_VALUE = 10000000;

public static List<String> filterDetailValuesByTypeAndLevel( int detailType, int levelType, Collection<CIAGroupingItemDetail> details )
{
	List<String> values = new ArrayList<String>();
	Iterator<CIAGroupingItemDetail> detailIter = details.iterator();
	CIAGroupingItemDetail detail;
	while ( detailIter.hasNext() )
	{
		detail = detailIter.next();
		if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == detailType
				&& detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == levelType )
		{
			values.add( detail.getDetailLevelValue() );
		}
	}

	return values;
}

public static List<String> filterDetailValuesByType( int detailType, Collection<CIAGroupingItemDetail> details )
{
	List<String> values = new ArrayList<String>();
	Iterator<CIAGroupingItemDetail> detailIter = details.iterator();
	CIAGroupingItemDetail detail;
	while ( detailIter.hasNext() )
	{
		detail = detailIter.next();
		if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == detailType )
		{
			values.add( detail.getDetailLevelValue() );
		}
	}

	return values;
}

public static List<CIAGroupingItemDetail> filterDetailsByTypeAndLevel( int detailType, int levelType, Collection<CIAGroupingItemDetail> details )
{
	List<CIAGroupingItemDetail> units = new ArrayList<CIAGroupingItemDetail>();
	Iterator<CIAGroupingItemDetail> detailIter = details.iterator();
	CIAGroupingItemDetail detail;
	while ( detailIter.hasNext() )
	{
		detail = detailIter.next();
		if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == detailType
				&& detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == levelType )
		{
			units.add( detail );
		}
	}

	return units;
}

public static List<CIAGroupingItemDetail> filterDetailsByType( int detailType, Collection<CIAGroupingItemDetail> details )
{
	List<CIAGroupingItemDetail> units = new ArrayList<CIAGroupingItemDetail>();
	Iterator<CIAGroupingItemDetail> detailIter = details.iterator();
	CIAGroupingItemDetail detail;
	while ( detailIter.hasNext() )
	{
		detail = detailIter.next();
		if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == detailType )
		{
			units.add( detail );
		}
	}

	return units;
}

public static List<String> filterUnitCostRangesByType( int detailType, Collection<CIAGroupingItemDetail> details )
{
	List<String> units = new ArrayList<String>();
	List<CIAGroupingItemDetail> rangeDetails = new ArrayList<CIAGroupingItemDetail>();
	Iterator<CIAGroupingItemDetail> detailIter = details.iterator();
	CIAGroupingItemDetail detail;
	String unitCostRangeString;
	while ( detailIter.hasNext() )
	{
		detail = detailIter.next();
		if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == detailType
				&& detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == CIAGroupingItemDetailLevel.UNIT_COST.getCiaGroupingItemDetailLevelId().intValue() )
		{
			rangeDetails.add( detail );
		}
	}
	BeanComparator rangeDetailComparator = new BeanComparator( "lowRange" );
	Collections.sort( rangeDetails, rangeDetailComparator );
	Iterator<CIAGroupingItemDetail> rangeDetailIter = rangeDetails.iterator();
	while( rangeDetailIter.hasNext() )
	{
		detail = rangeDetailIter.next();
		unitCostRangeString = constructUnitCostRangeString( detail );
		units.add( unitCostRangeString );
	}

	return units;
}

public static int sumBuyAmounts( Collection buyAmountDetails )
{
	int buyAmountSum = 0;
	Iterator detailIter = buyAmountDetails.iterator();
	CIAGroupingItemDetail detail;
	while ( detailIter.hasNext() )
	{
		detail = (CIAGroupingItemDetail)detailIter.next();
		buyAmountSum += detail.getUnits().intValue();
	}
	return buyAmountSum;
}

public static String constructUnitCostRangeString( CIAGroupingItemDetail unitCostRangeDetail )
{
	Integer lowerRange = unitCostRangeDetail.getLowRange();
	Integer upperRange = unitCostRangeDetail.getHighRange();
	if ( lowerRange == null || lowerRange.intValue() <= 0 )
	{
		return ( "Under $" + upperRange );
	}
	else if ( upperRange.intValue() == HIGH_PRICE_POINT_VALUE )
	{
		if ( lowerRange.intValue() == Integer.MIN_VALUE )
		{
			return "Model ";
		}
		else
		{
			return ( "Over $" + lowerRange );
		}
	}
	else
	{
		return ( "$" + lowerRange + " - $" + upperRange );
	}
}

}
