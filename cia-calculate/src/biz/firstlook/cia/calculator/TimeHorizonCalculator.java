package biz.firstlook.cia.calculator;

import biz.firstlook.commons.util.ILogger;

public class TimeHorizonCalculator extends AbstractPercentileCalculator
{

public TimeHorizonCalculator( ILogger logger )
{
    this.logger = logger;
}

public double createValue( ValuationDeal deal )
{
    double daysToSale = deal.getDaysToSale();
    logger
            .log(
                    "Days to sale for Sale of Inventory ID "
                            + deal.getInventoryId() + "=" + daysToSale,
                    getLogLevel("DiscountRateTimeHorizonProcessor.DaysToSaleForEachRecord"));
    return daysToSale;
}

}
