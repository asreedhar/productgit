package biz.firstlook.cia.calculator;

import biz.firstlook.commons.util.ILogger;

public class DiscountRateCalculator extends AbstractPercentileCalculator
{

public DiscountRateCalculator( ILogger logger )
{
	this.logger = logger;
}

public double createValue( ValuationDeal deal )
{
	double grossProfit = deal.getFeGrossProfit();
	double salePrice = deal.getSalePrice();
	double margin = calculateMargin( grossProfit, salePrice );

	logger.log( "FE Gross for Sale of InventoryID " + deal.getInventoryId() + "=" + deal.getFeGrossProfit(),
				getLogLevel( "DiscountRateTimeHorizonProcessor.FEGrossProfitForEachRecord" ) );
	logger.log( "Selling price for Sale of InventoryID " + deal.getInventoryId() + "=" + deal.getSalePrice(),
				getLogLevel( "DiscountRateTimeHorizonProcessor.SellingPriceForEachRecord" ) );
	logger.log( "Margin for Sale of InventoryID " + deal.getInventoryId() + "=" + margin,
				getLogLevel( "DiscountRateTimeHorizonProcessor.MarginForEachRecord" ) );

	return margin;
}

double calculateMargin( double grossProfit, double salePrice )
{
	if ( salePrice != grossProfit )
	{
		double margin = ( grossProfit ) / ( salePrice - grossProfit );
		return margin;
	}
	else
	{
		return 0;
	}
}
}
