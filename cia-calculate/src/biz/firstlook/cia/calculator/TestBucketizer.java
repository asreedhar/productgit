package biz.firstlook.cia.calculator;

import junit.framework.TestCase;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.apache.commons.math.stat.univariate.moment.Mean;
import org.apache.commons.math.stat.univariate.moment.StandardDeviation;

import biz.firstlook.cia.model.OptimalBucketStructure;

public class TestBucketizer extends TestCase
{

Bucketizer bucketizer;

public TestBucketizer( String name )
{
    super(name);
}

protected void setUp() throws Exception
{

    bucketizer = new CoreModelBucketizer();
}

public void testRoundThousand()
{
    assertEquals(1000.0, bucketizer.roundNearestThousand(1250.0),
            Double.MIN_VALUE);
    assertEquals(2342.532e6, bucketizer.roundNearestThousand(2342.53242e6),
            Double.MIN_VALUE);
    assertEquals(-1000.0, bucketizer.roundNearestThousand(-1250.0),
            Double.MIN_VALUE);
    assertEquals(2000.0, bucketizer.roundNearestThousand(1850.0),
            Double.MIN_VALUE);
    assertTrue(Double.isNaN(bucketizer.roundNearestThousand(Double.NaN)));
    assertEquals(Double.POSITIVE_INFINITY, bucketizer
            .roundNearestThousand(Double.POSITIVE_INFINITY), Double.MIN_VALUE);
}

public void testCalculateMeanNormal()
{
    double[] array = new double[]
    { 1.0, 2.0, 3.0 };
    assertEquals(2.0, bucketizer.calculateMedian(array), Double.MIN_VALUE);

    array = new double[]
    { -232.22, 223.22, 3e2, 1.000, 2.3242 };
    assertEquals(2.3242, bucketizer.calculateMedian(array), 0.01);
}

public void testCalculateMeanCrazyNaN()
{
    double[] array = new double[]
    { Double.NaN, 2.0, 3.0 };
    assertTrue(Double.isNaN(bucketizer.calculateMedian(array)));
}

public void testCalculateMeanOutrageousInifity()
{
    double[] array = new double[]
    { Double.POSITIVE_INFINITY, 2.0, 3.0 };
    assertTrue(Double.isNaN(bucketizer.calculateMedian(array)));
}

public void testCreateRanges()
{
    DoubleRange[] doubleRange = bucketizer.createRanges(6000, 2000, 10, 4000);
    assertEquals("Range size", 10, doubleRange.length);

    DoubleRange range;

    range = doubleRange[0];
    assertEquals("Wrong min", -4000, range.getMinimumDouble(), 0);
    assertEquals("Wrong Max", -2000, range.getMaximumDouble(), 0);

    range = doubleRange[1];
    assertEquals("Wrong min", -2000, range.getMinimumDouble(), 0);
    assertEquals("Wrong Max", 0, range.getMaximumDouble(), 0);

    range = doubleRange[9];
    assertEquals("Wrong min", 14000, range.getMinimumDouble(), 0);
    assertEquals("Wrong Max", Double.MAX_VALUE, range.getMaximumDouble(), 0);
}

public void testFindNumberBucketsBelowMedian()
{
    assertEquals(5, bucketizer.numberBucketsBelowMedian(10));
    assertEquals(4, bucketizer.numberBucketsBelowMedian(8));
    assertEquals(3, bucketizer.numberBucketsBelowMedian(6));
}

public void testFirstBucketUpperBound()
{
    assertEquals(6500, bucketizer.firstBucketUpperBound(4, 500, 8000), 0);
}

public void testCalculateStdDevOverMean()
{
    double[] ranges = new double[]
    { 1.0, 2.0, 3.0 };
    double o = bucketizer.calculateStdDevOverMean(new Mean().evaluate(ranges),
            new StandardDeviation().evaluate(ranges));
    assertEquals("Invalid o Value ", 0.5, o, 0.01);
}

public void testCalculateStdDevOverMeanDoubleNaN()
{
    double[] ranges = new double[]
    { Double.NaN, 2.0, 3.0 };
    double o = bucketizer.calculateStdDevOverMean(new Mean().evaluate(ranges),
            new StandardDeviation().evaluate(ranges));
    assertTrue("Invalid o Value ", Double.isNaN(o));
}

public void testCalculateStdDevOverMeanZero()
{
    double[] ranges = new double[]
    { 0.0, 0.0, 0.0 };
    double o = bucketizer.calculateStdDevOverMean(new Mean().evaluate(ranges),
            new StandardDeviation().evaluate(ranges));
    assertEquals("Invalid o Value ", 0.0, o, Double.MIN_VALUE);
}

public void testIndexOfMinSimple()
{
    double[] ranges = new double[]
    { 1.0, 2.0, 3.0 };
    assertEquals("Wrong index of o", 0, bucketizer.indexOfMin(ranges));

}

public void testIndexOfMinNaN()
{
    double[] ranges = new double[]
    { 1.0, 2.0, Double.NaN };
    assertEquals("Wrong index of o", -1, bucketizer.indexOfMin(ranges));

}

public void testIndexOfMinPositiveInfinity()
{
    double[] ranges = new double[]
    { 1.0, 2.0, Double.POSITIVE_INFINITY };
    assertEquals("Wrong index of o", 0, bucketizer.indexOfMin(ranges));

}

public void testCalculateRangeCountDouble()
{
    double[] points = new double[]
    { 1.0, 2.0, 3.0 };
    DoubleRange[] doubleRange = new DoubleRange[]
    { new DoubleRange(1.0, 3.0), new DoubleRange(1.0, 3.0),
            new DoubleRange(1.0, 3.0) };

    double[] returnedDoubleArray = bucketizer.calculateRangeCounts(points,
            doubleRange);

    assertEquals("Not right value", 2.0, returnedDoubleArray[0],
            Double.MIN_VALUE);

}

public void testCalculateRangeCountBorderConditions()
{
    double[] points = new double[]
    { 1000.00, 2000.46, 2999.59, 3000.00 };
    DoubleRange[] doubleRanges = new DoubleRange[]
    { new DoubleRange(1000, 2000), new DoubleRange(2000, 3000),
            new DoubleRange(3000, 4000) };

    double[] returnedDoubleArray = bucketizer.calculateRangeCounts(points,
            doubleRanges);

    assertEquals(1.0, returnedDoubleArray[0], Double.MIN_VALUE);
    assertEquals(2.0, returnedDoubleArray[1], Double.MIN_VALUE);
    assertEquals(1.0, returnedDoubleArray[2], Double.MIN_VALUE);
}

public void testCreateBuckets() throws Exception
{
    double[] points = new double[]
    { 21625, 22200, 28833, 29429, 34295, 34541, 34554, 34746, 34847, 34912,
            36561, 37780 };

    OptimalBucketStructure structure = new OptimalBucketStructure();
    IntRange[] buckets = bucketizer.retrieveBuckets(points, structure, 4000);

    IntRange firstBucket = (IntRange) buckets[0];
    assertEquals("First Bucket Min should be 4000", 4000.0, firstBucket
            .getMinimumInteger(), Double.MIN_VALUE);
    assertEquals("First Bucket Max should be 29000", 29000.0, firstBucket
            .getMaximumInteger(), Double.MIN_VALUE);

    IntRange secondBucket = (IntRange) buckets[1];
    assertEquals("Second Bucket Min should be 29000", 29000.0, secondBucket
            .getMinimumInteger(), Double.MIN_VALUE);
    assertEquals("Second Bucket Max should be 32000", 32000.0, secondBucket
            .getMaximumInteger(), Double.MIN_VALUE);

    IntRange lastBucket = (IntRange) buckets[buckets.length - 1];
    assertEquals("Last Bucket Min should be 41000", 41000.0, lastBucket
            .getMinimumInteger(), Double.MIN_VALUE);
    assertEquals("Last Bucket Max should be MAX Integer", lastBucket
            .getMaximumInteger(), Integer.MAX_VALUE);

    assertEquals("Number of buckets should be 6.", 6, buckets.length);
    assertEquals("Bucket Size should be 3000.", 3000, buckets[1]
            .getMaximumInteger()
            - buckets[1].getMinimumInteger());

}

public void testBucketsNeverNegative() throws Exception
{
    double[] points = new double[]
    { -1500, -1000, -1100, -1600, -1700, -2200, -2300, -2400, -2900, -3000,
            -3500, -3600, -4100, -4200 };

    try
    {
        OptimalBucketStructure structure = new OptimalBucketStructure();
        bucketizer.retrieveBuckets(points, structure, 4000);
        fail("Should throw a BucketException");
    } catch (BucketException be)
    {
    }
}

}