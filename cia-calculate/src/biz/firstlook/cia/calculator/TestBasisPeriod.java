package biz.firstlook.cia.calculator;

import junit.framework.TestCase;

public class TestBasisPeriod extends TestCase
{
private BasisPeriod priorBasisPeriod;
private BasisPeriod forecastBasisPeriod;
private CompositeBasisPeriod compositePeriod;
// nk - cvs test
public TestBasisPeriod( String name )
{
    super(name);
}

public void testRateOfSaleZeroDays()
{
    priorBasisPeriod = new BasisPeriod(0, 5, false);
    forecastBasisPeriod = new BasisPeriod();
    compositePeriod = new CompositeBasisPeriod(forecastBasisPeriod,
            priorBasisPeriod);
    assertEquals(0, compositePeriod.rateOfSale(), 0);
}

public void testRateOfSaleForBasisPeriod()
{
    priorBasisPeriod = new BasisPeriod(10, 5, false);
    forecastBasisPeriod = new BasisPeriod();
    compositePeriod = new CompositeBasisPeriod(forecastBasisPeriod,
            priorBasisPeriod);
    assertEquals(.50, compositePeriod.rateOfSale(), 0);
}

public void testRateOfSaleForCompositePeriod()
{

    priorBasisPeriod = new BasisPeriod(100, 10, false);
    forecastBasisPeriod = new BasisPeriod(200, 20, true);
    compositePeriod = new CompositeBasisPeriod(forecastBasisPeriod,
            priorBasisPeriod);

    assertEquals(.10, compositePeriod.rateOfSale(), 0);
}

public void testTargetInventoryAmount()
{
    priorBasisPeriod = new BasisPeriod(10, 5, false);
    forecastBasisPeriod = new BasisPeriod();
    compositePeriod = new CompositeBasisPeriod(forecastBasisPeriod,
            priorBasisPeriod);
    assertEquals(10, compositePeriod.targetInventoryAmount(20), 0);
}

}
