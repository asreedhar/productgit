package biz.firstlook.cia.calculator;

import java.util.List;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.math.stat.univariate.rank.Median;

public class CoreModelBucketizer extends Bucketizer
{

public CoreModelBucketizer()
{
    super();
}

protected double calculateMedian( double[] points )
{
    Median medianFunctor = new Median();
    double median = medianFunctor.evaluate(points);
    return median;
}

protected void addRange( List ranges, double lowerBound, double upperBound,
        int lowerUnitCostThreshold )
{
    ranges.add(new DoubleRange(lowerBound, upperBound));
}

protected double calculateLowerBound( double value, int lowerUnitCostThreshold )
{
    return value > lowerUnitCostThreshold ? value : lowerUnitCostThreshold;
}

}
