package biz.firstlook.cia.calculator;

public class CompositeBasisPeriod
{

private BasisPeriod forecastBasisPeriod;
private BasisPeriod priorBasisPeriod;

public boolean isComposite()
{
    if ( forecastBasisPeriod.getDays() > 0 )
    {
        return true;
    } else
    {
        return false;
    }
}

public CompositeBasisPeriod( BasisPeriod forecastBasisPeriod,
        BasisPeriod priorBasisPeriod )
{
    this.forecastBasisPeriod = forecastBasisPeriod;
    this.priorBasisPeriod = priorBasisPeriod;

}

public BasisPeriod getForecastBasisPeriod()
{
    return forecastBasisPeriod;
}

public BasisPeriod getPriorBasisPeriod()
{
    return priorBasisPeriod;
}

public void setForecastBasisPeriod( BasisPeriod period )
{
    forecastBasisPeriod = period;
}

public void setPriorBasisPeriod( BasisPeriod period )
{
    priorBasisPeriod = period;
}

public int getDays()
{
    return forecastBasisPeriod.getDays() + priorBasisPeriod.getDays();
}

public int getTotalUnitsSold()
{
    return forecastBasisPeriod.getTotalUnitsSold()
            + priorBasisPeriod.getTotalUnitsSold();
}

public double rateOfSale()
{
    if ( getDays() != 0 )
    {
        return getTotalUnitsSold() / (double) getDays();
    } else
    {
        return 0;
    }
}

public double targetInventoryAmount( int targetDaysSupply )
{
    return rateOfSale() * targetDaysSupply;
}

}
