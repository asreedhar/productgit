package biz.firstlook.cia.calculator;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.cia.model.CIAEngineCalculationDataBean;
import biz.firstlook.cia.model.ITargetUnits;
import biz.firstlook.transact.persist.model.Inventory;

public class TargetInventoryCalculator
{

public TargetInventoryCalculator()
{
	super();
}

public int calculatePowerZoneTargetUnits( List powerzoneGroupingValuations, int targetDaysSupply, int days, int sellThrough )
{
	Iterator pzGroupingIterator = powerzoneGroupingValuations.iterator();
	float rateOfSale = 0;
	int pzUnits = 0;

	while ( pzGroupingIterator.hasNext() )
	{
		CIAEngineCalculationDataBean gvu = (CIAEngineCalculationDataBean)pzGroupingIterator.next();
		rateOfSale = gvu.getRetailUnitsSold() / (float)days;
		float unadjustedTargetDaysSupply = rateOfSale * targetDaysSupply;
		double sellThroughPercentage = (double)sellThrough / 100;
		gvu.setTotalTargetUnits( (int)Math.round( unadjustedTargetDaysSupply / sellThroughPercentage ) );
		pzUnits += gvu.getTotalTargetUnits();
	}

	return pzUnits;
}

public double sumTotalValuation( List groupingValuationUnitsSoldList )
{
	double totalValuation = 0.0;
	CIAEngineCalculationDataBean groupingValuationUnitsSold;
	Iterator groupingValuationUnitsSoldListIter = groupingValuationUnitsSoldList.iterator();
	while ( groupingValuationUnitsSoldListIter.hasNext() )
	{
		groupingValuationUnitsSold = (CIAEngineCalculationDataBean)groupingValuationUnitsSoldListIter.next();
		if ( groupingValuationUnitsSold.getValuation() < 0.0 )
		{
			groupingValuationUnitsSold.setValuation( 0.0 );
		}
		totalValuation += groupingValuationUnitsSold.getValuation();
	}
	return totalValuation;
}

/**
 * Calculates Target Units for each item in the list.
 * @param itargetUnitsList
 * @param totalValuation
 * @param totalUnits
 */
public void calculateTargetUnitsByValuation( List itargetUnitsList, double totalValuation, long totalUnits )
{
	int targetUnits = 0;

	BeanComparator comparator = new BeanComparator( "valuation" );
	Collections.sort( itargetUnitsList, comparator );

	Iterator itargetUnitsIter = itargetUnitsList.iterator();
	ITargetUnits itargetUnits = null;
	while ( itargetUnitsIter.hasNext() )
	{
		itargetUnits = (ITargetUnits)itargetUnitsIter.next();
		if ( totalUnits < 0 )
		{
			targetUnits = 0;
		}
		else
		{
			targetUnits = (int)Math.round( ( itargetUnits.getValuation() / totalValuation ) * totalUnits );
			totalUnits -= targetUnits;
		}
		totalValuation -= itargetUnits.getValuation();

		itargetUnits.setTotalTargetUnits( targetUnits );
	}
}

public double[] createUnitCosts( List inventoryItems )
{
	double[] unitCosts = new double[inventoryItems.size()];
	int i = 0;
	Iterator inventoryIter = inventoryItems.iterator();
	while ( inventoryIter.hasNext() )
	{
		Inventory inventory = (Inventory)inventoryIter.next();
		unitCosts[i] = inventory.getUnitCost();
		i++;
	}

	return unitCosts;
}

}
