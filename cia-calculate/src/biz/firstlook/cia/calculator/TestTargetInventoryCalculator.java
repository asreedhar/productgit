package biz.firstlook.cia.calculator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIAEngineCalculationDataBean;

public class TestTargetInventoryCalculator extends TestCase
{

private TargetInventoryCalculator calculator;

public TestTargetInventoryCalculator( String arg0 )
{
	super( arg0 );
	calculator = new TargetInventoryCalculator();
}

public void testcalculatePercentTotalValuation()
{
	List valuationList = new ArrayList();
	CIAEngineCalculationDataBean gvu1 = new CIAEngineCalculationDataBean();
	gvu1.setRetailUnitsSold( 4 );

	CIAEngineCalculationDataBean gvu2 = new CIAEngineCalculationDataBean();
	gvu2.setRetailUnitsSold( 6 );

	CIAEngineCalculationDataBean gvu3 = new CIAEngineCalculationDataBean();
	gvu3.setRetailUnitsSold( 10 );

	valuationList.add( gvu1 );
	valuationList.add( gvu2 );
	valuationList.add( gvu3 );

	calculator.calculatePowerZoneTargetUnits( valuationList, 40, 20, 90 );

	assertEquals( 9, ( (CIAEngineCalculationDataBean)valuationList.get( 0 ) ).getTotalTargetUnits() );
	assertEquals( 13, ( (CIAEngineCalculationDataBean)valuationList.get( 1 ) ).getTotalTargetUnits() );
	assertEquals( 22, ( (CIAEngineCalculationDataBean)valuationList.get( 2 ) ).getTotalTargetUnits() );
}

/*
public void testDetermineTargetInventoriesPerRange()
{
	int totalTargetInventory = 16;
	List ranges = new ArrayList();

	CIAUnitCostPointRange uc1 = new CIAUnitCostPointRange();
	uc1.setLowerRange( 4000 );
	uc1.setUpperRange( 10000 );
	uc1.setValuation( 26098 );
	ranges.add( uc1 );

	CIAUnitCostPointRange uc2 = new CIAUnitCostPointRange();
	uc2.setLowerRange( 10000 );
	uc2.setUpperRange( 11000 );
	uc2.setValuation( 25461 );
	ranges.add( uc2 );

	CIAUnitCostPointRange uc3 = new CIAUnitCostPointRange();
	uc3.setLowerRange( 11000 );
	uc3.setUpperRange( 12000 );
	uc3.setValuation( 20580 );
	ranges.add( uc3 );

	CIAUnitCostPointRange uc4 = new CIAUnitCostPointRange();
	uc4.setLowerRange( 12000 );
	uc4.setUpperRange( 13000 );
	uc4.setValuation( 38670 );
	ranges.add( uc4 );

	CIAUnitCostPointRange uc5 = new CIAUnitCostPointRange();
	uc5.setLowerRange( 13000 );
	uc5.setUpperRange( 14000 );
	uc5.setValuation( 19330 );
	ranges.add( uc5 );

	CIAUnitCostPointRange uc6 = new CIAUnitCostPointRange();
	uc6.setLowerRange( 14000 );
	uc6.setUpperRange( 10000000 );
	uc6.setValuation( 23707 );
	ranges.add( uc6 );

	Iterator rangesIter = ranges.iterator();
	CIAUnitCostPointRange tempRange = null;
	double tempValTotal = 0;

	while ( rangesIter.hasNext() )
	{
		tempRange = (CIAUnitCostPointRange)rangesIter.next();
		tempValTotal += tempRange.getValuation();
	}

	calculator.calculateTargetUnitsByValuation( ranges, tempValTotal, totalTargetInventory );

	rangesIter = ranges.iterator();
	tempRange = null;
	double tempTotal = 0.0;

	while ( rangesIter.hasNext() )
	{
		tempRange = (CIAUnitCostPointRange)rangesIter.next();
		tempTotal += tempRange.getRangeLevelTargetUnits();
	}

	assertEquals( tempTotal, totalTargetInventory, 0 );

	BeanComparator comparator = new BeanComparator( "valuation" );
	Collections.sort( ranges, comparator );

	assertEquals( 2, ( (CIAUnitCostPointRange)ranges.get( 0 ) ).getRangeLevelTargetUnits(), 0 );
	assertEquals( 2, ( (CIAUnitCostPointRange)ranges.get( 1 ) ).getRangeLevelTargetUnits(), 0 );
	assertEquals( 2, ( (CIAUnitCostPointRange)ranges.get( 2 ) ).getRangeLevelTargetUnits(), 0 );
	assertEquals( 3, ( (CIAUnitCostPointRange)ranges.get( 3 ) ).getRangeLevelTargetUnits(), 0 );
	assertEquals( 3, ( (CIAUnitCostPointRange)ranges.get( 4 ) ).getRangeLevelTargetUnits(), 0 );
	assertEquals( 4, ( (CIAUnitCostPointRange)ranges.get( 5 ) ).getRangeLevelTargetUnits(), 0 );
}
*/

public void testNonPZCoreModelTargetInventory()
{
	List valuationList = new ArrayList();
	CIAEngineCalculationDataBean gvu1 = new CIAEngineCalculationDataBean();
	gvu1.setValuation( 26098.0 );

	CIAEngineCalculationDataBean gvu2 = new CIAEngineCalculationDataBean();
	gvu2.setValuation( 25461.0 );

	CIAEngineCalculationDataBean gvu3 = new CIAEngineCalculationDataBean();
	gvu3.setValuation( 20580.0 );

	CIAEngineCalculationDataBean gvu4 = new CIAEngineCalculationDataBean();
	gvu4.setValuation( 38670.0 );

	CIAEngineCalculationDataBean gvu5 = new CIAEngineCalculationDataBean();
	gvu5.setValuation( 19330.0 );

	CIAEngineCalculationDataBean gvu6 = new CIAEngineCalculationDataBean();
	gvu6.setValuation( 23707.0 );

	valuationList.add( gvu1 );
	valuationList.add( gvu2 );
	valuationList.add( gvu3 );
	valuationList.add( gvu4 );
	valuationList.add( gvu5 );
	valuationList.add( gvu6 );

	long remainingUnits = 16;
	double remainingValue = 153846;

	calculator.calculateTargetUnitsByValuation( valuationList, remainingValue, remainingUnits );

	assertEquals( 2, ( (CIAEngineCalculationDataBean)valuationList.get( 0 ) ).getTotalTargetUnits() );
	assertEquals( 2, ( (CIAEngineCalculationDataBean)valuationList.get( 1 ) ).getTotalTargetUnits() );
	assertEquals( 2, ( (CIAEngineCalculationDataBean)valuationList.get( 2 ) ).getTotalTargetUnits() );
	assertEquals( 3, ( (CIAEngineCalculationDataBean)valuationList.get( 3 ) ).getTotalTargetUnits() );
	assertEquals( 3, ( (CIAEngineCalculationDataBean)valuationList.get( 4 ) ).getTotalTargetUnits() );
	assertEquals( 4, ( (CIAEngineCalculationDataBean)valuationList.get( 5 ) ).getTotalTargetUnits() );

}

public void testSegmentTargetUnits()
{
	List valuationList = new ArrayList();
	CIABodyTypeDetail detail1 = new CIABodyTypeDetail();
	detail1.setValuation( 26098.0 );

	CIABodyTypeDetail detail2 = new CIABodyTypeDetail();
	detail2.setValuation( 25461.0 );

	CIABodyTypeDetail detail3 = new CIABodyTypeDetail();
	detail3.setValuation( 20580.0 );

	CIABodyTypeDetail detail4 = new CIABodyTypeDetail();
	detail4.setValuation( 38670.0 );

	CIABodyTypeDetail detail5 = new CIABodyTypeDetail();
	detail5.setValuation( 19330.0 );

	CIABodyTypeDetail detail6 = new CIABodyTypeDetail();
	detail6.setValuation( 23707.0 );

	valuationList.add( detail1 );
	valuationList.add( detail2 );
	valuationList.add( detail3 );
	valuationList.add( detail4 );
	valuationList.add( detail5 );
	valuationList.add( detail6 );

	long remainingUnits = 16;
	double remainingValue = 153846;

	calculator.calculateTargetUnitsByValuation( valuationList, remainingValue, remainingUnits );

	assertEquals( 2, ( (CIABodyTypeDetail)valuationList.get( 0 ) ).getTotalTargetUnits(), 0 );
	assertEquals( 2, ( (CIABodyTypeDetail)valuationList.get( 1 ) ).getTotalTargetUnits(), 0 );
	assertEquals( 2, ( (CIABodyTypeDetail)valuationList.get( 2 ) ).getTotalTargetUnits(), 0 );
	assertEquals( 3, ( (CIABodyTypeDetail)valuationList.get( 3 ) ).getTotalTargetUnits(), 0 );
	assertEquals( 3, ( (CIABodyTypeDetail)valuationList.get( 4 ) ).getTotalTargetUnits(), 0 );
	assertEquals( 4, ( (CIABodyTypeDetail)valuationList.get( 5 ) ).getTotalTargetUnits(), 0 );
}

public void testSumTotalValuation()
{
	List valuationList = new ArrayList();
	CIAEngineCalculationDataBean gvu1 = new CIAEngineCalculationDataBean();
	gvu1.setValuation( 26098.0 );

	CIAEngineCalculationDataBean gvu2 = new CIAEngineCalculationDataBean();
	gvu2.setValuation( -25461.0 );

	valuationList.add( gvu1 );
	valuationList.add( gvu2 );

	double totalValuation = calculator.sumTotalValuation( valuationList );

	assertEquals( 26098.0, totalValuation, 0 );
}

}
