package biz.firstlook.cia.calculator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.commons.util.MockLogger;

public class TestTimeHorizonCalculator extends TestCase
{
private static final double ACCEPTABLE_DIFFERENCE = .00001;

private TimeHorizonCalculator calculator;

public TestTimeHorizonCalculator( String name )
{
    super(name);
}

public void setUp()
{
    calculator = new TimeHorizonCalculator(new MockLogger());
}

public void testCalculate()
{
    List deals = new ArrayList();
    deals.add(new ValuationDeal(0, 8, 0, 0));
    deals.add(new ValuationDeal(0, 11, 0, 0));
    deals.add(new ValuationDeal(0, 14, 0, 0));
    deals.add(new ValuationDeal(0, 17, 0, 0));
    deals.add(new ValuationDeal(0, 20, 0, 0));
    deals.add(new ValuationDeal(0, 23, 0, 0));
    deals.add(new ValuationDeal(0, 26, 0, 0));
    deals.add(new ValuationDeal(0, 29, 0, 0));
    deals.add(new ValuationDeal(0, 32, 0, 0));
    deals.add(new ValuationDeal(0, 35, 0, 0));
    deals.add(new ValuationDeal(0, 38, 0, 0));
    deals.add(new ValuationDeal(0, 41, 0, 0));
    deals.add(new ValuationDeal(0, 44, 0, 0));
    deals.add(new ValuationDeal(0, 47, 0, 0));
    deals.add(new ValuationDeal(0, 50, 0, 0));

    double expectedTimeHorizon = 48.2;
    double actualTimeHorizon = calculator.calculate(deals, 90);

    assertEquals(expectedTimeHorizon, actualTimeHorizon, ACCEPTABLE_DIFFERENCE);
}

public void testTimeHorizonNoDeals()
{
    List deals = new ArrayList();

    double expectedRate = .00;
    double actualRate = calculator.calculate(deals, 90);

    assertEquals(expectedRate, actualRate, ACCEPTABLE_DIFFERENCE);
}
}
