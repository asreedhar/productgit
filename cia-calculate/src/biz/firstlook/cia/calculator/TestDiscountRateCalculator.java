package biz.firstlook.cia.calculator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.commons.util.MockLogger;

public class TestDiscountRateCalculator extends TestCase
{
private static final double ACCEPTABLE_DIFFERENCE = .00001;

private DiscountRateCalculator calculator;

public TestDiscountRateCalculator( String arg0 )
{
    super(arg0);
}

public void setUp()
{
    calculator = new DiscountRateCalculator(new MockLogger());
}

public void testCalculateMargin()
{
    double salePrice = 1000;
    double grossProfit = 500;
    double expectedMargin = 1.0;

    double actualMargin = calculator.calculateMargin(grossProfit, salePrice);
    assertEquals(expectedMargin, actualMargin, 0);
}

public void testCalculateMarginBoundary()
{
    double salePrice = 1000;
    double grossProfit = 1000;
    double expectedMargin = 0.0;

    double actualMargin = calculator.calculateMargin(grossProfit, salePrice);
    assertEquals(expectedMargin, actualMargin, 0);
}

public void testDiscountRate()
{
    List deals = new ArrayList();
    deals.add(new ValuationDeal(600, 0, 9400, 10000));
    deals.add(new ValuationDeal(700, 0, 10300, 11000));
    deals.add(new ValuationDeal(800, 0, 11200, 12000));
    deals.add(new ValuationDeal(900, 0, 12100, 13000));
    deals.add(new ValuationDeal(1000, 0, 13000, 14000));
    deals.add(new ValuationDeal(1100, 0, 13900, 15000));
    deals.add(new ValuationDeal(1200, 0, 14800, 16000));
    deals.add(new ValuationDeal(1300, 0, 15700, 17000));
    deals.add(new ValuationDeal(1400, 0, 16600, 18000));
    deals.add(new ValuationDeal(1500, 0, 17500, 19000));
    deals.add(new ValuationDeal(1600, 0, 18400, 20000));
    deals.add(new ValuationDeal(1700, 0, 19300, 21000));
    deals.add(new ValuationDeal(1800, 0, 20200, 22000));
    deals.add(new ValuationDeal(1900, 0, 21100, 23000));
    deals.add(new ValuationDeal(2000, 0, 22000, 24000));

    double expectedRate = .07825;
    double actualRate = calculator.calculate(deals, 35);

    assertEquals(expectedRate, actualRate, ACCEPTABLE_DIFFERENCE);
}

public void testDiscountRateNoDeals()
{
    List deals = new ArrayList();

    double expectedRate = .00;
    double actualRate = calculator.calculate(deals, 35);

    assertEquals(expectedRate, actualRate, ACCEPTABLE_DIFFERENCE);
}
}
