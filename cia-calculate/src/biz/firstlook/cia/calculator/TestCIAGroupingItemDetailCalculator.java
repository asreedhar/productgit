package biz.firstlook.cia.calculator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;

public class TestCIAGroupingItemDetailCalculator extends TestCase
{


public TestCIAGroupingItemDetailCalculator()
{
	super();
}

public void testFilterDetailValuesByTypeAndLevel()
{
	List<CIAGroupingItemDetail> ciaGroupingItemDetails = new ArrayList<CIAGroupingItemDetail>();

	CIAGroupingItemDetail detail1 = new CIAGroupingItemDetail();
	detail1.setDetailLevelValue( "Green" );
	detail1.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.PREFER );
	detail1.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.COLOR );
	ciaGroupingItemDetails.add( detail1 );

	CIAGroupingItemDetail detail2 = new CIAGroupingItemDetail();
	detail2.setDetailLevelValue( "Blue" );
	detail2.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.PREFER );
	detail2.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.COLOR );
	ciaGroupingItemDetails.add( detail2 );

	CIAGroupingItemDetail detail3 = new CIAGroupingItemDetail();
	detail3.setDetailLevelValue( "Black" );
	detail3.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.AVOID );
	detail3.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.COLOR );
	ciaGroupingItemDetails.add( detail3 );

	CIAGroupingItemDetail detail4 = new CIAGroupingItemDetail();
	detail4.setDetailLevelValue( "EX" );
	detail4.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.PREFER );
	detail4.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.TRIM );
	ciaGroupingItemDetails.add( detail4 );

	CIAGroupingItemDetail detail5 = new CIAGroupingItemDetail();
	detail5.setDetailLevelValue( "2005" );
	detail5.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.PREFER );
	detail5.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
	ciaGroupingItemDetails.add( detail5 );

	CIAGroupingItemDetail detail6 = new CIAGroupingItemDetail();
	detail6.setDetailLevelValue( "2001" );
	detail6.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.AVOID );
	detail6.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
	ciaGroupingItemDetails.add( detail6 );

	CIAGroupingItemDetail detail7 = new CIAGroupingItemDetail();
	detail7.setDetailLevelValue( "2002" );
	detail7.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.AVOID );
	detail7.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
	ciaGroupingItemDetails.add( detail7 );

	CIAGroupingItemDetail detail8 = new CIAGroupingItemDetail();
	detail8.setDetailLevelValue( "2003" );
	detail8.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.PREFER );
	detail8.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
	ciaGroupingItemDetails.add( detail8 );

	CIAGroupingItemDetail detail9 = new CIAGroupingItemDetail();
	detail9.setDetailLevelValue( "LX" );
	detail9.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.AVOID );
	detail9.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.TRIM );
	ciaGroupingItemDetails.add( detail9 );

	List preferColors = CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel( CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
															CIAGroupingItemDetailLevel.COLOR.getCiaGroupingItemDetailLevelId().intValue(),
															ciaGroupingItemDetails );
	assertEquals( "there should be 2 prefer colors", 2, preferColors.size() );
	String[] colors = (String[])preferColors.toArray( new String[preferColors.size()] );
	assertEquals( "first color should be Green", "Green", colors[0] );

	List avoidColors = CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel( CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
															CIAGroupingItemDetailLevel.COLOR.getCiaGroupingItemDetailLevelId().intValue(),
															ciaGroupingItemDetails );
	assertEquals( "there should be 1 avoid color", 1, avoidColors.size() );

	List preferTrims = CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel( CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
															CIAGroupingItemDetailLevel.TRIM.getCiaGroupingItemDetailLevelId().intValue(),
															ciaGroupingItemDetails );
	assertEquals( "there should be 1 prefer trim", 1, preferTrims.size() );

	List avoidTrims = CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel( CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
														CIAGroupingItemDetailLevel.TRIM.getCiaGroupingItemDetailLevelId().intValue(),
														ciaGroupingItemDetails );
	assertEquals( "there should be 1 avoid trim", 1, avoidTrims.size() );

	List preferYears = CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel( CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
															CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue(),
															ciaGroupingItemDetails );
	assertEquals( "there should be 2 prefer years", 2, preferYears.size() );

	List avoidYears = CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel( CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
														CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue(),
														ciaGroupingItemDetails );
	assertEquals( "there should be 2 avoid years", 2, avoidYears.size() );
}

public void testFilterDetailsByTypeAndLevel()
{
	List<CIAGroupingItemDetail> ciaGroupingItemDetails = new ArrayList<CIAGroupingItemDetail>();

	CIAGroupingItemDetail detail1 = new CIAGroupingItemDetail();
	detail1.setDetailLevelValue( "2002" );
	detail1.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.BUY );
	detail1.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
	detail1.setUnits( new Integer( 5 ) );
	ciaGroupingItemDetails.add( detail1 );

	CIAGroupingItemDetail detail2 = new CIAGroupingItemDetail();
	detail2.setDetailLevelValue( "2003" );
	detail2.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.BUY );
	detail2.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
	detail2.setUnits( new Integer( 2 ) );
	ciaGroupingItemDetails.add( detail2 );

	CIAGroupingItemDetail detail3 = new CIAGroupingItemDetail();
	detail3.setDetailLevelValue( "Black" );
	detail3.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.AVOID );
	detail3.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.COLOR );
	ciaGroupingItemDetails.add( detail3 );

	List buyYears = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel( CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId().intValue(),
													CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue(),
													ciaGroupingItemDetails );
	assertEquals( "there should be 2 buy amounts", 2, buyYears.size() );
	CIAGroupingItemDetail[] amounts = (CIAGroupingItemDetail[])buyYears.toArray( new CIAGroupingItemDetail[buyYears.size()] );
	assertEquals( "first amount should be 5", 5, amounts[0].getUnits().intValue() );

}

}
