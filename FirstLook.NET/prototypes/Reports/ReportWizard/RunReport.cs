using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using FirstLook.Common.Data;
using ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel;
using DataSet=ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel.DataSet;

namespace ReportWizard
{
    public delegate void RunReportEventHandler(object sender, RunReportEventArgs e);

    public partial class RunReport : Form
    {
        private const string ADOMD_PROVIDER = "Microsoft.AnalysisServices.AdomdClient.XmlaClientProvider";

        private const string SQL_PROVIDER = "System.Data.SqlClient";

        public event RunReportEventHandler Run;

        public event EventHandler Cancel;

        public RunReport()
        {
            InitializeComponent();
        }

        private Report m_report;

        private IDictionary<string, object> parameterValues;

        public Report Report
        {
            get { return m_report; }
            set { m_report = value; }
        }

        public IDictionary<string, object> ParameterValues
        {
            get { return parameterValues; }
            set { parameterValues = value; }
        }

        private void RunReport_Load(object sender, EventArgs e)
        {
            // server type combo box

            ServerTypeComboBox.ValueMember = "Value";

            ServerTypeComboBox.DisplayMember = "Label";

            ServerTypeComboBox.Items.AddRange(new ParameterValue[] {
                new ParameterValue("Database Engine", SQL_PROVIDER),
                new ParameterValue("Analysis Services", ADOMD_PROVIDER)
            });

            ServerTypeComboBox.SelectedIndex = 0;

            // progress bar

            RunReportProgressBar.Value = 0;
            RunReportProgressBar.Minimum = 0;
            RunReportProgressBar.Maximum = m_report.DataSets.Count + 1;
            RunReportProgressBar.Step = 1;

            // events

            RunButton.Click += RunButton_Click;

            CancelButton.Click += CancelButton_Click;

            // validation

            bool db = true;

            ServerNameTextBox.Text = (db ? "devdb01\\hal" : "HYPERION\\BETA");
            ServerNameTextBox.Validating += ServerName_Validating;
            ServerNameTextBox.Validated += delegate { ServerNameErrorProvider.SetError(ServerNameTextBox, ""); };

            LoginTextBox.Text = (db ? "firstlook" : "firstlook\\betarptsvcuser");
            LoginTextBox.Validating += Login_Validating;
            LoginTextBox.Validated += delegate { LoginErrorProvider.SetError(LoginTextBox, ""); };

            PasswordTextBox.Text = (db ? "1mS0rryD@ve" : "b3t@!r3p0rt5");
            PasswordTextBox.Validating += Password_Validating;
            PasswordTextBox.Validated += delegate { PasswordErrorProvider.SetError(PasswordTextBox, ""); };

            InitialCatalogTextBox.Text = (db ? "Reports" : "FirstLook OLAP DEV");
            InitialCatalogTextBox.Validating += InitialCatalog_Validating;
            InitialCatalogTextBox.Validated += delegate { InitialCatalogErrorProvider.SetError(InitialCatalogTextBox, ""); };

            ServerTypeComboBox.Validating += ServerType_Validating;
            ServerTypeComboBox.Validated += delegate { ServerTypeErrorProvider.SetError(ServerTypeComboBox, ""); };
        }

        private void ServerName_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(ServerNameTextBox.Text))
            {
                ServerNameErrorProvider.SetError(ServerNameTextBox, "Server Name cannot be null nor empty");
                e.Cancel = true;
            }
        }

        private void Login_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(LoginTextBox.Text))
            {
                LoginErrorProvider.SetError(LoginTextBox, "Login cannot be null nor empty");
                e.Cancel = true;
            }
        }

        private void Password_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(PasswordTextBox.Text))
            {
                PasswordErrorProvider.SetError(PasswordTextBox, "Password cannot be null nor empty");
                e.Cancel = true;
            }
        }

        private void InitialCatalog_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(InitialCatalogTextBox.Text))
            {
                InitialCatalogErrorProvider.SetError(InitialCatalogTextBox, "Password cannot be null nor empty");
                e.Cancel = true;
            }
        }

        private void ServerType_Validating(object sender, CancelEventArgs e)
        {
            if (ServerTypeComboBox.SelectedIndex == -1)
            {
                ServerTypeErrorProvider.SetError(ServerTypeComboBox, "Server Type must be selected");
                e.Cancel = true;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            if (Cancel != null)
                Cancel(sender, EventArgs.Empty);
        }

        private void RunButton_Click(object sender, EventArgs e)
        {
            if (m_report == null)
            {
                throw new NullReferenceException("Set the report on the RunReport form");
            }

            if (Run == null)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
                return;
            }

            RunReportProgressBar.Enabled = true;

            ParameterValue serverType = ServerTypeComboBox.SelectedItem as ParameterValue;
            
            if (serverType == null)
                throw new NullReferenceException("Server type was not selected");

            string serverName = ServerNameTextBox.Text;

            if (string.IsNullOrEmpty(serverName))
                throw new NullReferenceException("Server name was not completed");

            RunReportProgressBar.PerformStep();

            RunReportEventArgs args = new RunReportEventArgs("Report");

            CommandBehavior behaviour = (serverType.Value.Equals(ADOMD_PROVIDER))
                                            ? CommandBehavior.SingleResult
                                            : CommandBehavior.SchemaOnly;

            using (IDataConnection connection = DataProviderFactory.GetFactory(serverType.Value).CreateConnection(ConnectionString()))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                foreach (DataSet dataSet in m_report.DataSets)
                {
                    RunReportProgressBar.PerformStep();

                    using (IDataCommand command = connection.CreateCommand(dataSet, new DictionaryDataParameterValue(ParameterValues).DataParameterValue))
                    {
                        using (IDataReader reader = command.ExecuteReader(behaviour))
                        {
                            DataTable table = DataTableMapper.ToDataTable(reader);
                            table.TableName = dataSet.Name;
                            args.DataSet.Tables.Add(table);
                        }
                    }
                }
            }

            DialogResult = DialogResult.OK;

            Run(this, args);
        }

        private string ConnectionString()
        {
            return string.Format(
                    "Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}",
                    ServerNameTextBox.Text,
                    InitialCatalogTextBox.Text,
                    LoginTextBox.Text,
                    PasswordTextBox.Text);
        }
    }
}