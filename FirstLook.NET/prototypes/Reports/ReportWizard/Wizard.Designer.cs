using ReportParameter=ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel.ReportParameter;

namespace ReportWizard
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Wizard));
            this.ReportDefinitionOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.ReportDefinitionFileLabel = new System.Windows.Forms.Label();
            this.ReportDefinitionFile = new System.Windows.Forms.TextBox();
            this.MetaFile = new System.Windows.Forms.TextBox();
            this.MetaFileLabel = new System.Windows.Forms.Label();
            this.ReportToolStrip = new System.Windows.Forms.ToolStrip();
            this.ReportDefinitionDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.ReportDefinitionOpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportDefinitionSaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportDefinitionFixMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GenerateDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.GenerateTableMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GenerateRdlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MetaDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.MetaOpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MetaSaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitButton = new System.Windows.Forms.ToolStripButton();
            this.MetaOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.TableTabControl = new System.Windows.Forms.TabControl();
            this.ReportParametersTabPage = new System.Windows.Forms.TabPage();
            this.ReportParameterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GenerateTableSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.GenerateRdlSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.ReportParameterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UsedInQuery = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReportToolStrip.SuspendLayout();
            this.TableTabControl.SuspendLayout();
            this.ReportParametersTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReportParameterDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportParameterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ReportDefinitionOpenFileDialog
            // 
            this.ReportDefinitionOpenFileDialog.DefaultExt = "rdl";
            this.ReportDefinitionOpenFileDialog.Filter = "Report Files|*.rdl";
            this.ReportDefinitionOpenFileDialog.Title = "Open RDL";
            // 
            // ReportDefinitionFileLabel
            // 
            this.ReportDefinitionFileLabel.AutoSize = true;
            this.ReportDefinitionFileLabel.Location = new System.Drawing.Point(37, 35);
            this.ReportDefinitionFileLabel.Name = "ReportDefinitionFileLabel";
            this.ReportDefinitionFileLabel.Size = new System.Drawing.Size(29, 13);
            this.ReportDefinitionFileLabel.TabIndex = 8;
            this.ReportDefinitionFileLabel.Text = "RDL";
            // 
            // ReportDefinitionFile
            // 
            this.ReportDefinitionFile.Location = new System.Drawing.Point(74, 32);
            this.ReportDefinitionFile.Name = "ReportDefinitionFile";
            this.ReportDefinitionFile.ReadOnly = true;
            this.ReportDefinitionFile.Size = new System.Drawing.Size(583, 20);
            this.ReportDefinitionFile.TabIndex = 4;
            // 
            // MetaFile
            // 
            this.MetaFile.Location = new System.Drawing.Point(74, 60);
            this.MetaFile.Name = "MetaFile";
            this.MetaFile.ReadOnly = true;
            this.MetaFile.Size = new System.Drawing.Size(583, 20);
            this.MetaFile.TabIndex = 1;
            // 
            // MetaFileLabel
            // 
            this.MetaFileLabel.AutoSize = true;
            this.MetaFileLabel.Location = new System.Drawing.Point(35, 63);
            this.MetaFileLabel.Name = "MetaFileLabel";
            this.MetaFileLabel.Size = new System.Drawing.Size(31, 13);
            this.MetaFileLabel.TabIndex = 0;
            this.MetaFileLabel.Text = "Meta";
            // 
            // ReportToolStrip
            // 
            this.ReportToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReportDefinitionDropDownButton,
            this.GenerateDropDownButton,
            this.MetaDropDownButton,
            this.ExitButton});
            this.ReportToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ReportToolStrip.Name = "ReportToolStrip";
            this.ReportToolStrip.Size = new System.Drawing.Size(669, 25);
            this.ReportToolStrip.TabIndex = 10;
            this.ReportToolStrip.Text = "toolStrip1";
            // 
            // ReportDefinitionDropDownButton
            // 
            this.ReportDefinitionDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ReportDefinitionDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReportDefinitionOpenMenuItem,
            this.ReportDefinitionSaveMenuItem,
            this.ReportDefinitionFixMenuItem});
            this.ReportDefinitionDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("ReportDefinitionDropDownButton.Image")));
            this.ReportDefinitionDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ReportDefinitionDropDownButton.Name = "ReportDefinitionDropDownButton";
            this.ReportDefinitionDropDownButton.Size = new System.Drawing.Size(39, 22);
            this.ReportDefinitionDropDownButton.Text = "RDL";
            // 
            // ReportDefinitionOpenMenuItem
            // 
            this.ReportDefinitionOpenMenuItem.Image = global::ReportWizard.Properties.Resources.document_open_16;
            this.ReportDefinitionOpenMenuItem.Name = "ReportDefinitionOpenMenuItem";
            this.ReportDefinitionOpenMenuItem.Size = new System.Drawing.Size(111, 22);
            this.ReportDefinitionOpenMenuItem.Text = "Open";
            // 
            // ReportDefinitionSaveMenuItem
            // 
            this.ReportDefinitionSaveMenuItem.Enabled = false;
            this.ReportDefinitionSaveMenuItem.Image = global::ReportWizard.Properties.Resources.save_16;
            this.ReportDefinitionSaveMenuItem.Name = "ReportDefinitionSaveMenuItem";
            this.ReportDefinitionSaveMenuItem.Size = new System.Drawing.Size(111, 22);
            this.ReportDefinitionSaveMenuItem.Text = "Save";
            // 
            // ReportDefinitionFixMenuItem
            // 
            this.ReportDefinitionFixMenuItem.Enabled = false;
            this.ReportDefinitionFixMenuItem.Name = "ReportDefinitionFixMenuItem";
            this.ReportDefinitionFixMenuItem.Size = new System.Drawing.Size(111, 22);
            this.ReportDefinitionFixMenuItem.Text = "Fix";
            // 
            // GenerateDropDownButton
            // 
            this.GenerateDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.GenerateDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GenerateTableMenuItem,
            this.GenerateRdlMenuItem});
            this.GenerateDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("GenerateDropDownButton.Image")));
            this.GenerateDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GenerateDropDownButton.Name = "GenerateDropDownButton";
            this.GenerateDropDownButton.Size = new System.Drawing.Size(65, 22);
            this.GenerateDropDownButton.Text = "Generate";
            // 
            // GenerateTableMenuItem
            // 
            this.GenerateTableMenuItem.Name = "GenerateTableMenuItem";
            this.GenerateTableMenuItem.Size = new System.Drawing.Size(144, 22);
            this.GenerateTableMenuItem.Text = "Cache Table";
            // 
            // GenerateRdlMenuItem
            // 
            this.GenerateRdlMenuItem.Name = "GenerateRdlMenuItem";
            this.GenerateRdlMenuItem.Size = new System.Drawing.Size(144, 22);
            this.GenerateRdlMenuItem.Text = "Cache RDL";
            // 
            // MetaDropDownButton
            // 
            this.MetaDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MetaDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MetaOpenMenuItem,
            this.MetaSaveMenuItem});
            this.MetaDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("MetaDropDownButton.Image")));
            this.MetaDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MetaDropDownButton.Name = "MetaDropDownButton";
            this.MetaDropDownButton.Size = new System.Drawing.Size(44, 22);
            this.MetaDropDownButton.Text = "Meta";
            // 
            // MetaOpenMenuItem
            // 
            this.MetaOpenMenuItem.Image = global::ReportWizard.Properties.Resources.document_open_16;
            this.MetaOpenMenuItem.Name = "MetaOpenMenuItem";
            this.MetaOpenMenuItem.Size = new System.Drawing.Size(111, 22);
            this.MetaOpenMenuItem.Text = "Open";
            // 
            // MetaSaveMenuItem
            // 
            this.MetaSaveMenuItem.Image = global::ReportWizard.Properties.Resources.save_16;
            this.MetaSaveMenuItem.Name = "MetaSaveMenuItem";
            this.MetaSaveMenuItem.Size = new System.Drawing.Size(111, 22);
            this.MetaSaveMenuItem.Text = "Save";
            // 
            // ExitButton
            // 
            this.ExitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ExitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ExitButton.Image = global::ReportWizard.Properties.Resources.stop_16;
            this.ExitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(29, 22);
            this.ExitButton.Text = "Exit";
            // 
            // MetaOpenFileDialog
            // 
            this.MetaOpenFileDialog.Filter = "Meta files|*.xml";
            // 
            // TableTabControl
            // 
            this.TableTabControl.Controls.Add(this.ReportParametersTabPage);
            this.TableTabControl.Location = new System.Drawing.Point(12, 86);
            this.TableTabControl.Name = "TableTabControl";
            this.TableTabControl.SelectedIndex = 0;
            this.TableTabControl.Size = new System.Drawing.Size(645, 367);
            this.TableTabControl.TabIndex = 11;
            // 
            // ReportParametersTabPage
            // 
            this.ReportParametersTabPage.Controls.Add(this.ReportParameterDataGridView);
            this.ReportParametersTabPage.Location = new System.Drawing.Point(4, 22);
            this.ReportParametersTabPage.Name = "ReportParametersTabPage";
            this.ReportParametersTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ReportParametersTabPage.Size = new System.Drawing.Size(637, 341);
            this.ReportParametersTabPage.TabIndex = 0;
            this.ReportParametersTabPage.Text = "Report Parameters";
            this.ReportParametersTabPage.UseVisualStyleBackColor = true;
            // 
            // ReportParameterDataGridView
            // 
            this.ReportParameterDataGridView.AllowUserToAddRows = false;
            this.ReportParameterDataGridView.AllowUserToDeleteRows = false;
            this.ReportParameterDataGridView.AutoGenerateColumns = false;
            this.ReportParameterDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ReportParameterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReportParameterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.dataTypeDataGridViewTextBoxColumn,
            this.UsedInQuery,
            this.Value,
            this.columnNameDataGridViewTextBoxColumn,
            this.ColumnType});
            this.ReportParameterDataGridView.DataSource = this.ReportParameterBindingSource;
            this.ReportParameterDataGridView.Location = new System.Drawing.Point(6, 6);
            this.ReportParameterDataGridView.Name = "ReportParameterDataGridView";
            this.ReportParameterDataGridView.Size = new System.Drawing.Size(625, 329);
            this.ReportParameterDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "UsedInQuery";
            this.dataGridViewTextBoxColumn1.HeaderText = "UsedInQuery";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.DataPropertyName = "UsedInQuery";
            this.dataGridViewComboBoxColumn1.HeaderText = "UsedInQuery";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.DataPropertyName = "UsedInQuery";
            this.dataGridViewComboBoxColumn2.HeaderText = "UsedInQuery";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "true",
            "false"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.DataPropertyName = "UsedInQuery";
            this.dataGridViewComboBoxColumn3.HeaderText = "UsedInQuery";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.DataPropertyName = "UsedInQuery";
            this.dataGridViewComboBoxColumn4.HeaderText = "UsedInQuery";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "UsedInQuery";
            this.dataGridViewCheckBoxColumn1.HeaderText = "UsedInQuery";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // GenerateTableSaveFileDialog
            // 
            this.GenerateTableSaveFileDialog.DefaultExt = "sql";
            this.GenerateTableSaveFileDialog.Filter = "SQL Files|*.sql";
            this.GenerateTableSaveFileDialog.Title = "Save Cache Table SQL";
            // 
            // GenerateRdlSaveFileDialog
            // 
            this.GenerateRdlSaveFileDialog.DefaultExt = "rdl";
            this.GenerateRdlSaveFileDialog.Filter = "Report Files|*.rdl";
            this.GenerateRdlSaveFileDialog.Title = "Save Cache Report Definition";
            // 
            // ReportParameterBindingSource
            // 
            this.ReportParameterBindingSource.DataSource = typeof(ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel.ReportParameter);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dataTypeDataGridViewTextBoxColumn
            // 
            this.dataTypeDataGridViewTextBoxColumn.DataPropertyName = "DataType";
            this.dataTypeDataGridViewTextBoxColumn.HeaderText = "DataType";
            this.dataTypeDataGridViewTextBoxColumn.Name = "dataTypeDataGridViewTextBoxColumn";
            this.dataTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // UsedInQuery
            // 
            this.UsedInQuery.DataPropertyName = "UsedInQuery";
            this.UsedInQuery.HeaderText = "UsedInQuery";
            this.UsedInQuery.Name = "UsedInQuery";
            this.UsedInQuery.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UsedInQuery.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Value
            // 
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // columnNameDataGridViewTextBoxColumn
            // 
            this.columnNameDataGridViewTextBoxColumn.DataPropertyName = "ColumnName";
            this.columnNameDataGridViewTextBoxColumn.HeaderText = "ColumnName";
            this.columnNameDataGridViewTextBoxColumn.Name = "columnNameDataGridViewTextBoxColumn";
            // 
            // ColumnType
            // 
            this.ColumnType.DataPropertyName = "ColumnType";
            this.ColumnType.HeaderText = "ColumnType";
            this.ColumnType.Name = "ColumnType";
            this.ColumnType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 465);
            this.Controls.Add(this.TableTabControl);
            this.Controls.Add(this.ReportDefinitionFileLabel);
            this.Controls.Add(this.MetaFile);
            this.Controls.Add(this.ReportDefinitionFile);
            this.Controls.Add(this.ReportToolStrip);
            this.Controls.Add(this.MetaFileLabel);
            this.Name = "Wizard";
            this.Text = "Report Wizard";
            this.Load += new System.EventHandler(this.Wizard_Load);
            this.ReportToolStrip.ResumeLayout(false);
            this.ReportToolStrip.PerformLayout();
            this.TableTabControl.ResumeLayout(false);
            this.ReportParametersTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReportParameterDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportParameterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog ReportDefinitionOpenFileDialog;
        private System.Windows.Forms.TextBox ReportDefinitionFile;
        private System.Windows.Forms.Label ReportDefinitionFileLabel;
        private System.Windows.Forms.TextBox MetaFile;
        private System.Windows.Forms.Label MetaFileLabel;
        private System.Windows.Forms.ToolStrip ReportToolStrip;
        private System.Windows.Forms.ToolStripDropDownButton MetaDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem MetaOpenMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MetaSaveMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ReportDefinitionDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem ReportDefinitionOpenMenuItem;
        private System.Windows.Forms.OpenFileDialog MetaOpenFileDialog;
        private System.Windows.Forms.TabControl TableTabControl;
        private System.Windows.Forms.TabPage ReportParametersTabPage;
        private System.Windows.Forms.ToolStripDropDownButton GenerateDropDownButton;
        private System.Windows.Forms.ToolStripButton ExitButton;
        private System.Windows.Forms.DataGridView ReportParameterDataGridView;
        private System.Windows.Forms.BindingSource ReportParameterBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.ToolStripMenuItem ReportDefinitionSaveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReportDefinitionFixMenuItem;
        private System.Windows.Forms.ToolStripMenuItem GenerateTableMenuItem;
        private System.Windows.Forms.ToolStripMenuItem GenerateRdlMenuItem;
        private System.Windows.Forms.SaveFileDialog GenerateTableSaveFileDialog;
        private System.Windows.Forms.SaveFileDialog GenerateRdlSaveFileDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UsedInQuery;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnType;

    }
}

