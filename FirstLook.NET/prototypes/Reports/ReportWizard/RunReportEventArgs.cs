using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ReportWizard
{
    public class RunReportEventArgs : EventArgs
    {
        private readonly DataSet m_dataSet;

        public RunReportEventArgs(string dataSetName) : base()
        {
            m_dataSet = new DataSet(dataSetName);
        }

        public DataSet DataSet
        {
            get { return m_dataSet; }
        }
    }
}
