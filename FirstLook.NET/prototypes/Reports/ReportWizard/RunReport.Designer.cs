namespace ReportWizard
{
    partial class RunReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ServerTypeLabel = new System.Windows.Forms.Label();
            this.ServerNameLabel = new System.Windows.Forms.Label();
            this.ServerNameTextBox = new System.Windows.Forms.TextBox();
            this.LoginLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginTextBox = new System.Windows.Forms.TextBox();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.RunButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.InitialCatalogLabel = new System.Windows.Forms.Label();
            this.InitialCatalogTextBox = new System.Windows.Forms.TextBox();
            this.RunReportProgressBar = new System.Windows.Forms.ProgressBar();
            this.ServerTypeErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.ServerNameErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.InitialCatalogErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.LoginErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.PasswordErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.ServerTypeComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ServerTypeErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServerNameErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InitialCatalogErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // ServerTypeLabel
            // 
            this.ServerTypeLabel.AutoSize = true;
            this.ServerTypeLabel.Location = new System.Drawing.Point(30, 23);
            this.ServerTypeLabel.Name = "ServerTypeLabel";
            this.ServerTypeLabel.Size = new System.Drawing.Size(65, 13);
            this.ServerTypeLabel.TabIndex = 0;
            this.ServerTypeLabel.Text = "Server Type";
            // 
            // ServerNameLabel
            // 
            this.ServerNameLabel.AutoSize = true;
            this.ServerNameLabel.Location = new System.Drawing.Point(26, 51);
            this.ServerNameLabel.Name = "ServerNameLabel";
            this.ServerNameLabel.Size = new System.Drawing.Size(69, 13);
            this.ServerNameLabel.TabIndex = 1;
            this.ServerNameLabel.Text = "Server Name";
            // 
            // ServerNameTextBox
            // 
            this.ServerNameTextBox.Location = new System.Drawing.Point(101, 51);
            this.ServerNameTextBox.Name = "ServerNameTextBox";
            this.ServerNameTextBox.Size = new System.Drawing.Size(233, 20);
            this.ServerNameTextBox.TabIndex = 2;
            // 
            // LoginLabel
            // 
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.Location = new System.Drawing.Point(58, 114);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(33, 13);
            this.LoginLabel.TabIndex = 4;
            this.LoginLabel.Text = "Login";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Password";
            // 
            // LoginTextBox
            // 
            this.LoginTextBox.Location = new System.Drawing.Point(100, 114);
            this.LoginTextBox.Name = "LoginTextBox";
            this.LoginTextBox.Size = new System.Drawing.Size(233, 20);
            this.LoginTextBox.TabIndex = 4;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(100, 143);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(233, 20);
            this.PasswordTextBox.TabIndex = 5;
            this.PasswordTextBox.UseSystemPasswordChar = true;
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(100, 180);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(75, 23);
            this.RunButton.TabIndex = 6;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.CausesValidation = false;
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(181, 180);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // InitialCatalogLabel
            // 
            this.InitialCatalogLabel.AutoSize = true;
            this.InitialCatalogLabel.Location = new System.Drawing.Point(25, 81);
            this.InitialCatalogLabel.Name = "InitialCatalogLabel";
            this.InitialCatalogLabel.Size = new System.Drawing.Size(70, 13);
            this.InitialCatalogLabel.TabIndex = 10;
            this.InitialCatalogLabel.Text = "Initial Catalog";
            // 
            // InitialCatalogTextBox
            // 
            this.InitialCatalogTextBox.Location = new System.Drawing.Point(102, 81);
            this.InitialCatalogTextBox.Name = "InitialCatalogTextBox";
            this.InitialCatalogTextBox.Size = new System.Drawing.Size(231, 20);
            this.InitialCatalogTextBox.TabIndex = 3;
            // 
            // RunReportProgressBar
            // 
            this.RunReportProgressBar.Enabled = false;
            this.RunReportProgressBar.Location = new System.Drawing.Point(100, 220);
            this.RunReportProgressBar.Name = "RunReportProgressBar";
            this.RunReportProgressBar.Size = new System.Drawing.Size(233, 23);
            this.RunReportProgressBar.TabIndex = 13;
            // 
            // ServerTypeErrorProvider
            // 
            this.ServerTypeErrorProvider.ContainerControl = this;
            // 
            // ServerNameErrorProvider
            // 
            this.ServerNameErrorProvider.ContainerControl = this;
            // 
            // InitialCatalogErrorProvider
            // 
            this.InitialCatalogErrorProvider.ContainerControl = this;
            // 
            // LoginErrorProvider
            // 
            this.LoginErrorProvider.ContainerControl = this;
            // 
            // PasswordErrorProvider
            // 
            this.PasswordErrorProvider.ContainerControl = this;
            // 
            // ServerTypeComboBox
            // 
            this.ServerTypeComboBox.FormattingEnabled = true;
            this.ServerTypeComboBox.Location = new System.Drawing.Point(102, 23);
            this.ServerTypeComboBox.Name = "ServerTypeComboBox";
            this.ServerTypeComboBox.Size = new System.Drawing.Size(232, 21);
            this.ServerTypeComboBox.TabIndex = 14;
            // 
            // RunReport
            // 
            this.AcceptButton = this.RunButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 255);
            this.ControlBox = false;
            this.Controls.Add(this.ServerTypeComboBox);
            this.Controls.Add(this.RunReportProgressBar);
            this.Controls.Add(this.InitialCatalogTextBox);
            this.Controls.Add(this.InitialCatalogLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.PasswordTextBox);
            this.Controls.Add(this.LoginTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LoginLabel);
            this.Controls.Add(this.ServerNameTextBox);
            this.Controls.Add(this.ServerNameLabel);
            this.Controls.Add(this.ServerTypeLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RunReport";
            this.ShowInTaskbar = false;
            this.Text = "Run Report";
            this.Load += new System.EventHandler(this.RunReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ServerTypeErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServerNameErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InitialCatalogErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ServerTypeLabel;
        private System.Windows.Forms.Label ServerNameLabel;
        private System.Windows.Forms.TextBox ServerNameTextBox;
        private System.Windows.Forms.Label LoginLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LoginTextBox;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label InitialCatalogLabel;
        private System.Windows.Forms.TextBox InitialCatalogTextBox;
        private System.Windows.Forms.ProgressBar RunReportProgressBar;
        private System.Windows.Forms.ErrorProvider ServerTypeErrorProvider;
        private System.Windows.Forms.ErrorProvider ServerNameErrorProvider;
        private System.Windows.Forms.ErrorProvider InitialCatalogErrorProvider;
        private System.Windows.Forms.ErrorProvider LoginErrorProvider;
        private System.Windows.Forms.ErrorProvider PasswordErrorProvider;
        private System.Windows.Forms.ComboBox ServerTypeComboBox;
    }
}