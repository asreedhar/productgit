using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using FirstLook.Common.Data;
using ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel;
using DataSet=ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel.DataSet;

namespace ReportWizard
{
    public partial class Wizard : Form
    {
        private RunReport m_runReport;
        private Report m_report;

        public Wizard()
        {
            InitializeComponent();
        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            // menu actions

            ReportDefinitionOpenMenuItem.Click += ReportDefinitionOpenMenuItem_Click;

            ReportDefinitionSaveMenuItem.Click += ReportDefinitionSaveMenuItem_Click;

            ReportDefinitionFixMenuItem.Click += ReportDefinitionFixMenuItem_Click;

            GenerateTableMenuItem.Click += GenerateTableMenuItem_Click;

            GenerateRdlMenuItem.Click += GenerateRdlMenuItem_Click;

            MetaOpenMenuItem.Click += MetaOpenFileDialog_Click;

            ExitButton.Click += ExitButton_Click;
        }

        private void GenerateTableMenuItem_Click(object sender, EventArgs e)
        {
            GenerateTableSaveFileDialog.FileName = string.Format("cache.{0}.sql", m_report.Name());

            if (GenerateTableSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (Stream s = GenerateTableSaveFileDialog.OpenFile())
                {
                    using (StreamWriter w = new StreamWriter(s))
                    {
                        for (int i = 0; i < m_report.DataSets.Count; i++)
                        {
                            TextBox textBox = TableTabControl.TabPages[i + 1].Controls[1] as TextBox;

                            if (textBox != null)
                            {
                                w.Write(m_report.DataSets[i].CacheTableDefinition("cache", textBox.Text));
                            }
                        }
                    }
                }
            }
        }

        private void GenerateRdlMenuItem_Click(object sender, EventArgs e)
        {
            GenerateRdlSaveFileDialog.FileName = string.Format("{0} [Cache].rdl", m_report.Name());

            if (GenerateRdlSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Report c_report = m_report.Clone(GenerateRdlSaveFileDialog.FileName);

                for (int i = 0; i < c_report.DataSets.Count; i++)
                {
                    TextBox textBox = TableTabControl.TabPages[i + 1].Controls[1] as TextBox;

                    if (textBox != null)
                    {
                        DataSet dataSet = c_report.DataSets[i];
                        dataSet.Query.CommandType = CommandType.Text;
                        dataSet.Query.CommandText = dataSet.CacheQueryCommandText("cache", textBox.Text);
                        dataSet.Query.DataSourceName = "REPORTHOST";
                    }
                }

                c_report.Save();
            }
        }

        private void ReportDefinitionFixMenuItem_Click(object sender, EventArgs e)
        {
            m_runReport = new RunReport();

            m_runReport.Report = m_report;

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();

            foreach (DataGridViewRow row in ReportParameterDataGridView.Rows)
            {
                ReportParameter reportParameter = row.DataBoundItem as ReportParameter;
                
                if (reportParameter != null)
                    parameterValues[reportParameter.Name] = row.Cells["Value"].Value;
            }

            m_runReport.ParameterValues = parameterValues;

            m_runReport.Run += RunReport_Run;

            m_runReport.Cancel += RunReport_Cancel;

            m_runReport.ShowDialog();
        }

        private void RunReport_Run(object sender, RunReportEventArgs e)
        {
            m_runReport.Close();

            for (int i = 0; i < m_report.DataSets.Count; i++)
            {
                DataGridView dataSetView = TableTabControl.TabPages[i + 1].Controls[2] as DataGridView;

                if (dataSetView != null)
                {
                    DataTable table = e.DataSet.Tables[m_report.DataSets[i].Name];

                    foreach (DataColumn column in table.Columns)
                    {
                        foreach (DataGridViewRow row in dataSetView.Rows)
                        {
                            if (string.Equals(column.ColumnName, (string) row.Cells["fieldDataFieldDataGridViewTextBoxColumn"].Value, StringComparison.CurrentCultureIgnoreCase))
                            {
                                DataColumnInfo columnInfo = (DataColumnInfo) column.ExtendedProperties["DataColumnInfo"];

                                row.Cells["fieldDataFieldDataGridViewTextBoxColumn"].Value = column.ColumnName;

                                // do not override data- or column-type when analysis services

                                if (!column.DataType.FullName.Equals("System.Object"))
                                {
                                    row.Cells["fieldTypeNameDataGridViewTextBoxColumn"].Value = column.DataType.FullName;

                                    row.Cells["fieldCacheTypeDataGridViewTextBoxColumn"].Value = columnInfo.ColumnDeclaration();
                                }

                                // detect types for analysis-services

                                if (table.Rows.Count > 0)
                                {
                                    List<Type> types = new List<Type>();

                                    List<TypeCode> codes = new List<TypeCode>();

                                    foreach (DataRow r in table.Rows)
                                    {
                                        if (r[column] != null)
                                        {
                                            Type type = r[column].GetType();

                                            TypeCode code = Type.GetTypeCode(type);

                                            if (code.Equals(TypeCode.Object) || code.Equals(TypeCode.DBNull) || code.Equals(TypeCode.Empty))
                                                continue;
                                            
                                            else if (!codes.Contains(code)) {
                                                types.Add(type);
                                                codes.Add(code);
                                            }
                                        }
                                    }

                                    if (codes.Count > 0)
                                    {
                                        Type type = types[0];

                                        TypeCode code = codes[0];

                                        if (codes.Count > 1)
                                        {
                                            Debug.WriteLine(string.Format("Have {0} types!", codes.Count));
                                        }

                                        row.Cells["fieldTypeNameDataGridViewTextBoxColumn"].Value = type.FullName;

                                        row.Cells["fieldCacheTypeDataGridViewTextBoxColumn"].Value = DataColumnInfo.FromTypeCode(code).ColumnDeclaration();
                                    }
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        private void RunReport_Cancel(object sender, EventArgs e)
        {
            m_runReport.Close();
        }

        private void ReportDefinitionOpenMenuItem_Click(object sender, EventArgs e)
        {
            if (ReportDefinitionOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                ReportDefinitionOpenFileDialog_FileOk();
            }
        }

        private void ReportDefinitionOpenFileDialog_FileOk()
        {
            ReportDefinitionFile.Text = ReportDefinitionOpenFileDialog.FileName;

            m_report = new Resources.ReportDefinitionLanguage.ReportDefinitionLanguageDataSource().Load(
                    ReportDefinitionFile.Text);

            ReportParameterDataGridView.CellValidating += ReportParameterDataGridView_CellValidating;
            ReportParameterDataGridView.DataBindingComplete += ReportParameterDataGridView_DataBindingComplete;

            ReportParameterDataGridView.DataSource = m_report.ReportParameters;

            int count = TableTabControl.TabPages.Count;

            for (int i = count-1; i > 0; i--)
            {
                TableTabControl.TabPages.RemoveAt(i);
            }

            foreach (DataSet dataSet in m_report.DataSets)
            {
                Label tableNameLabel = new Label();
                tableNameLabel.AutoSize = true;
                tableNameLabel.Location = new System.Drawing.Point(6, 6);
                tableNameLabel.Size = new System.Drawing.Size(50, 13);
                tableNameLabel.Text = "Table Name";
                tableNameLabel.TabIndex = 1;

                TextBox tableNameTextBox = new TextBox();
                tableNameTextBox.AutoSize = true;
                tableNameTextBox.Location = new System.Drawing.Point(80, 6);
                tableNameTextBox.Size = new System.Drawing.Size(540, 20);
                tableNameTextBox.Text = dataSet.Name;
                tableNameLabel.TabIndex = 2;

                BindingSource dataSetBindingSource = new BindingSource(components);

                dataSetBindingSource.DataSource = typeof(Field);

                DataGridViewTextBoxColumn fieldNameDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                fieldNameDataGridViewTextBoxColumn.DataPropertyName = "Name";
                fieldNameDataGridViewTextBoxColumn.HeaderText = "Name";
                fieldNameDataGridViewTextBoxColumn.Name = "fieldNameDataGridViewTextBoxColumn";
                fieldNameDataGridViewTextBoxColumn.ReadOnly = true;

                DataGridViewComboBoxColumn fieldNameDataGridViewComboBoxColumn = new DataGridViewComboBoxColumn();
                fieldNameDataGridViewComboBoxColumn.DataPropertyName = "TypeName";
                fieldNameDataGridViewComboBoxColumn.HeaderText = "TypeName";
                fieldNameDataGridViewComboBoxColumn.Name = "fieldTypeNameDataGridViewTextBoxColumn";
                fieldNameDataGridViewComboBoxColumn.ReadOnly = false;

                fieldNameDataGridViewComboBoxColumn.DisplayMember = "Label";
                fieldNameDataGridViewComboBoxColumn.ValueMember = "Value";
                foreach (string type in Enum.GetNames(typeof(TypeCode)))
                    fieldNameDataGridViewComboBoxColumn.Items.Add(new ParameterValue(type, string.Format("System.{0}", type)));

                DataGridViewTextBoxColumn fieldDataFieldDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                fieldDataFieldDataGridViewTextBoxColumn.HeaderText = "DataField";
                fieldDataFieldDataGridViewTextBoxColumn.Name = "fieldDataFieldDataGridViewTextBoxColumn";
                fieldDataFieldDataGridViewTextBoxColumn.ReadOnly = true;

                DataGridViewTextBoxColumn fieldCacheFieldDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                fieldCacheFieldDataGridViewTextBoxColumn.DataPropertyName = "CacheField";
                fieldCacheFieldDataGridViewTextBoxColumn.HeaderText = "CacheField";
                fieldCacheFieldDataGridViewTextBoxColumn.Name = "fieldCacheFieldDataGridViewTextBoxColumn";
                fieldCacheFieldDataGridViewTextBoxColumn.ReadOnly = false;

                DataGridViewTextBoxColumn fieldCacheTypeDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
                fieldCacheTypeDataGridViewTextBoxColumn.DataPropertyName = "CacheType";
                fieldCacheTypeDataGridViewTextBoxColumn.HeaderText = "CacheType";
                fieldCacheTypeDataGridViewTextBoxColumn.Name = "fieldCacheTypeDataGridViewTextBoxColumn";
                fieldCacheTypeDataGridViewTextBoxColumn.ReadOnly = false;

                DataGridView dataSetView = new DataGridView();

                dataSetView.DataBindingComplete += DataSetDataGridView_DataBindingComplete;

                dataSetView.AllowUserToAddRows = false;
                dataSetView.AllowUserToDeleteRows = false;
                dataSetView.AutoGenerateColumns = false;
                dataSetView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dataSetView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                dataSetView.Columns.AddRange(new DataGridViewColumn[] {
                    fieldNameDataGridViewTextBoxColumn,
                    fieldNameDataGridViewComboBoxColumn,
                    fieldDataFieldDataGridViewTextBoxColumn,
                    fieldCacheFieldDataGridViewTextBoxColumn,
                    fieldCacheTypeDataGridViewTextBoxColumn});
                dataSetView.DataSource = dataSet.Fields;
                dataSetView.Location = new System.Drawing.Point(6, 40);
                dataSetView.Name = string.Format("{0}DataGridView", dataSet.Name);
                dataSetView.Size = new System.Drawing.Size(625, 318);
                dataSetView.TabIndex = 3;
                dataSetView.DataError += DataSetView_DataError;

                TabPage page = new TabPage();

                page.Controls.Add(tableNameLabel);
                page.Controls.Add(tableNameTextBox);
                page.Controls.Add(dataSetView);
                page.Location = new System.Drawing.Point(4, 22);
                page.Name = string.Format("{0}TabPage", dataSet.Name);
                page.Padding = new Padding(3);
                page.Size = new System.Drawing.Size(637, 330);
                page.TabIndex = 0;
                page.Text = dataSet.Name;
                page.UseVisualStyleBackColor = true;

                TableTabControl.TabPages.Add(page);
            }

            ReportDefinitionSaveMenuItem.Enabled = true;
            ReportDefinitionFixMenuItem.Enabled = true;
        }

        private void DataSetDataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grid = sender as DataGridView;

            if (grid != null)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    Field field = row.DataBoundItem as Field;

                    if (field != null)
                    {
                        row.Cells["fieldDataFieldDataGridViewTextBoxColumn"].Value = field.DataField.Name;
                    }
                }
            }
        }

        private void ReportParameterDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DataGridViewTextBoxCell cell = sender as DataGridViewTextBoxCell;

            if (cell != null && cell.OwningColumn.Name.Equals("Value"))
            {
                string value = cell.Value as string;

                e.Cancel = string.IsNullOrEmpty(value);
            }
        }

        private void ReportParameterDataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grid = sender as DataGridView;

            if (grid != null)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    ReportParameter reportParameter = row.DataBoundItem as ReportParameter;

                    if (reportParameter != null)
                    {
                        if (reportParameter.DefaultValue.Count > 0)
                        {
                            string defaultValue = reportParameter.DefaultValue[0];

                            if (defaultValue.StartsWith("="))
                            {
                                defaultValue = defaultValue.Substring(1);
                            }

                            row.Cells["Value"].Value = defaultValue;
                        }
                    }
                }
            }
        }

        private void DataSetView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            Debug.WriteLine(e.Exception.ToString());
        }

        private void ReportDefinitionSaveMenuItem_Click(object sender, EventArgs e)
        {
            if (m_report != null)
                m_report.Save();
        }

        private void MetaOpenFileDialog_Click(object sender, EventArgs e)
        {
            if (MetaOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                MetaFile.Text = MetaOpenFileDialog.FileName;
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            m_report = null;
            Close();
        }
    }
}