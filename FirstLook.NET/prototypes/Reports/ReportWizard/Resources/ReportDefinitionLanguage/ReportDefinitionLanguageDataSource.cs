using System;
using System.Xml;
using ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel;

namespace ReportWizard.Resources.ReportDefinitionLanguage
{
    public class ReportDefinitionLanguageDataSource
    {
        public ObjectModel.Report Load(string filename)
        {
            XmlDocument document = new XmlDocument();

            document.Load(filename);

            XmlElement root = document.DocumentElement;

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(document.NameTable);

            nsmgr.AddNamespace("r", document.DocumentElement.NamespaceURI);

            nsmgr.AddNamespace("rd", document.DocumentElement.Attributes["xmlns:rd"].Value);

            nsmgr.AddNamespace("cx", @"http://schemas.firstlook.biz/SQLServer/reporting/caching-extensions");

            ObjectModel.Report report = new ObjectModel.Report(filename, document);

            foreach (XmlNode reportParameter in root.SelectNodes("/r:Report/r:ReportParameters/r:ReportParameter", nsmgr))
            {
                ReportParameter parameter = new ReportParameter();

                parameter.Name = reportParameter.Attributes["Name"].Value;

                foreach (XmlNode child in reportParameter.ChildNodes)
                {
                    if (child.NodeType.Equals(XmlNodeType.Element))
                    {
                        if (child.Name.Equals("AllowBlank"))
                        {
                            parameter.AllowBlank = Convert.ToBoolean(child.InnerText);
                        }
                        else if (child.Name.Equals("DataType"))
                        {
                            parameter.DataType = child.InnerText;
                        }
                        else if (child.Name.Equals("Hidden"))
                        {
                            parameter.Hidden = Convert.ToBoolean(child.InnerText);
                        }
                        else if (child.Name.Equals("MultiValue"))
                        {
                            parameter.MultiValue = Convert.ToBoolean(child.InnerText);
                        }
                        else if (child.Name.Equals("Nullable"))
                        {
                            parameter.IsNullable = Convert.ToBoolean(child.InnerText);
                        }
                        else if (child.Name.Equals("Prompt"))
                        {
                            parameter.Prompt = child.InnerText;
                        }
                        else if (child.Name.Equals("UsedInQuery"))
                        {
                            if (child.InnerText.Equals("True"))
                            {
                                parameter.UsedInQuery = true;
                            }
                            else if (child.InnerText.Equals("False"))
                            {
                                parameter.UsedInQuery = false;
                            }
                        }
                        else if (child.Name.Equals("ValidValues"))
                        {
                            foreach (XmlNode xmlParameterValue in child.SelectNodes("r:ParameterValues/r:ParameterValue", nsmgr))
                            {
                                ParameterValue parameterValue = new ParameterValue();

                                foreach (XmlNode node in xmlParameterValue.ChildNodes)
                                {
                                    if (node.Name.Equals("Label"))
                                    {
                                        parameterValue.Label = node.InnerText;
                                    }
                                    else if (node.Name.Equals("Value"))
                                    {
                                        parameterValue.Value = node.InnerText;
                                    }
                                }

                                parameter.ValidValues.Add(parameterValue);
                            }
                        }
                        else if (child.Name.Equals("DefaultValue"))
                        {
                            foreach (XmlNode xmlDefaultValue in child.SelectNodes("r:Values/r:Value", nsmgr))
                            {
                                parameter.DefaultValue.Add(xmlDefaultValue.InnerText);
                            }
                        }
                        else if (child.Name.Equals("CachingExtension"))
                        {
                            XmlNode xmlCacheColumn = child.SelectSingleNode("cx:CacheColumn", nsmgr);

                            if (xmlCacheColumn != null)
                                PopulateCacheColumn(parameter, xmlCacheColumn);
                        }
                    }
                }

                report.ReportParameters.Add(parameter);
            }

            foreach (XmlNode reportDataSet in root.SelectNodes("/r:Report/r:DataSets/r:DataSet", nsmgr))
            {
                DataSet dataSet = new DataSet(report);

                dataSet.Name = reportDataSet.Attributes["Name"].Value;

                XmlNode xmlQuery = reportDataSet.SelectSingleNode("r:Query", nsmgr);

                PopulateQuery(dataSet.Query, xmlQuery, nsmgr);

                PopulateCacheManagement(dataSet, xmlQuery, nsmgr);

                foreach (XmlNode child in reportDataSet.SelectNodes("r:Fields/r:Field/r:DataField", nsmgr))
                {
                    Field field = new Field(dataSet);
                    field.Name = child.ParentNode.Attributes["Name"].Value;
                    field.DataField = DataFieldFactory.Parse(child.InnerText);

                    XmlNode typeName = child.ParentNode.SelectSingleNode("rd:TypeName", nsmgr);

                    if (typeName != null)
                    {
                        field.TypeName = typeName.InnerText;
                    }

                    XmlNode cacheColumn = child.ParentNode.SelectSingleNode("cx:CachingExtension/cx:CacheColumn", nsmgr);

                    if (cacheColumn != null)
                    {
                        PopulateCacheColumn(field, cacheColumn);
                    }
                    else if (field.DataField is SqlTypeName)
                    {
                        field.CacheField = field.DataField.Name.Replace("_", "");
                    }
                    else
                    {
                        field.CacheField = field.Name.Replace("_", "");
                    }

                    field.PropertyChanged += report.FieldChanged;

                    dataSet.Fields.Add(field);
                }

                report.DataSets.Add(dataSet);
            }

            return report;
        }

        private void PopulateCacheColumn(ReportParameter parameter, XmlNode xmlCacheColumn)
        {
            foreach (XmlNode node in xmlCacheColumn.ChildNodes)
            {
                if (node.Name.Equals("ColumnName"))
                {
                    parameter.ColumnName = node.InnerText;
                }
                else if (node.Name.Equals("ColumnTypeName"))
                {
                    parameter.ColumnType = node.InnerText;
                }
            }
        }

        private void PopulateCacheColumn(Field field, XmlNode xmlCacheColumn)
        {
            foreach (XmlNode node in xmlCacheColumn.ChildNodes)
            {
                if (node.Name.Equals("ColumnName"))
                {
                    field.CacheField = node.InnerText;
                }
                else if (node.Name.Equals("ColumnTypeName"))
                {
                    field.CacheType = node.InnerText;
                }
            }
        }

        private void PopulateCacheManagement(DataSet dataSet, XmlNode xmlQuery, XmlNamespaceManager nsmgr)
        {
            CacheDefinition cache = null;

            XmlNode xmlSelect = xmlQuery.SelectSingleNode("cx:CachingExtension/cx:CacheManagement/cx:Select/cx:Query", nsmgr);

            if (xmlSelect != null)
            {
                cache = new CacheDefinition(dataSet);

                PopulateQuery(cache.Select, xmlSelect, nsmgr);
            }

            XmlNode xmlInsert = xmlQuery.SelectSingleNode("cx:CachingExtension/cx:CacheManagement/cx:Insert/cx:Query", nsmgr);

            if (xmlInsert != null)
            {
                cache = new CacheDefinition(dataSet);

                PopulateQuery(cache.Insert, xmlInsert, nsmgr);
            }

            XmlNode xmlDelete = xmlQuery.SelectSingleNode("cx:CachingExtension/cx:CacheManagement/cx:Delete/cx:Query", nsmgr);

            if (xmlDelete != null)
            {
                if (cache == null)
                    cache = new CacheDefinition(dataSet);

                PopulateQuery(cache.Delete, xmlDelete, nsmgr);
            }

            if (cache != null)
            {
                dataSet.Cache = cache;
            }
        }

        private void PopulateQuery(Query query, XmlNode xmlQuery, XmlNamespaceManager nsmgr)
        {
            foreach (XmlNode child in xmlQuery.ChildNodes)
            {
                if (child.NodeType.Equals(XmlNodeType.Element))
                {
                    if (child.Name.Equals("CommandType"))
                    {
                        query.CommandType = (System.Data.CommandType)Enum.Parse(typeof(System.Data.CommandType), child.InnerText);
                    }
                    else if (child.Name.Equals("CommandText"))
                    {
                        query.CommandText = child.InnerText;
                    }
                    else if (child.Name.Equals("DataSourceName"))
                    {
                        query.DataSourceName = child.InnerText;
                    }
                    else if (child.Name.Equals("QueryParameters"))
                    {
                        foreach (XmlNode node in child.SelectNodes("r:QueryParameter/r:Value", nsmgr))
                        {
                            QueryParameter queryParameter = new QueryParameter(query);
                            queryParameter.Name = node.ParentNode.Attributes["Name"].Value;
                            queryParameter.Value = node.InnerText;
                            query.QueryParameters.Add(queryParameter);
                        }
                    }
                }
            }
        }
    }
}