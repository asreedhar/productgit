using System;
using System.Collections.Generic;
using FirstLook.Common.Data;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class ReportParameter : DomainObject, IDataParameterTemplate
    {
        private string m_name;
        private bool m_allowBlank;
        private string m_dataType;
        private bool m_hidden;
        private bool m_multiValue;
        private bool m_nullable;
        private string m_prompt;
        private Nullable<bool> m_usedInQuery;
        private readonly List<ParameterValue> m_validValues = new List<ParameterValue>();
        private readonly List<string> m_defaultValue = new List<string>();

        private string m_columnName;
        private string m_columnType;

        public ReportParameter() : base()
        {
        }

        protected ReportParameter(ReportParameter reportParameter) : this()
        {
            AllowBlank = reportParameter.AllowBlank;
            DataType = reportParameter.DataType;
            Hidden = reportParameter.Hidden;
            MultiValue = reportParameter.MultiValue;
            IsNullable = reportParameter.IsNullable;
            Prompt = reportParameter.Prompt;
            UsedInQuery = reportParameter.UsedInQuery;
            Name = reportParameter.Name;
            
            foreach (ParameterValue parameterValue in reportParameter.ValidValues)
                ValidValues.Add(parameterValue.Clone());

            foreach (string defaultValue in reportParameter.DefaultValue)
                DefaultValue.Add(defaultValue);

            ColumnName = reportParameter.ColumnName;
            ColumnType = reportParameter.ColumnType;
        }

        public bool AllowBlank
        {
            get { return m_allowBlank; }
            set { m_allowBlank = value; NotifyChanged("AllowBlank"); }
        }

        public string DataType
        {
            get { return m_dataType; }
            set { m_dataType = CleanString(value); NotifyChanged("DataType"); }
        }

        public bool Hidden
        {
            get { return m_hidden; }
            set { m_hidden = value; NotifyChanged("Hidden"); }
        }

        public bool MultiValue
        {
            get { return m_multiValue; }
            set { m_multiValue = value; NotifyChanged("MultiValue"); }
        }

        public bool IsNullable
        {
            get { return m_nullable; }
            set { m_nullable = value; NotifyChanged("IsNullable"); }
        }

        public string Prompt
        {
            get { return m_prompt; }
            set { m_prompt = CleanString(value); NotifyChanged("Prompt"); }
        }

        public Nullable<bool> UsedInQuery
        {
            get { return m_usedInQuery; }
            set { m_usedInQuery = value; NotifyChanged("UsedInQuery"); }
        }

        public string Name
        {
            get { return m_name; }
            set { m_name = CleanString(value); NotifyChanged("Name"); }
        }

        public List<ParameterValue> ValidValues
        {
            get { return m_validValues; }
        }

        public List<string> DefaultValue
        {
            get { return m_defaultValue; }
        }

        public string ColumnName
        {
            get { return m_columnName; }
            set { m_columnName = CleanString(value); NotifyChanged("ColumnName"); }
        }

        public string ColumnType
        {
            get { return m_columnType; }
            set { m_columnType = value; }
        }

        protected override List<Rule> CreateRules()
        {
            List<Rule> rules = base.CreateRules();
            rules.Add(new SimpleRule("ColumnName", "Column name cannot be blank.", delegate { return (UsedInQuery.HasValue && UsedInQuery.Value) ? CleanString(ColumnName).Length != 0 : true; }));
            return rules;
        }

        #region IDataParameterTemplate Members

        System.Data.DbType IDataParameterTemplate.DbType
        {
            get
            {
                System.Data.DbType result = System.Data.DbType.Int32;

                if (DataType.Equals("Boolean"))
                    result = System.Data.DbType.Boolean;
                else if (DataType.Equals("DateTime"))
                    result = System.Data.DbType.Boolean;
                else if (DataType.Equals("Float"))
                    result = System.Data.DbType.Decimal;
                else if (DataType.Equals("Integer"))
                    result = System.Data.DbType.Int32;
                else if (DataType.Equals("String"))
                    result = System.Data.DbType.String;

                return result;
            }
        }

        bool IDataParameterTemplate.IsNullable
        {
            get { return IsNullable; }
        }

        string IDataParameterTemplate.Name
        {
            get { return Name; }
        }

        #endregion

        public ReportParameter Clone()
        {
            return new ReportParameter(this);
        }
    }
}