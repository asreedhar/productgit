using System.Collections.Generic;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class Field : DomainObject
    {
        private readonly DataSet m_dataSet;
        private string m_name;
        private string m_value;
        private DataField m_dataField;
        private string m_typeName;

        private string m_cacheField;
        private string m_cacheType;

        public Field(DataSet m_dataSet)
        {
            this.m_dataSet = m_dataSet;
        }

        protected Field(Field field, DataSet m_dataSet)
        {
            this.m_dataSet = m_dataSet;
            Name = field.Name;
            Value = field.Value;
            DataField = field.DataField.Clone();
            TypeName = field.TypeName;
            CacheField = field.CacheField;
            CacheType = field.CacheType;
        }

        public DataSet DataSet
        {
            get { return m_dataSet; }
        }

        public string Name
        {
            get { return m_name; }
            set { m_name = CleanString(value); NotifyChanged("Name"); }
        }

        public string Value
        {
            get { return m_value; }
            set { m_value = CleanString(value); NotifyChanged("Value"); }
        }

        public DataField DataField
        {
            get { return m_dataField; }
            set { m_dataField = value; NotifyChanged("DataField"); }
        }

        public string TypeName
        {
            get { return m_typeName; }
            set { m_typeName = CleanString(value); NotifyChanged("TypeName"); }
        }

        public string CacheField
        {
            get { return m_cacheField; }
            set { m_cacheField = CleanString(value); NotifyChanged("CacheField"); }
        }

        public string CacheType
        {
            get { return m_cacheType; }
            set { m_cacheType = value; }
        }

        protected override List<Rule> CreateRules()
        {
            List<Rule> rules = base.CreateRules();
            rules.Add(new RequiredRule("Name", "Name cannot be blank."));
            rules.Add(new RequiredRule("CacheField", "CacheField cannot be blank."));
            rules.Add(new RequiredRule("TypeName", "TypeName cannot be blank."));
            return rules;
        }

        public Field Clone(DataSet dataSet)
        {
            return new Field(this, dataSet);
        }
    }
}