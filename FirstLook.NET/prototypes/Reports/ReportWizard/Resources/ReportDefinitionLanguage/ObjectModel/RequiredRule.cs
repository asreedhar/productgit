using System.Reflection;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class RequiredRule : Rule
    {
        public RequiredRule(string propertyName, string brokenDescription)
            : base(propertyName, brokenDescription)
        {
        }

        public override bool ValidateRule(DomainObject domainObject)
        {
            PropertyInfo pi = domainObject.GetType().GetProperty(PropertyName);
            string str = pi.GetValue(domainObject, null) as string;
            return ((str ?? string.Empty).Length != 0);
        }
    }
}
