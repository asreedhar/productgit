namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class ParameterValue : DomainObject
    {
        private string m_label;
        private string m_value;

        public ParameterValue()
            : this(null, null) { }

        protected ParameterValue(ParameterValue parameter)
            : this(parameter.Label, parameter.Value) { }

        public ParameterValue(string name, string value)
        {
            m_label = name;
            m_value = value;
        }

        public string Label
        {
            get { return m_label; }
            set { m_label = CleanString(value); NotifyChanged("Label"); }
        }

        public string Value
        {
            get { return m_value; }
            set { m_value = CleanString(value); NotifyChanged("Value"); }
        }

        public ParameterValue Clone()
        {
            return new ParameterValue(this);
        }
    }
}
