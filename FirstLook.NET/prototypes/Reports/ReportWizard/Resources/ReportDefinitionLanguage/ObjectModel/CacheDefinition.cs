namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class CacheDefinition
    {
        private readonly Query m_select;
        private readonly Query m_insert;
        private readonly Query m_delete;

        public CacheDefinition(DataSet dataSet)
        {
            m_select = new Query(dataSet);
            m_insert = new Query(dataSet);
            m_delete = new Query(dataSet);
        }

        public Query Select
        {
            get { return m_select; }
        }

        public Query Insert
        {
            get { return m_insert; }
        }

        public Query Delete
        {
            get { return m_delete; }
        }
    }
}
