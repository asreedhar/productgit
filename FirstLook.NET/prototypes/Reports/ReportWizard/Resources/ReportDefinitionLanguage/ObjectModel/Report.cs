using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using FirstLook.Common.Data;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class Report
    {
        private readonly string m_filename;
        private readonly XmlDocument m_document;
        private readonly List<ReportParameter> m_reportParameters = new List<ReportParameter>();
        private readonly List<DataSet> m_dataSets = new List<DataSet>();

        public Report(string filename, XmlDocument document)
        {
            m_filename = filename;
            m_document = document;
        }

        public List<ReportParameter> ReportParameters
        {
            get { return m_reportParameters; }
        }

        public List<DataSet> DataSets
        {
            get { return m_dataSets; }
        }

        public string Filename
        {
            get { return m_filename; }
        }

        public string Name()
        {
            int start = Filename.LastIndexOf(Path.DirectorySeparatorChar)+1;
            int end = Filename.LastIndexOf(".") - 1;
            return Filename.Substring(start, end - start);
        }

        public void Save()
        {
            AddCachingExtensions();

            XmlWriterSettings s = new XmlWriterSettings();
            s.CloseOutput = true;
            s.Encoding = System.Text.Encoding.Default;
            s.Indent = true;
            s.IndentChars = "  ";
            s.NewLineChars = Environment.NewLine;
            s.OmitXmlDeclaration = false;

            using (XmlWriter w = XmlWriter.Create(Filename, s))
            {
                m_document.Save(w);
            }
        }

        // test code

        static readonly string CachingExtensionsNamespace = @"http://schemas.firstlook.biz/SQLServer/reporting/caching-extensions";

        private static readonly string XmlSchemaNamespace = @"http://www.w3.org/2000/xmlns/";

        protected void AddCachingExtensions()
        {
            XmlNode root = m_document.DocumentElement;

            // do not apply extensions twice

            XmlNamespaceManager nsmgr = NsMgr();

            if (root.SelectSingleNode("//cx:CachingExtension", NsMgr()) != null)
                return;

            XmlAttribute attr = m_document.CreateAttribute("xmlns", "cx", XmlSchemaNamespace);
            attr.Value = CachingExtensionsNamespace;
            m_document.DocumentElement.Attributes.Append(attr);

            // r:Report/r:ReportParameter + cx:CachingExtension/CacheColumn

            foreach (ReportParameter reportParameter in ReportParameters)
            {
                XmlNode xmlReportParameter = root.SelectSingleNode(string.Format(
                    "/r:Report/r:ReportParameters/r:ReportParameter[@Name='{0}']",
                    reportParameter.Name), nsmgr);

                xmlReportParameter.AppendChild(CreateCacheColumn(reportParameter.ColumnName, reportParameter.ColumnType));
            }

            // r:Report/r:DataSets/r:DataSet/r:Query + cx:CachingExtension/CacheManagement

            foreach (DataSet dataSet in DataSets)
            {
                XmlNode xmlQuery = root.SelectSingleNode(string.Format(
                        "/r:Report/r:DataSets/r:DataSet[@Name='{0}']/r:Query",
                        dataSet.Name), nsmgr);

                XmlNode extension = m_document.CreateElement("cx", "CachingExtension", CachingExtensionsNamespace);

                XmlNode cacheManagement = m_document.CreateElement("CacheManagement", CachingExtensionsNamespace);

                XmlNode xmlSelect = m_document.CreateElement("Select", CachingExtensionsNamespace);
                xmlSelect.AppendChild(CreateCacheQuery(CacheSelect(dataSet), "Text", dataSet.Query.QueryParameters, "20", "REPORTHOST"));
                cacheManagement.AppendChild(xmlSelect);

                XmlNode xmlInsert = m_document.CreateElement("Insert", CachingExtensionsNamespace);
                xmlInsert.AppendChild(CreateCacheQuery(CacheInsert(dataSet), "Text", CacheInsertQueryParameters(dataSet), "20", "REPORTHOST"));
                cacheManagement.AppendChild(xmlInsert);

                XmlNode xmlDelete = m_document.CreateElement("Delete", CachingExtensionsNamespace);
                xmlDelete.AppendChild(CreateCacheQuery(CacheDelete(dataSet), "Text", dataSet.Query.QueryParameters, "20", "REPORTHOST"));
                cacheManagement.AppendChild(xmlDelete);

                extension.AppendChild(cacheManagement);

                xmlQuery.AppendChild(extension);
            }

            // r:Report/r:DataSets/r:DataSet/r:Fields/r:Field + cx:CachingExtension/CacheColumn

            foreach (DataSet dataSet in DataSets)
            {
                foreach (Field field in dataSet.Fields)
                {
                    XmlNode xmlField = root.SelectSingleNode(string.Format(
                        "/r:Report/r:DataSets/r:DataSet[@Name='{0}']/r:Fields/r:Field[@Name='{1}']",
                        dataSet.Name, field.Name), nsmgr);

                    xmlField.AppendChild(CreateCacheColumn(field.CacheField, field.CacheType));
                }
            }
        }

        private IEnumerable<QueryParameter> CacheInsertQueryParameters(DataSet set)
        {
            List<QueryParameter> parameters = new List<QueryParameter>();
            parameters.AddRange(set.Query.QueryParameters);
            foreach (Field field in set.Fields)
            {
                QueryParameter p = new QueryParameter(null);
                p.Name = "@" + field.CacheField;
                p.Value = "=Fields!" + field.Name + ".Value";
                parameters.Add(p);
            }
            return parameters;
        }

        protected string CacheInsert(DataSet dataSet)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("INSERT INTO {0} ({1}", dataSet.Name, Environment.NewLine);
            AppendInsertQueryParameters(dataSet, sb);
            AppendInsertFields(dataSet, sb);
            sb.AppendFormat("{0}\t[CacheExpiryDateTime]", Environment.NewLine);
            sb.AppendFormat("{0}) VALUES ({0}", Environment.NewLine);
            AppendInsertQueryParameterBindings(dataSet, sb);
            AppendInsertFieldBindings(dataSet, sb);
            sb.AppendFormat("{0}\t@CacheExpiryDateTime", Environment.NewLine);
            sb.AppendFormat("{0})", Environment.NewLine);
            return sb.ToString();
        }

        private void AppendInsertFieldBindings(DataSet set, StringBuilder sb)
        {
            foreach (Field field in set.Fields)
            {
                sb.AppendFormat("\t@{0},{1}", field.CacheField, Environment.NewLine);
            }
        }

        private void AppendInsertQueryParameterBindings(DataSet set, StringBuilder sb)
        {
            IList<IDataParameterTemplate> parameters = ((IDataCommandTemplate)set).Parameters;

            foreach (ReportParameter parameter in parameters)
            {
                sb.AppendFormat("\t@{0},{1}", parameter.Name, Environment.NewLine);
            }
        }

        private void AppendInsertFields(DataSet set, StringBuilder sb)
        {
            foreach (Field field in set.Fields)
            {
                sb.AppendFormat("\t[{0}],{1}", field.CacheField, Environment.NewLine);
            }
        }

        private void AppendInsertQueryParameters(DataSet set, StringBuilder sb)
        {
            IList<IDataParameterTemplate> parameters = ((IDataCommandTemplate)set).Parameters;

            foreach (ReportParameter parameter in parameters)
            {
                sb.AppendFormat("\t[{0}],{1}", parameter.Name, Environment.NewLine);
            }
        }

        protected string CacheDelete(DataSet dataSet)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("DELETE FROM {0} WHERE {1}", dataSet.Name, Environment.NewLine);
            AppendWhereClause(dataSet, sb);
            sb.AppendFormat("{0}AND\t[CacheExpiryDateTime] < GETDATE();{0}", Environment.NewLine);
            return sb.ToString();
        }

        protected string CacheSelect(DataSet dataSet)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT");
            for (int i = 0; i < dataSet.Fields.Count; i++)
            {
                if (i > 0) sb.Append(",");
                sb.AppendFormat("{1}\t[{0}]", dataSet.Fields[i].CacheField, Environment.NewLine);
            }
            sb.AppendLine("");
            sb.AppendFormat("FROM [{0}].[{1}]{2}", "cache", dataSet.Name, Environment.NewLine);
            sb.AppendLine("WHERE");
            AppendWhereClause(dataSet, sb);
            sb.AppendFormat("{0}AND\t[CacheExpiryDateTime] > GETDATE()", Environment.NewLine);
            return sb.ToString();
        }

        protected void AppendWhereClause(DataSet dataSet, StringBuilder sb)
        {
            Regex reportParameterRegex = new Regex("^=Parameters!(.*)\\.Value$");

            for (int i = 0; i < dataSet.Query.QueryParameters.Count; i++)
            {
                if (i == 0)
                    sb.Append("\t\t");
                else
                    sb.AppendFormat("{0}AND\t", Environment.NewLine);

                QueryParameter queryParameter = dataSet.Query.QueryParameters[i];

                Match match = reportParameterRegex.Match(queryParameter.Value);

                if (match.Success)
                {
                    ReportParameter reportParameter = dataSet.Report.ReportParameter(match.Groups[1].Value);

                    sb.AppendFormat("[{0}]={1}", reportParameter.ColumnName, queryParameter.Name);
                }
                else
                {
                    throw new IndexOutOfRangeException(string.Format(
                        "Cannot resolve query parameter '{0}'", queryParameter.Name));
                }
            }
        }

        protected XmlNode CreateCacheQuery(string commandText, string commandType, IEnumerable<QueryParameter> queryParameters, string timeout, string dataSourceName)
        {
            XmlNode xmlQuery = m_document.CreateElement("Query", CachingExtensionsNamespace);

            XmlNode ct = m_document.CreateElement("CommandText", CachingExtensionsNamespace);
            ct.AppendChild(m_document.CreateTextNode(commandText));
            xmlQuery.AppendChild(ct);

            XmlNode cy = m_document.CreateElement("CommandType", CachingExtensionsNamespace);
            cy.AppendChild(m_document.CreateTextNode(commandType));
            xmlQuery.AppendChild(cy);

            XmlNode qps = m_document.CreateElement("QueryParameters", CachingExtensionsNamespace);
            foreach (QueryParameter p in queryParameters)
            {
                XmlNode qp = m_document.CreateElement("QueryParameter", CachingExtensionsNamespace);
                XmlAttribute a = m_document.CreateAttribute("Name");
                a.Value = p.Name;
                qp.Attributes.Append(a);
                XmlNode qpv = m_document.CreateElement("Value", CachingExtensionsNamespace);
                qpv.AppendChild(m_document.CreateTextNode(p.Value));
                qp.AppendChild(qpv);
                qps.AppendChild(qp);
            }
            xmlQuery.AppendChild(qps);

            XmlNode to = m_document.CreateElement("Timeout", CachingExtensionsNamespace);
            to.AppendChild(m_document.CreateTextNode(timeout));
            xmlQuery.AppendChild(to);

            XmlNode ds = m_document.CreateElement("DataSourceName", CachingExtensionsNamespace);
            ds.AppendChild(m_document.CreateTextNode(dataSourceName));
            xmlQuery.AppendChild(ds);

            return xmlQuery;
        }

        protected XmlNode CreateCacheColumn(string columnName, string columnTypeName)
        {
            XmlNode extension = m_document.CreateElement("cx", "CachingExtension", CachingExtensionsNamespace);

            XmlNode cacheColumn = m_document.CreateElement("CacheColumn", CachingExtensionsNamespace);

            XmlElement name = m_document.CreateElement("ColumnName", CachingExtensionsNamespace);
            name.AppendChild(m_document.CreateTextNode(columnName));
            cacheColumn.AppendChild(name);

            XmlElement type = m_document.CreateElement("ColumnTypeName", CachingExtensionsNamespace);
            type.AppendChild(m_document.CreateTextNode(columnTypeName));
            cacheColumn.AppendChild(type);

            extension.AppendChild(cacheColumn);

            return extension;
        }

        // test code

        public ReportParameter ReportParameter(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name", "Report parameter names cannot be null nor empty");

            foreach (ReportParameter reportParameter in m_reportParameters)
                if (reportParameter.Name.Equals(name))
                    return reportParameter;
            
            throw new IndexOutOfRangeException(string.Format("No report parameter named '{0}'", name));
        }

        public void QueryChanged(object sender, PropertyChangedEventArgs e)
        {
            Query query = sender as Query;

            if (query != null)
            {
                XmlNamespaceManager nsmgr = NsMgr();

                string xpath = string.Format(
                    "/r:Report/r:DataSets/r:DataSet[@Name='{0}']/r:Query/r:{1}",
                    query.DataSet.Name,
                    e.PropertyName);

                Debug.WriteLine(string.Format("XPath: {0}", xpath));

                XmlNode xmlQuery = m_document.DocumentElement.SelectSingleNode(xpath, nsmgr);

                if (xmlQuery == null)
                {
                    Debug.WriteLine(string.Format("No such query element {0} in data set {1}", e.PropertyName, query.DataSet.Name));
                }
                else if (e.PropertyName.Equals("CommandText"))
                {
                    xmlQuery.InnerText = query.CommandText;
                }
                else if (e.PropertyName.Equals("CommandType"))
                {
                    xmlQuery.InnerText = query.CommandType.ToString();
                }
                else if (e.PropertyName.Equals("DataSourceName"))
                {
                    xmlQuery.InnerText = query.DataSourceName;
                }
                else
                {
                    Debug.WriteLine(string.Format("We do not update query element {0} in data set {1}", e.PropertyName, query.DataSet.Name));
                }
            }
        }

        public void FieldChanged(object sender, PropertyChangedEventArgs e)
        {
            Field field = sender as Field;

            if (field != null && e.PropertyName != null)
            {
                XmlNamespaceManager nsmgr = NsMgr();

                string xpath;

                if (e.PropertyName.Equals("TypeName"))
                    xpath = string.Format(
                        "/r:Report/r:DataSets/r:DataSet[@Name='{0}']/r:Fields/r:Field[@Name='{1}']/rd:TypeName",
                        field.DataSet.Name,
                        field.Name);
                else if (e.PropertyName.Equals("DataField"))
                    xpath = string.Format(
                        "/r:Report/r:DataSets/r:DataSet[@Name='{0}']/r:Fields/r:Field[@Name='{1}']/r:DataField",
                        field.DataSet.Name,
                        field.Name);
                else
                    return;

                Debug.WriteLine(string.Format("XPath: {0}", xpath));

                XmlNode xmlNode = m_document.DocumentElement.SelectSingleNode(xpath, nsmgr);

                if (e.PropertyName.Equals("TypeName"))
                {
                    if (xmlNode == null)
                    {
                        xpath = string.Format(
                            "/r:Report/r:DataSets/r:DataSet[@Name='{0}']/r:Fields/r:Field[@Name='{1}']",
                            field.DataSet.Name,
                            field.Name);

                        Debug.WriteLine(string.Format("XPath: {0}", xpath));

                        XmlNode xmlField = m_document.DocumentElement.SelectSingleNode(xpath, nsmgr);

                        if (xmlField != null)
                        {
                            xmlNode = m_document.CreateElement(
                                "rd",
                                "TypeName",
                                m_document.DocumentElement.Attributes["xmlns:rd"].Value);
                            xmlNode.InnerText = field.TypeName;
                            xmlField.AppendChild(xmlNode);
                        }
                        else
                        {
                            Debug.WriteLine(string.Format("No such field {0} in data set {1}", field.Name, field.DataSet.Name));
                        }
                    }
                    else
                    {
                        xmlNode.InnerText = field.TypeName;
                    }
                }
                else
                {
                    if (xmlNode == null)
                    {
                        Debug.WriteLine(string.Format("No such field {0} in data set {1}", field.Name, field.DataSet.Name));
                    }
                    else
                    {
                        xmlNode.InnerText = field.DataField.ToExternalForm();
                    }
                }
            }
        }

        private XmlNamespaceManager NsMgr()
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(m_document.NameTable);

            nsmgr.AddNamespace("r", m_document.DocumentElement.NamespaceURI);

            nsmgr.AddNamespace("rd", m_document.DocumentElement.Attributes["xmlns:rd"].Value);

            nsmgr.AddNamespace("cx", CachingExtensionsNamespace);

            return nsmgr;
        }

        public Report Clone(string filename)
        {
            XmlDocument c_document = (XmlDocument) m_document.CloneNode(true);

            XmlNamespaceManager nsmgr = NsMgr();

            foreach (XmlNode node in c_document.DocumentElement.SelectNodes("//rd:MdxQuery", nsmgr))
                node.ParentNode.RemoveChild(node);

            Report report = new Report(filename, c_document);

            foreach (ReportParameter reportParameter in ReportParameters)
                report.ReportParameters.Add(reportParameter.Clone());

            foreach (DataSet dataSet in DataSets)
                report.DataSets.Add(dataSet.Clone(report));

            foreach (DataSet dataSet in report.DataSets)
            {
                foreach (Field field in dataSet.Fields)
                {
                    field.PropertyChanged += report.FieldChanged;
                    field.DataField = DataFieldFactory.Parse(field.CacheField);
                }
            }

            return report;
        }
    }
}
