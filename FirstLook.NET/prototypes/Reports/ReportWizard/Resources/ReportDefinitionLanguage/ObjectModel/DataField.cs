using System.Xml;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public interface DataField
    {
        string Name
        {
            get; set;
        }

        string ToExternalForm();

        DataField Clone();
    }

    class DataFieldFactory
    {
        public static DataField Parse(string typeName)
        {
            if (typeName.StartsWith("<?xml"))
            {
                return new MdxTypeName(typeName);
            }
            else
            {
                return new SqlTypeName(typeName);
            }
        }
    }

    class SqlTypeName : DataField
    {
        private string typeName;

        public SqlTypeName(string typeName)
        {
            this.typeName = typeName;
        }

        protected SqlTypeName(SqlTypeName copy)
        {
            typeName = copy.typeName;
        }

        #region ITypeName Members

        string DataField.Name
        {
            get { return typeName; }
            set { typeName = value; }
        }

        string DataField.ToExternalForm()
        {
            return typeName;
        }

        DataField DataField.Clone()
        {
            return new SqlTypeName(this);
        }
        #endregion
    }

    class MdxTypeName : DataField
    {
        private readonly string uniqueName;
        private readonly string type;

        public MdxTypeName(string dataField)
        {
            XmlDocument fieldDoc = new XmlDocument();

            fieldDoc.LoadXml(dataField);

            uniqueName = fieldDoc.DocumentElement.Attributes["UniqueName"].Value;

            type = fieldDoc.DocumentElement.Attributes["xsi:type"].Value;
        }

        protected MdxTypeName(MdxTypeName copy)
        {
            uniqueName = copy.uniqueName;
            type = copy.type;
        }

        #region ITypeName Members

        string DataField.Name
        {
            get
            {
                if (type.Equals("Level"))
                {
                    return string.Format("{0}.[MEMBER_CAPTION]", uniqueName);
                }
                else
                {
                    return uniqueName;
                }
            }
            set
            {
                throw new System.Exception("The method or operation is not implemented.");
            }
        }

        string DataField.ToExternalForm()
        {
            return string.Format(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><Field xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"{0}\" UniqueName=\"{1}\" />",
                type, uniqueName);
        }

        DataField DataField.Clone()
        {
            return new MdxTypeName(this);
        }

        #endregion
    }
}
