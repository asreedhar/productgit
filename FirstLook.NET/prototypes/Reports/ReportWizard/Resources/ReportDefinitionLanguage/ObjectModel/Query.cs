using System.Collections.Generic;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class Query : DomainObject
    {
        private readonly DataSet m_dataSet;
        private string m_commandText;
        private System.Data.CommandType m_commandType;
        private string m_dataSourceName;
        private readonly List<QueryParameter> m_queryParameters = new List<QueryParameter>();

        public Query(DataSet dataSet) : base()
        {
            m_dataSet = dataSet;
        }

        protected Query(Query query) : this(query.DataSet)
        {
            CommandText = query.CommandText;
            CommandType = query.CommandType;
            DataSourceName = query.DataSourceName;

            foreach (QueryParameter queryParameter in query.QueryParameters)
                QueryParameters.Add(queryParameter.Clone());
        }

        public DataSet DataSet
        {
            get { return m_dataSet; }
        }

        public string CommandText
        {
            get { return m_commandText; }
            set { m_commandText = CleanString(value); NotifyChanged("CommandText"); }
        }

        public System.Data.CommandType CommandType
        {
            get { return m_commandType; }
            set { m_commandType = value; NotifyChanged("CommandType"); }
        }

        public string DataSourceName
        {
            get { return m_dataSourceName; }
            set { m_dataSourceName = CleanString(value); NotifyChanged("DataSourceName"); }
        }

        public List<QueryParameter> QueryParameters
        {
            get { return m_queryParameters; }
        }

        public Query Clone()
        {
            return new Query(this);
        }
    }
}