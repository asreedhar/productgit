namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class QueryParameter : DomainObject
    {
        private readonly Query m_query;
        private string m_name;
        private string m_value;

        public QueryParameter(Query query) : base()
        {
            m_query = query;
        }

        protected QueryParameter(QueryParameter queryParameter) : this(queryParameter.Query)
        {
            Name = queryParameter.Name;
            Value = queryParameter.Value;
        }

        public Query Query
        {
            get { return m_query; }
        }

        public string Name
        {
            get { return m_name; }
            set { m_name = CleanString(value); NotifyChanged("Name"); }
        }

        public string Value
        {
            get { return m_value; }
            set { m_value = CleanString(value); NotifyChanged("Value"); }
        }

        public QueryParameter Clone()
        {
            return new QueryParameter(this);
        }
    }
}