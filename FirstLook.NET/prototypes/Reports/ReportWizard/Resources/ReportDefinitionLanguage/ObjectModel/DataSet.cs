using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Data;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    public class DataSet : DomainObject, IDataCommandTemplate
    {
        private readonly Report m_report;
        private string m_name;
        private readonly Query m_query;
        private readonly List<Field> m_fields;
        private CacheDefinition m_cache;

        protected DataSet(Report report, Query query)
            : base()
        {
            m_report = report;
            m_fields = new List<Field>();
            m_query = (query ?? new Query(this));
            m_query.PropertyChanged += report.QueryChanged;
        }

        protected DataSet(Report report, DataSet dataSet)
            : this(report, dataSet.Query.Clone())
        {
            Name = dataSet.Name;
            foreach (Field field in dataSet.Fields)
                Fields.Add(field.Clone(this));
        }

        public DataSet(Report report) : this(report, (Query) null)
        {
        }

        public Report Report
        {
            get { return m_report; }
        }

        public string Name
        {
            get { return m_name; }
            set { m_name = CleanString(value); NotifyChanged("DataType"); }
        }

        public Query Query
        {
            get { return m_query; }
        }

        public List<Field> Fields
        {
            get { return m_fields; }
        }

        public CacheDefinition Cache
        {
            get { return m_cache; }
            set { m_cache = value; }
        }

        public string CacheInsertCommandText(string schemaName, string tableName)
        {
            string columns = null, columnBindings = null;

            return string.Format("INSERT INTO [{0}].[{1}] ({2}) VALUES ({3})", schemaName, tableName, columns, columnBindings);
        }

        public string CacheQueryCommandText(string schemaName, string tableName)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("DELETE FROM {0} WHERE {1}", tableName, Environment.NewLine);
            AppendWhereClause(sb);
            sb.AppendFormat("{0}AND\t[CacheExpiryDateTime] < GETDATE();{0}", Environment.NewLine);
            sb.AppendLine("SELECT");
            for (int i = 0; i < Fields.Count; i++)
            {
                if (i > 0)
                    sb.Append(",");
                sb.AppendFormat("{1}\t[{0}]", Fields[i].CacheField, Environment.NewLine);
            }
            sb.AppendLine("");
            sb.AppendFormat("FROM [{0}].[{1}]{2}", schemaName, tableName, Environment.NewLine);
            sb.AppendLine("WHERE");
            AppendWhereClause(sb);
            sb.AppendFormat("{0}AND\t[CacheExpiryDateTime] > GETDATE()", Environment.NewLine);
            return sb.ToString();
        }

        private void AppendWhereClause(StringBuilder sb)
        {
            Regex reportParameterRegex = new Regex("^=Parameters!(.*)\\.Value$");

            for (int i = 0; i < Query.QueryParameters.Count; i++)
            {
                if (i == 0)
                    sb.Append("\t\t");
                else
                    sb.AppendFormat("{0}AND\t", Environment.NewLine);

                QueryParameter queryParameter = Query.QueryParameters[i];

                Match match = reportParameterRegex.Match(queryParameter.Value);

                if (match.Success)
                {
                    ReportParameter reportParameter = m_report.ReportParameter(match.Groups[1].Value);

                    sb.AppendFormat("[{0}]={1}", reportParameter.ColumnName, queryParameter.Name);
                }
                else
                {
                    throw new IndexOutOfRangeException(string.Format(
                        "Cannot resolve query parameter '{0}'", queryParameter.Name));
                }
            }
        }

        public string CacheTableDefinition(string schemaName, string tableName)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{2}CREATE TABLE [{0}].[{1}] ({2}", schemaName, tableName, Environment.NewLine);

            sb.AppendLine("\t[Id] INT IDENTITY(1,1) NOT NULL,");

            IList<IDataParameterTemplate> parameters = ((IDataCommandTemplate)this).Parameters;

            foreach (ReportParameter parameter in parameters)
            {
                sb.AppendFormat("\t[{0}] {1} {2},{3}", parameter.Name, parameter.ColumnType, ToNullConstraint(parameter.IsNullable), Environment.NewLine);
            }

            foreach (Field field in Fields)
            {
                sb.AppendFormat("\t[{0}] {1} {2},{3}", field.CacheField, field.CacheType, ToNullConstraint(true), Environment.NewLine);
            }

            sb.AppendFormat("\t[CacheExpiryDateTime] DATETIME NOT NULL,{0}", Environment.NewLine);

            sb.AppendFormat("\tCONSTRAINT PK_{0} PRIMARY KEY NONCLUSTERED ({1}\t\tId{1}\t){1}){1}GO{1}{1}", tableName, Environment.NewLine);

            sb.AppendFormat("CREATE CLUSTERED INDEX IDX_{1} ON [{0}].[{1}] ({2}", schemaName, tableName, Environment.NewLine);

            for (int i = 0; i < parameters.Count; i++)
            {
                sb.AppendFormat("\t[{0}]", parameters[i].Name);
                if (i + 1 < parameters.Count)
                    sb.Append(",");
                sb.AppendLine("");
            }

            sb.AppendFormat("){0}GO{0}", Environment.NewLine);

            return sb.ToString();
        }

        private static string ToNullConstraint(bool nullable)
        {
            return (nullable ? "NULL" : "NOT NULL");
        }

        protected override List<Rule> CreateRules()
        {
            List<Rule> rules = base.CreateRules();
            rules.Add(new SimpleRule("Name", "Name cannot be blank.", delegate { return CleanString(Name).Length != 0; }));
            return rules;
        }

        #region IDataCommandTemplate Members

        string IDataCommandTemplate.CommandText
        {
            get { return m_query.CommandText; }
            set { m_query.CommandText = value; }
        }

        System.Data.CommandType IDataCommandTemplate.CommandType
        {
            get { return m_query.CommandType; }
            set { m_query.CommandType = value; }
        }

        IDataMap IDataCommandTemplate.DataMap
        {
            get
            {
                DataMap map = new DataMap();

                foreach (Field f in m_fields)
                {
                    map.AddDataMapEntry(new DataMapEntry(f.DataField.Name, f.Name, f.TypeName));
                }

                return map;
            }
            set
            {
                // no op
            }
        }

        IList<IDataParameterTemplate> IDataCommandTemplate.Parameters
        {
            get
            {
                List<IDataParameterTemplate> parameters = new List<IDataParameterTemplate>();

                foreach (QueryParameter queryParameter in m_query.QueryParameters)
                {
                    string name = queryParameter.Name;

                    if (name.StartsWith("@")) name = queryParameter.Name.Substring(1);

                    parameters.Add(m_report.ReportParameter(name));
                }

                return parameters;
            }
        }

        #endregion

        public DataSet Clone(Report report)
        {
            return new DataSet(report, this);
        }
    }
}