using System;
using System.Collections.Generic;
using FirstLook.Common.Data;

namespace ReportWizard.Resources.ReportDefinitionLanguage.ObjectModel
{
    class DataMap : IDataMap
    {
        private readonly IDictionary<string, IDataMapEntry> entries;

        public DataMap()
        {
            entries = new Dictionary<string, IDataMapEntry>();
        }

        public ICollection<IDataMapEntry> Entries
        {
            get { return entries.Values; }
        }

        public IDataMapEntry FindField(string fieldName)
        {
            return entries[fieldName];
        }

        public IDataMapEntry FindColumn(string columnName)
        {
            foreach (DataMapEntry entry in entries.Values)
            {
                if (entry.ColumnName.Equals(columnName))
                {
                    return entry;
                }
            }
            throw new IndexOutOfRangeException("Column: " + columnName + " does not exist in report.");
        }

        public void AddDataMapEntry(IDataMapEntry entry)
        {
            if (entries.Keys.Contains(entry.FieldName))
            {
                throw new ArgumentException("Field name: " + entry.FieldName + " already exists in ReportDatamap.");
            }
            entries.Add(entry.FieldName, entry);
        }
    }
}
