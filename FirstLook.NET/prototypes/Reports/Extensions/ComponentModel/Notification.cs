namespace Extensions.ComponentModel
{
    public enum Notification
    {
        Enabled,
        Disabled
    }
}
