using System.Xml;
using Extensions.Core.Spi;

namespace Extensions.Core
{
    public class CoreBuilder
    {
        private NamespaceHandler namespaceHandler;

        private XmlDocument document;

        public XmlDocument Document
        {
            get { return document; }
        }

        public Report Initialize(string path, NamespaceHandler handler)
        {
            namespaceHandler = handler;

            document = new XmlDocument();

            Document.Load(path);

            Report report = new Report();

            XmlNamespaceManager nsmgr = namespaceHandler(Document.NameTable);

            XmlNode xmlDescription = document.DocumentElement.SelectSingleNode("/r:Report/r:Description", nsmgr);

            if (xmlDescription != null)
            {
                report.Description = xmlDescription.InnerText;
            }

            foreach (XmlNode reportParameter in document.DocumentElement.SelectNodes("/r:Report/r:ReportParameters/r:ReportParameter", nsmgr))
            {
                ReportParameter parameter = new ReportParameter(reportParameter.Attributes["Name"].Value);

                foreach (XmlNode child in reportParameter.ChildNodes)
                {
                    if (child.NodeType.Equals(XmlNodeType.Element))
                    {
                        if (child.Name.Equals("DataType"))
                        {
                            parameter.DataType = child.InnerText;
                        }
                    }
                }

                report.Parameters.Add(parameter);
            }

            return report;
        }

        public void UpdateReportDescription(string description)
        {
            XmlNode node = document.DocumentElement.SelectSingleNode("/r:Report/r:Description", namespaceHandler(Document.NameTable));

            if (node == null)
            {
                node = document.CreateElement("Description", CoreManager.CoreNamespace);

                node.AppendChild(document.CreateTextNode(description));

                document.DocumentElement.AppendChild(node);
            }
            else
            {
                node.InnerText = description;
            }
        }

        public void InsertReportParameter(int index, string name, string dataType)
        {
            XmlNode xmlParameter = document.CreateElement("ReportParameter", CoreManager.CoreNamespace);

            XmlAttribute xmlName = document.CreateAttribute("Name");
            xmlName.Value = name;
            xmlParameter.Attributes.Append(xmlName);

            XmlNode xmlDataType = document.CreateElement("DataType", CoreManager.CoreNamespace);
            xmlDataType.AppendChild(document.CreateTextNode(dataType));
            xmlParameter.AppendChild(xmlDataType);

            XmlNode sibling = document.DocumentElement.SelectSingleNode(
                string.Format("/r:Report/r:ReportParameters/r:ReportParameter[{0}]", index),
                namespaceHandler(Document.NameTable));

            if (sibling == null)
            {
                XmlNode xmlParameters = document.CreateElement("ReportParameters", CoreManager.CoreNamespace);
                document.DocumentElement.AppendChild(xmlParameters);
                xmlParameters.AppendChild(xmlParameter);
            }
            else if (index == 0)
            {
                sibling.ParentNode.InsertBefore(xmlParameter, sibling);
            }
            else
            {
                sibling.ParentNode.InsertAfter(xmlParameter, sibling);
            }
        }

        public void RemoveReportParameter(int index)
        {
            XmlNode node = document.DocumentElement.SelectSingleNode(
                string.Format("/r:Report/r:ReportParameters/r:ReportParameter[{0}]", index),
                namespaceHandler(Document.NameTable));

            if (node != null)
            {
                node.ParentNode.RemoveChild(node);
            }
        }
    }
}
