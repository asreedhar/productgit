namespace Extensions.Core
{
    public class ReportParameter : CoreObject
    {
        private Report report;
        private string name;
        private string dataType;

        public ReportParameter(string name)
        {
            this.name = name;
        }

        public Report Report
        {
            get { return report; }
            internal set { report = value; }
        }

        public string Name
        {
            get { return name; }
            internal set { name = value; }
        }

        public string DataType
        {
            get { return dataType; }
            set { dataType = value; NotifyChanged("DataType"); }
        }
    }
}
