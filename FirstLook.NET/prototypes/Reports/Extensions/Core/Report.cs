using System.ComponentModel;

namespace Extensions.Core
{
    public class Report : CoreObject
    {
        private string description;

        private readonly BindingList<ReportParameter> parameters = new BindingList<ReportParameter>();

        public string Description
        {
            get { return description; }
            set { description = value; NotifyChanged("Description"); }
        }

        public BindingList<ReportParameter> Parameters
        {
            get { return parameters; }
        }
    }
}
