using System.Collections.Generic;
using Extensions.ComponentModel;

namespace Extensions.Core
{
    public delegate object ExtensionFactoryHandler(string provider, string extension, object obj);

    public class CoreObject : NotifyPropertyChanged
    {
        private ExtensionFactoryHandler extensionFactory;

        private readonly Dictionary<string,Dictionary<string,object>> extensions;

        public CoreObject() : base()
        {
            extensions = new Dictionary<string, Dictionary<string, object>>();
        }

        private Dictionary<string, object> GetExtensionCache(string provider)
        {
            Dictionary<string, object> extensionCache;

            if (extensions.ContainsKey(provider))
            {
                extensionCache = extensions[provider];
            }
            else
            {
                extensionCache = new Dictionary<string, object>();

                extensions[provider] = extensionCache;
            }

            return extensionCache;
        }

        public object Extend(string provider, string extension)
        {
            Dictionary<string, object> extensionCache = GetExtensionCache(provider);

            object obj;

            if (extensionCache.ContainsKey(extension))
            {
                obj = extensionCache[extension];
            }
            else
            {
                obj = extensionFactory(provider, extension, this);

                extensionCache[extension] = obj;
            }

            return obj;
        }

        public void Extend(string provider, string extension, object obj)
        {
            GetExtensionCache(provider)[extension] = obj;
        }

        internal void SetExtensionFactory(ExtensionFactoryHandler factory)
        {
            extensionFactory = factory;
        }
    }
}
