using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using Extensions.ComponentModel;
using Extensions.Core.Event;
using Extensions.Core.Spi;

namespace Extensions.Core
{
    public class CoreManager
    {
        public static readonly string CoreNamespace = "http://schemas.microsoft.com/sqlserver/reporting/2005/01/reportdefinition";

        private readonly string path;

        private CoreBuilder builder;

        private readonly Report report;

        private readonly Notification notification;

        private readonly Dictionary<string, IExtensionProvider> extensionProviders;

        public CoreManager(string path, Notification notification)
        {
            this.path = path;

            this.notification = notification;

            builder = new CoreBuilder();

            report = builder.Initialize(path, ExtensionProviderFactory.Instance.NamespaceHandler);

            extensionProviders = new Dictionary<string, IExtensionProvider>();

            foreach (XmlAttribute attribute in builder.Document.DocumentElement.Attributes)
            {
                if (attribute.Prefix.Equals("xmlns"))
                {
                    try
                    {
                        IExtensionProvider extensionProvider = ExtensionProviderFactory.Instance.GetExtensionProviderByXmlNamespaceUri(new Uri(attribute.Value));

                        extensionProvider.Initialize(report, builder.Document, notification);

                        extensionProviders[extensionProvider.Name] = extensionProvider;
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
            }

            report.SetExtensionFactory(ResolveExtension);

            if (notification == Notification.Enabled)
            {
                WatchPropertyChangeEvents();
            }
            else
            {
                DisablePropertyChangeEvents();
            }
        }

        public string Path
        {
            get { return path; }
        }

        public Report Report
        {
            get { return report; }
        }

        public void Save()
        {
            XmlWriterSettings s = new XmlWriterSettings();
            s.CloseOutput = true;
            s.Encoding = System.Text.Encoding.Default;
            s.Indent = true;
            s.IndentChars = "  ";
            s.NewLineChars = Environment.NewLine;
            s.OmitXmlDeclaration = false;

            using (XmlWriter w = XmlWriter.Create(Path, s))
            {
                builder.Document.Save(w);
            }
        }

        private object ResolveExtension(string provider, string extension, object obj)
        {
            IExtensionProvider extensionProvider;

            if (extensionProviders.ContainsKey(provider))
            {
                extensionProvider = extensionProviders[provider];
            }
            else
            {
                extensionProvider = ExtensionProviderFactory.Instance.GetExtensionProviderByName(provider);

                extensionProvider.Initialize(report, builder.Document, notification);

                extensionProviders[provider] = extensionProvider;
            }

            return extensionProvider.Extend(extension, obj);
        }

        private void DisablePropertyChangeEvents()
        {
            report.Parameters.RaiseListChangedEvents = false;

            builder = null;
        }

        private void WatchPropertyChangeEvents()
        {
            report.PropertyChanged += Report_PropertyChanged;

            report.Parameters.RaiseListChangedEvents = true;

            report.Parameters.ListChanged += Parameters_ListChanged;
        }

        private void Report_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            builder.UpdateReportDescription(report.Description);
        }

        private void Parameters_ListChanged(object sender, ListChangedEventArgs e)
        {
            // process the change

            int index = e.NewIndex;

            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                    ReportParameter parameter = report.Parameters[index];
                    builder.InsertReportParameter(index, parameter.Name, parameter.DataType);
                    break;
                case ListChangedType.ItemDeleted:
                    builder.RemoveReportParameter(index);
                    break;
            }

            // let the extensions process the change too (but after us)

            foreach (IExtensionProvider provider in extensionProviders.Values)
                if (provider is IListChangeEventHandler)
                    ((IListChangeEventHandler)provider).Parameters_ListChanged(sender, e);
        }
    }
}
