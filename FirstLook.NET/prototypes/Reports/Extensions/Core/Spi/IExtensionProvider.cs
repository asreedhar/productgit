using System;
using System.Xml;
using Extensions.ComponentModel;
using Extensions.Core;

namespace Extensions.Core.Spi
{
    public interface IExtensionProvider
    {
        Uri XmlNamespaceUri { get; }

        string XmlPrefix { get; }

        string Name { get; }

        NamespaceHandler DocumentNamespaces { get; }

        void Initialize(Uri xmlNamespaceUri, string xmlPrefix, NamespaceHandler documentNamespaces, string name);

        void Initialize(Report report, XmlDocument document, Notification notification);

        object Extend(string extension, object obj);
    }
}