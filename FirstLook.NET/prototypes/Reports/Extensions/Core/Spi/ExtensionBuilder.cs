using System.Xml;

namespace Extensions.Core.Spi
{
    public class ExtensionBuilder : IExtensionBuilder
    {
        private string xmlNamespaceUri;
        private string xmlPrefix;
        private XmlDocument document;
        private Report report;
        private NamespaceHandler documentNamespaces;

        public string XmlNamespaceUri
        {
            get { return xmlNamespaceUri; }
            set { xmlNamespaceUri = value; }
        }

        public string XmlPrefix
        {
            get { return xmlPrefix; }
            set { xmlPrefix = value; }
        }

        public XmlDocument Document
        {
            get { return document; }
            set { document = value; }
        }

        public Report Report
        {
            get { return report; }
            set { report = value; }
        }

        public NamespaceHandler DocumentNamespaces
        {
            get { return documentNamespaces; }
            set { documentNamespaces = value; }
        }

        protected XmlElement CreateElement(string elementName)
        {
            return document.CreateElement(xmlPrefix, elementName, xmlNamespaceUri);
        }

        protected void SetAttribute(XmlNode node, string attributeName, string attributeValue)
        {
            XmlAttribute target = null;

            foreach (XmlAttribute attr in node.Attributes)
            {
                if (attr.Prefix.Equals(xmlPrefix) && attr.LocalName.Equals(attributeName))
                {
                    target = attr;
                    break;
                }
            }

            if (target == null)
            {
                target = document.CreateAttribute(attributeName);

                node.Attributes.Append(target);
            }

            target.Value = attributeValue;
        }

        protected void RegisterNamespace()
        {
            bool registerNamespace = true;

            foreach (XmlAttribute xmlAttribute in document.DocumentElement.Attributes)
                if ("xmlns".Equals(xmlAttribute.Prefix) && XmlNamespaceUri.Equals(xmlAttribute.Value))
                    registerNamespace = false;

            if (registerNamespace)
            {
                XmlAttribute xmlAttribute = document.CreateAttribute("xmlns", XmlPrefix, "http://www.w3.org/2000/xmlns/");
                xmlAttribute.Value = XmlNamespaceUri;
                document.DocumentElement.Attributes.Append(xmlAttribute);
            }
        }

        protected XmlNode SelectSingleNode(string xpath)
        {
            return Document.DocumentElement.SelectSingleNode(xpath, DocumentNamespaces(Document.NameTable));
        }
    }
}
