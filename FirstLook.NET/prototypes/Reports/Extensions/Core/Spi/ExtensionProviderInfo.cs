namespace Extensions.Core.Spi
{
    internal class ExtensionProviderInfo
    {
        private readonly string xmlNamespaceUri;
        private readonly string xmlPrefix;
        private readonly string typeName;
        private readonly string name;

        public ExtensionProviderInfo(string xmlNamespaceUri, string xmlPrefix, string typeName, string name)
        {
            this.xmlNamespaceUri = xmlNamespaceUri;
            this.xmlPrefix = xmlPrefix;
            this.typeName = typeName;
            this.name = name;
        }

        public string XmlNamespaceUri
        {
            get { return xmlNamespaceUri; }
        }

        public string XmlPrefix
        {
            get { return xmlPrefix; }
        }

        public string TypeName
        {
            get { return typeName; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}