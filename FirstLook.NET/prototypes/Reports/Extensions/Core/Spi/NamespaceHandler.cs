using System.Xml;

namespace Extensions.Core.Spi
{
    /// <summary>
    /// Delegate that returns a list of the extension namespace-uris present in the document.
    /// The map-key is the namespace-uri and the value is the prefix.
    /// </summary>
    /// <returns>An map of the extension namespace-uris present in the document</returns>
    public delegate XmlNamespaceManager NamespaceHandler(XmlNameTable nameTable);
}