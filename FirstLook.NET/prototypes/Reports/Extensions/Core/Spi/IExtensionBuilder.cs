using System.Xml;

namespace Extensions.Core.Spi
{
    public interface IExtensionBuilder
    {
        string XmlNamespaceUri
        {
            get; set;
        }

        string XmlPrefix
        {
            get; set;
        }

        XmlDocument Document
        {
            get; set;
        }

        Report Report
        {
            get; set;
        }

        NamespaceHandler DocumentNamespaces
        {
            get; set;
        }
    }
}
