using System;
using System.Collections.Generic;
using System.Resources;
using System.Xml;
using Extensions.Core.Spi;

namespace Extensions.Core.Spi
{
    public class ExtensionProviderFactory
    {
        private static readonly ExtensionProviderFactory instance = new ExtensionProviderFactory();

        static ExtensionProviderFactory()
        {
            /* lazy singleton */
        }

        static public ExtensionProviderFactory Instance
        {
            get { return instance; }
        }

        private readonly List<ExtensionProviderInfo> providerList = new List<ExtensionProviderInfo>();

        private ExtensionProviderFactory()
        {
            ResourceManager rm = ExtensionProviderFactoryConfiguration.ResourceManager;

            string extensionProviders = rm.GetString("extension_providers");

            if (!string.IsNullOrEmpty(extensionProviders))
            {
                foreach (string extensionProvider in extensionProviders.Split(','))
                {
                    string xmlNamespaceUri = rm.GetString(string.Format("extension_{0}_xml_namespace", extensionProvider));
                    string xmlPrefix = rm.GetString(string.Format("extension_{0}_xml_prefix", extensionProvider));
                    string typeName = rm.GetString(string.Format("extension_{0}_type", extensionProvider));
                    string extensionName = rm.GetString(string.Format("extension_{0}_name", extensionProvider));

                    ExtensionProviderInfo provider = new ExtensionProviderInfo(
                        xmlNamespaceUri, xmlPrefix, typeName, extensionName);

                    providerList.Add(provider);
                }
            }
        }

        public IExtensionProvider GetExtensionProviderByName(string providerName)
        {
            if (string.IsNullOrEmpty(providerName))
                throw new ArgumentNullException("providerName", "IExtensionProvider names are neither null not empty");

            foreach (ExtensionProviderInfo extensionProvider in providerList)
            {
                if (extensionProvider.Name.Equals(providerName))
                {
                    return NewProvider(extensionProvider);
                }
            }

            throw new IndexOutOfRangeException(string.Format("Unhandled provider '{0}'", providerName));
        }

        public IExtensionProvider GetExtensionProviderByXmlNamespaceUri(Uri uri)
        {
            if (uri == null)
                throw new ArgumentNullException("uri", "Namespace URI are not null");

            foreach (ExtensionProviderInfo extensionProvider in providerList)
            {
                if (extensionProvider.XmlNamespaceUri.Equals(uri.ToString()))
                {
                    return NewProvider(extensionProvider);
                }
            }

            throw new IndexOutOfRangeException(string.Format("Unhandled provider uri '{0}'", uri));
        }

        public XmlNamespaceManager NamespaceHandler(XmlNameTable nameTable)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(nameTable);

            nsmgr.AddNamespace("r", "http://schemas.microsoft.com/sqlserver/reporting/2005/01/reportdefinition");

            foreach (ExtensionProviderInfo info in providerList)
            {
                nsmgr.AddNamespace(info.XmlPrefix, info.XmlNamespaceUri);
            }
            
            return nsmgr;
        }

        private IExtensionProvider NewProvider(ExtensionProviderInfo info)
        {
            IExtensionProvider provider = (IExtensionProvider)Type.GetType(info.TypeName).GetConstructor(Type.EmptyTypes).Invoke(new object[0]);
            provider.Initialize(new Uri(info.XmlNamespaceUri), info.XmlPrefix, NamespaceHandler, info.Name);
            return provider;
        }
    }
}