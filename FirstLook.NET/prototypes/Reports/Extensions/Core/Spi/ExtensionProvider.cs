using System;
using System.Xml;
using Extensions.ComponentModel;

namespace Extensions.Core.Spi
{
    public abstract class ExtensionProvider : IExtensionProvider
    {
        private Uri xmlNamespaceUri;
        private string xmlPrefix;
        private string name;
        private Notification notification;
        private NamespaceHandler documentNamespaces;

        public Uri XmlNamespaceUri
        {
            get { return xmlNamespaceUri; }
        }

        public string XmlPrefix
        {
            get { return xmlPrefix; }
        }

        public string Name
        {
            get { return name; }
        }

        public NamespaceHandler DocumentNamespaces
        {
            get { return documentNamespaces; }
        }

        public Notification Notification
        {
            get { return notification; }
            set { notification = value; }
        }

        public void Initialize(Uri xmlNamespaceUri, string xmlPrefix, NamespaceHandler documentNamespaces, string name)
        {
            this.xmlNamespaceUri = xmlNamespaceUri;
            this.xmlPrefix = xmlPrefix;
            this.documentNamespaces = documentNamespaces;
            this.name = name;
        }

        public abstract void Initialize(
            Report report,
            XmlDocument document,
            Notification notification);

        public abstract object Extend(string extension, object obj);
    }
}
