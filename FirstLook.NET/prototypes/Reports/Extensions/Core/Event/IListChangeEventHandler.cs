using System.ComponentModel;

namespace Extensions.Core.Event
{
    public interface IListChangeEventHandler
    {
        void Parameters_ListChanged(object sender, ListChangedEventArgs e);
    }
}