using System;
using System.Collections.Generic;
using System.Xml;
using Extensions.Core;
using Extensions.Core.Event;
using Extensions.Core.Spi;

namespace Extensions.Extensions.Presentation
{
    public class PresentationBuilder : ExtensionBuilder, IListChangeEventHandler
    {
        internal void Initialize()
        {
            RegisterNamespace();

            XmlNode xmlNode = SelectSingleNode("/r:Report/px:Title");

            if (xmlNode != null)
            {
                Report.Extend("Presentation", "Title", new ReportTitle(xmlNode.InnerText));
            }
        }

        public void UpdateTitle(string text)
        {
            XmlNode node = SelectSingleNode("/r:Report/px:Title");

            if (node == null)
            {
                node = CreateElement("Title");

                node.AppendChild(Document.CreateTextNode(text));

                Document.DocumentElement.AppendChild(node);
            }
            else
            {
                node.InnerText = text;
            }
        }

        void IListChangeEventHandler.Parameters_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            IList<ReportParameter> parameters = sender as IList<ReportParameter>;

            if (parameters != null)
            {
                Console.WriteLine(parameters[e.NewIndex].Name);
            }
        }
    }
}
