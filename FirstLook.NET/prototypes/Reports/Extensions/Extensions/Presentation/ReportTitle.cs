namespace Extensions.Extensions.Presentation
{
    public class ReportTitle : PresentationObject
    {
        private string text;

        public ReportTitle() { }

        public ReportTitle(string text)
        {
            this.text = text;
        }

        public string Text
        {
            get { return text; }
            set { text = value; NotifyChanged("Title"); }
        }
    }
}
