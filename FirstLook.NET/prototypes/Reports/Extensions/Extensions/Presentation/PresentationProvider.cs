using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using Extensions.ComponentModel;
using Extensions.Core;
using Extensions.Core.Event;
using Extensions.Core.Spi;

namespace Extensions.Extensions.Presentation
{
    public class PresentationProvider : ExtensionProvider, IExtensionProvider, IListChangeEventHandler
    {
        public enum ExtensionName
        {
            Title
        }

        private PresentationBuilder builder;

        private bool initialized;

        public PresentationProvider()
        {
            initialized = false;
        }

        public override void Initialize(Report report, XmlDocument document, Notification notification)
        {
            builder = new PresentationBuilder();
            builder.Report = report;
            builder.Document = document;
            builder.DocumentNamespaces = DocumentNamespaces;
            builder.XmlNamespaceUri = XmlNamespaceUri.ToString();
            builder.XmlPrefix = XmlPrefix;
            builder.Initialize();

            Notification = notification;

            initialized = true;
        }

        public override object Extend(string extension, object obj)
        {
            EnsureInitialized();

            if (string.IsNullOrEmpty(extension))
                throw new ArgumentNullException("extension", "Reporting extensions are neither null nor empty");

            if (obj == null)
                throw new ArgumentNullException("obj", "You cannot extend null references");

            ExtensionName extensionName = (ExtensionName)Enum.Parse(typeof(ExtensionName), extension);

            switch (extensionName)
            {
                case ExtensionName.Title:
                    return TitleExtension(obj);
                default:
                    throw new IndexOutOfRangeException(string.Format("Extension '{0}' not registered", extension));
            }
        }

        private void EnsureInitialized()
        {
            if (!initialized)
                throw new ApplicationException("Use of PresentationExtension before it is initialized");
        }

        private object TitleExtension(object obj)
        {
            if (obj is Report)
            {
                ReportTitle title = new ReportTitle();

                if (Notification == Notification.Enabled)
                    title.PropertyChanged += Title_PropertyChanged;

                return title;
            }

            throw new ArgumentException("Can only add a title to a report element", "obj");
        }

        private void Title_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ReportTitle title = sender as ReportTitle;

            if (title != null)
            {
                builder.UpdateTitle(title.Text);
            }
        }

        void IListChangeEventHandler.Parameters_ListChanged(object sender, ListChangedEventArgs e)
        {
            IList<ReportParameter> parameters = sender as IList<ReportParameter>;

            if (parameters != null)
            {
                Console.WriteLine(parameters[e.NewIndex].Name);
            }
        }
    }
}
