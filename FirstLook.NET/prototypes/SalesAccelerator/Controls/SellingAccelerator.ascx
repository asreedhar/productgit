<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SellingAccelerator.ascx.cs" Inherits="Controls_SellingAccelerator" %>


    <div id="accelerator">
        <div id="appraisals">
            <div id="customerInfo">
                <ul>
                    <li><label id="CustomerFirstNameLabel" for="CustomerFirstNameField" runat="server">Customer First Name</label><asp:TextBox runat="server" ID="customerFirstNameField"></asp:TextBox></li>
                    <li><label id="LastNameLabel" for="LastNameField" runat="server">Last Name</label><asp:TextBox runat="server" ID="LastNameField"></asp:TextBox></li>
                    <li><label id="PhoneLabel" for="PhoneField" runat="server">Phone</label><asp:TextBox runat="server" ID="PhoneField"></asp:TextBox></li>
                    <li><label id="EmailLabel" for="EmailField" runat="server">Email</label><asp:TextBox runat="server" ID="EmailField"></asp:TextBox></li>
                    <li><label id="SalespersonLabel" for="SalespersonField" runat="server">Salesperson</label><asp:TextBox runat="server" ID="SalespersonField"></asp:TextBox></li>
                    
                </ul>
                <asp:LinkButton ID="CustomerAppraisalLink" runat="server">Find Customer Appraisal</asp:LinkButton>
            </div>
            <div id="appraisalSearch">
                <h3>Appraisal Search</h3>
                <label id="Label1" for="LastNameField" runat="server">Last Name</label><asp:TextBox runat="server" ID="TextBox1"></asp:TextBox>
                <asp:Button ID="SearchAppraisalsButton" runat="server" Text="Search Appraisals"></asp:Button>
                <div class="closeButton"></div>    
                <asp:GridView ID="AppraisalSearchResults" runat="server"></asp:GridView>
            </div>
        </div>
        <div id="sellingSheetContent">
            <div id="photo">
                <asp:Image ID="Image1" ImageUrl="http://wrek.com/immutable/brucelime.gif" runat="server" />
                <a id="photoManagerLink">Go to Photo Manager</a>
                
            </div>
            <div id="pricingGraph">
            <h3>360&ordm; Pricing Market Analysis</h3>
            
            </div>
            <div id="sellingSheetFeatures">
                <div id="consumerHighlights">
                </div>
                <div id="selectedEquipment">
                </div>
                <div id="certifiedBenefits">
                </div>
                <asp:Button runat="server" ID="BuildSellingSheet" Text="Build Selling Sheet"></asp:Button>
            </div>
            
        </div>

    </div>