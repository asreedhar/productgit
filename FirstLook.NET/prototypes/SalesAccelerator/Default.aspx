﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Src="~/Controls/SellingAccelerator.ascx" TagPrefix="Controls" TagName="SellingAccelerator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link type="text/css" href="Resources/Accelerator.css" />
</head>
<body>
    <form id="form1" runat="server">
    <Controls:SellingAccelerator ID="SellingAccelerator" runat="server" />
    
    </form>
</body>
</html>
