using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace WebApplication.App_Code
{
    public class SampleDataSource
    {
        public string[] GetData()
        {
            return new string[] { "goodbye cruel world!" };
        }

        public DataTable GetMarketPricing()
        {
            string providerName = ConfigurationManager.ConnectionStrings["Market"].ProviderName;

            using (IDbConnection connection = DbProviderFactories.GetFactory(providerName).CreateConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["Market"].ConnectionString;
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Pricing.MarketPricing";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlCommand c = (SqlCommand) command;

                    c.Parameters.AddWithValue("OwnerHandle", "7EB3C338-B5C3-DC11-9377-0014221831B0");
                    c.Parameters.AddWithValue("VehicleHandle", "18497388");
                    c.Parameters.AddWithValue("SearchHandle", "7AB2FAFA-AFA7-421A-9260-796D311C7FFB");

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return ToDataTable(reader);
                    }
                }
            }
        }

        public static DataTable ToDataTable(IDataReader reader)
        {
            DataTable schemaTable = reader.GetSchemaTable();

            DataTable dataTable = new DataTable();

            List<string> columnNames = new List<string>();

            AddPrimaryKeyColumn(dataTable);

            for (int i = 0; i < schemaTable.Rows.Count; i++)
            {
                DataRow schemaTableRow = schemaTable.Rows[i];

                if (!dataTable.Columns.Contains(schemaTableRow["ColumnName"].ToString()))
                {
                    string columnName = schemaTableRow["ColumnName"].ToString();

                    DataColumn dataColumn = new DataColumn();
                    dataColumn.ColumnName = columnName;
                    dataColumn.Unique = Convert.ToBoolean(schemaTableRow["IsUnique"]);
                    dataColumn.AllowDBNull = Convert.ToBoolean(schemaTableRow["AllowDBNull"]);
                    dataColumn.ReadOnly = Convert.ToBoolean(schemaTableRow["IsReadOnly"]);
                    dataColumn.DataType = (Type) schemaTableRow["DataType"];
                    dataTable.Columns.Add(dataColumn);

                    columnNames.Add(columnName);
                }
            }

            while (reader.Read())
            {
                DataRow dataRow = dataTable.NewRow();

                for (int i = 0; i < columnNames.Count; i++)
                {
                    dataRow[columnNames[i]] = reader[columnNames[i]];
                }

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        internal static void AddPrimaryKeyColumn(DataTable dataTable)
        {
            DataColumn primaryKey = new DataColumn("__RowID__", Type.GetType("System.Int32"));
            primaryKey.AutoIncrement = true;
            primaryKey.AutoIncrementSeed = 1;
            primaryKey.AutoIncrementStep = 1;
            dataTable.Columns.Add(primaryKey);
            dataTable.PrimaryKey = new DataColumn[] { primaryKey };
        }
    }
}