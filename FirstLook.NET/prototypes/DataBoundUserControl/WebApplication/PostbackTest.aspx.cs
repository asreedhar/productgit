using System;
using System.Web.UI;

namespace WebApplication
{
    public partial class PostbackTest : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FindControl("ClientData1").DataBinding += ClientData1_DataBound;

            Label1.Text = "load";
        }

        protected void ClientData1_DataBound(object sender, EventArgs e)
        {
            Label1.Text = "selected";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}
