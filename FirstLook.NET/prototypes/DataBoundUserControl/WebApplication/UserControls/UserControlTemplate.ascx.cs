using System;
using System.Collections;
using System.Web.UI;
using WebControlLibrary;

namespace WebApplication.UserControls
{
    public partial class UserControlTemplate : DataBoundUserControl, IDataItemContainer
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void PerformDataBinding(IEnumerable data)
        {
            IEnumerator en = data.GetEnumerator();
            if (en.MoveNext())
                DataItem = en.Current;
            DataBindChildren();
        }

        private object dataItem = string.Empty;

        protected object DataItem
        {
            get { return dataItem; }
            set { dataItem = value; }
        }

        #region IDataItemContainer Members

        object IDataItemContainer.DataItem
        {
            get { return DataItem; }
        }

        int IDataItemContainer.DataItemIndex
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        int IDataItemContainer.DisplayIndex
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion
    }
}