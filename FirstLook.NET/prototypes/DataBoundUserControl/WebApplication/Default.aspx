<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication._Default" %>
<%@ Register Src="~/UserControls/UserControlTemplate.ascx" TagPrefix="ucx" TagName="SampleTemplate" %>
<%@ Register Assembly="WebControlLibrary" Namespace="WebControlLibrary" TagPrefix="wcl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
        function MyLoad()
        {
            var o = Sys.Application.findComponent('IdAttr');
            alert(o.get_MinMarketPrice());
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="WebApplication.App_Code.SampleDataSource" SelectMethod="GetData"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="WebApplication.App_Code.SampleDataSource" SelectMethod="GetMarketPricing"></asp:ObjectDataSource>
        <div>
            <p>User Data Bound Control</p>
            <dl>
                <dt>Hand Bound</dt>
                <dd><ucx:SampleTemplate ID="SampleTemplate1" runat="server" /></dd>
                <dt>Data Source ID</dt>
                <dd><ucx:SampleTemplate ID="SampleTemplate2" runat="server" DataSourceID="ObjectDataSource1" /></dd>
            </dl>
            <p>Client Data Control</p>
            <wcl:ClientSideData ID="ClientData1" runat="server" DataSourceID="ObjectDataSource2" />
        </div>
    </form>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(MyLoad);
    </script>
</body>
</html>
