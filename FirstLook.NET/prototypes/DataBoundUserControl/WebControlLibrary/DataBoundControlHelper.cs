using System.Web;
using System.Web.UI;

namespace WebControlLibrary
{
    internal static class DataBoundControlHelper
    {
        public static Control FindControl(Control control, string controlID)
        {
            Control namingContainer = control;
            Control control3 = null;
            if (control != control.Page)
            {
                while ((control3 == null) && (namingContainer != control.Page))
                {
                    namingContainer = namingContainer.NamingContainer;
                    if (namingContainer == null)
                    {
                        throw new HttpException(string.Format("DataBoundControlHelper_NoNamingContainer: {0} / {1}", new object[] { control.GetType().Name, control.ID }));
                    }
                    control3 = namingContainer.FindControl(controlID);
                }
                return control3;
            }
            return control.FindControl(controlID);
        }
    }
}
