using System;
using System.Collections;
using System.Web;
using System.Web.UI;

namespace WebControlLibrary
{
    public abstract class BaseDataBoundUserControl : UserControl
    {
        #region Fields
        private object _dataSource;
        private bool _inited;
        private bool _preRendered;
        private bool _requiresBindToNull;
        private bool _requiresDataBinding;
        private bool _throwOnDataPropertyChange;
        private static readonly object EventDataBound = new object(); 
        #endregion

        #region Events
        public event EventHandler DataBound
        {
            add { Events.AddHandler(EventDataBound, value); }
            remove { Events.RemoveHandler(EventDataBound, value); }
        }
        #endregion

        protected void ConfirmInitState()
        {
            _inited = true;
        }

        public override void DataBind()
        {
            if (DesignMode)
            {
                IDictionary designModeState = GetDesignModeState();
                if (((designModeState == null) || (designModeState["EnableDesignTimeDataBinding"] == null)) &&
                    (Site == null))
                {
                    return;
                }
            }
            PerformSelect();
        }

        protected virtual void EnsureDataBound()
        {
            try
            {
                _throwOnDataPropertyChange = true;
                if (RequiresDataBinding && ((DataSourceID.Length > 0) || _requiresBindToNull))
                {
                    DataBind();
                    _requiresBindToNull = false;
                }
            }
            finally
            {
                _throwOnDataPropertyChange = false;
            }
        }

        protected virtual void OnDataBound(EventArgs e)
        {
            EventHandler handler = Events[EventDataBound] as EventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnDataPropertyChanged()
        {
            if (_throwOnDataPropertyChange)
            {
                throw new HttpException(string.Format("DataBoundControl_InvalidDataPropertyChange: {0}", ID));
            }
            if (_inited)
            {
                RequiresDataBinding = true;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (Page != null)
            {
                Page.PreLoad += OnPagePreLoad;
                if (!IsViewStateEnabled && Page.IsPostBack)
                {
                    RequiresDataBinding = true;
                }
            }
        }

        protected virtual void OnPagePreLoad(object sender, EventArgs e)
        {
            _inited = true;
            if (Page != null)
            {
                Page.PreLoad -= OnPagePreLoad;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            _preRendered = true;
            EnsureDataBound();
            base.OnPreRender(e);
        }

        protected abstract void PerformSelect();
        protected abstract void ValidateDataSource(object dataSource);

        #region Properties
        public virtual object DataSource
        {
            get { return _dataSource; }
            set
            {
                if (value != null)
                {
                    ValidateDataSource(value);
                }
                _dataSource = value;
                OnDataPropertyChanged();
            }
        }

        public virtual string DataSourceID
        {
            get
            {
                object obj2 = ViewState["DataSourceID"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                if (string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(DataSourceID))
                {
                    _requiresBindToNull = true;
                }
                ViewState["DataSourceID"] = value;
                OnDataPropertyChanged();
            }
        }

        protected bool Initialized
        {
            get { return _inited; }
        }

        protected bool IsBoundUsingDataSourceID
        {
            get { return (DataSourceID.Length > 0); }
        }

        protected bool RequiresDataBinding
        {
            get { return _requiresDataBinding; }
            set
            {
                if (((value && _preRendered) && ((DataSourceID.Length > 0) && (Page != null))) && !Page.IsCallback)
                {
                    _requiresDataBinding = true;
                    EnsureDataBound();
                }
                else
                {
                    _requiresDataBinding = value;
                }
            }
        }
        #endregion
    }
}