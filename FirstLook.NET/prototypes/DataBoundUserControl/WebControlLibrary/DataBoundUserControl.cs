using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;

namespace WebControlLibrary
{
    public abstract class DataBoundUserControl : BaseDataBoundUserControl
    {
        #region Fields
        private DataSourceSelectArguments _arguments;
        private IDataSource _currentDataSource;
        private bool _currentDataSourceValid;
        private DataSourceView _currentView;
        private bool _currentViewIsFromDataSourceID;
        private bool _currentViewValid;
        private bool _ignoreDataSourceViewChanged;
        private bool _pagePreLoadFired;
        private const string DataBoundViewStateKey = "_!DataBound"; 
        #endregion

        // Methods
        protected DataBoundUserControl()
        {
        }

        private DataSourceView ConnectToDataSourceView()
        {
            if (!_currentViewValid || DesignMode)
            {
                if ((_currentView != null) && _currentViewIsFromDataSourceID)
                {
                    _currentView.DataSourceViewChanged -= OnDataSourceViewChanged;
                }
                _currentDataSource = GetDataSource();
                string dataMember = DataMember;
                if (_currentDataSource == null)
                {
                    _currentDataSource = new ReadOnlyDataSource(DataSource, dataMember);
                }
                else if (DataSource != null)
                {
                    throw new InvalidOperationException(
                        string.Format("DataControl_MultipleDataSources: {0}", new object[] {ID}));
                }
                _currentDataSourceValid = true;
                DataSourceView view = _currentDataSource.GetView(dataMember);
                if (view == null)
                {
                    throw new InvalidOperationException(
                        string.Format("DataControl_ViewNotFound: {0}", new object[] {ID}));
                }
                _currentViewIsFromDataSourceID = IsBoundUsingDataSourceID;
                _currentView = view;
                if ((_currentView != null) && _currentViewIsFromDataSourceID)
                {
                    _currentView.DataSourceViewChanged += OnDataSourceViewChanged;
                }
                _currentViewValid = true;
            }
            return _currentView;
        }

        protected virtual DataSourceSelectArguments CreateDataSourceSelectArguments()
        {
            return DataSourceSelectArguments.Empty;
        }

        protected virtual DataSourceView GetData()
        {
            return ConnectToDataSourceView();
        }

        protected virtual IDataSource GetDataSource()
        {
            if ((!DesignMode && _currentDataSourceValid) && (_currentDataSource != null))
            {
                return _currentDataSource;
            }
            IDataSource source = null;
            string dataSourceID = DataSourceID;
            if (dataSourceID.Length != 0)
            {
                Control control = DataBoundControlHelper.FindControl(this, dataSourceID);
                if (control == null)
                {
                    throw new HttpException(
                        string.Format("DataControl_DataSourceDoesntExist", new object[] {ID, dataSourceID}));
                }
                source = control as IDataSource;
                if (source == null)
                {
                    throw new HttpException(
                        string.Format("DataControl_DataSourceIDMustBeDataControl", new object[] {ID, dataSourceID}));
                }
            }
            return source;
        }

        protected void MarkAsDataBound()
        {
            ViewState[DataBoundViewStateKey] = true;
        }

        protected override void OnDataPropertyChanged()
        {
            _currentViewValid = false;
            _currentDataSourceValid = false;
            base.OnDataPropertyChanged();
        }

        protected virtual void OnDataSourceViewChanged(object sender, EventArgs e)
        {
            if (!_ignoreDataSourceViewChanged)
            {
                RequiresDataBinding = true;
            }
        }

        private void OnDataSourceViewSelectCallback(IEnumerable data)
        {
            _ignoreDataSourceViewChanged = false;
            if (DataSourceID.Length > 0)
            {
                OnDataBinding(EventArgs.Empty);
            }
            PerformDataBinding(data);
            OnDataBound(EventArgs.Empty);
        }

        protected override void OnLoad(EventArgs e)
        {
            ConfirmInitState();
            ConnectToDataSourceView();
            if (((Page != null) && !_pagePreLoadFired) && (ViewState[DataBoundViewStateKey] == null))
            {
                if (!Page.IsPostBack)
                {
                    RequiresDataBinding = true;
                }
                else if (IsViewStateEnabled)
                {
                    RequiresDataBinding = true;
                }
            }
            base.OnLoad(e);
        }

        protected override void OnPagePreLoad(object sender, EventArgs e)
        {
            base.OnPagePreLoad(sender, e);
            if (Page != null)
            {
                if (!Page.IsPostBack)
                {
                    RequiresDataBinding = true;
                }
                else if (IsViewStateEnabled && (ViewState[DataBoundViewStateKey] == null))
                {
                    RequiresDataBinding = true;
                }
            }
            _pagePreLoadFired = true;
        }

        protected virtual void PerformDataBinding(IEnumerable data)
        {
        }

        protected override void PerformSelect()
        {
            if (DataSourceID.Length == 0)
            {
                OnDataBinding(EventArgs.Empty);
            }
            DataSourceView data = GetData();
            _arguments = CreateDataSourceSelectArguments();
            _ignoreDataSourceViewChanged = true;
            RequiresDataBinding = false;
            MarkAsDataBound();
            data.Select(_arguments, OnDataSourceViewSelectCallback);
        }

        protected override void ValidateDataSource(object dataSource)
        {
            if (((dataSource != null) && !(dataSource is IListSource)) &&
                (!(dataSource is IEnumerable) && !(dataSource is IDataSource)))
            {
                throw new InvalidOperationException(string.Format("DataBoundControl_InvalidDataSourceType"));
            }
        }

        #region Properties
        public virtual string DataMember
        {
            get
            {
                object obj2 = ViewState["DataMember"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                ViewState["DataMember"] = value;
                OnDataPropertyChanged();
            }
        }

        [IDReferenceProperty(typeof(DataSourceControl))]
        public override string DataSourceID
        {
            get { return base.DataSourceID; }
            set { base.DataSourceID = value; }
        }

        protected DataSourceSelectArguments SelectArguments
        {
            get
            {
                if (_arguments == null)
                {
                    _arguments = CreateDataSourceSelectArguments();
                }
                return _arguments;
            }
        }
        #endregion
    }
}