
Type.registerNamespace('WebControlLibrary');

WebControlLibrary.ClientSideData = function (element)
{
    WebControlLibrary.ClientSideData.initializeBase(this,[element]);
    // initialize private variables
    this._VehicleEntityTypeID = null;
    this._MinMarketPrice = null;
    this._AvgMarketPrice = null;
    this._MaxMarketPrice = null;
    this._InventoryUnitCost = null;
    this._InventoryListPrice = null;
    this._MinPctMarketAvg = null;
    this._MaxPctMarketAvg = null;
    this._MinGrossProfit = null;
    this._MinPctMarketValue = null;
    this._MaxPctMarketValue = null;
    this._PctAvgMarketPrice = null;
    this._MinComparableMarketPrice = null;
    this._MaxComparableMarketPrice = null;
    this._NumListings = null;
    this._NumComparableListings = null;
    this._RankInListings = null;
    this._AppraisalValue = null;
    this._EstimatedReconditioningCost = null;
    this._SearchRadius = null;
}

WebControlLibrary.ClientSideData.prototype =
{
    initialize: function() {
        WebControlLibrary.ClientSideData.callBaseMethod(this, 'initialize');
    },
    get_VehicleEntityTypeID: function() {
        return this._VehicleEntityTypeID;
    },
    set_VehicleEntityTypeID: function(value) {
        if (this._VehicleEntityTypeID !== value) {
            this._VehicleEntityTypeID = value;
            this.raisePropertyChanged('VehicleEntityTypeID');
        }
    },
    get_MinMarketPrice: function() {
        return this._MinMarketPrice;
    },
    set_MinMarketPrice: function(value) {
        if (this._MinMarketPrice !== value) {
            this._MinMarketPrice = value;
            this.raisePropertyChanged('MinMarketPrice');
        }
    },
    get_AvgMarketPrice: function() {
        return this._AvgMarketPrice;
    },
    set_AvgMarketPrice: function(value) {
        if (this._AvgMarketPrice !== value) {
            this._AvgMarketPrice = value;
            this.raisePropertyChanged('AvgMarketPrice');
        }
    },
    get_MaxMarketPrice: function() {
        return this._MaxMarketPrice;
    },
    set_MaxMarketPrice: function(value) {
        if (this._MaxMarketPrice !== value) {
            this._MaxMarketPrice = value;
            this.raisePropertyChanged('MaxMarketPrice');
        }
    },
    get_InventoryUnitCost: function() {
        return this._InventoryUnitCost;
    },
    set_InventoryUnitCost: function(value) {
        if (this._InventoryUnitCost !== value) {
            this._InventoryUnitCost = value;
            this.raisePropertyChanged('InventoryUnitCost');
        }
    },
    get_InventoryListPrice: function() {
        return this._InventoryListPrice;
    },
    set_InventoryListPrice: function(value) {
        if (this._InventoryListPrice !== value) {
            this._InventoryListPrice = value;
            this.raisePropertyChanged('InventoryListPrice');
        }
    },
    get_MinPctMarketAvg: function() {
        return this._MinPctMarketAvg;
    },
    set_MinPctMarketAvg: function(value) {
        if (this._MinPctMarketAvg !== value) {
            this._MinPctMarketAvg = value;
            this.raisePropertyChanged('MinPctMarketAvg');
        }
    },
    get_MaxPctMarketAvg: function() {
        return this._MaxPctMarketAvg;
    },
    set_MaxPctMarketAvg: function(value) {
        if (this._MaxPctMarketAvg !== value) {
            this._MaxPctMarketAvg = value;
            this.raisePropertyChanged('MaxPctMarketAvg');
        }
    },
    get_MinGrossProfit: function() {
        return this._MinGrossProfit;
    },
    set_MinGrossProfit: function(value) {
        if (this._MinGrossProfit !== value) {
            this._MinGrossProfit = value;
            this.raisePropertyChanged('MinGrossProfit');
        }
    },
    get_MinPctMarketValue: function() {
        return this._MinPctMarketValue;
    },
    set_MinPctMarketValue: function(value) {
        if (this._MinPctMarketValue !== value) {
            this._MinPctMarketValue = value;
            this.raisePropertyChanged('MinPctMarketValue');
        }
    },
    get_MaxPctMarketValue: function() {
        return this._MaxPctMarketValue;
    },
    set_MaxPctMarketValue: function(value) {
        if (this._MaxPctMarketValue !== value) {
            this._MaxPctMarketValue = value;
            this.raisePropertyChanged('MaxPctMarketValue');
        }
    },
    get_PctAvgMarketPrice: function() {
        return this._PctAvgMarketPrice;
    },
    set_PctAvgMarketPrice: function(value) {
        if (this._PctAvgMarketPrice !== value) {
            this._PctAvgMarketPrice = value;
            this.raisePropertyChanged('PctAvgMarketPrice');
        }
    },
    get_MinComparableMarketPrice: function() {
        return this._MinComparableMarketPrice;
    },
    set_MinComparableMarketPrice: function(value) {
        if (this._MinComparableMarketPrice !== value) {
            this._MinComparableMarketPrice = value;
            this.raisePropertyChanged('MinComparableMarketPrice');
        }
    },
    get_MaxComparableMarketPrice: function() {
        return this._MaxComparableMarketPrice;
    },
    set_MaxComparableMarketPrice: function(value) {
        if (this._MaxComparableMarketPrice !== value) {
            this._MaxComparableMarketPrice = value;
            this.raisePropertyChanged('MaxComparableMarketPrice');
        }
    },
    get_NumListings: function() {
        return this._NumListings;
    },
    set_NumListings: function(value) {
        if (this._NumListings !== value) {
            this._NumListings = value;
            this.raisePropertyChanged('NumListings');
        }
    },
    get_NumComparableListings: function() {
        return this._NumComparableListings;
    },
    set_NumComparableListings: function(value) {
        if (this._NumComparableListings !== value) {
            this._NumComparableListings = value;
            this.raisePropertyChanged('NumComparableListings');
        }
    },
    get_RankInListings: function() {
        return this._RankInListings;
    },
    set_RankInListings: function(value) {
        if (this._RankInListings !== value) {
            this._RankInListings = value;
            this.raisePropertyChanged('RankInListings');
        }
    },
    get_AppraisalValue: function() {
        return this._AppraisalValue;
    },
    set_AppraisalValue: function(value) {
        if (this._AppraisalValue !== value) {
            this._AppraisalValue = value;
            this.raisePropertyChanged('AppraisalValue');
        }
    },
    get_EstimatedReconditioningCost: function() {
        return this._EstimatedReconditioningCost;
    },
    set_EstimatedReconditioningCost: function(value) {
        if (this._EstimatedReconditioningCost !== value) {
            this._EstimatedReconditioningCost = value;
            this.raisePropertyChanged('EstimatedReconditioningCost');
        }
    },
    get_SearchRadius: function() {
        return this._SearchRadius;
    },
    set_SearchRadius: function(value) {
        if (this._SearchRadius !== value) {
            this._SearchRadius = value;
            this.raisePropertyChanged('SearchRadius');
        }
    }
}

WebControlLibrary.ClientSideData.registerClass('WebControlLibrary.ClientSideData', Sys.Component);

Sys.Application.notifyScriptLoaded();
