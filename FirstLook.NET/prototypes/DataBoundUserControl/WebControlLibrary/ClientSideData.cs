using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("WebControlLibrary.ClientSideData.js", "text/javascript")]

namespace WebControlLibrary
{
    public class ClientSideData : DataBoundControl, IScriptControl
    {
        #region Page Life Cycle
        
        private ScriptManager _scriptManager = null;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!DesignMode)
            {
                _scriptManager = ScriptManager.GetCurrent(Page);
                if (_scriptManager != null)
                    _scriptManager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!DesignMode)
                _scriptManager.RegisterScriptDescriptors(this);
            // no need to render an element as there is no markup for this control
        }

        #endregion

        #region Properties

        private T GetViewState<T>(string key)
        {
            object obj = ViewState[key];
            if (obj == null)
                return default(T);
            else
                return (T) obj;
        }

        private void SetViewState<T>(string key, Nullable<T> value) where T : struct
        {
            if (value.HasValue)
                ViewState[key] = value.Value;
        }

        public int VehicleEntityTypeID
        {
            get { return GetViewState<int>("VehicleEntityTypeID"); }
            set { SetViewState<int>("VehicleEntityTypeID", value); }
        }

        public int? MinMarketPrice
        {
            get { return GetViewState<int>("MinMarketPrice"); }
            set { SetViewState("MinMarketPrice", value); }
        }

        public int? AvgMarketPrice
        {
            get { return GetViewState<int>("AvgMarketPrice"); }
            set { SetViewState("AvgMarketPrice", value); }
        }

        public int? MaxMarketPrice
        {
            get { return GetViewState<int>("MaxMarketPrice"); }
            set { SetViewState("MaxMarketPrice", value); }
        }

        public int? InventoryUnitCost
        {
            get { return GetViewState<int>("InventoryUnitCost"); }
            set { SetViewState("InventoryUnitCost", value); }
        }

        public int? InventoryListPrice
        {
            get { return GetViewState<int>("InventoryListPrice"); }
            set { SetViewState("InventoryListPrice", value); }
        }

        public int? MinPctMarketAvg
        {
            get { return GetViewState<int>("MinPctMarketAvg"); }
            set { SetViewState("MinPctMarketAvg", value); }
        }

        public int? MaxPctMarketAvg
        {
            get { return GetViewState<int>("MaxPctMarketAvg"); }
            set { SetViewState("MaxPctMarketAvg", value); }
        }

        public int? MinGrossProfit
        {
            get { return GetViewState<int>("MinGrossProfit"); }
            set { SetViewState("MinGrossProfit", value); }
        }

        public int? MinPctMarketValue
        {
            get { return GetViewState<int>("MinPctMarketValue"); }
            set { SetViewState("MinPctMarketValue", value); }
        }

        public int? MaxPctMarketValue
        {
            get { return GetViewState<int>("MaxPctMarketValue"); }
            set { SetViewState("MaxPctMarketValue", value); }
        }

        public int? PctAvgMarketPrice
        {
            get { return GetViewState<int>("PctAvgMarketPrice"); }
            set { SetViewState("PctAvgMarketPrice", value); }
        }

        public int? MinComparableMarketPrice
        {
            get { return GetViewState<int>("MinComparableMarketPrice"); }
            set { SetViewState("MinComparableMarketPrice", value); }
        }

        public int? MaxComparableMarketPrice
        {
            get { return GetViewState<int>("MaxComparableMarketPrice"); }
            set { SetViewState("MaxComparableMarketPrice", value); }
        }

        public int NumListings
        {
            get { return GetViewState<int>("NumListings"); }
            set { SetViewState<int>("NumListings", value); }
        }

        public int NumComparableListings
        {
            get { return GetViewState<int>("NumComparableListings"); }
            set { SetViewState<int>("NumComparableListings", value); }
        }

        public int? RankInListings
        {
            get { return GetViewState<int>("RankInListings"); }
            set { SetViewState("RankInListings", value); }
        }

        public int? AppraisalValue
        {
            get { return GetViewState<int>("AppraisalValue"); }
            set { SetViewState("AppraisalValue", value); }
        }

        public int? EstimatedReconditioningCost
        {
            get { return GetViewState<int>("EstimatedReconditioningCost"); }
            set { SetViewState("EstimatedReconditioningCost", value); }
        }

        public int? SearchRadius
        {
            get { return GetViewState<int>("SearchRadius"); }
            set { SetViewState("SearchRadius", value); }
        }

        #endregion

        #region Data Binding
        
        protected override void PerformDataBinding(IEnumerable data)
        {
            IEnumerator en = data.GetEnumerator();
            if (en.MoveNext())
            {
                object item = en.Current;

                VehicleEntityTypeID = (byte) DataBinder.Eval(item, "VehicleEntityTypeID");
                MinMarketPrice = GetInt(item, "MinMarketPrice");
                AvgMarketPrice = GetInt(item, "AvgMarketPrice");
                MaxMarketPrice = GetInt(item, "MaxMarketPrice");
                InventoryUnitCost = GetInt(item, "InventoryUnitCost");
                InventoryListPrice = GetInt(item, "InventoryListPrice");
                MinPctMarketAvg = GetInt(item, "MinPctMarketAvg");
                MaxPctMarketAvg = GetInt(item, "MaxPctMarketAvg");
                MinGrossProfit = GetInt(item, "MinGrossProfit");
                MinPctMarketValue = GetInt(item, "MinPctMarketValue");
                MaxPctMarketValue = GetInt(item, "MaxPctMarketValue");
                PctAvgMarketPrice = GetInt(item, "PctAvgMarketPrice");
                MinComparableMarketPrice = GetInt(item, "MinComparableMarketPrice");
                MaxComparableMarketPrice = GetInt(item, "MaxComparableMarketPrice");
                NumListings = (int)DataBinder.Eval(item, "NumListings");
                NumComparableListings = (int)DataBinder.Eval(item, "NumComparableListings");
                RankInListings = GetInt(item, "RankInListings");
                AppraisalValue = GetInt(item, "AppraisalValue");
                EstimatedReconditioningCost = GetInt(item, "EstimatedReconditioningCost");
                SearchRadius = GetInt(item, "SearchRadius");
            }
        }

        private static int? GetInt(object item, string property)
        {
            object o = DataBinder.Eval(item, property);
            if (o == null || o.Equals(DBNull.Value))
                return null;
            else
                return Convert.ToInt32(o);
        }

        #endregion

        #region IScriptControl Members

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptComponentDescriptor descriptor = new ScriptComponentDescriptor("WebControlLibrary.ClientSideData");
            AddProperty(descriptor, "VehicleEntityTypeID", VehicleEntityTypeID);
            AddProperty(descriptor, "MinMarketPrice", MinMarketPrice);
            AddProperty(descriptor, "AvgMarketPrice", AvgMarketPrice);
            AddProperty(descriptor, "MaxMarketPrice", MaxMarketPrice);
            AddProperty(descriptor, "InventoryUnitCost", InventoryUnitCost);
            AddProperty(descriptor, "InventoryListPrice", InventoryListPrice);
            AddProperty(descriptor, "MinPctMarketAvg", MinPctMarketAvg);
            AddProperty(descriptor, "MaxPctMarketAvg", MaxPctMarketAvg);
            AddProperty(descriptor, "MinGrossProfit", MinGrossProfit);
            AddProperty(descriptor, "MinPctMarketValue", MinPctMarketValue);
            AddProperty(descriptor, "MaxPctMarketValue", MaxPctMarketValue);
            AddProperty(descriptor, "PctAvgMarketPrice", PctAvgMarketPrice);
            AddProperty(descriptor, "MinComparableMarketPrice", MinComparableMarketPrice);
            AddProperty(descriptor, "MaxComparableMarketPrice", MaxComparableMarketPrice);
            AddProperty(descriptor, "NumListings", NumListings);
            AddProperty(descriptor, "NumComparableListings", NumComparableListings);
            AddProperty(descriptor, "RankInListings", RankInListings);
            AddProperty(descriptor, "AppraisalValue", AppraisalValue);
            AddProperty(descriptor, "EstimatedReconditioningCost", EstimatedReconditioningCost);
            AddProperty(descriptor, "SearchRadius", SearchRadius);
            descriptor.ID = "IdAttr";
            return new ScriptDescriptor[] { descriptor };
        }

        private static void AddProperty(ScriptComponentDescriptor descriptor, string property, object value)
        {
            if (value != null)
                descriptor.AddProperty(property, value);
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference reference = new ScriptReference();
            reference.Assembly = "WebControlLibrary";
            reference.Name = "WebControlLibrary.ClientSideData.js";
            return new ScriptReference[] { reference };
        }

        #endregion
    }
}
