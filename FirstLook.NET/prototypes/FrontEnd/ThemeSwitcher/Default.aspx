﻿<%@ page language="C#" autoeventwireup="true" inherits="_Default, ThemeSwitcher_Deploy" masterpagefile="~/Master_Pages/BasePage.master" theme="Flatgrey" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <title><asp:Literal id="PageTitle" runat="server">Appraisal Closing Rate</asp:Literal></title>
</asp:Content>



<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
        
        <div class="prince_top_right_ print_only_">
        <span id="ctl00_ctl00_Header_Placeholder_CurrentDateTime" class="right">Printed: 2/14/2008 2:37 PM</span><br />
        <span id="ctl00_ctl00_Header_Placeholder_CurrentUser" class="right">Printed by: FirstLook Admin</span>
        </div>

        
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <span class="buttonwrapper right hide_nonJS dyna_link">

<asp:HyperLink runat="server" id="changeTheme" OnLoad="ChangeTheme_Load" CssClass="button">Change Theme</asp:HyperLink>
 </span>
<span class="buttonwrapper right hide_nonJS dyna_link">
    <a id="ctl00_ctl00_Header_Placeholder_Page_Header_EmailClient_EmailButtonOpen" class="button" href="javascript:__doPostBack('ctl00$ctl00$Header_Placeholder$Page_Header$EmailClient$EmailButtonOpen','')">Email</a>
</span>




        <h2>Hendrick Automotive Group</h2>
        <h1>Appraisal Closing Rates</h1>
    </div>


        
        

    
    <!-- appraisal time period -->
    
    
    
    <span id="DrilldownTimeSpan" class="dyna_link">
        <span id="ctl00_ctl00_Body_A_PlaceHolder_Page_Body_AppraisalClosingRate_TimePeriodLabel">Average previous 4 weeks ending: 1/30/2008</span>
        <img id="ctl00_ctl00_Body_A_PlaceHolder_Page_Body_Image1" class="hide_nonJS" src="App_Themes/Flatgrey/Images/menu-icon.gif" alt="Click to open" style="border-width:0px;" />
    </span>
    <div id="selectDrilldownTimeRange" class="inlinepopup selectbox" style="display:none;">
        <div id="closeDrilldownTimeRange" class="closebox right"></div>
        <h3>Time Period for Appraisal Closing Rate:</h3>
        <select name="ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate_TimePeriod" onchange="javascript:setTimeout('__doPostBack(\'ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate_TimePeriod\',\'\')', 0)" id="ctl00_ctl00_Body_A_PlaceHolder_Page_Body_AppraisalClosingRate_TimePeriod">
	<option selected="selected" value="4weeks">Average previous 4 weeks</option>
	<option value="8weeks">Average previous 8 weeks</option>
	<option value="13weeks">Average previous 13 weeks</option>
	<option value="26weeks">Average previous 26 weeks</option>
	<option value="avgYTD">Average Year To Date</option>
	<option value="curMonth">Current Month</option>
	<option value="avgPriorMonth">Average prior month</option>
	<option value="avgPrior3Month">Average prior 3 months</option>

</select>
    </div>
    

    
    <!-- appraisal report -->
    
    
    
    
	<table class="popuptable">
		<thead>
			<tr>
				<th scope="col"><a href="javascript:__doPostBack('ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate','Sort$ClosingRateRank')">In-Group Rank</a></th>
				<th scope="col"><a href="javascript:__doPostBack('ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate','Sort$Dealer')">Dealership</a></th>
				<th scope="col"><a href="javascript:__doPostBack('ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate','Sort$ClosingRate')">Appraisal Closing Rate / Target</a></th>
				<th scope="col"><a href="javascript:__doPostBack('ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate','Sort$TradeInInventoryAnalyzed')">Trade Analyzer Usage</a></th>
				<th scope="col"><a href="javascript:__doPostBack('ctl00$ctl00$Body_A_PlaceHolder$Page_Body$AppraisalClosingRate','Sort$AvgImmediateWholesaleProfitInPeriod')">Avg. Imm. Wholesale Profit/Loss</a></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td class=" first">-</td>
				<td>Whole group</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a onclick="return false;" href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode="><span  class="percentbar " style="width: 21.2337067787096px" ></span></a></li>
                                <li class="percentlegend"><a onclick="return false;" href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=">15%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>54%</td>
				<td class=" last">$249</td>
			</tr>
		</tfoot>
		<tbody>
			<tr>
				<td class=" first">1</td>
				<td>Honda Cars of McKinney</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102008"><span  class="percentbar dyna_link" style="width: 80px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102008">57%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>8%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=102008">($831)</a></td>
			</tr>
			<tr>
				<td class=" first">2</td>
				<td>Darrell Waltrip Honda</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101153"><span  class="percentbar dyna_link" style="width: 36.0479041916168px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101153">26%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>79%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101153">($52)</a></td>
			</tr>
			<tr>
				<td class=" first">3</td>
				<td>Honda Cars of Bradenton</td>
				<td>
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101201"><span  class="percentbar dyna_link" style="width: 34.841628959276px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101201">25%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>77%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101201">$292</a></td>
			</tr>
			<tr>
				<td class=" first">4</td>
				<td>Superior Buick Cadillac</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100941"><span  class="percentbar dyna_link" style="width: 34.3859649122807px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100941">25%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>75%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100941">($8)</a></td>
			</tr>
			<tr>
				<td class=" first">5</td>
				<td>Pleasanton Automall</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102130"><span  class="percentbar dyna_link" style="width: 32.5405405405405px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102130">23%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>93%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=102130">$402</a></td>
			</tr>
			<tr>
				<td class=" first">6</td>
				<td>Honda of Concord</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100975"><span  class="percentbar dyna_link" style="width: 32.1739130434783px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100975">23%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>78%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100975">$80</a></td>
			</tr>
			<tr>
				<td class=" first">7</td>
				<td>Hendrick Chrysler Jeep</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101006"><span  class="percentbar dyna_link" style="width: 30px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101006">21%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>78%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101006">$973</a></td>
			</tr>
			<tr>
				<td class=" first">8</td>
				<td>Honda of El Cerrito</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100804"><span  class="percentbar dyna_link" style="width: 28.8549618320611px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100804">21%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>74%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100804">($119)</a></td>
			</tr>
			<tr>
				<td class=" first">9</td>
				<td>Hendrick Honda</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101239"><span  class="percentbar dyna_link" style="width: 28.498727735369px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101239">20%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>48%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101239">$332</a></td>
			</tr>
			<tr>
				<td class=" first">10</td>
				<td>Superior Chevrolet</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101766"><span  class="percentbar dyna_link" style="width: 27.7358490566038px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101766">20%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>33%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101766">$472</a></td>
			</tr>
			<tr>
				<td class=" first">11</td>
				<td>Hendrick City Chevrolet</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100147"><span  class="percentbar dyna_link" style="width: 26.7555555555556px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100147">19%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>66%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100147">$344</a></td>
			</tr>
			<tr>
				<td class=" first">12</td>
				<td>Toyota Scion of Concord</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101645"><span  class="percentbar dyna_link" style="width: 25.2px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101645">18%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>46%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101645">$178</a></td>
			</tr>
			<tr>
				<td class=" first">13</td>
				<td>Rick Hendrick Jeep Chrysler</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101203"><span  class="percentbar dyna_link" style="width: 25.1685393258427px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101203">18%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>61%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101203">$372</a></td>
			</tr>
			<tr>
				<td class=" first">14</td>
				<td>Honda Cars of Rock Hill</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102273"><span  class="percentbar dyna_link" style="width: 24.4036697247706px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102273">17%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>38%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=102273">$45</a></td>
			</tr>
			<tr>
				<td class=" first">15</td>
				<td>Rick Hendrick Toyota</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100239"><span  class="percentbar dyna_link" style="width: 23.9887640449438px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100239">17%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>67%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100239">$466</a></td>
			</tr>
			<tr>
				<td class=" first">16</td>
				<td>Hendrick Pontiac Buick GMC</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100845"><span  class="percentbar dyna_link" style="width: 23.972602739726px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100845">17%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>53%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100845">$382</a></td>
			</tr>
			<tr>
				<td class=" first">17</td>
				<td>Land Rover Charlotte</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102007"><span  class="percentbar dyna_link" style="width: 23.3333333333333px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102007">17%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>44%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=102007">$377</a></td>
			</tr>
			<tr>
				<td class=" first">18</td>
				<td>Toyota of Wilmington</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101634"><span  class="percentbar dyna_link" style="width: 22.9181494661922px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101634">16%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>76%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101634">$567</a></td>
			</tr>
			<tr>
				<td class=" first">19</td>
				<td>Hendrick Honda  of Woodbridge</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101010"><span  class="percentbar dyna_link" style="width: 21.2295081967213px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101010">15%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>82%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101010">$580</a></td>
			</tr>
			<tr>
				<td class=" first">20</td>
				<td>Colonial Chevrolet</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101108"><span  class="percentbar dyna_link" style="width: 20.4615384615385px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101108">15%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>72%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101108">$247</a></td>
			</tr>
			<tr>
				<td class=" first">21</td>
				<td>Hendrick Chevrolet Cadillac</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101855"><span  class="percentbar dyna_link" style="width: 17.5px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101855">13%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>71%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101855">($12)</a></td>
			</tr>
			<tr>
				<td class=" first">22</td>
				<td>Hendrick Honda of Charleston</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100246"><span  class="percentbar dyna_link" style="width: 17.2463768115942px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100246">12%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>71%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100246">$471</a></td>
			</tr>
			<tr>
				<td class=" first">23</td>
				<td>Hendrick Lexus</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101929"><span  class="percentbar dyna_link" style="width: 17.051282051282px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101929">12%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>37%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101929">$0</a></td>
			</tr>
			<tr>
				<td class=" first">24</td>
				<td>Hendrick Dodge - Cary Auto Mall</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100855"><span  class="percentbar dyna_link" style="width: 16.4705882352941px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100855">12%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>38%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100855">($490)</a></td>
			</tr>
			<tr>
				<td class=" first">25</td>
				<td>Superior Toyota</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101767"><span  class="percentbar dyna_link" style="width: 16.2790697674419px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101767">12%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>22%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101767">$431</a></td>
			</tr>
			<tr>
				<td class=" first">26</td>
				<td>Hendrick Cadillac Hummer -Cary Auto Mall</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100857"><span  class="percentbar dyna_link" style="width: 15.7142857142857px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100857">11%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>54%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100857">($181)</a></td>
			</tr>
			<tr>
				<td class=" first">27</td>
				<td>Hendrick Porsche</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101735"><span  class="percentbar dyna_link" style="width: 15.5555555555556px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101735">11%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>67%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101735">$100</a></td>
			</tr>
			<tr>
				<td class=" first">28</td>
				<td>Hendrick BMW/MINI</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100148"><span  class="percentbar dyna_link" style="width: 14.9466192170818px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100148">11%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>58%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100148">$200</a></td>
			</tr>
			<tr>
				<td class=" first">29</td>
				<td>Rick Hendrick Chevrolet Charleston</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101163"><span  class="percentbar dyna_link" style="width: 14.7738693467337px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101163">11%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>57%</td>
				<td class=" negativenumber last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101163">($165)</a></td>
			</tr>
			<tr>
				<td class=" first">30</td>
				<td>Rick Hendrick Chevrolet Durham</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100650"><span  class="percentbar dyna_link" style="width: 14.4117647058823px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100650">10%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>64%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100650">$601</a></td>
			</tr>
			<tr>
				<td class=" first">31</td>
				<td>Gwinnett Place Honda</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100805"><span  class="percentbar dyna_link" style="width: 14.2145593869732px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100805">10%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>49%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100805">$317</a></td>
			</tr>
			<tr>
				<td class=" first">32</td>
				<td>Terry Labonte Chevrolet</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100238"><span  class="percentbar dyna_link" style="width: 14.1891891891892px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100238">10%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>44%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100238">$321</a></td>
			</tr>
			<tr>
				<td class=" first">33</td>
				<td>Hendrick Chevrolet - Cary Auto Mall</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100854"><span  class="percentbar dyna_link" style="width: 13.0935251798561px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100854">9%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>67%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100854">$282</a></td>
			</tr>
			<tr>
				<td class=" first">34</td>
				<td>Superior Volvo</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101119"><span  class="percentbar dyna_link" style="width: 13.0666666666667px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101119">9%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>54%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101119">$434</a></td>
			</tr>
			<tr>
				<td class=" first">35</td>
				<td>Rick Hendrick Imports</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100590"><span  class="percentbar dyna_link" style="width: 12.6315789473684px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100590">9%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>67%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100590">$372</a></td>
			</tr>
			<tr>
				<td class=" first">36</td>
				<td>Performance Automall - Hendrick</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101005"><span  class="percentbar dyna_link" style="width: 10.9375px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101005">8%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>58%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101005">$388</a></td>
			</tr>
			<tr>
				<td class=" first">37</td>
				<td>Gwinnett Place Chevrolet</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100259"><span  class="percentbar dyna_link" style="width: 10.7006369426752px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=100259">8%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>39%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=100259">$481</a></td>
			</tr>
			<tr>
				<td class=" first">38</td>
				<td>Hendrick Acura</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101434"><span  class="percentbar dyna_link" style="width: 9.625px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101434">7%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>73%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101434">$141</a></td>
			</tr>
			<tr>
				<td class=" first">39</td>
				<td>Hendrick Motors - Hickory</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101138"><span  class="percentbar dyna_link" style="width: 6.42201834862385px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101138">5%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>57%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101138">$816</a></td>
			</tr>
			<tr>
				<td class=" first">40</td>
				<td>Kearny Mesa Chevrolet</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101439"><span  class="percentbar dyna_link" style="width: 3.24074074074074px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101439">2%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>92%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101439">$678</a></td>
			</tr>
			<tr>
				<td class=" first">41</td>
				<td>Acura of Concord</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101896"><span  class="percentbar dyna_link" style="width: 0px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101896"></a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>0%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101896">$211</a></td>
			</tr>
			<tr>
				<td class=" first">42</td>
				<td>East Bay BMW</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102302"><span  class="percentbar dyna_link" style="width: 0px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102302"></a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>&nbsp;</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=102302">$0</a></td>
			</tr>
			<tr>
				<td class=" first">43</td>
				<td>Saturn of Riverside</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101126"><span  class="percentbar dyna_link" style="width: 0px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101126"></a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>2%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101126">$0</a></td>
			</tr>
			<tr>
				<td class=" first">44</td>
				<td>Superior Acura</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102281"><span  class="percentbar dyna_link" style="width: 0px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=102281">0%</a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>0%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=102281">$362</a></td>
			</tr>
			<tr>
				<td class=" first">45</td>
				<td>Superior Lexus North</td>
				<td>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: 84px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101296"><span  class="percentbar dyna_link" style="width: 0px" ></span></a></li>
                                <li class="percentlegend"><a href="Reports/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=101296"></a></li>
                                </ul>
                            </li>
                        </ul>
                </td>
				<td>3%</td>
				<td class=" last"><a href="Reports/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=101296">$339</a></td>
			</tr>
		</tbody>
	</table>




        
        
        
        
        
        
        
    
        <div class="prince_bottom_left_ print_only_">
            <span id="ctl00_ctl00_Footer_PlaceHolder_CopyrightNotice">Copyright &copy; 2008 First Look, LLC. All Rights Reserved</span>
        </div>

        <div class="prince_bottom_right_ print_only_">
            <img class="print_image_" src="App_Themes/Flatgrey/Images/logo-firstlook-hires.png" style="border-width:0px;" />
        </div>

    
</asp:Content>