<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Fancy.aspx.cs" Inherits="Fancy" %>
<%@ Register Src="~/Controls/Email.ascx" TagName="Email" TagPrefix="pim" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="Javascripts/jquery.ui/1.5b/themes/flora/flora.all.css" type="text/css" media="screen" title="Flora (Default)" />
    <script src="Javascripts/jquery.ui/1.5b/jquery-1.2.3.min.js" type="text/javascript"></script>
    <script src="Javascripts/jquery.ui/1.5b/jquery.dimensions.js" type="text/javascript"></script>
    <script src="Javascripts/jquery.ui/1.5b/ui.dialog.js" type="text/javascript"></script>
    <script src="Javascripts/jquery.ui/1.5b/ui.mouse.js" type="text/javascript"></script>
    <script src="Javascripts/jquery.ui/1.5b/ui.resizable.js" type="text/javascript"></script>
    <script src="Javascripts/jquery.ui/1.5b/ui.draggable.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Prototype Email Client</h1>
        <h2>Disclaimer</h2>
        <ul>
            <li>This is a <em>sketch</em>.</li>
            <li>As such it is not complete.</li>
            <li>Neither is it guaranteed to work completely or correctly.</li>
            <li>It is ugly and orange. <small>(Simon cannot choose colors or make things pretty.)</small></li>
        </ul>
        <h2>Prototype</h2>
        <blockquote>Software usability resides in the total organization of the user interface, not in how features
        look so much as in how the features work and how they work together to make the user's job easier.</blockquote>
        <cite>Larry L. Constantine, Constantine on Peopleware, 1995.</cite>
        <p>Click the link button to open the prototype email client.</p>
        <asp:LinkButton ID="OpenEmailButton" runat="server" OnClick="OpenEmailButton_Click">Email</asp:LinkButton>
        <pim:Email ID="Email" runat="server" />
    </div>
    </form>
</body>
</html>
