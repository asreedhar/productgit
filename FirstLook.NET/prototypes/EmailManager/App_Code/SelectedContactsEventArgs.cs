using System;
using System.Collections.Generic;

namespace App_Code
{
    public class SelectedContactsEventArgs : EventArgs
    {
        private readonly IEnumerable<Dictionary<string, string>> values;

        public SelectedContactsEventArgs(IEnumerable<Dictionary<string, string>> values)
        {
            this.values = values;
        }

        public IEnumerable<Dictionary<string, string>> Values
        {
            get { return values; }
        }
    }
}