﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Simple.aspx.cs" Inherits="Simple" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add/Manage Email Addresses</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <fieldset>
            
            <legend>Choose a List</legend>
            
            <asp:SqlDataSource ID="EmailListDropDownListDataSource" runat="server"
                SelectCommand="SELECT ID, Name FROM EmailList ORDER BY Name"
                SelectCommandType="Text"
                ConnectionString="<%$ ConnectionStrings:SampleDB %>">
            </asp:SqlDataSource>
            
            <asp:Panel ID="EmptyEmailListPanel" runat="server" Visible="false">
                <p>There are currently no email lists</p>
            </asp:Panel>
            
            <asp:Panel ID="EmailListSelectionPanel" runat="server">
                <asp:Label ID="EmailListDropDownListLabel" runat="server" Text="Email List:" AssociatedControlID="EmailListDropDownList"></asp:Label>
                <asp:DropDownList ID="EmailListDropDownList" runat="server"
                        DataSourceID="EmailListDropDownListDataSource"
                        DataTextField="Name"
                        DataValueField="ID"
                        AutoPostBack="true"
                        OnDataBound="EmailListDropDownList_DataBound"
                        OnSelectedIndexChanged="EmailListDropDownList_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ImageButton ID="SelectEmailList" runat="server" AlternateText="Select List" OnClick="SelectEmailList_Click" ImageUrl="~/Images/128x128/arrow-right.png" Width="16" Height="16" />
            </asp:Panel>
        
        </fieldset>
        
        <fieldset>
            
            <legend>List Editor</legend>
            
            <asp:SqlDataSource ID="EmailListFormViewDataSource" runat="server"
                SelectCommand="SELECT ID, Name FROM EmailList WHERE ID = @ID"
                SelectCommandType="Text"
                InsertCommand="INSERT INTO EmailList (Name) VALUES (@Name)"
                InsertCommandType="Text"
                UpdateCommand="UPDATE EmailList SET Name = @Name WHERE ID = @ID"
                UpdateCommandType="Text"
                DeleteCommand="DELETE FROM Email WHERE EmailListID = @ID; DELETE FROM EmailList WHERE ID = @ID;"
                DeleteCommandType="Text"
                ConnectionString="<%$ ConnectionStrings:SampleDB %>">
                <SelectParameters>
                    <asp:ControlParameter ControlID="EmailListDropDownList" Name="ID" Type="Int32" />
                </SelectParameters>
                <InsertParameters>
                    <asp:Parameter Name="Name" Type="String" ConvertEmptyStringToNull="true" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                    <asp:Parameter Name="Name" Type="String" ConvertEmptyStringToNull="true" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
            </asp:SqlDataSource>
            
            <asp:DetailsView ID="EmailListFormView" runat="server" DataKeyNames="ID" AutoGenerateRows="false"
                    DataSourceID="EmailListFormViewDataSource"
                    OnItemInserted="EmailListFormView_Inserted"
                    OnItemDeleted="EmailListFormView_Deleted"
                    OnItemUpdated="EmailListFormView_Updated"
                    OnModeChanging="EmailListFormView_ModeChanging">
                <Fields>
                    <asp:BoundField AccessibleHeaderText="List Name" HeaderText="List Name" DataField="Name" />
                    <asp:CommandField
                        ButtonType="Image"
                        ControlStyle-Width="16"
                        ControlStyle-Height="16"
                        CancelImageUrl="~/Images/128x128/cancel.png"
                        EditImageUrl="~/Images/128x128/edit.png"
                        DeleteImageUrl="~/Images/128x128/edit-rem.png"
                        UpdateImageUrl="~/Images/128x128/save.png"
                        NewImageUrl="~/Images/128x128/edit-add.png"
                        InsertImageUrl="~/Images/128x128/save.png"
                        ShowCancelButton="true"
                        ShowDeleteButton="true"
                        ShowEditButton="true"
                        ShowInsertButton="true" />
                </Fields>
                <CommandRowStyle HorizontalAlign="Right" />
            </asp:DetailsView>
            
        </fieldset>
        
        <asp:Panel ID="EmailPanel" runat="server">
        
            <fieldset>
                
                <legend>List Recipients</legend>
                
                <asp:SqlDataSource ID="EmailGridViewDataSource" runat="server"
                    SelectCommand="SELECT ID, FirstName, LastName, Email FROM Email WHERE EmailListID = @EmailListID"
                    SelectCommandType="Text"
                    UpdateCommand="UPDATE Email SET FirstName = @FirstName, LastName = @LastName, Email = @Email WHERE ID = @ID"
                    UpdateCommandType="Text"
                    DeleteCommand="DELETE FROM Email WHERE ID = @ID"
                    DeleteCommandType="Text"
                    ConnectionString="<%$ ConnectionStrings:SampleDB %>">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="EmailListDropDownList" Name="EmailListID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="ID" Type="Int32" />
                        <asp:Parameter Name="FirstName" Type="String" ConvertEmptyStringToNull="true" />
                        <asp:Parameter Name="LastName" Type="String" ConvertEmptyStringToNull="true" />
                        <asp:Parameter Name="Email" Type="String" ConvertEmptyStringToNull="true" />
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="ID" Type="Int32" />
                    </DeleteParameters>
                </asp:SqlDataSource>
                
                <asp:Panel ID="EmptyEmailGridViewPanel" runat="server" Visible="false">
                    <p>There are currently no email addresses in this list</p>
                </asp:Panel>
                
                <asp:GridView ID="EmailGridView" runat="server"
                    DataSourceID="EmailGridViewDataSource"
                    DataKeyNames="ID"
                    AutoGenerateColumns="false"
                    AllowPaging="true"
                    AllowSorting="true"
                    PageSize="10"
                    OnDataBound="EmailGridView_DataBound"
                    Width="500">
                    <SelectedRowStyle Font-Italic="true" />
                    <Columns>
                        <asp:BoundField
                            AccessibleHeaderText="First Name"
                            HeaderText="First Name"
                            DataField="FirstName" />
                        <asp:BoundField
                            AccessibleHeaderText="Last Name"
                            HeaderText="Last Name"
                            DataField="LastName" />
                        <asp:BoundField
                            AccessibleHeaderText="Email Address"
                            HeaderText="Email Address"
                            DataField="Email" />
                        <asp:CommandField
                            ButtonType="Image"
                            ControlStyle-Width="16"
                            ControlStyle-Height="16"
                            CancelImageUrl="~/Images/128x128/cancel.png"
                            DeleteImageUrl="~/Images/128x128/delete.png"
                            SelectImageUrl="~/Images/128x128/arrow-right.png"
                            EditImageUrl="~/Images/128x128/edit.png"
                            UpdateImageUrl="~/Images/128x128/save.png"
                            NewImageUrl="~/Images/128x128/edit-add.png"
                            InsertImageUrl="~/Images/128x128/save.png"
                            ShowEditButton="true"
                            ShowDeleteButton="true"
                            ShowSelectButton="false"
                            ShowCancelButton="true" />
                    </Columns>
                </asp:GridView>
                
                <asp:SqlDataSource ID="EmailDetailsViewDataSource" runat="server"
                    InsertCommand="INSERT INTO Email (FirstName, LastName, Email, EmailListId) VALUES (@FirstName, @LastName, @Email, @EmailListId)"
                    InsertCommandType="Text"
                    ConnectionString="<%$ ConnectionStrings:SampleDB %>">
                    <InsertParameters>
                        <asp:Parameter Name="FirstName" Type="String" ConvertEmptyStringToNull="true" />
                        <asp:Parameter Name="LastName" Type="String" ConvertEmptyStringToNull="true" />
                        <asp:Parameter Name="Email" Type="String" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter ControlID="EmailListDropDownList" Name="EmailListID" Type="Int32" />
                    </InsertParameters>
                </asp:SqlDataSource>
                
                <asp:DetailsView ID="EmailDetailsView" runat="server" AutoGenerateRows="false"
                        DataSourceID="EmailDetailsViewDataSource"
                        DataKeyNames="ID"
                        DefaultMode="Insert"
                        OnItemInserted="EmailDetailsView_Inserted">
                    <Fields>
                        <asp:BoundField
                            AccessibleHeaderText="First Name"
                            HeaderText="First Name"
                            DataField="FirstName" />
                        <asp:BoundField
                            AccessibleHeaderText="Last Name"
                            HeaderText="Last Name"
                            DataField="LastName" />
                        <asp:BoundField
                            AccessibleHeaderText="Email Address"
                            HeaderText="Email Address"
                            DataField="Email" />
                        <asp:CommandField
                            ButtonType="Image"
                            ControlStyle-Width="16"
                            ControlStyle-Height="16"
                            InsertImageUrl="~/Images/128x128/save.png"
                            ShowCancelButton="false"
                            ShowInsertButton="true" />
                    </Fields>
                    <CommandRowStyle HorizontalAlign="Right" />
                    <HeaderTemplate>New Email Address</HeaderTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </asp:DetailsView>
                
            </fieldset>
        
        </asp:Panel>
        
    </div>
    </form>
</body>
</html>
