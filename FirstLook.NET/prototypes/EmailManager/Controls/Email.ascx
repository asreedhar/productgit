<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Email.ascx.cs" Inherits="Controls_Email" %>
<%@ Register Src="~/Controls/AddressBook.ascx" TagName="AddressBook" TagPrefix="pim" %>
<%@ Register Src="~/Controls/SelectContacts.ascx" TagName="SelectContacts" TagPrefix="pim" %>
<div id="EmailPanel" runat="server" class="flora">
    <asp:Label ID="EmailTitleLabel" runat="server" Text="New Email" CssClass="title"></asp:Label>
    <div style="margin-top: 10px;">
        <asp:LinkButton ID="SendEmailButton" runat="server" OnClick="SendEmailButton_Click">Send</asp:LinkButton>
        <asp:LinkButton ID="AddressBookButton" runat="server" OnClick="AddressBookButton_Click">Address Book</asp:LinkButton>
        <asp:LinkButton ID="CloseButton" runat="server" OnClick="CloseButton_Click">Close</asp:LinkButton>
    </div>
    <div style="margin-top: 10px;">
        <asp:LinkButton ID="ToLinkButton" runat="server" OnClick="ToLinkButton_Click">To:</asp:LinkButton>
        <asp:TextBox ID="ToAutoTextBox" runat="server" ReadOnly="true" Columns="50"></asp:TextBox>
        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="ToManualTextBox" runat="server" Columns="50"></asp:TextBox>
    </div>
    <pim:SelectContacts ID="SelectContacts" runat="server" OnCancelled="SelectContacts_Cancelled" OnSelectedContacts="SelectContacts_SelectedContacts" />
    <pim:AddressBook ID="AddressBook" runat="server" />
</div>