<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddressBook.ascx.cs" Inherits="Controls_AddressBook" %>
<%@ Register Src="~/Controls/Search.ascx" TagName="Search" TagPrefix="pim" %>
<%@ Register Src="~/Controls/Contact.ascx" TagName="Contact" TagPrefix="pim" %>
<%@ Register Src="~/Controls/DistributionList.ascx" TagName="DistributionList" TagPrefix="pim" %>
<div id="AddressBookPanel" runat="server" class="flora">
    <asp:Label ID="AddressBookTitleLabel" runat="server" Text="Address Book" CssClass="title"></asp:Label>
    <div style="margin-top: 10px;">
        <asp:LinkButton ID="NewContactButton" runat="server" OnClick="NewContactButton_Click">New Contact</asp:LinkButton>
        <asp:LinkButton ID="NewDistributionListButton" runat="server" OnClick="NewDistributionListButton_Click">New Distribution List</asp:LinkButton>
        <asp:LinkButton ID="CloseButton" runat="server" OnClick="CloseButton_Click">Close</asp:LinkButton>
    </div>
    <div style="margin-top: 10px;">
        <pim:Search ID="Search" runat="server" />
    </div>
    <div style="margin-top: 10px;">
        <pim:Contact ID="Contact" runat="server" Mode="Insert" OnInserted="Contact_Inserted" OnCancelled="Contact_Canceled" />
    </div>
    <div style="margin-top: 10px;">
        <pim:DistributionList ID="DistributionList" runat="server" Mode="Insert" OnClosed="DistributionList_Closed" OnDeleted="DistributionList_Deleted" />
    </div>
</div>