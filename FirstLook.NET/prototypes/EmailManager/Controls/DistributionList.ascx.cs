using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;

public partial class Controls_DistributionList : UserControl
{
    public delegate void CloseHandler(object sender, EventArgs e);
    public event CloseHandler Closed;

    public delegate void DeleteHandler(object sender, EventArgs e);
    public event DeleteHandler Deleted;

    private string mode = "Edit";

    public string DistributionListId
    {
        get { return Input_DistributionListID.Value; }
        set { Input_DistributionListID.Value = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public DataTable TemporaryDataTable
    {
        get
        {
            DataTable table = ViewState["TemporaryDataTable"] as DataTable;
            if (table == null)
            {
                table = new DataTable("DistributionListContacts");
                table.Columns.Add(new DataColumn("contact_type", typeof (Int32)));
                table.Columns.Add(new DataColumn("id", typeof(Int32)));
                table.Columns.Add(new DataColumn("name", typeof(string)));
                table.Columns.Add(new DataColumn("email", typeof(string)));
                ViewState["TemporaryDataTable"] = table;
            }
            return table;
        }
    }

    public void Open()
    {
        Visible = true;

        ScriptDialog();

        ViewState.Remove("TemporaryDataTable");

        if (string.Compare(Mode, "Insert") == 0)
        {
            DistributionListTitleLabel.Text = "New: Distribution List";

            DistributionListDetailsView.ChangeMode(DetailsViewMode.Insert);
        }
        else
        {
            DataView view = (DataView) DistributionListContactsDataSource.Select(new DataSourceSelectArguments());

            DataTable table = TemporaryDataTable;

            for (int i = 0; i < view.Count; i++)
            {
                DataRow row = table.NewRow();
                row["contact_type"] = Convert.ToInt32(view[i]["contact_type"]);
                row["id"] = Convert.ToInt32(view[i]["id"]);
                row["name"] = view[i]["name"];
                row["email"] = view[i]["email"];
                table.Rows.Add(row);
            }
        }

        DistributionListContactsGridView.DataSource = TemporaryDataTable;
    }

    public void Close()
    {
        Visible = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptDialog();

        DistributionListContactsGridView.DataSource = TemporaryDataTable;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        DistributionListContactsGridView.DataBind();
    }

    protected void SaveAndCloseButton_Click(object sender, EventArgs e)
    {
        if (string.Compare(Mode, "Insert") == 0)
        {
            DistributionListDetailsView.InsertItem(false);
        }
        else
        {
            DistributionListDetailsView.UpdateItem(false);
        }

        DistributionListContactsDataSource.Delete();

        foreach (DataRow row in TemporaryDataTable.Rows)
        {
            DistributionListContactsDataSource.InsertParameters[1].DefaultValue = Convert.ToString(row["contact_type"]);
            DistributionListContactsDataSource.InsertParameters[2].DefaultValue = Convert.ToString(row["id"]);
            DistributionListContactsDataSource.Insert();
        }

        CancelButton_Click(sender, e);
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        if (Closed != null)
            Closed(this, e);
    }

    protected void DeleteButton_Click(object sender, EventArgs e)
    {
        if (string.Compare(Mode, "Insert") == 0)
        {
            CancelButton_Click(sender, e);
        }
        else
        {
            DistributionListDetailsView.DeleteItem();

            if (Deleted != null)
                Deleted(this, e);
        }
    }

    protected void SelectContactsButton_Click(object sender, EventArgs e)
    {
        SelectContacts.Open();
    }

    protected void DistributionListContactsGridView_DataBound(object sender, EventArgs e)
    {
        EmptyDistributionListContacts.Visible = (DistributionListContactsGridView.Rows.Count == 0);
    }

    protected void DistributionListContactsGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        TemporaryDataTable.Rows.RemoveAt(e.RowIndex);
    }

    protected void SelectContacts_Cancelled(object sender, EventArgs e)
    {
        SelectContacts.Close();
    }

    protected void SelectContacts_SelectedContacts(object sender, SelectedContactsEventArgs e)
    {
        DataTable table = TemporaryDataTable;

        foreach (Dictionary<string, string> value in e.Values)
        {
            DataRow row = table.NewRow();
            row["contact_type"] = Convert.ToInt32(value["contact_type"]);
            row["id"] = Convert.ToInt32(value["id"]);
            row["name"] = value["name"];
            row["email"] = value["email"];
            table.Rows.Add(row);
        }
        
        DistributionListContactsGridView.DataBind();

        SelectContacts.Visible = false;
    }

    protected void DistributionListDataSource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        object id = e.Command.Parameters["@id"].Value;
        if (id != null)
            Input_DistributionListID.Value = Convert.ToString(id);
    }

    private void ScriptDialog()
    {
        if (Visible)
        {
            string closeManual = string.Format("__doPostBack('{0}','');", CancelButton.ClientID.Replace("_", "$"));

            string script =
                "$(document).ready(function(){$('#" + DistributionListPanel.ClientID + "').dialog({ title: 'Distribution List', width: 400, height: 300, container: 'form:first', position: 'right', close: function () {" +
                closeManual + "} });});";

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "DistributionList.Dialog." + DistributionListPanel.ClientID, script, true);
        }
    }
}
