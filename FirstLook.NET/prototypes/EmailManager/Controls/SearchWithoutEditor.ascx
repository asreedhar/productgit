<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchWithoutEditor.ascx.cs" Inherits="Controls_SearchWithoutEditor" %>
<div>
    <div style="float: left;">
        <asp:Label ID="FilterLabel" runat="server" Text="Search:" AssociatedControlID="FilterTextBox"></asp:Label>
        <asp:TextBox ID="FilterTextBox" runat="server" AutoPostBack="true"></asp:TextBox>
        <asp:Button ID="FilterButton" runat="server" Text="Go" />
    </div>
    <div style="float: left; margin-left: 10px;">
        <asp:Label ID="ContactTypeLabel" runat="server" Text="Contact Type:" AssociatedControlID="ContactTypeDropDownList"></asp:Label>
        <asp:DropDownList ID="ContactTypeDropDownList" runat="server" AutoPostBack="true">
            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Contact" Value="1"></asp:ListItem>
            <asp:ListItem Text="Distribution List" Value="2"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <span style="clear: both;"></span>
</div>
<div style="margin-top: 10px;">
    <asp:Label ID="EmptySearchResultsLabel" runat="server" Text="There are no results" Visible="false"></asp:Label>
    <asp:SqlDataSource ID="SearchResultDataSource" runat="server"
            SelectCommand="pim.address_book#list"
            SelectCommandType="StoredProcedure"
            DeleteCommand="pim.address_book_entry#delete"
            DeleteCommandType="StoredProcedure"
            ConnectionString="<%$ ConnectionStrings:SampleDB %>">
        <SelectParameters>
            <asp:ControlParameter ControlID="FilterTextBox" Name="filter" Type="string" ConvertEmptyStringToNull="false" DefaultValue="" />
            <asp:ControlParameter ControlID="ContactTypeDropDownList" Name="contact_type" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="int32" />
            <asp:Parameter Name="contact_type" Type="int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="SearchResultGridView" runat="server"
            AutoGenerateColumns="false"
            DataSourceID="SearchResultDataSource"
            DataKeyNames="id,contact_type"
            OnDataBound="SearchResultGridView_DataBound">
        <Columns>
            <asp:TemplateField ShowHeader="false">
                <ItemTemplate>
                    <asp:CheckBox ID="RowSelected" runat="server" Checked="false"></asp:CheckBox>
                    <asp:HiddenField ID="ContactId" runat="server" Value='<%# Eval("id") %>' />
                    <asp:HiddenField ID="ContactType" runat="server" Value='<%# Eval("contact_type") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField AccessibleHeaderText="Name" HeaderText="Name" DataField="name" />
            <asp:BoundField AccessibleHeaderText="Email" HeaderText="Email" DataField="email" />
        </Columns>
    </asp:GridView>
</div>