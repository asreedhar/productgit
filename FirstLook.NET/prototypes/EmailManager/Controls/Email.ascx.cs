using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using App_Code;

public partial class Controls_Email : UserControl
{
    public DataTable TemporaryDataTable
    {
        get
        {
            DataTable table = ViewState["TemporaryDataTable"] as DataTable;
            if (table == null)
            {
                table = new DataTable("DistributionListContacts");
                table.Columns.Add(new DataColumn("contact_type", typeof(Int32)));
                table.Columns.Add(new DataColumn("id", typeof(Int32)));
                table.Columns.Add(new DataColumn("name", typeof(string)));
                table.Columns.Add(new DataColumn("email", typeof(string)));
                ViewState["TemporaryDataTable"] = table;
            }
            return table;
        }
    }

    public void Open()
    {
        Visible = true;

        ScriptDialog();

        ViewState.Remove("TemporaryDataTable");

        ToAutoTextBox.Text = string.Empty;

        ToManualTextBox.Text = string.Empty;
    }

    public void Close()
    {
        Visible = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptDialog();
    }

    protected void SendEmailButton_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterClientScriptBlock(GetType(), "Email.Dialog.Send." + EmailPanel.ClientID, "alert('Email Sent!');", true);

        Close();
    }

    protected void AddressBookButton_Click(object sender, EventArgs e)
    {
        AddressBook.Open();
    }

    protected void CloseButton_Click(object sender, EventArgs e)
    {
        Close();
    }

    protected void ToLinkButton_Click(object sender, EventArgs e)
    {
        SelectContacts.Open();
    }

    protected void SelectContacts_Cancelled(object sender, EventArgs e)
    {
        SelectContacts.Close();
    }

    protected void SelectContacts_SelectedContacts(object sender, SelectedContactsEventArgs e)
    {
        DataTable table = TemporaryDataTable;

        foreach (Dictionary<string, string> value in e.Values)
        {
            DataRow row = table.NewRow();
            row["contact_type"] = Convert.ToInt32(value["contact_type"]);
            row["id"] = Convert.ToInt32(value["id"]);
            row["name"] = value["name"];
            row["email"] = value["email"];
            table.Rows.Add(row);
        }

        StringBuilder sb = new StringBuilder();

        foreach (DataRow row in table.Rows)
        {
            sb.Append(row["email"]).Append(";");
        }

        ToAutoTextBox.Text = sb.ToString();

        SelectContacts.Visible = false;
    }

    private void ScriptDialog()
    {
        if (Visible)
        {
            string closeManual = string.Format("__doPostBack('{0}','');", CloseButton.ClientID.Replace("_", "$"));

            string script =
                "$(document).ready(function(){$('#" + EmailPanel.ClientID + "').dialog({ title: 'New Email', width: 500, height: 400, container: 'form:first', position: 'left', close: function () {" +
                closeManual + "} });});";

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "AddressBook.Dialog." + EmailPanel.ClientID, script, true);
        }
    }
}
