using System;
using System.Web.UI;
using App_Code;

public partial class Controls_SelectContacts : UserControl
{
    public delegate void SelectedContactsEventHandler(object sender, SelectedContactsEventArgs e);
    public event SelectedContactsEventHandler SelectedContacts;

    public delegate void CanceledEventHandler(object sender, EventArgs e);
    public event CanceledEventHandler Cancelled;

    public void Open()
    {
        Visible = true;

        ScriptDialog();
    }

    public void Close()
    {
        Visible = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptDialog();
    }

    protected void OkButton_Click(object sender, EventArgs e)
    {
        if (SelectedContacts != null)
            SelectedContacts(this, new SelectedContactsEventArgs(Search.SelectedValues()));
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        if (Cancelled != null)
            Cancelled(this, EventArgs.Empty);
    }

    private void ScriptDialog()
    {
        if (Visible)
        {
            string closeManual = string.Format("__doPostBack('{0}','');", CancelButton.ClientID.Replace("_", "$"));

            string script =
                "$(document).ready(function(){$('#" + SelectContactsPanel.ClientID + "').dialog({ title: 'Select Contacts', width: 500, height: 450, container: 'form:first', close: function () {" +
                closeManual + "} });});";

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "SelectContacts.Dialog." + SelectContactsPanel.ClientID, script, true);
        }
    }
}
