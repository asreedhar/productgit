<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectContacts.ascx.cs" Inherits="Controls_SelectContacts" %>
<%@ Register Src="~/Controls/SearchWithoutEditor.ascx" TagName="Search" TagPrefix="pim" %>
<div id="SelectContactsPanel" runat="server" class="flora">
    <asp:Label ID="SelectContactsTitleLabel" runat="server" Text="Select Contacts"></asp:Label>
    <div>
        <pim:Search ID="Search" runat="server" />
    </div>
    <div style="margin-top: 10px;">
        <asp:LinkButton ID="OkButton" runat="server" OnClick="OkButton_Click">OK</asp:LinkButton>
        <asp:LinkButton ID="CancelButton" runat="server" OnClick="CancelButton_Click">Cancel</asp:LinkButton>
    </div>
</div>