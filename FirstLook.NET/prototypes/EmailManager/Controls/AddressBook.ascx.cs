using System;
using System.Web.UI;

public partial class Controls_AddressBook : UserControl
{
    public void Open()
    {
        Visible = true;

        ScriptDialog();
    }

    public void Close()
    {
        Visible = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptDialog();
    }

    protected void NewContactButton_Click(object sender, EventArgs e)
    {
        Contact.Open();
    }

    protected void NewDistributionListButton_Click(object sender, EventArgs e)
    {
        DistributionList.Open();
    }

    protected void Contact_Inserted(object sender, EventArgs e)
    {
        Contact.Close();

        Search.DataBind();
    }

    protected void Contact_Canceled(object sender, EventArgs e)
    {
        Contact.Close();
    }

    protected void DistributionList_Closed(object sender, EventArgs e)
    {
        DistributionList.Close();

        Search.DataBind();
    }

    protected void DistributionList_Deleted(object sender, EventArgs e)
    {
        DistributionList.Close();

        Search.DataBind();
    }

    protected void CloseButton_Click(object sender, EventArgs e)
    {
        Close();
    }

    private void ScriptDialog()
    {
        if (Visible)
        {
            string closeManual = string.Format("__doPostBack('{0}','');", CloseButton.ClientID.Replace("_", "$"));

            string script =
                "$(document).ready(function(){$('#" + AddressBookPanel.ClientID + "').dialog({ title: 'Address Book', width: 500, height: 400, container: 'form:first', position: 'top', close: function () {" +
                closeManual + "} });});";

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "AddressBook.Dialog." + AddressBookPanel.ClientID, script, true);
        }
    }
}
