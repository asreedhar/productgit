<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Contact.ascx.cs" Inherits="Controls_Contact" %>

<div id="ContactPanel" runat="server" class="flora">
    <asp:Label ID="ContactTitleLabel" runat="server" Text="Edit: Contact"></asp:Label>
    <asp:HiddenField ID="Input_ContactID" runat="server" />
    <asp:SqlDataSource ID="ContactDataSource" runat="server"
            SelectCommand="pim.contact#fetch"
            SelectCommandType="StoredProcedure"
            InsertCommand="pim.contact#create"
            InsertCommandType="StoredProcedure"
            UpdateCommand="pim.contact#update"
            UpdateCommandType="StoredProcedure"
            ConnectionString="<%$ ConnectionStrings:SampleDB %>">
        <SelectParameters>
            <asp:ControlParameter ControlID="Input_ContactID" Name="id" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="first_name" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="last_name" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="email" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="id" Type="Int32" Direction="Output" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="Input_ContactID" Name="id" Type="Int32" />
            <asp:Parameter Name="first_name" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="last_name" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="email" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:DetailsView ID="ContactDetailsView" runat="server"
            AutoGenerateRows="false"
            DataSourceID="ContactDataSource"
            DataKeyNames="id"
            DefaultMode="Edit"
            OnItemInserted="ContactDetailsView_Inserted"
            OnItemUpdated="ContactDetailsView_Updated"
            OnModeChanging="ContactDetailsView_ModeChanging">
        <Fields>
            <asp:BoundField AccessibleHeaderText="First Name" HeaderText="First Name" DataField="first_name" />
            <asp:BoundField AccessibleHeaderText="Last Name" HeaderText="Last Name" DataField="last_name" />
            <asp:BoundField AccessibleHeaderText="Email" HeaderText="Email" DataField="email" />
            <asp:CommandField ButtonType="Link" ShowCancelButton="true" ShowDeleteButton="false" ShowEditButton="true" ShowInsertButton="true" />
        </Fields>
    </asp:DetailsView>
</div>
