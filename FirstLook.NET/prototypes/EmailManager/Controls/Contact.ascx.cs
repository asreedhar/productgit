using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_Contact : UserControl
{
    public delegate void CancelHandler(object sender, EventArgs e);
    public event CancelHandler Cancelled;

    public delegate void InsertHandler(object sender, EventArgs e);
    public event InsertHandler Inserted;

    public delegate void UpdateHandler(object sender, EventArgs e);
    public event UpdateHandler Updated;

    private string mode;

    public string ContactId
    {
        get { return Input_ContactID.Value; }
        set { Input_ContactID.Value = value; }
    }

    public string Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public void Open()
    {
        Visible = true;

        ScriptDialog();
    }

    public void Close()
    {
        Visible = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptDialog();

        if (string.Compare(Mode, "Insert") == 0)
        {
            ContactTitleLabel.Text = "Insert: Contact";

            ContactDetailsView.ChangeMode(DetailsViewMode.Insert);
        }
    }

    protected void ContactDetailsView_Inserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (Inserted != null)
            Inserted(this, EventArgs.Empty);
    }

    protected void ContactDetailsView_Updated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (Updated != null)
            Updated(this, EventArgs.Empty);
    }

    protected void ContactDetailsView_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        if (e.CancelingEdit && Cancelled != null)
                Cancelled(this, EventArgs.Empty);
    }

    private void ScriptDialog()
    {
        if (Visible)
        {
            string closeManual = string.Format("__doPostBack('{0}','Cancel$-1');", ContactDetailsView.ClientID.Replace("_", "$"));

            string script =
                "$(document).ready(function(){$('#" + ContactPanel.ClientID + "').dialog({ title: 'Contact', width: 300, height: 200, container: 'form:first', position: 'bottom', close: function () {" +
                closeManual + "} });});";

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "Contact.Dialog." + ContactPanel.ClientID, script, true);
        }
    }
}
