<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DistributionList.ascx.cs" Inherits="Controls_DistributionList" %>
<%@ Register Src="~/Controls/SelectContacts.ascx" TagName="SelectContacts" TagPrefix="pim" %>
<div id="DistributionListPanel" runat="server" class="flora">
    <asp:Label ID="DistributionListTitleLabel" runat="server" Text="Edit: Distribution List"></asp:Label>
    <asp:HiddenField ID="Input_DistributionListID" runat="server" />
    <div style="margin-top: 10px;">
        <asp:LinkButton ID="SaveAndCloseButton" runat="server" OnClick="SaveAndCloseButton_Click">Save and Close</asp:LinkButton>
        <asp:LinkButton ID="DeleteButton" runat="server" OnClick="DeleteButton_Click">Delete</asp:LinkButton>
        <asp:LinkButton ID="SelectContactsButton" runat="server" OnClick="SelectContactsButton_Click">Select Contacts</asp:LinkButton>
        <asp:LinkButton ID="CancelButton" runat="server" OnClick="CancelButton_Click">Cancel</asp:LinkButton>
    </div>
    <div>
        <asp:SqlDataSource ID="DistributionListDataSource" runat="server"
                SelectCommand="pim.distribution_list#fetch"
                SelectCommandType="StoredProcedure"
                InsertCommand="pim.distribution_list#create"
                InsertCommandType="StoredProcedure"
                DeleteCommand="pim.distribution_list#delete"
                DeleteCommandType="StoredProcedure"
                UpdateCommand="pim.distribution_list#update"
                UpdateCommandType="StoredProcedure"
                ConnectionString="<%$ ConnectionStrings:SampleDB %>"
                OnInserted="DistributionListDataSource_Inserted">
            <SelectParameters>
                <asp:ControlParameter ControlID="Input_DistributionListID" Name="id" Type="Int32" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="id" Type="Int32" Direction="Output" />
            </InsertParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="Input_DistributionListID" Name="id" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="Input_DistributionListID" Name="id" Type="Int32" />
                <asp:Parameter Name="name" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:DetailsView ID="DistributionListDetailsView" runat="server"
                DataSourceID="DistributionListDataSource"
                DataKeyNames="id"
                DefaultMode="Edit"
                AutoGenerateRows="false">
            <Fields>
                <asp:BoundField AccessibleHeaderText="Name" HeaderText="Name" DataField="name" />
            </Fields>
        </asp:DetailsView>
    </div>
    <div>
        <asp:SqlDataSource ID="DistributionListContactsDataSource" runat="server"
                SelectCommand="pim.distribution_list_contacts#fetch"
                SelectCommandType="StoredProcedure"
                InsertCommand="pim.distribution_list_contacts#add"
                InsertCommandType="StoredProcedure"
                DeleteCommand="pim.distribution_list_contacts#delete"
                DeleteCommandType="StoredProcedure"
                ConnectionString="<%$ ConnectionStrings:SampleDB %>">
            <SelectParameters>
                <asp:ControlParameter ControlID="Input_DistributionListID" Name="distribution_list_id" Type="Int32" />
            </SelectParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="Input_DistributionListID" Name="distribution_list_id" Type="Int32" />
                <asp:Parameter Name="contact_type" Type="Int32" />
                <asp:Parameter Name="id" Type="Int32" />
            </InsertParameters>
            <DeleteParameters>
                <asp:ControlParameter ControlID="Input_DistributionListID" Name="distribution_list_id" Type="Int32" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <asp:Label ID="EmptyDistributionListContacts" runat="server" Text="There are no members in the distribution list" Visible="false"></asp:Label>
        <asp:GridView ID="DistributionListContactsGridView" runat="server"
                DataKeyNames="contact_type,id"
                AutoGenerateColumns="false"
                OnDataBound="DistributionListContactsGridView_DataBound"
                OnRowDeleting="DistributionListContactsGridView_RowDeleting">
            <Columns>
                <asp:BoundField AccessibleHeaderText="Name" HeaderText="Name" DataField="name" />
                <asp:BoundField AccessibleHeaderText="Email" HeaderText="Email" DataField="email" />
                <asp:CommandField ButtonType="Link" ShowCancelButton="false" ShowDeleteButton="true" ShowEditButton="false" ShowSelectButton="false" />
            </Columns>
        </asp:GridView>
    </div>
    <pim:SelectContacts ID="SelectContacts" runat="server" OnCancelled="SelectContacts_Cancelled" OnSelectedContacts="SelectContacts_SelectedContacts" />
</div>