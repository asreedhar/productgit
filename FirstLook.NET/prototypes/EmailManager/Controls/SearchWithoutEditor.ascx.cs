using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_SearchWithoutEditor : UserControl
{
    public IEnumerable<Dictionary<string, string>> SelectedValues()
    {
        List<Dictionary<string, string>> values = new List<Dictionary<string, string>>();

        foreach (GridViewRow row in SearchResultGridView.Rows)
        {
            CheckBox checkBox = row.Controls[0].FindControl("RowSelected") as CheckBox;
            if (checkBox != null)
            {
                if (checkBox.Checked)
                {
                    Dictionary<string, string> value = new Dictionary<string, string>();
                    value["id"] = ((HiddenField)row.Controls[0].FindControl("ContactId")).Value;
                    value["contact_type"] = ((HiddenField)row.Controls[0].FindControl("ContactType")).Value;
                    value["name"] = ((TableCell)(row.Controls[1])).Text;
                    value["email"] = ((TableCell)(row.Controls[2])).Text;
                    values.Add(value);
                }
            }
        }

        return values;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SearchResultGridView_DataBound(object sender, EventArgs e)
    {
        EmptySearchResultsLabel.Visible = (SearchResultGridView.Rows.Count == 0);
    }
}
