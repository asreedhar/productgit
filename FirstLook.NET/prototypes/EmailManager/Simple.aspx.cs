using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Simple : Page 
{
    private string selectedListName;

    protected void Page_Init(object sender, EventArgs e)
    {
        selectedListName = string.Empty;
    }

    protected void EmailListDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmailGridView.SelectedIndex = -1;
    }

    protected void EmailListDropDownList_DataBound(object sender, EventArgs e)
    {
        if (EmailListDropDownList.Items.Count == 0)
        {
            EmptyEmailListPanel.Visible = true;

            EmailListSelectionPanel.Visible = false;

            EmailPanel.Visible = false;

            EmailListFormView.ChangeMode(DetailsViewMode.Insert);
        }
        else
        {
            for (int i = 0; i < EmailListDropDownList.Items.Count; i++)
            {
                if (string.Compare(EmailListDropDownList.Items[i].Text, selectedListName) == 0)
                {
                    EmailListDropDownList.SelectedIndex = i;
                    break;
                }
            }
        }
    }
    
    protected void SelectEmailList_Click(object sender, EventArgs e)
    {
        EmailListFormView.ChangeMode(DetailsViewMode.ReadOnly);
    }

    protected void EmailListFormView_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        if (EmailListDropDownList.Items.Count == 0 && e.NewMode != DetailsViewMode.Insert)
        {
            e.Cancel = true;
        }
    }

    protected void EmailListFormView_Inserted(object sender, DetailsViewInsertedEventArgs e)
    {
        selectedListName = string.Format("{0}", e.Values["Name"]);

        EmptyEmailListPanel.Visible = false;

        EmailListSelectionPanel.Visible = true;

        EmailPanel.Visible = true;

        EmailListDropDownList.DataBind();

        EmailListFormView.ChangeMode(DetailsViewMode.ReadOnly);
    }

    protected void EmailListFormView_Deleted(object sender, DetailsViewDeletedEventArgs e)
    {
        EmailListDropDownList.DataBind();
    }

    protected void EmailListFormView_Updated(object sender, DetailsViewUpdatedEventArgs e)
    {
        selectedListName = string.Format("{0}", e.NewValues["Name"]);

        EmailListDropDownList.DataBind();
    }

    protected void EmailGridView_DataBound(object sender, EventArgs e)
    {
        EmptyEmailGridViewPanel.Visible = (EmailGridView.Rows.Count == 0);
    }

    protected void EmailDetailsView_Inserted(object sender, DetailsViewInsertedEventArgs e)
    {
        e.KeepInInsertMode = true;

        EmailGridView.DataBind();
    }
}
