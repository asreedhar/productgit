
create schema pim
go

create role pim_user
go

create table pim.contacts
(
	id int identity(1,1) not null,
	first_name varchar(50),
	last_name varchar(50),
	email varchar(100),
	constraint pk_pim_contacts primary key (id)
)
go

create table pim.distribution_lists
(
	id int identity(1,1) not null,
	name varchar(50),
	constraint pk_pim_distribution_lists primary key (id)
)
go

create table pim.distribution_list_members
(
	distribution_list_id int not null,
	member_contact_id int null,
	member_distribution_list_id int null,
	constraint ck_pim_distribution_list_members check ((member_contact_id is null and member_distribution_list_id is not null) or (member_contact_id is not null and member_distribution_list_id is null)),
	constraint fk_pim_distribution_list_members__pim_distribution_lists_owner foreign key (distribution_list_id) references pim.distribution_lists (id),
	constraint fk_pim_distribution_list_members__pim_distribution_lists_member foreign key (member_distribution_list_id) references pim.distribution_lists (id),
	constraint fk_pim_distribution_list_members__pim_contacts foreign key (member_contact_id) references pim.contacts (id)	
)
go

create clustered index ix_pim_distribution_list_members on pim.distribution_list_members (distribution_list_id, member_contact_id, member_distribution_list_id)
go

/* pim.contacts */

create procedure pim.contact#fetch
	@id int
as
	select	id, first_name, last_name, email
	from	pim.contacts
	where	id = @id
go

grant execute on pim.contact#fetch to pim_user
go

create procedure pim.contact#create
	@first_name varchar(50),
	@last_name varchar(50),
	@email varchar(100),
	@id int output
as
	insert into pim.contacts (first_name, last_name, email) values (@first_name, @last_name, @email)
	
	select @id = SCOPE_IDENTITY()
go

grant execute on pim.contact#create to pim_user
go

create procedure pim.contact#update
	@id int,
	@first_name varchar(50),
	@last_name varchar(50),
	@email varchar(100)
as
	update	pim.contacts
	set	first_name = @first_name, last_name = @last_name, email = @email
	where	id = @id
go

grant execute on pim.contact#update to pim_user
go

create procedure pim.contact#delete
	@id int
as
	delete from pim.distribution_list_members where member_contact_id = @id
	delete from pim.contacts where id = @id
go

grant execute on pim.contact#delete to pim_user
go

/* pim.distribution_lists */

create procedure pim.distribution_list#fetch
	@id int
as
	select	id, name
	from	pim.distribution_lists
	where	id = @id
go

grant execute on pim.distribution_list#fetch to pim_user
go

create procedure pim.distribution_list#create
	@name varchar(50),
	@id int output
as
	insert into pim.distribution_lists (name) values (@name);
	select	@id = SCOPE_IDENTITY();
go

grant execute on pim.distribution_list#create to pim_user
go

create procedure pim.distribution_list#delete
	@id int
as
	delete from pim.distribution_list_members where distribution_list_id = @id;
	delete from pim.distribution_lists where id = @id;
go

grant execute on pim.distribution_list#delete to pim_user
go

create procedure pim.distribution_list#update
	@id int,
	@name varchar(50)
as
	update pim.distribution_lists set name = @name where id = @id
go

grant execute on pim.distribution_list#update to pim_user
go

/* pim.distribution_list_contacts */

create procedure pim.distribution_list_contacts#fetch
	@distribution_list_id int
as
	select	t.contact_type, t.id, t.name, t.email
	from	(
			select	id, coalesce(first_name+' ','') + coalesce(last_name,'') as name, email, 1 as contact_type
			from	pim.contacts c
			join	pim.distribution_list_members m on m.member_contact_id = c.id
			where	m.distribution_list_id = @distribution_list_id
		union all
			select	id, name, '<distribution list>' email, 2 as contact_type
			from	pim.distribution_lists l
			join	pim.distribution_list_members m on m.member_distribution_list_id = l.id
			where	m.distribution_list_id = @distribution_list_id
		) t
go

grant execute on pim.distribution_list_contacts#fetch to pim_user
go

create procedure pim.distribution_list_contacts#delete
	@distribution_list_id int
as
	delete from pim.distribution_list_members
	where distribution_list_id = @distribution_list_id
go

grant execute on pim.distribution_list_contacts#delete to pim_user
go

create procedure pim.distribution_list_contacts#add
	@distribution_list_id int,
	@contact_type int,
	@id int
as
	if @contact_type = 1
		insert into pim.distribution_list_members (distribution_list_id, member_contact_id) values (@distribution_list_id, @id)
	else if @contact_type = 2
		insert into pim.distribution_list_members (distribution_list_id, member_distribution_list_id) values (@distribution_list_id, @id)
go

grant execute on pim.distribution_list_contacts#add to pim_user
go

/* address book */

create procedure pim.address_book#list
	@filter varchar(255),
	@contact_type int
as
	select	t.contact_type, t.id, t.name, t.email
	from	(
			select	id, coalesce(first_name+' ','') + coalesce(last_name,'') as name, email, 1 as contact_type
			from	pim.contacts
		union all
			select	id, name, '<distribution list>' email, 2 as contact_type
			from	pim.distribution_lists
		) t
	where	(@contact_type = 0 or t.contact_type = @contact_type)
	and	(nullif(@filter,'') is null or t.name like '%' + @filter + '%')
go

grant execute on pim.address_book#list to pim_user
go

create procedure pim.address_book_entry#delete
	@id int,
	@contact_type int
as
	if @contact_type = 1
		exec pim.contact#delete @id
	else
		exec pim.distribution_list#delete @id
go

grant execute on pim.address_book_entry#delete to pim_user
go
