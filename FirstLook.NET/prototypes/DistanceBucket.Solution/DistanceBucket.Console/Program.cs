using System.Collections.Generic;
using System.IO;
using DistanceBucket.Library;

namespace DistanceBucket.Console
{
	class Program
	{
		/// <summary>
		/// <para>Load the file <value>Location.csv</value> and generate bounding
		/// rectangles for each zip code at the following radii (in miles)</para>
		/// <list type="bullet">
		/// <item>10</term></item>
		/// <item>25</item>
		/// <item>50</item>
		/// <item>75</item>
		/// <item>100</item>
		/// <item>150</item>
		/// <item>250</item>
		/// <item>500</item>
		/// <item>750</item>
		/// <item>1000</item>
		/// </list>
		/// <para>The result of this computation is a comma delimited flat
		/// file whose columns are as follows.</para>
		/// <list type="table">
		/// <term>ZIP</term><description>The ZIP at the center of the circle</description>
		/// <term>Distance</term><description>The radius as an integer</description>
		/// <term>Lat1</term><description>The latitude of the upper left corner of the bounding rectangle.</description>
		/// <term>Long1</term><description>The longitude of the upper left corner of the bounding rectangle.</description>
		/// <term>Lat2</term><description>The latitude of the lower right corner of the bounding rectangle.</description>
		/// <term>Long2</term><description>The longitude of the lower right corner of the bounding rectangle.</description>
		/// </list>
		/// </summary>
		/// <param name="args">An array of distances</param>
		static void Main(string[] args)
		{
            int[] distances = new int[] { 10, 25, 50, 75, 100, 150, 250, 500, 750, 1000 };
            LocationList locations = new LocationList(new StreamReader(File.OpenRead(@"..\..\..\DistanceBucket.Data\Location.csv")));

            foreach (Location location in locations)
            {
                foreach (int distance in distances)
                {
                    foreach (Rectangle rect in locations.BoundingRectangles(locations.InsideCircle(location.ZipCode, distance)))
                    {
                        System.Console.WriteLine(
                            location.ZipCode + "," + 
                            distance + "," +
                            rect.UpperLeft.Latitude + "," + 
                            rect.UpperLeft.Longitude + "," +
                            rect.LowerRight.Latitude + "," + 
                            rect.LowerRight.Longitude);
                    }
                }
            }
		}
	}
}
