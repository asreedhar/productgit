using System.Collections.Generic;
using System.IO;
using DistanceBucket.Library;
using NUnit.Framework;

namespace DistanceBucket.Test
{
	[TestFixture]
	public class SearchTests
	{
		private LocationList locations;

		[TestFixtureSetUp]
		public virtual void FixtureSetUp()
		{
			using (FileStream stream = File.OpenRead(@"..\..\..\DistanceBucket.Data\Location.csv"))
			{
				using (StreamReader reader = new StreamReader(stream))
				{
					locations = new LocationList(reader);
				}
			}
		}

		[TestFixtureTearDown]
		public virtual void FixtureTearDown()
		{
			locations = null;
		}

		[Test]
		public virtual void NoOverlap()
		{
			List<Location> inside = locations.InsideCircle("60202", 150);

			List<Rectangle> rectangles = locations.BoundingRectangles(inside);

			foreach (Rectangle i in rectangles)
			{
				foreach (Rectangle j in rectangles)
				{
					Assert.IsNull(i.Intersection(j));
				}
			}
		}

		[Test]
		public virtual void CorrectContents()
		{
			List<Location> inside = locations.InsideCircle("60202", 150);

			List<Rectangle> rectangles = locations.BoundingRectangles(inside);

			foreach (Location location in inside)
			{
				bool found = false;

				foreach (Rectangle rectangle in rectangles)
				{
					if (rectangle.Contains(location.Coordinate))
					{
						found = true;
					}
				}

				Assert.IsTrue(found);
			}
		}

		[Test]
		public virtual void NoAdditionalValues()
		{
			List<Location> inside = locations.InsideCircle("60202", 150);

			List<Location> outside = locations.OutsideCircle("60202", 150);

			List<Rectangle> rectangles = locations.BoundingRectangles(inside);

			foreach (Location location in outside)
			{
				foreach (Rectangle rectangle in rectangles)
				{
					Assert.IsFalse(rectangle.Contains(location.Coordinate));
				}
			}
		}
	}
}