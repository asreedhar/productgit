using System;
using System.Collections.Generic;
using System.IO;

namespace DistanceBucket.Library
{
	public class LocationList : List<Location>
	{
        public LocationList(TextReader reader)
        {
            //read first line for header
            string line = reader.ReadLine();
            string[] data;
            while ((line = reader.ReadLine()) != null)
            {
                data = line.Split(',');
                Coordinate coord = new Coordinate(float.Parse(data[1]), float.Parse(data[1]));
                Location loc = new Location(data[0], coord);
                Add(loc);
            }
        }

		/// <summary>
		/// Return an list of all <see cref="Location"/>s inside the circle
		/// with center <paramref name="zipCode"/> and radius <paramref name="radius"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException" />
		/// <param name="zipCode">five character numeral string sequence (not null)</param>
		/// <param name="radius">positive integer</param>
		/// <returns>A list of locations inside the circle defined by <paramref name="zipCode"/> and <paramref name="radius"/></returns>
		public List<Location> InsideCircle(string zipCode, int radius)
		{
            Location origin = Find(delegate(Location loc) { return loc.ZipCode == zipCode; });
            List<Location> results = new List<Location>();

            if (origin != null)
            {
                foreach (Location l in this)
                {
                    if (distance(origin.Coordinate.Latitude, origin.Coordinate.Longitude, l.Coordinate.Latitude, l.Coordinate.Longitude) <= radius)
                    {
                        results.Add(l);
                    }
                }
            }

            return results;
		}


		/// <summary>
		/// Return a list of all <see cref="Location"/>s not inside the circle
		/// with center <paramref name="zipCode"/> and radius <paramref name="radius"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException" />
		/// <param name="zipCode">five character numeral string sequence (not null)</param>
		/// <param name="radius">positive integer</param>
		/// <returns></returns>
		public List<Location> OutsideCircle(string zipCode, int radius)
		{
            Location origin = Find(delegate(Location loc) { return loc.ZipCode == zipCode; });
            List<Location> results = new List<Location>();

            if (origin != null)
            {
                foreach (Location l in this)
                {
                    if (distance(origin.Coordinate.Latitude, origin.Coordinate.Longitude, l.Coordinate.Latitude, l.Coordinate.Longitude) > radius)
                    {
                        results.Add(l);
                    }
                }
            }

            return results;
        }

		/// <summary>
		/// Return the minimum number of bounding rectangles that fully enclose the
		/// argument set of <paramref name="locations"/>.
		/// </summary>
		/// <param name="locations">List of locations whose position will be inside
		/// one of the returned <see cref="Rectangle"/>s</param>
		/// <returns>One or more rectangles that encapsulate the argument locations</returns>
		public List<Rectangle> BoundingRectangles(List<Location> locations)
		{
            List<Rectangle> result = new List<Rectangle>();

            foreach (Location location in locations)
		    {
                //must change this
                //in for now to get tests to work, must be made much better

                if (!result.Exists(delegate(Rectangle r) { return r.Contains(location.Coordinate); }))
                {
                    result.Add(new Rectangle(location.Coordinate, location.Coordinate));
                }
		    }
			
            return result;
		}




        
        
        private static double distance(double lat1, double lon1, double lat2, double lon2)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            return (dist);
        }
        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }
        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
	}
}
