using System;

namespace DistanceBucket.Library
{
    public class Coordinate
    {
        private float latitude;
        private float longitude;

        public Coordinate()
        {
        }

        public Coordinate(float latitude, float longitude)
        {
            this.latitude = latNormal(latitude);
            this.longitude = lonNormal(longitude);
        }

        public float Latitude
        {
            get { return latitude; }
            set { latitude = latNormal(value); }
        }

        public float Longitude
        {
            get { return longitude; }
            set { longitude = lonNormal(value); }
        }



        /**
         * put longitude into the range [0, 360] deg
         *
         * @param lon lon to normalize
         * @return longitude into the range [0, 360] deg
         */
        static public float lonNormal360(float lon)
        {
            return lonNormal(lon, (float)180.0);
        }

        /**
         * put longitude into the range [center +/- 180] deg
         *
         * @param lon    lon to normalize
         * @param center center point
         * @return longitude into the range [center +/- 180] deg
         */
        static public float lonNormal(float lon, float center)
        {
            return (float)(center + Math.IEEERemainder(lon - center, 360.0));
        }

        /**
         * Normalize the longitude to lie between +/-180
         *
         * @param lon east latitude in degrees
         * @return normalized lon
         */
        static public float lonNormal(float lon)
        {
            if ((lon < -180) || (lon > 180))
            {
                return (float)(Math.IEEERemainder(lon, 360.0));
            }
            else
            {
                return lon;
            }
        }

        /**
         * Normalize the latitude to lie between +/-90
         *
         * @param lat north latitude in degrees
         * @return normalized lat
         */
        static public float latNormal(float lat)
        {
            if (lat < -90)
            {
                return -90;
            }
            else if (lat > 90)
            {
                return 90;
            }
            else
            {
                return lat;
            }
        }

        public bool Equals(Coordinate pt)
        {
            return pt.Latitude == latitude && pt.Longitude == longitude;
        }
    }
}
