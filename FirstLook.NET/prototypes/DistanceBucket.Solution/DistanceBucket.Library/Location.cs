namespace DistanceBucket.Library
{
	public class Location
	{
		private readonly string zipCode;
		private readonly Coordinate coordinate;

		public Location(string zipCode, Coordinate coordinate)
		{
			this.zipCode = zipCode;
			this.coordinate = coordinate;
		}

		public string ZipCode
		{
			get { return zipCode; }
		}

		public Coordinate Coordinate
		{
			get { return coordinate; }
		}
	}
}
