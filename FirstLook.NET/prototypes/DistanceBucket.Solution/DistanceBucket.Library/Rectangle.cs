using System;

namespace DistanceBucket.Library
{
    public class Rectangle
    {
        private readonly Coordinate upperRight;
        private readonly Coordinate lowerLeft;
        private bool crossDateline = false;
        private bool allLongitude = false;
        private double width;
        private double lon0;

        public Coordinate UpperRight
        {
            get
            {
                return upperRight;
            }
        }
        public Coordinate LowerRight
        {
            get
            {
                return new Coordinate(lowerLeft.Latitude, upperRight.Longitude);
            }
        }
        public Coordinate UpperLeft
        {
            get
            {
                return new Coordinate(upperRight.Latitude, lowerLeft.Longitude);
            }
        }
        public Coordinate LowerLeft
        {
            get
            {
                return lowerLeft;
            }
        }
        public bool CrossDateline
        {
            get
            {
                return crossDateline;
            }
        }
        public double Width
        {
            get
            {
                return width;
            }
        }
        public double Height
        {
            get
            {
                return LatMax - LatMin;
            }
        }
        public double CenterLon
        {
            get
            {
                return lon0;
            }
        }
        public double LonMin
        {
            get
            {
                return lowerLeft.Longitude;
            }
        }
        public double LonMax
        {
            get
            {
                return lowerLeft.Longitude + width;
            }
        }
        public double LatMin
        {
            get
            {
                return lowerLeft.Latitude;
            }
        }
        public double LatMax
        {
            get
            {
                return upperRight.Latitude;
            }
        }


        /**
         * Construct a lat/lon bounding box from two points.
         * The order of longitude coord of the two points matters:
         * pt1.lon is always the "left" point, then points contained within the box
         * increase (unless crossing the Dateline, in which case they jump to -180, but
         * then start increasing again) until pt2.lon
         * The order of lat doesnt matter: smaller will go to "lower" point (further south)
         *
         * @param left  left corner
         * @param right right corner
         */
        public Rectangle(Coordinate left, Coordinate right) : this(left, right.Latitude - left.Latitude, Coordinate.lonNormal360(right.Longitude - left.Longitude))
        {

        }
        private Rectangle(Coordinate p1, double deltaLat, double deltaLon)
        {
            double lonmin, lonmax;
            double latmin = Math.Min(p1.Latitude, p1.Latitude + deltaLat);
            double latmax = Math.Max(p1.Latitude, p1.Latitude + deltaLat);

            double lonpt = p1.Longitude;
            if (deltaLon > 0)
            {
                lonmin = lonpt;
                lonmax = lonpt + deltaLon;
                crossDateline = (lonmax > 180.0);
            }
            else
            {
                lonmax = lonpt;
                lonmin = lonpt + deltaLon;
                crossDateline = (lonmin < -180.0);
            }

            lowerLeft = new Coordinate((float)latmin, (float)lonmin);
            upperRight = new Coordinate((float)latmax, (float)lonmax);

            // these are an alternative way to view the longitude range
            width = Math.Abs(deltaLon);
            lon0 = Coordinate.lonNormal((float)(p1.Longitude + deltaLon / 2));
            allLongitude = (width >= 360.0);
        }

        public bool Equals(Rectangle other)
        {
            return lowerLeft.Equals(other.LowerLeft) && upperRight.Equals(other.UpperRight);
        }

        public bool Contains(Coordinate p)
        {
            double eps = 1.0e-9;

            // check lat first
            if ((p.Latitude + eps < lowerLeft.Latitude)
                || (p.Latitude - eps > upperRight.Latitude))
            {
                return false;
            }

            if (allLongitude)
                return true;

            if (crossDateline)
            {
                // bounding box crosses the +/- 180 seam
                return ((p.Longitude >= lowerLeft.Longitude) || (p.Longitude <= upperRight.Longitude));
            }
            else
            {
                // check "normal" lon case
                return ((p.Longitude >= lowerLeft.Longitude) && (p.Longitude <= upperRight.Longitude));
            }
        }
        public bool Contains(Rectangle b)
        {
            return (b.Width >= width) && b.Contains(upperRight) && b.Contains(lowerLeft);
        }

        public Rectangle Intersection(Rectangle clip)
        {
            double latMin = Math.Max(LatMin, clip.LatMin);
            double latMax = Math.Min(LatMax, clip.LatMax);
            double deltaLat = latMax - latMin;
            if (deltaLat < 0)
            {
                return null;
            }

            // lon as always is a pain : if not intersection, try +/- 360
            double lon1min = LonMin;
            double lon1max = LonMax;
            double lon2min = clip.LonMin;
            double lon2max = clip.LonMax;
            if (!Intersection(lon1min, lon1max, lon2min, lon2max))
            {
                lon2min = clip.LonMin + 360;
                lon2max = clip.LonMax + 360;
                if (!Intersection(lon1min, lon1max, lon2min, lon2max))
                {
                    lon2min = clip.LonMin - 360;
                    lon2max = clip.LonMax - 360;
                }
            }

            // we did our best to find an intersection
            double lonMin = Math.Max(lon1min, lon2min);
            double lonMax = Math.Min(lon1max, lon2max);
            double deltaLon = lonMax - lonMin;
            if (deltaLon < 0)
                return null;

            return new Rectangle(new Coordinate((float)latMin, (float)lonMin), deltaLat, deltaLon);
        }

        private static bool Intersection(double min1, double max1, double min2, double max2)
        {
            double min = Math.Max(min1, min2);
            double max = Math.Min(max1, max2);
            return min < max;
        }
    }
}