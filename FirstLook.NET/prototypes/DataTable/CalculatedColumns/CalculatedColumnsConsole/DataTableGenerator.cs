using System;
using System.Collections.Generic;
using System.Data;

namespace CalculatedColumnsConsole
{
    public class DataTableGenerator
    {
        public DataTable Generate()
        {
            DataTable table = EmptyTable();

            int[][] dataRows = new int[5][];
            dataRows[0] = new int[] { 1, 10 };
            dataRows[1] = new int[] { 2, 7 };
            dataRows[2] = new int[] { 3, 2 };
            dataRows[3] = new int[] { 4, 8 };
            dataRows[4] = new int[] { 5, 11 };

            foreach (int[] dataRow in dataRows)
            {
                DataRow row = table.NewRow();
                row["DealerID"] = dataRow[0];
                row["InventoryCount"] = dataRow[1];
                table.Rows.Add(row);
            }

            DataRow nullRow = table.NewRow();
            nullRow["DealerID"] = 6;
            nullRow["InventoryCount"] = DBNull.Value;
            table.Rows.Add(nullRow);

            return table;
        }

        public DataTable EmptyTable()
        {
            DataTable table = new DataTable("Dealers");

            DataColumn dealerColumn = new DataColumn("DealerID", typeof(int));
            dealerColumn.Unique = true;
            dealerColumn.AllowDBNull = false;
            dealerColumn.ReadOnly = true;

            table.Columns.Add(dealerColumn);

            DataColumn inventoryCountColumn = new DataColumn("InventoryCount", typeof(int));
            inventoryCountColumn.ReadOnly = true;

            table.Columns.Add(inventoryCountColumn);

            return table;
        }

        public void AddCalculatedColumns(DataTable table, string columnSelector)
        {
            DataColumn rowCountColumn = new DataColumn("RowCount", typeof(int), string.Format("Count({0}DealerID)", columnSelector));
            rowCountColumn.AllowDBNull = false;
            rowCountColumn.ReadOnly = true;

            table.Columns.Add(rowCountColumn);

            DataColumn inventoryTotalColumn = new DataColumn("InventorySum", typeof(int), string.Format("Sum({0}InventoryCount)", columnSelector));
            inventoryTotalColumn.AllowDBNull = false;
            inventoryTotalColumn.ReadOnly = true;

            table.Columns.Add(inventoryTotalColumn);

            DataColumn inventoryMinColumn = new DataColumn("InventoryMin", typeof(int), string.Format("Min({0}InventoryCount)", columnSelector));
            inventoryMinColumn.AllowDBNull = false;
            inventoryMinColumn.ReadOnly = true;

            table.Columns.Add(inventoryMinColumn);

            DataColumn inventoryMaxColumn = new DataColumn("InventoryMax", typeof(int), string.Format("Max({0}InventoryCount)", columnSelector));
            inventoryMaxColumn.AllowDBNull = false;
            inventoryMaxColumn.ReadOnly = true;

            table.Columns.Add(inventoryMaxColumn);

            DataColumn inventoryAvgColumn = new DataColumn("InventoryAvg", typeof(int), string.Format("Avg({0}InventoryCount)", columnSelector));
            inventoryAvgColumn.AllowDBNull = false;
            inventoryAvgColumn.ReadOnly = true;

            table.Columns.Add(inventoryAvgColumn);
        }

        public DataTable CopyTable(DataTable table)
        {
            IDataReader reader = table.CreateDataReader();

            DataTable schemaTable = reader.GetSchemaTable();

            DataTable tableCopy = new DataTable();

            List<string> columnNames = new List<string>();

            for (int i = 0; i < schemaTable.Rows.Count; i++)
            {
                DataRow schemaTableRow = schemaTable.Rows[i];

                if (!tableCopy.Columns.Contains(schemaTableRow["ColumnName"].ToString()))
                {
                    string columnName = schemaTableRow["ColumnName"].ToString();

                    DataColumn dataColumn = new DataColumn();
                    dataColumn.ColumnName = schemaTableRow["ColumnName"].ToString();
                    dataColumn.Unique = Convert.ToBoolean(schemaTableRow["IsUnique"]);
                    dataColumn.AllowDBNull = Convert.ToBoolean(schemaTableRow["AllowDBNull"]);
                    dataColumn.ReadOnly = Convert.ToBoolean(schemaTableRow["IsReadOnly"]);
                    dataColumn.DataType = (Type)schemaTableRow["DataType"];

                    tableCopy.Columns.Add(dataColumn);

                    columnNames.Add(columnName);
                }
            }

            while (reader.Read())
            {
                DataRow dataRow = tableCopy.NewRow();

                for (int i = 0; i < columnNames.Count; i++)
                {
                    string name = columnNames[i];

                    dataRow[name] = reader[name];
                }

                tableCopy.Rows.Add(dataRow);
            }

            return tableCopy;
        }
    }
}
