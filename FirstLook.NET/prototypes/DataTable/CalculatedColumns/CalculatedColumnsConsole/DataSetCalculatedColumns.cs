using System;
using System.Data;

namespace CalculatedColumnsConsole
{
    internal class DataSetCalculatedColumns
    {
        public void Run()
        {
            DataTableGenerator generator = new DataTableGenerator();

            DataTable dealers = generator.Generate();

            DataColumn dealerGroupColumnChild = new DataColumn("DealerGroupID", typeof(int), "100068");
            dealerGroupColumnChild.AllowDBNull = false;
            dealerGroupColumnChild.ReadOnly = true;

            dealers.Columns.Add(dealerGroupColumnChild);

            DataTable dealerGroups = new DataTable("DealerGroups");

            DataColumn dealerGroupColumnParent = new DataColumn("DealerGroupID", typeof(int));
            dealerGroupColumnParent.Unique = true;
            dealerGroupColumnParent.AllowDBNull = false;
            dealerGroupColumnParent.ReadOnly = true;

            dealerGroups.Columns.Add(dealerGroupColumnParent);

            DataRow dealerGroup = dealerGroups.NewRow();
            dealerGroup["DealerGroupID"] = 100068;

            dealerGroups.Rows.Add(dealerGroup);

            DataSet set = new DataSet();
            set.Tables.Add(dealers);
            set.Tables.Add(dealerGroups);
            set.Relations.Add(
                new DataRelation("Dealer_DealerGroup",
                    dealerGroups.Columns["DealerGroupID"],
                    dealers.Columns["DealerGroupID"],
                    false));

            generator.AddCalculatedColumns(dealerGroups, "Child(Dealer_DealerGroup).");

            Console.WriteLine("GroupID Sum Min Max Avg Rows");
            Console.WriteLine("------- --- --- --- --- ----");

            string fmt = "{0,7} {1,3} {2,3} {3,3} {4,3} {5,4}";

            foreach (DataRow row in generator.CopyTable(dealerGroups).Rows)
            {
                Console.WriteLine(string.Format(fmt, row["DealerGroupID"], row["InventorySum"], row["InventoryMin"], row["InventoryMax"], row["InventoryAvg"], row["RowCount"]));
            }

            Console.ReadKey();
        }
    }
}
