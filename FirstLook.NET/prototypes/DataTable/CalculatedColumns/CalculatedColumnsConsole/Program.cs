using System;
using System.Data;

namespace CalculatedColumnsConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            new DataTableCalculatedColumns().Run();

            new DataSetCalculatedColumns().Run();
        }
    }
}
