using System;
using System.Data;

namespace CalculatedColumnsConsole
{
    internal class DataTableCalculatedColumns
    {
        public void Run()
        {
            DataTableGenerator generator = new DataTableGenerator();

            DataTable table = generator.Generate();

            generator.AddCalculatedColumns(table, string.Empty);

            Console.WriteLine("DealerID Count Sum Min Max Avg");
            Console.WriteLine("-------- ----- --- --- --- ---");

            string fmt = "{0,8} {1,5} {2,3} {3,3} {4,3} {5,3}";

            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine(string.Format(fmt, row["DealerID"], row["InventoryCount"], row["InventorySum"], row["InventoryMin"], row["InventoryMax"], row["InventoryAvg"]));
            }

            Console.ReadKey();
        }
    }
}
