<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExtraData.aspx.cs" Inherits="UpdatePanel_ExtraData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ASP.NET Ajax Extensions - Update Panel - Extra Information</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Extra Information</h1>
        <asp:ScriptManager ID="PageScriptManager" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="MainPanel" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <p>Last Render: <asp:Label ID="PanelUpdateTime" OnLoad="PanelUpdateTime_Load" runat="server" /></p>
                <asp:Button ID="MainPanel_Button" runat="server" Text="Update Panel" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <p>Last Render: <asp:Label ID="PageUpdateTime" OnLoad="PageUpdateTime_Load" runat="server" /></p>
        <asp:Button ID="Page_Button" runat="server" Text="Update Page" />
        <p><asp:HyperLink ID="HyperLink1" NavigateUrl="Default.aspx" runat="server">Home</asp:HyperLink></p>
        <p id="Random">No Random Number</p>
    </div>
    </form>
</body>
</html>
