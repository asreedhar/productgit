using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UpdatePanel_DoublePanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void FirstPanelUpdateTime_Load(object sender, EventArgs e)
    {
        FirstPanelUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }

    protected void PageUpdateTime_Load(object sender, EventArgs e)
    {
        PageUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }

    protected void SecondPanelUpdateTime_Load(object sender, EventArgs e)
    {
        SecondPanelUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }
}
