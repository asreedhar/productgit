using System;

public partial class UpdatePanel_ExtraData : System.Web.UI.Page
{
    private static readonly Random r = new Random();

    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterClientScriptBlock(GetType(), "DataItem", @"
                function OnEndRequest(sender, args) {
                    var dataItem = args.get_dataItems()['__Page'];
                    if (dataItem) {
                        $get('Random').innerHTML = dataItem;
                    }
                }
                function pageLoad() {
                    var manager = Sys.WebForms.PageRequestManager.getInstance();
                    manager.add_endRequest(OnEndRequest);
                }
            ", true);
    }

    protected void PanelUpdateTime_Load(object sender, EventArgs e)
    {
        PanelUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }

    protected void PageUpdateTime_Load(object sender, EventArgs e)
    {
        PageUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (PageScriptManager.IsInAsyncPostBack)
        {
            PageScriptManager.RegisterDataItem(this, GetJsonState(), true);
        }
    }

    private string GetJsonState()
    {
        return r.Next().ToString();
    }
}
