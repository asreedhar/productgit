using System;

public partial class UpdatePanel_SinglePanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void PanelUpdateTime_Load(object sender, EventArgs e)
    {
        PanelUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }

    protected void PageUpdateTime_Load(object sender, EventArgs e)
    {
        PageUpdateTime.Text = DateTime.Now.ToLongTimeString();
    }
}
