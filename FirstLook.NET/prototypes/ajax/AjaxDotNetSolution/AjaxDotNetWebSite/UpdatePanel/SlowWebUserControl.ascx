<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SlowWebUserControl.ascx.cs" Inherits="UpdatePanel_SlowWebUserControl" %>

<asp:Label ID="ItemsLabel" AssociatedControlID="Items" Text="Delay" runat="server" />

<asp:TextBox ID="Items" runat="server">3</asp:TextBox>

<asp:ObjectDataSource ID="DelayObjectDataSource" runat="server" TypeName="SlowDataSource" SelectMethod="Select" EnableViewState="true">
    <SelectParameters>
        <asp:ControlParameter Name="delay" Type="Int32" ControlID="Items" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:Repeater ID="DelayRepeater" runat="server" DataSourceID="DelayObjectDataSource">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Container.DataItem %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
