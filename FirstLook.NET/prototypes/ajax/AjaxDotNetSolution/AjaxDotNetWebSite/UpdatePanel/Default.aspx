<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="UpdatePanel_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ASP.NET Ajax Extensions - Update Panel</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Update Panel Solutions</h1>
        <ul>
            <li><asp:HyperLink ID="SinglePanel" NavigateUrl="SinglePanel.aspx" TabIndex="1" runat="server">Single Panel</asp:HyperLink></li>
            <li><asp:HyperLink ID="DoublePanel" NavigateUrl="DoublePanel.aspx" TabIndex="2" runat="server">Double Panel</asp:HyperLink></li>
            <li><asp:HyperLink ID="ExtraData" NavigateUrl="ExtraData.aspx" TabIndex="3" runat="server">Extra Data</asp:HyperLink></li>
        </ul>
        <p><asp:HyperLink ID="HomeLink" NavigateUrl="../Default.aspx" runat="server">Home</asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
