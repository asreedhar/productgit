<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SinglePanel.aspx.cs" Inherits="UpdatePanel_SinglePanel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ASP.NET Ajax Extensions - Update Panel - Single</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Single Panel</h1>
        <asp:ScriptManager ID="PageScriptManager" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="MainPanel" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <p>Last Render: <asp:Label ID="PanelUpdateTime" OnLoad="PanelUpdateTime_Load" runat="server" /></p>
                <asp:Button ID="MainPanel_Button" runat="server" Text="Update Panel" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <p>Last Render: <asp:Label ID="PageUpdateTime" OnLoad="PageUpdateTime_Load" runat="server" /></p>
        <asp:Button ID="Page_Button" runat="server" Text="Update Page" />
        <p><asp:HyperLink ID="HyperLink1" NavigateUrl="Default.aspx" runat="server">Home</asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
