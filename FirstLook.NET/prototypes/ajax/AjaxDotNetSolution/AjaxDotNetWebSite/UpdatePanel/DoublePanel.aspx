<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DoublePanel.aspx.cs" Inherits="UpdatePanel_DoublePanel" %>

<%@ Register TagPrefix="custom" TagName="SlowControl" Src="~/UpdatePanel/SlowWebUserControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ASP.NET Ajax Extensions - Update Panel - Double</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Double Panel</h1>
        <p>The page has two user controls, each of which take their data from a data source with a delay of the number of seconds in the textbox.</p>
        <p>The initial page load is 6 seconds and page updates with no changes are instant.</p>
        <p>All changes are computed -- change two text-boxes get the sum of their delay. Change just one, get just that delay.</p>
        <p>If you turn off view state on the data-sources both are evaluated.</p>
        <asp:ScriptManager ID="PageScriptManager" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="FirstPanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div style="float:left; margin-left: 30px;">
                    <custom:SlowControl ID="FirstSlowControl" runat="server" />
                    <p>Last Render: <asp:Label ID="FirstPanelUpdateTime" OnLoad="FirstPanelUpdateTime_Load" runat="server" /></p>
                    <asp:Button ID="FirstPanel_Button" runat="server" Text="Update Panel" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="SecondPanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div style="float:left; margin-left: 30px;">
                    <custom:SlowControl ID="SecondSlowControl" runat="server" />
                    <p>Last Render: <asp:Label ID="SecondPanelUpdateTime" OnLoad="SecondPanelUpdateTime_Load" runat="server" /></p>
                    <asp:Button ID="SecondPanel_Button" runat="server" Text="Update Panel" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <p style="clear: both;">Last Render: <asp:Label ID="PageUpdateTime" OnLoad="PageUpdateTime_Load" runat="server" /></p>
        <asp:Button ID="Page_Button" runat="server" Text="Update Page" />
        <p><asp:HyperLink ID="HyperLink1" NavigateUrl="Default.aspx" runat="server">Home</asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
