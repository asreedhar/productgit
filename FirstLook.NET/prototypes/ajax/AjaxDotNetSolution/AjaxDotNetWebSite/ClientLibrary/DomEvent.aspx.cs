using System;

public partial class ClientLibrary_DomEvent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DropDown_Button_Click(object sender, EventArgs e)
    {
        RadioGroup.SelectedIndex = DropDown.SelectedIndex;
    }

    protected void RadioGroup_Button_Click(object sender, EventArgs e)
    {
        DropDown.SelectedIndex = RadioGroup.SelectedIndex;
    }

    protected void RenderTime_Load(object sender, EventArgs e)
    {
        RenderTime.Text = DateTime.Now.ToLongTimeString();
    }
}
