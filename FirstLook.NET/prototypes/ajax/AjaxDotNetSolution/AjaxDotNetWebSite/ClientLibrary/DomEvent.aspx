<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DomEvent.aspx.cs" Inherits="ClientLibrary_DomEvent" %>

<%@ Register Assembly="AjaxDotNetControls" Namespace="AjaxDotNetControls" TagPrefix="x" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ASP.NET AJAX - Client Library - DOM Event</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="MainScriptManager" runat="server">
            <Scripts>
                <asp:ScriptReference Path="DomEvent.js" />
            </Scripts>
        </asp:ScriptManager>
        <h1>DOM Event</h1>
        <fieldset>
            <legend>Data Passing</legend>
            
            <asp:Label ID="DropDown_Label" AssociatedControlID="DropDown" runat="server">Please choose a color:</asp:Label>
            <asp:DropDownList ID="DropDown" runat="server">
                <asp:ListItem Text="Green" Value="1" Selected="True" />
                <asp:ListItem Text="Yellow" Value="2" />
                <asp:ListItem Text="Red" Value="3" />
            </asp:DropDownList>
            <x:DropDownExtender ID="DropDownExtender1" runat="server" TargetControlID="DropDown" LabelControlID="DropDown_Label" ToggleImageUrl="~/ClientLibrary/images/menu-icon.gif" CancelImageUrl="~/ClientLibrary/images/menu-icon.gif"/>
            <br />
            <asp:Label ID="RadioGroup_Label" AssociatedControlID="RadioGroup" runat="server">Radio Group</asp:Label>
            <asp:RadioButtonList ID="RadioGroup" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Text="Green" Value="1" Selected="True" />
                <asp:ListItem Text="Yellow" Value="2" />
                <asp:ListItem Text="Red" Value="3" />
            </asp:RadioButtonList>
            <asp:Button ID="RadioGroup_Button" Text="To Select" OnClick="RadioGroup_Button_Click" runat="server" />
            <x:LinkButtonExtender ID="LinkButtonExtender1" runat="server" TargetControlID="RadioGroup_Button" />
        </fieldset>
        <p>Last Render: <asp:Label ID="RenderTime" runat="server" OnLoad="RenderTime_Load">[time]</asp:Label></p>
        <p><asp:HyperLink ID="HyperLink1" NavigateUrl="Default.aspx" runat="server">Home</asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
