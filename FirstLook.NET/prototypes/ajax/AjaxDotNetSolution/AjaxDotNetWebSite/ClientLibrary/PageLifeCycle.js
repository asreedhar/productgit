﻿
function MyInit(sender) {
    $get("init").innerHTML = "OK";
}

function MyLoad(sender) {
    $get("load").innerHTML = "OK";
}

function MyUnLoad(sender) {
    alert('unload');
}

Sys.Application.add_init(MyInit);

Sys.Application.add_load(MyLoad);

Sys.Application.add_unload(MyUnLoad);
