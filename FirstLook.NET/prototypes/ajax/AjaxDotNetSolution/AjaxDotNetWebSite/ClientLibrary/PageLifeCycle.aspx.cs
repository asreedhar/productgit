using System;

public partial class ClientLibrary_PageLifeCycle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterClientScriptInclude(this.GetType(), "PageLifeCycle-ClientScript", ResolveClientUrl("PageLifeCycle-ClientScript.js"));
        ClientScript.RegisterStartupScript(this.GetType(), "PageLifeCycle-Startup", "MyStartupScript();", true);
    }
}
