﻿
function PassToRadioGroup(eventElement) {
    for (var i = 0; i < document.forms[0].RadioGroup.length; i++) {
        if (i == document.forms[0].DropDown.selectedIndex) {
            document.forms[0].RadioGroup[i].checked = true;
            break;
        }
    }
    eventElement.preventDefault();
}

function PassToDropDown(eventElement) {
    for (var i = 0; i < document.forms[0].RadioGroup.length; i++) {
        if (document.forms[0].RadioGroup[i].checked) {
            document.forms[0].DropDown.selectedIndex = i;
            break;
        }
    }
    eventElement.preventDefault();
}

function AttachHandlers() {
    $clearHandlers($get('DropDown_Button'));
    $addHandler($get('DropDown_Button'), 'click', PassToRadioGroup);
    $clearHandlers($get('RadioGroup_Button'));
    $addHandler($get('RadioGroup_Button'), 'click', PassToDropDown);
}

Sys.Application.add_load(AttachHandlers);
