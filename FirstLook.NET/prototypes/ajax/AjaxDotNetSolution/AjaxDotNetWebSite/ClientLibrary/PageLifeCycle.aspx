<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageLifeCycle.aspx.cs" Inherits="ClientLibrary_PageLifeCycle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ASP.NET AJAX - Client Library - Page Life Cycle</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="MainScriptManager" runat="server">
            <Scripts>
                <asp:ScriptReference Path="PageLifeCycle.js" />
            </Scripts>
        </asp:ScriptManager>
        <h1>Page Life Cycle</h1>
        <ul>
            <li id="startup"></li>
            <li id="init"></li>
            <li id="load"></li>
        </ul>
        <p>This example uses <code>Sys.Application.add_load</code> etc.
           The client library will call the functions <code>pageLoad</code> and <code>pageUnload</code> if present too.</p>
        <p><asp:HyperLink ID="HyperLink1" NavigateUrl="Default.aspx" runat="server">Home</asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
