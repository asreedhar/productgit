<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="ClientLibrary_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ASP.NET Ajax Extensions - Client Library</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Client Library Solutions</h1>
        <ul>
            <li><asp:HyperLink NavigateUrl="PageLifeCycle.aspx" TabIndex="1" runat="server">Page Life Cycle</asp:HyperLink></li>
            <li><asp:HyperLink NavigateUrl="DomEvent.aspx" TabIndex="2" runat="server">DOM Event</asp:HyperLink></li>
        </ul>
        <p><asp:HyperLink NavigateUrl="../Default.aspx" runat="server">Home</asp:HyperLink></p>
    </div>
    </form>
</body>
</html>
