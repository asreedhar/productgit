using System;

/// <summary>
/// Pause page processing for a given delay and return the delay.
/// </summary>
public class SlowDataSource
{
    public string[] Select(int delay)
    {
        string [] times = new string[2];
        times[0] = DateTime.Now.ToLongTimeString();
        System.Threading.Thread.Sleep(delay * 1000);
        times[1] = DateTime.Now.ToLongTimeString();
        return times;
    }
}
