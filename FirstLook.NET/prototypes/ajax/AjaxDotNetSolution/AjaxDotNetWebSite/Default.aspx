﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASP.NET AJAX Extensions</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div>
            <h1>Index of Examples</h1>
            <ul>
                <li><asp:HyperLink ID="ClientLibrary" NavigateUrl="ClientLibrary/Default.aspx" TabIndex="1" runat="server">Client Library</asp:HyperLink></li>
                <li><asp:HyperLink ID="UpdatePanel" NavigateUrl="UpdatePanel/Default.aspx" TabIndex="2" runat="server">Update Panel</asp:HyperLink></li>
            </ul>
        </div>
    </form>
</body>
</html>
