Type.registerNamespace('AjaxDotNetControls');

AjaxDotNetControls.DropDownExtender = function (element)
{
    AjaxDotNetControls.DropDownExtender.initializeBase(this,[element]);
    this.anchor = null;
    this.toggleDiv = null;
    this.instructions = null;
    this._labelControlID = null;
    this._toggleImageUrl = null;
    this._cancelImageUrl = null;
}

AjaxDotNetControls.DropDownExtender.prototype =
{
    initialize: AjaxDotNetControls$DropDownExtender$initialize,
    dispose : AjaxDotNetControls$DropDownExtender$dispose,
    toggleDisplay : AjaxDotNetControls$DropDownExtender$toggleDisplay,
    get_labelControlID : AjaxDotNetControls$DropDownExtender$get_labelControlID,
    set_labelControlID : AjaxDotNetControls$DropDownExtender$set_labelControlID,    
    get_toggleImageUrl : AjaxDotNetControls$DropDownExtender$get_toggleImageUrl,
    set_toggleImageUrl : AjaxDotNetControls$DropDownExtender$set_toggleImageUrl,
    get_cancelImageUrl : AjaxDotNetControls$DropDownExtender$get_cancelImageUrl,
    set_cancelImageUrl : AjaxDotNetControls$DropDownExtender$set_cancelImageUrl
}

// this really only works for select controls - prob wanna restrict that
function AjaxDotNetControls$DropDownExtender$initialize()
{

    AjaxDotNetControls.DropDownExtender.callBaseMethod(this, 'initialize');
 
    alert('cancel: ' + this.get_cancelImageUrl());
 
    // get source select element
    var orgControl = this.get_element();
    var id = orgControl.id;

    // the existing label id is assigned to the new anchor
    // the existing label element is id-ed
    var label = document.getElementById(this.get_labelControlID());
    label.setAttribute('style', 'DISPLAY: none');
    label.id = label.id + '_inst';
    this.instructions = label;
    
    var span = document.createElement('span');
    span.innerHTML = orgControl.options[orgControl.selectedIndex].innerHTML;
    
    // create small arrow img
    var arrowImg = document.createElement('img');
    arrowImg.setAttribute('src', this.get_toggleImageUrl());

    // create new anchor element with same Id as orginal control
    this.anchor = document.createElement('a');
    this.anchor.id=label.id;
    this.anchor.appendChild(span);
    this.anchor.appendChild(arrowImg);

    // create display container div - set initial display to : none
    this.toggleDiv = document.createElement('div');
    this.toggleDiv.id = id + 'toggleDiv';
    this.toggleDiv.setAttribute('style', 'DISPLAY: none');
    var newControl = orgControl.cloneNode(true);
    this.toggleDiv.appendChild(newControl);
    
    // replace the drop down with the Anchor
    orgControl.parentNode.insertBefore(this.anchor, orgControl);    
    
    // remove org and replace with toggleDiv
    orgControl.parentNode.replaceChild(this.toggleDiv, orgControl);
    
    // set up onclick    
    this._onClickHandler = Function.createDelegate(this, this.toggleDisplay);
    $addHandlers(this.anchor, {'click': this.toggleDisplay}, this);
    
    alert('done');
   
}

function AjaxDotNetControls$DropDownExtender$toggleDisplay() {
    alert('rock');
    this.anchor.setAttribute('style', 'DISPLAY: none');
    this.toggleDiv.setAttribute('style', '');
    this.instructions.setAttribute('style', '');

   
    //this.toggleDiv.parentNode.insertBefore(this.toggleDiv, 
    
}

function AjaxDotNetControls$DropDownExtender$dispose() {
    $clearHandlers(this.get_element());
    AjaxDotNetControls.DropDownExtender.callBaseMethod(this, 'dispose');
}


function AjaxDotNetControls$DropDownExtender$get_labelControlID() {
    return this._labelControlID;
}

function AjaxDotNetControls$DropDownExtender$get_toggleImageUrl() {
    return this._toggleImageUrl;
}

function AjaxDotNetControls$DropDownExtender$set_labelControlID(lcid) {
    this._labelControlID = lcid;
}

function AjaxDotNetControls$DropDownExtender$set_toggleImageUrl(iu) {
    this._toggleImageUrl = iu;
}

function AjaxDotNetControls$DropDownExtender$get_cancelImageUrl() {
    return this._cancelImageUrl;
}

function AjaxDotNetControls$DropDownExtender$set_cancelImageUrl(iu) {
    this._cancelImageUrl = iu;
}


AjaxDotNetControls.DropDownExtender.registerClass('AjaxDotNetControls.DropDownExtender', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();