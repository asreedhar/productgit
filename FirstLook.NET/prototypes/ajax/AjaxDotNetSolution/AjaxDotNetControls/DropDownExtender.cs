using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("AjaxDotNetControls.DropDownExtenderBehavior.js", "text/javascript")]

namespace AjaxDotNetControls
{
    [TargetControlType(typeof(DropDownList))]
    public class DropDownExtender: ExtenderControl
    {
        private string labelControlID;

        private string toggleImageUrl;
        private string cancelImageUrl;

        [Category("Design"), Description("Url for toggle image")]
        public string ToggleImageUrl
        {
            get { return toggleImageUrl; }
            set { toggleImageUrl = value; }
        }
        [Category("Design"), Description("Url for cancelling out of div")]
        public string CancelImageUrl
        {
            get { return cancelImageUrl; }
            set { cancelImageUrl = value; }
        }

        [Category("Design")]
        public string LabelControlID
        {
            get { return labelControlID; }
            set { labelControlID = value; }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("AjaxDotNetControls.DropDownExtender", targetControl.ClientID);
            descriptor.AddProperty("labelControlID", Parent.FindControl(LabelControlID).ClientID);
            descriptor.AddProperty("toggleImageUrl", ResolveClientUrl(toggleImageUrl));            
            descriptor.AddProperty("cancelImageUrl", ResolveClientUrl(cancelImageUrl));            
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference reference = new ScriptReference();
            reference.Assembly = "AjaxDotNetControls";
            reference.Name = "AjaxDotNetControls.DropDownExtenderBehavior.js";
            return new ScriptReference[] { reference };
        }
    }
}
