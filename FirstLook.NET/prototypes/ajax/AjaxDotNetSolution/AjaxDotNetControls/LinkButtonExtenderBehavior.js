
Type.registerNamespace('AjaxDotNetControls');

AjaxDotNetControls.LinkButtonExtender = function (element)
{
    AjaxDotNetControls.LinkButtonExtender.initializeBase(this,[element]);
}

AjaxDotNetControls.LinkButtonExtender.prototype =
{
    initialize: AjaxDotNetControls$LinkButtonExtender$initialize
}

function AjaxDotNetControls$LinkButtonExtender$initialize()
{
    AjaxDotNetControls.LinkButtonExtender.callBaseMethod(this, 'initialize');
    
    // get source and null its id
    var from = this.get_element();
    var id = from.id;
    from.id = null;
    
    // create new element
    var into = document.createElement('a');
    into.id = id;
    into.href = "javascript:__doPostBack('" + id + "','')";
    into.appendChild(document.createTextNode(from.value));
    
    // replace the button with the link
    from.parentNode.replaceChild(into, from);
}

AjaxDotNetControls.LinkButtonExtender.registerClass('AjaxDotNetControls.LinkButtonExtender', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();
