using System.Collections.Generic;
using System.Web.UI;

[assembly: WebResource("AjaxDotNetControls.LinkButtonExtenderBehavior.js", "text/javascript")]

namespace AjaxDotNetControls
{
    [TargetControlType(typeof(Control))]
    public class LinkButtonExtender : ExtenderControl
    {
        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor;
            descriptor = new ScriptBehaviorDescriptor("AjaxDotNetControls.LinkButtonExtender", targetControl.ClientID);
            return new ScriptDescriptor[] {descriptor};
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference reference = new ScriptReference();
            reference.Assembly = "AjaxDotNetControls";
            reference.Name = "AjaxDotNetControls.LinkButtonExtenderBehavior.js";
            return new ScriptReference[] {reference};
        }
    }
}
