using System;
using System.Drawing;
using System.Web.UI.WebControls;

public partial class BasicDotNet_Validation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void HomeRolledValidate_Click(object sender, EventArgs e)
    {
        if (CodeName.Text.Length <= 3 || !Char.IsLetter(CodeName.Text[0]))
        {
            CodeNameRules.BackColor = Color.HotPink;
            ErrorMessage.Text = "Please Fix Your CodeName! <br/>";
        }
        int redFroot, yellowFroot, orangeFroot;
        if (!int.TryParse(RedFroot.Text, out redFroot))
        {
            RedFrootRules.BackColor = Color.HotPink;
            ErrorMessage.Text += "Please Fix Your Red Froot Order! <br/>";
        }
        if (!int.TryParse(YellowFroot.Text, out yellowFroot))
        {
            YellowFrootRules.BackColor = Color.HotPink;
            ErrorMessage.Text += "Please Fix Your Yellow Froot Order! <br/>";
        }
        if (!int.TryParse(OrangeFroot.Text, out orangeFroot))
        {
            OrangeFrootRules.BackColor = Color.HotPink;
            ErrorMessage.Text += "Please Fix Your Orange Froot Order! <br/>";
        }

        if (ErrorMessage.Text.Length == 0 && (redFroot + yellowFroot + orangeFroot) == 0)
        {
            ErrorMessage.Text = CodeName.Text + ", Why you no want Froot?<br/><br/>";
        }

        if (ErrorMessage.Text.Length == 0)
        {
            SuccessMessage.Text = "Thanks " + CodeName.Text + "!<br/>You ordered:<br/>&nbsp;&nbsp;&nbsp;" + redFroot +
                                  " Red Froot<br/>&nbsp;&nbsp;&nbsp;" +
                                  yellowFroot + " Yellow Frooot<br/>&nbsp;&nbsp;&nbsp;"
                                  + orangeFroot + " Orange Froot    <br/><b>You Rool!</b>";
        }
    }

    protected void ValidateCodeNameLength(object sender, ServerValidateEventArgs args)
    {
        if( args.Value.Length < 4)
        {
            args.IsValid = false;
        }
    }

    protected void ValidateCodeNameStartsWithCharacter(object source, ServerValidateEventArgs args)
    {
        if(!Char.IsLetter(args.Value[0]))
        {
            args.IsValid = false;
        }
    }

    protected void ValidateNumber(object source, ServerValidateEventArgs args)
    {
        int num;
        if (!int.TryParse(args.Value, out num))
        {
            args.IsValid = false;
        }
   } 


    protected void BuildSussessMessage(object sender, EventArgs e)
    {
        if (IsValid)
        {
            SuccessMessage2.Text = "Thanks " + CodeName2.Text + "!<br/>You ordered:<br/>&nbsp;&nbsp;&nbsp;" +
                                   RedFroot2.Text + " Red Froot<br/>&nbsp;&nbsp;&nbsp;" +
                                   YellowFroot2.Text + " Yellow Frooot<br/>&nbsp;&nbsp;&nbsp;"
                                   + OrangeFroot2.Text + " Orange Froot    <br/><b>You Rool!</b>";
        }
    }
}
