using System;
using System.Web.UI.WebControls;

public partial class BasicDotNet_ValidationWithMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ValidateCodeNameLength(object sender, ServerValidateEventArgs args)
    {
        if (args.Value.Length < 4)
        {
            args.IsValid = false;
        }
    }

    protected void ValidateCodeNameStartsWithCharacter(object source, ServerValidateEventArgs args)
    {
        if (!Char.IsLetter(args.Value[0]))
        {
            args.IsValid = false;
        }
    }

    protected void ValidateNumber(object source, ServerValidateEventArgs args)
    {
        int num;
        if (!int.TryParse(args.Value, out num))
        {
            args.IsValid = false;
        }
    }


    protected void BuildSussessMessage(object sender, EventArgs e)
    {
        if (IsValid)
        {
            SuccessMessage2.Text = "Thanks " + CodeName2.Text + "!<br/>You ordered:<br/>&nbsp;&nbsp;&nbsp;" +
                                   RedFroot2.Text + " Red Froot<br/>&nbsp;&nbsp;&nbsp;" +
                                   YellowFroot2.Text + " Yellow Frooot<br/>&nbsp;&nbsp;&nbsp;"
                                   + OrangeFroot2.Text + " Orange Froot    <br/><b>You Rool!</b>";
        }
    }

}
