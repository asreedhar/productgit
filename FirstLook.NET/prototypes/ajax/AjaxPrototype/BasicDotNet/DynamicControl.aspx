<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DynamicControl.aspx.cs" Inherits="BasicDotNet_DynamicControl" %>

<%@ Register TagPrefix="userControl" TagName="UpdatePanel" Src="~/BasicDotNet/UpdatePanelControl.ascx" %>
<%@ Register TagPrefix="userControl" TagName="Validation" Src="~/BasicDotNet/ValidationControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body bgcolor="e7e0b0">
    <form id="form1" runat="server">
    <div>
    
    <h1>Dynamic <blink><font color="blue">Froot</font><font color="red">r</font></blink></h1>
    
    How You Wanta Ya Froot?<br /><br />
    <table>
    <tr>
        <td align="center" style="background-color:#f7e654;">
            <asp:Button ID="ShowFrootzByColor" text="Froot By Color - Der" OnClick="HeWantsDaFrootChooser" runat="server" /><br />
            <asp:Button ID="ShowOrderFrootz" text="Order Frootz" OnClick="HeWantDaOrderFroot" runat="server" /><br />
            <asp:Button ID="ShowFrootzByColor2" text="More Froot By Color - Der" OnClick="HeWantsDaOtherFrootChooser" runat="server" /><br /> 
        </td>
        <td align="center" style="background-color:#a0bbd5;">
            <userControl:UpdatePanel ID="FrootzByColor" runat="server" Visible="false"/><br />
            <asp:Button ID="HideFrootzByColor" text="No More Froot By Color" OnClick="HeNoWantDaFrootChooser" runat="server" Visible="false"/>
        </td>
        <td align="center" style="background-color:#f8ad9b;">
            <userControl:Validation ID="FrootzCodeName" runat="server" Visible="false" />
            <asp:Button ID="HideOrderFrootz" text="Done With Frootr CodeName" OnClick="HeNoWantMoreOrder" runat="server" Visible="false"/>
        </td>
        <td align="center" style="background-color:#e1eef1;"> 
            <userControl:UpdatePanel ID="FrootzByColor2" runat="server" Visible="false"/><br />
            <asp:Button ID="HideFrootzByColor2" text="No More Froot By Color" OnClick="HeNoWantDaOtherFrootChooser" runat="server" Visible="false"/>
        </td>
    </tr>
    </table>
    </div>
        <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    <br /><br />
    <a href="../Default.aspx">HOME</a>
    </form>
</body>
</html>
