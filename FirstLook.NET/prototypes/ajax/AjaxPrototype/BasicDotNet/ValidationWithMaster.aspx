<%@ Page Language="C#" MasterPageFile="~/BasicDotNet/MasterPage.master" AutoEventWireup="true" CodeFile="ValidationWithMaster.aspx.cs" Inherits="BasicDotNet_ValidationWithMaster" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">

    <div style="background-color:#897793;">
        ASP Built In Validation<br /><br />
        <font color="blue">Froot</font><font color="red">r</font> CodeName:
        <asp:TextBox ID="CodeName2" runat="server"></asp:TextBox>
        <!-- client side validation examples -->
        <asp:RequiredFieldValidator ID="CodeNameValidator1" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="Please Enter a CodeName."
                SetFocusOnError="true" runat="server"/>
        <asp:RegularExpressionValidator ID="CodeNameValidator2" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="Numbers, Letters and _s only please."
                SetFocusOnError="true" ValidationExpression="\w*" runat="server" />
        
        <!-- serverside validation -->   
        <asp:CustomValidator ID="CodeNameValidator3" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="Please enter a name that start with a letter."
               OnServerValidate="ValidateCodeNameStartsWithCharacter"  SetFocusOnError="true" EnableClientScript="False"  runat="server"/>
        <asp:CustomValidator ID="CodeNameValidator4" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="CodeName must be longer than 3 Characters."
               OnServerValidate="ValidateCodeNameLength" SetFocusOnError="true" EnableClientScript="False"  runat="server"/>
        
        <br /><br />
        How many <font color="red">red</font> froot:
        <asp:TextBox ID="RedFroot2" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RedFrootValidator1" ControlToValidate="RedFroot2" Display="Dynamic" ErrorMessage="Please enter a Number"
            SetFocusOnError="true" runat="server" />
        <asp:CustomValidator ID="RedFrootValidator2" ControlToValidate="RedFroot2" Display="Dynamic" ErrorMessage="Digitz Only!"
            OnServerValidate="ValidateNumber" SetFocusOnError="true" EnableClientScript="false" runat="server" />
        <br />
        How many <font color="yellow">yellow</font> froot:
        <asp:TextBox ID="YellowFroot2" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="YellowFrootValidator1" ControlToValidate="YellowFroot2" Display="Dynamic" ErrorMessage="Please enter a Number"
            SetFocusOnError="true" runat="server" />
        <asp:CustomValidator ID="YellowFrootValidator2" ControlToValidate="YellowFroot2" Display="Dynamic" ErrorMessage="Digitz Only!"
            OnServerValidate="ValidateNumber" SetFocusOnError="true" EnableClientScript="false" runat="server" />
            
        <br />
        How many <font color="orange">orange</font> froot:
        <asp:TextBox ID="OrangeFroot2" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="OrangeFrootValidator1" ControlToValidate="OrangeFroot2" Display="Dynamic" ErrorMessage="Please enter a Number"
            SetFocusOnError="true" runat="server" />
        <asp:CustomValidator ID="OrangeFrootValidator2" ControlToValidate="OrangeFroot2" Display="Dynamic" ErrorMessage="Digitz Only!"
            OnServerValidate="ValidateNumber" SetFocusOnError="true" EnableClientScript="false" runat="server" />
        <br /><br />
        
        <asp:Button ID="Button1" runat="server" Text="Froot Me!" CausesValidation="true" OnClick="BuildSussessMessage"/>
        <br /><br />
        
        <asp:ValidationSummary ID="ValidationSummary" ShowSummary="true" DisplayMode="BulletList" runat="server" />
        
        <asp:Label ID="SuccessMessage2" EnableViewState="false" runat="server" />
        
        <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    
    </div>


</asp:Content>

