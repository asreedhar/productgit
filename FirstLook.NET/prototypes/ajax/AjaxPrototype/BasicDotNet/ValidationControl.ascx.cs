using System;
using System.Web.UI.WebControls;

public partial class BasicDotNet_ValidationControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }


    protected void ValidateCodeNameLength(object sender, ServerValidateEventArgs args)
    {
        if (args.Value.Length < 4)
        {
            args.IsValid = false;
        }
    }

    protected void ValidateCodeNameStartsWithCharacter(object source, ServerValidateEventArgs args)
    {
        if (!Char.IsLetter(args.Value[0]))
        {
            args.IsValid = false;
        }
    }

    protected void BuildSussessMessage(object sender, EventArgs e)
    {
        SuccessMessage.Text = "Thanks " + CodeName2.Text + "!";
    }
}
