<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdatePanel.aspx.cs" Inherits="BasicDotNet_UpdatePanel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Frootr - the Frooture is here</title>
</head>
<body bgcolor="f9e4e3">
    <form id="form1" runat="server">
    <div>
        <h1><blink><font color="blue">Froot</font><font color="red">r</font></blink></h1>
        <h3>You Picka da Color, We Picka da Fruit!</h3>

    <asp:ObjectDataSource
        ID="ColorsDataSource"
        TypeName="UpdatePanelDataSource"
        SelectMethod="FindColors"
        runat="server" />

    <asp:DropDownList ID="ColorsDropDown" DataSourceID="ColorsDataSource" runat="server" AppendDataBoundItems="true">
        <asp:ListItem Text="Please Select" Selected="True" Value="" />
    </asp:DropDownList>
    <asp:Button ID="ColorsButton" Text="Frootz!" runat="server" />
<br/><br/>
    <asp:ObjectDataSource
        ID="FrootDataSource"
        TypeName="UpdatePanelDataSource"
        SelectMethod="FindFroots"
        runat="server">
        <SelectParameters>
            <asp:ControlParameter ControlID="ColorsDropDown" Type="string" Name="color"/>
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:GridView ID="FrootsList" DataSourceID="FrootDataSource" runat="server" >
    </asp:GridView>
    <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    <br /><br />
    <a href="../Default.aspx">HOME</a>
    </div>
    </form>
</body>
</html>
