<%@ Page Language="C#" MasterPageFile="~/BasicDotNet/MasterPage.master" AutoEventWireup="true" CodeFile="UpdatePanelWithMaster.aspx.cs" Inherits="BasicDotNet_UpdatePanelWithMaster" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">

    <div style="background-color:#8d6653;">
        <h1><blink><font color="blue">Froot</font><font color="red">r</font></blink></h1>
        <h3>You Picka da Color, We Picka da Fruit!</h3>

    <asp:ObjectDataSource
        ID="ColorsDataSource"
        TypeName="UpdatePanelDataSource"
        SelectMethod="FindColors"
        runat="server" />

    <asp:DropDownList ID="ColorsDropDown" DataSourceID="ColorsDataSource" runat="server" AppendDataBoundItems="true">
        <asp:ListItem Text="Please Select" Selected="True" Value="" />
    </asp:DropDownList>
    <asp:Button ID="ColorsButton" Text="Frootz!" runat="server" />
<br/><br/>
    <asp:ObjectDataSource
        ID="FrootDataSource"
        TypeName="UpdatePanelDataSource"
        SelectMethod="FindFroots"
        runat="server">
        <SelectParameters>
            <asp:ControlParameter ControlID="ColorsDropDown" Type="string" Name="color" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:GridView ID="FrootsList" DataSourceID="FrootDataSource" runat="server" >
    </asp:GridView>
    <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    </div>

</asp:Content>

