<%@ Page Language="C#" MasterPageFile="~/BasicDotNet/MasterPage.master" AutoEventWireup="true" CodeFile="DynamicControlWithMaster.aspx.cs" Inherits="BasicDotNet_DynamicControlWithMaster" Title="Untitled Page" %>

<%@ Register TagPrefix="userControl" TagName="UpdatePanel" Src="~/BasicDotNet/UpdatePanelControl.ascx" %>
<%@ Register TagPrefix="userControl" TagName="Validation" Src="~/BasicDotNet/ValidationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">

    <div style="background-color:#e88209;">
    
    <h1>Dynamic <blink><font color="blue">Froot</font><font color="red">r</font></blink></h1>
    
    How You Wanta Ya Froot?<br /><br />
    <table>
    <tr>
        <td align="center" style="background-color:#fc8f9d;">
            <asp:Button ID="ShowFrootzByColor" text="Froot By Color - Der" OnClick="HeWantsDaFrootChooser" runat="server" /><br />
            <asp:Button ID="ShowOrderFrootz" text="Order Frootz" OnClick="HeWantDaOrderFroot" runat="server" /><br />
            <asp:Button ID="ShowFrootzByColor2" text="More Froot By Color - Der" OnClick="HeWantsDaOtherFrootChooser" runat="server" /><br /> 
        </td>
        <td align="center" style="background-color:#b33607;">
            <userControl:UpdatePanel ID="FrootzByColor" runat="server" Visible="false"/><br />
            <asp:Button ID="HideFrootzByColor" text="No More Froot By Color" OnClick="HeNoWantDaFrootChooser" runat="server" Visible="false"/>
        </td>
        <td align="center" style="background-color:#82c438;">
            <userControl:Validation ID="FrootzCodeName" runat="server" Visible="false" />
            <asp:Button ID="HideOrderFrootz" text="Done With Frootr CodeName" OnClick="HeNoWantMoreOrder" runat="server" Visible="false"/>
        </td>
        <td align="center" style="background-color:#bb95cd;"> 
            <userControl:UpdatePanel ID="FrootzByColor2" runat="server" Visible="false"/><br />
            <asp:Button ID="HideFrootzByColor2" text="No More Froot By Color" OnClick="HeNoWantDaOtherFrootChooser" runat="server" Visible="false"/>
        </td>
    </tr>
    </table>
    
        <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    </div>

</asp:Content>

