<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ValidationControl.ascx.cs" Inherits="BasicDotNet_ValidationControl" %>


<font color="blue">Froot</font><font color="red">r</font> CodeName:
<asp:TextBox ID="CodeName2" ValidationGroup="ValidationControlGroup" runat="server"></asp:TextBox><br /><br />


<!-- client side validation examples -->
<asp:RequiredFieldValidator ID="CodeNameValidator1" ControlToValidate="CodeName2" ErrorMessage="Please Enter a CodeName."
        SetFocusOnError="true" ValidationGroup="ValidationControlGroup" Display="none" runat="server"/>
<asp:RegularExpressionValidator ID="CodeNameValidator2" ControlToValidate="CodeName2" Display="none" ErrorMessage="Numbers, Letters and _s only please."
        SetFocusOnError="true" ValidationExpression="\w*" ValidationGroup="ValidationControlGroup"  runat="server" />

<!-- serverside validation -->   
<asp:CustomValidator ID="CodeNameValidator3" ControlToValidate="CodeName2" Display="none" ErrorMessage="Please enter a name that start with a letter."
       OnServerValidate="ValidateCodeNameStartsWithCharacter"  SetFocusOnError="true" EnableClientScript="False" ValidationGroup="ValidationControlGroup" runat="server"/>
<asp:CustomValidator ID="CodeNameValidator4" ControlToValidate="CodeName2" Display="none" ErrorMessage="CodeName must be longer than 3 Characters."
       OnServerValidate="ValidateCodeNameLength" SetFocusOnError="true" EnableClientScript="False" ValidationGroup="ValidationControlGroup"  runat="server"/>

<asp:ValidationSummary ID="ValidationSummary" ValidationGroup="ValidationControlGroup" ShowSummary="true" DisplayMode="BulletList" runat="server" />

<asp:Button ID="ValidationControlButton" CausesValidation="true" Text="Set Frootr CodeName" ValidationGroup="ValidationControlGroup"  OnClick="BuildSussessMessage" runat="server" /><br /><br />

<asp:Label ID="SuccessMessage" EnableViewState="false" runat="server"/><br /><br />

    This page was last rendered at: <%= DateTime.Now %><br /><br />