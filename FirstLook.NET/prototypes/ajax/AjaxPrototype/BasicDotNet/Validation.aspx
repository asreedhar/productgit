<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Validation.aspx.cs" Inherits="BasicDotNet_Validation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Frootr - the Frooture is here</title>
</head>
<body bgcolor="c1c4a5">
    <form id="form1" runat="server">

    
    <h1><blink><font color="blue">Froot</font><font color="red">r</font></blink></h1>
    <h3>Hey Buddy, How Mucha Froot You Need?</h3>
    
    <font color="red"><asp:Label ID="ErrorMessage" EnableViewState="false" runat="server" /></font>
    
    <div style="background-color:#6cfcbb;">
        Home Rolled Validation<br /><br />
        <font color="blue">Froot</font><font color="red">r</font> CodeName:
        <asp:TextBox ID="CodeName" runat="server"></asp:TextBox>
        <asp:Label ID="CodeNameRules" Font-Size="X-Small" EnableViewState="false" runat="server">(please enter a name longer than 3 letters and must start with a letter)</asp:Label>
        
        <br /><br />
        How many <font color="red">red</font> froot:
        <asp:TextBox ID="RedFroot" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:Label ID="RedFrootRules" Font-Size="X-Small" runat="server">(Numbers only please)</asp:Label>
        <br />
        How many <font color="yellow">yellow</font> froot:
        <asp:TextBox ID="YellowFroot" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:Label ID="YellowFrootRules" EnableViewState="false"  Font-Size="X-Small" runat="server">(Numbers only please)</asp:Label>
        <br />
        How many <font color="orange">orange</font> froot:
        <asp:TextBox ID="OrangeFroot" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:Label ID="OrangeFrootRules" EnableViewState="false"  Font-Size="X-Small" runat="server">(Numbers only please)</asp:Label>
        <br /><br />
        
        <asp:Button runat="server" Text="Froot Me!" OnClick="HomeRolledValidate_Click"/>
        <br /><br />
        
        <asp:Label ID="SuccessMessage" EnableViewState="false"  runat="server" />
    </div>
    
    
    <div style="background-color:#a9c78d;">
        ASP Built In Validation<br /><br />
        <font color="blue">Froot</font><font color="red">r</font> CodeName:
        <asp:TextBox ID="CodeName2" runat="server"></asp:TextBox>
        <!-- client side validation examples -->
        <asp:RequiredFieldValidator ID="CodeNameValidator1" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="Please Enter a CodeName."
                SetFocusOnError="true" runat="server"/>
        <asp:RegularExpressionValidator ID="CodeNameValidator2" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="Numbers, Letters and _s only please."
                SetFocusOnError="true" ValidationExpression="\w*" runat="server" />
        
        <!-- serverside validation -->   
        <asp:CustomValidator ID="CodeNameValidator3" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="Please enter a name that start with a letter."
               OnServerValidate="ValidateCodeNameStartsWithCharacter"  SetFocusOnError="true" EnableClientScript="False"  runat="server"/>
        <asp:CustomValidator ID="CodeNameValidator4" ControlToValidate="CodeName2" Display="Dynamic" ErrorMessage="CodeName must be longer than 3 Characters."
               OnServerValidate="ValidateCodeNameLength" SetFocusOnError="true" EnableClientScript="False"  runat="server"/>
        
        <br /><br />
        How many <font color="red">red</font> froot:
        <asp:TextBox ID="RedFroot2" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RedFrootValidator1" ControlToValidate="RedFroot2" Display="Dynamic" ErrorMessage="Please enter a Number"
            SetFocusOnError="true" runat="server" />
        <asp:CustomValidator ID="RedFrootValidator2" ControlToValidate="RedFroot2" Display="Dynamic" ErrorMessage="Digitz Only!"
            OnServerValidate="ValidateNumber" SetFocusOnError="true" EnableClientScript="false" runat="server" />
        <br />
        How many <font color="yellow">yellow</font> froot:
        <asp:TextBox ID="YellowFroot2" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="YellowFrootValidator1" ControlToValidate="YellowFroot2" Display="Dynamic" ErrorMessage="Please enter a Number"
            SetFocusOnError="true" runat="server" />
        <asp:CustomValidator ID="YellowFrootValidator2" ControlToValidate="YellowFroot2" Display="Dynamic" ErrorMessage="Digitz Only!"
            OnServerValidate="ValidateNumber" SetFocusOnError="true" EnableClientScript="false" runat="server" />
            
        <br />
        How many <font color="orange">orange</font> froot:
        <asp:TextBox ID="OrangeFroot2" runat="server" Text="0" MaxLength="2" Width="25"></asp:TextBox>
        <asp:RequiredFieldValidator ID="OrangeFrootValidator1" ControlToValidate="OrangeFroot2" Display="Dynamic" ErrorMessage="Please enter a Number"
            SetFocusOnError="true" runat="server" />
        <asp:CustomValidator ID="OrangeFrootValidator2" ControlToValidate="OrangeFroot2" Display="Dynamic" ErrorMessage="Digitz Only!"
            OnServerValidate="ValidateNumber" SetFocusOnError="true" EnableClientScript="false" runat="server" />
        <br /><br />
        
        <asp:Button ID="Button1" runat="server" Text="Froot Me!" CausesValidation="true" OnClick="BuildSussessMessage"/>
        <br /><br />
        
        <asp:ValidationSummary ID="ValidationSummary" ShowSummary="true" DisplayMode="BulletList" runat="server" />
        
        <asp:Label ID="SuccessMessage2" EnableViewState="false" runat="server" />
    
    </div>
    <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    <br /><br />
    <a href="../Default.aspx">HOME</a>

    </form>
</body>
</html>
