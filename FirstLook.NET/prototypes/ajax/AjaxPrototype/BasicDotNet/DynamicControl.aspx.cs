using System;

public partial class BasicDotNet_DynamicControl : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void HeWantsDaFrootChooser(object sender, EventArgs e)
    {
        FrootzByColor.Visible = true;
        ShowFrootzByColor.Enabled = false;
        HideFrootzByColor.Visible = true;
    }

    protected void HeNoWantDaFrootChooser(object sender, EventArgs e)
    {
        FrootzByColor.Visible = false;
        HideFrootzByColor.Visible = false;
        ShowFrootzByColor.Enabled = true;
    }

    protected void HeWantsDaOtherFrootChooser(object sender, EventArgs e)
    {
        ShowFrootzByColor2.Enabled = false;
        FrootzByColor2.Visible = true;
        HideFrootzByColor2.Visible = true;
    }

    protected void HeNoWantDaOtherFrootChooser(object sender, EventArgs e)
    {
        ShowFrootzByColor2.Enabled = true;
        FrootzByColor2.Visible = false;
        HideFrootzByColor2.Visible = false;
    }

    protected void HeWantDaOrderFroot(object sender, EventArgs e)
    {
        ShowOrderFrootz.Enabled = false;
        FrootzCodeName.Visible = true;
        HideOrderFrootz.Visible = true;
    }

    protected void HeNoWantMoreOrder(object sender, EventArgs e)
    {
        ShowOrderFrootz.Enabled = true;
        FrootzCodeName.Visible = false;
        HideOrderFrootz.Visible = false;
    }
}
