using System;
using System.Xml;
using Microsoft.Web.Preview.Services;

/// <summary>
/// Summary description for PrinceBridgeFile
/// </summary>
public partial class PrinceBridgeFile : BridgeHandler
{
    public override void TransformRequest()
    {
        string html = this.BridgeRequest.Args["HTML"] as string;
        base.TransformRequest();
    }

    public override void TransformResponse()
    {
        string responseXML = this.ServiceResponse.Response as string;
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(responseXML);

        //Check for errors returned by flickr
//        string responseStatus = doc.SelectSingleNode("/rsp/@stat").Value;
//
//        if (string.Compare(responseStatus, "ok", true) != 0)
//        {
//            throw new Exception(doc.SelectSingleNode("/rsp/err/@msg").Value);
//        }

        base.TransformResponse();
    }
}
