using System;
using System.IO;
using System.Text;
using System.Web.UI;

public partial class AjaxDotNet_AjaxWebServiceCall : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
// this calls our prince xml service and returns the pdf response

//        SuperService service = new SuperService();
//        byte[] content = service.test();
//
//        Response.Clear(); 
//        Response.AddHeader("Content-Disposition", "attachment; filename=test.pdf"); 
//        Response.AddHeader("Content-Length", content.Length.ToString()); 
//        Response.ContentType = "application/pdf"; 
//        Response.BinaryWrite(content); 
//        Response.End();
    }

    protected void PdfMe(object sender, EventArgs e)
    {
        Label.Text = "pdf me";
        Print.Value = "true";
//        SuperService service = new SuperService();
//        byte[] content = service.test();
//
//        Response.Clear(); 
//        Response.AddHeader("Content-Disposition", "attachment; filename=test.pdf"); 
//        Response.AddHeader("Content-Length", content.Length.ToString()); 
//        Response.ContentType = "application/pdf"; 
//        Response.BinaryWrite(content); 
//        Response.End();
    }

    protected override void Render(HtmlTextWriter writer)
    {
        
        StringBuilder sbOut = new StringBuilder();
        StringWriter swOut = new StringWriter(sbOut);
        HtmlTextWriter htwOut = new HtmlTextWriter(swOut);
        base.Render(htwOut);
        string sOut = sbOut.ToString();

        if ("true".Equals(Print.Value))
        {
            Print.Value = "false";
            SuperService service = new SuperService();
            byte[] content = service.test(sOut);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=test.pdf");
            Response.AddHeader("Content-Length", content.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(content);
            Response.End();
        }
        else
        {
            writer.Write(sOut);
        }
    }


    private void Page_PreRender(object sender, EventArgs e)
    {
        if(IsPostBack)
        {
            Label.Text = "Unload";
        }
    }

}
