using System;

public partial class AjaxDotNet_PdfGenerator : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SuperService service = new SuperService();
        string html = "<html><head></head><body>" + Request["body"] + "</body></html>";
        byte[] content = service.test(html);
        Response.Clear(); 
        Response.AddHeader("Content-Disposition", "attachment; filename=test.pdf"); 
        Response.AddHeader("Content-Length", content.Length.ToString()); 
        Response.ContentType = "application/pdf"; 
        Response.BinaryWrite(content); 
        Response.End();
    }
}
