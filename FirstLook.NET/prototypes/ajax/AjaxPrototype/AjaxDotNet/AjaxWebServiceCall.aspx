<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AjaxWebServiceCall.aspx.cs" Inherits="AjaxDotNet_AjaxWebServiceCall" %>

<%@ Register Assembly="Microsoft.Web.DynamicDataControls" Namespace="Microsoft.Web.DynamicDataControls"
    TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
    <script language="javascript" type="text/javascript">
        function callServer() {
            SuperService.ShowMeWatchaGot(OnComplete, OnTimeOut, OnError);
            return false;
        }

        function OnTimeOut(arg) {
        alert("TimeOut encountered when calling Say Hello.");
        }

        function OnError(arg) {
        alert("Error encountered when calling Say Hello.");
        }
        
        function populateBodyField() {
            alert(escape(document.body.innerHTML));
            document.form1.body.value = document.body.innerHTML;
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ScriptManager ID="ScriptManager" runat="server">
            <Scripts >
                <asp:ScriptReference Path="~/prototype.js" />
            </Scripts>
            <Services>
                <asp:ServiceReference Path="SuperService.asmx" />
            </Services>
        </asp:ScriptManager>

        <input type="button" id="Button1" value="Show Me Watch Got Server!" onclick="callServer();" />
        
        Server Says: <div id="serverResponse"></div>
        <br /><br />
        <asp:LinkButton ID="link" runat="server" Text="generatePdf with redirect please" PostBackUrl="~/AjaxDotNet/PdfGenerator.aspx" /><br /><br />
        <br /><br />
        <asp:LinkButton ID="LinkButton1" runat="server" Text="generatePdf with post back and hijacked render method please" OnClick="PdfMe" /><br /><br />
        
        <input type="hidden" id="body" value="value" runat="server"/>
        
        <asp:HiddenField ID="Print" Value="false" runat="server" />
        <asp:Label ID="Label" runat="server" />
    </div>
    </form>
</body>
</html>
