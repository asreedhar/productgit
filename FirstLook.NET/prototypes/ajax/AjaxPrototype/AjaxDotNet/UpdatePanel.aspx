<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdatePanel.aspx.cs" Inherits="AjaxDotNet_UpdatePanel" %>

<%@ Register Assembly="Microsoft.Web.Preview" Namespace="Microsoft.Web.Preview.Diagnostics"
    TagPrefix="cc" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body bgcolor="f9e4e3">
    <form id="form1" runat="server">
    <div>
        <h1><blink><font color="blue">Froot</font><font color="red">r</font></blink></h1>
        <h3>You Picka da Color, We Picka da Fruit!</h3>

    <asp:ObjectDataSource
        ID="ColorsDataSource"
        TypeName="UpdatePanelDataSource"
        SelectMethod="FindColors"
        runat="server" />
         
    <asp:DropDownList ID="ColorsDropDown" DataSourceID="ColorsDataSource" runat="server" AppendDataBoundItems="true">
        <asp:ListItem Text="Please Select" Selected="True" Value="" />
    </asp:DropDownList>
    
    <asp:Button ID="ColorsButton" Text="Frootz!" OnClick="SleepFor3Seconds" runat="server" />
    
<br/><br/>

<asp:ScriptManager ID="UpdateScriptManager" runat="server" />

<!-- This script allows a button outside of the updatePanel to trigger the update progress action.
        If you move the ColorsButton inside the UpdatePanel, you don't need this script.  -->
<script type="text/javascript"> 
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(function (sender, args) { 
        if (args.get_postBackElement().id == '<%= ColorsButton.ClientID %>') { 
            $find('<%= UpdateProgressFroots.ClientID %>').get_element().style.display = 'block'; 
        } 
    }); 
</script>

<asp:UpdateProgress ID="UpdateProgressFroots" AssociatedUpdatePanelID="UpdateFrootsList" runat="server">
    <ProgressTemplate>
        <img src="progress_bar.gif" />
    </ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel ID="UpdateFrootsList"  UpdateMode="Conditional" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ColorsButton" EventName="Click" />
    </Triggers>
    <ContentTemplate>
        <asp:ObjectDataSource
            ID="FrootDataSource"
            TypeName="UpdatePanelDataSource"
            SelectMethod="FindFroots"
            runat="server">
            <SelectParameters>
                <asp:ControlParameter ControlID="ColorsDropDown" Type="string" Name="color"/>
            </SelectParameters>
        </asp:ObjectDataSource>
        
        <asp:GridView ID="FrootsList" DataSourceID="FrootDataSource" runat="server" >
        </asp:GridView>

        <br /><br />
        This pane was last rendered at: <%= DateTime.Now %>
    </ContentTemplate>
</asp:UpdatePanel>

    <br /><br />
    This page was last rendered at: <%= DateTime.Now %>
    <br /><br />
    <a href="../Default.aspx">HOME</a>
    </div>
    </form>
    
</body>
    
    

</html>
