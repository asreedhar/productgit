<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrinceWebServiceCall.aspx.cs" Inherits="AjaxDotNet_PrinceWebServiceCall" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
    <script language="javascript" type="text/javascript">
        function PrincePlease()
        {
            PrinceXmlWebService.generate({"HTML":"<html><head>hi</head><body>body</body></html>"}, OnSuccess, OnNotSuccess);
        }
        function OnSuccess(results, response, context) {
            alert(results);
        }
        function OnNotSuccess(args) {
            alert(typeof(args));
        }
            
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
               <asp:ServiceReference Path="~/AjaxDotNet/PrinceBridgeFile.asbx" /> 
            </Services>
        </asp:ScriptManager>
        
        <input type="button" value="Click to Post to Prince" onclick="PrincePlease()" />
        <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px">
            <asp:Label ID="OutPut" runat="server"></asp:Label>
        </asp:Panel>
        
    <div>
    
    </div>
    </form>
</body>
</html>
