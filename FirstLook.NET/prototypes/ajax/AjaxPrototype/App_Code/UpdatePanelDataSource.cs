using System.Collections.Generic;

/// <summary>
/// Summary description for UpdatePanelDataSource
/// </summary>
public class UpdatePanelDataSource
{
	public UpdatePanelDataSource()
	{
	}

    public string[] FindColors()
    {
        string[] colors = {"red", "yellow", "orange"};
        return colors;
    }

    public string[] FindFroots(string color)
    {   
        List<string> froots = new List<string>();
        switch(color)
        {
            case "red":
                froots.Add("red apple");
                froots.Add("cherry");
                froots.Add("strawberry");
                break;
            case "yellow":
                froots.Add("yellow apple");
                froots.Add("squash");
                froots.Add("banana");
                break;
            case "orange":
                froots.Add("orange apple");
                froots.Add("orange");
                froots.Add("really orange");
                break;
        }
        return froots.ToArray();
    }
}
