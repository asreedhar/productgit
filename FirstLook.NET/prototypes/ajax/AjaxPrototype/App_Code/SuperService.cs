using System.IO;
using System.Web.Services;
using System.Web.UI;


/// <summary>
/// Summary description for SuperService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public class SuperService : System.Web.Services.WebService {

    public SuperService () {
    }

    [WebMethod]
    public string ShowMeWatchaGot()
    {
        return "Boo-yah!";
    }

    public byte[] test(string pagesHTML)
    {
        PrinceXmlWebServiceProxy service = new PrinceXmlWebServiceProxy();
        service.Url = "http://dweintrop01.firstlook.biz:8090/firstlook-queue-ws/services/PrinceXmlWebService";
        Input input = new Input();
        pagesHTML = File.ReadAllText(@"c:\Projects-HEAD\work_bench\test2.html");
        input.Content = pagesHTML; //"<html><head></head><body>test</body></html>";
        input.StyleSheetPaths = new string[0];
        input.ContentType = ContentType.Html;
        OutputType outputType = new OutputType();
        outputType.OutputTypeCode = OutputTypeCode.ByteStream;
        outputType.Item = new SharedFileSystem();
        Output output = new Output();
        Status status = service.generate(input, outputType, out output);
        return (byte[]) output.Item;
    }
    
}

