using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Web.Scripting;
using Microsoft.Web.DynamicDataControls;

public partial class PrintAdvertiser_ThirdPartyEntity : Microsoft.Web.DynamicDataControls.DynamicDataPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override IEnumerable GetColumns()
    {
        return new object[] { "PrintAdvertiserID", "ContactEmail", "VehicleOptionThirdPartyID" };
    }

    /* The following methods can be implemented here:
    
    GetDetailsColumns()
    GetRSSFields()
    InitRow(row)
    InitDataSource(dataSource)
    */

}
