﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Hurray for .NET!</title>
</head>
<body bgcolor="f2dd8a">
    <form id="form1" runat="server">
    <div>
    <h2>Basic .Net Implementation</h2>
    <ul>
        <li><a href="BasicDotNet/UpdatePanel.aspx">Use Case 1: Update Panel</a></li>
        <li><a href="BasicDotNet/Validation.aspx">Use Case 2: Validation</a></li>
        <li><a href="BasicDotNet/DynamicControl.aspx">Use Case 3: Dynamic Controls</a></li>
    </ul>
    <br />
    <h2>Basic .Net Implementation - with Master Pages</h2>
    <ul>
        <li><a href="BasicDotNet/UpdatePanelWithMaster.aspx">Use Case 1: Update Panel</a></li>
        <li><a href="BasicDotNet/ValidationWithMaster.aspx">Use Case 2: Validation</a></li>
        <li><a href="BasicDotNet/DynamicControlWithMaster.aspx">Use Case 3: Dynamic Controls</a></li>
    </ul>
     <h2>AJAX .Net Implementation Using AJAX Extension 1.0 only</h2>
    <ul>
        <li><a href="AjaxDotNet/UpdatePanel.aspx">Use Case 1: Update Panel</a></li>
        <li><a href="AjaxDotNet/AjaxWebServiceCall.aspx">Ajax Web Service Call</a></li>
    </ul>
    
    <h2>Dynamic Data Pages</h2>
    note: these pages point to devdb01/hal - all changes will actually be persisted, so proceed with care
    <ul>
        <li><a href="PrintAdvertiser_ThirdPartyEntity.aspx">Custom view of PrintAdvertiser_ThirdPartyEntity table</a></li>
        <li><a href="auto.axd">Auto Generated asp view of database</a></li>
    </ul>
    
    <br /><br />
    all the colors on this site are brought to you by: <a href="http://tbui01.firstlook.biz/colors.html">Tuan</a>
    </div>
    </form>
</body>
</html>
