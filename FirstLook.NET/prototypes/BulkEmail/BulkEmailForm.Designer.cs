namespace BulkEmail
{
    partial class BulkEmailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BulkEmailForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.DropDownButton_File = new System.Windows.Forms.ToolStripDropDownButton();
            this.MenuItem_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_InvalidAddress = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Report = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.DropDownButton_Edit = new System.Windows.Forms.ToolStripDropDownButton();
            this.MenuItem_Message = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_Attachment = new System.Windows.Forms.ToolStripMenuItem();
            this.Button_Send = new System.Windows.Forms.ToolStripButton();
            this.Contacts_Label = new System.Windows.Forms.Label();
            this.Contacts_TextBox = new System.Windows.Forms.TextBox();
            this.Message_Label = new System.Windows.Forms.Label();
            this.Message_TextBox = new System.Windows.Forms.TextBox();
            this.Attachment_Label = new System.Windows.Forms.Label();
            this.Attachment_TextBox = new System.Windows.Forms.TextBox();
            this.OpenFileDialog_Contacts = new System.Windows.Forms.OpenFileDialog();
            this.OpenFileDialog_Message = new System.Windows.Forms.OpenFileDialog();
            this.OpenFileDialog_Attachment = new System.Windows.Forms.OpenFileDialog();
            this.From_Label = new System.Windows.Forms.Label();
            this.From_TextBox = new System.Windows.Forms.TextBox();
            this.Reply_Label = new System.Windows.Forms.Label();
            this.Reply_TextBox = new System.Windows.Forms.TextBox();
            this.Subject_Label = new System.Windows.Forms.Label();
            this.Subject_TextBox = new System.Windows.Forms.TextBox();
            this.AttachName_Label = new System.Windows.Forms.Label();
            this.AttachName_TextBox = new System.Windows.Forms.TextBox();
            this.Host_Label = new System.Windows.Forms.Label();
            this.Host_TextBox = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SaveFileDialog_InvalidAddress = new System.Windows.Forms.SaveFileDialog();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DropDownButton_File,
            this.DropDownButton_Edit,
            this.Button_Send});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(451, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // DropDownButton_File
            // 
            this.DropDownButton_File.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DropDownButton_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_Open,
            this.MenuItem_Export,
            this.MenuItem_Exit});
            this.DropDownButton_File.Image = ((System.Drawing.Image)(resources.GetObject("DropDownButton_File.Image")));
            this.DropDownButton_File.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DropDownButton_File.Name = "DropDownButton_File";
            this.DropDownButton_File.Size = new System.Drawing.Size(36, 22);
            this.DropDownButton_File.Text = "&File";
            // 
            // MenuItem_Open
            // 
            this.MenuItem_Open.Name = "MenuItem_Open";
            this.MenuItem_Open.Size = new System.Drawing.Size(152, 22);
            this.MenuItem_Open.Text = "&Open";
            // 
            // MenuItem_Export
            // 
            this.MenuItem_Export.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_InvalidAddress,
            this.MenuItem_Report});
            this.MenuItem_Export.Name = "MenuItem_Export";
            this.MenuItem_Export.Size = new System.Drawing.Size(152, 22);
            this.MenuItem_Export.Text = "Export";
            // 
            // MenuItem_InvalidAddress
            // 
            this.MenuItem_InvalidAddress.Name = "MenuItem_InvalidAddress";
            this.MenuItem_InvalidAddress.Size = new System.Drawing.Size(178, 22);
            this.MenuItem_InvalidAddress.Text = "Invalid Address List";
            // 
            // MenuItem_Report
            // 
            this.MenuItem_Report.Name = "MenuItem_Report";
            this.MenuItem_Report.Size = new System.Drawing.Size(178, 22);
            this.MenuItem_Report.Text = "Report";
            // 
            // MenuItem_Exit
            // 
            this.MenuItem_Exit.Name = "MenuItem_Exit";
            this.MenuItem_Exit.Size = new System.Drawing.Size(152, 22);
            this.MenuItem_Exit.Text = "E&xit";
            // 
            // DropDownButton_Edit
            // 
            this.DropDownButton_Edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DropDownButton_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_Message,
            this.MenuItem_Attachment});
            this.DropDownButton_Edit.Image = ((System.Drawing.Image)(resources.GetObject("DropDownButton_Edit.Image")));
            this.DropDownButton_Edit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DropDownButton_Edit.Name = "DropDownButton_Edit";
            this.DropDownButton_Edit.Size = new System.Drawing.Size(38, 22);
            this.DropDownButton_Edit.Text = "&Edit";
            // 
            // MenuItem_Message
            // 
            this.MenuItem_Message.Name = "MenuItem_Message";
            this.MenuItem_Message.Size = new System.Drawing.Size(141, 22);
            this.MenuItem_Message.Text = "&Message";
            // 
            // MenuItem_Attachment
            // 
            this.MenuItem_Attachment.Name = "MenuItem_Attachment";
            this.MenuItem_Attachment.Size = new System.Drawing.Size(141, 22);
            this.MenuItem_Attachment.Text = "&Attachment";
            // 
            // Button_Send
            // 
            this.Button_Send.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Button_Send.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_Send.Enabled = false;
            this.Button_Send.Image = ((System.Drawing.Image)(resources.GetObject("Button_Send.Image")));
            this.Button_Send.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Send.Name = "Button_Send";
            this.Button_Send.Size = new System.Drawing.Size(35, 22);
            this.Button_Send.Text = "Send";
            // 
            // Contacts_Label
            // 
            this.Contacts_Label.AutoSize = true;
            this.Contacts_Label.Location = new System.Drawing.Point(12, 35);
            this.Contacts_Label.Name = "Contacts_Label";
            this.Contacts_Label.Size = new System.Drawing.Size(49, 13);
            this.Contacts_Label.TabIndex = 2;
            this.Contacts_Label.Text = "Contacts";
            // 
            // Contacts_TextBox
            // 
            this.Contacts_TextBox.Location = new System.Drawing.Point(92, 32);
            this.Contacts_TextBox.Name = "Contacts_TextBox";
            this.Contacts_TextBox.ReadOnly = true;
            this.Contacts_TextBox.Size = new System.Drawing.Size(347, 20);
            this.Contacts_TextBox.TabIndex = 3;
            this.Contacts_TextBox.Text = "None";
            // 
            // Message_Label
            // 
            this.Message_Label.AutoSize = true;
            this.Message_Label.Location = new System.Drawing.Point(12, 62);
            this.Message_Label.Name = "Message_Label";
            this.Message_Label.Size = new System.Drawing.Size(50, 13);
            this.Message_Label.TabIndex = 4;
            this.Message_Label.Text = "Message";
            // 
            // Message_TextBox
            // 
            this.Message_TextBox.Location = new System.Drawing.Point(92, 59);
            this.Message_TextBox.Name = "Message_TextBox";
            this.Message_TextBox.ReadOnly = true;
            this.Message_TextBox.Size = new System.Drawing.Size(347, 20);
            this.Message_TextBox.TabIndex = 5;
            this.Message_TextBox.Text = "None";
            // 
            // Attachment_Label
            // 
            this.Attachment_Label.AutoSize = true;
            this.Attachment_Label.Location = new System.Drawing.Point(12, 88);
            this.Attachment_Label.Name = "Attachment_Label";
            this.Attachment_Label.Size = new System.Drawing.Size(61, 13);
            this.Attachment_Label.TabIndex = 6;
            this.Attachment_Label.Text = "Attachment";
            // 
            // Attachment_TextBox
            // 
            this.Attachment_TextBox.Location = new System.Drawing.Point(92, 85);
            this.Attachment_TextBox.Name = "Attachment_TextBox";
            this.Attachment_TextBox.ReadOnly = true;
            this.Attachment_TextBox.Size = new System.Drawing.Size(347, 20);
            this.Attachment_TextBox.TabIndex = 7;
            this.Attachment_TextBox.Text = "None";
            // 
            // OpenFileDialog_Contacts
            // 
            this.OpenFileDialog_Contacts.DefaultExt = "txt";
            this.OpenFileDialog_Contacts.Filter = "Text Files|*.txt";
            this.OpenFileDialog_Contacts.Title = "Open Contacts";
            // 
            // OpenFileDialog_Message
            // 
            this.OpenFileDialog_Message.DefaultExt = "html";
            this.OpenFileDialog_Message.Filter = "HTML Files|*.html";
            this.OpenFileDialog_Message.Title = "Open Message";
            // 
            // OpenFileDialog_Attachment
            // 
            this.OpenFileDialog_Attachment.Title = "Open Attachment";
            // 
            // From_Label
            // 
            this.From_Label.AutoSize = true;
            this.From_Label.Location = new System.Drawing.Point(12, 117);
            this.From_Label.Name = "From_Label";
            this.From_Label.Size = new System.Drawing.Size(30, 13);
            this.From_Label.TabIndex = 9;
            this.From_Label.Text = "From";
            // 
            // From_TextBox
            // 
            this.From_TextBox.Location = new System.Drawing.Point(92, 114);
            this.From_TextBox.Name = "From_TextBox";
            this.From_TextBox.Size = new System.Drawing.Size(347, 20);
            this.From_TextBox.TabIndex = 10;
            this.From_TextBox.Text = "no-reply@firstlook.biz";
            // 
            // Reply_Label
            // 
            this.Reply_Label.AutoSize = true;
            this.Reply_Label.Location = new System.Drawing.Point(12, 143);
            this.Reply_Label.Name = "Reply_Label";
            this.Reply_Label.Size = new System.Drawing.Size(50, 13);
            this.Reply_Label.TabIndex = 11;
            this.Reply_Label.Text = "Reply To";
            // 
            // Reply_TextBox
            // 
            this.Reply_TextBox.Location = new System.Drawing.Point(92, 140);
            this.Reply_TextBox.Name = "Reply_TextBox";
            this.Reply_TextBox.Size = new System.Drawing.Size(347, 20);
            this.Reply_TextBox.TabIndex = 12;
            this.Reply_TextBox.Text = "no-reply@firstlook.biz";
            // 
            // Subject_Label
            // 
            this.Subject_Label.AutoSize = true;
            this.Subject_Label.Location = new System.Drawing.Point(12, 169);
            this.Subject_Label.Name = "Subject_Label";
            this.Subject_Label.Size = new System.Drawing.Size(43, 13);
            this.Subject_Label.TabIndex = 13;
            this.Subject_Label.Text = "Subject";
            // 
            // Subject_TextBox
            // 
            this.Subject_TextBox.Location = new System.Drawing.Point(92, 166);
            this.Subject_TextBox.Name = "Subject_TextBox";
            this.Subject_TextBox.Size = new System.Drawing.Size(347, 20);
            this.Subject_TextBox.TabIndex = 14;
            // 
            // AttachName_Label
            // 
            this.AttachName_Label.AutoSize = true;
            this.AttachName_Label.Location = new System.Drawing.Point(12, 195);
            this.AttachName_Label.Name = "AttachName_Label";
            this.AttachName_Label.Size = new System.Drawing.Size(69, 13);
            this.AttachName_Label.TabIndex = 15;
            this.AttachName_Label.Text = "Attach Name";
            // 
            // AttachName_TextBox
            // 
            this.AttachName_TextBox.Location = new System.Drawing.Point(92, 192);
            this.AttachName_TextBox.Name = "AttachName_TextBox";
            this.AttachName_TextBox.Size = new System.Drawing.Size(347, 20);
            this.AttachName_TextBox.TabIndex = 16;
            // 
            // Host_Label
            // 
            this.Host_Label.AutoSize = true;
            this.Host_Label.Location = new System.Drawing.Point(12, 221);
            this.Host_Label.Name = "Host_Label";
            this.Host_Label.Size = new System.Drawing.Size(57, 13);
            this.Host_Label.TabIndex = 17;
            this.Host_Label.Text = "Email Host";
            // 
            // Host_TextBox
            // 
            this.Host_TextBox.Location = new System.Drawing.Point(92, 218);
            this.Host_TextBox.Name = "Host_TextBox";
            this.Host_TextBox.Size = new System.Drawing.Size(347, 20);
            this.Host_TextBox.TabIndex = 18;
            this.Host_TextBox.Text = "ord1mail01.firstlook.biz";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(92, 248);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(347, 23);
            this.progressBar1.TabIndex = 19;
            // 
            // SaveFileDialog_InvalidAddress
            // 
            this.SaveFileDialog_InvalidAddress.DefaultExt = "txt";
            this.SaveFileDialog_InvalidAddress.Filter = "Text Files|*.txt";
            this.SaveFileDialog_InvalidAddress.Title = "Save Invalid Address List";
            // 
            // BulkEmailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 283);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.Host_TextBox);
            this.Controls.Add(this.Host_Label);
            this.Controls.Add(this.AttachName_TextBox);
            this.Controls.Add(this.AttachName_Label);
            this.Controls.Add(this.Subject_TextBox);
            this.Controls.Add(this.Subject_Label);
            this.Controls.Add(this.Reply_TextBox);
            this.Controls.Add(this.Reply_Label);
            this.Controls.Add(this.From_TextBox);
            this.Controls.Add(this.From_Label);
            this.Controls.Add(this.Attachment_TextBox);
            this.Controls.Add(this.Attachment_Label);
            this.Controls.Add(this.Message_TextBox);
            this.Controls.Add(this.Message_Label);
            this.Controls.Add(this.Contacts_TextBox);
            this.Controls.Add(this.Contacts_Label);
            this.Controls.Add(this.toolStrip1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BulkEmailForm";
            this.Text = "Bulk Email";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton DropDownButton_File;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Open;
        private System.Windows.Forms.ToolStripDropDownButton DropDownButton_Edit;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Message;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Attachment;
        private System.Windows.Forms.ToolStripButton Button_Send;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Export;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_InvalidAddress;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Report;
        private System.Windows.Forms.Label Contacts_Label;
        private System.Windows.Forms.TextBox Contacts_TextBox;
        private System.Windows.Forms.Label Message_Label;
        private System.Windows.Forms.TextBox Message_TextBox;
        private System.Windows.Forms.Label Attachment_Label;
        private System.Windows.Forms.TextBox Attachment_TextBox;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_Exit;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog_Contacts;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog_Message;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog_Attachment;
        private System.Windows.Forms.Label From_Label;
        private System.Windows.Forms.TextBox From_TextBox;
        private System.Windows.Forms.Label Reply_Label;
        private System.Windows.Forms.TextBox Reply_TextBox;
        private System.Windows.Forms.Label Subject_Label;
        private System.Windows.Forms.TextBox Subject_TextBox;
        private System.Windows.Forms.Label AttachName_Label;
        private System.Windows.Forms.TextBox AttachName_TextBox;
        private System.Windows.Forms.Label Host_Label;
        private System.Windows.Forms.TextBox Host_TextBox;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog_InvalidAddress;
    }
}

