using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Windows.Forms;

namespace BulkEmail
{
    public partial class BulkEmailForm : Form
    {
        private bool haveContactsFile;
        private bool haveMessageFile;
        private bool haveAttachmentFile;

        private MailAddress from;
        private MailAddress reply;
        private string subject = string.Empty;

        private readonly List<MailAddress> recipients = new List<MailAddress>();
        private string messageText;

        private readonly List<string> invalidAddresses = new List<string>();

        public BulkEmailForm()
        {
            InitializeComponent();

            Load += BulkEmailForm_Load;
        }

        private void BulkEmailForm_Load(object sender, EventArgs e)
        {
            // menu events
            MenuItem_Open.Click += MenuItem_Open_Click;
            MenuItem_Message.Click += MenuItem_Message_Click;
            MenuItem_Attachment.Click += MenuItem_Attachment_Click;
            MenuItem_InvalidAddress.Click += MenuItem_InvalidAddress_Click;
            Button_Send.Click += Button_Send_Click;
            MenuItem_Exit.Click += MenuItem_Exit_Click;
            // text events
            From_TextBox.TextChanged += From_TextBox_TextChanged;
            Reply_TextBox.TextChanged += Reply_TextBox_TextChanged;
            Subject_TextBox.TextChanged += Subject_TextBox_TextChanged;
            AttachName_TextBox.TextChanged += AttachName_TextBox_TextChanged;
            // process default values
            From_TextBox_TextChanged(sender, e);
            Reply_TextBox_TextChanged(sender, e);
            // stop resize
            FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void MenuItem_Open_Click(object sender, EventArgs e)
        {
            if (OpenFileDialog_Contacts.ShowDialog() == DialogResult.OK)
            {
                OpenFileDialog_Contacts_FileOk();
            }
        }

        private void OpenFileDialog_Contacts_FileOk()
        {
            Contacts_TextBox.Text = OpenFileDialog_Contacts.FileName;

            recipients.Clear();

            using (StreamReader reader = new StreamReader(OpenFileDialog_Contacts.OpenFile()))
            {
                string line = reader.ReadLine();

                while (!string.IsNullOrEmpty(line))
                {
                    try
                    {
                        recipients.Add(new MailAddress(line));
                    }
                    catch
                    {
                        invalidAddresses.Add(line);
                    }

                    line = reader.ReadLine();
                }
            }

            MenuItem_InvalidAddress.Enabled = invalidAddresses.Count > 0;

            haveContactsFile = true;

            Button_Send_Enable();
        }

        private void MenuItem_Message_Click(object sender, EventArgs e)
        {
            if (OpenFileDialog_Message.ShowDialog() == DialogResult.OK)
            {
                OpenFileDialog_Message_FileOk();
            }
        }

        private void OpenFileDialog_Message_FileOk()
        {
            Message_TextBox.Text = OpenFileDialog_Message.FileName;

            using (StreamReader reader = new StreamReader(OpenFileDialog_Message.OpenFile()))
            {
                messageText = reader.ReadToEnd();
            }

            haveMessageFile = true;

            Button_Send_Enable();
        }

        private void MenuItem_Attachment_Click(object sender, EventArgs e)
        {
            if (OpenFileDialog_Attachment.ShowDialog() == DialogResult.OK)
            {
                OpenFileDialog_Attachment_FileOk();
            }
        }

        private void OpenFileDialog_Attachment_FileOk()
        {
            Attachment_TextBox.Text = OpenFileDialog_Attachment.FileName;

            AttachName_TextBox.Text = new FileInfo(Attachment_TextBox.Text).Name;

            haveAttachmentFile = true;

            Button_Send_Enable();
        }

        private void MenuItem_InvalidAddress_Click(object sender, EventArgs e)
        {
            if (SaveFileDialog_InvalidAddress.ShowDialog() == DialogResult.OK)
            {
                SaveFileDialog_InvalidAddress_FileOk();
            }
        }

        private void SaveFileDialog_InvalidAddress_FileOk()
        {
            using (StreamWriter writer = new StreamWriter(SaveFileDialog_InvalidAddress.OpenFile()))
            {
                foreach (string address in invalidAddresses)
                    writer.WriteLine(address);
            }
        }

        private void From_TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                from = new MailAddress(From_TextBox.Text);
            }
            catch
            {
                from = null;
            }

            Button_Send_Enable();
        }

        private void Reply_TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                reply = new MailAddress(Reply_TextBox.Text);
            }
            catch
            {
                reply = null;
            }

            Button_Send_Enable();
        }

        private void Subject_TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                subject = Subject_TextBox.Text;
            }
            catch
            {
                subject = string.Empty;
            }

            Button_Send_Enable();
        }

        private void AttachName_TextBox_TextChanged(object sender, EventArgs e)
        {
            Button_Send_Enable();
        }

        private void Button_Send_Enable()
        {
            if (haveContactsFile && haveMessageFile)
            {
                if (recipients.Count > 0)
                {
                    if (!string.IsNullOrEmpty(messageText))
                    {
                        bool valid = (from != null);
                        valid &= (reply != null);
                        valid &= !string.IsNullOrEmpty(subject);
                        valid &= !string.IsNullOrEmpty(Host_TextBox.Text);

                        if (haveAttachmentFile)
                        {
                            valid &= !string.IsNullOrEmpty(AttachName_TextBox.Text);
                        }

                        Button_Send.Enabled = valid;
                    }
                }
            }
        }

        private void Button_Send_Click(object sender, EventArgs e)
        {
            Dictionary<string, List<MailAddress>> hosts = new Dictionary<string, List<MailAddress>>();

            foreach (MailAddress recipient in recipients)
            {
                string host = recipient.Host;
                if (!hosts.ContainsKey(host))
                    hosts.Add(host, new List<MailAddress>());
                hosts[host].Add(recipient);
            }

            progressBar1.Value = 0;

            int counter = 1;

            SmtpClient client = new SmtpClient(Host_TextBox.Text);

            foreach (string host in hosts.Keys)
            {
                MailMessage message = new MailMessage();
                message.From = from;
                message.ReplyTo = reply;

                foreach (MailAddress recipient in hosts[host])
                {
                    message.Bcc.Add(recipient);
                }

                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = messageText;

                if (haveAttachmentFile)
                {
                    string filename = Attachment_TextBox.Text;
                    string mediaType = "application/octet-stream";
                    if (filename.EndsWith(".pdf"))
                        mediaType = "application/pdf";
                    else if (filename.EndsWith(".doc"))
                        mediaType = "application/msword";
                    else if (filename.EndsWith(".html"))
                        mediaType = "text/html";
                    else if (filename.EndsWith(".ppt"))
                        mediaType = "application/vnd.ms-powerpoint";
                    else if (filename.EndsWith(".xls"))
                        mediaType = "application/vnd.ms-excel";
                    else if (filename.EndsWith(".txt"))
                        mediaType = "text/plain";
                    message.Attachments.Add(new Attachment(new MemoryStream(File.ReadAllBytes(filename)), AttachName_TextBox.Text, mediaType));
                }

                client.Send(message);

                progressBar1.Value = (int) Math.Ceiling((100.0d/hosts.Count)*counter);

                counter += 1;
            }
        }

        private void MenuItem_Exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}