﻿
Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.DraggableControlBehavior = function(element) {
    
    FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.initializeBase(this, [element]);
    
    // Variables
    this._handle = null;
    this._clientStateFieldID = null;
    this._tracking = false;
    this._lastClientX = 0;
    this._lastClientY = 0;

    // Delegates
    this._onmousedownDelegate = null;
    this._onmousemoveDelegate = null;
    this._onmouseupDelegate = null;
    this._onselectstartDelegate = null;
}

FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.prototype = {
    initialize : function() {
        FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.callBaseMethod(this, 'initialize');
        this._onmousedownDelegate = Function.createDelegate(this, this._onmousedown);
        this._onmousemoveDelegate = Function.createDelegate(this, this._onmousemove);
        this._onmouseupDelegate = Function.createDelegate(this, this._onmouseup);
        this._onselectstartDelegate = Function.createDelegate(this, this._onselectstart);
    },

    updated: function() {
        // restore position
        var frame = this.get_element();
        var state = this.get_clientState();
        if (state.top) {
            frame.style.top = state.top+'px';
        }
        if (state.left) {
            frame.style.left = state.left+'px';
        }
        // store position
        this._rememberPosition();
        // attach handler
        $addHandler(this._handle, 'mousedown', this._onmousedownDelegate);
        // add the move cursor
        this._handle.style.cursor = 'move';
        // call parent
        FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.callBaseMethod(this, 'updated');
    },

    dispose : function() {
        
        if (this._onmousedownDelegate) {
            $removeHandler(this._handle, 'mousedown', this._onmousedownDelegate);
            this._onmousedownDelegate = null;
        }
        
        if (this._onmousemoveDelegate) {
            if (this.tracking) {
                $removeHandler(document, 'mousemove', this._onmousemoveDelegate);
            }
            this._onmousemoveDelegate = null;
        }

        if (this._onmouseupDelegate) {
            if (this.tracking) {
                $removeHandler(document, 'mouseup', this._onmouseupDelegate);
            }
            this._onmouseupDelegate = null;
        }

        if (this._onselectstartDelegate) {
            if (this.tracking) {
                $removeHandler(document, 'selectstart', this._onselectstartDelegate);
                if (Sys.Browser.agent === Sys.Browser.Opera) {
                    $removeHandler(document, 'mousedown', this._onselectstartDelegate);
                }
            }
            this._onselectstartDelegate = null;
        }

        FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.callBaseMethod(this, 'dispose');
    },

    _onmousedown : function(e) {
    
        // TODO: Fix for new event model
        if (!e) {
            e = window.event;
        }
        
        this._onmousedownImplementation(e.clientX, e.clientY);
    },
    
    _onmousedownImplementation : function(clientX, clientY) {
        /// <summary>
        /// Setup the target for resizing when its handle receives a mousedown event
        /// </summary>
        /// <param name="clientX" type="Number" integer="true">
        /// X coordinate of the click
        /// </param>
        /// <param name="clientY" type="Number" integer="true">
        /// Y coordinate of the click
        /// </param>

        this._tracking = true;
        this._lastClientX = clientX;
        this._lastClientY = clientY;
        $addHandler(document, 'mousemove', this._onmousemoveDelegate);
        $addHandler(document, 'mouseup', this._onmouseupDelegate);
        $addHandler(document, 'selectstart', this._onselectstartDelegate);
        if (Sys.Browser.agent === Sys.Browser.Opera) {
            $addHandler(document, 'mousedown', this._onselectstartDelegate);
        }
    },

    _onmousemove : function(e) {
        /// <summary>
        /// Handler for the handle's mousemove event
        /// </summary>

        // TODO: Fix for new event model
        if (!e) {
            e = window.event;
        }
        
        this._onmousemoveImplementation(e.clientX, e.clientY);
    },
    
    _onmousemoveImplementation : function(clientX, clientY) {
        /// <summary>
        /// Resize the target when the handle receives mousemove events
        /// </summary>
        /// <param name="clientX" type="Number" integer="true">
        /// X coordinate of the click
        /// </param>
        /// <param name="clientY" type="Number" integer="true">
        /// Y coordinate of the click
        /// </param>

        if (this._tracking) {
            var deltaX = (clientX-this._lastClientX);
            var deltaY = (clientY-this._lastClientY);
            this._dragControl(clientX, clientY, deltaX, deltaY);
        }
    },

    _onmouseup : function() {
        /// <summary>
        /// Handler for the handle's mouseup event
        /// </summary>

        this._tracking = false;
        this._rememberPosition();
        $removeHandler(document, 'mousemove', this._onmousemoveDelegate);
        $removeHandler(document, 'mouseup', this._onmouseupDelegate);
        $removeHandler(document, 'selectstart', this._onselectstartDelegate);
        if (Sys.Browser.agent === Sys.Browser.Opera) {
            $removeHandler(document, 'mousedown', this._onselectstartDelegate);
        }
    },

    _onselectstart : function(e) {
        /// <summary>
        /// Handler for the target's selectstart event
        /// </summary>
        /// <param name="e" type="Sys.UI.DomEvent">
        /// Event info
        /// </param>

        // Don't allow selection during drag
        e.preventDefault();
        return false;
    },

    _dragControl : function(clientX, clientY, deltaX, deltaY) {
        /// <summary>
        /// Drag the target
        /// </summary>
        /// <param name="clientX" type="Number" integer="true">
        /// Starting X coordinate
        /// </param>
        /// <param name="clientY" type="Number" integer="true">
        /// Starting Y coordinate
        /// </param>
        /// <param name="deltaX" type="Number" integer="true">
        /// Change in the width
        /// </param>
        /// <param name="deltaY" type="Number" integer="true">
        /// Change in the height
        /// </param>

        var maxHeight = document.documentElement.clientHeight + document.documentElement.scrollTop;
        var maxWidth = document.documentElement.clientWidth;
        var element = this.get_element();
        var newX =  (element.offsetLeft+clientX-this._lastClientX);
        var newY = (element.offsetTop +clientY-this._lastClientY);
        if (newX + element.clientWidth > maxWidth-1) {
            newX = (maxWidth-1) - element.clientWidth;
        }
        else if (newX < 1) {
            newX = 1;
        }
        if (newY + element.clientHeight > maxHeight -1) {
            newY =(maxHeight-1) - element.clientHeight;
        }
        else if (newY < document.documentElement.scrollTop + 1) {
            newY = document.documentElement.scrollTop + 1;
        }
        element.style.left = newX+'px';
        element.style.top  = newY+'px';
        // Save last client X/Y
        this._lastClientX = clientX;
        this._lastClientY = clientY;
    },

    get_handle : function() {
        return this._handle;
    },
    set_handle : function(value) {
        if (this._handle != value) {
            this._handle = value;
            this.raisePropertyChanged('handle');
        }
    },
    
    get_clientStateFieldID : function() {
        return this._clientStateFieldID;
    },
    set_clientStateFieldID : function(value) {
        if (this._clientStateFieldID != value) {
            this._clientStateFieldID = value;
            this.raisePropertyChanged('clientStateFieldID');
        }
    },
    
    get_clientState : function() {
        if (this._clientStateFieldID) {
            var input = document.getElementById(this._clientStateFieldID);
            if (input) {
                if (input.value.length > 4) {
                    return eval('(' + input.value + ')');
                }
                return {};
            }
        }
        return null;
    },
    set_clientState : function(value) {
        if (this._clientStateFieldID) {
            var input = document.getElementById(this._clientStateFieldID);
            if (input) {
                var state = this.get_clientState();
                for (var property in value) {
                    state[property] = value[property];
                }
                value = state;
                var results = [];
                for (var property in value) {
                    if (typeof(value[property]) != "undefined") {
                        results.push(property + ': ' + value[property]);
                    }
                }
                input.value = '{' + results.join(', ') + '}';
            }
        }
    },
    
    _measurementToNumber : function(m) {
        return m.replace('px', '');
    },

    _rememberPosition : function() {
        var element = this.get_element();
        var state = {
            top: this._measurementToNumber(element.style.top),
            left: this._measurementToNumber(element.style.left)
        };
        this.set_clientState(state);
    }
}

FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.registerClass('FirstLook.Common.WebControls.Extenders.DraggableControlBehavior', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();
