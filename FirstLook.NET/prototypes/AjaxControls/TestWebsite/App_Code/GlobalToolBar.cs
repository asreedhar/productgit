using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Web;

namespace App_Code
{
    /// <summary>
    /// Summary description for ApplicationSiteMapProvider.
    /// </summary>
    /// <remarks>
    /// When the provider is configured in the web configuration file it must be
    /// marked as "security trimming enabled".
    /// </remarks>
    public class GlobalToolBarSiteMapProvider : StaticSiteMapProvider
    {
        private readonly string[] AllRoles = new string[] {"*"};

        string connectionStringName;

        SiteMapNode rootNode;

        public override void Initialize(string name, NameValueCollection attributes)
        {
            base.Initialize(name, attributes);

            if (attributes != null)
                connectionStringName = attributes["connectionStringName"];

            if (string.IsNullOrEmpty(connectionStringName))
                throw new ConfigurationErrorsException("Missing connectionStringName attribute");
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                if (rootNode != null)
                    return rootNode;

                SiteMapNode tmpRootNode = new SiteMapNode(this,
                   "RootNode",
                   null,
                   "Root Node",
                   "The Root Node of the SiteMap but is not part of the navigation");
                tmpRootNode.Roles = AllRoles;

                SiteMapNode memberProfile = new SiteMapNode(this, "Member Profile", "/IMT/EditMemberProfileAction.go", "Member Profile");
                memberProfile.Roles = AllRoles;
                memberProfile["target"] = "new_window";
                AddNode(memberProfile, tmpRootNode);

                SiteMapNode aboutFirtLook = new SiteMapNode(this, "About First Look", "/IMT/AboutUs.go", "About First Look");
                aboutFirtLook.Roles = AllRoles;
                aboutFirtLook["target"] = "planning";
                AddNode(aboutFirtLook, tmpRootNode);

                SiteMapNode contactFirstLook = new SiteMapNode(this, "Contact First Look", "/IMT/ContactUs.go", "Contact First Look");
                contactFirstLook.Roles = AllRoles;
                contactFirstLook["target"] = "planning";
                AddNode(contactFirstLook, tmpRootNode);

                SiteMapNode logOut = new SiteMapNode(this, "Log Out", "/pricing/LogOff.aspx", "Log Out"); // TODO: Needs to be a global url or configurable by solution
                logOut.Roles = AllRoles;
                AddNode(logOut, tmpRootNode); 

                rootNode = tmpRootNode;

                return rootNode;
            }
        }

        private void AddNodes(IEnumerable<Node> nodes, SiteMapNode parent)
        {
            foreach (Node n in nodes)
            {
                SiteMapNode node = new SiteMapNode(this, n.Name, n.Url, n.Name, n.Name);
                node.Roles = AllRoles;
                AddNode(node, parent);
            }
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            return rootNode;
        }

        class Node
        {
            private readonly string name;
            private readonly string url;
            private readonly bool needsNormalUser;
            private readonly int? needsUpgrade;
            private readonly bool needsStores;

            public Node(string name, string url, bool needsNormalUser, int? needsUpgrade, bool needsStores)
            {
                this.name = name;
                this.url = url;
                this.needsNormalUser = needsNormalUser;
                this.needsUpgrade = needsUpgrade;
                this.needsStores = needsStores;
            }

            public string Name
            {
                get { return name; }
            }

            public bool NeedsNormalUser
            {
                get { return needsNormalUser; }
            }

            public int? NeedsUpgrade
            {
                get { return needsUpgrade; }
            }

            public string Url
            {
                get { return url; }
            }

            public bool NeedsStores
            {
                get { return needsStores; }
            }
        }

        class CustomSiteMapNode : SiteMapNode
        {
            private readonly bool needsNormalUser;
            private readonly int? needsUpgrade;
            private readonly bool needsStores;

            public CustomSiteMapNode(SiteMapProvider provider, string key, string url, string title, string description, bool needsNormalUser, int? needsUpgrade, bool needsStores)
                : base(provider, key, url, title, description)
            {
                this.needsNormalUser = needsNormalUser;
                this.needsUpgrade = needsUpgrade;
                this.needsStores = needsStores;
            }

            public override bool IsAccessibleToUser(HttpContext context)
            {
                bool accessible = base.IsAccessibleToUser(context);

                if (accessible)
                {
                    NameValueCollection query = context.Request.QueryString;

                    if (needsStores)
                    {
                        string role = query["role"];
                        string multiple = query["multiple"];
                        accessible &= (
                            string.Equals("administrator", role) ||
                            string.Equals("account", role) ||
                            string.Equals(multiple, "true"));
                    }

                    if (needsNormalUser)
                    {
                        accessible &= string.Equals(query["type"], "normal");
                    }

                    if (needsUpgrade.HasValue)
                    {
                        string code = needsUpgrade.Value.ToString();
                        accessible &= string.Equals(query[code], "true");
                    }
                }

                return accessible;
            }
        }
    }
}