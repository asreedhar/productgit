using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Web;

namespace App_Code
{
    /// <summary>
    /// Summary description for ApplicationSiteMapProvider.
    /// </summary>
    /// <remarks>
    /// When the provider is configured in the web configuration file it must be
    /// marked as "security trimming enabled".
    /// </remarks>
    public class ApplicationSiteMapProvider : StaticSiteMapProvider
    {
        private readonly string[] AllRoles = new string[] {"*"};

        string connectionStringName;

        SiteMapNode rootNode;

        public override void Initialize(string name, NameValueCollection attributes)
        {
            base.Initialize(name, attributes);

            if (attributes != null)
                connectionStringName = attributes["connectionStringName"];

            if (string.IsNullOrEmpty(connectionStringName))
                throw new ConfigurationErrorsException("Missing connectionStringName attribute");
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                if (rootNode != null)
                    return rootNode;

                SiteMapNode tmpRootNode = new SiteMapNode(this,
                   "RootNode",
                   null,
                   "Root Node",
                   "The Root Node of the SiteMap but is not part of the navigation");
                tmpRootNode.Roles = AllRoles;

                SiteMapNode home = new SiteMapNode(this, "Home", "/home", "Home", "Home Page");
                home.Roles = AllRoles;
                AddNode(home, tmpRootNode);

                #region Tools Menu

                SiteMapNode tools = new SiteMapNode(this, "Tools", null, "Tools", "Tools Menu");
                tools.Roles = AllRoles;
                AddNode(tools, tmpRootNode);

                AddNodes(new Node[]
                    {
                        new Node("Custom Inventory Analysis", "/IMT/CIASummaryDisplayAction.go", true, 3, false),
                        new Node("First Look Search Engine", "/NextGen/SearchHomePage.go", true, 3, false),
                        new Node("Flash Locate", "/NextGen/FlashLocateSummary.go", true, null, false),
                        new Node("Forecaster", "/IMT/DealerOldHomeDisplayAction.go?forecast=1", true, 3, false),
                        new Node("Inventory Management Plan", "/NextGen/InventoryPlan.go", true, 2, false),
                        new Node("Loan Value - Book Value Calculator", "/NextGen/EquityAnalyzer.go", true, 11, false),
                        new Node("Management Summary", "/IMT/ucbp/TileManagementCenter.go", true, 4, false),
                        new Node("Performance Analyzer", "/IMT/PerformanceAnalyzerDisplayAction.go", true, 3, false),
                        new Node("Performance Dashboard", "/IMT/EdgeScoreCardDisplayAction.go", true, 7, false),
                        new Node("Trade Analyzer", "/home?ta=true", false, 1, false),
                        new Node("Trade Manager", "/IMT/TradeManagerDisplayAction.go?pageName=bullpen", false,1, false),
                        new Node("Vehicle Analyzer", "/IMT/VehicleAnalyzerDisplayAction.go", false, 3, false)
                    }, tools);

                #endregion

                #region Reports Menu

                SiteMapNode reports = new CustomSiteMapNode(this, "Reports", null, "Reports", "Reports Menu", true, null, false);
                reports.Roles = AllRoles;
                AddNode(reports, tmpRootNode);

                AddNodes(new Node[]
                     {
                         new Node("Action Plans", "/NextGen/ActionPlansReports.go", true, 2, false),
                         new Node("Deal Log", "/IMT/DealLogDisplayAction.go", true, 2, false),
                         new Node("Fastest Sellers", "/IMT/FullReportDisplayAction.go?weeks=26&;ReportType=FASTESTSELLER", true, 3, false),
                         new Node("Inventory Manager", "/IMT/DashboardDisplayAction.go?module=U", true, 3, false),
                         new Node("Inventory Overview", "/NextGen/InventoryOverview.go", true, 2, false),
                         new Node("Most Profitable Vehicles", "/IMT/FullReportDisplayAction.go?weeks=26&ReportType=MOSTPROFITABLE", true, 3, false),
                         new Node("Performance Management Reports", "/IMT/ReportCenterRedirectionAction.go", true, 7, false),
                         new Node("Pricing List", "/IMT/PricingListDisplayAction.go", true, 2, false),
                         new Node("Stocking Reports", "/NextGen/OptimalInventoryStockingReports.go", true, 3, false),
                         new Node("Top Sellers", "/IMT/FullReportDisplayAction.go?weeks=26&ReportType=TOPSELLER", true, 3, false),
                         new Node("Total Inventory Report", "/IMT/TotalInventoryReportDisplayAction.go", true, 2, false),
                         new Node("Trades & Purchases Reports", "/IMT/DealerTradesDisplayAction.go", true, 2, false),
                     }, reports);

                #endregion

                #region Redistribution Menu

                SiteMapNode redistribution = new CustomSiteMapNode(this, "Redistribution", "/IMT/RedistributionHomeDisplayAction.go", "Redistribution", "Redistribution Menu", true, null, false);
                redistribution.Roles = AllRoles;
                AddNode(redistribution, tmpRootNode);

                AddNodes(new Node[]
                     {
                         new Node("Aged Inventory Exchange", "/IMT/InventoryExchangeDisplayAction.go", true, 4, false),
                         new Node("Showroom", "/IMT/ShowroomDisplayAction.go", true, 4, false)
                     }, redistribution);

                #endregion

                SiteMapNode print = new SiteMapNode(this, "Print", null, "Print", "Print Page");
                print.Roles = AllRoles;
                AddNode(print, tmpRootNode);

                SiteMapNode exit = new CustomSiteMapNode(this, "Exit", null, "Exit Store", "Exit Store", false, null, true);
                exit.Roles = AllRoles;
                AddNode(exit, tmpRootNode);

                rootNode = tmpRootNode;

                return rootNode;
            }
        }

        private void AddNodes(IEnumerable<Node> nodes, SiteMapNode parent)
        {
            foreach (Node n in nodes)
            {
                SiteMapNode node = new CustomSiteMapNode(this, n.Name, n.Url, n.Name, n.Name, n.NeedsNormalUser, n.NeedsUpgrade, n.NeedsStores);
                node.Roles = AllRoles;
                AddNode(node, parent);
            }
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            return rootNode;
        }

        class Node
        {
            private readonly string name;
            private readonly string url;
            private readonly bool needsNormalUser;
            private readonly int? needsUpgrade;
            private readonly bool needsStores;

            public Node(string name, string url, bool needsNormalUser, int? needsUpgrade, bool needsStores)
            {
                this.name = name;
                this.url = url;
                this.needsNormalUser = needsNormalUser;
                this.needsUpgrade = needsUpgrade;
                this.needsStores = needsStores;
            }

            public string Name
            {
                get { return name; }
            }

            public bool NeedsNormalUser
            {
                get { return needsNormalUser; }
            }

            public int? NeedsUpgrade
            {
                get { return needsUpgrade; }
            }

            public string Url
            {
                get { return url; }
            }

            public bool NeedsStores
            {
                get { return needsStores; }
            }
        }

        class CustomSiteMapNode : SiteMapNode
        {
            private readonly bool needsNormalUser;
            private readonly int? needsUpgrade;
            private readonly bool needsStores;

            public CustomSiteMapNode(SiteMapProvider provider, string key, string url, string title, string description, bool needsNormalUser, int? needsUpgrade, bool needsStores)
                : base(provider, key, url, title, description)
            {
                this.needsNormalUser = needsNormalUser;
                this.needsUpgrade = needsUpgrade;
                this.needsStores = needsStores;
            }

            public override bool IsAccessibleToUser(HttpContext context)
            {
                bool accessible = base.IsAccessibleToUser(context);

                if (accessible)
                {
                    NameValueCollection query = context.Request.QueryString;

                    if (needsStores)
                    {
                        string role = query["role"];
                        string multiple = query["multiple"];
                        accessible &= (
                            string.Equals("administrator", role) ||
                            string.Equals("account", role) ||
                            string.Equals(multiple, "true"));
                    }

                    if (needsNormalUser)
                    {
                        accessible &= string.Equals(query["type"], "normal");
                    }

                    if (needsUpgrade.HasValue)
                    {
                        string code = needsUpgrade.Value.ToString();
                        accessible &= string.Equals(query[code], "true");
                    }
                }

                return accessible;
            }
        }
    }
}