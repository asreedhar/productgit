<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Manual.aspx.cs" Inherits="Dialog_Manual" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Debug" LoadScriptsBeforeUI="false" />
        <div>
            
            <div id="dialog" runat="server" class="ui-dialog" style="width: 500px; height: 300px; top: 200px; left:300px; z-index: 5;">
                <fieldset style="display: none;">
                    <asp:HiddenField ID="clientState" runat="server" Value="" />
                </fieldset>
                <div class="ui-dialog-container">
                    <div id="titlebar" runat="server" class="ui-dialog-titlebar">
                        <span class="ui-dialog-title">Dialog Title</span>
                        <button name="close" value="true" type="submit" class="ui-dialog-titlebar-close">X</button>
                    </div>
                    <div class="ui-dialog-content">
                        <div class="ui-dialog-content-wrapper">
                            <p>This will be a content template</p>
                        </div>
                    </div>
                </div>
                <div class="ui-dialog-buttonpane">
                    <ul>
                        <li>As will this</li>
                        <li><button type="submit" name="cancel" value="true">Cancel</button></li>
                        <li><button type="submit" name="accept" value="true">OK</button></li>
                    </ul>
                </div>
                <div class="ui-resizable-n ui-resizable-handle"></div>
			    <div class="ui-resizable-s ui-resizable-handle"></div>
			    <div class="ui-resizable-e ui-resizable-handle"></div>
			    <div class="ui-resizable-w ui-resizable-handle"></div>
			    <div class="ui-resizable-ne ui-resizable-handle"></div>
			    <div class="ui-resizable-se ui-resizable-handle"></div>
			    <div class="ui-resizable-sw ui-resizable-handle"></div>
			    <div class="ui-resizable-nw ui-resizable-handle"></div>
            </div>
            
            <cwc:ResizableControl ID="ResizeDialog" runat="server" TargetControlID="dialog" ClientStateFieldID="clientState">
                <MinimumDimension Height="200" Width="300" />
                <MaximumDimension Height="600" Width="800" />
            </cwc:ResizableControl>
            
            <cwc:DraggableControl ID="DraggableDialog" runat="server" TargetControlID="dialog" ClientStateFieldID="clientState" DragHandleID="titlebar" />
            
        </div>
    </form>
</body>
</html>
