<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Controls.aspx.cs" Inherits="Dialog_Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Debug" LoadScriptsBeforeUI="false" />
        
        <div>
            
            <ul>
                <li><code>ui-dialog-titlebar-close</code> has hover event to add/remove css class</li>
                <li>specify a class to handle dialog events (ok, cancel, close)</li>
                <li>client event handler?</li>
            </ul>
            
            <p><asp:Label ID="Label1" runat="server" Text="Not Clicked"></asp:Label></p>
            
            <cwc:Dialog ID="DialogProper" runat="server" TitleText="Sample Title" CssClass="blue" Width="400" Height="300" OnButtonClick="MessageBox_ButtonClick">
                <MinimumDimension Width="300" Height="200" />
                <MaximumDimension Width="800" Height="600" />
                <ItemTemplate>
                    <p>This <em>is</em> a content template.</p>
                    <asp:Button ID="Button1" runat="server" Text="Get Time!" OnClick="Button1_Click" />
                </ItemTemplate>
                <Buttons>
                    <cwc:DialogButton ButtonType="Button" Text="Wazoo" ButtonCommand="Ok" IsDefault="true" />
                    <cwc:DialogButton ButtonType="Button" Text="Furow" ButtonCommand="Cancel" IsCancel="true" />
                </Buttons>
            </cwc:Dialog>
            
            <cwc:MessageBox ID="MessageBox" runat="server" Width="350" Height="150" Left="450" TitleText="Helo" MessageText="This is a message dialog. Agree or ELSE!" OnButtonClick="MessageBox_ButtonClick" />
            
        </div>
        
    </form>
</body>
</html>
