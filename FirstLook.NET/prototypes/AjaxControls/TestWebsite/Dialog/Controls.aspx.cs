using System;
using System.Web.UI;
using FirstLook.Common.WebControls.UI;

public partial class Dialog_Controls : Page
{
    protected void Button1_Click(object sender, EventArgs e)
    {
        Label1.Text = string.Format("Clicked {0:T}", DateTime.Now);
    }

    protected void MessageBox_ButtonClick(object sender, DialogButtonClickEventArgs e)
    {
        Label1.Text = string.Format("Clicked {0}", e.Button);
    }
}
