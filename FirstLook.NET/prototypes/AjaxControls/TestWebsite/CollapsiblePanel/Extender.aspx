<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Extender.aspx.cs" Inherits="CollapsiblePanel_Extender" %>
<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Debug" LoadScriptsBeforeUI="false" />
        
        <div>
            <asp:Panel ID="CollapsePanelHead" runat="server" CssClass="ui-collapse-panel-head">
                <fieldset style="display:none;">
                    <asp:HiddenField ID="ClientState" runat="server" Value="true" OnValueChanged="ClientState_ValueChanged" />
                </fieldset>
                <asp:Panel ID="CollapsePanelTitle" runat="server" CssClass="ui-collapse-panel-titlebar">
                    <span class="ui-collapse-panel-title">What is ASP.NET AJAX?</span>
                    <span class="ui-collapse-panel-action"><asp:Label ID="ToggleLabel" runat="server">(Show Details)</asp:Label></span>
                    <span class="ui-collapse-panel-button"><asp:ImageButton ID="ToggleButton" runat="server" OnClick="ToggleButton_Click" OnPreRender="ToggleButton_PreRender" /></span>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="CollapsePanelBody" runat="server" CssClass="ui-collapse-panel-body">
                <asp:Panel ID="CollapsePanelContent" runat="server" CssClass="ui-collapse-panel-content">
                    <p> ASP.NET AJAX is a free framework for building a new generation of richer,
                        more interactive, highly personalized cross-browser web applications.  This
                        new web development technology from Microsoft integrates cross-browser client
                        script libraries with the ASP.NET 2.0 server-based development framework.
                        In addition, ASP.NET AJAX offers you the same type of development platform for
                        client-based web pages that ASP.NET offers for server-based pages.  And because
                        ASP.NET AJAX is an extension of ASP.NET, it is fully integrated with
                        server-based services. ASP.NET AJAX makes it possible to easily take advantage
                        of AJAX techniques on the web and enables you to create ASP.NET pages with a
                        rich, responsive UI and server communication.  However, AJAX isn't just for
                        ASP.NET.  You can take advantage of the rich client framework to easily build
                        client-centric web applications that integrate with any backend data provider
                        and run on most modern browsers. </p>
                </asp:Panel>
            </asp:Panel>
            
            <cwc:CollapsiblePanelExtender ID="CollapsiblePanelExtender" runat="server"
                TargetControlID="CollapsePanelBody"
                ClientStateFieldID="ClientState"
                CollapseControlID="CollapsePanelHead"
                ExpandControlID="CollapsePanelHead"
                Collapsed='<%# Collapsed() %>'
                CollapsedImage="~/App_Themes/Test/Images/collapse_panel_arrow_open.jpg"
                ExpandedImage="~/App_Themes/Test/Images/collapse_panel_arrow_close.jpg"
                CollapsedText="(Show Details...)"
                ExpandedText="(Hide Details...)"
                ImageControlID="ToggleButton"
                TextLabelID="ToggleLabel"
                SuppressPostBack="true"
                />
        </div>
    </form>
</body>
</html>
