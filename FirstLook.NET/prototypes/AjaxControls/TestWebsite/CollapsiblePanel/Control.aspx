<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Control.aspx.cs" Inherits="CollapsiblePanel_Control" %>
<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" LoadScriptsBeforeUI="false" />
        
        <div class="page-content">
            
            <cwc:CollapsiblePanel ID="CollapsiblePanel1" runat="server" Title="What is ASP.NET AJAX?" ButtonID="ToggleButton" LabelID="ToggleLabel" Collapsed="true" LazyItemTemplate="true">
                <CollapsedHeaderStyle Tooltip="(Show Details)" ImageUrl="~/App_Themes/Test/Images/collapse_panel_arrow_open.jpg" />
                <ExpandedHeaderStyle Tooltip="(Hide Details)" ImageUrl="~/App_Themes/Test/Images/collapse_panel_arrow_close.jpg" />
                <HeaderTemplate>
                    <div class="ui-collapse-panel-titlebar">
                        <span class="ui-collapse-panel-title"><%# Container.Title %></span>
                        <span class="ui-collapse-panel-action"><asp:Label ID="ToggleLabel" runat="server" Text='<%# Container.Tooltip %>' /></span>
                        <span class="ui-collapse-panel-button"><asp:ImageButton ID="ToggleButton" runat="server" ImageUrl='<%# Container.ImageUrl %>' AlternateText='<%# Container.Tooltip %>' ToolTip='<%# Container.Tooltip %>' /></span>
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <p> ASP.NET AJAX is a free framework for building a new generation of richer,
                        more interactive, highly personalized cross-browser web applications.  This
                        new web development technology from Microsoft integrates cross-browser client
                        script libraries with the ASP.NET 2.0 server-based development framework.
                        In addition, ASP.NET AJAX offers you the same type of development platform for
                        client-based web pages that ASP.NET offers for server-based pages.  And because
                        ASP.NET AJAX is an extension of ASP.NET, it is fully integrated with
                        server-based services. ASP.NET AJAX makes it possible to easily take advantage
                        of AJAX techniques on the web and enables you to create ASP.NET pages with a
                        rich, responsive UI and server communication.  However, AJAX isn't just for
                        ASP.NET.  You can take advantage of the rich client framework to easily build
                        client-centric web applications that integrate with any backend data provider
                        and run on most modern browsers. </p>
                </ItemTemplate>
            </cwc:CollapsiblePanel>
            
        </div>
    </form>
</body>
</html>
