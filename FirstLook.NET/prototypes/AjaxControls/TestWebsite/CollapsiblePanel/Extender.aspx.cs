using System;
using System.Web.UI;

public partial class CollapsiblePanel_Extender : Page
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        DataBindChildren();
    }

    protected void ToggleButton_Click(object sender, ImageClickEventArgs e)
    {
        if (IsOpen())
        {
            ClosePanel();
        }
        else
        {
            OpenPanel();
        }
    }

    protected void ToggleButton_PreRender(object sender, EventArgs e)
    {
        ToggleButton.ImageUrl = ResolveClientUrl("~/App_Themes/" + Theme +
            (IsOpen() ? "/Images/collapse_panel_arrow_close.jpg" : "/Images/collapse_panel_arrow_open.jpg"));
    }

    protected void ClientState_ValueChanged(object sender, EventArgs e)
    {
        if (IsOpen())
        {
            OpenPanel();
        }
        else
        {
            ClosePanel();
        }
    }

    private void ClosePanel()
    {
        ClientState.Value = "true";

        ToggleLabel.Text = "(Show Details)";

        CollapsePanelBody.Style["display"] = "none";
    }

    private void OpenPanel()
    {
        ClientState.Value = "false";

        ToggleLabel.Text = "(Hide Details)";

        CollapsePanelBody.Style.Remove("display");
    }

    protected bool Collapsed()
    {
        return (IsOpen() ? false : true);
    }

    private bool IsOpen()
    {
        return !string.Equals("true", ClientState.Value);
    }
}
