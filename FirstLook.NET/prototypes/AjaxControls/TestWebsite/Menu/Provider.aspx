<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Provider.aspx.cs" Inherits="Menu_Provider" EnableViewState="false" %>
<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Menu - Site Map Provider</title>
    <style type="text/css" media="screen">
        dl { float: left; clear: both; }
        dt { clear: left; float: left; }
        dd { float: none; }
        #Menu2 {
	        position: absolute;
	        top: 15px;
	        left: 0;
        }

        #Menu3 {
	        float: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" method="get">
        <div>
            <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" ShowStartingNode="false" SiteMapProvider="ApplicationSiteMap" />
            <asp:Menu ID="global_menu_bar_container" runat="server" CssClass="smallCaps blue" Orientation="Horizontal" DataSourceID="SiteMapDataSource1" />
            
            <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" ShowStartingNode="false" SiteMapProvider="GlobalToolbarSiteMap" />
            <asp:Menu ID="Menu3" runat="server" CssClass="ltGrey" Orientation="Horizontal" DataSourceID="SiteMapDataSource2" />
            <br />
            <fieldset>
                <legend>User Role</legend>
                <dl>
                    <dt><input type="radio" name="role" value="user" id="user" /></dt>
                    <dd><label for="user">User</label></dd>
                    <dt><input type="radio" name="role" value="account" id="account" /></dt>
                    <dd><label for="account">Account Representative</label></dd>
                    <dt><input type="radio" name="role" value="administrator" id="administrator" /></dt>
                    <dd><label for="administrator">Administrator</label></dd>
                </dl>
            </fieldset>
            <fieldset>
                <legend>User Type</legend>
                <dl>
                    <dt><input type="radio" name="type" value="normal" id="normal" /></dt>
                    <dd><label for="normal">Normal</label></dd>
                    <dt><input type="radio" name="type" value="limit" id="limit" /></dt>
                    <dd><label for="limit">Restricted</label></dd>
                </dl>
            </fieldset>
            <fieldset>
                <legend>Upgrades</legend>
                <dl>
                    <dt><input type="checkbox" name="1" value="true" id="u1" /></dt>
                    <dd><label for="u1">Trade Analyzer</label></dd>
                    <dt><input type="checkbox" name="2" value="true" id="u2" /></dt>
                    <dd><label for="u2">Aging Inventory Plan</label></dd>
                    <dt><input type="checkbox" name="3" value="true" id="u3" /></dt>
                    <dd><label for="u3">Purchasing Center</label></dd>
                    <dt><input type="checkbox" name="4" value="true" id="u4" /></dt>
                    <dd><label for="u4">Redistribution</label></dd>
                    <dt><input type="checkbox" name="7" value="true" id="u7" /></dt>
                    <dd><label for="u7">Performance Dashboard</label></dd>
                    <dt><input type="checkbox" name="11" value="true" id="u11" /></dt>
                    <dd><label for="u11">Equity Analyzer</label></dd>
                </dl>
            </fieldset>
            <fieldset>
                <legend>Multiple Store Access</legend>
                <dl>
                    <dt><input type="radio" name="multiple" value="true" id="y" /></dt>
                    <dd><label for="y">Yes</label></dd>
                    <dt><input type="radio" name="multiple" value="false" id="n" /></dt>
                    <dd><label for="n">No</label></dd>
                </dl>
            </fieldset>
            <input type="submit" name="submit" value="submit" />
        </div>
    </form>
</body>
</html>
