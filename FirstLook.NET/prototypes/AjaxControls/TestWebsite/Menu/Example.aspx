<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Example.aspx.cs" Inherits="Menu_Example" %>
<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Menu - Static Example</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:XmlDataSource ID="XmlDataSource1" runat="server">
                <Data>
                    <MapHomeNode url="~\Home.aspx" title="Home" description="Home">
                        <MapNode url="~\Music.aspx" title="Music" description="Music">
                            <MapNode url="~\Classical.aspx" title="Classical" description="Classical"/>
                            <MapNode url="~\Rock.aspx" title="Rock" description="Rock"/>
                            <MapNode url="~\Jazz.aspx" title="Jazz" description="Jazz"/>
                        </MapNode>
                        <MapNode url="~\Movies.aspx" title="Movies" description="Movies">
                            <MapNode url="~\Action.aspx" title="Action" description="Action"/>
                            <MapNode url="~\Drama.aspx" title="Drama" description="Drama"/>
                            <MapNode url="~\Musical.aspx" title="Musical" description="Musical"/>
                        </MapNode>
                    </MapHomeNode>
                </Data>
            </asp:XmlDataSource>
            <asp:Menu ID="NavigationMenu" StaticDisplayLevels="1" Orientation="Horizontal" DataSourceID="XmlDataSource1" runat="server" CssClass="demo">
                <DataBindings>
                    <asp:MenuItemBinding DataMember="MapHomeNode" Depth="0" TextField="title" NavigateUrlField="url" />
                    <asp:MenuItemBinding DataMember="MapNode" Depth="1" TextField="title" NavigateUrlField="url" />
                    <asp:MenuItemBinding DataMember="MapNode" Depth="2" TextField="title" NavigateUrlField="url" />
                </DataBindings>
            </asp:Menu>
            <asp:Menu ID="Menu1" runat="server" StaticDisplayLevels="1" Orientation="Vertical" CssClass="demo">
                <Items>
                    <asp:MenuItem Text="File" Value="File">
                        <asp:MenuItem Text="New" Value="New"></asp:MenuItem>
                        <asp:MenuItem Text="Open" Value="Open"></asp:MenuItem>
                    </asp:MenuItem>
                    <asp:MenuItem Text="Edit" Value="Edit">
                        <asp:MenuItem Text="Copy" Value="Copy"></asp:MenuItem>
                        <asp:MenuItem Text="Paste" Value="Paste"></asp:MenuItem>
                    </asp:MenuItem>
                    <asp:MenuItem Text="View" Value="View">
                        <asp:MenuItem Text="Normal" Value="Normal"></asp:MenuItem>
                        <asp:MenuItem Text="Preview" Value="Preview"></asp:MenuItem>
                    </asp:MenuItem>
                </Items>
            </asp:Menu>
        </div>
    </form>
</body>
</html>
