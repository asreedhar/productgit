using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Slider_Extenders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InputExtender1.ValueMappings.Add("1", "10");
            InputExtender1.ValueMappings.Add("2", "25");
            InputExtender1.ValueMappings.Add("3", "50");
            InputExtender1.ValueMappings.Add("4", "75");
            InputExtender1.ValueMappings.Add("5", "100");
            InputExtender1.ValueMappings.Add("6", "150");
            InputExtender1.ValueMappings.Add("7", "250");
            InputExtender1.ValueMappings.Add("8", "500");
            InputExtender1.ValueMappings.Add("9", "750");
            InputExtender1.ValueMappings.Add("10", "1000");
        }
    }
}
