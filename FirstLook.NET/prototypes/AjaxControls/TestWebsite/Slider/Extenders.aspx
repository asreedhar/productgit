<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Extenders.aspx.cs" Inherits="Slider_Extenders" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
    .ajax__slider_h_rail {position:relative;background:url('../Public/Images/slider_h_rail.gif') repeat-x;height:22px;width:150px;}
    .ajax__slider_h_handle {position:absolute;height:22px;width:10px;}
    .ajax__slider_v_rail {position:relative;background:url('../Public/Images/slider_v_rail.gif') repeat-y;width:22px;height:150px;}
    .ajax__slider_v_handle {position:absolute;height:10px;width:22px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Debug" LoadScriptsBeforeUI="false" />
    <div>
        <table style="margin:auto">
            <tr>
                <td>
                    Horizontal Slider ranging from -100 to +100 with 5 discrete values (5 steps),
                    bound to a Label control. Changing the value will cause the Slider to trigger
                    an update of the UpdatePanel that displays the current date and time.
                </td>
                <td style="width:205px">
                    <table>
                        <tr>
                            <td style="width:140px;">
                                <asp:TextBox ID="Slider1" runat="server" AutoPostBack="true" style="right:0px" Text="0" />
                            </td>
                            <td style="width:15px"></td>
                            <td style="width:auto">
                                <asp:Label ID="Slider1_Label" runat="server" style="text-align:right" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div style="padding-top:10px;text-align:center">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="lblUpdateDate" runat="server" style="font-size:80%;" Text="&nbsp;" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="Slider1" EventName="TextChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Vertical Slider ranging from 0 to 100, bound to a TextBox. Clicking on the rail will cause 
                    the handle to slide with a nice animation effect.
                </td>
                <td style="height:166px;">
                    <table style="display:inline;">
                        <tr>
                            <td><asp:TextBox ID="Slider2" runat="server" /></td>
                            <td><asp:TextBox ID="Slider2_BoundControl" runat="server" Width="30" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Slider instance that is accessible. Its borders allow the slider's rail
                    to be distinguished in high contrast mode. The TooltipText property on the slider's handle
                    indicates the current value of the slider and what is actionable for the user when focus is set on
                    that image. The value of the slider can be changed using the bound textbox so that it is completely 
                    usable without a mouse. Keyboard support for the Slider's handle is not in yet but will be available in
                    the later Toolkit releases.
                </td>
                <td style="width:205px">
                    <table>
                        <tr>
                            <td style="width:140px;border:solid 1px #808080">
                                <asp:TextBox ID="Slider3" runat="server" style="right:0px" Text="0" />
                            </td>
                            <td style="width:15px"></td>
                            <td style="width:auto">
                                <asp:TextBox ID="Slider3_BoundControl" runat="server" Width="30" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                <tr>
                    <td>
                        Slider instance using a decimal range from 0.1 to 1.0.
                    </td>
                    <td style="width: 205px">
                        <table>
                            <tr>
                                <td style="width: 140px;">
                                    <asp:TextBox ID="Slider4" runat="server" Style="right: 0px" />
                                </td>
                                <td style="width: 15px">
                                </td>
                                <td style="width: auto">
                                    <asp:Label ID="Slider4_BoundControl" runat="server" Style="text-align: right" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        
        <cwc:SliderExtender ID="SliderExtender1" runat="server"
            TargetControlID="Slider1"
            HandleCssClass="ajax__slider_h_handle"
            HandleImageUrl="../Public/Images/slider_h_handle.gif"
            RailCssClass="ajax__slider_h_rail"
            Minimum="1"
            Maximum="10"
            Steps="10" />
        
        <cwc:SliderLabelExtender ID="InputExtender1" runat="server"
            TargetControlID="Slider1"
            LabelID="Slider1_Label"
            LabelFormat="{0} miles" />
        
        <cwc:SliderExtender ID="SliderExtender2" runat="server"
            TargetControlID="Slider2"
            HandleCssClass="ajax__slider_v_handle"
            HandleImageUrl="../Public/Images/slider_v_handle.gif"
            RailCssClass="ajax__slider_v_rail"
            BoundControlID="Slider2_BoundControl"
            Orientation="Vertical"
            EnableHandleAnimation="true" />
            
        <cwc:SliderExtender ID="SliderExtender3" runat="server"
            TargetControlID="Slider3"
            HandleCssClass="ajax__slider_h_handle"
            HandleImageUrl="../Public/Images/slider_h_handle.gif"
            RailCssClass="ajax__slider_h_rail"
            BoundControlID="Slider3_BoundControl"
            Orientation="Horizontal"
            EnableHandleAnimation="true"
            TooltipText="Slider: value {0}. Please slide to change value." />
         
         <cwc:SliderExtender ID="SliderExtender4" runat="server"
            TargetControlID="Slider4"
            HandleCssClass="ajax__slider_h_handle"
            HandleImageUrl="../Public/Images/slider_h_handle.gif"
            RailCssClass="ajax__slider_h_rail"
            BoundControlID="Slider4_BoundControl"
            Orientation="Horizontal"
            EnableHandleAnimation="true"
            TooltipText="{0}"
            Decimals="2"
            Minimum="0.1"
            Maximum="1" />
            
    </div>
    </form>
</body>
</html>
