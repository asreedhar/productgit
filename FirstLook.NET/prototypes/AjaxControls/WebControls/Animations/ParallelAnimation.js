
﻿
$AA.ParallelAnimation=function(target,duration,fps,animations){$AA.ParallelAnimation.initializeBase(this,[target,duration,fps,animations]);}
$AA.ParallelAnimation.prototype={add:function(animation){$AA.ParallelAnimation.callBaseMethod(this,'add',[animation]);animation.setOwner(this);},onStart:function(){$AA.ParallelAnimation.callBaseMethod(this,'onStart');var animations=this.get_animations();for(var i=0;i<animations.length;i++){animations[i].onStart();}},onStep:function(percentage){var animations=this.get_animations();for(var i=0;i<animations.length;i++){animations[i].onStep(percentage);}},onEnd:function(){var animations=this.get_animations();for(var i=0;i<animations.length;i++){animations[i].onEnd();}
$AA.ParallelAnimation.callBaseMethod(this,'onEnd');}}
$AA.ParallelAnimation.registerClass('AjaxControlToolkit.Animation.ParallelAnimation',$AA.ParentAnimation);$AA.registerAnimation('parallel',$AA.ParallelAnimation);