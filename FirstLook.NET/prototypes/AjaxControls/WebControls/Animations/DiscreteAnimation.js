
﻿
$AA.DiscreteAnimation=function(target,duration,fps,property,propertyKey,values){$AA.DiscreteAnimation.initializeBase(this,[target,duration,fps,property,propertyKey]);this._values=(values&&values.length)?values:[];}
$AA.DiscreteAnimation.prototype={getAnimatedValue:function(percentage){var index=Math.floor(this.interpolate(0,this._values.length-1,percentage));return this._values[index];},get_values:function(){return this._values;},set_values:function(value){if(this._values!=value){this._values=value;this.raisePropertyChanged('values');}}}
$AA.DiscreteAnimation.registerClass('AjaxControlToolkit.Animation.DiscreteAnimation',$AA.PropertyAnimation);$AA.registerAnimation('discrete',$AA.DiscreteAnimation);