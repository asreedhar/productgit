
﻿
$AA.ConditionAnimation=function(target,duration,fps,animations,conditionScript){$AA.ConditionAnimation.initializeBase(this,[target,duration,fps,animations]);this._conditionScript=conditionScript;}
$AA.ConditionAnimation.prototype={getSelectedIndex:function(){var selected=-1;if(this._conditionScript&&this._conditionScript.length>0){try{selected=eval(this._conditionScript)?0:1;}catch(ex){}}
return selected;},get_conditionScript:function(){return this._conditionScript;},set_conditionScript:function(value){if(this._conditionScript!=value){this._conditionScript=value;this.raisePropertyChanged('conditionScript');}}}
$AA.ConditionAnimation.registerClass('AjaxControlToolkit.Animation.ConditionAnimation',$AA.SelectionAnimation);$AA.registerAnimation('condition',$AA.ConditionAnimation);