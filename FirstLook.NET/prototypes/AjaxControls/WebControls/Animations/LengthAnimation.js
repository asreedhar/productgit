
$AA.LengthAnimation=function(target,duration,fps,property,propertyKey,startValue,endValue,unit){$AA.LengthAnimation.initializeBase(this,[target,duration,fps,property,propertyKey,startValue,endValue]);this._unit=(unit!=null)?unit:'px';}
$AA.LengthAnimation.prototype={getAnimatedValue:function(percentage){var value=this.interpolate(this.get_startValue(),this.get_endValue(),percentage);return Math.round(value)+this._unit;},get_unit:function(){return this._unit;},set_unit:function(value){if(this._unit!=value){this._unit=value;this.raisePropertyChanged('unit');}}}
$AA.LengthAnimation.registerClass('AjaxControlToolkit.Animation.LengthAnimation',$AA.InterpolatedAnimation);$AA.registerAnimation('length',$AA.LengthAnimation);
