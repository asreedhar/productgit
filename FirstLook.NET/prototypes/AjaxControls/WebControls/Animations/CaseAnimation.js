
﻿
$AA.CaseAnimation=function(target,duration,fps,animations,selectScript){$AA.CaseAnimation.initializeBase(this,[target,duration,fps,animations]);this._selectScript=selectScript;}
$AA.CaseAnimation.prototype={getSelectedIndex:function(){var selected=-1;if(this._selectScript&&this._selectScript.length>0){try{var result=eval(this._selectScript)
if(result!==undefined)
selected=result;}catch(ex){}}
return selected;},get_selectScript:function(){return this._selectScript;},set_selectScript:function(value){if(this._selectScript!=value){this._selectScript=value;this.raisePropertyChanged('selectScript');}}}
$AA.CaseAnimation.registerClass('AjaxControlToolkit.Animation.CaseAnimation',$AA.SelectionAnimation);$AA.registerAnimation('case',$AA.CaseAnimation);