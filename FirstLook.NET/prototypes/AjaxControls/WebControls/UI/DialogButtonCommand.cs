namespace FirstLook.Common.WebControls.UI
{
    public enum DialogButtonCommand
    {
        NoButton,
        Ok,
        Cancel,
        Yes,
        No,
        Abort,
        Retry,
        Ignore,
        Escape
    }
}
