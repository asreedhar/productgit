using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
	public sealed class InputDialog : Dialog
	{
		[DefaultValue(true),
		 Category("Data"),
		 Description("Message Text")]
		public string MessageText
		{
			get
			{
				string value = (string)ViewState["MessageText"];
				if (string.IsNullOrEmpty(value))
					return string.Empty;
				return value;
			}
			set
			{
				if (string.Compare(MessageText, value) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("MessageText");
					}
					else
					{
						ViewState["MessageText"] = value;
					}
				}
			}
		}

		[DefaultValue(true),
		 Category("Data"),
		 Description("Input Text")]
		public string InputText
		{
			get
			{
				string value = (string)ViewState["InputText"];
				if (string.IsNullOrEmpty(value))
					return string.Empty;
				return value;
			}
			set
			{
				if (string.Compare(InputText, value) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("InputText");
					}
					else
					{
						ViewState["InputText"] = value;
					}
					CurrentText = value;
				}
			}
		}

		private string CurrentText
		{
			get
			{
				string value = (string)ViewState["CurrentText"];
				if (string.IsNullOrEmpty(value))
					return string.Empty;
				return value;
			}
			set
			{
				if (string.Compare(CurrentText, value) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("CurrentText");
					}
					else
					{
						ViewState["CurrentText"] = value;
					}
				}
			}
		}

		[DefaultValue(TextBoxMode.SingleLine),
		 Category("Display"),
		 Description("Text Box Mode")]
		public TextBoxMode TextMode
		{
			get
			{
				object value = ViewState["TextMode"];
				if (value == null)
					return TextBoxMode.SingleLine;
				return (TextBoxMode)value;
			}
			set
			{
				if (TextMode != value)
				{
					ViewState["TextMode"] = value;
				}
			}
		}

		[DefaultValue(1024),
		 Category("Display"),
		 Description("MaxLenght of Input")]
		public int MaxLength
		{
			get
			{
				object value = ViewState["MaxLength"];
				if (value == null)
					return 1024;
				return (int)value;
			}
			set
			{
				if (MaxLength != value)
				{
					ViewState["MaxLength"] = value;
				}
			}
		}

		[DefaultValue(20),
		 Category("Display"),
		 Description("Columns to display")]
		public int Columns
		{
			get
			{
				object value = ViewState["Columns"];
				if (value == null)
					return 20;
				return (int)value;
			}
			set
			{
				if (Columns != value)
				{
					ViewState["Columns"] = value;
				}
			}
		}

		[DefaultValue(1),
		Category("Display"),
		Description("Rows to display")]
		public int Rows
		{
			get
			{
				object value = ViewState["Rows"];
				if (value == null)
					return 1;
				return (int)value;
			}
			set
			{
				if (Rows != value)
				{
					ViewState["Rows"] = value;
				}
			}
		}

		protected override void SetItemControlValues(object sender, EventArgs e)
		{
			base.SetItemControlValues(sender, e);
			NamingContainerPanel control = (NamingContainerPanel) sender;
			
			control["MessageText"] = MessageText;
			control["InputText"] = CurrentText;
			control["TextMode"] = TextMode;
			control["MaxLength"] = MaxLength;
			control["Columns"] = Columns;
			control["Rows"] = Rows;
		}


		protected override ITemplate CreateDefaultItemTemplate()
		{
			return new InputDialogItemTemplate(TextChanged);
		}

		protected override DialogButtonCollection ButtonsInternal
		{
			get
			{
				DialogButtonCollection collection = base.ButtonsInternal;
				if (collection == null)
				{
					collection = new DialogButtonCollection();
					collection.Add(new DialogButton(DialogButtonCommand.Ok, false, false));
					collection.Add(new DialogButton(DialogButtonCommand.Cancel, true, true));
				}
				return collection;
			}
		}

		protected override bool OnBubbleEvent(object source, EventArgs args)
		{
			CommandEventArgs commandArgs = args as CommandEventArgs;

			if (commandArgs != null)
			{
				if (string.Compare(commandArgs.CommandName, DialogButton.CommandName) == 0)
				{
					int index;

					if (int.TryParse((string)commandArgs.CommandArgument, out index))
					{
						DialogButtonCommand command = (DialogButtonCommand)Enum.ToObject(typeof(DialogButtonCommand), index);
						switch(command)
						{
							case DialogButtonCommand.Yes:
							case DialogButtonCommand.Ok:
								SaveTextChange();
								break;
							case DialogButtonCommand.Cancel:
							case DialogButtonCommand.NoButton:
							case DialogButtonCommand.No:
							case DialogButtonCommand.Abort:
							case DialogButtonCommand.Ignore:
							case DialogButtonCommand.Escape:
								CancelTextChange();
								break;
							default:
								throw new ArgumentOutOfRangeException();
						}
						OnDialogButtonClick(new DialogButtonClickEventArgs(command));
					}

					return true;
				}
			}

			return base.OnBubbleEvent(source, args);
		}

		private void TextChanged(object sender, EventArgs e)
		{
			ITextControl text = sender as ITextControl;
			if (text != null)
			{
				CurrentText = text.Text;
			}
		}

		private void SaveTextChange()
		{
			TextBox input = ItemPanel.FindControl("dialogInputBox") as TextBox;
			if (input != null)
			{
				InputText = input.Text;
			}
		}

		private void CancelTextChange()
		{
			TextBox input = ItemPanel.FindControl("dialogInputBox") as TextBox;
			if (input != null)
			{
				CurrentText = InputText;
				input.Text = CurrentText;
			}
		}

		public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			List<ScriptDescriptor> descriptors = new List<ScriptDescriptor>();

			if (AllowResize)
			{
				ScriptBehaviorDescriptor resizable = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior", ClientID);
				resizable.AddProperty("clientStateFieldID", clientStateField.ClientID);
				if (minimumDimension != null)
				{
					resizable.AddProperty("minimumWidth", minimumDimension.Width.Value);
					resizable.AddProperty("minimumHeight", minimumDimension.Height.Value);
				}
				if (maximumDimension != null)
				{
					resizable.AddProperty("maximumWidth", maximumDimension.Width.Value);
					resizable.AddProperty("maximumHeight", maximumDimension.Height.Value);
				}
				descriptors.Add(resizable);
			}

			if (AllowDrag)
			{
				ScriptBehaviorDescriptor draggable = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior", ClientID);
				draggable.AddProperty("clientStateFieldID", clientStateField.ClientID);
				draggable.AddElementProperty("handle", headerPanel.ClientID);
				descriptors.Add(draggable);
			}

			ScriptBehaviorDescriptor inputDialog = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.UI.InputDialog", ClientID);
			inputDialog.AddElementProperty("ClientStateField", clientStateField.ClientID);
			inputDialog.AddProperty("ButtonCollection", actualButtons);
			inputDialog.ID = ClientID;
			inputDialog.AddElementProperty("InputBox", ItemPanel.FindControl("dialogInputBox").ClientID);
			inputDialog.AddProperty("MaxLength",MaxLength);
			inputDialog.AddProperty("CurrentText",CurrentText);
			descriptors.Add(inputDialog);

			return descriptors;
		}


		class InputDialogItemTemplate : ITemplate
		{
			private readonly EventHandler _textChangedHandler;


			public InputDialogItemTemplate(EventHandler _textChangedHandler)
			{
				this._textChangedHandler = _textChangedHandler;
			}

			public void InstantiateIn(Control container)
            {
                HtmlGenericControl para = new HtmlGenericControl("p");
                para.DataBinding += Message_DataBinding;
				para.ID = "dialogMessageContainer";
                container.Controls.Add(para);

				HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
				fieldset.Attributes.Add("class", "ui-input-fieldset");

				TextBox inputTextBox = new TextBox();
				inputTextBox.ID = "dialogInputBox";				
				inputTextBox.DataBinding += Input_DataBinding;
				inputTextBox.TextChanged += _textChangedHandler;

				fieldset.Controls.Add(inputTextBox);
				container.Controls.Add(fieldset);
            }

            private static void Message_DataBinding(object sender, EventArgs e)
            {
                HtmlGenericControl para = sender as HtmlGenericControl;

                if (para != null)
                {
                    IIndexedPropertyNamingContainer container = (IIndexedPropertyNamingContainer) para.NamingContainer;

                    para.InnerHtml = (string) container["MessageText"];
                }
            }

			private static void Input_DataBinding(object sender, EventArgs e)
            {
                TextBox input = sender as TextBox;

                if (input != null)
                {
                    IIndexedPropertyNamingContainer container = (IIndexedPropertyNamingContainer) input.NamingContainer;
                    input.Text = (string) container["InputText"];
                	input.Columns = (int) container["Columns"];
                	input.Rows = (int) container["Rows"];
                	input.TextMode = (TextBoxMode) container["TextMode"];
                	input.MaxLength = (int) container["MaxLength"];
                }
            }
        }
	}
}
