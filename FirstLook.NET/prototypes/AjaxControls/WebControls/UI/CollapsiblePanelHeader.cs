using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
    public class CollapsiblePanelHeader : Panel, INamingContainer
    {
        private bool collapsed;
        private string title;
        private string imageUrl;
        private string tooltip;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string ImageUrl
        {
            get { return imageUrl; }
            set { imageUrl = value; }
        }

        public string Tooltip
        {
            get { return tooltip; }
            set { tooltip = value; }
        }

        public bool Collapsed
        {
            get { return collapsed; }
            set { collapsed = value; }
        }
    }
}
