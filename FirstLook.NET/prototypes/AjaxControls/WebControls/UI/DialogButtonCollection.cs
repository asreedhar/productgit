using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Common.WebControls.UI
{
    public sealed class DialogButtonCollection : StateManagedCollection
    {
        // Fields
        private static readonly Type[] knownTypes = new Type[] { typeof(DialogButton) };

        // Methods
        public void Add(DialogButton field)
        {
            ((IList)this).Add(field);
        }

        public bool Contains(DialogButton field)
        {
            return ((IList)this).Contains(field);
        }

        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new DialogButton();
            }
            throw new ArgumentOutOfRangeException("index");
        }

        protected override Type[] GetKnownTypes()
        {
            return knownTypes;
        }

        public int IndexOf(DialogButton field)
        {
            return ((IList)this).IndexOf(field);
        }

        public void Insert(int index, DialogButton field)
        {
            ((IList)this).Insert(index, field);
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);

            ((DialogButton)value).ID = (index + 1).ToString();
        }

        protected override void OnValidate(object o)
        {
            base.OnValidate(o);
            if (!(o is DialogButton))
            {
                throw new ArgumentException("DataControlFieldCollection_InvalidType");
            }
        }

        public void Remove(DialogButton field)
        {
            ((IList)this).Remove(field);
        }

        public void RemoveAt(int index)
        {
            ((IList)this).RemoveAt(index);
        }

        protected override void SetDirtyObject(object o)
        {
            // do nothing
        }

        // Properties
        [Browsable(false)]
        public DialogButton this[int index]
        {
            get
            {
                return (this[index]);
            }
        }
    }
}
