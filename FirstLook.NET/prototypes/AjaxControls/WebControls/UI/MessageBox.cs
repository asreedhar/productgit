using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FirstLook.Common.WebControls.UI
{
    public class MessageBox : Dialog
    {
        [DefaultValue(true),
         Category("Data"),
         Description("Message Text")]
        public string MessageText
        {
            get
            {
                string value = (string)ViewState["MessageText"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (string.Compare(MessageText, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("MessageText");
                    }
                    else
                    {
                        ViewState["MessageText"] = value;
                    }
                }
            }
        }

        protected override void SetItemControlValues(object sender, EventArgs e)
        {
        	NamingContainerPanel control = (NamingContainerPanel) sender;
            control["MessageText"] = MessageText;
        }

        protected override ITemplate CreateDefaultItemTemplate()
        {
            return new MessageBoxItemTemplate();
        }

        protected override DialogButtonCollection ButtonsInternal
        {
            get
            {
                DialogButtonCollection collection = base.ButtonsInternal;
                if (collection == null)
                {
                    collection = new DialogButtonCollection();
                    collection.Add(new DialogButton(DialogButtonCommand.Ok, true, true));
                }
                return collection;
            }
        }

        class MessageBoxItemTemplate : ITemplate
        {
            public void InstantiateIn(Control container)
            {
                HtmlGenericControl para = new HtmlGenericControl("p");
                para.DataBinding += Message_DataBinding;
                container.Controls.Add(para);
            }

            private static void Message_DataBinding(object sender, EventArgs e)
            {
                HtmlGenericControl para = sender as HtmlGenericControl;

                if (para != null)
                {
                    IIndexedPropertyNamingContainer container = (IIndexedPropertyNamingContainer) para.NamingContainer;

                    para.InnerHtml = (string) container["MessageText"];
                }
            }
        }
    }
}

