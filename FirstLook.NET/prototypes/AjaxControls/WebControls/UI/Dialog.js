Type.registerNamespace('FirstLook.Common.WebControls.UI');


/**
 * Dialog
 * @namespace FirstLook.Common.WebControls.UI
 * @author Paul Monson
 * @version 0.1
 */

FirstLook.Common.WebControls.UI.Dialog = function(element) {
    FirstLook.Common.WebControls.UI.Dialog.initializeBase(this, [element]);
    this._ClientStateField = null;
    this._ButtonCollection = [];
};
FirstLook.Common.WebControls.UI.Dialog.prototype = {
    initialize: function() {
        FirstLook.Common.WebControls.UI.Dialog.callBaseMethod(this, 'initialize');
    },
    updated: function() {
        if (this._clickDelegate == undefined) {
            this._clickDelegate = Function.createDelegate(this, this._clickHandler);
        }
        for (var i = 0; i < this._ButtonCollection.length; i++) {
            Sys.UI.DomEvent.addHandler($get(this._ButtonCollection[i][0]), 'click', this._clickDelegate);
        }
    },
    dispose: function() {
        if (this._clickDelegate) {
            for (var i = 0; i < this._ButtonCollection.length; i++) {
                Sys.UI.DomEvent.removeHandler($get(this._ButtonCollection[i][0]), 'click', this._clickDelegate);
            };
            delete this._clickDelegate;
        }
        FirstLook.Common.WebControls.UI.Dialog.callBaseMethod(this, 'dispose');
    },
    get_ButtonCollection: function() {
        return this._ButtonCollection;
    },
    set_ButtonCollection: function(value) {
        this._ButtonCollection = value;
    },
    get_ClientStateField: function() {
        return this._ClientStateField;
    },
    set_ClientStateField: function(value) {
        this._ClientStateField = value;
    },
    show: function() {
        ;
        this.get_element().style.display = '';
        this.set_clientState({
            'hidden': false
        });
    },
    hide: function() {
        ;
        this.get_element().style.display = 'none';
        this.set_clientState({
            'hidden': true
        });
    },
    toggle: function() {
        if (this.get_clientState().hidden) {
            this.show();
        } else {
            this.hide();
        }
    },
    add_click: function(handler) {
        this.get_events().addHandler('click', handler);
    },
    remove_click: function(handler) {
        this.get_events().removeHandler('click', handler);
    },
    _clickHandler: function(event) {
        var h = this.get_events().getHandler('click');
        if (h) {
            var args = new Sys.ButtonEventArgs();
            for (var i = 0; i < this._ButtonCollection.length; i++) {
                if (this._ButtonCollection[i][0] == event.target.id) {
                    args.set_command(this._ButtonCollection[i][1]);
                }
            };
            h(this, args);
            if (args.get_cancel()) {
                event.preventDefault();
            }
        }
    },
    get_clientState: function() {
        if (this._ClientStateField) {
            var input = this._ClientStateField;
            if (input) {
                if (input.value.length > 4) {
                    return eval('(' + input.value + ')');
                }
                return {};
            }
        }
        return null;
    },
    set_clientState: function(value) {
        if (this._ClientStateField) {
            var input = this._ClientStateField;
            if (input) {
                var state = this.get_clientState();
                for (var property in value) {
                    state[property] = value[property];
                }
                value = state;
                var results = [];
                for (var property in value) {
                    if (typeof(value[property]) != "undefined") {
                        results.push(property + ': ' + value[property]);
                    }
                }
                input.value = '{' + results.join(', ') + '}';
            }
        }
    }
};

FirstLook.Common.WebControls.UI.Dialog.registerClass('FirstLook.Common.WebControls.UI.Dialog', Sys.UI.Behavior);

/**
 * InputDialog
 * @author Paul Monson
 * @version 0.1
 * @requires Dialog
 */
FirstLook.Common.WebControls.UI.InputDialog = function(element) {
    FirstLook.Common.WebControls.UI.InputDialog.initializeBase(this, [element]);
    this._ClientStateField = null;
    this._ButtonCollection = [];
    this._InputBox = null;
    this._CurrentText = "";
    this._MaxLength = null;
    this._InputText = null;
};
FirstLook.Common.WebControls.UI.InputDialog.prototype = (function() {
    var base = FirstLook.Common.WebControls.UI.Dialog.prototype;
    var _this = {
        initialize: function() {
            FirstLook.Common.WebControls.UI.InputDialog.callBaseMethod(this, 'initialize');
            this.get_events().addHandler('click', this.CancelTextChange);
            this.get_events().addHandler('click', this.ValidateLength);
        },
        dispose: function() {
            if (this._clickDelegate) {
                for (var i = 0; i < this._ButtonCollection.length; i++) {
                    Sys.UI.DomEvent.removeHandler($get(this._ButtonCollection[i][0]), 'click', this._clickDelegate);
                };
                delete this._clickDelegate;
            }
            FirstLook.Common.WebControls.UI.InputDialog.callBaseMethod(this, 'dispose');
        },
        get_InputBox: function() {
            return this._InputBox;
        },
        set_InputBox: function(value) {
            this._InputBox = value;
        },
        get_MaxLength: function() {
            return this._MaxLength;
        },
        set_MaxLength: function(value) {
            this._MaxLength = value;
        },
        get_CurrentText: function() {
            return this._CurrentText || '';
        },
        set_CurrentText: function(value) {
            this._CurrentText = value;
        },
        ValidateLength: function(sender, eventArgs) {
        	var cmd = eventArgs.get_command();
        	if (cmd == undefined) {
        		throw ["Button Command Not Defined", eventArgs];
        	}
        	if ((cmd == 1 || cmd == 3) && sender.get_MaxLength != undefined) {
        		if (sender.get_InputBox().value.length > sender.get_MaxLength()) {
        			alert("Your input must be less then " + sender.get_MaxLength() + " characters.");
        			eventArgs.set_cancel(true);
        			return false;
        		};
        	}
        	return true;
        },
        CancelTextChange: function(sender, eventArgs) {
            var cmd = eventArgs.get_command();
            if (cmd == undefined) {
                throw ["Button Command Not Defined", eventArgs];
            };
            if (cmd != 1 && cmd != 3) {
                sender.get_InputBox().value = sender.get_CurrentText();
                sender.hide();
                eventArgs.set_cancel(true);
            };
        }
    };
    var methods = {};
    for (m in base) {
        methods[m] = base[m];
    }
    for (m in _this) {
        methods[m] = _this[m];
    }
    return methods;
})();
FirstLook.Common.WebControls.UI.InputDialog.registerClass('FirstLook.Common.WebControls.UI.InputDialog', Sys.UI.Behavior);

// ====================
// = Helper Classes =
// ====================


/**
 * DialogButtonCommand
 * @namespace FirstLook.Common.WebControls.UI
 * @author Paul Monson
 * @version 0.1
 * @requires DialogButtonCommand
 */

FirstLook.Common.WebControls.UI.DialogButtonCommand = function() {};
FirstLook.Common.WebControls.UI.DialogButtonCommand.prototype = {
    NoButton: 0,
    Ok: 1,
    Cancel: 2,
    Yes: 3,
    No: 4,
    Abort: 5,
    Retry: 6,
    Ignore: 7,
    Escape: 8
};
FirstLook.Common.WebControls.UI.DialogButtonCommand.registerEnum("FirstLook.Common.WebControls.UI.DialogButtonCommand");

/**
 * ButtonEventArgs
 * @namespace Sys
 * @author Paul Monson
 * @version 0.1
 * @requires CancelEventArgs
 */

Sys.ButtonEventArgs = function Sys$ButtonEventArgs() {
    if (arguments.length !== 0) {
        throw Error.parameterCount();
    };
    Sys.ButtonEventArgs.initializeBase(this);

    this._command = FirstLook.Common.WebControls.UI.DialogButtonCommand.NoButton;
};

function Sys$ButtonEventArgs$get_command() {
    if (arguments.length !== 0) throw Error.parameterCount();
    return this._command;
}

function Sys$ButtonEventArgs$set_command(value) {
    var e = Function._validateParams(arguments, [{
        name: "value",
        type: FirstLook.Common.WebControls.UI.DialogButtonCommand
    }]);
    if (e) throw e;

    this._command = value;
}

Sys.ButtonEventArgs.prototype = {
    get_command: Sys$ButtonEventArgs$get_command,
    set_command: Sys$ButtonEventArgs$set_command
};

Sys.ButtonEventArgs.registerClass('Sys.ButtonEventArgs', Sys.CancelEventArgs);


Sys.Application.notifyScriptLoaded();