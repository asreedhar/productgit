using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;

namespace FirstLook.Common.WebControls.UI
{
    public partial class CollapsiblePanelHeaderStyle : Component, IStateManager
    {
        internal const string SetBitsKey = "_!SB";

        internal const int PROP_IMAGE_URL = 1 << 1;
        internal const int PROP_TOOLTIP = 1 << 2;

        private StateBag statebag;
        private bool marked;
        private int markedBits;
        private int setBits;

        [DefaultValue(""),
         Localizable(true),
         Bindable(true),
         Category("Appearance"),
         Description("CollapsiblePanel_Tooltip")]
        public string Tooltip
        {
            get
            {
                if (IsSet(PROP_TOOLTIP))
                {
                    return (string) ViewState["Tooltip"];
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (string.Compare(Tooltip, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("Tooltip");
                        ClearBit(PROP_TOOLTIP);
                    }
                    else
                    {
                        ViewState["Tooltip"] = value;
                        SetBit(PROP_TOOLTIP);
                    }
                }
            }
        }

        [Category("Appearance"),
         UrlProperty,
         DefaultValue(""),
         Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)),
         Description("CollapsiblePanel_ImageUrl")]
        public virtual string ImageUrl
        {
            get
            {
                if (IsSet(PROP_IMAGE_URL))
                {
                    return (string)ViewState["ImageUrl"];
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (string.Compare(ImageUrl, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("ImageUrl");
                        ClearBit(PROP_IMAGE_URL);
                    }
                    else
                    {
                        ViewState["ImageUrl"] = value;
                        SetBit(PROP_IMAGE_URL);
                    }
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        protected internal StateBag ViewState
        {
            get
            {
                if (statebag == null)
                {
                    statebag = new StateBag(false);

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)statebag).TrackViewState();
                    }
                }

                return statebag;
            }
        }

        protected bool IsSet(int bit)
        {
            return ((setBits & bit) != 0);
        }

        protected virtual void SetBit(int bit)
        {
            setBits |= bit;

            if (IsTrackingViewState)
            {
                markedBits |= bit;
            }
        }

        protected virtual void ClearBit(int bit)
        {
            setBits &= ~bit;

            if (IsTrackingViewState)
            {
                markedBits &= ~bit;
            }
        }

        #region IStateManager Members

        protected bool IsTrackingViewState
        {
            get { return marked; }
        }

        protected void LoadViewState(object state)
        {
            if (state != null)
            {
                ((IStateManager)ViewState).LoadViewState(state);
            }
            if (statebag != null)
            {
                object obj2 = ViewState[SetBitsKey];
                if (obj2 != null)
                {
                    markedBits = (int)obj2;
                    setBits |= markedBits;
                }
            }
        }

        protected object SaveViewState()
        {
            if (statebag != null)
            {
                if (markedBits != 0)
                {
                    ViewState[SetBitsKey] = markedBits;
                }

                return ((IStateManager)ViewState).SaveViewState();
            }

            return null;
        }

        protected void TrackViewState()
        {
            ((IStateManager)ViewState).TrackViewState();
            marked = true;
        }

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        #endregion

        #region Component

        public CollapsiblePanelHeaderStyle()
        {
            InitializeComponent();
        }

        public CollapsiblePanelHeaderStyle(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #endregion
    }
}
