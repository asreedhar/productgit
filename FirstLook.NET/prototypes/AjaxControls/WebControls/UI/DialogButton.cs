using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
    [TypeConverter(typeof(ExpandableObjectConverter)), DefaultProperty("ButtonCommand")]
    public class DialogButton : IStateManager
    {
        internal const string CommandName = "DialogButton_Command";

        #region DialogButton Members

        private readonly StateBag viewState = new StateBag();

        private Style controlStyle;

        protected StateBag ViewState
        {
            get { return viewState; }
        }

        public DialogButton()
        {
            // empty constructor is OK too
        }

        public DialogButton(DialogButtonCommand command, bool isDefault, bool isCancel)
        {
            ButtonCommand = command;
            IsDefault = isDefault;
            IsCancel = isCancel;
        }

        internal string ID
        {
            get
            {
                string str = (string)ViewState["ID"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(ID, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("ID");
                    }
                    else
                    {
                        ViewState["ID"] = value;
                    }
                }
            }
        }

        [DefaultValue(""), Localizable(true), Bindable(true), Category("Appearance"), Description("Image_AlternateText")]
        public string AlternateText
        {
            get
            {
                string str = (string)ViewState["AlternateText"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(AlternateText, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("AlternateText");
                    }
                    else
                    {
                        ViewState["AlternateText"] = value;
                    }
                }
            }
        }

        [Category("Appearance"), Description("ButtonColumn_ButtonCommand"), DefaultValue(0)]
        public DialogButtonCommand ButtonCommand
        {
            get
            {
                object value = ViewState["ButtonCommand"];
                if (value == null)
                    return DialogButtonCommand.NoButton;
                return (DialogButtonCommand)value;
            }
            set
            {
                if (!Equals(ButtonCommand, value))
                {
                    ViewState["ButtonCommand"] = value;
                }
            }
        }

        [Category("Appearance"), Description("ButtonColumn_ButtonType"), DefaultValue(0)]
        public DialogButtonType ButtonType
        {
            get
            {
                object value = ViewState["ButtonType"];
                if (value == null)
                    return DialogButtonType.Button;
                return (DialogButtonType)value;
            }
            set
            {
                if (!Equals(ButtonType, value))
                {
                    ViewState["ButtonType"] = value;
                }
            }
        }

        [Category("Appearance"), DefaultValue(""), Description("WebControl_CSSClassName")]
        public string CssClass
        {
            get
            {
                if (!ControlStyleCreated)
                {
                    return string.Empty;
                }
                return ControlStyle.CssClass;
            }
            set
            {
                ControlStyle.CssClass = value;
            }
        }

        [Category("Behavior"), Description("ButtonColumn_IsDefault"), DefaultValue(false)]
        public bool IsDefault
        {
            get
            {
                object value = ViewState["IsDefault"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (IsDefault != value)
                {
                    ViewState["IsDefault"] = value;
                }
            }
        }

        [Category("Behavior"), Description("ButtonColumn_IsCancel"), DefaultValue(false)]
        public bool IsCancel
        {
            get
            {
                object value = ViewState["IsCancel"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (IsCancel != value)
                {
                    ViewState["IsCancel"] = value;
                }
            }
        }

        [Description("Image_ImageAlign"), Category("Layout"), DefaultValue(0)]
        public virtual ImageAlign ImageAlign
        {
            get
            {
                object value = ViewState["ImageAlign"];
                if (value == null)
                    return ImageAlign.NotSet;
                return (ImageAlign)value;
                
            }
            set
            {
                if ((value < ImageAlign.NotSet) || (value > ImageAlign.TextTop))
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                ViewState["ImageAlign"] = value;
            }
        }
 
        [Category("Appearance"),
         UrlProperty,
         DefaultValue(""),
         Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)),
         Description("DialogButton_ImageUrl")]
        public virtual string ImageUrl
        {
            get
            {
                string str = (string)ViewState["ImageUrl"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(ImageUrl, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("ImageUrl");
                    }
                    else
                    {
                        ViewState["ImageUrl"] = value;
                    }
                }
            }
        }

        [DefaultValue(""), Category("Appearance"), Description("Button_Text"), Bindable(true), Localizable(true)]
        public string Text
        {
            get
            {
                string str = (string)ViewState["Text"];
                if (string.IsNullOrEmpty(str))
                    return ButtonCommand.ToString();
                return str;
            }
            set
            {
                if (string.Compare(Text, value) != 0)
                {
                    if (string.Compare(ButtonType.ToString(), value) == 0)
                    {
                        ViewState.Remove("Text");
                    }
                    else
                    {
                        ViewState["Text"] = value;
                    }
                }
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced),
         Browsable(false),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Description("WebControl_ControlStyleCreated")]
        public bool ControlStyleCreated
        {
            get
            {
                return (controlStyle != null);
            }
        }

        protected virtual Style CreateControlStyle()
        {
            return new Style(ViewState);
        }

        [Description("WebControl_ControlStyle"),
         Browsable(false),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Style ControlStyle
        {
            get
            {
                if (controlStyle == null)
                {
                    controlStyle = CreateControlStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)controlStyle).TrackViewState();
                    }
                }
                return controlStyle;
            }
        }

        internal WebControl CreateControl()
        {
            IButtonControl button;

            switch (ButtonType)
            {
                case DialogButtonType.Anchor:
                    button = new LinkButton();
                    break;
                case DialogButtonType.Button:
                    button = new HtmlButton();
                    break;
                case DialogButtonType.Image:
                    ImageButton image = new ImageButton();
                    image.AlternateText = AlternateText;
                    image.ImageUrl = ImageUrl;
                    image.ImageAlign = ImageAlign;
                    button = image;
                    break;
                case DialogButtonType.Input:
                    button = new Button();
                    break;
                default:
                    throw new InvalidOperationException("Unknown ButtonType: " + ButtonType);
            }

            button.CommandName = CommandName;
            button.CommandArgument = ((int)ButtonCommand).ToString();
            button.Text = Text;

            WebControl control = (WebControl) button;
            control.CssClass = CssClass;

            return control;
        }

        #endregion

        #region IStateManager Members

        private bool trackViewState;

        protected bool IsTrackingViewState
        {
            get
            {
                return trackViewState;
            }
        }

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        protected void LoadViewState(object state)
        {
            if (state != null)
            {
                ((IStateManager)ViewState).LoadViewState(state);
            }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        protected object SaveViewState()
        {
            return ((IStateManager)ViewState).SaveViewState();
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        protected void TrackViewState()
        {
            trackViewState = true;

            ((IStateManager)ViewState).TrackViewState();
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        #endregion

        class HtmlButton : Button
        {
            protected override HtmlTextWriterTag TagKey
            {
                get
                {
                    return HtmlTextWriterTag.Button;
                }
            }

            protected override string TagName
            {
                get
                {
                    return "button";
                }
            }

            protected override void RenderContents(HtmlTextWriter writer)
            {
                writer.Write(Text);
            }
        }
    }
}
