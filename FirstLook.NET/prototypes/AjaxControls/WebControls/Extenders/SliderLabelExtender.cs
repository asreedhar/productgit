using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(TextBox))]
    public class SliderLabelExtender : ExtenderControl
    {
        /// <summary>
        /// Get/Set the ID of the server control to which the input field is written.
        /// </summary>
        /// <remarks>
        /// The server control should be an ITextControl.
        /// </remarks>
        [IDReferenceProperty(typeof(ITextControl))]
        [DefaultValue("")]
        [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification = "Following ASP.NET AJAX pattern")]
        public string LabelID
        {
            get
            {
                string value = (string) ViewState["LabelID"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (!string.Equals(LabelID, value))
                {
                    ViewState["LabelID"] = value;
                }
            }
        }

        public string LabelFormat
        {
            get
            {
                string value = (string)ViewState["LabelFormat"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (!string.Equals(LabelFormat, value))
                {
                    ViewState["LabelFormat"] = value;
                }
            }
        }

        public bool HasLabelFormat
        {
            get
            {
                return (!string.IsNullOrEmpty((string) ViewState["LabelFormat"]));
            }
        }

        public Hashtable ValueMappings
        {
            get
            {
                Hashtable value = (Hashtable)ViewState["ValueMappings"];

                if (value == null)
                {
                    value = new Hashtable();

                    ViewState["ValueMappings"] = value;
                }

                return value;
            }
        }

        public bool HasValueMappings
        {
            get
            {
                Hashtable value = (Hashtable)ViewState["ValueMappings"];
                if (value != null)
                    return value.Count > 0;
                return false;
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.SliderLabelBehavior", targetControl.ClientID);
            descriptor.AddProperty("LabelID", FindControl(LabelID).ClientID);
            if (HasLabelFormat)
                descriptor.AddProperty("LabelFormat", LabelFormat);
            if (HasValueMappings)
                descriptor.AddScriptProperty("ValueMappings", new JavaScriptSerializer().Serialize(ValueMappings));
            return new ScriptDescriptor[] {descriptor};
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[]
                {
                    new ScriptReference("FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.js",
                                        "FirstLook.Common.WebControls")
                };
        }
    }
}
