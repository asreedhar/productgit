using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(Control))]
    public sealed class DraggableControl : ExtenderControl
    {
        [DefaultValue(""),
         Description("Hidden field in which client state is persisted"),
         Category("Behavior"),
         IDReferenceProperty(typeof(HiddenField))]
        public string ClientStateFieldID
        {
            get
            {
                string value = (string)ViewState["ClientStateFieldID"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (string.Compare(ClientStateFieldID, value) != 0)
                {
                    ViewState["ClientStateFieldID"] = value;
                }
            }
        }

        [DefaultValue(""),
         Description("Control that acts as draggable handle"),
         Category("Behavior"),
         IDReferenceProperty(typeof(WebControl))]
        public string DragHandleID
        {
            get
            {
                string handle = (string) ViewState["DragHandleID"];
                if (string.IsNullOrEmpty(handle))
                    return string.Empty;
                return handle;
            }
            set
            {
                if (string.Compare(DragHandleID, value) != 0)
                {
                    ViewState["DragHandleID"] = value;
                }
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            Control handle = FindControl(DragHandleID);
            if (handle == null)
            {
                throw new InvalidOperationException("Invalid DragHandleID");
            }

            Control state = FindControl(ClientStateFieldID);
            if (state == null)
            {
                throw new InvalidOperationException("Invalid ClientStateFieldID");
            }

            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior", targetControl.ClientID);
            descriptor.AddElementProperty("handle", handle.ClientID);
            descriptor.AddProperty("clientStateFieldID", state.ClientID);
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[] { new ScriptReference("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.js", "FirstLook.Common.WebControls") };
        }
    }
}
