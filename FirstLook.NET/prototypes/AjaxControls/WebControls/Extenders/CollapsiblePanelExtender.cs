using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.CollapsiblePanelBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.CollapsiblePanelBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(Control))]
    public class CollapsiblePanelExtender : ExtenderControl
    {
        [IDReferenceProperty(typeof(HiddenField))]
        [DefaultValue("")]
        public string ClientStateFieldID
        {
            get { return GetPropertyValue("ClientStateFieldID", ""); }
            set { SetPropertyValue("ClientStateFieldID", value); }
        }

        [IDReferenceProperty(typeof(WebControl))]
        [DefaultValue("")]
        public string CollapseControlID
        {
            get { return GetPropertyValue("CollapseControlID", ""); }
            set { SetPropertyValue("CollapseControlID", value); }
        }

        [IDReferenceProperty(typeof(WebControl))]
        [DefaultValue("")]
        public string ExpandControlID
        {
            get { return GetPropertyValue("ExpandControlID", ""); }
            set { SetPropertyValue("ExpandControlID", value); }
        }

        [DefaultValue(false)]
        public bool AutoCollapse
        {
            get { return GetPropertyValue("AutoCollapse", false); }
            set { SetPropertyValue("AutoCollapse", value); }
        }

        [DefaultValue(false)]
        public bool AutoExpand
        {
            get { return GetPropertyValue("AutoExpand", false); }
            set { SetPropertyValue("AutoExpand", value); }
        }

        [DefaultValue(-1)]
        public int CollapsedSize
        {
            get { return GetPropertyValue("CollapseHeight", -1); }
            set { SetPropertyValue("CollapseHeight", value); }
        }

        [DefaultValue(-1)]
        public int ExpandedSize
        {
            get { return GetPropertyValue("ExpandedSize", -1); }
            set { SetPropertyValue("ExpandedSize", value); }
        }

        [DefaultValue(false)]
        public bool ScrollContents
        {
            get { return GetPropertyValue("ScrollContents", false); }
            set { SetPropertyValue("ScrollContents", value); }
        }

        [DefaultValue(false)]
        public bool SuppressPostBack
        {
            get { return GetPropertyValue("SuppressPostBack", false); }
            set { SetPropertyValue("SuppressPostBack", value); }
        }

        [DefaultValue(false)]
        public bool Collapsed
        {
            get { return GetPropertyValue("Collapsed", false); }
            set { SetPropertyValue("Collapsed", value); }
        }

        [DefaultValue("")]
        public string CollapsedText
        {
            get { return GetPropertyValue("CollapsedText", ""); }
            set { SetPropertyValue("CollapsedText", value); }
        }

        [DefaultValue("")]
        public string ExpandedText
        {
            get { return GetPropertyValue("ExpandedText", ""); }
            set { SetPropertyValue("ExpandedText", value); }
        }

        [IDReferenceProperty(typeof(Label))]
        [DefaultValue("")]
        public string TextLabelID
        {
            get { return GetPropertyValue("TextLabelID", ""); }
            set { SetPropertyValue("TextLabelID", value); }
        }

        [DefaultValue("")]
        [UrlProperty]
        public string ExpandedImage
        {
            get { return GetPropertyValue("ExpandedImage", ""); }
            set { SetPropertyValue("ExpandedImage", value); }
        }

        [DefaultValue("")]
        [UrlProperty]
        public string CollapsedImage
        {
            get { return GetPropertyValue("CollapsedImage", ""); }
            set { SetPropertyValue("CollapsedImage", value); }
        }

        [IDReferenceProperty(typeof(Image))]
        [DefaultValue("")]
        public string ImageControlID
        {
            get { return GetPropertyValue("ImageControlID", ""); }
            set { SetPropertyValue("ImageControlID", value); }
        }

        [DefaultValue(CollapsiblePanelExpandDirection.Vertical)]
        public CollapsiblePanelExpandDirection ExpandDirection
        {
            get { return GetPropertyValue("ExpandDirection", CollapsiblePanelExpandDirection.Vertical); }
            set { SetPropertyValue("ExpandDirection", value); }
        }

        private TValue GetPropertyValue<TValue>(string propertyName, TValue nullValue)
        {
            if (ViewState[propertyName] == null)
            {
                return nullValue;
            }
            return (TValue)ViewState[propertyName];
        }

        protected void SetPropertyValue<TValue>(string propertyName, TValue value)
        {
            ViewState[propertyName] = value;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void EnsureValid()
        {
            if ((ExpandedText != null || CollapsedText != null) && TextLabelID == null)
            {
                throw new ArgumentException("If CollapsedText or ExpandedText is set, TextLabelID must also be set.");
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            EnsureValid();

            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("AjaxControlToolkit.CollapsiblePanelBehavior", targetControl.ClientID);
            descriptor.AddProperty("ClientStateFieldID", ClientStateFieldID);
            if (CollapsedSize != -1)
                descriptor.AddProperty("CollapsedSize", CollapsedSize);
            if (ExpandedSize != -1)
                descriptor.AddProperty("ExpandedSize", ExpandedSize);
            descriptor.AddProperty("CollapseControlID", CollapseControlID);
            descriptor.AddProperty("ExpandControlID", ExpandControlID);
            descriptor.AddProperty("Collapsed", Collapsed);
            descriptor.AddProperty("ScrollContents", ScrollContents);
            descriptor.AddProperty("SuppressPostBack", SuppressPostBack);
            descriptor.AddProperty("CollapsedText", CollapsedText);
            descriptor.AddProperty("ExpandedText", ExpandedText);
            descriptor.AddProperty("TextLabelID", TextLabelID);
            descriptor.AddProperty("CollapsedImage", ResolveClientUrl(CollapsedImage));
            descriptor.AddProperty("ExpandedImage", ResolveClientUrl(ExpandedImage));
            descriptor.AddProperty("ImageControlID", ImageControlID);
            descriptor.AddProperty("ExpandDirection", ExpandDirection);

            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[]
                {
                    new ScriptReference("FirstLook.Common.WebControls.Timer.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.Animation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Extenders.CollapsiblePanelBehavior.js", "FirstLook.Common.WebControls")
                };
        }
    }
}
