using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(Control)),
     ParseChildren(true),
     PersistChildren(false)]
    public sealed class ResizableControl : ExtenderControl
    {
        private Dimension maximumDimension;
        private Dimension minimumDimension;

        [IDReferenceProperty(typeof(HiddenField))]
        public string ClientStateFieldID
        {
            get
            {
                string value = (string)ViewState["ClientStateFieldID"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (string.Compare(ClientStateFieldID, value) != 0)
                {
                    ViewState["ClientStateFieldID"] = value;
                }
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Maximum Control Size"),
         Category("Styles"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dimension MaximumDimension
        {
            get
            {
                if (maximumDimension == null)
                {
                    maximumDimension = new Dimension();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)maximumDimension).TrackViewState();
                    }
                }
                return maximumDimension;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Minimum Control Size"),
         Category("Styles"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dimension MinimumDimension
        {
            get
            {
                if (minimumDimension == null)
                {
                    minimumDimension = new Dimension();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)minimumDimension).TrackViewState();
                    }
                }
                return minimumDimension;
            }
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                object[] objArray = (object[])savedState;
                if (objArray[0] != null)
                {
                    base.LoadViewState(objArray[0]);
                }
                if (objArray[1] != null)
                {
                    ((IStateManager)MinimumDimension).LoadViewState(objArray[1]);
                }
                if (objArray[2] != null)
                {
                    ((IStateManager)MaximumDimension).LoadViewState(objArray[2]);
                }
            }
        }

        protected override object SaveViewState()
        {
            object obj0 = base.SaveViewState();
            object obj1 = (minimumDimension != null) ? ((IStateManager)minimumDimension).SaveViewState() : null;
            object obj2 = (maximumDimension != null) ? ((IStateManager)maximumDimension).SaveViewState() : null;
            return new object[] { obj0, obj1, obj2 };
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            Control state = FindControl(ClientStateFieldID);

            if (state == null)
            {
                throw new InvalidOperationException("Invalid ClientStateFieldID");
            }

            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior", targetControl.ClientID);
            descriptor.AddProperty("clientStateFieldID", state.ClientID);
            if (minimumDimension != null)
            {
                descriptor.AddProperty("minimumWidth", minimumDimension.Width.Value);
                descriptor.AddProperty("minimumHeight", minimumDimension.Height.Value);
            }
            if (maximumDimension != null)
            {
                descriptor.AddProperty("maximumWidth", maximumDimension.Width.Value);
                descriptor.AddProperty("maximumHeight", maximumDimension.Height.Value);
            }
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[] { new ScriptReference("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior.js", "FirstLook.Common.WebControls") };
        }
    }
}
