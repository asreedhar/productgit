
Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.SliderLabelBehavior = function (element)
{
    FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.initializeBase(this,[element]);
    this._LabelID = null;
    this._LabelFormat = '{0}';
    this._ValueMappings = {};
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$initialize () {
    FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.callBaseMethod(this, 'initialize');
    this._OnChangeDelegate = Function.createDelegate(this, this._OnChange);
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$dispose () {
    // dispose of drop-down change event handlers
    var input = this.get_element();
    if (input) {
        $removeHandler(input, 'change', this._OnChangeDelegate);
    }
    delete this._OnChangeDelegate;
    // finally
    FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.callBaseMethod(this, 'dispose');
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$updated () {
    var input = this.get_element();
    var label = this.get_Label();
    if (input && label) {
        this._OnChange();
        $addHandler(input, 'change', this._OnChangeDelegate);
    }
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_Label () {
    return $get(this._LabelID);
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_LabelID () {
    return this._LabelID;
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$set_LabelID (value) {
    this._LabelID = value;
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_LabelFormat () {
    return this._LabelFormat;
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$set_LabelFormat (value) {
    this._LabelFormat = value;
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_ValueMappings () {
    return this._ValueMappings;
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$set_ValueMappings (value) {
    this._ValueMappings = value;
}

function FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$_OnChange (e) {
    var input = this.get_element();
    var label = this.get_Label();
    var format = this.get_LabelFormat()
    if (input && label) {
        var mappings = this.get_ValueMappings();
        var value = input.value;
        for (var i in mappings) {
            if (i == value) {
                value = mappings[i];
            }
        }
        label.innerHTML = String.localeFormat(format, value);
    }
}

FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.prototype =
{
    initialize:         FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$initialize,
    dispose:            FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$dispose,
    updated:            FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$updated,
    get_Label:          FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_Label,
    get_LabelID:        FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_LabelID,
    set_LabelID:        FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$set_LabelID,
    get_LabelFormat:    FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_LabelFormat,
    set_LabelFormat:    FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$set_LabelFormat,
    get_ValueMappings:  FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$get_ValueMappings,
    set_ValueMappings:  FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$set_ValueMappings,
    _OnChange:          FirstLook$Common$WebControls$Extenders$SliderLabelBehavior$_OnChange
}

FirstLook.Common.WebControls.Extenders.SliderLabelBehavior.registerClass('FirstLook.Common.WebControls.Extenders.SliderLabelBehavior', Sys.UI.Behavior);

