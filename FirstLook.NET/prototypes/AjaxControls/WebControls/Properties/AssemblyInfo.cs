﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WebControls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Incisent Technologies, LLC.")]
[assembly: AssemblyProduct("WebControls")]
[assembly: AssemblyCopyright("Copyright © Incisent Technologies, LLC. 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9b6e6f98-3494-4691-89ce-c3b485772576")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

// web assembly resources

[assembly: WebResource("FirstLook.Common.WebControls.DragDropScripts.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.DragDropScripts.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Timer.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Timer.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.Action.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.Action.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.Animation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.Animation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.CaseAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.CaseAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.ColorAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.ColorAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.ConditionAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.ConditionAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.DiscreteAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.DiscreteAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.FadeAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.FadeAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.LengthAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.MoveAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.MoveAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.ParallelAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.ParallelAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.ParentAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.ParentAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.PropertyAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.PulseAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.PulseAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.ResizeAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.ResizeAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.SelectionAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.SelectionAnimation.debug.js", "text/javascript")]

[assembly: WebResource("FirstLook.Common.WebControls.Animations.SequenceAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.SequenceAnimation.debug.js", "text/javascript")]
