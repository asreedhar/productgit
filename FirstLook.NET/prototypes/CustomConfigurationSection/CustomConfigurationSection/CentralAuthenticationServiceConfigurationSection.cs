using System.Configuration;

namespace CustomConfigurationSection
{
    public class CentralAuthenticationServiceConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("applicationServerName", IsRequired = true)]
        public string ApplicationServerName
        {
            get { return (string)this["applicationServerName"]; }
            set { this["applicationServerName"] = value; }
        }

        [ConfigurationProperty("validateUrl", IsRequired = true)]
        public string ValidateUrl
        {
            get { return (string) this["validateUrl"]; }
            set { this["validateUrl"] = value; }
        }

        [ConfigurationProperty("loginUrl", IsRequired = true)]
        public string LoginUrl
        {
            get { return (string) this["loginUrl"]; }
            set { this["loginUrl"] = value; }
        }

        [ConfigurationProperty("logoutUrl", IsRequired = true)]
        public string LogoutUrl
        {
            get { return (string) this["logoutUrl"]; }
            set { this["logoutUrl"] = value; }
        }

        [ConfigurationProperty("gateway", DefaultValue = false, IsRequired = true)]
        public bool Gateway
        {
            get { return (bool) this["gateway"]; }
            set { this["gateway"] = value; }
        }

        [ConfigurationProperty("renew", DefaultValue = false, IsRequired = true)]
        public bool Renew
        {
            get { return (bool) this["renew"]; }
            set { this["renew"] = value; }
        }

        [ConfigurationProperty("exceptions", IsDefaultCollection = true)]
        public ExceptionConfigurationElement Exceptions
        {
            get { return (ExceptionConfigurationElement) this["exceptions"]; }
        }
    }
}