using System.Configuration;

namespace CustomConfigurationSection
{
    public class DirectoryConfigurationElement : ConfigurationElement
    {
        public DirectoryConfigurationElement()
        {
            Name = string.Empty;
        }

        public DirectoryConfigurationElement(string name)
        {
            Name = name;
        }

        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }
    }
}
