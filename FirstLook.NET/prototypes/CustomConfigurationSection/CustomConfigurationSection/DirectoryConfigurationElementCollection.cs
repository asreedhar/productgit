using System.Configuration;

namespace CustomConfigurationSection
{
    public class DirectoryConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new DirectoryConfigurationElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new DirectoryConfigurationElement(elementName);
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DirectoryConfigurationElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "directories"; }
        }
    }
}
