using System.Configuration;

namespace CustomConfigurationSection
{
    public class FileExtensionConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FileExtensionConfigurationElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new FileExtensionConfigurationElement(elementName);
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FileExtensionConfigurationElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "extensions"; }
        }
    }
}
