using System.Configuration;

namespace CustomConfigurationSection
{
    public class FileConfigurationElement : ConfigurationElement
    {
        public FileConfigurationElement()
        {
            Name = string.Empty;
        }

        public FileConfigurationElement(string name)
        {
            Name = name;
        }

        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }
}
