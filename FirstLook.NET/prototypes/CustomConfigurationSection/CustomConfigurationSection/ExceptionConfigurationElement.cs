using System.Configuration;

namespace CustomConfigurationSection
{
    /// <summary>
    /// Summary description for ExceptionConfigurationElement
    /// </summary>
    public class ExceptionConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("directories", IsDefaultCollection = true)]
        public DirectoryConfigurationElementCollection Directories
        {
            get
            {
                return (DirectoryConfigurationElementCollection)this["directories"];
            }
        }

        [ConfigurationProperty("fileExtensions", IsDefaultCollection = false)]
        public FileExtensionConfigurationElementCollection FileExtensions
        {
            get
            {
                return (FileExtensionConfigurationElementCollection)this["fileExtensions"];
            }
        }

        [ConfigurationProperty("files", IsDefaultCollection = false)]
        public FileConfigurationElementCollection Files
        {
            get
            {
                return (FileConfigurationElementCollection)this["files"];
            }
        }
    }
}