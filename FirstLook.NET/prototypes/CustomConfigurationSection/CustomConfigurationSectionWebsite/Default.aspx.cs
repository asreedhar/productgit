using System;
using System.Configuration;
using System.Web.UI;
using CustomConfigurationSection;

public partial class _Default : Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CentralAuthenticationServiceConfigurationSection configuration = (CentralAuthenticationServiceConfigurationSection)
                ConfigurationManager.GetSection("centralAuthenticationService");

            if (configuration == null)
            {
                Label1.Text = "N/A";
                Label2.Text = "N/A";
                Label3.Text = "N/A";
                Label4.Text = "N/A";
                Label5.Text = "N/A";
                Label6.Text = "N/A";
            }
            else
            {
                Label1.Text = configuration.ValidateUrl;
                Label2.Text = configuration.LoginUrl;
                Label3.Text = configuration.LogoutUrl;
                Label4.Text = configuration.Gateway.ToString();
                Label5.Text = configuration.Renew.ToString();
                Label6.Text = configuration.ApplicationServerName;
            }
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        // string path = Request.AppRelativeCurrentExecutionFilePath;

        string path = PathTextBox.Text;

        if (string.IsNullOrEmpty(path))
        {
            ResultTextBox.Text = "Invalid Path!";
        }
        else
        {
            CentralAuthenticationServiceConfigurationSection configuration = (CentralAuthenticationServiceConfigurationSection)
                ConfigurationManager.GetSection("centralAuthenticationService");

            if (configuration == null)
            {
                ResultTextBox.Text = "No Custom Section!";
            }
            else
            {
                string filename = path;
                string extension = (path.LastIndexOf('.') != -1)
                    ? string.Empty
                    : path.Substring(path.LastIndexOf('.'));
                string directory = path.Substring(0,path.LastIndexOf('/')+1);

                bool ignore = false;

                foreach (DirectoryConfigurationElement element in configuration.Exceptions.Directories)
                {
                    if (ElementNameIsValid(element.Name))
                        continue;

                    if (string.Compare(element.Name, directory, true) == 0 || directory.StartsWith(element.Name))
                    {
                        ignore = true;
                        break;
                    }
                }

                if (!ignore)
                {
                    foreach (FileExtensionConfigurationElement element in configuration.Exceptions.FileExtensions)
                    {
                        if (ElementNameIsValid(element.Name))
                            continue;

                        if (string.Compare(element.Name, extension, true) == 0)
                        {
                            ignore = true;
                            break;
                        }
                    }
                }

                if (!ignore)
                {
                    foreach (FileConfigurationElement element in configuration.Exceptions.Files)
                    {
                        if (ElementNameIsValid(element.Name))
                            continue;

                        if (string.Compare(element.Name, filename, true) == 0)
                        {
                            ignore = true;
                            break;
                        }
                    }
                }

                ResultTextBox.Text = ignore ? "No Authentication" : "Authentication";
            }
        }
    }

    private static bool ElementNameIsValid(string elementName)
    {
        return string.IsNullOrEmpty(elementName) || !elementName.StartsWith("~/");
    }
}
