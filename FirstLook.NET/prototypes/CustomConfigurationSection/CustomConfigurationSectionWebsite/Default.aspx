﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Custom Configuration - Test Form</title>
    <style type="text/css">
    q { font-family: courier; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Test Configuration Section</h1>
        <h2>Properties</h2>
        <table border="1">
            <thead>
                <tr><th>Property</th><th>Value</th></tr>
            </thead>
            <tbody>
                <tr><td>Validate URL</td><td>
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td></tr>
                <tr><td>Login URL</td><td>
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td></tr>
                <tr><td>Logout URL</td><td>
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td></tr>
                <tr><td>Gateway</td><td>
                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></td></tr>
                <tr><td>Renew</td><td>
                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td></tr>
                <tr><td>Application Server Name</td><td>
                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td></tr>
            </tbody>
        </table>
        <h2>Request Test</h2>
        <p>Enter a path, such as <q>~/Public/Images/indicator.png</q>, into the text box and click submit.</p>
        <p>The result will be calculated and rendered in the second text box.</p>
        <p>The entered path must start with <q>~/</q> to reflect an application relative path.</p>
        <asp:Label ID="PathLabel" runat="server" Text="Enter Path: " AssociatedControlID="PathTextBox"></asp:Label>
        <asp:TextBox ID="PathTextBox" runat="server"></asp:TextBox>
        <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
        <br />
        <asp:Label ID="Result" runat="server" Text="Result: "></asp:Label>
        <asp:TextBox ID="ResultTextBox" runat="server" ReadOnly="true"></asp:TextBox>
    </div>
    </form>
</body>
</html>
