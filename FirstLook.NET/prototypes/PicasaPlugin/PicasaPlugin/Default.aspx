﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstLook.Photos.PicasaUploader.Default" ValidateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FirstLook Photo Uploader</title>
</head>
<body>
    <form method="post" id="UploaderForm" runat="server" enctype="multipart/form-data" action="Upload.aspx">   
    
        <asp:Panel ID="PicasaUploaderLayout" runat="server" Visible="true">
        
            <h1>FIRSTLOOK UPLOADER</h1>                
                              
            <input type="submit" value="Upload Files"/>
            <br />
            <br />
                         
            <asp:Repeater ID="PicasaImageRepeater" runat="server">
                <HeaderTemplate><table></HeaderTemplate>
                <FooterTemplate></table></FooterTemplate>
                <ItemTemplate>
                    <tr>
                        <td>                                                                                    
                            <img src="<%# XPath("photo:thumbnail", nsmgr) %>" alt="<%# XPath("title") %>" />                                                
                            <input type="hidden" name='<%# XPath("photo:imgsrc", nsmgr) %>' />
                        </td>                    
                    </tr>
                </ItemTemplate>
            </asp:Repeater> 
                               
        </asp:Panel>
    
    </form>
</body>
</html>
