﻿using System;

namespace FirstLook.Photos.PicasaUploader
{
    public partial class View : System.Web.UI.Page
    {
        public string baseDirectory = @"C:\\Projects\\5.19\\FirstLook.NET\\prototypes\\PicasaPlugin\\PicasaPlugin\\UploadedImages\\";
        public string[] directories;

        /// <summary>
        /// Get the list of files in the uploaded file directory.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                directories = System.IO.Directory.GetDirectories(baseDirectory);
            }
        }
    }
}
