﻿using System;
using System.Xml;

namespace FirstLook.Photos.PicasaUploader
{
    public partial class Default : System.Web.UI.Page
    {
        private XmlDocument xmlDoc;
        protected XmlNamespaceManager nsmgr;

        /// <summary>
        /// Parse the Picasa rss xml feed, and bind it to the image repeater.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                xmlDoc = new XmlDocument();

                string rssXml = Request["rss"];
                if (rssXml != null)
                {
                    xmlDoc.LoadXml(rssXml);

                    nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                    nsmgr.AddNamespace("photo", "http://www.pheed.com/pheed/");
                    nsmgr.AddNamespace("media", "http://search.yahoo.com/msrss/");

                    XmlNode rssNode = xmlDoc.SelectSingleNode("rss", nsmgr);

                    if (rssNode != null && rssNode.HasChildNodes)
                    {
                        PicasaImageRepeater.DataSource = xmlDoc.SelectNodes("rss/channel/item", nsmgr);
                        PicasaImageRepeater.DataBind();
                    }
                }
            }
        }
    }
}
