﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Web;
using System.Windows.Media.Imaging;

namespace FirstLook.Photos.PicasaUploader
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            SavePostedFiles();
        }

        /// <summary>
        /// Read the uploaded files from the Post, and save them to the server. Also, write a result page to the response.
        /// </summary>
        private void SavePostedFiles()
        {
            const string fileBasePath = @"C:\\Projects\\5.19\\FirstLook.NET\\prototypes\\PicasaPlugin\\PicasaPlugin\\UploadedImages\\";

            if (!Directory.Exists(fileBasePath))
            {
                Directory.CreateDirectory(fileBasePath);
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile file = Request.Files[i];

                string filePath = fileBasePath;

                try
                {
                    BitmapDecoder decoder = BitmapDecoder.Create(file.InputStream, BitmapCreateOptions.None,
                                                                 BitmapCacheOption.OnLoad);
                    BitmapMetadata metadata = (BitmapMetadata) decoder.Frames[0].Metadata;

                    ReadOnlyCollection<string> keywords = metadata.Keywords;

                    // Check if the keyword is a VIN.
                    foreach (string keyword in keywords)
                    {
                        if (keyword.Length == 17)
                        {
                            filePath += keyword + @"\\";
                        }
                    }

                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                }
                catch(Exception)
                {                    
                }

                file.SaveAs(filePath + file.FileName);
            }

            Response.StatusCode = 200;                        
            Response.Write("https://cclouston01.firstlook.biz/PicasaPlugin/View.aspx");            
            Response.End();
        }
    }
}
