﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="View.aspx.cs" Inherits="FirstLook.Photos.PicasaUploader.View" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>View Uploaded Files</title>    
</head>

<body>
    <form id="form1" runat="server">
    
        <asp:Panel ID="UploadedFileLayout" runat="server" Visible="true">
        
            <h1>UPLOADED FILES</h1>                
                                  
            <h2>No VIN</h2>                                                                                                                 
                                          
            <%
                string[] files = System.IO.Directory.GetFiles(baseDirectory);
                
                foreach (string file in files)
                {
                    string filePath = file.Replace(@"C:\\Projects\\5.19\\FirstLook.NET\\prototypes\\PicasaPlugin\\PicasaPlugin\\UploadedImages\\", @"https://cclouston01.firstlook.biz/PicasaPlugin/UploadedImages/");
                    filePath = filePath.Replace(@"\",@"/");
                                              
                    %>
                    
                    <a href="<% Response.Write(filePath); %>"><img src="<% Response.Write(filePath); %>" alt="<% Response.Write(filePath); %>" width="400px"/></a>
                    <br /><br />
                    
                    <% 
                }
                
                foreach (string directory in directories)
                {

                    files = System.IO.Directory.GetFiles(directory);
                    
                    string vin = directory.Substring(directory.LastIndexOf(@"\") + 1);                        
                    
                    %>
                    
                    <h2><% Response.Write(vin); %></h2>
                    
                    <%

                    foreach (string file in files)
                    {
                        string filePath = file.Replace(@"C:\\Projects\\5.19\\FirstLook.NET\\prototypes\\PicasaPlugin\\PicasaPlugin\\UploadedImages\\", @"https://cclouston01.firstlook.biz/PicasaPlugin/UploadedImages/");
                        filePath = filePath.Replace(@"\",@"/");
                                                  
                        %>
                        
                        <a href="<% Response.Write(filePath); %>"><img src="<% Response.Write(filePath); %>" alt="<% Response.Write(filePath); %>" width="400px"/></a>
                        <br /><br />
                        
                        <% 
                    }
                }
            %>
                               
        </asp:Panel>
        
    </form>
</body>
</html>
