﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HtmlHeadAdapterTest</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                <asp:ListItem Text="item1" Value="item1"></asp:ListItem>
                <asp:ListItem Text="item2" Value="item2"></asp:ListItem>
                <asp:ListItem Text="item3" Value="item3"></asp:ListItem>
            </asp:CheckBoxList>
            <p>
                Hello World
            </p>
        </div>
    </form>
</body>
</html>
