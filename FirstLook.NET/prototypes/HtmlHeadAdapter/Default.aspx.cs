using System;
using System.Web.UI;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Text = Request.Browser.Capabilities["type"].ToString();
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (string.Compare(Request.QueryString["theme"], "sample") == 0)
        {
            Page.Theme = "Sample";
        }
        else
        {
            Page.Theme = "Default";
        }
    }
}