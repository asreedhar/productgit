/*!
 * jQuery JavaScript Library v1.7.2
 * http://jquery.com/
 *
 * Copyright 2011, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 * Copyright 2011, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 *
 * Date: Wed Mar 21 12:46:34 2012 -0700
 */
(function(window, undefined) {

    // Use the correct document accordingly with window argument (sandbox)
    var document = window.document,
        navigator = window.navigator,
        location = window.location;
    var jQuery = (function() {

        // Define a local copy of jQuery
        var jQuery = function(selector, context) {
                // The jQuery object is actually just the init constructor 'enhanced'
                return new jQuery.fn.init(selector, context, rootjQuery);
            },

            // Map over jQuery in case of overwrite
            _jQuery = window.jQuery,

            // Map over the $ in case of overwrite
            _$ = window.$,

            // A central reference to the root jQuery(document)
            rootjQuery,

            // A simple way to check for HTML strings or ID strings
            // Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
            quickExpr = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,

            // Check if a string has a non-whitespace character in it
            rnotwhite = /\S/,

            // Used for trimming whitespace
            trimLeft = /^\s+/,
            trimRight = /\s+$/,

            // Match a standalone tag
            rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>)?$/,

            // JSON RegExp
            rvalidchars = /^[\],:{}\s]*$/,
            rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
            rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
            rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g,

            // Useragent RegExp
            rwebkit = /(webkit)[ \/]([\w.]+)/,
            ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/,
            rmsie = /(msie) ([\w.]+)/,
            rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/,

            // Matches dashed string for camelizing
            rdashAlpha = /-([a-z]|[0-9])/ig,
            rmsPrefix = /^-ms-/,

            // Used by jQuery.camelCase as callback to replace()
            fcamelCase = function(all, letter) {
                return (letter + "").toUpperCase();
            },

            // Keep a UserAgent string for use with jQuery.browser
            userAgent = navigator.userAgent,

            // For matching the engine and version of the browser
            browserMatch,

            // The deferred used on DOM ready
            readyList,

            // The ready event handler
            DOMContentLoaded,

            // Save a reference to some core methods
            toString = Object.prototype.toString,
            hasOwn = Object.prototype.hasOwnProperty,
            push = Array.prototype.push,
            slice = Array.prototype.slice,
            trim = String.prototype.trim,
            indexOf = Array.prototype.indexOf,

            // [[Class]] -> type pairs
            class2type = {};

        jQuery.fn = jQuery.prototype = {
            constructor: jQuery,
            init: function(selector, context, rootjQuery) {
                var match, elem, ret, doc;

                // Handle $(""), $(null), or $(undefined)
                if (!selector) {
                    return this;
                }

                // Handle $(DOMElement)
                if (selector.nodeType) {
                    this.context = this[0] = selector;
                    this.length = 1;
                    return this;
                }

                // The body element only exists once, optimize finding it
                if (selector === "body" && !context && document.body) {
                    this.context = document;
                    this[0] = document.body;
                    this.selector = selector;
                    this.length = 1;
                    return this;
                }

                // Handle HTML strings
                if (typeof selector === "string") {
                    // Are we dealing with HTML string or an ID?
                    if (selector.charAt(0) === "<" && selector.charAt(selector.length - 1) === ">" && selector.length >= 3) {
                        // Assume that strings that start and end with <> are HTML and skip the regex check
                        match = [null, selector, null];

                    } else {
                        match = quickExpr.exec(selector);
                    }

                    // Verify a match, and that no context was specified for #id
                    if (match && (match[1] || !context)) {

                        // HANDLE: $(html) -> $(array)
                        if (match[1]) {
                            context = context instanceof jQuery ? context[0] : context;
                            doc = (context ? context.ownerDocument || context : document);

                            // If a single string is passed in and it's a single tag
                            // just do a createElement and skip the rest
                            ret = rsingleTag.exec(selector);

                            if (ret) {
                                if (jQuery.isPlainObject(context)) {
                                    selector = [document.createElement(ret[1])];
                                    jQuery.fn.attr.call(selector, context, true);

                                } else {
                                    selector = [doc.createElement(ret[1])];
                                }

                            } else {
                                ret = jQuery.buildFragment([match[1]], [doc]);
                                selector = (ret.cacheable ? jQuery.clone(ret.fragment) : ret.fragment).childNodes;
                            }

                            return jQuery.merge(this, selector);

                            // HANDLE: $("#id")
                        } else {
                            elem = document.getElementById(match[2]);

                            // Check parentNode to catch when Blackberry 4.6 returns
                            // nodes that are no longer in the document #6963
                            if (elem && elem.parentNode) {
                                // Handle the case where IE and Opera return items
                                // by name instead of ID
                                if (elem.id !== match[2]) {
                                    return rootjQuery.find(selector);
                                }

                                // Otherwise, we inject the element directly into the jQuery object
                                this.length = 1;
                                this[0] = elem;
                            }

                            this.context = document;
                            this.selector = selector;
                            return this;
                        }

                        // HANDLE: $(expr, $(...))
                    } else if (!context || context.jquery) {
                        return (context || rootjQuery).find(selector);

                        // HANDLE: $(expr, context)
                        // (which is just equivalent to: $(context).find(expr)
                    } else {
                        return this.constructor(context).find(selector);
                    }

                    // HANDLE: $(function)
                    // Shortcut for document ready
                } else if (jQuery.isFunction(selector)) {
                    return rootjQuery.ready(selector);
                }

                if (selector.selector !== undefined) {
                    this.selector = selector.selector;
                    this.context = selector.context;
                }

                return jQuery.makeArray(selector, this);
            },

            // Start with an empty selector
            selector: "",

            // The current version of jQuery being used
            jquery: "1.7.2",

            // The default length of a jQuery object is 0
            length: 0,

            // The number of elements contained in the matched element set
            size: function() {
                return this.length;
            },

            toArray: function() {
                return slice.call(this, 0);
            },

            // Get the Nth element in the matched element set OR
            // Get the whole matched element set as a clean array
            get: function(num) {
                return num == null ?

                // Return a 'clean' array
                this.toArray() :

                // Return just the object
                (num < 0 ? this[this.length + num] : this[num]);
            },

            // Take an array of elements and push it onto the stack
            // (returning the new matched element set)
            pushStack: function(elems, name, selector) {
                // Build a new jQuery matched element set
                var ret = this.constructor();

                if (jQuery.isArray(elems)) {
                    push.apply(ret, elems);

                } else {
                    jQuery.merge(ret, elems);
                }

                // Add the old object onto the stack (as a reference)
                ret.prevObject = this;

                ret.context = this.context;

                if (name === "find") {
                    ret.selector = this.selector + (this.selector ? " " : "") + selector;
                } else if (name) {
                    ret.selector = this.selector + "." + name + "(" + selector + ")";
                }

                // Return the newly-formed element set
                return ret;
            },

            // Execute a callback for every element in the matched set.
            // (You can seed the arguments with an array of args, but this is
            // only used internally.)
            each: function(callback, args) {
                return jQuery.each(this, callback, args);
            },

            ready: function(fn) {
                // Attach the listeners
                jQuery.bindReady();

                // Add the callback
                readyList.add(fn);

                return this;
            },

            eq: function(i) {
                i = +i;
                return i === -1 ? this.slice(i) : this.slice(i, i + 1);
            },

            first: function() {
                return this.eq(0);
            },

            last: function() {
                return this.eq(-1);
            },

            slice: function() {
                return this.pushStack(slice.apply(this, arguments), "slice", slice.call(arguments).join(","));
            },

            map: function(callback) {
                return this.pushStack(jQuery.map(this, function(elem, i) {
                    return callback.call(elem, i, elem);
                }));
            },

            end: function() {
                return this.prevObject || this.constructor(null);
            },

            // For internal use only.
            // Behaves like an Array's method, not like a jQuery method.
            push: push,
            sort: [].sort,
            splice: [].splice
        };

        // Give the init function the jQuery prototype for later instantiation
        jQuery.fn.init.prototype = jQuery.fn;

        jQuery.extend = jQuery.fn.extend = function() {
            var options, name, src, copy, copyIsArray, clone, target = arguments[0] || {},
                i = 1,
                length = arguments.length,
                deep = false;

            // Handle a deep copy situation
            if (typeof target === "boolean") {
                deep = target;
                target = arguments[1] || {};
                // skip the boolean and the target
                i = 2;
            }

            // Handle case when target is a string or something (possible in deep copy)
            if (typeof target !== "object" && !jQuery.isFunction(target)) {
                target = {};
            }

            // extend jQuery itself if only one argument is passed
            if (length === i) {
                target = this;
                --i;
            }

            for (; i < length; i++) {
                // Only deal with non-null/undefined values
                if ((options = arguments[i]) != null) {
                    // Extend the base object
                    for (name in options) {
                        src = target[name];
                        copy = options[name];

                        // Prevent never-ending loop
                        if (target === copy) {
                            continue;
                        }

                        // Recurse if we're merging plain objects or arrays
                        if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                            if (copyIsArray) {
                                copyIsArray = false;
                                clone = src && jQuery.isArray(src) ? src : [];

                            } else {
                                clone = src && jQuery.isPlainObject(src) ? src : {};
                            }

                            // Never move original objects, clone them
                            target[name] = jQuery.extend(deep, clone, copy);

                            // Don't bring in undefined values
                        } else if (copy !== undefined) {
                            target[name] = copy;
                        }
                    }
                }
            }

            // Return the modified object
            return target;
        };

        jQuery.extend({
            noConflict: function(deep) {
                if (window.$ === jQuery) {
                    window.$ = _$;
                }

                if (deep && window.jQuery === jQuery) {
                    window.jQuery = _jQuery;
                }

                return jQuery;
            },

            // Is the DOM ready to be used? Set to true once it occurs.
            isReady: false,

            // A counter to track how many items to wait for before
            // the ready event fires. See #6781
            readyWait: 1,

            // Hold (or release) the ready event
            holdReady: function(hold) {
                if (hold) {
                    jQuery.readyWait++;
                } else {
                    jQuery.ready(true);
                }
            },

            // Handle when the DOM is ready
            ready: function(wait) {
                // Either a released hold or an DOMready/load event and not yet ready
                if ((wait === true && !--jQuery.readyWait) || (wait !== true && !jQuery.isReady)) {
                    // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
                    if (!document.body) {
                        return setTimeout(jQuery.ready, 1);
                    }

                    // Remember that the DOM is ready
                    jQuery.isReady = true;

                    // If a normal DOM Ready event fired, decrement, and wait if need be
                    if (wait !== true && --jQuery.readyWait > 0) {
                        return;
                    }

                    // If there are functions bound, to execute
                    readyList.fireWith(document, [jQuery]);

                    // Trigger any bound ready events
                    if (jQuery.fn.trigger) {
                        jQuery(document).trigger("ready").off("ready");
                    }
                }
            },

            bindReady: function() {
                if (readyList) {
                    return;
                }

                readyList = jQuery.Callbacks("once memory");

                // Catch cases where $(document).ready() is called after the
                // browser event has already occurred.
                if (document.readyState === "complete") {
                    // Handle it asynchronously to allow scripts the opportunity to delay ready
                    return setTimeout(jQuery.ready, 1);
                }

                // Mozilla, Opera and webkit nightlies currently support this event
                if (document.addEventListener) {
                    // Use the handy event callback
                    document.addEventListener("DOMContentLoaded", DOMContentLoaded, false);

                    // A fallback to window.onload, that will always work
                    window.addEventListener("load", jQuery.ready, false);

                    // If IE event model is used
                } else if (document.attachEvent) {
                    // ensure firing before onload,
                    // maybe late but safe also for iframes
                    document.attachEvent("onreadystatechange", DOMContentLoaded);

                    // A fallback to window.onload, that will always work
                    window.attachEvent("onload", jQuery.ready);

                    // If IE and not a frame
                    // continually check to see if the document is ready
                    var toplevel = false;

                    try {
                        toplevel = window.frameElement == null;
                    } catch (e) {}

                    if (document.documentElement.doScroll && toplevel) {
                        doScrollCheck();
                    }
                }
            },

            // See test/unit/core.js for details concerning isFunction.
            // Since version 1.3, DOM methods and functions like alert
            // aren't supported. They return false on IE (#2968).
            isFunction: function(obj) {
                return jQuery.type(obj) === "function";
            },

            isArray: Array.isArray ||
            function(obj) {
                return jQuery.type(obj) === "array";
            },

            isWindow: function(obj) {
                return obj != null && obj == obj.window;
            },

            isNumeric: function(obj) {
                return !isNaN(parseFloat(obj)) && isFinite(obj);
            },

            type: function(obj) {
                return obj == null ? String(obj) : class2type[toString.call(obj)] || "object";
            },

            isPlainObject: function(obj) {
                // Must be an Object.
                // Because of IE, we also have to check the presence of the constructor property.
                // Make sure that DOM nodes and window objects don't pass through, as well
                if (!obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow(obj)) {
                    return false;
                }

                try {
                    // Not own constructor property must be Object
                    if (obj.constructor && !hasOwn.call(obj, "constructor") && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                        return false;
                    }
                } catch (e) {
                    // IE8,9 Will throw exceptions on certain host objects #9897
                    return false;
                }

                // Own properties are enumerated firstly, so to speed up,
                // if last one is own, then all properties are own.
                var key;
                for (key in obj) {}

                return key === undefined || hasOwn.call(obj, key);
            },

            isEmptyObject: function(obj) {
                for (var name in obj) {
                    return false;
                }
                return true;
            },

            error: function(msg) {
                throw new Error(msg);
            },

            parseJSON: function(data) {
                if (typeof data !== "string" || !data) {
                    return null;
                }

                // Make sure leading/trailing whitespace is removed (IE can't handle it)
                data = jQuery.trim(data);

                // Attempt to parse using the native JSON parser first
                if (window.JSON && window.JSON.parse) {
                    return window.JSON.parse(data);
                }

                // Make sure the incoming data is actual JSON
                // Logic borrowed from http://json.org/json2.js
                if (rvalidchars.test(data.replace(rvalidescape, "@").replace(rvalidtokens, "]").replace(rvalidbraces, ""))) {

                    return (new Function("return " + data))();

                }
                jQuery.error("Invalid JSON: " + data);
            },

            // Cross-browser xml parsing
            parseXML: function(data) {
                if (typeof data !== "string" || !data) {
                    return null;
                }
                var xml, tmp;
                try {
                    if (window.DOMParser) { // Standard
                        tmp = new DOMParser();
                        xml = tmp.parseFromString(data, "text/xml");
                    } else { // IE
                        xml = new ActiveXObject("Microsoft.XMLDOM");
                        xml.async = "false";
                        xml.loadXML(data);
                    }
                } catch (e) {
                    xml = undefined;
                }
                if (!xml || !xml.documentElement || xml.getElementsByTagName("parsererror").length) {
                    jQuery.error("Invalid XML: " + data);
                }
                return xml;
            },

            noop: function() {},

            // Evaluates a script in a global context
            // Workarounds based on findings by Jim Driscoll
            // http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
            globalEval: function(data) {
                if (data && rnotwhite.test(data)) {
                    // We use execScript on Internet Explorer
                    // We use an anonymous function so that context is window
                    // rather than jQuery in Firefox
                    (window.execScript ||
                    function(data) {
                        window["eval"].call(window, data);
                    })(data);
                }
            },

            // Convert dashed to camelCase; used by the css and data modules
            // Microsoft forgot to hump their vendor prefix (#9572)
            camelCase: function(string) {
                return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
            },

            nodeName: function(elem, name) {
                return elem.nodeName && elem.nodeName.toUpperCase() === name.toUpperCase();
            },

            // args is for internal usage only
            each: function(object, callback, args) {
                var name, i = 0,
                    length = object.length,
                    isObj = length === undefined || jQuery.isFunction(object);

                if (args) {
                    if (isObj) {
                        for (name in object) {
                            if (callback.apply(object[name], args) === false) {
                                break;
                            }
                        }
                    } else {
                        for (; i < length;) {
                            if (callback.apply(object[i++], args) === false) {
                                break;
                            }
                        }
                    }

                    // A special, fast, case for the most common use of each
                } else {
                    if (isObj) {
                        for (name in object) {
                            if (callback.call(object[name], name, object[name]) === false) {
                                break;
                            }
                        }
                    } else {
                        for (; i < length;) {
                            if (callback.call(object[i], i, object[i++]) === false) {
                                break;
                            }
                        }
                    }
                }

                return object;
            },

            // Use native String.trim function wherever possible
            trim: trim ?
            function(text) {
                return text == null ? "" : trim.call(text);
            } :

            // Otherwise use our own trimming functionality


            function(text) {
                return text == null ? "" : text.toString().replace(trimLeft, "").replace(trimRight, "");
            },

            // results is for internal usage only
            makeArray: function(array, results) {
                var ret = results || [];

                if (array != null) {
                    // The window, strings (and functions) also have 'length'
                    // Tweaked logic slightly to handle Blackberry 4.7 RegExp issues #6930
                    var type = jQuery.type(array);

                    if (array.length == null || type === "string" || type === "function" || type === "regexp" || jQuery.isWindow(array)) {
                        push.call(ret, array);
                    } else {
                        jQuery.merge(ret, array);
                    }
                }

                return ret;
            },

            inArray: function(elem, array, i) {
                var len;

                if (array) {
                    if (indexOf) {
                        return indexOf.call(array, elem, i);
                    }

                    len = array.length;
                    i = i ? i < 0 ? Math.max(0, len + i) : i : 0;

                    for (; i < len; i++) {
                        // Skip accessing in sparse arrays
                        if (i in array && array[i] === elem) {
                            return i;
                        }
                    }
                }

                return -1;
            },

            merge: function(first, second) {
                var i = first.length,
                    j = 0;

                if (typeof second.length === "number") {
                    for (var l = second.length; j < l; j++) {
                        first[i++] = second[j];
                    }

                } else {
                    while (second[j] !== undefined) {
                        first[i++] = second[j++];
                    }
                }

                first.length = i;

                return first;
            },

            grep: function(elems, callback, inv) {
                var ret = [],
                    retVal;
                inv = !! inv;

                // Go through the array, only saving the items
                // that pass the validator function
                for (var i = 0, length = elems.length; i < length; i++) {
                    retVal = !! callback(elems[i], i);
                    if (inv !== retVal) {
                        ret.push(elems[i]);
                    }
                }

                return ret;
            },

            // arg is for internal usage only
            map: function(elems, callback, arg) {
                var value, key, ret = [],
                    i = 0,
                    length = elems.length,
                    // jquery objects are treated as arrays
                    isArray = elems instanceof jQuery || length !== undefined && typeof length === "number" && ((length > 0 && elems[0] && elems[length - 1]) || length === 0 || jQuery.isArray(elems));

                // Go through the array, translating each of the items to their
                if (isArray) {
                    for (; i < length; i++) {
                        value = callback(elems[i], i, arg);

                        if (value != null) {
                            ret[ret.length] = value;
                        }
                    }

                    // Go through every key on the object,
                } else {
                    for (key in elems) {
                        value = callback(elems[key], key, arg);

                        if (value != null) {
                            ret[ret.length] = value;
                        }
                    }
                }

                // Flatten any nested arrays
                return ret.concat.apply([], ret);
            },

            // A global GUID counter for objects
            guid: 1,

            // Bind a function to a context, optionally partially applying any
            // arguments.
            proxy: function(fn, context) {
                if (typeof context === "string") {
                    var tmp = fn[context];
                    context = fn;
                    fn = tmp;
                }

                // Quick check to determine if target is callable, in the spec
                // this throws a TypeError, but we will just return undefined.
                if (!jQuery.isFunction(fn)) {
                    return undefined;
                }

                // Simulated bind
                var args = slice.call(arguments, 2),
                    proxy = function() {
                        return fn.apply(context, args.concat(slice.call(arguments)));
                    };

                // Set the guid of unique handler to the same of original handler, so it can be removed
                proxy.guid = fn.guid = fn.guid || proxy.guid || jQuery.guid++;

                return proxy;
            },

            // Mutifunctional method to get and set values to a collection
            // The value/s can optionally be executed if it's a function
            access: function(elems, fn, key, value, chainable, emptyGet, pass) {
                var exec, bulk = key == null,
                    i = 0,
                    length = elems.length;

                // Sets many values
                if (key && typeof key === "object") {
                    for (i in key) {
                        jQuery.access(elems, fn, i, key[i], 1, emptyGet, value);
                    }
                    chainable = 1;

                    // Sets one value
                } else if (value !== undefined) {
                    // Optionally, function values get executed if exec is true
                    exec = pass === undefined && jQuery.isFunction(value);

                    if (bulk) {
                        // Bulk operations only iterate when executing function values
                        if (exec) {
                            exec = fn;
                            fn = function(elem, key, value) {
                                return exec.call(jQuery(elem), value);
                            };

                            // Otherwise they run against the entire set
                        } else {
                            fn.call(elems, value);
                            fn = null;
                        }
                    }

                    if (fn) {
                        for (; i < length; i++) {
                            fn(elems[i], key, exec ? value.call(elems[i], i, fn(elems[i], key)) : value, pass);
                        }
                    }

                    chainable = 1;
                }

                return chainable ? elems :

                // Gets
                bulk ? fn.call(elems) : length ? fn(elems[0], key) : emptyGet;
            },

            now: function() {
                return (new Date()).getTime();
            },

            // Use of jQuery.browser is frowned upon.
            // More details: http://docs.jquery.com/Utilities/jQuery.browser
            uaMatch: function(ua) {
                ua = ua.toLowerCase();

                var match = rwebkit.exec(ua) || ropera.exec(ua) || rmsie.exec(ua) || ua.indexOf("compatible") < 0 && rmozilla.exec(ua) || [];

                return {
                    browser: match[1] || "",
                    version: match[2] || "0"
                };
            },

            sub: function() {
                function jQuerySub(selector, context) {
                    return new jQuerySub.fn.init(selector, context);
                }
                jQuery.extend(true, jQuerySub, this);
                jQuerySub.superclass = this;
                jQuerySub.fn = jQuerySub.prototype = this();
                jQuerySub.fn.constructor = jQuerySub;
                jQuerySub.sub = this.sub;
                jQuerySub.fn.init = function init(selector, context) {
                    if (context && context instanceof jQuery && !(context instanceof jQuerySub)) {
                        context = jQuerySub(context);
                    }

                    return jQuery.fn.init.call(this, selector, context, rootjQuerySub);
                };
                jQuerySub.fn.init.prototype = jQuerySub.fn;
                var rootjQuerySub = jQuerySub(document);
                return jQuerySub;
            },

            browser: {}
        });

        // Populate the class2type map
        jQuery.each("Boolean Number String Function Array Date RegExp Object".split(" "), function(i, name) {
            class2type["[object " + name + "]"] = name.toLowerCase();
        });

        browserMatch = jQuery.uaMatch(userAgent);
        if (browserMatch.browser) {
            jQuery.browser[browserMatch.browser] = true;
            jQuery.browser.version = browserMatch.version;
        }

        // Deprecated, use jQuery.browser.webkit instead
        if (jQuery.browser.webkit) {
            jQuery.browser.safari = true;
        }

        // IE doesn't match non-breaking spaces with \s
        if (rnotwhite.test("\xA0")) {
            trimLeft = /^[\s\xA0]+/;
            trimRight = /[\s\xA0]+$/;
        }

        // All jQuery objects should point back to these
        rootjQuery = jQuery(document);

        // Cleanup functions for the document ready method
        if (document.addEventListener) {
            DOMContentLoaded = function() {
                document.removeEventListener("DOMContentLoaded", DOMContentLoaded, false);
                jQuery.ready();
            };

        } else if (document.attachEvent) {
            DOMContentLoaded = function() {
                // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
                if (document.readyState === "complete") {
                    document.detachEvent("onreadystatechange", DOMContentLoaded);
                    jQuery.ready();
                }
            };
        }

        // The DOM ready check for Internet Explorer


        function doScrollCheck() {
            if (jQuery.isReady) {
                return;
            }

            try {
                // If IE is used, use the trick by Diego Perini
                // http://javascript.nwbox.com/IEContentLoaded/
                document.documentElement.doScroll("left");
            } catch (e) {
                setTimeout(doScrollCheck, 1);
                return;
            }

            // and execute any waiting functions
            jQuery.ready();
        }

        return jQuery;

    })();


    // String to Object flags format cache
    var flagsCache = {};

    // Convert String-formatted flags into Object-formatted ones and store in cache


    function createFlags(flags) {
        var object = flagsCache[flags] = {},
            i, length;
        flags = flags.split(/\s+/);
        for (i = 0, length = flags.length; i < length; i++) {
            object[flags[i]] = true;
        }
        return object;
    }

    /*
     * Create a callback list using the following parameters:
     *
     *	flags:	an optional list of space-separated flags that will change how
     *			the callback list behaves
     *
     * By default a callback list will act like an event callback list and can be
     * "fired" multiple times.
     *
     * Possible flags:
     *
     *	once:			will ensure the callback list can only be fired once (like a Deferred)
     *
     *	memory:			will keep track of previous values and will call any callback added
     *					after the list has been fired right away with the latest "memorized"
     *					values (like a Deferred)
     *
     *	unique:			will ensure a callback can only be added once (no duplicate in the list)
     *
     *	stopOnFalse:	interrupt callings when a callback returns false
     *
     */
    jQuery.Callbacks = function(flags) {

        // Convert flags from String-formatted to Object-formatted
        // (we check in cache first)
        flags = flags ? (flagsCache[flags] || createFlags(flags)) : {};

        var // Actual callback list
        list = [],
            // Stack of fire calls for repeatable lists
            stack = [],
            // Last fire value (for non-forgettable lists)
            memory,
            // Flag to know if list was already fired
            fired,
            // Flag to know if list is currently firing
            firing,
            // First callback to fire (used internally by add and fireWith)
            firingStart,
            // End of the loop when firing
            firingLength,
            // Index of currently firing callback (modified by remove if needed)
            firingIndex,
            // Add one or several callbacks to the list
            add = function(args) {
                var i, length, elem, type, actual;
                for (i = 0, length = args.length; i < length; i++) {
                    elem = args[i];
                    type = jQuery.type(elem);
                    if (type === "array") {
                        // Inspect recursively
                        add(elem);
                    } else if (type === "function") {
                        // Add if not in unique mode and callback is not in
                        if (!flags.unique || !self.has(elem)) {
                            list.push(elem);
                        }
                    }
                }
            },
            // Fire callbacks
            fire = function(context, args) {
                args = args || [];
                memory = !flags.memory || [context, args];
                fired = true;
                firing = true;
                firingIndex = firingStart || 0;
                firingStart = 0;
                firingLength = list.length;
                for (; list && firingIndex < firingLength; firingIndex++) {
                    if (list[firingIndex].apply(context, args) === false && flags.stopOnFalse) {
                        memory = true; // Mark as halted
                        break;
                    }
                }
                firing = false;
                if (list) {
                    if (!flags.once) {
                        if (stack && stack.length) {
                            memory = stack.shift();
                            self.fireWith(memory[0], memory[1]);
                        }
                    } else if (memory === true) {
                        self.disable();
                    } else {
                        list = [];
                    }
                }
            },
            // Actual Callbacks object
            self = {
                // Add a callback or a collection of callbacks to the list
                add: function() {
                    if (list) {
                        var length = list.length;
                        add(arguments);
                        // Do we need to add the callbacks to the
                        // current firing batch?
                        if (firing) {
                            firingLength = list.length;
                            // With memory, if we're not firing then
                            // we should call right away, unless previous
                            // firing was halted (stopOnFalse)
                        } else if (memory && memory !== true) {
                            firingStart = length;
                            fire(memory[0], memory[1]);
                        }
                    }
                    return this;
                },
                // Remove a callback from the list
                remove: function() {
                    if (list) {
                        var args = arguments,
                            argIndex = 0,
                            argLength = args.length;
                        for (; argIndex < argLength; argIndex++) {
                            for (var i = 0; i < list.length; i++) {
                                if (args[argIndex] === list[i]) {
                                    // Handle firingIndex and firingLength
                                    if (firing) {
                                        if (i <= firingLength) {
                                            firingLength--;
                                            if (i <= firingIndex) {
                                                firingIndex--;
                                            }
                                        }
                                    }
                                    // Remove the element
                                    list.splice(i--, 1);
                                    // If we have some unicity property then
                                    // we only need to do this once
                                    if (flags.unique) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    return this;
                },
                // Control if a given callback is in the list
                has: function(fn) {
                    if (list) {
                        var i = 0,
                            length = list.length;
                        for (; i < length; i++) {
                            if (fn === list[i]) {
                                return true;
                            }
                        }
                    }
                    return false;
                },
                // Remove all callbacks from the list
                empty: function() {
                    list = [];
                    return this;
                },
                // Have the list do nothing anymore
                disable: function() {
                    list = stack = memory = undefined;
                    return this;
                },
                // Is it disabled?
                disabled: function() {
                    return !list;
                },
                // Lock the list in its current state
                lock: function() {
                    stack = undefined;
                    if (!memory || memory === true) {
                        self.disable();
                    }
                    return this;
                },
                // Is it locked?
                locked: function() {
                    return !stack;
                },
                // Call all callbacks with the given context and arguments
                fireWith: function(context, args) {
                    if (stack) {
                        if (firing) {
                            if (!flags.once) {
                                stack.push([context, args]);
                            }
                        } else if (!(flags.once && memory)) {
                            fire(context, args);
                        }
                    }
                    return this;
                },
                // Call all the callbacks with the given arguments
                fire: function() {
                    self.fireWith(this, arguments);
                    return this;
                },
                // To know if the callbacks have already been called at least once
                fired: function() {
                    return !!fired;
                }
            };

        return self;
    };




    var // Static reference to slice
    sliceDeferred = [].slice;

    jQuery.extend({

        Deferred: function(func) {
            var doneList = jQuery.Callbacks("once memory"),
                failList = jQuery.Callbacks("once memory"),
                progressList = jQuery.Callbacks("memory"),
                state = "pending",
                lists = {
                    resolve: doneList,
                    reject: failList,
                    notify: progressList
                },
                promise = {
                    done: doneList.add,
                    fail: failList.add,
                    progress: progressList.add,

                    state: function() {
                        return state;
                    },

                    // Deprecated
                    isResolved: doneList.fired,
                    isRejected: failList.fired,

                    then: function(doneCallbacks, failCallbacks, progressCallbacks) {
                        deferred.done(doneCallbacks).fail(failCallbacks).progress(progressCallbacks);
                        return this;
                    },
                    always: function() {
                        deferred.done.apply(deferred, arguments).fail.apply(deferred, arguments);
                        return this;
                    },
                    pipe: function(fnDone, fnFail, fnProgress) {
                        return jQuery.Deferred(function(newDefer) {
                            jQuery.each({
                                done: [fnDone, "resolve"],
                                fail: [fnFail, "reject"],
                                progress: [fnProgress, "notify"]
                            }, function(handler, data) {
                                var fn = data[0],
                                    action = data[1],
                                    returned;
                                if (jQuery.isFunction(fn)) {
                                    deferred[handler](function() {
                                        returned = fn.apply(this, arguments);
                                        if (returned && jQuery.isFunction(returned.promise)) {
                                            returned.promise().then(newDefer.resolve, newDefer.reject, newDefer.notify);
                                        } else {
                                            newDefer[action + "With"](this === deferred ? newDefer : this, [returned]);
                                        }
                                    });
                                } else {
                                    deferred[handler](newDefer[action]);
                                }
                            });
                        }).promise();
                    },
                    // Get a promise for this deferred
                    // If obj is provided, the promise aspect is added to the object
                    promise: function(obj) {
                        if (obj == null) {
                            obj = promise;
                        } else {
                            for (var key in promise) {
                                obj[key] = promise[key];
                            }
                        }
                        return obj;
                    }
                },
                deferred = promise.promise({}),
                key;

            for (key in lists) {
                deferred[key] = lists[key].fire;
                deferred[key + "With"] = lists[key].fireWith;
            }

            // Handle state
            deferred.done(function() {
                state = "resolved";
            }, failList.disable, progressList.lock).fail(function() {
                state = "rejected";
            }, doneList.disable, progressList.lock);

            // Call given func if any
            if (func) {
                func.call(deferred, deferred);
            }

            // All done!
            return deferred;
        },

        // Deferred helper
        when: function(firstParam) {
            var args = sliceDeferred.call(arguments, 0),
                i = 0,
                length = args.length,
                pValues = new Array(length),
                count = length,
                pCount = length,
                deferred = length <= 1 && firstParam && jQuery.isFunction(firstParam.promise) ? firstParam : jQuery.Deferred(),
                promise = deferred.promise();

            function resolveFunc(i) {
                return function(value) {
                    args[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value;
                    if (!(--count)) {
                        deferred.resolveWith(deferred, args);
                    }
                };
            }

            function progressFunc(i) {
                return function(value) {
                    pValues[i] = arguments.length > 1 ? sliceDeferred.call(arguments, 0) : value;
                    deferred.notifyWith(promise, pValues);
                };
            }
            if (length > 1) {
                for (; i < length; i++) {
                    if (args[i] && args[i].promise && jQuery.isFunction(args[i].promise)) {
                        args[i].promise().then(resolveFunc(i), deferred.reject, progressFunc(i));
                    } else {
                        --count;
                    }
                }
                if (!count) {
                    deferred.resolveWith(deferred, args);
                }
            } else if (deferred !== firstParam) {
                deferred.resolveWith(deferred, length ? [firstParam] : []);
            }
            return promise;
        }
    });




    jQuery.support = (function() {

        var support, all, a, select, opt, input, fragment, tds, events, eventName, i, isSupported, div = document.createElement("div"),
            documentElement = document.documentElement;

        // Preliminary tests
        div.setAttribute("className", "t");
        div.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>";

        all = div.getElementsByTagName("*");
        a = div.getElementsByTagName("a")[0];

        // Can't get basic test support
        if (!all || !all.length || !a) {
            return {};
        }

        // First batch of supports tests
        select = document.createElement("select");
        opt = select.appendChild(document.createElement("option"));
        input = div.getElementsByTagName("input")[0];

        support = {
            // IE strips leading whitespace when .innerHTML is used
            leadingWhitespace: (div.firstChild.nodeType === 3),

            // Make sure that tbody elements aren't automatically inserted
            // IE will insert them into empty tables
            tbody: !div.getElementsByTagName("tbody").length,

            // Make sure that link elements get serialized correctly by innerHTML
            // This requires a wrapper element in IE
            htmlSerialize: !! div.getElementsByTagName("link").length,

            // Get the style information from getAttribute
            // (IE uses .cssText instead)
            style: /top/.test(a.getAttribute("style")),

            // Make sure that URLs aren't manipulated
            // (IE normalizes it by default)
            hrefNormalized: (a.getAttribute("href") === "/a"),

            // Make sure that element opacity exists
            // (IE uses filter instead)
            // Use a regex to work around a WebKit issue. See #5145
            opacity: /^0.55/.test(a.style.opacity),

            // Verify style float existence
            // (IE uses styleFloat instead of cssFloat)
            cssFloat: !! a.style.cssFloat,

            // Make sure that if no value is specified for a checkbox
            // that it defaults to "on".
            // (WebKit defaults to "" instead)
            checkOn: (input.value === "on"),

            // Make sure that a selected-by-default option has a working selected property.
            // (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
            optSelected: opt.selected,

            // Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
            getSetAttribute: div.className !== "t",

            // Tests for enctype support on a form(#6743)
            enctype: !! document.createElement("form").enctype,

            // Makes sure cloning an html5 element does not cause problems
            // Where outerHTML is undefined, this still works
            html5Clone: document.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>",

            // Will be defined later
            submitBubbles: true,
            changeBubbles: true,
            focusinBubbles: false,
            deleteExpando: true,
            noCloneEvent: true,
            inlineBlockNeedsLayout: false,
            shrinkWrapBlocks: false,
            reliableMarginRight: true,
            pixelMargin: true
        };

        // jQuery.boxModel DEPRECATED in 1.3, use jQuery.support.boxModel instead
        jQuery.boxModel = support.boxModel = (document.compatMode === "CSS1Compat");

        // Make sure checked status is properly cloned
        input.checked = true;
        support.noCloneChecked = input.cloneNode(true).checked;

        // Make sure that the options inside disabled selects aren't marked as disabled
        // (WebKit marks them as disabled)
        select.disabled = true;
        support.optDisabled = !opt.disabled;

        // Test to see if it's possible to delete an expando from an element
        // Fails in Internet Explorer
        try {
            delete div.test;
        } catch (e) {
            support.deleteExpando = false;
        }

        if (!div.addEventListener && div.attachEvent && div.fireEvent) {
            div.attachEvent("onclick", function() {
                // Cloning a node shouldn't copy over any
                // bound event handlers (IE does this)
                support.noCloneEvent = false;
            });
            div.cloneNode(true).fireEvent("onclick");
        }

        // Check if a radio maintains its value
        // after being appended to the DOM
        input = document.createElement("input");
        input.value = "t";
        input.setAttribute("type", "radio");
        support.radioValue = input.value === "t";

        input.setAttribute("checked", "checked");

        // #11217 - WebKit loses check when the name is after the checked attribute
        input.setAttribute("name", "t");

        div.appendChild(input);
        fragment = document.createDocumentFragment();
        fragment.appendChild(div.lastChild);

        // WebKit doesn't clone checked state correctly in fragments
        support.checkClone = fragment.cloneNode(true).cloneNode(true).lastChild.checked;

        // Check if a disconnected checkbox will retain its checked
        // value of true after appended to the DOM (IE6/7)
        support.appendChecked = input.checked;

        fragment.removeChild(input);
        fragment.appendChild(div);

        // Technique from Juriy Zaytsev
        // http://perfectionkills.com/detecting-event-support-without-browser-sniffing/
        // We only care about the case where non-standard event systems
        // are used, namely in IE. Short-circuiting here helps us to
        // avoid an eval call (in setAttribute) which can cause CSP
        // to go haywire. See: https://developer.mozilla.org/en/Security/CSP
        if (div.attachEvent) {
            for (i in {
                submit: 1,
                change: 1,
                focusin: 1
            }) {
                eventName = "on" + i;
                isSupported = (eventName in div);
                if (!isSupported) {
                    div.setAttribute(eventName, "return;");
                    isSupported = (typeof div[eventName] === "function");
                }
                support[i + "Bubbles"] = isSupported;
            }
        }

        fragment.removeChild(div);

        // Null elements to avoid leaks in IE
        fragment = select = opt = div = input = null;

        // Run tests that need a body at doc ready
        jQuery(function() {
            var container, outer, inner, table, td, offsetSupport, marginDiv, conMarginTop, style, html, positionTopLeftWidthHeight, paddingMarginBorderVisibility, paddingMarginBorder, body = document.getElementsByTagName("body")[0];

            if (!body) {
                // Return for frameset docs that don't have a body
                return;
            }

            conMarginTop = 1;
            paddingMarginBorder = "padding:0;margin:0;border:";
            positionTopLeftWidthHeight = "position:absolute;top:0;left:0;width:1px;height:1px;";
            paddingMarginBorderVisibility = paddingMarginBorder + "0;visibility:hidden;";
            style = "style='" + positionTopLeftWidthHeight + paddingMarginBorder + "5px solid #000;";
            html = "<div " + style + "display:block;'><div style='" + paddingMarginBorder + "0;display:block;overflow:hidden;'></div></div>" + "<table " + style + "' cellpadding='0' cellspacing='0'>" + "<tr><td></td></tr></table>";

            container = document.createElement("div");
            container.style.cssText = paddingMarginBorderVisibility + "width:0;height:0;position:static;top:0;margin-top:" + conMarginTop + "px";
            body.insertBefore(container, body.firstChild);

            // Construct the test element
            div = document.createElement("div");
            container.appendChild(div);

            // Check if table cells still have offsetWidth/Height when they are set
            // to display:none and there are still other visible table cells in a
            // table row; if so, offsetWidth/Height are not reliable for use when
            // determining if an element has been hidden directly using
            // display:none (it is still safe to use offsets if a parent element is
            // hidden; don safety goggles and see bug #4512 for more information).
            // (only IE 8 fails this test)
            div.innerHTML = "<table><tr><td style='" + paddingMarginBorder + "0;display:none'></td><td>t</td></tr></table>";
            tds = div.getElementsByTagName("td");
            isSupported = (tds[0].offsetHeight === 0);

            tds[0].style.display = "";
            tds[1].style.display = "none";

            // Check if empty table cells still have offsetWidth/Height
            // (IE <= 8 fail this test)
            support.reliableHiddenOffsets = isSupported && (tds[0].offsetHeight === 0);

            // Check if div with explicit width and no margin-right incorrectly
            // gets computed margin-right based on width of container. For more
            // info see bug #3333
            // Fails in WebKit before Feb 2011 nightlies
            // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
            if (window.getComputedStyle) {
                div.innerHTML = "";
                marginDiv = document.createElement("div");
                marginDiv.style.width = "0";
                marginDiv.style.marginRight = "0";
                div.style.width = "2px";
                div.appendChild(marginDiv);
                support.reliableMarginRight = (parseInt((window.getComputedStyle(marginDiv, null) || {
                    marginRight: 0
                }).marginRight, 10) || 0) === 0;
            }

            if (typeof div.style.zoom !== "undefined") {
                // Check if natively block-level elements act like inline-block
                // elements when setting their display to 'inline' and giving
                // them layout
                // (IE < 8 does this)
                div.innerHTML = "";
                div.style.width = div.style.padding = "1px";
                div.style.border = 0;
                div.style.overflow = "hidden";
                div.style.display = "inline";
                div.style.zoom = 1;
                support.inlineBlockNeedsLayout = (div.offsetWidth === 3);

                // Check if elements with layout shrink-wrap their children
                // (IE 6 does this)
                div.style.display = "block";
                div.style.overflow = "visible";
                div.innerHTML = "<div style='width:5px;'></div>";
                support.shrinkWrapBlocks = (div.offsetWidth !== 3);
            }

            div.style.cssText = positionTopLeftWidthHeight + paddingMarginBorderVisibility;
            div.innerHTML = html;

            outer = div.firstChild;
            inner = outer.firstChild;
            td = outer.nextSibling.firstChild.firstChild;

            offsetSupport = {
                doesNotAddBorder: (inner.offsetTop !== 5),
                doesAddBorderForTableAndCells: (td.offsetTop === 5)
            };

            inner.style.position = "fixed";
            inner.style.top = "20px";

            // safari subtracts parent border width here which is 5px
            offsetSupport.fixedPosition = (inner.offsetTop === 20 || inner.offsetTop === 15);
            inner.style.position = inner.style.top = "";

            outer.style.overflow = "hidden";
            outer.style.position = "relative";

            offsetSupport.subtractsBorderForOverflowNotVisible = (inner.offsetTop === -5);
            offsetSupport.doesNotIncludeMarginInBodyOffset = (body.offsetTop !== conMarginTop);

            if (window.getComputedStyle) {
                div.style.marginTop = "1%";
                support.pixelMargin = (window.getComputedStyle(div, null) || {
                    marginTop: 0
                }).marginTop !== "1%";
            }

            if (typeof container.style.zoom !== "undefined") {
                container.style.zoom = 1;
            }

            body.removeChild(container);
            marginDiv = div = container = null;

            jQuery.extend(support, offsetSupport);
        });

        return support;
    })();




    var rbrace = /^(?:\{.*\}|\[.*\])$/,
        rmultiDash = /([A-Z])/g;

    jQuery.extend({
        cache: {},

        // Please use with caution
        uuid: 0,

        // Unique for each copy of jQuery on the page
        // Non-digits removed to match rinlinejQuery
        expando: "jQuery" + (jQuery.fn.jquery + Math.random()).replace(/\D/g, ""),

        // The following elements throw uncatchable exceptions if you
        // attempt to add expando properties to them.
        noData: {
            "embed": true,
            // Ban all objects except for Flash (which handle expandos)
            "object": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            "applet": true
        },

        hasData: function(elem) {
            elem = elem.nodeType ? jQuery.cache[elem[jQuery.expando]] : elem[jQuery.expando];
            return !!elem && !isEmptyDataObject(elem);
        },

        data: function(elem, name, data, pvt /* Internal Use Only */ ) {
            if (!jQuery.acceptData(elem)) {
                return;
            }

            var privateCache, thisCache, ret, internalKey = jQuery.expando,
                getByName = typeof name === "string",

                // We have to handle DOM nodes and JS objects differently because IE6-7
                // can't GC object references properly across the DOM-JS boundary
                isNode = elem.nodeType,

                // Only DOM nodes need the global jQuery cache; JS object data is
                // attached directly to the object so GC can occur automatically
                cache = isNode ? jQuery.cache : elem,

                // Only defining an ID for JS objects if its cache already exists allows
                // the code to shortcut on the same path as a DOM node with no cache
                id = isNode ? elem[internalKey] : elem[internalKey] && internalKey,
                isEvents = name === "events";

            // Avoid doing any more work than we need to when trying to get data on an
            // object that has no data at all
            if ((!id || !cache[id] || (!isEvents && !pvt && !cache[id].data)) && getByName && data === undefined) {
                return;
            }

            if (!id) {
                // Only DOM nodes need a new unique ID for each element since their data
                // ends up in the global cache
                if (isNode) {
                    elem[internalKey] = id = ++jQuery.uuid;
                } else {
                    id = internalKey;
                }
            }

            if (!cache[id]) {
                cache[id] = {};

                // Avoids exposing jQuery metadata on plain JS objects when the object
                // is serialized using JSON.stringify
                if (!isNode) {
                    cache[id].toJSON = jQuery.noop;
                }
            }

            // An object can be passed to jQuery.data instead of a key/value pair; this gets
            // shallow copied over onto the existing cache
            if (typeof name === "object" || typeof name === "function") {
                if (pvt) {
                    cache[id] = jQuery.extend(cache[id], name);
                } else {
                    cache[id].data = jQuery.extend(cache[id].data, name);
                }
            }

            privateCache = thisCache = cache[id];

            // jQuery data() is stored in a separate object inside the object's internal data
            // cache in order to avoid key collisions between internal data and user-defined
            // data.
            if (!pvt) {
                if (!thisCache.data) {
                    thisCache.data = {};
                }

                thisCache = thisCache.data;
            }

            if (data !== undefined) {
                thisCache[jQuery.camelCase(name)] = data;
            }

            // Users should not attempt to inspect the internal events object using jQuery.data,
            // it is undocumented and subject to change. But does anyone listen? No.
            if (isEvents && !thisCache[name]) {
                return privateCache.events;
            }

            // Check for both converted-to-camel and non-converted data property names
            // If a data property was specified
            if (getByName) {

                // First Try to find as-is property data
                ret = thisCache[name];

                // Test for null|undefined property data
                if (ret == null) {

                    // Try to find the camelCased property
                    ret = thisCache[jQuery.camelCase(name)];
                }
            } else {
                ret = thisCache;
            }

            return ret;
        },

        removeData: function(elem, name, pvt /* Internal Use Only */ ) {
            if (!jQuery.acceptData(elem)) {
                return;
            }

            var thisCache, i, l,

            // Reference to internal data cache key
            internalKey = jQuery.expando,

                isNode = elem.nodeType,

                // See jQuery.data for more information
                cache = isNode ? jQuery.cache : elem,

                // See jQuery.data for more information
                id = isNode ? elem[internalKey] : internalKey;

            // If there is already no cache entry for this object, there is no
            // purpose in continuing
            if (!cache[id]) {
                return;
            }

            if (name) {

                thisCache = pvt ? cache[id] : cache[id].data;

                if (thisCache) {

                    // Support array or space separated string names for data keys
                    if (!jQuery.isArray(name)) {

                        // try the string as a key before any manipulation
                        if (name in thisCache) {
                            name = [name];
                        } else {

                            // split the camel cased version by spaces unless a key with the spaces exists
                            name = jQuery.camelCase(name);
                            if (name in thisCache) {
                                name = [name];
                            } else {
                                name = name.split(" ");
                            }
                        }
                    }

                    for (i = 0, l = name.length; i < l; i++) {
                        delete thisCache[name[i]];
                    }

                    // If there is no data left in the cache, we want to continue
                    // and let the cache object itself get destroyed
                    if (!(pvt ? isEmptyDataObject : jQuery.isEmptyObject)(thisCache)) {
                        return;
                    }
                }
            }

            // See jQuery.data for more information
            if (!pvt) {
                delete cache[id].data;

                // Don't destroy the parent cache unless the internal data object
                // had been the only thing left in it
                if (!isEmptyDataObject(cache[id])) {
                    return;
                }
            }

            // Browsers that fail expando deletion also refuse to delete expandos on
            // the window, but it will allow it on all other JS objects; other browsers
            // don't care
            // Ensure that `cache` is not a window object #10080
            if (jQuery.support.deleteExpando || !cache.setInterval) {
                delete cache[id];
            } else {
                cache[id] = null;
            }

            // We destroyed the cache and need to eliminate the expando on the node to avoid
            // false lookups in the cache for entries that no longer exist
            if (isNode) {
                // IE does not allow us to delete expando properties from nodes,
                // nor does it have a removeAttribute function on Document nodes;
                // we must handle all of these cases
                if (jQuery.support.deleteExpando) {
                    delete elem[internalKey];
                } else if (elem.removeAttribute) {
                    elem.removeAttribute(internalKey);
                } else {
                    elem[internalKey] = null;
                }
            }
        },

        // For internal use only.
        _data: function(elem, name, data) {
            return jQuery.data(elem, name, data, true);
        },

        // A method for determining if a DOM node can handle the data expando
        acceptData: function(elem) {
            if (elem.nodeName) {
                var match = jQuery.noData[elem.nodeName.toLowerCase()];

                if (match) {
                    return !(match === true || elem.getAttribute("classid") !== match);
                }
            }

            return true;
        }
    });

    jQuery.fn.extend({
        data: function(key, value) {
            var parts, part, attr, name, l, elem = this[0],
                i = 0,
                data = null;

            // Gets all values
            if (key === undefined) {
                if (this.length) {
                    data = jQuery.data(elem);

                    if (elem.nodeType === 1 && !jQuery._data(elem, "parsedAttrs")) {
                        attr = elem.attributes;
                        for (l = attr.length; i < l; i++) {
                            name = attr[i].name;

                            if (name.indexOf("data-") === 0) {
                                name = jQuery.camelCase(name.substring(5));

                                dataAttr(elem, name, data[name]);
                            }
                        }
                        jQuery._data(elem, "parsedAttrs", true);
                    }
                }

                return data;
            }

            // Sets multiple values
            if (typeof key === "object") {
                return this.each(function() {
                    jQuery.data(this, key);
                });
            }

            parts = key.split(".", 2);
            parts[1] = parts[1] ? "." + parts[1] : "";
            part = parts[1] + "!";

            return jQuery.access(this, function(value) {

                if (value === undefined) {
                    data = this.triggerHandler("getData" + part, [parts[0]]);

                    // Try to fetch any internally stored data first
                    if (data === undefined && elem) {
                        data = jQuery.data(elem, key);
                        data = dataAttr(elem, key, data);
                    }

                    return data === undefined && parts[1] ? this.data(parts[0]) : data;
                }

                parts[1] = value;
                this.each(function() {
                    var self = jQuery(this);

                    self.triggerHandler("setData" + part, parts);
                    jQuery.data(this, key, value);
                    self.triggerHandler("changeData" + part, parts);
                });
            }, null, value, arguments.length > 1, null, false);
        },

        removeData: function(key) {
            return this.each(function() {
                jQuery.removeData(this, key);
            });
        }
    });

    function dataAttr(elem, key, data) {
        // If nothing was found internally, try to fetch any
        // data from the HTML5 data-* attribute
        if (data === undefined && elem.nodeType === 1) {

            var name = "data-" + key.replace(rmultiDash, "-$1").toLowerCase();

            data = elem.getAttribute(name);

            if (typeof data === "string") {
                try {
                    data = data === "true" ? true : data === "false" ? false : data === "null" ? null : jQuery.isNumeric(data) ? +data : rbrace.test(data) ? jQuery.parseJSON(data) : data;
                } catch (e) {}

                // Make sure we set the data so it isn't changed later
                jQuery.data(elem, key, data);

            } else {
                data = undefined;
            }
        }

        return data;
    }

    // checks a cache object for emptiness


    function isEmptyDataObject(obj) {
        for (var name in obj) {

            // if the public data object is empty, the private is still empty
            if (name === "data" && jQuery.isEmptyObject(obj[name])) {
                continue;
            }
            if (name !== "toJSON") {
                return false;
            }
        }

        return true;
    }




    function handleQueueMarkDefer(elem, type, src) {
        var deferDataKey = type + "defer",
            queueDataKey = type + "queue",
            markDataKey = type + "mark",
            defer = jQuery._data(elem, deferDataKey);
        if (defer && (src === "queue" || !jQuery._data(elem, queueDataKey)) && (src === "mark" || !jQuery._data(elem, markDataKey))) {
            // Give room for hard-coded callbacks to fire first
            // and eventually mark/queue something else on the element
            setTimeout(function() {
                if (!jQuery._data(elem, queueDataKey) && !jQuery._data(elem, markDataKey)) {
                    jQuery.removeData(elem, deferDataKey, true);
                    defer.fire();
                }
            }, 0);
        }
    }

    jQuery.extend({

        _mark: function(elem, type) {
            if (elem) {
                type = (type || "fx") + "mark";
                jQuery._data(elem, type, (jQuery._data(elem, type) || 0) + 1);
            }
        },

        _unmark: function(force, elem, type) {
            if (force !== true) {
                type = elem;
                elem = force;
                force = false;
            }
            if (elem) {
                type = type || "fx";
                var key = type + "mark",
                    count = force ? 0 : ((jQuery._data(elem, key) || 1) - 1);
                if (count) {
                    jQuery._data(elem, key, count);
                } else {
                    jQuery.removeData(elem, key, true);
                    handleQueueMarkDefer(elem, type, "mark");
                }
            }
        },

        queue: function(elem, type, data) {
            var q;
            if (elem) {
                type = (type || "fx") + "queue";
                q = jQuery._data(elem, type);

                // Speed up dequeue by getting out quickly if this is just a lookup
                if (data) {
                    if (!q || jQuery.isArray(data)) {
                        q = jQuery._data(elem, type, jQuery.makeArray(data));
                    } else {
                        q.push(data);
                    }
                }
                return q || [];
            }
        },

        dequeue: function(elem, type) {
            type = type || "fx";

            var queue = jQuery.queue(elem, type),
                fn = queue.shift(),
                hooks = {};

            // If the fx queue is dequeued, always remove the progress sentinel
            if (fn === "inprogress") {
                fn = queue.shift();
            }

            if (fn) {
                // Add a progress sentinel to prevent the fx queue from being
                // automatically dequeued
                if (type === "fx") {
                    queue.unshift("inprogress");
                }

                jQuery._data(elem, type + ".run", hooks);
                fn.call(elem, function() {
                    jQuery.dequeue(elem, type);
                }, hooks);
            }

            if (!queue.length) {
                jQuery.removeData(elem, type + "queue " + type + ".run", true);
                handleQueueMarkDefer(elem, type, "queue");
            }
        }
    });

    jQuery.fn.extend({
        queue: function(type, data) {
            var setter = 2;

            if (typeof type !== "string") {
                data = type;
                type = "fx";
                setter--;
            }

            if (arguments.length < setter) {
                return jQuery.queue(this[0], type);
            }

            return data === undefined ? this : this.each(function() {
                var queue = jQuery.queue(this, type, data);

                if (type === "fx" && queue[0] !== "inprogress") {
                    jQuery.dequeue(this, type);
                }
            });
        },
        dequeue: function(type) {
            return this.each(function() {
                jQuery.dequeue(this, type);
            });
        },
        // Based off of the plugin by Clint Helfers, with permission.
        // http://blindsignals.com/index.php/2009/07/jquery-delay/
        delay: function(time, type) {
            time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
            type = type || "fx";

            return this.queue(type, function(next, hooks) {
                var timeout = setTimeout(next, time);
                hooks.stop = function() {
                    clearTimeout(timeout);
                };
            });
        },
        clearQueue: function(type) {
            return this.queue(type || "fx", []);
        },
        // Get a promise resolved when queues of a certain type
        // are emptied (fx is the type by default)
        promise: function(type, object) {
            if (typeof type !== "string") {
                object = type;
                type = undefined;
            }
            type = type || "fx";
            var defer = jQuery.Deferred(),
                elements = this,
                i = elements.length,
                count = 1,
                deferDataKey = type + "defer",
                queueDataKey = type + "queue",
                markDataKey = type + "mark",
                tmp;

            function resolve() {
                if (!(--count)) {
                    defer.resolveWith(elements, [elements]);
                }
            }
            while (i--) {
                if ((tmp = jQuery.data(elements[i], deferDataKey, undefined, true) || (jQuery.data(elements[i], queueDataKey, undefined, true) || jQuery.data(elements[i], markDataKey, undefined, true)) && jQuery.data(elements[i], deferDataKey, jQuery.Callbacks("once memory"), true))) {
                    count++;
                    tmp.add(resolve);
                }
            }
            resolve();
            return defer.promise(object);
        }
    });




    var rclass = /[\n\t\r]/g,
        rspace = /\s+/,
        rreturn = /\r/g,
        rtype = /^(?:button|input)$/i,
        rfocusable = /^(?:button|input|object|select|textarea)$/i,
        rclickable = /^a(?:rea)?$/i,
        rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        getSetAttribute = jQuery.support.getSetAttribute,
        nodeHook, boolHook, fixSpecified;

    jQuery.fn.extend({
        attr: function(name, value) {
            return jQuery.access(this, jQuery.attr, name, value, arguments.length > 1);
        },

        removeAttr: function(name) {
            return this.each(function() {
                jQuery.removeAttr(this, name);
            });
        },

        prop: function(name, value) {
            return jQuery.access(this, jQuery.prop, name, value, arguments.length > 1);
        },

        removeProp: function(name) {
            name = jQuery.propFix[name] || name;
            return this.each(function() {
                // try/catch handles cases where IE balks (such as removing a property on window)
                try {
                    this[name] = undefined;
                    delete this[name];
                } catch (e) {}
            });
        },

        addClass: function(value) {
            var classNames, i, l, elem, setClass, c, cl;

            if (jQuery.isFunction(value)) {
                return this.each(function(j) {
                    jQuery(this).addClass(value.call(this, j, this.className));
                });
            }

            if (value && typeof value === "string") {
                classNames = value.split(rspace);

                for (i = 0, l = this.length; i < l; i++) {
                    elem = this[i];

                    if (elem.nodeType === 1) {
                        if (!elem.className && classNames.length === 1) {
                            elem.className = value;

                        } else {
                            setClass = " " + elem.className + " ";

                            for (c = 0, cl = classNames.length; c < cl; c++) {
                                if (!~setClass.indexOf(" " + classNames[c] + " ")) {
                                    setClass += classNames[c] + " ";
                                }
                            }
                            elem.className = jQuery.trim(setClass);
                        }
                    }
                }
            }

            return this;
        },

        removeClass: function(value) {
            var classNames, i, l, elem, className, c, cl;

            if (jQuery.isFunction(value)) {
                return this.each(function(j) {
                    jQuery(this).removeClass(value.call(this, j, this.className));
                });
            }

            if ((value && typeof value === "string") || value === undefined) {
                classNames = (value || "").split(rspace);

                for (i = 0, l = this.length; i < l; i++) {
                    elem = this[i];

                    if (elem.nodeType === 1 && elem.className) {
                        if (value) {
                            className = (" " + elem.className + " ").replace(rclass, " ");
                            for (c = 0, cl = classNames.length; c < cl; c++) {
                                className = className.replace(" " + classNames[c] + " ", " ");
                            }
                            elem.className = jQuery.trim(className);

                        } else {
                            elem.className = "";
                        }
                    }
                }
            }

            return this;
        },

        toggleClass: function(value, stateVal) {
            var type = typeof value,
                isBool = typeof stateVal === "boolean";

            if (jQuery.isFunction(value)) {
                return this.each(function(i) {
                    jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal);
                });
            }

            return this.each(function() {
                if (type === "string") {
                    // toggle individual class names
                    var className, i = 0,
                        self = jQuery(this),
                        state = stateVal,
                        classNames = value.split(rspace);

                    while ((className = classNames[i++])) {
                        // check each className given, space seperated list
                        state = isBool ? state : !self.hasClass(className);
                        self[state ? "addClass" : "removeClass"](className);
                    }

                } else if (type === "undefined" || type === "boolean") {
                    if (this.className) {
                        // store className if set
                        jQuery._data(this, "__className__", this.className);
                    }

                    // toggle whole className
                    this.className = this.className || value === false ? "" : jQuery._data(this, "__className__") || "";
                }
            });
        },

        hasClass: function(selector) {
            var className = " " + selector + " ",
                i = 0,
                l = this.length;
            for (; i < l; i++) {
                if (this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf(className) > -1) {
                    return true;
                }
            }

            return false;
        },

        val: function(value) {
            var hooks, ret, isFunction, elem = this[0];

            if (!arguments.length) {
                if (elem) {
                    hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];

                    if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) {
                        return ret;
                    }

                    ret = elem.value;

                    return typeof ret === "string" ?
                    // handle most common string cases
                    ret.replace(rreturn, "") :
                    // handle cases where value is null/undef or number
                    ret == null ? "" : ret;
                }

                return;
            }

            isFunction = jQuery.isFunction(value);

            return this.each(function(i) {
                var self = jQuery(this),
                    val;

                if (this.nodeType !== 1) {
                    return;
                }

                if (isFunction) {
                    val = value.call(this, i, self.val());
                } else {
                    val = value;
                }

                // Treat null/undefined as ""; convert numbers to string
                if (val == null) {
                    val = "";
                } else if (typeof val === "number") {
                    val += "";
                } else if (jQuery.isArray(val)) {
                    val = jQuery.map(val, function(value) {
                        return value == null ? "" : value + "";
                    });
                }

                hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];

                // If set returns undefined, fall back to normal setting
                if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) {
                    this.value = val;
                }
            });
        }
    });

    jQuery.extend({
        valHooks: {
            option: {
                get: function(elem) {
                    // attributes.value is undefined in Blackberry 4.7 but
                    // uses .value. See #6932
                    var val = elem.attributes.value;
                    return !val || val.specified ? elem.value : elem.text;
                }
            },
            select: {
                get: function(elem) {
                    var value, i, max, option, index = elem.selectedIndex,
                        values = [],
                        options = elem.options,
                        one = elem.type === "select-one";

                    // Nothing was selected
                    if (index < 0) {
                        return null;
                    }

                    // Loop through all the selected options
                    i = one ? index : 0;
                    max = one ? index + 1 : options.length;
                    for (; i < max; i++) {
                        option = options[i];

                        // Don't return options that are disabled or in a disabled optgroup
                        if (option.selected && (jQuery.support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null) && (!option.parentNode.disabled || !jQuery.nodeName(option.parentNode, "optgroup"))) {

                            // Get the specific value for the option
                            value = jQuery(option).val();

                            // We don't need an array for one selects
                            if (one) {
                                return value;
                            }

                            // Multi-Selects return an array
                            values.push(value);
                        }
                    }

                    // Fixes Bug #2551 -- select.val() broken in IE after form.reset()
                    if (one && !values.length && options.length) {
                        return jQuery(options[index]).val();
                    }

                    return values;
                },

                set: function(elem, value) {
                    var values = jQuery.makeArray(value);

                    jQuery(elem).find("option").each(function() {
                        this.selected = jQuery.inArray(jQuery(this).val(), values) >= 0;
                    });

                    if (!values.length) {
                        elem.selectedIndex = -1;
                    }
                    return values;
                }
            }
        },

        attrFn: {
            val: true,
            css: true,
            html: true,
            text: true,
            data: true,
            width: true,
            height: true,
            offset: true
        },

        attr: function(elem, name, value, pass) {
            var ret, hooks, notxml, nType = elem.nodeType;

            // don't get/set attributes on text, comment and attribute nodes
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                return;
            }

            if (pass && name in jQuery.attrFn) {
                return jQuery(elem)[name](value);
            }

            // Fallback to prop when attributes are not supported
            if (typeof elem.getAttribute === "undefined") {
                return jQuery.prop(elem, name, value);
            }

            notxml = nType !== 1 || !jQuery.isXMLDoc(elem);

            // All attributes are lowercase
            // Grab necessary hook if one is defined
            if (notxml) {
                name = name.toLowerCase();
                hooks = jQuery.attrHooks[name] || (rboolean.test(name) ? boolHook : nodeHook);
            }

            if (value !== undefined) {

                if (value === null) {
                    jQuery.removeAttr(elem, name);
                    return;

                } else if (hooks && "set" in hooks && notxml && (ret = hooks.set(elem, value, name)) !== undefined) {
                    return ret;

                } else {
                    elem.setAttribute(name, "" + value);
                    return value;
                }

            } else if (hooks && "get" in hooks && notxml && (ret = hooks.get(elem, name)) !== null) {
                return ret;

            } else {

                ret = elem.getAttribute(name);

                // Non-existent attributes return null, we normalize to undefined
                return ret === null ? undefined : ret;
            }
        },

        removeAttr: function(elem, value) {
            var propName, attrNames, name, l, isBool, i = 0;

            if (value && elem.nodeType === 1) {
                attrNames = value.toLowerCase().split(rspace);
                l = attrNames.length;

                for (; i < l; i++) {
                    name = attrNames[i];

                    if (name) {
                        propName = jQuery.propFix[name] || name;
                        isBool = rboolean.test(name);

                        // See #9699 for explanation of this approach (setting first, then removal)
                        // Do not do this for boolean attributes (see #10870)
                        if (!isBool) {
                            jQuery.attr(elem, name, "");
                        }
                        elem.removeAttribute(getSetAttribute ? name : propName);

                        // Set corresponding property to false for boolean attributes
                        if (isBool && propName in elem) {
                            elem[propName] = false;
                        }
                    }
                }
            }
        },

        attrHooks: {
            type: {
                set: function(elem, value) {
                    // We can't allow the type property to be changed (since it causes problems in IE)
                    if (rtype.test(elem.nodeName) && elem.parentNode) {
                        jQuery.error("type property can't be changed");
                    } else if (!jQuery.support.radioValue && value === "radio" && jQuery.nodeName(elem, "input")) {
                        // Setting the type on a radio button after the value resets the value in IE6-9
                        // Reset value to it's default in case type is set after value
                        // This is for element creation
                        var val = elem.value;
                        elem.setAttribute("type", value);
                        if (val) {
                            elem.value = val;
                        }
                        return value;
                    }
                }
            },
            // Use the value property for back compat
            // Use the nodeHook for button elements in IE6/7 (#1954)
            value: {
                get: function(elem, name) {
                    if (nodeHook && jQuery.nodeName(elem, "button")) {
                        return nodeHook.get(elem, name);
                    }
                    return name in elem ? elem.value : null;
                },
                set: function(elem, value, name) {
                    if (nodeHook && jQuery.nodeName(elem, "button")) {
                        return nodeHook.set(elem, value, name);
                    }
                    // Does not return so that setAttribute is also used
                    elem.value = value;
                }
            }
        },

        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },

        prop: function(elem, name, value) {
            var ret, hooks, notxml, nType = elem.nodeType;

            // don't get/set properties on text, comment and attribute nodes
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                return;
            }

            notxml = nType !== 1 || !jQuery.isXMLDoc(elem);

            if (notxml) {
                // Fix name and attach hooks
                name = jQuery.propFix[name] || name;
                hooks = jQuery.propHooks[name];
            }

            if (value !== undefined) {
                if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                    return ret;

                } else {
                    return (elem[name] = value);
                }

            } else {
                if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
                    return ret;

                } else {
                    return elem[name];
                }
            }
        },

        propHooks: {
            tabIndex: {
                get: function(elem) {
                    // elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
                    // http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
                    var attributeNode = elem.getAttributeNode("tabindex");

                    return attributeNode && attributeNode.specified ? parseInt(attributeNode.value, 10) : rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href ? 0 : undefined;
                }
            }
        }
    });

    // Add the tabIndex propHook to attrHooks for back-compat (different case is intentional)
    jQuery.attrHooks.tabindex = jQuery.propHooks.tabIndex;

    // Hook for boolean attributes
    boolHook = {
        get: function(elem, name) {
            // Align boolean attributes with corresponding properties
            // Fall back to attribute presence where some booleans are not supported
            var attrNode, property = jQuery.prop(elem, name);
            return property === true || typeof property !== "boolean" && (attrNode = elem.getAttributeNode(name)) && attrNode.nodeValue !== false ? name.toLowerCase() : undefined;
        },
        set: function(elem, value, name) {
            var propName;
            if (value === false) {
                // Remove boolean attributes when set to false
                jQuery.removeAttr(elem, name);
            } else {
                // value is true since we know at this point it's type boolean and not false
                // Set boolean attributes to the same name and set the DOM property
                propName = jQuery.propFix[name] || name;
                if (propName in elem) {
                    // Only set the IDL specifically if it already exists on the element
                    elem[propName] = true;
                }

                elem.setAttribute(name, name.toLowerCase());
            }
            return name;
        }
    };

    // IE6/7 do not support getting/setting some attributes with get/setAttribute
    if (!getSetAttribute) {

        fixSpecified = {
            name: true,
            id: true,
            coords: true
        };

        // Use this for any attribute in IE6/7
        // This fixes almost every IE6/7 issue
        nodeHook = jQuery.valHooks.button = {
            get: function(elem, name) {
                var ret;
                ret = elem.getAttributeNode(name);
                return ret && (fixSpecified[name] ? ret.nodeValue !== "" : ret.specified) ? ret.nodeValue : undefined;
            },
            set: function(elem, value, name) {
                // Set the existing or create a new attribute node
                var ret = elem.getAttributeNode(name);
                if (!ret) {
                    ret = document.createAttribute(name);
                    elem.setAttributeNode(ret);
                }
                return (ret.nodeValue = value + "");
            }
        };

        // Apply the nodeHook to tabindex
        jQuery.attrHooks.tabindex.set = nodeHook.set;

        // Set width and height to auto instead of 0 on empty string( Bug #8150 )
        // This is for removals
        jQuery.each(["width", "height"], function(i, name) {
            jQuery.attrHooks[name] = jQuery.extend(jQuery.attrHooks[name], {
                set: function(elem, value) {
                    if (value === "") {
                        elem.setAttribute(name, "auto");
                        return value;
                    }
                }
            });
        });

        // Set contenteditable to false on removals(#10429)
        // Setting to empty string throws an error as an invalid value
        jQuery.attrHooks.contenteditable = {
            get: nodeHook.get,
            set: function(elem, value, name) {
                if (value === "") {
                    value = "false";
                }
                nodeHook.set(elem, value, name);
            }
        };
    }


    // Some attributes require a special call on IE
    if (!jQuery.support.hrefNormalized) {
        jQuery.each(["href", "src", "width", "height"], function(i, name) {
            jQuery.attrHooks[name] = jQuery.extend(jQuery.attrHooks[name], {
                get: function(elem) {
                    var ret = elem.getAttribute(name, 2);
                    return ret === null ? undefined : ret;
                }
            });
        });
    }

    if (!jQuery.support.style) {
        jQuery.attrHooks.style = {
            get: function(elem) {
                // Return undefined in the case of empty string
                // Normalize to lowercase since IE uppercases css property names
                return elem.style.cssText.toLowerCase() || undefined;
            },
            set: function(elem, value) {
                return (elem.style.cssText = "" + value);
            }
        };
    }

    // Safari mis-reports the default selected property of an option
    // Accessing the parent's selectedIndex property fixes it
    if (!jQuery.support.optSelected) {
        jQuery.propHooks.selected = jQuery.extend(jQuery.propHooks.selected, {
            get: function(elem) {
                var parent = elem.parentNode;

                if (parent) {
                    parent.selectedIndex;

                    // Make sure that it also works with optgroups, see #5701
                    if (parent.parentNode) {
                        parent.parentNode.selectedIndex;
                    }
                }
                return null;
            }
        });
    }

    // IE6/7 call enctype encoding
    if (!jQuery.support.enctype) {
        jQuery.propFix.enctype = "encoding";
    }

    // Radios and checkboxes getter/setter
    if (!jQuery.support.checkOn) {
        jQuery.each(["radio", "checkbox"], function() {
            jQuery.valHooks[this] = {
                get: function(elem) {
                    // Handle the case where in Webkit "" is returned instead of "on" if a value isn't specified
                    return elem.getAttribute("value") === null ? "on" : elem.value;
                }
            };
        });
    }
    jQuery.each(["radio", "checkbox"], function() {
        jQuery.valHooks[this] = jQuery.extend(jQuery.valHooks[this], {
            set: function(elem, value) {
                if (jQuery.isArray(value)) {
                    return (elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0);
                }
            }
        });
    });




    var rformElems = /^(?:textarea|input|select)$/i,
        rtypenamespace = /^([^\.]*)?(?:\.(.+))?$/,
        rhoverHack = /(?:^|\s)hover(\.\S+)?\b/,
        rkeyEvent = /^key/,
        rmouseEvent = /^(?:mouse|contextmenu)|click/,
        rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
        rquickIs = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,
        quickParse = function(selector) {
            var quick = rquickIs.exec(selector);
            if (quick) {
                //   0  1    2   3
                // [ _, tag, id, class ]
                quick[1] = (quick[1] || "").toLowerCase();
                quick[3] = quick[3] && new RegExp("(?:^|\\s)" + quick[3] + "(?:\\s|$)");
            }
            return quick;
        },
        quickIs = function(elem, m) {
            var attrs = elem.attributes || {};
            return (
            (!m[1] || elem.nodeName.toLowerCase() === m[1]) && (!m[2] || (attrs.id || {}).value === m[2]) && (!m[3] || m[3].test((attrs["class"] || {}).value)));
        },
        hoverHack = function(events) {
            return jQuery.event.special.hover ? events : events.replace(rhoverHack, "mouseenter$1 mouseleave$1");
        };

    /*
     * Helper functions for managing events -- not part of the public interface.
     * Props to Dean Edwards' addEvent library for many of the ideas.
     */
    jQuery.event = {

        add: function(elem, types, handler, data, selector) {

            var elemData, eventHandle, events, t, tns, type, namespaces, handleObj, handleObjIn, quick, handlers, special;

            // Don't attach events to noData or text/comment nodes (allow plain objects tho)
            if (elem.nodeType === 3 || elem.nodeType === 8 || !types || !handler || !(elemData = jQuery._data(elem))) {
                return;
            }

            // Caller can pass in an object of custom data in lieu of the handler
            if (handler.handler) {
                handleObjIn = handler;
                handler = handleObjIn.handler;
                selector = handleObjIn.selector;
            }

            // Make sure that the handler has a unique ID, used to find/remove it later
            if (!handler.guid) {
                handler.guid = jQuery.guid++;
            }

            // Init the element's event structure and main handler, if this is the first
            events = elemData.events;
            if (!events) {
                elemData.events = events = {};
            }
            eventHandle = elemData.handle;
            if (!eventHandle) {
                elemData.handle = eventHandle = function(e) {
                    // Discard the second event of a jQuery.event.trigger() and
                    // when an event is called after a page has unloaded
                    return typeof jQuery !== "undefined" && (!e || jQuery.event.triggered !== e.type) ? jQuery.event.dispatch.apply(eventHandle.elem, arguments) : undefined;
                };
                // Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
                eventHandle.elem = elem;
            }

            // Handle multiple events separated by a space
            // jQuery(...).bind("mouseover mouseout", fn);
            types = jQuery.trim(hoverHack(types)).split(" ");
            for (t = 0; t < types.length; t++) {

                tns = rtypenamespace.exec(types[t]) || [];
                type = tns[1];
                namespaces = (tns[2] || "").split(".").sort();

                // If event changes its type, use the special event handlers for the changed type
                special = jQuery.event.special[type] || {};

                // If selector defined, determine special event api type, otherwise given type
                type = (selector ? special.delegateType : special.bindType) || type;

                // Update special based on newly reset type
                special = jQuery.event.special[type] || {};

                // handleObj is passed to all event handlers
                handleObj = jQuery.extend({
                    type: type,
                    origType: tns[1],
                    data: data,
                    handler: handler,
                    guid: handler.guid,
                    selector: selector,
                    quick: selector && quickParse(selector),
                    namespace: namespaces.join(".")
                }, handleObjIn);

                // Init the event handler queue if we're the first
                handlers = events[type];
                if (!handlers) {
                    handlers = events[type] = [];
                    handlers.delegateCount = 0;

                    // Only use addEventListener/attachEvent if the special events handler returns false
                    if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
                        // Bind the global event handler to the element
                        if (elem.addEventListener) {
                            elem.addEventListener(type, eventHandle, false);

                        } else if (elem.attachEvent) {
                            elem.attachEvent("on" + type, eventHandle);
                        }
                    }
                }

                if (special.add) {
                    special.add.call(elem, handleObj);

                    if (!handleObj.handler.guid) {
                        handleObj.handler.guid = handler.guid;
                    }
                }

                // Add to the element's handler list, delegates in front
                if (selector) {
                    handlers.splice(handlers.delegateCount++, 0, handleObj);
                } else {
                    handlers.push(handleObj);
                }

                // Keep track of which events have ever been used, for event optimization
                jQuery.event.global[type] = true;
            }

            // Nullify elem to prevent memory leaks in IE
            elem = null;
        },

        global: {},

        // Detach an event or set of events from an element
        remove: function(elem, types, handler, selector, mappedTypes) {

            var elemData = jQuery.hasData(elem) && jQuery._data(elem),
                t, tns, type, origType, namespaces, origCount, j, events, special, handle, eventType, handleObj;

            if (!elemData || !(events = elemData.events)) {
                return;
            }

            // Once for each type.namespace in types; type may be omitted
            types = jQuery.trim(hoverHack(types || "")).split(" ");
            for (t = 0; t < types.length; t++) {
                tns = rtypenamespace.exec(types[t]) || [];
                type = origType = tns[1];
                namespaces = tns[2];

                // Unbind all events (on this namespace, if provided) for the element
                if (!type) {
                    for (type in events) {
                        jQuery.event.remove(elem, type + types[t], handler, selector, true);
                    }
                    continue;
                }

                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                eventType = events[type] || [];
                origCount = eventType.length;
                namespaces = namespaces ? new RegExp("(^|\\.)" + namespaces.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null;

                // Remove matching events
                for (j = 0; j < eventType.length; j++) {
                    handleObj = eventType[j];

                    if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!namespaces || namespaces.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === "**" && handleObj.selector)) {
                        eventType.splice(j--, 1);

                        if (handleObj.selector) {
                            eventType.delegateCount--;
                        }
                        if (special.remove) {
                            special.remove.call(elem, handleObj);
                        }
                    }
                }

                // Remove generic event handler if we removed something and no more handlers exist
                // (avoids potential for endless recursion during removal of special event handlers)
                if (eventType.length === 0 && origCount !== eventType.length) {
                    if (!special.teardown || special.teardown.call(elem, namespaces) === false) {
                        jQuery.removeEvent(elem, type, elemData.handle);
                    }

                    delete events[type];
                }
            }

            // Remove the expando if it's no longer used
            if (jQuery.isEmptyObject(events)) {
                handle = elemData.handle;
                if (handle) {
                    handle.elem = null;
                }

                // removeData also checks for emptiness and clears the expando if empty
                // so use it instead of delete
                jQuery.removeData(elem, ["events", "handle"], true);
            }
        },

        // Events that are safe to short-circuit if no handlers are attached.
        // Native DOM events should not be added, they may have inline handlers.
        customEvent: {
            "getData": true,
            "setData": true,
            "changeData": true
        },

        trigger: function(event, data, elem, onlyHandlers) {
            // Don't do events on text and comment nodes
            if (elem && (elem.nodeType === 3 || elem.nodeType === 8)) {
                return;
            }

            // Event object or event type
            var type = event.type || event,
                namespaces = [],
                cache, exclusive, i, cur, old, ontype, special, handle, eventPath, bubbleType;

            // focus/blur morphs to focusin/out; ensure we're not firing them right now
            if (rfocusMorph.test(type + jQuery.event.triggered)) {
                return;
            }

            if (type.indexOf("!") >= 0) {
                // Exclusive events trigger only for the exact event (no namespaces)
                type = type.slice(0, -1);
                exclusive = true;
            }

            if (type.indexOf(".") >= 0) {
                // Namespaced trigger; create a regexp to match event type in handle()
                namespaces = type.split(".");
                type = namespaces.shift();
                namespaces.sort();
            }

            if ((!elem || jQuery.event.customEvent[type]) && !jQuery.event.global[type]) {
                // No jQuery handlers for this event type, and it can't have inline handlers
                return;
            }

            // Caller can pass in an Event, Object, or just an event type string
            event = typeof event === "object" ?
            // jQuery.Event object
            event[jQuery.expando] ? event :
            // Object literal
            new jQuery.Event(type, event) :
            // Just the event type (string)
            new jQuery.Event(type);

            event.type = type;
            event.isTrigger = true;
            event.exclusive = exclusive;
            event.namespace = namespaces.join(".");
            event.namespace_re = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
            ontype = type.indexOf(":") < 0 ? "on" + type : "";

            // Handle a global trigger
            if (!elem) {

                // TODO: Stop taunting the data cache; remove global events and always attach to document
                cache = jQuery.cache;
                for (i in cache) {
                    if (cache[i].events && cache[i].events[type]) {
                        jQuery.event.trigger(event, data, cache[i].handle.elem, true);
                    }
                }
                return;
            }

            // Clean up the event in case it is being reused
            event.result = undefined;
            if (!event.target) {
                event.target = elem;
            }

            // Clone any incoming data and prepend the event, creating the handler arg list
            data = data != null ? jQuery.makeArray(data) : [];
            data.unshift(event);

            // Allow special events to draw outside the lines
            special = jQuery.event.special[type] || {};
            if (special.trigger && special.trigger.apply(elem, data) === false) {
                return;
            }

            // Determine event propagation path in advance, per W3C events spec (#9951)
            // Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
            eventPath = [
                [elem, special.bindType || type]
            ];
            if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {

                bubbleType = special.delegateType || type;
                cur = rfocusMorph.test(bubbleType + type) ? elem : elem.parentNode;
                old = null;
                for (; cur; cur = cur.parentNode) {
                    eventPath.push([cur, bubbleType]);
                    old = cur;
                }

                // Only add window if we got to document (e.g., not plain obj or detached DOM)
                if (old && old === elem.ownerDocument) {
                    eventPath.push([old.defaultView || old.parentWindow || window, bubbleType]);
                }
            }

            // Fire handlers on the event path
            for (i = 0; i < eventPath.length && !event.isPropagationStopped(); i++) {

                cur = eventPath[i][0];
                event.type = eventPath[i][1];

                handle = (jQuery._data(cur, "events") || {})[event.type] && jQuery._data(cur, "handle");
                if (handle) {
                    handle.apply(cur, data);
                }
                // Note that this is a bare JS function and not a jQuery handler
                handle = ontype && cur[ontype];
                if (handle && jQuery.acceptData(cur) && handle.apply(cur, data) === false) {
                    event.preventDefault();
                }
            }
            event.type = type;

            // If nobody prevented the default action, do it now
            if (!onlyHandlers && !event.isDefaultPrevented()) {

                if ((!special._default || special._default.apply(elem.ownerDocument, data) === false) && !(type === "click" && jQuery.nodeName(elem, "a")) && jQuery.acceptData(elem)) {

                    // Call a native DOM method on the target with the same name name as the event.
                    // Can't use an .isFunction() check here because IE6/7 fails that test.
                    // Don't do default actions on window, that's where global variables be (#6170)
                    // IE<9 dies on focus/blur to hidden element (#1486)
                    if (ontype && elem[type] && ((type !== "focus" && type !== "blur") || event.target.offsetWidth !== 0) && !jQuery.isWindow(elem)) {

                        // Don't re-trigger an onFOO event when we call its FOO() method
                        old = elem[ontype];

                        if (old) {
                            elem[ontype] = null;
                        }

                        // Prevent re-triggering of the same event, since we already bubbled it above
                        jQuery.event.triggered = type;
                        elem[type]();
                        jQuery.event.triggered = undefined;

                        if (old) {
                            elem[ontype] = old;
                        }
                    }
                }
            }

            return event.result;
        },

        dispatch: function(event) {

            // Make a writable jQuery.Event from the native event object
            event = jQuery.event.fix(event || window.event);

            var handlers = ((jQuery._data(this, "events") || {})[event.type] || []),
                delegateCount = handlers.delegateCount,
                args = [].slice.call(arguments, 0),
                run_all = !event.exclusive && !event.namespace,
                special = jQuery.event.special[event.type] || {},
                handlerQueue = [],
                i, j, cur, jqcur, ret, selMatch, matched, matches, handleObj, sel, related;

            // Use the fix-ed jQuery.Event rather than the (read-only) native event
            args[0] = event;
            event.delegateTarget = this;

            // Call the preDispatch hook for the mapped type, and let it bail if desired
            if (special.preDispatch && special.preDispatch.call(this, event) === false) {
                return;
            }

            // Determine handlers that should run if there are delegated events
            // Avoid non-left-click bubbling in Firefox (#3861)
            if (delegateCount && !(event.button && event.type === "click")) {

                // Pregenerate a single jQuery object for reuse with .is()
                jqcur = jQuery(this);
                jqcur.context = this.ownerDocument || this;

                for (cur = event.target; cur != this; cur = cur.parentNode || this) {

                    // Don't process events on disabled elements (#6911, #8165)
                    if (cur.disabled !== true) {
                        selMatch = {};
                        matches = [];
                        jqcur[0] = cur;
                        for (i = 0; i < delegateCount; i++) {
                            handleObj = handlers[i];
                            sel = handleObj.selector;

                            if (selMatch[sel] === undefined) {
                                selMatch[sel] = (
                                handleObj.quick ? quickIs(cur, handleObj.quick) : jqcur.is(sel));
                            }
                            if (selMatch[sel]) {
                                matches.push(handleObj);
                            }
                        }
                        if (matches.length) {
                            handlerQueue.push({
                                elem: cur,
                                matches: matches
                            });
                        }
                    }
                }
            }

            // Add the remaining (directly-bound) handlers
            if (handlers.length > delegateCount) {
                handlerQueue.push({
                    elem: this,
                    matches: handlers.slice(delegateCount)
                });
            }

            // Run delegates first; they may want to stop propagation beneath us
            for (i = 0; i < handlerQueue.length && !event.isPropagationStopped(); i++) {
                matched = handlerQueue[i];
                event.currentTarget = matched.elem;

                for (j = 0; j < matched.matches.length && !event.isImmediatePropagationStopped(); j++) {
                    handleObj = matched.matches[j];

                    // Triggered event must either 1) be non-exclusive and have no namespace, or
                    // 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
                    if (run_all || (!event.namespace && !handleObj.namespace) || event.namespace_re && event.namespace_re.test(handleObj.namespace)) {

                        event.data = handleObj.data;
                        event.handleObj = handleObj;

                        ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);

                        if (ret !== undefined) {
                            event.result = ret;
                            if (ret === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                        }
                    }
                }
            }

            // Call the postDispatch hook for the mapped type
            if (special.postDispatch) {
                special.postDispatch.call(this, event);
            }

            return event.result;
        },

        // Includes some event props shared by KeyEvent and MouseEvent
        // *** attrChange attrName relatedNode srcElement  are not normalized, non-W3C, deprecated, will be removed in 1.8 ***
        props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

        fixHooks: {},

        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(event, original) {

                // Add which for key events
                if (event.which == null) {
                    event.which = original.charCode != null ? original.charCode : original.keyCode;
                }

                return event;
            }
        },

        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(event, original) {
                var eventDoc, doc, body, button = original.button,
                    fromElement = original.fromElement;

                // Calculate pageX/Y if missing and clientX/Y available
                if (event.pageX == null && original.clientX != null) {
                    eventDoc = event.target.ownerDocument || document;
                    doc = eventDoc.documentElement;
                    body = eventDoc.body;

                    event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                    event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
                }

                // Add relatedTarget, if necessary
                if (!event.relatedTarget && fromElement) {
                    event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
                }

                // Add which for click: 1 === left; 2 === middle; 3 === right
                // Note: button is not normalized, so don't use it
                if (!event.which && button !== undefined) {
                    event.which = (button & 1 ? 1 : (button & 2 ? 3 : (button & 4 ? 2 : 0)));
                }

                return event;
            }
        },

        fix: function(event) {
            if (event[jQuery.expando]) {
                return event;
            }

            // Create a writable copy of the event object and normalize some properties
            var i, prop, originalEvent = event,
                fixHook = jQuery.event.fixHooks[event.type] || {},
                copy = fixHook.props ? this.props.concat(fixHook.props) : this.props;

            event = jQuery.Event(originalEvent);

            for (i = copy.length; i;) {
                prop = copy[--i];
                event[prop] = originalEvent[prop];
            }

            // Fix target property, if necessary (#1925, IE 6/7/8 & Safari2)
            if (!event.target) {
                event.target = originalEvent.srcElement || document;
            }

            // Target should not be a text node (#504, Safari)
            if (event.target.nodeType === 3) {
                event.target = event.target.parentNode;
            }

            // For mouse/key events; add metaKey if it's not there (#3368, IE6/7/8)
            if (event.metaKey === undefined) {
                event.metaKey = event.ctrlKey;
            }

            return fixHook.filter ? fixHook.filter(event, originalEvent) : event;
        },

        special: {
            ready: {
                // Make sure the ready event is setup
                setup: jQuery.bindReady
            },

            load: {
                // Prevent triggered image.load events from bubbling to window.load
                noBubble: true
            },

            focus: {
                delegateType: "focusin"
            },
            blur: {
                delegateType: "focusout"
            },

            beforeunload: {
                setup: function(data, namespaces, eventHandle) {
                    // We only want to do this special case on windows
                    if (jQuery.isWindow(this)) {
                        this.onbeforeunload = eventHandle;
                    }
                },

                teardown: function(namespaces, eventHandle) {
                    if (this.onbeforeunload === eventHandle) {
                        this.onbeforeunload = null;
                    }
                }
            }
        },

        simulate: function(type, elem, event, bubble) {
            // Piggyback on a donor event to simulate a different one.
            // Fake originalEvent to avoid donor's stopPropagation, but if the
            // simulated event prevents default then we do the same on the donor.
            var e = jQuery.extend(
            new jQuery.Event(), event, {
                type: type,
                isSimulated: true,
                originalEvent: {}
            });
            if (bubble) {
                jQuery.event.trigger(e, null, elem);
            } else {
                jQuery.event.dispatch.call(elem, e);
            }
            if (e.isDefaultPrevented()) {
                event.preventDefault();
            }
        }
    };

    // Some plugins are using, but it's undocumented/deprecated and will be removed.
    // The 1.7 special event interface should provide all the hooks needed now.
    jQuery.event.handle = jQuery.event.dispatch;

    jQuery.removeEvent = document.removeEventListener ?
    function(elem, type, handle) {
        if (elem.removeEventListener) {
            elem.removeEventListener(type, handle, false);
        }
    } : function(elem, type, handle) {
        if (elem.detachEvent) {
            elem.detachEvent("on" + type, handle);
        }
    };

    jQuery.Event = function(src, props) {
        // Allow instantiation without the 'new' keyword
        if (!(this instanceof jQuery.Event)) {
            return new jQuery.Event(src, props);
        }

        // Event object
        if (src && src.type) {
            this.originalEvent = src;
            this.type = src.type;

            // Events bubbling up the document may have been marked as prevented
            // by a handler lower down the tree; reflect the correct value.
            this.isDefaultPrevented = (src.defaultPrevented || src.returnValue === false || src.getPreventDefault && src.getPreventDefault()) ? returnTrue : returnFalse;

            // Event type
        } else {
            this.type = src;
        }

        // Put explicitly provided properties onto the event object
        if (props) {
            jQuery.extend(this, props);
        }

        // Create a timestamp if incoming event doesn't have one
        this.timeStamp = src && src.timeStamp || jQuery.now();

        // Mark it as fixed
        this[jQuery.expando] = true;
    };

    function returnFalse() {
        return false;
    }

    function returnTrue() {
        return true;
    }

    // jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
    // http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
    jQuery.Event.prototype = {
        preventDefault: function() {
            this.isDefaultPrevented = returnTrue;

            var e = this.originalEvent;
            if (!e) {
                return;
            }

            // if preventDefault exists run it on the original event
            if (e.preventDefault) {
                e.preventDefault();

                // otherwise set the returnValue property of the original event to false (IE)
            } else {
                e.returnValue = false;
            }
        },
        stopPropagation: function() {
            this.isPropagationStopped = returnTrue;

            var e = this.originalEvent;
            if (!e) {
                return;
            }
            // if stopPropagation exists run it on the original event
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            // otherwise set the cancelBubble property of the original event to true (IE)
            e.cancelBubble = true;
        },
        stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = returnTrue;
            this.stopPropagation();
        },
        isDefaultPrevented: returnFalse,
        isPropagationStopped: returnFalse,
        isImmediatePropagationStopped: returnFalse
    };

    // Create mouseenter/leave events using mouseover/out and event-time checks
    jQuery.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function(orig, fix) {
        jQuery.event.special[orig] = {
            delegateType: fix,
            bindType: fix,

            handle: function(event) {
                var target = this,
                    related = event.relatedTarget,
                    handleObj = event.handleObj,
                    selector = handleObj.selector,
                    ret;

                // For mousenter/leave call the handler if related is outside the target.
                // NB: No relatedTarget if the mouse left/entered the browser window
                if (!related || (related !== target && !jQuery.contains(target, related))) {
                    event.type = handleObj.origType;
                    ret = handleObj.handler.apply(this, arguments);
                    event.type = fix;
                }
                return ret;
            }
        };
    });

    // IE submit delegation
    if (!jQuery.support.submitBubbles) {

        jQuery.event.special.submit = {
            setup: function() {
                // Only need this for delegated form submit events
                if (jQuery.nodeName(this, "form")) {
                    return false;
                }

                // Lazy-add a submit handler when a descendant form may potentially be submitted
                jQuery.event.add(this, "click._submit keypress._submit", function(e) {
                    // Node name check avoids a VML-related crash in IE (#9807)
                    var elem = e.target,
                        form = jQuery.nodeName(elem, "input") || jQuery.nodeName(elem, "button") ? elem.form : undefined;
                    if (form && !form._submit_attached) {
                        jQuery.event.add(form, "submit._submit", function(event) {
                            event._submit_bubble = true;
                        });
                        form._submit_attached = true;
                    }
                });
                // return undefined since we don't need an event listener
            },

            postDispatch: function(event) {
                // If form was submitted by the user, bubble the event up the tree
                if (event._submit_bubble) {
                    delete event._submit_bubble;
                    if (this.parentNode && !event.isTrigger) {
                        jQuery.event.simulate("submit", this.parentNode, event, true);
                    }
                }
            },

            teardown: function() {
                // Only need this for delegated form submit events
                if (jQuery.nodeName(this, "form")) {
                    return false;
                }

                // Remove delegated handlers; cleanData eventually reaps submit handlers attached above
                jQuery.event.remove(this, "._submit");
            }
        };
    }

    // IE change delegation and checkbox/radio fix
    if (!jQuery.support.changeBubbles) {

        jQuery.event.special.change = {

            setup: function() {

                if (rformElems.test(this.nodeName)) {
                    // IE doesn't fire change on a check/radio until blur; trigger it on click
                    // after a propertychange. Eat the blur-change in special.change.handle.
                    // This still fires onchange a second time for check/radio after blur.
                    if (this.type === "checkbox" || this.type === "radio") {
                        jQuery.event.add(this, "propertychange._change", function(event) {
                            if (event.originalEvent.propertyName === "checked") {
                                this._just_changed = true;
                            }
                        });
                        jQuery.event.add(this, "click._change", function(event) {
                            if (this._just_changed && !event.isTrigger) {
                                this._just_changed = false;
                                jQuery.event.simulate("change", this, event, true);
                            }
                        });
                    }
                    return false;
                }
                // Delegated event; lazy-add a change handler on descendant inputs
                jQuery.event.add(this, "beforeactivate._change", function(e) {
                    var elem = e.target;

                    if (rformElems.test(elem.nodeName) && !elem._change_attached) {
                        jQuery.event.add(elem, "change._change", function(event) {
                            if (this.parentNode && !event.isSimulated && !event.isTrigger) {
                                jQuery.event.simulate("change", this.parentNode, event, true);
                            }
                        });
                        elem._change_attached = true;
                    }
                });
            },

            handle: function(event) {
                var elem = event.target;

                // Swallow native change events from checkbox/radio, we already triggered them above
                if (this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox")) {
                    return event.handleObj.handler.apply(this, arguments);
                }
            },

            teardown: function() {
                jQuery.event.remove(this, "._change");

                return rformElems.test(this.nodeName);
            }
        };
    }

    // Create "bubbling" focus and blur events
    if (!jQuery.support.focusinBubbles) {
        jQuery.each({
            focus: "focusin",
            blur: "focusout"
        }, function(orig, fix) {

            // Attach a single capturing handler while someone wants focusin/focusout
            var attaches = 0,
                handler = function(event) {
                    jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), true);
                };

            jQuery.event.special[fix] = {
                setup: function() {
                    if (attaches++ === 0) {
                        document.addEventListener(orig, handler, true);
                    }
                },
                teardown: function() {
                    if (--attaches === 0) {
                        document.removeEventListener(orig, handler, true);
                    }
                }
            };
        });
    }

    jQuery.fn.extend({

        on: function(types, selector, data, fn, /*INTERNAL*/ one) {
            var origFn, type;

            // Types can be a map of types/handlers
            if (typeof types === "object") {
                // ( types-Object, selector, data )
                if (typeof selector !== "string") { // && selector != null
                    // ( types-Object, data )
                    data = data || selector;
                    selector = undefined;
                }
                for (type in types) {
                    this.on(type, selector, data, types[type], one);
                }
                return this;
            }

            if (data == null && fn == null) {
                // ( types, fn )
                fn = selector;
                data = selector = undefined;
            } else if (fn == null) {
                if (typeof selector === "string") {
                    // ( types, selector, fn )
                    fn = data;
                    data = undefined;
                } else {
                    // ( types, data, fn )
                    fn = data;
                    data = selector;
                    selector = undefined;
                }
            }
            if (fn === false) {
                fn = returnFalse;
            } else if (!fn) {
                return this;
            }

            if (one === 1) {
                origFn = fn;
                fn = function(event) {
                    // Can use an empty set, since event contains the info
                    jQuery().off(event);
                    return origFn.apply(this, arguments);
                };
                // Use same guid so caller can remove using origFn
                fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
            }
            return this.each(function() {
                jQuery.event.add(this, types, fn, data, selector);
            });
        },
        one: function(types, selector, data, fn) {
            return this.on(types, selector, data, fn, 1);
        },
        off: function(types, selector, fn) {
            if (types && types.preventDefault && types.handleObj) {
                // ( event )  dispatched jQuery.Event
                var handleObj = types.handleObj;
                jQuery(types.delegateTarget).off(
                handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
                return this;
            }
            if (typeof types === "object") {
                // ( types-object [, selector] )
                for (var type in types) {
                    this.off(type, selector, types[type]);
                }
                return this;
            }
            if (selector === false || typeof selector === "function") {
                // ( types [, fn] )
                fn = selector;
                selector = undefined;
            }
            if (fn === false) {
                fn = returnFalse;
            }
            return this.each(function() {
                jQuery.event.remove(this, types, fn, selector);
            });
        },

        bind: function(types, data, fn) {
            return this.on(types, null, data, fn);
        },
        unbind: function(types, fn) {
            return this.off(types, null, fn);
        },

        live: function(types, data, fn) {
            jQuery(this.context).on(types, this.selector, data, fn);
            return this;
        },
        die: function(types, fn) {
            jQuery(this.context).off(types, this.selector || "**", fn);
            return this;
        },

        delegate: function(selector, types, data, fn) {
            return this.on(types, selector, data, fn);
        },
        undelegate: function(selector, types, fn) {
            // ( namespace ) or ( selector, types [, fn] )
            return arguments.length == 1 ? this.off(selector, "**") : this.off(types, selector, fn);
        },

        trigger: function(type, data) {
            return this.each(function() {
                jQuery.event.trigger(type, data, this);
            });
        },
        triggerHandler: function(type, data) {
            if (this[0]) {
                return jQuery.event.trigger(type, data, this[0], true);
            }
        },

        toggle: function(fn) {
            // Save reference to arguments for access in closure
            var args = arguments,
                guid = fn.guid || jQuery.guid++,
                i = 0,
                toggler = function(event) {
                    // Figure out which function to execute
                    var lastToggle = (jQuery._data(this, "lastToggle" + fn.guid) || 0) % i;
                    jQuery._data(this, "lastToggle" + fn.guid, lastToggle + 1);

                    // Make sure that clicks stop
                    event.preventDefault();

                    // and execute the function
                    return args[lastToggle].apply(this, arguments) || false;
                };

            // link all the functions, so any of them can unbind this click handler
            toggler.guid = guid;
            while (i < args.length) {
                args[i++].guid = guid;
            }

            return this.click(toggler);
        },

        hover: function(fnOver, fnOut) {
            return this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
        }
    });

    jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup error contextmenu").split(" "), function(i, name) {

        // Handle event binding
        jQuery.fn[name] = function(data, fn) {
            if (fn == null) {
                fn = data;
                data = null;
            }

            return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
        };

        if (jQuery.attrFn) {
            jQuery.attrFn[name] = true;
        }

        if (rkeyEvent.test(name)) {
            jQuery.event.fixHooks[name] = jQuery.event.keyHooks;
        }

        if (rmouseEvent.test(name)) {
            jQuery.event.fixHooks[name] = jQuery.event.mouseHooks;
        }
    });



    /*!
     * Sizzle CSS Selector Engine
     *  Copyright 2011, The Dojo Foundation
     *  Released under the MIT, BSD, and GPL Licenses.
     *  More information: http://sizzlejs.com/
     */
    (function() {

        var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
            expando = "sizcache" + (Math.random() + '').replace('.', ''),
            done = 0,
            toString = Object.prototype.toString,
            hasDuplicate = false,
            baseHasDuplicate = true,
            rBackslash = /\\/g,
            rReturn = /\r\n/g,
            rNonWord = /\W/;

        // Here we check if the JavaScript engine is using some sort of
        // optimization where it does not always call our comparision
        // function. If that is the case, discard the hasDuplicate value.
        //   Thus far that includes Google Chrome.
        [0, 0].sort(function() {
            baseHasDuplicate = false;
            return 0;
        });

        var Sizzle = function(selector, context, results, seed) {
                results = results || [];
                context = context || document;

                var origContext = context;

                if (context.nodeType !== 1 && context.nodeType !== 9) {
                    return [];
                }

                if (!selector || typeof selector !== "string") {
                    return results;
                }

                var m, set, checkSet, extra, ret, cur, pop, i, prune = true,
                    contextXML = Sizzle.isXML(context),
                    parts = [],
                    soFar = selector;

                // Reset the position of the chunker regexp (start from head)
                do {
                    chunker.exec("");
                    m = chunker.exec(soFar);

                    if (m) {
                        soFar = m[3];

                        parts.push(m[1]);

                        if (m[2]) {
                            extra = m[3];
                            break;
                        }
                    }
                } while (m);

                if (parts.length > 1 && origPOS.exec(selector)) {

                    if (parts.length === 2 && Expr.relative[parts[0]]) {
                        set = posProcess(parts[0] + parts[1], context, seed);

                    } else {
                        set = Expr.relative[parts[0]] ? [context] : Sizzle(parts.shift(), context);

                        while (parts.length) {
                            selector = parts.shift();

                            if (Expr.relative[selector]) {
                                selector += parts.shift();
                            }

                            set = posProcess(selector, set, seed);
                        }
                    }

                } else {
                    // Take a shortcut and set the context if the root selector is an ID
                    // (but not if it'll be faster if the inner selector is an ID)
                    if (!seed && parts.length > 1 && context.nodeType === 9 && !contextXML && Expr.match.ID.test(parts[0]) && !Expr.match.ID.test(parts[parts.length - 1])) {

                        ret = Sizzle.find(parts.shift(), context, contextXML);
                        context = ret.expr ? Sizzle.filter(ret.expr, ret.set)[0] : ret.set[0];
                    }

                    if (context) {
                        ret = seed ? {
                            expr: parts.pop(),
                            set: makeArray(seed)
                        } : Sizzle.find(parts.pop(), parts.length === 1 && (parts[0] === "~" || parts[0] === "+") && context.parentNode ? context.parentNode : context, contextXML);

                        set = ret.expr ? Sizzle.filter(ret.expr, ret.set) : ret.set;

                        if (parts.length > 0) {
                            checkSet = makeArray(set);

                        } else {
                            prune = false;
                        }

                        while (parts.length) {
                            cur = parts.pop();
                            pop = cur;

                            if (!Expr.relative[cur]) {
                                cur = "";
                            } else {
                                pop = parts.pop();
                            }

                            if (pop == null) {
                                pop = context;
                            }

                            Expr.relative[cur](checkSet, pop, contextXML);
                        }

                    } else {
                        checkSet = parts = [];
                    }
                }

                if (!checkSet) {
                    checkSet = set;
                }

                if (!checkSet) {
                    Sizzle.error(cur || selector);
                }

                if (toString.call(checkSet) === "[object Array]") {
                    if (!prune) {
                        results.push.apply(results, checkSet);

                    } else if (context && context.nodeType === 1) {
                        for (i = 0; checkSet[i] != null; i++) {
                            if (checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && Sizzle.contains(context, checkSet[i]))) {
                                results.push(set[i]);
                            }
                        }

                    } else {
                        for (i = 0; checkSet[i] != null; i++) {
                            if (checkSet[i] && checkSet[i].nodeType === 1) {
                                results.push(set[i]);
                            }
                        }
                    }

                } else {
                    makeArray(checkSet, results);
                }

                if (extra) {
                    Sizzle(extra, origContext, results, seed);
                    Sizzle.uniqueSort(results);
                }

                return results;
            };

        Sizzle.uniqueSort = function(results) {
            if (sortOrder) {
                hasDuplicate = baseHasDuplicate;
                results.sort(sortOrder);

                if (hasDuplicate) {
                    for (var i = 1; i < results.length; i++) {
                        if (results[i] === results[i - 1]) {
                            results.splice(i--, 1);
                        }
                    }
                }
            }

            return results;
        };

        Sizzle.matches = function(expr, set) {
            return Sizzle(expr, null, null, set);
        };

        Sizzle.matchesSelector = function(node, expr) {
            return Sizzle(expr, null, null, [node]).length > 0;
        };

        Sizzle.find = function(expr, context, isXML) {
            var set, i, len, match, type, left;

            if (!expr) {
                return [];
            }

            for (i = 0, len = Expr.order.length; i < len; i++) {
                type = Expr.order[i];

                if ((match = Expr.leftMatch[type].exec(expr))) {
                    left = match[1];
                    match.splice(1, 1);

                    if (left.substr(left.length - 1) !== "\\") {
                        match[1] = (match[1] || "").replace(rBackslash, "");
                        set = Expr.find[type](match, context, isXML);

                        if (set != null) {
                            expr = expr.replace(Expr.match[type], "");
                            break;
                        }
                    }
                }
            }

            if (!set) {
                set = typeof context.getElementsByTagName !== "undefined" ? context.getElementsByTagName("*") : [];
            }

            return {
                set: set,
                expr: expr
            };
        };

        Sizzle.filter = function(expr, set, inplace, not) {
            var match, anyFound, type, found, item, filter, left, i, pass, old = expr,
                result = [],
                curLoop = set,
                isXMLFilter = set && set[0] && Sizzle.isXML(set[0]);

            while (expr && set.length) {
                for (type in Expr.filter) {
                    if ((match = Expr.leftMatch[type].exec(expr)) != null && match[2]) {
                        filter = Expr.filter[type];
                        left = match[1];

                        anyFound = false;

                        match.splice(1, 1);

                        if (left.substr(left.length - 1) === "\\") {
                            continue;
                        }

                        if (curLoop === result) {
                            result = [];
                        }

                        if (Expr.preFilter[type]) {
                            match = Expr.preFilter[type](match, curLoop, inplace, result, not, isXMLFilter);

                            if (!match) {
                                anyFound = found = true;

                            } else if (match === true) {
                                continue;
                            }
                        }

                        if (match) {
                            for (i = 0;
                            (item = curLoop[i]) != null; i++) {
                                if (item) {
                                    found = filter(item, match, i, curLoop);
                                    pass = not ^ found;

                                    if (inplace && found != null) {
                                        if (pass) {
                                            anyFound = true;

                                        } else {
                                            curLoop[i] = false;
                                        }

                                    } else if (pass) {
                                        result.push(item);
                                        anyFound = true;
                                    }
                                }
                            }
                        }

                        if (found !== undefined) {
                            if (!inplace) {
                                curLoop = result;
                            }

                            expr = expr.replace(Expr.match[type], "");

                            if (!anyFound) {
                                return [];
                            }

                            break;
                        }
                    }
                }

                // Improper expression
                if (expr === old) {
                    if (anyFound == null) {
                        Sizzle.error(expr);

                    } else {
                        break;
                    }
                }

                old = expr;
            }

            return curLoop;
        };

        Sizzle.error = function(msg) {
            throw new Error("Syntax error, unrecognized expression: " + msg);
        };

        /**
         * Utility function for retreiving the text value of an array of DOM nodes
         * @param {Array|Element} elem
         */
        var getText = Sizzle.getText = function(elem) {
                var i, node, nodeType = elem.nodeType,
                    ret = "";

                if (nodeType) {
                    if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
                        // Use textContent || innerText for elements
                        if (typeof elem.textContent === 'string') {
                            return elem.textContent;
                        } else if (typeof elem.innerText === 'string') {
                            // Replace IE's carriage returns
                            return elem.innerText.replace(rReturn, '');
                        } else {
                            // Traverse it's children
                            for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                                ret += getText(elem);
                            }
                        }
                    } else if (nodeType === 3 || nodeType === 4) {
                        return elem.nodeValue;
                    }
                } else {

                    // If no nodeType, this is expected to be an array
                    for (i = 0;
                    (node = elem[i]); i++) {
                        // Do not traverse comment nodes
                        if (node.nodeType !== 8) {
                            ret += getText(node);
                        }
                    }
                }
                return ret;
            };

        var Expr = Sizzle.selectors = {
            order: ["ID", "NAME", "TAG"],

            match: {
                ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
                CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
                NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
                ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
                TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
                CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
                POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
                PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/
            },

            leftMatch: {},

            attrMap: {
                "class": "className",
                "for": "htmlFor"
            },

            attrHandle: {
                href: function(elem) {
                    return elem.getAttribute("href");
                },
                type: function(elem) {
                    return elem.getAttribute("type");
                }
            },

            relative: {
                "+": function(checkSet, part) {
                    var isPartStr = typeof part === "string",
                        isTag = isPartStr && !rNonWord.test(part),
                        isPartStrNotTag = isPartStr && !isTag;

                    if (isTag) {
                        part = part.toLowerCase();
                    }

                    for (var i = 0, l = checkSet.length, elem; i < l; i++) {
                        if ((elem = checkSet[i])) {
                            while ((elem = elem.previousSibling) && elem.nodeType !== 1) {}

                            checkSet[i] = isPartStrNotTag || elem && elem.nodeName.toLowerCase() === part ? elem || false : elem === part;
                        }
                    }

                    if (isPartStrNotTag) {
                        Sizzle.filter(part, checkSet, true);
                    }
                },

                ">": function(checkSet, part) {
                    var elem, isPartStr = typeof part === "string",
                        i = 0,
                        l = checkSet.length;

                    if (isPartStr && !rNonWord.test(part)) {
                        part = part.toLowerCase();

                        for (; i < l; i++) {
                            elem = checkSet[i];

                            if (elem) {
                                var parent = elem.parentNode;
                                checkSet[i] = parent.nodeName.toLowerCase() === part ? parent : false;
                            }
                        }

                    } else {
                        for (; i < l; i++) {
                            elem = checkSet[i];

                            if (elem) {
                                checkSet[i] = isPartStr ? elem.parentNode : elem.parentNode === part;
                            }
                        }

                        if (isPartStr) {
                            Sizzle.filter(part, checkSet, true);
                        }
                    }
                },

                "": function(checkSet, part, isXML) {
                    var nodeCheck, doneName = done++,
                        checkFn = dirCheck;

                    if (typeof part === "string" && !rNonWord.test(part)) {
                        part = part.toLowerCase();
                        nodeCheck = part;
                        checkFn = dirNodeCheck;
                    }

                    checkFn("parentNode", part, doneName, checkSet, nodeCheck, isXML);
                },

                "~": function(checkSet, part, isXML) {
                    var nodeCheck, doneName = done++,
                        checkFn = dirCheck;

                    if (typeof part === "string" && !rNonWord.test(part)) {
                        part = part.toLowerCase();
                        nodeCheck = part;
                        checkFn = dirNodeCheck;
                    }

                    checkFn("previousSibling", part, doneName, checkSet, nodeCheck, isXML);
                }
            },

            find: {
                ID: function(match, context, isXML) {
                    if (typeof context.getElementById !== "undefined" && !isXML) {
                        var m = context.getElementById(match[1]);
                        // Check parentNode to catch when Blackberry 4.6 returns
                        // nodes that are no longer in the document #6963
                        return m && m.parentNode ? [m] : [];
                    }
                },

                NAME: function(match, context) {
                    if (typeof context.getElementsByName !== "undefined") {
                        var ret = [],
                            results = context.getElementsByName(match[1]);

                        for (var i = 0, l = results.length; i < l; i++) {
                            if (results[i].getAttribute("name") === match[1]) {
                                ret.push(results[i]);
                            }
                        }

                        return ret.length === 0 ? null : ret;
                    }
                },

                TAG: function(match, context) {
                    if (typeof context.getElementsByTagName !== "undefined") {
                        return context.getElementsByTagName(match[1]);
                    }
                }
            },
            preFilter: {
                CLASS: function(match, curLoop, inplace, result, not, isXML) {
                    match = " " + match[1].replace(rBackslash, "") + " ";

                    if (isXML) {
                        return match;
                    }

                    for (var i = 0, elem;
                    (elem = curLoop[i]) != null; i++) {
                        if (elem) {
                            if (not ^ (elem.className && (" " + elem.className + " ").replace(/[\t\n\r]/g, " ").indexOf(match) >= 0)) {
                                if (!inplace) {
                                    result.push(elem);
                                }

                            } else if (inplace) {
                                curLoop[i] = false;
                            }
                        }
                    }

                    return false;
                },

                ID: function(match) {
                    return match[1].replace(rBackslash, "");
                },

                TAG: function(match, curLoop) {
                    return match[1].replace(rBackslash, "").toLowerCase();
                },

                CHILD: function(match) {
                    if (match[1] === "nth") {
                        if (!match[2]) {
                            Sizzle.error(match[0]);
                        }

                        match[2] = match[2].replace(/^\+|\s*/g, '');

                        // parse equations like 'even', 'odd', '5', '2n', '3n+2', '4n-1', '-n+6'
                        var test = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(
                        match[2] === "even" && "2n" || match[2] === "odd" && "2n+1" || !/\D/.test(match[2]) && "0n+" + match[2] || match[2]);

                        // calculate the numbers (first)n+(last) including if they are negative
                        match[2] = (test[1] + (test[2] || 1)) - 0;
                        match[3] = test[3] - 0;
                    } else if (match[2]) {
                        Sizzle.error(match[0]);
                    }

                    // TODO: Move to normal caching system
                    match[0] = done++;

                    return match;
                },

                ATTR: function(match, curLoop, inplace, result, not, isXML) {
                    var name = match[1] = match[1].replace(rBackslash, "");

                    if (!isXML && Expr.attrMap[name]) {
                        match[1] = Expr.attrMap[name];
                    }

                    // Handle if an un-quoted value was used
                    match[4] = (match[4] || match[5] || "").replace(rBackslash, "");

                    if (match[2] === "~=") {
                        match[4] = " " + match[4] + " ";
                    }

                    return match;
                },

                PSEUDO: function(match, curLoop, inplace, result, not) {
                    if (match[1] === "not") {
                        // If we're dealing with a complex expression, or a simple one
                        if ((chunker.exec(match[3]) || "").length > 1 || /^\w/.test(match[3])) {
                            match[3] = Sizzle(match[3], null, null, curLoop);

                        } else {
                            var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not);

                            if (!inplace) {
                                result.push.apply(result, ret);
                            }

                            return false;
                        }

                    } else if (Expr.match.POS.test(match[0]) || Expr.match.CHILD.test(match[0])) {
                        return true;
                    }

                    return match;
                },

                POS: function(match) {
                    match.unshift(true);

                    return match;
                }
            },

            filters: {
                enabled: function(elem) {
                    return elem.disabled === false && elem.type !== "hidden";
                },

                disabled: function(elem) {
                    return elem.disabled === true;
                },

                checked: function(elem) {
                    return elem.checked === true;
                },

                selected: function(elem) {
                    // Accessing this property makes selected-by-default
                    // options in Safari work properly
                    if (elem.parentNode) {
                        elem.parentNode.selectedIndex;
                    }

                    return elem.selected === true;
                },

                parent: function(elem) {
                    return !!elem.firstChild;
                },

                empty: function(elem) {
                    return !elem.firstChild;
                },

                has: function(elem, i, match) {
                    return !!Sizzle(match[3], elem).length;
                },

                header: function(elem) {
                    return (/h\d/i).test(elem.nodeName);
                },

                text: function(elem) {
                    var attr = elem.getAttribute("type"),
                        type = elem.type;
                    // IE6 and 7 will map elem.type to 'text' for new HTML5 types (search, etc)
                    // use getAttribute instead to test this case
                    return elem.nodeName.toLowerCase() === "input" && "text" === type && (attr === type || attr === null);
                },

                radio: function(elem) {
                    return elem.nodeName.toLowerCase() === "input" && "radio" === elem.type;
                },

                checkbox: function(elem) {
                    return elem.nodeName.toLowerCase() === "input" && "checkbox" === elem.type;
                },

                file: function(elem) {
                    return elem.nodeName.toLowerCase() === "input" && "file" === elem.type;
                },

                password: function(elem) {
                    return elem.nodeName.toLowerCase() === "input" && "password" === elem.type;
                },

                submit: function(elem) {
                    var name = elem.nodeName.toLowerCase();
                    return (name === "input" || name === "button") && "submit" === elem.type;
                },

                image: function(elem) {
                    return elem.nodeName.toLowerCase() === "input" && "image" === elem.type;
                },

                reset: function(elem) {
                    var name = elem.nodeName.toLowerCase();
                    return (name === "input" || name === "button") && "reset" === elem.type;
                },

                button: function(elem) {
                    var name = elem.nodeName.toLowerCase();
                    return name === "input" && "button" === elem.type || name === "button";
                },

                input: function(elem) {
                    return (/input|select|textarea|button/i).test(elem.nodeName);
                },

                focus: function(elem) {
                    return elem === elem.ownerDocument.activeElement;
                }
            },
            setFilters: {
                first: function(elem, i) {
                    return i === 0;
                },

                last: function(elem, i, match, array) {
                    return i === array.length - 1;
                },

                even: function(elem, i) {
                    return i % 2 === 0;
                },

                odd: function(elem, i) {
                    return i % 2 === 1;
                },

                lt: function(elem, i, match) {
                    return i < match[3] - 0;
                },

                gt: function(elem, i, match) {
                    return i > match[3] - 0;
                },

                nth: function(elem, i, match) {
                    return match[3] - 0 === i;
                },

                eq: function(elem, i, match) {
                    return match[3] - 0 === i;
                }
            },
            filter: {
                PSEUDO: function(elem, match, i, array) {
                    var name = match[1],
                        filter = Expr.filters[name];

                    if (filter) {
                        return filter(elem, i, match, array);

                    } else if (name === "contains") {
                        return (elem.textContent || elem.innerText || getText([elem]) || "").indexOf(match[3]) >= 0;

                    } else if (name === "not") {
                        var not = match[3];

                        for (var j = 0, l = not.length; j < l; j++) {
                            if (not[j] === elem) {
                                return false;
                            }
                        }

                        return true;

                    } else {
                        Sizzle.error(name);
                    }
                },

                CHILD: function(elem, match) {
                    var first, last, doneName, parent, cache, count, diff, type = match[1],
                        node = elem;

                    switch (type) {
                    case "only":
                    case "first":
                        while ((node = node.previousSibling)) {
                            if (node.nodeType === 1) {
                                return false;
                            }
                        }

                        if (type === "first") {
                            return true;
                        }

                        node = elem;

                        /* falls through */
                    case "last":
                        while ((node = node.nextSibling)) {
                            if (node.nodeType === 1) {
                                return false;
                            }
                        }

                        return true;

                    case "nth":
                        first = match[2];
                        last = match[3];

                        if (first === 1 && last === 0) {
                            return true;
                        }

                        doneName = match[0];
                        parent = elem.parentNode;

                        if (parent && (parent[expando] !== doneName || !elem.nodeIndex)) {
                            count = 0;

                            for (node = parent.firstChild; node; node = node.nextSibling) {
                                if (node.nodeType === 1) {
                                    node.nodeIndex = ++count;
                                }
                            }

                            parent[expando] = doneName;
                        }

                        diff = elem.nodeIndex - last;

                        if (first === 0) {
                            return diff === 0;

                        } else {
                            return (diff % first === 0 && diff / first >= 0);
                        }
                    }
                },

                ID: function(elem, match) {
                    return elem.nodeType === 1 && elem.getAttribute("id") === match;
                },

                TAG: function(elem, match) {
                    return (match === "*" && elem.nodeType === 1) || !! elem.nodeName && elem.nodeName.toLowerCase() === match;
                },

                CLASS: function(elem, match) {
                    return (" " + (elem.className || elem.getAttribute("class")) + " ").indexOf(match) > -1;
                },

                ATTR: function(elem, match) {
                    var name = match[1],
                        result = Sizzle.attr ? Sizzle.attr(elem, name) : Expr.attrHandle[name] ? Expr.attrHandle[name](elem) : elem[name] != null ? elem[name] : elem.getAttribute(name),
                        value = result + "",
                        type = match[2],
                        check = match[4];

                    return result == null ? type === "!=" : !type && Sizzle.attr ? result != null : type === "=" ? value === check : type === "*=" ? value.indexOf(check) >= 0 : type === "~=" ? (" " + value + " ").indexOf(check) >= 0 : !check ? value && result !== false : type === "!=" ? value !== check : type === "^=" ? value.indexOf(check) === 0 : type === "$=" ? value.substr(value.length - check.length) === check : type === "|=" ? value === check || value.substr(0, check.length + 1) === check + "-" : false;
                },

                POS: function(elem, match, i, array) {
                    var name = match[2],
                        filter = Expr.setFilters[name];

                    if (filter) {
                        return filter(elem, i, match, array);
                    }
                }
            }
        };

        var origPOS = Expr.match.POS,
            fescape = function(all, num) {
                return "\\" + (num - 0 + 1);
            };

        for (var type in Expr.match) {
            Expr.match[type] = new RegExp(Expr.match[type].source + (/(?![^\[]*\])(?![^\(]*\))/.source));
            Expr.leftMatch[type] = new RegExp(/(^(?:.|\r|\n)*?)/.source + Expr.match[type].source.replace(/\\(\d+)/g, fescape));
        }
        // Expose origPOS
        // "global" as in regardless of relation to brackets/parens
        Expr.match.globalPOS = origPOS;

        var makeArray = function(array, results) {
                array = Array.prototype.slice.call(array, 0);

                if (results) {
                    results.push.apply(results, array);
                    return results;
                }

                return array;
            };

        // Perform a simple check to determine if the browser is capable of
        // converting a NodeList to an array using builtin methods.
        // Also verifies that the returned array holds DOM nodes
        // (which is not the case in the Blackberry browser)
        try {
            Array.prototype.slice.call(document.documentElement.childNodes, 0)[0].nodeType;

            // Provide a fallback method if it does not work
        } catch (e) {
            makeArray = function(array, results) {
                var i = 0,
                    ret = results || [];

                if (toString.call(array) === "[object Array]") {
                    Array.prototype.push.apply(ret, array);

                } else {
                    if (typeof array.length === "number") {
                        for (var l = array.length; i < l; i++) {
                            ret.push(array[i]);
                        }

                    } else {
                        for (; array[i]; i++) {
                            ret.push(array[i]);
                        }
                    }
                }

                return ret;
            };
        }

        var sortOrder, siblingCheck;

        if (document.documentElement.compareDocumentPosition) {
            sortOrder = function(a, b) {
                if (a === b) {
                    hasDuplicate = true;
                    return 0;
                }

                if (!a.compareDocumentPosition || !b.compareDocumentPosition) {
                    return a.compareDocumentPosition ? -1 : 1;
                }

                return a.compareDocumentPosition(b) & 4 ? -1 : 1;
            };

        } else {
            sortOrder = function(a, b) {
                // The nodes are identical, we can exit early
                if (a === b) {
                    hasDuplicate = true;
                    return 0;

                    // Fallback to using sourceIndex (in IE) if it's available on both nodes
                } else if (a.sourceIndex && b.sourceIndex) {
                    return a.sourceIndex - b.sourceIndex;
                }

                var al, bl, ap = [],
                    bp = [],
                    aup = a.parentNode,
                    bup = b.parentNode,
                    cur = aup;

                // If the nodes are siblings (or identical) we can do a quick check
                if (aup === bup) {
                    return siblingCheck(a, b);

                    // If no parents were found then the nodes are disconnected
                } else if (!aup) {
                    return -1;

                } else if (!bup) {
                    return 1;
                }

                // Otherwise they're somewhere else in the tree so we need
                // to build up a full list of the parentNodes for comparison
                while (cur) {
                    ap.unshift(cur);
                    cur = cur.parentNode;
                }

                cur = bup;

                while (cur) {
                    bp.unshift(cur);
                    cur = cur.parentNode;
                }

                al = ap.length;
                bl = bp.length;

                // Start walking down the tree looking for a discrepancy
                for (var i = 0; i < al && i < bl; i++) {
                    if (ap[i] !== bp[i]) {
                        return siblingCheck(ap[i], bp[i]);
                    }
                }

                // We ended someplace up the tree so do a sibling check
                return i === al ? siblingCheck(a, bp[i], -1) : siblingCheck(ap[i], b, 1);
            };

            siblingCheck = function(a, b, ret) {
                if (a === b) {
                    return ret;
                }

                var cur = a.nextSibling;

                while (cur) {
                    if (cur === b) {
                        return -1;
                    }

                    cur = cur.nextSibling;
                }

                return 1;
            };
        }

        // Check to see if the browser returns elements by name when
        // querying by getElementById (and provide a workaround)
        (function() {
            // We're going to inject a fake input element with a specified name
            var form = document.createElement("div"),
                id = "script" + (new Date()).getTime(),
                root = document.documentElement;

            form.innerHTML = "<a name='" + id + "'/>";

            // Inject it into the root element, check its status, and remove it quickly
            root.insertBefore(form, root.firstChild);

            // The workaround has to do additional checks after a getElementById
            // Which slows things down for other browsers (hence the branching)
            if (document.getElementById(id)) {
                Expr.find.ID = function(match, context, isXML) {
                    if (typeof context.getElementById !== "undefined" && !isXML) {
                        var m = context.getElementById(match[1]);

                        return m ? m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ? [m] : undefined : [];
                    }
                };

                Expr.filter.ID = function(elem, match) {
                    var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");

                    return elem.nodeType === 1 && node && node.nodeValue === match;
                };
            }

            root.removeChild(form);

            // release memory in IE
            root = form = null;
        })();

        (function() {
            // Check to see if the browser returns only elements
            // when doing getElementsByTagName("*")

            // Create a fake element
            var div = document.createElement("div");
            div.appendChild(document.createComment(""));

            // Make sure no comments are found
            if (div.getElementsByTagName("*").length > 0) {
                Expr.find.TAG = function(match, context) {
                    var results = context.getElementsByTagName(match[1]);

                    // Filter out possible comments
                    if (match[1] === "*") {
                        var tmp = [];

                        for (var i = 0; results[i]; i++) {
                            if (results[i].nodeType === 1) {
                                tmp.push(results[i]);
                            }
                        }

                        results = tmp;
                    }

                    return results;
                };
            }

            // Check to see if an attribute returns normalized href attributes
            div.innerHTML = "<a href='#'></a>";

            if (div.firstChild && typeof div.firstChild.getAttribute !== "undefined" && div.firstChild.getAttribute("href") !== "#") {

                Expr.attrHandle.href = function(elem) {
                    return elem.getAttribute("href", 2);
                };
            }

            // release memory in IE
            div = null;
        })();

        if (document.querySelectorAll) {
            (function() {
                var oldSizzle = Sizzle,
                    div = document.createElement("div"),
                    id = "__sizzle__";

                div.innerHTML = "<p class='TEST'></p>";

                // Safari can't handle uppercase or unicode characters when
                // in quirks mode.
                if (div.querySelectorAll && div.querySelectorAll(".TEST").length === 0) {
                    return;
                }

                Sizzle = function(query, context, extra, seed) {
                    context = context || document;

                    // Only use querySelectorAll on non-XML documents
                    // (ID selectors don't work in non-HTML documents)
                    if (!seed && !Sizzle.isXML(context)) {
                        // See if we find a selector to speed up
                        var match = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(query);

                        if (match && (context.nodeType === 1 || context.nodeType === 9)) {
                            // Speed-up: Sizzle("TAG")
                            if (match[1]) {
                                return makeArray(context.getElementsByTagName(query), extra);

                                // Speed-up: Sizzle(".CLASS")
                            } else if (match[2] && Expr.find.CLASS && context.getElementsByClassName) {
                                return makeArray(context.getElementsByClassName(match[2]), extra);
                            }
                        }

                        if (context.nodeType === 9) {
                            // Speed-up: Sizzle("body")
                            // The body element only exists once, optimize finding it
                            if (query === "body" && context.body) {
                                return makeArray([context.body], extra);

                                // Speed-up: Sizzle("#ID")
                            } else if (match && match[3]) {
                                var elem = context.getElementById(match[3]);

                                // Check parentNode to catch when Blackberry 4.6 returns
                                // nodes that are no longer in the document #6963
                                if (elem && elem.parentNode) {
                                    // Handle the case where IE and Opera return items
                                    // by name instead of ID
                                    if (elem.id === match[3]) {
                                        return makeArray([elem], extra);
                                    }

                                } else {
                                    return makeArray([], extra);
                                }
                            }

                            try {
                                return makeArray(context.querySelectorAll(query), extra);
                            } catch (qsaError) {}

                            // qSA works strangely on Element-rooted queries
                            // We can work around this by specifying an extra ID on the root
                            // and working up from there (Thanks to Andrew Dupont for the technique)
                            // IE 8 doesn't work on object elements
                        } else if (context.nodeType === 1 && context.nodeName.toLowerCase() !== "object") {
                            var oldContext = context,
                                old = context.getAttribute("id"),
                                nid = old || id,
                                hasParent = context.parentNode,
                                relativeHierarchySelector = /^\s*[+~]/.test(query);

                            if (!old) {
                                context.setAttribute("id", nid);
                            } else {
                                nid = nid.replace(/'/g, "\\$&");
                            }
                            if (relativeHierarchySelector && hasParent) {
                                context = context.parentNode;
                            }

                            try {
                                if (!relativeHierarchySelector || hasParent) {
                                    return makeArray(context.querySelectorAll("[id='" + nid + "'] " + query), extra);
                                }

                            } catch (pseudoError) {} finally {
                                if (!old) {
                                    oldContext.removeAttribute("id");
                                }
                            }
                        }
                    }

                    return oldSizzle(query, context, extra, seed);
                };

                for (var prop in oldSizzle) {
                    Sizzle[prop] = oldSizzle[prop];
                }

                // release memory in IE
                div = null;
            })();
        }

        (function() {
            var html = document.documentElement,
                matches = html.matchesSelector || html.mozMatchesSelector || html.webkitMatchesSelector || html.msMatchesSelector;

            if (matches) {
                // Check to see if it's possible to do matchesSelector
                // on a disconnected node (IE 9 fails this)
                var disconnectedMatch = !matches.call(document.createElement("div"), "div"),
                    pseudoWorks = false;

                try {
                    // This should fail with an exception
                    // Gecko does not error, returns false instead
                    matches.call(document.documentElement, "[test!='']:sizzle");

                } catch (pseudoError) {
                    pseudoWorks = true;
                }

                Sizzle.matchesSelector = function(node, expr) {
                    // Make sure that attribute selectors are quoted
                    expr = expr.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");

                    if (!Sizzle.isXML(node)) {
                        try {
                            if (pseudoWorks || !Expr.match.PSEUDO.test(expr) && !/!=/.test(expr)) {
                                var ret = matches.call(node, expr);

                                // IE 9's matchesSelector returns false on disconnected nodes
                                if (ret || !disconnectedMatch ||
                                // As well, disconnected nodes are said to be in a document
                                // fragment in IE 9, so check for that
                                node.document && node.document.nodeType !== 11) {
                                    return ret;
                                }
                            }
                        } catch (e) {}
                    }

                    return Sizzle(expr, null, null, [node]).length > 0;
                };
            }
        })();

        (function() {
            var div = document.createElement("div");

            div.innerHTML = "<div class='test e'></div><div class='test'></div>";

            // Opera can't find a second classname (in 9.6)
            // Also, make sure that getElementsByClassName actually exists
            if (!div.getElementsByClassName || div.getElementsByClassName("e").length === 0) {
                return;
            }

            // Safari caches class attributes, doesn't catch changes (in 3.2)
            div.lastChild.className = "e";

            if (div.getElementsByClassName("e").length === 1) {
                return;
            }

            Expr.order.splice(1, 0, "CLASS");
            Expr.find.CLASS = function(match, context, isXML) {
                if (typeof context.getElementsByClassName !== "undefined" && !isXML) {
                    return context.getElementsByClassName(match[1]);
                }
            };

            // release memory in IE
            div = null;
        })();

        function dirNodeCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
            for (var i = 0, l = checkSet.length; i < l; i++) {
                var elem = checkSet[i];

                if (elem) {
                    var match = false;

                    elem = elem[dir];

                    while (elem) {
                        if (elem[expando] === doneName) {
                            match = checkSet[elem.sizset];
                            break;
                        }

                        if (elem.nodeType === 1 && !isXML) {
                            elem[expando] = doneName;
                            elem.sizset = i;
                        }

                        if (elem.nodeName.toLowerCase() === cur) {
                            match = elem;
                            break;
                        }

                        elem = elem[dir];
                    }

                    checkSet[i] = match;
                }
            }
        }

        function dirCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
            for (var i = 0, l = checkSet.length; i < l; i++) {
                var elem = checkSet[i];

                if (elem) {
                    var match = false;

                    elem = elem[dir];

                    while (elem) {
                        if (elem[expando] === doneName) {
                            match = checkSet[elem.sizset];
                            break;
                        }

                        if (elem.nodeType === 1) {
                            if (!isXML) {
                                elem[expando] = doneName;
                                elem.sizset = i;
                            }

                            if (typeof cur !== "string") {
                                if (elem === cur) {
                                    match = true;
                                    break;
                                }

                            } else if (Sizzle.filter(cur, [elem]).length > 0) {
                                match = elem;
                                break;
                            }
                        }

                        elem = elem[dir];
                    }

                    checkSet[i] = match;
                }
            }
        }

        if (document.documentElement.contains) {
            Sizzle.contains = function(a, b) {
                return a !== b && (a.contains ? a.contains(b) : true);
            };

        } else if (document.documentElement.compareDocumentPosition) {
            Sizzle.contains = function(a, b) {
                return !!(a.compareDocumentPosition(b) & 16);
            };

        } else {
            Sizzle.contains = function() {
                return false;
            };
        }

        Sizzle.isXML = function(elem) {
            // documentElement is verified for cases where it doesn't yet exist
            // (such as loading iframes in IE - #4833)
            var documentElement = (elem ? elem.ownerDocument || elem : 0).documentElement;

            return documentElement ? documentElement.nodeName !== "HTML" : false;
        };

        var posProcess = function(selector, context, seed) {
                var match, tmpSet = [],
                    later = "",
                    root = context.nodeType ? [context] : context;

                // Position selectors must be done after the filter
                // And so must :not(positional) so we move all PSEUDOs to the end
                while ((match = Expr.match.PSEUDO.exec(selector))) {
                    later += match[0];
                    selector = selector.replace(Expr.match.PSEUDO, "");
                }

                selector = Expr.relative[selector] ? selector + "*" : selector;

                for (var i = 0, l = root.length; i < l; i++) {
                    Sizzle(selector, root[i], tmpSet, seed);
                }

                return Sizzle.filter(later, tmpSet);
            };

        // EXPOSE
        // Override sizzle attribute retrieval
        Sizzle.attr = jQuery.attr;
        Sizzle.selectors.attrMap = {};
        jQuery.find = Sizzle;
        jQuery.expr = Sizzle.selectors;
        jQuery.expr[":"] = jQuery.expr.filters;
        jQuery.unique = Sizzle.uniqueSort;
        jQuery.text = Sizzle.getText;
        jQuery.isXMLDoc = Sizzle.isXML;
        jQuery.contains = Sizzle.contains;


    })();


    var runtil = /Until$/,
        rparentsprev = /^(?:parents|prevUntil|prevAll)/,
        // Note: This RegExp should be improved, or likely pulled from Sizzle
        rmultiselector = /,/,
        isSimple = /^.[^:#\[\.,]*$/,
        slice = Array.prototype.slice,
        POS = jQuery.expr.match.globalPOS,
        // methods guaranteed to produce a unique set when starting from a unique set
        guaranteedUnique = {
            children: true,
            contents: true,
            next: true,
            prev: true
        };

    jQuery.fn.extend({
        find: function(selector) {
            var self = this,
                i, l;

            if (typeof selector !== "string") {
                return jQuery(selector).filter(function() {
                    for (i = 0, l = self.length; i < l; i++) {
                        if (jQuery.contains(self[i], this)) {
                            return true;
                        }
                    }
                });
            }

            var ret = this.pushStack("", "find", selector),
                length, n, r;

            for (i = 0, l = this.length; i < l; i++) {
                length = ret.length;
                jQuery.find(selector, this[i], ret);

                if (i > 0) {
                    // Make sure that the results are unique
                    for (n = length; n < ret.length; n++) {
                        for (r = 0; r < length; r++) {
                            if (ret[r] === ret[n]) {
                                ret.splice(n--, 1);
                                break;
                            }
                        }
                    }
                }
            }

            return ret;
        },

        has: function(target) {
            var targets = jQuery(target);
            return this.filter(function() {
                for (var i = 0, l = targets.length; i < l; i++) {
                    if (jQuery.contains(this, targets[i])) {
                        return true;
                    }
                }
            });
        },

        not: function(selector) {
            return this.pushStack(winnow(this, selector, false), "not", selector);
        },

        filter: function(selector) {
            return this.pushStack(winnow(this, selector, true), "filter", selector);
        },

        is: function(selector) {
            return !!selector && (
            typeof selector === "string" ?
            // If this is a positional selector, check membership in the returned set
            // so $("p:first").is("p:last") won't return true for a doc with two "p".
            POS.test(selector) ? jQuery(selector, this.context).index(this[0]) >= 0 : jQuery.filter(selector, this).length > 0 : this.filter(selector).length > 0);
        },

        closest: function(selectors, context) {
            var ret = [],
                i, l, cur = this[0];

            // Array (deprecated as of jQuery 1.7)
            if (jQuery.isArray(selectors)) {
                var level = 1;

                while (cur && cur.ownerDocument && cur !== context) {
                    for (i = 0; i < selectors.length; i++) {

                        if (jQuery(cur).is(selectors[i])) {
                            ret.push({
                                selector: selectors[i],
                                elem: cur,
                                level: level
                            });
                        }
                    }

                    cur = cur.parentNode;
                    level++;
                }

                return ret;
            }

            // String
            var pos = POS.test(selectors) || typeof selectors !== "string" ? jQuery(selectors, context || this.context) : 0;

            for (i = 0, l = this.length; i < l; i++) {
                cur = this[i];

                while (cur) {
                    if (pos ? pos.index(cur) > -1 : jQuery.find.matchesSelector(cur, selectors)) {
                        ret.push(cur);
                        break;

                    } else {
                        cur = cur.parentNode;
                        if (!cur || !cur.ownerDocument || cur === context || cur.nodeType === 11) {
                            break;
                        }
                    }
                }
            }

            ret = ret.length > 1 ? jQuery.unique(ret) : ret;

            return this.pushStack(ret, "closest", selectors);
        },

        // Determine the position of an element within
        // the matched set of elements
        index: function(elem) {

            // No argument, return index in parent
            if (!elem) {
                return (this[0] && this[0].parentNode) ? this.prevAll().length : -1;
            }

            // index in selector
            if (typeof elem === "string") {
                return jQuery.inArray(this[0], jQuery(elem));
            }

            // Locate the position of the desired element
            return jQuery.inArray(
            // If it receives a jQuery object, the first element is used
            elem.jquery ? elem[0] : elem, this);
        },

        add: function(selector, context) {
            var set = typeof selector === "string" ? jQuery(selector, context) : jQuery.makeArray(selector && selector.nodeType ? [selector] : selector),
                all = jQuery.merge(this.get(), set);

            return this.pushStack(isDisconnected(set[0]) || isDisconnected(all[0]) ? all : jQuery.unique(all));
        },

        andSelf: function() {
            return this.add(this.prevObject);
        }
    });

    // A painfully simple check to see if an element is disconnected
    // from a document (should be improved, where feasible).


    function isDisconnected(node) {
        return !node || !node.parentNode || node.parentNode.nodeType === 11;
    }

    jQuery.each({
        parent: function(elem) {
            var parent = elem.parentNode;
            return parent && parent.nodeType !== 11 ? parent : null;
        },
        parents: function(elem) {
            return jQuery.dir(elem, "parentNode");
        },
        parentsUntil: function(elem, i, until) {
            return jQuery.dir(elem, "parentNode", until);
        },
        next: function(elem) {
            return jQuery.nth(elem, 2, "nextSibling");
        },
        prev: function(elem) {
            return jQuery.nth(elem, 2, "previousSibling");
        },
        nextAll: function(elem) {
            return jQuery.dir(elem, "nextSibling");
        },
        prevAll: function(elem) {
            return jQuery.dir(elem, "previousSibling");
        },
        nextUntil: function(elem, i, until) {
            return jQuery.dir(elem, "nextSibling", until);
        },
        prevUntil: function(elem, i, until) {
            return jQuery.dir(elem, "previousSibling", until);
        },
        siblings: function(elem) {
            return jQuery.sibling((elem.parentNode || {}).firstChild, elem);
        },
        children: function(elem) {
            return jQuery.sibling(elem.firstChild);
        },
        contents: function(elem) {
            return jQuery.nodeName(elem, "iframe") ? elem.contentDocument || elem.contentWindow.document : jQuery.makeArray(elem.childNodes);
        }
    }, function(name, fn) {
        jQuery.fn[name] = function(until, selector) {
            var ret = jQuery.map(this, fn, until);

            if (!runtil.test(name)) {
                selector = until;
            }

            if (selector && typeof selector === "string") {
                ret = jQuery.filter(selector, ret);
            }

            ret = this.length > 1 && !guaranteedUnique[name] ? jQuery.unique(ret) : ret;

            if ((this.length > 1 || rmultiselector.test(selector)) && rparentsprev.test(name)) {
                ret = ret.reverse();
            }

            return this.pushStack(ret, name, slice.call(arguments).join(","));
        };
    });

    jQuery.extend({
        filter: function(expr, elems, not) {
            if (not) {
                expr = ":not(" + expr + ")";
            }

            return elems.length === 1 ? jQuery.find.matchesSelector(elems[0], expr) ? [elems[0]] : [] : jQuery.find.matches(expr, elems);
        },

        dir: function(elem, dir, until) {
            var matched = [],
                cur = elem[dir];

            while (cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery(cur).is(until))) {
                if (cur.nodeType === 1) {
                    matched.push(cur);
                }
                cur = cur[dir];
            }
            return matched;
        },

        nth: function(cur, result, dir, elem) {
            result = result || 1;
            var num = 0;

            for (; cur; cur = cur[dir]) {
                if (cur.nodeType === 1 && ++num === result) {
                    break;
                }
            }

            return cur;
        },

        sibling: function(n, elem) {
            var r = [];

            for (; n; n = n.nextSibling) {
                if (n.nodeType === 1 && n !== elem) {
                    r.push(n);
                }
            }

            return r;
        }
    });

    // Implement the identical functionality for filter and not


    function winnow(elements, qualifier, keep) {

        // Can't pass null or undefined to indexOf in Firefox 4
        // Set to 0 to skip string check
        qualifier = qualifier || 0;

        if (jQuery.isFunction(qualifier)) {
            return jQuery.grep(elements, function(elem, i) {
                var retVal = !! qualifier.call(elem, i, elem);
                return retVal === keep;
            });

        } else if (qualifier.nodeType) {
            return jQuery.grep(elements, function(elem, i) {
                return (elem === qualifier) === keep;
            });

        } else if (typeof qualifier === "string") {
            var filtered = jQuery.grep(elements, function(elem) {
                return elem.nodeType === 1;
            });

            if (isSimple.test(qualifier)) {
                return jQuery.filter(qualifier, filtered, !keep);
            } else {
                qualifier = jQuery.filter(qualifier, filtered);
            }
        }

        return jQuery.grep(elements, function(elem, i) {
            return (jQuery.inArray(elem, qualifier) >= 0) === keep;
        });
    }




    function createSafeFragment(document) {
        var list = nodeNames.split("|"),
            safeFrag = document.createDocumentFragment();

        if (safeFrag.createElement) {
            while (list.length) {
                safeFrag.createElement(
                list.pop());
            }
        }
        return safeFrag;
    }

    var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" + "header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        rinlinejQuery = / jQuery\d+="(?:\d+|null)"/g,
        rleadingWhitespace = /^\s+/,
        rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,
        rtagName = /<([\w:]+)/,
        rtbody = /<tbody/i,
        rhtml = /<|&#?\w+;/,
        rnoInnerhtml = /<(?:script|style)/i,
        rnocache = /<(?:script|object|embed|option|style)/i,
        rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
        // checked="checked" or checked
        rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
        rscriptType = /\/(java|ecma)script/i,
        rcleanScript = /^\s*<!(?:\[CDATA\[|\-\-)/,
        wrapMap = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            area: [1, "<map>", "</map>"],
            _default: [0, "", ""]
        },
        safeFragment = createSafeFragment(document);

    wrapMap.optgroup = wrapMap.option;
    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
    wrapMap.th = wrapMap.td;

    // IE can't serialize <link> and <script> tags normally
    if (!jQuery.support.htmlSerialize) {
        wrapMap._default = [1, "div<div>", "</div>"];
    }

    jQuery.fn.extend({
        text: function(value) {
            return jQuery.access(this, function(value) {
                return value === undefined ? jQuery.text(this) : this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(value));
            }, null, value, arguments.length);
        },

        wrapAll: function(html) {
            if (jQuery.isFunction(html)) {
                return this.each(function(i) {
                    jQuery(this).wrapAll(html.call(this, i));
                });
            }

            if (this[0]) {
                // The elements to wrap the target around
                var wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);

                if (this[0].parentNode) {
                    wrap.insertBefore(this[0]);
                }

                wrap.map(function() {
                    var elem = this;

                    while (elem.firstChild && elem.firstChild.nodeType === 1) {
                        elem = elem.firstChild;
                    }

                    return elem;
                }).append(this);
            }

            return this;
        },

        wrapInner: function(html) {
            if (jQuery.isFunction(html)) {
                return this.each(function(i) {
                    jQuery(this).wrapInner(html.call(this, i));
                });
            }

            return this.each(function() {
                var self = jQuery(this),
                    contents = self.contents();

                if (contents.length) {
                    contents.wrapAll(html);

                } else {
                    self.append(html);
                }
            });
        },

        wrap: function(html) {
            var isFunction = jQuery.isFunction(html);

            return this.each(function(i) {
                jQuery(this).wrapAll(isFunction ? html.call(this, i) : html);
            });
        },

        unwrap: function() {
            return this.parent().each(function() {
                if (!jQuery.nodeName(this, "body")) {
                    jQuery(this).replaceWith(this.childNodes);
                }
            }).end();
        },

        append: function() {
            return this.domManip(arguments, true, function(elem) {
                if (this.nodeType === 1) {
                    this.appendChild(elem);
                }
            });
        },

        prepend: function() {
            return this.domManip(arguments, true, function(elem) {
                if (this.nodeType === 1) {
                    this.insertBefore(elem, this.firstChild);
                }
            });
        },

        before: function() {
            if (this[0] && this[0].parentNode) {
                return this.domManip(arguments, false, function(elem) {
                    this.parentNode.insertBefore(elem, this);
                });
            } else if (arguments.length) {
                var set = jQuery.clean(arguments);
                set.push.apply(set, this.toArray());
                return this.pushStack(set, "before", arguments);
            }
        },

        after: function() {
            if (this[0] && this[0].parentNode) {
                return this.domManip(arguments, false, function(elem) {
                    this.parentNode.insertBefore(elem, this.nextSibling);
                });
            } else if (arguments.length) {
                var set = this.pushStack(this, "after", arguments);
                set.push.apply(set, jQuery.clean(arguments));
                return set;
            }
        },

        // keepData is for internal use only--do not document
        remove: function(selector, keepData) {
            for (var i = 0, elem;
            (elem = this[i]) != null; i++) {
                if (!selector || jQuery.filter(selector, [elem]).length) {
                    if (!keepData && elem.nodeType === 1) {
                        jQuery.cleanData(elem.getElementsByTagName("*"));
                        jQuery.cleanData([elem]);
                    }

                    if (elem.parentNode) {
                        elem.parentNode.removeChild(elem);
                    }
                }
            }

            return this;
        },

        empty: function() {
            for (var i = 0, elem;
            (elem = this[i]) != null; i++) {
                // Remove element nodes and prevent memory leaks
                if (elem.nodeType === 1) {
                    jQuery.cleanData(elem.getElementsByTagName("*"));
                }

                // Remove any remaining nodes
                while (elem.firstChild) {
                    elem.removeChild(elem.firstChild);
                }
            }

            return this;
        },

        clone: function(dataAndEvents, deepDataAndEvents) {
            dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
            deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

            return this.map(function() {
                return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
            });
        },

        html: function(value) {
            return jQuery.access(this, function(value) {
                var elem = this[0] || {},
                    i = 0,
                    l = this.length;

                if (value === undefined) {
                    return elem.nodeType === 1 ? elem.innerHTML.replace(rinlinejQuery, "") : null;
                }


                if (typeof value === "string" && !rnoInnerhtml.test(value) && (jQuery.support.leadingWhitespace || !rleadingWhitespace.test(value)) && !wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()]) {

                    value = value.replace(rxhtmlTag, "<$1></$2>");

                    try {
                        for (; i < l; i++) {
                            // Remove element nodes and prevent memory leaks
                            elem = this[i] || {};
                            if (elem.nodeType === 1) {
                                jQuery.cleanData(elem.getElementsByTagName("*"));
                                elem.innerHTML = value;
                            }
                        }

                        elem = 0;

                        // If using innerHTML throws an exception, use the fallback method
                    } catch (e) {}
                }

                if (elem) {
                    this.empty().append(value);
                }
            }, null, value, arguments.length);
        },

        replaceWith: function(value) {
            if (this[0] && this[0].parentNode) {
                // Make sure that the elements are removed from the DOM before they are inserted
                // this can help fix replacing a parent with child elements
                if (jQuery.isFunction(value)) {
                    return this.each(function(i) {
                        var self = jQuery(this),
                            old = self.html();
                        self.replaceWith(value.call(this, i, old));
                    });
                }

                if (typeof value !== "string") {
                    value = jQuery(value).detach();
                }

                return this.each(function() {
                    var next = this.nextSibling,
                        parent = this.parentNode;

                    jQuery(this).remove();

                    if (next) {
                        jQuery(next).before(value);
                    } else {
                        jQuery(parent).append(value);
                    }
                });
            } else {
                return this.length ? this.pushStack(jQuery(jQuery.isFunction(value) ? value() : value), "replaceWith", value) : this;
            }
        },

        detach: function(selector) {
            return this.remove(selector, true);
        },

        domManip: function(args, table, callback) {
            var results, first, fragment, parent, value = args[0],
                scripts = [];

            // We can't cloneNode fragments that contain checked, in WebKit
            if (!jQuery.support.checkClone && arguments.length === 3 && typeof value === "string" && rchecked.test(value)) {
                return this.each(function() {
                    jQuery(this).domManip(args, table, callback, true);
                });
            }

            if (jQuery.isFunction(value)) {
                return this.each(function(i) {
                    var self = jQuery(this);
                    args[0] = value.call(this, i, table ? self.html() : undefined);
                    self.domManip(args, table, callback);
                });
            }

            if (this[0]) {
                parent = value && value.parentNode;

                // If we're in a fragment, just use that instead of building a new one
                if (jQuery.support.parentNode && parent && parent.nodeType === 11 && parent.childNodes.length === this.length) {
                    results = {
                        fragment: parent
                    };

                } else {
                    results = jQuery.buildFragment(args, this, scripts);
                }

                fragment = results.fragment;

                if (fragment.childNodes.length === 1) {
                    first = fragment = fragment.firstChild;
                } else {
                    first = fragment.firstChild;
                }

                if (first) {
                    table = table && jQuery.nodeName(first, "tr");

                    for (var i = 0, l = this.length, lastIndex = l - 1; i < l; i++) {
                        callback.call(
                        table ? root(this[i], first) : this[i],
                        // Make sure that we do not leak memory by inadvertently discarding
                        // the original fragment (which might have attached data) instead of
                        // using it; in addition, use the original fragment object for the last
                        // item instead of first because it can end up being emptied incorrectly
                        // in certain situations (Bug #8070).
                        // Fragments from the fragment cache must always be cloned and never used
                        // in place.
                        results.cacheable || (l > 1 && i < lastIndex) ? jQuery.clone(fragment, true, true) : fragment);
                    }
                }

                if (scripts.length) {
                    jQuery.each(scripts, function(i, elem) {
                        if (elem.src) {
                            jQuery.ajax({
                                type: "GET",
                                global: false,
                                url: elem.src,
                                async: false,
                                dataType: "script"
                            });
                        } else {
                            jQuery.globalEval((elem.text || elem.textContent || elem.innerHTML || "").replace(rcleanScript, "/*$0*/"));
                        }

                        if (elem.parentNode) {
                            elem.parentNode.removeChild(elem);
                        }
                    });
                }
            }

            return this;
        }
    });

    function root(elem, cur) {
        return jQuery.nodeName(elem, "table") ? (elem.getElementsByTagName("tbody")[0] || elem.appendChild(elem.ownerDocument.createElement("tbody"))) : elem;
    }

    function cloneCopyEvent(src, dest) {

        if (dest.nodeType !== 1 || !jQuery.hasData(src)) {
            return;
        }

        var type, i, l, oldData = jQuery._data(src),
            curData = jQuery._data(dest, oldData),
            events = oldData.events;

        if (events) {
            delete curData.handle;
            curData.events = {};

            for (type in events) {
                for (i = 0, l = events[type].length; i < l; i++) {
                    jQuery.event.add(dest, type, events[type][i]);
                }
            }
        }

        // make the cloned public data object a copy from the original
        if (curData.data) {
            curData.data = jQuery.extend({}, curData.data);
        }
    }

    function cloneFixAttributes(src, dest) {
        var nodeName;

        // We do not need to do anything for non-Elements
        if (dest.nodeType !== 1) {
            return;
        }

        // clearAttributes removes the attributes, which we don't want,
        // but also removes the attachEvent events, which we *do* want
        if (dest.clearAttributes) {
            dest.clearAttributes();
        }

        // mergeAttributes, in contrast, only merges back on the
        // original attributes, not the events
        if (dest.mergeAttributes) {
            dest.mergeAttributes(src);
        }

        nodeName = dest.nodeName.toLowerCase();

        // IE6-8 fail to clone children inside object elements that use
        // the proprietary classid attribute value (rather than the type
        // attribute) to identify the type of content to display
        if (nodeName === "object") {
            dest.outerHTML = src.outerHTML;

        } else if (nodeName === "input" && (src.type === "checkbox" || src.type === "radio")) {
            // IE6-8 fails to persist the checked state of a cloned checkbox
            // or radio button. Worse, IE6-7 fail to give the cloned element
            // a checked appearance if the defaultChecked value isn't also set
            if (src.checked) {
                dest.defaultChecked = dest.checked = src.checked;
            }

            // IE6-7 get confused and end up setting the value of a cloned
            // checkbox/radio button to an empty string instead of "on"
            if (dest.value !== src.value) {
                dest.value = src.value;
            }

            // IE6-8 fails to return the selected option to the default selected
            // state when cloning options
        } else if (nodeName === "option") {
            dest.selected = src.defaultSelected;

            // IE6-8 fails to set the defaultValue to the correct value when
            // cloning other types of input fields
        } else if (nodeName === "input" || nodeName === "textarea") {
            dest.defaultValue = src.defaultValue;

            // IE blanks contents when cloning scripts
        } else if (nodeName === "script" && dest.text !== src.text) {
            dest.text = src.text;
        }

        // Event data gets referenced instead of copied if the expando
        // gets copied too
        dest.removeAttribute(jQuery.expando);

        // Clear flags for bubbling special change/submit events, they must
        // be reattached when the newly cloned events are first activated
        dest.removeAttribute("_submit_attached");
        dest.removeAttribute("_change_attached");
    }

    jQuery.buildFragment = function(args, nodes, scripts) {
        var fragment, cacheable, cacheresults, doc, first = args[0];

        // nodes may contain either an explicit document object,
        // a jQuery collection or context object.
        // If nodes[0] contains a valid object to assign to doc
        if (nodes && nodes[0]) {
            doc = nodes[0].ownerDocument || nodes[0];
        }

        // Ensure that an attr object doesn't incorrectly stand in as a document object
        // Chrome and Firefox seem to allow this to occur and will throw exception
        // Fixes #8950
        if (!doc.createDocumentFragment) {
            doc = document;
        }

        // Only cache "small" (1/2 KB) HTML strings that are associated with the main document
        // Cloning options loses the selected state, so don't cache them
        // IE 6 doesn't like it when you put <object> or <embed> elements in a fragment
        // Also, WebKit does not clone 'checked' attributes on cloneNode, so don't cache
        // Lastly, IE6,7,8 will not correctly reuse cached fragments that were created from unknown elems #10501
        if (args.length === 1 && typeof first === "string" && first.length < 512 && doc === document && first.charAt(0) === "<" && !rnocache.test(first) && (jQuery.support.checkClone || !rchecked.test(first)) && (jQuery.support.html5Clone || !rnoshimcache.test(first))) {

            cacheable = true;

            cacheresults = jQuery.fragments[first];
            if (cacheresults && cacheresults !== 1) {
                fragment = cacheresults;
            }
        }

        if (!fragment) {
            fragment = doc.createDocumentFragment();
            jQuery.clean(args, doc, fragment, scripts);
        }

        if (cacheable) {
            jQuery.fragments[first] = cacheresults ? fragment : 1;
        }

        return {
            fragment: fragment,
            cacheable: cacheable
        };
    };

    jQuery.fragments = {};

    jQuery.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(name, original) {
        jQuery.fn[name] = function(selector) {
            var ret = [],
                insert = jQuery(selector),
                parent = this.length === 1 && this[0].parentNode;

            if (parent && parent.nodeType === 11 && parent.childNodes.length === 1 && insert.length === 1) {
                insert[original](this[0]);
                return this;

            } else {
                for (var i = 0, l = insert.length; i < l; i++) {
                    var elems = (i > 0 ? this.clone(true) : this).get();
                    jQuery(insert[i])[original](elems);
                    ret = ret.concat(elems);
                }

                return this.pushStack(ret, name, insert.selector);
            }
        };
    });

    function getAll(elem) {
        if (typeof elem.getElementsByTagName !== "undefined") {
            return elem.getElementsByTagName("*");

        } else if (typeof elem.querySelectorAll !== "undefined") {
            return elem.querySelectorAll("*");

        } else {
            return [];
        }
    }

    // Used in clean, fixes the defaultChecked property


    function fixDefaultChecked(elem) {
        if (elem.type === "checkbox" || elem.type === "radio") {
            elem.defaultChecked = elem.checked;
        }
    }
    // Finds all inputs and passes them to fixDefaultChecked


    function findInputs(elem) {
        var nodeName = (elem.nodeName || "").toLowerCase();
        if (nodeName === "input") {
            fixDefaultChecked(elem);
            // Skip scripts, get other children
        } else if (nodeName !== "script" && typeof elem.getElementsByTagName !== "undefined") {
            jQuery.grep(elem.getElementsByTagName("input"), fixDefaultChecked);
        }
    }

    // Derived From: http://www.iecss.com/shimprove/javascript/shimprove.1-0-1.js


    function shimCloneNode(elem) {
        var div = document.createElement("div");
        safeFragment.appendChild(div);

        div.innerHTML = elem.outerHTML;
        return div.firstChild;
    }

    jQuery.extend({
        clone: function(elem, dataAndEvents, deepDataAndEvents) {
            var srcElements, destElements, i,
            // IE<=8 does not properly clone detached, unknown element nodes
            clone = jQuery.support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test("<" + elem.nodeName + ">") ? elem.cloneNode(true) : shimCloneNode(elem);

            if ((!jQuery.support.noCloneEvent || !jQuery.support.noCloneChecked) && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
                // IE copies events bound via attachEvent when using cloneNode.
                // Calling detachEvent on the clone will also remove the events
                // from the original. In order to get around this, we use some
                // proprietary methods to clear the events. Thanks to MooTools
                // guys for this hotness.
                cloneFixAttributes(elem, clone);

                // Using Sizzle here is crazy slow, so we use getElementsByTagName instead
                srcElements = getAll(elem);
                destElements = getAll(clone);

                // Weird iteration because IE will replace the length property
                // with an element if you are cloning the body and one of the
                // elements on the page has a name or id of "length"
                for (i = 0; srcElements[i]; ++i) {
                    // Ensure that the destination node is not null; Fixes #9587
                    if (destElements[i]) {
                        cloneFixAttributes(srcElements[i], destElements[i]);
                    }
                }
            }

            // Copy the events from the original to the clone
            if (dataAndEvents) {
                cloneCopyEvent(elem, clone);

                if (deepDataAndEvents) {
                    srcElements = getAll(elem);
                    destElements = getAll(clone);

                    for (i = 0; srcElements[i]; ++i) {
                        cloneCopyEvent(srcElements[i], destElements[i]);
                    }
                }
            }

            srcElements = destElements = null;

            // Return the cloned set
            return clone;
        },

        clean: function(elems, context, fragment, scripts) {
            var checkScriptType, script, j, ret = [];

            context = context || document;

            // !context.createElement fails in IE with an error but returns typeof 'object'
            if (typeof context.createElement === "undefined") {
                context = context.ownerDocument || context[0] && context[0].ownerDocument || document;
            }

            for (var i = 0, elem;
            (elem = elems[i]) != null; i++) {
                if (typeof elem === "number") {
                    elem += "";
                }

                if (!elem) {
                    continue;
                }

                // Convert html string into DOM nodes
                if (typeof elem === "string") {
                    if (!rhtml.test(elem)) {
                        elem = context.createTextNode(elem);
                    } else {
                        // Fix "XHTML"-style tags in all browsers
                        elem = elem.replace(rxhtmlTag, "<$1></$2>");

                        // Trim whitespace, otherwise indexOf won't work as expected
                        var tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase(),
                            wrap = wrapMap[tag] || wrapMap._default,
                            depth = wrap[0],
                            div = context.createElement("div"),
                            safeChildNodes = safeFragment.childNodes,
                            remove;

                        // Append wrapper element to unknown element safe doc fragment
                        if (context === document) {
                            // Use the fragment we've already created for this document
                            safeFragment.appendChild(div);
                        } else {
                            // Use a fragment created with the owner document
                            createSafeFragment(context).appendChild(div);
                        }

                        // Go to html and back, then peel off extra wrappers
                        div.innerHTML = wrap[1] + elem + wrap[2];

                        // Move to the right depth
                        while (depth--) {
                            div = div.lastChild;
                        }

                        // Remove IE's autoinserted <tbody> from table fragments
                        if (!jQuery.support.tbody) {

                            // String was a <table>, *may* have spurious <tbody>
                            var hasBody = rtbody.test(elem),
                                tbody = tag === "table" && !hasBody ? div.firstChild && div.firstChild.childNodes :

                                // String was a bare <thead> or <tfoot>
                                wrap[1] === "<table>" && !hasBody ? div.childNodes : [];

                            for (j = tbody.length - 1; j >= 0; --j) {
                                if (jQuery.nodeName(tbody[j], "tbody") && !tbody[j].childNodes.length) {
                                    tbody[j].parentNode.removeChild(tbody[j]);
                                }
                            }
                        }

                        // IE completely kills leading whitespace when innerHTML is used
                        if (!jQuery.support.leadingWhitespace && rleadingWhitespace.test(elem)) {
                            div.insertBefore(context.createTextNode(rleadingWhitespace.exec(elem)[0]), div.firstChild);
                        }

                        elem = div.childNodes;

                        // Clear elements from DocumentFragment (safeFragment or otherwise)
                        // to avoid hoarding elements. Fixes #11356
                        if (div) {
                            div.parentNode.removeChild(div);

                            // Guard against -1 index exceptions in FF3.6
                            if (safeChildNodes.length > 0) {
                                remove = safeChildNodes[safeChildNodes.length - 1];

                                if (remove && remove.parentNode) {
                                    remove.parentNode.removeChild(remove);
                                }
                            }
                        }
                    }
                }

                // Resets defaultChecked for any radios and checkboxes
                // about to be appended to the DOM in IE 6/7 (#8060)
                var len;
                if (!jQuery.support.appendChecked) {
                    if (elem[0] && typeof(len = elem.length) === "number") {
                        for (j = 0; j < len; j++) {
                            findInputs(elem[j]);
                        }
                    } else {
                        findInputs(elem);
                    }
                }

                if (elem.nodeType) {
                    ret.push(elem);
                } else {
                    ret = jQuery.merge(ret, elem);
                }
            }

            if (fragment) {
                checkScriptType = function(elem) {
                    return !elem.type || rscriptType.test(elem.type);
                };
                for (i = 0; ret[i]; i++) {
                    script = ret[i];
                    if (scripts && jQuery.nodeName(script, "script") && (!script.type || rscriptType.test(script.type))) {
                        scripts.push(script.parentNode ? script.parentNode.removeChild(script) : script);

                    } else {
                        if (script.nodeType === 1) {
                            var jsTags = jQuery.grep(script.getElementsByTagName("script"), checkScriptType);

                            ret.splice.apply(ret, [i + 1, 0].concat(jsTags));
                        }
                        fragment.appendChild(script);
                    }
                }
            }

            return ret;
        },

        cleanData: function(elems) {
            var data, id, cache = jQuery.cache,
                special = jQuery.event.special,
                deleteExpando = jQuery.support.deleteExpando;

            for (var i = 0, elem;
            (elem = elems[i]) != null; i++) {
                if (elem.nodeName && jQuery.noData[elem.nodeName.toLowerCase()]) {
                    continue;
                }

                id = elem[jQuery.expando];

                if (id) {
                    data = cache[id];

                    if (data && data.events) {
                        for (var type in data.events) {
                            if (special[type]) {
                                jQuery.event.remove(elem, type);

                                // This is a shortcut to avoid jQuery.event.remove's overhead
                            } else {
                                jQuery.removeEvent(elem, type, data.handle);
                            }
                        }

                        // Null the DOM reference to avoid IE6/7/8 leak (#7054)
                        if (data.handle) {
                            data.handle.elem = null;
                        }
                    }

                    if (deleteExpando) {
                        delete elem[jQuery.expando];

                    } else if (elem.removeAttribute) {
                        elem.removeAttribute(jQuery.expando);
                    }

                    delete cache[id];
                }
            }
        }
    });




    var ralpha = /alpha\([^)]*\)/i,
        ropacity = /opacity=([^)]*)/,
        // fixed for IE9, see #8346
        rupper = /([A-Z]|^ms)/g,
        rnum = /^[\-+]?(?:\d*\.)?\d+$/i,
        rnumnonpx = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i,
        rrelNum = /^([\-+])=([\-+.\de]+)/,
        rmargin = /^margin/,

        cssShow = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },

        // order is important!
        cssExpand = ["Top", "Right", "Bottom", "Left"],

        curCSS,

        getComputedStyle, currentStyle;

    jQuery.fn.css = function(name, value) {
        return jQuery.access(this, function(elem, name, value) {
            return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
        }, name, value, arguments.length > 1);
    };

    jQuery.extend({
        // Add in style property hooks for overriding the default
        // behavior of getting and setting a style property
        cssHooks: {
            opacity: {
                get: function(elem, computed) {
                    if (computed) {
                        // We should always get a number back from opacity
                        var ret = curCSS(elem, "opacity");
                        return ret === "" ? "1" : ret;

                    } else {
                        return elem.style.opacity;
                    }
                }
            }
        },

        // Exclude the following css properties to add px
        cssNumber: {
            "fillOpacity": true,
            "fontWeight": true,
            "lineHeight": true,
            "opacity": true,
            "orphans": true,
            "widows": true,
            "zIndex": true,
            "zoom": true
        },

        // Add in properties whose names you wish to fix before
        // setting or getting the value
        cssProps: {
            // normalize float css property
            "float": jQuery.support.cssFloat ? "cssFloat" : "styleFloat"
        },

        // Get and set the style property on a DOM Node
        style: function(elem, name, value, extra) {
            // Don't set styles on text and comment nodes
            if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
                return;
            }

            // Make sure that we're working with the right name
            var ret, type, origName = jQuery.camelCase(name),
                style = elem.style,
                hooks = jQuery.cssHooks[origName];

            name = jQuery.cssProps[origName] || origName;

            // Check if we're setting a value
            if (value !== undefined) {
                type = typeof value;

                // convert relative number strings (+= or -=) to relative numbers. #7345
                if (type === "string" && (ret = rrelNum.exec(value))) {
                    value = (+(ret[1] + 1) * +ret[2]) + parseFloat(jQuery.css(elem, name));
                    // Fixes bug #9237
                    type = "number";
                }

                // Make sure that NaN and null values aren't set. See: #7116
                if (value == null || type === "number" && isNaN(value)) {
                    return;
                }

                // If a number was passed in, add 'px' to the (except for certain CSS properties)
                if (type === "number" && !jQuery.cssNumber[origName]) {
                    value += "px";
                }

                // If a hook was provided, use that value, otherwise just set the specified value
                if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value)) !== undefined) {
                    // Wrapped to prevent IE from throwing errors when 'invalid' values are provided
                    // Fixes bug #5509
                    try {
                        style[name] = value;
                    } catch (e) {}
                }

            } else {
                // If a hook was provided get the non-computed value from there
                if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
                    return ret;
                }

                // Otherwise just get the value from the style object
                return style[name];
            }
        },

        css: function(elem, name, extra) {
            var ret, hooks;

            // Make sure that we're working with the right name
            name = jQuery.camelCase(name);
            hooks = jQuery.cssHooks[name];
            name = jQuery.cssProps[name] || name;

            // cssFloat needs a special treatment
            if (name === "cssFloat") {
                name = "float";
            }

            // If a hook was provided get the computed value from there
            if (hooks && "get" in hooks && (ret = hooks.get(elem, true, extra)) !== undefined) {
                return ret;

                // Otherwise, if a way to get the computed value exists, use that
            } else if (curCSS) {
                return curCSS(elem, name);
            }
        },

        // A method for quickly swapping in/out CSS properties to get correct calculations
        swap: function(elem, options, callback) {
            var old = {},
                ret, name;

            // Remember the old values, and insert the new ones
            for (name in options) {
                old[name] = elem.style[name];
                elem.style[name] = options[name];
            }

            ret = callback.call(elem);

            // Revert the old values
            for (name in options) {
                elem.style[name] = old[name];
            }

            return ret;
        }
    });

    // DEPRECATED in 1.3, Use jQuery.css() instead
    jQuery.curCSS = jQuery.css;

    if (document.defaultView && document.defaultView.getComputedStyle) {
        getComputedStyle = function(elem, name) {
            var ret, defaultView, computedStyle, width, style = elem.style;

            name = name.replace(rupper, "-$1").toLowerCase();

            if ((defaultView = elem.ownerDocument.defaultView) && (computedStyle = defaultView.getComputedStyle(elem, null))) {

                ret = computedStyle.getPropertyValue(name);
                if (ret === "" && !jQuery.contains(elem.ownerDocument.documentElement, elem)) {
                    ret = jQuery.style(elem, name);
                }
            }

            // A tribute to the "awesome hack by Dean Edwards"
            // WebKit uses "computed value (percentage if specified)" instead of "used value" for margins
            // which is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
            if (!jQuery.support.pixelMargin && computedStyle && rmargin.test(name) && rnumnonpx.test(ret)) {
                width = style.width;
                style.width = ret;
                ret = computedStyle.width;
                style.width = width;
            }

            return ret;
        };
    }

    if (document.documentElement.currentStyle) {
        currentStyle = function(elem, name) {
            var left, rsLeft, uncomputed, ret = elem.currentStyle && elem.currentStyle[name],
                style = elem.style;

            // Avoid setting ret to empty string here
            // so we don't default to auto
            if (ret == null && style && (uncomputed = style[name])) {
                ret = uncomputed;
            }

            // From the awesome hack by Dean Edwards
            // http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

            // If we're not dealing with a regular pixel number
            // but a number that has a weird ending, we need to convert it to pixels
            if (rnumnonpx.test(ret)) {

                // Remember the original values
                left = style.left;
                rsLeft = elem.runtimeStyle && elem.runtimeStyle.left;

                // Put in the new values to get a computed value out
                if (rsLeft) {
                    elem.runtimeStyle.left = elem.currentStyle.left;
                }
                style.left = name === "fontSize" ? "1em" : ret;
                ret = style.pixelLeft + "px";

                // Revert the changed values
                style.left = left;
                if (rsLeft) {
                    elem.runtimeStyle.left = rsLeft;
                }
            }

            return ret === "" ? "auto" : ret;
        };
    }

    curCSS = getComputedStyle || currentStyle;

    function getWidthOrHeight(elem, name, extra) {

        // Start with offset property
        var val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
            i = name === "width" ? 1 : 0,
            len = 4;

        if (val > 0) {
            if (extra !== "border") {
                for (; i < len; i += 2) {
                    if (!extra) {
                        val -= parseFloat(jQuery.css(elem, "padding" + cssExpand[i])) || 0;
                    }
                    if (extra === "margin") {
                        val += parseFloat(jQuery.css(elem, extra + cssExpand[i])) || 0;
                    } else {
                        val -= parseFloat(jQuery.css(elem, "border" + cssExpand[i] + "Width")) || 0;
                    }
                }
            }

            return val + "px";
        }

        // Fall back to computed then uncomputed css if necessary
        val = curCSS(elem, name);
        if (val < 0 || val == null) {
            val = elem.style[name];
        }

        // Computed unit is not pixels. Stop here and return.
        if (rnumnonpx.test(val)) {
            return val;
        }

        // Normalize "", auto, and prepare for extra
        val = parseFloat(val) || 0;

        // Add padding, border, margin
        if (extra) {
            for (; i < len; i += 2) {
                val += parseFloat(jQuery.css(elem, "padding" + cssExpand[i])) || 0;
                if (extra !== "padding") {
                    val += parseFloat(jQuery.css(elem, "border" + cssExpand[i] + "Width")) || 0;
                }
                if (extra === "margin") {
                    val += parseFloat(jQuery.css(elem, extra + cssExpand[i])) || 0;
                }
            }
        }

        return val + "px";
    }

    jQuery.each(["height", "width"], function(i, name) {
        jQuery.cssHooks[name] = {
            get: function(elem, computed, extra) {
                if (computed) {
                    if (elem.offsetWidth !== 0) {
                        return getWidthOrHeight(elem, name, extra);
                    } else {
                        return jQuery.swap(elem, cssShow, function() {
                            return getWidthOrHeight(elem, name, extra);
                        });
                    }
                }
            },

            set: function(elem, value) {
                return rnum.test(value) ? value + "px" : value;
            }
        };
    });

    if (!jQuery.support.opacity) {
        jQuery.cssHooks.opacity = {
            get: function(elem, computed) {
                // IE uses filters for opacity
                return ropacity.test((computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "") ? (parseFloat(RegExp.$1) / 100) + "" : computed ? "1" : "";
            },

            set: function(elem, value) {
                var style = elem.style,
                    currentStyle = elem.currentStyle,
                    opacity = jQuery.isNumeric(value) ? "alpha(opacity=" + value * 100 + ")" : "",
                    filter = currentStyle && currentStyle.filter || style.filter || "";

                // IE has trouble with opacity if it does not have layout
                // Force it by setting the zoom level
                style.zoom = 1;

                // if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
                if (value >= 1 && jQuery.trim(filter.replace(ralpha, "")) === "") {

                    // Setting style.filter to null, "" & " " still leave "filter:" in the cssText
                    // if "filter:" is present at all, clearType is disabled, we want to avoid this
                    // style.removeAttribute is IE Only, but so apparently is this code path...
                    style.removeAttribute("filter");

                    // if there there is no filter style applied in a css rule, we are done
                    if (currentStyle && !currentStyle.filter) {
                        return;
                    }
                }

                // otherwise, set new filter values
                style.filter = ralpha.test(filter) ? filter.replace(ralpha, opacity) : filter + " " + opacity;
            }
        };
    }

    jQuery(function() {
        // This hook cannot be added until DOM ready because the support test
        // for it is not run until after DOM ready
        if (!jQuery.support.reliableMarginRight) {
            jQuery.cssHooks.marginRight = {
                get: function(elem, computed) {
                    // WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
                    // Work around by temporarily setting element display to inline-block
                    return jQuery.swap(elem, {
                        "display": "inline-block"
                    }, function() {
                        if (computed) {
                            return curCSS(elem, "margin-right");
                        } else {
                            return elem.style.marginRight;
                        }
                    });
                }
            };
        }
    });

    if (jQuery.expr && jQuery.expr.filters) {
        jQuery.expr.filters.hidden = function(elem) {
            var width = elem.offsetWidth,
                height = elem.offsetHeight;

            return (width === 0 && height === 0) || (!jQuery.support.reliableHiddenOffsets && ((elem.style && elem.style.display) || jQuery.css(elem, "display")) === "none");
        };

        jQuery.expr.filters.visible = function(elem) {
            return !jQuery.expr.filters.hidden(elem);
        };
    }

    // These hooks are used by animate to expand properties
    jQuery.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(prefix, suffix) {

        jQuery.cssHooks[prefix + suffix] = {
            expand: function(value) {
                var i,

                // assumes a single number if not a string
                parts = typeof value === "string" ? value.split(" ") : [value],
                    expanded = {};

                for (i = 0; i < 4; i++) {
                    expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                }

                return expanded;
            }
        };
    });




    var r20 = /%20/g,
        rbracket = /\[\]$/,
        rCRLF = /\r?\n/g,
        rhash = /#.*$/,
        rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
        // IE leaves an \r character at EOL
        rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
        // #7653, #8125, #8152: local protocol detection
        rlocalProtocol = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
        rnoContent = /^(?:GET|HEAD)$/,
        rprotocol = /^\/\//,
        rquery = /\?/,
        rscript = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
        rselectTextarea = /^(?:select|textarea)/i,
        rspacesAjax = /\s+/,
        rts = /([?&])_=[^&]*/,
        rurl = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,

        // Keep a copy of the old load method
        _load = jQuery.fn.load,

        /* Prefilters
         * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
         * 2) These are called:
         *    - BEFORE asking for a transport
         *    - AFTER param serialization (s.data is a string if s.processData is true)
         * 3) key is the dataType
         * 4) the catchall symbol "*" can be used
         * 5) execution will start with transport dataType and THEN continue down to "*" if needed
         */
        prefilters = {},

        /* Transports bindings
         * 1) key is the dataType
         * 2) the catchall symbol "*" can be used
         * 3) selection will start with transport dataType and THEN go to "*" if needed
         */
        transports = {},

        // Document location
        ajaxLocation,

        // Document location segments
        ajaxLocParts,

        // Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
        allTypes = ["*/"] + ["*"];

    // #8138, IE may throw an exception when accessing
    // a field from window.location if document.domain has been set
    try {
        ajaxLocation = location.href;
    } catch (e) {
        // Use the href attribute of an A element
        // since IE will modify it given document.location
        ajaxLocation = document.createElement("a");
        ajaxLocation.href = "";
        ajaxLocation = ajaxLocation.href;
    }

    // Segment location into parts
    ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];

    // Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport


    function addToPrefiltersOrTransports(structure) {

        // dataTypeExpression is optional and defaults to "*"
        return function(dataTypeExpression, func) {

            if (typeof dataTypeExpression !== "string") {
                func = dataTypeExpression;
                dataTypeExpression = "*";
            }

            if (jQuery.isFunction(func)) {
                var dataTypes = dataTypeExpression.toLowerCase().split(rspacesAjax),
                    i = 0,
                    length = dataTypes.length,
                    dataType, list, placeBefore;

                // For each dataType in the dataTypeExpression
                for (; i < length; i++) {
                    dataType = dataTypes[i];
                    // We control if we're asked to add before
                    // any existing element
                    placeBefore = /^\+/.test(dataType);
                    if (placeBefore) {
                        dataType = dataType.substr(1) || "*";
                    }
                    list = structure[dataType] = structure[dataType] || [];
                    // then we add to the structure accordingly
                    list[placeBefore ? "unshift" : "push"](func);
                }
            }
        };
    }

    // Base inspection function for prefilters and transports


    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, dataType /* internal */ , inspected /* internal */ ) {

        dataType = dataType || options.dataTypes[0];
        inspected = inspected || {};

        inspected[dataType] = true;

        var list = structure[dataType],
            i = 0,
            length = list ? list.length : 0,
            executeOnly = (structure === prefilters),
            selection;

        for (; i < length && (executeOnly || !selection); i++) {
            selection = list[i](options, originalOptions, jqXHR);
            // If we got redirected to another dataType
            // we try there if executing only and not done already
            if (typeof selection === "string") {
                if (!executeOnly || inspected[selection]) {
                    selection = undefined;
                } else {
                    options.dataTypes.unshift(selection);
                    selection = inspectPrefiltersOrTransports(
                    structure, options, originalOptions, jqXHR, selection, inspected);
                }
            }
        }
        // If we're only executing or nothing was selected
        // we try the catchall dataType if not done already
        if ((executeOnly || !selection) && !inspected["*"]) {
            selection = inspectPrefiltersOrTransports(
            structure, options, originalOptions, jqXHR, "*", inspected);
        }
        // unnecessary when only executing (prefilters)
        // but it'll be ignored by the caller in that case
        return selection;
    }

    // A special extend for ajax options
    // that takes "flat" options (not to be deep extended)
    // Fixes #9887


    function ajaxExtend(target, src) {
        var key, deep, flatOptions = jQuery.ajaxSettings.flatOptions || {};
        for (key in src) {
            if (src[key] !== undefined) {
                (flatOptions[key] ? target : (deep || (deep = {})))[key] = src[key];
            }
        }
        if (deep) {
            jQuery.extend(true, target, deep);
        }
    }

    jQuery.fn.extend({
        load: function(url, params, callback) {
            if (typeof url !== "string" && _load) {
                return _load.apply(this, arguments);

                // Don't do a request if no elements are being requested
            } else if (!this.length) {
                return this;
            }

            var off = url.indexOf(" ");
            if (off >= 0) {
                var selector = url.slice(off, url.length);
                url = url.slice(0, off);
            }

            // Default to a GET request
            var type = "GET";

            // If the second parameter was provided
            if (params) {
                // If it's a function
                if (jQuery.isFunction(params)) {
                    // We assume that it's the callback
                    callback = params;
                    params = undefined;

                    // Otherwise, build a param string
                } else if (typeof params === "object") {
                    params = jQuery.param(params, jQuery.ajaxSettings.traditional);
                    type = "POST";
                }
            }

            var self = this;

            // Request the remote document
            jQuery.ajax({
                url: url,
                type: type,
                dataType: "html",
                data: params,
                // Complete callback (responseText is used internally)
                complete: function(jqXHR, status, responseText) {
                    // Store the response as specified by the jqXHR object
                    responseText = jqXHR.responseText;
                    // If successful, inject the HTML into all the matched elements
                    if (jqXHR.isResolved()) {
                        // #4825: Get the actual response in case
                        // a dataFilter is present in ajaxSettings
                        jqXHR.done(function(r) {
                            responseText = r;
                        });
                        // See if a selector was specified
                        self.html(selector ?
                        // Create a dummy div to hold the results
                        jQuery("<div>")
                        // inject the contents of the document in, removing the scripts
                        // to avoid any 'Permission Denied' errors in IE
                        .append(responseText.replace(rscript, ""))

                        // Locate the specified elements
                        .find(selector) :

                        // If not, just inject the full result
                        responseText);
                    }

                    if (callback) {
                        self.each(callback, [responseText, status, jqXHR]);
                    }
                }
            });

            return this;
        },

        serialize: function() {
            return jQuery.param(this.serializeArray());
        },

        serializeArray: function() {
            return this.map(function() {
                return this.elements ? jQuery.makeArray(this.elements) : this;
            }).filter(function() {
                return this.name && !this.disabled && (this.checked || rselectTextarea.test(this.nodeName) || rinput.test(this.type));
            }).map(function(i, elem) {
                var val = jQuery(this).val();

                return val == null ? null : jQuery.isArray(val) ? jQuery.map(val, function(val, i) {
                    return {
                        name: elem.name,
                        value: val.replace(rCRLF, "\r\n")
                    };
                }) : {
                    name: elem.name,
                    value: val.replace(rCRLF, "\r\n")
                };
            }).get();
        }
    });

    // Attach a bunch of functions for handling common AJAX events
    jQuery.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(i, o) {
        jQuery.fn[o] = function(f) {
            return this.on(o, f);
        };
    });

    jQuery.each(["get", "post"], function(i, method) {
        jQuery[method] = function(url, data, callback, type) {
            // shift arguments if data argument was omitted
            if (jQuery.isFunction(data)) {
                type = type || callback;
                callback = data;
                data = undefined;
            }

            return jQuery.ajax({
                type: method,
                url: url,
                data: data,
                success: callback,
                dataType: type
            });
        };
    });

    jQuery.extend({

        getScript: function(url, callback) {
            return jQuery.get(url, undefined, callback, "script");
        },

        getJSON: function(url, data, callback) {
            return jQuery.get(url, data, callback, "json");
        },

        // Creates a full fledged settings object into target
        // with both ajaxSettings and settings fields.
        // If target is omitted, writes into ajaxSettings.
        ajaxSetup: function(target, settings) {
            if (settings) {
                // Building a settings object
                ajaxExtend(target, jQuery.ajaxSettings);
            } else {
                // Extending ajaxSettings
                settings = target;
                target = jQuery.ajaxSettings;
            }
            ajaxExtend(target, settings);
            return target;
        },

        ajaxSettings: {
            url: ajaxLocation,
            isLocal: rlocalProtocol.test(ajaxLocParts[1]),
            global: true,
            type: "GET",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            processData: true,
            async: true,
            /*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		traditional: false,
		headers: {},
		*/

            accepts: {
                xml: "application/xml, text/xml",
                html: "text/html",
                text: "text/plain",
                json: "application/json, text/javascript",
                "*": allTypes
            },

            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },

            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },

            // List of data converters
            // 1) key format is "source_type destination_type" (a single space in-between)
            // 2) the catchall symbol "*" can be used for source_type
            converters: {

                // Convert anything to text
                "* text": window.String,

                // Text to html (true = no transformation)
                "text html": true,

                // Evaluate text as a json expression
                "text json": jQuery.parseJSON,

                // Parse text as xml
                "text xml": jQuery.parseXML
            },

            // For options that shouldn't be deep extended:
            // you can add your own custom options here if
            // and when you create one that shouldn't be
            // deep extended (see ajaxExtend)
            flatOptions: {
                context: true,
                url: true
            }
        },

        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
        ajaxTransport: addToPrefiltersOrTransports(transports),

        // Main method
        ajax: function(url, options) {

            // If url is an object, simulate pre-1.5 signature
            if (typeof url === "object") {
                options = url;
                url = undefined;
            }

            // Force options to be an object
            options = options || {};

            var // Create the final options object
            s = jQuery.ajaxSetup({}, options),
                // Callbacks context
                callbackContext = s.context || s,
                // Context for global events
                // It's the callbackContext if one was provided in the options
                // and if it's a DOM node or a jQuery collection
                globalEventContext = callbackContext !== s && (callbackContext.nodeType || callbackContext instanceof jQuery) ? jQuery(callbackContext) : jQuery.event,
                // Deferreds
                deferred = jQuery.Deferred(),
                completeDeferred = jQuery.Callbacks("once memory"),
                // Status-dependent callbacks
                statusCode = s.statusCode || {},
                // ifModified key
                ifModifiedKey,
                // Headers (they are sent all at once)
                requestHeaders = {},
                requestHeadersNames = {},
                // Response headers
                responseHeadersString, responseHeaders,
                // transport
                transport,
                // timeout handle
                timeoutTimer,
                // Cross-domain detection vars
                parts,
                // The jqXHR state
                state = 0,
                // To know if global events are to be dispatched
                fireGlobals,
                // Loop variable
                i,
                // Fake xhr
                jqXHR = {

                    readyState: 0,

                    // Caches the header
                    setRequestHeader: function(name, value) {
                        if (!state) {
                            var lname = name.toLowerCase();
                            name = requestHeadersNames[lname] = requestHeadersNames[lname] || name;
                            requestHeaders[name] = value;
                        }
                        return this;
                    },

                    // Raw string
                    getAllResponseHeaders: function() {
                        return state === 2 ? responseHeadersString : null;
                    },

                    // Builds headers hashtable if needed
                    getResponseHeader: function(key) {
                        var match;
                        if (state === 2) {
                            if (!responseHeaders) {
                                responseHeaders = {};
                                while ((match = rheaders.exec(responseHeadersString))) {
                                    responseHeaders[match[1].toLowerCase()] = match[2];
                                }
                            }
                            match = responseHeaders[key.toLowerCase()];
                        }
                        return match === undefined ? null : match;
                    },

                    // Overrides response content-type header
                    overrideMimeType: function(type) {
                        if (!state) {
                            s.mimeType = type;
                        }
                        return this;
                    },

                    // Cancel the request
                    abort: function(statusText) {
                        statusText = statusText || "abort";
                        if (transport) {
                            transport.abort(statusText);
                        }
                        done(0, statusText);
                        return this;
                    }
                };

            // Callback for when everything is done
            // It is defined here because jslint complains if it is declared
            // at the end of the function (which would be more logical and readable)


            function done(status, nativeStatusText, responses, headers) {

                // Called once
                if (state === 2) {
                    return;
                }

                // State is "done" now
                state = 2;

                // Clear timeout if it exists
                if (timeoutTimer) {
                    clearTimeout(timeoutTimer);
                }

                // Dereference transport for early garbage collection
                // (no matter how long the jqXHR object will be used)
                transport = undefined;

                // Cache response headers
                responseHeadersString = headers || "";

                // Set readyState
                jqXHR.readyState = status > 0 ? 4 : 0;

                var isSuccess, success, error, statusText = nativeStatusText,
                    response = responses ? ajaxHandleResponses(s, jqXHR, responses) : undefined,
                    lastModified, etag;

                // If successful, handle type chaining
                if (status >= 200 && status < 300 || status === 304) {

                    // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
                    if (s.ifModified) {

                        if ((lastModified = jqXHR.getResponseHeader("Last-Modified"))) {
                            jQuery.lastModified[ifModifiedKey] = lastModified;
                        }
                        if ((etag = jqXHR.getResponseHeader("Etag"))) {
                            jQuery.etag[ifModifiedKey] = etag;
                        }
                    }

                    // If not modified
                    if (status === 304) {

                        statusText = "notmodified";
                        isSuccess = true;

                        // If we have data
                    } else {

                        try {
                            success = ajaxConvert(s, response);
                            statusText = "success";
                            isSuccess = true;
                        } catch (e) {
                            // We have a parsererror
                            statusText = "parsererror";
                            error = e;
                        }
                    }
                } else {
                    // We extract error from statusText
                    // then normalize statusText and status for non-aborts
                    error = statusText;
                    if (!statusText || status) {
                        statusText = "error";
                        if (status < 0) {
                            status = 0;
                        }
                    }
                }

                // Set data for the fake xhr object
                jqXHR.status = status;
                jqXHR.statusText = "" + (nativeStatusText || statusText);

                // Success/Error
                if (isSuccess) {
                    deferred.resolveWith(callbackContext, [success, statusText, jqXHR]);
                } else {
                    deferred.rejectWith(callbackContext, [jqXHR, statusText, error]);
                }

                // Status-dependent callbacks
                jqXHR.statusCode(statusCode);
                statusCode = undefined;

                if (fireGlobals) {
                    globalEventContext.trigger("ajax" + (isSuccess ? "Success" : "Error"), [jqXHR, s, isSuccess ? success : error]);
                }

                // Complete
                completeDeferred.fireWith(callbackContext, [jqXHR, statusText]);

                if (fireGlobals) {
                    globalEventContext.trigger("ajaxComplete", [jqXHR, s]);
                    // Handle the global AJAX counter
                    if (!(--jQuery.active)) {
                        jQuery.event.trigger("ajaxStop");
                    }
                }
            }

            // Attach deferreds
            deferred.promise(jqXHR);
            jqXHR.success = jqXHR.done;
            jqXHR.error = jqXHR.fail;
            jqXHR.complete = completeDeferred.add;

            // Status-dependent callbacks
            jqXHR.statusCode = function(map) {
                if (map) {
                    var tmp;
                    if (state < 2) {
                        for (tmp in map) {
                            statusCode[tmp] = [statusCode[tmp], map[tmp]];
                        }
                    } else {
                        tmp = map[jqXHR.status];
                        jqXHR.then(tmp, tmp);
                    }
                }
                return this;
            };

            // Remove hash character (#7531: and string promotion)
            // Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
            // We also use the url parameter if available
            s.url = ((url || s.url) + "").replace(rhash, "").replace(rprotocol, ajaxLocParts[1] + "//");

            // Extract dataTypes list
            s.dataTypes = jQuery.trim(s.dataType || "*").toLowerCase().split(rspacesAjax);

            // Determine if a cross-domain request is in order
            if (s.crossDomain == null) {
                parts = rurl.exec(s.url.toLowerCase());
                s.crossDomain = !! (parts && (parts[1] != ajaxLocParts[1] || parts[2] != ajaxLocParts[2] || (parts[3] || (parts[1] === "http:" ? 80 : 443)) != (ajaxLocParts[3] || (ajaxLocParts[1] === "http:" ? 80 : 443))));
            }

            // Convert data if not already a string
            if (s.data && s.processData && typeof s.data !== "string") {
                s.data = jQuery.param(s.data, s.traditional);
            }

            // Apply prefilters
            inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);

            // If request was aborted inside a prefilter, stop there
            if (state === 2) {
                return false;
            }

            // We can fire global events as of now if asked to
            fireGlobals = s.global;

            // Uppercase the type
            s.type = s.type.toUpperCase();

            // Determine if request has content
            s.hasContent = !rnoContent.test(s.type);

            // Watch for a new set of requests
            if (fireGlobals && jQuery.active++ === 0) {
                jQuery.event.trigger("ajaxStart");
            }

            // More options handling for requests with no content
            if (!s.hasContent) {

                // If data is available, append data to url
                if (s.data) {
                    s.url += (rquery.test(s.url) ? "&" : "?") + s.data;
                    // #9682: remove data so that it's not used in an eventual retry
                    delete s.data;
                }

                // Get ifModifiedKey before adding the anti-cache parameter
                ifModifiedKey = s.url;

                // Add anti-cache in url if needed
                if (s.cache === false) {

                    var ts = jQuery.now(),
                        // try replacing _= if it is there
                        ret = s.url.replace(rts, "$1_=" + ts);

                    // if nothing was replaced, add timestamp to the end
                    s.url = ret + ((ret === s.url) ? (rquery.test(s.url) ? "&" : "?") + "_=" + ts : "");
                }
            }

            // Set the correct header, if data is being sent
            if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
                jqXHR.setRequestHeader("Content-Type", s.contentType);
            }

            // Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
            if (s.ifModified) {
                ifModifiedKey = ifModifiedKey || s.url;
                if (jQuery.lastModified[ifModifiedKey]) {
                    jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[ifModifiedKey]);
                }
                if (jQuery.etag[ifModifiedKey]) {
                    jqXHR.setRequestHeader("If-None-Match", jQuery.etag[ifModifiedKey]);
                }
            }

            // Set the Accepts header for the server, depending on the dataType
            jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]);

            // Check for headers option
            for (i in s.headers) {
                jqXHR.setRequestHeader(i, s.headers[i]);
            }

            // Allow custom headers/mimetypes and early abort
            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) {
                // Abort if not done already
                jqXHR.abort();
                return false;

            }

            // Install callbacks on deferreds
            for (i in {
                success: 1,
                error: 1,
                complete: 1
            }) {
                jqXHR[i](s[i]);
            }

            // Get transport
            transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);

            // If no transport, we auto-abort
            if (!transport) {
                done(-1, "No Transport");
            } else {
                jqXHR.readyState = 1;
                // Send global event
                if (fireGlobals) {
                    globalEventContext.trigger("ajaxSend", [jqXHR, s]);
                }
                // Timeout
                if (s.async && s.timeout > 0) {
                    timeoutTimer = setTimeout(function() {
                        jqXHR.abort("timeout");
                    }, s.timeout);
                }

                try {
                    state = 1;
                    transport.send(requestHeaders, done);
                } catch (e) {
                    // Propagate exception as error if not done
                    if (state < 2) {
                        done(-1, e);
                        // Simply rethrow otherwise
                    } else {
                        throw e;
                    }
                }
            }

            return jqXHR;
        },

        // Serialize an array of form elements or a set of
        // key/values into a query string
        param: function(a, traditional) {
            var s = [],
                add = function(key, value) {
                    // If value is a function, invoke it and return its value
                    value = jQuery.isFunction(value) ? value() : value;
                    s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
                };

            // Set traditional to true for jQuery <= 1.3.2 behavior.
            if (traditional === undefined) {
                traditional = jQuery.ajaxSettings.traditional;
            }

            // If an array was passed in, assume that it is an array of form elements.
            if (jQuery.isArray(a) || (a.jquery && !jQuery.isPlainObject(a))) {
                // Serialize the form elements
                jQuery.each(a, function() {
                    add(this.name, this.value);
                });

            } else {
                // If traditional, encode the "old" way (the way 1.3.2 or older
                // did it), otherwise encode params recursively.
                for (var prefix in a) {
                    buildParams(prefix, a[prefix], traditional, add);
                }
            }

            // Return the resulting serialization
            return s.join("&").replace(r20, "+");
        }
    });

    function buildParams(prefix, obj, traditional, add) {
        if (jQuery.isArray(obj)) {
            // Serialize array item.
            jQuery.each(obj, function(i, v) {
                if (traditional || rbracket.test(prefix)) {
                    // Treat each array item as a scalar.
                    add(prefix, v);

                } else {
                    // If array item is non-scalar (array or object), encode its
                    // numeric index to resolve deserialization ambiguity issues.
                    // Note that rack (as of 1.0.0) can't currently deserialize
                    // nested arrays properly, and attempting to do so may cause
                    // a server error. Possible fixes are to modify rack's
                    // deserialization algorithm or to provide an option or flag
                    // to force array serialization to be shallow.
                    buildParams(prefix + "[" + (typeof v === "object" ? i : "") + "]", v, traditional, add);
                }
            });

        } else if (!traditional && jQuery.type(obj) === "object") {
            // Serialize object item.
            for (var name in obj) {
                buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
            }

        } else {
            // Serialize scalar item.
            add(prefix, obj);
        }
    }

    // This is still on the jQuery object... for now
    // Want to move this to jQuery.ajax some day
    jQuery.extend({

        // Counter for holding the number of active queries
        active: 0,

        // Last-Modified header cache for next request
        lastModified: {},
        etag: {}

    });

    /* Handles responses to an ajax request:
     * - sets all responseXXX fields accordingly
     * - finds the right dataType (mediates between content-type and expected dataType)
     * - returns the corresponding response
     */

    function ajaxHandleResponses(s, jqXHR, responses) {

        var contents = s.contents,
            dataTypes = s.dataTypes,
            responseFields = s.responseFields,
            ct, type, finalDataType, firstDataType;

        // Fill responseXXX fields
        for (type in responseFields) {
            if (type in responses) {
                jqXHR[responseFields[type]] = responses[type];
            }
        }

        // Remove auto dataType and get content-type in the process
        while (dataTypes[0] === "*") {
            dataTypes.shift();
            if (ct === undefined) {
                ct = s.mimeType || jqXHR.getResponseHeader("content-type");
            }
        }

        // Check if we're dealing with a known content-type
        if (ct) {
            for (type in contents) {
                if (contents[type] && contents[type].test(ct)) {
                    dataTypes.unshift(type);
                    break;
                }
            }
        }

        // Check to see if we have a response for the expected dataType
        if (dataTypes[0] in responses) {
            finalDataType = dataTypes[0];
        } else {
            // Try convertible dataTypes
            for (type in responses) {
                if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) {
                    finalDataType = type;
                    break;
                }
                if (!firstDataType) {
                    firstDataType = type;
                }
            }
            // Or just use first one
            finalDataType = finalDataType || firstDataType;
        }

        // If we found a dataType
        // We add the dataType to the list if needed
        // and return the corresponding response
        if (finalDataType) {
            if (finalDataType !== dataTypes[0]) {
                dataTypes.unshift(finalDataType);
            }
            return responses[finalDataType];
        }
    }

    // Chain conversions given the request and the original response


    function ajaxConvert(s, response) {

        // Apply the dataFilter if provided
        if (s.dataFilter) {
            response = s.dataFilter(response, s.dataType);
        }

        var dataTypes = s.dataTypes,
            converters = {},
            i, key, length = dataTypes.length,
            tmp,
            // Current and previous dataTypes
            current = dataTypes[0],
            prev,
            // Conversion expression
            conversion,
            // Conversion function
            conv,
            // Conversion functions (transitive conversion)
            conv1, conv2;

        // For each dataType in the chain
        for (i = 1; i < length; i++) {

            // Create converters map
            // with lowercased keys
            if (i === 1) {
                for (key in s.converters) {
                    if (typeof key === "string") {
                        converters[key.toLowerCase()] = s.converters[key];
                    }
                }
            }

            // Get the dataTypes
            prev = current;
            current = dataTypes[i];

            // If current is auto dataType, update it to prev
            if (current === "*") {
                current = prev;
                // If no auto and dataTypes are actually different
            } else if (prev !== "*" && prev !== current) {

                // Get the converter
                conversion = prev + " " + current;
                conv = converters[conversion] || converters["* " + current];

                // If there is no direct converter, search transitively
                if (!conv) {
                    conv2 = undefined;
                    for (conv1 in converters) {
                        tmp = conv1.split(" ");
                        if (tmp[0] === prev || tmp[0] === "*") {
                            conv2 = converters[tmp[1] + " " + current];
                            if (conv2) {
                                conv1 = converters[conv1];
                                if (conv1 === true) {
                                    conv = conv2;
                                } else if (conv2 === true) {
                                    conv = conv1;
                                }
                                break;
                            }
                        }
                    }
                }
                // If we found no converter, dispatch an error
                if (!(conv || conv2)) {
                    jQuery.error("No conversion from " + conversion.replace(" ", " to "));
                }
                // If found converter is not an equivalence
                if (conv !== true) {
                    // Convert with 1 or 2 converters accordingly
                    response = conv ? conv(response) : conv2(conv1(response));
                }
            }
        }
        return response;
    }




    var jsc = jQuery.now(),
        jsre = /(\=)\?(&|$)|\?\?/i;

    // Default jsonp settings
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            return jQuery.expando + "_" + (jsc++);
        }
    });

    // Detect, normalize options and install callbacks for jsonp requests
    jQuery.ajaxPrefilter("json jsonp", function(s, originalSettings, jqXHR) {

        var inspectData = (typeof s.data === "string") && /^application\/x\-www\-form\-urlencoded/.test(s.contentType);

        if (s.dataTypes[0] === "jsonp" || s.jsonp !== false && (jsre.test(s.url) || inspectData && jsre.test(s.data))) {

            var responseContainer, jsonpCallback = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback,
                previous = window[jsonpCallback],
                url = s.url,
                data = s.data,
                replace = "$1" + jsonpCallback + "$2";

            if (s.jsonp !== false) {
                url = url.replace(jsre, replace);
                if (s.url === url) {
                    if (inspectData) {
                        data = data.replace(jsre, replace);
                    }
                    if (s.data === data) {
                        // Add callback manually
                        url += (/\?/.test(url) ? "&" : "?") + s.jsonp + "=" + jsonpCallback;
                    }
                }
            }

            s.url = url;
            s.data = data;

            // Install callback
            window[jsonpCallback] = function(response) {
                responseContainer = [response];
            };

            // Clean-up function
            jqXHR.always(function() {
                // Set callback back to previous value
                window[jsonpCallback] = previous;
                // Call if it was a function and we have a response
                if (responseContainer && jQuery.isFunction(previous)) {
                    window[jsonpCallback](responseContainer[0]);
                }
            });

            // Use data converter to retrieve json after script execution
            s.converters["script json"] = function() {
                if (!responseContainer) {
                    jQuery.error(jsonpCallback + " was not called");
                }
                return responseContainer[0];
            };

            // force json dataType
            s.dataTypes[0] = "json";

            // Delegate to script
            return "script";
        }
    });




    // Install script dataType
    jQuery.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /javascript|ecmascript/
        },
        converters: {
            "text script": function(text) {
                jQuery.globalEval(text);
                return text;
            }
        }
    });

    // Handle cache's special case and global
    jQuery.ajaxPrefilter("script", function(s) {
        if (s.cache === undefined) {
            s.cache = false;
        }
        if (s.crossDomain) {
            s.type = "GET";
            s.global = false;
        }
    });

    // Bind script tag hack transport
    jQuery.ajaxTransport("script", function(s) {

        // This transport only deals with cross domain requests
        if (s.crossDomain) {

            var script, head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;

            return {

                send: function(_, callback) {

                    script = document.createElement("script");

                    script.async = "async";

                    if (s.scriptCharset) {
                        script.charset = s.scriptCharset;
                    }

                    script.src = s.url;

                    // Attach handlers for all browsers
                    script.onload = script.onreadystatechange = function(_, isAbort) {

                        if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {

                            // Handle memory leak in IE
                            script.onload = script.onreadystatechange = null;

                            // Remove the script
                            if (head && script.parentNode) {
                                head.removeChild(script);
                            }

                            // Dereference the script
                            script = undefined;

                            // Callback if not abort
                            if (!isAbort) {
                                callback(200, "success");
                            }
                        }
                    };
                    // Use insertBefore instead of appendChild  to circumvent an IE6 bug.
                    // This arises when a base node is used (#2709 and #4378).
                    head.insertBefore(script, head.firstChild);
                },

                abort: function() {
                    if (script) {
                        script.onload(0, 1);
                    }
                }
            };
        }
    });




    var // #5280: Internet Explorer will keep connections alive if we don't abort on unload
    xhrOnUnloadAbort = window.ActiveXObject ?
    function() {
        // Abort all pending requests
        for (var key in xhrCallbacks) {
            xhrCallbacks[key](0, 1);
        }
    } : false, xhrId = 0, xhrCallbacks;

    // Functions to create xhrs


    function createStandardXHR() {
        try {
            return new window.XMLHttpRequest();
        } catch (e) {}
    }

    function createActiveXHR() {
        try {
            return new window.ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
    }

    // Create the request object
    // (This is still attached to ajaxSettings for backward compatibility)
    jQuery.ajaxSettings.xhr = window.ActiveXObject ?
    /* Microsoft failed to properly
     * implement the XMLHttpRequest in IE7 (can't request local files),
     * so we use the ActiveXObject when it is available
     * Additionally XMLHttpRequest can be disabled in IE7/IE8 so
     * we need a fallback.
     */

    function() {
        return !this.isLocal && createStandardXHR() || createActiveXHR();
    } :
    // For all other browsers, use the standard XMLHttpRequest object
    createStandardXHR;

    // Determine support properties
    (function(xhr) {
        jQuery.extend(jQuery.support, {
            ajax: !! xhr,
            cors: !! xhr && ("withCredentials" in xhr)
        });
    })(jQuery.ajaxSettings.xhr());

    // Create transport if the browser can provide an xhr
    if (jQuery.support.ajax) {

        jQuery.ajaxTransport(function(s) {
            // Cross domain only allowed if supported through XMLHttpRequest
            if (!s.crossDomain || jQuery.support.cors) {

                var callback;

                return {
                    send: function(headers, complete) {

                        // Get a new xhr
                        var xhr = s.xhr(),
                            handle, i;

                        // Open the socket
                        // Passing null username, generates a login popup on Opera (#2865)
                        if (s.username) {
                            xhr.open(s.type, s.url, s.async, s.username, s.password);
                        } else {
                            xhr.open(s.type, s.url, s.async);
                        }

                        // Apply custom fields if provided
                        if (s.xhrFields) {
                            for (i in s.xhrFields) {
                                xhr[i] = s.xhrFields[i];
                            }
                        }

                        // Override mime type if needed
                        if (s.mimeType && xhr.overrideMimeType) {
                            xhr.overrideMimeType(s.mimeType);
                        }

                        // X-Requested-With header
                        // For cross-domain requests, seeing as conditions for a preflight are
                        // akin to a jigsaw puzzle, we simply never set it to be sure.
                        // (it can always be set on a per-request basis or even using ajaxSetup)
                        // For same-domain requests, won't change header if already provided.
                        if (!s.crossDomain && !headers["X-Requested-With"]) {
                            headers["X-Requested-With"] = "XMLHttpRequest";
                        }

                        // Need an extra try/catch for cross domain requests in Firefox 3
                        try {
                            for (i in headers) {
                                xhr.setRequestHeader(i, headers[i]);
                            }
                        } catch (_) {}

                        // Do send the request
                        // This may raise an exception which is actually
                        // handled in jQuery.ajax (so no try/catch here)
                        xhr.send((s.hasContent && s.data) || null);

                        // Listener
                        callback = function(_, isAbort) {

                            var status, statusText, responseHeaders, responses, xml;

                            // Firefox throws exceptions when accessing properties
                            // of an xhr when a network error occured
                            // http://helpful.knobs-dials.com/index.php/Component_returned_failure_code:_0x80040111_(NS_ERROR_NOT_AVAILABLE)
                            try {

                                // Was never called and is aborted or complete
                                if (callback && (isAbort || xhr.readyState === 4)) {

                                    // Only called once
                                    callback = undefined;

                                    // Do not keep as active anymore
                                    if (handle) {
                                        xhr.onreadystatechange = jQuery.noop;
                                        if (xhrOnUnloadAbort) {
                                            delete xhrCallbacks[handle];
                                        }
                                    }

                                    // If it's an abort
                                    if (isAbort) {
                                        // Abort it manually if needed
                                        if (xhr.readyState !== 4) {
                                            xhr.abort();
                                        }
                                    } else {
                                        status = xhr.status;
                                        responseHeaders = xhr.getAllResponseHeaders();
                                        responses = {};
                                        xml = xhr.responseXML;

                                        // Construct response list
                                        if (xml && xml.documentElement /* #4958 */ ) {
                                            responses.xml = xml;
                                        }

                                        // When requesting binary data, IE6-9 will throw an exception
                                        // on any attempt to access responseText (#11426)
                                        try {
                                            responses.text = xhr.responseText;
                                        } catch (_) {}

                                        // Firefox throws an exception when accessing
                                        // statusText for faulty cross-domain requests
                                        try {
                                            statusText = xhr.statusText;
                                        } catch (e) {
                                            // We normalize with Webkit giving an empty statusText
                                            statusText = "";
                                        }

                                        // Filter status for non standard behaviors

                                        // If the request is local and we have data: assume a success
                                        // (success with no data won't get notified, that's the best we
                                        // can do given current implementations)
                                        if (!status && s.isLocal && !s.crossDomain) {
                                            status = responses.text ? 200 : 404;
                                            // IE - #1450: sometimes returns 1223 when it should be 204
                                        } else if (status === 1223) {
                                            status = 204;
                                        }
                                    }
                                }
                            } catch (firefoxAccessException) {
                                if (!isAbort) {
                                    complete(-1, firefoxAccessException);
                                }
                            }

                            // Call complete if needed
                            if (responses) {
                                complete(status, statusText, responses, responseHeaders);
                            }
                        };

                        // if we're in sync mode or it's in cache
                        // and has been retrieved directly (IE6 & IE7)
                        // we need to manually fire the callback
                        if (!s.async || xhr.readyState === 4) {
                            callback();
                        } else {
                            handle = ++xhrId;
                            if (xhrOnUnloadAbort) {
                                // Create the active xhrs callbacks list if needed
                                // and attach the unload handler
                                if (!xhrCallbacks) {
                                    xhrCallbacks = {};
                                    jQuery(window).unload(xhrOnUnloadAbort);
                                }
                                // Add to list of active xhrs callbacks
                                xhrCallbacks[handle] = callback;
                            }
                            xhr.onreadystatechange = callback;
                        }
                    },

                    abort: function() {
                        if (callback) {
                            callback(0, 1);
                        }
                    }
                };
            }
        });
    }




    var elemdisplay = {},
        iframe, iframeDoc, rfxtypes = /^(?:toggle|show|hide)$/,
        rfxnum = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,
        timerId, fxAttrs = [
        // height animations
        ["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"],
        // width animations
        ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"],
        // opacity animations
        ["opacity"]],
        fxNow;

    jQuery.fn.extend({
        show: function(speed, easing, callback) {
            var elem, display;

            if (speed || speed === 0) {
                return this.animate(genFx("show", 3), speed, easing, callback);

            } else {
                for (var i = 0, j = this.length; i < j; i++) {
                    elem = this[i];

                    if (elem.style) {
                        display = elem.style.display;

                        // Reset the inline display of this element to learn if it is
                        // being hidden by cascaded rules or not
                        if (!jQuery._data(elem, "olddisplay") && display === "none") {
                            display = elem.style.display = "";
                        }

                        // Set elements which have been overridden with display: none
                        // in a stylesheet to whatever the default browser style is
                        // for such an element
                        if ((display === "" && jQuery.css(elem, "display") === "none") || !jQuery.contains(elem.ownerDocument.documentElement, elem)) {
                            jQuery._data(elem, "olddisplay", defaultDisplay(elem.nodeName));
                        }
                    }
                }

                // Set the display of most of the elements in a second loop
                // to avoid the constant reflow
                for (i = 0; i < j; i++) {
                    elem = this[i];

                    if (elem.style) {
                        display = elem.style.display;

                        if (display === "" || display === "none") {
                            elem.style.display = jQuery._data(elem, "olddisplay") || "";
                        }
                    }
                }

                return this;
            }
        },

        hide: function(speed, easing, callback) {
            if (speed || speed === 0) {
                return this.animate(genFx("hide", 3), speed, easing, callback);

            } else {
                var elem, display, i = 0,
                    j = this.length;

                for (; i < j; i++) {
                    elem = this[i];
                    if (elem.style) {
                        display = jQuery.css(elem, "display");

                        if (display !== "none" && !jQuery._data(elem, "olddisplay")) {
                            jQuery._data(elem, "olddisplay", display);
                        }
                    }
                }

                // Set the display of the elements in a second loop
                // to avoid the constant reflow
                for (i = 0; i < j; i++) {
                    if (this[i].style) {
                        this[i].style.display = "none";
                    }
                }

                return this;
            }
        },

        // Save the old toggle function
        _toggle: jQuery.fn.toggle,

        toggle: function(fn, fn2, callback) {
            var bool = typeof fn === "boolean";

            if (jQuery.isFunction(fn) && jQuery.isFunction(fn2)) {
                this._toggle.apply(this, arguments);

            } else if (fn == null || bool) {
                this.each(function() {
                    var state = bool ? fn : jQuery(this).is(":hidden");
                    jQuery(this)[state ? "show" : "hide"]();
                });

            } else {
                this.animate(genFx("toggle", 3), fn, fn2, callback);
            }

            return this;
        },

        fadeTo: function(speed, to, easing, callback) {
            return this.filter(":hidden").css("opacity", 0).show().end().animate({
                opacity: to
            }, speed, easing, callback);
        },

        animate: function(prop, speed, easing, callback) {
            var optall = jQuery.speed(speed, easing, callback);

            if (jQuery.isEmptyObject(prop)) {
                return this.each(optall.complete, [false]);
            }

            // Do not change referenced properties as per-property easing will be lost
            prop = jQuery.extend({}, prop);

            function doAnimation() {
                // XXX 'this' does not always have a nodeName when running the
                // test suite
                if (optall.queue === false) {
                    jQuery._mark(this);
                }

                var opt = jQuery.extend({}, optall),
                    isElement = this.nodeType === 1,
                    hidden = isElement && jQuery(this).is(":hidden"),
                    name, val, p, e, hooks, replace, parts, start, end, unit, method;

                // will store per property easing and be used to determine when an animation is complete
                opt.animatedProperties = {};

                // first pass over propertys to expand / normalize
                for (p in prop) {
                    name = jQuery.camelCase(p);
                    if (p !== name) {
                        prop[name] = prop[p];
                        delete prop[p];
                    }

                    if ((hooks = jQuery.cssHooks[name]) && "expand" in hooks) {
                        replace = hooks.expand(prop[name]);
                        delete prop[name];

                        // not quite $.extend, this wont overwrite keys already present.
                        // also - reusing 'p' from above because we have the correct "name"
                        for (p in replace) {
                            if (!(p in prop)) {
                                prop[p] = replace[p];
                            }
                        }
                    }
                }

                for (name in prop) {
                    val = prop[name];
                    // easing resolution: per property > opt.specialEasing > opt.easing > 'swing' (default)
                    if (jQuery.isArray(val)) {
                        opt.animatedProperties[name] = val[1];
                        val = prop[name] = val[0];
                    } else {
                        opt.animatedProperties[name] = opt.specialEasing && opt.specialEasing[name] || opt.easing || 'swing';
                    }

                    if (val === "hide" && hidden || val === "show" && !hidden) {
                        return opt.complete.call(this);
                    }

                    if (isElement && (name === "height" || name === "width")) {
                        // Make sure that nothing sneaks out
                        // Record all 3 overflow attributes because IE does not
                        // change the overflow attribute when overflowX and
                        // overflowY are set to the same value
                        opt.overflow = [this.style.overflow, this.style.overflowX, this.style.overflowY];

                        // Set display property to inline-block for height/width
                        // animations on inline elements that are having width/height animated
                        if (jQuery.css(this, "display") === "inline" && jQuery.css(this, "float") === "none") {

                            // inline-level elements accept inline-block;
                            // block-level elements need to be inline with layout
                            if (!jQuery.support.inlineBlockNeedsLayout || defaultDisplay(this.nodeName) === "inline") {
                                this.style.display = "inline-block";

                            } else {
                                this.style.zoom = 1;
                            }
                        }
                    }
                }

                if (opt.overflow != null) {
                    this.style.overflow = "hidden";
                }

                for (p in prop) {
                    e = new jQuery.fx(this, opt, p);
                    val = prop[p];

                    if (rfxtypes.test(val)) {

                        // Tracks whether to show or hide based on private
                        // data attached to the element
                        method = jQuery._data(this, "toggle" + p) || (val === "toggle" ? hidden ? "show" : "hide" : 0);
                        if (method) {
                            jQuery._data(this, "toggle" + p, method === "show" ? "hide" : "show");
                            e[method]();
                        } else {
                            e[val]();
                        }

                    } else {
                        parts = rfxnum.exec(val);
                        start = e.cur();

                        if (parts) {
                            end = parseFloat(parts[2]);
                            unit = parts[3] || (jQuery.cssNumber[p] ? "" : "px");

                            // We need to compute starting value
                            if (unit !== "px") {
                                jQuery.style(this, p, (end || 1) + unit);
                                start = ((end || 1) / e.cur()) * start;
                                jQuery.style(this, p, start + unit);
                            }

                            // If a +=/-= token was provided, we're doing a relative animation
                            if (parts[1]) {
                                end = ((parts[1] === "-=" ? -1 : 1) * end) + start;
                            }

                            e.custom(start, end, unit);

                        } else {
                            e.custom(start, val, "");
                        }
                    }
                }

                // For JS strict compliance
                return true;
            }

            return optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
        },

        stop: function(type, clearQueue, gotoEnd) {
            if (typeof type !== "string") {
                gotoEnd = clearQueue;
                clearQueue = type;
                type = undefined;
            }
            if (clearQueue && type !== false) {
                this.queue(type || "fx", []);
            }

            return this.each(function() {
                var index, hadTimers = false,
                    timers = jQuery.timers,
                    data = jQuery._data(this);

                // clear marker counters if we know they won't be
                if (!gotoEnd) {
                    jQuery._unmark(true, this);
                }

                function stopQueue(elem, data, index) {
                    var hooks = data[index];
                    jQuery.removeData(elem, index, true);
                    hooks.stop(gotoEnd);
                }

                if (type == null) {
                    for (index in data) {
                        if (data[index] && data[index].stop && index.indexOf(".run") === index.length - 4) {
                            stopQueue(this, data, index);
                        }
                    }
                } else if (data[index = type + ".run"] && data[index].stop) {
                    stopQueue(this, data, index);
                }

                for (index = timers.length; index--;) {
                    if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                        if (gotoEnd) {

                            // force the next step to be the last
                            timers[index](true);
                        } else {
                            timers[index].saveState();
                        }
                        hadTimers = true;
                        timers.splice(index, 1);
                    }
                }

                // start the next in the queue if the last step wasn't forced
                // timers currently will call their complete callbacks, which will dequeue
                // but only if they were gotoEnd
                if (!(gotoEnd && hadTimers)) {
                    jQuery.dequeue(this, type);
                }
            });
        }

    });

    // Animations created synchronously will run synchronously


    function createFxNow() {
        setTimeout(clearFxNow, 0);
        return (fxNow = jQuery.now());
    }

    function clearFxNow() {
        fxNow = undefined;
    }

    // Generate parameters to create a standard animation


    function genFx(type, num) {
        var obj = {};

        jQuery.each(fxAttrs.concat.apply([], fxAttrs.slice(0, num)), function() {
            obj[this] = type;
        });

        return obj;
    }

    // Generate shortcuts for custom animations
    jQuery.each({
        slideDown: genFx("show", 1),
        slideUp: genFx("hide", 1),
        slideToggle: genFx("toggle", 1),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(name, props) {
        jQuery.fn[name] = function(speed, easing, callback) {
            return this.animate(props, speed, easing, callback);
        };
    });

    jQuery.extend({
        speed: function(speed, easing, fn) {
            var opt = speed && typeof speed === "object" ? jQuery.extend({}, speed) : {
                complete: fn || !fn && easing || jQuery.isFunction(speed) && speed,
                duration: speed,
                easing: fn && easing || easing && !jQuery.isFunction(easing) && easing
            };

            opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default;

            // normalize opt.queue - true/undefined/null -> "fx"
            if (opt.queue == null || opt.queue === true) {
                opt.queue = "fx";
            }

            // Queueing
            opt.old = opt.complete;

            opt.complete = function(noUnmark) {
                if (jQuery.isFunction(opt.old)) {
                    opt.old.call(this);
                }

                if (opt.queue) {
                    jQuery.dequeue(this, opt.queue);
                } else if (noUnmark !== false) {
                    jQuery._unmark(this);
                }
            };

            return opt;
        },

        easing: {
            linear: function(p) {
                return p;
            },
            swing: function(p) {
                return (-Math.cos(p * Math.PI) / 2) + 0.5;
            }
        },

        timers: [],

        fx: function(elem, options, prop) {
            this.options = options;
            this.elem = elem;
            this.prop = prop;

            options.orig = options.orig || {};
        }

    });

    jQuery.fx.prototype = {
        // Simple function for setting a style value
        update: function() {
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this);
            }

            (jQuery.fx.step[this.prop] || jQuery.fx.step._default)(this);
        },

        // Get the current size
        cur: function() {
            if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) {
                return this.elem[this.prop];
            }

            var parsed, r = jQuery.css(this.elem, this.prop);
            // Empty strings, null, undefined and "auto" are converted to 0,
            // complex values such as "rotate(1rad)" are returned as is,
            // simple values such as "10px" are parsed to Float.
            return isNaN(parsed = parseFloat(r)) ? !r || r === "auto" ? 0 : r : parsed;
        },

        // Start an animation from one number to another
        custom: function(from, to, unit) {
            var self = this,
                fx = jQuery.fx;

            this.startTime = fxNow || createFxNow();
            this.end = to;
            this.now = this.start = from;
            this.pos = this.state = 0;
            this.unit = unit || this.unit || (jQuery.cssNumber[this.prop] ? "" : "px");

            function t(gotoEnd) {
                return self.step(gotoEnd);
            }

            t.queue = this.options.queue;
            t.elem = this.elem;
            t.saveState = function() {
                if (jQuery._data(self.elem, "fxshow" + self.prop) === undefined) {
                    if (self.options.hide) {
                        jQuery._data(self.elem, "fxshow" + self.prop, self.start);
                    } else if (self.options.show) {
                        jQuery._data(self.elem, "fxshow" + self.prop, self.end);
                    }
                }
            };

            if (t() && jQuery.timers.push(t) && !timerId) {
                timerId = setInterval(fx.tick, fx.interval);
            }
        },

        // Simple 'show' function
        show: function() {
            var dataShow = jQuery._data(this.elem, "fxshow" + this.prop);

            // Remember where we started, so that we can go back to it later
            this.options.orig[this.prop] = dataShow || jQuery.style(this.elem, this.prop);
            this.options.show = true;

            // Begin the animation
            // Make sure that we start at a small width/height to avoid any flash of content
            if (dataShow !== undefined) {
                // This show is picking up where a previous hide or show left off
                this.custom(this.cur(), dataShow);
            } else {
                this.custom(this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur());
            }

            // Start by showing the element
            jQuery(this.elem).show();
        },

        // Simple 'hide' function
        hide: function() {
            // Remember where we started, so that we can go back to it later
            this.options.orig[this.prop] = jQuery._data(this.elem, "fxshow" + this.prop) || jQuery.style(this.elem, this.prop);
            this.options.hide = true;

            // Begin the animation
            this.custom(this.cur(), 0);
        },

        // Each step of an animation
        step: function(gotoEnd) {
            var p, n, complete, t = fxNow || createFxNow(),
                done = true,
                elem = this.elem,
                options = this.options;

            if (gotoEnd || t >= options.duration + this.startTime) {
                this.now = this.end;
                this.pos = this.state = 1;
                this.update();

                options.animatedProperties[this.prop] = true;

                for (p in options.animatedProperties) {
                    if (options.animatedProperties[p] !== true) {
                        done = false;
                    }
                }

                if (done) {
                    // Reset the overflow
                    if (options.overflow != null && !jQuery.support.shrinkWrapBlocks) {

                        jQuery.each(["", "X", "Y"], function(index, value) {
                            elem.style["overflow" + value] = options.overflow[index];
                        });
                    }

                    // Hide the element if the "hide" operation was done
                    if (options.hide) {
                        jQuery(elem).hide();
                    }

                    // Reset the properties, if the item has been hidden or shown
                    if (options.hide || options.show) {
                        for (p in options.animatedProperties) {
                            jQuery.style(elem, p, options.orig[p]);
                            jQuery.removeData(elem, "fxshow" + p, true);
                            // Toggle data is no longer needed
                            jQuery.removeData(elem, "toggle" + p, true);
                        }
                    }

                    // Execute the complete function
                    // in the event that the complete function throws an exception
                    // we must ensure it won't be called twice. #5684
                    complete = options.complete;
                    if (complete) {

                        options.complete = false;
                        complete.call(elem);
                    }
                }

                return false;

            } else {
                // classical easing cannot be used with an Infinity duration
                if (options.duration == Infinity) {
                    this.now = t;
                } else {
                    n = t - this.startTime;
                    this.state = n / options.duration;

                    // Perform the easing function, defaults to swing
                    this.pos = jQuery.easing[options.animatedProperties[this.prop]](this.state, n, 0, 1, options.duration);
                    this.now = this.start + ((this.end - this.start) * this.pos);
                }
                // Perform the next step of the animation
                this.update();
            }

            return true;
        }
    };

    jQuery.extend(jQuery.fx, {
        tick: function() {
            var timer, timers = jQuery.timers,
                i = 0;

            for (; i < timers.length; i++) {
                timer = timers[i];
                // Checks the timer has not already been removed
                if (!timer() && timers[i] === timer) {
                    timers.splice(i--, 1);
                }
            }

            if (!timers.length) {
                jQuery.fx.stop();
            }
        },

        interval: 13,

        stop: function() {
            clearInterval(timerId);
            timerId = null;
        },

        speeds: {
            slow: 600,
            fast: 200,
            // Default speed
            _default: 400
        },

        step: {
            opacity: function(fx) {
                jQuery.style(fx.elem, "opacity", fx.now);
            },

            _default: function(fx) {
                if (fx.elem.style && fx.elem.style[fx.prop] != null) {
                    fx.elem.style[fx.prop] = fx.now + fx.unit;
                } else {
                    fx.elem[fx.prop] = fx.now;
                }
            }
        }
    });

    // Ensure props that can't be negative don't go there on undershoot easing
    jQuery.each(fxAttrs.concat.apply([], fxAttrs), function(i, prop) {
        // exclude marginTop, marginLeft, marginBottom and marginRight from this list
        if (prop.indexOf("margin")) {
            jQuery.fx.step[prop] = function(fx) {
                jQuery.style(fx.elem, prop, Math.max(0, fx.now) + fx.unit);
            };
        }
    });

    if (jQuery.expr && jQuery.expr.filters) {
        jQuery.expr.filters.animated = function(elem) {
            return jQuery.grep(jQuery.timers, function(fn) {
                return elem === fn.elem;
            }).length;
        };
    }

    // Try to restore the default display value of an element


    function defaultDisplay(nodeName) {

        if (!elemdisplay[nodeName]) {

            var body = document.body,
                elem = jQuery("<" + nodeName + ">").appendTo(body),
                display = elem.css("display");
            elem.remove();

            // If the simple way fails,
            // get element's real default display by attaching it to a temp iframe
            if (display === "none" || display === "") {
                // No iframe to use yet, so create it
                if (!iframe) {
                    iframe = document.createElement("iframe");
                    iframe.frameBorder = iframe.width = iframe.height = 0;
                }

                body.appendChild(iframe);

                // Create a cacheable copy of the iframe document on first call.
                // IE and Opera will allow us to reuse the iframeDoc without re-writing the fake HTML
                // document to it; WebKit & Firefox won't allow reusing the iframe document.
                if (!iframeDoc || !iframe.createElement) {
                    iframeDoc = (iframe.contentWindow || iframe.contentDocument).document;
                    iframeDoc.write((jQuery.support.boxModel ? "<!doctype html>" : "") + "<html><body>");
                    iframeDoc.close();
                }

                elem = iframeDoc.createElement(nodeName);

                iframeDoc.body.appendChild(elem);

                display = jQuery.css(elem, "display");
                body.removeChild(iframe);
            }

            // Store the correct default display
            elemdisplay[nodeName] = display;
        }

        return elemdisplay[nodeName];
    }




    var getOffset, rtable = /^t(?:able|d|h)$/i,
        rroot = /^(?:body|html)$/i;

    if ("getBoundingClientRect" in document.documentElement) {
        getOffset = function(elem, doc, docElem, box) {
            try {
                box = elem.getBoundingClientRect();
            } catch (e) {}

            // Make sure we're not dealing with a disconnected DOM node
            if (!box || !jQuery.contains(docElem, elem)) {
                return box ? {
                    top: box.top,
                    left: box.left
                } : {
                    top: 0,
                    left: 0
                };
            }

            var body = doc.body,
                win = getWindow(doc),
                clientTop = docElem.clientTop || body.clientTop || 0,
                clientLeft = docElem.clientLeft || body.clientLeft || 0,
                scrollTop = win.pageYOffset || jQuery.support.boxModel && docElem.scrollTop || body.scrollTop,
                scrollLeft = win.pageXOffset || jQuery.support.boxModel && docElem.scrollLeft || body.scrollLeft,
                top = box.top + scrollTop - clientTop,
                left = box.left + scrollLeft - clientLeft;

            return {
                top: top,
                left: left
            };
        };

    } else {
        getOffset = function(elem, doc, docElem) {
            var computedStyle, offsetParent = elem.offsetParent,
                prevOffsetParent = elem,
                body = doc.body,
                defaultView = doc.defaultView,
                prevComputedStyle = defaultView ? defaultView.getComputedStyle(elem, null) : elem.currentStyle,
                top = elem.offsetTop,
                left = elem.offsetLeft;

            while ((elem = elem.parentNode) && elem !== body && elem !== docElem) {
                if (jQuery.support.fixedPosition && prevComputedStyle.position === "fixed") {
                    break;
                }

                computedStyle = defaultView ? defaultView.getComputedStyle(elem, null) : elem.currentStyle;
                top -= elem.scrollTop;
                left -= elem.scrollLeft;

                if (elem === offsetParent) {
                    top += elem.offsetTop;
                    left += elem.offsetLeft;

                    if (jQuery.support.doesNotAddBorder && !(jQuery.support.doesAddBorderForTableAndCells && rtable.test(elem.nodeName))) {
                        top += parseFloat(computedStyle.borderTopWidth) || 0;
                        left += parseFloat(computedStyle.borderLeftWidth) || 0;
                    }

                    prevOffsetParent = offsetParent;
                    offsetParent = elem.offsetParent;
                }

                if (jQuery.support.subtractsBorderForOverflowNotVisible && computedStyle.overflow !== "visible") {
                    top += parseFloat(computedStyle.borderTopWidth) || 0;
                    left += parseFloat(computedStyle.borderLeftWidth) || 0;
                }

                prevComputedStyle = computedStyle;
            }

            if (prevComputedStyle.position === "relative" || prevComputedStyle.position === "static") {
                top += body.offsetTop;
                left += body.offsetLeft;
            }

            if (jQuery.support.fixedPosition && prevComputedStyle.position === "fixed") {
                top += Math.max(docElem.scrollTop, body.scrollTop);
                left += Math.max(docElem.scrollLeft, body.scrollLeft);
            }

            return {
                top: top,
                left: left
            };
        };
    }

    jQuery.fn.offset = function(options) {
        if (arguments.length) {
            return options === undefined ? this : this.each(function(i) {
                jQuery.offset.setOffset(this, options, i);
            });
        }

        var elem = this[0],
            doc = elem && elem.ownerDocument;

        if (!doc) {
            return null;
        }

        if (elem === doc.body) {
            return jQuery.offset.bodyOffset(elem);
        }

        return getOffset(elem, doc, doc.documentElement);
    };

    jQuery.offset = {

        bodyOffset: function(body) {
            var top = body.offsetTop,
                left = body.offsetLeft;

            if (jQuery.support.doesNotIncludeMarginInBodyOffset) {
                top += parseFloat(jQuery.css(body, "marginTop")) || 0;
                left += parseFloat(jQuery.css(body, "marginLeft")) || 0;
            }

            return {
                top: top,
                left: left
            };
        },

        setOffset: function(elem, options, i) {
            var position = jQuery.css(elem, "position");

            // set position first, in-case top/left are set even on static elem
            if (position === "static") {
                elem.style.position = "relative";
            }

            var curElem = jQuery(elem),
                curOffset = curElem.offset(),
                curCSSTop = jQuery.css(elem, "top"),
                curCSSLeft = jQuery.css(elem, "left"),
                calculatePosition = (position === "absolute" || position === "fixed") && jQuery.inArray("auto", [curCSSTop, curCSSLeft]) > -1,
                props = {},
                curPosition = {},
                curTop, curLeft;

            // need to be able to calculate position if either top or left is auto and position is either absolute or fixed
            if (calculatePosition) {
                curPosition = curElem.position();
                curTop = curPosition.top;
                curLeft = curPosition.left;
            } else {
                curTop = parseFloat(curCSSTop) || 0;
                curLeft = parseFloat(curCSSLeft) || 0;
            }

            if (jQuery.isFunction(options)) {
                options = options.call(elem, i, curOffset);
            }

            if (options.top != null) {
                props.top = (options.top - curOffset.top) + curTop;
            }
            if (options.left != null) {
                props.left = (options.left - curOffset.left) + curLeft;
            }

            if ("using" in options) {
                options.using.call(elem, props);
            } else {
                curElem.css(props);
            }
        }
    };


    jQuery.fn.extend({

        position: function() {
            if (!this[0]) {
                return null;
            }

            var elem = this[0],

                // Get *real* offsetParent
                offsetParent = this.offsetParent(),

                // Get correct offsets
                offset = this.offset(),
                parentOffset = rroot.test(offsetParent[0].nodeName) ? {
                    top: 0,
                    left: 0
                } : offsetParent.offset();

            // Subtract element margins
            // note: when an element has margin: auto the offsetLeft and marginLeft
            // are the same in Safari causing offset.left to incorrectly be 0
            offset.top -= parseFloat(jQuery.css(elem, "marginTop")) || 0;
            offset.left -= parseFloat(jQuery.css(elem, "marginLeft")) || 0;

            // Add offsetParent borders
            parentOffset.top += parseFloat(jQuery.css(offsetParent[0], "borderTopWidth")) || 0;
            parentOffset.left += parseFloat(jQuery.css(offsetParent[0], "borderLeftWidth")) || 0;

            // Subtract the two offsets
            return {
                top: offset.top - parentOffset.top,
                left: offset.left - parentOffset.left
            };
        },

        offsetParent: function() {
            return this.map(function() {
                var offsetParent = this.offsetParent || document.body;
                while (offsetParent && (!rroot.test(offsetParent.nodeName) && jQuery.css(offsetParent, "position") === "static")) {
                    offsetParent = offsetParent.offsetParent;
                }
                return offsetParent;
            });
        }
    });


    // Create scrollLeft and scrollTop methods
    jQuery.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(method, prop) {
        var top = /Y/.test(prop);

        jQuery.fn[method] = function(val) {
            return jQuery.access(this, function(elem, method, val) {
                var win = getWindow(elem);

                if (val === undefined) {
                    return win ? (prop in win) ? win[prop] : jQuery.support.boxModel && win.document.documentElement[method] || win.document.body[method] : elem[method];
                }

                if (win) {
                    win.scrollTo(!top ? val : jQuery(win).scrollLeft(), top ? val : jQuery(win).scrollTop());

                } else {
                    elem[method] = val;
                }
            }, method, val, arguments.length, null);
        };
    });

    function getWindow(elem) {
        return jQuery.isWindow(elem) ? elem : elem.nodeType === 9 ? elem.defaultView || elem.parentWindow : false;
    }




    // Create width, height, innerHeight, innerWidth, outerHeight and outerWidth methods
    jQuery.each({
        Height: "height",
        Width: "width"
    }, function(name, type) {
        var clientProp = "client" + name,
            scrollProp = "scroll" + name,
            offsetProp = "offset" + name;

        // innerHeight and innerWidth
        jQuery.fn["inner" + name] = function() {
            var elem = this[0];
            return elem ? elem.style ? parseFloat(jQuery.css(elem, type, "padding")) : this[type]() : null;
        };

        // outerHeight and outerWidth
        jQuery.fn["outer" + name] = function(margin) {
            var elem = this[0];
            return elem ? elem.style ? parseFloat(jQuery.css(elem, type, margin ? "margin" : "border")) : this[type]() : null;
        };

        jQuery.fn[type] = function(value) {
            return jQuery.access(this, function(elem, type, value) {
                var doc, docElemProp, orig, ret;

                if (jQuery.isWindow(elem)) {
                    // 3rd condition allows Nokia support, as it supports the docElem prop but not CSS1Compat
                    doc = elem.document;
                    docElemProp = doc.documentElement[clientProp];
                    return jQuery.support.boxModel && docElemProp || doc.body && doc.body[clientProp] || docElemProp;
                }

                // Get document width or height
                if (elem.nodeType === 9) {
                    // Either scroll[Width/Height] or offset[Width/Height], whichever is greater
                    doc = elem.documentElement;

                    // when a window > document, IE6 reports a offset[Width/Height] > client[Width/Height]
                    // so we can't use max, as it'll choose the incorrect offset[Width/Height]
                    // instead we use the correct client[Width/Height]
                    // support:IE6
                    if (doc[clientProp] >= doc[scrollProp]) {
                        return doc[clientProp];
                    }

                    return Math.max(
                    elem.body[scrollProp], doc[scrollProp], elem.body[offsetProp], doc[offsetProp]);
                }

                // Get width or height on the element
                if (value === undefined) {
                    orig = jQuery.css(elem, type);
                    ret = parseFloat(orig);
                    return jQuery.isNumeric(ret) ? ret : orig;
                }

                // Set the width or height on the element
                jQuery(elem).css(type, value);
            }, type, value, arguments.length, null);
        };
    });




    // Expose jQuery to the global object
    window.jQuery = window.$ = jQuery;

    // Expose jQuery as an AMD module, but only for AMD loaders that
    // understand the issues with loading multiple versions of jQuery
    // in a page that all might call define(). The loader will indicate
    // they have special allowances for multiple jQuery versions by
    // specifying define.amd.jQuery = true. Register as a named module,
    // since jQuery can be concatenated with other files that may use define,
    // but not use a proper concatenation script that understands anonymous
    // AMD modules. A named AMD is safest and most robust way to register.
    // Lowercase jquery is used because AMD module names are derived from
    // file names, and jQuery is normally delivered in a lowercase file name.
    // Do this after creating the global so that if an AMD module wants to call
    // noConflict to hide this version of jQuery, it will work.
    if (typeof define === "function" && define.amd && define.amd.jQuery) {
        define("jquery", [], function() {
            return jQuery;
        });
    }



})(window);

/*
    json2.js
    2011-10-19

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.
var JSON;
if (!JSON) {
    JSON = {};
}

(function() {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function(key) {

            return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + 'Z' : null;
        };

        String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function(key) {
            return this.valueOf();
        };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap, indent, meta = { // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"': '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.
        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function(a) {
            var c = meta[a];
            return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

        // Produce a string from holder[key].
        var i, // The loop counter.
        k, // The member key.
        v, // The member value.
        length, mind = gap,
            partial, value = holder[key];

        // If the value has a toJSON method, call it to obtain a replacement value.
        if (value && typeof value === 'object' && typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.
        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

        // What happens next depends on the value's type.
        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

            // JSON numbers must be finite. Encode non-finite numbers as null.
            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

            // If the value is a boolean or null, convert it to a string. Note:
            // typeof null does not produce 'null'. The case is included here in
            // the remote chance that this gets fixed someday.
            return String(value);

            // If the type is 'object', we might be dealing with an object or an array or
            // null.
        case 'object':

            // Due to a specification blunder in ECMAScript, typeof null is 'object',
            // so watch out for that case.
            if (!value) {
                return 'null';
            }

            // Make an array to hold the partial results of stringifying this object value.
            gap += indent;
            partial = [];

            // Is the value an array?
            if (Object.prototype.toString.apply(value) === '[object Array]') {

                // The value is an array. Stringify every element. Use null as a placeholder
                // for non-JSON values.
                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

                // Join all of the elements together, separated with commas, and wrap them in
                // brackets.
                v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

            // If the replacer is an array, use it to select the members to be stringified.
            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

                // Otherwise, iterate through all of the keys in the object.
                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

            // Join all of the member texts together, separated with commas,
            // and wrap them in braces.
            v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

    // If the JSON object does not yet have a stringify method, give it one.
    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function(value, replacer, space) {

            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.
            var i;
            gap = '';
            indent = '';

            // If the space parameter is a number, make an indent string containing that
            // many spaces.
            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

                // If the space parameter is a string, it will be used as the indent string.
            } else if (typeof space === 'string') {
                indent = space;
            }

            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.
            rep = replacer;
            if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

            // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.
            return str('', {
                '': value
            });
        };
    }


    // If the JSON object does not yet have a parse method, give it one.
    if (typeof JSON.parse !== 'function') {
        JSON.parse = function(text, reviver) {

            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.
            var j;

            function walk(holder, key) {

                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.
                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.
            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function(a) {
                    return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

            // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with '()' and 'new'
            // because they can cause invocation, and '=' because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.

            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
            // replace all simple value tokens with ']' characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or ']' or
            // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
            if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.
                j = eval('(' + text + ')');

                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.
                return typeof reviver === 'function' ? walk({
                    '': j
                }, '') : j;
            }

            // If the text is not JSON parseable, then a SyntaxError is thrown.
            throw new SyntaxError('JSON.parse');
        };
    }
}());

/**
 * Lawnchair!
 * ---
 * clientside json store
 *
 */
var Lawnchair = function() {
        // lawnchair requires json 
        if (!JSON) throw 'JSON unavailable! Include http://www.json.org/json2.js to fix.'
        // options are optional; callback is not
        if (arguments.length <= 2 && arguments.length > 0) {
            var callback = (typeof arguments[0] === 'function') ? arguments[0] : arguments[1],
                options = (typeof arguments[0] === 'function') ? {} : arguments[0]
        } else {
            throw 'Incorrect # of ctor args!'
        }
        // TODO perhaps allow for pub/sub instead?
        if (typeof callback !== 'function') throw 'No callback was provided';
        // ensure we init with this set to the Lawnchair prototype
        var self = (!(this instanceof Lawnchair)) ? new Lawnchair(options, callback) : this
        // default configuration 
        self.record = options.record || 'record' // default for records
        self.name = options.name || 'records' // default name for underlying store
        // mixin first valid  adapter
        var adapter
        // if the adapter is passed in we try to load that only
        if (options.adapter) {
            adapter = Lawnchair.adapters[self.indexOf(Lawnchair.adapters, options.adapter)]
            adapter = adapter.valid() ? adapter : undefined
            // otherwise find the first valid adapter for this env
        } else {
            for (var i = 0, l = Lawnchair.adapters.length; i < l; i++) {
                adapter = Lawnchair.adapters[i].valid() ? Lawnchair.adapters[i] : undefined
                if (adapter) break
            }
        }
        // we have failed 
        if (!adapter) throw 'No valid adapter.'
        // yay! mixin the adapter 
        for (var j in adapter)
        self[j] = adapter[j]
        // call init for each mixed in plugin
        for (var i = 0, l = Lawnchair.plugins.length; i < l; i++)
        Lawnchair.plugins[i].call(self)
        // init the adapter 
        self.init(options, callback)
        // called as a function or as a ctor with new always return an instance
        return self
    }
Lawnchair.adapters = []
/** 
 * queues an adapter for mixin
 * ===
 * - ensures an adapter conforms to a specific interface
 *
 */
Lawnchair.adapter = function(id, obj) {
    // add the adapter id to the adapter obj
    // ugly here for a  cleaner dsl for implementing adapters
    obj['adapter'] = id
    // methods required to implement a lawnchair adapter 
    var implementing = 'adapter valid init keys save batch get exists all remove nuke'.split(' '),
        indexOf = this.prototype.indexOf
        // mix in the adapter   
    for (var i in obj) {
        if (indexOf(implementing, i) === -1) throw 'Invalid adapter! Nonstandard method: ' + i
    }
    // if we made it this far the adapter interface is valid 
    Lawnchair.adapters.push(obj)
}
Lawnchair.plugins = []

/**
 * generic shallow extension for plugins
 * ===
 * - if an init method is found it registers it to be called when the lawnchair is inited
 * - yes we could use hasOwnProp but nobody here is an asshole
 */
Lawnchair.plugin = function(obj) {
    for (var i in obj)
    i === 'init' ? Lawnchair.plugins.push(obj[i]) : this.prototype[i] = obj[i]
}
/**
 * helpers
 *
 */
Lawnchair.prototype = {
    isArray: Array.isArray ||
    function(o) {
        return Object.prototype.toString.call(o) === '[object Array]'
    },
    /**
     * this code exists for ie8... for more background see:
     * http://www.flickr.com/photos/westcoastlogic/5955365742/in/photostream
     */
    indexOf: function(ary, item, i, l) {
        if (ary.indexOf) return ary.indexOf(item);
        for (i = 0, l = ary.length; i < l; i++) if (ary[i] === item) return i;
        return -1
    },
    // awesome shorthand callbacks as strings. this is shameless theft from dojo.
    lambda: function(callback) {
        return this.fn(this.record, callback)
    },
    // first stab at named parameters for terse callbacks; dojo: first != best // ;D
    fn: function(name, callback) {
        return typeof callback == 'string' ? new Function(name, callback) : callback
    },
    // returns a unique identifier (by way of Backbone.localStorage.js)
    // TODO investigate smaller UUIDs to cut on storage cost
    uuid: function() {
        var S4 = function() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    },
    // a classic iterator
    each: function(callback) {
        var cb = this.lambda(callback)
        // iterate from chain
        if (this.__results) {
            for (var i = 0, l = this.__results.length; i < l; i++) cb.call(this, this.__results[i], i)
        }
        // otherwise iterate the entire collection 
        else {
            this.all(function(r) {
                for (var i = 0, l = r.length; i < l; i++) cb.call(this, r[i], i)
            })
        }
        return this
    }
    // --
};

(function() {
    /**!
     * dom storage adapter
     * ===
     * - originally authored by Joseph Pecoraro
     *
     */
    //
    // TODO does it make sense to be chainable all over the place?
    // chainable: nuke, remove, all, get, save, all    
    // not chainable: valid, keys
    //


    function gen_adapter(storageKey) {
        return {
            // ensure we are in an env with localStorage 
            valid: function() {
                return !!window[storageKey]
            },
            init: function(options, callback) {
                // yay dom!
                this.storage = window[storageKey]
                // indexer helper code
                var self = this
                // the indexer is an encapsulation of the helpers needed to keep an ordered index of the keys
                this.indexer = {
                    // the key
                    key: self.name + '._index_',
                    // returns the index
                    all: function() {
                        var a = JSON.parse(self.storage.getItem(this.key))
                        if (a == null) self.storage.setItem(this.key, JSON.stringify([])) // lazy init
                        return JSON.parse(self.storage.getItem(this.key))
                    },
                    // adds a key to the index
                    add: function(key) {
                        var a = this.all()
                        a.push(key)
                        self.storage.setItem(this.key, JSON.stringify(a))
                    },
                    // deletes a key from the index
                    del: function(key) {
                        var a = this.all(),
                            r = []
                            // FIXME this is crazy inefficient but I'm in a strata meeting and half concentrating
                        for (var i = 0, l = a.length; i < l; i++) {
                            if (a[i] != key) r.push(a[i])
                        }
                        self.storage.setItem(this.key, JSON.stringify(r))
                    },
                    // returns index for a key
                    find: function(key) {
                        var a = this.all()
                        for (var i = 0, l = a.length; i < l; i++) {
                            if (key === a[i]) return i
                        }
                        return false
                    }
                }
                if (callback) this.fn(this.name, callback).call(this, this)
            },
            save: function(obj, callback) {
                var key = obj.key || this.uuid()
                // if the key is not in the index push it on
                if (!this.indexer.find(key)) this.indexer.add(key)
                // now we kil the key and use it in the store colleciton    
                delete obj.key;
                this.storage.setItem(key, JSON.stringify(obj))
                if (callback) {
                    obj.key = key
                    this.lambda(callback).call(this, obj)
                }
                return this
            },
            batch: function(ary, callback) {
                var saved = []
                // not particularily efficient but this is more for sqlite situations
                for (var i = 0, l = ary.length; i < l; i++) {
                    this.save(ary[i], function(r) {
                        saved.push(r)
                    })
                }
                if (callback) this.lambda(callback).call(this, saved);
                return this
            },
            // accepts [options], callback
            keys: function() {
                // TODO support limit/offset options here
                var limit = options.limit || null,
                    offset = options.offset || 0
                if (callback) this.lambda(callback).call(this, this.indexer.all())
            },
            get: function(key, callback) {
                if (this.isArray(key)) {
                    var r = []
                    for (var i = 0, l = key.length; i < l; i++) {
                        var obj = JSON.parse(this.storage.getItem(key[i]))
                        if (obj) {
                            obj.key = key[i]
                            r.push(obj)
                        }
                    }
                    if (callback) this.lambda(callback).call(this, r)
                } else {
                    var obj = JSON.parse(this.storage.getItem(key))
                    if (obj) obj.key = key
                    if (callback) this.lambda(callback).call(this, obj)
                }
                return this
            },
            // NOTE adapters cannot set this.__results but plugins do
            // this probably should be reviewed
            all: function(callback) {
                var idx = this.indexer.all(),
                    r = [],
                    o
                for (var i = 0, l = idx.length; i < l; i++) {
                    o = JSON.parse(this.storage.getItem(idx[i]))
                    o.key = idx[i]
                    r.push(o)
                }
                if (callback) this.fn(this.name, callback).call(this, r);
                return this
            },
            remove: function(keyOrObj, callback) {
                var key = typeof keyOrObj === 'string' ? keyOrObj : keyOrObj.key
                this.indexer.del(key)
                this.storage.removeItem(key)
                if (callback) this.lambda(callback).call(this);
                return this
            },
            nuke: function(callback) {
                this.all(function(r) {
                    for (var i = 0, l = r.length; i < l; i++) {
                        this.remove(r[i]);
                    }
                    if (callback) this.lambda(callback).call(this)
                })
                return this
            }
        }
    }

    Lawnchair.adapter('dom', gen_adapter("localStorage"));

    /**!
     * ie userdata adaptor
     *
     */
    Lawnchair.adapter('ie-userdata', {
        valid: function() {
            return typeof(document.body.addBehavior) != 'undefined';
        },
        init: function() {
            var s = document.createElement('span');
            s.style.behavior = 'url(\'#default#userData\')';
            s.style.position = 'absolute';
            s.style.left = 10000;
            document.body.appendChild(s);
            this.storage = s;
            this.storage.load('lawnchair');
        },
        get: function(key, callback) {
            var obj = JSON.parse(this.storage.getAttribute(key) || 'null');
            if (obj) {
                obj.key = key;
            }
            if (callback) callback(obj);
        },
        save: function(obj, callback) {
            var id = obj.key || 'lc' + this.uuid();
            delete obj.key;
            this.storage.setAttribute(id, JSON.stringify(obj));
            this.storage.save('lawnchair');
            if (callback) {
                obj.key = id;
                callback(obj);
            }
        },
        all: function(callback) {
            var cb = this.terseToVerboseCallback(callback);
            var ca = this.storage.XMLDocument.firstChild.attributes;
            var yar = [];
            var v, o;
            // yo ho yo ho a pirates life for me
            for (var i = 0, l = ca.length; i < l; i++) {
                v = ca[i];
                o = JSON.parse(v.nodeValue || 'null');
                if (o) {
                    o.key = v.nodeName;
                    yar.push(o);
                }
            }
            if (cb) cb(yar);
        },
        remove: function(keyOrObj, callback) {
            var key = (typeof keyOrObj == 'string') ? keyOrObj : keyOrObj.key;
            this.storage.removeAttribute(key);
            this.storage.save('lawnchair');
            if (callback) callback();
        },
        nuke: function(callback) {
            var that = this;
            this.all(function(r) {
                for (var i = 0, l = r.length; i < l; i++) {
                    if (r[i].key) that.remove(r[i].key);
                }
                if (callback) callback();
            });
        }
    });

    Lawnchair.adapter('memory', (function() {
        var storage = {},
            index = []
        return {
            valid: function() {
                return true
            },
            init: function(opts, cb) {
                this.fn(this.name, cb).call(this, this)
                return this
            },
            keys: function() {
                return index
            },
            save: function(obj, cb) {
                var key = obj.key || this.uuid()

                if (obj.key) delete obj.key

                this.exists(key, function(exists) {
                    if (!exists) index.push(key);
                    storage[key] = obj

                    if (cb) {
                        obj.key = key
                        this.lambda(cb).call(this, obj)
                    }
                })
                return this
            },
            batch: function(objs, cb) {
                var r = []
                for (var i = 0, l = objs.length; i < l; i++) {
                    this.save(objs[i], function(record) {
                        r.push(record)
                    })
                }
                if (cb) this.lambda(cb).call(this, r);
                return this
            },
            get: function(keyOrArray, cb) {
                var r;
                if (this.isArray(keyOrArray)) {
                    r = []
                    for (var i = 0, l = keyOrArray.length; i < l; i++) {
                        r.push(storage[keyOrArray[i]])
                    }
                } else {
                    r = storage[keyOrArray]
                    if (r) r.key = keyOrArray
                }
                if (cb) this.lambda(cb).call(this, r);
                return this
            },
            exists: function(key, cb) {
                this.lambda(cb).call(this, !! (storage[key]))
                return this
            },
            all: function(cb) {
                var r = []
                for (var i = 0, l = index.length; i < l; i++) {
                    var obj = storage[index[i]]
                    obj.key = index[i]
                    r.push(obj)
                }
                this.fn(this.name, cb).call(this, r)
                return this
            },
            remove: function(keyOrArray, cb) {
                var del = this.isArray(keyOrArray) ? keyOrArray : [keyOrArray]
                for (var i = 0, l = del.length; i < l; i++) {
                    delete storage[del[i]]
                    index.splice(Lawnchair.prototype.indexOf(index, del[i]), 1)
                }
                if (cb) this.lambda(cb).call(this);
                return this
            },
            nuke: function(cb) {
                storage = {}
                index = []
                if (cb) this.lambda(cb).call(this);
                return this
            }
        }
        /////
    })());;

    Lawnchair.adapter('session', gen_adapter("sessionStorage"));
})();


//     Underscore.js 1.2.1
//     (c) 2011 Jeremy Ashkenas, DocumentCloud Inc.
//     Underscore is freely distributable under the MIT license.
//     Portions of Underscore are inspired or borrowed from Prototype,
//     Oliver Steele's Functional, and John Resig's Micro-Templating.
//     For all details and documentation:
//     http://documentcloud.github.com/underscore
(function() {

    // Baseline setup
    // --------------

    // Establish the root object, `window` in the browser, or `global` on the server.
    var root = this;

    // Save the previous value of the `_` variable.
    var previousUnderscore = root._;

    // Establish the object that gets returned to break out of a loop iteration.
    var breaker = {};

    // Save bytes in the minified (but not gzipped) version:
    var ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        FuncProto = Function.prototype;

    // Create quick reference variables for speed access to core prototypes.
    var slice = ArrayProto.slice,
        unshift = ArrayProto.unshift,
        toString = ObjProto.toString,
        hasOwnProperty = ObjProto.hasOwnProperty;

    // All **ECMAScript 5** native function implementations that we hope to use
    // are declared here.
    var
    nativeForEach = ArrayProto.forEach,
        nativeMap = ArrayProto.map,
        nativeReduce = ArrayProto.reduce,
        nativeReduceRight = ArrayProto.reduceRight,
        nativeFilter = ArrayProto.filter,
        nativeEvery = ArrayProto.every,
        nativeSome = ArrayProto.some,
        nativeIndexOf = ArrayProto.indexOf,
        nativeLastIndexOf = ArrayProto.lastIndexOf,
        nativeIsArray = Array.isArray,
        nativeKeys = Object.keys,
        nativeBind = FuncProto.bind;

    // Create a safe reference to the Underscore object for use below.
    var _ = function(obj) {
            return new wrapper(obj);
        };

    // Export the Underscore object for **Node.js** and **"CommonJS"**, with
    // backwards-compatibility for the old `require()` API. If we're not in
    // CommonJS, add `_` to the global object.
    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = _;
        }
        exports._ = _;
    } else if (typeof define === 'function' && define.amd) {
        // Register as a named module with AMD.
        define('underscore', function() {
            return _;
        });
    } else {
        // Exported as a string, for Closure Compiler "advanced" mode.
        root['_'] = _;
    }

    // Current version.
    _.VERSION = '1.2.1';

    // Collection Functions
    // --------------------

    // The cornerstone, an `each` implementation, aka `forEach`.
    // Handles objects with the built-in `forEach`, arrays, and raw objects.
    // Delegates to **ECMAScript 5**'s native `forEach` if available.
    var each = _.each = _.forEach = function(obj, iterator, context) {
            if (obj == null) return;
            if (nativeForEach && obj.forEach === nativeForEach) {
                obj.forEach(iterator, context);
            } else if (obj.length === +obj.length) {
                for (var i = 0, l = obj.length; i < l; i++) {
                    if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) return;
                }
            } else {
                for (var key in obj) {
                    if (hasOwnProperty.call(obj, key)) {
                        if (iterator.call(context, obj[key], key, obj) === breaker) return;
                    }
                }
            }
        };

    // Return the results of applying the iterator to each element.
    // Delegates to **ECMAScript 5**'s native `map` if available.
    _.map = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
        each(obj, function(value, index, list) {
            results[results.length] = iterator.call(context, value, index, list);
        });
        return results;
    };

    // **Reduce** builds up a single result from a list of values, aka `inject`,
    // or `foldl`. Delegates to **ECMAScript 5**'s native `reduce` if available.
    _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
        var initial = memo !== void 0;
        if (obj == null) obj = [];
        if (nativeReduce && obj.reduce === nativeReduce) {
            if (context) iterator = _.bind(iterator, context);
            return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
        }
        each(obj, function(value, index, list) {
            if (!initial) {
                memo = value;
                initial = true;
            } else {
                memo = iterator.call(context, memo, value, index, list);
            }
        });
        if (!initial) throw new TypeError("Reduce of empty array with no initial value");
        return memo;
    };

    // The right-associative version of reduce, also known as `foldr`.
    // Delegates to **ECMAScript 5**'s native `reduceRight` if available.
    _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
        if (obj == null) obj = [];
        if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
            if (context) iterator = _.bind(iterator, context);
            return memo !== void 0 ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
        }
        var reversed = (_.isArray(obj) ? obj.slice() : _.toArray(obj)).reverse();
        return _.reduce(reversed, iterator, memo, context);
    };

    // Return the first value which passes a truth test. Aliased as `detect`.
    _.find = _.detect = function(obj, iterator, context) {
        var result;
        any(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) {
                result = value;
                return true;
            }
        });
        return result;
    };

    // Return all the elements that pass a truth test.
    // Delegates to **ECMAScript 5**'s native `filter` if available.
    // Aliased as `select`.
    _.filter = _.select = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
        each(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) results[results.length] = value;
        });
        return results;
    };

    // Return all the elements for which a truth test fails.
    _.reject = function(obj, iterator, context) {
        var results = [];
        if (obj == null) return results;
        each(obj, function(value, index, list) {
            if (!iterator.call(context, value, index, list)) results[results.length] = value;
        });
        return results;
    };

    // Determine whether all of the elements match a truth test.
    // Delegates to **ECMAScript 5**'s native `every` if available.
    // Aliased as `all`.
    _.every = _.all = function(obj, iterator, context) {
        var result = true;
        if (obj == null) return result;
        if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
        each(obj, function(value, index, list) {
            if (!(result = result && iterator.call(context, value, index, list))) return breaker;
        });
        return result;
    };

    // Determine if at least one element in the object matches a truth test.
    // Delegates to **ECMAScript 5**'s native `some` if available.
    // Aliased as `any`.
    var any = _.some = _.any = function(obj, iterator, context) {
            iterator = iterator || _.identity;
            var result = false;
            if (obj == null) return result;
            if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
            each(obj, function(value, index, list) {
                if (result |= iterator.call(context, value, index, list)) return breaker;
            });
            return !!result;
        };

    // Determine if a given value is included in the array or object using `===`.
    // Aliased as `contains`.
    _.include = _.contains = function(obj, target) {
        var found = false;
        if (obj == null) return found;
        if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
        found = any(obj, function(value) {
            if (value === target) return true;
        });
        return found;
    };

    // Invoke a method (with arguments) on every item in a collection.
    _.invoke = function(obj, method) {
        var args = slice.call(arguments, 2);
        return _.map(obj, function(value) {
            return (method.call ? method || value : value[method]).apply(value, args);
        });
    };

    // Convenience version of a common use case of `map`: fetching a property.
    _.pluck = function(obj, key) {
        return _.map(obj, function(value) {
            return value[key];
        });
    };

    // Return the maximum element or (element-based computation).
    _.max = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj)) return Math.max.apply(Math, obj);
        if (!iterator && _.isEmpty(obj)) return -Infinity;
        var result = {
            computed: -Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed >= result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };

    // Return the minimum element (or element-based computation).
    _.min = function(obj, iterator, context) {
        if (!iterator && _.isArray(obj)) return Math.min.apply(Math, obj);
        if (!iterator && _.isEmpty(obj)) return Infinity;
        var result = {
            computed: Infinity
        };
        each(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed < result.computed && (result = {
                value: value,
                computed: computed
            });
        });
        return result.value;
    };

    // Shuffle an array.
    _.shuffle = function(obj) {
        var shuffled = [],
            rand;
        each(obj, function(value, index, list) {
            if (index == 0) {
                shuffled[0] = value;
            } else {
                rand = Math.floor(Math.random() * (index + 1));
                shuffled[index] = shuffled[rand];
                shuffled[rand] = value;
            }
        });
        return shuffled;
    };

    // Sort the object's values by a criterion produced by an iterator.
    _.sortBy = function(obj, iterator, context) {
        return _.pluck(_.map(obj, function(value, index, list) {
            return {
                value: value,
                criteria: iterator.call(context, value, index, list)
            };
        }).sort(function(left, right) {
            var a = left.criteria,
                b = right.criteria;
            return a < b ? -1 : a > b ? 1 : 0;
        }), 'value');
    };

    // Groups the object's values by a criterion. Pass either a string attribute
    // to group by, or a function that returns the criterion.
    _.groupBy = function(obj, val) {
        var result = {};
        var iterator = _.isFunction(val) ? val : function(obj) {
                return obj[val];
            };
        each(obj, function(value, index) {
            var key = iterator(value, index);
            (result[key] || (result[key] = [])).push(value);
        });
        return result;
    };

    // Use a comparator function to figure out at what index an object should
    // be inserted so as to maintain order. Uses binary search.
    _.sortedIndex = function(array, obj, iterator) {
        iterator || (iterator = _.identity);
        var low = 0,
            high = array.length;
        while (low < high) {
            var mid = (low + high) >> 1;
            iterator(array[mid]) < iterator(obj) ? low = mid + 1 : high = mid;
        }
        return low;
    };

    // Safely convert anything iterable into a real, live array.
    _.toArray = function(iterable) {
        if (!iterable) return [];
        if (iterable.toArray) return iterable.toArray();
        if (_.isArray(iterable)) return slice.call(iterable);
        if (_.isArguments(iterable)) return slice.call(iterable);
        return _.values(iterable);
    };

    // Return the number of elements in an object.
    _.size = function(obj) {
        return _.toArray(obj).length;
    };

    // Array Functions
    // ---------------

    // Get the first element of an array. Passing **n** will return the first N
    // values in the array. Aliased as `head`. The **guard** check allows it to work
    // with `_.map`.
    _.first = _.head = function(array, n, guard) {
        return (n != null) && !guard ? slice.call(array, 0, n) : array[0];
    };

    // Returns everything but the last entry of the array. Especcialy useful on
    // the arguments object. Passing **n** will return all the values in
    // the array, excluding the last N. The **guard** check allows it to work with
    // `_.map`.
    _.initial = function(array, n, guard) {
        return slice.call(array, 0, array.length - ((n == null) || guard ? 1 : n));
    };

    // Get the last element of an array. Passing **n** will return the last N
    // values in the array. The **guard** check allows it to work with `_.map`.
    _.last = function(array, n, guard) {
        return (n != null) && !guard ? slice.call(array, array.length - n) : array[array.length - 1];
    };

    // Returns everything but the first entry of the array. Aliased as `tail`.
    // Especially useful on the arguments object. Passing an **index** will return
    // the rest of the values in the array from that index onward. The **guard**
    // check allows it to work with `_.map`.
    _.rest = _.tail = function(array, index, guard) {
        return slice.call(array, (index == null) || guard ? 1 : index);
    };

    // Trim out all falsy values from an array.
    _.compact = function(array) {
        return _.filter(array, function(value) {
            return !!value;
        });
    };

    // Return a completely flattened version of an array.
    _.flatten = function(array, shallow) {
        return _.reduce(array, function(memo, value) {
            if (_.isArray(value)) return memo.concat(shallow ? value : _.flatten(value));
            memo[memo.length] = value;
            return memo;
        }, []);
    };

    // Return a version of the array that does not contain the specified value(s).
    _.without = function(array) {
        return _.difference(array, slice.call(arguments, 1));
    };

    // Produce a duplicate-free version of the array. If the array has already
    // been sorted, you have the option of using a faster algorithm.
    // Aliased as `unique`.
    _.uniq = _.unique = function(array, isSorted, iterator) {
        var initial = iterator ? _.map(array, iterator) : array;
        var result = [];
        _.reduce(initial, function(memo, el, i) {
            if (0 == i || (isSorted === true ? _.last(memo) != el : !_.include(memo, el))) {
                memo[memo.length] = el;
                result[result.length] = array[i];
            }
            return memo;
        }, []);
        return result;
    };

    // Produce an array that contains the union: each distinct element from all of
    // the passed-in arrays.
    _.union = function() {
        return _.uniq(_.flatten(arguments, true));
    };

    // Produce an array that contains every item shared between all the
    // passed-in arrays. (Aliased as "intersect" for back-compat.)
    _.intersection = _.intersect = function(array) {
        var rest = slice.call(arguments, 1);
        return _.filter(_.uniq(array), function(item) {
            return _.every(rest, function(other) {
                return _.indexOf(other, item) >= 0;
            });
        });
    };

    // Take the difference between one array and another.
    // Only the elements present in just the first array will remain.
    _.difference = function(array, other) {
        return _.filter(array, function(value) {
            return !_.include(other, value);
        });
    };

    // Zip together multiple lists into a single array -- elements that share
    // an index go together.
    _.zip = function() {
        var args = slice.call(arguments);
        var length = _.max(_.pluck(args, 'length'));
        var results = new Array(length);
        for (var i = 0; i < length; i++) results[i] = _.pluck(args, "" + i);
        return results;
    };

    // If the browser doesn't supply us with indexOf (I'm looking at you, **MSIE**),
    // we need this function. Return the position of the first occurrence of an
    // item in an array, or -1 if the item is not included in the array.
    // Delegates to **ECMAScript 5**'s native `indexOf` if available.
    // If the array is large and already in sort order, pass `true`
    // for **isSorted** to use binary search.
    _.indexOf = function(array, item, isSorted) {
        if (array == null) return -1;
        var i, l;
        if (isSorted) {
            i = _.sortedIndex(array, item);
            return array[i] === item ? i : -1;
        }
        if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item);
        for (i = 0, l = array.length; i < l; i++) if (array[i] === item) return i;
        return -1;
    };

    // Delegates to **ECMAScript 5**'s native `lastIndexOf` if available.
    _.lastIndexOf = function(array, item) {
        if (array == null) return -1;
        if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) return array.lastIndexOf(item);
        var i = array.length;
        while (i--) if (array[i] === item) return i;
        return -1;
    };

    // Generate an integer Array containing an arithmetic progression. A port of
    // the native Python `range()` function. See
    // [the Python documentation](http://docs.python.org/library/functions.html#range).
    _.range = function(start, stop, step) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }
        step = arguments[2] || 1;

        var len = Math.max(Math.ceil((stop - start) / step), 0);
        var idx = 0;
        var range = new Array(len);

        while (idx < len) {
            range[idx++] = start;
            start += step;
        }

        return range;
    };

    // Function (ahem) Functions
    // ------------------

    // Reusable constructor function for prototype setting.
    var ctor = function() {};

    // Create a function bound to a given object (assigning `this`, and arguments,
    // optionally). Binding with arguments is also known as `curry`.
    // Delegates to **ECMAScript 5**'s native `Function.bind` if available.
    // We check for `func.bind` first, to fail fast when `func` is undefined.
    _.bind = function bind(func, context) {
        var bound, args;
        if (func.bind === nativeBind && nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
        if (!_.isFunction(func)) throw new TypeError;
        args = slice.call(arguments, 2);
        return bound = function() {
            if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
            ctor.prototype = func.prototype;
            var self = new ctor;
            var result = func.apply(self, args.concat(slice.call(arguments)));
            if (Object(result) === result) return result;
            return self;
        };
    };

    // Bind all of an object's methods to that object. Useful for ensuring that
    // all callbacks defined on an object belong to it.
    _.bindAll = function(obj) {
        var funcs = slice.call(arguments, 1);
        if (funcs.length == 0) funcs = _.functions(obj);
        each(funcs, function(f) {
            obj[f] = _.bind(obj[f], obj);
        });
        return obj;
    };

    // Memoize an expensive function by storing its results.
    _.memoize = function(func, hasher) {
        var memo = {};
        hasher || (hasher = _.identity);
        return function() {
            var key = hasher.apply(this, arguments);
            return hasOwnProperty.call(memo, key) ? memo[key] : (memo[key] = func.apply(this, arguments));
        };
    };

    // Delays a function for the given number of milliseconds, and then calls
    // it with the arguments supplied.
    _.delay = function(func, wait) {
        var args = slice.call(arguments, 2);
        return setTimeout(function() {
            return func.apply(func, args);
        }, wait);
    };

    // Defers a function, scheduling it to run after the current call stack has
    // cleared.
    _.defer = function(func) {
        return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1)));
    };

    // Returns a function, that, when invoked, will only be triggered at most once
    // during a given window of time.
    _.throttle = function(func, wait) {
        var timeout, context, args, throttling, finishThrottle;
        finishThrottle = _.debounce(function() {
            throttling = false;
        }, wait);
        return function() {
            context = this;
            args = arguments;
            var throttler = function() {
                    timeout = null;
                    func.apply(context, args);
                    finishThrottle();
                };
            if (!timeout) timeout = setTimeout(throttler, wait);
            if (!throttling) func.apply(context, args);
            if (finishThrottle) finishThrottle();
            throttling = true;
        };
    };

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds.
    _.debounce = function(func, wait) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var throttler = function() {
                    timeout = null;
                    func.apply(context, args);
                };
            clearTimeout(timeout);
            timeout = setTimeout(throttler, wait);
        };
    };

    // Returns a function that will be executed at most one time, no matter how
    // often you call it. Useful for lazy initialization.
    _.once = function(func) {
        var ran = false,
            memo;
        return function() {
            if (ran) return memo;
            ran = true;
            return memo = func.apply(this, arguments);
        };
    };

    // Returns the first function passed as an argument to the second,
    // allowing you to adjust arguments, run code before and after, and
    // conditionally execute the original function.
    _.wrap = function(func, wrapper) {
        return function() {
            var args = [func].concat(slice.call(arguments));
            return wrapper.apply(this, args);
        };
    };

    // Returns a function that is the composition of a list of functions, each
    // consuming the return value of the function that follows.
    _.compose = function() {
        var funcs = slice.call(arguments);
        return function() {
            var args = slice.call(arguments);
            for (var i = funcs.length - 1; i >= 0; i--) {
                args = [funcs[i].apply(this, args)];
            }
            return args[0];
        };
    };

    // Returns a function that will only be executed after being called N times.
    _.after = function(times, func) {
        return function() {
            if (--times < 1) {
                return func.apply(this, arguments);
            }
        };
    };

    // Object Functions
    // ----------------

    // Retrieve the names of an object's properties.
    // Delegates to **ECMAScript 5**'s native `Object.keys`
    _.keys = nativeKeys ||
    function(obj) {
        if (obj !== Object(obj)) throw new TypeError('Invalid object');
        var keys = [];
        for (var key in obj) if (hasOwnProperty.call(obj, key)) keys[keys.length] = key;
        return keys;
    };

    // Retrieve the values of an object's properties.
    _.values = function(obj) {
        return _.map(obj, _.identity);
    };

    // Return a sorted list of the function names available on the object.
    // Aliased as `methods`
    _.functions = _.methods = function(obj) {
        var names = [];
        for (var key in obj) {
            if (_.isFunction(obj[key])) names.push(key);
        }
        return names.sort();
    };

    // Extend a given object with all the properties in passed-in object(s).
    _.extend = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            for (var prop in source) {
                if (source[prop] !== void 0) obj[prop] = source[prop];
            }
        });
        return obj;
    };

    // Fill in a given object with default properties.
    _.defaults = function(obj) {
        each(slice.call(arguments, 1), function(source) {
            for (var prop in source) {
                if (obj[prop] == null) obj[prop] = source[prop];
            }
        });
        return obj;
    };

    // Create a (shallow-cloned) duplicate of an object.
    _.clone = function(obj) {
        if (!_.isObject(obj)) return obj;
        return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
    };

    // Invokes interceptor with the obj, and then returns obj.
    // The primary purpose of this method is to "tap into" a method chain, in
    // order to perform operations on intermediate results within the chain.
    _.tap = function(obj, interceptor) {
        interceptor(obj);
        return obj;
    };

    // Internal recursive comparison function.


    function eq(a, b, stack) {
        // Identical objects are equal. `0 === -0`, but they aren't identical.
        // See the Harmony `egal` proposal: http://wiki.ecmascript.org/doku.php?id=harmony:egal.
        if (a === b) return a !== 0 || 1 / a == 1 / b;
        // A strict comparison is necessary because `null == undefined`.
        if ((a == null) || (b == null)) return a === b;
        // Unwrap any wrapped objects.
        if (a._chain) a = a._wrapped;
        if (b._chain) b = b._wrapped;
        // Invoke a custom `isEqual` method if one is provided.
        if (_.isFunction(a.isEqual)) return a.isEqual(b);
        if (_.isFunction(b.isEqual)) return b.isEqual(a);
        // Compare object types.
        var typeA = typeof a;
        if (typeA != typeof b) return false;
        // Optimization; ensure that both values are truthy or falsy.
        if (!a != !b) return false;
        // `NaN` values are equal.
        if (_.isNaN(a)) return _.isNaN(b);
        // Compare string objects by value.
        var isStringA = _.isString(a),
            isStringB = _.isString(b);
        if (isStringA || isStringB) return isStringA && isStringB && String(a) == String(b);
        // Compare number objects by value.
        var isNumberA = _.isNumber(a),
            isNumberB = _.isNumber(b);
        if (isNumberA || isNumberB) return isNumberA && isNumberB && +a == +b;
        // Compare boolean objects by value. The value of `true` is 1; the value of `false` is 0.
        var isBooleanA = _.isBoolean(a),
            isBooleanB = _.isBoolean(b);
        if (isBooleanA || isBooleanB) return isBooleanA && isBooleanB && +a == +b;
        // Compare dates by their millisecond values.
        var isDateA = _.isDate(a),
            isDateB = _.isDate(b);
        if (isDateA || isDateB) return isDateA && isDateB && a.getTime() == b.getTime();
        // Compare RegExps by their source patterns and flags.
        var isRegExpA = _.isRegExp(a),
            isRegExpB = _.isRegExp(b);
        if (isRegExpA || isRegExpB) {
            // Ensure commutative equality for RegExps.
            return isRegExpA && isRegExpB && a.source == b.source && a.global == b.global && a.multiline == b.multiline && a.ignoreCase == b.ignoreCase;
        }
        // Ensure that both values are objects.
        if (typeA != 'object') return false;
        // Arrays or Arraylikes with different lengths are not equal.
        if (a.length !== b.length) return false;
        // Objects with different constructors are not equal.
        if (a.constructor !== b.constructor) return false;
        // Assume equality for cyclic structures. The algorithm for detecting cyclic
        // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
        var length = stack.length;
        while (length--) {
            // Linear search. Performance is inversely proportional to the number of
            // unique nested structures.
            if (stack[length] == a) return true;
        }
        // Add the first object to the stack of traversed objects.
        stack.push(a);
        var size = 0,
            result = true;
        // Deep compare objects.
        for (var key in a) {
            if (hasOwnProperty.call(a, key)) {
                // Count the expected number of properties.
                size++;
                // Deep compare each member.
                if (!(result = hasOwnProperty.call(b, key) && eq(a[key], b[key], stack))) break;
            }
        }
        // Ensure that both objects contain the same number of properties.
        if (result) {
            for (key in b) {
                if (hasOwnProperty.call(b, key) && !size--) break;
            }
            result = !size;
        }
        // Remove the first object from the stack of traversed objects.
        stack.pop();
        return result;
    }

    // Perform a deep comparison to check if two objects are equal.
    _.isEqual = function(a, b) {
        return eq(a, b, []);
    };

    // Is a given array, string, or object empty?
    // An "empty" object has no enumerable own-properties.
    _.isEmpty = function(obj) {
        if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
        for (var key in obj) if (hasOwnProperty.call(obj, key)) return false;
        return true;
    };

    // Is a given value a DOM element?
    _.isElement = function(obj) {
        return !!(obj && obj.nodeType == 1);
    };

    // Is a given value an array?
    // Delegates to ECMA5's native Array.isArray
    _.isArray = nativeIsArray ||
    function(obj) {
        return toString.call(obj) == '[object Array]';
    };

    // Is a given variable an object?
    _.isObject = function(obj) {
        return obj === Object(obj);
    };

    // Is a given variable an arguments object?
    if (toString.call(arguments) == '[object Arguments]') {
        _.isArguments = function(obj) {
            return toString.call(obj) == '[object Arguments]';
        };
    } else {
        _.isArguments = function(obj) {
            return !!(obj && hasOwnProperty.call(obj, 'callee'));
        };
    }

    // Is a given value a function?
    _.isFunction = function(obj) {
        return toString.call(obj) == '[object Function]';
    };

    // Is a given value a string?
    _.isString = function(obj) {
        return toString.call(obj) == '[object String]';
    };

    // Is a given value a number?
    _.isNumber = function(obj) {
        return toString.call(obj) == '[object Number]';
    };

    // Is the given value `NaN`?
    _.isNaN = function(obj) {
        // `NaN` is the only value for which `===` is not reflexive.
        return obj !== obj;
    };

    // Is a given value a boolean?
    _.isBoolean = function(obj) {
        return obj === true || obj === false || toString.call(obj) == '[object Boolean]';
    };

    // Is a given value a date?
    _.isDate = function(obj) {
        return toString.call(obj) == '[object Date]';
    };

    // Is the given value a regular expression?
    _.isRegExp = function(obj) {
        return toString.call(obj) == '[object RegExp]';
    };

    // Is a given value equal to null?
    _.isNull = function(obj) {
        return obj === null;
    };

    // Is a given variable undefined?
    _.isUndefined = function(obj) {
        return obj === void 0;
    };

    // Utility Functions
    // -----------------

    // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
    // previous owner. Returns a reference to the Underscore object.
    _.noConflict = function() {
        root._ = previousUnderscore;
        return this;
    };

    // Keep the identity function around for default iterators.
    _.identity = function(value) {
        return value;
    };

    // Run a function **n** times.
    _.times = function(n, iterator, context) {
        for (var i = 0; i < n; i++) iterator.call(context, i);
    };

    // Escape a string for HTML interpolation.
    _.escape = function(string) {
        return ('' + string).replace(/&(?!\w+;|#\d+;|#x[\da-f]+;)/gi, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g, '&#x2F;');
    };

    // Add your own custom functions to the Underscore object, ensuring that
    // they're correctly added to the OOP wrapper as well.
    _.mixin = function(obj) {
        each(_.functions(obj), function(name) {
            addToWrapper(name, _[name] = obj[name]);
        });
    };

    // Generate a unique integer id (unique within the entire client session).
    // Useful for temporary DOM ids.
    var idCounter = 0;
    _.uniqueId = function(prefix) {
        var id = idCounter++;
        return prefix ? prefix + id : id;
    };

    // By default, Underscore uses ERB-style template delimiters, change the
    // following template settings to use alternative delimiters.
    _.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };

    // JavaScript micro-templating, similar to John Resig's implementation.
    // Underscore templating handles arbitrary delimiters, preserves whitespace,
    // and correctly escapes quotes within interpolated code.
    _.template = function(str, data) {
        var c = _.templateSettings;
        var tmpl = 'var __p=[],print=function(){__p.push.apply(__p,arguments);};' + 'with(obj||{}){__p.push(\'' + str.replace(/\\/g, '\\\\').replace(/'/g, "\\'").replace(c.escape, function(match, code) {
            return "',_.escape(" + code.replace(/\\'/g, "'") + "),'";
        }).replace(c.interpolate, function(match, code) {
            return "'," + code.replace(/\\'/g, "'") + ",'";
        }).replace(c.evaluate || null, function(match, code) {
            return "');" + code.replace(/\\'/g, "'").replace(/[\r\n\t]/g, ' ') + "__p.push('";
        }).replace(/\r/g, '\\r').replace(/\n/g, '\\n').replace(/\t/g, '\\t') + "');}return __p.join('');";
        var func = new Function('obj', tmpl);
        return data ? func(data) : func;
    };

    // The OOP Wrapper
    // ---------------

    // If Underscore is called as a function, it returns a wrapped object that
    // can be used OO-style. This wrapper holds altered versions of all the
    // underscore functions. Wrapped objects may be chained.
    var wrapper = function(obj) {
            this._wrapped = obj;
        };

    // Expose `wrapper.prototype` as `_.prototype`
    _.prototype = wrapper.prototype;

    // Helper function to continue chaining intermediate results.
    var result = function(obj, chain) {
            return chain ? _(obj).chain() : obj;
        };

    // A method to easily add functions to the OOP wrapper.
    var addToWrapper = function(name, func) {
            wrapper.prototype[name] = function() {
                var args = slice.call(arguments);
                unshift.call(args, this._wrapped);
                return result(func.apply(_, args), this._chain);
            };
        };

    // Add all of the Underscore functions to the wrapper object.
    _.mixin(_);

    // Add all mutator Array functions to the wrapper.
    each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
        var method = ArrayProto[name];
        wrapper.prototype[name] = function() {
            method.apply(this._wrapped, arguments);
            return result(this._wrapped, this._chain);
        };
    });

    // Add all accessor Array functions to the wrapper.
    each(['concat', 'join', 'slice'], function(name) {
        var method = ArrayProto[name];
        wrapper.prototype[name] = function() {
            return result(method.apply(this._wrapped, arguments), this._chain);
        };
    });

    // Start chaining a wrapped Underscore object.
    wrapper.prototype.chain = function() {
        this._chain = true;
        return this;
    };

    // Extracts the result from a wrapped and chained object.
    wrapper.prototype.value = function() {
        return this._wrapped;
    };

})();

(function() {
    /*
            fastFrag
                Turn JSON into HTML, http://github.com/gregory80/fastFrag
                
            Usage: fastFrag.create( { content : "hello world" });
            // creates Document Fragment: <div>hello world</div>
            
            Learn more: http://github.com/gregory80/fastFrag/blob/master/README.md or http://fastfrag.org
            
            Convert HTML to fastFrag (fast!): http://json.fastfrag.org/
    */

    var fastFrag = {
        create: function(params) {
            base_frag = d.createDocumentFragment();
            return assembleHTML(params);
        },
        version: "1.1.3.1"

    };
    window.fastFrag = fastFrag;

    var d = document,
        base_frag = null;

    function assembleHTML(params) {
        var el;
        if (params && params.length === undefined) {
            el = _singleNode(params);
            base_frag.appendChild(el);
        } else {
            var sub_frag = d.createDocumentFragment(),
                k;
            for (k in params) {
                el = _singleNode(params[k]);
                sub_frag.appendChild(el);
            }
            return sub_frag;
        }
        return base_frag;
    }

    // helpers


    function _singleNode(o) {
        var el, txt;
        if (o.text !== undefined) {
            el = d.createTextNode(o.text || "");
        } else {
            el = _make_element(o);
            txt = _process_node(o);
            try {
                el.appendChild(txt);
            } catch (e) {}
        }
        return el;
    }

    function _mke_attribute(el, attrs) {
        for (var k in attrs) {
            // yuck, setting disabled to false or none still breaks browsers, skip it instead
            if (k === "disabled" && !attrs[k]) {
                continue;
            }
            // IE7 barfs if you try to set style via a style attribute on the element
            // deprecate this in favor of top level?
            if (k.toLowerCase() === "style") {
                el.cssText = el.style.cssText = attrs[k];
            }
            // setAttribute is buggy in IE7/8, .value works in 8, but not in 7
            if (k.toLowerCase() === "value") {
                // buttons conveniently default to type=submit in IE8 & FF, the only browsers that fuss about this
                if (el.type == "button") {
                    attributeNode = d.createAttribute("value");
                    attributeNode.nodeValue = attrs[k];
                    el.setAttributeNode(attributeNode);
                } else {
                    el.value = attrs[k]; // IE8, FF
                }
            } else {
                el.setAttribute(k, _safe(attrs[k]));
            }
        }
    }

    function _make_element(o) {
        var el_name, el;
        el_name = o.type || "div";
        el = _mke(el_name);
        // Starting in ver1.0.5, can use any mix of o.attributes || o.attr  || o.attrs
        if (o.attributes || o.attr || o.attrs) {
            _mke_attribute(el, o.attributes || o.attr || o.attrs);
        }
        if (o.id) {
            el.id = o.id;
        }
        if (o.css) {
            el.className = o.css;
        }
        if (o.style) {
            el.cssText = el.style.cssText = o.style;
        } // add style attribute..
        if (o.value) {
            el.value = o.value;
        } // add value attr.
        return el;
    }

    function _process_node(o) {
        var txt = null,
            cntnt = o.c || o.content,
            content_type = typeof cntnt,
            txt_value;
        // JS thinks both Object and Array are typeof object, let assembleHTML guide it 
        if (content_type === "object") {
            txt = assembleHTML(cntnt);
        } else if (content_type === "string") {
            txt = d.createTextNode(cntnt);
        } else {
            // this might be an intger or float or boolean, it's all text to HTML..
            txt_value = (cntnt !== undefined) ? (cntnt.toString() || "") : "";
            txt = d.createTextNode(txt_value);
        }
        return txt
    }
    // short cut / conveince methods


    function _mke(elem) {
        return d.createElement(elem);
    }

    function _safe(string) {
        var txt_node = d.createTextNode(string);
        var txt = (txt_node.nodeValue).toString(); // put in a text node, then grab it
        txt_node = null
        return txt;
    }

})();

/*
 * Raphael 1.5.2 - JavaScript Vector Library
 *
 * Copyright (c) 2010 Dmitry Baranovskiy (http://raphaeljs.com)
 * Licensed under the MIT (http://raphaeljs.com/license.html) license.
 */
(function() {
    function aZ() {
        if (aZ.is(arguments[0], p)) {
            var b = arguments[0],
                d = B[o](aZ, b.splice(0, 3 + aZ.is(b[0], aA))),
                S = d.set();
            for (var e = 0, E = b[ar]; e < E; e++) {
                var R = b[e] || {};
                Q[ae](R.type) && S[aX](d[R.type]().attr(R))
            }
            return S
        }
        return B[o](aZ, arguments)
    }
    aZ.version = "1.5.2";
    var a7 = /[, ]+/,
        Q = {
            circle: 1,
            rect: 1,
            path: 1,
            ellipse: 1,
            text: 1,
            image: 1
        },
        X = /\{(\d+)\}/g,
        aW = "prototype",
        ae = "hasOwnProperty",
        J = document,
        bE = window,
        aC = {
            was: Object[aW][ae].call(bE, "Raphael"),
            is: bE.Raphael
        },
        aF = function() {
            this.customAttributes = {}
        },
        aG, n = "appendChild",
        o = "apply",
        z = "concat",
        bi = "createTouch" in J,
        O = "",
        a6 = " ",
        bg = String,
        bd = "split",
        U = "click dblclick mousedown mousemove mouseout mouseover mouseup touchstart touchmove touchend orientationchange touchcancel gesturestart gesturechange gestureend" [bd](a6),
        bw = {
            mousedown: "touchstart",
            mousemove: "touchmove",
            mouseup: "touchend"
        },
        an = "join",
        ar = "length",
        at = bg[aW].toLowerCase,
        av = Math,
        ax = av.max,
        ay = av.min,
        f = av.abs,
        aT = av.pow,
        aR = av.PI,
        aA = "number",
        bh = "string",
        p = "array",
        bv = "toString",
        V = "fill",
        aB = Object[aW][bv],
        aE = {},
        aX = "push",
        am = /^url\(['"]?([^\)]+?)['"]?\)$/i,
        x = /^\s*((#[a-f\d]{6})|(#[a-f\d]{3})|rgba?\(\s*([\d\.]+%?\s*,\s*[\d\.]+%?\s*,\s*[\d\.]+(?:%?\s*,\s*[\d\.]+)?)%?\s*\)|hsba?\(\s*([\d\.]+(?:deg|\xb0|%)?\s*,\s*[\d\.]+%?\s*,\s*[\d\.]+(?:%?\s*,\s*[\d\.]+)?)%?\s*\)|hsla?\(\s*([\d\.]+(?:deg|\xb0|%)?\s*,\s*[\d\.]+%?\s*,\s*[\d\.]+(?:%?\s*,\s*[\d\.]+)?)%?\s*\))\s*$/i,
        al = {
            "NaN": 1,
            "Infinity": 1,
            "-Infinity": 1
        },
        t = /^(?:cubic-)?bezier\(([^,]+),([^,]+),([^,]+),([^\)]+)\)/,
        a4 = av.round,
        a9 = "setAttribute",
        br = parseFloat,
        bu = parseInt,
        az = " progid:DXImageTransform.Microsoft",
        bA = bg[aW].toUpperCase,
        r = {
            blur: 0,
            "clip-rect": "0 0 1e9 1e9",
            cursor: "default",
            cx: 0,
            cy: 0,
            fill: "#fff",
            "fill-opacity": 1,
            font: '10px "Arial"',
            "font-family": '"Arial"',
            "font-size": "10",
            "font-style": "normal",
            "font-weight": 400,
            gradient: 0,
            height: 0,
            href: "http://raphaeljs.com/",
            opacity: 1,
            path: "M0,0",
            r: 0,
            rotation: 0,
            rx: 0,
            ry: 0,
            scale: "1 1",
            src: "",
            stroke: "#000",
            "stroke-dasharray": "",
            "stroke-linecap": "butt",
            "stroke-linejoin": "butt",
            "stroke-miterlimit": 0,
            "stroke-opacity": 1,
            "stroke-width": 1,
            target: "_blank",
            "text-anchor": "middle",
            title: "Raphael",
            translation: "0 0",
            width: 0,
            x: 0,
            y: 0
        },
        q = {
            along: "along",
            blur: aA,
            "clip-rect": "csv",
            cx: aA,
            cy: aA,
            fill: "colour",
            "fill-opacity": aA,
            "font-size": aA,
            height: aA,
            opacity: aA,
            path: "path",
            r: aA,
            rotation: "csv",
            rx: aA,
            ry: aA,
            scale: "csv",
            stroke: "colour",
            "stroke-opacity": aA,
            "stroke-width": aA,
            translation: "csv",
            width: aA,
            x: aA,
            y: aA
        },
        a5 = "replace",
        m = /^(from|to|\d+%?)$/,
        y = /\s*,\s*/,
        ah = {
            hs: 1,
            rg: 1
        },
        aD = /,?([achlmqrstvxz]),?/gi,
        aL = /([achlmqstvz])[\s,]*((-?\d*\.?\d*(?:e[-+]?\d+)?\s*,?\s*)+)/ig,
        aQ = /(-?\d*\.?\d*(?:e[-+]?\d+)?)\s*,?\s*/ig,
        a0 = /^r(?:\(([^,]+?)\s*,\s*([^\)]+?)\))?/,
        bc = function(d, e) {
            return d.key - e.key
        };
    aZ.type = (bE.SVGAngle || J.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? "SVG" : "VML");
    if (aZ.type == "VML") {
        var I = J.createElement("div"),
            s;
        I.innerHTML = '<v:shape adj="1"/>';
        s = I.firstChild;
        s.style.behavior = "url(#default#VML)";
        if (!(s && typeof s.adj == "object")) {
            return aZ.type = null
        }
        I = null
    }
    aZ.svg = !(aZ.vml = aZ.type == "VML");
    aF[aW] = aZ[aW];
    aG = aF[aW];
    aZ._id = 0;
    aZ._oid = 0;
    aZ.fn = {};
    aZ.is = function(b, d) {
        d = at.call(d);
        if (d == "finite") {
            return !al[ae](+b)
        }
        return (d == "null" && b === null) || (d == typeof b) || (d == "object" && b === Object(b)) || (d == "array" && Array.isArray && Array.isArray(b)) || aB.call(b).slice(8, -1).toLowerCase() == d
    };
    aZ.angle = function(d, R, e, S, i, bH) {
        if (i == null) {
            var b = d - e,
                E = R - S;
            if (!b && !E) {
                return 0
            }
            return ((b < 0) * 180 + av.atan(-E / -b) * 180 / aR + 360) % 360
        } else {
            return aZ.angle(d, R, i, bH) - aZ.angle(e, S, i, bH)
        }
    };
    aZ.rad = function(b) {
        return b % 360 * aR / 180
    };
    aZ.deg = function(b) {
        return b * 180 / aR % 360
    };
    aZ.snapTo = function(R, E, e) {
        e = aZ.is(e, "finite") ? e : 10;
        if (aZ.is(R, p)) {
            var b = R.length;
            while (b--) {
                if (f(R[b] - E) <= e) {
                    return R[b]
                }
            }
        } else {
            R = +R;
            var d = E % R;
            if (d < e) {
                return E - d
            }
            if (d > R - e) {
                return E - d + R
            }
        }
        return E
    };

    function D() {
        var d = [],
            b = 0;
        for (; b < 32; b++) {
            d[b] = (~~ (av.random() * 16))[bv](16)
        }
        d[12] = 4;
        d[16] = ((d[16] & 3) | 8)[bv](16);
        return "r-" + d[an]("")
    }
    aZ.setWindow = function(b) {
        bE = b;
        J = bE.document
    };
    var bt = function(d) {
            if (aZ.vml) {
                var bI = /^\s+|\s+$/g;
                var b;
                try {
                    var E = new ActiveXObject("htmlfile");
                    E.write("<body>");
                    E.close();
                    b = E.body
                } catch (R) {
                    b = createPopup().document.body
                }
                var bH = b.createTextRange();
                bt = w(function(i) {
                    try {
                        b.style.color = bg(i)[a5](bI, O);
                        var bK = bH.queryCommandValue("ForeColor");
                        bK = ((bK & 255) << 16) | (bK & 65280) | ((bK & 16711680) >>> 16);
                        return "#" + ("000000" + bK[bv](16)).slice(-6)
                    } catch (bJ) {
                        return "none"
                    }
                })
            } else {
                var S = J.createElement("i");
                S.title = "Rapha\xebl Colour Picker";
                S.style.display = "none";
                J.body[n](S);
                bt = w(function(e) {
                    S.style.color = e;
                    return J.defaultView.getComputedStyle(S, O).getPropertyValue("color")
                })
            }
            return bt(d)
        },
        af = function() {
            return "hsb(" + [this.h, this.s, this.b] + ")"
        },
        ag = function() {
            return "hsl(" + [this.h, this.s, this.l] + ")"
        },
        a3 = function() {
            return this.hex
        };
    aZ.hsb2rgb = function(e, E, d, i) {
        if (aZ.is(e, "object") && "h" in e && "s" in e && "b" in e) {
            d = e.b;
            E = e.s;
            e = e.h;
            i = e.o
        }
        return aZ.hsl2rgb(e, E, d / 2, i)
    };
    aZ.hsl2rgb = function(R, bL, bH, bI) {
        if (aZ.is(R, "object") && "h" in R && "s" in R && "l" in R) {
            bH = R.l;
            bL = R.s;
            R = R.h
        }
        if (R > 1 || bL > 1 || bH > 1) {
            R /= 360;
            bL /= 100;
            bH /= 100
        }
        var bK = {},
            e = ["r", "g", "b"],
            bN, bM, bO, bJ, E, d;
        if (!bL) {
            bK = {
                r: bH,
                g: bH,
                b: bH
            }
        } else {
            if (bH < 0.5) {
                bN = bH * (1 + bL)
            } else {
                bN = bH + bL - bH * bL
            }
            bM = 2 * bH - bN;
            for (var S = 0; S < 3; S++) {
                bO = R + 1 / 3 * -(S - 1);
                bO < 0 && bO++;
                bO > 1 && bO--;
                if (bO * 6 < 1) {
                    bK[e[S]] = bM + (bN - bM) * 6 * bO
                } else {
                    if (bO * 2 < 1) {
                        bK[e[S]] = bN
                    } else {
                        if (bO * 3 < 2) {
                            bK[e[S]] = bM + (bN - bM) * (2 / 3 - bO) * 6
                        } else {
                            bK[e[S]] = bM
                        }
                    }
                }
            }
        }
        bK.r *= 255;
        bK.g *= 255;
        bK.b *= 255;
        bK.hex = "#" + (16777216 | bK.b | (bK.g << 8) | (bK.r << 16)).toString(16).slice(1);
        aZ.is(bI, "finite") && (bK.opacity = bI);
        bK.toString = a3;
        return bK
    };
    aZ.rgb2hsb = function(bI, E, b) {
        if (E == null && aZ.is(bI, "object") && "r" in bI && "g" in bI && "b" in bI) {
            b = bI.b;
            E = bI.g;
            bI = bI.r
        }
        if (E == null && aZ.is(bI, bh)) {
            var e = aZ.getRGB(bI);
            bI = e.r;
            E = e.g;
            b = e.b
        }
        if (bI > 1 || E > 1 || b > 1) {
            bI /= 255;
            E /= 255;
            b /= 255
        }
        var S = ax(bI, E, b),
            bH = ay(bI, E, b),
            R, bJ, d = S;
        if (bH == S) {
            return {
                h: 0,
                s: 0,
                b: S,
                toString: af
            }
        } else {
            var i = (S - bH);
            bJ = i / S;
            if (bI == S) {
                R = (E - b) / i
            } else {
                if (E == S) {
                    R = 2 + ((b - bI) / i)
                } else {
                    R = 4 + ((bI - E) / i)
                }
            }
            R /= 6;
            R < 0 && R++;
            R > 1 && R--
        }
        return {
            h: R,
            s: bJ,
            b: d,
            toString: af
        }
    };
    aZ.rgb2hsl = function(bJ, i, b) {
        if (i == null && aZ.is(bJ, "object") && "r" in bJ && "g" in bJ && "b" in bJ) {
            b = bJ.b;
            i = bJ.g;
            bJ = bJ.r
        }
        if (i == null && aZ.is(bJ, bh)) {
            var d = aZ.getRGB(bJ);
            bJ = d.r;
            i = d.g;
            b = d.b
        }
        if (bJ > 1 || i > 1 || b > 1) {
            bJ /= 255;
            i /= 255;
            b /= 255
        }
        var bH = ax(bJ, i, b),
            bI = ay(bJ, i, b),
            E, bK, S = (bH + bI) / 2,
            R;
        if (bI == bH) {
            R = {
                h: 0,
                s: 0,
                l: S
            }
        } else {
            var e = bH - bI;
            bK = S < 0.5 ? e / (bH + bI) : e / (2 - bH - bI);
            if (bJ == bH) {
                E = (i - b) / e
            } else {
                if (i == bH) {
                    E = 2 + (b - bJ) / e
                } else {
                    E = 4 + (bJ - i) / e
                }
            }
            E /= 6;
            E < 0 && E++;
            E > 1 && E--;
            R = {
                h: E,
                s: bK,
                l: S
            }
        }
        R.toString = ag;
        return R
    };
    aZ._path2string = function() {
        return this.join(",")[a5](aD, "$1")
    };

    function w(b, i, e) {
        function d() {
            var E = Array[aW].slice.call(arguments, 0),
                R = E[an]("\u25ba"),
                S = d.cache = d.cache || {},
                bH = d.count = d.count || [];
            if (S[ae](R)) {
                return e ? e(S[R]) : S[R]
            }
            bH[ar] >= 1000 && delete S[bH.shift()];
            bH[aX](R);
            S[R] = b[o](i, E);
            return e ? e(S[R]) : S[R]
        }
        return d
    }
    aZ.getRGB = w(function(d) {
        if (!d || !! ((d = bg(d)).indexOf("-") + 1)) {
            return {
                r: -1,
                g: -1,
                b: -1,
                hex: "none",
                error: 1
            }
        }
        if (d == "none") {
            return {
                r: -1,
                g: -1,
                b: -1,
                hex: "none"
            }
        }!(ah[ae](d.toLowerCase().substring(0, 2)) || d.charAt() == "#") && (d = bt(d));
        var R, E, e, b, i, bH, bI, S = d.match(x);
        if (S) {
            if (S[2]) {
                b = bu(S[2].substring(5), 16);
                e = bu(S[2].substring(3, 5), 16);
                E = bu(S[2].substring(1, 3), 16)
            }
            if (S[3]) {
                b = bu((bH = S[3].charAt(3)) + bH, 16);
                e = bu((bH = S[3].charAt(2)) + bH, 16);
                E = bu((bH = S[3].charAt(1)) + bH, 16)
            }
            if (S[4]) {
                bI = S[4][bd](y);
                E = br(bI[0]);
                bI[0].slice(-1) == "%" && (E *= 2.55);
                e = br(bI[1]);
                bI[1].slice(-1) == "%" && (e *= 2.55);
                b = br(bI[2]);
                bI[2].slice(-1) == "%" && (b *= 2.55);
                S[1].toLowerCase().slice(0, 4) == "rgba" && (i = br(bI[3]));
                bI[3] && bI[3].slice(-1) == "%" && (i /= 100)
            }
            if (S[5]) {
                bI = S[5][bd](y);
                E = br(bI[0]);
                bI[0].slice(-1) == "%" && (E *= 2.55);
                e = br(bI[1]);
                bI[1].slice(-1) == "%" && (e *= 2.55);
                b = br(bI[2]);
                bI[2].slice(-1) == "%" && (b *= 2.55);
                (bI[0].slice(-3) == "deg" || bI[0].slice(-1) == "\xb0") && (E /= 360);
                S[1].toLowerCase().slice(0, 4) == "hsba" && (i = br(bI[3]));
                bI[3] && bI[3].slice(-1) == "%" && (i /= 100);
                return aZ.hsb2rgb(E, e, b, i)
            }
            if (S[6]) {
                bI = S[6][bd](y);
                E = br(bI[0]);
                bI[0].slice(-1) == "%" && (E *= 2.55);
                e = br(bI[1]);
                bI[1].slice(-1) == "%" && (e *= 2.55);
                b = br(bI[2]);
                bI[2].slice(-1) == "%" && (b *= 2.55);
                (bI[0].slice(-3) == "deg" || bI[0].slice(-1) == "\xb0") && (E /= 360);
                S[1].toLowerCase().slice(0, 4) == "hsla" && (i = br(bI[3]));
                bI[3] && bI[3].slice(-1) == "%" && (i /= 100);
                return aZ.hsl2rgb(E, e, b, i)
            }
            S = {
                r: E,
                g: e,
                b: b
            };
            S.hex = "#" + (16777216 | b | (e << 8) | (E << 16)).toString(16).slice(1);
            aZ.is(i, "finite") && (S.opacity = i);
            return S
        }
        return {
            r: -1,
            g: -1,
            b: -1,
            hex: "none",
            error: 1
        }
    }, aZ);
    aZ.getColor = function(e) {
        var d = this.getColor.start = this.getColor.start || {
            h: 0,
            s: 1,
            b: e || 0.75
        },
            b = this.hsb2rgb(d.h, d.s, d.b);
        d.h += 0.075;
        if (d.h > 1) {
            d.h = 0;
            d.s -= 0.2;
            d.s <= 0 && (this.getColor.start = {
                h: 0,
                s: 1,
                b: d.b
            })
        }
        return b.hex
    };
    aZ.getColor.reset = function() {
        delete this.start
    };
    aZ.parsePathString = w(function(e) {
        if (!e) {
            return null
        }
        var d = {
            a: 7,
            c: 6,
            h: 1,
            l: 2,
            m: 2,
            q: 4,
            s: 4,
            t: 2,
            v: 1,
            z: 0
        },
            b = [];
        if (aZ.is(e, p) && aZ.is(e[0], p)) {
            b = aK(e)
        }
        if (!b[ar]) {
            bg(e)[a5](aL, function(i, E, R) {
                var bH = [],
                    S = at.call(E);
                R[a5](aQ, function(bI, bJ) {
                    bJ && bH[aX](+bJ)
                });
                if (S == "m" && bH[ar] > 2) {
                    b[aX]([E][z](bH.splice(0, 2)));
                    S = "l";
                    E = E == "m" ? "l" : "L"
                }
                while (bH[ar] >= d[S]) {
                    b[aX]([E][z](bH.splice(0, d[S])));
                    if (!d[S]) {
                        break
                    }
                }
            })
        }
        b[bv] = aZ._path2string;
        return b
    });
    aZ.findDotsAtSegment = function(bN, bO, i, E, R, S, bP, bQ, bR) {
        var bS = 1 - bR,
            bT = aT(bS, 3) * bN + aT(bS, 2) * 3 * bR * i + bS * 3 * bR * bR * R + aT(bR, 3) * bP,
            bU = aT(bS, 3) * bO + aT(bS, 2) * 3 * bR * E + bS * 3 * bR * bR * S + aT(bR, 3) * bQ,
            bJ = bN + 2 * bR * (i - bN) + bR * bR * (R - 2 * i + bN),
            bK = bO + 2 * bR * (E - bO) + bR * bR * (S - 2 * E + bO),
            bL = i + 2 * bR * (R - i) + bR * bR * (bP - 2 * R + i),
            bM = E + 2 * bR * (S - E) + bR * bR * (bQ - 2 * S + E),
            d = (1 - bR) * bN + bR * i,
            e = (1 - bR) * bO + bR * E,
            bH = (1 - bR) * R + bR * bP,
            bI = (1 - bR) * S + bR * bQ,
            b = (90 - av.atan((bJ - bL) / (bK - bM)) * 180 / aR);
        (bJ > bL || bK < bM) && (b += 180);
        return {
            x: bT,
            y: bU,
            m: {
                x: bJ,
                y: bK
            },
            n: {
                x: bL,
                y: bM
            },
            start: {
                x: d,
                y: e
            },
            end: {
                x: bH,
                y: bI
            },
            alpha: b
        }
    };
    var aM = w(function(R) {
        if (!R) {
            return {
                x: 0,
                y: 0,
                width: 0,
                height: 0
            }
        }
        R = aI(R);
        var S = 0,
            bJ = 0,
            bH = [],
            bK = [],
            E;
        for (var d = 0, e = R[ar]; d < e; d++) {
            E = R[d];
            if (E[0] == "M") {
                S = E[1];
                bJ = E[2];
                bH[aX](S);
                bK[aX](bJ)
            } else {
                var b = G(S, bJ, E[1], E[2], E[3], E[4], E[5], E[6]);
                bH = bH[z](b.min.x, b.max.x);
                bK = bK[z](b.min.y, b.max.y);
                S = E[5];
                bJ = E[6]
            }
        }
        var bI = ay[o](0, bH),
            bL = ay[o](0, bK);
        return {
            x: bI,
            y: bL,
            width: ax[o](0, bH) - bI,
            height: ax[o](0, bK) - bL
        }
    }),
        aK = function(R) {
            var S = [];
            if (!aZ.is(R, p) || !aZ.is(R && R[0], p)) {
                R = aZ.parsePathString(R)
            }
            for (var b = 0, d = R[ar]; b < d; b++) {
                S[b] = [];
                for (var e = 0, E = R[b][ar]; e < E; e++) {
                    S[b][e] = R[b][e]
                }
            }
            S[bv] = aZ._path2string;
            return S
        },
        aP = w(function(bL) {
            if (!aZ.is(bL, p) || !aZ.is(bL && bL[0], p)) {
                bL = aZ.parsePathString(bL)
            }
            var bN = [],
                bP = 0,
                bQ = 0,
                bI = 0,
                bJ = 0,
                bO = 0;
            if (bL[0][0] == "M") {
                bP = bL[0][1];
                bQ = bL[0][2];
                bI = bP;
                bJ = bQ;
                bO++;
                bN[aX](["M", bP, bQ])
            }
            for (var b = bO, d = bL[ar]; b < d; b++) {
                var bM = bN[b] = [],
                    bK = bL[b];
                if (bK[0] != at.call(bK[0])) {
                    bM[0] = at.call(bK[0]);
                    switch (bM[0]) {
                    case "a":
                        bM[1] = bK[1];
                        bM[2] = bK[2];
                        bM[3] = bK[3];
                        bM[4] = bK[4];
                        bM[5] = bK[5];
                        bM[6] = +(bK[6] - bP).toFixed(3);
                        bM[7] = +(bK[7] - bQ).toFixed(3);
                        break;
                    case "v":
                        bM[1] = +(bK[1] - bQ).toFixed(3);
                        break;
                    case "m":
                        bI = bK[1];
                        bJ = bK[2];
                    default:
                        for (var e = 1, E = bK[ar]; e < E; e++) {
                            bM[e] = +(bK[e] - ((e % 2) ? bP : bQ)).toFixed(3)
                        }
                    }
                } else {
                    bM = bN[b] = [];
                    if (bK[0] == "m") {
                        bI = bK[1] + bP;
                        bJ = bK[2] + bQ
                    }
                    for (var R = 0, S = bK[ar]; R < S; R++) {
                        bN[b][R] = bK[R]
                    }
                }
                var bH = bN[b][ar];
                switch (bN[b][0]) {
                case "z":
                    bP = bI;
                    bQ = bJ;
                    break;
                case "h":
                    bP += +bN[b][bH - 1];
                    break;
                case "v":
                    bQ += +bN[b][bH - 1];
                    break;
                default:
                    bP += +bN[b][bH - 2];
                    bQ += +bN[b][bH - 1]
                }
            }
            bN[bv] = aZ._path2string;
            return bN
        }, 0, aK),
        aO = w(function(bK) {
            if (!aZ.is(bK, p) || !aZ.is(bK && bK[0], p)) {
                bK = aZ.parsePathString(bK)
            }
            var bM = [],
                bO = 0,
                bP = 0,
                bH = 0,
                bI = 0,
                bN = 0;
            if (bK[0][0] == "M") {
                bO = +bK[0][1];
                bP = +bK[0][2];
                bH = bO;
                bI = bP;
                bN++;
                bM[0] = ["M", bO, bP]
            }
            for (var b = bN, d = bK[ar]; b < d; b++) {
                var bL = bM[b] = [],
                    bJ = bK[b];
                if (bJ[0] != bA.call(bJ[0])) {
                    bL[0] = bA.call(bJ[0]);
                    switch (bL[0]) {
                    case "A":
                        bL[1] = bJ[1];
                        bL[2] = bJ[2];
                        bL[3] = bJ[3];
                        bL[4] = bJ[4];
                        bL[5] = bJ[5];
                        bL[6] = +(bJ[6] + bO);
                        bL[7] = +(bJ[7] + bP);
                        break;
                    case "V":
                        bL[1] = +bJ[1] + bP;
                        break;
                    case "H":
                        bL[1] = +bJ[1] + bO;
                        break;
                    case "M":
                        bH = +bJ[1] + bO;
                        bI = +bJ[2] + bP;
                    default:
                        for (var e = 1, E = bJ[ar]; e < E; e++) {
                            bL[e] = +bJ[e] + ((e % 2) ? bO : bP)
                        }
                    }
                } else {
                    for (var R = 0, S = bJ[ar]; R < S; R++) {
                        bM[b][R] = bJ[R]
                    }
                }
                switch (bL[0]) {
                case "Z":
                    bO = bH;
                    bP = bI;
                    break;
                case "H":
                    bO = bL[1];
                    break;
                case "V":
                    bP = bL[1];
                    break;
                case "M":
                    bH = bM[b][bM[b][ar] - 2];
                    bI = bM[b][bM[b][ar] - 1];
                default:
                    bO = bM[b][bM[b][ar] - 2];
                    bP = bM[b][bM[b][ar] - 1]
                }
            }
            bM[bv] = aZ._path2string;
            return bM
        }, null, aK),
        ap = function(b, e, d, i) {
            return [b, e, d, i, d, i]
        },
        aY = function(E, S, e, i, R, bH) {
            var b = 1 / 3,
                d = 2 / 3;
            return [b * E + d * e, b * S + d * i, b * R + d * e, b * bH + d * i, R, bH]
        },
        c = function(cc, ch, b2, b4, d, bS, b9, cd, ci, bZ) {
            var b = aR * 120 / 180,
                bY = aR / 180 * (+d || 0),
                b0 = [],
                cf, b1 = w(function(ck, cm, i) {
                    var cl = ck * av.cos(i) - cm * av.sin(i),
                        cn = ck * av.sin(i) + cm * av.cos(i);
                    return {
                        x: cl,
                        y: cn
                    }
                });
            if (!bZ) {
                cf = b1(cc, ch, -bY);
                cc = cf.x;
                ch = cf.y;
                cf = b1(cd, ci, -bY);
                cd = cf.x;
                ci = cf.y;
                var R = av.cos(aR / 180 * d),
                    b8 = av.sin(aR / 180 * d),
                    cb = (cc - cd) / 2,
                    cg = (ch - ci) / 2;
                var bM = (cb * cb) / (b2 * b2) + (cg * cg) / (b4 * b4);
                if (bM > 1) {
                    bM = av.sqrt(bM);
                    b2 = bM * b2;
                    b4 = bM * b4
                }
                var b3 = b2 * b2,
                    b5 = b4 * b4,
                    bR = (bS == b9 ? -1 : 1) * av.sqrt(f((b3 * b5 - b3 * cg * cg - b5 * cb * cb) / (b3 * cg * cg + b5 * cb * cb))),
                    S = bR * b2 * cg / b4 + (cc + cd) / 2,
                    bH = bR * -b4 * cb / b2 + (ch + ci) / 2,
                    bJ = av.asin(((ch - bH) / b4).toFixed(9)),
                    bK = av.asin(((ci - bH) / b4).toFixed(9));
                bJ = cc < S ? aR - bJ : bJ;
                bK = cd < S ? aR - bK : bK;
                bJ < 0 && (bJ = aR * 2 + bJ);
                bK < 0 && (bK = aR * 2 + bK);
                if (b9 && bJ > bK) {
                    bJ = bJ - aR * 2
                }
                if (!b9 && bK > bJ) {
                    bK = bK - aR * 2
                }
            } else {
                bJ = bZ[0];
                bK = bZ[1];
                S = bZ[2];
                bH = bZ[3]
            }
            var bI = bK - bJ;
            if (f(bI) > b) {
                var bL = bK,
                    ce = cd,
                    cj = ci;
                bK = bJ + b * (b9 && bK > bJ ? 1 : -1);
                cd = S + b2 * av.cos(bK);
                ci = bH + b4 * av.sin(bK);
                b0 = c(cd, ci, b2, b4, d, 0, b9, ce, cj, [bK, bL, S, bH])
            }
            bI = bK - bJ;
            var e = av.cos(bJ),
                b6 = av.sin(bJ),
                E = av.cos(bK),
                b7 = av.sin(bK),
                ca = av.tan(bI / 4),
                bN = 4 / 3 * b2 * ca,
                bO = 4 / 3 * b4 * ca,
                bT = [cc, ch],
                bU = [cc + bN * b6, ch - bO * e],
                bV = [cd + bN * b7, ci - bO * E],
                bW = [cd, ci];
            bU[0] = 2 * bT[0] - bU[0];
            bU[1] = 2 * bT[1] - bU[1];
            if (bZ) {
                return [bU, bV, bW][z](b0)
            } else {
                b0 = [bU, bV, bW][z](b0)[an]()[bd](",");
                var bX = [];
                for (var bP = 0, bQ = b0[ar]; bP < bQ; bP++) {
                    bX[bP] = bP % 2 ? b1(b0[bP - 1], b0[bP], bY).y : b1(b0[bP], b0[bP + 1], bY).x
                }
                return bX
            }
        },
        W = function(E, R, b, d, e, i, S, bH, bI) {
            var bJ = 1 - bI;
            return {
                x: aT(bJ, 3) * E + aT(bJ, 2) * 3 * bI * b + bJ * 3 * bI * bI * e + aT(bI, 3) * S,
                y: aT(bJ, 3) * R + aT(bJ, 2) * 3 * bI * d + bJ * 3 * bI * bI * i + aT(bI, 3) * bH
            }
        },
        G = w(function(bJ, bK, E, R, S, bH, bL, bM) {
            var d = (S - 2 * E + bJ) - (bL - 2 * S + E),
                e = 2 * (E - bJ) - 2 * (S - E),
                i = bJ - E,
                bN = (-e + av.sqrt(e * e - 4 * d * i)) / 2 / d,
                bO = (-e - av.sqrt(e * e - 4 * d * i)) / 2 / d,
                bQ = [bK, bM],
                bP = [bJ, bL],
                bI;
            f(bN) > "1e12" && (bN = 0.5);
            f(bO) > "1e12" && (bO = 0.5);
            if (bN > 0 && bN < 1) {
                bI = W(bJ, bK, E, R, S, bH, bL, bM, bN);
                bP[aX](bI.x);
                bQ[aX](bI.y)
            }
            if (bO > 0 && bO < 1) {
                bI = W(bJ, bK, E, R, S, bH, bL, bM, bO);
                bP[aX](bI.x);
                bQ[aX](bI.y)
            }
            d = (bH - 2 * R + bK) - (bM - 2 * bH + R);
            e = 2 * (R - bK) - 2 * (bH - R);
            i = bK - R;
            bN = (-e + av.sqrt(e * e - 4 * d * i)) / 2 / d;
            bO = (-e - av.sqrt(e * e - 4 * d * i)) / 2 / d;
            f(bN) > "1e12" && (bN = 0.5);
            f(bO) > "1e12" && (bO = 0.5);
            if (bN > 0 && bN < 1) {
                bI = W(bJ, bK, E, R, S, bH, bL, bM, bN);
                bP[aX](bI.x);
                bQ[aX](bI.y)
            }
            if (bO > 0 && bO < 1) {
                bI = W(bJ, bK, E, R, S, bH, bL, bM, bO);
                bP[aX](bI.x);
                bQ[aX](bI.y)
            }
            return {
                min: {
                    x: ay[o](0, bP),
                    y: ay[o](0, bQ)
                },
                max: {
                    x: ax[o](0, bP),
                    y: ax[o](0, bQ)
                }
            }
        }),
        aI = w(function(bJ, bK) {
            var bH = aO(bJ),
                bI = bK && aO(bK),
                b = {
                    x: 0,
                    y: 0,
                    bx: 0,
                    by: 0,
                    X: 0,
                    Y: 0,
                    qx: null,
                    qy: null
                },
                d = {
                    x: 0,
                    y: 0,
                    bx: 0,
                    by: 0,
                    X: 0,
                    Y: 0,
                    qx: null,
                    qy: null
                },
                bL = function(bS, i) {
                    var bQ, bR;
                    if (!bS) {
                        return ["C", i.x, i.y, i.x, i.y, i.x, i.y]
                    }!(bS[0] in {
                        T: 1,
                        Q: 1
                    }) && (i.qx = i.qy = null);
                    switch (bS[0]) {
                    case "M":
                        i.X = bS[1];
                        i.Y = bS[2];
                        break;
                    case "A":
                        bS = ["C"][z](c[o](0, [i.x, i.y][z](bS.slice(1))));
                        break;
                    case "S":
                        bQ = i.x + (i.x - (i.bx || i.x));
                        bR = i.y + (i.y - (i.by || i.y));
                        bS = ["C", bQ, bR][z](bS.slice(1));
                        break;
                    case "T":
                        i.qx = i.x + (i.x - (i.qx || i.x));
                        i.qy = i.y + (i.y - (i.qy || i.y));
                        bS = ["C"][z](aY(i.x, i.y, i.qx, i.qy, bS[1], bS[2]));
                        break;
                    case "Q":
                        i.qx = bS[1];
                        i.qy = bS[2];
                        bS = ["C"][z](aY(i.x, i.y, bS[1], bS[2], bS[3], bS[4]));
                        break;
                    case "L":
                        bS = ["C"][z](ap(i.x, i.y, bS[1], bS[2]));
                        break;
                    case "H":
                        bS = ["C"][z](ap(i.x, i.y, bS[1], i.y));
                        break;
                    case "V":
                        bS = ["C"][z](ap(i.x, i.y, i.x, bS[1]));
                        break;
                    case "Z":
                        bS = ["C"][z](ap(i.x, i.y, i.X, i.Y));
                        break
                    }
                    return bS
                },
                e = function(bS, bQ) {
                    if (bS[bQ][ar] > 7) {
                        bS[bQ].shift();
                        var bR = bS[bQ];
                        while (bR[ar]) {
                            bS.splice(bQ++, 0, ["C"][z](bR.splice(0, 6)))
                        }
                        bS.splice(bQ, 1);
                        S = ax(bH[ar], bI && bI[ar] || 0)
                    }
                },
                E = function(bT, bU, bQ, bR, bS) {
                    if (bT && bU && bT[bS][0] == "M" && bU[bS][0] != "M") {
                        bU.splice(bS, 0, ["M", bR.x, bR.y]);
                        bQ.bx = 0;
                        bQ.by = 0;
                        bQ.x = bT[bS][1];
                        bQ.y = bT[bS][2];
                        S = ax(bH[ar], bI && bI[ar] || 0)
                    }
                };
            for (var R = 0, S = ax(bH[ar], bI && bI[ar] || 0); R < S; R++) {
                bH[R] = bL(bH[R], b);
                e(bH, R);
                bI && (bI[R] = bL(bI[R], d));
                bI && e(bI, R);
                E(bH, bI, b, d, R);
                E(bI, bH, d, b, R);
                var bM = bH[R],
                    bN = bI && bI[R],
                    bP = bM[ar],
                    bO = bI && bN[ar];
                b.x = bM[bP - 2];
                b.y = bM[bP - 1];
                b.bx = br(bM[bP - 4]) || b.x;
                b.by = br(bM[bP - 3]) || b.y;
                d.bx = bI && (br(bN[bO - 4]) || d.x);
                d.by = bI && (br(bN[bO - 3]) || d.y);
                d.x = bI && bN[bO - 2];
                d.y = bI && bN[bO - 1]
            }
            return bI ? [bH, bI] : bH
        }, null, aK),
        aH = w(function(S) {
            var E = [];
            for (var bH = 0, bI = S[ar]; bH < bI; bH++) {
                var e = {},
                    bK = S[bH].match(/^([^:]*):?([\d\.]*)/);
                e.color = aZ.getRGB(bK[1]);
                if (e.color.error) {
                    return null
                }
                e.color = e.color.hex;
                bK[2] && (e.offset = bK[2] + "%");
                E[aX](e)
            }
            for (bH = 1, bI = E[ar] - 1; bH < bI; bH++) {
                if (!E[bH].offset) {
                    var bL = br(E[bH - 1].offset || 0),
                        R = 0;
                    for (var bJ = bH + 1; bJ < bI; bJ++) {
                        if (E[bJ].offset) {
                            R = E[bJ].offset;
                            break
                        }
                    }
                    if (!R) {
                        R = 100;
                        bJ = bI
                    }
                    R = br(R);
                    var b = (R - bL) / (bJ - bH + 1);
                    for (; bH < bJ; bH++) {
                        bL += b;
                        E[bH].offset = bL + "%"
                    }
                }
            }
            return E
        }),
        Y = function(i, E, e, d) {
            var b;
            if (aZ.is(i, bh) || aZ.is(i, "object")) {
                b = aZ.is(i, bh) ? J.getElementById(i) : i;
                if (b.tagName) {
                    if (E == null) {
                        return {
                            container: b,
                            width: b.style.pixelWidth || b.offsetWidth,
                            height: b.style.pixelHeight || b.offsetHeight
                        }
                    } else {
                        return {
                            container: b,
                            width: E,
                            height: e
                        }
                    }
                }
            } else {
                return {
                    container: 1,
                    x: i,
                    y: E,
                    width: e,
                    height: d
                }
            }
        },
        aS = function(d, b) {
            var i = this;
            for (var e in b) {
                if (b[ae](e) && !(e in d)) {
                    switch (typeof b[e]) {
                    case "function":
                        (function(E) {
                            d[e] = d === i ? E : function() {
                                return E[o](i, arguments)
                            }
                        })(b[e]);
                        break;
                    case "object":
                        d[e] = d[e] || {};
                        aS.call(this, d[e], b[e]);
                        break;
                    default:
                        d[e] = b[e];
                        break
                    }
                }
            }
        },
        bj = function(b, d) {
            b == d.top && (d.top = b.prev);
            b == d.bottom && (d.bottom = b.next);
            b.next && (b.next.prev = b.prev);
            b.prev && (b.prev.next = b.next)
        },
        bs = function(b, d) {
            if (d.top === b) {
                return
            }
            bj(b, d);
            b.next = null;
            b.prev = d.top;
            d.top.next = b;
            d.top = b
        },
        bq = function(b, d) {
            if (d.bottom === b) {
                return
            }
            bj(b, d);
            b.next = d.bottom;
            b.prev = null;
            d.bottom.prev = b;
            d.bottom = b
        },
        aj = function(b, d, e) {
            bj(b, e);
            d == e.top && (e.top = b);
            d.next && (d.next.prev = b);
            b.next = d.next;
            b.prev = d;
            d.next = b
        },
        ak = function(b, d, e) {
            bj(b, e);
            d == e.bottom && (e.bottom = b);
            d.prev && (d.prev.next = b);
            b.prev = d.prev;
            d.prev = b;
            b.next = d
        },
        a2 = function(b) {
            return function() {
                throw new Error("Rapha\xebl: you are calling to method \u201c" + b + "\u201d of removed object")
            }
        };
    aZ.pathToRelative = aP;
    if (aZ.svg) {
        aG.svgns = "http://www.w3.org/2000/svg";
        aG.xlink = "http://www.w3.org/1999/xlink";
        a4 = function(b) {
            return +b + (~~b === b) * 0.5
        };
        var a = function(d, b) {
                if (b) {
                    for (var e in b) {
                        if (b[ae](e)) {
                            d[a9](e, bg(b[e]))
                        }
                    }
                } else {
                    d = J.createElementNS(aG.svgns, d);
                    d.style.webkitTapHighlightColor = "rgba(0,0,0,0)";
                    return d
                }
            };
        aZ[bv] = function() {
            return "Your browser supports SVG.\nYou are running Rapha\xebl " + this.version
        };
        var bn = function(e, i) {
                var b = a("path");
                i.canvas && i.canvas[n](b);
                var d = new P(b, i);
                d.type = "path";
                ba(d, {
                    fill: "none",
                    stroke: "#000",
                    path: e
                });
                return d
            };
        var h = function(bL, S, bO) {
                var bP = "linear",
                    E = 0.5,
                    R = 0.5,
                    bM = bL.style;
                S = bg(S)[a5](a0, function(bS, i, bR) {
                    bP = "radial";
                    if (i && bR) {
                        E = br(i);
                        R = br(bR);
                        var bT = ((R > 0.5) * 2 - 1);
                        aT(E - 0.5, 2) + aT(R - 0.5, 2) > 0.25 && (R = av.sqrt(0.25 - aT(E - 0.5, 2)) * bT + 0.5) && R != 0.5 && (R = R.toFixed(5) - 1e-05 * bT)
                    }
                    return O
                });
                S = S[bd](/\s*\-\s*/);
                if (bP == "linear") {
                    var b = S.shift();
                    b = -br(b);
                    if (isNaN(b)) {
                        return null
                    }
                    var bQ = [0, 0, av.cos(b * aR / 180), av.sin(b * aR / 180)],
                        bK = 1 / (ax(f(bQ[2]), f(bQ[3])) || 1);
                    bQ[2] *= bK;
                    bQ[3] *= bK;
                    if (bQ[2] < 0) {
                        bQ[0] = -bQ[2];
                        bQ[2] = 0
                    }
                    if (bQ[3] < 0) {
                        bQ[1] = -bQ[3];
                        bQ[3] = 0
                    }
                }
                var d = aH(S);
                if (!d) {
                    return null
                }
                var bI = bL.getAttribute(V);
                bI = bI.match(/^url\(#(.*)\)$/);
                bI && bO.defs.removeChild(J.getElementById(bI[1]));
                var e = a(bP + "Gradient");
                e.id = D();
                a(e, bP == "radial" ? {
                    fx: E,
                    fy: R
                } : {
                    x1: bQ[0],
                    y1: bQ[1],
                    x2: bQ[2],
                    y2: bQ[3]
                });
                bO.defs[n](e);
                for (var bH = 0, bJ = d[ar]; bH < bJ; bH++) {
                    var bN = a("stop");
                    a(bN, {
                        offset: d[bH].offset ? d[bH].offset : !bH ? "0%" : "100%",
                        "stop-color": d[bH].color || "#fff"
                    });
                    e[n](bN)
                }
                a(bL, {
                    fill: "url(#" + e.id + ")",
                    opacity: 1,
                    "fill-opacity": 1
                });
                bM.fill = O;
                bM.opacity = 1;
                bM.fillOpacity = 1;
                return 1
            };
        var bz = function(d) {
                var b = d.getBBox();
                a(d.pattern, {
                    patternTransform: aZ.format("translate({0},{1})", b.x, b.y)
                })
            };
        var ba = function(bO, bP) {
                var S = {
                    "": [0],
                    none: [0],
                    "-": [3, 1],
                    ".": [1, 1],
                    "-.": [3, 1, 1, 1],
                    "-..": [3, 1, 1, 1, 1, 1],
                    ". ": [1, 3],
                    "- ": [4, 3],
                    "--": [8, 3],
                    "- .": [4, 3, 1, 3],
                    "--.": [8, 3, 1, 3],
                    "--..": [8, 3, 1, 3, 1, 3]
                },
                    bN = bO.node,
                    e = bO.attrs,
                    bT = bO.rotate(),
                    b = function(b1, b2) {
                        b2 = S[at.call(b2)];
                        if (b2) {
                            var b3 = b1.attrs["stroke-width"] || "1",
                                bY = {
                                    round: b3,
                                    square: b3,
                                    butt: 0
                                }[b1.attrs["stroke-linecap"] || bP["stroke-linecap"]] || 0,
                                bZ = [];
                            var b0 = b2[ar];
                            while (b0--) {
                                bZ[b0] = b2[b0] * b3 + ((b0 % 2) ? 1 : -1) * bY
                            }
                            a(bN, {
                                "stroke-dasharray": bZ[an](",")
                            })
                        }
                    };
                bP[ae]("rotation") && (bT = bP.rotation);
                var bU = bg(bT)[bd](a7);
                if (!(bU.length - 1)) {
                    bU = null
                } else {
                    bU[1] = +bU[1];
                    bU[2] = +bU[2]
                }
                br(bT) && bO.rotate(0, true);
                for (var d in bP) {
                    if (bP[ae](d)) {
                        if (!r[ae](d)) {
                            continue
                        }
                        var bW = bP[d];
                        e[d] = bW;
                        switch (d) {
                        case "blur":
                            bO.blur(bW);
                            break;
                        case "rotation":
                            bO.rotate(bW, true);
                            break;
                        case "href":
                        case "title":
                        case "target":
                            var bQ = bN.parentNode;
                            if (at.call(bQ.tagName) != "a") {
                                var bJ = a("a");
                                bQ.insertBefore(bJ, bN);
                                bJ[n](bN);
                                bQ = bJ
                            }
                            if (d == "target" && bW == "blank") {
                                bQ.setAttributeNS(bO.paper.xlink, "show", "new")
                            } else {
                                bQ.setAttributeNS(bO.paper.xlink, d, bW)
                            }
                            break;
                        case "cursor":
                            bN.style.cursor = bW;
                            break;
                        case "clip-rect":
                            var bS = bg(bW)[bd](a7);
                            if (bS[ar] == 4) {
                                bO.clip && bO.clip.parentNode.parentNode.removeChild(bO.clip.parentNode);
                                var bH = a("clipPath"),
                                    bR = a("rect");
                                bH.id = D();
                                a(bR, {
                                    x: bS[0],
                                    y: bS[1],
                                    width: bS[2],
                                    height: bS[3]
                                });
                                bH[n](bR);
                                bO.paper.defs[n](bH);
                                a(bN, {
                                    "clip-path": "url(#" + bH.id + ")"
                                });
                                bO.clip = bR
                            }
                            if (!bW) {
                                var i = J.getElementById(bN.getAttribute("clip-path")[a5](/(^url\(#|\)$)/g, O));
                                i && i.parentNode.removeChild(i);
                                a(bN, {
                                    "clip-path": O
                                });
                                delete bO.clip
                            }
                            break;
                        case "path":
                            if (bO.type == "path") {
                                a(bN, {
                                    d: bW ? e.path = aO(bW) : "M0,0"
                                })
                            }
                            break;
                        case "width":
                            bN[a9](d, bW);
                            if (e.fx) {
                                d = "x";
                                bW = e.x
                            } else {
                                break
                            }
                        case "x":
                            if (e.fx) {
                                bW = -e.x - (e.width || 0)
                            }
                        case "rx":
                            if (d == "rx" && bO.type == "rect") {
                                break
                            }
                        case "cx":
                            bU && (d == "x" || d == "cx") && (bU[1] += bW - e[d]);
                            bN[a9](d, bW);
                            bO.pattern && bz(bO);
                            break;
                        case "height":
                            bN[a9](d, bW);
                            if (e.fy) {
                                d = "y";
                                bW = e.y
                            } else {
                                break
                            }
                        case "y":
                            if (e.fy) {
                                bW = -e.y - (e.height || 0)
                            }
                        case "ry":
                            if (d == "ry" && bO.type == "rect") {
                                break
                            }
                        case "cy":
                            bU && (d == "y" || d == "cy") && (bU[2] += bW - e[d]);
                            bN[a9](d, bW);
                            bO.pattern && bz(bO);
                            break;
                        case "r":
                            if (bO.type == "rect") {
                                a(bN, {
                                    rx: bW,
                                    ry: bW
                                })
                            } else {
                                bN[a9](d, bW)
                            }
                            break;
                        case "src":
                            if (bO.type == "image") {
                                bN.setAttributeNS(bO.paper.xlink, "href", bW)
                            }
                            break;
                        case "stroke-width":
                            bN.style.strokeWidth = bW;
                            bN[a9](d, bW);
                            if (e["stroke-dasharray"]) {
                                b(bO, e["stroke-dasharray"])
                            }
                            break;
                        case "stroke-dasharray":
                            b(bO, bW);
                            break;
                        case "translation":
                            var bX = bg(bW)[bd](a7);
                            bX[0] = +bX[0] || 0;
                            bX[1] = +bX[1] || 0;
                            if (bU) {
                                bU[1] += bX[0];
                                bU[2] += bX[1]
                            }
                            bx.call(bO, bX[0], bX[1]);
                            break;
                        case "scale":
                            bX = bg(bW)[bd](a7);
                            bO.scale(+bX[0] || 1, +bX[1] || +bX[0] || 1, isNaN(br(bX[2])) ? null : +bX[2], isNaN(br(bX[3])) ? null : +bX[3]);
                            break;
                        case V:
                            var bM = bg(bW).match(am);
                            if (bM) {
                                bH = a("pattern");
                                var bK = a("image");
                                bH.id = D();
                                a(bH, {
                                    x: 0,
                                    y: 0,
                                    patternUnits: "userSpaceOnUse",
                                    height: 1,
                                    width: 1
                                });
                                a(bK, {
                                    x: 0,
                                    y: 0
                                });
                                bK.setAttributeNS(bO.paper.xlink, "href", bM[1]);
                                bH[n](bK);
                                var bL = J.createElement("img");
                                bL.style.cssText = "position:absolute;left:-9999em;top-9999em";
                                bL.onload = function() {
                                    a(bH, {
                                        width: this.offsetWidth,
                                        height: this.offsetHeight
                                    });
                                    a(bK, {
                                        width: this.offsetWidth,
                                        height: this.offsetHeight
                                    });
                                    J.body.removeChild(this);
                                    bO.paper.safari()
                                };
                                J.body[n](bL);
                                bL.src = bM[1];
                                bO.paper.defs[n](bH);
                                bN.style.fill = "url(#" + bH.id + ")";
                                a(bN, {
                                    fill: "url(#" + bH.id + ")"
                                });
                                bO.pattern = bH;
                                bO.pattern && bz(bO);
                                break
                            }
                            var E = aZ.getRGB(bW);
                            if (!E.error) {
                                delete bP.gradient;
                                delete e.gradient;
                                !aZ.is(e.opacity, "undefined") && aZ.is(bP.opacity, "undefined") && a(bN, {
                                    opacity: e.opacity
                                });
                                !aZ.is(e["fill-opacity"], "undefined") && aZ.is(bP["fill-opacity"], "undefined") && a(bN, {
                                    "fill-opacity": e["fill-opacity"]
                                })
                            } else {
                                if ((({
                                    circle: 1,
                                    ellipse: 1
                                })[ae](bO.type) || bg(bW).charAt() != "r") && h(bN, bW, bO.paper)) {
                                    e.gradient = bW;
                                    e.fill = "none";
                                    break
                                }
                            }
                            E[ae]("opacity") && a(bN, {
                                "fill-opacity": E.opacity > 1 ? E.opacity / 100 : E.opacity
                            });
                        case "stroke":
                            E = aZ.getRGB(bW);
                            bN[a9](d, E.hex);
                            d == "stroke" && E[ae]("opacity") && a(bN, {
                                "stroke-opacity": E.opacity > 1 ? E.opacity / 100 : E.opacity
                            });
                            break;
                        case "gradient":
                            (({
                                circle: 1,
                                ellipse: 1
                            })[ae](bO.type) || bg(bW).charAt() != "r") && h(bN, bW, bO.paper);
                            break;
                        case "opacity":
                            if (e.gradient && !e[ae]("stroke-opacity")) {
                                a(bN, {
                                    "stroke-opacity": bW > 1 ? bW / 100 : bW
                                })
                            }
                        case "fill-opacity":
                            if (e.gradient) {
                                var bI = J.getElementById(bN.getAttribute(V)[a5](/^url\(#|\)$/g, O));
                                if (bI) {
                                    var bV = bI.getElementsByTagName("stop");
                                    bV[bV[ar] - 1][a9]("stop-opacity", bW)
                                }
                                break
                            }
                        default:
                            d == "font-size" && (bW = bu(bW, 10) + "px");
                            var R = d[a5](/(\-.)/g, function(bY) {
                                return bA.call(bY.substring(1))
                            });
                            bN.style[R] = bW;
                            bN[a9](d, bW);
                            break
                        }
                    }
                }
                by(bO, bP);
                if (bU) {
                    bO.rotate(bU.join(a6))
                } else {
                    br(bT) && bO.rotate(bT, true)
                }
            };
        var aq = 1.2,
            by = function(E, bJ) {
                if (E.type != "text" || !(bJ[ae]("text") || bJ[ae]("font") || bJ[ae]("font-size") || bJ[ae]("x") || bJ[ae]("y"))) {
                    return
                }
                var b = E.attrs,
                    bI = E.node,
                    R = bI.firstChild ? bu(J.defaultView.getComputedStyle(bI.firstChild, O).getPropertyValue("font-size"), 10) : 10;
                if (bJ[ae]("text")) {
                    b.text = bJ.text;
                    while (bI.firstChild) {
                        bI.removeChild(bI.firstChild)
                    }
                    var bK = bg(bJ.text)[bd]("\n");
                    for (var S = 0, bH = bK[ar]; S < bH; S++) {
                        if (bK[S]) {
                            var bL = a("tspan");
                            S && a(bL, {
                                dy: R * aq,
                                x: b.x
                            });
                            bL[n](J.createTextNode(bK[S]));
                            bI[n](bL)
                        }
                    }
                } else {
                    bK = bI.getElementsByTagName("tspan");
                    for (S = 0, bH = bK[ar]; S < bH; S++) {
                        S && a(bK[S], {
                            dy: R * aq,
                            x: b.x
                        })
                    }
                }
                a(bI, {
                    y: b.y
                });
                var d = E.getBBox(),
                    e = b.y - (d.y + d.height / 2);
                e && aZ.is(e, "finite") && a(bI, {
                    y: b.y + e
                })
            },
            P = function(b, d) {
                var e = 0,
                    i = 0;
                this[0] = b;
                this.id = aZ._oid++;
                this.node = b;
                b.raphael = this;
                this.paper = d;
                this.attrs = this.attrs || {};
                this.transformations = [];
                this._ = {
                    tx: 0,
                    ty: 0,
                    rt: {
                        deg: 0,
                        cx: 0,
                        cy: 0
                    },
                    sx: 1,
                    sy: 1
                };
                !d.bottom && (d.bottom = this);
                this.prev = d.top;
                d.top && (d.top.next = this);
                d.top = this;
                this.next = null
            };
        var T = P[aW];
        P[aW].rotate = function(i, d, e) {
            if (this.removed) {
                return this
            }
            if (i == null) {
                if (this._.rt.cx) {
                    return [this._.rt.deg, this._.rt.cx, this._.rt.cy][an](a6)
                }
                return this._.rt.deg
            }
            var b = this.getBBox();
            i = bg(i)[bd](a7);
            if (i[ar] - 1) {
                d = br(i[1]);
                e = br(i[2])
            }
            i = br(i[0]);
            if (d != null && d !== false) {
                this._.rt.deg = i
            } else {
                this._.rt.deg += i
            }(e == null) && (d = null);
            this._.rt.cx = d;
            this._.rt.cy = e;
            d = d == null ? b.x + b.width / 2 : d;
            e = e == null ? b.y + b.height / 2 : e;
            if (this._.rt.deg) {
                this.transformations[0] = aZ.format("rotate({0} {1} {2})", this._.rt.deg, d, e);
                this.clip && a(this.clip, {
                    transform: aZ.format("rotate({0} {1} {2})", -this._.rt.deg, d, e)
                })
            } else {
                this.transformations[0] = O;
                this.clip && a(this.clip, {
                    transform: O
                })
            }
            a(this.node, {
                transform: this.transformations[an](a6)
            });
            return this
        };
        P[aW].hide = function() {
            !this.removed && (this.node.style.display = "none");
            return this
        };
        P[aW].show = function() {
            !this.removed && (this.node.style.display = "");
            return this
        };
        P[aW].remove = function() {
            if (this.removed) {
                return
            }
            bj(this, this.paper);
            this.node.parentNode.removeChild(this.node);
            for (var b in this) {
                delete this[b]
            }
            this.removed = true
        };
        P[aW].getBBox = function() {
            if (this.removed) {
                return this
            }
            if (this.type == "path") {
                return aM(this.attrs.path)
            }
            if (this.node.style.display == "none") {
                this.show();
                var R = true
            }
            var d = {};
            try {
                d = this.node.getBBox()
            } catch (E) {} finally {
                d = d || {}
            }
            if (this.type == "text") {
                d = {
                    x: d.x,
                    y: Infinity,
                    width: 0,
                    height: 0
                };
                for (var S = 0, bH = this.node.getNumberOfChars(); S < bH; S++) {
                    var b = this.node.getExtentOfChar(S);
                    (b.y < d.y) && (d.y = b.y);
                    (b.y + b.height - d.y > d.height) && (d.height = b.y + b.height - d.y);
                    (b.x + b.width - d.x > d.width) && (d.width = b.x + b.width - d.x)
                }
            }
            R && this.hide();
            return d
        };
        P[aW].attr = function(R, bK) {
            if (this.removed) {
                return this
            }
            if (R == null) {
                var bI = {};
                for (var b in this.attrs) {
                    if (this.attrs[ae](b)) {
                        bI[b] = this.attrs[b]
                    }
                }
                this._.rt.deg && (bI.rotation = this.rotate());
                (this._.sx != 1 || this._.sy != 1) && (bI.scale = this.scale());
                bI.gradient && bI.fill == "none" && (bI.fill = bI.gradient) && delete bI.gradient;
                return bI
            }
            if (bK == null && aZ.is(R, bh)) {
                if (R == "translation") {
                    return bx.call(this)
                }
                if (R == "rotation") {
                    return this.rotate()
                }
                if (R == "scale") {
                    return this.scale()
                }
                if (R == V && this.attrs.fill == "none" && this.attrs.gradient) {
                    return this.attrs.gradient
                }
                return this.attrs[R]
            }
            if (bK == null && aZ.is(R, p)) {
                var bL = {};
                for (var d = 0, e = R.length; d < e; d++) {
                    bL[R[d]] = this.attr(R[d])
                }
                return bL
            }
            if (bK != null) {
                var bH = {};
                bH[R] = bK
            } else {
                if (R != null && aZ.is(R, "object")) {
                    bH = R
                }
            }
            for (var E in this.paper.customAttributes) {
                if (this.paper.customAttributes[ae](E) && bH[ae](E) && aZ.is(this.paper.customAttributes[E], "function")) {
                    var S = this.paper.customAttributes[E].apply(this, [][z](bH[E]));
                    this.attrs[E] = bH[E];
                    for (var bJ in S) {
                        if (S[ae](bJ)) {
                            bH[bJ] = S[bJ]
                        }
                    }
                }
            }
            ba(this, bH);
            return this
        };
        P[aW].toFront = function() {
            if (this.removed) {
                return this
            }
            this.node.parentNode[n](this.node);
            var b = this.paper;
            b.top != this && bs(this, b);
            return this
        };
        P[aW].toBack = function() {
            if (this.removed) {
                return this
            }
            if (this.node.parentNode.firstChild != this.node) {
                this.node.parentNode.insertBefore(this.node, this.node.parentNode.firstChild);
                bq(this, this.paper);
                var b = this.paper
            }
            return this
        };
        P[aW].insertAfter = function(b) {
            if (this.removed) {
                return this
            }
            var d = b.node || b[b.length - 1].node;
            if (d.nextSibling) {
                d.parentNode.insertBefore(this.node, d.nextSibling)
            } else {
                d.parentNode[n](this.node)
            }
            aj(this, b, this.paper);
            return this
        };
        P[aW].insertBefore = function(b) {
            if (this.removed) {
                return this
            }
            var d = b.node || b[0].node;
            d.parentNode.insertBefore(this.node, d);
            ak(this, b, this.paper);
            return this
        };
        P[aW].blur = function(e) {
            var i = this;
            if (+e !== 0) {
                var d = a("filter"),
                    b = a("feGaussianBlur");
                i.attrs.blur = e;
                d.id = D();
                a(b, {
                    stdDeviation: +e || 1.5
                });
                d.appendChild(b);
                i.paper.defs.appendChild(d);
                i._blur = d;
                a(i.node, {
                    filter: "url(#" + d.id + ")"
                })
            } else {
                if (i._blur) {
                    i._blur.parentNode.removeChild(i._blur);
                    delete i._blur;
                    delete i.attrs.blur
                }
                i.node.removeAttribute("filter")
            }
        };
        var bk = function(i, E, R, d) {
                var b = a("circle");
                i.canvas && i.canvas[n](b);
                var e = new P(b, i);
                e.attrs = {
                    cx: E,
                    cy: R,
                    r: d,
                    fill: "none",
                    stroke: "#000"
                };
                e.type = "circle";
                a(b, e.attrs);
                return e
            },
            bo = function(E, S, bH, R, d, e) {
                var b = a("rect");
                E.canvas && E.canvas[n](b);
                var i = new P(b, E);
                i.attrs = {
                    x: S,
                    y: bH,
                    width: R,
                    height: d,
                    r: e || 0,
                    rx: e || 0,
                    ry: e || 0,
                    fill: "none",
                    stroke: "#000"
                };
                i.type = "rect";
                a(b, i.attrs);
                return i
            },
            bl = function(E, R, S, e, i) {
                var b = a("ellipse");
                E.canvas && E.canvas[n](b);
                var d = new P(b, E);
                d.attrs = {
                    cx: R,
                    cy: S,
                    rx: e,
                    ry: i,
                    fill: "none",
                    stroke: "#000"
                };
                d.type = "ellipse";
                a(b, d.attrs);
                return d
            },
            bm = function(E, i, S, bH, R, d) {
                var b = a("image");
                a(b, {
                    x: S,
                    y: bH,
                    width: R,
                    height: d,
                    preserveAspectRatio: "none"
                });
                b.setAttributeNS(E.xlink, "href", i);
                E.canvas && E.canvas[n](b);
                var e = new P(b, E);
                e.attrs = {
                    x: S,
                    y: bH,
                    width: R,
                    height: d,
                    src: i
                };
                e.type = "image";
                return e
            },
            bp = function(e, E, R, i) {
                var b = a("text");
                a(b, {
                    x: E,
                    y: R,
                    "text-anchor": "middle"
                });
                e.canvas && e.canvas[n](b);
                var d = new P(b, e);
                d.attrs = {
                    x: E,
                    y: R,
                    "text-anchor": "middle",
                    text: i,
                    font: r.font,
                    stroke: "none",
                    fill: "#000"
                };
                d.type = "text";
                ba(d, d.attrs);
                return d
            },
            bb = function(d, b) {
                this.width = d || this.width;
                this.height = b || this.height;
                this.canvas[a9]("width", this.width);
                this.canvas[a9]("height", this.height);
                return this
            },
            B = function() {
                var d = Y[o](0, arguments),
                    e = d && d.container,
                    R = d.x,
                    S = d.y,
                    E = d.width,
                    i = d.height;
                if (!e) {
                    throw new Error("SVG container not found.")
                }
                var b = a("svg");
                R = R || 0;
                S = S || 0;
                E = E || 512;
                i = i || 342;
                a(b, {
                    xmlns: "http://www.w3.org/2000/svg",
                    version: 1.1,
                    width: E,
                    height: i
                });
                if (e == 1) {
                    b.style.cssText = "position:absolute;left:" + R + "px;top:" + S + "px";
                    J.body[n](b)
                } else {
                    if (e.firstChild) {
                        e.insertBefore(b, e.firstChild)
                    } else {
                        e[n](b)
                    }
                }
                e = new aF;
                e.width = E;
                e.height = i;
                e.canvas = b;
                aS.call(e, e, aZ.fn);
                e.clear();
                return e
            };
        aG.clear = function() {
            var b = this.canvas;
            while (b.firstChild) {
                b.removeChild(b.firstChild)
            }
            this.bottom = this.top = null;
            (this.desc = a("desc"))[n](J.createTextNode("Created with Rapha\xebl"));
            b[n](this.desc);
            b[n](this.defs = a("defs"))
        };
        aG.remove = function() {
            this.canvas.parentNode && this.canvas.parentNode.removeChild(this.canvas);
            for (var b in this) {
                this[b] = a2(b)
            }
        }
    }
    if (aZ.vml) {
        var au = {
            M: "m",
            L: "l",
            C: "c",
            Z: "x",
            m: "t",
            l: "r",
            c: "v",
            z: "x"
        },
            u = /([clmz]),?([^clmz]*)/gi,
            v = / progid:\S+Blur\([^\)]+\)/g,
            bC = /-?[^,\s-]+/g,
            A = 1000 + a6 + 1000,
            bG = 10,
            aN = {
                path: 1,
                rect: 1
            },
            aJ = function(bI) {
                var bL = /[ahqstv]/ig,
                    b = aO;
                bg(bI).match(bL) && (b = aI);
                bL = /[clmz]/g;
                if (b == aO && !bg(bI).match(bL)) {
                    var bK = bg(bI)[a5](u, function(i, bN, bM) {
                        var bQ = [],
                            bO = at.call(bN) == "m",
                            bP = au[bN];
                        bM[a5](bC, function(bR) {
                            if (bO && bQ[ar] == 2) {
                                bP += bQ + au[bN == "m" ? "l" : "L"];
                                bQ = []
                            }
                            bQ[aX](a4(bR * bG))
                        });
                        return bP + bQ
                    });
                    return bK
                }
                var bH = b(bI),
                    S, bJ;
                bK = [];
                for (var d = 0, e = bH[ar]; d < e; d++) {
                    S = bH[d];
                    bJ = at.call(bH[d][0]);
                    bJ == "z" && (bJ = "x");
                    for (var E = 1, R = S[ar]; E < R; E++) {
                        bJ += a4(S[E] * bG) + (E != R - 1 ? "," : O)
                    }
                    bK[aX](bJ)
                }
                return bK[an](a6)
            };
        aZ[bv] = function() {
            return "Your browser doesn\u2019t support SVG. Falling down to VML.\nYou are running Rapha\xebl " + this.version
        };
        bn = function(R, S) {
            var e = C("group");
            e.style.cssText = "position:absolute;left:0;top:0;width:" + S.width + "px;height:" + S.height + "px";
            e.coordsize = S.coordsize;
            e.coordorigin = S.coordorigin;
            var d = C("shape"),
                i = d.style;
            i.width = S.width + "px";
            i.height = S.height + "px";
            d.coordsize = A;
            d.coordorigin = S.coordorigin;
            e[n](d);
            var E = new P(d, e, S),
                b = {
                    fill: "none",
                    stroke: "#000"
                };
            R && (b.path = R);
            E.type = "path";
            E.path = [];
            E.Path = O;
            ba(E, b);
            S.canvas[n](e);
            return E
        };
        ba = function(bL, bO) {
            bL.attrs = bL.attrs || {};
            var bK = bL.node,
                b = bL.attrs,
                bR = bK.style,
                bV, bI = (bO.x != b.x || bO.y != b.y || bO.width != b.width || bO.height != b.height || bO.r != b.r) && bL.type == "rect",
                bQ = bL;
            for (var bN in bO) {
                if (bO[ae](bN)) {
                    b[bN] = bO[bN]
                }
            }
            if (bI) {
                b.path = a1(b.x, b.y, b.width, b.height, b.r);
                bL.X = b.x;
                bL.Y = b.y;
                bL.W = b.width;
                bL.H = b.height
            }
            bO.href && (bK.href = bO.href);
            bO.title && (bK.title = bO.title);
            bO.target && (bK.target = bO.target);
            bO.cursor && (bR.cursor = bO.cursor);
            "blur" in bO && bL.blur(bO.blur);
            if (bO.path && bL.type == "path" || bI) {
                bK.path = aJ(b.path)
            }
            if (bO.rotation != null) {
                bL.rotate(bO.rotation, true)
            }
            if (bO.translation) {
                bV = bg(bO.translation)[bd](a7);
                bx.call(bL, bV[0], bV[1]);
                if (bL._.rt.cx != null) {
                    bL._.rt.cx += +bV[0];
                    bL._.rt.cy += +bV[1];
                    bL.setBox(bL.attrs, bV[0], bV[1])
                }
            }
            if (bO.scale) {
                bV = bg(bO.scale)[bd](a7);
                bL.scale(+bV[0] || 1, +bV[1] || +bV[0] || 1, +bV[2] || null, +bV[3] || null)
            }
            if ("clip-rect" in bO) {
                var bP = bg(bO["clip-rect"])[bd](a7);
                if (bP[ar] == 4) {
                    bP[2] = +bP[2] + (+bP[0]);
                    bP[3] = +bP[3] + (+bP[1]);
                    var e = bK.clipRect || J.createElement("div"),
                        i = e.style,
                        R = bK.parentNode;
                    i.clip = aZ.format("rect({1}px {2}px {3}px {0}px)", bP);
                    if (!bK.clipRect) {
                        i.position = "absolute";
                        i.top = 0;
                        i.left = 0;
                        i.width = bL.paper.width + "px";
                        i.height = bL.paper.height + "px";
                        R.parentNode.insertBefore(e, R);
                        e[n](R);
                        bK.clipRect = e
                    }
                }
                if (!bO["clip-rect"]) {
                    bK.clipRect && (bK.clipRect.style.clip = O)
                }
            }
            if (bL.type == "image" && bO.src) {
                bK.src = bO.src
            }
            if (bL.type == "image" && bO.opacity) {
                bK.filterOpacity = az + ".Alpha(opacity=" + (bO.opacity * 100) + ")";
                bR.filter = (bK.filterMatrix || O) + (bK.filterOpacity || O)
            }
            bO.font && (bR.font = bO.font);
            bO["font-family"] && (bR.fontFamily = '"' + bO["font-family"][bd](",")[0][a5](/^['"]+|['"]+$/g, O) + '"');
            bO["font-size"] && (bR.fontSize = bO["font-size"]);
            bO["font-weight"] && (bR.fontWeight = bO["font-weight"]);
            bO["font-style"] && (bR.fontStyle = bO["font-style"]);
            if (bO.opacity != null || bO["stroke-width"] != null || bO.fill != null || bO.stroke != null || bO["stroke-width"] != null || bO["stroke-opacity"] != null || bO["fill-opacity"] != null || bO["stroke-dasharray"] != null || bO["stroke-miterlimit"] != null || bO["stroke-linejoin"] != null || bO["stroke-linecap"] != null) {
                bK = bL.shape || bK;
                var E = (bK.getElementsByTagName(V) && bK.getElementsByTagName(V)[0]),
                    bH = false;
                !E && (bH = E = C(V));
                if ("fill-opacity" in bO || "opacity" in bO) {
                    var bM = ((+b["fill-opacity"] + 1 || 2) - 1) * ((+b.opacity + 1 || 2) - 1) * ((+aZ.getRGB(bO.fill).o + 1 || 2) - 1);
                    bM = ay(ax(bM, 0), 1);
                    E.opacity = bM
                }
                bO.fill && (E.on = true);
                if (E.on == null || bO.fill == "none") {
                    E.on = false
                }
                if (E.on && bO.fill) {
                    var S = bO.fill.match(am);
                    if (S) {
                        E.src = S[1];
                        E.type = "tile"
                    } else {
                        E.color = aZ.getRGB(bO.fill).hex;
                        E.src = O;
                        E.type = "solid";
                        if (aZ.getRGB(bO.fill).error && (bQ.type in {
                            circle: 1,
                            ellipse: 1
                        } || bg(bO.fill).charAt() != "r") && h(bQ, bO.fill)) {
                            b.fill = "none";
                            b.gradient = bO.fill
                        }
                    }
                }
                bH && bK[n](E);
                var bS = (bK.getElementsByTagName("stroke") && bK.getElementsByTagName("stroke")[0]),
                    bJ = false;
                !bS && (bJ = bS = C("stroke"));
                if ((bO.stroke && bO.stroke != "none") || bO["stroke-width"] || bO["stroke-opacity"] != null || bO["stroke-dasharray"] || bO["stroke-miterlimit"] || bO["stroke-linejoin"] || bO["stroke-linecap"]) {
                    bS.on = true
                }(bO.stroke == "none" || bS.on == null || bO.stroke == 0 || bO["stroke-width"] == 0) && (bS.on = false);
                var bT = aZ.getRGB(bO.stroke);
                bS.on && bO.stroke && (bS.color = bT.hex);
                bM = ((+b["stroke-opacity"] + 1 || 2) - 1) * ((+b.opacity + 1 || 2) - 1) * ((+bT.o + 1 || 2) - 1);
                var bU = (br(bO["stroke-width"]) || 1) * 0.75;
                bM = ay(ax(bM, 0), 1);
                bO["stroke-width"] == null && (bU = b["stroke-width"]);
                bO["stroke-width"] && (bS.weight = bU);
                bU && bU < 1 && (bM *= bU) && (bS.weight = 1);
                bS.opacity = bM;
                bO["stroke-linejoin"] && (bS.joinstyle = bO["stroke-linejoin"] || "miter");
                bS.miterlimit = bO["stroke-miterlimit"] || 8;
                bO["stroke-linecap"] && (bS.endcap = bO["stroke-linecap"] == "butt" ? "flat" : bO["stroke-linecap"] == "square" ? "square" : "round");
                if (bO["stroke-dasharray"]) {
                    var d = {
                        "-": "shortdash",
                        ".": "shortdot",
                        "-.": "shortdashdot",
                        "-..": "shortdashdotdot",
                        ". ": "dot",
                        "- ": "dash",
                        "--": "longdash",
                        "- .": "dashdot",
                        "--.": "longdashdot",
                        "--..": "longdashdotdot"
                    };
                    bS.dashstyle = d[ae](bO["stroke-dasharray"]) ? d[bO["stroke-dasharray"]] : O
                }
                bJ && bK[n](bS)
            }
            if (bQ.type == "text") {
                bR = bQ.paper.span.style;
                b.font && (bR.font = b.font);
                b["font-family"] && (bR.fontFamily = b["font-family"]);
                b["font-size"] && (bR.fontSize = b["font-size"]);
                b["font-weight"] && (bR.fontWeight = b["font-weight"]);
                b["font-style"] && (bR.fontStyle = b["font-style"]);
                bQ.node.string && (bQ.paper.span.innerHTML = bg(bQ.node.string)[a5](/</g, "&#60;")[a5](/&/g, "&#38;")[a5](/\n/g, "<br>"));
                bQ.W = b.w = bQ.paper.span.offsetWidth;
                bQ.H = b.h = bQ.paper.span.offsetHeight;
                bQ.X = b.x;
                bQ.Y = b.y + a4(bQ.H / 2);
                switch (b["text-anchor"]) {
                case "start":
                    bQ.node.style["v-text-align"] = "left";
                    bQ.bbx = a4(bQ.W / 2);
                    break;
                case "end":
                    bQ.node.style["v-text-align"] = "right";
                    bQ.bbx = -a4(bQ.W / 2);
                    break;
                default:
                    bQ.node.style["v-text-align"] = "center";
                    break
                }
            }
        };
        h = function(bK, bH) {
            bK.attrs = bK.attrs || {};
            var d = bK.attrs,
                R, bL = "linear",
                S = ".5 .5";
            bK.attrs.gradient = bH;
            bH = bg(bH)[a5](a0, function(i, bM, bN) {
                bL = "radial";
                if (bM && bN) {
                    bM = br(bM);
                    bN = br(bN);
                    aT(bM - 0.5, 2) + aT(bN - 0.5, 2) > 0.25 && (bN = av.sqrt(0.25 - aT(bM - 0.5, 2)) * ((bN > 0.5) * 2 - 1) + 0.5);
                    S = bM + a6 + bN
                }
                return O
            });
            bH = bH[bd](/\s*\-\s*/);
            if (bL == "linear") {
                var b = bH.shift();
                b = -br(b);
                if (isNaN(b)) {
                    return null
                }
            }
            var E = aH(bH);
            if (!E) {
                return null
            }
            bK = bK.shape || bK.node;
            R = bK.getElementsByTagName(V)[0] || C(V);
            !R.parentNode && bK.appendChild(R);
            if (E[ar]) {
                R.on = true;
                R.method = "none";
                R.color = E[0].color;
                R.color2 = E[E[ar] - 1].color;
                var e = [];
                for (var bI = 0, bJ = E[ar]; bI < bJ; bI++) {
                    E[bI].offset && e[aX](E[bI].offset + a6 + E[bI].color)
                }
                R.colors && (R.colors.value = e[ar] ? e[an]() : "0% " + R.color);
                if (bL == "radial") {
                    R.type = "gradientradial";
                    R.focus = "100%";
                    R.focussize = S;
                    R.focusposition = S
                } else {
                    R.type = "gradient";
                    R.angle = (270 - b) % 360
                }
            }
            return 1
        };
        P = function(d, b, S) {
            var e = 0,
                i = 0,
                E = 0,
                R = 1;
            this[0] = d;
            this.id = aZ._oid++;
            this.node = d;
            d.raphael = this;
            this.X = 0;
            this.Y = 0;
            this.attrs = {};
            this.Group = b;
            this.paper = S;
            this._ = {
                tx: 0,
                ty: 0,
                rt: {
                    deg: 0
                },
                sx: 1,
                sy: 1
            };
            !S.bottom && (S.bottom = this);
            this.prev = S.top;
            S.top && (S.top.next = this);
            S.top = this;
            this.next = null
        };
        T = P[aW];
        T.rotate = function(e, b, d) {
            if (this.removed) {
                return this
            }
            if (e == null) {
                if (this._.rt.cx) {
                    return [this._.rt.deg, this._.rt.cx, this._.rt.cy][an](a6)
                }
                return this._.rt.deg
            }
            e = bg(e)[bd](a7);
            if (e[ar] - 1) {
                b = br(e[1]);
                d = br(e[2])
            }
            e = br(e[0]);
            if (b != null) {
                this._.rt.deg = e
            } else {
                this._.rt.deg += e
            }
            d == null && (b = null);
            this._.rt.cx = b;
            this._.rt.cy = d;
            this.setBox(this.attrs, b, d);
            this.Group.style.rotation = this._.rt.deg;
            return this
        };
        T.setBox = function(bK, d, e) {
            if (this.removed) {
                return this
            }
            var R = this.Group.style,
                bJ = (this.shape && this.shape.style) || this.node.style;
            bK = bK || {};
            for (var bH in bK) {
                if (bK[ae](bH)) {
                    this.attrs[bH] = bK[bH]
                }
            }
            d = d || this._.rt.cx;
            e = e || this._.rt.cy;
            var b = this.attrs,
                bO, bP, bN, S;
            switch (this.type) {
            case "circle":
                bO = b.cx - b.r;
                bP = b.cy - b.r;
                bN = S = b.r * 2;
                break;
            case "ellipse":
                bO = b.cx - b.rx;
                bP = b.cy - b.ry;
                bN = b.rx * 2;
                S = b.ry * 2;
                break;
            case "image":
                bO = +b.x;
                bP = +b.y;
                bN = b.width || 0;
                S = b.height || 0;
                break;
            case "text":
                this.textpath.v = ["m", a4(b.x), ", ", a4(b.y - 2), "l", a4(b.x) + 1, ", ", a4(b.y - 2)][an](O);
                bO = b.x - a4(this.W / 2);
                bP = b.y - this.H / 2;
                bN = this.W;
                S = this.H;
                break;
            case "rect":
            case "path":
                if (!this.attrs.path) {
                    bO = 0;
                    bP = 0;
                    bN = this.paper.width;
                    S = this.paper.height
                } else {
                    var E = aM(this.attrs.path);
                    bO = E.x;
                    bP = E.y;
                    bN = E.width;
                    S = E.height
                }
                break;
            default:
                bO = 0;
                bP = 0;
                bN = this.paper.width;
                S = this.paper.height;
                break
            }
            d = (d == null) ? bO + bN / 2 : d;
            e = (e == null) ? bP + S / 2 : e;
            var bI = d - this.paper.width / 2,
                bM = e - this.paper.height / 2,
                bL;
            R.left != (bL = bI + "px") && (R.left = bL);
            R.top != (bL = bM + "px") && (R.top = bL);
            this.X = aN[ae](this.type) ? -bI : bO;
            this.Y = aN[ae](this.type) ? -bM : bP;
            this.W = bN;
            this.H = S;
            if (aN[ae](this.type)) {
                bJ.left != (bL = -bI * bG + "px") && (bJ.left = bL);
                bJ.top != (bL = -bM * bG + "px") && (bJ.top = bL)
            } else {
                if (this.type == "text") {
                    bJ.left != (bL = -bI + "px") && (bJ.left = bL);
                    bJ.top != (bL = -bM + "px") && (bJ.top = bL)
                } else {
                    R.width != (bL = this.paper.width + "px") && (R.width = bL);
                    R.height != (bL = this.paper.height + "px") && (R.height = bL);
                    bJ.left != (bL = bO - bI + "px") && (bJ.left = bL);
                    bJ.top != (bL = bP - bM + "px") && (bJ.top = bL);
                    bJ.width != (bL = bN + "px") && (bJ.width = bL);
                    bJ.height != (bL = S + "px") && (bJ.height = bL)
                }
            }
        };
        T.hide = function() {
            !this.removed && (this.Group.style.display = "none");
            return this
        };
        T.show = function() {
            !this.removed && (this.Group.style.display = "block");
            return this
        };
        T.getBBox = function() {
            if (this.removed) {
                return this
            }
            if (aN[ae](this.type)) {
                return aM(this.attrs.path)
            }
            return {
                x: this.X + (this.bbx || 0),
                y: this.Y,
                width: this.W,
                height: this.H
            }
        };
        T.remove = function() {
            if (this.removed) {
                return
            }
            bj(this, this.paper);
            this.node.parentNode.removeChild(this.node);
            this.Group.parentNode.removeChild(this.Group);
            this.shape && this.shape.parentNode.removeChild(this.shape);
            for (var b in this) {
                delete this[b]
            }
            this.removed = true
        };
        T.attr = function(E, bJ) {
            if (this.removed) {
                return this
            }
            if (E == null) {
                var bH = {};
                for (var b in this.attrs) {
                    if (this.attrs[ae](b)) {
                        bH[b] = this.attrs[b]
                    }
                }
                this._.rt.deg && (bH.rotation = this.rotate());
                (this._.sx != 1 || this._.sy != 1) && (bH.scale = this.scale());
                bH.gradient && bH.fill == "none" && (bH.fill = bH.gradient) && delete bH.gradient;
                return bH
            }
            if (bJ == null && aZ.is(E, "string")) {
                if (E == "translation") {
                    return bx.call(this)
                }
                if (E == "rotation") {
                    return this.rotate()
                }
                if (E == "scale") {
                    return this.scale()
                }
                if (E == V && this.attrs.fill == "none" && this.attrs.gradient) {
                    return this.attrs.gradient
                }
                return this.attrs[E]
            }
            if (this.attrs && bJ == null && aZ.is(E, p)) {
                var d, bK = {};
                for (b = 0, d = E[ar]; b < d; b++) {
                    bK[E[b]] = this.attr(E[b])
                }
                return bK
            }
            var S;
            if (bJ != null) {
                S = {};
                S[E] = bJ
            }
            bJ == null && aZ.is(E, "object") && (S = E);
            if (S) {
                for (var e in this.paper.customAttributes) {
                    if (this.paper.customAttributes[ae](e) && S[ae](e) && aZ.is(this.paper.customAttributes[e], "function")) {
                        var R = this.paper.customAttributes[e].apply(this, [][z](S[e]));
                        this.attrs[e] = S[e];
                        for (var bI in R) {
                            if (R[ae](bI)) {
                                S[bI] = R[bI]
                            }
                        }
                    }
                }
                if (S.text && this.type == "text") {
                    this.node.string = S.text
                }
                ba(this, S);
                if (S.gradient && (({
                    circle: 1,
                    ellipse: 1
                })[ae](this.type) || bg(S.gradient).charAt() != "r")) {
                    h(this, S.gradient)
                }(!aN[ae](this.type) || this._.rt.deg) && this.setBox(this.attrs)
            }
            return this
        };
        T.toFront = function() {
            !this.removed && this.Group.parentNode[n](this.Group);
            this.paper.top != this && bs(this, this.paper);
            return this
        };
        T.toBack = function() {
            if (this.removed) {
                return this
            }
            if (this.Group.parentNode.firstChild != this.Group) {
                this.Group.parentNode.insertBefore(this.Group, this.Group.parentNode.firstChild);
                bq(this, this.paper)
            }
            return this
        };
        T.insertAfter = function(b) {
            if (this.removed) {
                return this
            }
            if (b.constructor == a8) {
                b = b[b.length - 1]
            }
            if (b.Group.nextSibling) {
                b.Group.parentNode.insertBefore(this.Group, b.Group.nextSibling)
            } else {
                b.Group.parentNode[n](this.Group)
            }
            aj(this, b, this.paper);
            return this
        };
        T.insertBefore = function(b) {
            if (this.removed) {
                return this
            }
            if (b.constructor == a8) {
                b = b[0]
            }
            b.Group.parentNode.insertBefore(this.Group, b.Group);
            ak(this, b, this.paper);
            return this
        };
        T.blur = function(e) {
            var d = this.node.runtimeStyle,
                b = d.filter;
            b = b.replace(v, O);
            if (+e !== 0) {
                this.attrs.blur = e;
                d.filter = b + a6 + az + ".Blur(pixelradius=" + (+e || 1.5) + ")";
                d.margin = aZ.format("-{0}px 0 0 -{0}px", a4(+e || 1.5))
            } else {
                d.filter = b;
                d.margin = 0;
                delete this.attrs.blur
            }
        };
        bk = function(R, S, bH, i) {
            var b = C("group"),
                d = C("oval"),
                e = d.style;
            b.style.cssText = "position:absolute;left:0;top:0;width:" + R.width + "px;height:" + R.height + "px";
            b.coordsize = A;
            b.coordorigin = R.coordorigin;
            b[n](d);
            var E = new P(d, b, R);
            E.type = "circle";
            ba(E, {
                stroke: "#000",
                fill: "none"
            });
            E.attrs.cx = S;
            E.attrs.cy = bH;
            E.attrs.r = i;
            E.setBox({
                x: S - i,
                y: bH - i,
                width: i * 2,
                height: i * 2
            });
            R.canvas[n](b);
            return E
        };

        function a1(i, E, e, b, d) {
            if (d) {
                return aZ.format("M{0},{1}l{2},0a{3},{3},0,0,1,{3},{3}l0,{5}a{3},{3},0,0,1,{4},{3}l{6},0a{3},{3},0,0,1,{4},{4}l0,{7}a{3},{3},0,0,1,{3},{4}z", i + d, E, e - d * 2, d, -d, b - d * 2, d * 2 - e, d * 2 - b)
            } else {
                return aZ.format("M{0},{1}l{2},0,0,{3},{4},0z", i, E, e, b, -e)
            }
        }
        bo = function(R, bH, bI, S, d, i) {
            var e = a1(bH, bI, S, d, i),
                E = R.path(e),
                b = E.attrs;
            E.X = b.x = bH;
            E.Y = b.y = bI;
            E.W = b.width = S;
            E.H = b.height = d;
            b.r = i;
            b.path = e;
            E.type = "rect";
            return E
        };
        bl = function(S, bH, bI, E, R) {
            var b = C("group"),
                d = C("oval"),
                e = d.style;
            b.style.cssText = "position:absolute;left:0;top:0;width:" + S.width + "px;height:" + S.height + "px";
            b.coordsize = A;
            b.coordorigin = S.coordorigin;
            b[n](d);
            var i = new P(d, b, S);
            i.type = "ellipse";
            ba(i, {
                stroke: "#000"
            });
            i.attrs.cx = bH;
            i.attrs.cy = bI;
            i.attrs.rx = E;
            i.attrs.ry = R;
            i.setBox({
                x: bH - E,
                y: bI - R,
                width: E * 2,
                height: R * 2
            });
            S.canvas[n](b);
            return i
        };
        bm = function(R, E, bH, bI, S, d) {
            var b = C("group"),
                e = C("image");
            b.style.cssText = "position:absolute;left:0;top:0;width:" + R.width + "px;height:" + R.height + "px";
            b.coordsize = A;
            b.coordorigin = R.coordorigin;
            e.src = E;
            b[n](e);
            var i = new P(e, b, R);
            i.type = "image";
            i.attrs.src = E;
            i.attrs.x = bH;
            i.attrs.y = bI;
            i.attrs.w = S;
            i.attrs.h = d;
            i.setBox({
                x: bH,
                y: bI,
                width: S,
                height: d
            });
            R.canvas[n](b);
            return i
        };
        bp = function(bI, bJ, bK, bH) {
            var d = C("group"),
                b = C("shape"),
                i = b.style,
                E = C("path"),
                R = E.style,
                e = C("textpath");
            d.style.cssText = "position:absolute;left:0;top:0;width:" + bI.width + "px;height:" + bI.height + "px";
            d.coordsize = A;
            d.coordorigin = bI.coordorigin;
            E.v = aZ.format("m{0},{1}l{2},{1}", a4(bJ * 10), a4(bK * 10), a4(bJ * 10) + 1);
            E.textpathok = true;
            i.width = bI.width;
            i.height = bI.height;
            e.string = bg(bH);
            e.on = true;
            b[n](e);
            b[n](E);
            d[n](b);
            var S = new P(e, d, bI);
            S.shape = b;
            S.textpath = E;
            S.type = "text";
            S.attrs.text = bH;
            S.attrs.x = bJ;
            S.attrs.y = bK;
            S.attrs.w = 1;
            S.attrs.h = 1;
            ba(S, {
                font: r.font,
                stroke: "none",
                fill: "#000"
            });
            S.setBox();
            bI.canvas[n](d);
            return S
        };
        bb = function(e, d) {
            var b = this.canvas.style;
            e == +e && (e += "px");
            d == +d && (d += "px");
            b.width = e;
            b.height = d;
            b.clip = "rect(0 " + e + " " + d + " 0)";
            return this
        };
        var C;
        J.createStyleSheet().addRule(".rvml", "behavior:url(#default#VML)");
        try {
            !J.namespaces.rvml && J.namespaces.add("rvml", "urn:schemas-microsoft-com:vml");
            C = function(b) {
                return J.createElement("<rvml:" + b + ' class="rvml">')
            }
        } catch (N) {
            C = function(b) {
                return J.createElement("<" + b + ' xmlns="urn:schemas-microsoft.com:vml" class="rvml">')
            }
        }
        B = function() {
            var d = Y[o](0, arguments),
                e = d.container,
                E = d.height,
                S, bH = d.width,
                bI = d.x,
                bJ = d.y;
            if (!e) {
                throw new Error("VML container not found.")
            }
            var R = new aF,
                b = R.canvas = J.createElement("div"),
                i = b.style;
            bI = bI || 0;
            bJ = bJ || 0;
            bH = bH || 512;
            E = E || 342;
            bH == +bH && (bH += "px");
            E == +E && (E += "px");
            R.width = 1000;
            R.height = 1000;
            R.coordsize = bG * 1000 + a6 + bG * 1000;
            R.coordorigin = "0 0";
            R.span = J.createElement("span");
            R.span.style.cssText = "position:absolute;left:-9999em;top:-9999em;padding:0;margin:0;line-height:1;display:inline;";
            b[n](R.span);
            i.cssText = aZ.format("top:0;left:0;width:{0};height:{1};display:inline-block;position:relative;clip:rect(0 {0} {1} 0);overflow:hidden", bH, E);
            if (e == 1) {
                J.body[n](b);
                i.left = bI + "px";
                i.top = bJ + "px";
                i.position = "absolute"
            } else {
                if (e.firstChild) {
                    e.insertBefore(b, e.firstChild)
                } else {
                    e[n](b)
                }
            }
            aS.call(R, R, aZ.fn);
            return R
        };
        aG.clear = function() {
            this.canvas.innerHTML = O;
            this.span = J.createElement("span");
            this.span.style.cssText = "position:absolute;left:-9999em;top:-9999em;padding:0;margin:0;line-height:1;display:inline;";
            this.canvas[n](this.span);
            this.bottom = this.top = null
        };
        aG.remove = function() {
            this.canvas.parentNode.removeChild(this.canvas);
            for (var b in this) {
                this[b] = a2(b)
            }
            return true
        }
    }
    var bD = navigator.userAgent.match(/Version\/(.*?)\s/);
    if ((navigator.vendor == "Apple Computer, Inc.") && (bD && bD[1] < 4 || navigator.platform.slice(0, 2) == "iP")) {
        aG.safari = function() {
            var b = this.rect(-99, -99, this.width + 99, this.height + 99).attr({
                stroke: "none"
            });
            bE.setTimeout(function() {
                b.remove()
            })
        }
    } else {
        aG.safari = function() {}
    }
    var aU = function() {
            this.returnValue = false
        },
        aV = function() {
            return this.originalEvent.preventDefault()
        },
        be = function() {
            this.cancelBubble = true
        },
        bf = function() {
            return this.originalEvent.stopPropagation()
        },
        g = (function() {
            if (J.addEventListener) {
                return function(i, R, e, b) {
                    var E = bi && bw[R] ? bw[R] : R;
                    var d = function(S) {
                            if (bi && bw[ae](R)) {
                                for (var bH = 0, bI = S.targetTouches && S.targetTouches.length; bH < bI; bH++) {
                                    if (S.targetTouches[bH].target == i) {
                                        var bJ = S;
                                        S = S.targetTouches[bH];
                                        S.originalEvent = bJ;
                                        S.preventDefault = aV;
                                        S.stopPropagation = bf;
                                        break
                                    }
                                }
                            }
                            return e.call(b, S)
                        };
                    i.addEventListener(E, d, false);
                    return function() {
                        i.removeEventListener(E, d, false);
                        return true
                    }
                }
            } else {
                if (J.attachEvent) {
                    return function(E, R, i, d) {
                        var e = function(S) {
                                S = S || bE.event;
                                S.preventDefault = S.preventDefault || aU;
                                S.stopPropagation = S.stopPropagation || be;
                                return i.call(d, S)
                            };
                        E.attachEvent("on" + R, e);
                        var b = function() {
                                E.detachEvent("on" + R, e);
                                return true
                            };
                        return b
                    }
                }
            }
        })(),
        K = [],
        L = function(d) {
            var bJ = d.clientX,
                bK = d.clientY,
                bH = J.documentElement.scrollTop || J.body.scrollTop,
                S = J.documentElement.scrollLeft || J.body.scrollLeft,
                b, R = K.length;
            while (R--) {
                b = K[R];
                if (bi) {
                    var E = d.touches.length,
                        bI;
                    while (E--) {
                        bI = d.touches[E];
                        if (bI.identifier == b.el._drag.id) {
                            bJ = bI.clientX;
                            bK = bI.clientY;
                            (d.originalEvent ? d.originalEvent : d).preventDefault();
                            break
                        }
                    }
                } else {
                    d.preventDefault()
                }
                bJ += S;
                bK += bH;
                b.move && b.move.call(b.move_scope || b.el, bJ - b.el._drag.x, bK - b.el._drag.y, bJ, bK, d)
            }
        },
        M = function(d) {
            aZ.unmousemove(L).unmouseup(M);
            var E = K.length,
                b;
            while (E--) {
                b = K[E];
                b.el._drag = {};
                b.end && b.end.call(b.end_scope || b.start_scope || b.move_scope || b.el, d)
            }
            K = []
        };
    for (var ai = U[ar]; ai--;) {
        (function(b) {
            aZ[b] = P[aW][b] = function(d, e) {
                if (aZ.is(d, "function")) {
                    this.events = this.events || [];
                    this.events.push({
                        name: b,
                        f: d,
                        unbind: g(this.shape || this.node || J, b, d, e || this)
                    })
                }
                return this
            };
            aZ["un" + b] = P[aW]["un" + b] = function(e) {
                var d = this.events,
                    i = d[ar];
                while (i--) {
                    if (d[i].name == b && d[i].f == e) {
                        d[i].unbind();
                        d.splice(i, 1);
                        !d.length && delete this.events;
                        return this
                    }
                }
                return this
            }
        })(U[ai])
    }
    T.hover = function(b, d, e, i) {
        return this.mouseover(b, e).mouseout(d, i || e)
    };
    T.unhover = function(b, d) {
        return this.unmouseover(b).unmouseout(d)
    };
    T.drag = function(i, E, e, d, R, b) {
        this._drag = {};
        this.mousedown(function(S) {
            (S.originalEvent || S).preventDefault();
            var bI = J.documentElement.scrollTop || J.body.scrollTop,
                bH = J.documentElement.scrollLeft || J.body.scrollLeft;
            this._drag.x = S.clientX + bH;
            this._drag.y = S.clientY + bI;
            this._drag.id = S.identifier;
            E && E.call(R || d || this, S.clientX + bH, S.clientY + bI, S);
            !K.length && aZ.mousemove(L).mouseup(M);
            K.push({
                el: this,
                move: i,
                end: e,
                move_scope: d,
                start_scope: R,
                end_scope: b
            })
        });
        return this
    };
    T.undrag = function(e, E, d) {
        var b = K.length;
        while (b--) {
            K[b].el == this && (K[b].move == e && K[b].end == d) && K.splice(b++, 1)
        }!K.length && aZ.unmousemove(L).unmouseup(M)
    };
    aG.circle = function(d, e, b) {
        return bk(this, d || 0, e || 0, b || 0)
    };
    aG.rect = function(i, E, e, b, d) {
        return bo(this, i || 0, E || 0, e || 0, b || 0, d || 0)
    };
    aG.ellipse = function(e, i, b, d) {
        return bl(this, e || 0, i || 0, b || 0, d || 0)
    };
    aG.path = function(b) {
        b && !aZ.is(b, bh) && !aZ.is(b[0], p) && (b += O);
        return bn(aZ.format[o](aZ, arguments), this)
    };
    aG.image = function(d, i, E, e, b) {
        return bm(this, d || "about:blank", i || 0, E || 0, e || 0, b || 0)
    };
    aG.text = function(d, e, b) {
        return bp(this, d || 0, e || 0, bg(b))
    };
    aG.set = function(b) {
        arguments[ar] > 1 && (b = Array[aW].splice.call(arguments, 0, arguments[ar]));
        return new a8(b)
    };
    aG.setSize = bb;
    aG.top = aG.bottom = null;
    aG.raphael = aZ;

    function bF() {
        return this.x + a6 + this.y
    }
    T.resetScale = function() {
        if (this.removed) {
            return this
        }
        this._.sx = 1;
        this._.sy = 1;
        this.attrs.scale = "1 1"
    };
    T.scale = function(ca, cb, e, E) {
        if (this.removed) {
            return this
        }
        if (ca == null && cb == null) {
            return {
                x: this._.sx,
                y: this._.sy,
                toString: bF
            }
        }
        cb = cb || ca;
        !+cb && (cb = ca);
        var bM, bN, R, S, b = this.attrs;
        if (ca != 0) {
            var d = this.getBBox(),
                b6 = d.x + d.width / 2,
                b7 = d.y + d.height / 2,
                bV = f(ca / this._.sx),
                bW = f(cb / this._.sy);
            e = (+e || e == 0) ? e : b6;
            E = (+E || E == 0) ? E : b7;
            var b4 = this._.sx > 0,
                b5 = this._.sy > 0,
                bI = ~~ (ca / f(ca)),
                bJ = ~~ (cb / f(cb)),
                bK = bV * bI,
                bL = bW * bJ,
                b8 = this.node.style,
                bX = e + f(b6 - e) * bK * (b6 > e == b4 ? 1 : -1),
                bY = E + f(b7 - E) * bL * (b7 > E == b5 ? 1 : -1),
                bO = (ca * bI > cb * bJ ? bW : bV);
            switch (this.type) {
            case "rect":
            case "image":
                var b0 = b.width * bV,
                    bZ = b.height * bW;
                this.attr({
                    height: bZ,
                    r: b.r * bO,
                    width: b0,
                    x: bX - b0 / 2,
                    y: bY - bZ / 2
                });
                break;
            case "circle":
            case "ellipse":
                this.attr({
                    rx: b.rx * bV,
                    ry: b.ry * bW,
                    r: b.r * bO,
                    cx: bX,
                    cy: bY
                });
                break;
            case "text":
                this.attr({
                    x: bX,
                    y: bY
                });
                break;
            case "path":
                var b3 = aP(b.path),
                    b9 = true,
                    bP = b4 ? bK : bV,
                    bQ = b5 ? bL : bW;
                for (var bR = 0, bS = b3[ar]; bR < bS; bR++) {
                    var b1 = b3[bR],
                        b2 = bA.call(b1[0]);
                    if (b2 == "M" && b9) {
                        continue
                    } else {
                        b9 = false
                    }
                    if (b2 == "A") {
                        b1[b3[bR][ar] - 2] *= bP;
                        b1[b3[bR][ar] - 1] *= bQ;
                        b1[1] *= bV;
                        b1[2] *= bW;
                        b1[5] = +(bI + bJ ? !! +b1[5] : !+b1[5])
                    } else {
                        if (b2 == "H") {
                            for (var bT = 1, bU = b1[ar]; bT < bU; bT++) {
                                b1[bT] *= bP
                            }
                        } else {
                            if (b2 == "V") {
                                for (bT = 1, bU = b1[ar]; bT < bU; bT++) {
                                    b1[bT] *= bQ
                                }
                            } else {
                                for (bT = 1, bU = b1[ar]; bT < bU; bT++) {
                                    b1[bT] *= (bT % 2) ? bP : bQ
                                }
                            }
                        }
                    }
                }
                var bH = aM(b3);
                bM = bX - bH.x - bH.width / 2;
                bN = bY - bH.y - bH.height / 2;
                b3[0][1] += bM;
                b3[0][2] += bN;
                this.attr({
                    path: b3
                });
                break
            }
            if (this.type in {
                text: 1,
                image: 1
            } && (bI != 1 || bJ != 1)) {
                if (this.transformations) {
                    this.transformations[2] = "scale(" [z](bI, ",", bJ, ")");
                    this.node[a9]("transform", this.transformations[an](a6));
                    bM = (bI == -1) ? -b.x - (b0 || 0) : b.x;
                    bN = (bJ == -1) ? -b.y - (bZ || 0) : b.y;
                    this.attr({
                        x: bM,
                        y: bN
                    });
                    b.fx = bI - 1;
                    b.fy = bJ - 1
                } else {
                    this.node.filterMatrix = az + ".Matrix(M11=" [z](bI, ", M12=0, M21=0, M22=", bJ, ", Dx=0, Dy=0, sizingmethod='auto expand', filtertype='bilinear')");
                    b8.filter = (this.node.filterMatrix || O) + (this.node.filterOpacity || O)
                }
            } else {
                if (this.transformations) {
                    this.transformations[2] = O;
                    this.node[a9]("transform", this.transformations[an](a6));
                    b.fx = 0;
                    b.fy = 0
                } else {
                    this.node.filterMatrix = O;
                    b8.filter = (this.node.filterMatrix || O) + (this.node.filterOpacity || O)
                }
            }
            b.scale = [ca, cb, e, E][an](a6);
            this._.sx = ca;
            this._.sy = cb
        }
        return this
    };
    T.clone = function() {
        if (this.removed) {
            return null
        }
        var b = this.attr();
        delete b.scale;
        delete b.translation;
        return this.paper[this.type]().attr(b)
    };
    var H = {},
        ab = function(bM, bN, b, d, e, E, bO, bP, bJ) {
            var bI = 0,
                bQ = 100,
                bK = [bM, bN, b, d, e, E, bO, bP].join(),
                R = H[bK],
                bL, S;
            !R && (H[bK] = R = {
                data: []
            });
            R.timer && clearTimeout(R.timer);
            R.timer = setTimeout(function() {
                delete H[bK]
            }, 2000);
            if (bJ != null) {
                var bR = ab(bM, bN, b, d, e, E, bO, bP);
                bQ = ~~bR * 10
            }
            for (var bH = 0; bH < bQ + 1; bH++) {
                if (R.data[bJ] > bH) {
                    S = R.data[bH * bQ]
                } else {
                    S = aZ.findDotsAtSegment(bM, bN, b, d, e, E, bO, bP, bH / bQ);
                    R.data[bH] = S
                }
                bH && (bI += aT(aT(bL.x - S.x, 2) + aT(bL.y - S.y, 2), 0.5));
                if (bJ != null && bI >= bJ) {
                    return S
                }
                bL = S
            }
            if (bJ == null) {
                return bI
            }
        },
        Z = function(b, d) {
            return function(bK, bH, bI) {
                bK = aI(bK);
                var bO, bP, bJ, R, bM = "",
                    bN = {},
                    bL, S = 0;
                for (var e = 0, E = bK.length; e < E; e++) {
                    bJ = bK[e];
                    if (bJ[0] == "M") {
                        bO = +bJ[1];
                        bP = +bJ[2]
                    } else {
                        R = ab(bO, bP, bJ[1], bJ[2], bJ[3], bJ[4], bJ[5], bJ[6]);
                        if (S + R > bH) {
                            if (d && !bN.start) {
                                bL = ab(bO, bP, bJ[1], bJ[2], bJ[3], bJ[4], bJ[5], bJ[6], bH - S);
                                bM += ["C", bL.start.x, bL.start.y, bL.m.x, bL.m.y, bL.x, bL.y];
                                if (bI) {
                                    return bM
                                }
                                bN.start = bM;
                                bM = ["M", bL.x, bL.y + "C", bL.n.x, bL.n.y, bL.end.x, bL.end.y, bJ[5], bJ[6]][an]();
                                S += R;
                                bO = +bJ[5];
                                bP = +bJ[6];
                                continue
                            }
                            if (!b && !d) {
                                bL = ab(bO, bP, bJ[1], bJ[2], bJ[3], bJ[4], bJ[5], bJ[6], bH - S);
                                return {
                                    x: bL.x,
                                    y: bL.y,
                                    alpha: bL.alpha
                                }
                            }
                        }
                        S += R;
                        bO = +bJ[5];
                        bP = +bJ[6]
                    }
                    bM += bJ
                }
                bN.end = bM;
                bL = b ? S : d ? bN : aZ.findDotsAtSegment(bO, bP, bJ[1], bJ[2], bJ[3], bJ[4], bJ[5], bJ[6], 1);
                bL.alpha && (bL = {
                    x: bL.x,
                    y: bL.y,
                    alpha: bL.alpha
                });
                return bL
            }
        };
    var ad = Z(1),
        aa = Z(),
        ac = Z(0, 1);
    T.getTotalLength = function() {
        if (this.type != "path") {
            return
        }
        if (this.node.getTotalLength) {
            return this.node.getTotalLength()
        }
        return ad(this.attrs.path)
    };
    T.getPointAtLength = function(b) {
        if (this.type != "path") {
            return
        }
        return aa(this.attrs.path, b)
    };
    T.getSubpath = function(d, e) {
        if (this.type != "path") {
            return
        }
        if (f(this.getTotalLength() - e) < "1e-6") {
            return ac(this.attrs.path, d).end
        }
        var b = ac(this.attrs.path, e, 1);
        return d ? ac(b, d).end : b
    };
    aZ.easing_formulas = {
        linear: function(b) {
            return b
        },
        "<": function(b) {
            return aT(b, 3)
        },
        ">": function(b) {
            return aT(b - 1, 3) + 1
        },
        "<>": function(b) {
            b = b * 2;
            if (b < 1) {
                return aT(b, 3) / 2
            }
            b -= 2;
            return (aT(b, 3) + 2) / 2
        },
        backIn: function(b) {
            var d = 1.70158;
            return b * b * ((d + 1) * b - d)
        },
        backOut: function(b) {
            b = b - 1;
            var d = 1.70158;
            return b * b * ((d + 1) * b + d) + 1
        },
        elastic: function(b) {
            if (b == 0 || b == 1) {
                return b
            }
            var d = 0.3,
                e = d / 4;
            return aT(2, -10 * b) * av.sin((b - e) * (2 * aR) / d) + 1
        },
        bounce: function(d) {
            var i = 7.5625,
                e = 2.75,
                b;
            if (d < (1 / e)) {
                b = i * d * d
            } else {
                if (d < (2 / e)) {
                    d -= (1.5 / e);
                    b = i * d * d + 0.75
                } else {
                    if (d < (2.5 / e)) {
                        d -= (2.25 / e);
                        b = i * d * d + 0.9375
                    } else {
                        d -= (2.625 / e);
                        b = i * d * d + 0.984375
                    }
                }
            }
            return b
        }
    };
    var l = [],
        k = function() {
            var bP = +new Date;
            for (var bM = 0; bM < l[ar]; bM++) {
                var E = l[bM];
                if (E.stop || E.el.removed) {
                    continue
                }
                var bV = bP - E.start,
                    bN = E.ms,
                    R = E.easing,
                    S = E.from,
                    d = E.diff,
                    bW = E.to,
                    bT = E.t,
                    bU = E.el,
                    bS = {},
                    bO;
                if (bV < bN) {
                    var bR = R(bV / bN);
                    for (var b in S) {
                        if (S[ae](b)) {
                            switch (q[b]) {
                            case "along":
                                bO = bR * bN * d[b];
                                bW.back && (bO = bW.len - bO);
                                var bQ = aa(bW[b], bO);
                                bU.translate(d.sx - d.x || 0, d.sy - d.y || 0);
                                d.x = bQ.x;
                                d.y = bQ.y;
                                bU.translate(bQ.x - d.sx, bQ.y - d.sy);
                                bW.rot && bU.rotate(d.r + bQ.alpha, bQ.x, bQ.y);
                                break;
                            case aA:
                                bO = +S[b] + bR * bN * d[b];
                                break;
                            case "colour":
                                bO = "rgb(" + [bB(a4(S[b].r + bR * bN * d[b].r)), bB(a4(S[b].g + bR * bN * d[b].g)), bB(a4(S[b].b + bR * bN * d[b].b))][an](",") + ")";
                                break;
                            case "path":
                                bO = [];
                                for (var bI = 0, bJ = S[b][ar]; bI < bJ; bI++) {
                                    bO[bI] = [S[b][bI][0]];
                                    for (var bK = 1, bL = S[b][bI][ar]; bK < bL; bK++) {
                                        bO[bI][bK] = +S[b][bI][bK] + bR * bN * d[b][bI][bK]
                                    }
                                    bO[bI] = bO[bI][an](a6)
                                }
                                bO = bO[an](a6);
                                break;
                            case "csv":
                                switch (b) {
                                case "translation":
                                    var bX = bR * bN * d[b][0] - bT.x,
                                        bY = bR * bN * d[b][1] - bT.y;
                                    bT.x += bX;
                                    bT.y += bY;
                                    bO = bX + a6 + bY;
                                    break;
                                case "rotation":
                                    bO = +S[b][0] + bR * bN * d[b][0];
                                    S[b][1] && (bO += "," + S[b][1] + "," + S[b][2]);
                                    break;
                                case "scale":
                                    bO = [+S[b][0] + bR * bN * d[b][0], +S[b][1] + bR * bN * d[b][1], (2 in bW[b] ? bW[b][2] : O), (3 in bW[b] ? bW[b][3] : O)][an](a6);
                                    break;
                                case "clip-rect":
                                    bO = [];
                                    bI = 4;
                                    while (bI--) {
                                        bO[bI] = +S[b][bI] + bR * bN * d[b][bI]
                                    }
                                    break
                                }
                                break;
                            default:
                                var bH = [].concat(S[b]);
                                bO = [];
                                bI = bU.paper.customAttributes[b].length;
                                while (bI--) {
                                    bO[bI] = +bH[bI] + bR * bN * d[b][bI]
                                }
                                break
                            }
                            bS[b] = bO
                        }
                    }
                    bU.attr(bS);
                    bU._run && bU._run.call(bU)
                } else {
                    if (bW.along) {
                        bQ = aa(bW.along, bW.len * !bW.back);
                        bU.translate(d.sx - (d.x || 0) + bQ.x - d.sx, d.sy - (d.y || 0) + bQ.y - d.sy);
                        bW.rot && bU.rotate(d.r + bQ.alpha, bQ.x, bQ.y)
                    }(bT.x || bT.y) && bU.translate(-bT.x, -bT.y);
                    bW.scale && (bW.scale += O);
                    bU.attr(bW);
                    l.splice(bM--, 1)
                }
            }
            aZ.svg && bU && bU.paper && bU.paper.safari();
            l[ar] && setTimeout(k)
        },
        ao = function(b, e, R, i, E) {
            var d = R - i;
            e.timeouts.push(setTimeout(function() {
                aZ.is(E, "function") && E.call(e);
                e.animate(b, d, b.easing)
            }, i))
        },
        bB = function(b) {
            return ax(ay(b, 255), 0)
        },
        bx = function(d, e) {
            if (d == null) {
                return {
                    x: this._.tx,
                    y: this._.ty,
                    toString: bF
                }
            }
            this._.tx += +d;
            this._.ty += +e;
            switch (this.type) {
            case "circle":
            case "ellipse":
                this.attr({
                    cx: +d + this.attrs.cx,
                    cy: +e + this.attrs.cy
                });
                break;
            case "rect":
            case "image":
            case "text":
                this.attr({
                    x: +d + this.attrs.x,
                    y: +e + this.attrs.y
                });
                break;
            case "path":
                var b = aP(this.attrs.path);
                b[0][1] += +d;
                b[0][2] += +e;
                this.attr({
                    path: b
                });
                break
            }
            return this
        };
    T.animateWith = function(e, bH, S, d, b) {
        for (var E = 0, R = l.length; E < R; E++) {
            if (l[E].el.id == e.id) {
                bH.start = l[E].start
            }
        }
        return this.animate(bH, S, d, b)
    };
    T.animateAlong = j();
    T.animateAlongBack = j(1);

    function j(b) {
        return function(E, e, R, d) {
            var i = {
                back: b
            };
            aZ.is(R, "function") ? (d = R) : (i.rot = R);
            E && E.constructor == P && (E = E.attrs.path);
            E && (i.along = E);
            return this.animate(i, e, d)
        }
    }
    function F(bO, bH, bI, bJ, bK, S) {
        var E = 3 * bH,
            e = 3 * (bJ - bH) - E,
            b = 1 - E - e,
            R = 3 * bI,
            i = 3 * (bK - bI) - R,
            d = 1 - R - i;

        function bL(bP) {
            return ((b * bP + e) * bP + E) * bP
        }
        function bM(bR, bP) {
            var bQ = bN(bR, bP);
            return ((d * bQ + i) * bQ + R) * bQ
        }
        function bN(bV, bQ) {
            var bS, bT, bU, bW, bP, bR;
            for (bU = bV, bR = 0; bR < 8; bR++) {
                bW = bL(bU) - bV;
                if (f(bW) < bQ) {
                    return bU
                }
                bP = (3 * b * bU + 2 * e) * bU + E;
                if (f(bP) < 1e-06) {
                    break
                }
                bU = bU - bW / bP
            }
            bS = 0;
            bT = 1;
            bU = bV;
            if (bU < bS) {
                return bS
            }
            if (bU > bT) {
                return bT
            }
            while (bS < bT) {
                bW = bL(bU);
                if (f(bW - bV) < bQ) {
                    return bU
                }
                if (bV > bW) {
                    bS = bU
                } else {
                    bT = bU
                }
                bU = (bT - bS) / 2 + bS
            }
            return bU
        }
        return bM(bO, 1 / (200 * S))
    }
    T.onAnimation = function(b) {
        this._run = b || 0;
        return this
    };
    T.animate = function(bV, bU, bI, R) {
        var bK = this;
        bK.timeouts = bK.timeouts || [];
        if (aZ.is(bI, "function") || !bI) {
            R = bI || null
        }
        if (bK.removed) {
            R && R.call(bK);
            return bK
        }
        var bL = {},
            bY = {},
            b = false,
            bH = {};
        for (var d in bV) {
            if (bV[ae](d)) {
                if (q[ae](d) || bK.paper.customAttributes[ae](d)) {
                    b = true;
                    bL[d] = bK.attr(d);
                    (bL[d] == null) && (bL[d] = r[d]);
                    bY[d] = bV[d];
                    switch (q[d]) {
                    case "along":
                        var bT = ad(bV[d]);
                        var bX = aa(bV[d], bT * !! bV.back);
                        var E = bK.getBBox();
                        bH[d] = bT / bU;
                        bH.tx = E.x;
                        bH.ty = E.y;
                        bH.sx = bX.x;
                        bH.sy = bX.y;
                        bY.rot = bV.rot;
                        bY.back = bV.back;
                        bY.len = bT;
                        bV.rot && (bH.r = br(bK.rotate()) || 0);
                        break;
                    case aA:
                        bH[d] = (bY[d] - bL[d]) / bU;
                        break;
                    case "colour":
                        bL[d] = aZ.getRGB(bL[d]);
                        var bZ = aZ.getRGB(bY[d]);
                        bH[d] = {
                            r: (bZ.r - bL[d].r) / bU,
                            g: (bZ.g - bL[d].g) / bU,
                            b: (bZ.b - bL[d].b) / bU
                        };
                        break;
                    case "path":
                        var bW = aI(bL[d], bY[d]);
                        bL[d] = bW[0];
                        var b0 = bW[1];
                        bH[d] = [];
                        for (var bN = 0, bO = bL[d][ar]; bN < bO; bN++) {
                            bH[d][bN] = [0];
                            for (var bP = 1, bQ = bL[d][bN][ar]; bP < bQ; bP++) {
                                bH[d][bN][bP] = (b0[bN][bP] - bL[d][bN][bP]) / bU
                            }
                        }
                        break;
                    case "csv":
                        var b1 = bg(bV[d])[bd](a7),
                            bM = bg(bL[d])[bd](a7);
                        switch (d) {
                        case "translation":
                            bL[d] = [0, 0];
                            bH[d] = [b1[0] / bU, b1[1] / bU];
                            break;
                        case "rotation":
                            bL[d] = (bM[1] == b1[1] && bM[2] == b1[2]) ? bM : [0, b1[1], b1[2]];
                            bH[d] = [(b1[0] - bL[d][0]) / bU, 0, 0];
                            break;
                        case "scale":
                            bV[d] = b1;
                            bL[d] = bg(bL[d])[bd](a7);
                            bH[d] = [(b1[0] - bL[d][0]) / bU, (b1[1] - bL[d][1]) / bU, 0, 0];
                            break;
                        case "clip-rect":
                            bL[d] = bg(bL[d])[bd](a7);
                            bH[d] = [];
                            bN = 4;
                            while (bN--) {
                                bH[d][bN] = (b1[bN] - bL[d][bN]) / bU
                            }
                            break
                        }
                        bY[d] = b1;
                        break;
                    default:
                        b1 = [].concat(bV[d]);
                        bM = [].concat(bL[d]);
                        bH[d] = [];
                        bN = bK.paper.customAttributes[d][ar];
                        while (bN--) {
                            bH[d][bN] = ((b1[bN] || 0) - (bM[bN] || 0)) / bU
                        }
                        break
                    }
                }
            }
        }
        if (!b) {
            var e = [],
                bS;
            for (var bR in bV) {
                if (bV[ae](bR) && m.test(bR)) {
                    d = {
                        value: bV[bR]
                    };
                    bR == "from" && (bR = 0);
                    bR == "to" && (bR = 100);
                    d.key = bu(bR, 10);
                    e.push(d)
                }
            }
            e.sort(bc);
            if (e[0].key) {
                e.unshift({
                    key: 0,
                    value: bK.attrs
                })
            }
            for (bN = 0, bO = e[ar]; bN < bO; bN++) {
                ao(e[bN].value, bK, bU / 100 * e[bN].key, bU / 100 * (e[bN - 1] && e[bN - 1].key || 0), e[bN - 1] && e[bN - 1].value.callback)
            }
            bS = e[e[ar] - 1].value.callback;
            if (bS) {
                bK.timeouts.push(setTimeout(function() {
                    bS.call(bK)
                }, bU))
            }
        } else {
            var bJ = aZ.easing_formulas[bI];
            if (!bJ) {
                bJ = bg(bI).match(t);
                if (bJ && bJ[ar] == 5) {
                    var S = bJ;
                    bJ = function(i) {
                        return F(i, +S[1], +S[2], +S[3], +S[4], bU)
                    }
                } else {
                    bJ = function(i) {
                        return i
                    }
                }
            }
            l.push({
                start: bV.start || +new Date,
                ms: bU,
                easing: bJ,
                from: bL,
                diff: bH,
                to: bY,
                el: bK,
                t: {
                    x: 0,
                    y: 0
                }
            });
            aZ.is(R, "function") && (bK._ac = setTimeout(function() {
                R.call(bK)
            }, bU));
            l[ar] == 1 && setTimeout(k)
        }
        return this
    };
    T.stop = function() {
        for (var b = 0; b < l.length; b++) {
            l[b].el.id == this.id && l.splice(b--, 1)
        }
        for (b = 0, ii = this.timeouts && this.timeouts.length; b < ii; b++) {
            clearTimeout(this.timeouts[b])
        }
        this.timeouts = [];
        clearTimeout(this._ac);
        delete this._ac;
        return this
    };
    T.translate = function(b, d) {
        return this.attr({
            translation: b + " " + d
        })
    };
    T[bv] = function() {
        return "Rapha\xebl\u2019s object"
    };
    aZ.ae = l;
    var a8 = function(e) {
            this.items = [];
            this[ar] = 0;
            this.type = "set";
            if (e) {
                for (var b = 0, d = e[ar]; b < d; b++) {
                    if (e[b] && (e[b].constructor == P || e[b].constructor == a8)) {
                        this[this.items[ar]] = this.items[this.items[ar]] = e[b];
                        this[ar]++
                    }
                }
            }
        };
    a8[aW][aX] = function() {
        var e, E;
        for (var b = 0, d = arguments[ar]; b < d; b++) {
            e = arguments[b];
            if (e && (e.constructor == P || e.constructor == a8)) {
                E = this.items[ar];
                this[E] = this.items[E] = e;
                this[ar]++
            }
        }
        return this
    };
    a8[aW].pop = function() {
        delete this[this[ar]--];
        return this.items.pop()
    };
    for (var aw in T) {
        if (T[ae](aw)) {
            a8[aW][aw] = (function(b) {
                return function() {
                    for (var d = 0, e = this.items[ar]; d < e; d++) {
                        this.items[d][b][o](this.items[d], arguments)
                    }
                    return this
                }
            })(aw)
        }
    }
    a8[aW].attr = function(R, S) {
        if (R && aZ.is(R, p) && aZ.is(R[0], "object")) {
            for (var e = 0, E = R[ar]; e < E; e++) {
                this.items[e].attr(R[e])
            }
        } else {
            for (var b = 0, d = this.items[ar]; b < d; b++) {
                this.items[b].attr(R, S)
            }
        }
        return this
    };
    a8[aW].animate = function(bI, bH, e, b) {
        (aZ.is(e, "function") || !e) && (b = e || null);
        var S = this.items[ar],
            E = S,
            R, bJ = this,
            d;
        b && (d = function() {
            !--S && b.call(bJ)
        });
        e = aZ.is(e, bh) ? e : d;
        R = this.items[--E].animate(bI, bH, e, d);
        while (E--) {
            this.items[E] && !this.items[E].removed && this.items[E].animateWith(R, bI, bH, e, d)
        }
        return this
    };
    a8[aW].insertAfter = function(b) {
        var d = this.items[ar];
        while (d--) {
            this.items[d].insertAfter(b)
        }
        return this
    };
    a8[aW].getBBox = function() {
        var R = [],
            S = [],
            E = [],
            d = [];
        for (var e = this.items[ar]; e--;) {
            var b = this.items[e].getBBox();
            R[aX](b.x);
            S[aX](b.y);
            E[aX](b.x + b.width);
            d[aX](b.y + b.height)
        }
        R = ay[o](0, R);
        S = ay[o](0, S);
        return {
            x: R,
            y: S,
            width: ax[o](0, E) - R,
            height: ax[o](0, d) - S
        }
    };
    a8[aW].clone = function(e) {
        e = new a8;
        for (var b = 0, d = this.items[ar]; b < d; b++) {
            e[aX](this.items[b].clone())
        }
        return e
    };
    aZ.registerFont = function(d) {
        if (!d.face) {
            return d
        }
        this.fonts = this.fonts || {};
        var e = {
            w: d.w,
            face: {},
            glyphs: {}
        },
            b = d.face["font-family"];
        for (var S in d.face) {
            if (d.face[ae](S)) {
                e.face[S] = d.face[S]
            }
        }
        if (this.fonts[b]) {
            this.fonts[b][aX](e)
        } else {
            this.fonts[b] = [e]
        }
        if (!d.svg) {
            e.face["units-per-em"] = bu(d.face["units-per-em"], 10);
            for (var i in d.glyphs) {
                if (d.glyphs[ae](i)) {
                    var R = d.glyphs[i];
                    e.glyphs[i] = {
                        w: R.w,
                        k: {},
                        d: R.d && "M" + R.d[a5](/[mlcxtrv]/g, function(bH) {
                            return {
                                l: "L",
                                c: "C",
                                x: "z",
                                t: "m",
                                r: "l",
                                v: "c"
                            }[bH] || "M"
                        }) + "z"
                    };
                    if (R.k) {
                        for (var E in R.k) {
                            if (R[ae](E)) {
                                e.glyphs[i].k[E] = R.k[E]
                            }
                        }
                    }
                }
            }
        }
        return d
    };
    aG.getFont = function(b, bK, bI, bH) {
        bH = bH || "normal";
        bI = bI || "normal";
        bK = +bK || {
            normal: 400,
            bold: 700,
            lighter: 300,
            bolder: 800
        }[bK] || 400;
        if (!aZ.fonts) {
            return
        }
        var d = aZ.fonts[b];
        if (!d) {
            var S = new RegExp("(^|\\s)" + b[a5](/[^\w\d\s+!~.:_-]/g, O) + "(\\s|$)", "i");
            for (var e in aZ.fonts) {
                if (aZ.fonts[ae](e)) {
                    if (S.test(e)) {
                        d = aZ.fonts[e];
                        break
                    }
                }
            }
        }
        var bJ;
        if (d) {
            for (var E = 0, R = d[ar]; E < R; E++) {
                bJ = d[E];
                if (bJ.face["font-weight"] == bK && (bJ.face["font-style"] == bI || !bJ.face["font-style"]) && bJ.face["font-stretch"] == bH) {
                    break
                }
            }
        }
        return bJ
    };
    aG.print = function(bS, bT, bQ, e, bP, bJ, bH) {
        bJ = bJ || "middle";
        bH = ax(ay(bH || 0, 1), -1);
        var bK = this.set(),
            bI = bg(bQ)[bd](O),
            bO = 0,
            bL = O,
            bN;
        aZ.is(e, bQ) && (e = this.getFont(e));
        if (e) {
            bN = (bP || 16) / e.face["units-per-em"];
            var b = e.face.bbox.split(a7),
                bR = +b[0],
                E = +b[1] + (bJ == "baseline" ? b[3] - b[1] + (+e.face.descent) : (b[3] - b[1]) / 2);
            for (var R = 0, S = bI[ar]; R < S; R++) {
                var bM = R && e.glyphs[bI[R - 1]] || {},
                    d = e.glyphs[bI[R]];
                bO += R ? (bM.w || e.w) + (bM.k && bM.k[bI[R]] || 0) + (e.w * bH) : 0;
                d && d.d && bK[aX](this.path(d.d).attr({
                    fill: "#000",
                    stroke: "none",
                    translation: [bO, 0]
                }))
            }
            bK.scale(bN, bN, bR, E).translate(bS - bR, bT - E)
        }
        return bK
    };
    aZ.format = function(e, d) {
        var b = aZ.is(d, p) ? [0][z](d) : arguments;
        e && aZ.is(e, bh) && b[ar] - 1 && (e = e[a5](X, function(R, E) {
            return b[++E] == null ? O : b[E]
        }));
        return e || O
    };
    aZ.ninja = function() {
        aC.was ? (bE.Raphael = aC.is) : delete Raphael;
        return aZ
    };
    aZ.el = T;
    aZ.st = a8[aW];
    aC.was ? (bE.Raphael = aZ) : (Raphael = aZ)
})();
(function() {
    function MissingRequiresError(message) {
        this.message = message;
    }
    MissingRequiresError.prototype = new Error();

    function BadDataForView(obj, data) {
        this.obj = obj;
        this.data = data;
        this.message = "Bad Data For View";
    }
    BadDataForView.prototype = new Error();

    function BadStrictPatternMatch(obj, data, pattern) {
        this.obj = obj;
        this.data = data;
        this.pattern = pattern;
        this.message = "Bad Data for Strict Pattern Match";
    }
    BadStrictPatternMatch.prototype = new Error();

    function ComponentNotDefined(key, obj) {
        this.key = key;
        this.obj = obj;
        this.message = key + " is not defined";
    }
    ComponentNotDefined.prototype = new Error();

    function FailedToCreateComponent(key, config) {
        this.key = key;
        this.config = config;
    }
    FailedToCreateComponent.prototype = new Error();

    /// reduce helpers
    _.mixin({
        group_by: function(collection, prop) {
            return _.reduce(collection, function(group, item) {
                var value;
                if (_.isString(prop)) {
                    value = _.indexOf(prop, ".") === -1 ? item[prop] : _.access(item, prop);
                    (group[value] = group[value] || []).push(item);
                } else if (_.isFunction(prop)) {
                    value = prop(item);
                    (group[value] = group[value] || []).push(item);
                }
                return group;
            }, {});
        },
        join: function(collection, prop, other_collection, other_prop) {
            other_prop = _.isUndefined(other_prop) ? prop : other_prop;
            var other_map = _.group_by(other_collection, other_prop);
            var is_function = _.isFunction(prop);
            return _.reduce(collection, function(results, item) {
                var value;
                value = is_function ? prop(item) : item[prop];
                _.each(other_map[value], function(other_item) {
                    results.push(_.extend({}, item, other_item));
                }, []);
                return results;
            }, []);
        },
        missing: function(array, values) {
            return _.filter(array, function(value) {
                return !_.include(values, value);
            });
        },
        access: function(item, prop) {
            var path = prop.split("."),
                h = _.head(path),
                t = _.tail(path);
            if (path.length > 1) {
                if (h === "_") {
                    return _(item).chain().compact().map(function(o) {
                        return _.access(o, t.join("."));
                    }).value();
                } else {
                    return _.access(item[h], t.join("."));
                }
            }
            return item[h];
        },
        JSONclone: function(obj) {
            return JSON.parse(JSON.stringify(obj));
        },
        zipExtend: function(obj, obj2) {
            return _(obj).chain().zip(obj2).map(function(i) {
                return _.extend(i[0], i[1]);
            }).value();
        },
        ensure: function(obj, prop, value, memo) {
            var path = prop.split("."),
                h = _.head(path),
                t = _.tail(path);

            if (_.isUndefined(obj[h]) || _.isNull(obj[h])) {
                obj[h] = {};
            }

            if (_.isUndefined(memo)) {
                memo = obj;
            }

            if (path.length > 1) {
                return _.ensure(obj[h], t.join("."), value, memo);
            }
            if (_.isEqual({}, obj[h]) && !_.isUndefined(value)) {
                obj[h] = value;
            }
            return memo;
        },
        patternGate: function(pattern) {
            return generate_pattern_gate(pattern);
        },
        nullablePatternGate: function(pattern) {
            return function(data) {
                return _.isNullOrMatch(pattern, data);
            }
        },
        isMatch: function(pattern, data) {
            return generate_pattern_gate(pattern)(data)
        },
        isNullOrMatch: function(pattern, data) {
            if (_.isNull(data)) {
                return true;
            } else {
                return generate_pattern_gate(pattern)(data)
            }
        }
    });

    _.mixin({
        deepClone: function(obj) {
            if (typeof obj !== 'object' || obj === null) {
                return obj;
            }

            var c = obj instanceof Array ? [] : {};

            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    c[i] = _.deepClone(obj[i]);
                }
            }

            return c;
        },
        diff: function(obj1, obj2) {
            var cycle_check = [];

            function diff_worker(obj1, obj2, c) {
                c = c || _.identity;
                if (_.isUndefined(obj1)) obj1 = {};
                if (_.isUndefined(obj2)) obj2 = {};

                var val1, val2, mod = {},
                    add = {},
                    del = {},
                    ret;
                _.each(obj2, function(val2, key) {
                    try {
                        /// Treats all access problemsa as undefined, PM
                        val1 = obj1[key];
                    } catch (e) {
                        val1 = undefined;
                    }
                    if (_.isUndefined(val1)) {
                        add[key] = val2;
                    } else if (typeof val1 !== typeof val2) {
                        mod[key] = val2;
                    } else if (!_.isEqual(val1, val2)) {
                        if (_.isObject(val2)) {
                            if (_.include(cycle_check, val2)) {
                                return false;
                            }
                            ret = diff_worker(val1, val2); // consider thunking, PM
                            if (!_.isEmpty(ret.mod)) {
                                mod[key] = jQuery.extend(true, {}, ret.mod);
                            }
                            if (!_.isEmpty(ret.add)) {
                                add[key] = jQuery.extend(true, {}, ret.add);
                            }
                            if (!_.isEmpty(ret.del)) {
                                del[key] = jQuery.extend(true, {}, ret.del);
                            }
                            cycle_check.push(val2);
                        } else {
                            mod[key] = val2;
                        }
                    }
                })

                _.each(obj1, function(val1, key) {
                    if (_.isUndefined(obj2[key])) del[key] = true;
                })

                return {
                    mod: mod,
                    add: add,
                    del: del
                };
            }

            return (_.trampoline(diff_worker))(obj1, obj2);
        },
        thunk: function(fn) {
            var args = _.toArray(arguments);
            if (args.length > 1) {
                args = args.slice(1);
            }
            return function() {
                return fn.apply(null, args);
            }
        },
        trampoline: function(fn) {
            function trampoline_worker(bouncer) {
                while (_.isFunction(bouncer)) {
                    bouncer = bouncer();
                }
                return bouncer;
            }

            return function() {
                return trampoline_worker(fn.apply(null, _.toArray(arguments)));
            }
        },
        nullableModel: function(type) {
            var nullable = function() {
                    _.extend(this, new type());
                }
            nullable.prototype.isStillNull = function() {
                var test = new type();
                return _.reduce(this, function(m, v, k) {
                    if (!m) return m;
                    var tv = _.isEqual({}, test[k]) ? null : _.isEqual(v, {}) ? {} : test[k];
                    return _.isEqual(v, tv);
                }, true);
            }
            nullable.prototype.toJSON = function() {
                if (this.isStillNull()) {
                    return null;
                } else {
                    return _.extend(new type(), this);
                }
            }
            return nullable;
        },
        makeLightBox: function(value, key) {
            return {
                css: "overlay black",
                id: "wrapper_" + key,
                content: [{
                    css: "mask"
                }, {
                    css: "overlay_content",
                    content: [{
                        type: "a",
                        css: "button_close",
                        attr: {
                            href: "#"
                        },
                        content: "\u00D7"
                    }, {
                        id: key,
                        content: {
                            css: "pointless_design cf",
                            content: value
                        }
                    }]
                }]
            }
        },
        /// This function builds an array of functions to be called,
        /// "when" a gating function finally returns true, PM
        callCollectFactory: function(arr, when, context) {
            var func;
            /// Only create a gate when "when" is actually a function
            if (!_.isFunction(when)) {
                throw new Error("When must be a functor");
            }
            /// Depend on Hoisting, and declare our forward chaining 
            /// append and call when "when" is true function
            func = _.bind(function(add, test_context, args) {
                /// At call time we have a local return value variable
                var vals;
                /// If the function to add to the call list is a function add it
                if (_.isFunction(add)) {
                    // if we have a global context for these calls we bind the function to that context and capture the local arguments
                    arr.push(_.isUndefined(context) ? add : _.bind(add, context, args));
                }
                /// Test if we are ready to execute the call list
                if (when(test_context)) {
                    /// Our gate is open so,
                    // Build a map of the return value of all functions in the list, and return them
                    vals = _.map(arr, function(f) {
                        return _.isFunction(f) ? f() : f;
                    });
                }
                /// Our gate is closed so,
                // return the original function 
                // (carrying the parent function context so arr, when, context, and func are persisted)
                return func;
            })
            return func;
        },
        arrayStringToTree: function(obj, delim) {
            var reducer = function(m, v, k, list) {
                    var next = v.split(delim);
                    _.each(next, function(evt, idx, inner_list) {
                        m[evt] = {};
                        var child = inner_list[idx + 1];
                        if (!_.isUndefined(child)) {
                            m[evt][child] = _.reduce(_.filter(list, function(item) {
                                return _.head(item.split(delim)) === child;
                            }), reducer, {});
                            delete m[evt][child][child];
                        }
                    })

                    return m;
                };

            try {
                var results = _.reduce(obj, reducer, {})
            } catch (ex) {
                throw new Error("There is likely a Cycle in your obj");
            }

            return results;
        },
        getElsforCard: function(key) {
            var cards;
            if (key.indexOf(".") !== -1) {
                cards = [FirstLook.cards.get(key)];
            } else {
                cards = FirstLook.cards.get_all(key);
            }
            var els = _(cards).chain().map(function(a) {
                var vals = _(a.views).chain().values().compact().value();
                return _(vals).chain().values().map(_.values).flatten().compact().filter(function(b) {
                    return _.isFunction(b.get_el)
                }).value()
            }).flatten().compact().invoke("get_el").reduce(function(m, v) {
                return m.add(v);
            }, $()).value();
            return els;
        }
    })

    /// validation helpers
    _.mixin({
        isVin: function(text) {
            if (text === null || text === undefined) {
                return false;
            }
            if (text.length != 17) {
                return false;
            }
            text = text.toUpperCase();
            if (/^[ABCDEFGHJKLMNPRSTUVWXYZ0123456789]{17}$/.test(text) === false) {
                return false;
            }
            var factors = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2];
            var char_map = {
                "A": 1,
                "B": 2,
                "C": 3,
                "D": 4,
                "E": 5,
                "F": 6,
                "G": 7,
                "H": 8,
                "J": 1,
                "K": 2,
                "L": 3,
                "M": 4,
                "N": 5,
                "P": 7,
                "R": 9,
                "S": 2,
                "T": 3,
                "U": 4,
                "V": 5,
                "W": 6,
                "X": 7,
                "Y": 8,
                "Z": 9
            };

            var total = 0;
            for (var i = 0; i < 17; i++) {
                var v = text.charCodeAt(i),
                    f = factors[i];
                var text_array = text.split("");
                v = char_map[text_array[i]] || parseInt(text_array[i], 10);

                total += (v * f);
            }
            var remainder = total % 11;
            if (remainder == 10) {
                remainder = 'X';
            } else {
                remainder = remainder.toString();
            }
            return text_array[8] == remainder;
        },
        isPlatform: _.memoize(function(pattern) {
            return !!pattern.test(navigator.appVersion);
        })
    });

    /// string helpers
    _.mixin({
        concat: function(arr) {
            if (_.isArray(arr)) {
                return arr.join("");
            }
            return _.toArray(arr).join("")
        },
        trim: function(text) {
            var str = text.replace(/^\s\s*/, ''),
                ws = /\s/,
                i = str.length;
            while (ws.test(str.charAt(--i)));
            return str.slice(0, i + 1);
        },
        padString: function(arr) {
            return _.concat([" ", arr, " "])
        },
        toPx: function(val) {
            return _.concat(val, "px");
        },
        toPct: function(val) {
            return _.concat(val, "pct");
        },
        comma: function(value) {
            value = _.isNumber(value) ? value.toString() : "";
            var regex = /(\d+)(\d{3})/;
            while (regex.test(value)) {
                value = value.replace(regex, '$1,$2');
            }
            return value;
        },
        toInt: function(value) {
            if (_.isDate(value)) {
                value = value.toJSON();
            }
            return parseInt(value.replace(/[^\d]*/g, ""), 10) || 0;
        },
        money: function(value) {
            return _.moneyOrSymbol(value, "-0-");
        },
        moneyOrSymbol: function(value, symbol) {
            if (_.isUndefined(value) || value === 0) {
                return symbol;
            }
            return "$" + _.comma(Math.round(value));
        },
        moneyAdjusted: function(value) {
            var indicator = value === 0 ? "" : value > 0 ? "+" : "-";
            return indicator + _.money(Math.abs(value));
        },
        moneyToK: function(value) {
            return "$" + _.roundToK(value);
        },
        roundToK: function(value) {
            if (value > 999) {
                return Math.round(value / 1e3) + "k";
            } else {
                return value;
            }
        },
        shortVin: function(value) {
            return value[0] + "_" + value[9] + "_" + value.slice(11, 17);
        },
        asmxDateParse: function(date_string) {
            var date;
            if (_.isDate(date_string)) {
                date = date_string;
            } else {
                date = new Date(parseInt(date_string.replace(/\/Date\((\d+)\)\//, "$1"), 10));
            }
            return date;
        },
        asmxDate: function(date_string) {
            return _.asmxDateParse(date_string).toDateString();
        },
        shortDate: function(d) {
            function padIntLeft(val, width) {
                var s = val.toString();
                while (s.length < width) {
                    s = "0" + s;
                }
                return s;
            }
            return padIntLeft(d.getMonth() + 1, 2) + "/" + padIntLeft(d.getDate(), 2) + "/" + d.getFullYear();
        }
    });

    /// Array Helpers
    _.mixin({
        usStates: function() {
            return [{
                name: "Alabama",
                value: "AL"
            }, {
                name: "Alaska",
                value: "AK"
            }, {
                name: "Arizona",
                value: "AZ"
            }, {
                name: "Arkansas",
                value: "AR"
            }, {
                name: "California",
                value: "CA"
            }, {
                name: "Colorado",
                value: "CO"
            }, {
                name: "Connecticut",
                value: "CT"
            }, {
                name: "Delaware",
                value: "DE"
            }, {
                name: "Florida",
                value: "FL"
            }, {
                name: "Georgia",
                value: "GA"
            }, {
                name: "Hawaii",
                value: "HI"
            }, {
                name: "Idaho",
                value: "ID"
            }, {
                name: "Illinois",
                value: "IL"
            }, {
                name: "Indiana",
                value: "IN"
            }, {
                name: "Iowa",
                value: "IA"
            }, {
                name: "Kansas",
                value: "KS"
            }, {
                name: "Kentucky",
                value: "KY"
            }, {
                name: "Louisiana",
                value: "LA"
            }, {
                name: "Maine",
                value: "ME"
            }, {
                name: "Maryland",
                value: "MD"
            }, {
                name: "Massachusetts",
                value: "MA"
            }, {
                name: "Michigan",
                value: "MI"
            }, {
                name: "Minnesota",
                value: "MN"
            }, {
                name: "Mississippi",
                value: "MS"
            }, {
                name: "Missouri",
                value: "MO"
            }, {
                name: "Montana",
                value: "MT"
            }, {
                name: "Nebraska",
                value: "NE"
            }, {
                name: "Nevada",
                value: "NV"
            }, {
                name: "New Hampshire",
                value: "NH"
            }, {
                name: "New Jersey",
                value: "NJ"
            }, {
                name: "New Mexico",
                value: "NM"
            }, {
                name: "New York",
                value: "NY"
            }, {
                name: "North Carolina",
                value: "NC"
            }, {
                name: "North Dakota",
                value: "ND"
            }, {
                name: "Ohio",
                value: "OH"
            }, {
                name: "Oklahoma",
                value: "OK"
            }, {
                name: "Oregon",
                value: "OR"
            }, {
                name: "Pennsylvania",
                value: "PA"
            }, {
                name: "Rhode Island",
                value: "RI"
            }, {
                name: "South Carolina",
                value: "SC"
            }, {
                name: "South Dakota",
                value: "SD"
            }, {
                name: "Tennessee",
                value: "TN"
            }, {
                name: "Texas",
                value: "TX"
            }, {
                name: "Utah",
                value: "UT"
            }, {
                name: "Vermont",
                value: "VT"
            }, {
                name: "Virginia",
                value: "VA"
            }, {
                name: "Washington",
                value: "WA"
            }, {
                name: "West Virginia",
                value: "WV"
            }, {
                name: "Wisconsin",
                value: "WI"
            }, {
                name: "Wyoming",
                value: "WY"
            }];
        },
        require: function(args, props) {
            try {
                /// Should be updated to work with nonaurgument objects, PM
                var test_obj = args;
                if (_.isUndefined(args)) throw new MissingRequiresError("Requres Arugments");
                if (_.isArguments(args)) test_obj = args[0];
                if ( !! props && typeof props.length == "number") {
                    _.each(props, function(prop) {
                        if (_.isUndefined(test_obj[prop])) {
                            throw new MissingRequiresError(_.concat(["Requires: ", prop]));
                        }
                    });
                }
            } catch (e) {
                if (e instanceof MissingRequiresError) {
                    FirstLook.errors.add(e, "Requires" + _.uniqueId());
                }
            }
            return true;
        }
    });

    _.mixin({
        inputToObj: function(dom) {
            var values = _.reduce($(":input", dom), function(m, i) {
                m[$(i).attr('name')] = $(i).val();
                return m;
            }, {});

            return values;
        },
        fragmentToString: function(frag) {
            return _.reduce(frag.childNodes, function(m, v) {
                return _.concat([m, v.outerHTML || $(v).text()])
            }, "");
        }
    });

    /// Event Helpers
    _.mixin({
        preventDefault: function(evt) {
            evt.preventDefault();
        },
        logEvent: function(evt) {
            var type, ns;
            if (evt) {
                if (evt instanceof jQuery.Event) {
                    type = evt.type;
                    ns = evt.namespace;
                }
            }
            if (console && console.log) console.log(type, ns, arguments);
        },
        log: function() {
            if (console && console.log) console.log(arguments);
        },
        trace: function() {}
    });

    _.mixin({
        rgbToHsl: function(r, g, b) {
            r /= 255;
            g /= 255;
            b /= 255;
            var max = Math.max(r, g, b),
                min = Math.min(r, g, b);
            var h, s, l = (max + min) / 2;

            if (max == min) {
                h = s = 0; // achromatic
            } else {
                var d = max - min;
                s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
                }
                h /= 6;
            }

            return [h, s, l];
        },
        hslToRgb: function(h, s, l) {
            var r, g, b;

            function hue2rgb(p, q, t) {
                if (t < 0) t += 1;
                if (t > 1) t -= 1;
                if (t < 1 / 6) return p + (q - p) * 6 * t;
                if (t < 1 / 2) return q;
                if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                return p;
            }

            if (s === 0) {
                r = g = b = l; // achromatic
            } else {
                var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                var p = 2 * l - q;
                r = hue2rgb(p, q, h + 1 / 3);
                g = hue2rgb(p, q, h);
                b = hue2rgb(p, q, h - 1 / 3);
            }

            return [r * 255, g * 255, b * 255];
        },
        hexToRgb: function(hex) {
            var r = parseInt(hex.slice(1, 3), 16),
                g = parseInt(hex.slice(3, 5), 16),
                b = parseInt(hex.slice(5, 7), 16);
            return [r, g, b];
        },
        lightnessForRgb: function(hex) {
            var hsl = _.rgbToHsl.apply({}, _.hexToRgb(hex));
            return hsl[2];
        }
    });


    /// Pattern Helper... should probably be protected and not in this space, PM


    function where(value, key) {
        var state, return_value = true;
        var isObject = !(_.isString(value) || _.isNumber(value) || _.isBoolean(value) || _.isDate(value) || _.isNaN(value) || _.isFunction(value) || _.isArray(value));
        if (_.isString(key)) {
            state = this[key];
            if (value === _) {
                return_value = !_.isUndefined(state);
            } else if (_.isFunction(value)) {
                return_value = value(state);
            } else if (_.isUndefined(state)) {
                return_value = false;
            } else if (_.isArray(value) && value.length > 0) {
                if (_.isArray(state) && state.length > 0) {
                    return_value = _.all(value[0], where, state[0]);
                } else {
                    // Array with pattern item always matches an empty array
                    return_value = true;
                }
            } else {
                return_value = _.isEqual(value, state);
            }
        }
        if (isObject) return_value = _.all(value, where, state);
        return return_value;
    }

    /// Template Code, PM
    var DOM = {};

    function getDOM() {
        DOM = {
            main: $("#main"),
            header: $("#page_header"),
            sections: $("#section_holder")
        };
    }

    //// Component Templates
    var Templates = (function() {
        function func(args, body) {
            return _.concat(["function(", args, "){", body, "}"]);
        }

        function inner(args, body) {
            return func(args, escape_outer(body));
        }

        function escape_outer(body) {
            return _.concat(["%>", body, "<%"]);
        }

        function make_attr(attr_name, value) {
            return value ? _.concat([" ", attr_name, "='", value, "'"]) : "";
        }

        function make_idx_attr(attr_name, value, idxer) {
            return value ? _.concat([" ", attr_name, "='<%=", value, "[", idxer, "]%>'"]) : "";
        }

        function make_each(items, item, each) {
            return _.concat(["<% _.each(", items, ",", inner(item, each), "); %>"]);
        }

        var tmpl = {};

        tmpl.generateGroupedSelectList = function(config, as_string) {
            _.require(arguments, ["data_accessor", "text_accessor", "item_accessor"]);

            var list_class = make_attr("class", config.class_name),
                list_items, groups, list_tmp, text_tmp = _([config.text_accessor]).chain().flatten().reduce(function(memo, acc) {
                    return _.concat([memo, "<span class='", acc.replace(".", "-"), "'><%= item.", acc, " %></span> "]);
                }, "").value(),
                data_tmp = _([config.data_accessor]).chain().flatten().reduce(function(memo, acc) {
                    return _.concat([memo, " data-", acc.replace(".", "-"), "='<%= item.", acc, " %>'"]);
                }, "").value(),

                items = make_each("group", "item", _.concat(["<li", data_tmp, ">", text_tmp, "</li>"]));
            groups = make_each(config.item_accessor, "group,v,k", _.concat(["<h3 class='select_list_group_name'><%= v %></h3><ul", list_class, ">", items, "</ul>"]));

            if (as_string) {
                return list_tmp;
            } else {
                return _.template(groups);
            }
        };
        tmpl.generateSelectList = function(config, as_string) {
            _.require(arguments, ["text_accessor", "item_accessor"]);

            var list_class = make_attr("class", config.class_name),
                list_items, list_tmp, class_name_tmp = make_idx_attr("class", config.item_class_name_accessor ? "item" : "", config.item_class_name_accessor),
                text_tmp = _([config.text_accessor]).chain().flatten().reduce(function(memo, acc) {
                    return _.concat([memo, "<span class='", acc, "'><%= item.", acc, " %></span> "]);
                }, "").value(),
                data_tmp = _.isUndefined(config.data_accessor) ? "" : _([config.data_accessor]).chain().flatten().reduce(function(memo, acc) {
                    return _.concat([memo, " data-", acc.replace(".", "-"), "='<%= item.", acc, " %>'"]);
                }, "").value();

            list_items = make_each(config.item_accessor, "item, idx", _.concat(["<li", data_tmp, class_name_tmp, ">", text_tmp, "</li>"]));
            list_tmp = _.concat(["<ul", list_class, ">", list_items, "</ul>"]);

            if (as_string) {
                return list_tmp;
            } else {
                return _.template(list_tmp);
            }
        };
        tmpl.generateButton = function(config, as_string) {
            var id = make_attr("id", config.id),
                className = make_attr("class", config.className),
                button_tmp = _.concat(["<button", id, className, "><%= text %></button>"]);
            if (as_string) {
                return button_tmp;
            } else {
                return _.template(button_tmp);
            }
        };
        tmpl.generateInput = function(config, as_string) {
            var input_tmpl = _.concat(["<p class='field'><label><%= input.", //
            config.label_accessor, " %></label>", //
            "<% if (_.isArray(input.options)) { %>", //
            "<select name='<%= input.", // 
            config.name_accessor, //
            " %>'>", //
            make_each("input.options", "option", _.concat([ //
            "<option value='<%= option.value %>'", //
            "<% if (option.value == input.", //
            config.value_accessor, //
            ") { %> selected='true' <% } %>", // 
            "><%= option.name %></option>"])), //
            "</select>", //
            "<% } else { %>", //
            "<input name='<%= input.", //
            config.name_accessor, //
            " %>' type='<%= input.", //
            (config.type_accessor), //
            " || 'text' %>' value='<%= input.", //
            config.value_accessor, //
            " %>' />", //
            "<% } %>", //
            "</p>"]);
            if (as_string) {
                return input_tmpl;
            } else {
                return _.template(input_tmpl);
            }
        };
        tmpl.generateFieldset = function(config, as_string) {
            _.require(arguments, ["input_accessor", "label_accessor", "value_accessor", "type_accessor"]);

            var inputs_tmpl = make_each(config.input_accessor, "input", tmpl.generateInput({
                name_accessor: config.name_accessor,
                value_accessor: config.value_accessor,
                type_accessor: config.type_accessor,
                label_accessor: config.label_accessor
            }, true)),
                fieldset_id = make_attr('id', config.id),
                fieldset_class = make_attr('class', config.class_name),
                button_tmpl = _.isUndefined(config.button_accessor) ? "" : make_each(config.button_accessor, "button, idx", "<button class='fieldset_button' id='b_" + fieldset_id + "_<%= idx %>'><%= button %></button>");
            var legend_tmpl = _.isUndefined(config.legend_accessor) ? "" : _.concat(["<legend><%= ", config.legend_accessor, " %></legend>"]);
            var fieldset_tmpl = _.concat(["<fieldset", fieldset_id, fieldset_class, ">", legend_tmpl, inputs_tmpl, "</fieldset>", button_tmpl]);

            if (as_string) {
                return fieldset_tmpl;
            } else {
                return _.template(fieldset_tmpl);
            }
        };
        tmpl.generateTableRow = function(accessor, config, as_string, tag) {
            var col_span_attr = make_idx_attr("colspan", config.col_span_accessor, "idx"),
                col_class_name = make_idx_attr("class", config.col_class_name_accessor, "idx"),
                col_data = make_idx_attr("data-col", config.col_data_accessor, "idx"),
                row_class_name = make_idx_attr("class", config.row_class_name_accessor, "r_idx"),
                row_data = make_idx_attr("data-row", config.row_data_accessor, "r_idx"),
                row_tmpl;

            if (_.isUndefined(tag) && _.isString(config.body_header_col_accessor)) {
                tag = _.concat(["<% if (idx === ", config.body_header_col_accessor, ") {%>th<% } else { %>td<% } %>"]);
            }

            row_tmpl = make_each(accessor, "row, r_idx", _.concat(["<tr", row_class_name, row_data, ">", //
            make_each("row", "item, idx", _.concat(["<", tag || "td", col_span_attr, col_class_name, col_data, "><%= item %></", tag || "td", ">"])), "</tr>"]));

            if (as_string) {
                return row_tmpl;
            } else {
                return _.template(row_tmpl);
            }
        };
        tmpl.generateTable = function(config, as_string) {
            _.require(arguments, ["head_accessor", "body_accessor"]);

            var class_attr = _.isUndefined(config.classname_accessor) ? "" : make_attr("class", _.concat(["<%= ", config.classname_accessor, " %>"])),
                id_attr = make_attr("id", config.id),
                caption_tmpl = _.isUndefined(config.caption_accessor) ? "" : _.concat(["<% if (_.isString(", config.caption_accessor, ")) { %><caption><%=", config.caption_accessor, " %></caption> <% } %>"]),

                table_head_tmpl = _.concat(["<thead>", tmpl.generateTableRow(config.head_accessor, config, true, "th"), "</thead>"]),
                table_body_tmpl = _.concat(["<tbody>", tmpl.generateTableRow(config.body_accessor, config, true), "</tbody>"]),
                table_tmpl = _.concat(["<table", id_attr, class_attr, ">", caption_tmpl, table_head_tmpl, table_body_tmpl, "</table>"]);

            if (as_string) {
                return table_tmpl;
            } else {
                return _.template(table_tmpl);
            }
        };
        return {
            clearPage: function() {
                DOM.sections.html("");
            },
            appendToPage: function(html) {
                DOM.sections.append(html);
            },
            appendToEl: function(html, el) {
                $(el).append(html);
            },
            animateIn: (function() {
                return function() {
                    DOM.sections.fadeIn(250);
                    return 250;
                };
            })(),
            animateOut: (function() {
                return function() {
                    DOM.sections.fadeOut(100);
                    return 250;
                };
            })(),
            generateGroupedSelectList: tmpl.generateGroupedSelectList,
            generateSelectList: tmpl.generateSelectList,
            generateFieldset: tmpl.generateFieldset,
            generateTable: tmpl.generateTable
        };
    })();

//    var FirstLook = {};
//    FirstLook.templates = Templates;
//    if (_.isUndefined(window.FirstLook)) window.FirstLook = FirstLook;

//    var persistence = new Lawnchair({
//        name: "Session",
//        record: "state",
//        adapter: _.find(Lawnchair.adapters, function(a) {
//            return a.adapter === "memory"
//        })
//    }, function() {});
//    FirstLook.Session = {};
//    FirstLook.Session = {
//        lastHash: "",
//        matches: "([A-Za-z0-9_\\-%]+)",
//        patterns: {},
//        search_patterns: {},
//        inTransaction: false,
//        transactionHash: "",
//        add_url_pattern: function(pattern, key) {
//            var regexPattrn = pattern.replace(":" + key, FirstLook.Session.matches);
//            FirstLook.Session.patterns[key] = new RegExp(regexPattrn);
//        },
//        wire: function(wires) {
//            _.each(wires, FirstLook.Session.add_url_pattern);
//        },
//        get_state: function(key) {
//            var matches, hash = window.location.hash,
//                search = window.location.search,
//                val = null;

//            matches = _.reduce(FirstLook.Session.patterns, function(return_value, value, key) {
//                var inner_matches = [],
//                    regex;
//                if (value.test(this)) inner_matches = value.exec(this);
//                return_value[key] = inner_matches[1] ? decodeURIComponent(inner_matches[1]) : null;
//                if (return_value[key] === null) {
//                    regex = new RegExp("[?|&]" + key + "=([^&]*)");
//                    if (search.match(regex)) return_value[key] = search.match(regex)[1];
//                }
//                return return_value;
//            }, {}, hash);
//            if (!_.isUndefined(FirstLook.Session.patterns[key]) && FirstLook.Session.patterns[key].test(hash)) {

//                matches = FirstLook.Session.patterns[key].exec(hash);
//                return decodeURIComponent(matches[1]) || null;
//            }

//            val = _.isNull(matches[key]) ? null : decodeURIComponent(matches[key]);
//            if (_.isNull(val)) {
//                persistence.get(key, function(v) {
//                    if (v && v.value !== null) val = v.value;
//                })
//            }

//            return val;
//        },
//        set_state: function(key, value, passed) {
//            var hash = passed || window.location.hash,
//                new_hash = hash,
//                pattern = FirstLook.Session.patterns[key],
//                escaped_value = encodeURIComponent(value);
//            if (!_.isRegExp(pattern)) {
//                throw "No Wireing Defined";
//            } else if (pattern.test(hash)) {
//                if (value === null) {
//                    persistence.destroy(key);
//                    new_hash = hash.replace(pattern, "");
//                } else {
//                    persistence.save({
//                        key: key,
//                        value: value
//                    });
//                    new_hash = hash.replace(pattern, pattern.source.replace(FirstLook.Session.matches, escaped_value));
//                }
//            } else if (value !== null) {
//                persistence.remove(key);
//                persistence.save({
//                    key: key,
//                    value: value
//                });
//                new_hash += pattern.source.replace(FirstLook.Session.matches, escaped_value + "/");
//            }

//            if (!FirstLook.Session.inTransaction) {
//                //window.location.hash = new_hash;
//                $(window).trigger("hashchange");
//            } else {
//                FirstLook.Session.transactionHash += new_hash;
//            }
//            return new_hash;
//        },
//        set_states: function(obj) {
//            FirstLook.Session.inTransaction = true;
//            var hash = window.location.hash;
//            if (hash[hash.length - 1] !== "/") {
//                hash += "/";
//            }
//            var new_hash = _.reduce(obj, function(memo, val, key) {
//                var h = FirstLook.Session.set_state(key, val, memo);
//                return h;
//            }, hash);
//            //window.location.hash = new_hash.replace(/\/\//g, '');
//            $(window).trigger("hashchange");
//            FirstLook.Session.transactionHash = "";
//            FirstLook.Session.inTransaction = false;
//        },
//        onChange: function(evt) {
//            persistence.remove('hash');
//            persistence.save({
//                key: 'hash',
//                value: window.location.hash
//            });
//            FirstLook.Session.lastHash = window.location.hash;
//        },
//        checkForChange: function() {
//            if (FirstLook.Session.lastHash != window.location.hash) {
//                $(window).trigger("hashchange");
//            }
//        },
//        pop: function() {
//            var a = _.compact(window.location.hash.split("/"));
//            if (a.length > 2) {
//                FirstLook.Session.set_state(a[a.length - 2], "");
//            }
//        },
//        clear: function() {
//            //window.location.hash = "";
//            $(window).trigger("hashchange");
//        }
//    };
//    $(window).bind("hashchange", FirstLook.Session.onChange);

//    function generateTemplate(card, key) {
//        _.require(arguments, ["view", "pattern"]);

//        var template = {};

//        if (!_.isUndefined(card.default_view)) {
//            template.default_view = card.default_view;
//        }

//        if (!_.isUndefined(card.pattern)) {
//            template.can_render = (function() {
//                var gate = generate_pattern_gate(card.pattern);
//                return function(test_data) {
//                    return gate(_.isUndefined(test_data) ? FirstLook.Session.get_state() : test_data);
//                };
//            })();
//        }

//        if (card.view) {
//            template.views = {};
//            _.reduce(card.view, generateView, template.views, key);
//        }

//        template.build = function(data, opts) {
//            var views = this.views,
//                is_array = _.isArray(views),
//                temp_views, view_to_render;

//            if (is_array) {
//                temp_views = views;
//            } else {
//                temp_views = _.sortBy(views, function(v) {
//                    return _.isNumber(v.order) ? v.order : Infinity;
//                });
//            }

//            view_to_render = _.detect(temp_views, function(v) {
//                if (_.isUndefined(v.pattern)) return false;
//                var return_value = generate_pattern_gate(v.pattern)(data);
//                if (!return_value) {
//                    $(FirstLook.cards).trigger("viewNotRendered", [v, data]);
//                }
//                return return_value;
//            });

//            if (view_to_render) {
//                _.invoke(views, "clean", true);
//                view_to_render.build(data, opts);
//            }

//        };

//        return template;
//    }

//    function generate_pattern_gate(pattern, error_on_false) {
//        return function(data) {
//            var return_value = _.all(pattern, where, data);
//            if (!return_value && _.isBoolean(error_on_false) && error_on_false) {
//                throw new BadStrictPatternMatch(this, data, pattern);
//            }
//            return return_value;
//        };
//    }

//    function generateView(value, field, key, list) {
//        var card_key = this;
//        var newView = {};
//        var components = [];
//        var known_keys = ["pattern", "behaviors", "order"];
//        var view_fields = _.reject(field, function(value, key) {
//            return _.contains(known_keys, key);
//        });

//        newView.__card_key__ = card_key;
//        newView.__type__ = _.concat([key, "_", _.uniqueId()]);

//        newView.pattern = field.pattern;
//        newView.order = field.order;

//        _.each(field, function(inner_value, inner_key, inner_list) {
//            if (_.contains(known_keys, inner_key)) return;
//            if (_.isUndefined(newView[inner_key])) newView[inner_key] = {};
//            try {
//                newView[inner_key] = generateComponent(inner_value);
//                // Considering Exposing pattern here and "bequeathing" it to the view, PM
//            } catch (e) {
//                if (e instanceof ComponentNotDefined) {
//                    FirstLook.errors.add(new ComponentNotDefined(inner_key, newView));
//                }
//                if (e instanceof FailedToCreateComponent) {
//                    FirstLook.errors.add(e);
//                }
//            }
//            components.push(newView[inner_key]);
//        });

//        newView.build = function(data, opts) {
//            var self = this;
//            if (_.isUndefined(opts)) opts = {};

//            if (!opts.avoid_render) FirstLook.cards.render(this);
//            _.each(components, function(item) {
//                item.build(data, self.__type__, self.__card_key__);
//            });
//        };
//        newView.clean = function(args) {
//            _.invoke(components, "clean", args);
//        };

//        value[key] = newView;

//        return value;
//    }

//    function generateBaseComponent(base) {
//        return _.extend(base, {
//            rendered: false,
//            build: function(data, view_type, card_key) {
//                var pass = false;
//                try {
//                    pass = this.data_gate(data);
//                } catch (e) {
//                    if (e instanceof BadStrictPatternMatch) {
//                        FirstLook.errors.add(e, "BadMatch" + _.uniqueId());
//                        throw new BadDataForView(this, data);
//                    }
//                }

//                var filtered_data = data;
//                if (_.isFunction(this.filter)) {
//                    filtered_data = this.filter(jQuery.extend(true, {}, data));
//                }
//                this.data = filtered_data;
//                var merged_data = _.extend(filtered_data, _.deepClone(this.static_data));
//                $(this).trigger("prepare_data", [merged_data]);
//                $(this).trigger("before_clean");
//                var el = this.get_el(),
//                    wrapper, newRender = !el.size();
//                if (newRender) {
//                    wrapper = $(this.__wrapper__);
//                    if (document.getElementById(wrapper.attr("id")) === null) {
//                        el = this.$el = $(this.__wrapper__);
//                        if (_.isUndefined(el.attr("id"))) {
//                            el.attr("id", this.__type__);
//                        }
//                    } else {
//                        el = this.$el = $("#" + wrapper.attr("id"));
//                    }
//                    if (this.class_name) el.addClass(this.class_name);
//                }
//                $(this).trigger("before_dom_render");
//                if (_.isArray(merged_data) && merged_data.length === 0) {
//                    $(this).trigger("dom_rendered");
//                    return;
//                }
//                var new_dom = this.template(merged_data);
//                if (_.isString(new_dom) || _.isElement(new_dom)) {
//                    new_dom = $(new_dom).addClass(this.__type__);
//                } else if (new_dom.nodeType == 11 || new_dom instanceof DocumentFragment) { // IE || Standard
//                    $(new_dom.childNodes).addClass(this.__type__);
//                }

//                el.append(new_dom);

//                if (newRender) {
//                    var view = $("#" + view_type);
//                    var section = FirstLook.cards.get_card_section(card_key);
//                    if (view.size() > 0) {
//                        Templates.appendToEl(el, view);
//                    } else if (section !== null) {
//                        Templates.appendToEl(el, section);
//                    } else {
//                        Templates.appendToPage(el);
//                    }
//                }
//                $(this).trigger("dom_rendered");
//            },
//            clean: function(clean_all, by_class) {
//                $(this).trigger("before_clean");
//                var el = this.get_el();
//                var classed_el = el.find("." + this.__type__);
//                if (clean_all && !el.is("header, article, footer")) { // these are special blocks for tha page, PM
//                    el.remove();
//                } else if (by_class && classed_el.length > 0) {
//                    classed_el.remove();
//                } else {
//                    //el.children().remove("." + this.__type__);
//                    el.children().remove();
//                }
//                this.$el = null;
//            },
//            get_el: function() {
//                var return_val = this.$el = this.$el || $(document.getElementById($(this.__wrapper__).attr("id"))) || $("#" + this.__type__);
//                return return_val;
//            },
//            build_complete: function() {
//                $(this).trigger("build_complete");
//            },
//            behaviors: {}
//        });
//    }

//    function bequeath(accessor) {
//        return function(memo, value, key) {
//            function handle_array_bequeath(_memo, _value, _key) { // Hack to fix extending arrays...PM
//                if (_.isArray(_value) && _.isArray(memo[_key])) {
//                    _.extend(memo[_key][0], _value[0]);
//                    delete _memo[_key];
//                }
//                return _memo;
//            }

//            var accessed_value = value[accessor],
//                avoid_extend = (_.isEqual(value, _) || _.isUndefined(accessed_value));
//            if (!avoid_extend) {
//                _.reduce(accessed_value, handle_array_bequeath, accessed_value);
//                _.extend(memo, accessed_value);
//            }
//            return memo;
//        };
//    }

//    function generateComponent(config) {
//        var return_value = {},
//            type_behaviors = [],
//            config_clone = _.clone(config);

//        if (_.isUndefined(config) || _.isNull(config)) {
//            throw new ComponentNotDefined();
//        }

//        _.require(config_clone, ["type", "pattern"]);
//        // TODO: should I test for existance fo behaviors?
//        /// Build Full pattern from behaviors and get the gate
//        _.reduce(config_clone.behaviors, bequeath("pattern"), config_clone.pattern);
//        return_value.data_gate = generate_pattern_gate(config_clone.pattern, true);

//        generateBaseComponent(return_value);

//        /// Generate the unique id for the componentType
//        return_value.__type__ = _.concat([config_clone.type, "_", _.uniqueId()]);
//        return_value.__wrapper__ = config_clone.wrapper || "<div />";

//        if (config_clone.static_data) {
//            return_value.static_data = config_clone.static_data;
//        }
//        if (config_clone.filter) {
//            return_value.filter = config_clone.filter;
//        }
//        if (config_clone.class_name) {
//            return_value.class_name = config_clone.class_name;
//        }

//        try {
//            switch (config.type) {
//            case "List":
//                _.require(config_clone, components.List.requires);

//                _.extend(return_value, components.List.extend_to);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.List.generateTemplate(config_clone);
//                break;
//            case "GroupedList":
//                _.require(config_clone, components.GroupedList.requires);

//                _.extend(return_value, components.GroupedList.extend_to);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.GroupedList.generateTemplate(config_clone);
//                break;
//            case "Fieldset":
//                _.require(config_clone, components.Fieldset.requires);

//                _.extend(return_value, components.Fieldset.extend_to);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.Fieldset.generateTemplate(config_clone);
//                break;
//            case "Table":
//                _.require(config_clone, components.Table.requires);
//                _.extend(return_value, components.Table.extend_to);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.Table.generateTemplate(config_clone);
//                break;
//            case "Tabs":
//                _.require(config_clone, components.Table.requires);

//                return_value.components = config_clone.components;

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.Tabs.generateTemplate();
//                break;
//            case "Static":
//                _.require(config_clone, ["template"]);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = config_clone.template;
//                break;
//            case "Placeholder":
//                _.require(config_clone, ["component"]);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.Placeholder.generateTemplate(config_clone);
//                break;
//            case "Cardholder":
//                _.require(config_clone, ["card"]);

//                apply_behaviors(config_clone, return_value);

//                return_value.template = components.Cardholder.generateTemplate(config_clone);
//                break;
//            default:
//                break;
//            }
//        } catch (e) {
//            throw new FailedToCreateComponent(return_value.__type__, config_clone);
//        }
//        return return_value;
//    }

//    function apply_behaviors(config, obj) {
//        /// Ensure required attributes for behaviors
//        _.each(config.behaviors, function(value, key) {
//            var behave = behaviors[key];
//            if (_.isUndefined(behave)) return;
//            _.require(value, behave.requires || []);

//            obj.behaviors[key] = value;

//            if (!_.isUndefined(behave.bequeath)) {
//                _.each(behave.bequeath, function(_value, _key) {
//                    config[_value] = config.behaviors[key][_value];
//                });
//            }

//            if (_.isArray(behave.listens_to)) apply_behavior_events(behave, obj);

//            _.each(value.binds || {}, function(value, key) {
//                /// If the value is a string we call the event with the object as the scope,
//                if (_.isString(value)) {
//                    $(obj).bind(key, function(evt) {
//                        $(obj).trigger(value, _.toArray(arguments).slice(1));
//                    });
//                } else if (_.isFunction(value)) {
//                    $(obj).bind(key, value);
//                } else if (value.scope && value.type) {
//                    // otherwise we raise the event with the passed in scope and type attributes, PM
//                    $(obj).bind(key, function(evt) {
//                        $(value.scope).trigger(value.type, _.toArray(arguments).slice(1));
//                    });
//                }
//            });
//        });
//    }

//    function apply_behavior_events(behavior, obj) {
//        _.each(behavior.listens_to, function(value) {
//            var handler = behavior["on_" + value];
//            if (_.isFunction(handler)) $(obj).bind(value, handler);
//        });
//    }

//    var behaviors = {
//        // Behavior Helper Methods
//        get_instance_var: function(scope, key) {
//            return scope[key];
//        },
//        get_instance_type_var: function(scope, type, key) {
//            return scope.behaviors[type][key];
//        },
//        get_instance_data: function(scope, type, key) {
//            var accessor = behaviors.get_instance_type_var(scope, type, key) || "";

//            function get_access(scope, accessor, tail) {
//                if (tail && tail.length > 0) {
//                    return get_access(scope[accessor] || {}, _.first(tail), _.rest(tail));
//                }
//                return scope[accessor];
//            }
//            return get_access(scope, "data", accessor.split("."));
//        },
//        get_type_var: function(type, key) {
//            return behaviors[type][key];
//        },
//        get_type: function(type) {
//            return behaviors[type];
//        },
//        dom_listenable: {
//            requires: [],
//            listens_to: ["prepare_data", "before_dom_render", "dom_rendered", "before_clean", "build_complete"],
//            on_prepare_data: function(evt, data) {
//                var handler = behaviors.get_instance_type_var(this, "dom_listenable", "on_prepare_data");
//                if (_.isFunction(handler)) {
//                    handler.apply(this, [evt, data]);
//                }
//            },
//            on_before_dom_render: function(evt, data) {
//                var handler = behaviors.get_instance_type_var(this, "dom_listenable", "on_before_dom_render");
//                if (_.isFunction(handler)) {
//                    handler.apply(this, [evt, data]);
//                }
//            },
//            on_dom_rendered: function(evt, data) {
//                var handler = behaviors.get_instance_type_var(this, "dom_listenable", "on_dom_rendered");
//                if (_.isFunction(handler)) {
//                    handler.apply(this, [evt, data]);
//                }
//            },
//            on_before_clean: function(evt, data) {
//                var handler = behaviors.get_instance_type_var(this, "dom_listenable", "on_before_clean");
//                if (_.isFunction(handler)) {
//                    handler.apply(this, [evt, data]);
//                }
//            },
//            on_build_complete: function(evt, data) {
//                var handler = behaviors.get_instance_type_var(this, "dom_listenable", "on_build_complete");
//                if (_.isFunction(handler)) {
//                    handler.apply(this, [evt, data]);
//                }
//            }
//        },
//        // Behavior Types
//        raises_changed: {
//            requires: [],
//            listens_to: ["change", "dom_rendered", "before_clean"],
//            raises: [],
//            on_change: function(evt, data) {
//                var bound = behaviors.get_instance_type_var(this, "raises_changed", "binds");
//                // To prevent infinate loops now
//                $(this).trigger("changing", data);
//            },
//            on_dom_rendered: function(evt) {
//                var throttled_event = _.bind(function(evt) {
//                    // Standard Char + NumPad, or Backspace
//                    if ((evt.which > 47 && evt.which < 91) || (evt.which > 95 && evt.which < 112 || evt.which === 8)) {
//                        $(this).trigger("change", [evt.target, $(evt.target).val()]);
//                    }
//                }, this)
//                $(this.get_el()).delegate(":input", "keyup", throttled_event);
//            },
//            on_before_clean: function(evt) {
//                $(this.get_el()).undelegate("keyup");
//            }
//        },
//        listens_change: {
//            requires: [],
//            listens_to: ["changing"],
//            raises: ["changed"],
//            on_changing: function(evt, data) {
//                $(this).trigger("changed", data);
//            }
//        },
//        selectable: {
//            requires: ["data_accessor"],
//            bequeath: ["data_accessor"],
//            raises: ["item_selected"],
//            listens_to: ["dom_rendered", "before_clean"],
//            on_dom_rendered: function() {
//                var raising = $.proxy(behaviors.get_type_var("selectable", "on_item_selected"), this),
//                    el = this.get_el(),
//                    selector = behaviors.get_instance_type_var(this, "selectable", "item_selector"); // refactor so it can also handler clicks with out a item selector (i.e. the whole comonent);
//                $(el).on("click", selector, raising);
//            },
//            on_before_clean: function() {
//                var selector = behaviors.get_instance_type_var(this, "selectable", "item_selector"),
//                    el = this.get_el();

//                $(el).off("click", selector); // reconfigure to die
//            },
//            on_item_selected: function(evt) {
//                var data_accessor = behaviors.get_instance_type_var(this, "selectable", "data_accessor"),
//                    data, el = this.get_el(),
//                    selector = behaviors.get_instance_type_var(this, "selectable", "item_selector"),
//                    target = $(evt.target).is(selector) ? evt.target : $(evt.target).parents(selector);

//                if (_.isArray(data_accessor)) {
//                    data = _([data_accessor]).chain().flatten().reduce(function(memo, acc) {
//                        var a = acc.replace(".", "-").toLowerCase();
//                        memo[acc] = this.attr('data-' + a);
//                        return memo;
//                    }, {}, $(target)).value();
//                } else {
//                    data = $(target).attr('data-' + data_accessor);
//                }
//                $(target).addClass("selected");
//                el.find(selector + '.selected').not(target).removeClass('selected');
//                $(this).trigger("item_selected", [data]);
//            }
//        },
//        multi_selectable: {
//            requires: ["multi_data_accessor", "multi_item_accessor"],
//            bequeath: ["multi_data_accessor", "multi_item_accessor"],
//            raises: ["item_selected"],
//            listens_to: ["dom_rendered", "before_clean", "selection_set"],
//            on_dom_rendered: function() {
//                var raising = $.proxy(behaviors.get_type_var("multi_selectable", "on_item_selected"), this),
//                    el = this.get_el(),
//                    selector = behaviors.get_instance_var(this, "item_selector"),
//                    items = behaviors.get_instance_data(this, "multi_selectable", "multi_item_accessor"); // refactor so it can also handler clicks with out a item selector (i.e. the whole comonent);
//                $(selector, el).bind("click", raising);
//                $(this).trigger("selection_set", [items]);
//            },
//            on_before_clean: function() {
//                var selector = behaviors.get_instance_var(this, "item_selector"),
//                    el = this.get_el();

//                $(selector, el).unbind("click"); // reconfigure to die
//            },
//            on_item_selected: function(evt) {
//                var data_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "multi_data_accessor"),
//                    map_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "map_data_accessor"),
//                    data, el = this.get_el(),
//                    selector = behaviors.get_instance_var(this, "item_selector"),
//                    target = $(evt.target).is(selector) ? evt.target : $(evt.target).parents(selector);

//                if (_.isArray(data_accessor)) {
//                    data = _.reduce(data_accessor, function(memo, attr) {
//                        memo[attr] = this.attr('data-' + attr);
//                        return memo;
//                    }, {}, $(target));
//                } else {
//                    data = $(target).attr('data-' + (map_accessor || data_accessor));
//                }
//                $(target).toggleClass("selected");

//                $(this).trigger("item_selected", [data, !! $(target).hasClass("selected")]);
//            },
//            on_selection_set: function(evt, data) {
//                var data_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "multi_data_accessor"),
//                    map_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "map_data_accessor"),
//                    el = this.get_el(),
//                    selector = behaviors.get_instance_var(this, "item_selector"),
//                    items = el.find(selector),
//                    selected = _(data).chain().filter(function(i) {
//                        return i.Selected;
//                    }).pluck(data_accessor).invoke("toString").value(),
//                    disabled = _(data).chain().filter(function(i) {
//                        return !i.Enabled;
//                    }).pluck(data_accessor).value();

//                _.each(items, function(item) {
//                    var item_data = $(item).attr("data-" + (map_accessor || data_accessor)),
//                        is_selected = _(selected).contains(item_data),
//                        is_disabled = _(disabled).contains(item_data);

//                    $(item)[is_selected ? "addClass" : "removeClass"]("selected");
//                    $(item)[is_disabled ? "addClass" : "removeClass"]("disabled");
//                    if (is_disabled) {
//                        $(item).unbind("click");
//                    }
//                });
//            }
//        },
//        pageable: {
//            requires: ["page_size_accessor", "total_items_accessor", "start_index_accessor"],
//            raises: ["next", "prev", "start_index_change"],
//            listens_to: ["dom_rendered", "before_clean", "should_call_next", "should_call_prev"],
//            has_next: function() {
//                var t = behaviors.get_instance_data(this, "pageable", "total_items_accessor"),
//                    s = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
//                    m = behaviors.get_instance_data(this, "pageable", "page_size_accessor");
//                return t > s + m;
//            },
//            has_prev: function() {
//                var s = behaviors.get_instance_data(this, "pageable", "start_index_accessor");
//                return s > 0;
//            },
//            page_to: function(evt) {
//                if ($(evt.target).is("button")) {
//                    var s = behaviors.get_instance_data(this, "pageable", "page_size_accessor"),
//                        v = $(evt.target).val();
//                    $(this).trigger("start_index_change", [(v - 1) * s]);
//                }
//            },
//            next: function() {
//                var s = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
//                    m = behaviors.get_instance_data(this, "pageable", "page_size_accessor"),
//                    new_start_index = s + m;
//                $(this).trigger("start_index_change", [new_start_index]);
//            },
//            prev: function() {
//                self = this;
//                var s = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
//                    m = behaviors.get_instance_data(this, "pageable", "page_size_accessor"),
//                    new_start_index = s - m;
//                $(this).trigger("start_index_change", [new_start_index]);
//            },
//            on_dom_rendered: function() {
//                var has_next = behaviors.get_type_var("pageable", "has_next").apply(this),
//                    has_prev = behaviors.get_type_var("pageable", "has_prev").apply(this),
//                    append_more = behaviors.get_type_var("pageable", "append_more_link");

//                if (has_next || has_prev) {
//                    append_more.apply(this);
//                }
//            },
//            on_before_clean: function() {
//                var el = this.get_el();
//                $("li.paging", el).unbind("click").remove();
//            },
//            append_more_link: function() {
//                var el = this.get_el(),
//                    next_action = $.proxy(behaviors.get_type_var("pageable", "page_to"), this),
//                    max = behaviors.get_instance_data(this, "pageable", "total_items_accessor"),
//                    start = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
//                    per_page = behaviors.get_instance_data(this, "pageable", "page_size_accessor"),
//                    num_pages = Math.ceil(max / per_page) + 1,
//                    current_page = Math.floor(start / per_page) + 1,
//                    frag = {
//                        type: "ul",
//                        css: "paging_list",
//                        content: []
//                    },
//                    full_frag, starting_page = 1,
//                    pagging_controls = num_pages,
//                    has_prev_ten = false,
//                    has_next_ten = false;

//                if (num_pages > 10) {
//                    pagging_controls = current_page >= 4 ? current_page + 4 : 10;
//                    starting_page = current_page > 5 ? (current_page - 4) : starting_page;
//                    if (starting_page <= 0) starting_page = 0;

//                    if (current_page > 10) {
//                        has_prev_ten = true;
//                    }
//                    if ((num_pages - current_page) > 10) {
//                        has_next_ten = true;
//                    }
//                }

//                if (has_prev_ten) {
//                    frag.content.push({
//                        type: "li",
//                        content: {
//                            type: "button",
//                            attrs: {
//                                type: "button",
//                                value: starting_page - 10
//                            },
//                            css: "button light",
//                            content: "Previous 10"
//                        }
//                    })
//                }

//                for (var i = starting_page; i < pagging_controls; i += 1) {
//                    frag.content.push({
//                        type: "li",
//                        css: i === current_page ? "current" : "",
//                        content: {
//                            type: "button",
//                            attrs: {
//                                type: "button",
//                                value: i
//                            },
//                            css: "button light",
//                            content: i
//                        }
//                    })
//                }

//                if (has_next_ten) {
//                    frag.content.push({
//                        type: "li",
//                        content: {
//                            type: "button",
//                            attrs: {
//                                type: "button",
//                                value: starting_page + 10
//                            },
//                            css: "button light",
//                            content: "Next 10"
//                        }
//                    })
//                }

//                full_frag = fastFrag.create(frag);

//                $(el).append(full_frag);
//                $("ul.paging_list", el).bind("click", next_action);
//            },
//            on_should_call_next: function() {
//                var has_next = behaviors.get_type_var("pageable", "has_next").apply(this),
//                    next_action = $.proxy(behaviors.get_type_var("pageable", "next"), this);

//                if (has_next) next_action();
//            },
//            on_should_call_prev: function() {
//                var has_prev = behaviors.get_type_var("pageable", "has_prev").apply(this),
//                    prev_action = $.proxy(behaviors.get_type_var("pageable", "prev"), this);

//                if (has_prev) prev_action();
//            }
//        },
//        sortable: {
//            requires: ["sort_columns_accessor", "dom_data_accessor", "dom_sort_selector"],
//            raises: ["sort_change"],
//            listens_to: ["dom_rendered", "before_clean"],
//            on_dom_rendered: function() {
//                var el = this.get_el(),
//                    sort_action = $.proxy(behaviors.get_type_var("sortable", "sort_change"), this),
//                    selector = behaviors.get_instance_type_var(this, "sortable", "dom_sort_selector");

//                behaviors.get_type_var("sortable", "apply_indicator").apply(this);

//                $(selector, el).bind("click", sort_action);
//            },
//            on_before_clean: function() {
//                var el = this.get_el(),
//                    sort_action = $.proxy(behaviors.get_type_var("sortable", "sort_change"), this),
//                    selector = behaviors.get_instance_type_var(this, "sortable", "dom_sort_selector");

//                $(selector).unbind("click", sort_action);
//            },
//            apply_indicator: function() {
//                var el = this.get_el(),
//                    selector = behaviors.get_instance_type_var(this, "sortable", "dom_sort_selector"),
//                    acc = behaviors.get_instance_type_var(this, "sortable", "dom_data_accessor"),
//                    sort_keys = behaviors.get_instance_data(this, "sortable", "sort_columns_accessor"),
//                    first_key = sort_keys[0],
//                    each = function(idx, item) {
//                        var e = $(item);
//                        if (e.data(acc) === first_key.ColumnName) {
//                            e.addClass(first_key.Ascending ? "asc" : "desc");
//                        } else {
//                            e.removeClass("asc desc");
//                        }
//                    };

//                $(selector, el).each(each);
//            },
//            sort_change: function(evt) {
//                var sort_keys = behaviors.get_instance_data(this, "sortable", "sort_columns_accessor"),
//                    acc = behaviors.get_instance_type_var(this, "sortable", "dom_data_accessor"),
//                    key = $(evt.target).data(acc),
//                    idx, first_key;

//                if ( !! !key) return;

//                if (_.isArray(sort_keys)) {
//                    idx = _(sort_keys).chain().pluck("ColumnName").indexOf(key).value()
//                    if (idx !== -1) {
//                        if (idx === 0) {
//                            sort_keys[0].Ascending = !sort_keys[0].Ascending;
//                        } else {
//                            first_key = sort_keys[idx];
//                            sort_keys.splice(idx, 1);
//                            sort_keys.unshift(first_key);
//                        }
//                    } else {
//                        sort_keys.unshift({
//                            ColumnName: key,
//                            Ascending: false
//                        })
//                    }
//                }

//                $(this).trigger("sort_change", [sort_keys]);
//            }
//        },
//        load_indicatable: {
//            requires: ["url_matches"],
//            listens_to: ["dom_rendered"],
//            raises: ["loading", "loaded"],
//            on_dom_rendered: function() {
//                var el = this.get_el(),
//                    matcher = behaviors.get_instance_type_var(this, "load_indicatable", "url_matches"),
//                    loading_raiser = $.proxy(function(request, settings) {
//                        $(this).trigger("loading", [request, settings]);
//                    }, this),
//                    complete_raiser = $.proxy(function(request, settings) {
//                        $(this).trigger("loaded", [request, settings]);
//                    }, this);

//                $(el).ajaxSend(function(evt, request, settings) {
//                    if (matcher(settings.url)) {
//                        $(this).addClass("loading");
//                        loading_raiser(request, settings);
//                    }
//                })

//                $(el).ajaxComplete(function(evt, request, settings) {
//                    if (matcher(settings.url)) {
//                        $(this).removeClass("loading");
//                        complete_raiser(request, settings);
//                    }
//                });
//            }
//        },
//        load_maskable: {
//            requires: ["delay", "text"],
//            listens_to: ["loading", "loaded"],
//            on_loading: function() {
//                var el = this.get_el(),
//                    frag = {
//                        content: [{
//                            content: behaviors.get_instance_type_var(this, "load_maskable", "text"),
//                            css: "loader_mask",
//                            attr: {
//                                style: "width: " + $(el).width() + "px; height: " + $(el).height() + "px"
//                            }
//                        }, {
//                            type: "div",
//                            css: "mask_image"
//                        }]
//                    };

//                this.load_action = function() {
//                    $(el).prepend(fastFrag.create(frag));
//                };
//                this.loader = _.delay(this.load_action, behaviors.get_instance_type_var(this, "load_maskable", "delay"));
//            },
//            on_loaded: function() {
//                clearTimeout(this.loader);
//                this.load_action = function() {};

//                $("div.loader_mask", this.get_el()).remove();
//            }
//        },
//        groupable: {
//            requires: ["group_accessor"],
//            raises: ["data_prepared"],
//            listens_to: ["prepare_data"],
//            bequeath: ["group_accessor"],
//            on_prepare_data: function(evt, data) {
//                var group = behaviors.get_instance_type_var(this, "groupable", "group_accessor"),
//                    by = behaviors.get_instance_type_var(this, "groupable", "group_by_accessor");

//                data[group] = _.group_by(data[group], by);

//                return data;
//            }
//        },
//        data_mergable: {
//            requires: ["data_destination_accessor"],
//            listens_to: ["prepare_data"],
//            on_prepare_data: function(evt, data) {
//                var data_destination_accessor = behaviors.get_instance_type_var(this, "data_mergable", "data_destination_accessor");
//                if (data[data_destination_accessor]) {
//                    _.each(data[data_destination_accessor], function(value, key) {
//                        if (_.isString(value.data) && _.indexOf(value.data, ".") !== -1) {
//                            value.data = _.reduce(value.data.split("."), function(memo, acc) {
//                                var return_value = memo[acc];
//                                return return_value;
//                            }, data);
//                        } else {
//                            value.data = data[value.data] || "";
//                        }
//                        if (_.isString(value.options)) {
//                            if (_.indexOf(value.options, ".") !== -1) {
//                                value.options = _.reduce(value.data.split("."), function(memo, acc) {
//                                    var return_value = memo[acc];
//                                    return return_value;
//                                }, data);
//                            } else {
//                                value.options = data[value.options] || [];
//                            }
//                        }
//                    });
//                }
//            }
//        },
//        submitable: {
//            requires: ["event", "scope"],
//            raises: ["submited"],
//            listens_to: ["dom_rendered", "clean"],
//            on_dom_rendered: function() {
//                var event_name = behaviors.get_instance_type_var(this, "submitable", "event");
//                var scope = behaviors.get_instance_type_var(this, "submitable", "scope");
//                var submit_on_change = behaviors.get_instance_type_var(this, "submitable", "submit_on_change");
//                var possible_var;
//                var raise_event = function(evt) {
//                        evt.stopPropagation();
//                        var params = {};
//                        var inputs = el.find("input, select, textbox");
//                        _.reduce(inputs, function(memo, item) {
//                            if (item.type == "checkbox") {
//                                memo[item.name] = $(item).is(":checked");
//                            } else if (item.type == "radio") {
//                                if ($(item).is(":checked")) memo[item.name] = $(item).val();
//                            } else {
//                                evt.preventDefault(); // calling this on radio/checkbox in IE8 prevents the item from being checked
//                                memo[item.name] = $(item).val();
//                            }
//                            return memo;
//                        }, params);
//                        if ($(evt.target).is("button")) {

//                            // many buttons don't have value attribute, breaks firefox - JW
//                            // this should handle getting the value in IE6+, but if it doesn't we'll parse the text
//                            possible_var = $(evt.target).val() ? $(evt.target).val() : evt.target.getAttributeNode("value") ? evt.target.getAttributeNode("value").nodeValue : null;
//                            if (possible_var === "" && /\n/.test(evt.target.innerText) !== -1 && !! evt.target.innerText) {
//                                possible_var = evt.target.innerText.split(/\n/)[1]
//                            }
//                            params[evt.target.name] = possible_var;
//                        }
//                        $(scope).trigger(event_name, [params, evt]); // serializeArray with custom info, PM
//                    };
//                var el = this.get_el();
//                el.find("button").bind("click", raise_event);
//                el.find("input").bind("keypress", function(evt) {
//                    if (evt.charCode === 13) {
//                        raise_event(evt);
//                    }
//                });
//                if (submit_on_change) {
//                    el.find(":input").bind("change", raise_event);
//                }
//            },
//            on_clean: function() {
//                var el = this.get_el();
//                $("button", el).unbind("click");
//            }
//        },
//        rebuildable: {
//            requires: [],
//            raises: ["rebuilt"],
//            listens_to: ["rebuild"],
//            on_rebuild: function(evt, data) {
//                this.clean(false, true);
//                this.build(data);
//            }
//        },
//        tabable: {
//            requires: ["application_accessor"],
//            bequeath: ["application_accessor"],
//            raises: ["tab_change"],
//            listens_to: ["dom_rendered", "before_clean"],
//            on_dom_rendered: function() {
//                var applications = behaviors.get_instance_data(this, "tabable", "application_accessor");
//                var sub_tabs = behaviors.get_instance_data(this, "tabable", "sub_tabs_accessor");
//                var options = behaviors.get_instance_data(this, "tabable", "application_options_accessor");
//                _.each(applications, function(v) {
//                    _.delay(v.main, 0, _.isFunction(options) ? options(v) : {});
//                });

//                var id_suffix = this.get_el().attr("id");
//                var tabs = $("#tabs_" + id_suffix);

//                $(":first-child", tabs).addClass("active");
//                tabs.find("li").bind("click", function(evt) {
//                    var el = $(evt.target);
//                    if (!el.is("li")) return;

//                    var idx = el.attr("data-idx"),
//                        adjusted = idx == "0" ? idx : parseInt(idx, 10) * $(document.body).width(),
//                        tab_group = el.parent(),
//                        tab_groups;

//                    if (sub_tabs) {
//                        var classes = _.reduce(sub_tabs, function(m, v) {
//                            return m + ".tab_button_" + v + ", ";
//                        }, "");
//                        if (el.is(classes)) {
//                            tab_group.addClass("small_tab");
//                            tab_groups = tab_group.siblings("ul.tabs");
//                            tab_groups.hide();
//                            var at_idx = tab_groups.length - idx;
//                            tab_groups.slice(at_idx, at_idx + 1).show();
//                        } else {
//                            tab_group.removeClass("small_tab");
//                            tab_group.siblings().hide();
//                        }
//                    }

//                    $("#tab_panel_" + id_suffix).css({
//                        "left": "-" + adjusted + "px"
//                    });
//                    tab_group.children().removeClass("active");
//                    el.addClass("active");
//                });
//            },
//            on_before_clean: function() {
//                var applications = behaviors.get_instance_data(this, "tabable", "application_accessor"),
//                    applications_with_clean = _.select(applications, function(i) {
//                        return _.isFunction(i.clean);
//                    });

//                _.invoke(applications_with_clean, "clean");

//                var id_suffix = this.get_el().attr("id");
//                var tabs = $("#tabs_" + id_suffix);
//                tabs.unbind().remove();
//            }
//        },
//        hideable: {
//            requires: ["visible_on_render", "animate"],
//            listens_to: ["dom_rendered", "hide", "show"],
//            on_dom_rendered: function(evt) {
//                var visible_on_render = behaviors.get_instance_type_var(this, "hideable", "visible_on_render");
//                if (!visible_on_render) {
//                    this.get_el().hide();
//                } else {
//                    this.get_el().show();
//                }
//            },
//            on_hide: function(evt) {
//                var animate_out = behaviors.get_instance_type_var(this, "hideable", "animate");
//                this.get_el()[animate_out ? "fadeOut" : "hide"]();
//            },
//            on_show: function(evt) {
//                var animate_in = behaviors.get_instance_type_var(this, "hideable", "animate");
//                this.get_el()[animate_in ? "fadeIn" : "show"]();
//            }
//        },
//        focusable: {
//            requires: [],
//            listens_to: ["set_to_focus"],
//            on_set_to_focus: function() {
//                this.get_el().findw(":input:first").focus();
//            }
//        }
//    };

//    var components = {
//        List: {
//            requires: ["item_accessor", "text_accessor"],
//            extend_to: {
//                item_selector: "li"
//            },
//            generateTemplate: function(config) {
//                return FirstLook.templates.generateSelectList(config);
//            }
//        },
//        GroupedList: {
//            requires: ["item_accessor", "text_accessor"],
//            extend_to: {
//                item_selector: "li"
//            },
//            generateTemplate: function(config) {
//                return FirstLook.templates.generateGroupedSelectList(config);
//            }
//        },
//        Fieldset: {
//            requires: ["input_accessor", "label_accessor", "type_accessor"],
//            generateTemplate: function(config) {
//                return FirstLook.templates.generateFieldset(config);
//            }
//        },
//        Table: {
//            requires: [],
//            generateTemplate: function(config) {
//                return FirstLook.templates.generateTable(config);
//            }
//        },
//        Tabs: {
//            requires: ["keys"],
//            generateTemplate: function() {
//                return function(data) {
//                    data.labels = data.labels || [];
//                    var w = $(document.body).width();
//                    var cards = _.reduce(data.keys, function(memo, card) {
//                        memo.push(FirstLook.cards.get(card));
//                        return memo;
//                    }, []);

//                    var el = this.get_el();

//                    var tabs = $("<ul />").attr("id", "tabs_" + this.__type__).addClass("tabs");

//                    var els = _.map(data.keys, function(id, idx) {
//                        var tab_panel = $("<div/>").addClass("card_" + id).addClass("tab tab_" + idx).width(w).css({
//                            "float": "left",
//                            "white-space": "normal"
//                        });
//                        var card = cards[idx];

//                        _.each(card.views, function(v, k, l) {
//                            var view = $("<div />").attr({
//                                "id": v.__type__
//                            });
//                            tab_panel.append(view);
//                        });

//                        tabs.append($("<li/>").text(data.labels[idx] || id).attr({
//                            "data-idx": idx
//                        }).addClass("tab_button tab_button_" + idx + " tab_button_" + id));

//                        return tab_panel;
//                    });
//                    el.width(w).css({
//                        "overflow": "hidden"
//                    });

//                    var panels = $("<div />").attr({
//                        "id": "tab_panel_" + this.__type__
//                    }).css({
//                        "white-space": "nowrap",
//                        "position": "relative",
//                        "-webkit-transition": "left 0.25s ease-in-out"
//                    });

//                    panels.width(w * (cards.length + 1));

//                    tabs.prependTo(panels);

//                    _.invoke(els, "appendTo", panels);

//                    return panels;
//                };
//            }
//        },
//        Placeholder: {
//            requires: ["component"],
//            generateTemplate: function(config) {
//                return function(data) {
//                    if (!config._component) {
//                        config._component = generateComponent(FirstLook.components.get(config.component));
//                    }
//                    if (!config.rendered) {
//                        config.rendered = true;
//                        return "<div id='" + config._component.__type__ + "'></div>";
//                    } else {
//                        _.defer(function() {
//                            config._component.clean();
//                            config._component.build(data);
//                        });
//                    }
//                    return null;
//                };
//            }
//        },
//        Cardholder: {
//            requires: ["card"],
//            generateTemplate: function(config) {
//                return function(data) {
//                    if (!config._card) {
//                        config._card = FirstLook.cards.get(config.card);
//                    }
//                    if (!config.rendered) {
//                        config.rendered = true;
//                        var t = _(config._card.views).chain().pluck("__type__").reduce(function(memo, v) {
//                            var r = _.concat([memo, "<div id='", v, "'></div>", ""]);
//                            return r;
//                        }, "").value();
//                        return t;
//                    } else {
//                        _.defer(function() {
//                            config._card.build(data, {
//                                avoid_render: true
//                            });
//                        });
//                    }
//                };
//            }
//        }
//    };

//    var patterns = {};

//    var cards = {
//        rendered: false,
//        lastRenderedString: "",
//        lastRendered: undefined,
//        lastRenderedCollection: undefined,
//        cards: [],
//        key_card: [],
//        card_data: {},
//        card_section: {},
//        render: function(cls) { /// Consider renaming... it's not really rendered here, PM
//            $(this).trigger("renderCalled.start", [cls, cards]);
//            var rendering, time;

//            rendering = cls.__type__ || JSON.stringify(cls);

//            if (!cards.rendered) FirstLook.templates.animateIn();

//            if (cards.lastRenderedString !== "" && !_.isEqual(rendering, cards.lastRenderedString) && $("#" + cls.__type__).size() === 0) {
//                // time = FirstLook.templates.animateOut();
//                // _.delay(_.bind(cards.lastRendered.clean, cards.lastRendered, true), time);
//                // if (cards.lastRenderedCollection) {
//                //	_.delay(_.bind(cards.lastRenderedCollection.clean, cards.lastRenderedCollection, true), time);
//                // }
//                // _.delay(_.bind(FirstLook.templates.animateIn, cards, true), time + 100);
//                cards.lastRenderedCollection = cls;
//            }

//            cards.lastRenderedString = rendering;
//            cards.lastRendered = cls;
//            cards.rendered = true;
//            $(this).trigger("renderCalled.finished", [cls, cards, rendering, time]);
//            return time || 0;
//        },
//        add: function(card, key) {
//            $(this).trigger("itemAdded", [key, card]);
//            this.key_card.push(key);
//            this.cards.push(generateTemplate(card, key));
//            $(this).trigger("itemAdded.finish", [key, this.cards[this.cards.length - 1]]);
//        },
//        get: function(key) {
//            var idx = _.indexOf(this.key_card, key);
//            if (idx !== -1) {
//                return this.cards[idx];
//            }
//            return null;
//        },
//        get_all: function(key) {
//            return _.map(_.filter(this.key_card, function(v) {
//                return v.indexOf(key) === 0
//            }), _.bind(this.get, this));
//        },
//        register_card_data: function(key, data) {
//            $(this).trigger("dataRegistered", [key, data]);
//            this.card_data[key] = data;
//        },
//        register_card_section: function(key, dom) {
//            $(this).trigger("sectionRegistered", [key, dom]);
//            if (_.isString(dom)) {
//                dom = document.getElementById(dom);
//            }
//            this.card_section[key] = dom;
//        },
//        get_card_section: function(key) {
//            return this.card_section[key] || null;
//        },
//        simple_make: function make_simple_card(lu) {
//            if (_.isArray(lu)) {
//                return _.reduce(lu, function(m, v, k) {
//                    var comp = FirstLook.components.get(v);
//                    if (comp.pattern !== _) m.pattern = _.extend(m.pattern, comp.pattern);
//                    m.view.controls.pattern = m.pattern;
//                    m.view.controls["Control_" + k] = comp;
//                    return m;
//                }, {
//                    title: "",
//                    pattern: {},
//                    view: {
//                        controls: {}
//                    }
//                });
//            } else {
//                var comp = FirstLook.components.get(lu)
//                return {
//                    title: "",
//                    pattern: comp.pattern,
//                    view: {
//                        controls: {
//                            pattern: comp.pattern,
//                            Controls: comp
//                        }
//                    }
//                }
//            }
//        },
//        error_make: function(lu) {
//            var simple = FirstLook.cards.simple_make(lu);

//            simple.view.error = _.extend({}, simple.view.controls);
//            simple.view.error.pattern = _.extend({
//                errorType: _.isString,
//                errorResponse: {
//                    responseText: _.isString
//                }
//            }, simple.view.controls.pattern);
//            simple.view.error.order = 0;
//            simple.view.error.error = FirstLook.components.get("Application.ServerError");

//            return simple;
//        }
//    };
//    FirstLook.cards = cards;

//    function create_obj() {
//        return function() {
//            if (_.isFunction(this.init)) {
//                this.init();
//            }
//        }
//    }
//    var proto_registry = {
//        init: function() {
//            this.items = [];
//            this.key_card = []
//        },
//        add: function(comp, key) {
//            $(this).trigger("itemAdded", [key, comp]);
//            this.key_card.push(key);
//            this.items.push(comp);
//        },
//        get: function(key) {
//            var idx = _.indexOf(this.key_card, key);
//            if (idx !== -1) {
//                return this.items[idx];
//            }
//            return null;
//        },
//        get_all: function() {
//            return this.items;
//        }
//    };

//    var registered_components = {
//        add: function(comp, key) {
//            registered_components.key_card.push(key);
//            registered_components.components.push(comp);
//        },
//        get: function(key) {
//            var idx = _.indexOf(registered_components.key_card, key);
//            if (idx !== -1) {
//                return registered_components.components[idx];
//            }
//            return null;
//        },
//        key_card: [],
//        components: []
//    };
//    var registry = create_obj();
//    registry.prototype = proto_registry;

//    FirstLook.components = new registry();
//    FirstLook.errors = new registry();
//    FirstLook.trackError = function(err, args) {
//        FirstLook.errors.add([err, args], err.message);
//        console.log(err.message);
//    }

//    FirstLook.records = new registry();

//    function onReady() {
//        getDOM(); /// Load DOM Template Items
//        $("form").bind("submit", _.preventDefault)
//    }
//    $(document).ready(onReady);

//    FirstLook.components.add({
//        type: "Static",
//        pattern: {
//            errorType: _.isString,
//            errorResponse: {
//                responseText: _.isString
//            }
//        },
//        template: function(data) {
//            return "<p class='error'>" + data.message + "</p>";
//        },
//        filter: function(data) {
//            var message = JSON.parse(data.errorResponse.responseText);

//            if (_.isString(message.Message)) {
//                message = message.Message;
//            } else {
//                message = data.errorType;
//            }
//            data.message = message;
//            return data;
//        }
//    }, "Application.ServerError");

//    FirstLook.logAll = function() {
//        $(FirstLook.cards).bind("itemAdded", _.logEvent);
//        $(FirstLook.cards).bind("itemAdded.finished", _.logEvent);
//        $(FirstLook.cards).bind("renderCalled.start", _.logEvent);
//        $(FirstLook.cards).bind("renderCalled.finished", _.logEvent);
//        $(FirstLook.cards).bind("dataRegistered", _.logEvent);
//        $(FirstLook.cards).bind("sectionRegistered", _.logEvent);
//        $(FirstLook.cards).bind("viewNotRendered", _.logEvent);
//        $(FirstLook.errors).bind("itemAdded", _.logEvent);
//        $(FirstLook.components).bind("itemAdded", _.logEvent);
//    }

//    FirstLook.logComponentEvents = function(comp) {
//        var list = _.reduce(comp.raises, function(m, e) {
//            m[e] = _.logEvent;
//            return m;
//        }, {});
//        $(comp).bind(list);
//    }

//    FirstLook.cmd = function(o) {
//        return _.reduce(o.listensTo, function(m, i) {
//            var stp = _.indexOf(i, ".")
//            var s = i.substring(stp === -1 ? -1 : stp + 1);
//            m[s] = function() {
//                var e = jQuery.Event(i);
//                $(o).trigger(e, _.toArray(arguments));
//                return e;
//            }
//            return m;
//        }, {})
//    }

//    FirstLook.getEventPath = function(o) {
//        if (_.isUndefined(o.chain)) throw new Error("getEventPath must be called with an object with a defined path");
//        var chain;

//        if (arguments.length > 1) {
//            chain = _(arguments).chain().pluck("chain").flatten().value();
//        } else {
//            chain = o.chain;
//        }

//        // remove type info if there
//        chain = _.map(chain, function(item) {
//            return item.replace(/\[.*\]\->/g, "->")
//        });

//        var paths = _.arrayStringToTree(chain, "->");
//        return paths;
//    }
//    FirstLook.getAPI = function(o) {
//        if (_.isUndefined(o.chain)) throw new Error("getEventPath must be called with an object with a defined path");
//        var chain;

//        if (arguments.length > 1) {
//            chain = _(arguments).chain().pluck("chain").flatten().value();
//        } else {
//            chain = o.chain;
//        }

//        var paths = _.arrayStringToTree(chain, "->");

//        // Remove Raises only from api tree
//        var unique_raising_events = _.intersection(o.raises, _(paths).chain().keys().map(function(item) {
//            return item.replace(/\[.*\]/g, "")
//        }).value());
//        _.each(unique_raising_events, function(evt) {
//            delete paths[evt];
//        })
//        return paths;
//    }

//    FirstLook.SectionWriter = {
//        generateSummaryContentBox: function(css, id_overall, id_summary, id_content, css_overall, css_summary, css_content) {
//            return {
//                type: "section",
//                id: id_overall,
//                css: _.concat(["cf collapsed ", css]),
//                content: {
//                    css: "pointless_design collapsed cf",
//                    content: [{
//                        css: _.concat(["summary ", css_summary]),
//                        id: id_summary
//                    }, {
//                        css: _.concat(["section_content cf hidden ", css_content]),
//                        id: id_content
//                    }]
//                }
//            }
//        }
//    }
//    FirstLook.SectionWriter.generateGreyBox = _.bind(FirstLook.SectionWriter.generateSummaryContentBox, {}, "grey_box");
//    FirstLook.SectionWriter.generateSolidWhiteBox = _.bind(FirstLook.SectionWriter.generateSummaryContentBox, {}, "solid_white_box");

//    $('ul.tabs button').live("click", function() {
//        var target_name = $(this).attr('name') ? $(this).attr('name').replace("show_", "#") : "";
//        var target = $(target_name);
//        if (target.length > 0) {
//            target.siblings(".tab_content").hide(0, function() {
//                $(this).addClass("hidden");
//            });
//            target.removeClass("hidden").show(0);

//            $(this).parent().siblings().removeClass("current");
//            $(this).parent().addClass("current");
//        }
//    })

//    $("section .summary").live("click", function() {
//        var target = $(this).next(".section_content"),
//            me$ = $(this),
//            currentIsVisible = target.is(":visible"),
//            shouldBeVisible = !currentIsVisible;

//        var slideCallback = function() {
//                target.toggleClass("hidden", !shouldBeVisible);
//            };

//        target.stop(true);

//        if (shouldBeVisible) {
//            target.slideDown(150, slideCallback);
//        } else {
//            target.slideUp(150, slideCallback);
//        }

//        me$.parent().toggleClass("open", shouldBeVisible);
//        me$.parent().toggleClass("collapsed", !shouldBeVisible);

//        me$.parent().parent().toggleClass("open", shouldBeVisible);
//        me$.parent().parent().toggleClass("collapsed", !shouldBeVisible);
//    });

//    $(".button_close").live("click", function(evt) {
//        var target = $(this).parents(".overlay");
//        target.toggleClass("hidden");
//    })

//    $("a").live("click", function(evt) {
//        var href = $(evt.target).attr("href");
//        if (!(_.isUndefined(href))) {
//            if (href.substring(0, 1) === "#") {
//                evt.preventDefault();
//                $(href).focus();
//            }
//        }
//    })

//})();

//// fixes IE8 Date.parse from ISO strings
//// https://github.com/csnover/js-iso8601
//(function(Date, undefined) {
//    var origParse = Date.parse,
//        numericKeys = [1, 4, 5, 6, 7, 10, 11];
//    Date.parse = function(date) {
//        var timestamp, struct, minutesOffset = 0;

//        // ES5 �15.9.4.2 states that the string should attempt to be parsed as a Date Time String Format string
//        // before falling back to any implementation-specific date parsing, so that�s what we do, even if native
//        // implementations could be faster
//        // 1 YYYY 2 MM 3 DD 4 HH 5 mm 6 ss 7 msec 8 Z 9 � 10 tzHH 11 tzmm
//        if ((struct = /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(date))) {
//            // avoid NaN timestamps caused by �undefined� values being passed to Date.UTC
//            for (var i = 0, k;
//            (k = numericKeys[i]); ++i) {
//                struct[k] = +struct[k] || 0;
//            }

//            // allow undefined days and months
//            struct[2] = (+struct[2] || 1) - 1;
//            struct[3] = +struct[3] || 1;

//            if (struct[8] !== 'Z' && struct[9] !== undefined) {
//                minutesOffset = struct[10] * 60 + struct[11];

//                if (struct[9] === '+') {
//                    minutesOffset = 0 - minutesOffset;
//                }
//            }

//            timestamp = Date.UTC(struct[1], struct[2], struct[3], struct[4], struct[5] + minutesOffset, struct[6], struct[7]);
//        } else {
//            timestamp = origParse ? origParse(date) : NaN;
//        }

//        return timestamp;
//    };
//}(Date));


///* Implement AJAX 'Loading...' feedback FOGBUGZ 21710 */
//(function() {

//    // Keep track of state ourself because sometimes ajaxStart happens before dom ready.
//    var indicator$, error$, showStatus = false,
//        shouldShow = false;

//    $(document).on("ajaxStart", function() {
//        updateVisibility(true);
//    });

//    $(document).on("ajaxStop", function() {
//        updateVisibility(false);
//    });

//    $(document).on("ajaxError", function(evt, xhr, sets, error) {
//        var msg;
//        try {
//            msg = JSON.parse(xhr.responseText).Message;
//        } catch (e) {
//            msg = error;
//        }
//        error$.attr("title", msg);
//        error$.show().delay(2500).fadeOut("slow");
//        error$.mouseover(function() {
//            error$.stop(true).mouseout(function() {
//                error$.stop(true).fadeOut("slow");
//            })
//        })
//    })

//    jQuery(function($) {
//        $("body").append('<div id="ajax-loader" class="global_loader"><div style="display:none">Loading...</div></div>');
//        $("body").append('<div id="ajax-loader-error" class="global_loader"><div style="display:none">An error occured while communicating with the server</div></div>');
//        error$ = $("#ajax-loader-error div");
//        indicator$ = $("#ajax-loader div");
//        updateVisibility(shouldShow);
//    });

//    function updateVisibility(show) {
//        shouldShow = show;
//        if (!indicator$ || showStatus === shouldShow) return;

//        if (shouldShow) {
//            indicator$.stop(true).delay(100).fadeTo("slow", 0.8);
//        } else {
//            indicator$.stop(true).fadeTo("slow", 0);
//        }
//        showStatus = shouldShow;
//    }
})();
