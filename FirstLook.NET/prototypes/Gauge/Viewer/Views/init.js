﻿requirejs.config({
    baseUrl: "../"
});

define(function (require) {
    var ko = require("Globals/knockout-2.1.0");
    var ViewModel = require("ViewModels/ViewModel");
    ko.applyBindings(new ViewModel());
});