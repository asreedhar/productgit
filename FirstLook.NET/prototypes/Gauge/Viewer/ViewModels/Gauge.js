﻿define(function (require) {

    function Gauge(config, viewModel) {
        this.config = config;
        this.viewModel = viewModel;
    };

    Gauge.prototype.Create = function () {

        var thisVar = { "rendered": false, "behaviors": { "dom_listenable": {}, "raises_changed": { "binds": { "changing": { "type": "Deal.TargetSellingPriceChange"}}} }, "__type__": "Static_3", "__wrapper__": "<div id='ping_overview'></div>", "$el": { "0": {}, "context": {}, "length": 1 }, "data": { "SearchSummaryDetail": [{ "Description": "CTS Sedan", "HasMarketDaySupply": true, "MarketDaySupply": 90, "IsReference": true, "IsSearch": true, "NumberOfListings": 6, "ListPrice": { "HasMinimum": false, "Minimum": 0, "HasAverage": true, "Average": 27983, "HasMaximum": false, "Maximum": 0, "SampleSize": 0 }, "Mileage": { "HasMinimum": false, "Minimum": 0, "HasAverage": true, "Average": 31691, "HasMaximum": false, "Maximum": 0, "SampleSize": 0} }, { "Description": "CTS Wagon", "HasMarketDaySupply": true, "MarketDaySupply": 0, "IsReference": false, "IsSearch": true, "NumberOfListings": 1, "ListPrice": { "HasMinimum": false, "Minimum": 0, "HasAverage": true, "Average": 34480, "HasMaximum": false, "Maximum": 0, "SampleSize": 0 }, "Mileage": { "HasMinimum": false, "Minimum": 0, "HasAverage": true, "Average": 30417, "HasMaximum": false, "Maximum": 0, "SampleSize": 0}}], "Pricing": { "VehicleEntityTypeId": 2, "VehicleEntityId": 1827170, "HasMileage": true, "Mileage": 12345, "HasMarketDaySupply": true, "MarketDaySupply": 79, "HasInventoryUnitCost": false, "InventoryUnitCost": 0, "HasInventoryListPrice": false, "InventoryListPrice": 0, "HasPctAvgMarketPrice": true, "PctAvgMarketPrice": 59, "NumListings": 7, "NumComparableListings": 6, "HasAppraisalValue": true, "AppraisalValue": 17250, "HasEstimatedReconditioningCost": false, "EstimatedReconditioningCost": 0, "HasTargetGrossProfit": false, "TargetGrossProfit": 0, "HasSearchRadius": true, "SearchRadius": 100, "Vin": "1G6DA5EG0A0103683", "MarketPrice": { "HasMinimum": true, "Minimum": 23977, "HasAverage": true, "Average": 29065, "HasMaximum": true, "Maximum": 34480, "SampleSize": 0 }, "VehicleMileage": { "HasMinimum": true, "Minimum": 17439, "HasAverage": true, "Average": 31509, "HasMaximum": true, "Maximum": 50585, "SampleSize": 0 }, "PctMarketAvg": { "HasMinimum": true, "Minimum": 96, "HasAverage": false, "Average": 0, "HasMaximum": true, "Maximum": 104, "SampleSize": 0 }, "GrossProfit": { "HasMinimum": true, "Minimum": 0, "HasAverage": false, "Average": 0, "HasMaximum": false, "Maximum": 0, "SampleSize": 0 }, "PctMarketValue": { "HasMinimum": true, "Minimum": 82, "HasAverage": false, "Average": 0, "HasMaximum": true, "Maximum": 119, "SampleSize": 0 }, "ComparableMarketPrice": { "HasMinimum": true, "Minimum": 7266, "HasAverage": false, "Average": 0, "HasMaximum": true, "Maximum": 145325, "SampleSize": 0 }, "JDPowerSalePrice": { "HasMinimum": false, "Minimum": 0, "HasAverage": false, "Average": 0, "HasMaximum": false, "Maximum": 0, "SampleSize": 0 }, "NaaaSalePrice": { "HasMinimum": false, "Minimum": 0, "HasAverage": true, "Average": 24800, "HasMaximum": false, "Maximum": 0, "SampleSize": 0}}} };

        thisVar.get_el = function () {
            var return_val = thisVar.$el = $(document.getElementById($(thisVar.__wrapper__).attr("id"))) || $("#" + this.__type__);
            return return_val;
        }

        var data = thisVar.data;
        var context = thisVar;
        var sliderContainer = $("#avg_graphic", thisVar.get_el()).get(0),
        r = new Raphael(sliderContainer, 450, 96),
        o = $(sliderContainer).offset(),
        b,
        min_mark,
        avg_mark,
        max_mark,
        min_market = data.Pricing.MarketPrice.Minimum,
        avg_market = data.Pricing.MarketPrice.Average,
        max_market = data.Pricing.MarketPrice.Maximum,
        range_market = max_market - min_market,
        m = range_market / 100,
        green_bars = {},
        width;
        var path_resource = {
            skinny: {
                base_path: "M0 37 L504 37 L504 57 L0 57 Z",
                power_zone: "M0 37 L50 37 L50 57 L50 57 L0 57 Z",
                update_start: function (path, point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3$4" + point + "$6");
                },
                update_end: function (path, point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3" + (point) + "$5" + point + "$7");
                },
                height: 20,
                offset: {
                    top: 37,
                    left: 27,
                    angle_offset: 27
                },
                should_shrink: false,
                handle: "M6 36 L12 43 L12 60 L0 60 L0 43 Z",
                handle_disabled: "M0 46 L6 60 L12 46 L6 33 Z",
                handle_width: 20,
                warning_text: 20
            },
            fat: {
                base_path: "M27 36 L444 36 L417 63 L0 63 Z",
                power_zone: "M0 36 L50 36 L85 49 L50 63 L0 63 Z",
                update_start: function (path, point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3$4" + point + "$6");
                },
                update_end: function (path, point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3" + (point + 30) + "$5" + point + "$7");
                },
                height: 27,
                offset: {
                    top: 36,
                    left: 27,
                    angle_offset: 27
                },
                should_shrink: true,
                handle: "M6 35 L12 42 L12 64 L0 64 L0 42 Z",
                handle_message: "APPRAISAL VALUE",
                handle_disabled: "M0 49 L6 67 L12 49 L6 32 Z",
                handle_width: 20,
                warning_text: 54
            }
        },
        graphic_resource = path_resource.fat,
        //TODO update for MMP
        hasSufficentListings = data.Pricing.NumComparableListings >= 5,
        handle = {},
        handle_text = {},
        enabled = true;

        b = Gauge._make_graphic(graphic_resource, r, o);
        Gauge._make_maker_bars(graphic_resource, 40, b, r, "#f7ae09");

        if (hasSufficentListings) {
            Gauge._set_area(graphic_resource, green_bars, Gauge._make_power_zone(graphic_resource, r), data.Pricing.PctMarketAvg.Minimum, data.Pricing.PctMarketAvg.Maximum, avg_market, min_market, max_market, r, b);

            min_mark = Gauge._make_named_marker(graphic_resource, 0.0, "LOW", min_market, b, r);
            avg_mark = Gauge._make_named_marker(graphic_resource, Gauge._get_x(min_market, m, avg_market), "AVG.", avg_market, b, r);
            max_mark = Gauge._make_named_marker(graphic_resource, 1.0, "HIGH", max_market, b, r);
        }

        function show_bubble() { }
        function hide_bubble() { }
        function format_comma(value) { }

        this.update_slider = function () { };
        this.enable_slider = function () {
            if (!enabled) {
                if (handle.attr) {
                    handle.attr({
                        "fill": "90-#fff-#d1d1d1-#f7f7f5-#fff",
                        "fill-opacity": 1
                    });
                }
                enabled = true;
            }
        }
        this.disable_slider = function () {
            if (enabled) {
                if (handle.attr) {
                    handle.attr({
                        "fill": "#ddd",
                        "fill-opacity": 0.8
                    });
                }
                enabled = false;
            }
        }
        if (hasSufficentListings) {
            handle = Gauge._make_handle(this.viewModel, m, min_market, graphic_resource, enabled, o, b, r);
            handle_message = Gauge._make_handle_text(b, r, this.config);
            this.enable_slider();
            this.update_slider = (function (handle, min, max) {
                var width = b.getBBox().width - graphic_resource.offset.angle_offset;
                var isVML = handle.paper.raphael.type == "VML";
                return function (price) {
                    if (price < min || isNaN(price)) price = min;
                    var center = width * Gauge._get_x(min_market, m, price),
            		current = handle.attr("translation").x,
            		isOutOfBoundsLower = center < 0,
            		isOutOfBoundsUpper = center > width - graphic_resource.offset.left,
            		delta;

                    if (isOutOfBoundsUpper) center = width - graphic_resource.offset.left;
                    if (isOutOfBoundsLower) center = 0;

                    if (isOutOfBoundsUpper || isOutOfBoundsLower) {
                        handle.attr({
                            fill: "#fff",
                            "fill-opacity": 0.5
                        });
                    }

                    if (!handle._is_sliding) {
                        delta = center + graphic_resource.offset.left - graphic_resource.handle_width - current;
                        handle.translate(delta, 0);
                        Gauge._update_handle_messge(handle_message, delta, price);
                    }
                }
            })(handle, min_market, max_market, b);
            this.update_slider(data.Pricing.InventoryListPrice);
        }
    }

    Gauge._make_power_zone = function (graphic_resource, _r) {
        var suggested_area = _r.path(graphic_resource.power_zone);
        suggested_area.attr({
            "fill": "90-#36b449-#a2dcaa",
            "stroke": "#2c9b3c",
            "clip": graphic_resource.base_path
        });

        return suggested_area;
    }

    Gauge._get_x = function (min_market, m, y) {
        // Get _pct
        // Slider roughly works like a slope intercept function y = xm + b, PM
        return ((y - min_market) / m) / 100;
    }

    Gauge._get_y = function (min_market, m, x) {
        // Get _dollar
        // Slider roughly works like a slope intercept function y = xm + b, PM
        return (x * m) * 100 + min_market;
    }

    Gauge._make_graphic = function (graphic_resource, _r, _o) {
        var path = graphic_resource.base_path,
            shadow = _r.path(path),
            bar = _r.path(path),
            set = _r.set();

        set.push(shadow, bar);
        set.push(bar);

        shadow.attr({
            "stroke": "none",
            "fill": "90-#d5d9e2-#adb0b5-#d5d9e2",
            "translation": "2,3"
        });
        shadow.scale(1.005, 1.005);

        bar.attr({
            "fill": "90-#fcb912-#fdeca6",
            "stroke": "#e0c400"
        });

        return set;
    }

    Gauge._make_named_marker = function (graphic_resource, pct, label, amt, box, _r) {
        var width = box.getBBox().width,
            x_angle_length = graphic_resource.offset.angle_offset,
            left_offset = graphic_resource.offset.left,
            x = Math.round((width - (x_angle_length * 2)) * pct) + left_offset,
            path = ["M", x, " ", (graphic_resource.offset.top - 1), " L", x, " ", (graphic_resource.offset.top + graphic_resource.height + 4)].join(""),
            bar = _r.path(path).attr({
                "stroke": "#a39568",
                "stroke-width": 2
            }),
            t1 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 11, label).attr({
                "font-size": 10,
                "fill": "#737373"
            }),
            t2 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 22, "$" + _.comma(amt)).attr({
                "font-size": 12,
                "font-weight": "bold",
                "fill": "#41455e"
            });
        s = _r.set();
        s.push(bar, t1, t2);

        return s;
    }

    Gauge._make_maker_bars = function (graphic_resource, num, box, _r, color, min, max, shrink) {
        var x_angle_length = graphic_resource.offset.angle_offset,
            x_offset = graphic_resource.offset.left,
            width = box.getBBox().width - (2 * x_angle_length),
            set = _r.set(),
            values,
            i = 0,
            last = 0;

        min = min !== 0 ? min || -Infinity : 0;
        max = max !== 0 ? max || Infinity : 0;

        function get_marker_path(pct, y1, y2) {
            if (pct === Infinity) pct = 100;
            var x = pct * width + x_angle_length;
            return {
                x: x,
                y1: y1,
                y2: y2
            };
        }

        function toLinePath(obj) {
            return ["M", obj.x, " ", obj.y1, " L", obj.x, " ", obj.y2].join("");
        }

        for (i = 0; i <= num; i++) {
            values = get_marker_path(i / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
            if (values.x >= min && values.x <= max) {
                last = i;
                set.push(_r.path(toLinePath(values)).attr({
                    "stroke": color
                }));
            }
        }

        if (shrink) {
            var slope = 0.5;
            var off = 0;
            while (true) {
                values = get_marker_path(++last / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                off = slope * (values.x - max);
                values.y1 = graphic_resource.offset.top + off;
                values.y2 = graphic_resource.height + graphic_resource.offset.top - off;
                if (values.y1 >= values.y2) {
                    break;
                }
                set.push(_r.path(toLinePath(values)).attr({
                    "stroke": color
                }));
            }
        }

        return set;
    }

    Gauge._set_area = function (graphic_resource, green_bars, suggested_area, start, end, avg, min, max, _r, b) {
        var old_path = suggested_area.attr("path") + "",
        // To String Hack
            new_path = old_path,
            x_angle_length = graphic_resource.offset.angle_offset,
            left_offset = graphic_resource.offset.left,
            width = b.getBBox().width - (2 * x_angle_length),
            path_start = width * (avg * (start * 0.01) - min) / (max - min) + left_offset,
            path_end = width * (avg * (end * 0.01) - min) / (max - min) + left_offset;

        if (path_start < x_angle_length) path_start = x_angle_length; // Min is the low point of the slider, 
        if (path_end > width) path_end = width; // Max is the high point of the slider,
        if (path_end < 0) {
            path_start = path_end = 0;
            suggested_area.hide();
        }
        new_path = graphic_resource.update_start(new_path, path_start);
        new_path = graphic_resource.update_end(new_path, path_end);

        if (green_bars.remove) green_bars.remove();
        green_bars = Gauge._make_maker_bars(graphic_resource, 40, b, _r, "#2c9b3c", path_start, path_end, graphic_resource.should_shrink);

        suggested_area.attr({
            "path": new_path
        });
    }

    Gauge._make_handle = function (viewModel, m, min_market, graphic_resource, enabled, o, b, _r) {
        var handle = _r.path(graphic_resource.handle),
            width = b.getBBox().width;
        if (handle.paper.raphael.type == "VML") {
            handle.node.UNSELECTABLE = "on"; // Fuck you IE please doen't load you're "accelerators", PM
        }

        handle.attr({
            stroke: "#969daf",
            "stroke-linecap": "square",
            "stroke-linejoin": "miter",
            fill: "90-#fff-#d1d1d1-#f7f7f5-#fff"
        });

        var drag = {
            start: function (x, y) {
                if (!enabled) return;
                handle._is_sliding = true;
                this.ox = x;
                this.offset = this.getBBox();
                this.attr({
                    "fill": "#fff",
                    "fill-opacity": 0.8
                });
            },
            end: function (x, y) {
                if (!enabled) return;
                handle._is_sliding = false;
                this.attr({
                    "fill": "90-#fff-#d1d1d1-#f7f7f5-#fff",
                    "fill-opacity": 1
                });
            },
            move: function (dx, dy) {
                if (!enabled) return;
                var d = this.ox + dx,
            		pos = this.attr("translation").x,
            		nx = d - pos - o.left - (graphic_resource.handle_width / 2),
            		a = (pos - 27 + 6) / (width - 59),
            		can_move_left = nx > 0 || a >= -0.02,
            		can_move_right = nx < 0 || a <= 1.02,
            		val;

                if (can_move_left && can_move_right) {
                    this.translate(nx, 0);
                    val = (Math.round(Gauge._get_y(min_market, m, a) / 10) * 10) - 1;
                    Gauge._update_handle_messge(handle_message, nx, val);

                    viewModel.updateTargetSellingPrice(val);
                }
            }
        };

        handle.drag(drag.move, drag.start, drag.end);

        return handle;
    }

    Gauge._make_handle_text = function (b, _r, configText) {
        var value = _r.text(-58, 16, ""),
            max = _r.text(0, 16, ""),
            label,
            line = _r.path("M-58 25 L100 25 Z"),
            arrow = _r.path("M3 25 L9 25 L6 33 L3 25 Z"),
            text_set = _r.set();
        set = _r.set();
        max.attr({
            "font-family": "'Arial Black'"
        });
        value.attr({
            "font-family": "'Arial Black'",
            fill: "#00601f"
        });
        value.id = "text_holder";

        label = _r.text(max.getBBox().width, 16, configText);

        text_set.push(value, max, label);

        text_set.attr({
            "text-anchor": "start",
            "font-size": 12
        })

        set.push(text_set, line, arrow);

        label.attr("fill", "#438556");
        max.attr("fill", "#438556");

        line.attr({
            fill: "#438556",
            stroke: "#438556"
        })

        arrow.attr({
            fill: "#438556",
            stroke: "#438556"
        })

        return set;
    }

    Gauge._update_handle_messge = function (handle_message, nx, value) {
        var current = handle_message[2].attr("translation").x;
        var delta = current + nx;
        if (Gauge._update_handle_messge_has_been_called && (delta < 65 || delta > 345)) {
            handle_message[2].translate(nx, 0);
        } else {
            handle_message.translate(nx, 0);
        }
        handle_message[0][0].attr("text", _.money(value));
        Gauge._update_handle_messge_has_been_called = true;
    }

    Gauge._update_handle_messge_has_been_called = false;

    return Gauge;

});