﻿define(function (require) {

    var ko = require("Globals/knockout-2.1.0");
    var Gauge = require("ViewModels/Gauge");

    function ViewModel() {
        var lowMileage = this.GetMileage();
        var highMileage = this.GetMileage();
        highMileage.push('Unlimited');

        this.gauge = new Gauge("TARGET SELLING PRICE", this);
        this.gauge.Create();
        
        this.appraisalValue = ko.observable(Globals.appraisal_value);
        this.appraisalValueFormatted = ko.computed(function () {
            return _.moneyOrSymbol(this.appraisalValue(), "N/A");
        }, this);

        this.estimatedRecon = ko.observable(Globals.estimated_recon);
        this.estimatedReconFormatted = ko.computed({
            read: function () {
                return _.moneyOrSymbol(this.estimatedRecon(), "N/A");
            },
            write: function (value) {
                this.estimatedRecon(_.toInt(value));
                this.gauge.update_slider(this.targetSellingPrice());
            },
            owner: this
        }, this);

        this.targetGrossProfit = ko.observable(Globals.target_gross_profit);
        this.targetGrossProfitFormatted = ko.computed({
            read: function () {
                return _.moneyOrSymbol(this.targetGrossProfit(), "N/A");
            },
            write: function (value) {
                var val = value.indexOf("-") <= 1 && value.indexOf("-") >= 0 ? -1 * _.toInt(value) : _.toInt(value);
                this.targetGrossProfit(val);
                this.gauge.update_slider(this.targetSellingPrice());
            },
            owner: this
        }, this);

        this.updateTargetSellingPrice = function (value) {
            this.targetGrossProfit(value - this.appraisalValue() + this.estimatedRecon());
        }

        this.targetSellingPrice = ko.computed(function () {
            return parseInt(this.appraisalValue()) + parseInt(this.targetGrossProfit()) - parseInt(this.estimatedRecon());
        }, this);

        this.targetSellingPriceFormatted = ko.computed(function () {
            return _.moneyOrSymbol(this.targetSellingPrice(), "N/A");
        }, this);

        this.mileageLow = lowMileage;
        this.mileageHigh = highMileage;

        this.gauge.update_slider(this.targetSellingPrice());
    };

    ViewModel.prototype.GetMileage = function () {
        var returnArr = ['0mi', '1,000mi', '2,000mi'];
        for (i = 5; i <= 125; i += 5) {
            returnArr.push(i.toString() + ',000mi');
        }
        return returnArr;
    }

    return ViewModel;

});