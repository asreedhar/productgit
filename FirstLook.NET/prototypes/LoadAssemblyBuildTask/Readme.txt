README
======

This is a simple build task that loads a list of assemblies into the AppDomain.

I updated the web deployment project, adding at the end the following XML.

  <UsingTask TaskName="LoadOutputAssemblies" AssemblyFile="$(MSBuildExtensionsPath)\FirstLook.Build.dll" />
  <ItemGroup>
    <AssemblyFiles Include="$(Configuration)\bin\FirstLook.Common.Security.CentralAuthenticationService.dll" />
  </ItemGroup>
  <Target Name="BeforeMerge">
	<LoadOutputAssemblies AssemblyFiles="@(AssemblyFiles)" />
  </Target>
  
This registers the new task (LoadOutputAssemblies) and supplies a list of files.  Without this the
CAS client configuration cannot be substituted (which sucks).  I still need to decide where to keep
the build targets (I'm thinking in a utils directory at the same level as the master solution).

I'll keep you updated.

Simon