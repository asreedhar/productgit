using System;
using System.Reflection;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace FirstLook.Build
{
    /// <summary>
    /// Load a generated assembly into the AppDomain.  This is used to make sure
    /// that our classes can be used in a web deployment project configuration
    /// replacement task.
    /// </summary>
    /// <see cref="Readme.txt"/>
    public class LoadOutputAssemblies : Task
    {
        private ITaskItem[] assemblyFiles;

        public override bool Execute()
        {
            
            string assemblyName = string.Empty;
            try
            {
                foreach (ITaskItem item in AssemblyFiles)
                {
                    assemblyName = item.GetMetadata("Identity");

                    base.Log.LogMessage(
                        MessageImportance.High, "Try and load assembly {0}.",
                        new object[] {assemblyName});

                    Assembly.LoadFrom(assemblyName);

                    base.Log.LogMessage("Assembly {0} loaded.", new object[] {assemblyName});
                }
                
                return true;
            }
            catch (Exception e)
            {
                base.Log.LogError("Failed to load assembly {0}", new object[] {assemblyName});

                base.Log.LogErrorFromException(e, true);
                
                return false;
            }
        }

        [Required]
        public ITaskItem[] AssemblyFiles
        {
            get
            {
                return assemblyFiles;
            }
            set
            {
                assemblyFiles = value;
            }
        }
    }
}
