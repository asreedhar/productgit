﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Build.Utilities;
using Microsoft.Build.Framework;

namespace FirstLook.BuildTasks
{
    public class SetVersion : Task
    {

        public override bool Execute()
        {
            string text;
            bool success = true;
            try
            {
                using (TextReader stream = new StreamReader(FirstLookAssemblyInfoPath))
                    text = stream.ReadToEnd();

                text = Regex.Replace(text, @"Version\s*=.*[\d\.\d\.\d\.\d]", string.Format("Version = \"{0}", Version));

                using (TextWriter writer = new StreamWriter(FirstLookAssemblyInfoPath, false))
                    writer.Write(text);
            }
            catch(Exception e)
            {
                Log.LogError(e.Message);
                success = false;
            }

            return success;
        }

        [Required]
        public string FirstLookAssemblyInfoPath { get; set; }

        [Required]
        public string Version { get; set; }
    }
}
