﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace FirstLook.BuildTasks
{
    public class SuccessFile : Task
    {
        private const string FileName = "Success.txt";
        
        public override bool Execute()
        {
            bool success = true;
            try
            {
                string path = Path.Combine(FilePath, FileName);
                using (TextWriter writer = new StreamWriter(path, false))
                {
                    string message = string.Format("Build successfully finished at {0}", DateTime.Now);
                    writer.WriteLine(message);
                }
            }
            catch (Exception e)
            {
                Log.LogError(e.Message);
                success = false;
            }
            
            return success;
        }

        [Required]
        public string FilePath { get; set; }
    }
}
