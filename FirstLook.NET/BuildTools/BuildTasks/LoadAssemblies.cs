﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace FirstLook.BuildTasks
{
    public class LoadAssemblies : Task
    {
        public override bool Execute()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            
            foreach (var asm in AssemblyFiles.Select(item => new FileInfo(item.GetMetadata("Identity"))).Where(asm => asm.Exists))
            {
                Assembly.Load(LoadFile(asm));
            }

            return true;
        }

        [Required]
        public ITaskItem[] AssemblyFiles { get; set; }

        static private byte[] LoadFile(FileInfo file)
        {
            using (var fs = file.OpenRead())
            {
                var buffer = new byte[(int)fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                return buffer;
            }
        }

        static private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()
                .Where(assembly => string.Compare(assembly.FullName, args.Name, StringComparison.OrdinalIgnoreCase) == 0))
            {
                return assembly;
            }

            throw new ArgumentException("Could not find the assembly!");
        }
    }
}