﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace FirstLook.BuildTasks
{
    public class NCoverRemoveTests : Task
    {
        [Required]
        public string CoverageFile { get; set; }

        public override bool Execute()
        {
            bool success = true;
            try
            {
                var regex = new Regex(@"test[s]?$", RegexOptions.IgnoreCase);
                string xml;
                using (StreamReader reader = new StreamReader(CoverageFile))
                {
                    xml = reader.ReadToEnd();
                }

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                var nodes = doc.SelectNodes("//module[@assembly]");
                var list = new List<XmlNode>();
                foreach (XmlNode node in nodes)
                {
                    string assembly = node.Attributes["assembly"].Value;
                    if (regex.IsMatch(assembly))
                        list.Add(node);
                }

                foreach (var remove in list)
                    remove.ParentNode.RemoveChild(remove);

                using (StreamWriter writer = new StreamWriter(CoverageFile))
                {
                    doc.Save(writer);
                }
            }
            catch (Exception e)
            {
                Log.LogError(e.Message);
                success = false;
            }

            return success;
        }
    }
}
