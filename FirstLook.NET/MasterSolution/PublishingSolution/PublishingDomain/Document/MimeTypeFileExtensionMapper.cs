﻿namespace PublishingDomain.Document
{
    /// <summary>
    /// Maps file extensions to mime types.  Only handles a few file types which we might serve.
    /// </summary>
    internal class MimeTypeFileExtensionMapper
    {

        internal static string MimeType(string fileExtension)
        {
            string retval;

            // Add "." if needed.
            if( !fileExtension.StartsWith("."))
            {
                fileExtension = "." + fileExtension;
            }

            switch (fileExtension.ToLower())
            {
                case ".doc": retval = "application/msword"; break;
                case ".docx": retval = "application/msword"; break;
                case ".pdf": retval = "application/pdf"; break;
                case ".xls": retval = "application/vnd.ms-excel"; break;
                case ".xlsx": retval = "application/vnd.ms-excel"; break;
                case ".xml": retval = "application/xml"; break;

                default: retval = "unknown/unknown"; break;
            }
            return retval;
        }

    }
}