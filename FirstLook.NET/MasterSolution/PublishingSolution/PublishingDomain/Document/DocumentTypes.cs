﻿namespace PublishingDomain.Document
{
    /// <summary>
    /// Document types currently supported by the publishing domain.
    /// </summary>
    public enum DocumentTypes
    {
        ValueAnalyzer=1
    }

    internal class DocumentTypeExtensions
    {
        /// <summary>
        /// Given a document type, return the correct file extension.
        /// </summary>
        /// <param name="docType"></param>
        /// <returns></returns>
        internal static string GetExtension( DocumentTypes docType )
        {
            if (docType == DocumentTypes.ValueAnalyzer) return ".pdf";            
            // Expand as more document types are created.
            return string.Empty;
        }
    }
}