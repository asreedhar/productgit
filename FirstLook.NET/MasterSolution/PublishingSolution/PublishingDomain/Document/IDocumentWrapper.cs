namespace PublishingDomain.Document
{
    /// <summary>
    /// The public API for document wrappers.  A document wrapper exposes the document's raw bytes along with
    /// some useful information.
    /// </summary>
    public interface IDocumentWrapper
    {
        byte[] DocumentSource { get; set; }
        string MimeType { get; set; }
        string Name { get; set; }
    }
}