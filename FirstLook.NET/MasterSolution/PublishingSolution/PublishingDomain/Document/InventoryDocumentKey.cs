﻿using System.Text;

namespace PublishingDomain.Document
{
    /// <summary>
    /// Inventory documents are identified by a document key.  The key consists of the business unit code, the vin,
    /// the stock number, and a document type.
    /// </summary>
    internal class InventoryDocumentKey : IInventoryDocumentKey
    {
        public string BusinessUnitCode { get; set; }
        public string VIN { get; set; }
        public string StockNumber { get; set; }
        public DocumentTypes DocumentType { get; set; }

        public string Encode()
        {
            const string DELIM = "_";

            // Put together the encoded key and verify that the length does not exceed 250 characters.
            var sb = new StringBuilder();
            sb.Append(BusinessUnitCode);
            sb.Append(DELIM);
            sb.Append(DocumentType);
            sb.Append(DELIM);
            sb.Append(StockNumber);
            sb.Append(DELIM);
            sb.Append(VIN);

            return sb.ToString();
        }
    }
}