﻿using PublishingDomain.FileSystem;

namespace PublishingDomain.Document
{
    /// <summary>
    /// A factory of document wrappers.
    /// </summary>
    internal class DocumentWrapperFactory
    {
        internal static IDocumentWrapper GetDocumentWrapper(IInventoryDocumentKey documentKey, byte[] fileSource)
        {
            // Get the file extension.
            var fileExtension = DocumentTypeExtensions.GetExtension(documentKey.DocumentType);

            var wrapper = new DocumentWrapper
                              {
                                  DocumentSource = fileSource,
                                  MimeType = MimeTypeFileExtensionMapper.MimeType(fileExtension),
                                  Name = FileUtilities.FileName(documentKey, fileExtension)
                              };

            return wrapper;
        }

        
    }
}