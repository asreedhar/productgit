﻿using System;

namespace PublishingDomain.Document
{
    /// <summary>
    /// A document wrapper contains a document's raw source (a byte[]), and a document desecriptor.
    /// </summary>
    [Serializable]
    internal class DocumentWrapper : IDocumentWrapper
    {
        public byte[] DocumentSource { get; set; }
        public string MimeType { get; set; }
        public string Name { get; set; }
    }
}