﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using FirstLook.Common.Impersonation;
using PublishingDomain.Diagnostics;
using PublishingDomain.Document;

namespace PublishingDomain.FileSystem
{
    public class InventoryDocumentRepository : IInventoryDocumentRepository
    {
        private readonly string _documentRoot;
        private const string DFS_CONNECTION = "MaxMarketingDFS";

        /// <summary>
        /// Ctor.  Files are saved/accessed at: documentRoot\docType\businessUnitCode\vin\stockNumber\
        /// </summary>
        /// <param name="documentRoot"></param>
        public InventoryDocumentRepository(string documentRoot)
        {
            _documentRoot = documentRoot;            
        }

        public InventoryDocumentRepository()
        {
            // Use the document root from the config file.
            _documentRoot = ConfigurationManager.AppSettings["InventoryDocumentRoot"];
        }        

        /// <summary>
        /// Save the file, returning the full file path.  The file type is implied by the documentKey.DocumentType
        /// This method is public for test purposes.  Clients should interact with void Save().
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="fileSource"></param>
        /// <returns></returns>
        public string SaveToFileSystem( IInventoryDocumentKey documentKey, byte[] fileSource )
        {
            string fullFilePath;

            using (ImpersonationCredentials.FromConnectionString(DFS_CONNECTION).LogOn())
            {
                var fileDescriptor = FileUtilities.GetFileSummary(documentKey, _documentRoot);

                // Create the directory if needed.
                var di = new DirectoryInfo(fileDescriptor.DirectoryPath);

                if (!di.Exists)
                {
                    di.Create();
                }

                // Write the document to the file system - overwriting it if a file with the same name already exists.
                fullFilePath = fileDescriptor.FullFilePath;
                File.WriteAllBytes(fullFilePath, fileSource);
            }

            PublishingCounters.Instance.Increment( FileSystemCounters.Write, FileSystemCounters.WritesPerSecond );            
            Trace.WriteLine("Thread: " + Thread.CurrentThread.ManagedThreadId + " filesystem write.");

            return fullFilePath;
        }
        
        /// <summary>
        /// Save the document.  The document type / mime type is implied by the documentKey.DocumentType.  This is the public API. 
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="fileSource"></param>
        public void Save(IInventoryDocumentKey documentKey, byte[] fileSource)
        {
            NameValueCollection details;
            Save(documentKey, fileSource, out details);
        }

        public void Save(IInventoryDocumentKey documentKey, byte[] fileSource, out NameValueCollection details)
        {
            var fullFilePath = SaveToFileSystem(documentKey, fileSource);
            details = new NameValueCollection {{"full_file_path", fullFilePath}};
        }

        /// <summary>
        /// Get the document identified by the supplied key.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <returns></returns>
        public IDocumentWrapper GetDocument(IInventoryDocumentKey documentKey)
        {
            byte[] byteArray;

            using (ImpersonationCredentials.FromConnectionString(DFS_CONNECTION).LogOn())
            {

                // Get a file summary
                var fileSummary = FileUtilities.GetFileSummary(documentKey, _documentRoot);

                // Does the document exist?  
                if (!Exists(documentKey))
                {
                    return null;
                }

                var files = GetFile(documentKey);
                FileInfo file = null;

                // If we have more than one file (we shouldn't), use the most recent one.
                if (files.Length > 1)
                {
                    DateTime mostRecent = DateTime.MinValue;

                    foreach (var f in files)
                    {
                        if (f.LastWriteTime > mostRecent)
                        {
                            file = f;
                            mostRecent = f.LastWriteTime;
                        }
                    }

                    // If we couldn't figure out which file is the most recent, throw.
                    if (file == null)
                    {
                        throw new ApplicationException(
                            "Multiple files exist.  Could not determine which is most recent.");
                    }
                }
                else
                {
                    file = files[0];
                }

                // Make sure that this is the file we care about.
                var fullFilePath = fileSummary.FullFilePath;

                if (String.Compare(Path.GetFullPath(file.FullName), Path.GetFullPath(fullFilePath), true) != 0)
                {
                    throw new ApplicationException(String.Format("Found file {0} does not match expected file {1}",
                                                                 file.FullName, fullFilePath));
                }

                // Get the Document's contents
                byteArray = File.ReadAllBytes(fullFilePath);
            }

            Trace.WriteLine("Thread: " + Thread.CurrentThread.ManagedThreadId + " filesystem read.");
            PublishingCounters.Instance.Increment(FileSystemCounters.Read, FileSystemCounters.ReadsPerSecond);

            // Return a document wrapper.
            return DocumentWrapperFactory.GetDocumentWrapper(documentKey, byteArray);
        }

        
        private FileInfo[] GetFile(IInventoryDocumentKey documentKey)
        {
            using (ImpersonationCredentials.FromConnectionString(DFS_CONNECTION).LogOn())
            {
                // Get a file summary
                var fileSummary = FileUtilities.GetFileSummary(documentKey, _documentRoot);

                // Make sure the directory has some files.
                var di = new DirectoryInfo(fileSummary.DirectoryPath);
                return di.GetFiles();
            }
        }


        /// <summary>
        /// Does the document exist for this key?
        /// </summary>
        /// <param name="documentKey"></param>
        /// <returns></returns>
        public bool Exists(IInventoryDocumentKey documentKey)
        {
            //using (ImpersonationCredentials.FromConnectionString(DFS_CONNECTION).LogOn())
            {
                var dirExists = new DirectoryInfo(FileUtilities.DirectoryPath(documentKey, _documentRoot)).Exists;
                if (!dirExists)
                {
                    return false;
                }

                // Do any files exist?
                return GetFile(documentKey).Length > 0;
            }
        }
    }
}