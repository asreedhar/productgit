﻿using System.IO;
using PublishingDomain.Document;

namespace PublishingDomain.FileSystem
{
    internal static class FileUtilities
    {
        /// <summary>
        /// Return all information about the file.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="documentRoot"></param>
        /// <returns></returns>
        internal static IFileSummary GetFileSummary(IInventoryDocumentKey documentKey, string documentRoot)
        {
            // Create a new FileSummary
            var f = new FileSummary();

            // Set the extension
            var fileExtension = FormatExtension(DocumentTypeExtensions.GetExtension(documentKey.DocumentType));
            f.FileExtension = fileExtension;

            // Set the file name
            f.FileName = FileName(documentKey, fileExtension);

            // Set the full file path
            f.FullFilePath = FullFilePath(documentKey, fileExtension, documentRoot);

            // set the mime type
            f.MimeType = MimeTypeFileExtensionMapper.MimeType(fileExtension);

            // set the dir path
            f.DirectoryPath = DirectoryPath(documentKey, documentRoot);

            return f;
        }

        /// <summary>
        /// What is the file name?
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        internal static string FileName(IInventoryDocumentKey documentKey, string fileExtension)
        {
            return documentKey.Encode() + FormatExtension(fileExtension);
        }

        /// <summary>
        /// Make sure the file extension has the "." and is lower case.
        /// </summary>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        internal static string FormatExtension(string fileExtension)
        {
            fileExtension = fileExtension.ToLower();
            if (!fileExtension.StartsWith("."))
            {
                fileExtension = "." + fileExtension;
            }

            return fileExtension;
        }

        /// <summary>
        /// Where will the document be stored?  
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="documentRoot"></param>
        /// <returns></returns>
        internal static string DirectoryPath(IInventoryDocumentKey documentKey, string documentRoot)
        {
            // documentRoot\docType\businessUnitCode\vin\stockNumber\
            string fullPath = Path.Combine(documentRoot, documentKey.DocumentType.ToString());
            fullPath = Path.Combine(fullPath, documentKey.BusinessUnitCode);
            fullPath = Path.Combine(fullPath, documentKey.VIN);
            fullPath = Path.Combine(fullPath, documentKey.StockNumber);

            return fullPath;
        }

        /// <summary>
        /// Construct the full file path.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="fileExtension"></param>
        /// <param name="documentRoot"></param>
        /// <returns></returns>
        internal static string FullFilePath(IInventoryDocumentKey documentKey, string fileExtension, string documentRoot)
        {
            return Path.Combine(DirectoryPath(documentKey, documentRoot), FileName(documentKey, fileExtension));
        }


        private class FileSummary : IFileSummary
        {
            public string FullFilePath { get; set; }
            public string FileName { get; set; }
            public string FileExtension { get; set; }
            public string MimeType { get; set; }
            public string DirectoryPath { get; set; }
        }

    }

    /// <summary>
    /// An IFileSummary exposes all of the information we know about a file.
    /// </summary>
    internal interface IFileSummary
    {
        string FullFilePath { get; }
        string FileName { get; }
        string FileExtension { get; }
        string MimeType { get; }
        string DirectoryPath { get; }
    }
}