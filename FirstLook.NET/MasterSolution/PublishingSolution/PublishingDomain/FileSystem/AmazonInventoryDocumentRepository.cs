﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using AmazonServices.S3;
using PublishingDomain.Diagnostics;
using PublishingDomain.Document;

namespace PublishingDomain.FileSystem
{
    public class AmazonInventoryDocumentRepository : IInventoryDocumentRepository
    {
        private string _documentRoot;
        private NameValueCollection _amzHeaders;

        public AmazonInventoryDocumentRepository()
        {
            _documentRoot = ConfigurationManager.AppSettings["AWSInventoryDocumentRoot"];
        }

        public AmazonInventoryDocumentRepository(string documentRoot = null)
        {
            _documentRoot = (String.IsNullOrEmpty(documentRoot)) ? ConfigurationManager.AppSettings["AWSInventoryDocumentRoot"] : documentRoot;
        }

        public void Save(IInventoryDocumentKey documentKey, byte[] fileSource)
        {
            NameValueCollection details;
            Save(documentKey, fileSource, out details);
        }

        public void Save(IInventoryDocumentKey documentKey, byte[] fileSource, out NameValueCollection details)
        {
            var fileDescriptor = FileUtilities.GetFileSummary(documentKey, _documentRoot);

            FileStore fileStore = GetFileStore(documentKey);

            string bucketName, keyName;
            fileStore.Write(fileDescriptor.FullFilePath, new MemoryStream(fileSource), out bucketName, out keyName);

            details = new NameValueCollection {{"bucket", bucketName}, {"key", keyName}};
        }

        private FileStore GetFileStore(IInventoryDocumentKey documentKey)
        {
            _amzHeaders = new NameValueCollection
            {
                {"x-amz-meta-businessunitcode",    documentKey.BusinessUnitCode},
                {"x-amz-meta-stocknumber",         documentKey.StockNumber},
                {"x-amz-meta-vin",                 documentKey.VIN},
                {"x-amz-meta-documenttype",        Enum.GetName(typeof(DocumentTypes), documentKey.DocumentType)}
            };

            // TODO: Need to use global naming rules. Must move PDFs over to new buckets first.
            FileStore fileStore = new FileStore("Incisent_Merchandising_FileStore_Inventory", _amzHeaders, "application/pdf");
            return fileStore;
        }

        public Document.IDocumentWrapper GetDocument(IInventoryDocumentKey documentKey)
        {
            byte[] byteArray;

            // Get a file summary
            var fileDescriptor = FileUtilities.GetFileSummary(documentKey, _documentRoot);

            bool exists = false;
            FileStore fileStore = GetFileStore(documentKey);
            using (Stream inputDocument = fileStore.Exists(fileDescriptor.FullFilePath, out exists))
            {
                if (!exists || inputDocument.Length.Equals(0))
                {
                    return null;
                }

                byteArray = ReadFully(inputDocument);
            }

            Trace.WriteLine("Thread: " + Thread.CurrentThread.ManagedThreadId + " filesystem read.");
            PublishingCounters.Instance.Increment(FileSystemCounters.Read, FileSystemCounters.ReadsPerSecond);

            // Return a document wrapper.
            return DocumentWrapperFactory.GetDocumentWrapper(documentKey, byteArray);
        }

        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Just a boolean for whether the file exists.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <returns></returns>
        public bool Exists(IInventoryDocumentKey documentKey)
        {
            var fileDescriptor = FileUtilities.GetFileSummary(documentKey, _documentRoot);

            bool exists = false;
            FileStore fileStore = GetFileStore(documentKey);
            exists = fileStore.ExistsNoStream(fileDescriptor.FullFilePath);

            return exists;
        }

        public bool Exists(IInventoryDocumentKey documentKey, out IDocumentWrapper documentWrapper)
        {
            documentWrapper = null;
            documentWrapper = GetDocument(documentKey);
            return documentWrapper != null;
        }
    }
}
