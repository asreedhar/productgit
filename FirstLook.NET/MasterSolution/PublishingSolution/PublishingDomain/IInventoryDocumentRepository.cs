using System.Collections.Specialized;
using PublishingDomain.Document;

namespace PublishingDomain
{
    /// <summary>
    /// An inventory document repository provides behavior to save and retrieve inventory documents.
    /// </summary>
    public interface IInventoryDocumentRepository
    {
        /// <summary>
        /// Save the file.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="fileSource"></param>
        /// <returns></returns>
        void Save( IInventoryDocumentKey documentKey, byte[] fileSource );

        /// <summary>
        /// Save the file, returning implementation-specific details about where it was stored.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <param name="fileSource"></param>
        /// <param name="details"></param>
        void Save( IInventoryDocumentKey documentKey, byte[] fileSource, out NameValueCollection details);

        /// <summary>
        /// Retrieve the document identified by the supplied key.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <returns></returns>
        IDocumentWrapper GetDocument( IInventoryDocumentKey documentKey );
    }
}