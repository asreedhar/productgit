﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;
using FirstLook.Common.Core.Command;
using PublishingDomain.Document;

namespace PublishingDomain
{
    /// <summary>
    /// A utility for converting between an InventoryDocumentKey and a query string.  Also can convert
    /// a name value collection to a query string.
    /// </summary>
    public static class InventoryDocumentKeyQueryStringMapper
    {
        private const string BUSINESS_UNIT_CODE = "buc";
        private const string VIN = "vin";
        private const string STOCK_NUM = "snum";
        private const string DOC_TYPE = "doctype";

        public static IInventoryDocumentKey Create( NameValueCollection queryString )
        {
            return new InventoryDocumentKey
                       {
                           BusinessUnitCode = queryString[BUSINESS_UNIT_CODE],
                           DocumentType = (DocumentTypes) Enum.Parse(typeof (DocumentTypes), queryString[DOC_TYPE]),
                           StockNumber = queryString[STOCK_NUM],
                           VIN = queryString[VIN]
                       };
        }

        public static IInventoryDocumentKey FromVin(NameValueCollection queryString)
        {
            Debug.Assert(queryString[VIN] != null);

            InventoryCommand command = new InventoryCommand(queryString[VIN]);
            AbstractCommand.DoRun(command);
            var nameValues = new NameValueCollection
                      {
                          {BUSINESS_UNIT_CODE, command.BusinessUnitCode},
                          {VIN, command.Vin},
                          {STOCK_NUM, command.StockNumber},
                          {DOC_TYPE, 1.ToString()}
                      };

            return Create(nameValues);
        }

        public static bool IsVinOnly(NameValueCollection queryString)
        {
            return (queryString[VIN] != null && queryString[DOC_TYPE] == null && queryString[STOCK_NUM] == null && queryString[BUSINESS_UNIT_CODE] == null);
        }

        public static bool IsVinAndDocType(NameValueCollection queryString)
        {
            return (queryString[VIN] != null && queryString[DOC_TYPE] != null && queryString[STOCK_NUM] == null && queryString[BUSINESS_UNIT_CODE] == null);
        }

        public static string CreateShortQueryString(IInventoryDocumentKey key)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("{0}={1}&", VIN, key.VIN);
            sb.AppendFormat("{0}={1}", DOC_TYPE, (int)key.DocumentType);

            return sb.ToString();
        }

        public static string CreateQueryString( IInventoryDocumentKey documentKey )
        {
            NameValueCollection collection = Convert(documentKey);

            StringBuilder sb = new StringBuilder();

            foreach( string key in collection.Keys )
            {
                sb.Append(key);
                sb.Append("=");
                sb.Append(collection[key]);
                sb.Append("&");
            }

            var queryString = sb.ToString();
            
            // remove the trailing '&' 
            if( queryString.EndsWith("&"))
            {
                queryString = queryString.TrimEnd('&');
            }

            return queryString;
        }

        private static NameValueCollection Convert(IInventoryDocumentKey key)
        {
            return new NameValueCollection
                      {
                          {BUSINESS_UNIT_CODE, key.BusinessUnitCode},
                          {VIN, key.VIN},
                          {STOCK_NUM, key.StockNumber},
                          {DOC_TYPE, ((int) key.DocumentType).ToString()}
                      };

        }


        public static IInventoryDocumentKey FromVinAndDocType(NameValueCollection queryString)
        {
            var command = new InventoryCommand(queryString[VIN]);
            AbstractCommand.DoRun(command);
            var nameValues = new NameValueCollection
                      {
                          {BUSINESS_UNIT_CODE, command.BusinessUnitCode},
                          {VIN, command.Vin},
                          {STOCK_NUM, command.StockNumber},
                          {DOC_TYPE, queryString[DOC_TYPE]}
                      };

            return Create(nameValues);
        }
    }


}
