using PublishingDomain.Document;

namespace PublishingDomain
{
    public interface IInventoryDocumentKey
    {
        string BusinessUnitCode { get; set; }
        string VIN { get; set; }
        string StockNumber { get; set; }
        DocumentTypes DocumentType { get; set; }
        string Encode();
    }
}