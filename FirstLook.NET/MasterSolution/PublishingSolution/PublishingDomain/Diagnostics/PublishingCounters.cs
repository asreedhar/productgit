﻿using System;
using System.Diagnostics;

namespace PublishingDomain.Diagnostics
{
    /// <summary>
    /// A singleton facade around performance counters used in publishing.
    /// </summary>
    public sealed class PublishingCounters
    {

        // File system counters
        private static IIncrement _readCounter, _writeCounter, _readsPerSecond, _writesPerSecond;

        // Cache counters
        private static IIncrement _getCounter, _setCounter, _missCounter, _hitCounter, _getsPerSecond, _setsPerSecond, _missesPerSecond, _hitsPerSecond;

        // Singleton
        private static readonly PublishingCounters instance = new PublishingCounters();

        // Singleton
        public static PublishingCounters Instance
        {
            get
            {
                return instance;
            }
        }


        // Explicit static constructor to tell c# compiler not to mark type as beforefieldinit
        static PublishingCounters() { }

        // Private ctor.
        PublishingCounters()
        {
            ConstructFileSystemCounters();
            ConstructCacheCounters();
        }

        /// <summary>
        /// File system counters
        /// </summary>
        private static void ConstructFileSystemCounters()
        {
            const string CATEGORY_NAME = CounterData.FILE_SYSTEM_CATEGORY_NAME;

            // Init static counters.
            if (_readCounter == null)
            {
                _readCounter = ConstructCounter(FileSystemCounters.Read, CATEGORY_NAME);
            }

            if (_readsPerSecond == null)
            {
                _readsPerSecond = ConstructCounter(FileSystemCounters.ReadsPerSecond, CATEGORY_NAME);
            }

            if (_writeCounter == null)
            {
                _writeCounter = ConstructCounter(FileSystemCounters.Write, CATEGORY_NAME);
            }

            if (_writesPerSecond == null)
            {
                _writesPerSecond = ConstructCounter(FileSystemCounters.WritesPerSecond, CATEGORY_NAME);
            }
        }

        /// <summary>
        /// Cache counters
        /// </summary>
        private static void ConstructCacheCounters()
        {
            const string CATEGORY_NAME = CounterData.CACHE_CATEGORY_NAME;

            // Init static counters.
            if (_getCounter == null)
            {
                _getCounter = ConstructCounter(CacheCounters.Get, CATEGORY_NAME);
            }

            if (_getsPerSecond == null)
            {
                _getsPerSecond = ConstructCounter(CacheCounters.GetsPerSecond, CATEGORY_NAME);
            }

            if (_setCounter == null)
            {
                _setCounter = ConstructCounter(CacheCounters.Set, CATEGORY_NAME);
            }

            if (_setsPerSecond == null)
            {
                _setsPerSecond = ConstructCounter(CacheCounters.SetsPerSecond, CATEGORY_NAME);
            }

            if (_missCounter == null)
            {
                _missCounter = ConstructCounter(CacheCounters.Miss, CATEGORY_NAME);
            }

            if (_missesPerSecond == null)
            {
                _missesPerSecond = ConstructCounter(CacheCounters.MissessPerSecond, CATEGORY_NAME);
            }

            if (_hitCounter == null)
            {
                _hitCounter = ConstructCounter(CacheCounters.Hit, CATEGORY_NAME);
            }

            if (_hitsPerSecond == null)
            {
                _hitsPerSecond = ConstructCounter(CacheCounters.HitsPerSecond, CATEGORY_NAME);
            }
        }

        private static IIncrement ConstructCounter(FileSystemCounters counter, string categoryName)
        {
            return ConstructCounter(CounterData.GetCounterName(counter), categoryName);
        }

        private static IIncrement ConstructCounter(CacheCounters counter, string categoryName)
        {
            return ConstructCounter(CounterData.GetCounterName(counter), categoryName);
        }

        private static IIncrement ConstructCounter(string counterName, string categoryName)
        {
            try
            {
                // null object pattern.
                if (!PerformanceCounterCategory.Exists(categoryName))
                {
                    return new NullPerformanceCounter();
                }

                // return a wrapped counter.
                var performanceCounter = new PerformanceCounter(categoryName, counterName, false);
                return new PerformanceCounterWrapper(performanceCounter);
            }
            catch (Exception)
            {
                // Something went wrong geting the counter.
                return new NullPerformanceCounter();
            }

        }



        internal void Increment( params FileSystemCounters[] counters)
        {
            foreach (var counter in counters)
            {
                switch (counter)
                {
                    case FileSystemCounters.Read:
                        _readCounter.Increment();
                        break;
                    case FileSystemCounters.Write:
                        _writeCounter.Increment();
                        break;
                    case FileSystemCounters.ReadsPerSecond:
                        _readsPerSecond.Increment();
                        break;
                    case FileSystemCounters.WritesPerSecond:
                        _writesPerSecond.Increment();
                        break;
                    default:
                        // do nothing.
                        break;
                }
            }

        }

        internal void Increment( params CacheCounters[] counters)
        {
            foreach (var counter in counters)
            {
                switch (counter)
                {
                    case CacheCounters.Get:
                        _getCounter.Increment();
                        break;
                    case CacheCounters.GetsPerSecond:
                        _getsPerSecond.Increment();
                        break;
                    case CacheCounters.Set:
                        _setCounter.Increment();
                        break;
                    case CacheCounters.SetsPerSecond:
                        _setsPerSecond.Increment();
                        break;
                    case CacheCounters.Miss:
                        _missCounter.Increment();
                        break;
                    case CacheCounters.MissessPerSecond:
                        _missesPerSecond.Increment();
                        break;
                    case CacheCounters.Hit:
                        _hitCounter.Increment();
                        break;
                    case CacheCounters.HitsPerSecond:
                        _hitsPerSecond.Increment();
                        break;
                    default:
                        // do nothing
                        break;
                }

            }
        }

    }
}