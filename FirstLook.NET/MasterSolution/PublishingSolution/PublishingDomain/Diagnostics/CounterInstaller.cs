﻿using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;

namespace PublishingDomain.Diagnostics
{
    /// <code>installutil "C:\full\path\to\FirstLook.VehicleHistoryReport.DomainModel.dll"</code>.
    /// <see cref="http://msdn2.microsoft.com/en-us/library/50614e95(VS.80).aspx"/>
    [RunInstaller(true)]
    public class CounterInstaller : Installer
    {
        public CounterInstaller()
        {
            InstallFileSystemCounters();
            InstallCacheCounters();
        }

        private static void InstallFileSystemCounters()
        {
            // Return if category already exists.
            if (PerformanceCounterCategory.Exists(CounterData.FILE_SYSTEM_CATEGORY_NAME)) return;
            
            var counters = new CounterCreationDataCollection();

            // 1 - total number of reads
            var totalReads = new CounterCreationData
                                 {
                                     CounterName = CounterData.GetCounterName(FileSystemCounters.Read),
                                     CounterHelp = CounterData.GetCounterHelp(FileSystemCounters.Read),
                                     CounterType = PerformanceCounterType.NumberOfItems64
                                 };
            counters.Add(totalReads);

            // 2 - total number of writes
            var totalWrites = new CounterCreationData
                                  {
                                      CounterName = CounterData.GetCounterName(FileSystemCounters.Write),
                                      CounterHelp = CounterData.GetCounterHelp(FileSystemCounters.Write),
                                      CounterType = PerformanceCounterType.NumberOfItems64
                                  };
            counters.Add(totalWrites);

            // 3 - reads per second
            var readsPerSecond = new CounterCreationData
                                     {
                                         CounterName = CounterData.GetCounterName(FileSystemCounters.ReadsPerSecond),
                                         CounterHelp = CounterData.GetCounterHelp(FileSystemCounters.ReadsPerSecond),
                                         CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                                     };
            counters.Add(readsPerSecond);

            // 4 - writes per second
            var writesPerSecond = new CounterCreationData
                                      {
                                          CounterName = CounterData.GetCounterName(FileSystemCounters.WritesPerSecond),
                                          CounterHelp = CounterData.GetCounterHelp(FileSystemCounters.WritesPerSecond),
                                          CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                                      };
            counters.Add(writesPerSecond);


            // create new category with the counters above
            PerformanceCounterCategory.Create(CounterData.FILE_SYSTEM_CATEGORY_NAME, 
                                              CounterData.FILE_SYSTEM_CATEGORY_DESC,
                                              PerformanceCounterCategoryType.SingleInstance, counters);
        }

        private static void InstallCacheCounters()
        {
            // Return if counters already exist.
            if (PerformanceCounterCategory.Exists(CounterData.CACHE_CATEGORY_NAME)) return;
            
            var counters = new CounterCreationDataCollection();

            // total get ops
            var totalGets = new CounterCreationData
                                {
                                    CounterName = CounterData.GetCounterName(CacheCounters.Get),
                                    CounterHelp = CounterData.GetCounterHelp(CacheCounters.Get),
                                    CounterType = PerformanceCounterType.NumberOfItems64
                                };
            counters.Add(totalGets);

            // gets per second
            var getsPerSecond = new CounterCreationData
                                    {
                                        CounterName = CounterData.GetCounterName(CacheCounters.GetsPerSecond),
                                        CounterHelp = CounterData.GetCounterHelp(CacheCounters.GetsPerSecond),
                                        CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                                    };
            counters.Add(getsPerSecond);

            // total set ops
            var totalSets = new CounterCreationData
                                {
                                    CounterName = CounterData.GetCounterName(CacheCounters.Set),
                                    CounterHelp = CounterData.GetCounterHelp(CacheCounters.Set),
                                    CounterType = PerformanceCounterType.NumberOfItems64
                                };
            counters.Add(totalSets);

            // sets per second
            var setsPerSecond = new CounterCreationData
                                    {
                                        CounterName = CounterData.GetCounterName(CacheCounters.SetsPerSecond),
                                        CounterHelp = CounterData.GetCounterHelp(CacheCounters.SetsPerSecond),
                                        CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                                    };
            counters.Add(setsPerSecond);

            // total misses
            var totalMiss = new CounterCreationData
                                {
                                    CounterName = CounterData.GetCounterName(CacheCounters.Miss),
                                    CounterHelp = CounterData.GetCounterHelp(CacheCounters.Miss),
                                    CounterType = PerformanceCounterType.NumberOfItems64
                                };
            counters.Add(totalMiss);

            // misses per second
            var missesPerSecond = new CounterCreationData
                                      {
                                          CounterName = CounterData.GetCounterName(CacheCounters.MissessPerSecond),
                                          CounterHelp = CounterData.GetCounterHelp(CacheCounters.MissessPerSecond),
                                          CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                                      };
            counters.Add(missesPerSecond);

            // total hits
            var totalHits = new CounterCreationData
                                {
                                    CounterName = CounterData.GetCounterName(CacheCounters.Hit),
                                    CounterHelp = CounterData.GetCounterHelp(CacheCounters.Hit),
                                    CounterType = PerformanceCounterType.NumberOfItems64
                                };
            counters.Add(totalHits);

            // hits per second
            var hitsPerSecond = new CounterCreationData
                                    {
                                        CounterName = CounterData.GetCounterName(CacheCounters.HitsPerSecond),
                                        CounterHelp = CounterData.GetCounterHelp(CacheCounters.HitsPerSecond),
                                        CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                                    };
            counters.Add(hitsPerSecond);

            // create new category with the counters above
            PerformanceCounterCategory.Create(CounterData.CACHE_CATEGORY_NAME, 
                                              CounterData.CACHE_CATEGORY_DESC,
                                              PerformanceCounterCategoryType.SingleInstance, counters);
        }

    }
}