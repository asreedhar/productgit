using System.Diagnostics;

namespace PublishingDomain.Diagnostics
{
    internal class PerformanceCounterWrapper : IIncrement
    {
        private readonly PerformanceCounter _counter;

        internal PerformanceCounterWrapper(PerformanceCounter counter)
        {
            _counter = counter;
        }

        public void Increment()
        {
            _counter.Increment();
        }
    }
}