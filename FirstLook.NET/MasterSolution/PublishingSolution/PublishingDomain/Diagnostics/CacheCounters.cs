namespace PublishingDomain.Diagnostics
{
    public enum CacheCounters
    {
        Get = 1,
        GetsPerSecond,
        Set,
        SetsPerSecond,
        Miss,
        MissessPerSecond,
        Hit,
        HitsPerSecond
    }
}