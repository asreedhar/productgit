﻿namespace PublishingDomain.Diagnostics
{
    public interface IIncrement
    {
        void Increment();
    }
}