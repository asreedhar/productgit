namespace PublishingDomain.Diagnostics
{
    public enum FileSystemCounters
    {
        Read = 1,
        Write,
        ReadsPerSecond,
        WritesPerSecond
    }
}