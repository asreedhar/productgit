﻿namespace PublishingDomain.Diagnostics
{
    /// <summary>
    /// Returns counter names and help text.
    /// </summary>
    internal static class CounterData
    {
        public const string FILE_SYSTEM_CATEGORY_NAME = "PublishingFileSystem";
        public const string CACHE_CATEGORY_NAME = "PublishingCache";

        public const string FILE_SYSTEM_CATEGORY_DESC = "Document file system access";
        public const string CACHE_CATEGORY_DESC = "Document cache access";


        public static string GetCounterName(FileSystemCounters counter)
        {
            switch (counter)
            {
                case FileSystemCounters.Read:
                    return "# READ operations executed";
                case FileSystemCounters.Write:
                    return "# WRITE operations executed";
                case FileSystemCounters.ReadsPerSecond:
                    return "# READ operations per second";
                case FileSystemCounters.WritesPerSecond:
                    return "# WRITE operations per second";
                default:
                    return "unknown";
            }
        }
        public static string GetCounterHelp(FileSystemCounters counter)
        {
            switch (counter)
            {
                case FileSystemCounters.Read:
                    return "Total number of read operations executed";
                case FileSystemCounters.Write:
                    return "Total number of write operations executed";
                case FileSystemCounters.ReadsPerSecond:
                    return "Read operations per second";
                case FileSystemCounters.WritesPerSecond:
                    return "Write operations per second";
                default:
                    return "unknown";
            }

        }

        public static string GetCounterName(CacheCounters counter)
        {
            switch (counter)
            {
                case CacheCounters.Get:
                    return "# GET operations executed";
                case CacheCounters.Set:
                    return "# SET operations executed";
                case CacheCounters.Miss:
                    return "# Cache MISSES";
                case CacheCounters.Hit:
                    return "# Cache HITS";
                case CacheCounters.GetsPerSecond:
                    return "# GET operations per second";
                case CacheCounters.HitsPerSecond:
                    return "# hits per second";
                case CacheCounters.MissessPerSecond:
                    return "# misses per second";
                case CacheCounters.SetsPerSecond:
                    return "# SET operations per second";
                default:
                    return "unknown";
            }

        }
        public static string GetCounterHelp(CacheCounters counter)
        {
            switch (counter)
            {
                case CacheCounters.Get:
                    return "Total number of GET operations executed";

                case CacheCounters.GetsPerSecond:
                    return "GET operations per second";

                case CacheCounters.Set:
                    return "Total number of SET operations executed";

                case CacheCounters.SetsPerSecond:
                    return "SET operations per second";

                case CacheCounters.Miss:
                    return "Total number of cache misses";

                case CacheCounters.MissessPerSecond:
                    return "Cache misses per second";

                case CacheCounters.Hit:
                    return "Total number of cache hits";

                case CacheCounters.HitsPerSecond:
                    return "Cache hits per second";

                default:
                    return "unknown";
            }

        }
    }
}