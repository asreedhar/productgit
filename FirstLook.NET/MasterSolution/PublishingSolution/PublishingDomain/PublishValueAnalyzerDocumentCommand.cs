﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using log4net;
using PublishingDomain.Document;

namespace PublishingDomain
{
    /// <summary>
    /// A public command by which client code can publish value analyzer documents.
    /// </summary>
    public class PublishValueAnalyzerDocumentCommand : AbstractCommand 
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PublishValueAnalyzerDocumentCommand).FullName);

        private readonly string _businessUnitCode;
        private readonly string _vin;
        private readonly string _stockNumber;
        private readonly byte[] _document;
        private readonly ILogger _logger;
        private readonly IInventoryDocumentRepository _documentRepository;

        private NameValueCollection _publicationDetails; 

        private readonly string _publishedUrlPrefix;
        
        public string PublishedUrl { get; private set; }
        public string ShortUrl { get; private set; }

        public PublishValueAnalyzerDocumentCommand(string businessUnitCode, string vin, string stockNumber, byte[] document, 
            ILogger logger, IInventoryDocumentRepository documentRepository)
        {
            _businessUnitCode = businessUnitCode;
            _vin = vin;
            _stockNumber = stockNumber;
            _document = document;
            _logger = logger;
            _documentRepository = documentRepository;

            PublishedUrl = String.Empty;
            ShortUrl = String.Empty;
             
            _publishedUrlPrefix = ConfigurationManager.AppSettings["ValueAnalyzerUrlPrefix"];
        }

        protected override void Run()
        {
            try
            {
                // Construct a document key
                var key = InventoryDocumentKeyFactory.Create(_businessUnitCode, _vin, _stockNumber,
                                                             DocumentTypes.ValueAnalyzer);
                // Save document, capturing the publication details.
                _documentRepository.Save(key, _document, out _publicationDetails);

                // Generate the long and short urls.
                var queryString = InventoryDocumentKeyQueryStringMapper.CreateShortQueryString(key);

                PublishedUrl = _publishedUrlPrefix + "?" + queryString;
                ShortUrl = GenerateShortUrl(PublishedUrl);
            }
            catch (Exception ex)
            {
                Log.Error("Error saving pdf to disk", ex);
                _logger.Log(ex);
            }

        }

        public NameValueCollection PublicationDetails
        {
            get { return _publicationDetails; }
        }

        private string GenerateShortUrl(string longUrl)
        {
            const string TINY_URL_API = "http://tinyurl.com/api-create.php?url={0}";
            var tinyUrlApi = string.Format(TINY_URL_API, longUrl);

            var shortUrl = string.Empty;

            // Try to generate the short url.                
            try
            {
                WebRequest request = WebRequest.Create(tinyUrlApi);
                request.Timeout = 10000; // 10 seconds timeout
                var response = request.GetResponse();
                using (var s = response.GetResponseStream())
                {
                    var reader = new StreamReader(s);
                    shortUrl = reader.ReadToEnd();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
            }

            return shortUrl;
        }

       
    }
}