using PublishingDomain.Document;

namespace PublishingDomain
{
    public static class InventoryDocumentKeyFactory
    {
        public static IInventoryDocumentKey Create(string businessUnitCode, string vin, string stockNumber, DocumentTypes documentType)
        {
            return new InventoryDocumentKey
                       {
                           BusinessUnitCode = businessUnitCode,
                           DocumentType = documentType,
                           StockNumber = stockNumber,
                           VIN = vin
                       };
        }
    }
}