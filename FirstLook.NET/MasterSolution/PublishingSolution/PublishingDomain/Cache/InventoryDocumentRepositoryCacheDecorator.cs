﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading;

using FirstLook.Common.Core;
using MAX.Caching;
using PublishingDomain.Diagnostics;
using PublishingDomain.Document;

namespace PublishingDomain.Cache
{
    /// <summary>
    /// A cache decorator around an IInventoryDocumentRepository
    /// </summary>
    public class InventoryDocumentRepositoryCacheDecorator : IInventoryDocumentRepository
    {
        private readonly ICacheWrapper _cache;
        private readonly ILogger _logger;
        
        private static readonly object CacheLock = new object();
        private const int MAX_KEY_LENGTH = 250;
        private const int MAX_DATA_LENGTH = 1048576; // 1 MB

        private readonly IInventoryDocumentRepository _repository;

        public InventoryDocumentRepositoryCacheDecorator(IInventoryDocumentRepository repository, ILogger logger, ICacheWrapper cache)
        {
            _repository = repository;
            _logger = logger;
            _cache = cache;
        }

        public void Save(IInventoryDocumentKey documentKey, byte[] fileSource)
        {
            NameValueCollection details;
            Save(documentKey, fileSource, out details);
        }

        public void Save(IInventoryDocumentKey documentKey, byte[] fileSource, out NameValueCollection details)
        {
            // Check the lengths of key and data - an application exception is thrown if they are too big.
            var encodedKey = documentKey.Encode();
            VerifyLengths(encodedKey, fileSource);

            // Call the underlying repository to save.
            _repository.Save(documentKey, fileSource, out details);

            // update the cache
            lock (CacheLock)
            {
                var wrapper = DocumentWrapperFactory.GetDocumentWrapper(documentKey, fileSource);

                _cache.Set(encodedKey, wrapper);

                PublishingCounters.Instance.Increment(CacheCounters.Set, CacheCounters.SetsPerSecond);
                Trace.WriteLine("Thread: " + Thread.CurrentThread.ManagedThreadId + " cache set");
            }
        }

        public IDocumentWrapper GetDocument(IInventoryDocumentKey documentKey)
        {
            // Try to get the document wrapper from the cache.
            IDocumentWrapper wrapper;
            string encodedKey = documentKey.Encode();

            try
            {
                object obj = _cache.Get( encodedKey );
                PublishingCounters.Instance.Increment(CacheCounters.Get, CacheCounters.GetsPerSecond);

                // Is it a cache miss?
                if (obj == null)
                {
                    PublishingCounters.Instance.Increment(CacheCounters.Miss, CacheCounters.MissessPerSecond);
                }
                else
                {
                    wrapper = obj as DocumentWrapper;

                    if (wrapper != null)
                    {
                        Trace.WriteLine("Thread: " + Thread.CurrentThread.ManagedThreadId + " cache hit");
                        PublishingCounters.Instance.Increment(CacheCounters.Hit, CacheCounters.HitsPerSecond);

                        return wrapper;
                    }
                }

            }
            catch (Exception ex)
            {
                // Something went wrong.
                _logger.Log(ex);
            }

            // Get the document from the file system repository.
            wrapper = _repository.GetDocument(documentKey);

            // Add the document wrapper to the cache.
            lock (CacheLock)
            {
                _cache.Set( encodedKey, wrapper);

                Trace.WriteLine("Thread: " + Thread.CurrentThread.ManagedThreadId + " cache set");
                PublishingCounters.Instance.Increment(CacheCounters.Set, CacheCounters.SetsPerSecond);
            }

            return wrapper;
        }

       
        /// <summary>
        /// Verify that the key and data are not too "long".  Throw an exception if they are.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        private static void VerifyLengths(string key, ICollection<byte> data)
        {
            if (key.Length > MAX_KEY_LENGTH)
            {
                throw new ApplicationException("The key length is too long.");
            }

            if (data.Count > MAX_DATA_LENGTH)
            {
                throw new ApplicationException("The file is too large.");
            }
        }

    }
}