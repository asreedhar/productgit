﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;


namespace PublishingDomain
{
    public class InventoryCommand : AbstractCommand
    {
        public string Vin { get; private set;}
        public string StockNumber { get; private set; }
        public string BusinessUnitCode {get; private set;}

        public InventoryCommand(string vin)
        {
            Vin = vin;
        }

        protected override void Run()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Marketing.GetInventoryInfoFromVin";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameterWithValue("vin", DbType.String, false, Vin);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("No inventory found for vin");

                        StockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));
                        BusinessUnitCode = reader.GetString(reader.GetOrdinal("BusinessUnitCode"));
                    }
                }
            }
        }
    }
}
