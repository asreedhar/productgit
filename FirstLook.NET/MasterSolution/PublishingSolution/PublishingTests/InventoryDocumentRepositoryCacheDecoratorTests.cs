﻿using FirstLook.Common.Core;
using MAX.Caching;
using NUnit.Framework;
using PublishingDomain;
using PublishingDomain.Cache;
using PublishingDomain.Document;
using PublishingDomain.FileSystem;

namespace PublishingTests
{
    /// <summary>
    /// A test of the cache decorator.  Note - you must have memcached running on the nodes specified in the TestFixtureSetup.
    /// </summary>
    [TestFixture]
    public class InventoryDocumentRepositoryCacheDecoratorTests  
    {
        private string _root;
        private string _vin;
        private string _businessUnitCode;
        private string _stockNumber;

        private IInventoryDocumentKey _key;
        private InventoryDocumentRepository _repository;

        private ICacheWrapper _cache;
        private ILogger _logger;


        private InventoryDocumentRepositoryCacheDecorator _cachedRepository;

        [TestFixtureSetUp]
        public void InventoryDocumentRepositoryCacheDecoratorTestsSetup()
        {
            _root = "C:\\DocRoot";
            _vin = "99988877766655544";
            _businessUnitCode = "BROWN002";
            _stockNumber = "54321";

            _key = InventoryDocumentKeyFactory.Create(_businessUnitCode, _vin, _stockNumber, DocumentTypes.ValueAnalyzer);
            _repository = new InventoryDocumentRepository(_root);

            _cache = new MemcachedClientWrapper();
            _logger = new NullLogger();

            _cachedRepository = new InventoryDocumentRepositoryCacheDecorator(_repository, _logger, _cache);
        }

        [Test]
        public void CanWriteAndReadFromCache()
        {
            var b = RandomHelpers.GetRandomBytes(1024 * 100); // 100 KB
            _cachedRepository.Save(_key, b);

            var wrapper = new MemcachedClientWrapper();

            object obj = wrapper.Get(_key.Encode());

            Assert.IsNotNull(obj); 
        }

        [Test]
        public void VerifyCacheIsConsistentAfterWrite()
        {
            var b = RandomHelpers.GetRandomBytes(1024 * 100); // 100 KB
            _cachedRepository.Save(_key, b);

            var wrapper = new MemcachedClientWrapper();
            var docWrapper = (DocumentWrapper) wrapper.Get(_key.Encode());

            Assert.IsNotNull( docWrapper );

            var bites = docWrapper.DocumentSource;

            Assert.AreEqual( b.Length, bites.Length );

            for( int i = 0; i < b.Length; i++)
            {
                int original = b[i];
                int cached = bites[i];

                Assert.AreEqual(original, cached);                
            }

            Assert.AreEqual("application/pdf", docWrapper.MimeType);

            string name = _key.Encode() + ".pdf";
            Assert.AreEqual( name, docWrapper.Name );
        }
    }
}
