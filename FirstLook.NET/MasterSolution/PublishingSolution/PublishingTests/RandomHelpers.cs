﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace PublishingTests
{
    internal static class RandomHelpers
    {
        internal static byte[] GetRandomBytes(int maxLength)
        {
            var random = new Random((int)DateTime.Now.Ticks ^ Thread.CurrentThread.ManagedThreadId);
            var bites = new byte[maxLength];
            random.NextBytes(bites);

            return bites;
        }

        internal static IList<string> GetRandomVins(int num)
        {
            var vins = new List<string>(num);

            var random = new Random((int)DateTime.Now.Ticks ^ Thread.CurrentThread.ManagedThreadId);

            for (int i = 0; i < num; i++)
            {
                // Build a random VIN
                var bites = new byte[17];
                random.NextBytes(bites);

                var vinBuilder = new StringBuilder(17);
                foreach (int bite in bites)
                {
                    vinBuilder.Append(bite);
                }

                var vin = vinBuilder.ToString(0, 17);
                vins.Add(vin);
            }

            return vins;
        }

        internal static string PickRandom(IList<string> strings)
        {
            // randomly pick one of the strings
            if (strings.Count == 0) return string.Empty;

            var random = new Random((int)DateTime.Now.Ticks ^ Thread.CurrentThread.ManagedThreadId);

            var index = random.Next(strings.Count);

            return strings[index];
        }
    }
}
