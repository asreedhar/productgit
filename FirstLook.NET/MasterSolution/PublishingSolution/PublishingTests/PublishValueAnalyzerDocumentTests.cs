﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using Moq;
using NUnit.Framework;
using PublishingDomain;
using PublishingDomain.Document;

namespace PublishingTests
{
    [TestFixture]
    public class PublishValueAnalyzerDocumentTests
    {
        private const string BUSINESS_UNIT_CODE = "ZBROWN01";
        private const string STOCK_NUM = "123";
        private const string VIN = "12345678901234567";

        private ILogger _nullLogger;
        private IInventoryDocumentRepository _nullRepository;

        [TestFixtureSetUp]
        public void Setup()
        {
            // A null logger
            var mockLogger = new Mock<ILogger>();
            mockLogger.Setup(l => l.Log(It.IsAny<Exception>()));
            mockLogger.Setup(l => l.Log(It.IsAny<string>(), It.IsAny<object>()));

            _nullLogger = mockLogger.Object;

            // a null repository
            var mockRepository = new Mock<IInventoryDocumentRepository>();
            mockRepository.Setup(r => r.Save(It.IsAny<IInventoryDocumentKey>(), It.IsAny<byte[]>()));
            mockRepository.Setup(
                r => r.GetDocument(
                    It.IsAny<IInventoryDocumentKey>())).
                Returns( 
                    default(IDocumentWrapper)
                    );
            _nullRepository = mockRepository.Object;

        }

        [Test]
        public void CanConstruct()
        {
            PublishValueAnalyzerDocumentCommand command = Command();
            Assert.IsNotNull(command);
        }

        [Test]
        public void CanPublish()
        {
            PublishValueAnalyzerDocumentCommand command = Command();

            AbstractCommand.DoRun( command );

            Assert.IsNotNull(command.PublishedUrl);
            Assert.IsNotNull(command.ShortUrl);

            Console.WriteLine(command.PublishedUrl);
            Console.WriteLine(command.ShortUrl);

            Assert.IsNotEmpty( command.PublishedUrl );
            Assert.IsNotEmpty( command.ShortUrl );

        }

        private PublishValueAnalyzerDocumentCommand Command()
        {
            return new PublishValueAnalyzerDocumentCommand(BUSINESS_UNIT_CODE,
                                                        VIN,
                                                        STOCK_NUM,
                                                        RandomHelpers.GetRandomBytes(100 * 1024),
                                                        _nullLogger,
                                                        _nullRepository);
        }

    }
}
