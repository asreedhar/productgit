﻿using Moq;
using NUnit.Framework;
using PublishingDomain;
using PublishingDomain.Document;

namespace PublishingTests
{
    [TestFixture]
    public class QueryStringMapperTests
    {
        [Test]
        public void CreateShortQueryString_should_return_query_string_with_just_vin_and_doc_type()
        {
            var stubDocumentKey = new Mock<IInventoryDocumentKey>();
            stubDocumentKey.Setup(k => k.VIN).Returns("WBAPK7C58BA000007");
            stubDocumentKey.Setup(k => k.DocumentType).Returns(DocumentTypes.ValueAnalyzer);

            var queryString = InventoryDocumentKeyQueryStringMapper.CreateShortQueryString(stubDocumentKey.Object);

            Assert.AreEqual("vin=WBAPK7C58BA000007&doctype=1", queryString);
        }
    }
}
