﻿using System;
using System.Threading;
using System.IO;
using NUnit.Framework;
using PublishingDomain;
using PublishingDomain.Document;
using PublishingDomain.FileSystem;


namespace PublishingTests
{
    [TestFixture]
    public class InventoryDocumentRepositoryTests
    {
        private string _vin;
        private string _businessUnitCode;
        private string _stockNumber;

        private IInventoryDocumentKey _key;
        private string _tempFolderPath;
        private InventoryDocumentRepository _repository;


        [TestFixtureSetUp]
        public void InventoryDocumentRepositoryTestsSetup()
        {
            _vin = "12345678901234567";
            _businessUnitCode = "BROWN001";
            _stockNumber = "12345";

            _key = InventoryDocumentKeyFactory.Create(_businessUnitCode, _vin, _stockNumber, DocumentTypes.ValueAnalyzer);

            // Create a temp folder
            string randomFolderName = DateTime.Now.Ticks.ToString();
            _tempFolderPath = Directory.CreateDirectory(Path.Combine("C:\\", randomFolderName)).FullName;

            // Our repository will use the new folder.
            _repository = new InventoryDocumentRepository(_tempFolderPath);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            // Delete the temp folder we created.
            Directory.Delete( _tempFolderPath, true );
        }

        [Test]
        public void CanConstruct()
        {
            Assert.IsNotNull(_repository);
        }

        [Test]
        public void ExistsReturnsFalse()
        {
            var repository = new InventoryDocumentRepository("C:\\DOES_NOT_EXIST");
            Assert.IsFalse(repository.Exists(_key));
        }


        [Test]
        public void SaveWritesExpectedBytesToDisk()
        {
            var b = RandomHelpers.GetRandomBytes(1024 * 100); // 100 KB
            var filePath = _repository.SaveToFileSystem(_key, b);
            Console.WriteLine(filePath);

            Assert.IsNotEmpty(filePath);
            Assert.IsTrue(File.Exists(filePath));
            Assert.IsTrue(_repository.Exists(_key));

            var fi = new FileInfo(filePath);
            Assert.IsNotNull(fi.Directory);
            Assert.AreEqual(1, fi.Directory.GetFiles().Length);

            var bites = File.ReadAllBytes(fi.FullName);

            Assert.AreEqual(b, bites);
        }

        [Test]
        public void SaveOverWritesExistingFile()
        {
            var b1 = RandomHelpers.GetRandomBytes(1024 * 100); // 100 KB
            var filePath = _repository.SaveToFileSystem(_key, b1);
            Console.WriteLine(filePath);

            var fi = new FileInfo(filePath);
            var firstWriteTime = fi.LastWriteTime.Ticks;

            Assert.AreEqual(b1.Length, fi.Length);
            Assert.AreEqual(b1, File.ReadAllBytes(fi.FullName));

            // wait 
            Thread.Sleep(100);

            // overwrite the file
            var b2 = RandomHelpers.GetRandomBytes(1024 * 200); // 200 KB
            filePath = _repository.SaveToFileSystem(_key, b2);
            fi = new FileInfo(filePath);

            var secondWriteTime = fi.LastWriteTime.Ticks;

            Assert.IsTrue(secondWriteTime > firstWriteTime);
            Assert.AreEqual(b2.Length, fi.Length);
            Assert.AreEqual(b2, File.ReadAllBytes(fi.FullName));
        }


        [Test]
        public void GetDocumentReturnsWhatWasSaved()
        {
            var b = RandomHelpers.GetRandomBytes(1024 * 100); // 100 KB
            _repository.Save(_key, b);

            var wrapper = _repository.GetDocument(_key);

            Assert.IsNotNull(wrapper);

            var bites = wrapper.DocumentSource;

            Assert.AreEqual(b.Length, bites.Length);

            for (int i = 0; i < b.Length; i++)
            {
                int original = b[i];
                int cached = bites[i];

                Assert.AreEqual(original, cached);
            }

            Assert.AreEqual("application/pdf", wrapper.MimeType);

            string name = _key.Encode() + ".pdf";
            Assert.AreEqual(name, wrapper.Name);
        }

    }

}
