using System;
using System.Configuration;
using System.Globalization;
using System.Web;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using PublishingDomain;
using PublishingDomain.Document;
using PublishingDomain.FileSystem;

namespace PublishingWebApplication
{
    public class InventoryDocumentHandler : IHttpHandler
    {
        // Replace with another logger if needed.
        private ILogger _logger = new HealthMonitoringWrapper();
        
        private const string ErrorPageUrl = "~/Pages/PdfNotFound.htm";
        
        public void ProcessRequest(HttpContext context)
        {
            if (InventoryDocumentKeyQueryStringMapper.IsVinOnly(context.Request.QueryString))
            {
                ProcessPDFUrlRequest(context);
            }
            else if (InventoryDocumentKeyQueryStringMapper.IsVinAndDocType(context.Request.QueryString))
            {
                ProcessVinAndDocType(context);
            }
            else
            {
               ProcessPDF(context);
            }
        }

        private void ProcessVinAndDocType(HttpContext context)
        {
            SendPdfResponse(context, ctx => InventoryDocumentKeyQueryStringMapper.FromVinAndDocType(ctx.Request.QueryString));
        }

        private void ProcessPDF(HttpContext context)
        {
            SendPdfResponse(context, ctx => InventoryDocumentKeyQueryStringMapper.Create(ctx.Request.QueryString));
        }

        private void SendPdfResponse(HttpContext context, Func<HttpContext, IInventoryDocumentKey> getDocumentKey)
        {
            try
            {
                var key = getDocumentKey(context);

                // Replace with another repository if needed.
                var amzRepository = new AmazonInventoryDocumentRepository();
                IDocumentWrapper documentWrapper = null;
                if (!amzRepository.Exists(key, out documentWrapper))
                {
                    var oldRepository = new InventoryDocumentRepository();
                    documentWrapper = oldRepository.GetDocument(key);
                }

                // Add the document to the response.
                context.Response.Clear();
                context.Response.ContentType = documentWrapper.MimeType;
                context.Response.AddHeader("Cache-Control", "must-revalidate");
                context.Response.AddHeader("Content-Disposition", 
                    string.Format("inline;filename=\"{0}\"", documentWrapper.Name));
                context.Response.AppendHeader("Content-Length", documentWrapper.DocumentSource.Length.ToString(CultureInfo.InvariantCulture));
                context.Response.BinaryWrite(documentWrapper.DocumentSource);
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
                RespondError(context);
            }
            finally
            {
                // can't do this in the try block because it throws a ThreadAbortedException
                context.Response.End();
            }
        }

        private void ProcessPDFUrlRequest(HttpContext context)
        {
            try
            {
                IInventoryDocumentKey key = InventoryDocumentKeyQueryStringMapper.FromVin(context.Request.QueryString);
                var queryString = InventoryDocumentKeyQueryStringMapper.CreateQueryString(key);

                // Replace with another repository if needed.
                var amazonRepository = new AmazonInventoryDocumentRepository();
                if (!amazonRepository.Exists(key))
                {
                    var repository = new InventoryDocumentRepository();
                    if (!repository.Exists(key))
                        throw new InvalidOperationException("No document.");
                }
                
                var url = ConfigurationManager.AppSettings["ValueAnalyzerUrlPrefix"] + "?" + queryString;
                string response = string.Format("MAXPublishPage.WritePdfLink(\"{0}\")", url);

                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.Write(response);
            }
            catch (Exception e)
            {
                _logger.Log(e);
                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.Write(String.Empty);
            }
            finally
            {
                context.Response.End();
            }
        }

        private static void RespondError(HttpContext context)
        {
            context.Response.Redirect(
                VirtualPathUtility.ToAbsolute(ErrorPageUrl));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
