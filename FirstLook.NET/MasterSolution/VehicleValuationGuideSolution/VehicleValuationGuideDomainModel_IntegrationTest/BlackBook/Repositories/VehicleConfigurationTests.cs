﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Repositories
{
    /// <summary>
    /// BlackBook vehicle configuration loading and saving tests.
    /// </summary>
    [TestFixture]
    public class VehicleConfigurationTests
    {
        ///<summary>
        /// Initialize the registry with the required components.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<DomainModel.Module>();
            registry.Register<Client.DomainModel.Clients.Module>();
            registry.Register<Client.DomainModel.Vehicles.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<ICache, MemoryCache>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Test the saving of a number of publications, and the loading of the list of details on those publications.
        /// </summary>
        [Test]
        public void PublicationListSaveAndLoadTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            State state = Utility.RandomState();

            Car car = Utility.RandomCar();
            BookDate bookDate = Utility.BookDate();
            repo.Save_Car(car, state.Code, bookDate.Id);

            VehicleConfiguration configuration1 = Utility.RandomVehicleConfiguration();
            configuration1.State = state;
            configuration1.Uvc = car.Uvc;
            configuration1.Vin = car.Vin;

            VehicleConfiguration configuration2 = Utility.RandomVehicleConfiguration();
            configuration2.State = state;
            configuration2.Uvc = car.Uvc;
            configuration2.Vin = car.Vin;

            VehicleConfiguration configuration3 = Utility.RandomVehicleConfiguration();
            configuration3.State = state;
            configuration3.Uvc = car.Uvc;
            configuration3.Vin = car.Vin;

            Matrix matrix1 = Utility.RandomMatrix();
            Matrix matrix2 = Utility.RandomMatrix();
            Matrix matrix3 = Utility.RandomMatrix();

            IBroker broker = new DealerRepository().Broker(100590);
            VehicleIdentification vehicle = new VehicleRepository().Identification(configuration1.Vin);
            ClientVehicleIdentification clientVehicle = new VehicleRepository().Identification(broker, vehicle);

            IPrincipal principal = new DealerRepository().Principal(new Guid("03C286ED-D55A-4981-B42C-38E60BD07536"));

            IEdition<IPublication> publication1 = repo.Save(broker, principal, clientVehicle, configuration1, matrix1, bookDate.Id);            
            IEdition<IPublication> publication2 = repo.Save(broker, principal, clientVehicle, configuration2, matrix2, bookDate.Id);            
            IEdition<IPublication> publication3 = repo.Save(broker, principal, clientVehicle, configuration3, matrix3, bookDate.Id);                        

            IEdition<IPublication> publication4 = repo.Load(broker, clientVehicle, publication1.Data.VehicleConfigurationHistoryId);
            IEdition<IPublication> publication5 = repo.Load(broker, clientVehicle, publication2.Data.VehicleConfigurationHistoryId);
            IEdition<IPublication> publication6 = repo.Load(broker, clientVehicle, publication3.Data.VehicleConfigurationHistoryId);
            
            IList<IEdition<IPublication>> publications = new List<IEdition<IPublication>> { publication4, publication5, publication6 };
            
            IList<IEdition<IPublicationInfo>> publicationInfos = repo.Load(broker, clientVehicle);

            Assert.AreEqual(3, publicationInfos.Count);

            foreach (IEdition<IPublicationInfo> info in publicationInfos)
            {
                bool found = false;

                foreach (IEdition<IPublication> publication in publications)
                {                    
                    if (info.Data.VehicleConfigurationHistoryId == publication.Data.VehicleConfigurationHistoryId)
                    {
                        found = true;
                        
                        Assert.AreEqual(info.DateRange.BeginDate, publication.DateRange.BeginDate);
                        Assert.AreEqual(info.DateRange.EndDate, publication.DateRange.EndDate);

                        Assert.AreEqual(info.User.Id, publication.User.Id);
                        Assert.AreEqual(info.User.FirstName, publication.User.FirstName);
                        Assert.AreEqual(info.User.LastName, publication.User.LastName);
                        Assert.AreEqual(info.User.UserName, publication.User.UserName);

                        Assert.AreEqual(info.Data.ChangeAgent, publication.Data.ChangeAgent);
                        Assert.AreEqual(info.Data.ChangeType, publication.Data.ChangeType);                        
                    }                    
                }
                if (!found)
                {
                    Assert.Fail("Couldn't find publication.");
                }
            }            
        }

        /// <summary>
        /// Test the saving and loading of publications.
        /// </summary>
        [Test]
        public void PublicationSaveAndLoadTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            State state = Utility.RandomState();

            Car car = Utility.RandomCar();
            BookDate bookDate = Utility.BookDate();
            repo.Save_Car(car, state.Code, bookDate.Id);

            VehicleConfiguration configuration = Utility.RandomVehicleConfiguration();
            configuration.State = state;
            configuration.Uvc = car.Uvc;
            configuration.Vin = car.Vin;

            Matrix matrix = Utility.RandomMatrix();

            IBroker broker = new DealerRepository().Broker(100590);
            VehicleIdentification vehicle = new VehicleRepository().Identification(configuration.Vin);
            ClientVehicleIdentification clientVehicle = new VehicleRepository().Identification(broker, vehicle);

            IPrincipal principal = new DealerRepository().Principal(new Guid("03C286ED-D55A-4981-B42C-38E60BD07536"));

            IEdition<IPublication> publication1 = repo.Save(broker, principal, clientVehicle, configuration, matrix, bookDate.Id);
            IEdition<IPublication> publication2 = repo.Load(broker, clientVehicle, publication1.Data.VehicleConfigurationHistoryId);

            Utility.Compare(publication1, publication2);
        }

        /// <summary>
        /// Test the saving and loading of publications when the loading is done by date.
        /// </summary>
        [Test]
        public void PublicationSaveAndLoadByDateTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            State state = Utility.RandomState();

            Car car = Utility.RandomCar();
            BookDate bookDate = Utility.BookDate();
            repo.Save_Car(car, state.Code, bookDate.Id);

            VehicleConfiguration configuration = Utility.RandomVehicleConfiguration();
            configuration.State = state;
            configuration.Uvc = car.Uvc;
            configuration.Vin = car.Vin;

            Matrix matrix = Utility.RandomMatrix();

            IBroker broker = new DealerRepository().Broker(100590);
            VehicleIdentification vehicle = new VehicleRepository().Identification(configuration.Vin);
            ClientVehicleIdentification clientVehicle = new VehicleRepository().Identification(broker, vehicle);

            IPrincipal principal = new DealerRepository().Principal(new Guid("03C286ED-D55A-4981-B42C-38E60BD07536"));

            IEdition<IPublication> publication1 = repo.Save(broker, principal, clientVehicle, configuration, matrix, bookDate.Id);

            DateTime marker = DateTime.Now;
            IEdition<IPublication> publication2 = repo.Load(broker, clientVehicle, marker);

            IEdition<IPublication> publication3 = repo.Save(broker, principal, clientVehicle, configuration, matrix, bookDate.Id);
            IEdition<IPublication> publication4 = repo.Load(broker, clientVehicle, marker);
            
            IEdition<IPublication> publication5 = repo.Load(broker, clientVehicle, DateTime.Now);

            Utility.Compare(publication1, publication2);            
            Utility.Compare(publication2, publication4, true);
            Utility.Compare(publication3, publication5);
        }

        /// <summary>
        /// Test the saving and loading of a vehicle configuration.
        /// </summary>
        [Test]
        public void SaveAndLoadTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();
            
            State state = Utility.RandomState();            

            Car car = Utility.RandomCar();
            BookDate bookDate = Utility.BookDate();
            repo.Save_Car(car, state.Code, bookDate.Id);

            VehicleConfiguration configuration = Utility.RandomVehicleConfiguration();
            configuration.State = state;
            configuration.Uvc = car.Uvc;
            configuration.Vin = car.Vin;

            IBroker broker = new DealerRepository().Broker(100590);
            VehicleIdentification vehicle = new VehicleRepository().Identification(configuration.Vin);
            ClientVehicleIdentification clientVehicle = new VehicleRepository().Identification(broker, vehicle);

            VehicleConfigurationEntity entity1 = repo.Save_Configuration(broker, clientVehicle, configuration, bookDate.Id);
            VehicleConfigurationEntity entity2 = repo.Load_Configuration(configuration.Uvc, configuration.Vin,
                                                                         configuration.State.Code, configuration.Mileage);            

            Utility.Compare(entity1, entity2);
        }                            
    }
}
