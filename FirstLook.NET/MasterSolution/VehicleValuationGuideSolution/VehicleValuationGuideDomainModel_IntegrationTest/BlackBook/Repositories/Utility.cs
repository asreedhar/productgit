﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Repositories
{
    /// <summary>
    /// Utility functions for BlackBook repository testing.
    /// </summary>
    public static class Utility
    {
        #region Test Data

        /// <summary>
        /// Get the current book date, inserting one into the database if none exist.
        /// </summary>
        /// <returns>Current book date.</returns>
        internal static BlackBook.Model.BookDate BookDate()
        {
            return new BlackBook.Model.BookDate { Id = 2011071400 }; // Temp
        }

        /// <summary>
        /// Utility random number generator.
        /// </summary>
        internal static Random Random = new Random();

        /// <summary>
        /// List of two letter US state identifiers.
        /// </summary>
        internal static List<string> States = new List<string>                                                  
        {
            "",   "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA",
            "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM",
            "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA",
            "WV", "WI", "WY", "PR", "DC"                                                      
        };

        /// <summary>
        /// Helper function for generating either an integer that will be null 1/7th of the time.
        /// </summary>        
        /// <returns>Integer, or null value.</returns>
        internal static int? RandOrNull()
        {
            int rand = Random.Next();
            return rand % 7 == 0 ? (int?)null : rand;
        }

        /// <summary>
        /// Helper list of alphanumerics.
        /// </summary>
        internal static string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        /// <summary>
        /// Get a string of random characters of the given length.
        /// </summary>
        /// <param name="length">Length of the string.</param>
        /// <returns>Random string.</returns>
        internal static string RandomString(int length)
        {
            return new string(Enumerable.Repeat(chars, length)
                             .Select(s => s[Random.Next(s.Length)])
                             .ToArray());
        }

        /// <summary>
        /// Returns a random string of random length.
        /// </summary>
        /// <returns>Random string.</returns>
        internal static string RandomString()
        {
            return RandomString(Random.Next(10) + 1);
        }

        /// <summary>
        /// Get a random (but valid) state object.        
        /// </summary>
        /// <returns>US State.</returns>
        internal static BlackBook.Model.State RandomState()
        {
            return new BlackBook.Model.State
            {
                Code = States[Random.Next(52)]
            };
        }

        /// <summary>
        /// Create a car that has been populated with random values.        
        /// </summary>
        /// <returns>New car with random values.</returns>
        internal static BlackBook.Model.Car RandomCar()
        {
            BlackBook.Model.Car car = new BlackBook.Model.Car();

            car.Year      = 1970 + Random.Next(40);
            car.Make      = RandomString();
            car.Model     = RandomString();
            car.Series    = RandomString();
            car.BodyStyle = RandomString();
            car.Vin       = RandomString(17);
            car.Uvc       = RandomString(10);
            car.Msrp      = RandOrNull();
            car.Mileage   = RandOrNull();

            car.FinanceAdvance = RandOrNull();

            car.ResidualValue = new BlackBook.Model.ResidualValue
            {
                Resid12 = RandOrNull(),
                Resid24 = RandOrNull(),
                Resid30 = RandOrNull(),
                Resid36 = RandOrNull(),
                Resid42 = RandOrNull(),
                Resid48 = RandOrNull(),
                Resid60 = RandOrNull(),
                Resid72 = RandOrNull()
            };

            car.TradeInValue = new BlackBook.Model.Value
            {
                ExtraClean = RandOrNull(),
                Clean      = RandOrNull(),
                Average    = RandOrNull(),
                Rough      = RandOrNull()
            };

            car.WholesaleValue = new BlackBook.Model.Value
            {
                ExtraClean = RandOrNull(),
                Clean      = RandOrNull(),
                Average    = RandOrNull(),
                Rough      = RandOrNull()
            };

            car.RetailValue = new BlackBook.Model.Value
            {
                ExtraClean = RandOrNull(),
                Clean      = RandOrNull(),
                Average    = RandOrNull(),
                Rough      = RandOrNull()
            };

            int mileageCount = Random.Next(5);

            for (int i = 0; i < mileageCount; i++)
            {
                int beginRange = Random.Next(999999);

                car.MileageAdjustments.Add(new BlackBook.Model.MileageAdjustment
                {
                    BeginRange     = beginRange,
                    EndRange       = beginRange + Random.Next(9999),
                    FinanceAdvance = RandOrNull(),
                    ExtraClean     = RandOrNull(),
                    Clean          = RandOrNull(),
                    Average        = RandOrNull(),
                    Rough          = RandOrNull()
                });
            }

            car.PriceIncludes = RandomString();

            int optionCount = Random.Next(5);

            Dictionary<string, int> adCodes = new Dictionary<string, int>();
            for (int i = 0; i < optionCount; i++)
            {       
                string adCode;
                do
                {
                    adCode = RandomString(2);
                } while (adCodes.ContainsKey(adCode));
                adCodes[adCode] = 1;

                car.Options.Add(new BlackBook.Model.Option
                {
                    AdCode      = adCode,
                    Description = RandomString(),
                    OptionAdjustmentSign = (BlackBook.Model.OptionAdjustmentSign)(Random.Next(2) == 0 ? 1 : 2),
                    Amount      = RandOrNull(),
                    Flag        = Random.Next(2) == 1 ? true : false,
                    Residual = new BlackBook.Model.ResidualValue
                    {
                        Resid12 = RandOrNull(),
                        Resid24 = RandOrNull(),
                        Resid30 = RandOrNull(),
                        Resid36 = RandOrNull(),
                        Resid42 = RandOrNull(),
                        Resid48 = RandOrNull(),
                        Resid60 = RandOrNull(),
                        Resid72 = RandOrNull()
                    }
                });
            }

            car.WheelBase         = RandomString();
            car.TaxableHorsepower = RandomString();
            car.Weight            = RandomString();
            car.TireSize          = RandomString();
            car.BaseHorsepower    = RandomString();
            car.FuelType          = RandomString();
            car.Cylinders         = RandomString();
            car.DriveTrain        = RandomString();
            car.Transmission      = RandomString();
            car.Engine            = RandomString();
            car.VehicleClass      = RandomString();

            return car;
        }

        /// <summary>
        /// Create a standard equipment object that has been populated with random values.        
        /// </summary>
        /// <returns>New standard equipment with random values.</returns>
        public static BlackBook.Model.StandardEquipment RandomEquipment()
        {
            BlackBook.Model.StandardEquipment equipment = new BlackBook.Model.StandardEquipment();

            int categoryCount = Random.Next(5);
            for (int i = 0; i < categoryCount; i++)
            {
                string category = RandomString();

                equipment.Categories.Add(category);

                int subCategoryCount = categoryCount + Random.Next(3);

                for (int j = 0; j < subCategoryCount; j++)
                {
                    equipment.StandardEquipmentRecords.Add(new BlackBook.Model.StandardEquipmentRecord
                    {
                        Category    = category,
                        Subcategory = RandomString(),
                        Value       = RandomString()
                    });
                }
            }

            return equipment;
        }

        /// <summary>
        /// Create a price matrix that has been populated with random values.
        /// </summary>
        /// <returns>New matrix with random values.</returns>
        internal static BlackBook.Model.Api.Matrix RandomMatrix()
        {
            BlackBook.Model.Api.Matrix matrix = new BlackBook.Model.Api.Matrix();

            foreach (BlackBook.Model.Api.Market market in Enum.GetValues(typeof(BlackBook.Model.Api.Market)))
            {
                foreach (BlackBook.Model.Api.Condition condition in Enum.GetValues(typeof(BlackBook.Model.Api.Condition)))
                {
                    foreach (BlackBook.Model.Api.Valuation valuation in Enum.GetValues(typeof(BlackBook.Model.Api.Valuation)))
                    {
                        BlackBook.Model.Api.Matrix.Cell cell = matrix[market, condition, valuation];
                        cell.Value = RandOrNull();

                        if (market    == BlackBook.Model.Api.Market.Undefined || 
                            condition == BlackBook.Model.Api.Condition.Undefined || 
                            valuation == BlackBook.Model.Api.Valuation.Undefined)
                        {
                            cell.Visibility = BlackBook.Model.Api.Matrix.CellVisibility.NotDisplayed;
                        }
                        else
                        {
                            cell.Visibility = BlackBook.Model.Api.Matrix.CellVisibility.Value;
                        }
                    }
                }
            }

            return matrix;
        }        

        /// <summary>
        /// Create a vehicle configuration that has been populated with random values.
        /// </summary>
        /// <returns>New vehicle configuration with random values.</returns>
        internal static BlackBook.Model.Api.VehicleConfiguration RandomVehicleConfiguration()
        {
            string vin = RandomString(9);
            string uvc = RandomString(10);
            int? mileage = RandOrNull();
            BlackBook.Model.State state = RandomState();

            BlackBook.Model.Api.VehicleConfiguration configuration = 
                new BlackBook.Model.Api.VehicleConfiguration(uvc, state, vin, mileage, BookDate());

            int optionCount = Random.Next(5);

            for (int i = 0; i < optionCount; i++)
            {
                BlackBook.Model.Api.OptionAction action = new BlackBook.Model.Api.OptionAction();
                action.Uoc = RandomString(4);
                action.ActionType = Random.Next(2) == 1 ? BlackBook.Model.Api.OptionActionType.Add : BlackBook.Model.Api.OptionActionType.Remove;
                
                configuration.OptionActions.Add(action);
            }

            optionCount = Random.Next(5);

            for (int i = 0; i < optionCount; i++)
            {
                BlackBook.Model.Api.OptionState optionState = new BlackBook.Model.Api.OptionState();

                optionState.Enabled = Random.Next(2) == 1 ? true : false;
                optionState.Selected = Random.Next(2) == 1 ? true : false;
                optionState.Uoc = RandomString(4);

                configuration.OptionStates.Add(optionState);
            }

            return configuration;
        }

        #endregion

        #region Comparers

        internal static void Compare(BlackBook.Model.Car car1, BlackBook.Model.Car car2)
        {            
            // Car / history.
            Assert.AreEqual(car1.Hash(), car2.Hash());
            Assert.AreEqual(car1.Uvc,    car2.Uvc);
            Assert.AreEqual(DomainModel.Utility.Model.Vin.Squish(car1.Vin),
                            DomainModel.Utility.Model.Vin.Squish(car2.Vin));

            // Attributes.
            Assert.AreEqual(car1.BaseHorsepower,    car2.BaseHorsepower);
            Assert.AreEqual(car1.Cylinders,         car2.Cylinders);
            Assert.AreEqual(car1.DriveTrain,        car2.DriveTrain);
            Assert.AreEqual(car1.Engine,            car2.Engine);
            Assert.AreEqual(car1.FuelType,          car2.FuelType);
            Assert.AreEqual(car1.TaxableHorsepower, car2.TaxableHorsepower);
            Assert.AreEqual(car1.WheelBase,         car2.WheelBase);
            Assert.AreEqual(car1.Weight,            car2.Weight);
            Assert.AreEqual(car1.TireSize,          car2.TireSize);
            Assert.AreEqual(car1.Transmission,      car2.Transmission);
            Assert.AreEqual(car1.VehicleClass,      car2.VehicleClass);

            // Description.
            Assert.AreEqual(car1.BodyStyle, car2.BodyStyle);
            Assert.AreEqual(car1.Make,      car2.Make);
            Assert.AreEqual(car1.Model,     car2.Model);
            Assert.AreEqual(car1.Series,    car2.Series);
            Assert.AreEqual(car1.Year,      car2.Year);

            // Prices.
            Assert.AreEqual(car1.FinanceAdvance, car2.FinanceAdvance);
            Assert.AreEqual(car1.Mileage,        car2.Mileage);
            Assert.AreEqual(car1.Msrp,           car2.Msrp);
            Assert.AreEqual(car1.PriceIncludes,  car2.PriceIncludes);

            // Residual value.
            Assert.AreEqual(car1.ResidualValue.Resid12, car2.ResidualValue.Resid12);
            Assert.AreEqual(car1.ResidualValue.Resid24, car2.ResidualValue.Resid24);
            Assert.AreEqual(car1.ResidualValue.Resid30, car2.ResidualValue.Resid30);
            Assert.AreEqual(car1.ResidualValue.Resid36, car2.ResidualValue.Resid36);
            Assert.AreEqual(car1.ResidualValue.Resid42, car2.ResidualValue.Resid42);
            Assert.AreEqual(car1.ResidualValue.Resid48, car2.ResidualValue.Resid48);
            Assert.AreEqual(car1.ResidualValue.Resid60, car2.ResidualValue.Resid60);
            Assert.AreEqual(car1.ResidualValue.Resid72, car2.ResidualValue.Resid72);

            // Retail value.
            Assert.AreEqual(car1.RetailValue.Average,    car2.RetailValue.Average);
            Assert.AreEqual(car1.RetailValue.Clean,      car2.RetailValue.Clean);
            Assert.AreEqual(car1.RetailValue.ExtraClean, car2.RetailValue.ExtraClean);
            Assert.AreEqual(car1.RetailValue.Rough,      car2.RetailValue.Rough);

            // Trade-in value.
            Assert.AreEqual(car1.TradeInValue.Average,    car2.TradeInValue.Average);
            Assert.AreEqual(car1.TradeInValue.Clean,      car2.TradeInValue.Clean);
            Assert.AreEqual(car1.TradeInValue.ExtraClean, car2.TradeInValue.ExtraClean);
            Assert.AreEqual(car1.TradeInValue.Rough,      car2.TradeInValue.Rough);

            // Wholesale value.
            Assert.AreEqual(car1.WholesaleValue.Average,    car2.WholesaleValue.Average);
            Assert.AreEqual(car1.WholesaleValue.Clean,      car2.WholesaleValue.Clean);
            Assert.AreEqual(car1.WholesaleValue.ExtraClean, car2.WholesaleValue.ExtraClean);
            Assert.AreEqual(car1.WholesaleValue.Rough,      car2.WholesaleValue.Rough);

            // Mileage adjustments.
            foreach (BlackBook.Model.MileageAdjustment adjustment1 in car1.MileageAdjustments)
            {
                bool found = false;

                foreach (BlackBook.Model.MileageAdjustment adjustment2 in car2.MileageAdjustments)
                {
                    if (adjustment1.BeginRange == adjustment2.BeginRange)
                    {
                        Assert.AreEqual(adjustment1.Average,        adjustment2.Average);
                        Assert.AreEqual(adjustment1.BeginRange,     adjustment2.BeginRange);
                        Assert.AreEqual(adjustment1.Clean,          adjustment2.Clean);
                        Assert.AreEqual(adjustment1.EndRange,       adjustment2.EndRange);
                        Assert.AreEqual(adjustment1.ExtraClean,     adjustment2.ExtraClean);
                        Assert.AreEqual(adjustment1.FinanceAdvance, adjustment2.FinanceAdvance);

                        found = true;
                    }
                }
                if (!found)
                {
                    Assert.Fail("Could not match mileage adjustments.");
                }
            }

            // Options.
            foreach (BlackBook.Model.Option option1 in car1.Options)
            {
                bool found = false;

                foreach (BlackBook.Model.Option option2 in car2.Options)
                {
                    if (option1.UniversalOptionCode == option2.UniversalOptionCode)
                    {
                        Assert.AreEqual(option1.AdCode.Trim(),        option2.AdCode.Trim());
                        Assert.AreEqual(option1.Amount,               option2.Amount);
                        Assert.AreEqual(option1.Description,          option2.Description);
                        Assert.AreEqual(option1.Flag,                 option2.Flag);
                        Assert.AreEqual(option1.OptionAdjustmentSign, option2.OptionAdjustmentSign);

                        Assert.AreEqual(option1.Residual.Resid12, option2.Residual.Resid12);
                        Assert.AreEqual(option1.Residual.Resid24, option2.Residual.Resid24);
                        Assert.AreEqual(option1.Residual.Resid30, option2.Residual.Resid30);
                        Assert.AreEqual(option1.Residual.Resid36, option2.Residual.Resid36);
                        Assert.AreEqual(option1.Residual.Resid42, option2.Residual.Resid42);
                        Assert.AreEqual(option1.Residual.Resid48, option2.Residual.Resid48);
                        Assert.AreEqual(option1.Residual.Resid60, option2.Residual.Resid60);
                        Assert.AreEqual(option1.Residual.Resid72, option2.Residual.Resid72);

                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    Assert.Fail("Could not match options.");
                }
            }
        }

        /// <summary>
        /// Compare the value of two car entities. Will fail an assertion if the two are not the same.
        /// </summary>
        /// <param name="entity1">First car entity to compare.</param>
        /// <param name="entity2">Second car entity to compare.</param>        
        internal static void Compare(CarEntity entity1, CarEntity entity2)
        {
            // Audit.
            Assert.AreEqual(entity1.AuditRow.Id,        entity2.AuditRow.Id);
            Assert.AreEqual(entity1.AuditRow.ValidFrom, entity2.AuditRow.ValidFrom);
            Assert.AreEqual(entity1.AuditRow.ValidUpTo, entity2.AuditRow.ValidUpTo);
            Assert.AreEqual(entity1.AuditRow.WasDelete, entity2.AuditRow.WasDelete);
            Assert.AreEqual(entity1.AuditRow.WasInsert, entity2.AuditRow.WasInsert);
            Assert.AreEqual(entity1.AuditRow.WasUpdate, entity2.AuditRow.WasUpdate);

            // Car / history.            
            Assert.AreEqual(entity1.HashCode,     entity2.HashCode);
            Assert.AreEqual(entity1.CarId,        entity2.CarId);
            Assert.AreEqual(entity1.CarHistoryId, entity2.CarHistoryId);
            
            // Car values.
            Compare(entity1.Car, entity2.Car);            
        }

        /// <summary>
        /// Compare two standard equipment entities.
        /// </summary>
        /// <param name="entity1">First entity to compare.</param>
        /// <param name="entity2">Second entity to compare.</param>        
        internal static void Compare(StandardEquipmentEntity entity1, StandardEquipmentEntity entity2)
        {
            Assert.AreEqual(entity1.AuditRow.Id,        entity2.AuditRow.Id);
            Assert.AreEqual(entity1.AuditRow.ValidFrom, entity2.AuditRow.ValidFrom);
            Assert.AreEqual(entity1.AuditRow.ValidUpTo, entity2.AuditRow.ValidUpTo);
            Assert.AreEqual(entity1.AuditRow.WasDelete, entity2.AuditRow.WasDelete);
            Assert.AreEqual(entity1.AuditRow.WasInsert, entity2.AuditRow.WasInsert);
            Assert.AreEqual(entity1.AuditRow.WasUpdate, entity2.AuditRow.WasUpdate);
            Assert.AreEqual(entity1.HashCode,           entity2.HashCode);
            Assert.AreEqual(entity1.HistoryId,          entity2.HistoryId);
            Assert.AreEqual(entity1.Id,                 entity2.Id);
            Assert.AreEqual(entity1.Uvc,                entity2.Uvc);

            Assert.AreEqual(entity1.StandardEquipment.Categories.Count,               entity2.StandardEquipment.Categories.Count);
            Assert.AreEqual(entity1.StandardEquipment.StandardEquipmentRecords.Count, entity2.StandardEquipment.StandardEquipmentRecords.Count);

            foreach (string category1 in entity1.StandardEquipment.Categories)
            {
                bool found = false;

                foreach (string category2 in entity2.StandardEquipment.Categories)
                {
                    if (category1.Equals(category2))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Assert.Fail("Could not match categories.");
                }
            }

            foreach (BlackBook.Model.StandardEquipmentRecord record1 in entity1.StandardEquipment.StandardEquipmentRecords)
            {
                bool found = false;

                foreach (BlackBook.Model.StandardEquipmentRecord record2 in entity2.StandardEquipment.StandardEquipmentRecords)
                {
                    if (record1.Category.Equals(record2.Category) &&
                        record1.Subcategory.Equals(record2.Subcategory) && 
                        record1.Value.Equals(record2.Value))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Assert.Fail("Could not match records.");
                }
            }
        }

        /// <summary>
        /// Compare two price matrices.
        /// </summary>
        /// <param name="matrix1">First matrix to compare.</param>
        /// <param name="matrix2">Second matrix to compare.</param>
        internal static void Compare(BlackBook.Model.Api.Matrix matrix1, BlackBook.Model.Api.Matrix matrix2)
        {
            foreach (BlackBook.Model.Api.Market market in Enum.GetValues(typeof(BlackBook.Model.Api.Market)))
            {
                if (market == BlackBook.Model.Api.Market.Undefined)
                {
                    continue;
                }

                foreach (BlackBook.Model.Api.Condition condition in Enum.GetValues(typeof(BlackBook.Model.Api.Condition)))
                {
                    if (condition == BlackBook.Model.Api.Condition.Undefined)
                    {
                        continue;
                    }

                    foreach (BlackBook.Model.Api.Valuation valuation in Enum.GetValues(typeof(BlackBook.Model.Api.Valuation)))
                    {
                        if (valuation == BlackBook.Model.Api.Valuation.Undefined)
                        {
                            continue;
                        }

                        BlackBook.Model.Api.Matrix.Cell cell1 = matrix1[market, condition, valuation];
                        BlackBook.Model.Api.Matrix.Cell cell2 = matrix2[market, condition, valuation];

                        Assert.AreEqual(cell1.Value,      cell2.Value);
                        Assert.AreEqual(cell1.Visibility, cell2.Visibility);
                    }
                }
            }
        }

        /// <summary>
        /// Compare two vehicle configurations.
        /// </summary>
        /// <param name="configuration1">First configuration to compare.</param>
        /// <param name="configuration2">Second configuration to compare.</param>
        internal static void Compare(BlackBook.Model.Api.IVehicleConfiguration configuration1, BlackBook.Model.Api.IVehicleConfiguration configuration2)
        {
            Assert.AreEqual(configuration1.Mileage, configuration2.Mileage);
            Assert.AreEqual(configuration1.State.Code, configuration2.State.Code);
            Assert.AreEqual(configuration1.Uvc, configuration2.Uvc);

            Assert.AreEqual(DomainModel.Utility.Model.Vin.Squish(configuration1.Vin),
                            DomainModel.Utility.Model.Vin.Squish(configuration2.Vin));

            Assert.AreEqual(configuration1.OptionActions.Count, configuration2.OptionActions.Count);

            for (int i = 0; i < configuration1.OptionActions.Count; i++)
            {
                Assert.AreEqual(configuration1.OptionActions[i].ActionType, configuration2.OptionActions[i].ActionType);
                Assert.AreEqual(configuration1.OptionActions[i].Uoc, configuration2.OptionActions[i].Uoc);
            }

            Assert.AreEqual(configuration1.OptionStates.Count, configuration2.OptionStates.Count);

            foreach (BlackBook.Model.Api.IOptionState optionState1 in configuration1.OptionStates)
            {
                bool found = false;

                foreach (BlackBook.Model.Api.IOptionState optionState2 in configuration2.OptionStates)
                {
                    if (optionState1.Uoc == optionState2.Uoc &&
                        optionState1.Enabled == optionState2.Enabled &&
                        optionState1.Selected == optionState2.Selected)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Assert.Fail("Could not match option states.");
                }
            }
        }

        /// <summary>
        /// Compare two vehicle configuration entities.
        /// </summary>
        /// <param name="entity1">First entity to compare.</param>
        /// <param name="entity2">Second entity to compare.</param>
        internal static void Compare(VehicleConfigurationEntity entity1, VehicleConfigurationEntity entity2)
        {            
            Assert.AreEqual(entity1.AuditRow.Id,        entity2.AuditRow.Id);
            Assert.AreEqual(entity1.AuditRow.ValidFrom, entity2.AuditRow.ValidFrom);
            Assert.AreEqual(entity1.AuditRow.ValidUpTo, entity2.AuditRow.ValidUpTo);
            Assert.AreEqual(entity1.AuditRow.WasDelete, entity2.AuditRow.WasDelete);
            Assert.AreEqual(entity1.AuditRow.WasInsert, entity2.AuditRow.WasInsert);
            Assert.AreEqual(entity1.AuditRow.WasUpdate, entity2.AuditRow.WasUpdate);
            
            Assert.AreEqual(entity1.CarHistoryId,                  entity2.CarHistoryId);
            Assert.AreEqual(entity1.VehicleConfigurationId,        entity2.VehicleConfigurationId);
            Assert.AreEqual(entity1.VehicleConfigurationHistoryId, entity2.VehicleConfigurationHistoryId);

            Assert.AreEqual(entity1.VehicleConfiguration.Mileage,    entity2.VehicleConfiguration.Mileage);
            Assert.AreEqual(entity1.VehicleConfiguration.State.Code, entity2.VehicleConfiguration.State.Code);
            Assert.AreEqual(entity1.VehicleConfiguration.Uvc,        entity2.VehicleConfiguration.Uvc);

            Assert.AreEqual(DomainModel.Utility.Model.Vin.Squish(entity1.VehicleConfiguration.Vin),
                            DomainModel.Utility.Model.Vin.Squish(entity2.VehicleConfiguration.Vin));

            Assert.AreEqual(entity1.VehicleConfiguration.OptionActions.Count, entity2.VehicleConfiguration.OptionActions.Count);

            for (int i = 0; i < entity1.VehicleConfiguration.OptionActions.Count; i++)
            {
                Assert.AreEqual(entity1.VehicleConfiguration.OptionActions[i].ActionType, entity2.VehicleConfiguration.OptionActions[i].ActionType);
                Assert.AreEqual(entity1.VehicleConfiguration.OptionActions[i].Uoc, entity2.VehicleConfiguration.OptionActions[i].Uoc);
            }

            Assert.AreEqual(entity1.VehicleConfiguration.OptionStates.Count, entity2.VehicleConfiguration.OptionStates.Count);

            foreach (BlackBook.Model.Api.IOptionState optionState1 in entity1.VehicleConfiguration.OptionStates)
            {
                bool found = false;

                foreach (BlackBook.Model.Api.IOptionState optionState2 in entity2.VehicleConfiguration.OptionStates)
                {
                    if (optionState1.Uoc      == optionState2.Uoc     &&
                        optionState1.Enabled  == optionState2.Enabled &&
                        optionState1.Selected == optionState2.Selected)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Assert.Fail("Could not match option states.");
                }
            }
        }

        /// <summary>
        /// Compare two publication editions.
        /// </summary>
        /// <param name="publication1">First publication to save.</param>
        /// <param name="publication2">Second publication to save.</param>        
        internal static void Compare(BlackBook.Model.Api.IEdition<BlackBook.Model.Api.IPublication> publication1, 
                                     BlackBook.Model.Api.IEdition<BlackBook.Model.Api.IPublication> publication2)
        {
            Compare(publication1, publication2, false);
        }

        /// <summary>
        /// Compare two publication editions.
        /// </summary>
        /// <param name="publication1">First publication to save.</param>
        /// <param name="publication2">Second publication to save.</param>
        /// <param name="ignoreEndDate">Should we ignore differences in the end valid date?</param>
        internal static void Compare(BlackBook.Model.Api.IEdition<BlackBook.Model.Api.IPublication> publication1, 
                                     BlackBook.Model.Api.IEdition<BlackBook.Model.Api.IPublication> publication2,
                                     bool ignoreEndDate)
        {
            Assert.AreEqual(publication1.DateRange.BeginDate, publication2.DateRange.BeginDate);
            if (!ignoreEndDate)
            {
                Assert.AreEqual(publication1.DateRange.EndDate,   publication2.DateRange.EndDate);
            }
                        
            Assert.AreEqual(publication1.User.Id,        publication2.User.Id);
            Assert.AreEqual(publication1.User.FirstName, publication2.User.FirstName);
            Assert.AreEqual(publication1.User.LastName,  publication2.User.LastName);
            Assert.AreEqual(publication1.User.UserName,  publication2.User.UserName);

            Assert.AreEqual(publication1.Data.ChangeAgent, publication2.Data.ChangeAgent);
            Assert.AreEqual(publication1.Data.ChangeType,  publication2.Data.ChangeType);
            
            Assert.AreEqual(publication1.Data.VehicleConfigurationHistoryId, publication2.Data.VehicleConfigurationHistoryId);
            
            Compare(publication1.Data.Car,                  publication2.Data.Car);
            Compare(publication1.Data.Matrix,               publication2.Data.Matrix);
            Compare(publication1.Data.VehicleConfiguration, publication2.Data.VehicleConfiguration);
        }

        #endregion
    }
}
