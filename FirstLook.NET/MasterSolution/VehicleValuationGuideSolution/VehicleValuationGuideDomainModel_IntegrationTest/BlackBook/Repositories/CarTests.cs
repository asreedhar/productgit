﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Repositories
{
    /// <summary>
    /// BlackBook car loading and saving tests.
    /// </summary>
    [TestFixture]
    public class CarTests
    {        
        /// <summary>
        /// Initialize the registry with the required components.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            
            registry.Register<DomainModel.Module>();
            registry.Register<Client.DomainModel.Clients.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);           
        }                                   

        /// <summary>
        /// Save a car with random values, load it back from the database, and ensure the values are the same.
        /// </summary>
        [Test]
        public void SaveAndLoadTest()
        {            
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            State state = Utility.RandomState(); 
            Car car = Utility.RandomCar();
            BookDate bookDate = Utility.BookDate();

            // Compare the load and save.
            CarEntity entity  = repo.Save_Car(car, state.Code, bookDate.Id);
            CarEntity entity2 = repo.Load_Car(car.Uvc, car.Vin, state.Code);
            Utility.Compare(entity, entity2);            
        }          

        /// <summary>
        /// Test that cache checking and inserting works.
        /// </summary>
        [Test]
        public void CacheTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            Car car = Utility.RandomCar();
            State state = Utility.RandomState();
            BookDate bookDate = Utility.BookDate();

            repo.Save_Car(car, state.Code, bookDate.Id);

            // No cache should be in the database.
            bool valid = repo.CarCache_IsValid(car.Uvc, car.Vin, state.Code);
            Assert.IsFalse(valid);

            // This should insert the cache.
            repo.CarCache_Extend(car.Uvc, car.Vin, state.Code);

            valid = repo.CarCache_IsValid(car.Uvc, car.Vin, state.Code);
            Assert.IsTrue(valid);
        }        

        /// <summary>
        /// Test that cache checking and inserting works when using car identifiers instead of values.
        /// </summary>
        [Test]
        public void CacheByIdTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            Car car = Utility.RandomCar();
            State state = Utility.RandomState();
            BookDate bookDate = Utility.BookDate();

            CarEntity entity = repo.Save_Car(car, state.Code, bookDate.Id);

            // No cache should be in the database.
            bool valid = repo.CarCache_IsValid(car.Uvc, car.Vin, state.Code);
            Assert.IsFalse(valid);

            // This should insert the cache.
            repo.CarCache_Extend(entity.CarId);

            valid = repo.CarCache_IsValid(car.Uvc, car.Vin, state.Code);
            Assert.IsTrue(valid);
        }

        /// <summary>
        /// Test that trying to extend the cache of a nonexisting vehicle throws an exception.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DataException))]
        public void CacheExceptionTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            Car car = Utility.RandomCar();
            State state = Utility.RandomState();

            // No cache should be in the database.
            bool valid = repo.CarCache_IsValid(car.Uvc, car.Vin, state.Code);
            Assert.IsFalse(valid);

            // This should throw the exception.
            repo.CarCache_Extend(car.Uvc, car.Vin, state.Code);
        }
    }
}
