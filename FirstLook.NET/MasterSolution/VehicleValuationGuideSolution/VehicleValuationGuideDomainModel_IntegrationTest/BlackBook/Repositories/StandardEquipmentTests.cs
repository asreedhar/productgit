﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Repositories
{
    /// <summary>
    /// BlackBook standard equipment loading and saving tests.
    /// </summary>
    [TestFixture]
    public class StandardEquipmentTests
    {
        /// Initialize the registry with the required components.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<DomainModel.Module>();
            registry.Register<Client.DomainModel.Clients.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
        }     

        /// <summary>
        /// Test the saving and loading of standard equipment.
        /// </summary>
        [Test]
        public void SaveAndLoadTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            StandardEquipment equipment = Utility.RandomEquipment();

            string uvc = Utility.RandomString(10);
            BookDate bookDate = Utility.BookDate();
            
            StandardEquipmentEntity entity1 = repo.Save_Equipment(uvc, equipment, bookDate.Id);
            StandardEquipmentEntity entity2 = repo.Load_Equipment(uvc);

            Utility.Compare(entity1, entity2);
        }

        /// <summary>
        /// Test the saving and loading of standard equipment.
        /// </summary>
        [Test]
        public void CacheTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            StandardEquipment equipment = Utility.RandomEquipment();

            string uvc = Utility.RandomString(10);
            BookDate bookDate = Utility.BookDate();
            repo.Save_Equipment(uvc, equipment, bookDate.Id);

            bool valid = repo.EquipmentCache_IsValid(uvc);
            Assert.IsFalse(valid);

            repo.EquipmentCache_Extend(uvc);

            valid = repo.EquipmentCache_IsValid(uvc);
            Assert.IsTrue(valid);
        }

        /// <summary>
        /// Test the saving and loading of standard equipment.
        /// </summary>
        [Test]
        public void CacheByIdTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            StandardEquipment equipment = Utility.RandomEquipment();

            string uvc = Utility.RandomString(10);
            BookDate bookDate = Utility.BookDate();
            StandardEquipmentEntity entity = repo.Save_Equipment(uvc, equipment, bookDate.Id);

            bool valid = repo.EquipmentCache_IsValid(uvc);
            Assert.IsFalse(valid);

            repo.EquipmentCache_Extend(entity.Id);

            valid = repo.EquipmentCache_IsValid(uvc);
            Assert.IsTrue(valid);
        }

        /// <summary>
        /// Test that trying to extend the cache of a nonexisting standard equipment throws an exception.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DataException))]
        public void CacheExceptionTest()
        {
            IBlackBookRepository repo = new BlackBook.Repositories.BlackBookRepository();

            string uvc = Utility.RandomString(10);

            bool valid = repo.EquipmentCache_IsValid(uvc);
            Assert.IsFalse(valid);

            repo.EquipmentCache_Extend(uvc);
        }
    }
}
