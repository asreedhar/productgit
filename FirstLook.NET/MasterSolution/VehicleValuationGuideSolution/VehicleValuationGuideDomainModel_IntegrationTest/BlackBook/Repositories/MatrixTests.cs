﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Repositories
{
    /// <summary>
    /// BlackBook value loading and saving tests.
    /// </summary>
    [TestFixture]
    public class MatrixTests
    {        
        /// <summary>
        /// Initialize the registry with the required components.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<DomainModel.Module>();
            registry.Register<Client.DomainModel.Clients.Module>();
            registry.Register<Client.DomainModel.Vehicles.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<ICache, MemoryCache>(ImplementationScope.Shared);
        }


        /// <summary>
        /// Test the saving and loading of a matrix of price values.
        /// </summary>
        [Test]
        public void SaveAndLoadTest()
        {
            IBlackBookRepository repo = new BlackBookRepository();

            Matrix matrix1 = Utility.RandomMatrix();

            BlackBook.Model.State state = Utility.RandomState();
            BlackBook.Model.BookDate bookDate = Utility.BookDate();

            CarEntity carEntity = repo.Save_Car(Utility.RandomCar(), state.Code, bookDate.Id);
            
            VehicleConfiguration configuration = Utility.RandomVehicleConfiguration();                        
            configuration.Uvc = carEntity.Car.Uvc;
            configuration.Vin = carEntity.Car.Vin;            
            configuration.State = state;

            IBroker broker = new DealerRepository().Broker(100590);
            VehicleIdentification vehicle = new VehicleRepository().Identification(configuration.Vin);                        
            ClientVehicleIdentification clientVehicle = new VehicleRepository().Identification(broker, vehicle);

            VehicleConfigurationEntity configEntity = repo.Save_Configuration(broker, clientVehicle, configuration, bookDate.Id);

            Matrix matrix2 = repo.Save_Matrix(carEntity.CarHistoryId, configEntity.VehicleConfigurationHistoryId, matrix1, bookDate.Id);
            Matrix matrix3 = repo.Load_Matrix(carEntity.CarHistoryId, configEntity.VehicleConfigurationHistoryId);

            Utility.Compare(matrix1, matrix2);
            Utility.Compare(matrix2, matrix3);                      
        }  
    }
}
