﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Helpers;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    [TestFixture]
    public class SqlSerializerTest
    {
        private ISqlService _service;
        private BookDate _bookDate;

        [SetUp]
        public void SetUp()
        {
            RegistryFactory.GetRegistry().Register<Module>();
            _service = RegistryFactory.GetResolver().Resolve<ISqlService>();
            
            ISqlSerializer<BookDate> serializer = RegistryFactory.GetResolver().Resolve<ISqlSerializer<BookDate>>();
            _bookDate = new BookDate {Id = serializer.Deserialize(_service.BookDate()).Max(x => x.Id) };
        }

        [TearDown]
        public void TearDown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        [Test]
        public void TestBookDate()
        {
            using (IDataReader reader = _service.BookDate())
            {
                Assert.IsNotNull(reader);

                IList<BookDate> values = new BookDateSerializer().Deserialize(reader);

                Assert.IsNotNull(values);

                Assert.Greater(values.Count, 0);
            }
        }

        [Test]
        public void TestYear()
        {
            using (IDataReader reader = _service.Query(_bookDate.Id))
            {
                Assert.IsNotNull(reader);

                IList<Year> values = new YearSerializer().Deserialize(reader);

                Assert.IsNotNull(values);

                Assert.Greater(values.Count, 0);
            }
        }

        [Test]
        public void TestMake()
        {
            using (IDataReader reader = _service.Query(_bookDate.Id, 2005))
            {
                Assert.IsNotNull(reader);

                IList<Make> values = new MakeSerializer().Deserialize(reader);

                Assert.IsNotNull(values);

                Assert.Greater(values.Count, 0);
            }
        }

        [Test]
        public void TestModel()
        {
            using (IDataReader reader = _service.Query(_bookDate.Id, 2005, 15)) // 2005 Ford
            {
                Assert.IsNotNull(reader);

                IList<Model> values = new ModelSerializer().Deserialize(reader);

                Assert.IsNotNull(values);

                Assert.Greater(values.Count, 0);
            }
        }

        [Test]
        public void TestTrim()
        {
            using (IDataReader reader = _service.Query(_bookDate.Id, 2005, 15, 841)) // 2005 Ford F150 Regular Cab
            {
                Assert.IsNotNull(reader);

                IList<Trim> values = new TrimSerializer().Deserialize(reader);

                Assert.IsNotNull(values);

                Assert.Greater(values.Count, 0);
            }
        }


        // Also test VehicleOption & Specification
        [Test]
        public void TestVehicle()
        {
            using (IDataReader reader = _service.Vehicle(_bookDate.Id, (int)TestCar.FordF150)) // 2005 Ford F150 Regular Cab XL Pickup 4D 6 1/2 ft
            {
                Assert.IsNotNull(reader);

                Vehicle vehicle = new VehicleSerializer().DeserializeGraph(reader);
                
                Assert.IsNotNull(vehicle);
            }
        }

        [Test]
        public void TestBasePrice()
        {
            using (IDataReader reader = _service.BasePrices(_bookDate.Id, new Region { Id = (int)Helpers.Region.Illinois }, (int)TestCar.FordF150)) // 2005 Ford F150 Regular Cab XL Pickup 4D 6 1/2 ft
            {
                Assert.IsNotNull(reader);

                IList<Price> prices = new PriceSerializer().Deserialize(reader);

                Assert.IsNotNull(prices);
            }
        }

        [Test]
        public void TestMileageAdjustment()
        {
            using (IDataReader reader = _service.MileageAdjustment(_bookDate.Id, VehicleType.UsedCar, 2005, 17)) // 2005 Ford F150 Regular Cab XL Pickup 4D 6 1/2 ft
            {
                Assert.IsNotNull(reader);

                IList<MileageAdjustment> adjustments = new MileageAdjustmentSerializer().Deserialize(reader);

                Assert.IsNotNull(adjustments);
            }
        }

        [Test]
        public void TestOptionAdjustments()
        {
            using (IDataReader reader = _service.OptionAdjustments(_bookDate.Id, (int)TestCar.FordF150)) // 2005 Ford F150 Regular Cab XL Pickup 4D 6 1/2 ft
            {
                Assert.IsNotNull(reader);

                IList<VehicleOptionAdjustment> adjustments = new VehicleOptionAdjustmentSerializer().Deserialize(reader);

                Assert.IsNotNull(adjustments);
            }
        }

        [Test]
        public void TestRegion()
        {
            using (IDataReader reader = _service.Regions(_bookDate.Id))
            {
                Assert.IsNotNull(reader);

                IList<Region> regions = new RegionSerializer().Deserialize(reader);

                Assert.IsNotNull(regions);

                Assert.Greater(regions.Count, 0);
            }
        }

        [Test]
        public void TestRegionAdjustment()
        {
            using (IDataReader reader = _service.RegionAdjustments(_bookDate.Id, (int)TestCar.FordF150))
            {
                Assert.IsNotNull(reader);

                IList<RegionAdjustment> regionAdjustments = new RegionAdjustmentSerializer().Deserialize(reader);

                Assert.IsNotNull(regionAdjustments);

                Assert.Greater(regionAdjustments.Count, 0);
            }
        }
    }
}
