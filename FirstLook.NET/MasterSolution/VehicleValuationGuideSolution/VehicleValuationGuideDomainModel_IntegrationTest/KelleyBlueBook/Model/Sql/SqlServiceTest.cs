﻿using System.Data;
using System.Linq;
using NUnit.Framework;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Helpers;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    [TestFixture]
    public class SqlServiceTest
    {
        private ISqlService _service;
        private BookDate _bookDate;

        [TestFixtureSetUp]
        public void Setup()
        {
            _service = new CurrentSqlService();
            ISqlSerializer<BookDate> serializer = new BookDateSerializer();
            _bookDate = new BookDate { Id = serializer.Deserialize(_service.BookDate()).Max(x => x.Id) };
        }

        [Test]
        public void TestBookDate()
        {
            IDataReader reader = _service.BookDate();

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Book Dates");
        }

        [Test]
        public void TestYear()
        {            
            IDataReader reader = _service.Query(_bookDate.Id);

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Years");
        }

        [Test]
        public void TestMake()
        {            
            IDataReader reader = _service.Query(_bookDate.Id, 2005);

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Makes");
        }

        [Test]
        public void TestModel()
        {
            IDataReader reader = _service.Query(_bookDate.Id, 2005, 15); // 2005 Ford

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Models");
        }

        [Test]
        public void TestTrim()
        {
            IDataReader reader = _service.Query(_bookDate.Id, 2005, 15, 841); // 2005 Ford F150 Regular Cab

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Trims");
        }

        [Test]
        public void TestVehicle()
        {
            IDataReader reader = _service.Vehicle(_bookDate.Id, (int)TestCar.FordF150); // 2005 Ford F150 Regular Cab XL Pickup 4D 6 1/2 ft

            Assert.IsNotNull(reader);

            string[] names = new[] {"Year", "Make", "Model", "Trim", "Vehicle", "Options", "Relations"};

            for (int i = 0; i < names.Length; i++)
            {
                if (i > 0)
                {
                    if (!reader.NextResult() || !reader.Read())
                    {
                        Assert.Fail(names[i]);
                    }
                }
                else
                {
                    if (!reader.Read())
                    {
                        Assert.Fail(names[i]);
                    }
                }
            }
        }

        [Test]
        public void TestBasePrice()
        {
            IDataReader reader = _service.BasePrices(_bookDate.Id, new Region { Id = (int)Helpers.Region.NationalBase }, (int)TestCar.FordF150);  // NOTE: Base prices only exists for NationalBase region

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Base Price");
        }

        [Test]
        public void TestMileageAdjustment()
        {
            IDataReader reader = _service.MileageAdjustment(_bookDate.Id, VehicleType.UsedCar, 2010, 4);

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Mileage Adjustment");
        }

        [Test]
        public void TestOptionAdjustments()
        {
            IDataReader reader = _service.OptionAdjustments(_bookDate.Id, (int)TestCar.FordF150);

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Option Adjustment");
        }

        [Test]
        public void TestRegionAdjustments()
        {
            IDataReader reader = _service.RegionAdjustments(_bookDate.Id, (int)TestCar.FordF150);

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Region Adjustments");
        }

        [Test]
        public void TestRegions()
        {
            IDataReader reader = _service.Regions(_bookDate.Id);

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Regions");
        }

        [Test]
        public void TestRegion()
        {
            IDataReader reader = _service.Region(_bookDate.Id, "60607");  // Any valid zip should produce a region

            Assert.IsNotNull(reader);

            int counter = 0;

            while (reader.Read())
            {
                counter++;
            }

            Assert.Greater(counter, 0, "No Region");
        }
    }
}
