﻿
namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Helpers
{
    public enum Region
    {
        NationalBase = 9,
        Alabama = 12,
        Alaska = 13,
        Arizona = 14,
        Arkansas = 15,
        California = 16,
        Colorado = 17,
        Connecticut = 18,
        Delaware = 19,
        DisOfColumbia = 20,
        Florida = 21,
        Georgia = 22,
        Hawaii = 23,
        Idaho = 24,
        Illinois = 25,
        Indiana = 26,
        Iowa = 27,
        Kansas = 28,
        Kentucky = 29,
        Louisiana = 30,
        Maine = 31,
        Maryland = 32,
        Massachusetts = 33,
        Michigan = 34,
        Minnesota = 35,
        Mississippi = 36,
        Missouri = 37,
        Montana = 38,
        Nebraska = 39,
        Nevada = 40,
        NewHampshire = 41,
        NewJersey = 42,
        NewMexico = 43,
        NewYork = 44,
        NorthCarolina = 45,
        NorthDakota = 46,
        Ohio = 47,
        Oklahoma = 48,
        Oregon = 49,
        Pennsylvania = 50,
        RhodeIsland = 51,
        SouthCarolina = 52,
        SouthDakota = 53,
        Tennessee = 54,
        Texas = 55,
        Utah = 56,
        Vermont = 57,
        Virginia = 58,
        Washington = 59,
        WestVirginia = 60,
        Wisconsin = 61,
        Wyoming = 62
    }

    public enum TestCar
    {
        FordF150 = 349453  // 2005 Ford F150 Regular Cab XL Pickup 4D 6 1/2 ft
    }
}
