﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VehicleValuationGuideWebApplication._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ul>
            <li>Pages
                <ul>
                    <li>
                        <asp:HyperLink ID="BlackBookBookPage" NavigateUrl="~/Pages/BlackBook/Book.aspx" runat="server"
                            Text="Black Book" /></li>
                    <li>
                        <asp:HyperLink ID="KelleyBlueBookBookPage" NavigateUrl="~/Pages/KelleyBlueBook/Book.aspx"
                            runat="server" Text="Kelley Blue Book" /></li>
                    <li>
                        <asp:HyperLink ID="NadaBookPage" NavigateUrl="~/Pages/Nada/Book.aspx" runat="server"
                            Text="NADA" /></li>
                    <li>
                        <asp:HyperLink ID="GalvesPage" NavigateUrl="~/Pages/Galves/Book.aspx" runat="server"
                            Text="Galves" /></li>
                    <li>
                        <asp:HyperLink ID="EdmundsPage" NavigateUrl="~/Pages/Edmunds/Book.aspx" runat="server"
                            Text="Edmunds" /></li>
                    <li>
                        <asp:HyperLink ID="NaaaPage" NavigateUrl="~/Pages/Naaa/Book.aspx" runat="server"
                            Text="NAAA" /></li>
                    <li>
                        <asp:HyperLink ID="ManheimPage" NavigateUrl="~/Pages/Manheim/Book.aspx" runat="server"
                            Text="Manheim" /></li>
                </ul>
            </li>
            <li>Mobile Pages
                <ul>
                    <li>
                        <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Pages/BlackBook/MobileDemo.aspx" runat="server"
                            Text="Black Book" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Pages/KelleyBlueBook/MobileDemo.aspx"
                            runat="server" Text="Kelley Blue Book" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink3" NavigateUrl="~/Pages/Nada/MobileDemo.aspx" runat="server"
                            Text="NADA" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink4" NavigateUrl="~/Pages/Galves/MobileDemo.aspx" runat="server"
                            Text="Galves" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink7" NavigateUrl="~/Pages/Edmunds/MobileDemo.aspx" runat="server"
                            Text="Edmunds" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink5" NavigateUrl="~/Pages/Naaa/MobileDemo.aspx" runat="server"
                            Text="NAAA" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink6" NavigateUrl="~/Pages/Manheim/MobileDemo.aspx" runat="server"
                            Text="Manheim" /></li>
                    <li>
                        <asp:HyperLink ID="TabsHyperLink" NavigateUrl="~/Pages/MobileTabs.aspx" runat="server" 
                            Text="Mobile Tabs" /></li>
                </ul>
            </li>
            <li>Services
                <ul>
                    <li>
                        <asp:HyperLink ID="BlackBookService" NavigateUrl="~/Services/BlackBook.asmx" runat="server"
                            Text="Black Book" /></li>
                    <li>
                        <asp:HyperLink ID="KelleyBlueBookService" NavigateUrl="~/Services/KelleyBlueBook.asmx" runat="server"
                            Text="Kelley Blue Book" /></li>
                    <li>
                        <asp:HyperLink ID="GalvesService" NavigateUrl="~/Services/Galves.asmx" runat="server"
                            Text="Galves" /></li>
                    <li>
                        <asp:HyperLink ID="NadaService" NavigateUrl="~/Services/Nada.asmx" runat="server"
                            Text="NADA" /></li>
                    <li>
                        <asp:HyperLink ID="EdmundsService" NavigateUrl="~/Services/Edmunds.asmx" runat="server"
                            Text="Edmunds" /></li>
                    <li>
                        <asp:HyperLink ID="NaaaService" NavigateUrl="~/Services/Naaa.asmx" runat="server"
                            Text="NAAA" /></li>
                    <li>
                        <asp:HyperLink ID="ManheimService" NavigateUrl="~/Services/Manheim.asmx" runat="server"
                            Text="Manheim" /></li>
                </ul>
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
