﻿using System.ComponentModel;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications;
using ICommandFactory=FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.ICommandFactory;

namespace FirstLook.VehicleValuationGuide.WebApplication.Services
{
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class KelleyBlueBook : WebService
    {
        [WebMethod]
        public RegionResultsDto Region(RegionArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<RegionResultsDto, RegionArgumentsDto> command = factory.CreateRegionCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public RegionsResultsDto Regions(RegionsArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<RegionsResultsDto, RegionsArgumentsDto> command = factory.CreateRegionsCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public TraversalResultsDto Traversal(TraversalArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<TraversalResultsDto, TraversalArgumentsDto> command = factory.CreateTraversalCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public SuccessorResultsDto Successor(SuccessorArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SuccessorResultsDto, SuccessorArgumentsDto> command = factory.CreateSuccessorCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public InitialValuationResultsDto InitialValuation(InitialValuationArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto> command = factory.CreateInitialValuationCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public AdjustedValuationResultsDto AdjustedValuation(AdjustedValuationArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto> command = factory.CreateAdjustedValuationCommand();

            return ExecuteCommand(command, arguments);
        }

        #region Publications

        [WebMethod]
        public PublicationListResultsDto PublicationList(PublicationListArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>> command = factory.CreatePublicationListCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        [WebMethod]
        public PublicationOnDateResultsDto PublicationOnDate(PublicationOnDateArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>> command = factory.CreatePublicationOnDateCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        [WebMethod]
        public PublicationLoadResultsDto PublicationLoad(PublicationLoadArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<PublicationLoadResultsDto, IdentityContextDto<PublicationLoadArgumentsDto>> command = factory.CreatePublicationLoadCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        [WebMethod]
        public PublicationSaveResultsDto PublicationSave(PublicationSaveArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>> command = factory.CreatePublicationSaveCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Helper to add identity information to a command's arguments.
        /// </summary>
        /// <typeparam name="T">Type of the command arguments.</typeparam>
        /// <param name="arguments">Command arguments to be wrapped into a identity context object.</param>
        /// <returns>Command arguments wrapped inside an identity context object.</returns>
        private static IdentityContextDto<T> Parameterize<T>(T arguments)
        {
            return new IdentityContextDto<T>
            {
                Identity = new IdentityDto
                {
                    Name = HttpContext.Current.User.Identity.Name,
                    AuthorityName = HttpContext.Current.User.Identity.AuthenticationType
                },
                Arguments = arguments
            };
        }

        #endregion

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;

        }
    }
}
