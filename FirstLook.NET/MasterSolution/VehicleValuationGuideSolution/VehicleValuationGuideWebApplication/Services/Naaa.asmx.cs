﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;
using ICommandFactory=FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.ICommandFactory;

namespace FirstLook.VehicleValuationGuide.WebApplication.Services
{
    /// <summary>
    /// Summary description for Naaa
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Naaa : WebService
    {
        [WebMethod]
        public FormResultsDto Form(FormArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FormResultsDto, FormArgumentsDto> command = factory.CreateFormCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public ValuationResultsDto Valuation(ValuationArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<ValuationResultsDto, ValuationArgumentsDto> command = factory.CreateValuationCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public TransactionsResultsDto Transactions(TransactionsArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<TransactionsResultsDto, TransactionsArgumentsDto> command = factory.CreateTransactionsCommand();

            return ExecuteCommand(command, arguments);
        }

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;

        }
    }
}
