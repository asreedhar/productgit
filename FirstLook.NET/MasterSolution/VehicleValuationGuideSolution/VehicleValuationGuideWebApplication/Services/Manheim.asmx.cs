﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;
using ICommandFactory=FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.ICommandFactory;

namespace FirstLook.VehicleValuationGuide.WebApplication.Services
{
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Manheim : WebService
    {
        [WebMethod]
        public TraversalResultsDto Traversal(TraversalArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<TraversalResultsDto, TraversalArgumentsDto> command = factory.CreateTraversalCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public SuccessorResultsDto Successor(SuccessorArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SuccessorResultsDto, SuccessorArgumentsDto> command = factory.CreateSuccessorCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public DomainResultsDto Domain(DomainArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<DomainResultsDto, DomainArgumentsDto> command = factory.CreateDomainCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public SaleSummaryResultsDto SaleSummary(SaleSummaryArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaleSummaryResultsDto, SaleSummaryArgumentsDto> command = factory.CreateSaleSummaryCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public ListingResultsDto Listing(ListingArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<ListingResultsDto, ListingArgumentsDto> command = factory.CreateListingCommand();

            return ExecuteCommand(command, arguments);
        }

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;

        }
    }
}
