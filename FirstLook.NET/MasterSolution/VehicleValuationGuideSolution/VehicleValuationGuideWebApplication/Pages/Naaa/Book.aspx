﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Book.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.Naaa.Book" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>NADA / NAAA: AuctionNet</title>

    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>

    <style type="text/css" media="screen">
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        fieldset { width: 450px; overflow: auto; }
        #decoder, #transactions { float: left; }
        #report { float: right; }
        #transactions { clear: left; width: 900px; }
    </style>
</head>
<body>
    <form id="NaaaForm" runat="server">
    
    <div id="nav_header">
    </div>
    <div id="main_content">
        <div id="header">
            <h1>NADA / NAAA: AuctionNet</h1>
        </div>
        <div id="body">
            <fieldset id="decoder">
                <legend>Decoder</legend>
                <table>
                    <tbody id="automatic">
                        <tr><th>VIN</th><td><input type="text" name="vin" id="vin" maxlength="17" /></td><td class="last"><input type="submit" name="decode" id="decode" value="Decode" /></td></tr>
                        <tr><td colspan="3" id="traversal_status"></td></tr>
                    </tbody>
                    <tbody id="adjustments">
                        <tr><th>Mileage</th><td><input type="text" id="mileage" name="mileage" /></td><td></td></tr>
                        <tr><th>Area</th><td><select id="area" name="area"></select></td><td></td></tr>
                        <tr><th>Period</th><td><select id="period" name="period"></select></td><td class="last"></td></tr>
                    </tbody>
                    <tbody id="manual">
                        <tr><th>Year</th><td><select id="year" name="year"></select></td><td class="last"></td></tr>
                        <tr><th>Make</th><td><select id="make" name="make"></select></td><td class="last"></td></tr>
                        <tr><th>Series</th><td><select id="series" name="series"></select></td><td class="last"></td></tr>
                        <tr><th>Body</th><td><select id="body_style" name="body_style"></select></td><td class="last"></td></tr>
                        <tr><th>Synthetic</th><td><select id="synthetic" name="synthetic"></select></td><td class="last"></td></tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="report">
                <legend>Report</legend>
                <table class="results_table" border="1">
                    <thead>
                        <tr>
                            <th></th>
                            <th>High</th>
                            <th>Average of <span id="overall_count">--</span></th>
                            <th>Low</th>
                            <th><span id="similar_count">--</span>Vehicles with Similar Mileage</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Sale Price</th>
                            <td id="sale_price_max">--</td>
                            <td id="sale_price_avg">--</td>
                            <td id="sale_price_min">--</td>
                            <td id="sale_price_similar">--</td>
                        </tr>
                        <tr>
                            <th>Related Avg. Mileage</th>
                            <td id="mileage_max">--</td>
                            <td id="mileage_avg">--</td>
                            <td id="mileage_min">--</td>
                            <td id="mileage_similar">--</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">
                                *NAAA Regions have been grouped to reflect industry standard national regions.
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </fieldset>
            <fieldset id="transactions">
                <legend>Transactions</legend>
                <div>
                    <table border="1">
                        <thead>
                            <TR>
                                <TH abbr="Deal" scope="col">Deal</TH>
                                <TH abbr="Sale Date" scope="col">Sale Date</TH>
                                <TH abbr="NAAA Region" scope="col">NAAA Region</TH>
                                <TH abbr="Sale Type" scope="col">Sale Type</TH>
                                <TH abbr="NAAA Series" scope="col">NAAA Series</TH>
                                <TH abbr="Price" scope="col">Price</TH>
                                <TH abbr="Mileage" scope="col">Mileage</TH>
                                <TH abbr="Engine" scope="col">Engine</TH>
                                <TH abbr="Transmission" scope="col">Transmission</TH>
                            </TR>
                        </thead>
                        <tbody id="transaction_body">
                            
                        </tbody>
                    </table>
                    <!-- pagination here -->
                </div>
            </fieldset>
        </div>
    </div>

    <script src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../Public/Scripts/App/Naaa/Application.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../Public/Scripts/App/Naaa/TestTemplates.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }

        var _Naaa = new Naaa();
        _Naaa.main();
    </script>
    
    </form>
</body>
</html>
