﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileDemo.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.Nada.MobileDemo" %>
<!doctype html>

<html lang="en">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="yes" />
    <link rel="stylesheet" href="/resources/Scripts/FirstLook.Mobile/css/style.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <header id="header" class="toolbar clearfix">
            <h1 id="#page_title">Nada</h1>
        </header>
        <div id="main" class="clearfix">
            <div id="scrollable" class="handle">
            </div>
        </div>
        <footer id="footer" class="clearfix">
        </footer>     
    </div>
    </form>

  <script src="/resources/Scripts/jQuery/1.6/jquery.js"></script>
  <script src="/resources/Scripts/underscore/1.1.4/underscore.js"></script>
  <script src="/resources/Scripts/Dragdealer/0.9.5/dragdealer.js"></script>
  <script src="/resources/Scripts/FirstLook.Mobile/lib.js"></script>

  <script src="../../Public/Scripts/App/Nada/Application.js"></script>
  <script src="../../Public/Scripts/App/Nada/MobileTemplate.js"></script>

  <script type="text/javascript" charset="utf-8">
       var wires = {
           buid: "buid/:buid",
           vuid: "vuid/:vuid",
           vin: "vin/:vin"
       }

       FirstLook.Session.wire(wires);

       $(Nada).bind(Nada.raises.join(" "), function(evt) {
           console.log([evt.namespace,evt.type,arguments]);
       });

       var options = {
           buid: FirstLook.Session.get_state("buid"),
           vuid: FirstLook.Session.get_state("vuid"),
           vin: FirstLook.Session.get_state("vin")
       };

       var _Nada = new Nada();
       _Nada.main(options);


       if (typeof JSON === "undefined") {
           $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
       }
   </script>
</body>
</html>
