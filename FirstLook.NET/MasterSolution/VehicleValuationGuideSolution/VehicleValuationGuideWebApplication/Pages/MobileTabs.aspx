﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileTabs.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.MobileTabs" %>
<!doctype html>

<html lang="en">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="yes" />
    <link rel="stylesheet" href="/resources/Scripts/FirstLook.Mobile/css/style.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <header id="header" class="toolbar clearfix">
            <h1 id="#page_title">Manual Books</h1>
        </header>
        <div id="main" class="clearfix">
            <div id="scrollable" class="handle">
            </div>
        </div>
        <footer id="footer" class="clearfix">
        </footer>     
    </div>
    </form>

  <script src="/resources/Scripts/jQuery/1.6/jquery.js"></script>
  <script src="/resources/Scripts/underscore/1.1.4/underscore.js"></script>
  <script src="/resources/Scripts/Dragdealer/0.9.5/dragdealer.js"></script>
  <script src="/resources/Scripts/FirstLook.Mobile/lib.js"></script>

  <script src="../Public/Scripts/App/BlackBook/Application.js"></script>
  <script src="../Public/Scripts/App/BlackBook/MobileTemplate.js"></script>
  <script src="../Public/Scripts/App/Galves/Application.js"></script>
  <script src="../Public/Scripts/App/Galves/MobileTemplate.js"></script>
  <script src="../Public/Scripts/App/KelleyBlueBook/Application.js"></script>
  <script src="../Public/Scripts/App/KelleyBlueBook/MobileTemplate.js"></script>
  <script src="../Public/Scripts/App/Naaa/Application.js"></script>
  <script src="../Public/Scripts/App/Naaa/MobileTemplate.js"></script>
  <script src="../Public/Scripts/App/Nada/Application.js"></script>
  <script src="../Public/Scripts/App/Nada/MobileTemplate.js"></script>
  <script src="../Public/Scripts/App/Edmunds/Application.js"></script>
  <script src="../Public/Scripts/App/Edmunds/MobileTemplate.js"></script>

  <script type="text/javascript" charset="utf-8">
       (function() {
           var tab = {
               title: "Manual",
               pattern: _,
               view: {
                   Manual: {
                       Tabs: {
                           type: "Tabs",
                           pattern: {
                               keys: [_.isString]
                           },
                           behaviors: {
                               tabable: {
                                   pattern: {
                                       applications: [{
                                           main: _.isFunction
                                       }]
                                   },
                                   application_accessor: "applications"
                               }
                           }
                       }
                   }
               }
           }

           FirstLook.cards.add(tab, "Tab");
       })()

       if (typeof JSON === "undefined") {
           $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
       }

       $(function() {
           var data = {
               keys: [
                   "BlackBook",
                   "KelleyBlueBook",
                   "Galves",
                   "Naaa",
                   "Nada",
                   "Edmunds"
               ], 
               applications: [
                   new BlackBook(),
                   new KelleyBlueBook(),
                   new Galves(),
                   new Naaa(),
                   new Nada(),
                   new Edmunds()
               ]
           }
           var c = FirstLook.cards.get("Tab");
           c.views.Manual.clean();
           c.views.Manual.build(data);
       });
   </script>
</body>
</html>
