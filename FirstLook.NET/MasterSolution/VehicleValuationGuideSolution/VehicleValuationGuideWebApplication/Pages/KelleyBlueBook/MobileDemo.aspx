﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileDemo.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.KelleyBlueBook.MobileDemo" %>
<!doctype html>

<html lang="en">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="yes" />
    <link rel="stylesheet" href="/resources/Scripts/FirstLook.Mobile/css/style.css"/>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-22250230-1']);
    _gaq.push(['_trackPageview']);

    (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <header id="header" class="toolbar clearfix">
            <h1 id="#page_title">Kelley Blue Book</h1>
        </header>
        <div id="main" class="clearfix">
            <div id="scrollable" class="handle">
            </div>
        </div>
        <footer id="footer" class="clearfix">
        </footer>     
    </div>
    </form>

  <script src="/resources/Scripts/jQuery/1.6/jquery.min.js"></script>
  <script src="/resources/Scripts/underscore/1.1.4/underscore.js"></script>
  <script src="/resources/Scripts/Dragdealer/0.9.5/dragdealer.js"></script>
  <script src="/resources/Scripts/FirstLook.Mobile/lib.js"></script>

  <script src="../../Public/Scripts/App/KelleyBlueBook/Application.js"></script>
  <script src="../../Public/Scripts/App/KelleyBlueBook/MobileTemplate.js"></script>

  <script type="text/javascript" charset="utf-8">
       var wires = {
           vin: "vin/:vin",
           kbb_state: "kbb_s/:kbb_state",
           kbb_vid: "kbb_vid/:kbb_vid",
           mileage: "m/:mileage",
       }

       FirstLook.Session.wire(wires);

       var options = {
           vin: FirstLook.Session.get_state("vin"),
           vehicleId: FirstLook.Session.get_state("kbb_vid"),
           state: FirstLook.Session.get_state("kbb_state")
       }

       var _KelleyBlueBook = new KelleyBlueBook();
       _KelleyBlueBook.main(options);

       $(KelleyBlueBook).bind({
            "KelleyBlueBook.SuccessorLoaded": function(evt, data) {
                if (data.CurrentNode && !data.CurrentNode.HasVehicleId) {
                    //FirstLook.Session.set_state("kbb_state", data.CurrentNode.State);
                    //FirstLook.Session.set_state("kbb_vid", null);
                }
            },
            "KelleyBlueBook.SelectionPathChange": function(evt, data) {
                if (data.VehicleId !== "0") {
                    //FirstLook.Session.set_state("kbb_vid", data.VehicleId);
                    //FirstLook.Session.set_state("kbb_state", null);
                }
            }
       })

       var kbb_hash = window.location.hash;
       $(window).bind("hashchange", function(evt) {
           if (window.location.hash == kbb_hash) return;
           kbb_hash = window.location.hash;


           var selected = {
               vin: FirstLook.Session.get_state("vin"),
               vehicleId: FirstLook.Session.get_state("kbb_vid"),
               state: FirstLook.Session.get_state("kbb_state")
           }

            if (selected.vin) {
                $(KelleyBlueBook).trigger("KelleyBlueBook.VinChange", selected.vin);
            } else if (selected.vehicleId) {
                $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", selected.vehicleId);
            } else if (selected.state) {
                $(KelleyBlueBook).trigger("KelleyBlueBook.SelectionChange", selected.state);
            }
       })

       if (typeof JSON === "undefined") {
           $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
       }
   </script>
</body>
</html>
