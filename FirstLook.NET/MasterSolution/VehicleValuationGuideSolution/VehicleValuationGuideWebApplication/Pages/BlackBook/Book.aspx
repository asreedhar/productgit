﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Book.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.BlackBook.Book" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Black Book USA</title>

    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    
    <style type="text/css" media="screen">
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        fieldset { width: 450px; overflow: auto; }
        #decoder, #options { float: left; }
        #vehicle_information, #prices { float: right; }
        #options, #colors { clear: left; }
        #vehicle_information td, #vehicle_information th { width: 100px; }
    </style>
</head>
<body>
    <form id="BlackBookForm" runat="server">
    
    <div id="nav_header">
    </div>
    <div id="main_content">
        <div id="header">
            <h1>Black Book USA</h1>
        </div>
        <div id="body">
            <fieldset id="decoder">
                <legend>Decoder</legend>
                <table>
                    <tbody id="automatic">
                        <tr><th>VIN</th><td><input type="text" name="vin" id="vin" maxlength="17" /></td><td class="last"><input type="submit" name="decode" id="decode" value="Decode" /></td></tr>
                        <tr><td colspan="3" id="traversal_status"></td></tr>
                    </tbody>
                    <tbody id="adjustments">
                        <tr><th>Mileage</th><td><input type="text" name="mileage" id="mileage" /></td><td class="last"></td></tr>
                        <tr><th>State</th><td><select id="state" name="state"></select></td><td class="last"></td></tr>
                    </tbody>
                    <tbody id="manual">
                        <tr><th>Year</th><td><select id="year" name="year"></select></td><td class="last"></td></tr>
                        <tr><th>Make</th><td><select id="make" name="make"></select></td><td class="last"></td></tr>
                        <tr><th>Model</th><td><select id="model" name="model"></select></td><td class="last"></td></tr>
                        <tr><th>Series</th><td><select id="series" name="series"></select></td><td class="last"></td></tr>
                        <tr><th>Body Style</th><td><select id="body_style" name="body_style"></select></td><td class="last"></td></tr>
                        <tr><th>Synthetic</th><td><select id="synthetic" name="synthetic"></select></td><td class="last"></td></tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="vehicle_information">
                <legend>Vehicle Information</legend>
                <table border="1">
                    <tbody>
                        <tr><th>UVC</th><td id="uvc"></td><th>Vehicle Class</th><td id="vehicle_class"></td></tr>
                        <tr><th>MSRP</th><td id="msrp"></td><th>Finance Advance</th><td id="finance_advance"></td></tr>
                        <tr><th>Wheel Base</th><td id="wheel_base"></td><th>Taxable Horsepower</th><td id="taxable_horsepower"></td></tr>
                        <tr><th>Weight</th><td id="weight"></td><th>Tire Size</th><td id="tire_size"></td></tr>
                        <tr><th>Base Horsepower</th><td id="base_horsepower"></td><th>Fuel Type</th><td id="fuel_type"></td></tr>
                        <tr><th>Cylinders</th><td id="cylinders"></td><th>Drive Train</th><td id="drive_train"></td></tr>
                        <tr><th>Engine</th><td id="engine"></td><th>Transmission</th><td id="transmission"></td></tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="options">
                <legend>Options</legend>
            </fieldset>
            <fieldset id="prices">
                <legend>Prices</legend>
            </fieldset>
            <fieldset id="colors">
                <legend>Colors</legend>
            </fieldset>
            <fieldset id="subject">
                <legend>Broker / Vehicle</legend>
                <table border="1">
                    <tbody>
                        <tr>
                            <th><label for="broker">Broker</label></th>
                            <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                            <td><a href="#broker">Select</a></td>
                            <!-- on click: clear vehicle and publication list -->
                        </tr>
                        <tr>
                            <th><label for="vehicle">Vehicle</label></th>
                            <td><input type="text" id="vehicle" name="vehicle" maxlength="36" /></td>
                            <td><a href="#vehicle">Select</a></td>
                            <!-- on click: publication list -->
                        </tr>
                        <tr>
                            <th><label for="publication_date">Publication Date</label></th>
                            <td><input type="text" id="publication_date" name="publication_date" maxlength="10" /></td>
                            <td><a href="#publication_date">Select</a></td>
                            <!-- on click: publication on date -->
                        </tr>
                        <tr>
                            <th colspan="3"><a href="#save">Save</a></th>
                            <!-- on click: save current vehicle configuration -->
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="publications">
                <legend>Publications</legend>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Begin<br />Date</th>
                            <th>End<br />Date</th>
                            <th>Change<br />Type</th>
                            <th>Change<br />Agent</th>
                            <th>User</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="publications_body">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Load</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/BlackBook/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/BlackBook/TestTemplates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _BlackBook = new BlackBook();
        _BlackBook.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>
    
    </form>
</body>
</html>
