﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileDemo.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.BlackBook.MobileDemo" %>
<!doctype html>

<html lang="en">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="yes" />
    <link rel="stylesheet" href="/resources/Scripts/FirstLook.Mobile/css/style.css"/>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-22250230-1']);
    _gaq.push(['_trackPageview']);

    (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <header id="header" class="toolbar clearfix">
            <h1 id="#page_title">Black Book</h1>
        </header>
        <div id="main" class="clearfix">
            <div id="scrollable" class="handle">
            </div>
        </div>
        <footer id="footer" class="clearfix">
        </footer>     
    </div>
    </form>

  <script src="/resources/Scripts/jQuery/1.6/jquery.js"></script>
  <script src="/resources/Scripts/underscore/1.1.4/underscore.js"></script>
  <script src="/resources/Scripts/Dragdealer/0.9.5/dragdealer.js"></script>
  <script src="/resources/Scripts/FirstLook.Mobile/lib.js"></script>

  <script src="../../Public/Scripts/App/BlackBook/Application.js"></script>
  <script src="../../Public/Scripts/App/BlackBook/MobileTemplate.js"></script>

  <script type="text/javascript" charset="utf-8">
       var wires = {
           vin: "vin/:vin",
           bb_state: "bb_s/:bb_state",
           bb_uvc: "bb_uvc/:bb_uvc",
           mileage: "m/:mileage", }

       FirstLook.Session.wire(wires);

       $(BlackBook).bind(BlackBook.raises.join(" "), function(evt) {
           console.log([evt.namespace,evt.type,arguments]);
       });

       var options = {
           vin: FirstLook.Session.get_state("vin"),
           vehicleId: FirstLook.Session.get_state("bb_uvc"),
           state: FirstLook.Session.get_state("bb_state")
       }

       var _BlackBook = new BlackBook();
       _BlackBook.main(options);

       /*
       $(BlackBook).bind({
            "BlackBook.SuccessorLoaded": function(evt, data) {
                if (data.Path.CurrentNode && !data.Path.CurrentNode.HasVehicleId) {
                    //FirstLook.Session.set_state("bb_state", data.Path.CurrentNode.State);
                    FirstLook.Session.set_state("bb_uvc", null);
                }
            },
            "BlackBook.SelectionPathChange": function(evt, data) {
                if (data.VehicleId !== "0") {
                    FirstLook.Session.set_state("bb_uvc", data.VehicleId);
                    //FirstLook.Session.set_state("bb_state", null);
                }
            }
       })
       */

       /*
       var bb_hash = window.location.hash;
       $(window).bind("hashchange", function(evt) {
           if (window.location.hash == bb_hash) return;
           bb_hash = window.location.hash;

           var selected = {
               vin: FirstLook.Session.get_state("vin"),
               uvc: FirstLook.Session.get_state("bb_uvc"),
               state: FirstLook.Session.get_state("bb_state")
           }

            if (selected.vin) {
                $(BlackBook).trigger("BlackBook.VinChange", selected.vin);
            } else if (selected.vehicleId) {
                $(BlackBook).trigger("BlackBook.UvcChange", selected.vehicleId);
            } else if (selected.state ) {
                $(BlackBook).trigger("BlackBook.SelectionChange", selected.state);
            }

       })
       */

       if (typeof JSON === "undefined") {
           $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
       }
   </script>
</body>
</html>
