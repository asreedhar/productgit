﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Book.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.Manheim.Book" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Manheim</title>

    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    
    <style type="text/css" media="screen">
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        fieldset { width: 450px; overflow: auto; }
        #decoder { float: left; }
        #auction, #listing, #details { clear: left; }
        #menu { clear: left; }
        #menu ul { padding-left: 1ex; list-style-type: none; list-style-position: inside; }
        #menu li {  float: left; width: 20ex; padding: 1ex; border: solid 1px grey; margin-left: 2px; text-align: center; }
        .selected { background-color: Lime; }
        .hidden { display: none; }
        #detail_equipment th, #detail_seller th { text-align: right; }
        #auction input { width: 12ex; }
    </style>
</head>
<body>
    <form id="ManheimForm" runat="server">
    
    <div id="nav_header">
    </div>
    <div id="main_content">
        <div id="header">
            <h1>Manheim</h1>
        </div>
        <div id="body">
            <fieldset id="decoder">
                <legend>Decoder</legend>
                <table>
                    <tbody id="automatic">
                        <tr>
                            <th><label for="broker">Broker</label></th>
                            <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                            <td><a href="#broker">Select</a></td>
                        </tr>
                        <tr>
                            <th><label for="vin">VIN</label></th>
                            <td><input type="text" name="vin" id="vin" maxlength="17" /></td>
                            <td><a href="#vin">Decode</a></td>
                        </tr>
                        <tr><td colspan="3" id="traversal_status"></td></tr>
                    </tbody>
                    <tbody id="manual">
                        <tr><th>Year</th><td><select id="decoder_year" name="decoder_year"></select></td><td></td></tr>
                        <tr><th>Make</th><td><select id="decoder_make" name="decoder_make"></select></td><td></td></tr>
                        <tr><th>Model</th><td><select id="decoder_model" name="decoder_model"></select></td><td></td></tr>
                        <tr><th>Body</th><td><select id="decoder_body" name="decoder_body"></select></td><td></td></tr>
                        <tr><th>Synthetic</th><td><select id="decoder_synthetic" name="decoder_synthetic"></select></td><td></td></tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="vehicle">
                <legend>Vehicle</legend>
            </fieldset>
            <div id="menu">
                <ul>
                    <li class="selected"><a href="#auction">Auction</a></li>
                    <li><a href="#listing">Listing</a></li>
                    <li><a href="#details">Details</a></li>
                </ul>
            </div>
            <div id="auction">
                <table border="1">
                    <caption>Auction</caption>
                    <thead>
                        <tr>
                            <th colspan="2">Auction</th>
                            <th>Channel</th>
                            <th colspan="2">Consignor</th>
                            <th>Lane</th>
                            <th>Sale Date</th>
                            <th>Sale #</th>
                            <th>Vehicles</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <select id="auction_auction_id" name="auction_auction_id">
                                </select>
                            </td>
                            <td>
                                <select id="auction_channel_code" name="auction_channel_code">
                                </select>
                            </td>
                            <td colspan="2">
                                <select id="auction_consignor" name="auction_consignor">
                                </select>
                            </td>
                            <td></td>
                            <td>
                                <select id="auction_sale_date" name="auction_sale_date">
                                </select>
                            </td>
                            <td></td>
                            <td>
                                <dl>
                                    <dt>Price</dt>
                                    <dd><input type="text" name="auction_price_min" id="auction_price_min" /> to <input type="text" name="auction_price_max" id="auction_price_max" /></dd>
                                    <dt>Mileage</dt>
                                    <dd><input type="text" name="auction_mileage_min" id="auction_mileage_min" /> to <input type="text" name="auction_mileage_max" id="auction_mileage_max" /></dd>
                                    <dt>Year</dt>
                                    <dd><select name="auction_year" id="auction_year"></select></dd>
                                    <dt>Make</dt>
                                    <dd>
                                        <select id="auction_make" name="auction_make">
                                        </select>
                                    </dd>
                                    <dt>Model</dt>
                                    <dd>
                                        <select id="auction_model" name="auction_model">
                                        </select>
                                    </dd>
                                    <dt>Body</dt>
                                    <dd>
                                        <select id="auction_body" name="auction_body">
                                        </select>
                                    </dd>
                                </dl>
                            </td>
                        </tr>
                    </thead>
                    <tbody id="auction_results">
                        <tr>
                            <td>CINA</td>
                            <td>Manheim Cincinnati</td>
                            <td>SIMULCAST</td>
                            <td>OPEN</td>
                            <td>Open Sale</td>
                            <td>3</td>
                            <td>2011-02-17</td>
                            <td>3</td>
                            <td>30</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="listing" class="hidden">
                <dl>
                    <dt><label for="listing_auction_id">Auction</label></dt>
                    <dd><select name="listing_auction_id" id="listing_auction_id">
                        <option>All</option>
                    </select></dd>
                    <dt><label for="listing_consignor">Consignor</label></dt>
                    <dd><select name="listing_consignor" id="listing_consignor">
                        <option>All</option>
                    </select></dd>
                </dl>
                <table border="1">
                    <caption>Listing</caption>
                    <thead>
                        <tr>
                            <th>Channel</th>
                            <th>Consignor</th>
                            <th>Sale Date</th>
                            <th>Sale</th>
                            <th>Lane</th>
                            <th>Run</th>
                            <th>VIN</th>
                            <th>Year / Make / Model / Body / Trim</th>
                            <th>Mileage</th>
                            <th>Comments</th>
                        </tr>
                        <tr>
                            <td>
                                <select name="listing_channel_code" id="listing_channel_code">
                                    <option>All</option>
                                </select>
                            </td>
                            <td></td>
                            <td>
                                <select id="listing_sale_date" name="listing_sale_date">
                                </select>
                            </td>
                            <td></td>
                            <td>
                                <select name="listing_lane" id="listing_lane">
                                    <option>All</option>
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <dl>
                                    <dt>Year</dt>
                                    <dd><select name="listing_year" id="listing_year"></select></dd>
                                    <dt>Make</dt>
                                    <dd>
                                        <select id="listing_make" name="listing_make">
                                        </select>
                                    </dd>
                                    <dt>Model</dt>
                                    <dd>
                                        <select id="listing_model" name="listing_model">
                                        </select>
                                    </dd>
                                    <dt>Body</dt>
                                    <dd>
                                        <select id="listing_body" name="listing_body">
                                        </select>
                                    </dd>
                                </dl>
                            </td>
                            <td><input type="text" name="listing_mileage_min" id="listing_mileage_min" /> to <input type="text" name="listing_mileage_max" id="listing_mileage_max" /></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody id="listing_results">
                        <tr>
                            <td>SIMULCAST</td>
                            <td>OPEN</td>
                            <td>3</td>
                            <td>2011-02-17</td>
                            <td>7</td>
                            <td>5</td>
                            <td>154</td>
                            <td>1FMCU93G39KA49026</td>
                            <td>2009 FORD ESCAPE 4WD V6 4D CUV 3.0L XLT</td>
                            <td>106,793</td>
                            <td>UPPER ENGINE NOISE</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="details" class="hidden">
                <h3>Vehicle</h3>
                <dl>
                    <dt>Year</dt>
                    <dd id="details_year"></dd>
                    <dt>Make</dt>
                    <dd id="details_make"></dd>
                    <dt>Model</dt>
                    <dd id="details_model"></dd>
                    <dt>Body</dt>
                    <dd id="details_body"></dd>
                    <dt>VIN</dt>
                    <dd id="details_vin"></dd>
                    <dt>Mileage</dt>
                    <dd id="details_mileage"></dd>
                </dl>
                <h3>Seller Address</h3>
                <dl>
                    <dt>City</dt>
                    <dd id="details_seller_city"></dd>
                    <dt>State / Province / Region</dt>
                    <dd id="details_seller_state"></dd>
                    <dt>Postal Code</dt>
                    <dd id="details_seller_zip"></dd>
                    <dt>Country</dt>
                    <dd id="details_seller_country"></dd>
                </dl>
                <h3>Equipment</h3>
                <dl id="details_equipment">
                </dl>
            </div>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Manheim/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Manheim/TestTemplates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Manheim = new Manheim();
        _Manheim.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>
    
    </form>
</body>
</html>
