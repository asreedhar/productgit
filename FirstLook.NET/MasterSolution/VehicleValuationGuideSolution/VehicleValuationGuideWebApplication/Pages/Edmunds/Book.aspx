﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Book.aspx.cs" Inherits="FirstLook.VehicleValuationGuide.WebApplication.Pages.Edmunds.Book" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Edmunds TMV</title>

    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    
    <style type="text/css" media="screen">
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        fieldset { width: 450px; overflow: auto; }
        #decoder, #options { float: left; }
        #colors { float: right; }
        #options, #prices { clear: left; }
        #prices {width: 940px;}
        #prices div {float: left;}
    </style>
</head>
<body>
    <form id="EdmundsForm" runat="server">
    
    <div id="nav_header">
    </div>
    <div id="main_content">
        <div id="header">
            <h1>Edmunds TMV</h1>
        </div>
        <div id="body">
            <fieldset id="decoder">
                <legend>Decoder</legend>
                <table>
                    <tbody id="automatic">
                        <tr><th>VIN</th><td><input type="text" name="vin" id="vin" maxlength="17" /></td><td class="last"><input type="submit" name="decode" id="decode" value="Decode" /></td></tr>
                        <tr><td colspan="3" id="traversal_status"></td></tr>
                    </tbody>
                    <tbody id="adjustments">
                        <tr><th>Mileage</th><td><input type="text" name="mileage" id="mileage" /></td><td class="last"></td></tr>
                        <tr><th>State</th><td><select id="state" name="state"></select></td><td class="last"></td></tr>
                    </tbody>
                    <tbody id="manual">
                        <tr><th>Year</th><td><select id="year" name="year"></select></td><td class="last"></td></tr>
                        <tr><th>Make</th><td><select id="make" name="make"></select></td><td class="last"></td></tr>
                        <tr><th>Model</th><td><select id="model" name="model"></select></td><td class="last"></td></tr>
                        <tr><th>Style</th><td><select id="style" name="style"></select></td><td class="last"></td></tr>
                        <tr><th>Synthetic</th><td><select id="synthetic" name="synthetic"></select></td><td class="last"></td></tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="colors">
                <legend>Colors</legend>
            </fieldset>
            <fieldset id="options">
                <legend>Options</legend>
            </fieldset>
            <fieldset id="prices">
                <legend>Prices</legend>
            </fieldset>
            <fieldset id="subject">
                <legend>Broker / Vehicle</legend>
                <table border="1">
                    <tbody>
                        <tr>
                            <th><label for="broker">Broker</label></th>
                            <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                            <td><a href="#broker">Select</a></td>
                            <!-- on click: clear vehicle and publication list -->
                        </tr>
                        <tr>
                            <th><label for="vehicle">Vehicle</label></th>
                            <td><input type="text" id="vehicle" name="vehicle" maxlength="36" /></td>
                            <td><a href="#vehicle">Select</a></td>
                            <!-- on click: publication list -->
                        </tr>
                        <tr>
                            <th><label for="publication_date">Publication Date</label></th>
                            <td><input type="text" id="publication_date" name="publication_date" maxlength="10" /></td>
                            <td><a href="#publication_date">Select</a></td>
                            <!-- on click: publication on date -->
                        </tr>
                        <tr>
                            <th colspan="3"><a href="#save">Save</a></th>
                            <!-- on click: save current vehicle configuration -->
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="publications">
                <legend>Publications</legend>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Begin<br />Date</th>
                            <th>End<br />Date</th>
                            <th>Change<br />Type</th>
                            <th>Change<br />Agent</th>
                            <th>User</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="publications_body">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Load</td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Edmunds/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Edmunds/TestTemplates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Edmunds = new Edmunds();
        _Edmunds.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>
    
    </form>
</body>
</html>
