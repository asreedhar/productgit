if (typeof Galves !== "function") {
    var Galves = function() {};
}

(function() {
    var State = {
        Mileage: null,
        State: 'Illinois',
        Vin: '',
        VehicleId: '',
        VehicleConfiguration: [],
        /// Publication State
        Broker: '',
        Vehicle: ''
    };

    /* =================== */
    /* == STATE: Classes = */
    /* =================== */
    /* traversal dto */
    function PathDto() {
        this.CurrentNode = null;
        this.Successors = [];
    }

    function NodeDto() {
        this.Parent = null;
        this.Children = [];
        this.VehicleId = 0;
        this.HasVehicleId = false;
        this.Value = "";
        this.State = "";
        this.Label = "";
    }

    /* valuation dto */

    function BookDateDto() {
        this.Id = 0;
        this.Value = '';
    }

    function PriceTableDto() {
        this.Rows = [];
    }

    function PriceTableRowDto() {
        this.RowType = null;
        this.Price = null;
    }

    var PriceTableRowTypeDto = {
        "Undefined": 0,
        "Base": 1,
        "Option": 2,
        "Mileage": 3,
        "Final": 4,
        "toString": function(i) {
            var names = ["Undefined", "Base", "Options", "Mileage", "Final"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function PricesDto() {
        this.TradeIn = 0.0;
        this.Retail = 0.0;
    }

    function AdjustmentDto() {
        this.Name = "";
        this.Amount = 0;
    }

    function VehicleConfigurationDto() {
        this.BookDate = null;
        this.VehicleId = 0;
        this.Vin = "";
        this.State = null;
        this.Mileage = null;
        this.HasMileage = false;
        this.AdjustmentStates = [];
        this.AdjustmentActions = [];
    }

    function AdjustmentStateDto() {
        this.AdjustmentName = "";
        this.Enabled = false;
        this.Selected = false;
    }

    AdjustmentStateDto.prototype = {
        AdjustmentName: "",
        Enabled: false,
        Selected: false
    };

    function AdjustmentActionDto() {
        this.ActionType = 0;
        this.AdjustmentName = "";
    }

    var AdjustmentActionTypeDto = {
        "Undefined": 0,
        "Add": 1,
        "Remove": 2
    };

    function StateDto() {
        this.Name = '';
    }

    var TraversalStatusDto = {
        "Success": 0,
        "InvalidVinLength": 1,
        "InvalidVinCharacters": 2,
        "InvalidVinChecksum": 3,
        "NoDataForVin": 4,
        "toString": function(i) {
            var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    /* publication dto */

    var ChangeAgentDto = {
        "Undefined": 0,
        "User": 1,
        "System": 2,
        "toString": function(i) {
            var names = ["Undefined", "User Change", "System Change"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    // TODO: Message from Simon to Paul
    // TODO: This is a list of the bit flags passed from the server.
    // TODO: END

    var ChangeTypeDto = {
        "None": 0,
        "BookDate": (1 << 0),
        "State": (1 << 1),
        "VehicleId": (1 << 2),
        "Mileage": (1 << 3),
        "AdjustmentActions": (1 << 4),
        "AdjustmentStates": (1 << 5),
        "toString": function(i) {
            var names = ["None", "BookDate", "State", "VehicleId", "Mileage", "Adjustment Actions", "Adjustment States"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function UserDto() {
        this.FirstName = null;
        this.LastName = null;
        this.UserName = null;
    }

    function EditionDto() {
        this.BeginDate = null;
        this.EndDate = null;
        this.User = null;
    }

    function PublicationInfoDto() {
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
    }

    function PublicationDto() {
        // from info
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
        // own properties
        this.VehicleConfiguration = null;
        this.Table = null;
        this.Adjustments = [];
    }

    /* envelopes dto */

    function StatesArgumentsDto() {
        EventBinder(this);
    }

    function StatesResultsDto() {
        this.Arguments = null;
        this.States = [];
    }

    function TraversalArgumentsDto() {
        this.Vin = '';
        EventBinder(this);
    }

    function TraversalResultsDto() {
        this.Arguments = null;
        this.Path = null;
        this.Status = 0;
    }

    function SuccessorArgumentsDto() {
        this.Node = "";
        this.Successor = "";
        EventBinder(this);
    }

    function SuccessorResultsDto() {
        this.Arguments = null;
        this.Path = null;
    }

    function InitialValuationArgumentsDto() {
        this.Vin = '';
        this.VehicleId = 0;
        this.State = '';
        this.Mileage = null;
        this.HasMileage = false;
        EventBinder(this);
    }

    function InitialValuationResultsDto() {
        this.Arguments = null;
        this.Table = null;
        this.Adjustments = [];
        this.VehicleConfiguration = null;
    }

    function AdjustedValuationArgumentsDto() {
        this.Vin = '';
        this.VehicleId = 0;
        this.State = '';
        this.Mileage = null;
        this.HasMileage = false;
        this.VehicleConfiguration = null;
        this.AdjustmentAction = null;
        EventBinder(this);
    }

    function AdjustedValuationResultsDto() {
        this.Table = null;
        this.Arguments = null;
        this.VehicleConfiguration = null;
    }

    /* publication envelopes dto */

    function PublicationListArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        // -- (none) --
        // events please
        EventBinder(this);
    }

    function PublicationListResultsDto() {
        // self
        this.Arguments = null;
        this.Publications = [];
    }

    function PublicationOnDateArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.On = '';
        // events please
        EventBinder(this);
    }

    function PublicationOnDateResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    function PublicationLoadArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.Id = 0;
        // events please
        EventBinder(this);
    }

    function PublicationLoadResultsDto() {
        // self
        this.Arguments = '';
        this.Publication = '';
    }

    function PublicationSaveArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.VehicleConfiguration = null;
        // events please
        EventBinder(this);
    }

    function PublicationSaveResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    /* == END: Classes == */

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }
    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Galves).trigger("Galves.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "States": genericService("/VehicleValuationGuide/Services/Galves.asmx/States", StatesResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.StatesArgumentsDto"
                }
            });
        }),
        "Traversal": genericService("/VehicleValuationGuide/Services/Galves.asmx/Traversal", TraversalResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
                    "Vin": this.Vin
                }
            });
        }),
        "Successor": genericService("/VehicleValuationGuide/Services/Galves.asmx/Successor", SuccessorResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
                    "Node": this.Node,
                    "Successor": this.Successor
                }
            });
        }),
        "InitialValuation": genericService("/VehicleValuationGuide/Services/Galves.asmx/InitialValuation", InitialValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin),
                    "VehicleId": this.VehicleId,
                    "State": (this.State == null ? '' : this.State),
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage : 0),
                    "HasMileage": ((/^\d+$/.test(this.Mileage)) ? true : false)
                }
            });
        }),
        "AdjustedValuation": genericService("/VehicleValuationGuide/Services/Galves.asmx/AdjustedValuation", AdjustedValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin),
                    "VehicleId": this.VehicleId,
                    "State": (this.State == null ? '' : this.State),
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage : 0),
                    "HasMileage": ((/^\d+$/.test(this.Mileage)) ? true : false),
                    "VehicleConfiguration": this.VehicleConfiguration,
                    "AdjustmentAction": this.AdjustmentAction
                }
            });
        }),
        "PublicationList": genericService("/VehicleValuationGuide/Services/Galves.asmx/PublicationList", PublicationListResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications.PublicationListArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "PublicationOnDate": genericService("/VehicleValuationGuide/Services/Galves.asmx/PublicationOnDate", PublicationOnDateResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications.PublicationOnDateArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "On": this.On
                }
            });
        }),
        "PublicationLoad": genericService("/VehicleValuationGuide/Services/Galves.asmx/PublicationLoad", PublicationLoadResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications.PublicationLoadArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "Id": this.Id
                }
            });
        }),
        "PublicationSave": genericService("/VehicleValuationGuide/Services/Galves.asmx/PublicationSave", PublicationSaveResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        })
    };

    var Events = {
        "StatesArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Galves).trigger("Galves.StatesLoaded", [data.States, State.State]);
            }
        },
        "TraversalArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var hasVehicleId = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.VehicleId;
                if (hasVehicleId) {
                    $(Galves).trigger("Galves.VehicleIdChange", data.Path.CurrentNode.VehicleId);
                }
                if (!hasVehicleId || this.RunNodeTemplate) {
                    $(Galves).trigger("Galves.TraversalLoaded", data);
                }
            }
        },
        "SuccessorArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Galves).trigger("Galves.SuccessorLoaded", data);
            }
        },
        "InitialValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // save the state
                State.VehicleConfiguration = data.VehicleConfiguration;
                // build the page
                $(Galves).trigger("Galves.InitialValueLoaded", data);
            },
            stateChange: function() {
                if (State.VehicleId != null && State.VehicleId != '') {
                    var valuation = new InitialValuationArgumentsDto();
                    valuation.VehicleId = State.VehicleId;
                    valuation.Vin = State.Vin;
                    valuation.State = State.State;
                    valuation.Mileage = State.Mileage;
                    $(valuation).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "AdjustedValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // save the state
                State.VehicleConfiguration = data.VehicleConfiguration;
                // build the page
                $(Galves).trigger("Galves.AdjustedValueLoaded", data);
            },
            stateChange: function(event, action) {
                if (State.VehicleId != null && State.VehicleId != '') {
                    var valuation = new AdjustedValuationArgumentsDto();
                    valuation.VehicleId = State.VehicleId;
                    valuation.Vin = State.Vin;
                    valuation.State = State.State;
                    valuation.Mileage = State.Mileage;
                    valuation.VehicleConfiguration = State.VehicleConfiguration;
                    valuation.AdjustmentAction = action || null;
                    $(valuation).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "PublicationListArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Galves).trigger("Galves.PublicationListLoaded", data);
            }
        },
        "PublicationOnDateArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
		if (data.Publication.VehicleConfiguration) {
                    State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                    State.VehicleId = data.Publication.VehicleConfiguration.VehicleId;
		}
                $(Galves).trigger("Galves.PublicationLoaded", data);
            }
        },
        "PublicationLoadArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                State.VehicleId = data.Publication.VehicleConfiguration.VehicleId;
                $(Galves).trigger("Galves.PublicationLoaded", data);
            }
        },
        "PublicationSaveArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Galves).trigger("Galves.PublicationSaved", data);
            }
        }
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        "StatesArgumentsDto": genericMapper(StatesArgumentsDto),
        "StatesResultsDto": genericMapper(StatesResultsDto, {
            "Arguments": "StatesArgumentsDto",
            "States": "StateDto"
        }),
        "TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
        "TraversalResultsDto": genericMapper(TraversalResultsDto, {
            "Arguments": "TraversalArgumentsDto",
            "Path": "PathDto"
        }),
        "SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
        "SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
            "Arguments": "SuccessorArgumentsDto",
            "Path": "PathDto"
        }),
        "NodeDto": genericMapper(NodeDto, {
            "Parent": "NodeDto",
            "Children": "NodeDto"
        }),
        "PathDto": genericMapper(PathDto, {
            "CurrentNode": "NodeDto",
            "Successors": "NodeDto"
        }),
        "InitialValuationArgumentsDto": genericMapper(InitialValuationArgumentsDto),
        "InitialValuationResultsDto": genericMapper(InitialValuationResultsDto, {
            "Arguments": "InitialValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Table": "PriceTableDto",
            "Adjustments": "AdjustmentDto"
        }),
        "AdjustedValuationArgumentsDto": genericMapper(AdjustedValuationArgumentsDto, {
            "AdjustmentAction": "AdjustedValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto"
        }),
        "AdjustedValuationResultsDto": genericMapper(AdjustedValuationResultsDto, {
            "Arguments": "AdjustedValuationArgumentsDto",
            "Table": "PriceTableDto",
            "VehicleConfiguration": "VehicleConfigurationDto"
        }),
        "PriceTableDto": genericMapper(PriceTableDto, {
            "Rows": "PriceTableRowDto"
        }),
        "PriceTableRowDto": function(json) {
            var result = new PriceTableRowDto();
            for (var i in json) {
                if (!json.hasOwnProperty(i)) continue;
                if (i == 'Prices') {
                    result.Prices = DataMapper.PricesDto(json[i]);
                } else if (i == 'RowType') {
                    switch (json[i]) {
                        case 1:
                            result.RowType = PriceTableRowTypeDto.Base;
                            break;
                        case 2:
                            result.RowType = PriceTableRowTypeDto.Option;
                            break;
                        case 3:
                            result.RowType = PriceTableRowTypeDto.Mileage;
                            break;
                        case 4:
                            result.RowType = PriceTableRowTypeDto.Final;
                            break;
                        default:
                            break;
                    }
                }
            }
            return result;
        },
        "PricesDto": genericMapper(PricesDto),
        "AdjustmentDto": function(json) {
            var result = new AdjustmentDto();
            for (var i in json) {
                if (!json.hasOwnProperty(i)) continue;
                if (result[i] !== undefined) {
                    result[i] = json[i];
                }
            }
            return result;
        },
        "BookDateDto": genericMapper(BookDateDto),
        "StateDto": genericMapper(StateDto),
        "VehicleConfigurationDto": genericMapper(VehicleConfigurationDto, {
            "BookDate": "BookDateDto",
            "AdjustmentStates": "AdjustmentStateDto",
            "AdjustmentActions": "AdjustmentActionDto"
        }),
        "AdjustmentStateDto": genericMapper(AdjustmentStateDto),
        "AdjustmentActionDto": genericMapper(AdjustmentActionDto),
        /* publication api */
        "PublicationListArgumentsDto": genericMapper(PublicationListArgumentsDto),
        "PublicationListResultsDto": genericMapper(PublicationListResultsDto, {
            "Arguments": "PublicationListArgumentsDto",
            "Publications": "PublicationInfoDto"
        }),
        "PublicationOnDateArgumentsDto": genericMapper(PublicationOnDateArgumentsDto),
        "PublicationOnDateResultsDto": genericMapper(PublicationOnDateResultsDto, {
            "Arguments": "PublicationOnDateArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationLoadArgumentsDto": genericMapper(PublicationLoadArgumentsDto),
        "PublicationLoadResultsDto": genericMapper(PublicationLoadResultsDto, {
            "Arguments": "PublicationLoadArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationSaveArgumentsDto": genericMapper(PublicationSaveArgumentsDto),
        "PublicationSaveResultsDto": genericMapper(PublicationSaveResultsDto, {
            "Arguments": "PublicationSaveArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "UserDto": genericMapper(UserDto),
        "EditionDto": genericMapper(EditionDto, {
            "User": "UserDto"
        }),
        "PublicationInfoDto": genericMapper(PublicationInfoDto, {
            "Edition": "EditionDto"
        }),
        "PublicationDto": genericMapper(PublicationDto, {
            "Edition": "EditionDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Table": "PriceTableDto",
            "Adjustments": "AdjustmentDto"
        })
    };

    var EventBinder = function(obj) {
        if (obj instanceof StatesArgumentsDto) {
            $(obj).bind("fetchComplete", Events.StatesArgumentsDto.fetchComplete);
        }
        if (obj instanceof TraversalArgumentsDto) {
            $(obj).bind("fetchComplete", Events.TraversalArgumentsDto.fetchComplete);
        }
        if (obj instanceof SuccessorArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SuccessorArgumentsDto.fetchComplete);
        }
        if (obj instanceof InitialValuationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.InitialValuationArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.InitialValuationArgumentsDto.stateChange);
        }
        if (obj instanceof AdjustedValuationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.AdjustedValuationArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.AdjustedValuationArgumentsDto.stateChange);
        }
        if (obj instanceof PublicationListArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationListArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationOnDateArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationOnDateArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationLoadArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationLoadArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationSaveArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationSaveArgumentsDto.fetchComplete);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(StatesArgumentsDto, DataMapper.StatesArgumentsDto);
        BindDataMapper(StatesResultsDto, DataMapper.StatesResultsDto);

        BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
        BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

        BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
        BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

        BindDataMapper(NodeDto, DataMapper.NodeDto);
        BindDataMapper(PathDto, DataMapper.PathDto);

        BindDataMapper(InitialValuationArgumentsDto, DataMapper.InitialValuationArgumentsDto);
        BindDataMapper(InitialValuationResultsDto, DataMapper.InitialValuationResultsDto);

        BindDataMapper(AdjustedValuationArgumentsDto, DataMapper.AdjustedValuationArgumentsDto);
        BindDataMapper(AdjustedValuationResultsDto, DataMapper.AdjustedValuationResultsDto);

        BindDataMapper(PublicationListArgumentsDto, DataMapper.PublicationListArgumentsDto);
        BindDataMapper(PublicationListResultsDto, DataMapper.PublicationListResultsDto);

        BindDataMapper(PublicationOnDateArgumentsDto, DataMapper.PublicationOnDateArgumentsDto);
        BindDataMapper(PublicationOnDateResultsDto, DataMapper.PublicationOnDateResultsDto);

        BindDataMapper(PublicationLoadArgumentsDto, DataMapper.PublicationLoadArgumentsDto);
        BindDataMapper(PublicationLoadResultsDto, DataMapper.PublicationLoadResultsDto);

        BindDataMapper(PublicationSaveArgumentsDto, DataMapper.PublicationSaveArgumentsDto);
        BindDataMapper(PublicationSaveResultsDto, DataMapper.PublicationSaveResultsDto);

        BindDataMapper(UserDto, DataMapper.UserDto);
        BindDataMapper(EditionDto, DataMapper.EditionDto);
        BindDataMapper(PublicationDto, DataMapper.PublicationDto);
        BindDataMapper(PublicationInfoDto, DataMapper.PublicationInfoDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(StatesArgumentsDto, Services.States);

        BindFetch(TraversalArgumentsDto, Services.Traversal);
        BindFetch(SuccessorArgumentsDto, Services.Successor);

        BindFetch(InitialValuationArgumentsDto, Services.InitialValuation);
        BindFetch(AdjustedValuationArgumentsDto, Services.AdjustedValuation);

        BindFetch(PublicationListArgumentsDto, Services.PublicationList);
        BindFetch(PublicationOnDateArgumentsDto, Services.PublicationOnDate);
        BindFetch(PublicationLoadArgumentsDto, Services.PublicationLoad);
        BindFetch(PublicationSaveArgumentsDto, Services.PublicationSave);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }

        if (selected.buid) {
            $(Galves).trigger("Galves.BrokerChange", selected.buid);
        }

        if (selected.state) {
            State.State = selected.state;
        }
        if (selected.mileage) {
            State.Mileage = selected.mileage;
        }

        // Always fetch states, PM
        $(new StatesArgumentsDto()).trigger("fetch");

        if (selected.vuid) {
            $(Galves).trigger("Galves.VehicleChange", selected.vuid);
        } else {
            $(Galves).trigger("Galves.Start", selected);
        }

        State.Vin = selected.vin || State.Vin;
        State.VehicleId = selected.vehicleId || State.VehicleId;
    };

    this.raises = this.raises || [];
    this.raises.push("Galves.StatesLoaded");
    this.raises.push("Galves.TraversalLoaded");
    this.raises.push("Galves.SuccessorLoaded");
    this.raises.push("Galves.InitialValueLoaded");
    this.raises.push("Galves.PublicationListLoaded");
    this.raises.push("Galves.PublicationLoaded");
    this.raises.push("Galves.PublicationSaved");
    this.raises.push("Galves.ErrorState");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Galves.MileageChange");
    this.listensTo.push("Galves.StateChange");
    this.listensTo.push("Galves.SelectionChange");
    this.listensTo.push("Galves.VehicleIdChange");
    this.listensTo.push("Galves.VinChange");
    this.listensTo.push("Galves.AddOption");
    this.listensTo.push("Galves.RemoveOption");
    this.listensTo.push("Galves.LoadPublicationList");
    this.listensTo.push("Galves.LoadPublicationOnDate");
    this.listensTo.push("Galves.LoadPublication");
    this.listensTo.push("Galves.SavePublication");

    var PublicEvents = {
        "Galves.MileageChange": function(evt, newMileage) {
            if (State.Mileage !== newMileage) {
                State.Mileage = newMileage;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Galves.StateChange": function(evt, newState) {
            if (State.State !== newState) {
                State.State = newState;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Galves.SelectionChange": function(evt, next, successors) {
            var hasVehicleId = successors && successors[0] && successors[0].VehicleId;
            if (hasVehicleId) {
                $(Galves).trigger("Galves.VehicleIdChange", next);
            } else {
                var successor = new SuccessorArgumentsDto();
                successor.Node = null;
                successor.Successor = next;
                $(successor).trigger("fetch");
            }
        },
        "Galves.VehicleIdChange": function(evt, newVehicleId) {
            if (State.VehicleId !== newVehicleId) {
                State.VehicleId = newVehicleId;
                $(new InitialValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Galves.VinChange": function(evt, newVin, runNodeTemplate) {
            if (State.Vin !== newVin) {
                State.Vin = newVin;
                State.VehicleId = '';

                if (newVin !== '') {
                    var traversal = new TraversalArgumentsDto();
                    traversal.Vin = State.Vin;
                    traversal.RunNodeTemplate = runNodeTemplate;
                    $(traversal).trigger('fetch'); // TODO: events need to be framework independent
                }
            }
        },
        "Galves.AddOption": function(evt, newOptionName) {
            var action = new AdjustmentActionDto();
            action.AdjustmentName = newOptionName;
            action.ActionType = AdjustmentActionTypeDto.Add;
            $.grep(State.VehicleConfiguration.AdjustmentStates, function(item) {
                if (item.AdjustmentName == action.AdjustmentName) {
                    item.Selected = true;
                }
            });
            $(new AdjustedValuationArgumentsDto()).trigger("stateChange", [action]);
        },
        "Galves.RemoveOption": function(evt, oldOptionName) {
            var action = new AdjustmentActionDto();
            action.AdjustmentName = oldOptionName;
            action.ActionType = AdjustmentActionTypeDto.Remove;
            $.grep(State.VehicleConfiguration.AdjustmentStates, function(item) {
                if (item.AdjustmentName == action.AdjustmentName) {
                    item.Selected = false;
                }
            });
            $(new AdjustedValuationArgumentsDto()).trigger("stateChange", [action]);
        },
        "Galves.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
            State.Vehicle = '';
        },
        "Galves.VehicleChange": function(evt, newVehicle) {
            State.Vehicle = newVehicle;
        },
        "Galves.LoadPublicationList": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publicationList = new PublicationListArgumentsDto();
                publicationList.Broker = State.Broker;
                publicationList.Vehicle = State.Vehicle;
                $(publicationList).trigger("fetch");
            }
        },
        "Galves.LoadPublicationOnDate": function(evt, theDate) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationOnDateArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.On = theDate;
                $(publication).trigger("fetch");
            }
        },
        "Galves.LoadPublication": function(evt, theId) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationLoadArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.Id = theId;
                $(publication).trigger("fetch");
            }
        },
        "Galves.SavePublication": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '' && State.VehicleConfiguration !== null) {
                var publication = new PublicationSaveArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.VehicleConfiguration = State.VehicleConfiguration;
                $(publication).trigger("fetch");
            }
        },
        "Galves.Start": function(evt, values) {
            values.vin = values.vin || State.Vin;
            values.vehicleId = values.vehicleId || State.VehicleId;

            if (values.vin) {
                /// We have a vin so start with the entered vin, PM
                State.Vin = '';
                $(Galves).trigger("Galves.VinChange", values.vin);
            } else if (values.vehicleId) {
                /// Holy shit we have a style already... lets just get that, PM
                $(Galves).trigger("Galves.VehicleIdChange", values.vehicleId);
            } else {
                /// Well we don't know a damn thing about the vehicle, lets provide the
                /// starting point for a selection, PM
                $(new TraversalArgumentsDto()).trigger("fetch");
            }
        }
    };
    $(Galves).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.FormatPriceTableTypeRow = PriceTableRowTypeDto.toString;
    this.FormatTraversalStatus = TraversalStatusDto.toString;
}).apply(Galves);

