if (typeof Galves === "undefined") {
    var Galves = {};
}
(function() {
    this.raises = this.raises || [];
    this.raises.push("Galves.StateChange");
    this.raises.push("Galves.MileageChange");
    this.raises.push("Galves.VinChange");
    this.raises.push("Galves.VehicleIdChange");
    this.raises.push("Galves.SelectionChange");
    this.raises.push("Galves.AddOption");
    this.raises.push("Galves.RemoveOption");
    this.raises.push("Galves.LoadPublicationList");
    this.raises.push("Galves.LoadPublicationOnDate");
    this.raises.push("Galves.LoadPublication");
    this.raises.push("Galves.SavePublication");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Galves.StatesLoaded");
    this.listensTo.push("Galves.TraversalLoaded");
    this.listensTo.push("Galves.SuccessorLoaded");
    this.listensTo.push("Galves.InitialValueLoaded");
    this.listensTo.push("Galves.AdjustedValueLoaded");
    this.listensTo.push("Galves.PublicationListLoaded");
    this.listensTo.push("Galves.PublicationLoaded");
    this.listensTo.push("Galves.PublicationSaved");

    var PublicEvents = {
        "Galves.StatesLoaded": function(evt, states, currentState) {
            var template = new AdjustmentTemplateBuilder(states, currentState);
            template.clean();
            template.init();
        },
        "Galves.TraversalLoaded": function(evt, data) {
            var template = new NodeTemplateBuilder(data.Path, data.Status);
            template.clean();
            template.init();
        },
        "Galves.SuccessorLoaded": function(evt, data) {
            var template = new NodeTemplateBuilder(data.Path, 0);
            template.clean();
            template.init();
        },
        "Galves.InitialValueLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data);
            template.clean();
            template.init();
        },
        "Galves.AdjustedValueLoaded": function(evt, data) {
            var template = new AdjustedTemplateBuilder(data);
            template.clean();
            template.init();
        },
        "Galves.PublicationListLoaded": function(evt, data) {
            var template = new PublicationListTemplateBuilder(data.Publications); ;
            template.clean();
            template.init();
        },
        "Galves.PublicationLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        },
        "Galves.PublicationSaved": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        }
    };
    $(Galves).bind(PublicEvents);

    var AdjustmentTemplateBuilder = function(data, state) {
        this.data = data;
        this.defaultState = state;
        this.$el = $("#adjustments");
        this.$dom = {
            State: $('#state'),
            Mileage: $('#mileage'),
            // publication api
            Form: $('#subject'),
            Save: $('#save'),
            Broker: $('#broker'),
            Vehicle: $('#vehicle'),
            PublicationDate: $('#publication_date')
        };
    };

    AdjustmentTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var list = this.data;
            var select = this.$dom.State;
            select.html('');
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var state = $('<option></option>').val(item.Name).html(item.Name);
                if (item.Name == this.defaultState) {
                    state.attr('selected', 'selected');
                }
                select.append(state);
            }
            select.unbind('change');
            select.bind('change', $.proxy(this.stateChange, this));
            var input = this.$dom.Mileage;
            input.unbind('change');
            input.bind('change', $.proxy(this.mileageChange, this));
            // broker / vehicle
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVehicleOrDate, this));
        },
        clean: function() {
            this.$dom.State.unbind('change');
            this.$dom.Mileage.unbind('change');
            this.$dom.Form.unbind('click');
        },
        stateChange: function(evt) {
            $(Galves).trigger("Galves.StateChange", this.$dom.State.val());
        },
        mileageChange: function(evt) {
            $(Galves).trigger("Galves.MileageChange", this.$dom.Mileage.val());
        },
        changeBrokerOrVehicleOrDate: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                e = document.getElementById(n.substring(1))
                v = (e == null ? null : e.value),
                c = null;
                if (n == '#broker') {
                    $(Galves).trigger("Galves.BrokerChange", v);
                    this.$dom.Vehicle.val('');
                }
                else
                    if (n == '#vehicle') {
                    $(Galves).trigger("Galves.VehicleChange", v);
                    $(Galves).trigger("Galves.LoadPublicationList");
                }
                else
                    if (n == '#publication_date') {
                    $(Galves).trigger("Galves.LoadPublicationOnDate", v);
                }
                else
                    if (n == '#save') {
                    $(Galves).trigger("Galves.SavePublication");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var NodeTemplateBuilder = function(data, status) {
        this.data = data;
        this.status = status;
    };

    NodeTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var bindDropDown = function(s, v, p, items, selected) {
                var prompt = $('<option></option>').val('').html('... Please Select ...');
                s.html('');
                s.append(prompt);
                for (i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    var val = item.State;
                    if (item.HasVehicleId == true) {
                        val = item.VehicleId;
                    }
                    var opt = $('<option></option>').val(val).html(item.Value);
                    if (item.Value == selected) {
                        opt.attr('selected', 'selected');
                    }
                    s.append(opt);
                }
                if (v == false) {
                    function fetchSuccessor(evt) {
                        var next = s.val();
                        if (next != '') {
                            // empty other drop downs
                            var inputs = ['year', 'manufacturer', 'model', 'style', 'synthetic'].slice(p);
                            for (var i = 0; i < inputs.length; i++) {
                                document.getElementById(inputs[i]).options.length = 0;
                            }
                            // reset vin
                            if (inputs.length > 0) {
                                $(Galves).trigger("Galves.VinChange", '');
                                $('#vin').val('');
                            }
                            // request data
                            $(Galves).trigger("Galves.SelectionChange", next);
                        }
                    }
                    s.unbind();
                    s.bind('change', fetchSuccessor);
                }
                else {
                    function fetchInitialValuation(evt) {
                        $(Galves).trigger("Galves.VehicleIdChange", s.val());
                    }
                    s.unbind();
                    s.bind('change', fetchInitialValuation);
                }
            };
            var bindQueryByVin = function() {
                var decode = $('#GalvesForm');
                var decoder = function(evt) {
                    var val = $("#vin").val();
                    // update state
                    $(Galves).trigger("Galves.VinChange", [val, true]);
                    // do not submit (paranoid)
                    return false;
                };
                decode.unbind('submit', decoder);
                decode.bind('submit', decoder);
            };
            var bindNode = function(node, successors, selected) {
                var valuation = successors.length > 0 && !!successors[0].VehicleId;
                var positions = {
                    'Year': 2,
                    'Manufacturer': 3,
                    'Model': 4,
                    'Style': 5
                };
                var position = positions[node.Label] || 1;
                switch (node.Label) {
                    case '':
                        bindQueryByVin();
                        bindDropDown($("#year"), valuation, position, successors, selected);
                        break;
                    case 'Year':
                        bindDropDown($("#manufacturer"), valuation, position, successors, selected);
                        break;
                    case 'Manufacturer':
                        bindDropDown($("#model"), valuation, position, successors, selected);
                        break;
                    case 'Model':
                        bindDropDown($("#style"), valuation, position, successors, selected);
                        break;
                    case 'Vin':
                        if (node.HasVehicleId == true && node.VehicleId != '') {
                            $(Galves).trigger("Galves.VehicleIdChange", node.VehicleId);
                            selected = successors[0].Value;
                        }
                        position = positions[node.Parent.Label];
                        if (node.Parent.Label == 'Model') {
                            bindDropDown($("#style"), valuation, position, successors, selected);
                        }
                        else {
                            var x = [];
                            for (var j = 0; j < successors.length; j++) {
                                var s = successors[j];
                                var u = s.VehicleId;
                                var t = [];
                                while (s != null) {
                                    t.push(s.Value);
                                    s = s.Parent;
                                }
                                var n = t.reverse().join(' ');
                                x.push({
                                    'VehicleId': u,
                                    'HasVehicleId': true,
                                    'Value': n,
                                    'State': u,
                                    'Label': 'Synthetic'
                                });
                            }
                            bindDropDown($("#synthetic"), valuation, position, x);
                        }
                        break;
                }
            };
            var bindNodeRecursive = function(node, successors, selected) {
                if (node.Parent != null) {
                    bindNodeRecursive(node.Parent, node.Parent.Children, node.Value);
                }
                bindNode(node, successors, selected);
            };
            if (this.status == 0) {
                // structure: '' > 'year' > 'manufacturer' > 'model' [> 'vin'] > 'style'
                if (this.data.CurrentNode.Label == 'Vin') {
                    bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    bindNode(this.data.CurrentNode, this.data.Successors);
                }
                $("#traversal_status").html('');
            }
            else {
                $("#traversal_status").html(Galves.FormatTraversalStatus(this.status));
            }
        },
        clean: function() {
            $("fieldset").find("div").empty();
        }
    };

    var InitialTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $("#results");
    };

    InitialTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._options();
            this._table();
        },
        clean: function() {
            // this.$el.html('');
        },
        _options: function() {
            var info = $("#options");
            info.find("div").empty(); // clean up from last time
            var hd = $("<div></div>");
            $(info).append(hd);

            var adjustmentList = this.data.Adjustments;
            var th = '<thead><tr><th></th><th>Adjustment</th><th>Amount</th></tr></thead>';
            var tb = '<tbody>';
            for (var i = 0; i < adjustmentList.length; i++) {
                var adjustment = adjustmentList[i];
                var amount = adjustment.Amount;
                var state = jQuery.grep(
                this.data.VehicleConfiguration.AdjustmentStates, function(v) {
                    return v.AdjustmentName == adjustment.Name;
                });
                var attr = '';
                if (state.length == 1 && state[0].Selected === true) {
                    attr = 'checked="checked"';
                }
                if (state.length == 1 && state[0].Enabled === false) {
                    attr += ' disabled="disabled"';
                }
                var nm = adjustment.Name.replace('"', '\\u0022');
                var tr = '<tr><td><input type="checkbox" name="' + nm + '" id="' + nm + '" ' + attr + ' /></td>' + '<td>' + adjustment.Name + '</td>' + '<td>' + amount + '</td></tr>';
                tb += tr;
            }
            tb += '</tr></tbody>';
            $(hd).html('<table>' + th + tb + '</table>');

            var optionSelected = function(evt) {
                if (evt.target) {
                    var nm = evt.target.name.replace('\\u0022', '"'),
                    ck = evt.target.checked;
                    $(Galves).trigger(ck ? "Galves.AddOption" : "Galves.RemoveOption", nm);
                }
            };

            $(hd).bind('change', optionSelected);
        },
        _table: function() {
            var info = $("#prices");
            // clean up from last time ...
            info.find("div").empty();
            // one price table ...
            var table = this.data.Table;
            var hd = $("<div></div>");
            var th = '<thead><tr><th></th><th>Trade In</th><th>Retail</th></tr></thead>';
            var tb = '<tbody>';
            for (var i = 0; i < table.Rows.length; i++) {
                var tableRow = table.Rows[i];
                var tc = Galves.FormatPriceTableTypeRow(tableRow.RowType),
                    lc = tc.toLowerCase();
                var tr = '<tr>'
                + '<td>' + tc + '</td>'
                + '<td id="trade_in_' + lc + '">' + tableRow.Prices.TradeIn + '</td>'
                + '<td id="retail_' + lc + '">' + tableRow.Prices.Retail + '</td>'
                + '</tr>';
                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + th + tb + '</table>');
            $(info).append(hd);
        }
    };

    var AdjustedTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $("#prices");
    };

    AdjustedTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();
            this._accessories();
        },
        clean: function() {
            this.$el.html('');
        },
        _accessories: function() {
            var states = this.data.VehicleConfiguration.AdjustmentStates;
            for (var i = 0; i < states.length; i++) {
                var state = states[i],
                nm = state.AdjustmentName.replace('"', '\\u0022').replace("ADD:", "").replace("DED:", "");
                el = document.getElementById(nm);
                $(el).attr('checked', state.Selected);
            }
        }
    };

    AdjustedTemplateBuilder.prototype._table = InitialTemplateBuilder.prototype._table;

    var PublicationListTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#publications'),
            Body: $('#publications_body')
        };
    };

    PublicationListTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();
        },
        clean: function() {
            this.$dom.Body.empty();
            this.$dom.Form.unbind('click');
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                + '<td>' + item.Edition.BeginDate + '</td>'
                + '<td>' + item.Edition.EndDate + '</td>'
                + '<td>' + item.ChangeType + '</td>'
                + '<td>' + item.ChangeAgent + '</td>'
                + '<td>' + item.Edition.User.UserName + '</td>'
                + '<td><a href="#' + item.Id + '">Load</a></td>'
                + '</tr>';
                code += tr;
            }
            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.selectPublication, this));
        },
        selectPublication: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    p = tgt.href.substring(i + 1);
                $(Galves).trigger("Galves.LoadPublication", p);
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

}).apply(Galves);

