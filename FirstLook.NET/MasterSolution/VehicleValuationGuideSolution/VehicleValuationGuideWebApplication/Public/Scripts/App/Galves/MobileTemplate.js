if (typeof Galves === "undefined") {
	var Galves = {};
}

(function() {
	this.raises = this.raises || [];
	this.raises.push("Galves.StateChange");
	this.raises.push("Galves.MileageChange");
	this.raises.push("Galves.VinChange");
	this.raises.push("Galves.VehicleIdChange");
	this.raises.push("Galves.SelectionChange");
	this.raises.push("Galves.AddOption");
	this.raises.push("Galves.RemoveOption");

	this.listensTo = this.listensTo || [];
	this.listensTo.push("Galves.StatesLoaded");
	this.listensTo.push("Galves.TraversalLoaded");
	this.listensTo.push("Galves.SuccessorLoaded");
	this.listensTo.push("Galves.InitialValueLoaded");
	this.listensTo.push("Galves.AdjustedValueLoaded");

	var State = {
		States: [],
		CurrentState: ""
	};

	var PublicEvents = {
		"Galves.StatesLoaded": function(evt, states, currentState) {
			State.States = _.reduce(states, function(memo, state) {
				memo.push({
					name: state.Name,
					value: state.Name
				});
				return memo;
			},
			[]);

			State.CurrentState = currentState;
		},
		"Galves.TraversalLoaded": function(evt, data) {
			var card = FirstLook.cards.get("Galves");

			card.build(data);
		},
		"Galves.SuccessorLoaded": function(evt, data) {
			var card = FirstLook.cards.get("Galves");

			if (data.Path.CurrentNode && data.Path.CurrentNode.HasVehicleId) {
				$(Galves).trigger("Galves.VehicleIdChange", data.Path.CurrentNode.VehicleId);
			} else {
				card.build(data);
			}
		},
		"Galves.SelectionPathChange": function(evt, data) {
			if (data.VehicleId !== "0") {
				$(Galves).trigger("Galves.VehicleIdChange", data.VehicleId);
			} else {
				$(Galves).trigger("Galves.SelectionChange", data.State);
			}
		},
		"Galves.InitialValueLoaded": function(evt, data) {
			var card = FirstLook.cards.get("Galves");

			data.States = State.States;

			card.build(data);
		},
		"Galves.AdjustedValueLoaded": function(evt, data) {
			var card = FirstLook.cards.get("Galves");

			$(card.views.values.EquipmentList).trigger("selection_set", [data.VehicleConfiguration.AdjustmentStates]);
			$(card.views.values.ValueTable).trigger("rebuild", [data]);
		},
		/// Component Bridge Events
		"Galves.EquipmentItemSelected": function(evt, name, isSelected) {
			$(Galves).trigger(isSelected ? "Galves.AddOption": "Galves.RemoveOption", [name]);
		},
		"Galves.MileageOrStateChange": function(evt, data) {
			$(Galves).trigger("Galves.MileageChange", data.Mileage);
			$(Galves).trigger("Galves.StateChange", data.State);
		},
		"Galves.MileageChange": function(evt, data) {
			var card = FirstLook.cards.get("Galves");
			if (card.views.values.ValueFieldSet.$el) {
				card.views.values.ValueFieldSet.$el.find("input[name=Mileage]").val(data);
			}
		},
		"Galves.StateChange": function(evt, data) {
			var card = FirstLook.cards.get("Galves");
			if (card.views.values.ValueFieldSet.$el) {
				card.views.values.ValueFieldSet.$el.find("select[name=State]").val(data);
			}
		},
		"Galves.SelectionChangeWithDecode": function(evt, data) {
			var newSelection = JSON.parse(data.NewGalvesSelection);
			$(Galves).trigger("Galves.SelectionPathChange", newSelection);
		},
		"Galves.PublicationLoaded": function(evt, data) {
			var card = FirstLook.cards.get("Galves");

			data.Publication.States = State.States;

			if (data.Publication.VehicleConfiguration) {
				data.Publication.States = State.States;

				card.build(data.Publication);
			} else {
				$(Galves).trigger("Galves.Start", {});
			}
		},
		"Galves.VehicleChange": function(evt, data) {
			$(Galves).trigger("Galves.LoadPublicationOnDate", '2038-01-01');
		},
		"Galves.ErrorState": function(evt, data) {
			var card = FirstLook.cards.get("KelleyBlueBook");
			card.build(data);
		}
	};
	$(Galves).bind(PublicEvents);

    /// Return matrix of values to configure Table for Galves Tab, PM
	function table_fitler(data) {
		var table = [],
		row_finder = function(type) {
			return function(i) {
				return i.RowType == type;
			};
		},
		o = _.detect(data.Table.Rows, row_finder(2)),
		m = _.detect(data.Table.Rows, row_finder(3)),
		f = _.detect(data.Table.Rows, row_finder(4));

		data.caption = "Values";

		table.head = [["", "Options<br />Mileage", "Final"]];
		table.push(["Trade In", //
		_.moneyAdjusted(o.Prices.TradeIn) + "<br />" + _.moneyAdjusted(m.Prices.TradeIn), //
		_.money(f.Prices.TradeIn)]);
		table.push(["Retail", //
		_.moneyAdjusted(o.Prices.Retail) + "<br />" + _.moneyAdjusted(m.Prices.Retail), //
		_.money(f.Prices.Retail)]);

		table.header_col = 0;
		table.col_class_name = ["market", "adjustments", "final"];

		data.table = table;

		return data;
	}

    /// Return matrix of values to configure Table for Galves Summary, PM
	function summary_fitler(data) {
		var table = [],
		row_finder = function(type) {
			return function(i) {
				return i.RowType == type;
			};
		},
		f = _.detect(data.Table.Rows, row_finder(4));

		data.caption = "Galves Values";

		table.head = [["Trade In", "Retail"]];
		table.push([_.money(f.Prices.TradeIn), _.money(f.Prices.Retail)]);

		table.col_class_name = ["trade-in", "retail"];

		data.table = table;

		return data;
	}

	FirstLook.components.add({
		type: "Table",
		pattern: {
			Table: {
				Rows: [{
					Prices: {
						TradeIn: _.isNumber,
						Retail: _.isNumber
					}
				}]
			}
		},
		behaviors: {
			rebuildable: {}
		},
		class_name: "g_value_tables",
		filter: table_fitler,
		caption_accessor: "caption",
		body_accessor: "table",
		head_accessor: "table.head",
		col_class_name_accessor: "table.col_class_name",
		body_header_col_accessor: "table.header_col"
	},
	"Galves.Table");

	FirstLook.components.add({
		type: "Table",
		pattern: {
			Table: {
				Rows: [{
					Prices: {
						TradeIn: _.isNumber,
						Retail: _.isNumber
					}
				}]
			}
		},
		behaviors: {
			rebuildable: {}
		},
		class_name: "g_value_tables",
		filter: summary_fitler,
		caption_accessor: "caption",
		body_accessor: "table",
		head_accessor: "table.head",
		col_class_name_accessor: "table.col_class_name",
		body_header_col_accessor: "table.header_col"
	},
	"Galves.Summary");

	FirstLook.components.add({
		type: "Fieldset",
		pattern: {
			Status: function(v) {
				return v === 0; // Success
			},
			Path: {
				Successors: [{
					State: _.isString,
					Value: _.isString,
					Label: _.isString,
					VehicleId: _
				}]
			}
		},
		behaviors: {
			submitable: {
				event: "Galves.SelectionChangeWithDecode",
				scope: Galves,
				submit_on_change: true
			}
		},
		legend_accessor: "fieldset.legend",
		input_accessor: "fieldset.inputs",
		name_accessor: "name",
		button_accessor: "fieldset.buttons",
		label_accessor: "label",
		value_accessor: "value",
		type_accessor: "type",
		filter: function(data) {
			var options = _.map(data.Path.Successors, function(succ) {
				return {
					name: succ.Value,
					value: JSON.stringify({
						State: succ.State,
						VehicleId: succ.VehicleId
					})
				};
			}),
			fieldset = {
				legend: "Galves " + data.Path.Successors[0].Label,
				inputs: [{
					name: "NewGalvesSelection",
					label: data.Path.Successors[0].Label,
					options: options
				}],
				buttons: []
			};

			options.unshift({
				name: "Select...",
				value: ""
			});
			data.fieldset = fieldset;
			return data;
		}
	},
	"Galves.Select");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='error'>Problem with the VIN</p>";
		}
	},
	"Galves.VinProblem");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='message'>No Galves Data For VIN</p>";
		}
	},
	"Galves.NoData");

	var state_patterns = {
		select: {
			Path: {
				Successors: _.isArray
			},
			Status: function(v) {
				return v === 0 || _.isUndefined(v);
			}
		},
		values: {
			VehicleConfiguration: _
		},
		vin_problem: {
			Status: function(v) {
				return v >= 1 && v <= 3;
			}
		},
		data_problem: {
			Status: function(v) {
				return v === 4;
			}
		},
		error: {
			errorType: _,
			errorResponse: _
		}
	};

	var overview_card = {
		pattern: _,
		view: {
			select: {
				pattern: state_patterns.select,
				Select: FirstLook.components.get("Galves.Select")
			},
			summary: {
				pattern: state_patterns.values,
				Summary: FirstLook.components.get("Galves.Summary")
			},
			vin_problem: {
				pattern: state_patterns.vin_problem,
				Status: FirstLook.components.get("Galves.VinProblem")
			},
			data_problem: {
				pattern: state_patterns.data_problem,
				Status: FirstLook.components.get("Galves.NoData")
			},
			error: {
				pattern: state_patterns.error,
				ErrorMessage: FirstLook.components.get("Application.ServerError")
			}
		}
	};
	FirstLook.cards.add(overview_card, "GalvesSummary");

	var cards = {
		title: "Galves",
		pattern: _,
		view: {
			select: {
				pattern: state_patterns.select,
				Title: {
					type: "Static",
					pattern: _,
					template: function() {
						return "<h1 class='logo_galves'>Galves</h1>";
					}
				},
				Review: {
					type: "Static",
					pattern: _,
					template: function(data) {
						return "<p>" + _.pluck(data._selection, "Value").join(" | ") + "</p>";
					},
					filter: function(data) {
						var selection = [];
						function append_parent(v) {
							selection.push(v);
							if (v.Parent) {
								append_parent(v.Parent);
							}
							return _.first(selection, selection.length - 1);
						}

						data._selection = append_parent(data.Path.CurrentNode);
						return data;
					}
				},
				TraversalList: {
					type: "List",
					class_name: "select_list vid_select",
					pattern: {
						Path: {
							CurrentNode: {
								HasVehicleId: false
							},
							Successors: [{
								Value: _
							}]
						}
					},
					behaviors: {
						selectable: {
							pattern: {
								Path: {
									Successors: [{
										State: _.isString,
										VehicleId: _
									}]
								}
							},
							data_accessor: ["State", "VehicleId"],
							binds: {
								"item_selected": {
									type: "Galves.SelectionPathChange",
									scope: Galves
								}
							}
						}
					},
					item_accessor: "Path.Successors",
					text_accessor: "Value",
					data_accessor: "Value"
				}
			},
			values: {
				pattern: state_patterns.values,
				ValueFieldSet: {
					type: "Fieldset",
					pattern: {
						VehicleConfiguration: {
							Mileage: _.isNumber,
							State: {
								Name: _.isString
							}
						}
					},
					behaviors: {
						data_mergable: {
							data_destination_accessor: "inputs"
						},
						submitable: {
							event: "Galves.MileageOrStateChange",
							scope: Galves,
							submit_on_change: true
						}
					},
					class_name: "book_fieldset",
					legend_accessor: "legend",
					input_accessor: "inputs",
					name_accessor: "name",
					button_accessor: "buttons",
					label_accessor: "label",
					value_accessor: "data",
					type_accessor: "type",
					static_data: {
						legend: "Galves",
						inputs: [{
							name: "State",
							label: "States",
							data: "VehicleConfiguration.State.Name",
							options: "States"
						},
						{
							name: "Mileage",
							label: "Mileage",
							data: "VehicleConfiguration.Mileage",
							type: "tel"
						}],
						buttons: ["Update"]
					}
				},
				ValueTable: FirstLook.components.get("Galves.Table"),
				EquipmentList: {
					type: "List",
					behaviors: {
						multi_selectable: {
							pattern: {
								VehicleConfiguration: {
									AdjustmentStates: [{
										AdjustmentName: _.isString,
										Selected: _.isBoolean
									}]
								}
							},
							multi_item_accessor: "VehicleConfiguration.AdjustmentStates",
							multi_data_accessor: "AdjustmentName",
							selected_accessor: "Selected",
							binds: {
								"item_selected": {
									scope: Galves,
									type: "Galves.EquipmentItemSelected"
								}
							}
						}
					},
					pattern: {
						VehicleConfiguration: {
							AdjustmentStates: [{
								AdjustmentName: _.isString
							}]
						}
					},
					class_name: "equipment_list",
					item_accessor: "VehicleConfiguration.AdjustmentStates",
					text_accessor: "AdjustmentName",
					data_accessor: "AdjustmentName"
				}
			},
			vin_problem: {
				pattern: state_patterns.vin_problem,
				VinProblem: FirstLook.components.get("Galves.VinProblem")
			},
			data_problem: {
				pattern: state_patterns.data_problem,
				DataProblem: FirstLook.components.get("Galves.NoData")
			},
			error: {
				pattern: state_patterns.error,
				ErrorMessage: FirstLook.components.get("Application.ServerError")
			}
		}
	};

	FirstLook.cards.add(cards, "Galves");
}).apply(Galves);

