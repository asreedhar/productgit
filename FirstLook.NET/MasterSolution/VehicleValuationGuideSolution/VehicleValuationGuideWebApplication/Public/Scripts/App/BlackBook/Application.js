if (typeof(BlackBook) !== "function") {
    var BlackBook = function() {};
}

(function() {
    var ns = this;

    var State = {
        State: 'IL',
        Vin: '',
        Uvc: '',
        Mileage: null,
        VehicleConfiguration: null,
        /// Publication State
        Broker: '',
        Vehicle: ''
    };

    /* traversal dto */
    function PathDto() {
        this.CurrentNode = null;
        this.Successors = [];
    }

    function NodeDto() {
        this.Parent = null;
        this.Children = [];
        this.Uvc = "";
        this.Label = "";
        this.Value = "";
        this.State = "";
    }

    /* valuation dto */
    function PriceTablesDto() {
        this.Retail = null;
        this.Wholesale = null;
        this.TradeIn = null;
        this.FinanceAdvance = null;
    }

    function PriceTableDto() {
        this.HasExtraClean = true;
        this.HasClean = true;
        this.HasAverage = true;
        this.HasRough = true;
        this.Rows = [];
    }

    function PriceTableRowDto() {
        this.RowType = null;
        this.Prices = null;
    }

    var PriceTableRowTypeDto = {
        "Undefined": 0,
        "Base": 1,
        "Option": 2,
        "Mileage": 3,
        "Final": 4,
        "toString": function(i) {
            var names = ["Undefined", "Base", "Option", "Mileage", "Final"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function PricesDto() {
        this.ExtraClean = 0;
        this.Clean = 0;
        this.Average = 0;
        this.Rough = 0;
    }

    function OptionDto() {
        this.Code = '';
        this.Description = '';
        this.Prices = null;
    }

    function VehicleInformationDto() {
        this.Uvc = '';
        this.Msrp = 0;
        this.FinanceAdvance = 0;
        this.WheelBase = '';
        this.TaxableHorsepower = '';
        this.Weight = '';
        this.TireSize = '';
        this.BaseHorsepower = '';
        this.FuelType = '';
        this.Cylinders = '';
        this.DriveTrain = '';
        this.Transmission = '';
        this.Engine = '';
        this.VehicleClass = '';
    }

    function StateDto() {
        this.Code = '';
        this.Name = '';
    }

    function ColorDto() {
        this.ColorType = null;
        this.Description = null;
        this.Swatches = [];
    }

    function VehicleConfigurationDto() {
        this.Uvc = 0;
        this.Vin = "";
        this.State = null;
        this.Mileage = null;
        this.OptionStates = [];
        this.OptionActions = [];
        this.BookDate = null;
    }
    
    function BookDateDto() {
        this.Id = 0;
    }

    function OptionStateDto() {
        this.Uoc = "";
        this.Enabled = false;
        this.Selected = false;
    }

    function OptionActionDto() {
        this.ActionType = 0;
        this.Uoc = "";
    }

    var OptionActionTypeDto = {
        "Undefined": 0,
        "Add": 1,
        "Remove": 2
    };

    var TraversalStatusDto = {
        "Success": 0,
        "InvalidVinLength": 1,
        "InvalidVinCharacters": 2,
        "InvalidVinChecksum": 3,
        "NoDataForVin": 4,
        "toString": function(i) {
            var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    /* publication dto */

    var ChangeAgentDto = {
        "Undefined": 0,
        "User": 1,
        "System": 2,
        "toString": function(i) {
            var names = ["Undefined", "User Change", "System Change"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    // TODO: Message from Simon to Paul
    // TODO: This is a list of the bit flags passed from the server.
    // TODO: END

    var ChangeTypeDto = {
        "None": 0,
        "Period": (1 << 0),
        "State": (1 << 1),
        "Uid": (1 << 2),
        "Mileage": (1 << 3),
        "OptionActions": (1 << 4),
        "OptionStates": (1 << 5),
        "toString": function(i) {
            var names = ["None", "BookDate", "State", "StyleId", "Mileage", "Option Actions", "Option States"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function UserDto() {
        this.FirstName = null;
        this.LastName = null;
        this.UserName = null;
    }

    function EditionDto() {
        this.BeginDate = null;
        this.EndDate = null;
        this.User = null;
    }

    function PublicationInfoDto() {
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
    }

    function PublicationDto() {
        // from info
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
        // own properties
        this.VehicleConfiguration = null;
        this.Tables = null;
        this.Options = [];
        this.Colors = [];
    }

    /* envelopes dto */
    function StatesArgumentsDto() {
        EventBinder(this);
    }

    function StatesResultsDto() {
        this.Arguments = null;
        this.States = [];
    }

    function TraversalArgumentsDto() {
        this.State = "";
        this.Vin = "";
        EventBinder(this);
    }

    function TraversalResultsDto() {
        this.Arguments = null;
        this.Path = null;
        this.Status = 0;
    }

    function SuccessorArgumentsDto() {
        this.State = "";
        this.Node = "";
        this.Successor = "";
        EventBinder(this);
    }

    function SuccessorResultsDto() {
        this.Arguments = null;
        this.Path = null;
    }

    function InitialValuationArgumentsDto() {
        this.Vin = '';
        this.Uvc = '';
        this.Mileage = null;
        this.HasMileage = false;
        EventBinder(this);
    }

    function InitialValuationResultsDto() {
        this.Tables = null;
        this.Arguments = null;
        this.Options = [];
        this.Colors = [];
        this.VehicleConfiguration = null;
    }

    function AdjustedValuationArgumentsDto(action) {
        this.VehicleConfiguration = null;
        this.State = 0;
        this.Mileage = null;
        this.HasMileage = null;
        this.OptionAction = action || null;
        EventBinder(this);
    }

    function AdjustedValuationResultsDto() {
        this.Tables = null;
        this.Arguments = null;
        this.VehicleConfiguration = null;
    }

    /* publication envelopes dto */

    function PublicationListArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        // -- (none) --
        // events please
        EventBinder(this);
    }

    function PublicationListResultsDto() {
        // self
        this.Arguments = null;
        this.Publications = [];
    }

    function PublicationOnDateArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.On = '';
        // events please
        EventBinder(this);
    }

    function PublicationOnDateResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    function PublicationLoadArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.Id = 0;
        // events please
        EventBinder(this);
    }

    function PublicationLoadResultsDto() {
        // self
        this.Arguments = '';
        this.Publication = '';
    }

    function PublicationSaveArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.VehicleConfiguration = null;
        // events please
        EventBinder(this);
    }

    function PublicationSaveResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    /* == END: Classes == */

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "StatesArgumentsDto": genericMapper(StatesArgumentsDto),
        "StatesResultsDto": genericMapper(StatesResultsDto, {
            "Arguments": "StatesArgumentsDto",
            "States": "StateDto"
        }),
        "TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
        "TraversalResultsDto": genericMapper(TraversalResultsDto, {
            "Arguments": "TraversalArgumentsDto",
            "Path": "PathDto"
        }),
        "SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
        "SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
            "Arguments": "SuccessorArgumentsDto",
            "Path": "PathDto"
        }),
        "NodeDto": genericMapper(NodeDto, {
            "Parent": "NodeDto",
            "Children": "NodeDto"
        }),
        "PathDto": genericMapper(PathDto, {
            "CurrentNode": "NodeDto",
            "Successors": "NodeDto"
        }),
        "InitialValuationArgumentsDto": genericMapper(InitialValuationArgumentsDto),
        "InitialValuationResultsDto": genericMapper(InitialValuationResultsDto, {
            "Arguments": "InitialValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "VehicleInformation": "VehicleInformationDto",
            "Tables": "PriceTablesDto",
            "Options": "OptionDto",
            "Color": "ColorDto"
        }),
        "AdjustedValuationArgumentsDto": genericMapper(AdjustedValuationArgumentsDto, {
            "VehicleConfiguration": "VehicleConfigurationDto"
        }),
        "AdjustedValuationResultsDto": genericMapper(AdjustedValuationResultsDto, {
            "Arguments": "AdjustedValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Tables": "PriceTablesDto"
        }),
        "PriceTablesDto": genericMapper(PriceTablesDto, {
            "Retail": "PriceTableDto",
            "Wholesale": "PriceTableDto",
            "TradeIn": "PriceTableDto",
            "FinanceAdvance": "PriceTableDto"
        }),
        "PriceTableDto": genericMapper(PriceTableDto, {
            "Rows": "PriceTableRowDto"
        }),
        "PriceTableRowDto": function(json) {
            var result = new PriceTableRowDto();
            for (var i in json) {
                if (!json.hasOwnProperty(i)) continue;
                if (i == 'Prices') {
                    result.Prices = DataMapper.PricesDto(json[i]);
                } else if (i == 'RowType') {
                    switch (json[i]) {
                        case 1:
                            result.RowType = PriceTableRowTypeDto.Base;
                            break;
                        case 2:
                            result.RowType = PriceTableRowTypeDto.Option;
                            break;
                        case 3:
                            result.RowType = PriceTableRowTypeDto.Mileage;
                            break;
                        case 4:
                            result.RowType = PriceTableRowTypeDto.Final;
                            break;
                        default:
                            throw "Bad RowType";
                    }
                }
            }
            return result;
        },
        "PricesDto": genericMapper(PricesDto),
        "OptionDto": genericMapper(OptionDto, {
            "Prices": "PricesDto"
        }),
        "VehicleInformationDto": genericMapper(VehicleInformationDto),
        "StateDto": genericMapper(StateDto),
        "ColorDto": genericMapper(ColorDto),
        "VehicleConfigurationDto": genericMapper(VehicleConfigurationDto, {
            "BookDate": "BookDateDto",
            "OptionStates": "OptionStateDto",
            "OptionActions": "OptionActionDto"
        }),
        "BookDateDto": genericMapper(BookDateDto),
        "OptionStateDto": genericMapper(OptionStateDto),
        "OptionActionDto": genericMapper(OptionActionDto),
        /* publication api */
        "PublicationListArgumentsDto": genericMapper(PublicationListArgumentsDto),
        "PublicationListResultsDto": genericMapper(PublicationListResultsDto, {
            "Arguments": "PublicationListArgumentsDto",
            "Publications": "PublicationInfoDto"
        }),
        "PublicationOnDateArgumentsDto": genericMapper(PublicationOnDateArgumentsDto),
        "PublicationOnDateResultsDto": genericMapper(PublicationOnDateResultsDto, {
            "Arguments": "PublicationOnDateArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationLoadArgumentsDto": genericMapper(PublicationLoadArgumentsDto),
        "PublicationLoadResultsDto": genericMapper(PublicationLoadResultsDto, {
            "Arguments": "PublicationLoadArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationSaveArgumentsDto": genericMapper(PublicationSaveArgumentsDto),
        "PublicationSaveResultsDto": genericMapper(PublicationSaveResultsDto, {
            "Arguments": "PublicationSaveArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "UserDto": genericMapper(UserDto),
        "EditionDto": genericMapper(EditionDto, {
            "User": "UserDto"
        }),
        "PublicationInfoDto": genericMapper(PublicationInfoDto, {
            "Edition": "EditionDto"
        }),
        "PublicationDto": genericMapper(PublicationDto, {
            "Edition": "EditionDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "VehicleInformation": "VehicleInformationDto",
            "Tables": "PriceTablesDto",
            "Options": "OptionDto",
            "Colors": "ColorDto"
        })
    };

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(BlackBook).trigger("BlackBook.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "States": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/States", StatesResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.StatesArgumentsDto"
                }
            });
        }),
        "Traversal": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/Traversal", TraversalResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin),
                    "State": (this.State == null ? '' : this.State)
                }
            });
        }),
        "Successor": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/Successor", SuccessorResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
                    "Node": this.Node,
                    "Successor": this.Successor
                }
            });
        }),
        "InitialValuation": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/InitialValuation", InitialValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin),
                    "Uvc": this.Uvc,
                    "State": (this.State == null ? '' : this.State),
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage : 0),
                    "HasMileage": ((/^\d+$/.test(this.Mileage)) ? true : false)
                }
            });
        }),
        "AdjustedValuation": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/AdjustedValuation", AdjustedValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto",
                    "State": (this.State == null ? '' : this.State),
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage : 0),
                    "HasMileage": ((/^\d+$/.test(this.Mileage)) ? true : false),
                    "OptionAction": this.OptionAction,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        }),
        "PublicationList": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/PublicationList", PublicationListResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications.PublicationListArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "PublicationOnDate": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/PublicationOnDate", PublicationOnDateResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications.PublicationOnDateArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "On": this.On
                }
            });
        }),
        "PublicationLoad": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/PublicationLoad", PublicationLoadResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications.PublicationLoadArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "Id": this.Id
                }
            });
        }),
        "PublicationSave": genericService("/VehicleValuationGuide/Services/BlackBook.asmx/PublicationSave", PublicationSaveResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        })
    };

    var Events = {
        "StatesArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(BlackBook).trigger("BlackBook.StatesLoaded", [data.States, State.State]);
            }
        },
        "TraversalArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var hasUvc = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.Uvc;
                if (hasUvc) {
                    $(BlackBook).trigger("BlackBook.UvcChange", [data.Path.CurrentNode.Uvc]);
                }
                if (!hasUvc || this.RunNodeTemplate) {
                    $(BlackBook).trigger("BlackBook.TraversalLoaded", data);
                }
                $(BlackBook).trigger("BlackBook.NodeTemplateBuilder.init", [data.Path]);
            }
        },
        "SuccessorArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(BlackBook).trigger("BlackBook.SuccessorLoaded", data);
            }
        },
        "InitialValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // remember configuration
                State.VehicleConfiguration = data.VehicleConfiguration;
                // update page
                $(BlackBook).trigger("BlackBook.InitialValueLoaded", data);
            },
            stateChange: function() {
                if (State.Uvc != null && State.Uvc != '') {
                    var valuation = new InitialValuationArgumentsDto();
                    valuation.Uvc = State.Uvc;
                    valuation.Vin = State.Vin;
                    valuation.Mileage = State.Mileage;
                    valuation.State = State.State;
                    valuation.HasMileage = State.Mileage != null;
                    $(valuation).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "AdjustedValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // remember configuration
                State.VehicleConfiguration = data.VehicleConfiguration;
                // update page
                $(BlackBook).trigger("BlackBook.AdjustedValueLoaded", data);
            },
            stateChange: function() {
                if (State.Uvc != null && State.Uvc != '') {
                    var valuation = new AdjustedValuationArgumentsDto();
                    valuation.VehicleConfiguration = State.VehicleConfiguration;
                    valuation.State = State.State;
                    valuation.Mileage = State.Mileage;
                    valuation.HasMileage = State.Mileage != null;
                    valuation.OptionAction = this.OptionAction || null;
                    $(valuation).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "PublicationListArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(BlackBook).trigger("BlackBook.PublicationListLoaded", data);
            }
        },
        "PublicationOnDateArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                if (data.Publication.VehicleConfiguration) {
                    State.Uvc = data.Publication.VehicleConfiguration.Uvc;
                    State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                }
                $(BlackBook).trigger("BlackBook.PublicationLoaded", data);
            }
        },
        "PublicationLoadArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                State.Uvc = data.Publication.VehicleConfiguration.Uvc;
                State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                $(BlackBook).trigger("BlackBook.PublicationLoaded", data);
            }
        },
        "PublicationSaveArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(BlackBook).trigger("BlackBook.PublicationSaved", data);
            }
        }
    };

    var EventBinder = function(obj) {
        var type = EventBinder.getType(obj);
        if (!!Events[type]) {
            var events = Events[type];
            for (var e in events) {
                if (!events.hasOwnProperty(e)) continue;
                $(obj).bind(e, events[e]); // TODO: events need to be framework independent
            }
        }
    };
    EventBinder.map = {
        'StatesArgumentsDto': StatesArgumentsDto,
        'TraversalArgumentsDto': TraversalArgumentsDto,
        'SuccessorArgumentsDto': SuccessorArgumentsDto,
        'InitialValuationArgumentsDto': InitialValuationArgumentsDto,
        'AdjustedValuationArgumentsDto': AdjustedValuationArgumentsDto,
        'PublicationListArgumentsDto': PublicationListArgumentsDto,
        'PublicationOnDateArgumentsDto': PublicationOnDateArgumentsDto,
        'PublicationLoadArgumentsDto': PublicationLoadArgumentsDto,
        'PublicationSaveArgumentsDto': PublicationSaveArgumentsDto
    };
    EventBinder.getType = function(obj) {
        for (var type in EventBinder.map) {
            if (!EventBinder.map.hasOwnProperty(type)) continue;
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(StatesArgumentsDto, DataMapper.StatesArgumentsDto);
        BindDataMapper(StatesResultsDto, DataMapper.StatesResultsDto);

        BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
        BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

        BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
        BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

        BindDataMapper(NodeDto, DataMapper.NodeDto);
        BindDataMapper(PathDto, DataMapper.PathDto);

        BindDataMapper(InitialValuationArgumentsDto, DataMapper.InitialValuationArgumentsDto);
        BindDataMapper(InitialValuationResultsDto, DataMapper.InitialValuationResultsDto);

        BindDataMapper(AdjustedValuationArgumentsDto, DataMapper.AdjustedValuationArgumentsDto);
        BindDataMapper(AdjustedValuationResultsDto, DataMapper.AdjustedValuationResultsDto);

        BindDataMapper(PriceTablesDto, DataMapper.PriceTablesDto);
        BindDataMapper(PriceTableDto, DataMapper.PriceTableDto);
        BindDataMapper(PriceTableRowDto, DataMapper.PriceTableRowDto);
        BindDataMapper(PricesDto, DataMapper.PricesDto);
        BindDataMapper(OptionDto, DataMapper.OptionDto);
        BindDataMapper(VehicleInformationDto, DataMapper.VehicleInformationDto);
        BindDataMapper(StateDto, DataMapper.StateDto);
        BindDataMapper(ColorDto, DataMapper.ColorDto);

        BindDataMapper(PublicationListArgumentsDto, DataMapper.PublicationListArgumentsDto);
        BindDataMapper(PublicationListResultsDto, DataMapper.PublicationListResultsDto);

        BindDataMapper(PublicationOnDateArgumentsDto, DataMapper.PublicationOnDateArgumentsDto);
        BindDataMapper(PublicationOnDateResultsDto, DataMapper.PublicationOnDateResultsDto);

        BindDataMapper(PublicationLoadArgumentsDto, DataMapper.PublicationLoadArgumentsDto);
        BindDataMapper(PublicationLoadResultsDto, DataMapper.PublicationLoadResultsDto);

        BindDataMapper(PublicationSaveArgumentsDto, DataMapper.PublicationSaveArgumentsDto);
        BindDataMapper(PublicationSaveResultsDto, DataMapper.PublicationSaveResultsDto);

        BindDataMapper(UserDto, DataMapper.UserDto);
        BindDataMapper(EditionDto, DataMapper.EditionDto);
        BindDataMapper(PublicationDto, DataMapper.PublicationDto);
        BindDataMapper(PublicationInfoDto, DataMapper.PublicationInfoDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
        }

        BindFetch(StatesArgumentsDto, Services.States);

        BindFetch(TraversalArgumentsDto, Services.Traversal);
        BindFetch(SuccessorArgumentsDto, Services.Successor);

        BindFetch(InitialValuationArgumentsDto, Services.InitialValuation);
        BindFetch(AdjustedValuationArgumentsDto, Services.AdjustedValuation);

        BindFetch(PublicationListArgumentsDto, Services.PublicationList);
        BindFetch(PublicationOnDateArgumentsDto, Services.PublicationOnDate);
        BindFetch(PublicationLoadArgumentsDto, Services.PublicationLoad);
        BindFetch(PublicationSaveArgumentsDto, Services.PublicationSave);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selected) {
        this.State = State; /// TODO: refactor so this can is not a single global, PM
        if (typeof selected === "undefined") {
            selected = {};
        }

        if (selected.buid) {
            $(BlackBook).trigger("BlackBook.BrokerChange", selected.buid);
        }

        if (selected.state) {
            State.State = selected.state;
        }
        if (selected.mileage) {
            State.Mileage = selected.mileage;
        }

        /// We always want to select states, PM
        $(new StatesArgumentsDto()).trigger("fetch"); // TODO: events need to be framework independent

        if (selected.vuid) {
            $(BlackBook).trigger("BlackBook.VehicleChange", selected.vuid);
        } else {
            $(BlackBook).trigger("BlackBook.Start", selected);
        }

        State.Vin = selected.vin || State.Vin;
        State.Uvc = selected.uvc || State.Uvc;
    };

    this.raises = this.raises || [];
    this.raises.push("BlackBook.StatesLoaded");
    this.raises.push("BlackBook.TraversalLoaded");
    this.raises.push("BlackBook.SuccessorLoaded");
    this.raises.push("BlackBook.InitialValueLoaded");
    this.raises.push("BlackBook.AdjustedValueLoaded");
    this.raises.push("BlackBook.PublicationListLoaded");
    this.raises.push("BlackBook.PublicationLoaded");
    this.raises.push("BlackBook.PublicationSaved");
    this.raises.push("BlackBook.ErrorState");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("BlackBook.MileageChange");
    this.listensTo.push("BlackBook.StateChange");
    this.listensTo.push("BlackBook.VinChange");
    this.listensTo.push("BlackBook.UvcChange");
    this.listensTo.push("BlackBook.AddOption");
    this.listensTo.push("BlackBook.RemoveOption");
    this.listensTo.push("BlackBook.SelectionChange");
    this.listensTo.push("BlackBook.LoadPublicationList");
    this.listensTo.push("BlackBook.LoadPublicationOnDate");
    this.listensTo.push("BlackBook.LoadPublication");
    this.listensTo.push("BlackBook.SavePublication");


    var PublicEvents = {
        "BlackBook.MileageChange": function(evt, newMileage) {
            if (State.Mileage !== newMileage) {
                State.Mileage = newMileage;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
            }
        },
        "BlackBook.StateChange": function(evt, newState) {
            if (State.State !== newState) {
                State.State = newState;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
            }
        },
        "BlackBook.VinChange": function (evt, newVin, runNodeTemplate) {
            if (State.Vin !== newVin) {
                State.Vin = newVin;
                State.Uvc = '';

                if (newVin !== '') {
                    var traversal = new TraversalArgumentsDto();
                    traversal.Vin = State.Vin;
                    traversal.State = State.State;
                    traversal.RunNodeTemplate = runNodeTemplate || false;
                    $(traversal).trigger('fetch'); // TODO: events need to be framework independent
                }
            }
        },
        "BlackBook.UvcChange": function(evt, newUvc) {
            State.Uvc = newUvc;
            if (newUvc !== "") {
                var initial = new InitialValuationArgumentsDto();
                $(initial).trigger("stateChange");
            };
        },
        "BlackBook.AddOption": function(evt, optionName) {
            var action = new OptionActionDto();
            action.ActionType = OptionActionTypeDto.Add;
            action.Uoc = optionName;
            $(new AdjustedValuationArgumentsDto(action)).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "BlackBook.RemoveOption": function(evt, optionName) {
            var action = new OptionActionDto();
            action.ActionType = OptionActionTypeDto.Remove;
            action.Uoc = optionName;
            $(new AdjustedValuationArgumentsDto(action)).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "BlackBook.SelectionChange": function(evt, next, items) {
            if (items && items.length > 0 && items[0].Uvc !== "") {
                $(BlackBook).trigger("BlackBook.UvcChange", next);
            } else {
                var successor = new SuccessorArgumentsDto();
                successor.Successor = next;
                $(successor).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "BlackBook.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
            State.Vehicle = '';
        },
        "BlackBook.VehicleChange": function(evt, newVehicle) {
            State.Vehicle = newVehicle;
        },
        "BlackBook.LoadPublicationList": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publicationList = new PublicationListArgumentsDto();
                publicationList.Broker = State.Broker;
                publicationList.Vehicle = State.Vehicle;
                $(publicationList).trigger("fetch");
            }
        },
        "BlackBook.LoadPublicationOnDate": function(evt, theDate) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationOnDateArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.On = theDate;
                $(publication).trigger("fetch");
            }
        },
        "BlackBook.LoadPublication": function(evt, theId) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationLoadArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.Id = theId;
                $(publication).trigger("fetch");
            }
        },
        "BlackBook.SavePublication": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '' && State.VehicleConfiguration !== null) {
                var publication = new PublicationSaveArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.VehicleConfiguration = State.VehicleConfiguration;
                $(publication).trigger("fetch");
            }
        },
        "BlackBook.Start": function(evt, values) {
            values.vin = values.vin || State.Vin;
            values.uvc = values.uvc || State.Uvc;

            if (values.vin) {
                /// We have a vin so start with the entered vin, PM
                State.Vin = '';
                $(BlackBook).trigger("BlackBook.VinChange", values.vin);
            } else if (values.uvc) {
                /// Holy shit we have a style already... lets just get that, PM
                State.Uvc = '';
                $(BlackBook).trigger("BlackBook.UvcChange", selected.uvc);
            } else {
                /// Well we don't know a damn thing about the vehicle, lets provide the
                /// starting point for a selection, PM
                $(new TraversalArgumentsDto()).trigger("fetch");
            }
        }
    };
    $(BlackBook).bind(PublicEvents);
    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.FormatPriceTableTypeRow = PriceTableRowTypeDto.toString;
    this.FormatTraversalStatus = TraversalStatusDto.toString;

}).apply(BlackBook);

