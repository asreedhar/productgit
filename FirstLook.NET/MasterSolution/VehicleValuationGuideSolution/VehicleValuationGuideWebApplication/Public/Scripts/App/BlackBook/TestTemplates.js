if (typeof(BlackBook) !== "function") {
    var BlackBook = function() {};
}

(function() {
    this.raises = this.raises || [];
    this.raises.push("BlackBook.StateChange");
    this.raises.push("BlackBook.MileageChange");
    this.raises.push("BlackBook.VinChange");
    this.raises.push("BlackBook.SelectionChange");
    this.raises.push("BlackBook.AddOption");
    this.raises.push("BlackBook.RemoveOption");
    this.raises.push("BlackBook.LoadPublicationList");
    this.raises.push("BlackBook.LoadPublicationOnDate");
    this.raises.push("BlackBook.LoadPublication");
    this.raises.push("BlackBook.SavePublication");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("BlackBook.StatesLoaded");
    this.listensTo.push("BlackBook.TraversalLoaded");
    this.listensTo.push("BlackBook.SuccessorLoaded");
    this.listensTo.push("BlackBook.InitialValueLoaded");
    this.listensTo.push("BlackBook.AdjustedValueLoaded");
    this.listensTo.push("BlackBook.PublicationListLoaded");
    this.listensTo.push("BlackBook.PublicationLoaded");
    this.listensTo.push("BlackBook.PublicationSaved");

    var PublicEvents = {
        "BlackBook.StatesLoaded": function(evt, states) {
            var template = new AdjustmentTemplateBuilder(states);
            template.clean();
            template.init();
        },
        "BlackBook.TraversalLoaded": function(evt, data) {
            var traversalTemplateBuilder = new NodeTemplateBuilder(data.Path, data.Status);
            traversalTemplateBuilder.clean();
            traversalTemplateBuilder.init();
        },
        "BlackBook.SuccessorLoaded": function(evt, data) {
            var successorTemplateBuilder = new NodeTemplateBuilder(data.Path, 0);
            successorTemplateBuilder.clean();
            successorTemplateBuilder.init();
        },
        "BlackBook.InitialValueLoaded": function(evt, data) {
            var initialTemplateBuilder = new InitialTemplateBuilder(data);
            initialTemplateBuilder.clean();
            initialTemplateBuilder.init();
        },
        "BlackBook.AdjustedValueLoaded": function(evt, data) {
            var adjustedTemplateBuilder = new AdjustedTemplateBuilder(data);
            adjustedTemplateBuilder.clean();
            adjustedTemplateBuilder.init();
        },
        "BlackBook.PublicationListLoaded": function(evt, data) {
            var template = new PublicationListTemplateBuilder(data.Publications); ;
            template.clean();
            template.init();
        },
        "BlackBook.PublicationLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        },
        "BlackBook.PublicationSaved": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        }
    };
    $(BlackBook).bind(PublicEvents);

    var AdjustmentTemplateBuilder = function(data, state) {
        this.data = data;
        this.$dom = {
            State: $('#state'),
            Mileage: $('#mileage'),
            // publication api
            Form: $('#subject'),
            Save: $('#save'),
            Broker: $('#broker'),
            Vehicle: $('#vehicle'),
            PublicationDate: $('#publication_date')
        };
    };

    AdjustmentTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var list = this.data;
            var select = this.$dom.State;
            var national = $('<option></option>').val('').html('National');
            select.append(national);
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var state = $('<option></option>').val(item.Code).html(item.Name);
                select.append(state);
            }
            select.bind('change', $.proxy(this.stateChange, this));
            var input = this.$dom.Mileage;
            input.unbind('change');
            input.bind('change', $.proxy(this.mileageChange, this));
            // broker / vehicle
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVehicleOrDate, this));
        },
        clean: function() {
            this.$dom.State.html('').unbind('change');
            this.$dom.Mileage.unbind('change');
        },
        stateChange: function(evt) {
            $(BlackBook).trigger("BlackBook.StateChange", [$(evt.target).val()]);
        },
        mileageChange: function(evt) {
            $(BlackBook).trigger("BlackBook.MileageChange", [$(evt.target).val()]);
        },
        changeBrokerOrVehicleOrDate: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                e = document.getElementById(n.substring(1)),
                v = (e == null ? null : e.value),
                c = null;
                if (n == '#broker') {
                    $(BlackBook).trigger("BlackBook.BrokerChange", v);
                    this.$dom.Vehicle.val('');
                }
                else
                    if (n == '#vehicle') {
                    $(BlackBook).trigger("BlackBook.VehicleChange", v);
                    $(BlackBook).trigger("BlackBook.LoadPublicationList");
                }
                else
                    if (n == '#publication_date') {
                    $(BlackBook).trigger("BlackBook.LoadPublicationOnDate", v);
                }
                else
                    if (n == '#save') {
                    $(BlackBook).trigger("BlackBook.SavePublication");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };
    var NodeTemplateBuilder = function(data, status) {
        this.data = data;
        this.status = status;
        this.$el = $("#criteria");
    };

    NodeTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var bindDropDown = function(s, p, items, selected) {
                var prompt = $('<option></option>').val('').html('... Please Select ...');
                var isAlone = items.length === 1;
                s.html('');
                s.append(prompt);
                if (typeof selected === "undefined" && isAlone) {
                    selected = items[0].Value; // Select a lone item
                }
                for (i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    var val = item.State;
                    if (item.Uvc != null && item.Uvc != '') {
                        val = item.Uvc;
                    }
                    var opt = $('<option></option>').val(val).html(item.Value);
                    if (item.Value == selected) {
                        opt.attr('selected', 'selected');

                    }
                    s.append(opt);
                }
                function fetchSuccessor(evt) {
                    var next = s.val();
                    if (next != '') {
                        // empty other drop downs
                        var inputs = ['year', 'make', 'model', 'series', 'body_style', 'synthetic'].slice(p);
                        for (var i = 0; i < inputs.length; i++) {
                            document.getElementById(inputs[i]).options.length = 0;
                        }
                        // reset vin
                        if (inputs.length > 0) {
                            $(BlackBook).trigger("BlackBook.VinChange", '');
                            $('#vin').val('');
                        }
                        // request data
                        $(BlackBook).trigger("BlackBook.SelectionChange", [next, items]);
                    }
                }
                s.unbind();
                s.bind('change', fetchSuccessor);
            };
            var bindQueryByVin = function() {
                var decode = $('#BlackBookForm');
                var decoder = function(evt) {
                    $(BlackBook).trigger("BlackBook.VinChange", [$("#vin").val(), true]);
                    evt.preventDefault();
                };
                decode.unbind('submit', decoder);
                decode.bind('submit', decoder);
            };
            var bindNode = function(node, successors, selected) {
                var select = null;
                var position = 0;
                //var valuation = successors.length > 0 && !!successors[0].Uvc;
                switch (node.Label) {
                    case '':
                        select = $("#year");
                        position = 1;
                        bindQueryByVin();
                        break;
                    case 'Year':
                        select = $("#make");
                        position = 2;
                        break;
                    case 'Make':
                        select = $("#model");
                        position = 3;
                        break;
                    case 'Model':
                        select = $("#series");
                        position = 4;
                        break;
                    case 'Series':
                        select = $("#body_style");
                        position = 5;
                        break;
                    case 'Vin':
                        if (node.Parent.Label == 'Series') {
                            select = $("#body_style");
                            position = 5;
                        }
                        else {
                            var select = $('#synthetic');
                            var p = $('<option></option>').val('').html('... Please Select ...');
                            select.append(p);
                            for (var j = 0; j < successors.length; j++) {
                                var s = successors[j];
                                var u = s.Uvc;
                                var t = [];
                                while (s != null) {
                                    t.push(s.Value);
                                    s = s.Parent;
                                }
                                var n = t.reverse().join(' ');
                                var o = $('<option></option>').val(u).html(n);
                                select.append(o);
                            }
                            select.unbind();
                            select.bind('change', function(evt) {
                                $(BlackBook).trigger("BlackBook.SelectionChange", [$(select).val(), successors]);
                            });
                            // clean other drop-downs
                            $('#series').html('');
                            $('#style').html('');
                        }
                        break;
                }
                if (position > 0) {
                    bindDropDown(select, position, successors, selected);
                }
            };
            var bindNodeRecursive = function(node, successors, selected) {
                if (node.Parent != null) {
                    bindNodeRecursive(node.Parent, node.Parent.Children, node.Value);
                }
                bindNode(node, successors, selected);
            };
            if (this.status == 0) {
                // structure: '' > 'year' > 'make' > 'model' > 'series' > [> 'vin'] > 'body style'
                if (this.data.CurrentNode.Label == 'Vin') {
                    bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    bindNode(this.data.CurrentNode, this.data.Successors);
                }
                $("#traversal_status").html('');
            }
            else {
                $("#traversal_status").html(BlackBook.FormatTraversalStatus(this.status));
            }
        },
        clean: function() {
            $("#results").find("div").empty();
        }
    };

    var InitialTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $("#results");
    };

    InitialTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._vehicleInformation();
            this._options();
            this._tables();
            this._colors();
        },
        clean: function() {
            //this.$el.html('');
        },
        _vehicleInformation: function() {
            var vehicle = this.data.VehicleInformation;
            for (var i in vehicle) {
                var el = null;
                switch (i) {
                    case 'Uvc':
                        el = 'uvc';
                        break;
                    case 'Msrp':
                        el = 'msrp';
                        break;
                    case 'FinanceAdvance':
                        el = 'finance_advance';
                        break;
                    case 'WheelBase':
                        el = 'wheel_base';
                        break;
                    case 'TaxableHorsepower':
                        el = 'taxable_horsepower';
                        break;
                    case 'Weight':
                        el = 'weight';
                        break;
                    case 'TireSize':
                        el = 'tire_size';
                        break;
                    case 'BaseHorsepower':
                        el = 'base_horsepower';
                        break;
                    case 'FuelType':
                        el = 'fuel_type';
                        break;
                    case 'Cylinders':
                        el = 'cylinders';
                        break;
                    case 'DriveTrain':
                        el = 'drive_train';
                        break;
                    case 'Transmission':
                        el = 'transmission';
                        break;
                    case 'Engine':
                        el = 'engine';
                        break;
                    case 'VehicleClass':
                        el = 'vehicle_class';
                        break;
                }
                if (el != null) {
                    $('#' + el).html(vehicle[i]);
                }
            }
        },
        _options: function() {
            var info = $('#options');
            var hd = $("<div></div>");
            $(info).append(hd);
            var optionList = this.data.Options;
            var th = '<thead><tr><th></th><th>Option</th><th>Extra Clean</th><th>Clean</th><th>Average</th><th>Rough</th></tr></thead>';
            var tb = '<tbody>';
            for (var i = 0; i < optionList.length; i++) {
                var option = optionList[i];
                var tr = '<tr>' + '<td><input type="checkbox" name="' + option.Code + '" id="' + option.Code + '" /></td>' + '<td>' + option.Description + '</td>' + '<td>' + option.Prices.ExtraClean + '</td>' + '<td>' + option.Prices.Clean + '</td>' + '<td>' + option.Prices.Average + '</td>' + '<td>' + option.Prices.Rough + '</td>' + '</tr>';
                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + th + tb + '</table>');

            var f = function(evt) {
                if (evt.target) {
                    $(BlackBook).trigger(evt.target.checked ? "BlackBook.AddOption" : "BlackBook.RemoveOption", [evt.target.name]);
                }
            };

            $(hd).bind('change', f);

            this.$el.append(info);
        },
        _tables: function() {
            var info = $('#prices');
            info.find('div').empty();
            // three price tables ...
            $(info).append(this._table('Retail', this.data.Tables.Retail));
            $(info).append(this._table('Wholesale', this.data.Tables.Wholesale));
            $(info).append(this._table('Trade-In', this.data.Tables.TradeIn));
            $(info).append(this._table('Finance Advance', this.data.Tables.FinanceAdvance));
        },
        _table: function(name, table) {
            var hd = $("<div></div>");
            var th = '<thead><tr><th></th><th>Extra Clean</th><th>Clean</th><th>Average</th><th>Rough</th></tr></thead>';
            var tb = '<tbody>';
            var cp = '<caption>' + name + '</caption>';
            for (var i = 0; i < table.Rows.length; i++) {
                var tableRow = table.Rows[i];
                var tableRowType = BlackBook.FormatPriceTableTypeRow(tableRow.RowType);
                var px = name.toLowerCase() + '_' + tableRowType.toLowerCase();
                var tr = '<tr>' + '<td>' + tableRowType + '</td>' + '<td id="' + px + '_extra_clean">' + tableRow.Prices.ExtraClean + '</td>' + '<td id="' + px + '_clean">' + tableRow.Prices.Clean + '</td>' + '<td id="' + px + '_average">' + tableRow.Prices.Average + '</td>' + '<td id="' + px + '_rough">' + tableRow.Prices.Rough + '</td>' + '</tr>';
                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + cp + th + tb + '</table>');
            return hd;
        },
        _colors: function() {
            var info = $('#colors');
            var hd = $("<div></div>");
            var cps = ["Exterior", "Interior", "Leather"];
            var tbs = '';
            for (var ctr = 1; ctr < 4; ctr++) {
                var cp = '<caption>' + cps[ctr - 1] + '</caption>';
                var th = '<thead><tr><th>Name</th><th>Color 1</th><th>Color 2</th><th>Color 3</th></tr></thead>';
                var tb = '<tbody>';
                for (var i = 0; i < this.data.Colors.length; i++) {
                    var color = this.data.Colors[i];
                    if (color.ColorType != ctr) {
                        continue;
                    }
                    var tr = '<tr>' + '<td>' + color.Description + '</td>' + '<td style="background-color:' + color.Swatches[0] + '">&nbsp;</td>' + '<td style="background-color:' + color.Swatches[1] + '">&nbsp;</td>' + '<td style="background-color:' + color.Swatches[2] + '">&nbsp;</td>' + '</tr>';
                    tb += tr;
                }
                tb += '</tbody>';
                tbs += '<table>' + cp + th + tb + '</table>';
            }
            $(hd).html(tbs);
            $(info).append(hd)
            this.$el.append(info);
        }
    };

    var AdjustedTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $("#prices");
    };

    AdjustedTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._tables();
        },
        clean: function() {
            this.$el.find('div').remove();
        }
    };

    AdjustedTemplateBuilder.prototype._tables = InitialTemplateBuilder.prototype._tables;

    AdjustedTemplateBuilder.prototype._table = InitialTemplateBuilder.prototype._table;

    var PublicationListTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#publications'),
            Body: $('#publications_body')
        };
    };

    PublicationListTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();
        },
        clean: function() {
            this.$dom.Body.empty();
            this.$dom.Form.unbind('click');
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                + '<td>' + item.Edition.BeginDate + '</td>'
                + '<td>' + item.Edition.EndDate + '</td>'
                + '<td>' + item.ChangeType + '</td>'
                + '<td>' + item.ChangeAgent + '</td>'
                + '<td>' + item.Edition.User.UserName + '</td>'
                + '<td><a href="#' + item.Id + '">Load</a></td>'
                + '</tr>';
                code += tr;
            }
            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.selectPublication, this));
        },
        selectPublication: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    p = tgt.href.substring(i + 1);
                $(BlackBook).trigger("BlackBook.LoadPublication", p);
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    this.AdjustmentTemplateBuilder = AdjustmentTemplateBuilder;
    this.AdjustedTemplateBuilder = AdjustedTemplateBuilder;
    this.NodeTemplateBuilder = NodeTemplateBuilder;
}).apply(BlackBook);

