if (typeof BlackBook === "undefined") {
    BlackBook = {};
} (function() {
    this.raises = this.raises || [];
    this.raises.push("BlackBook.StateChange");
    this.raises.push("BlackBook.MileageChange");
    this.raises.push("BlackBook.VinChange");
    this.raises.push("BlackBook.UvcChange");
    this.raises.push("BlackBook.SelectionChange");
    this.raises.push("BlackBook.AddOption");
    this.raises.push("BlackBook.RemoveOption");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("BlackBook.StatesLoaded");
    this.listensTo.push("BlackBook.TraversalLoaded");
    this.listensTo.push("BlackBook.SuccessorLoaded");
    this.listensTo.push("BlackBook.InitialValueLoaded");
    this.listensTo.push("BlackBook.AdjustedValueLoaded");

    var State = {
        States: [],
        CurrentState: ""
    };

    var PublicEvents = {
        "BlackBook.StatesLoaded": function(evt, states, currentState) {
            State.States = _.reduce(states, function(memo, state) {
                memo.push({
                    name: state.Name,
                    value: state.Code
                });
                return memo;
            },
            []);
            State.CurrentState = currentState;
        },
        "BlackBook.TraversalLoaded": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");

            card.build(data);
        },
        "BlackBook.SuccessorLoaded": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");

            if (data.Path.CurrentNode && data.Path.CurrentNode.HasVehicleId) {
                $(BlackBook).trigger("BlackBook.UvcChange", data.Path.CurrentNode.VehicleId);
            } else {
                card.build(data);
            }
        },
        "BlackBook.InitialValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");

            data.States = State.States;

            card.build(data);
        },
        "BlackBook.AdjustedValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");

            $(card.views.values.EquipmentList).trigger("selection_set", [data.VehicleConfiguration.OptionStates]);

            $(card.views.values.Carousel).trigger("rebuild", [data]);
            $(card.views.values.FinanceAdvance).trigger("rebuild", [data]);
        },
        /// Component Bridge Events
        "BlackBook.SelectionPathChange": function(evt, data) {
            if (data.Uvc !== "") {
                $(BlackBook).trigger("BlackBook.UvcChange", data.Uvc);
            } else {
                $(BlackBook).trigger("BlackBook.SelectionChange", data.State);
            }
        },
        "BlackBook.SelectionChangeWithDecode": function(evt, data) {
            var newSelection = JSON.parse(data.NewBlackBookSelection);
            $(BlackBook).trigger("BlackBook.SelectionPathChange", newSelection);
        },
        "BlackBook.EquipmentItemSelected": function(evt, name, isSelected) {
            $(BlackBook).trigger(isSelected ? "BlackBook.AddOption": "BlackBook.RemoveOption", [name]);
        },
        "BlackBook.MileageOrStateChange": function(evt, data) {
            $(BlackBook).trigger("BlackBook.MileageChange", data.Mileage);
            $(BlackBook).trigger("BlackBook.StateChange", data.State);
        },
        "BlackBook.MileageChange": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("input[name=Mileage]").val(data);
            }
        },
        "BlackBook.StateChange": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("select[name=State]").val(data);
            }
        },
        "BlackBook.PublicationLoaded": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");

            if (data.Publication.VehicleConfiguration) {
                data.Publication.States = State.States;

                card.build(data.Publication);
            } else {
                $(BlackBook).trigger("BlackBook.Start", {});
            }
        },
        "BlackBook.VehicleChange": function(evt, data) {
            $(BlackBook).trigger("BlackBook.LoadPublicationOnDate", '2038-01-01');
        },
        "BlackBook.ErrorState": function(evt, data) {
            var card = FirstLook.cards.get("BlackBook");
            card.build(data);
        }
    };
    $(BlackBook).bind(PublicEvents);

    function tableFilter(market, title) {
        return function(data) {
            var table = [],
            data_table = data.Tables[market],
            row_finder = function(type) {
                return function(i) {
                    return i.RowType == type;
                };
            },
            row_builder = function(type, title) {
                return [title || type, _.moneyAdjusted(o.Prices[type]) + "<br />" + _.moneyAdjusted(m.Prices[type]), _.money(f.Prices[type])];
            },
            o = _.detect(data_table.Rows, row_finder(2)),
            m = _.detect(data_table.Rows, row_finder(3)),
            f = _.detect(data_table.Rows, row_finder(4));

            data.caption = title;

            table.head = [["", "Options<br />Mileage", "Final"]];

            table.push(row_builder("ExtraClean", "Extra Clean"));
            table.push(row_builder("Clean"));
            table.push(row_builder("Average"));
            table.push(row_builder("Rough"));

            table.header_col = 0;
            table.col_class_name = ["market", "adjustments", "final"];

            data.table = table;

            return data;
        };
    }

    function tableSummary(market, title) {
        return function(data) {
            var table = [],
            data_table = data.Tables[market],
            row_finder = function(type) {
                return function(i) {
                    return i.RowType == type;
                };
            },
            f = _.detect(data_table.Rows, row_finder(4));

            data.caption = title;

            table.head = [["Extra Clean", "Clean", "Average", "Rough"]];

            table.push([_.money(f.Prices["ExtraClean"]), _.money(f.Prices["Clean"]), _.money(f.Prices["Average"]), _.money(f.Prices["Rough"])]);

            table.col_class_name = ["extra-clean", "clean", "average", "rough"];

            data.table = table;

            return data;
        };
    }

    function make_table(key, title) {
        var pattern = {
            Tables: {}
        };
        pattern.Tables[key] = {
            Rows: [{
                Prices: {
                    ExtraClean: _.isNumber,
                    Clean: _.isNumber,
                    Average: _.isNumber,
                    Rough: _.isNumber
                }
            }]
        }
        return {
            type: "Table",
            pattern: pattern,
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: tableFilter(key, title),
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            body_header_col_accessor: "table.header_col"
        }
    }

    function make_summary(key, title) {
        var pattern = {
            Tables: {}
        };
        pattern.Tables[key] = {
            Rows: [{
                Prices: {
                    ExtraClean: _.isNumber,
                    Clean: _.isNumber,
                    Average: _.isNumber,
                    Rough: _.isNumber
                }
            }]
        }
        return {
            type: "Table",
            pattern: pattern,
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: tableSummary(key, title),
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            body_header_col_accessor: "table.header_col"
        }
    }

    function make_select() {
        return {
            type: "Fieldset",
            pattern: {
                Path: {
                    Successors: [{
                        Value: _.isString
                    }]
                }
            },
            behaviors: {
                submitable: {
                    event: "BlackBook.SelectionChangeWithDecode",
                    scope: BlackBook,
                    submit_on_change: true
                }
            },
            legend_accessor: "fieldset.legend",
            input_accessor: "fieldset.inputs",
            name_accessor: "name",
            button_accessor: "fieldset.buttons",
            label_accessor: "label",
            value_accessor: "value",
            type_accessor: "type",
            filter: function(data) {
                var options = _.map(data.Path.Successors, function(succ) {
                    var t = [];
                    var s = succ;
                    while (s != null) {
                        t.push(s.Value);
                        s = s.Parent;
                    }
                    var n = t.reverse().join(' ');
                    return {
                        name: n,
                        value: JSON.stringify({
                            State: succ.State,
                            Uvc: succ.Uvc
                        })
                    }
                }),
                fieldset = {
                    legend: "BlackBook " + data.Path.Successors[0].Label,
                    inputs: [{
                        name: "NewBlackBookSelection",
                        label: data.Path.Successors[0].Label,
                        options: options
                    }],
                    buttons: []
                }
                options.unshift({
                    name: "Select...",
                    value: ""
                });
                data.fieldset = fieldset;
                return data;
            }
        }
    }

    FirstLook.components.add(make_table("Retail", "Retail Values"), "BlackBook.RetailTable");
    FirstLook.components.add(make_table("Wholesale", "Wholesale Values"), "BlackBook.WholesaleTable");
    FirstLook.components.add(make_table("TradeIn", "TradeIn Values"), "BlackBook.TradeInTable");

    FirstLook.components.add(make_summary("Retail", "BlackBook Retail Values"), "BlackBook.RetailSummary");
    FirstLook.components.add(make_summary("Wholesale", "BlackBook Wholesale Values"), "BlackBook.WholesaleSummary");
    FirstLook.components.add(make_summary("TradeIn", "BlackBook TradeIn Values"), "BlackBook.TradeInSummary");
    FirstLook.components.add(make_select(), "BlackBook.Select");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='error'>Problem with the VIN</p>";
		}
	},
	"BlackBook.VinProblem");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='message'>No BlackBook Data For VIN</p>";
		}
	},
	"BlackBook.NoData");

    var state_patterns = {
        select: {
            Path: {
                Successors: _.isArray
            },
            Status: function(v) {
                return v === 0 || _.isUndefined(v); // Traversal is 0, Successor is undefined
            }
        },
        values: {
            VehicleConfiguration: _
        },
        vin_problem: {
			Status: function(v) {
				return v >= 1 && v <= 3;
			}
        },
        data_problem: {
			Status: function(v) {
				return v === 4;
			}
        },
        error: {
			errorType: _,
			errorResponse: _
        }
    };

    var overview_card = {
        pattern: _,
        view: {
            select: {
                pattern: state_patterns.select,
                select: FirstLook.components.get("BlackBook.Select")
            },
            values: {
                pattern: state_patterns.values,
                values: FirstLook.components.get("BlackBook.WholesaleSummary")
            },
            vin_problem: {
                pattern: state_patterns.vin_problem,
                message: FirstLook.components.get("BlackBook.VinProblem")
            },
            data_problem: {
                pattern: state_patterns.data_problem,
                message: FirstLook.components.get("BlackBook.NoData")
            },
            error: {
                pattern: state_patterns.error,
                ErrorMessage: FirstLook.components.get("Application.ServerError")
            }
        }
    };

    FirstLook.cards.add(overview_card, "BlackBookSummary");

    var cards = {
        title: "BlackBook",
        pattern: _,
        view: {
            select: {
                pattern: state_patterns.select,
                Title: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<h1 class='logo_black_book'>Black Book</h1>";
                    }
                },
                Review: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<p>"+_.pluck(data._selection, "Value").join(" | ")+"</p>";
                    },
                    filter: function(data) {
                        var selection = [];
                        function append_parent(v) {
                            selection.push(v);
                            if (v.Parent) {
                                append_parent(v.Parent);
                            }
                            return _.first(selection, selection.length-1);
                        }
                        
                        data._selection = append_parent(data.Path.CurrentNode);
                        return data;
                    }
                },
                TraversalList: {
                    type: "List",
                    class_name: "select_list vid_select",
                    pattern: {
                        Path: {
                        Successors: [{
                            Value: _.isString
                        }]
                        }
                    },
                    behaviors: {
                        selectable: {
                            pattern: {
                                Path: {
                                Successors: [{
                                    State: _.isString,
                                    Uvc: _
                                }]
                                }
                            },
                            data_accessor: ["State", "Uvc"],
                            binds: {
                                "item_selected": {
                                    type: "BlackBook.SelectionPathChange",
                                    scope: BlackBook
                                }
                            }
                        }
                    },
                    item_accessor: "Path.Successors",
                    text_accessor: "Value",
                    data_accessor: "Value"
                }
            },
            values: {
                pattern: state_patterns.values,
                ValueFieldSet: {
                    type: "Fieldset",
                    pattern: {
                        VehicleConfiguration: {
                            State: _.isString,
                            Mileage: _.isNumber
                        }
                    },
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "BlackBook.MileageOrStateChange",
                            scope: BlackBook,
                            submit_on_change: true
                        }
                    },
                    class_name: "book_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "BlackBook",
                        inputs: [{
                            name: "State",
                            label: "States",
                            data: "VehicleConfiguration.State",
                            options: "States"
                        },
                        {
                            name: "Mileage",
                            label: "Mileage",
                            data: "VehicleConfiguration.Mileage",
                            type: "tel"
                        }],
                        buttons: ["Update"]
                    }
                },
                Carousel: {
                    type: "Carousel",
                    pattern: _,
                    behaviors: {
                        carouselable: {}
                    },
                    components: ["BlackBook.WholesaleTable", "BlackBook.RetailTable", "BlackBook.TradeInTable"]
                },
                FinanceAdvance:  {
                    type: "Table",
                    pattern: {
                        Tables: {
                            FinanceAdvance:  _
                        }
                    },
                    behaviors: {
                        rebuildable: {}
                    },
                    class_name: "kbb_value_tables",
                    filter: function(data) {
                        var table = [],
                        data_table = data.Tables["FinanceAdvance"],
                        row_finder = function(type) {
                            return function(i) {
                                return i.RowType == type;
                            };
                        },
                        o = _.detect(data_table.Rows, row_finder(2));
                        f = _.detect(data_table.Rows, row_finder(4));

                        data.caption = "";

                        table.head = [["", "Options", "Final"]];

                        table.push(["Finance Advance", _.money(o.Prices["Average"]), _.money(f.Prices["Average"])]);

                        table.header_col = 0;
                        table.col_class_name = ["market", "options", "final"];

                        data.table = table;

                        return data;
                    },
                    caption_accessor: "caption",
                    body_accessor: "table",
                    head_accessor: "table.head",
                    col_class_name_accessor: "table.col_class_name",
                    body_header_col_accessor: "table.header_col"
                },
                EquipmentList: {
                    type: "List",
                    behaviors: {
                        multi_selectable: {
                            pattern: {
                                VehicleConfiguration: {
                                    OptionStates: [{
                                        Uoc: _,
                                        Selected: _.isBoolean,
                                        Enabled: _.isBoolean
                                    }]
                                }
                            },
                            multi_item_accessor: "VehicleConfiguration.OptionStates",
                            multi_data_accessor: "Uoc",
                            map_data_accessor: "Code",
                            selected_accessor: "Selected",
                            enabled_accessor: "Enabled",
                            binds: {
                                "item_selected": {
                                    scope: BlackBook,
                                    type: "BlackBook.EquipmentItemSelected"
                                }
                            }
                        }
                    },
                    pattern: {
                        Options: [{
                            Description: _,
                            Code: _
                        }]
                    },
                    class_name: "equipment_list",
                    item_accessor: "Options",
                    text_accessor: "Description",
                    data_accessor: "Code"
                }
            },
            vin_problem: {
                pattern: state_patterns.vin_problem,
                message: FirstLook.components.get("BlackBook.VinProblem")
            },
            data_problem: {
                pattern: state_patterns.data_problem,
                message: FirstLook.components.get("BlackBook.NoData")
            },
            error: {
                pattern: state_patterns.error,
                ErrorMessage: FirstLook.components.get("Application.ServerError")
            }
        }
    };

    FirstLook.cards.add(cards, "BlackBook");
}).apply(BlackBook);

