if (typeof(Edmunds) !== "function") {
    var Edmunds = function() { };
}

(function() {
    var ns = this;

    var State = {
        State: 'IL',
        Vin: null,
        StyleId: null,
        ColorId: null,
        GenericColorId: null,
        ManufacturerColorId: null,
        Mileage: null,
        VehicleConfiguration: null,
        /// Publication State
        Broker: '',
        Vehicle: ''
    };

    /* traversal dto */

    function PathDto() {
        this.CurrentNode = null;
        this.Successors = [];
    }

    function NodeDto() {
        this.Parent = null;
        this.Children = [];
        this.Label = "";
        this.Value = "";
        this.State = "";
        this.StyleId = 0;
        this.HasStyleId = false;
    }

    /* valuation dto */

    function DataLoadDto() {
        this.Id = null;
        this.Value = null;
    }
    
    function PriceTablesDto() {
        this.Retail = null;
        this.PrivateParty = null;
        this.TradeIn = null;
    }

    function PriceTableDto() {
        this.Rows = [];
    }

    function PriceTableRowDto() {
        this.RowType = null;
        this.Prices = null;
    }

    var PriceTableRowTypeDto = {
        "Undefined": 0,
        "Base": 1,
        "Option": 2,
        "Mileage": 3,
        "Color": 4,
        "Region": 5,
        "Condition": 6,
        "Final": 7,
        "toString": function(i) {
            var names = ["Undefined", "Base", "Option", "Mileage", "Color", "Region", "Condition",  "Final"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function PricesDto() {
        this.Outstanding = 0;
        this.Clean = 0;
        this.Average = 0;
        this.Rough = 0;
    }

    function OptionDto() {
        this.Id = 0;
        this.Name = '';
        this.Retail = 0;
        this.PrivateParty = 0;
        this.TradeIn = 0;
    }

    function StateDto() {
        this.Code = '';
        this.Name = '';
    }

    function GenericColorDto() {
        this.Id = 0;
        this.Name = '';
    }

    var ColorTypeDto = {
        "Undefined": 0,
        "Exterior": 1,
        "Interior": 2,
        "toString": function(i) {
            var names = ["Undefined", "Exterior", "Interior"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function ManufacturerColorDto() {
        this.GenericColor = null;
        this.Id = 0;
        this.Name = '';
        this.Code = '';
        this.Color = '';
        this.ColorType = 0;
    }

    function VehicleConfigurationDto() {
        this.DataLoad = null;
        this.StyleId = 0;
        this.Vin = "";
        this.State = null;
        this.Mileage = null;
        this.GenericColorId = null;
        this.ManufacturerColorId = null;
        this.HasMileage = false;
        this.HasGenericColorId = false;
        this.HasManufacturerColorId = false;
        this.OptionStates = [];
        this.OptionActions = [];
    }

    function OptionStateDto() {
        this.OptionId = "";
        this.Selected = false;
    }

    function OptionActionDto() {
        this.ActionType = 0;
        this.OptionId = "";
    }

    var OptionActionTypeDto = {
        "Undefined": 0,
        "Add": 1,
        "Remove": 2
    };

    var TraversalStatusDto = {
        "Success": 0,
        "InvalidVinLength": 1,
        "InvalidVinCharacters": 2,
        "InvalidVinChecksum": 3,
        "NoDataForVin": 4,
        "toString": function(i) {
            var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    /* publication dto */

    var ChangeAgentDto = {
        "Undefined": 0,
        "User": 1,
        "System": 2,
        "toString": function(i) {
            var names = ["Undefined", "User Change", "System Change"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    // TODO: Message from Simon to Paul
    // TODO: This is a list of the bit flags passed from the server.
    // TODO: END

    var ChangeTypeDto = {
        "None": 0,
        "BookDate": (1 << 0),
        "State": (1 << 1),
        "StyleId": (1 << 2),
        "Mileage": (1 << 3),
        "OptionActions": (1 << 4),
        "OptionStates": (1 << 5),
        "toString": function(i) {
            var names = ["None", "BookDate", "State", "StyleId", "Mileage", "Option Actions", "Option States"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function UserDto() {
        this.FirstName = null;
        this.LastName = null;
        this.UserName = null;
    }

    function EditionDto() {
        this.BeginDate = null;
        this.EndDate = null;
        this.User = null;
    }

    function PublicationInfoDto() {
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
    }

    function PublicationDto() {
        // from info
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
        // own properties
        this.VehicleConfiguration = null;
        this.Tables = null;
        this.Options = [];
        this.Colors = [];
    }

    /* envelopes dto */

    function StatesArgumentsDto() {
        EventBinder(this);
    }

    function StatesResultsDto() {
        this.Arguments = null;
        this.States = [];
    }

    function TraversalArgumentsDto() {
        this.Vin = "";
        EventBinder(this);
    }

    function TraversalResultsDto() {
        this.Arguments = null;
        this.Path = null;
        this.Status = 0;
    }

    function SuccessorArgumentsDto() {
        this.State = "";
        this.Node = "";
        this.Successor = "";
        EventBinder(this);
    }

    function SuccessorResultsDto() {
        this.Arguments = null;
        this.Path = null;
    }

    function InitialValuationArgumentsDto() {
        this.Vin = null;
        this.State = null;
        this.StyleId = 0;
        this.Mileage = null;
        this.GenericColorId = 0;
        this.ManufacturerColorId = 0;
        this.HasMileage = false;
        this.HasGenericColorId = false;
        this.HasManufacturerColorId = false;
        EventBinder(this);
    }

    function InitialValuationResultsDto() {
        this.Tables = null;
        this.Arguments = null;
        this.Options = [];
        this.Colors = [];
        this.VehicleConfiguration = null;
    }

    function AdjustedValuationArgumentsDto(action) {
        this.VehicleConfiguration = null;
        // updates to vehicle
        this.State = null;
        this.Mileage = null;
        this.GenericColorId = 0;
        this.ManufacturerColorId = 0;
        this.HasMileage = false;
        this.HasGenericColorId = false;
        this.HasManufacturerColorId = false;
        // or a change to the selected options
        this.OptionAction = action || null;
        EventBinder(this);
    }

    function AdjustedValuationResultsDto() {
        this.Tables = null;
        this.Arguments = null;
        this.VehicleConfiguration = null;
    }

    /* publication envelopes dto */

    function PublicationListArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        // -- (none) --
        // events please
        EventBinder(this);
    }

    function PublicationListResultsDto() {
        // self
        this.Arguments = null;
        this.Publications = [];
    }

    function PublicationOnDateArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.On = '';
        // events please
        EventBinder(this);
    }

    function PublicationOnDateResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    function PublicationLoadArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.Id = 0;
        // events please
        EventBinder(this);
    }

    function PublicationLoadResultsDto() {
        // self
        this.Arguments = '';
        this.Publication = '';
    }

    function PublicationSaveArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.VehicleConfiguration = null;
        // events please
        EventBinder(this);
    }

    function PublicationSaveResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    /* == END: Classes == */

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
			prop,
			subJson,
			idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "StatesArgumentsDto": genericMapper(StatesArgumentsDto),
        "StatesResultsDto": genericMapper(StatesResultsDto, {
            "Arguments": "StatesArgumentsDto",
            "States": "StateDto"
        }),
        "TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
        "TraversalResultsDto": genericMapper(TraversalResultsDto, {
            "Arguments": "TraversalArgumentsDto",
            "Path": "PathDto"
        }),
        "SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
        "SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
            "Arguments": "SuccessorArgumentsDto",
            "Path": "PathDto"
        }),
        "NodeDto": genericMapper(NodeDto, {
            "Parent": "NodeDto",
            "Children": "NodeDto"
        }),
        "PathDto": genericMapper(PathDto, {
            "CurrentNode": "NodeDto",
            "Successors": "NodeDto"
        }),
        "InitialValuationArgumentsDto": genericMapper(InitialValuationArgumentsDto),
        "InitialValuationResultsDto": genericMapper(InitialValuationResultsDto, {
            "Arguments": "InitialValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Tables": "PriceTablesDto",
            "Options": "OptionDto",
            "Colors": "ManufacturerColorDto"
        }),
        "AdjustedValuationArgumentsDto": genericMapper(AdjustedValuationArgumentsDto, {
            "VehicleConfiguration": "VehicleConfigurationDto"
        }),
        "AdjustedValuationResultsDto": genericMapper(AdjustedValuationResultsDto, {
            "Arguments": "AdjustedValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Tables": "PriceTablesDto"
        }),
        "PriceTablesDto": genericMapper(PriceTablesDto, {
            "Retail": "PriceTableDto",
            "Wholesale": "PriceTableDto",
            "TradeIn": "PriceTableDto"
        }),
        "PriceTableDto": genericMapper(PriceTableDto, {
            "Rows": "PriceTableRowDto"
        }),
        "PriceTableRowDto": function(json) {
            var result = new PriceTableRowDto();
            for (var i in json) {
                if (!json.hasOwnProperty(i)) continue;
                if (i == 'Prices') {
                    result.Prices = DataMapper.PricesDto(json[i]);
                } else if (i == 'RowType') {
                    switch (json[i]) {
                        case 1:
                            result.RowType = PriceTableRowTypeDto.Base;
                            break;
                        case 2:
                            result.RowType = PriceTableRowTypeDto.Option;
                            break;
                        case 3:
                            result.RowType = PriceTableRowTypeDto.Mileage;
                            break;
                        case 4:
                            result.RowType = PriceTableRowTypeDto.Color;
                            break;
                        case 5:
                            result.RowType = PriceTableRowTypeDto.Region;
                            break;
                        case 6:
                            result.RowType = PriceTableRowTypeDto.Condition;
                            break;
                        case 7:
                            result.RowType = PriceTableRowTypeDto.Final;
                            break;
                        default:
                            throw "Bad RowType";
                    }
                }
            }
            return result;
        },
        "PricesDto": genericMapper(PricesDto),
        "OptionDto": genericMapper(OptionDto),
        "DataLoadDto": genericMapper(DataLoadDto),
        "StateDto": genericMapper(StateDto),
        "GenericColorDto": genericMapper(GenericColorDto),
        "ManufacturerColorDto": genericMapper(ManufacturerColorDto, {
            "GenericColor": "GenericColorDto"
        }),
        "VehicleConfigurationDto": genericMapper(VehicleConfigurationDto, {
            "OptionStates": "OptionStateDto",
            "OptionActions": "OptionActionDto",
            "DataLoad": "DataLoadDto",
            "State": "StateDto"
        }),
        "OptionStateDto": genericMapper(OptionStateDto),
        "OptionActionDto": genericMapper(OptionActionDto),
        /* publication api */
        "PublicationListArgumentsDto": genericMapper(PublicationListArgumentsDto),
        "PublicationListResultsDto": genericMapper(PublicationListResultsDto, {
            "Arguments": "PublicationListArgumentsDto",
            "Publications": "PublicationInfoDto"
        }),
        "PublicationOnDateArgumentsDto": genericMapper(PublicationOnDateArgumentsDto),
        "PublicationOnDateResultsDto": genericMapper(PublicationOnDateResultsDto, {
            "Arguments": "PublicationOnDateArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationLoadArgumentsDto": genericMapper(PublicationLoadArgumentsDto),
        "PublicationLoadResultsDto": genericMapper(PublicationLoadResultsDto, {
            "Arguments": "PublicationLoadArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationSaveArgumentsDto": genericMapper(PublicationSaveArgumentsDto),
        "PublicationSaveResultsDto": genericMapper(PublicationSaveResultsDto, {
            "Arguments": "PublicationSaveArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "UserDto": genericMapper(UserDto),
        "EditionDto": genericMapper(EditionDto, {
            "User": "UserDto"
        }),
        "PublicationInfoDto": genericMapper(PublicationInfoDto, {
            "Edition": "EditionDto"
        }),
        "PublicationDto": genericMapper(PublicationDto, {
            "Edition": "EditionDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Tables": "PriceTablesDto",
            "Options": "OptionDto",
            "Colors": "ManufacturerColorDto"
        })
    };

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Edmunds).trigger("Edmunds.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "States": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/States", StatesResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.StatesArgumentsDto"
                }
            });
        }),
        "Traversal": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/traversal", TraversalResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
                    "Vin": (this.Vin === null ? '' : this.Vin)
                }
            });
        }),
        "Successor": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/Successor", SuccessorResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
                    "Node": this.Node,
                    "Successor": this.Successor
                }
            });
        }),
        "InitialValuation": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/InitialValuation", InitialValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto",
                    "Vin": (this.Vin === null ? '' : this.Vin),
                    "StyleId": this.StyleId,
                    "State": this.State,
                    "Mileage": (this.Mileage === null ? '0' : this.Mileage),
                    "HasMileage": (this.Mileage === null ? false : true),
                    "GenericColorId": (this.GenericColorId === null ? '0' : this.GenericColorId),
                    "HasGenericColorId": (this.GenericColorId === null ? false : true),
                    "ManufacturerColorId": (this.ManufacturerColorId === null ? '0' : this.ManufacturerColorId),
                    "HasManufacturerColorId": (this.ManufacturerColorId === null ? false : true)
                }
            });
        }),
        "AdjustedValuation": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/AdjustedValuation", AdjustedValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto",
                    "State": (this.State === null ? '' : this.State),
                    "Mileage": (this.Mileage === null ? '0' : this.Mileage),
                    "HasMileage": (this.Mileage === null ? false : true),
                    "GenericColorId": (this.GenericColorId === null ? '0' : this.GenericColorId),
                    "HasGenericColorId": (this.GenericColorId === null ? false : true),
                    "ManufacturerColorId": (this.ManufacturerColorId === null ? '0' : this.ManufacturerColorId),
                    "HasManufacturerColorId": (this.ManufacturerColorId === null ? false : true),
                    "OptionAction": this.OptionAction,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        }),
        "PublicationList": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/PublicationList", PublicationListResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications.PublicationListArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "PublicationOnDate": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/PublicationOnDate", PublicationOnDateResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications.PublicationOnDateArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "On": this.On
                }
            });
        }),
        "PublicationLoad": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/PublicationLoad", PublicationLoadResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications.PublicationLoadArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "Id": this.Id
                }
            });
        }),
        "PublicationSave": genericService("/VehicleValuationGuide/Services/Edmunds.asmx/PublicationSave", PublicationSaveResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        })
    };

    var Events = {
        "StatesArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Edmunds).trigger("Edmunds.StatesLoaded", [data.States, State.State]);
            }
        },
        "TraversalArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var hasStyleId = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.StyleId;
                if (hasStyleId) {
                    $(Edmunds).trigger("Edmunds.StyleChange", data.Path.CurrentNode.StyleId);
                }
                if (!hasStyleId || this.RunNodeTemplate) {
                    $(Edmunds).trigger("Edmunds.TraversalLoaded", [data]);
                }
            }
        },
        "SuccessorArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Edmunds).trigger("Edmunds.SuccessorLoaded", [data]);
            }
        },
        "InitialValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // remember configuration
                State.VehicleConfiguration = data.VehicleConfiguration;
                // update page
                $(Edmunds).trigger("Edmunds.InitialValuationLoaded", [data]);
            },
            stateChange: function() {
                if (State.StyleId !== null && State.StyleId !== 0) {
                    var valuation = new InitialValuationArgumentsDto();
                    valuation.StyleId = State.StyleId;
                    valuation.State = State.State;
                    valuation.Vin = State.Vin;
                    valuation.Mileage = State.Mileage;
                    valuation.HasMileage = State.Mileage !== null;
                    valuation.GenericColorId = State.GenericColorId;
                    valuation.HasGenericColorId = State.GenericColorId !== null;
                    valuation.ManufacturerColorId = State.ManufacturerColorId;
                    valuation.HasManufacturerColorId = State.ManufacturerColorId !== null;
                    $(valuation).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "AdjustedValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // remember configuration
                State.VehicleConfiguration = data.VehicleConfiguration;
                // update page
                $(Edmunds).trigger("Edmunds.AdjustedValueLoaded", [data]);
            },
            stateChange: function() {
                if (State.StyleId !== null && State.StyleId !== 0) {
                    var valuation = new AdjustedValuationArgumentsDto();
                    valuation.VehicleConfiguration = State.VehicleConfiguration;
                    valuation.State = State.State;
                    valuation.Mileage = State.Mileage;
                    valuation.HasMileage = State.Mileage !== null;
                    valuation.GenericColorId = State.GenericColorId;
                    valuation.HasGenericColorId = State.GenericColorId !== null;
                    valuation.ManufacturerColorId = State.ManufacturerColorId;
                    valuation.HasManufacturerColorId = State.ManufacturerColorId !== null;
                    valuation.OptionAction = this.OptionAction || null;
                    $(valuation).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "PublicationListArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Edmunds).trigger("Edmunds.PublicationListLoaded", data);
            }
        },
        "PublicationOnDateArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
		if (data.Publication.VehicleConfiguration) {
                	State.StyleId = data.Publication.VehicleConfiguration.StyleId;
                	State.VehicleConfiguration = data.Publication.VehicleConfiguration;
		}
                $(Edmunds).trigger("Edmunds.PublicationLoaded", data);
            }
        },
        "PublicationLoadArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                State.StyleId = data.Publication.VehicleConfiguration.StyleId;
                State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                $(Edmunds).trigger("Edmunds.PublicationLoaded", data);
            }
        },
        "PublicationSaveArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Edmunds).trigger("Edmunds.PublicationSaved", data);
            }
        }
    };

    var EventBinder = function(obj) {
        var type = EventBinder.getType(obj);
        if (!!Events[type]) {
            var events = Events[type];
            for (var e in events) {
                if (!events.hasOwnProperty(e)) continue;
                $(obj).bind(e, events[e]); // TODO: events need to be framework independent
            }
        }
    };
    EventBinder.map = {
        'StatesArgumentsDto': StatesArgumentsDto,
        'TraversalArgumentsDto': TraversalArgumentsDto,
        'SuccessorArgumentsDto': SuccessorArgumentsDto,
        'InitialValuationArgumentsDto': InitialValuationArgumentsDto,
        'AdjustedValuationArgumentsDto': AdjustedValuationArgumentsDto,
        'PublicationListArgumentsDto': PublicationListArgumentsDto,
        'PublicationOnDateArgumentsDto': PublicationOnDateArgumentsDto,
        'PublicationLoadArgumentsDto': PublicationLoadArgumentsDto,
        'PublicationSaveArgumentsDto': PublicationSaveArgumentsDto
    };
    EventBinder.getType = function(obj) {
        for (var type in EventBinder.map) {
            if (!EventBinder.map.hasOwnProperty(type)) continue;
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };
    
    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(StatesArgumentsDto, DataMapper.StatesArgumentsDto);
        BindDataMapper(StatesResultsDto, DataMapper.StatesResultsDto);

        BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
        BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

        BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
        BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

        BindDataMapper(NodeDto, DataMapper.NodeDto);
        BindDataMapper(PathDto, DataMapper.PathDto);

        BindDataMapper(InitialValuationArgumentsDto, DataMapper.InitialValuationArgumentsDto);
        BindDataMapper(InitialValuationResultsDto, DataMapper.InitialValuationResultsDto);

        BindDataMapper(AdjustedValuationArgumentsDto, DataMapper.AdjustedValuationArgumentsDto);
        BindDataMapper(AdjustedValuationResultsDto, DataMapper.AdjustedValuationResultsDto);

        BindDataMapper(PriceTablesDto, DataMapper.PriceTablesDto);
        BindDataMapper(PriceTableDto, DataMapper.PriceTableDto);
        BindDataMapper(PriceTableRowDto, DataMapper.PriceTableRowDto);
        BindDataMapper(PricesDto, DataMapper.PricesDto);
        BindDataMapper(OptionDto, DataMapper.OptionDto);
        BindDataMapper(StateDto, DataMapper.StateDto);
        BindDataMapper(GenericColorDto, DataMapper.GenericColorDto);
        BindDataMapper(ManufacturerColorDto, DataMapper.ManufacturerColorDto);

        BindDataMapper(PublicationListArgumentsDto, DataMapper.PublicationListArgumentsDto);
        BindDataMapper(PublicationListResultsDto, DataMapper.PublicationListResultsDto);

        BindDataMapper(PublicationOnDateArgumentsDto, DataMapper.PublicationOnDateArgumentsDto);
        BindDataMapper(PublicationOnDateResultsDto, DataMapper.PublicationOnDateResultsDto);

        BindDataMapper(PublicationLoadArgumentsDto, DataMapper.PublicationLoadArgumentsDto);
        BindDataMapper(PublicationLoadResultsDto, DataMapper.PublicationLoadResultsDto);

        BindDataMapper(PublicationSaveArgumentsDto, DataMapper.PublicationSaveArgumentsDto);
        BindDataMapper(PublicationSaveResultsDto, DataMapper.PublicationSaveResultsDto);

        BindDataMapper(UserDto, DataMapper.UserDto);
        BindDataMapper(EditionDto, DataMapper.EditionDto);
        BindDataMapper(PublicationDto, DataMapper.PublicationDto);
        BindDataMapper(PublicationInfoDto, DataMapper.PublicationInfoDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
        }

        BindFetch(StatesArgumentsDto, Services.States);

        BindFetch(TraversalArgumentsDto, Services.Traversal);
        BindFetch(SuccessorArgumentsDto, Services.Successor);

        BindFetch(InitialValuationArgumentsDto, Services.InitialValuation);
        BindFetch(AdjustedValuationArgumentsDto, Services.AdjustedValuation);

        BindFetch(PublicationListArgumentsDto, Services.PublicationList);
        BindFetch(PublicationOnDateArgumentsDto, Services.PublicationOnDate);
        BindFetch(PublicationLoadArgumentsDto, Services.PublicationLoad);
        BindFetch(PublicationSaveArgumentsDto, Services.PublicationSave);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }

        if (selected.buid) {
            $(Edmunds).trigger("Edmunds.BrokerChange", selected.buid);
        }

        if (selected.state) {
            State.State = selected.state;
        }
        if (selected.mileage) {
            State.Mileage = selected.mileage;
        }

        /// We always want to select states, PM
        $(new StatesArgumentsDto()).trigger("fetch");

        if (selected.vuid) {
            $(Edmunds).trigger("Edmunds.VehicleChange", selected.vuid);
        } else {
            $(Edmunds).trigger("Edmunds.Start", selected);
        }

        State.Vin = selected.vin || State.Vin;
        State.StyleId = selected.styleId || State.StyleId;
    };

    this.raises = this.raises || [];
    this.raises.push("Edmunds.StatesLoaded");
    this.raises.push("Edmunds.TraversalLoaded");
    this.raises.push("Edmunds.SuccessorLoaded");
    this.raises.push("Edmunds.StyleChange");
    this.raises.push("Edmunds.InitialValuationLoaded");
    this.raises.push("Edmunds.AdjustedValueLoaded");
    this.raises.push("Edmunds.PublicationListLoaded");
    this.raises.push("Edmunds.PublicationLoaded");
    this.raises.push("Edmunds.PublicationSaved");
    this.raises.push("Edmunds.ErrorState");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Edmunds.MileageChange");
    this.listensTo.push("Edmunds.StateChange");
    this.listensTo.push("Edmunds.VinChange");
    this.listensTo.push("Edmunds.StyleChange");
    this.listensTo.push("Edmunds.AddOption");
    this.listensTo.push("Edmunds.RemoveOption");
    this.listensTo.push("Edmunds.SelectionChange");
    this.listensTo.push("Edmunds.LoadPublicationList");
    this.listensTo.push("Edmunds.LoadPublicationOnDate");
    this.listensTo.push("Edmunds.LoadPublication");
    this.listensTo.push("Edmunds.SavePublication");

    var PublicEvents = {
        "Edmunds.MileageChange": function(evt, newMileage) {
            if (State.Mileage !== newMileage) {
                State.Mileage = newMileage;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Edmunds.StateChange": function(evt, newState) {
            if (State.State !== newState) {
                State.State = newState;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Edmunds.ColorChange": function(evt, newGenericColorId, newManufacturerColorId) {
            if (State.GenericColorId !== newGenericColorId) {
                State.GenericColorId = newGenericColorId;
                State.ManufacturerColorId = newManufacturerColorId;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
            }
        },
        "Edmunds.VinChange": function(evt, newVin, runNodeTemplate) {
            if (State.Vin !== newVin) {
                State.Vin = newVin;
                State.StyleId = null;

                if (newVin !== '') {
                    var traversal = new TraversalArgumentsDto();
                    traversal.Vin = State.Vin;
                    traversal.State = State.State;
                    traversal.RunNodeTemplate = runNodeTemplate || false;
                    $(traversal).trigger('fetch');
                }
            }
        },
        "Edmunds.StyleChange": function(evt, styleId) {
            if (State.StyleId !== styleId) {
                State.StyleId = styleId;
                $(new InitialValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Edmunds.AddOption": function(evt, optionId) {
            var action = new OptionActionDto();
            action.ActionType = OptionActionTypeDto.Add;
            action.OptionId = optionId;
            $(new AdjustedValuationArgumentsDto(action)).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Edmunds.RemoveOption": function(evt, optionId) {
            var action = new OptionActionDto();
            action.ActionType = OptionActionTypeDto.Remove;
            action.OptionId = optionId;
            $(new AdjustedValuationArgumentsDto(action)).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Edmunds.SelectionChange": function(evt, next, successors) {
            var hasStyleId = successors && successors.length > 0 && !!successors[0].StyleId;
            if (hasStyleId) {
                $(Edmunds).trigger("Edmunds.StyleChange", [next]);
            } else {
                var successor = new SuccessorArgumentsDto();
                successor.Successor = next;
                $(successor).trigger("fetch");
            }
        },
        "Edmunds.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
            State.Vehicle = '';
        },
        "Edmunds.VehicleChange": function(evt, newVehicle) {
            State.Vehicle = newVehicle;
        },
        "Edmunds.LoadPublicationList": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publicationList = new PublicationListArgumentsDto();
                publicationList.Broker = State.Broker;
                publicationList.Vehicle = State.Vehicle;
                $(publicationList).trigger("fetch");
            }
        },
        "Edmunds.LoadPublicationOnDate": function(evt, theDate) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationOnDateArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.On = theDate;
                $(publication).trigger("fetch");
            }
        },
        "Edmunds.LoadPublication": function(evt, theId) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationLoadArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.Id = theId;
                $(publication).trigger("fetch");
            }
        },
        "Edmunds.SavePublication": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '' && State.VehicleConfiguration !== null) {
                var publication = new PublicationSaveArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.VehicleConfiguration = State.VehicleConfiguration;
                $(publication).trigger("fetch");
            }
        },
        "Edmunds.Start": function(evt, values) {
            values.vin = values.vin || State.Vin;
            values.styleId = values.styleId || State.StyleId;

            if (values.vin) {
                /// We have a vin so start with the entered vin, PM
                State.Vin = '';
                $(Edmunds).trigger("Edmunds.VinChange", values.vin);
            } else if (values.styleId) {
                /// Holy shit we have a style already... lets just get that, PM
                State.StyleId = '';
                $(Edmunds).trigger("Edmunds.StyleChange", selected.styleId);
            } else {
                /// Well we don't know a damn thing about the vehicle, lets provide the
                /// starting point for a selection, PM
                $(new TraversalArgumentsDto()).trigger("fetch");
            }
        }
    };

    $(ns).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.FormatPriceTableTypeRow = PriceTableRowTypeDto.toString;
    this.FormatTraversalStatus = TraversalStatusDto.toString;
}).apply(Edmunds);

