if (typeof(Edmunds) !== "function") {
    var Edmunds = function() { };
}
(function() {
    this.listensTo = this.raises || [];
    this.listensTo.push("Edmunds.StatesLoaded");
    this.listensTo.push("Edmunds.TraversalLoaded");
    this.listensTo.push("Edmunds.SuccessorLoaded");
    this.listensTo.push("Edmunds.StyleChange");
    this.listensTo.push("Edmunds.InitialValuationLoaded");
    this.listensTo.push("Edmunds.AdjustedValueLoaded");
    this.listensTo.push("Edmunds.PublicationListLoaded");
    this.listensTo.push("Edmunds.PublicationLoaded");
    this.listensTo.push("Edmunds.PublicationSaved");

    this.raises = this.listensTo || [];
    this.raises.push("Edmunds.MileageChange");
    this.raises.push("Edmunds.StateChange");
    this.raises.push("Edmunds.VinChange");
    this.raises.push("Edmunds.StyleChange");
    this.raises.push("Edmunds.AddOption");
    this.raises.push("Edmunds.RemoveOption");
    this.raises.push("Edmunds.SelectionChange");
    this.raises.push("Edmunds.LoadPublicationList");
    this.raises.push("Edmunds.LoadPublicationOnDate");
    this.raises.push("Edmunds.LoadPublication");
    this.raises.push("Edmunds.SavePublication");

    var PublicEvents = {
        "Edmunds.StatesLoaded": function(evt, states, currentState) {
            var template = new AdjustmentTemplateBuilder(states, currentState);
            template.clean();
            template.init();
        },
        "Edmunds.TraversalLoaded": function(evt, data){
            var template = new NodeTemplateBuilder(data.Path, data.Status);
            template.clean();
            template.init();
        },
        "Edmunds.SuccessorLoaded": function(evt, data){
            var template = new NodeTemplateBuilder(data.Path, 0);
            template.clean();
            template.init();
        },
        "Edmunds.InitialValuationLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data);
            template.clean();
            template.init();
        },
        "Edmunds.AdjustedValueLoaded": function(evt, data) {
            var template = new AdjustedTemplateBuilder(data);
            template.clean();
            template.init();
        },
        "Edmunds.PublicationListLoaded": function(evt, data) {
            var template = new PublicationListTemplateBuilder(data.Publications); ;
            template.clean();
            template.init();
        },
        "Edmunds.PublicationLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        },
        "Edmunds.PublicationSaved": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        }
    };
    $(Edmunds).bind(PublicEvents);

    var ns = this;

    var AdjustmentTemplateBuilder = function(data, state) {
        this.data = data;
        this.state = state;
        this.$dom = {
            State: $('#state'),
            Mileage: $('#mileage'),
            // publication api
            Form: $('#subject'),
            Save: $('#save'),
            Broker: $('#broker'),
            Vehicle: $('#vehicle'),
            PublicationDate: $('#publication_date')
        }
    };

    AdjustmentTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var list = this.data;
            var select = this.$dom.State;
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var state = $('<option></option>').val(item.Code).html(item.Name);
                if (item.Code == this.state) {
                    state.attr('selected', 'selected');
                }
                select.append(state);
            }
            select.unbind('change');
            select.bind('change', $.proxy(this.stateChange, this));
            var input = this.$dom.Mileage;
            input.unbind('change');
            input.bind('change', $.proxy(this.mileageChange, this));
            // broker / vehicle
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVehicleOrDate, this));
        },
        clean: function() {
            this.$dom.State.html('').unbind('change');
            this.$dom.Mileage.unbind('change');
        },
        stateChange: function(evt) {
            $(Edmunds).trigger("Edmunds.StateChange", [$(evt.target).val()]);
        },
        mileageChange: function(evt) {
            $(Edmunds).trigger("Edmunds.MileageChange", [$(evt.target).val()]);
        },
        changeBrokerOrVehicleOrDate: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                e = document.getElementById(n.substring(1)),
                v = (e == null ? null : e.value),
                c = null;
                if (n == '#broker') {
                    $(Edmunds).trigger("Edmunds.BrokerChange", v);
                    this.$dom.Vehicle.val('');
                }
                else
                    if (n == '#vehicle') {
                    $(Edmunds).trigger("Edmunds.VehicleChange", v);
                    $(Edmunds).trigger("Edmunds.LoadPublicationList");
                }
                else
                    if (n == '#publication_date') {
                    $(Edmunds).trigger("Edmunds.LoadPublicationOnDate", v);
                }
                else
                    if (n == '#save') {
                    $(Edmunds).trigger("Edmunds.SavePublication");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var NodeTemplateBuilder = function(data, status) {
        this.data = data;
        this.status = status;
        this.$el = $("#criteria");
    };

    NodeTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var bindDropDown = function(s, p, items, selected) {
                var prompt = $('<option></option>').val('').html('... Please Select ...');
                var isAlone = items.length === 1;
                s.html('');
                s.append(prompt);
                if (typeof selected === "undefined" && isAlone) {
                    selected = items[0].Value; // Select a lone item
                };
                for (i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    var val = item.State;
                    if (item.StyleId != null && item.StyleId != 0) {
                        val = item.StyleId;
                    }
                    var opt = $('<option></option>').val(val).html(item.Value);
                    if (item.Value == selected) {
                        opt.attr('selected', 'selected');

                    }
                    s.append(opt);
                }
                function fetchSuccessor(evt) {
                    var next = s.val();
                    if (next != '') {
                        // empty other drop downs
                        var inputs = ['year', 'make', 'model', 'style', 'synthetic'].slice(p);
                        for (var i = 0; i < inputs.length; i++) {
                            document.getElementById(inputs[i]).options.length = 0
                        }
                        // reset vin
                        if (inputs.length > 0) {
                            $(Edmunds).trigger("Edmunds.VinChange", ['']);
                            $('#vin').val('');
                        }
                        // request data
                        $(Edmunds).trigger("Edmunds.SelectionChange", [next, items]);
                    }
                }
                s.unbind();
                s.bind('change', fetchSuccessor);
            };
            var bindQueryByVin = function() {
                var decode = $('#EdmundsForm');
                var decoder = function(evt) {
                    $(Edmunds).trigger("Edmunds.VinChange", [$("#vin").val(), true]);
                    evt.preventDefault();
                };
                decode.unbind('submit', decoder);
                decode.bind('submit', decoder);
            };
            var bindNode = function(node, successors, selected) {
                var select = null;
                var position = 0;
                //var valuation = successors.length > 0 && !!successors[0].Uvc;
                switch (node.Label) {
                    case '':
                        select = $("#year");
                        position = 1;
                        bindQueryByVin();
                        break;
                    case 'Year':
                        select = $("#make");
                        position = 2;
                        break;
                    case 'Make':
                        select = $("#model");
                        position = 3;
                        break;
                    case 'Model':
                        select = $("#style");
                        position = 4;
                        break;
                    case 'Vin':
                        if (node.Parent.Label == 'Model') {
                            select = $("#style");
                            position = 4;
                        }
                        else {
                            var select = $('#synthetic');
                            var p = $('<option></option>').val('').html('... Please Select ...');
                            select.append(p);
                            for (var j = 0; j < successors.length; j++) {
                                var s = successors[j];
                                var u = s.StyleId;
                                var t = [];
                                while (s != null) {
                                    t.push(s.Value);
                                    s = s.Parent;
                                }
                                var n = t.reverse().join(' ');
                                var o = $('<option></option>').val(u).html(n);
                                select.append(o);
                            }
                            select.unbind();
                            select.bind('change', function(evt) {
                                $(Edmunds).trigger("Edmunds.SelectionChange", [select.val(), successors]);
                            });
                            // clean other drop-downs
                            $('#style').html('');
                        }
                        break;
                }
                if (position > 0) {
                    bindDropDown(select, position, successors, selected);
                }
            };
            var bindNodeRecursive = function(node, successors, selected) {
                if (node.Parent != null) {
                    bindNodeRecursive(node.Parent, node.Parent.Children, node.Value);
                }
                bindNode(node, successors, selected);
            };
            if (this.status == 0) {
                // structure: '' > 'year' > 'make' > 'model' [> 'vin'] > 'style' >
                if (this.data.CurrentNode.Label == 'Vin') {
                    bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    bindNode(this.data.CurrentNode, this.data.Successors);
                }
                $("#traversal_status").html('');
            }
            else {
                $("#traversal_status").html(ns.FormatTraversalStatus(this.status));
            }
        },
        clean: function() {
            $("#results").find("div").empty();
        }
    };

    var InitialTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $("#results");
    };

    InitialTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._colors();
            this._options();
            this._tables();
        },
        clean: function() {
            $('#options div').empty();
            $('#colors div').empty();
            $('#prices div').empty();
        },
        _options: function() {
            var info = $('#options');
            var hd = $("<div></div>");
            $(info).append(hd);
            var optionList = this.data.Options;
            var th = '<thead><tr><th></th><th>Option</th><th>Retail</th><th>Private Party</th><th>Trade In</th></tr></thead>';
            var tb = '<tbody>';
            for (var i = 0; i < optionList.length; i++) {
                var option = optionList[i];
                var tr = '<tr>' + '<td><input type="checkbox" name="' + option.Id + '" id="' + option.Id + '" /></td>' + '<td>' + option.Name + '</td>' + '<td>' + option.Retail + '</td>' + '<td>' + option.PrivateParty + '</td>' + '<td>' + option.TradeIn + '</td>' + '</tr>';
                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + th + tb + '</table>');

            var f = function(evt) {
                if (evt.target) {
                    var ck = evt.target.checked;
                    $(Edmunds).trigger(ck ? "Edmunds.AddOption" : "Edmunds.RemoveOption", [evt.target.name]);
                }
            };

            $(hd).bind('change', f);

            this.$el.append(info);
        },
        _tables: function() {
            var info = $('#prices');
            info.find('div').empty();
            // three price tables ...
            $(info).append(this._table('Retail', this.data.Tables.Retail));
            $(info).append(this._table('Private Party', this.data.Tables.PrivateParty));
            $(info).append(this._table('Trade-In', this.data.Tables.TradeIn));
        },
        _table: function(name, table) {
            var hd = $("<div></div>");
            var th = '<thead><tr><th></th><th>Outstanding</th><th>Clean</th><th>Average</th><th>Rough</th></tr></thead>';
            var tb = '<tbody>';
            var cp = '<caption>' + name + '</caption>';
            for (var i = 0; i < table.Rows.length; i++) {
                var tableRow = table.Rows[i];
                var tableRowType = ns.FormatPriceTableTypeRow(tableRow.RowType);
                var px = name.toLowerCase().replace(/[ -]/, '_') + '_' + tableRowType.toLowerCase();
                var tr = '<tr>' + '<td>' + tableRowType + '</td>' + '<td id="' + px + '_outstanding">' + tableRow.Prices.Outstanding + '</td>' + '<td id="' + px + '_clean">' + tableRow.Prices.Clean + '</td>' + '<td id="' + px + '_average">' + tableRow.Prices.Average + '</td>' + '<td id="' + px + '_rough">' + tableRow.Prices.Rough + '</td>' + '</tr>';
                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + cp + th + tb + '</table>');
            return hd;
        },
        _colors: function() {
            var info = $('#colors');
            var hd = $("<div></div>");
            var cps = ["Exterior", "Interior"];
            var tbs = '';
            for (var ctr = 1; ctr < 3; ctr++) {
                var cp = '<caption>' + cps[ctr - 1] + '</caption>';
                var th = '<thead><tr><th></th><th>Manufacturer <br/> Name</th><th>Generic <br/> Name</th><th>Color</th></tr></thead>';
                var tb = '<tbody>';
                for (var i = 0; i < this.data.Colors.length; i++) {
                    var color = this.data.Colors[i];
                    if (color.ColorType != ctr) {
                        continue;
                    }
                    var tr = '<tr>'
                     + '<td><input type="radio" group="" name="' + cps[ctr - 1] + '" id="' + color.GenericColor.Id + '_' + color.Id + '" /></td>'
                     + '<td>' + color.Name + '</td>'
                     + '<td>' + color.GenericColor.Name + '</td>'
                     + '<td style="background-color:' + color.Color + '">&nbsp;</td>'
                     + '</tr>';
                    tb += tr;
                }
                tb += '</tbody>';
                tbs += '<table>' + cp + th + tb + '</table>';
            }

            var f = function(evt) {
                var t = evt.target;
                if (t) {
                    var n = t.name, i = t.id;
                    if (n == 'Exterior') {
                        $(Edmunds).trigger("Edmunds.ColorChange", i.split('_'));
                    }
                }
            };

            $(hd).html(tbs);

            $(hd).bind('change', f);

            $(info).append(hd)

            this.$el.append(info);
        }
    };

    var AdjustedTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $("#prices");
    };

    AdjustedTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._tables();
        },
        clean: function() {
            this.$el.find('div').remove();
        }
    };

    AdjustedTemplateBuilder.prototype._tables = InitialTemplateBuilder.prototype._tables;
    AdjustedTemplateBuilder.prototype._table = InitialTemplateBuilder.prototype._table;

    var PublicationListTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#publications'),
            Body: $('#publications_body')
        };
    };

    PublicationListTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();
        },
        clean: function() {
            this.$dom.Body.empty();
            this.$dom.Form.unbind('click');
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                + '<td>' + item.Edition.BeginDate + '</td>'
                + '<td>' + item.Edition.EndDate + '</td>'
                + '<td>' + item.ChangeType + '</td>'
                + '<td>' + item.ChangeAgent + '</td>'
                + '<td>' + item.Edition.User.UserName + '</td>'
                + '<td><a href="#' + item.Id + '">Load</a></td>'
                + '</tr>';
                code += tr;
            }
            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.selectPublication, this));
        },
        selectPublication: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    p = tgt.href.substring(i + 1);
                $(Edmunds).trigger("Edmunds.LoadPublication", p);
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    this.AdjustmentTemplateBuilder = AdjustmentTemplateBuilder;
    this.AdjustedTemplateBuilder = AdjustedTemplateBuilder;
    this.NodeTemplateBuilder = NodeTemplateBuilder;
    this.InitialTemplateBuilder = InitialTemplateBuilder;

}).apply(Edmunds);

