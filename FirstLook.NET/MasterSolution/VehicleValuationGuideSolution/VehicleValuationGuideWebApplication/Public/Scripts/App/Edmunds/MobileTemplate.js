if (typeof Edmunds !== "function") {
    var Edmunds = function() {};
}
(function() {
    this.listensTo = this.raises || [];
    this.listensTo.push("Edmunds.StatesLoaded");
    this.listensTo.push("Edmunds.TraversalLoaded");
    this.listensTo.push("Edmunds.StyleChange");
    this.listensTo.push("Edmunds.InitialValuationLoaded");
    this.listensTo.push("Edmunds.AdjustedValueLoaded");
    this.listensTo.push("Edmunds.SuccessorLoaded");

    this.raises = this.listensTo || [];
    this.raises.push("Edmunds.MileageChange");
    this.raises.push("Edmunds.StateChange");
    this.raises.push("Edmunds.VinChange");
    this.raises.push("Edmunds.StyleChange");
    this.raises.push("Edmunds.AddOption");
    this.raises.push("Edmunds.RemoveOption");
    this.raises.push("Edmunds.SelectionChange");

    var State = {
        States: [],
        CurrentState: ""
    };

    var PublicEvents = {
        "Edmunds.StatesLoaded": function(evt, states, currentState) {
            State.States = _.reduce(states, function(memo, state) {
                memo.push({
                    name: state.Name,
                    value: state.Code
                });
                return memo;
            },
            []);
            State.CurrentState = currentState;
        },
        "Edmunds.TraversalLoaded": function(evt, data, status){
            var card = FirstLook.cards.get("Edmunds");

            card.build(data);
        },
        "Edmunds.SuccessorLoaded": function(evt, data){
            var card = FirstLook.cards.get("Edmunds");

            card.build(data);
        },
        "Edmunds.InitialValuationLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Edmunds");

            data.States = State.States;

            card.build(data);
        },
        "Edmunds.AdjustedValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Edmunds");

            $(card.views.values.Carousel).trigger("rebuild",[data])
        },
        "Edmunds.SelectionPathChange": function(evt, data) {
            if (data.StyleId !== "0") { 
                $(Edmunds).trigger("Edmunds.StyleChange", [data.StyleId]);
            } else {
                $(Edmunds).trigger("Edmunds.SelectionChange", [data.State]);
            }
        },
        "Edmunds.SelectionChangeWithDecode": function(evt, data) {
            var newSelection = JSON.parse(data.NewEdmundsSelection);
            $(Edmunds).trigger("Edmunds.SelectionPathChange", newSelection);
        },
        "Edmunds.EquipmentItemSelected": function(evt, data, isSelected) {
            $(Edmunds).trigger(isSelected ? "Edmunds.AddOption" : "Edmunds.RemoveOption", [data]);
        },
        "Edmunds.ColorItemSelected": function(evt, data, isSelected) {
            var genericColor = $(evt.target).attr("data-GenericColor-id")
            $(Edmunds).trigger("Edmunds.ColorChange", [genericColor, data]);
        },
        "Edmunds.MileageOrStateChange": function(evt, data) {
            $(Edmunds).trigger("Edmunds.MileageChange", data.Mileage);
            $(Edmunds).trigger("Edmunds.StateChange", data.State);
        },
        "Edmunds.MileageChange": function(evt, data) {
            var card = FirstLook.cards.get("Edmunds");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("input[name=Mileage]").val(data);
            }
        },
        "Edmunds.StateChange": function(evt, data) {
            var card = FirstLook.cards.get("Edmunds");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("select[name=State]").val(data);
            }
        },
        "Edmunds.PublicationLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Edmunds");

            if (data.Publication.VehicleConfiguration) {
                data.Publication.States = State.States;

                card.build(data.Publication);
            } else {
                $(Edmunds).trigger("Edmunds.Start", {});
            }
        },
        "Edmunds.VehicleChange": function(evt, data) {
            $(Edmunds).trigger("Edmunds.LoadPublicationOnDate", '2038-01-01');
        },
        "Edmunds.ErrorState": function(evt, data) {
            var card = FirstLook.cards.get("Edmunds");
            card.build(data);
        }
    };
    $(Edmunds).bind(PublicEvents);

    function edmundsTableFilter(market, title) {
        return function(data) {
            var table = [],
            data_table = data.Tables[market],
            row_finder = function(type) {
                return function(i) {
                    return i.RowType == type;
                };
            },
            row_builder = function(type, title) {
                return [title || type, 
                _.moneyAdjusted(o.Prices[type]) + "<br />" + _.moneyAdjusted(m.Prices[type]),
                _.money(f.Prices[type])];
            },
            o = _.detect(data_table.Rows, row_finder(2)),
            m = _.detect(data_table.Rows, row_finder(3)),
            c = _.detect(data_table.Rows, row_finder(4)),
            f = _.detect(data_table.Rows, row_finder(7));

            data.caption = title;
            table.header_col = 0;
            table.head = [["", "Options<br />Mileage", "Final"]];
            table.col_class_name = ["market", "adjustments", "final"];
            table.row_class_name = ["outstanding","clean","average","rough"];

            table.push(row_builder("Outstanding"));
            table.push(row_builder("Clean"));
            table.push(row_builder("Average"));
            table.push(row_builder("Rough"));

            data.table = table;

            return data;
        };
    }

    function edmundsSummaryFilter(market, title) {
        return function(data) {
            var table = [],
            data_table = data.Tables[market],
            row_finder = function(type) {
                return function(i) {
                    return i.RowType == type;
                };
            },
            row_builder = function(type, title) {
                return [title || type, 
                _.moneyAdjusted(o.Prices[type]) + "<br />" + _.moneyAdjusted(m.Prices[type]),
                _.money(f.Prices[type])];
            },
            f = _.detect(data_table.Rows, row_finder(7));

            data.caption = title;
            table.head = [["Outstanding", "Clean", "Average", "Rough"]];
            table.col_class_name = ["outstanding", "clean", "average", "rough"];

            table.push(_.map([f.Prices.Outstanding, f.Prices.Clean, f.Prices.Average, f.Prices.Rough], _.money));

            data.table = table;

            return data;
        };
    }

    function make_table(key, title) {
        var pattern = {
            Tables: {}
        };
        pattern.Tables[key] = _;
        return {
            type: "Table",
            pattern: pattern,
            class_name: "edmunds_value_tables",
            filter: edmundsTableFilter(key, title),
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            row_class_name_accessor: "table.row_class_name",
            body_header_col_accessor: "table.header_col"
        }
    }

    function make_summary(key, title) {
        var pattern = {
            Tables: {}
        };
        pattern.Tables[key] = _;
        return {
            type: "Table",
            pattern: pattern,
            class_name: "edmunds_value_tables",
            filter: edmundsSummaryFilter(key, title),
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            body_header_col_accessor: "table.header_col"
        }
    }

    function make_select() {
        return {
            type: "Fieldset",
            pattern: {
                Path: {
                    Successors: [{
                        State: _.isString,
                        Label: _.isString,
                        Value: _.isString,
                        StyleId: _
                    }]
                }
            },
            behaviors: {
                submitable: {
                    event: "Edmunds.SelectionChangeWithDecode",
                    scope: Edmunds,
                    submit_on_change: true
                }
            },
            legend_accessor: "fieldset.legend",
            input_accessor: "fieldset.inputs",
            name_accessor: "name",
            button_accessor: "fieldset.buttons",
            label_accessor: "label",
            value_accessor: "value",
            type_accessor: "type",
            filter: function(data) {
                var options = _.map(data.Path.Successors, function(succ) {
                    return {
                        name: succ.Value,
                        value: JSON.stringify({State: succ.State, StyleId: succ.StyleId})
                    }
                }),
                fieldset = {
                    legend: "Edmunds " + data.Path.Successors[0].Label,
                    inputs: [{
                        name: "NewEdmundsSelection",
                        label: data.Path.Successors[0].Label,
                        options: options
                    }],
                    buttons: []
                }
                options.unshift({name: "Select...", value: ""});
                data.fieldset = fieldset;
                return data;
            }
        }
    }

    FirstLook.components.add(make_table("Retail", "Retail Values"), "Edmunds.RetailTable");
    FirstLook.components.add(make_table("PrivateParty", "Private Party Values"), "Edmunds.PrivatePartyTable");
    FirstLook.components.add(make_table("TradeIn", "TradeIn Values"), "Edmunds.TradeInTable");

    FirstLook.components.add(make_select(), "Edmunds.Select");
    FirstLook.components.add(make_summary("Retail", "Edmunds Retail Values"), "Edmunds.RetailSummary");
    FirstLook.components.add(make_summary("PrivateParty", "Edmunds Private Party Values"), "Edmunds.PrivatePartySummary");
    FirstLook.components.add(make_summary("TradeIn", "Edmunds Trade In Values"), "Edmunds.TradeInSummary");


	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='error'>Problem with the VIN</p>";
		}
	},
	"Edmunds.VinProblem");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='message'>No Edmunds Data For VIN</p>";
		}
	},
	"Edmunds.NoData");

    var state_patterns = {
        select: {
            Path: {
                Successors: _.isArray
            },
            Status: function(v) {
                return v === 0;
            }
        },
        values: {
            VehicleConfiguration: _
        },
        vin_problem: {
			Status: function(v) {
				return v >= 1 && v <= 3;
			}
        },
        data_problem: {
			Status: function(v) {
				return v === 4;
			}
        },
        error: {
			errorType: _,
			errorResponse: _
        }
    };

    var overview_card = {
        pattern: _,
        view: {
        select: {
            pattern: state_patterns.select,
            select: FirstLook.components.get("Edmunds.Select")
        },
        values: {
            pattern: state_patterns.values,
            values: FirstLook.components.get("Edmunds.TradeInSummary")
        },
        vin_problem: {
            pattern: state_patterns.vin_problem,
            vin_problem: FirstLook.components.get("Edmunds.VinProblem")
        },
        data_problem: {
            pattern: state_patterns.data_problem,
            data_problem: FirstLook.components.get("Edmunds.NoData")
        },
        error: {
            pattern: state_patterns.error,
            error: FirstLook.components.get("Application.ServerError")
        }
        }
    }

    FirstLook.cards.add(overview_card, "EdmundsSummary");

    var cards = {
        title: "Edmunds",
        pattern: _,
        view: {
            select: {
                pattern: state_patterns.select,
                Title: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<h1 class='logo_edmunds'>Edmunds</h1>";
                    }
                },
                Review: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<p>"+_.pluck(data._selection, "Value").join(" | ")+"</p>";
                    },
                    filter: function(data) {
                        var selection = [];
                        function append_parent(v) {
                            selection.push(v);
                            if (v.Parent) {
                                append_parent(v.Parent);
                            }
                            return _.first(selection, selection.length-1);
                        }
                        
                        data._selection = append_parent(data.Path.CurrentNode);
                        return data;
                    }
                },
                TraversalList: {
                    type: "List",
                    pattern: {
                        Path: {
                            Successors: [{
                                Value: _.isString
                            }]
                        }
                    },
                    behaviors: {
                        selectable: {
                            pattern: {
                                Path: {
                                    Successors: [{
                                        State: _.isString,
                                        StyleId: _
                                    }]
                                }
                            },
                            data_accessor: ["State", "StyleId"],
                            binds: {
                                "item_selected": {
                                    type: "Edmunds.SelectionPathChange",
                                    scope: Edmunds
                                }
                            }
                        }
                    },
                    class_name: "select_list vid_select",
                    item_accessor: "Path.Successors",
                    text_accessor: "Value"
                }
            },
            values: {
                pattern: state_patterns.values,
                ValueFieldSet: {
                    type: "Fieldset",
                    pattern: {
                        VehicleConfiguration: {
                            Mileage: _.isNumber,
                            State: {
                                Code: _.isString
                            }
                        }
                    },
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "Edmunds.MileageOrStateChange",
                            scope: Edmunds,
                            submit_on_change: true
                        }
                    },
                    class_name: "book_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "Edmunds",
                        inputs: [{
                            name: "State",
                            label: "States",
                            data: "VehicleConfiguration.State.Code",
                            options: "States"
                        },
                        {
                            name: "Mileage",
                            label: "Mileage",
                            data: "VehicleConfiguration.Mileage",
                            type: "tel"
                        }],
                        buttons: ["Update"]
                    }
                },
                Carousel: {
                    type: "Carousel",
                    pattern: _,
                    behaviors: {
                        carouselable: {}
                    },
                    components: ["Edmunds.RetailTable", "Edmunds.PrivatePartyTable", "Edmunds.TradeInTable"]
                },
                EquipmentTitle: {
                    type: "Static",
                    pattern: {},
                    template: function() {
                        return "<h2>Equipment</h2>";
                    }
                },
                EquipmentList: {
                    type: "List",
                    behaviors: {
                        multi_selectable: {
                            pattern: {
                                VehicleConfiguration: {
                                    OptionStates: [{
                                        OptionId: _,
                                        Selected: _.isBoolean
                                    }]
                                }
                            },
                            multi_item_accessor: "VehicleConfiguration.OptionStates",
                            multi_data_accessor: "OptionId",
                            map_data_accessor: "Id",
                            selected_accessor: "Selected",
                            binds: {
                                "item_selected": {
                                    scope: Edmunds,
                                    type: "Edmunds.EquipmentItemSelected"
                                }
                            }
                        }
                    },
                    pattern: {
                        Options: [{
                            Name: _,
                            Retail: _,
                            PrivateParty: _,
                            TradeIn: _,
                            Id: _
                        }]
                    },
                    class_name: "equipment_list",
                    item_accessor: "Options",
                    text_accessor: "Name",
                    data_accessor: "Id"
                },
                ColorTitle: {
                    type: "Static",
                    pattern: {},
                    template: function() {
                        return "<h2>Colors</h2>";
                    }
                },
                ColorList: {
                    type: "List",
                    pattern: {
                        Colors: [{
                            Id: _.isNumber,
                            Code: _.isString,
                            Color: _.isString
                        }]
                    },
                    behaviors: {
                        multi_selectable: {
                            pattern: {
                                VehicleConfiguration: {
                                    OptionStates: [{
                                        OptionId: _,
                                        Selected: _.isBoolean
                                    }]
                                }
                            },
                            multi_item_accessor: "VehicleConfiguration.OptionStates",
                            multi_data_accessor: "OptionId",
                            map_data_accessor: "Id",
                            selected_accessor: "Selected",
                            binds: {
                                "item_selected": {
                                    scope: Edmunds,
                                    type: "Edmunds.ColorItemSelected"
                                }
                            }
                        }
                    },
                    class_name: "color_list",
                    item_accessor: "Colors",
                    text_accessor: "Name",
                    data_accessor: ["Id","Code","Color","GenericColor.Id"]
                }
            },
            vin_problem: {
                pattern: state_patterns.vin_problem,
                message: FirstLook.components.get("Edmunds.VinProblem")
            },
            data_problem: {
                pattern: state_patterns.data_problem,
                message: FirstLook.components.get("Edmunds.NoData")
            },
            error: {
                pattern: state_patterns.error,
                ErrorMessage: FirstLook.components.get("Application.ServerError")
            }
        }
    };

    FirstLook.cards.add(cards, "Edmunds");
}).apply(Edmunds);
