
(function() {

    var ns = this;

    var Callback = {
        NodeTemplate: ''
    };

    var LayoutTemplateBuilder = function() {
        this.$dom = {
            Form: $('#decoder'),
            Broker: $('#broker'),
            Vin: $('#vin'),
            Menu: $('#menu'),
            Menu$Item: $('#menu a')
        };
    };

    LayoutTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVin, this));
            this.$dom.Menu.bind('click', $.proxy(this.changeMenuItem, this));
        },
        clean: function() {
            this.$dom.Menu.unbind('change');
        },
        changeMenuItem: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "LI") {
                tgt = tgt.getElementsByTagName("A")[0];
            }
            this.$dom.Menu$Item.each(function(index) {
                var parent = $(this).parent();
                var i = this.href.lastIndexOf('#'), e = this.href.substring(i);
                if (this.href == tgt.href) {
                    parent.addClass('selected');
                    $(e).removeClass('hidden');
                }
                else {
                    parent.removeClass('selected');
                    $(e).addClass('hidden');
                }
            });
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        changeBrokerOrVin: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    e = tgt.href.substring(i),
                    v = document.getElementById(e.substring(1)).value,
                    c = null;
                if (e == '#broker') {
                    c = new ns.BrokerChange(v);
                }
                else if (e == '#vin') {
                    Callback.NodeTemplate = 'decoder'; // TODO: move vin back into NodeTemplate?
                    c = new ns.VinChange(v);
                }
                if (c) {
                    $(c).trigger("set");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var NodeTemplateBuilder = function(data, status) {
        this.data = data;
        this.status = status;
        this.$dom = {
            Year: $('#decoder_year, #auction_year, #listing_year'),
            Make: $('#decoder_make, #auction_make, #listing_make'),
            Model: $('#decoder_model, #auction_model, #listing_model'),
            Body: $('#decoder_body, #auction_body, #listing_body'),
            Synthetic: $('#decoder_synthetic'),
            Status: $("#traversal_status"),
            Form: $('#ManheimForm'),
            Vin: $('#vin')
        };
    };

    NodeTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            if (this.status == 0) {
                if (this.data.CurrentNode.Label == 'Vin') {
                    this.bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    var label = this.data.CurrentNode.Label,
                        ucf = function(s) { return s.charAt(0).toUpperCase() + s.substring(1); },
                        pfx = Callback.NodeTemplate
                    scope = ucf(pfx);
                    if (label) {
                        var selector = function(e, i) { return e.id.indexOf(pfx) == 0 },
                            val = function(e, f) { return $().add(jQuery.grep(e, f)).find('option:selected').text(); },
                            year = val(this.$dom.Year, selector),
                            make = val(this.$dom.Make, selector),
                            model = val(this.$dom.Model, selector),
                            body = val(this.$dom.Body, selector);
                        $(new ns.VehicleTypeChange(scope, year, make, model, body)).trigger('set');
                    }
                    this.bindNode(this.data.CurrentNode, this.data.Successors);
                }
                this.$dom.Status.empty();
            }
            else {
                this.$dom.Status.html(ns.FormatTraversalStatus(this.status));
            }
        },
        clean: function() {
            // $("fieldset").find("div").empty();
        },
        el: function(label) {
            var el;
            switch (label) {
                case '':
                    el = this.$dom.Year;
                    break;
                case 'Year':
                    el = this.$dom.Make;
                    break;
                case 'Make':
                    el = this.$dom.Model;
                    break;
                case 'Model':
                    el = this.$dom.Body;
                    break;
                default:
                    el = $();
                    break;
            }
            return $().add(jQuery.grep(el,
                function(e, i) {
                    return e.id.indexOf(Callback.NodeTemplate) == 0;
                }));
        },
        els: function(label) {
            var els = $();
            switch (label) {
                case '':
                    els = els.add(this.$dom.Make).add(this.$dom.Model).add(this.$dom.Body);
                    break;
                case 'Year':
                    els = els.add(this.$dom.Model).add(this.$dom.Body);
                    break;
                case 'Make':
                    els = els.add(this.$dom.Body);
                    break;
            }
            return $().add(jQuery.grep(els,
                function(e, i) {
                    return e.id.indexOf(Callback.NodeTemplate) == 0;
                }));
        },
        bindNodeRecursive: function(node, successors, selected) {
            if (node.Parent != null) {
                this.bindNodeRecursive(node.Parent, node.Parent.Children, node.Id);
            }
            this.bindNode(node, successors, selected);
        },
        bindNode: function(node, successors, selected) {
            var valuation = successors.length > 0 && !!successors[0].Mid;
            var el = this.el(node.Label),
                els = this.els(node.Label);
            switch (node.Label) {
                case '':
                case 'Year':
                case 'Make':
                case 'Model':
                    this.bindDropDown(el, valuation, els, successors, selected);
                    break;
                case 'Vin':
                    if (node.HasMid == true && node.Mid) {
                        $(new ns.MidChange(node.Mid)).trigger("set");
                        selected = successors[0].Id;
                    }
                    el = this.el(node.Parent.Label);
                    els = this.els(node.Parent.Label);
                    if (node.Parent.Label == 'Model') {
                        this.bindDropDown(el, valuation, els, successors, selected);
                    }
                    else {
                        var x = [];
                        for (var j = 0; j < successors.length; j++) {
                            var s = successors[j];
                            var i = s.Id;
                            var u = s.Mid;
                            var t = [];
                            while (s != null) {
                                t.push(s.Name);
                                s = s.Parent;
                            }
                            var n = t.reverse().join(' ');
                            x.push({
                                'Mid': u,
                                'HasMid': true,
                                'Id': i,
                                'Name': n,
                                'State': u,
                                'Label': 'Synthetic'
                            });
                        }
                        this.bindDropDown(this.$dom.Synthetic, valuation, els, x);
                    }
                    break;
            }
        },
        bindDropDown: function(s, v, p, items, selected) {
            var prompt = $('<option></option>').val('').html('... Please Select ...');
            s.empty();
            s.append(prompt);
            for (i = 0, l = items.length; i < l; i++) {
                var item = items[i];
                var val = item.State;
                if (item.HasMid == true && item.Mid) {
                    val = item.Mid;
                }
                var opt = $('<option></option>').val(val).html(item.Name);
                if (item.Id == selected) {
                    opt.attr('selected', 'selected');
                }
                s.append(opt);
            }
            if (v == false) {
                var f = (function(el, els, vin) {
                    return function(evt) {
                        var id = evt.target.id,
                            px = id.substring(0, id.indexOf('_')),
                            nx = evt.target.value;
                        if (nx != '') {
                            Callback.NodeTemplate = px;
                            var lst = jQuery.grep(els,
                                function(e, i) {
                                    return e.id.indexOf(Callback.NodeTemplate) == 0;
                                });
                            $().add(lst).empty();
                            if (lst.length > 0) {
                                $(new ns.VinChange(vin.empty().val())).trigger('set');
                            }
                            $(new ns.SelectionChange(nx)).trigger("set");
                        }
                    }
                })(s, p, this.$dom.Vin);
                s.unbind();
                s.bind('change', f);
            }
            else {
                var g = (function(el) {
                    return function(evt) {
                        $(new ns.MidChange(el.val())).trigger("set");
                    }
                })(s);
                s.unbind();
                s.bind('change', g);
            }
        }
    };

    var DomainTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Body: $('#auction_results, #listing_results'),
            Values$AuctionId: $('#auction_auction_id, #listing_auction_id'),
            Values$ChannelCode: $('#auction_channel_code, #listing_channel_code'),
            Values$Consignor: $('#auction_consignor, #listing_consignor'),
            Values$SaleDate: $('#auction_sale_date, #listing_sale_date'),
            Values$Prices: $('#auction_price_min, #listing_price_min, #auction_price_max, #listing_price_max'),
            Values$Mileages: $('#auction_mileage_min, #listing_mileage_min, #auction_mileage_max, #listing_mileage_max')
        };
    };

    DomainTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {

            var populate = function(el, list, change) {
                el.empty();
                el.append($('<option value="" selected="selected">All</option>'));
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    var opt = $('<option></option>').val(item.Value).html(item.Label || item.Value);
                    el.append(opt);
                }
                el.bind('change', change);
            };

            populate(
                this.$dom.Values$AuctionId,
                this.data.Auctions,
                this.selectAuction);

            populate(
                this.$dom.Values$ChannelCode,
                this.data.ChannelCodes,
                this.selectChannelCode);

            populate(
                this.$dom.Values$Consignor,
                this.data.Consignors,
                this.selectConsignor);

            populate(
                this.$dom.Values$SaleDate,
                this.data.SaleDates,
                this.selectSaleDate);

            this.$dom.Values$Prices.bind('change', $.proxy(this.changePrice, this));

            this.$dom.Values$Mileages.bind('change', $.proxy(this.changeMileage, this));

        },
        clean: function() {
            this.$dom.Values$AuctionId.html('').unbind();
            this.$dom.Values$ChannelCode.html('').unbind();
            this.$dom.Values$Consignor.html('').unbind();
            this.$dom.Values$SaleDate.html('').unbind();
        },
        changePrice: function(evt) {
            var el = evt.target,
                vl = $(el).val(),
                id = el.id,
                nm = id.substring(0, id.indexOf('_')),
                ty = id.substring(id.lastIndexOf('_')),
                px = id.substring(0, id.lastIndexOf('_')),
                uc = nm.charAt(0).toUpperCase() + nm.slice(1);
            if (vl === '') vl = 0;
            var xl = $().add(jQuery.grep(this.$dom.Values$Prices,
                function(e, i) { return e.id.indexOf(px) == 0 && e.id.indexOf(ty) == -1; })).val();
            var min = (ty == 'min' ? vl : xl),
                max = (ty == 'min' ? xl : vl);
            var pat = /^\d+$/,
                mnn = pat.test(min),
                mxn = pat.test(max);
            if (mnn && mxn) {
                if (min < max) {
                    $(new ns.PriceChange(uc, min, max)).trigger('set');
                }
            }
        },
        changeMileage: function(evt) {
            var el = evt.target,
                vl = $(el).val(),
                id = el.id,
                nm = id.substring(0, id.indexOf('_')),
                ty = id.substring(id.lastIndexOf('_')),
                px = id.substring(0, id.lastIndexOf('_')),
                uc = nm.charAt(0).toUpperCase() + nm.slice(1);
            if (vl === '') vl = 0;
            var xl = $().add(jQuery.grep(this.$dom.Values$Mileages,
                function(e, i) { return e.id.indexOf(px) == 0 && e.id.indexOf(ty) == -1; })).val();
            var min = (ty == 'min' ? vl : xl),
                max = (ty == 'min' ? xl : vl);
            var pat = /^\d+$/,
                mnn = pat.test(min),
                mxn = pat.test(max);
            if (mnn && mxn) {
                if (min < max) {
                    $(new ns.MileageChange(uc, min, max)).trigger('set');
                }
            }
        },
        selectAuction: function(evt) {
            var el = evt.target,
                vl = $(el).val(),
                id = el.id,
                nm = id.substring(0, id.indexOf('_')),
                uc = nm.charAt(0).toUpperCase() + nm.slice(1);
            if (vl === '') vl = null;
            $(new ns.AuctionChange(uc, vl)).trigger('set');
            // stop event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        selectChannelCode: function(evt) {
            var el = evt.target,
                vl = $(el).val(),
                id = el.id,
                nm = id.substring(0, id.indexOf('_')),
                uc = nm.charAt(0).toUpperCase() + nm.slice(1);
            if (vl === '') vl = null;
            $(new ns.ChannelChange(uc, vl)).trigger('set');
            // stop event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        selectConsignor: function(evt) {
            var el = evt.target,
                vl = $(el).val(),
                id = el.id,
                nm = id.substring(0, id.indexOf('_')),
                uc = nm.charAt(0).toUpperCase() + nm.slice(1);
            if (vl === '') vl = null;
            $(new ns.ConsignorChange(uc, vl)).trigger('set');
            // stop event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        selectSaleDate: function(evt) {
            var el = evt.target,
                vl = $(el).val(),
                id = el.id,
                nm = id.substring(0, id.indexOf('_')),
                uc = nm.charAt(0).toUpperCase() + nm.slice(1);
            if (vl === '') vl = null;
            $(new ns.SaleDateChange(uc, vl)).trigger('set');
            // stop event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var SaleSummaryTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Body: $('#auction_results')
        };
    };

    SaleSummaryTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var list = this.data.Results;
            var tb = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var date = new Date(parseInt(item.SaleDate.replace(/\/Date\((\d+)\)\//, "$1"), 10));
                var tr = '<tr id="row_' + i + '">'
                 + '<td>' + item.AuctionId + '</td>'
                 + '<td>' + item.AuctionName + '</td>'
                 + '<td>' + item.ChannelCode + '</td>'
                 + '<td>' + item.Consignor + '</td>'
                 + '<td>' + item.Description + '</td>'
                 + '<td>' + item.Lane + '</td>'
                 + '<td>' + date.toDateString() + '</td>'
                 + '<td>' + item.SaleNumber + '</td>'
                 + '<td><a href="#row_' + i + '">' + item.Vehicles + '</a></td>'
                 + '</tr>';
                tb += tr;
            }
            this.$dom.Body.html(tb);
            this.$dom.Body.bind('click', this.selectSale);
        },
        clean: function() {
            this.$dom.Body.html('');
        },
        selectSale: function(evt) {
            var tgt = evt.target, upd = true;
            if (tgt.nodeName == "TD") {
                var els = tgt.getElementsByTagName("A");
                if (els.length) {
                    tgt = els[0]
                }
                else {
                    upd = false;
                }
            }
            if (upd) {
                var href = tgt.href;
                var id = href.substring(href.lastIndexOf('#'));
                var auctionId = $(id).find('td:nth-child(1)').html(),
                channel = $(id).find('td:nth-child(3)').html(),
                consignor = $(id).find('td:nth-child(4)').html(),
                saleDate = $(id).find('td:nth-child(7)').html();
                $(new ns.AuctionSelect(auctionId, channel, consignor, saleDate)).trigger('set');
                // change tab
                $('#menu li:nth-child(2)').click();
                // update individual drop-downs (is safe as the application js will not trigger as no change)
                $('#listing_auction_id').val(auctionId);
                $('#listing_consignor').val(consignor);
                $('#listing_channel_code').val(channel);
                $('#listing_sale_date').val(ns.ToIsoDate(saleDate));
                // stop event
            }
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var ListingTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Body: $('#listing_results')
        };
    };

    ListingTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var list = this.data.Results;
            var tb = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var date = new Date(parseInt(item.SaleDate.replace(/\/Date\((\d+)\)\//, "$1"), 10));
                var vt = item.Vehicle.Type, vd = item.Vehicle.Detail, vin = vt.VinInfo.Vin;
                var tr = '<tr id="row_' + i + '">'
                 + '<td>' + item.Channel + '</td>'
                 + '<td>' + item.Consignor + '</td>'
                 + '<td>' + date.toDateString() + '</td>'
                 + '<td>' + item.SaleInfo.SaleNumber + '</td>'
                 + '<td>' + item.SaleInfo.LaneNumber + '</td>'
                 + '<td>' + item.SaleInfo.RunNumber + '</td>'
                 + '<td><a href="#' + vin + '">' + vin + '</a></td>'
                 + '<td>' + vt.Year.Label + ' ' + vt.Make.Label + ' ' + vt.Model.Label + ' ' + vt.Body.Label + '</td>'
                 + '<td>' + vd.Mileage + '</td>'
                 + '<td>' + (item.Comments || '') + '</td>'
                 + '</tr>';
                tb += tr;
            }
            this.$dom.Body.html(tb);
            this.$dom.Body.bind('click', this.selectListing);
        },
        clean: function() {
            this.$dom.Body.html('');
        },
        selectListing: function(evt) {
            var href = evt.target.href;
            var vin = href.substring(href.lastIndexOf('#') + 1);
            $(new ns.VehicleSelect(vin)).trigger('set');
            $('#menu li:nth-child(3)').click();
            // stop event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var VehicleTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Container: $('#details'),
            // vehicle
            Year: $('#details_year'),
            Make: $('#details_make'),
            Model: $('#details_model'),
            Body: $('#details_body'),
            Vin: $('#details_vin'),
            Mileage: $('#details_mileage'),
            // seller
            Seller$City: $('#details_seller_city'),
            Seller$State: $('#details_seller_state'),
            Seller$Zip: $('#details_seller_zip'),
            Seller$Country: $('#details_seller_country'),
            // equipment
            Equipment: $('#details_equipment')
        };
    };

    VehicleTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var vt = this.data.Vehicle.Type,
                vd = this.data.Vehicle.Detail,
                sd = this.data.SellerAddress;
            // vehicle type
            this.$dom.Year.text(vt.Year.Label);
            this.$dom.Make.text(vt.Make.Label);
            this.$dom.Model.text(vt.Model.Label);
            this.$dom.Body.text(vt.Body.Label);
            this.$dom.Vin.text(vt.VinInfo.Vin);
            this.$dom.Mileage.text(vd.Mileage);
            // seller address
            this.$dom.Seller$City.text(sd.City);
            this.$dom.Seller$State.text(sd.StateProvinceRegion);
            this.$dom.Seller$Zip.text(sd.PostalCode);
            this.$dom.Seller$Country.text(sd.Country);
            // equipment
            var el = this.$dom.Equipment;
            el.empty();
            var magic = ['Airbag', 'InteriorColor', 'ExteriorColor', 'Engine', 'Drivetrain', 'Roof', 'Transmission'];
            for (var i = 0, l = vd.Options.length; i < l; i++) {
                var option = vd.Options[i];
                var dt = $('<dt></dt>').text($.inArray(option.Type, magic) >= 0 ? option.Type : option.Display),
                    dd = $('<dd></dd>').text(option.Value);
                el.append(dt).append(dd);
            }
        },
        clean: function() {
            this.$dom.Container.find('dd').text('');
            this.$dom.Equipment.empty();
        }
    };

    this.LayoutTemplateBuilder = LayoutTemplateBuilder;
    this.NodeTemplateBuilder = NodeTemplateBuilder;
    this.DomainTemplateBuilder = DomainTemplateBuilder;
    this.SaleSummaryTemplateBuilder = SaleSummaryTemplateBuilder;
    this.ListingTemplateBuilder = ListingTemplateBuilder;
    this.VehicleTemplateBuilder = VehicleTemplateBuilder;

}).apply(Manheim);
