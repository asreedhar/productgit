
if (typeof Manheim !== "function") {
    var Manheim = function() { };
}

(function() {
    var ns = this;

    var State = {
        Broker: null,
        Vin: '',
        Mid: '',
        // auction criteria
        Auction$AuctionId: null,
        Auction$BodyDesc: null,
        Auction$ChannelCode: null,
        Auction$Consignor: null,
        Auction$MakeDesc: null,
        Auction$MaxMileage: null,
        Auction$MaxPrice: null,
        Auction$MaxVehicleYear: null,
        Auction$MinMileage: null,
        Auction$MinPrice: null,
        Auction$MinVehicleYear: null,
        Auction$ModelDesc: null,
        Auction$SaleDate: null,
        // listing criteria
        Listing$AuctionId: null,
        Listing$BodyDesc: null,
        Listing$ChannelCode: null,
        Listing$Consignor: null,
        Listing$MakeDesc: null,
        Listing$MaxMileage: null,
        Listing$MaxPrice: null,
        Listing$MaxVehicleYear: null,
        Listing$MinMileage: null,
        Listing$MinPrice: null,
        Listing$MinVehicleYear: null,
        Listing$ModelDesc: null,
        Listing$SaleDate: null,
        // current page of listings
        Listing$Items: [],
        Listing$TotalRowCount: 0
    };

    function ToCriteria(map, pfx) {
        var s = function(a, b, c) {
            a[b] = map[c + '$' + b];
        }
        var c = new CriteriaDto();
        s(c, 'AuctionId', pfx);
        s(c, 'BodyDesc', pfx);
        s(c, 'ChannelCode', pfx);
        s(c, 'Consignor', pfx);
        s(c, 'MakeDesc', pfx);
        s(c, 'MaxMileage', pfx);
        s(c, 'MaxPrice', pfx);
        s(c, 'MaxVehicleYear', pfx);
        s(c, 'MinMileage', pfx);
        s(c, 'MinPrice', pfx);
        s(c, 'MinVehicleYear', pfx);
        s(c, 'ModelDesc', pfx);
        s(c, 'SaleDate', pfx);
        return c;
    }

    /* ==================== */
    /* == START: Classes == */
    /* ==================== */

    /* traversal dto */

    function PathDto() {
        this.CurrentNode = null;
        this.Successors = [];
    }

    function NodeDto() {
        this.Parent = null;
        this.Children = [];
        this.Mid = 0;
        this.HasMid = false;
        this.Id = "";
        this.Name = "";
        this.State = "";
        this.Label = "";
    }

    /* sale summary */

    function SaleSummaryDto() {
        this.AuctionId = "";
        this.AuctionName = "";
        this.ChannelCode = "";
        this.Consignor = "";
        this.Description = "";
        this.Lane = "";
        this.SaleDate = "";
        this.SaleNumber = "";
        this.Vehicles = 0;
    }

    function CriteriaDto() {
        this.AuctionId = null;
        this.BodyDesc = null;
        this.ChannelCode = null;
        this.Consignor = null;
        this.MakeDesc = null;
        this.MaxMileage = null;
        this.MaxPrice = null;
        this.MaxVehicleYear = null;
        this.MinMileage = null;
        this.MinPrice = null;
        this.MinVehicleYear = null;
        this.ModelDesc = null;
        this.SaleDate = null;
    }

    function LabelValueDto() {
        this.Label = "";
        this.Value = "";
    }

    function AddressDto() {
        this.City = '';
        this.Country = '';
        this.PostalCode = '';
        this.StateProvinceRegion = '';
    }

    function SaleInfoDto() {
        this.LaneNumber = 0;
        this.RunNumber = 0;
        this.SaleNumber = 0;
        this.SaleYear = 0;
    }

    function ListingDto() {
        this.Channel = '';
        this.Consignor = '';
        this.SaleDate = null;
        this.ChannelCode = '';
        this.Comments = '';
        this.DateModified = null;
        this.PhysicalLocation = '';
        this.SaleInfo = null;
        this.SellerAddress = null;
        this.Vehicle = null;
    }

    function VehicleOptionDto() {
        this.Display = '';
        this.Value = '';
        this.Type = '';
    }

    function VehicleDetailDto() {
        this.Options = [];
        this.Description = '';
        this.InvoicePrice = 0;
        this.Mileage = 0;
        this.MsrPrice = 0;
        this.RetailPrice = 0;
        this.StockNumber = '';
    }

    function VinInfoDto() {
        this.Vin = '';
        this.VinPrefix8 = '';
        this.YearCharacter = '';
    }

    function VehicleTypeDto() {
        this.Year = null;
        this.Make = null;
        this.Model = null;
        this.Body = null;
        this.Mid = '';
        this.VinInfoDto = null;
    }

    function VehicleDto() {
        this.Type = null;
        this.Detail = null;
    }

    /* envelopes dto */

    var TraversalStatusDto = {
        "Success": 0,
        "InvalidVinLength": 1,
        "InvalidVinCharacters": 2,
        "InvalidVinChecksum": 3,
        "NoDataForVin": 4,
        "toString": function(i) {
            var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function TraversalArgumentsDto() {
        this.Vin = '';
        EventBinder(this);
    }

    function TraversalResultsDto() {
        this.Arguments = null;
        this.Path = null;
        this.Status = 0;
    }

    function SuccessorArgumentsDto() {
        this.Node = "";
        this.Successor = "";
        EventBinder(this);
    }

    function SuccessorResultsDto() {
        this.Arguments = null;
        this.Path = null;
    }

    function SaleSummaryArgumentsDto() {
        this.StartRowIndex = 0;
        this.MaximumRows = 25;
        this.SortColumns = '';
        this.Broker = '00000000-0000-0000-0000-000000000000';
        this.Criteria = null;
        EventBinder(this);
    }

    function SaleSummaryResultsDto() {
        this.Arguments = null;
        this.Results = [];
        this.Auctions = [];
        this.ChannelCodes = [];
        this.Consignors = [];
        this.SaleDates = [];
    }

    function ListingArgumentsDto() {
        this.StartRowIndex = 0;
        this.MaximumRows = 25;
        this.SortColumns = '';
        this.Broker = '00000000-0000-0000-0000-000000000000';
        this.Criteria = null;
        EventBinder(this);
    }

    function ListingResultsDto() {
        this.Arguments = null;
        this.Results = [];
        this.ChannelCodes = [];
        this.Consignors = [];
        this.SaleDates = [];
    }

    function DomainArgumentsDto() {
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function DomainResultsDto() {
        this.Arguments = null;
        this.Auctions = [];
        this.ChannelCodes = [];
        this.Consignors = [];
        this.SaleDates = [];
    }

    /* Change Classes */

    function SelectionChange(next, successors) {
        this.next = next;
        this.successors = successors;
        EventBinder(this);
    }

    function VinChange(newVin, runNodeTemplate) {
        this.newVin = newVin;
        this.runNodeTemplate = runNodeTemplate || false;
        EventBinder(this);
    }

    function MidChange(newMid) {
        this.newVehicleId = newMid;
        EventBinder(this);
    }

    function AuctionChange(scope, newAuctionId) {
        this.newAuctionId = newAuctionId;
        this.scope = scope;
        EventBinder(this);
    }

    function ChannelChange(scope, newChannelCode) {
        this.newChannelCode = newChannelCode;
        this.scope = scope;
        EventBinder(this);
    }

    function ConsignorChange(scope, newConsignor) {
        this.newConsignor = newConsignor;
        this.scope = scope;
        EventBinder(this);
    }

    function SaleDateChange(scope, newSaleDate) {
        this.newSaleDate = newSaleDate;
        this.scope = scope;
        EventBinder(this);
    }
    function toIsoDate(text) {
        var da = new Date(text),
            dy = da.getFullYear(),
            dm = da.getMonth() + 1,
            dd = da.getDate();
        if (dy < 1970) dy = dy + 100;
        var ys = new String(dy),
            ms = new String(dm),
            ds = new String(dd);
        if (ms.length == 1) ms = "0" + ms;
        if (ds.length == 1) ds = "0" + ds;
        return ys + "-" + ms + "-" + ds;
    }

    function AuctionSelect(newAuctionId, newChannelCode, newConsignor, newSaleDate) {
        this.newAuctionId = newAuctionId;
        this.newChannelCode = newChannelCode;
        this.newConsignor = newConsignor;
        this.newSaleDate = toIsoDate(newSaleDate);
        EventBinder(this);
    }

    function VehicleSelect(newVin) {
        this.newVin = newVin;
        EventBinder(this);
    }

    function VehicleTypeChange(scope, newYear, newMake, newModel, newBody) {
        this.newYear = newYear || null;
        this.newMake = newMake || null;
        this.newModel = newModel || null;
        this.newBody = newBody || null;
        this.scope = scope;
        EventBinder(this);
    }

    function MileageChange(scope, min, max) {
        this.scope = scope;
        this.min = min;
        this.max = max;
        EventBinder(this);
    }

    function PriceChange(scope, min, max) {
        this.scope = scope;
        this.min = min;
        this.max = max;
        EventBinder(this);
    }

    /* == END: Classes == */

    function IsValidVin(text) {
        if (text == null) {
            return false;
        }
        if (text.length != 17) {
            return false;
        }
        text = text.toUpperCase();
        if (/^[ABCDEFGHJKLMNPRSTUVWXYZ0123456789]{17}$/.test(text) == false) {
            return false;
        }
        var factors = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2];
        var char_map = { "A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8, "J": 1, "K": 2, "L": 3, "M": 4, "N": 5, "P": 7, "R": 9, "S": 2, "T": 3, "U": 4, "V": 5, "W": 6, "X": 7, "Y": 8, "Z": 9 };

        var total = 0;
        for (var i = 0; i < 17; i++) {
            var v = text.charCodeAt(i),
			f = factors[i];
            v = char_map[text[i]] || parseInt(text[i], 10);

            total += (v * f);
        }
        var remainder = total % 11;
        if (remainder == 10) {
            remainder = 'X';
        }
        else {
            remainder = remainder.toString();
        }
        return text[8] == remainder;
    }

    var Events = {
        "TraversalArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var hasMid = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.Mid;
                if (hasMid) {
                    $(new MidChange(data.Path.CurrentNode.Mid)).trigger("set");
                }
                if (!hasMid || this.RunNodeTemplate) {
                    var template = new ns.NodeTemplateBuilder(data.Path, data.Status);
                    template.clean();
                    template.init();
                }
            }
        },
        "SuccessorArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var template = new ns.NodeTemplateBuilder(data.Path, 0);
                template.clean();
                template.init();
            }
        },
        "SaleSummaryArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var template = new ns.SaleSummaryTemplateBuilder(data);
                template.clean();
                template.init();
            },
            stateChange: function(event) {
                // this.Broker = State.Broker;
                this.Criteria = ToCriteria(State, 'Auction');
                $(this).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "ListingArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // remember listings
                State.Listing$Items = data.Results;
                State.Listing$TotalRowCount = data.TotalRowCount;
                // render listings
                var template = new ns.ListingTemplateBuilder(data);
                template.clean();
                template.init();
            },
            stateChange: function(event) {
                // this.Broker = State.Broker;
                this.Criteria = ToCriteria(State, 'Listing');
                $(this).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "DomainArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var template = new ns.DomainTemplateBuilder(data);
                template.clean();
                template.init();
            }
        },
        "SelectionChange": {
            set: function() {
                var hasMid = this.successors && this.successors.length > 0 && !!this.successors[0].Mid; /// TODO: it appears that Simon's now passing this down. PM
                if (hasMid) {
                    var newMid = new MidChange(this.next);
                    $(newMid).trigger('set');
                } else {
                    var successor = new SuccessorArgumentsDto();
                    successor.Node = null;
                    successor.Successor = this.next;
                    $(successor).trigger("fetch"); // TODO: events need to be framework independent
                }
            }
        },
        "VinChange": {
            set: function() {
                if (State.Vin !== this.newVin) {
                    State.Vin = this.newVin;
                    State.Mid = '';

                    if (this.newVin !== '') {
                        var traversal = new TraversalArgumentsDto();
                        traversal.Vin = State.Vin;
                        traversal.RunNodeTemplate = this.runNodeTemplate;
                        $(traversal).trigger("fetch"); // TODO: events need to be framework independent
                    }
                }
            }
        },
        "MidChange": {
            set: function() {
                if (State.Mid !== this.newMid && this.newMid !== null) {
                    State.Mid = this.newMid;
                    // TODO
                    // $(new InitialValuationArgumentsDto()).trigger("stateChange");
                }
            }
        },
        "AuctionChange": {
            set: function() {
                var prop = this.scope + '$AuctionId',
                    oldAuctionId = State[prop];
                if (oldAuctionId !== this.newAuctionId) {
                    State[prop] = this.newAuctionId;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
        "ChannelChange": {
            set: function() {
                var prop = this.scope + '$ChannelCode',
                    oldChannelCode = State[prop];
                if (oldChannelCode !== this.newChannelCode) {
                    State[prop] = this.newChannelCode;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
        "ConsignorChange": {
            set: function() {
                var prop = this.scope + '$Consignor',
                    oldConsignor = State[prop];
                if (oldConsignor !== this.newConsignor) {
                    State[prop] = this.newConsignor;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
        "SaleDateChange": {
            set: function() {
                var prop = this.scope + '$SaleDate',
                    oldSaleDate = State[prop];
                if (oldSaleDate !== this.newSaleDate) {
                    State[prop] = this.newSaleDate;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
        "AuctionSelect": {
            set: function() {
                State['Listing$AuctionId'] = this.newAuctionId;
                State['Listing$ChannelCode'] = this.newChannelCode;
                State['Listing$Consignor'] = this.newConsignor;
                State['Listing$SaleDate'] = this.newSaleDate;

                State['Listing$MinPrice'] = State['Auction$MinPrice'];
                State['Listing$MaxPrice'] = State['Auction$MaxPrice'];
                State['Listing$MinMileage'] = State['Auction$MinMileage'];
                State['Listing$MaxMileage'] = State['Auction$MaxMileage'];
                
                $(new ListingArgumentsDto()).trigger("stateChange");
            }
        },
        "VehicleSelect": {
            set: function() {
                for (var i = 0, l = State.Listing$Items.length; i < l; i++) {
                    var item = State.Listing$Items[i];
                    if (item.Vehicle.Type.VinInfo.Vin === this.newVin) {
                        var template = new ns.VehicleTemplateBuilder(item);
                        template.clean();
                        template.init();
                        break;
                    }
                }
            }
        },
        "VehicleTypeChange": {
            set: function() {
                var minYearName = this.scope + '$MinVehicleYear',
                    maxYearName = this.scope + '$MaxVehicleYear',
                    makeName = this.scope + '$MakeDesc',
                    modelName = this.scope + '$ModelDesc',
                    bodyName = this.scope + '$BodyDesc',
                    oldMinYear = State[minYearName],
                    oldMaxYear = State[maxYearName],
                    oldMake = State[makeName],
                    oldModel = State[modelName],
                    oldBody = State[bodyName];
                if ((oldMinYear !== this.newYear) ||
                    (oldMaxYear !== this.newYear) ||
                    (oldMake !== this.newMake) ||
                    (oldModel !== this.newModel) ||
                    (oldBody !== this.newBody)) {
                    State[minYearName] = this.newYear;
                    State[maxYearName] = this.newYear;
                    State[makeName] = this.newMake;
                    State[modelName] = this.newModel;
                    State[bodyName] = this.newBody;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
        "MileageChange": {
            set: function() {
                var minName = this.scope + '$MinMileage',
                    maxName = this.scope + '$MaxMileage',
                    oldMin = State[minName],
                    oldMax = State[maxName];
                if ((oldMin !== this.min) || (oldMax !== this.max)) {
                    State[minName] = this.min;
                    State[maxName] = this.max;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
        "PriceChange": {
            set: function() {
                var minName = this.scope + '$MinPrice',
                    maxName = this.scope + '$MaxPrice',
                    oldMin = State[minName],
                    oldMax = State[maxName];
                if ((oldMin !== this.min) || (oldMax !== this.max)) {
                    State[minName] = this.min;
                    State[maxName] = this.max;
                    if (this.scope == 'Auction') {
                        $(new SaleSummaryArgumentsDto()).trigger("stateChange");
                    }
                    else {
                        $(new ListingArgumentsDto()).trigger("stateChange");
                    }
                }
            }
        },
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
			prop,
			subJson,
			idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        "TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
        "TraversalResultsDto": genericMapper(TraversalResultsDto, {
            "Arguments": "TraversalArgumentsDto",
            "Path": "PathDto"
        }),
        "SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
        "SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
            "Arguments": "SuccessorResultsDto",
            "Path": "PathDto"
        }),
        "SaleSummaryArgumentsDto": genericMapper(SaleSummaryArgumentsDto),
        "SaleSummaryResultsDto": genericMapper(SaleSummaryResultsDto, {
            "Arguments": "SaleSummaryArgumentsDto",
            "Results": "SaleSummaryDto"
        }),
        "ListingArgumentsDto": genericMapper(ListingArgumentsDto),
        "ListingResultsDto": genericMapper(ListingResultsDto, {
            "Arguments": "ListingArgumentsDto",
            "Results": "ListingDto"
        }),
        "DomainArgumentsDto": genericMapper(DomainArgumentsDto),
        "DomainResultsDto": genericMapper(DomainResultsDto, {
            "Arguments": "DomainArgumentsDto",
            "Auctions": "LabelValueDto",
            "ChannelCodes": "LabelValueDto",
            "Consignors": "LabelValueDto",
            "SaleDates": "LabelValueDto"
        }),
        "NodeDto": genericMapper(NodeDto, {
            "Parent": "NodeDto",
            "Children": "NodeDto"
        }),
        "PathDto": genericMapper(PathDto, {
            "CurrentNode": "NodeDto",
            "Successors": "NodeDto"
        }),
        "SaleSummaryDto": genericMapper(SaleSummaryDto),
        "ListingDto": genericMapper(ListingDto, {
            "SaleInfo": "SaleInfoDto",
            "SellerAddress": "AddressDto",
            "Vehicle": "VehicleDto"
        }),
        "VehicleDto": genericMapper(VehicleDto, {
            "Type": "VehicleTypeDto",
            "Detail": "VehicleDetailDto"
        }),
        "VehicleTypeDto": genericMapper(VehicleTypeDto, {
            "Year": "LabelValueDto",
            "Make": "LabelValueDto",
            "Model": "LabelValueDto",
            "Body": "LabelValueDto",
            "VinInfo": "VinInfoDto"
        }),
        "VehicleDetailDto": genericMapper(VehicleDetailDto, {
            "Options": "VehicleOptionDto"
        }),
        "AddressDto": genericMapper(AddressDto),
        "VinInfoDto": genericMapper(VinInfoDto),
        "SaleInfoDto": genericMapper(SaleInfoDto),
        "VehicleOptionDto": genericMapper(VehicleOptionDto),
        "LabelValueDto": genericMapper(LabelValueDto)
    };

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }
    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (console) console.log(xhr, status, errorThrown);
                    if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Traversal": genericService("/VehicleValuationGuide/Services/Manheim.asmx/Traversal", TraversalResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin)
                }
            });
        }),
        "Successor": genericService("/VehicleValuationGuide/Services/Manheim.asmx/Successor", SuccessorResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
                    "Node": this.Node,
                    "Successor": this.Successor
                }
            });
        }),
        "Domain": genericService("/VehicleValuationGuide/Services/Manheim.asmx/Domain", DomainResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes.DomainArgumentsDto",
                    "Broker": this.Broker
                }
            });
        }),
        "SaleSummary": genericService("/VehicleValuationGuide/Services/Manheim.asmx/SaleSummary", SaleSummaryResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes.SaleSummaryArgumentsDto",
                    "Broker": this.Broker,
                    "Criteria": this.Criteria,
                    "StartRowIndex": this.StartRowIndex,
                    "MaximumRows": this.MaximumRows,
                    "SortOrder": this.SortOrder
                }
            });
        }),
        "Listing": genericService("/VehicleValuationGuide/Services/Manheim.asmx/Listing", ListingResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes.ListingArgumentsDto",
                    "Broker": this.Broker,
                    "Criteria": this.Criteria,
                    "StartRowIndex": this.StartRowIndex,
                    "MaximumRows": this.MaximumRows,
                    "SortOrder": this.SortOrder
                }
            });
        })
    };

    var EventBinder = function(obj) {
        if (obj instanceof TraversalArgumentsDto) {
            $(obj).bind("fetchComplete", Events.TraversalArgumentsDto.fetchComplete);
        }
        if (obj instanceof SuccessorArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SuccessorArgumentsDto.fetchComplete);
        }
        if (obj instanceof DomainArgumentsDto) {
            $(obj).bind("fetchComplete", Events.DomainArgumentsDto.fetchComplete);
        }
        if (obj instanceof SaleSummaryArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SaleSummaryArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.SaleSummaryArgumentsDto.stateChange);
        }
        if (obj instanceof ListingArgumentsDto) {
            $(obj).bind("fetchComplete", Events.ListingArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.ListingArgumentsDto.stateChange);
        }
        if (obj instanceof SelectionChange) {
            $(obj).bind("set", Events.SelectionChange.set);
        }
        if (obj instanceof VinChange) {
            $(obj).bind("set", Events.VinChange.set);
        }
        if (obj instanceof MidChange) {
            $(obj).bind("set", Events.MidChange.set);
        }
        if (obj instanceof AuctionSelect) {
            $(obj).bind("set", Events.AuctionSelect.set);
        }
        if (obj instanceof AuctionChange) {
            $(obj).bind("set", Events.AuctionChange.set);
        }
        if (obj instanceof ChannelChange) {
            $(obj).bind("set", Events.ChannelChange.set);
        }
        if (obj instanceof ConsignorChange) {
            $(obj).bind("set", Events.ConsignorChange.set);
        }
        if (obj instanceof SaleDateChange) {
            $(obj).bind("set", Events.SaleDateChange.set);
        }
        if (obj instanceof VehicleSelect) {
            $(obj).bind("set", Events.VehicleSelect.set);
        }
        if (obj instanceof VehicleTypeChange) {
            $(obj).bind("set", Events.VehicleTypeChange.set);
        }
        if (obj instanceof MileageChange) {
            $(obj).bind("set", Events.MileageChange.set);
        }
        if (obj instanceof PriceChange) {
            $(obj).bind("set", Events.PriceChange.set);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
        BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

        BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
        BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

        BindDataMapper(NodeDto, DataMapper.NodeDto);
        BindDataMapper(PathDto, DataMapper.PathDto);

        BindDataMapper(DomainArgumentsDto, DataMapper.DomainArgumentsDto);
        BindDataMapper(DomainResultsDto, DataMapper.DomainResultsDto);

        BindDataMapper(SaleSummaryArgumentsDto, DataMapper.SaleSummaryArgumentsDto);
        BindDataMapper(SaleSummaryResultsDto, DataMapper.SaleSummaryResultsDto);

        BindDataMapper(SaleSummaryDto, DataMapper.SaleSummaryDto);

        BindDataMapper(ListingArgumentsDto, DataMapper.ListingArgumentsDto);
        BindDataMapper(ListingResultsDto, DataMapper.ListingResultsDto);

        BindDataMapper(AddressDto, DataMapper.AddressDto);
        BindDataMapper(SaleInfoDto, DataMapper.SaleInfoDto);
        BindDataMapper(VehicleDetailDto, DataMapper.VehicleDetailDto);
        BindDataMapper(VehicleTypeDto, DataMapper.VehicleTypeDto);
        BindDataMapper(VehicleDto, DataMapper.VehicleDto);
        BindDataMapper(VehicleOptionDto, DataMapper.VehicleOptionDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                if (!source.hasOwnProperty(m)) continue;
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(TraversalArgumentsDto, Services.Traversal);
        BindFetch(SuccessorArgumentsDto, Services.Successor);

        BindFetch(DomainArgumentsDto, Services.Domain);

        BindFetch(SaleSummaryArgumentsDto, Services.SaleSummary);

        BindFetch(ListingArgumentsDto, Services.Listing);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selection) {
        if (typeof selection == "undefined") {
            selection = {};
        };

        var layout = new ns.LayoutTemplateBuilder();
        layout.clean();
        layout.init();

        $(new DomainArgumentsDto()).trigger("fetch");

        if (selection.vin) {
            $(new VinChange(selection.vin)).trigger("set");
        } else if (selection.mid) {
            $(new MidChange(selection.mid)).trigger("set");
        } else {
            $(new TraversalArgumentsDto()).trigger("fetch");
            $(new ListingArgumentsDto()).trigger("fetch");
            $(new SaleSummaryArgumentsDto()).trigger("fetch");
        }
    };

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.FormatTraversalStatus = TraversalStatusDto.toString;
    this.ToIsoDate = toIsoDate;

    /* ================== */
    /* = PUBLIC CLASSES = */
    /* ================== */
    this.TraversalArgumentsDto = TraversalArgumentsDto;
    this.SuccessorArgumentsDto = SuccessorArgumentsDto;

    this.SelectionChange = SelectionChange;
    this.VinChange = VinChange;
    this.MidChange = MidChange;

    this.AuctionSelect = AuctionSelect;
    this.AuctionChange = AuctionChange;
    this.ChannelChange = ChannelChange;
    this.ConsignorChange = ConsignorChange;
    this.SaleDateChange = SaleDateChange;
    this.MileageChange = MileageChange;
    this.PriceChange = PriceChange;
    this.VehicleSelect = VehicleSelect;
    this.VehicleTypeChange = VehicleTypeChange;

}).apply(Manheim);
