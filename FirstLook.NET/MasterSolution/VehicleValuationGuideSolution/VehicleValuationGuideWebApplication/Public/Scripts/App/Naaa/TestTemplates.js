if (typeof Naaa !== "function") {
    var Naaa = function() {};
} 
(function() {
    this.raises = this.raises || [];
    this.raises.push("Naaa.StateChange");
    this.raises.push("Naaa.MileageChange");
    this.raises.push("Naaa.VinChange");
    this.raises.push("Naaa.UidChange");
    this.raises.push("Naaa.SelectionChange");
    this.raises.push("Naaa.AddOption");
    this.raises.push("Naaa.RemoveOption");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Naaa.FormLoaded");
    this.listensTo.push("Naaa.TraversalLoaded");
    this.listensTo.push("Naaa.SuccessorLoaded");
    this.listensTo.push("Naaa.InitialValueLoaded");
    this.listensTo.push("Naaa.AdjustedValueLoaded");
    
    var PublicEvents = {
        "Naaa.FormLoaded": function(evt, data, area, period) {
            var template = new FormTemplateBuilder(data, area, period);
            template.clean();
            template.init();
        },
        "Naaa.ValuationLoaded": function(evt, data) {
            var template = new ValuationTemplateBuilder(data);
            template.init();
            template.build();
        },
        "Naaa.TraversalLoaded": function(evt, path, status) {
            var template = new NodeTemplateBuilder(path, status);
            template.init();
            template.build();
        },
        "Naaa.SuccessorLoaded": function(evt, path) {
            var template = new NodeTemplateBuilder(path, 0);
            template.init();
            template.build();
        },
        "Naaa.TransactionsLoaded": function(evt, data) {
            var template = new TransactionsTemplateBuilder(data);
            template.init();
            template.build();
        }
    };
    $(Naaa).bind(PublicEvents);

    var FormTemplateBuilder = function(data, area, period) {
        this.data = data;
        this.defaultArea = area;
        this.defaultPeriod = period;
    };

    FormTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var populate = function(select, list, selected, change) {
                select.html('');
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    var state = $('<option></option>').val(item.Id).html(item.Name);
                    if (item.Id == this.defaultState) {
                        state.attr('selected', 'selected');
                    }
                    select.append(state);
                }
                select.unbind();
                select.bind('change', change);
            };
            // fill drop down and listen for changes
            populate($('#area'), this.data.Areas, this.defaultArea, this._areaChange);
            populate($('#period'), this.data.Periods, this.defaultPeriod, this._periodChange);
            // listen for mileage changes
            var input = $('#mileage');
            input.unbind();
            input.bind('change', this._mileageChange);
        },
        clean: function() {

        },
        _areaChange: function(evt) {
            $(Naaa).trigger("Naaa.AreaChange", $("#area").val());
        },
        _periodChange: function(evt) {
            $(Naaa).trigger("Naaa.PeriodChange", $("#period").val());
        },
        _mileageChange: function(evt) {
            $(Naaa).trigger("Naaa.MileageChange", $("#mileage").val());
        }
    };

    var NodeTemplateBuilder = function(data, status) {
        this.data = data;
        this.status = status;
    };

    NodeTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var bindDropDown = function(s, v, p, items, selected) {
                // add prompt
                var prompt = $('<option></option>').val('').html('... Please Select ...');
                s.html('');
                s.append(prompt);
                // add items
                for (i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    var val = item.State;
                    if (item.HasUid == true && item.Uid != 0) {
                        val = item.Uid;
                    }
                    var opt = $('<option></option>').val(val).html(item.Name);
                    if (item.Id == selected) {
                        opt.attr('selected', 'selected');
                    }
                    s.append(opt);
                }
                // attach change event
                if (v == false) {
                    function fetchSuccessor(evt) {
                        var next = s.val();
                        if (next != '') {
                            // empty follow up drop downs
                            var inputs = ['year', 'make', 'series', 'body_style', 'synthetic'].slice(p);
                            for (var i = 0; i < inputs.length; i++) {
                                document.getElementById(inputs[i]).options.length = 0
                            }
                            // reset vin
                            if (inputs.length > 0) {
                                $(Naaa).trigger("Naaa.VinChange", '');
                                $('#vin').val('');
                            }

                            $(Naaa).trigger("Naaa.SelectionChange", [next]);
                        }
                    }
                    s.unbind();
                    s.bind('change', fetchSuccessor);
                }
                else {
                    function fetchInitialValuation(evt) {
                        $(Naaa).trigger("Naaa.UidChange", s.val());
                    }
                    s.unbind();
                    s.bind('change', fetchInitialValuation);
                }
            };
            var bindQueryByVin = function() {
                var decode = $('#NaaaForm');
                var query = function(evt) {
                    evt.preventDefault();
                    // remember vin
                    $(Naaa).trigger("Naaa.VinChange", $("#vin").val(), true);
                }
                decode.unbind('submit', query);
                decode.bind('submit', query);
            };
            var bindNode = function(node, successors, selected) {
                var valuation = successors.length > 0 && !!successors[0].HasUid;
                var positions = {
                    'Year': 2,
                    'Make': 3,
                    'Series': 4,
                    'Body': 5
                };
                var position = positions[node.Label] || 1;
                switch (node.Label) {
                    case '':
                        bindQueryByVin();
                        bindDropDown($("#year"), valuation, position, successors, selected);
                        break;
                    case 'Year':
                        bindDropDown($("#make"), valuation, position, successors, selected);
                        break;
                    case 'Make':
                        bindDropDown($("#series"), valuation, position, successors, selected);
                        break;
                    case 'Series':
                        bindDropDown($("#body_style"), valuation, position, successors, selected);
                        break;
                    case 'Vin':
                        if (node.HasUid == true && node.Uid != 0) {
                            $(Naaa).trigger("Naaa.UidChange", node.Uid);
                            selected = successors[0].Id;
                        }
                        position = positions[node.Parent.Label];
                        if (node.Parent.Label == 'Series') {
                            bindDropDown($("#body_style"), valuation, position, successors, selected);
                        }
                        else {
                            var x = [];
                            for (var j = 0; j < successors.length; j++) {
                                var k = successors[j];
                                var t = [];
                                var s = k;
                                while (s != null) {
                                    t.push(s.Name);
                                    s = s.Parent;
                                }
                                var n = t.reverse().join(' ');
                                x.push({
                                    'Uid': k.Uid,
                                    'HasUid': k.HasUid,
                                    'Id': k.Id,
                                    'Name': n,
                                    'State': k.State,
                                    'Label': 'Synthetic'
                                });
                            }
                            bindDropDown($("#synthetic"), valuation, position, x);
                        }
                        break;
                }
            };
            var bindNodeRecursive = function(node, successors, selected) {
                if (node.Parent != null) {
                    bindNodeRecursive(node.Parent, node.Parent.Children, node.Id);
                }
                bindNode(node, successors, selected);
            };
            if (this.status == 0) {
                // structure: '' > 'year' > 'make' > 'series' [> 'vin'] > 'body'
                if (this.data.CurrentNode.Label == 'Vin') {
                    bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    bindNode(this.data.CurrentNode, this.data.Successors);
                }
                $("#traversal_status").html('');
            }
            else {
                $("#traversal_status").html(Naaa.FormatTraversalStatus(this.status));
            }
        },
        clean: function() {
            $("#transaction_body").empty();
        }
    };

    var ValuationTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            'overall_count': $('#overall_count'),
            'similar_count': $('#similar_count'),
            'sale_price_max': $('#sale_price_max'),
            'sale_price_avg': $('#sale_price_avg'),
            'sale_price_min': $('#sale_price_min'),
            'sale_price_similar': $('#sale_price_similar'),
            'mileage_max': $('#mileage_max'),
            'mileage_avg': $('#mileage_avg'),
            'mileage_min': $('#mileage_min'),
            'mileage_similar': $('#mileage_similar')
        };
    };

    ValuationTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var report = this.data.Report;
            // counts
            this.$dom.overall_count.html(report.SalePriceReport.SampleSize);
            this.$dom.similar_count.html(report.SalePriceSimilarMileageReport.SampleSize);
            // sale price
            this.$dom.sale_price_max.html(report.SalePriceReport.Maximum);
            this.$dom.sale_price_avg.html(report.SalePriceReport.Average);
            this.$dom.sale_price_min.html(report.SalePriceReport.Minimum);
            this.$dom.sale_price_similar.html(report.SalePriceSimilarMileageReport.SampleSize);
            // mileage
            this.$dom.mileage_max.html(report.MileageReport.Maximum);
            this.$dom.mileage_avg.html(report.MileageReport.Average);
            this.$dom.mileage_min.html(report.MileageReport.Minimum);
            this.$dom.mileage_similar.html(report.LowMileage + ' - ' + report.HighMileage);
        },
        clean: function() {
            var notApplicable = '--';
            // counts
            $('#overall_count').html(notApplicable);
            $('#similar_count').html(notApplicable);
            // sale price
            $('#sale_price_max').html(notApplicable);
            $('#sale_price_avg').html(notApplicable);
            $('#sale_price_min').html(notApplicable);
            $('#sale_price_similar').html(notApplicable);
            // mileage
            $('#mileage_max').html(notApplicable);
            $('#mileage_avg').html(notApplicable);
            $('#mileage_min').html(notApplicable);
            $('#mileage_similar').html(notApplicable);
        }
    };

    var TransactionsTemplateBuilder = function(data) {
        this.data = data;
        this.$el = $('#transaction_body');
    };

    TransactionsTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var d = this.data.Transactions;
            var l = d.length;
            var tb = '';
            for (var i = 0; i < l; i++) {
                var t = d[i];
                tb += '<tr>'
                    + '<td>' + t.RowNumber + '</td>'
                    + '<td>' + t.SaleDate + '</td>'
                    + '<td>' + t.RegionName + '</td>'
                    + '<td>' + t.SaleTypeName + '</td>'
                    + '<td>' + t.Series + '</td>'
                    + '<td>' + t.SalePrice + '</td>'
                    + '<td>' + t.Mileage + '</td>'
                    + '<td>' + t.Engine + '</td>'
                    + '<td>' + t.Transmission + '</td>'
                    + '</tr>';
            }

            this.$el.html(tb);
        },
        clean: function() {
            this.$el.html('');
        }
    };

}).apply(Naaa);
