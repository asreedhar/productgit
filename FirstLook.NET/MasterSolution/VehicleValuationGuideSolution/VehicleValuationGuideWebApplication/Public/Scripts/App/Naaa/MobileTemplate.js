if (typeof Naaa !== "function") {
    var Naaa = function() {};
} (function() {
    this.raises = this.raises || [];
    this.raises.push("Naaa.StateChange");
    this.raises.push("Naaa.MileageChange");
    this.raises.push("Naaa.VinChange");
    this.raises.push("Naaa.UidChange");
    this.raises.push("Naaa.SelectionChange");
    this.raises.push("Naaa.AddOption");
    this.raises.push("Naaa.RemoveOption");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Naaa.FormLoaded");
    this.listensTo.push("Naaa.TraversalLoaded");
    this.listensTo.push("Naaa.SuccessorLoaded");
    this.listensTo.push("Naaa.InitialValueLoaded");
    this.listensTo.push("Naaa.AdjustedValueLoaded");

    var State = {
        Areas: [],
        Periods: [],
        Mileage: 0,
        CurrentArea: 1,
        CurrentPeriod: 11,
        TotalRowCount: 0,
        MaximumRows: 0,
        StartRowIndex: 0,
        Loaded: false,
        TransactionsLoaded: false,
        ReportLoaded: false,
        Sort: "SaleTypeName",
        Transactions: []
    };

    var PublicEvents = {
        "Naaa.FormLoaded": function(evt, data, currentArea, currentPeriod) {
            State.Areas = _.reduce(data.Areas, function(memo, state) {
                memo.push({
                    name: state.Name,
                    value: state.Id
                });
                return memo;
            },
            []);
            State.Periods = _.reduce(data.Periods, function(memo, state) {
                memo.push({
                    name: state.Name,
                    value: state.Id
                });
                return memo;
            },
            []);
            State.CurrentArea = currentArea;
            State.CurrentPeriod = currentPeriod;
        },
        "Naaa.TraversalLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");

            card.build(data);
        },
        "Naaa.SuccessorLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");

            if (data.CurrentNode && data.CurrentNode.HasVehicleId) {
                $(Naaa).trigger("Naaa.UidChange", data.CurrentNode.VehicleId);
            } else {
                card.build(data);
            }
        },
        "Naaa.ValuationLoaded": function(evt, data) {
            State.Report = data.Report;
            State.Mileage = data.Arguments.Mileage;
            State.CurrentArea = data.Arguments.Area;
            State.CurrentPeriod = data.Arguments.Period;
            State.ValuationLoaded = true;
            if (State.TransactionsLoaded) {
                $(Naaa).trigger("Naaa.ReportLoaded", State);
            }
        },
        "Naaa.TransactionsLoaded": function(evt, data) {
            State.Transactions = data.Transactions;
            State.CurrentArea = data.Arguments.Area;
            State.CurrentPeriod = data.Arguments.Period;
            State.StartRowIndex = data.Arguments.StartRowIndex;
            State.MaximumRows = data.Arguments.MaximumRows;
            State.TotalRowCount = data.TotalRowCount;
            State.TransactionsLoaded = true;
            if (State.ValuationLoaded) {
                $(Naaa).trigger("Naaa.ReportLoaded", State);
            }
        },
        /// Component Bridge Events
        "Naaa.ReportLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");

            if (State.Loaded) {
                $(card.views.report.Report).trigger("rebuild", data);
                $(card.views.report.TransactionList).trigger("rebuild", data);
            } else if (State.TransactionsLoaded && State.ValuationLoaded) {
                card.build(data);
                State.Loaded = true;
            }
        },
        "Naaa.SelectionPathChange": function(evt, data) {
            if (data.Uid !== "0") {
                $(Naaa).trigger("Naaa.UidChange", data.Uid);
            } else {
                $(Naaa).trigger("Naaa.SelectionChange", data.State);
            }
        },
        "Naaa.SelectionChangeWithDecode": function(evt, data) {
            var newSelection = JSON.parse(data.NewNaaaSelection);
            $(Naaa).trigger("Naaa.SelectionPathChange", newSelection);
        },
        "Naaa.FormChange": function(evt, data) {
            $(Naaa).trigger("Naaa.MileageChange", data.Mileage || 0);
            $(Naaa).trigger("Naaa.PeriodChange", data.Period || 11);
            $(Naaa).trigger("Naaa.AreaChange", data.Area || 1);
        },
        "Naaa.MileageChange": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");
            if (card.views.report.Selection.$el) {
                card.views.report.Selection.$el.find("input[name=Mileage]").val(data);
            }
        },
        "Naaa.StateChange": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");
            if (card.views.report.Selection.$el) {
                card.views.report.Selection.$el.find("select[name=State]").val(data);
            }
        },
        "Naaa.SortChange": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");

            State.Sort = data.Sort;
            
            $(card.views.report.TransactionList).trigger("rebuild", State);
        },
        "Naaa.ErrorState": function(evt, data) {
            var card = FirstLook.cards.get("Naaa");
            card.build(data);
        }
    };
    $(Naaa).bind(PublicEvents);

    function make_report() {
        return {
            type: "Table",
            pattern: {
                Report: {
                    SalePriceReport: {
                        Minimum: _.isNumber,
                        Average: _.isNumber,
                        Maximum: _.isNumber,
                        SampleSize: _.isNumber
                    },
                    SalePriceSimilarMileageReport: {
                        Minimum: _.isNumber,
                        Average: _.isNumber,
                        Maximum: _.isNumber,
                        SampleSize: _.isNumber
                    },
                    MileageReport: {
                        Minimum: _.isNumber,
                        Average: _.isNumber,
                        Maximum: _.isNumber,
                        SampleSize: _.isNumber
                    },
                    LowMileage: _.isNumber,
                    HighMileage: _.isNumber
                }
            },
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: function(data) {
                var table = [],
                report = data.Report;

                function row_builder(acc, title, format) {
                    var r = report[acc];
                    if (!format) {
                        format = _.identity;
                    }
                    return [title, format(r.Minimum), format(r.Average) + " of " + r.SampleSize, format(r.Maximum)];
                }

                table.head = [["", "High", "Avg", "Low"]];
                table.col_class_name = ["", "high", "avg", "low"];
                table.header_col = 0;
                table.push(row_builder("SalePriceReport", "Sale Prices", _.money));
                table.push(row_builder("MileageReport", "Mileages", _.roundToK));
                table.push(row_builder("SalePriceSimilarMileageReport", "Like Mileage Prices"), _.money);

                data.caption = "NAAA Like Mileage: " + report.LowMileage + " " + report.HighMileage;

                data.table = table;
                return data;
            },
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            body_header_col_accessor: "table.header_col"

        };
    }

    function make_select() {
        return {
            type: "Fieldset",
            pattern: {
                Successors: [{
                    State: _.isString,
                    Label: _.isString,
                    Name: _.isString,
                    Uid: _
                }]
            },
            behaviors: {
                submitable: {
                    event: "Naaa.SelectionChangeWithDecode",
                    scope: Naaa,
                    submit_on_change: true
                }
            },
            legend_accessor: "fieldset.legend",
            input_accessor: "fieldset.inputs",
            name_accessor: "name",
            button_accessor: "fieldset.buttons",
            label_accessor: "label",
            value_accessor: "value",
            type_accessor: "type",
            filter: function(data) {
                var options = _.map(data.Successors, function(succ) {
                    return {
                        name: succ.Name,
                        value: JSON.stringify({State: succ.State, Uid: succ.Uid})
                    };
                }),
                fieldset = {
                    legend: "NAAA " + data.Successors[0].Label,
                    inputs: [{
                        name: "NewNaaaSelection",
                        label: data.Successors[0].Label,
                        options: options
                    }],
                    buttons: []
                };
                options.unshift({name: "Select...", value: ""});
                data.fieldset = fieldset;
                return data;
            }
        };
    }

    FirstLook.components.add(make_report(), "Naaa.Report");
    FirstLook.components.add(make_select(), "Naaa.Select");

    var state_pattern = {
        select: {
            CurrentNode: _,
            Successors: function(v) {
                return _.isArray(v) && v.length > 0;
            }
        },
        report: {
            Report: _,
            Transactions: _
        },
        error: {
            errorType: _,
            errorResponse: _
        }
    };

    var overview_card = {
        pattern: _,
        view: {
            select: {
                pattern: state_pattern.select,
                select: FirstLook.components.get("Naaa.Select")
            },
            report: {
                pattern: state_pattern.report,
                select: FirstLook.components.get("Naaa.Report")
            },
            error: {
                pattern: state_pattern.error,
                select: FirstLook.components.get("Application.ServerError")
            }
        }
    };
    FirstLook.cards.add(overview_card, "NaaaSummary");

    var cards = {
        title: "Naaa",
        pattern: _,
        view: {
            select: {
                order: 0,
                pattern: state_pattern.select,
                Title: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<h1 class='logo_naaa'>NAAA</h1>";
                    }
                },
                Review: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<p>"+_.pluck(data._selection, "Name").join(" | ")+"</p>";
                    },
                    filter: function(data) {
                        var selection = [];
                        function append_parent(v) {
                            selection.push(v);
                            if (v.Parent) {
                                append_parent(v.Parent);
                            }
                            return _.first(selection, selection.length-1);
                        }
                        
                        data._selection = append_parent(data.CurrentNode);
                        return data;
                    }
                },
                TraversalList: {
                    type: "List",
                    class_name: "select_list vid_select",
                    pattern: {
                        Successors: [{
                            Name: _.isString,
                            Id: _.isNumber
                        }]
                    },
                    behaviors: {
                        selectable: {
                            pattern: {
                                Successors: [{
                                    State: _.isString,
                                    Uid: _.isNumber
                                }]
                            },
                            data_accessor: ["State", "Uid"],
                            binds: {
                                "item_selected": {
                                    type: "Naaa.SelectionPathChange",
                                    scope: Naaa
                                }
                            }
                        },
                        rebuildable: {}
                    },
                    item_accessor: "Successors",
                    text_accessor: "Name",
                    data_accessor: "State"
                }
            },
            report: {
                order: 1,
                pattern: state_pattern.report,
                Selection: {
                    type: "Fieldset",
                    pattern: {
                        Mileage: _.isNumber,
                        CurrentArea: _.isNumber,
                        CurrentPeriod: _.isNumber
                    },
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "Naaa.FormChange",
                            scope: Naaa,
                            submit_on_change: true
                        }
                    },
                    class_name: "book_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "NAAA",
                        inputs: [{
                            name: "Area",
                            label: "States",
                            data: "Area",
                            options: "Areas"
                        },
                        {
                            name: "Period",
                            label: "Period",
                            data: "Period",
                            options: "Periods"
                        },
                        {
                            name: "Mileage",
                            label: "Mileage",
                            data: "Mileage",
                            type: "tel"
                        }],
                        buttons: ["Update"]
                    }
                },
                Report: FirstLook.components.get("Naaa.Report"),
                ListControls: {
                    type: "Fieldset",
                    pattern: _,
                    behaviors: {
                        submitable: {
                            event: "Naaa.SortChange",
                            scope: Naaa,
                            submit_on_change: true
                        }
                    },
                    class_name: "book_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "Transactions",
                        inputs: [{
                            name: "Sort",
                            label: "Sort",
                            options: [{
                                name: "SaleType",
                                value: "SaleTypeName"
                            },{
                                name: "Region",
                                value: "RegionName"
                            }, {
                                name: "Sale Date",
                                value: "FormatedSaleDate"
                            }, {
                                name: "Engine",
                                value: "Engine"
                            }, {
                                name: "Transmission",
                                value: "Transmission"
                            }]
                        }],
                        buttons: ["Update"]
                    }
                },
                TransactionList: {
                    type: "GroupedList",
                    pattern: {
                        Transactions: [{
                            Series: _.isString,
                            SalePrice: _,
                            Mileage: _
                        }]
                    },
                    behaviors: {
                        rebuildable: {},
                        pageable: {
                            pattern: {
                                MaximumRows: _.isNumber,
                                StartRowIndex: _.isNumber,
                                TotalRowCount: _.isNumber
                            },
                            page_size_accessor: "MaximumRows",
                            total_items_accessor: "TotalRowCount",
                            start_index_accessor: "StartRowIndex"
                        },
                        infinate_scrollable: {}
                    },
                    class_name: "equipment_list",
                    item_accessor: "SortedTransactions",
                    text_accessor: ["Description", "FormatedMileage", "FormatedSalePrice"],
                    data_accessor: ["RowNumber", "SalePrice"],
                    filter: function(data) {
                        var d = data;
                        var isSaleType = State.Sort == "SaleTypeName";
                        function format(t) {
                            t.FormatedMileage = _.roundToK(t.Mileage);
                            t.FormatedSalePrice = _.money(t.SalePrice);
                            t.FormatedSaleDate = _.asmxDate(t.SaleDate);
                            t.Description = isSaleType ? t.RegionName : t.SaleTypeName;

                            return t;
                        }
                        _.each(d.Transactions, format);
                        d.SortedTransactions = _.groupBy(d.Transactions, State.Sort);
                        return d;
                    }
                }
            },
            error: {
                pattern: state_pattern.error,
                ErrorMessage: FirstLook.components.get("Application.ServerError")
            }
        }
    };

    FirstLook.cards.add(cards, "Naaa");
}).apply(Naaa);

