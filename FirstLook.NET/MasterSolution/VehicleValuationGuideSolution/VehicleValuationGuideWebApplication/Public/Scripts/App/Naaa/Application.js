if (typeof Naaa !== "function") {
    var Naaa = function() {};
}

(function() {
    var ns = this;

    var State = {
        Vin: '',
        Uid: '',
        Vic: '',
        Mileage: 0,
        Area: 1,
        /// TODO: Remove hardcoded 'National'
        Period: 11 /// TODO: Remove hardcoded '2 week'
    };

    /* ==================== */
    /* == START: Classes == */
    /* ==================== */

    /* form dto */

    function AreaDto() {
        this.Id = 0;
        this.Name = '';
    }

    function PeriodDto() {
        this.Id = 0;
        this.Name = '';
    }

    /* valuation dto */

    function ReportDto() {
        this.SalePriceReport = null;
        this.SalePriceSimilarMileageReport = null;
        this.MileageReport = null;
        this.LowMileage = 0;
        this.HighMileage = 0;
    }

    function AggregateDto() {
        this.Minimum = null;
        this.Average = null;
        this.Maximum = null;
        this.SampleSize = 0;
        this.HasMinimum = false;
        this.HasAverage = false;
        this.HasMaximum = false;
    }

    /* transaction dto */

    function TransactionDto() {
        this.RowNumber = 0;
        this.SaleDate = new Date();
        this.RegionName = '';
        this.SaleTypeName = '';
        this.Series = '';
        this.SalePrice = 0.0;
        this.Mileage = 0;
        this.Engine = '';
        this.Transmission = '';
    }

    /* envelopes dto */

    function FormArgumentsDto() {
        EventBinder(this);
    }

    function FormResultsDto() {
        this.Arguments = null;
        this.Areas = [];
        this.Periods = [];
    }

    function ValuationArgumentsDto() {
        this.Uid = 0;
        this.Area = 0;
        this.Period = 0;
        this.Mileage = 0;
        EventBinder(this);
    }

    function ValuationResultsDto() {
        this.Report = null;
        this.Arguments = null;
    }

    function TransactionsArgumentsDto() {
        this.SortColumns = '';
        this.MaximumRows = 25;
        this.StartRowIndex = 0;
        this.Uid = 0;
        this.Area = 0;
        this.Period = 0;
        EventBinder(this);
    }

    function TransactionsResultsDto() {
        this.TotalRowCount = 0;
        this.Transactions = [];
        this.Arguments = null;
    }

    /* traversal */

    function PathDto() {
        this.CurrentNode = null;
        this.Successors = [];
    }

    function NodeDto() {
        this.Parent = null;
        this.Children = [];
        this.Uid = 0;
        this.HasUid = false;
        this.Id = 0;
        this.Name = "";
        this.State = "";
        this.Label = "";
    }

    var TraversalStatusDto = {
        "Success": 0,
        "InvalidVinLength": 1,
        "InvalidVinCharacters": 2,
        "InvalidVinChecksum": 3,
        "NoDataForVin": 4,
        "toString": function(i) {
            var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    /* envelopes */

    function TraversalArgumentsDto() {
        this.Vin = '';
        EventBinder(this);
    }

    function TraversalResultsDto() {
        this.Arguments = null;
        this.Path = null;
        this.Status = 0;
    }

    function SuccessorArgumentsDto() {
        this.Node = "";
        this.Successor = "";
        EventBinder(this);
    }

    function SuccessorResultsDto() {
        this.Arguments = null;
        this.Path = null;
    }

    /* == END: Classes == */
    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Naaa).trigger("Naaa.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if ( !! response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Form": genericService("/VehicleValuationGuide/Services/Naaa.asmx/Form", FormResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes.FormArgumentsDto"
                }
            });
        }),
        "Traversal": genericService("/VehicleValuationGuide/Services/Nada.asmx/Traversal", TraversalResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
                    "Vin": (this.Vin == null ? '': this.Vin)
                }
            });
        }),
        "Successor": genericService("/VehicleValuationGuide/Services/Nada.asmx/Successor", SuccessorResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
                    "Node": this.Node,
                    "Successor": this.Successor
                }
            });
        }),
        "Valuation": genericService("/VehicleValuationGuide/Services/Naaa.asmx/Valuation", ValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes.ValuationArgumentsDto",
                    "Area": this.Area,
                    "Period": this.Period,
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage: 0),
                    "Uid": this.Uid
                }
            });
        }),
        "Transactions": genericService("/VehicleValuationGuide/Services/Naaa.asmx/Transactions", TransactionsResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes.TransactionsArgumentsDto",
                    "Area": this.Area,
                    "Period": this.Period,
                    "Uid": this.Uid,
                    "SortColumns": this.SortColumns,
                    "MaximumRows": this.MaximumRows,
                    "StartRowIndex": this.StartRowIndex
                }
            });
        })
    };

    var Events = {
        "FormArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Naaa).trigger("Naaa.FormLoaded", [data, State.Area, State.Period]);
            }
        },
        "TraversalArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                var hasUid = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.Uid;
                if (hasUid) {
                    $(Naaa).trigger("Naaa.UidChange", [data.Path.CurrentNode.Uid]);
                }
                if (!hasUid || this.RunNodeTemplate) {
                    /// Review the test page template to see why this should be called, PM
                    $(Naaa).trigger("Naaa.TraversalLoaded", [data.Path, data.Status]);
                }
            }
        },
        "SuccessorArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                $(Naaa).trigger("Naaa.SuccessorLoaded", [data.Path, 0]);
            }
        },
        "ValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // build the page
                $(Naaa).trigger("Naaa.ValuationLoaded", [data]);
            },
            stateChange: function(event) {
                if (State.Uid !== '') {
                    var valuation = new ValuationArgumentsDto();
                    valuation.Uid = State.Uid;
                    valuation.Area = State.Area;
                    valuation.Period = State.Period;
                    valuation.Mileage = State.Mileage;
                    $(valuation).trigger("fetch");

                    var transactions = new TransactionsArgumentsDto();
                    transactions.Uid = State.Uid;
                    transactions.Area = State.Area;
                    transactions.Period = State.Period;
                    transactions.SortColumns = 'RowNumber';
                    transactions.MaximumRows = 25;
                    transactions.StartRowIndex = 0;
                    $(transactions).trigger("fetch");
                }
            }
        },
        "TransactionsArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // build the page
                $(Naaa).trigger("Naaa.TransactionsLoaded", [data]);
            },
            stateChange: function(event, action) {
                /// TODO: Pagination
            }
        }
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        "FormArgumentsDto": genericMapper(FormArgumentsDto),
        "FormResultsDto": genericMapper(FormResultsDto, {
            "Arguments": "FormArgumentsDto",
            "Areas": "AreaDto",
            "Periods": "PeriodDto"
        }),
        "TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
        "TraversalResultsDto": genericMapper(TraversalResultsDto, {
            "Arguments": "TraversalArgumentsDto",
            "Path": "PathDto"
        }),
        "SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
        "SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
            "Arguments": "SuccessorArgumentsDto",
            "Path": "PathDto"
        }),
        "NodeDto": genericMapper(NodeDto, {
            "Children": "NodeDto",
            "Parent": "NodeDto"
        }),
        "PathDto": genericMapper(PathDto, {
            "CurrentNode": "NodeDto",
            "Successors": "NodeDto"
        }),
        "ValuationArgumentsDto": genericMapper(ValuationArgumentsDto),
        "ValuationResultsDto": genericMapper(ValuationResultsDto, {
            "Arguments": "ValuationArgumentsDto",
            "Report": "ReportDto"
        }),
        "TransactionsArgumentsDto": genericMapper(TransactionsArgumentsDto),
        "TransactionsResultsDto": genericMapper(TransactionsResultsDto, {
            "Arguments": "TransactionsArgumentsDto",
            "Transactions": "TransactionDto"
        }),
        "ReportDto": genericMapper(ReportDto, {
            "SalePriceReport": "AggregateDto",
            "SalePriceSimilarMileageReport": "AggregateDto",
            "MileageReport": "AggregateDto"
        }),
        "AggregateDto": genericMapper(AggregateDto),
        "TransactionDto": genericMapper(TransactionDto),
        "AreaDto": genericMapper(AreaDto),
        "PeriodDto": genericMapper(PeriodDto)
    };

    var EventBinder = function(obj) {
        if (obj instanceof FormArgumentsDto) {
            $(obj).bind("fetchComplete", Events.FormArgumentsDto.fetchComplete);
        }
        if (obj instanceof TraversalArgumentsDto) {
            $(obj).bind("fetchComplete", Events.TraversalArgumentsDto.fetchComplete);
        }
        if (obj instanceof SuccessorArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SuccessorArgumentsDto.fetchComplete);
        }
        if (obj instanceof ValuationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.ValuationArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.ValuationArgumentsDto.stateChange);
        }
        if (obj instanceof TransactionsArgumentsDto) {
            $(obj).bind("fetchComplete", Events.TransactionsArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.TransactionsArgumentsDto.stateChange);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(FormArgumentsDto, DataMapper.FormArgumentsDto);
        BindDataMapper(FormResultsDto, DataMapper.FormResultsDto);

        BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
        BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

        BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
        BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

        BindDataMapper(NodeDto, DataMapper.NodeDto);
        BindDataMapper(PathDto, DataMapper.PathDto);

        BindDataMapper(ValuationArgumentsDto, DataMapper.ValuationArgumentsDto);
        BindDataMapper(ValuationResultsDto, DataMapper.ValuationResultsDto);

        BindDataMapper(TransactionsArgumentsDto, DataMapper.TransactionsArgumentsDto);
        BindDataMapper(TransactionsResultsDto, DataMapper.TransactionsResultsDto);

        BindDataMapper(ReportDto, DataMapper.ReportDto);
        BindDataMapper(AggregateDto, DataMapper.AggregateDto);
        BindDataMapper(TransactionDto, DataMapper.TransactionDto);
        BindDataMapper(AreaDto, DataMapper.AreaDto);
        BindDataMapper(PeriodDto, DataMapper.PeriodDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                if (!source.hasOwnProperty(m)) continue;
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(FormArgumentsDto, Services.Form);

        BindFetch(TraversalArgumentsDto, Services.Traversal);
        BindFetch(SuccessorArgumentsDto, Services.Successor);

        BindFetch(ValuationArgumentsDto, Services.Valuation);
        BindFetch(TransactionsArgumentsDto, Services.Transactions);
    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selection) {
        if (typeof selection == "undefined") {
            selection = {};
        }

        $(new FormArgumentsDto()).trigger("fetch");

        if (selection.state) {
            State.State = selection.state;
        }
        if (selection.mileage) {
            State.Mileage = selection.mileage;
        }

        if (selection.vin) {
            $(Naaa).trigger("Naaa.VinChange", selection.vin);
        } else if (selection.uid) {
            $(Naaa).trigger("Naaa.UidChange", selection.uid);
        } else {
            $(new TraversalArgumentsDto()).trigger("fetch");
        }
    };

    var PublicEvents = {
        "Naaa.AreaChange": function(evt, newArea) {
            if (State.Area !== newArea) {
                State.Area = newArea;
                $(new ValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Naaa.PeriodChange": function(evt, newPeriod) {
            if (State.Period !== newPeriod) {
                State.Period = newPeriod;
                $(new ValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Naaa.MileageChange": function(evt, newMileage) {
            if (State.Mileage !== newMileage) {
                State.Mileage = newMileage;
                $(new ValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Naaa.SelectionChange": function(evt, next, successors) {
            var hasUid = (successors && successors[0] && successors[0].Uid);
            if (hasUid) {
                $(Naaa).trigger("Naaa.UidChange", [next]);
            } else {
                var successor = new SuccessorArgumentsDto();
                successor.Successor = next;
                $(successor).trigger('fetch');
            }
        },
        "Naaa.VinChange": function(evt, newVin, runNodeTemplate) {
            if (State.Vin !== newVin) {
                State.Vin = newVin;

                if (newVin !== '') {
                    var traversal = new TraversalArgumentsDto();
                    traversal.Vin = State.Vin;
                    traversal.RunNodeTemplate = runNodeTemplate;
                    $(traversal).trigger("fetch");

                    $(new ValuationArgumentsDto()).trigger("stateChange");
                }
            }
        },
        "Naaa.UidChange": function(evt, newUid) {
            if (State.Uid !== newUid) {
                State.Uid = newUid;

                $(new ValuationArgumentsDto()).trigger("stateChange");
            }
        }
    };
    $(Naaa).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.FormatTraversalStatus = TraversalStatusDto.toString;
}).apply(Naaa);

