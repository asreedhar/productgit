if (typeof Nada !== "function") {
    var Nada = function() {};
}

(function() {

    var ns = this;

    var State = {
        Mileage: null,
        State: 'IL',
        /// TODO: remove hardcoded default State, PM
        Vin: '',
        Uid: '',
        VehicleConfiguration: null,
        /// Publication State
        Broker: '',
        Vehicle: ''
    };

    /* ==================== */
    /* == START: Classes == */
    /* ==================== */
    /* traversal dto */
    function PathDto() {
        this.CurrentNode = null;
        this.Successors = [];
    }

    function NodeDto() {
        this.Parent = null;
        this.Children = [];
        this.Uid = 0;
        this.HasUid = false;
        this.Id = 0;
        this.Name = "";
        this.State = "";
        this.Label = "";
    }

    /* valuation dto */
    function PriceTableDto() {
        this.HasTradeInClean = true;
        this.HasTradeInAverage = true;
        this.HasTradeInRough = true;
        this.HasLoan = true;
        this.HasRetail = true;
        this.Rows = [];
    }

    function PriceTableRowDto() {
        this.RowType = null;
        this.Prices = null;
    }

    var PriceTableRowTypeDto = {
        "Undefined": 0,
        "Base": 1,
        "Option": 2,
        "Mileage": 3,
        "Final": 4,
        "toString": function(i) {
            var names = ["Undefined", "Base", "Options", "Mileage", "Final"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function PricesDto() {
        this.TradeInClean = 0.0;
        this.TradeInAverage = 0.0;
        this.TradeInRough = 0.0;
        this.Loan = 0.0;
        this.Retail = 0.0;
        this.HasTradeInClean = true;
        this.HasTradeInAverage = true;
        this.HasTradeInRough = true;
        this.HasLoan = true;
        this.HasRetail = true;
    }

    function AccessoryDto() {
        this.Code = 0;
        this.Description = "";
        this.Prices = null;
    }

    function VehicleConfigurationDto() {
        this.Period = null;
        this.Uid = 0;
        this.Vin = "";
        this.State = null;
        this.Mileage = null;
        this.HasMileage = false;
        this.AccessoryStates = [];
        this.AccessoryActions = [];
    }

    function AccessoryStateDto() {
        this.AccessoryCode = "";
        this.Selected = false;
        this.Enabled = true;
    }

    function AccessoryActionDto() {
        this.ActionType = 0;
        this.AccessoryCode = "";
    }

    var AccessoryActionTypeDto = {
        "Undefined": 0,
        "Add": 1,
        "Remove": 2
    };

    function StateDto() {
        this.Code = '';
        this.Name = '';
    }

    var TraversalStatusDto = {
        "Success": 0,
        "InvalidVinLength": 1,
        "InvalidVinCharacters": 2,
        "InvalidVinChecksum": 3,
        "NoDataForVin": 4,
        "toString": function(i) {
            var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function PeriodDto() {
        this.Id = 0;
        this.Year = '';
        this.Month = '';
    }

    /* publication dto */

    var ChangeAgentDto = {
        "Undefined": 0,
        "User": 1,
        "System": 2,
        "toString": function(i) {
            var names = ["Undefined", "User Change", "System Change"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    // TODO: Message from Simon to Paul
    // TODO: This is a list of the bit flags passed from the server.
    // TODO: END

    var ChangeTypeDto = {
        "None": 0,
        "Period": (1 << 0),
        "State": (1 << 1),
        "Uid": (1 << 2),
        "Mileage": (1 << 3),
        "AccessoryActions": (1 << 4),
        "AccessoryStates": (1 << 5),
        "toString": function(i) {
            var names = ["None", "Period", "State", "Uid", "Mileage", "Accessory Actions", "Accessory States"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    function UserDto() {
        this.FirstName = null;
        this.LastName = null;
        this.UserName = null;
    }

    function EditionDto() {
        this.BeginDate = null;
        this.EndDate = null;
        this.User = null;
    }

    function PublicationInfoDto() {
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
    }

    function PublicationDto() {
        // from info
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
        // own properties
        this.VehicleConfiguration = null;
        this.Table = null;
        this.Accessories = [];
    }

    /* envelopes dto */

    function StatesArgumentsDto() {
        EventBinder(this);
    }

    function StatesResultsDto() {
        this.Arguments = null;
        this.States = [];
    }

    function TraversalArgumentsDto() {
        this.Vin = '';
        EventBinder(this);
    }

    function TraversalResultsDto() {
        this.Arguments = null;
        this.Path = null;
        this.Status = 0;
    }

    function SuccessorArgumentsDto() {
        this.Node = "";
        this.Successor = "";
        EventBinder(this);
    }

    function SuccessorResultsDto() {
        this.Arguments = null;
        this.Path = null;
    }

    function InitialValuationArgumentsDto() {
        this.Vin = '';
        this.Uid = 0;
        this.State = '';
        this.Mileage = null;
        this.HasMileage = false;
        EventBinder(this);
    }

    function InitialValuationResultsDto() {
        this.Table = null;
        this.Arguments = null;
        this.Accessories = [];
        this.VehicleConfiguration = null;
    }

    function AdjustedValuationArgumentsDto() {
        this.State = '';
        this.Mileage = null;
        this.HasMileage = false;
        this.VehicleConfiguration = null;
        this.AccessoryAction = null;
        EventBinder(this);
    }

    function AdjustedValuationResultsDto() {
        this.Table = null;
        this.Arguments = null;
        this.VehicleConfiguration = null;
    }

    /* publication envelopes dto */

    function PublicationListArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        // -- (none) --
        // events please
        EventBinder(this);
    }

    function PublicationListResultsDto() {
        // self
        this.Arguments = null;
        this.Publications = [];
    }

    function PublicationOnDateArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.On = '';
        // events please
        EventBinder(this);
    }

    function PublicationOnDateResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    function PublicationLoadArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.Id = 0;
        // events please
        EventBinder(this);
    }

    function PublicationLoadResultsDto() {
        // self
        this.Arguments = '';
        this.Publication = '';
    }

    function PublicationSaveArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.VehicleConfiguration = null;
        // events please
        EventBinder(this);
    }

    function PublicationSaveResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    /* == END: Classes == */

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }
    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Nada).trigger("Nada.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "States": genericService("/VehicleValuationGuide/Services/Nada.asmx/States", StatesResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.StatesArgumentsDto"
                }
            });
        }),
        "Traversal": genericService("/VehicleValuationGuide/Services/Nada.asmx/Traversal", TraversalResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin)
                }
            });
        }),
        "Successor": genericService("/VehicleValuationGuide/Services/Nada.asmx/Successor", SuccessorResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
                    "Node": this.Node,
                    "Successor": this.Successor
                }
            });
        }),
        "InitialValuation": genericService("/VehicleValuationGuide/Services/Nada.asmx/InitialValuation", InitialValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto",
                    "Vin": (this.Vin == null ? '' : this.Vin),
                    "Uid": this.Uid,
                    "State": (this.State == null ? '' : this.State),
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage : 0),
                    "HasMileage": ((/^\d+$/.test(this.Mileage)) ? true : false)
                }
            });
        }),
        "AdjustedValuation": genericService("/VehicleValuationGuide/Services/Nada.asmx/AdjustedValuation", AdjustedValuationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto",
                    "State": (this.State == null ? '' : this.State),
                    "Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage : 0),
                    "HasMileage": ((/^\d+$/.test(this.Mileage)) ? true : false),
                    "VehicleConfiguration": this.VehicleConfiguration,
                    "AccessoryAction": this.AccessoryAction
                }
            });
        }),
        "PublicationList": genericService("/VehicleValuationGuide/Services/Nada.asmx/PublicationList", PublicationListResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications.PublicationListArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "PublicationOnDate": genericService("/VehicleValuationGuide/Services/Nada.asmx/PublicationOnDate", PublicationOnDateResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications.PublicationOnDateArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "On": this.On
                }
            });
        }),
        "PublicationLoad": genericService("/VehicleValuationGuide/Services/Nada.asmx/PublicationLoad", PublicationLoadResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications.PublicationLoadArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "Id": this.Id
                }
            });
        }),
        "PublicationSave": genericService("/VehicleValuationGuide/Services/Nada.asmx/PublicationSave", PublicationSaveResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        })
    };

    var Events = {
        "StatesArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Nada).trigger("Nada.StatesLoaded", [data.States, State.State]);
            }
        },
        "TraversalArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                var hasUid = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.Uid;
                if (hasUid) {
                    $(Nada).trigger("Nada.UidChange", data.Path.CurrentNode.Uid);
                }
                if (!hasUid || this.RunNodeTemplate) {
                    $(Nada).trigger("Nada.TraversalLoaded", [data.Path, data.Status]);
                }
            }
        },
        "SuccessorArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Nada).trigger("Nada.SuccessorLoaded", [data.Path, 0]);
            }
        },
        "InitialValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // save the state
                State.VehicleConfiguration = data.VehicleConfiguration;
                // build the page
                $(Nada).trigger("Nada.InitialValueLoaded", data);
            },
            stateChange: function(event) {
                if (State.Uid !== '') {
                    var valuation = new InitialValuationArgumentsDto();
                    valuation.Uid = State.Uid;
                    valuation.Vin = State.Vin;
                    valuation.Mileage = State.Mileage;
                    valuation.State = State.State;
                    $(valuation).trigger("fetch");
                }
            }
        },
        "AdjustedValuationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // save the state
                State.VehicleConfiguration = data.VehicleConfiguration;
                // build the page
                $(Nada).trigger("Nada.AdjustedValueLoaded", data);
            },
            stateChange: function(event, action) {
                if (State.Uid !== '') {
                    var args = new AdjustedValuationArgumentsDto();
                    args.State = State.State;
                    args.Mileage = State.Mileage;
                    args.VehicleConfiguration = State.VehicleConfiguration;
                    args.AccessoryAction = action || null;

                    $(args).trigger('fetch');
                }
            }
        },
        "PublicationListArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Nada).trigger("Nada.PublicationListLoaded", data);
            }
        },
        "PublicationOnDateArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                if (data.Publication.VehicleConfiguration) {
                    State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                    State.Uid = data.Publication.VehicleConfiguration.Uid;
		}
                $(Nada).trigger("Nada.PublicationLoaded", data);
            }
        },
        "PublicationLoadArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                State.Uid = data.Publication.VehicleConfiguration.Uid;
                $(Nada).trigger("Nada.PublicationLoaded", data);
            }
        },
        "PublicationSaveArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                $(Nada).trigger("Nada.PublicationSaved", data);
            }
        }
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        "StatesArgumentsDto": genericMapper(StatesArgumentsDto),
        "StatesResultsDto": genericMapper(StatesResultsDto, {
            "Arguments": "StatesArgumentsDto",
            "States": "StateDto"
        }),
        "TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
        "TraversalResultsDto": genericMapper(TraversalResultsDto, {
            "Arguments": "TraversalArgumentsDto",
            "Path": "PathDto"
        }),
        "SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
        "SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
            "Arguments": "SuccessorArgumentsDto",
            "Path": "PathDto"
        }),
        "NodeDto": genericMapper(NodeDto, {
            "Children": "NodeDto",
            "Parent": "NodeDto"
        }),
        "PathDto": genericMapper(PathDto, {
            "CurrentNode": "NodeDto",
            "Successors": "NodeDto"
        }),
        "InitialValuationArgumentsDto": genericMapper(InitialValuationArgumentsDto),
        "InitialValuationResultsDto": genericMapper(InitialValuationResultsDto, {
            "Arguments": "InitialValuationArgumentsDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Table": "PriceTableDto",
            "Accessories": "AccessoryDto"
        }),
        "AdjustedValuationArgumentsDto": genericMapper(AdjustedValuationArgumentsDto, {
            "AccessoryAction": "AccessoryActionDto",
            "VehicleConfiguration": "VehicleConfigurationDto"
        }),
        "AdjustedValuationResultsDto": genericMapper(AdjustedValuationResultsDto, {
            "Arguments": "AdjustedValuationArgumentsDto",
            "Table": "PriceTableDto",
            "VehicleConfiguration": "VehicleConfigurationDto"
        }),
        "PriceTableDto": genericMapper(PriceTableDto, {
            "Rows": "PriceTableRowDto"
        }),
        "PriceTableRowDto": function(json) {
            var result = new PriceTableRowDto();
            for (var i in json) {
                if (!json.hasOwnProperty(i)) continue;
                if (i == 'Prices') {
                    result.Prices = DataMapper.PricesDto(json[i]);
                } else if (i == 'RowType') {
                    switch (json[i]) {
                        case 1:
                            result.RowType = PriceTableRowTypeDto.Base;
                            break;
                        case 2:
                            result.RowType = PriceTableRowTypeDto.Option;
                            break;
                        case 3:
                            result.RowType = PriceTableRowTypeDto.Mileage;
                            break;
                        case 4:
                            result.RowType = PriceTableRowTypeDto.Final;
                            break;
                        default:
                            break;
                    }
                }
            }
            return result;
        },
        "PricesDto": genericMapper(PricesDto),
        "AccessoryDto": genericMapper(AccessoryDto, {
            "Prices": "PricesDto"
        }),
        "StateDto": genericMapper(StateDto),
        "PeriodDto": genericMapper(PeriodDto),
        "VehicleConfigurationDto": genericMapper(VehicleConfigurationDto, {
            "Period": "PeriodDto",
            "State": "StateDto",
            "AccessoryStates": "AccessoryStateDto",
            "AccessoryActions": "AccessoryActionDto"
        }),
        "AccessoryStateDto": genericMapper(AccessoryStateDto),
        "AccessoryActionDto": genericMapper(AccessoryActionDto),
        /* publication api */
        "PublicationListArgumentsDto": genericMapper(PublicationListArgumentsDto),
        "PublicationListResultsDto": genericMapper(PublicationListResultsDto, {
            "Arguments": "PublicationListArgumentsDto",
            "Publications": "PublicationInfoDto"
        }),
        "PublicationOnDateArgumentsDto": genericMapper(PublicationOnDateArgumentsDto),
        "PublicationOnDateResultsDto": genericMapper(PublicationOnDateResultsDto, {
            "Arguments": "PublicationOnDateArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationLoadArgumentsDto": genericMapper(PublicationLoadArgumentsDto),
        "PublicationLoadResultsDto": genericMapper(PublicationLoadResultsDto, {
            "Arguments": "PublicationLoadArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationSaveArgumentsDto": genericMapper(PublicationSaveArgumentsDto),
        "PublicationSaveResultsDto": genericMapper(PublicationSaveResultsDto, {
            "Arguments": "PublicationSaveArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "UserDto": genericMapper(UserDto),
        "EditionDto": genericMapper(EditionDto, {
            "User": "UserDto"
        }),
        "PublicationInfoDto": genericMapper(PublicationInfoDto, {
            "Edition": "EditionDto"
        }),
        "PublicationDto": genericMapper(PublicationDto, {
            "Edition": "EditionDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Table": "PriceTableDto",
            "Accessories": "AccessoryDto"
        })
    };

    var EventBinder = function(obj) {
        if (obj instanceof StatesArgumentsDto) {
            $(obj).bind("fetchComplete", Events.StatesArgumentsDto.fetchComplete);
        }
        if (obj instanceof TraversalArgumentsDto) {
            $(obj).bind("fetchComplete", Events.TraversalArgumentsDto.fetchComplete);
        }
        if (obj instanceof SuccessorArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SuccessorArgumentsDto.fetchComplete);
        }
        if (obj instanceof InitialValuationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.InitialValuationArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.InitialValuationArgumentsDto.stateChange);
        }
        if (obj instanceof AdjustedValuationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.AdjustedValuationArgumentsDto.fetchComplete);
            $(obj).bind("stateChange", Events.AdjustedValuationArgumentsDto.stateChange);
        }
        if (obj instanceof PublicationListArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationListArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationOnDateArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationOnDateArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationLoadArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationLoadArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationSaveArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationSaveArgumentsDto.fetchComplete);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(StatesArgumentsDto, DataMapper.StatesArgumentsDto);
        BindDataMapper(StatesResultsDto, DataMapper.StatesResultsDto);

        BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
        BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

        BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
        BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

        BindDataMapper(NodeDto, DataMapper.NodeDto);
        BindDataMapper(PathDto, DataMapper.PathDto);

        BindDataMapper(InitialValuationArgumentsDto, DataMapper.InitialValuationArgumentsDto);
        BindDataMapper(InitialValuationResultsDto, DataMapper.InitialValuationResultsDto);

        BindDataMapper(AdjustedValuationArgumentsDto, DataMapper.AdjustedValuationArgumentsDto);
        BindDataMapper(AdjustedValuationResultsDto, DataMapper.AdjustedValuationResultsDto);

        BindDataMapper(PriceTableDto, DataMapper.PriceTableDto);
        BindDataMapper(PriceTableRowDto, DataMapper.PriceTableRowDto);
        BindDataMapper(PricesDto, DataMapper.PricesDto);
        BindDataMapper(AccessoryDto, DataMapper.AccessoryDto);
        BindDataMapper(VehicleConfigurationDto, DataMapper.VehicleConfigurationDto);
        BindDataMapper(AccessoryStateDto, DataMapper.AccessoryStateDto);
        BindDataMapper(AccessoryActionDto, DataMapper.AccessoryActionDto);
        BindDataMapper(StateDto, DataMapper.StateDto);
        BindDataMapper(PeriodDto, DataMapper.PeriodDto);

        BindDataMapper(PublicationListArgumentsDto, DataMapper.PublicationListArgumentsDto);
        BindDataMapper(PublicationListResultsDto, DataMapper.PublicationListResultsDto);

        BindDataMapper(PublicationOnDateArgumentsDto, DataMapper.PublicationOnDateArgumentsDto);
        BindDataMapper(PublicationOnDateResultsDto, DataMapper.PublicationOnDateResultsDto);

        BindDataMapper(PublicationLoadArgumentsDto, DataMapper.PublicationLoadArgumentsDto);
        BindDataMapper(PublicationLoadResultsDto, DataMapper.PublicationLoadResultsDto);

        BindDataMapper(PublicationSaveArgumentsDto, DataMapper.PublicationSaveArgumentsDto);
        BindDataMapper(PublicationSaveResultsDto, DataMapper.PublicationSaveResultsDto);

        BindDataMapper(UserDto, DataMapper.UserDto);
        BindDataMapper(EditionDto, DataMapper.EditionDto);
        BindDataMapper(PublicationDto, DataMapper.PublicationDto);
        BindDataMapper(PublicationInfoDto, DataMapper.PublicationInfoDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                if (!source.hasOwnProperty(m)) continue;
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(StatesArgumentsDto, Services.States);

        BindFetch(TraversalArgumentsDto, Services.Traversal);
        BindFetch(SuccessorArgumentsDto, Services.Successor);

        BindFetch(InitialValuationArgumentsDto, Services.InitialValuation);
        BindFetch(AdjustedValuationArgumentsDto, Services.AdjustedValuation);

        BindFetch(PublicationListArgumentsDto, Services.PublicationList);
        BindFetch(PublicationOnDateArgumentsDto, Services.PublicationOnDate);
        BindFetch(PublicationLoadArgumentsDto, Services.PublicationLoad);
        BindFetch(PublicationSaveArgumentsDto, Services.PublicationSave);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selection) {
        this.State = State; /// TODO: refactor so this can is not a single global, PM

        if (typeof selection == "undefined") {
            selection = {};
        }

        if (selection.buid) {
            $(Nada).trigger("Nada.BrokerChange", selection.buid);
        }

        if (selection.state) {
            State.State = selection.state;
        }
        if (selection.mileage) {
            State.Mileage = selection.mileage;
        }

        $(new StatesArgumentsDto()).trigger("fetch");
        $(Nada).trigger("Nada.Main", selection);
        if (selection.vuid) {
            $(Nada).trigger("Nada.VehicleChange", selection.vuid);
        } else {
            $(Nada).trigger("Nada.Start", selection);
        }

        State.Vehicle = selection.vuid || State.Vehicle;
        State.Vin = selection.vin || State.Vin;
        State.Uid = selection.uid || State.Uid;
    };

    this.raises = this.raises || [];
    this.raises.push("Nada.StatesLoaded");
    this.raises.push("Nada.TraversalLoaded");
    this.raises.push("Nada.SuccessorLoaded");
    this.raises.push("Nada.InitialValueLoaded");
    this.raises.push("Nada.AdjustedValueLoaded");
    this.raises.push("Nada.PublicationListLoaded");
    this.raises.push("Nada.PublicationLoaded");
    this.raises.push("Nada.PublicationSaved");
    this.raises.push("Nada.ErrorState");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Nada.StateChange");
    this.listensTo.push("Nada.MileageChange");
    this.listensTo.push("Nada.VinChange");
    this.listensTo.push("Nada.UidChange");
    this.listensTo.push("Nada.SelectionChange");
    this.listensTo.push("Nada.AddOption");
    this.listensTo.push("Nada.BrokerChange");
    this.listensTo.push("Nada.VehicleChange");
    this.listensTo.push("Nada.LoadPublicationList");
    this.listensTo.push("Nada.LoadPublicationOnDate");
    this.listensTo.push("Nada.LoadPublication");
    this.listensTo.push("Nada.SavePublication");

    var PublicEvents = {
        "Nada.StateChange": function(evt, newState) {
            if (State.State !== newState) {
                State.State = newState;
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Nada.MileageChange": function(evt, newMileage) {
            if (State.Mileage !== newMileage) {
                State.Mileage = newMileage;
                $(Nada).trigger("Nada.MileageChange", newMileage);
                $(new AdjustedValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Nada.SelectionChange": function(evt, next, successors) {
            var hasUid = successors && successors[0] && successors[0].Uid;
            if (hasUid) {
                $(Nada).trigger("Nada.UidChange", selection.next);
            } else {
                var successor = new SuccessorArgumentsDto();
                successor.Successor = next;
                $(successor).trigger('fetch');
            }
        },
        "Nada.AddOption": function(evt, optionName) {
            var action = new AccessoryActionDto();
            action.AccessoryCode = optionName;
            action.ActionType = AccessoryActionTypeDto.Add;

            $(new AdjustedValuationArgumentsDto()).trigger("stateChange", [action]);
        },
        "Nada.RemoveOption": function(evt, optionName) {
            var action = new AccessoryActionDto();
            action.AccessoryCode = optionName;
            action.ActionType = AccessoryActionTypeDto.Remove;

            $(new AdjustedValuationArgumentsDto()).trigger("stateChange", [action]);
        },
        "Nada.VinChange": function(evt, newVin, runNodeTemplate) {
            if (State.Vin !== newVin) {
                State.Vin = newVin;

                if (newVin !== '') {
                    var traversal = new TraversalArgumentsDto();
                    traversal.Vin = State.Vin;
                    traversal.RunNodeTemplate = runNodeTemplate;
                    $(traversal).trigger("fetch");

                    $(new InitialValuationArgumentsDto()).trigger("stateChange");
                }
            }
        },
        "Nada.UidChange": function(evt, newUid) {
            if (State.Uid !== newUid) {
                State.Uid = newUid;

                $(new InitialValuationArgumentsDto()).trigger("stateChange");
            }
        },
        "Nada.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
            State.Vehicle = '';
        },
        "Nada.VehicleChange": function(evt, newVehicle) {
            State.Vehicle = newVehicle;
        },
        "Nada.LoadPublicationList": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publicationList = new PublicationListArgumentsDto();
                publicationList.Broker = State.Broker;
                publicationList.Vehicle = State.Vehicle;
                $(publicationList).trigger("fetch");
            }
        },
        "Nada.LoadPublicationOnDate": function(evt, theDate) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationOnDateArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.On = theDate;
                $(publication).trigger("fetch");
            }
        },
        "Nada.LoadPublication": function(evt, theId) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationLoadArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.Id = theId;
                $(publication).trigger("fetch");
            }
        },
        "Nada.SavePublication": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '' && State.VehicleConfiguration !== null) {
                var publication = new PublicationSaveArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.VehicleConfiguration = State.VehicleConfiguration;
                $(publication).trigger("fetch");
            }
        },
        "Nada.Start": function(evt, values) {
            values.vin = values.vin || State.Vin;
            values.uid = values.uid || State.Uid;

            if (values.vin) {
                State.Vin = "";
                $(Nada).trigger("Nada.VinChange", values.vin);
            } else if (values.uid) {
                State.Uid = "";
                $(Nada).trigger("Nada.UidChange", values.uid);
            } else {
                $(new TraversalArgumentsDto()).trigger("fetch");
            }
        }
    };
    $(Nada).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.FormatPriceTableTypeRow = PriceTableRowTypeDto.toString;
    this.FormatTraversalStatus = TraversalStatusDto.toString;

}).apply(Nada);

