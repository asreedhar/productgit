if (typeof Nada !== "function") {
	var Nada = function() {};
}

(function() {
    this.raises = this.raises || [];
    this.raises.push("Nada.StateChange");
    this.raises.push("Nada.MileageChange");
    this.raises.push("Nada.VinChange");
    this.raises.push("Nada.UidChange");
    this.raises.push("Nada.SelectionChange");
    this.raises.push("Nada.AddOption");
    this.raises.push("Nada.RemoveOption");
    this.raises.push("Nada.BrokerChange");
    this.raises.push("Nada.VehicleChange");
    this.raises.push("Nada.LoadPublicationList");
    this.raises.push("Nada.LoadPublicationOnDate");
    this.raises.push("Nada.LoadPublication");
    this.raises.push("Nada.SavePublication");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Nada.StatesLoaded");
    this.listensTo.push("Nada.TraversalLoaded");
    this.listensTo.push("Nada.SuccessorLoaded");
    this.listensTo.push("Nada.InitialValueLoaded");
    this.listensTo.push("Nada.AdjustedValueLoaded");
    this.listensTo.push("Nada.PublicationListLoaded");
    this.listensTo.push("Nada.PublicationLoaded");
    this.listensTo.push("Nada.PublicationSaved");

    var PublicEvents = {
        "Nada.StatesLoaded": function(evt, states, currentState) {
            var template = new AdjustmentTemplateBuilder(states, currentState);
            template.clean();
            template.init();
        },
        "Nada.TraversalLoaded": function(evt, data, status) {
            var template = new NodeTemplateBuilder(data, status);
            template.clean();
            template.init();
        },
        "Nada.SuccessorLoaded": function(evt, data, status) {
            var template = new NodeTemplateBuilder(data, status);
            template.clean();
            template.init();
        },
        "Nada.InitialValueLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data);
            template.clean();
            template.init();
        },
        "Nada.AdjustedValueLoaded": function(evt, data) {
            var template = new AdjustedTemplateBuilder(data);
            template.clean();
            template.init();
        },
        "Nada.PublicationListLoaded": function(evt, data) {
            var template = new PublicationListTemplateBuilder(data.Publications); ;
            template.clean();
            template.init();
        },
        "Nada.PublicationLoaded": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        },
        "Nada.PublicationSaved": function(evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        }
    };
    $(Nada).bind(PublicEvents);

    var AdjustmentTemplateBuilder = function(data, state) {
        this.data = data;
        this.defaultState = state;
        this.$dom = {
            State: $('#state'),
            Mileage: $('#mileage'),
            // publication api
            Form: $('#subject'),
            Save: $('#save'),
            Broker: $('#broker'),
            Vehicle: $('#vehicle'),
            PublicationDate: $('#publication_date')
        };
    };

    AdjustmentTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            // states
            var list = this.data;
            var select = this.$dom.State;
            select.html('');
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var state = $('<option></option>').val(item.Code).html(item.Name);
                if (item.Code == this.defaultState) {
                    state.attr('selected', 'selected');
                }
                select.append(state);
            }
            select.unbind();
            select.bind('change', $.proxy(this.stateChange, this));
            // mileage
            var input = this.$dom.Mileage;
            input.unbind();
            input.bind('change', $.proxy(this.mileageChange, this));
            // broker / vehicle
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVehicleOrDate, this));
        },
        clean: function() {
            this.$dom.State.unbind('change');
            this.$dom.Mileage.unbind('change');
            this.$dom.Form.unbind('click');
        },
        stateChange: function(evt) {
            $(Nada).trigger("Nada.StateChange", this.$dom.State.val());
        },
        mileageChange: function(evt) {
            $(Nada).trigger("Nada.MileageChange", this.$dom.Mileage.val());
        },
        changeBrokerOrVehicleOrDate: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                e = document.getElementById(n.substring(1))
                v = (e == null ? null : e.value),
                c = null;
                if (n == '#broker') {
                    $(Nada).trigger("Nada.BrokerChange", v);
                    this.$dom.Vehicle.val('');
                }
                else
                    if (n == '#vehicle') {
                    $(Nada).trigger("Nada.VehicleChange", v);
                    $(Nada).trigger("Nada.LoadPublicationList");
                }
                else
                    if (n == '#publication_date') {
                    $(Nada).trigger("Nada.LoadPublicationOnDate", v);
                }
                else
                    if (n == '#save') {
                    $(Nada).trigger("Nada.SavePublication");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var NodeTemplateBuilder = function(data, status) {
        this.data = data;
        this.status = status;
    };

    NodeTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var bindDropDown = function(s, v, p, items, selected) {
                // add prompt
                var prompt = $('<option></option>').val('').html('... Please Select ...');
                s.html('');
                s.append(prompt);
                // add items
                for (i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    var val = item.State;
                    if (item.HasUid == true && item.Uid != 0) {
                        val = item.Uid;
                    }
                    var opt = $('<option></option>').val(val).html(item.Name);
                    if (item.Id == selected) {
                        opt.attr('selected', 'selected');
                    }
                    s.append(opt);
                }
                // attach change event
                if (v == false) {
                    function fetchSuccessor(evt) {
                        var next = s.val();
                        if (next != '') {
                            // empty follow up drop downs
                            var inputs = ['year', 'make', 'series', 'body_style', 'synthetic'].slice(p);
                            for (var i = 0; i < inputs.length; i++) {
                                document.getElementById(inputs[i]).options.length = 0
                            }
                            // reset vin
                            if (inputs.length > 0) {
                                $(Nada).trigger("Nada.VinChange", '');
                                $('#vin').val('');
                            }

                            $(Nada).trigger("Nada.SelectionChange", next);
                        }
                    }
                    s.unbind();
                    s.bind('change', fetchSuccessor);
                }
                else {
                    function fetchInitialValuation(evt) {
                        $(Nada).trigger("Nada.UidChange", s.val());
                    }
                    s.unbind();
                    s.bind('change', fetchInitialValuation);
                }
            };
            var bindQueryByVin = function() {
                var decode = $('#NadaForm');
                var query = function(evt) {
                    evt.preventDefault();
                    // remember vin
                    $(Nada).trigger("Nada.VinChange", [$("#vin").val(), true]);
                }
                decode.unbind('submit', query);
                decode.bind('submit', query);
            };
            var bindNode = function(node, successors, selected) {
                var valuation = successors.length > 0 && !!successors[0].HasUid;
                var positions = {
                    'Year': 2,
                    'Make': 3,
                    'Series': 4,
                    'Body': 5
                };
                var position = positions[node.Label] || 1;
                switch (node.Label) {
                    case '':
                        bindQueryByVin();
                        bindDropDown($("#year"), valuation, position, successors, selected);
                        break;
                    case 'Year':
                        bindDropDown($("#make"), valuation, position, successors, selected);
                        break;
                    case 'Make':
                        bindDropDown($("#series"), valuation, position, successors, selected);
                        break;
                    case 'Series':
                        bindDropDown($("#body_style"), valuation, position, successors, selected);
                        break;
                    case 'Vin':
                        if (node.HasUid == true && node.Uid != 0) {
                            $(Nada).trigger("Nada.UidChange", node.Uid);
                            selected = successors[0].Id;
                        }
                        position = positions[node.Parent.Label];
                        if (node.Parent.Label == 'Series') {
                            bindDropDown($("#body_style"), valuation, position, successors, selected);
                        }
                        else {
                            var x = [];
                            for (var j = 0; j < successors.length; j++) {
                                var k = successors[j];
                                var t = [];
                                var s = k;
                                while (s != null) {
                                    t.push(s.Name);
                                    s = s.Parent;
                                }
                                var n = t.reverse().join(' ');
                                x.push({
                                    'Uid': k.Uid,
                                    'HasUid': k.HasUid,
                                    'Id': k.Id,
                                    'Name': n,
                                    'State': k.State,
                                    'Label': 'Synthetic'
                                });
                            }
                            bindDropDown($("#synthetic"), valuation, position, x);
                        }
                        break;
                }
            };
            var bindNodeRecursive = function(node, successors, selected) {
                if (node.Parent != null) {
                    bindNodeRecursive(node.Parent, node.Parent.Children, node.Id);
                }
                bindNode(node, successors, selected);
            };
            if (this.status == 0) {
                // structure: '' > 'year' > 'make' > 'series' [> 'vin'] > 'body'
                if (this.data.CurrentNode.Label == 'Vin') {
                    bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    bindNode(this.data.CurrentNode, this.data.Successors);
                }
                $("#traversal_status").html('');
            }
            else {
                $("#traversal_status").html(Nada.FormatTraversalStatus(this.status));
            }
        },
        clean: function() {
            $("fieldset div").empty();
        }
    };

    var InitialTemplateBuilder = function(data) {
        this.data = data;
    };

    InitialTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._accessories();
            this._table();
        },
        clean: function() {
            $("#accessories div").empty();
            $("#accessories").attr('rel', 'state:clean');
            $("#prices div").empty();
            $("#prices").attr('rel', 'state:clean');
        },
        _accessories: function() {
            var info = $("#accessories");
            info.find("div").empty(); // clean up from last time
            var hd = $("<div></div>");
            $(info).append(hd);
            var accessoryList = this.data.Accessories;
            var th = '<thead><tr><th></th><th>Accessory</th><th>Trade In</th><th>Loan</th><th>Retail</th></tr></thead>';
            var tb = '<tbody>';
            for (var i = 0; i < accessoryList.length; i++) {
                var accessory = accessoryList[i];
                var state = jQuery.grep(
				this.data.VehicleConfiguration.AccessoryStates, function(v) {
				    return v.AccessoryCode == accessory.Code;
				});
                var attr = '';
                if (state.length == 1 && state[0].Selected) {
                    attr += ' checked="checked"';
                }
                if (state.length == 1 && !state[0].Enabled) {
                    attr += ' disabled="disabled"';
                }
                var tr = '<tr><td><input type="checkbox" name="' + accessory.Code + '" id="' + accessory.Code + '" ' + attr + ' /></td>' + '<td>' + accessory.Description + '</td>' + '<td>' + accessory.Prices.TradeInClean + '</td>' + '<td>' + accessory.Prices.Loan + '</td>' + '<td>' + accessory.Prices.Retail + '</td></tr>';
                tb += tr;
            }
            tb += '</tr></tbody>';
            $(hd).html('<table>' + th + tb + '</table>');

            var optionSelected = function(evt) {
                if (evt.target) {
                    $("#accessories").attr('rel', 'state:changing');
                    var nm = evt.target.name,
					ck = evt.target.checked;

                    $(Nada).trigger(ck ? "Nada.AddOption" : "Nada.RemoveOption", nm);
                }
            };

            $(hd).bind('change', optionSelected);

            $("#accessories").attr('rel', 'state:ready');
        },
        _table: function() {
            var info = $("#prices");
            // clean up from last time ...
            info.find("div").empty();
            // one price table ...
            var hd = $("<div></div>");
            var th = '<thead><tr><th></th><th>Rough<br/>Trade In </th><th>Average<br/>Trade In</th><th>Clean<br/>Trade In</th><th>Loan</th><th>Clean<br/>Retail</th></tr></thead>';
            var tb = '<tbody>';
            var tableRows = this.data.Table.Rows;
            for (var i = 0; i < tableRows.length; i++) {
                var tableRow = tableRows[i];
                var tr = '<tr>' + '<td>' + Nada.FormatPriceTableTypeRow(tableRow.RowType) + '</td>' + '<td>' + tableRow.Prices.TradeInRough + '</td>' + '<td>' + tableRow.Prices.TradeInAverage + '</td>' + '<td>' + tableRow.Prices.TradeInClean + '</td>' + '<td>' + tableRow.Prices.Loan + '</td>' + '<td>' + tableRow.Prices.Retail + '</td>' + '</tr>';
                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + th + tb + '</table>');
            $(info).append(hd);
            $("#prices").attr('rel', 'state:ready');
        }
    };

    var AdjustedTemplateBuilder = function(data) {
        this.data = data;
    };

    AdjustedTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();
            this._accessories();
        },
        clean: function() {
            $("#prices div").empty();
            $("#prices").attr('rel', 'state:clean');
        },
        _accessories: function() {
            var states = this.data.VehicleConfiguration.AccessoryStates;
            for (var i = 0; i < states.length; i++) {
                var state = states[i],
				el = document.getElementById(state.AccessoryCode);
                $(el).attr('checked', state.Selected);
            }
        }
    };

    AdjustedTemplateBuilder.prototype._table = InitialTemplateBuilder.prototype._table;

    var PublicationListTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#publications'),
            Body: $('#publications_body')
        };
    };

    PublicationListTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();
        },
        clean: function() {
            this.$dom.Body.empty();
            this.$dom.Form.unbind('click');
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                + '<td>' + item.Edition.BeginDate + '</td>'
                + '<td>' + item.Edition.EndDate + '</td>'
                + '<td>' + item.ChangeType + '</td>'
                + '<td>' + item.ChangeAgent + '</td>'
                + '<td>' + item.Edition.User.UserName + '</td>'
                + '<td><a href="#' + item.Id + '">Load</a></td>'
                + '</tr>';
                code += tr;
            }
            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.selectPublication, this));
        },
        selectPublication: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    p = tgt.href.substring(i+1);
                $(Nada).trigger("Nada.LoadPublication", p);
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

}).apply(Nada);

