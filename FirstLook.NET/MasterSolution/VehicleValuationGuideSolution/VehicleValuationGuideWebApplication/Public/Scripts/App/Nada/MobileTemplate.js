if (typeof Nada !== "function") {
    var Nada = function() {};
} (function() {
    this.raises = this.raises || [];
    this.raises.push("Nada.StateChange");
    this.raises.push("Nada.MileageChange");
    this.raises.push("Nada.VinChange");
    this.raises.push("Nada.UidChange");
    this.raises.push("Nada.SelectionChange");
    this.raises.push("Nada.AddOption");
    this.raises.push("Nada.RemoveOption");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Nada.StatesLoaded");
    this.listensTo.push("Nada.TraversalLoaded");
    this.listensTo.push("Nada.SuccessorLoaded");
    this.listensTo.push("Nada.InitialValueLoaded");
    this.listensTo.push("Nada.AdjustedValueLoaded");

    var State = {
        States: [],
        CurrentState: ""
    };

    var PublicEvents = {
        "Nada.StatesLoaded": function(evt, states, currentState) {
            State.States = _.reduce(states, function(memo, state) {
                memo.push({
                    name: state.Name,
                    value: state.Code
                });
                return memo;
            },
            []);
            State.CurrentState = currentState;
        },
        "Nada.TraversalLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Nada");

            _.invoke(card.views, "clean", true);
            // card.views.select.clean(true);
            card.views.select.build(data);
        },
        "Nada.SuccessorLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Nada");

            if (data.CurrentNode && data.CurrentNode.HasVehicleId) {
                $(Nada).trigger("Nada.UidChange", data.CurrentNode.VehicleId);
            } else {
                _.invoke(card.views, "clean", true);
                // card.views.select.clean(true);
                card.views.select.build(data);
            }
        },
        "Nada.InitialValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Nada");

            data.States = State.States;

            _.invoke(card.views, "clean", true);
            // card.views.values.clean();
            card.views.values.build(data);
        },
        "Nada.AdjustedValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Nada");

            $(card.views.values.EquipmentList).trigger("selection_set", [data.VehicleConfiguration.AccessoryStates]);

            $(card.views.values.Values).trigger("rebuild", [data]);
        },
        /// Component Bridge Events
        "Nada.SelectionPathChange": function(evt, data) {
            if (data.Uid !== "0") {
                $(Nada).trigger("Nada.UidChange", data.Uid);
            } else {
                $(Nada).trigger("Nada.SelectionChange", data.State);
            }
        },
        "Nada.SelectionChangeWithDecode": function(evt, data) {
            var newSelection = JSON.parse(data.NewNadaSelection);
            $(Nada).trigger("Nada.SelectionPathChange", newSelection);
        },
        "Nada.EquipmentItemSelected": function(evt, name, isSelected) {
            $(Nada).trigger(isSelected ? "Nada.AddOption": "Nada.RemoveOption", [name]);
        },
        "Nada.MileageOrStateChange": function(evt, data) {
            $(Nada).trigger("Nada.MileageChange", data.Mileage);
            $(Nada).trigger("Nada.StateChange", data.State);
        },
        "Nada.MileageChange": function(evt, data) {
            var card = FirstLook.cards.get("Nada");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("input[name=Mileage]").val(data);
            }
        },
        "Nada.StateChange": function(evt, data) {
            var card = FirstLook.cards.get("Nada");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("select[name=State]").val(data);
            }
        },
        "Nada.PublicationLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Nada");

            if (data.Publication.VehicleConfiguration) {
                data.Publication.States = State.States;

                _.invoke(card.views, "clean", true);
                // card.views.values.clean();
                card.views.values.build(data.Publication);
            } else {
                $(Nada).trigger("Nada.Start", {});
            }
        },
        "Nada.VehicleChange": function(evt, data) {
            $(Nada).trigger("Nada.LoadPublicationOnDate", '2038-01-01');
        },
        "Nada.ErrorState": function(evt, data) {
            FirstLook.cards.get("Nada").build(data);
        }
    };
    $(Nada).bind(PublicEvents);

    function summary_fitler(data) {
        var table = [],
        data_table = data.Table,
        row_finder = function(type) {
            return function(i) {
                return i.RowType == type;
            };
        },
        f = _.detect(data_table.Rows, row_finder(4));

        data.caption = "NADA Values";

        table.head = [["Clean", "Average", "Rough"]];

        table.push(_.map([f.Prices.TradeInClean, f.Prices.TradeInAverage, f.Prices.TradeInRough], _.money));
        table.col_class_name = ["clean", "average", "rough"];
        data.table = table;

        return data;
    }

    function table_filter(data) {
        var table = [],
        data_table = data.Table,
        row_finder = function(type) {
            return function(i) {
                return i.RowType == type;
            };
        },
        row_builder = function(type, title) {
            return [title || type, _.moneyAdjusted(o.Prices[type]) + "<br />" + _.moneyAdjusted(m.Prices[type]), _.money(f.Prices[type])];
        },
        o = _.detect(data_table.Rows, row_finder(2)),
        m = _.detect(data_table.Rows, row_finder(3)),
        f = _.detect(data_table.Rows, row_finder(4));

        data.caption = "NADA Values";

        table.head = [["", "Options<br />Mileage", "Final"]];

        table.push(row_builder("TradeInClean", "Trade In Clean"));
        table.push(row_builder("TradeInAverage", "Trade In Average"));
        table.push(row_builder("TradeInRough", "Trade In Rough"));
        table.push(row_builder("Loan"));
        table.push(row_builder("Retail"));

        table.header_col = 0;
        table.col_class_name = ["market", "adjustments", "final"];
        table.row_class_name = ["clean","average","rough","loan","retail"];

        data.table = table;

        return data;
    }

    function make_table() {
        return {
            type: "Table",
            pattern: {
                Table: {
                    Rows: [{
                        Prices: {
                            TradeInClean: _.isNumber,
                            TradeInAverage: _.isNumber,
                            TradeInRough: _.isNumber,
                            Loan: _.isNumber,
                            Retail: _.isNumber
                        }
                    }]
                }
            },
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: table_filter,
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            row_class_name_accessor: "table.row_class_name",
            body_header_col_accessor: "table.header_col"
        };
    }

    function make_summary() {
        return {
            type: "Table",
            pattern: {
                Table: {
                    Rows: [{
                        Prices: {
                            TradeInClean: _.isNumber,
                            TradeInAverage: _.isNumber,
                            TradeInRough: _.isNumber,
                            Loan: _.isNumber,
                            Retail: _.isNumber
                        }
                    }]
                }
            },
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: summary_fitler,
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            body_header_col_accessor: "table.header_col"
        };
    }

    function make_select() {
        return {
            type: "Fieldset",
            pattern: {
                Successors: [{
                    State: _.isString,
                    Label: _.isString,
                    Name: _.isString,
                    Uid: _
                }]
            },
            behaviors: {
                submitable: {
                    event: "Nada.SelectionChangeWithDecode",
                    scope: Nada,
                    submit_on_change: true
                }
            },
            legend_accessor: "fieldset.legend",
            input_accessor: "fieldset.inputs",
            name_accessor: "name",
            button_accessor: "fieldset.buttons",
            label_accessor: "label",
            value_accessor: "value",
            type_accessor: "type",
            filter: function(data) {
                var options = _.map(data.Successors, function(succ) {
                    return {
                        name: succ.Name,
                        value: JSON.stringify({State: succ.State, Uid: succ.Uid})
                    }
                }),
                fieldset = {
                    legend: "NADA " + data.Successors[0].Label,
                    inputs: [{
                        name: "NewNadaSelection",
                        label: data.Successors[0].Label,
                        options: options
                    }],
                    buttons: []
                }
                options.unshift({name: "Select...", value: ""});
                data.fieldset = fieldset;
                return data;
            }
        }
    }

    FirstLook.components.add(make_table(), "Nada.Table");
    FirstLook.components.add(make_summary(), "Nada.Summary");
    FirstLook.components.add(make_select(), "Nada.Select");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='error'>Problem with the VIN</p>";
		}
	},
	"Nada.VinProblem");

	FirstLook.components.add({
		type: "Static",
		pattern: _,
		template: function(data) {
			return "<p class='message'>No NADA Data For VIN</p>";
		}
	},
	"Nada.NoData");

    var state_pattern = {
        select: {
            Successors: function(v) {
                return _.isArray(v) && v.length > 0;
            },
            Status: function(v) {
				return v === 0 || _.isUndefined(v);
            }
        },
        values: {
            VehicleConfiguration: _
        },
		vin_problem: {
			Status: function(v) {
				return v >= 1 && v <= 3;
			}
		},
		data_problem: {
			Status: function(v) {
				return v === 4;
			}
		},
        error: {
            errorType: _,
            errorResponse: _
        }
    };

    var overview_card = {
        pattern: _,
        view: {
            select: {
                pattern: state_pattern.select,
                select: FirstLook.components.get("Nada.Select")
            },
            values: {
                pattern: state_pattern.values,
                values: FirstLook.components.get("Nada.Summary")
            },
            vin_problem: {
                pattern: state_pattern.vin_problem,
                message: FirstLook.components.get("Nada.VinProblem")
            },
            data_problem: {
                pattern: state_pattern.data_problem,
                message: FirstLook.components.get("Nada.NoData")
            },
            error: {
                pattern: state_pattern.error,
                message: FirstLook.components.get("Application.ServerError")
            }
        }
    };
    FirstLook.cards.add(overview_card, "NadaSummary");

    var cards = {
        title: "Nada",
        pattern: _,
        view: {
            select: {
                order: 0,
                pattern: state_pattern.select,
                Title: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<h1 class='logo_nada'>NADA</h1>";
                    }
                },
                Review: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<p>"+_.pluck(data._selection, "Name").join(" | ")+"</p>";
                    },
                    filter: function(data) {
                        var selection = [];
                        function append_parent(v) {
                            selection.push(v);
                            if (v.Parent) {
                                append_parent(v.Parent);
                            }
                            return _.first(selection, selection.length-1);
                        }
                        
                        data._selection = append_parent(data.CurrentNode);
                        return data;
                    }
                },
                TraversalList: {
                    type: "List",
                    class_name: "select_list vid_select",
                    pattern: {
                        Successors: [{
                            Name: _.isString,
                            Id: _.isNumber
                        }]
                    },
                    behaviors: {
                        selectable: {
                            pattern: {
                                Successors: [{
                                    State: _.isString,
                                    Uid: _
                                }]
                            },
                            data_accessor: ["State", "Uid"],
                            binds: {
                                "item_selected": {
                                    type: "Nada.SelectionPathChange",
                                    scope: Nada
                                }
                            }
                        }
                    },
                    item_accessor: "Successors",
                    text_accessor: "Name",
                    data_accessor: "State"
                }
            },
            values: {
                order: 1,
                pattern: state_pattern.values,
                ValueFieldSet: {
                    type: "Fieldset",
                    pattern: {
                        VehicleConfiguration: {
                            Mileage: _.isNumber,
                            State: {
                                Code: function(data) {
                                    return data.length == 2 && _.isString(data);
                                }
                            }
                        }
                    },
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "Nada.MileageOrStateChange",
                            scope: Nada,
                            submit_on_change: true
                        }
                    },
                    class_name: "book_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "Nada",
                        inputs: [{
                            name: "State",
                            label: "States",
                            data: "VehicleConfiguration.State.Code",
                            options: "States"
                        },
                        {
                            name: "Mileage",
                            label: "Mileage",
                            data: "VehicleConfiguration.Mileage",
                            type: "tel"
                        }],
                        buttons: ["Update"]
                    }
                },
                Values: FirstLook.components.get("Nada.Table"),
                EquipmentList: {
                    type: "List",
                    behaviors: {
                        multi_selectable: {
                            pattern: {
                                VehicleConfiguration: {
                                    AccessoryStates: [{
                                        AccessoryCode: _.isString,
                                        Selected: _.isBoolean
                                    }]
                                }
                            },
                            multi_item_accessor: "VehicleConfiguration.AccessoryStates",
                            multi_data_accessor: "AccessoryCode",
                            map_data_accessor: "Code",
                            selected_accessor: "Selected",
                            binds: {
                                "item_selected": {
                                    scope: Nada,
                                    type: "Nada.EquipmentItemSelected"
                                }
                            }
                        }
                    },
                    pattern: {
                        Accessories: [{
                            Description: _,
                            Code: _
                        }]
                    },
                    class_name: "equipment_list",
                    item_accessor: "Accessories",
                    text_accessor: "Description",
                    data_accessor: "Code"
                }
            },
            vin_probelm: {
                pattern: state_pattern.vin_probelm,
                Message: FirstLook.components.get("Nada.VinProblem")
            },
            data_problem: {
                pattern: state_pattern.data_problem,
                Message: FirstLook.components.get("Nada.NoData")
            },
            error: {
                pattern: state_pattern.error,
                ErrorMessage: FirstLook.components.get("Application.ServerError")
            }
        }
    };

    FirstLook.cards.add(cards, "Nada");
}).apply(Nada);

