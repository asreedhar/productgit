if (typeof KelleyBlueBook !== "function") {
    var KelleyBlueBook = function() {};
} 
(function() {
    this.raises = this.raises || [];
    this.raises.push("KelleyBlueBook.RegionChange");
    this.raises.push("KelleyBlueBook.ZipCodeChange");
    this.raises.push("KelleyBlueBook.MileageChange");
    this.raises.push("KelleyBlueBook.VinChange");
    this.raises.push("KelleyBlueBook.VehicleIdChange");
    this.raises.push("KelleyBlueBook.SelectionChange");
    this.raises.push("KelleyBlueBook.AddOption");
    this.raises.push("KelleyBlueBook.RemoveOption");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("KelleyBlueBook.RegionsLoaded");
    this.listensTo.push("KelleyBlueBook.TraversalLoaded");
    this.listensTo.push("KelleyBlueBook.SuccessorLoaded");
    this.listensTo.push("KelleyBlueBook.InitialValueLoaded");
    this.listensTo.push("KelleyBlueBook.AdjustedValueLoaded");
    this.listensTo.push("KelleyBlueBook.StateChange");

    var State = {
        Regions: [],
        CurrentRegion: ""
    };

    var PublicEvents = {
        "KelleyBlueBook.RegionsLoaded": function(evt, regions, currentRegion) {
            State.Regions = _.reduce(regions, function(memo, region) {
                memo.push({
                    name: region.Name.replace("Region ", ""),
                    value: region.Id
                });
                return memo;
            },
            []);
            State.CurrentRegion = currentRegion;
        },
        "KelleyBlueBook.TraversalLoaded": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");

            card.build(data);
        },
        "KelleyBlueBook.SuccessorLoaded": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");

            if (data.Path.CurrentNode && data.Path.CurrentNode.HasVehicleId) {
                $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", data.Path.CurrentNode.VehicleId);
            } else {
                card.build(data);
            }
        },
        "KelleyBlueBook.InitialValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");

            data.Regions = State.Regions;

            card.build(data);
        },
        "KelleyBlueBook.AdjustedValueLoaded": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");

            $(card.views.values.EquipmentList).trigger("selection_set", [data.VehicleConfiguration.OptionStates]);

            $(card.views.values.Carousel).trigger("rebuild", [data]);
        },
        /// Component Bridge Events
        "KelleyBlueBook.SelectionPathChange": function(evt, data) {
            if (data.VehicleId !== "0") {
                $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", data.VehicleId);
            } else {
                $(KelleyBlueBook).trigger("KelleyBlueBook.SelectionChange", data.State);
            }
        },
        "KelleyBlueBook.SelectionChangeWithDecode": function(evt, data) {
            var newSelection = JSON.parse(data.NewKelleyblueBookSelection);
            $(KelleyBlueBook).trigger("KelleyBlueBook.SelectionPathChange", newSelection);
        },
        "KelleyBlueBook.EquipmentItemSelected": function(evt, name, isSelected) {
            $(KelleyBlueBook).trigger(isSelected ? "KelleyBlueBook.AddOption": "KelleyBlueBook.RemoveOption", [name]);
        },
        "KelleyBlueBook.MileageOrStateChange": function(evt, data) {
            $(KelleyBlueBook).trigger("KelleyBlueBook.MileageChange", data.Mileage);
            $(KelleyBlueBook).trigger("KelleyBlueBook.RegionChange", data.RegionId);
            evt.preventDefault();
        },
        "KelleyBlueBook.MileageChange": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("input[name=Mileage]").val(data);
            }
        },
        "KelleyBlueBook.ZipCodeChange": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");
            if (card.views.values.ValueFieldSet.$el) {
                card.views.values.ValueFieldSet.$el.find("input[name=ZipCode]").val(data);
            }
        },
        "KelleyBlueBook.StateChange": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");
            var _newStateName = _.find(_.usStates(), function(state) {
                return this == state.value;
            }, data);
            var _newRegion = _.find(State.Regions, function(region) {
                return this == region.name;
            }, _newStateName.name);
            if ( !! _newRegion) {
                $(KelleyBlueBook).trigger("KelleyBlueBook.RegionChange", _newRegion.value);
                if (card.views.values.ValueFieldSet.$el) {
                    card.views.values.ValueFieldSet.$el.find("select[name=RegionId]").val(_newRegion.value);
                }
            }
        },
        "KelleyBlueBook.PublicationLoaded": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");

            if (data.Publication.VehicleConfiguration) {
                data.Publication.Regions = State.Regions;

                card.build(data.Publication);
            } else {
                $(KelleyBlueBook).trigger("KelleyBlueBook.Start", {});
            }
        },
        "KelleyBlueBook.VehicleChange": function(evt, data) {
            $(KelleyBlueBook).trigger("KelleyBlueBook.LoadPublicationOnDate", '2038-01-01');
        },
        "KelleyBlueBook.ErrorState": function(evt, data) {
            var card = FirstLook.cards.get("KelleyBlueBook");
            card.build(data);
        }
    };
    $(KelleyBlueBook).bind(PublicEvents);

    function kelleyTableSummary(market, title) {
        return function(data) {
            var table = [],
            data_table = data.Tables[market],
            row_finder = function(type) {
                return function(i) {
                    return i.RowType == type;
                };
            },
            f = _.detect(data_table.Rows, row_finder(4));

            data.caption = title;

            table.head = [["Excellent", "Very Good", "Good", "Fair"]];

            table.push([_.money(f.Prices["Excellent"]), _.money(f.Prices["VeryGood"]), _.money(f.Prices["Good"]), _.money(f.Prices["Fair"])]);

            table.col_class_name = ["excellent", "very_good", "good", "fair"];

            data.table = table;

            return data;
        }
    }

    function kelleyTableFilter(market, title) {
        return function(data) {
            var table = [],
            data_table = data.Tables[market],
            row_finder = function(type) {
                return function(i) {
                    return i.RowType == type;
                };
            },
            row_builder = function(type) {
                return [type, _.moneyAdjusted(o.Prices[type]) + "<br />" + _.moneyAdjusted(m.Prices[type]), _.money(f.Prices[type])];
            },
            o = _.detect(data_table.Rows, row_finder(2)),
            m = _.detect(data_table.Rows, row_finder(3)),
            f = _.detect(data_table.Rows, row_finder(4));

            data.caption = title;

            table.head = [["", "Options<br />Mileage", "Final"]];

            table.push(row_builder("Excellent"));
            table.push(row_builder("VeryGood"));
            table.push(row_builder("Good"));
            table.push(row_builder("Fair"));

            table.header_col = 0;
            table.col_class_name = ["market", "adjustments", "final"];
            table.row_class_name = ["excellent", "very_good", "good", "fair"];

            data.table = table;

            return data;
        }
    }

    function make_table(key, title) {
        var pattern = {
            Tables: {}
        };
        pattern.Tables[key] = {
            Rows: [{
                Prices: {
                    Excellent: _.isNumber,
                    Good: _.isNumber,
                    Fair: _.isNumber,
                    VeryGood: _.isNumber
                }
            }]
        }

        return {
            type: "Table",
            pattern: _,
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: kelleyTableFilter(key, title),
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            row_class_name_accessor: "table.row_class_name",
            body_header_col_accessor: "table.header_col"
        }
    }
    function make_summary(key, title) {
        var pattern = {
            Tables: {}
        };
        pattern.Tables[key] = {
            Rows: [{
                Prices: {
                    Excellent: _.isNumber,
                    VeryGood: _.isNumber,
                    Good: _.isNumber,
                    Fair: _.isNumber
                }
            }]
        }

        return {
            type: "Table",
            pattern: _,
            behaviors: {
                rebuildable: {}
            },
            class_name: "kbb_value_tables",
            filter: kelleyTableSummary(key, title),
            caption_accessor: "caption",
            body_accessor: "table",
            head_accessor: "table.head",
            col_class_name_accessor: "table.col_class_name",
            body_header_col_accessor: "table.header_col"
        }
    }

    function make_select() {
        return {
            type: "Fieldset",
            pattern: {
                Path: {
                    Successors: [{
                        State: _.isString,
                        Label: _.isString,
                        Name: _.isString
                    }]
                }
            },
            behaviors: {
                submitable: {
                    event: "KelleyBlueBook.SelectionChangeWithDecode",
                    scope: KelleyBlueBook,
                    submit_on_change: true
                }
            },
            legend_accessor: "fieldset.legend",
            input_accessor: "fieldset.inputs",
            name_accessor: "name",
            button_accessor: "fieldset.buttons",
            label_accessor: "label",
            value_accessor: "value",
            type_accessor: "type",
            filter: function(data) {
                var options = _.map(data.Path.Successors, function(succ) {
                    return {
                        name: succ.Name,
                        value: JSON.stringify({
                            State: succ.State,
                            VehicleId: succ.VehicleId
                        })
                    }
                }),
                fieldset = {
                    legend: "Kelley " + data.Path.Successors[0].Label,
                    inputs: [{
                        name: "NewKelleyblueBookSelection",
                        label: data.Path.Successors[0].Label,
                        options: options
                    }],
                    buttons: []
                }
                options.unshift({
                    name: "Select...",
                    value: ""
                });
                data.fieldset = fieldset;
                return data;
            }
        }
    }

    FirstLook.components.add(make_table("WholesaleLending", "Wholesale Values"), "KelleyBlueBook.WholesaleLendingTable");
    FirstLook.components.add(make_table("SuggestedRetail", "Retail Values"), "KelleyBlueBook.SuggestedRetailTable");
    FirstLook.components.add(make_table("TradeIn", "TradeIn Values"), "KelleyBlueBook.TradeInTable");
    FirstLook.components.add(make_table("PrivateParty", "Private Party Values"), "KelleyBlueBook.PrivatePartyTable");
    FirstLook.components.add(make_table("Auction", "Auction Values"), "KelleyBlueBook.AuctionTable");

    FirstLook.components.add(make_summary("WholesaleLending", "KBB Wholesale Values"), "KelleyBlueBook.WholesaleLendingSummary");
    FirstLook.components.add(make_summary("SuggestedRetail", "KBB Retail Values"), "KelleyBlueBook.SuggestedRetailSummary");
    FirstLook.components.add(make_summary("TradeIn", "KBB TradeIn Values"), "KelleyBlueBook.TradeInSummary");
    FirstLook.components.add(make_summary("PrivateParty", "KBB Private Party Values"), "KelleyBlueBook.PrivatePartySummary");
    FirstLook.components.add(make_summary("Auction", "KBB Auction Values"), "KelleyBlueBook.AuctionSummary");
    FirstLook.components.add(make_select(), "KelleyBlueBook.Select");

    FirstLook.components.add({
        type: "Static",
        pattern: _,
        template: function(data) {
            return "<p class='error'>Problem with the VIN</p>";
        }
    },
    "KelleyBlueBook.VinProblem");

    FirstLook.components.add({
        type: "Static",
        pattern: _,
        template: function(data) {
            return "<p class='message'>No Kelley Blue Book Data For VIN</p>";
        }
    },
    "KelleyBlueBook.NoData");

    var state_pattern = {
        select: {
            Path: {
                Successors: _.isArray
            },
            Status: function(v) {
                return v === 0 || _.isUndefined(v);
            }
        },
        values: {
            VehicleConfiguration: _
        },
        vin_problem: {
            Status: function(v) {
                return v >= 1 && v <= 3;
            }
        },
        data_problem: {
            Status: function(v) {
                return v === 4;
            }
        },
        error: {
            errorType: _,
            errorResponse: _
        }
    }

    var overview_card = {
        pattern: _,
        view: {
            select: {
                pattern: state_pattern.select,
                select: FirstLook.components.get("KelleyBlueBook.Select")
            },
            values: {
                pattern: state_pattern.values,
                values: FirstLook.components.get("KelleyBlueBook.WholesaleLendingSummary")
            },
            vin_problem: {
                pattern: state_pattern.vin_problem,
                message: FirstLook.components.get("KelleyBlueBook.VinProblem")
            },
            data_problem: {
                pattern: state_pattern.data_problem,
                message: FirstLook.components.get("KelleyBlueBook.NoData")
            },
            error: {
                pattern: state_pattern.error,
                message: FirstLook.components.get("Application.ServerError")
            }
        }
    };
    FirstLook.cards.add(overview_card, "KelleyBlueBookSummary");

    var cards = {
        title: "Kelley",
        pattern: _,
        view: {
            select: {
                order: 0,
                pattern: state_pattern.select,
                Title: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<h1 class='logo_kelly_blue_book'>Kelly Blue Book</h1>";
                    }
                },
                Review: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<p>" + _.pluck(data._selection, "Name").join(" | ") + "</p>";
                    },
                    filter: function(data) {
                        var selection = [];
                        function append_parent(v) {
                            selection.push(v);
                            if (v.Parent) {
                                append_parent(v.Parent);
                            }
                            return _.first(selection, selection.length - 1);
                        }

                        data._selection = append_parent(data.Path.CurrentNode);
                        return data;
                    }
                },
                TraversalList: {
                    type: "List",
                    class_name: "select_list vid_select",
                    pattern: {
                        Path: {
                            Successors: [{
                                Name: _.isString,
                                Id: _.isNumber
                            }]
                        }
                    },
                    behaviors: {
                        selectable: {
                            pattern: {
                                Path: {
                                    Successors: [{
                                        State: _.isString,
                                        VehicleId: _
                                    }]
                                }
                            },
                            data_accessor: ["State", "VehicleId"],
                            binds: {
                                "item_selected": {
                                    type: "KelleyBlueBook.SelectionPathChange",
                                    scope: KelleyBlueBook
                                }
                            }
                        }
                    },
                    item_accessor: "Path.Successors",
                    text_accessor: "Name",
                    data_accessor: "Id"
                }
            },
            values: {
                order: 1,
                pattern: state_pattern.values,
                ValueFieldSet: {
                    type: "Fieldset",
                    pattern: {
                        Arguments: {
                            Mileage: _.isNumber
                        }
                    },
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "KelleyBlueBook.MileageOrStateChange",
                            scope: KelleyBlueBook,
                            submit_on_change: true
                        }
                    },
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    class_name: "book_fieldset",
                    static_data: {
                        legend: "KelleyBlueBook",
                        inputs: [{
                            name: "RegionId",
                            label: "Regions",
                            data: "Arguments.Region.Id",
                            options: "Regions"
                        },
                        {
                            name: "Mileage",
                            label: "Mileage",
                            data: "Arguments.Mileage",
                            type: "tel"
                        }],
                        buttons: ["Update"]
                    }
                },
                Carousel: {
                    type: "Carousel",
                    pattern: _,
                    components: ["KelleyBlueBook.WholesaleLendingTable", "KelleyBlueBook.SuggestedRetailTable", "KelleyBlueBook.TradeInTable", "KelleyBlueBook.PrivatePartyTable", "KelleyBlueBook.AuctionTable"],
                    behaviors: {
                        carouselable: {}
                    }
                },
                EquipmentList: {
                    type: "List",
                    behaviors: {
                        multi_selectable: {
                            pattern: {
                                VehicleConfiguration: {
                                    OptionStates: [{
                                        OptionId: _,
                                        Selected: _.isBoolean
                                    }]
                                }
                            },
                            multi_item_accessor: "VehicleConfiguration.OptionStates",
                            multi_data_accessor: "OptionId",
                            map_data_accessor: "Id",
                            selected_accessor: "Selected",
                            binds: {
                                "item_selected": {
                                    scope: KelleyBlueBook,
                                    type: "KelleyBlueBook.EquipmentItemSelected"
                                }
                            }
                        }
                    },
                    pattern: {
                        Options: [{
                            Name: _,
                            Id: _
                        }]
                    },
                    class_name: "equipment_list",
                    item_accessor: "Options",
                    text_accessor: "Name",
                    data_accessor: ["Id", "OptionType", "OptionCategory"]
                }
            },
            vin_problem: {
                pattern: state_pattern.vin_problem,
                Message: FirstLook.components.get("KelleyBlueBook.VinProblem")
            },
            data_problem: {
                pattern: state_pattern.vin_problem,
                Message: FirstLook.components.get("KelleyBlueBook.NoData")
            },
            error: {
                pattern: state_pattern.error,
                ErrorMessage: FirstLook.components.get("Application.ServerError")
            }
        }
    };

    FirstLook.cards.add(cards, "KelleyBlueBook");
})(KelleyBlueBook);

