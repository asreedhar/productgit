if (typeof KelleyBlueBook !== "function") {
	var KelleyBlueBook = function() {};
}

(function() {
	var ns = this;

	var State = {
		Mileage: null,
        RegionId: 25, //-1, // 25 is Illinois - must have default region otherwise decode will not work
		ZipCode: '',    //'60607',
		/// TODO: remove hardcoded default State, PM
		Vin: '',
		VehicleId: '',
		VehicleConfiguration: [],
        /// Publication State
        Broker: '',
        Vehicle: '',
        Regions: []
	};

	/* ==================== */
	/* == START: Classes == */
	/* ==================== */

    function RegionDto() {
        this.Id = 0;
		this.Name = '';
		this.Type = 0;
	}

    var RegionTypeDto = {
		"Undefined": 0,
		"NationalBase": 5,
		"VimsRegionality": 7,
		"toString": function(i) {
			var names = ["Undefined", "National Base", "Vims Regionality"];
			if (i >= 0 && i < names.length) {
				return names[i];
			}
			return i;
		}
	};

	/* traversal dto */
	function PathDto() {
		this.CurrentNode = null;
		this.Successors = [];
	}

	function NodeDto() {
		this.Parent = null;
		this.Children = [];
		this.VehicleId = 0;
		this.HasVehicleId = false;
		this.Id = 0;
		this.Name = "";
		this.SortOrder = 0;
		this.State = "";
		this.Label = "";
	}

	/* valuation dto */

    function BookDateDto() {
        this.Id = 0;        
    }

	function PriceTablesDto() {
		this.WholesaleLending = null;
		this.SuggestedRetail = null;
		this.TradeIn = null;
		this.PrivateParty = null;
		this.Auction = null;
	}

	function PriceTableDto() {
		this.HasExcellent = true;
		this.HasGood = true;
		this.HasFair = true;
		this.HasVeryGood = true;
		this.Rows = [];
	}

	function PriceTableRowDto() {
		this.RowType = null;
		this.Prices = null;
	}

	var PriceTableRowTypeDto = {
		"Undefined": 0,
		"Base": 1,
		"Option": 2,
		"Mileage": 3,
		"Final": 4,
		"toString": function(i) {
			var names = ["Undefined", "Base", "Options", "Mileage", "Final"];
			if (i >= 0 && i < names.length) {
				return names[i];
			}
			return i;
		}
	};

	function PricesDto() {
		this.Excellent = 0;
		this.Good = 0;
		this.VeryGood = 0;
		this.Fair = 0;
		this.HasExcellent = false;
		this.HasGood = false;
		this.HasVeryGood = false;
		this.HasFair = false;
	}

	function OptionDto() {
		this.Id = 0;
		this.Name = "";
		this.SortOrder = 0;
		this.OptionType = 0;
		this.OptionCategory = 0;
		this.Tables = null;
	}

	var OptionTypeDto = {
		"Undefined": 0,
		"Equipment": 4,
		"Option": 5,
		"CertifiedPreOwned": 7
	};

	var OptionCategoryDto = {
		"Undefined": 0,
		"DriveTrain": 1,
		"Engine": 2,
		"Transmission": 3
	};

	function VehicleConfigurationDto() {
	    this.BookDate = null;
		this.VehicleId = 0;
		this.Vin = "";
		this.ZipCode = "";
		this.Region = null;
        this.Mileage = null;
		this.IsMissingEngine = false;
		this.IsMissingDriveTrain = false;
		this.IsMissingTransmission = false;
		this.OptionStates = [];
		this.OptionActions = [];
	}

	function OptionStateDto() {
		this.OptionId = 0;
		this.Selected = false;
	}

	function OptionActionDto() {
		this.ActionType = 0;
		this.OptionId = 0;
	}

	var OptionActionTypeDto = {
		"Undefined": 0,
		"Add": 1,
		"Remove": 2
	};

	function SpecificationDto() {
		this.Id = '';
		this.Name = '';
		this.Unit = '';
		this.Value = '';
		this.SpecificationType = 0;
	}

	var SpecificationTypeDto = {
		"Undefined": 0,
		"Specifications": 1,
		"Warranty": 2,
		"Dimensions": 3,
		"General": 4,
		"Safety": 5,
		"Features": 6,
		"MpgRatings": 7,
		"Engine": 8,
		"CrashTest": 9,
		"Performance": 10
	};

	var TraversalStatusDto = {
		"Success": 0,
		"InvalidVinLength": 1,
		"InvalidVinCharacters": 2,
		"InvalidVinChecksum": 3,
		"NoDataForVin": 4,
		"toString": function(i) {
			var names = ["Success", "Invalid Vin Length", "Invalid Vin Characters", "Invalid Vin Checksum", "No Data For Vin"];
			if (i >= 0 && i < names.length) {
				return names[i];
			}
			return i;
		}
	};

    /* publication dto */

    var ChangeAgentDto = {
        "Undefined": 0,
        "User": 1,
        "System": 2,
        "toString": function(i) {
            var names = ["Undefined", "User Change", "System Change"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    // TODO: Message from Simon to Paul
    // TODO: This is a list of the bit flags passed from the server.
    // TODO: END

    var ChangeTypeDto = {
        "None": 0,
        "BookDate": (1 << 0),
        "Region": (1 << 1),
        "VehicleId": (1 << 2),
        "Mileage": (1 << 3),
        "OptionActions": (1 << 4),
        "OptionStates": (1 << 5),
        "ZipCode": (1 << 6)
        /* // TODO: rewrite
        "toString": function(i) {
            var names = ["None", "BookDate", "Region", "VehicleId", "Mileage", "Option Actions", "Option States", "ZipCode"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
        */
    };

    function UserDto() {
        this.FirstName = null;
        this.LastName = null;
        this.UserName = null;
    }

    function EditionDto() {
        this.BeginDate = null;
        this.EndDate = null;
        this.User = null;
    }

    function PublicationInfoDto() {
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
    }

    function PublicationDto() {
        // from info
        this.ChangeAgent = 0;
        this.ChangeType = 0;
        this.Id = 0;
        this.Edition = null;
        // own properties
        this.VehicleConfiguration = null;
        this.Tables = null;
        this.Options = [];
    }

	/* envelopes dto */
    function RegionArgumentsDto() {
        this.ZipCode = '';
		EventBinder(this);
	}

	function RegionResultsDto() {
		this.Arguments = null;
		this.Region = null;
	}

    function RegionsArgumentsDto() {
		EventBinder(this);
	}

	function RegionsResultsDto() {
		this.Arguments = null;
		this.Regions = [];
	}

	function TraversalArgumentsDto() {
		this.Vin = '';
		EventBinder(this);
	}

	function TraversalResultsDto() {
		this.Arguments = null;
		this.Path = null;
		this.Status = 0;
	}

	function SuccessorArgumentsDto() {
		this.Node = "";
		this.Successor = "";
		EventBinder(this);
	}

	function SuccessorResultsDto() {
		this.Arguments = null;
		this.Path = null;
	}

	function InitialValuationArgumentsDto() {
		this.Vin = '';
		this.VehicleId = 0;
		this.ZipCode = '';
		this.Mileage = null;
        this.Region = null;
		this.HasMileage = false;
		EventBinder(this);
	}

	function InitialValuationResultsDto() {
		this.Tables = null;
		this.Arguments = null;
		this.Options = [];
		this.Specifications = [];
		this.VehicleConfiguration = null;
	}

	function AdjustedValuationArgumentsDto() {
		this.Vin = '';
		this.VehicleId = 0;
		this.ZipCode = '';
		this.Mileage = null;
		this.HasMileage = false;
		this.VehicleConfiguration = null;
		this.OptionAction = null;
        this.Region = null;
		EventBinder(this);
	}

	function AdjustedValuationResultsDto() {
		this.Tables = null;
		this.Arguments = null;
		this.VehicleConfiguration = null;
		EventBinder(this);
	}
	
	/* publication envelopes dto */

    function PublicationListArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        // -- (none) --
        // events please
        EventBinder(this);
    }

    function PublicationListResultsDto() {
        // self
        this.Arguments = null;
        this.Publications = [];
    }

    function PublicationOnDateArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.On = '';
        // events please
        EventBinder(this);
    }

    function PublicationOnDateResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

    function PublicationLoadArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.Id = 0;
        // events please
        EventBinder(this);
    }

    function PublicationLoadResultsDto() {
        // self
        this.Arguments = '';
        this.Publication = '';
    }

    function PublicationSaveArgumentsDto() {
        // parent
        this.Broker = '';
        this.Vehicle = '';
        // self
        this.VehicleConfiguration = null;
        // events please
        EventBinder(this);
    }

    function PublicationSaveResultsDto() {
        // self
        this.Arguments = null;
        this.Publication = null;
    }

	/* Change Classes */
	/* == END: Classes == */
	var Events = {
        "RegionArgumentsDto": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
				// render the data
				$(KelleyBlueBook).trigger("KelleyBlueBook.RegionLoaded", [data.Region]);
			}
		},
        "RegionsArgumentsDto": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
                State.Regions = data.Regions;
				// render the data
				$(KelleyBlueBook).trigger("KelleyBlueBook.RegionsLoaded", [data.Regions, State.RegionId]);
			}
		},
		"TraversalArgumentsDto": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
                var hasVehicleId = data.Path.CurrentNode.Label == "Vin" && data.Path.CurrentNode.VehicleId;
				if (hasVehicleId) {
					$(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", data.Path.CurrentNode.VehicleId);
				}
				if (!hasVehicleId || this.RunNodeTemplate) {
					$(KelleyBlueBook).trigger("KelleyBlueBook.TraversalLoaded", [data]);
				}
			}
		},
		"SuccessorArgumentsDto": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
				$(KelleyBlueBook).trigger("KelleyBlueBook.SuccessorLoaded", [data]);
			}
		},
		"InitialValuationArgumentsDto": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
				// save the state
				State.VehicleConfiguration = data.VehicleConfiguration;
				// build the page
				$(KelleyBlueBook).trigger("KelleyBlueBook.InitialValueLoaded", data);
			},
			stateChange: function(event) {
				if (State.VehicleId !== '') {
					var valuation = new InitialValuationArgumentsDto();
					valuation.VehicleId = State.VehicleId;
					valuation.Vin = State.Vin;
					valuation.Mileage = State.Mileage;
                    valuation.ZipCode = State.ZipCode;

                    valuation.Region = new RegionDto();
                    valuation.Region.Id = State.RegionId;

					$(valuation).trigger("fetch");
				}
			}
		},
		"AdjustedValuationArgumentsDto": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
				// save the state
				State.VehicleConfiguration = data.VehicleConfiguration;
				// build the page
				$(KelleyBlueBook).trigger("KelleyBlueBook.AdjustedValueLoaded", data);
			},
			stateChange: function(event, action) {
				if (State.VehicleId !== '') {
					var args = new AdjustedValuationArgumentsDto();
					args.VehicleId = State.VehicleId;
					args.Vin = State.Vin;
                    args.Region = new RegionDto();
                    args.Region.Id = State.RegionId;
					args.ZipCode = State.ZipCode;
					args.Mileage = State.Mileage;
					args.VehicleConfiguration = State.VehicleConfiguration;
					args.OptionAction = action || null;
					$(args).trigger('fetch');
				}
			}
		},
        "PublicationListArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(KelleyBlueBook).trigger("KelleyBlueBook.PublicationListLoaded", data);
            }
        },
        "PublicationOnDateArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                if (data.Publication.VehicleConfiguration) {
                    State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                    State.VehicleId = data.Publication.VehicleConfiguration.VehicleId;
                }
                $(KelleyBlueBook).trigger("KelleyBlueBook.PublicationLoaded", data);
            }
        },
        "PublicationLoadArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                State.VehicleConfiguration = data.Publication.VehicleConfiguration;
                State.VehicleId = data.Publication.VehicleConfiguration.VehicleId;
                $(KelleyBlueBook).trigger("KelleyBlueBook.PublicationLoaded", data);
            }
        },
        "PublicationSaveArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(KelleyBlueBook).trigger("KelleyBlueBook.PublicationSaved", data);
            }
        }
	};

	function genericMapper(type, config) {
		if (typeof config != "object") {
			config = {};
		}
		return function(json) {
			var result = new type(),
			prop,
			subJson,
			idx;
			for (prop in json) {
				if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
				subJson = json[prop];
				if (config.hasOwnProperty(prop)) {
					if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
						for (idx in subJson) {
							if (!subJson.hasOwnProperty(idx)) continue;
							result[prop].push(DataMapper[config[prop]](subJson[idx]));
						}
					} else {
						result[prop] = DataMapper[config[prop]](subJson);
					}
				} else if (typeof result[prop] !== "undefined") { // don't overwrite properties
					result[prop] = subJson;
				}
			}
			return result;
		};
	}
	var DataMapper = {
        "RegionArgumentsDto": genericMapper(RegionArgumentsDto),
		"RegionResultsDto": genericMapper(RegionResultsDto, {
			"Arguments": "RegionArgumentsDto",
			"Region": "RegionDto"
		}),
        "RegionsArgumentsDto": genericMapper(RegionsArgumentsDto),
		"RegionsResultsDto": genericMapper(RegionsResultsDto, {
			"Arguments": "RegionsArgumentsDto",
			"Regions": "RegionDto"
		}),
		"TraversalArgumentsDto": genericMapper(TraversalArgumentsDto),
		"TraversalResultsDto": genericMapper(TraversalResultsDto, {
			"Arguments": "TraversalArgumentsDto",
			"Path": "PathDto"
		}),
		"SuccessorArgumentsDto": genericMapper(SuccessorArgumentsDto),
		"SuccessorResultsDto": genericMapper(SuccessorResultsDto, {
			"Arguments": "SuccessorResultsDto",
			"Path": "PathDto"
		}),
		"NodeDto": genericMapper(NodeDto, {
			"Parent": "NodeDto",
			"Children": "NodeDto"
		}),
		"PathDto": genericMapper(PathDto, {
			"CurrentNode": "NodeDto",
			"Successors": "NodeDto"
		}),
		"InitialValuationArgumentsDto": genericMapper(InitialValuationArgumentsDto),
		"InitialValuationResultsDto": genericMapper(InitialValuationResultsDto, {
			"Arguments": "InitialValuationArgumentsDto",
			"VehicleConfiguration": "VehicleConfigurationDto",
			"Tables": "PriceTablesDto",
			"Options": "OptionDto",
			"Specifications": "SpecificationDto"
		}),
		"AdjustedValuationArgumentsDto": genericMapper(AdjustedValuationArgumentsDto, {
			"OptionAction": "OptionActionDto",
			"VehicleConfiguration": "VehicleConfigurationDto"
		}),
		"AdjustedValuationResultsDto": genericMapper(AdjustedValuationResultsDto, {
			"Arguments": "AdjustedValuationArgumentsDto",
			"Tables": "PriceTablesDto",
			"VehicleConfiguration": "VehicleConfigurationDto"
		}),
		"PriceTablesDto": function(json) {
			var result = new PriceTablesDto();
			for (var i in json) {
				if (!json.hasOwnProperty(i)) continue;
				if (i == 'WholesaleLending') {
					result.WholesaleLending = DataMapper.PriceTableDto(json[i]);
				} else if (i == 'SuggestedRetail') {
					result.SuggestedRetail = DataMapper.PriceTableDto(json[i]);
				} else if (i == 'TradeIn') {
					result.TradeIn = DataMapper.PriceTableDto(json[i]);
				} else if (i == 'PrivateParty') {
					result.PrivateParty = DataMapper.PriceTableDto(json[i]);
				} else if (i == 'Auction') {
					result.Auction = DataMapper.PriceTableDto(json[i]);
				}
			}
			return result;
		},
		"PriceTableDto": genericMapper(PriceTableDto, {
			"Rows": "PriceTableRowDto"
		}),
		"PriceTableRowDto": function(json) {
			var result = new PriceTableRowDto();
			for (var i in json) {
				if (!json.hasOwnProperty(i)) continue;
				if (i == 'Prices') {
					result.Prices = DataMapper.PricesDto(json[i]);
				} else if (i == 'RowType') {
					switch (json[i]) { //TODO: remove assignement to self
					case 1:
						result.RowType = PriceTableRowTypeDto.Base;
						break;
					case 2:
						result.RowType = PriceTableRowTypeDto.Option;
						break;
					case 3:
						result.RowType = PriceTableRowTypeDto.Mileage;
						break;
					case 4:
						result.RowType = PriceTableRowTypeDto.Final;
						break;
					default:
						//throw new Error('RowType is invalid');
						break;
					}
				}
			}
			return result;
		},
		"PricesDto": genericMapper(PricesDto),
		"OptionDto": function(json) {
			var result = new OptionDto();
			for (var i in json) {
				if (!json.hasOwnProperty(i)) continue;
				if (i == 'Tables') {
					result.Tables = DataMapper.PriceTablesDto(json[i]);
				} else if (i == 'OptionType') {
					switch (json[i]) { //TODO: remove assignment to self
					case 4:
						result.OptionType = OptionTypeDto.Equipment;
						break;
					case 5:
						result.OptionType = OptionTypeDto.Option;
						break;
					case 7:
						result.OptionType = OptionTypeDto.CertifiedPreOwned;
						break;
					default:
						//throw new Error('OptionType is invalid');
						break;
					}
				} else if (i == 'OptionCategory') {
					switch (json[i]) { //TODO: remove assignment to self
					case 1:
						result.OptionCategory = OptionCategoryDto.DriveTrain;
						break;
					case 2:
						result.OptionCategory = OptionCategoryDto.Engine;
						break;
					case 3:
						result.OptionCategory = OptionCategoryDto.Transmission;
						break;
					default:
						//throw new Error('OptionCategory is invalid');
						break;
					}
				} else if (result[i] !== undefined) {
					result[i] = json[i];
				}
			}
			return result;
		},
		"BookDateDto": genericMapper(BookDateDto),
        "RegionDto": genericMapper(RegionDto),
		"VehicleConfigurationDto": genericMapper(VehicleConfigurationDto, {
		    "BookDate": "BookDateDto",
			"OptionStates": "OptionStateDto",
			"OptionActions": "OptionActionDto"
		}),
		"OptionStateDto": genericMapper(OptionStateDto),
		"OptionActionDto": genericMapper(OptionActionDto),
		"SpecificationDto": genericMapper(SpecificationDto),
        /* publication api */
        "PublicationListArgumentsDto": genericMapper(PublicationListArgumentsDto),
        "PublicationListResultsDto": genericMapper(PublicationListResultsDto, {
            "Arguments": "PublicationListArgumentsDto",
            "Publications": "PublicationInfoDto"
        }),
        "PublicationOnDateArgumentsDto": genericMapper(PublicationOnDateArgumentsDto),
        "PublicationOnDateResultsDto": genericMapper(PublicationOnDateResultsDto, {
            "Arguments": "PublicationOnDateArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationLoadArgumentsDto": genericMapper(PublicationLoadArgumentsDto),
        "PublicationLoadResultsDto": genericMapper(PublicationLoadResultsDto, {
            "Arguments": "PublicationLoadArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "PublicationSaveArgumentsDto": genericMapper(PublicationSaveArgumentsDto),
        "PublicationSaveResultsDto": genericMapper(PublicationSaveResultsDto, {
            "Arguments": "PublicationSaveArgumentsDto",
            "Publication": "PublicationDto"
        }),
        "UserDto": genericMapper(UserDto),
        "EditionDto": genericMapper(EditionDto, {
            "User": "UserDto"
        }),
        "PublicationInfoDto": genericMapper(PublicationInfoDto, {
            "Edition": "EditionDto"
        }),
        "PublicationDto": genericMapper(PublicationDto, {
            "Edition": "EditionDto",
            "VehicleConfiguration": "VehicleConfigurationDto",
            "Tables": "PriceTablesDto",
            "Options": "OptionDto"
        })
	};

	function genericService(serviceUrl, resultType, getDataFunc) {
		return {
			AjaxSetup: {
				"url": serviceUrl
			},
			fetch: function() {
				var me = this;
				var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
				ajaxSetup.data = this.getData();
				function onFetchSuccess(json) {
					var results = resultType.fromJSON(json.d);
					$(me).trigger("fetchComplete", [results]);
				}
				ajaxSetup.success = onFetchSuccess;
				$.ajax(ajaxSetup);
			},
			getData: getDataFunc
		};
	}
	var Services = {
		"Default": {
			AjaxSetup: {
				type: "POST",
				dataType: "json",
				processData: false,
				contentType: "application/json; charset=utf-8",
				error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(KelleyBlueBook).trigger("KelleyBlueBook.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
						var response = JSON.parse(xhr.responseText);
						if ( !!  response['auth-login']) {
							window.location.assign(response['auth-login']);
						}
					}
				}
			}
		},
        "Region": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/Region", RegionResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.RegionArgumentsDto",
					"ZipCode": (this.ZipCode == null ? '': this.ZipCode)
				}
			});
		}),
        "Regions": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/Regions", RegionsResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.RegionsArgumentsDto"
				}
			});
		}),
		"Traversal": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/Traversal", TraversalResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.TraversalArgumentsDto",
					"Vin": (this.Vin == null ? '': this.Vin)
				}
			});
		}),
		"Successor": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/Successor", SuccessorResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.SuccessorArgumentsDto",
					"Node": this.Node,
					"Successor": this.Successor
				}
			});
		}),
		"InitialValuation": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/InitialValuation", InitialValuationResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto",
					"Vin": (this.Vin == null ? '': this.Vin),
					"VehicleId": this.VehicleId,
                    "Region": this.Region,
					"ZipCode": (this.ZipCode == null ? '': this.ZipCode),
					"Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage: 0),
					"HasMileage": ((/^\d+$/.test(this.Mileage)) ? true: false)
				}
			});
		}),
		"AdjustedValuation": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/AdjustedValuation", AdjustedValuationResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto",
					"Vin": (this.Vin == null ? '': this.Vin),
					"VehicleId": this.VehicleId,
                    "Region": this.Region,
					"ZipCode": (this.ZipCode == null ? '': this.ZipCode),
					"Mileage": ((/^\d+$/.test(this.Mileage)) ? this.Mileage: 0),
					"HasMileage": ((/^\d+$/.test(this.Mileage)) ? true: false),
					"VehicleConfiguration": this.VehicleConfiguration,
					"OptionAction": this.OptionAction
				}
			});
		}),
        "PublicationList": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/PublicationList", PublicationListResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications.PublicationListArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "PublicationOnDate": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/PublicationOnDate", PublicationOnDateResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications.PublicationOnDateArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "On": this.On
                }
            });
        }),
        "PublicationLoad": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/PublicationLoad", PublicationLoadResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications.PublicationLoadArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "Id": this.Id
                }
            });
        }),
        "PublicationSave": genericService("/VehicleValuationGuide/Services/KelleyBlueBook.asmx/PublicationSave", PublicationSaveResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle,
                    "VehicleConfiguration": this.VehicleConfiguration
                }
            });
        })
	};

	var EventBinder = function(obj) {
        if (obj instanceof RegionArgumentsDto) {
			$(obj).bind("fetchComplete", Events.RegionArgumentsDto.fetchComplete);
		}
        if (obj instanceof RegionsArgumentsDto) {
			$(obj).bind("fetchComplete", Events.RegionsArgumentsDto.fetchComplete);
		}
		if (obj instanceof TraversalArgumentsDto) {
			$(obj).bind("fetchComplete", Events.TraversalArgumentsDto.fetchComplete);
		}
		if (obj instanceof SuccessorArgumentsDto) {
			$(obj).bind("fetchComplete", Events.SuccessorArgumentsDto.fetchComplete);
		}
		if (obj instanceof InitialValuationArgumentsDto) {
			$(obj).bind("fetchComplete", Events.InitialValuationArgumentsDto.fetchComplete);
			$(obj).bind("stateChange", Events.InitialValuationArgumentsDto.stateChange);
		}
		if (obj instanceof AdjustedValuationArgumentsDto) {
			$(obj).bind("fetchComplete", Events.AdjustedValuationArgumentsDto.fetchComplete);
			$(obj).bind("stateChange", Events.AdjustedValuationArgumentsDto.stateChange);
		}
		if (obj instanceof PublicationListArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationListArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationOnDateArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationOnDateArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationLoadArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationLoadArgumentsDto.fetchComplete);
        }
        if (obj instanceof PublicationSaveArgumentsDto) {
            $(obj).bind("fetchComplete", Events.PublicationSaveArgumentsDto.fetchComplete);
        }
	};

	// ====================
	// = Bind DataMappers =
	// ====================
	(function() {
		function BindDataMapper(target, source) {
			target.fromJSON = source;
		}

        BindDataMapper(RegionArgumentsDto, DataMapper.RegionArgumentsDto);
		BindDataMapper(RegionResultsDto, DataMapper.RegionResultsDto);

        BindDataMapper(RegionsArgumentsDto, DataMapper.RegionsArgumentsDto);
		BindDataMapper(RegionsResultsDto, DataMapper.RegionsResultsDto);

		BindDataMapper(TraversalArgumentsDto, DataMapper.TraversalArgumentsDto);
		BindDataMapper(TraversalResultsDto, DataMapper.TraversalResultsDto);

		BindDataMapper(SuccessorArgumentsDto, DataMapper.SuccessorArgumentsDto);
		BindDataMapper(SuccessorResultsDto, DataMapper.SuccessorResultsDto);

		BindDataMapper(NodeDto, DataMapper.NodeDto);
		BindDataMapper(PathDto, DataMapper.PathDto);

		BindDataMapper(InitialValuationArgumentsDto, DataMapper.InitialValuationArgumentsDto);
		BindDataMapper(InitialValuationResultsDto, DataMapper.InitialValuationResultsDto);

		BindDataMapper(AdjustedValuationArgumentsDto, DataMapper.AdjustedValuationArgumentsDto);
		BindDataMapper(AdjustedValuationResultsDto, DataMapper.AdjustedValuationResultsDto);

		BindDataMapper(PriceTablesDto, DataMapper.PriceTablesDto);
		BindDataMapper(PriceTableDto, DataMapper.PriceTableDto);
		BindDataMapper(PriceTableRowDto, DataMapper.PriceTableRowDto);
		BindDataMapper(PricesDto, DataMapper.PricesDto);
		BindDataMapper(OptionDto, DataMapper.OptionDto);
		BindDataMapper(VehicleConfigurationDto, DataMapper.VehicleConfigurationDto);
		BindDataMapper(OptionStateDto, DataMapper.OptionStateDto);
		BindDataMapper(OptionActionDto, DataMapper.OptionActionDto);
        BindDataMapper(RegionDto, DataMapper.RegionDto);
		
		BindDataMapper(PublicationListArgumentsDto, DataMapper.PublicationListArgumentsDto);
        BindDataMapper(PublicationListResultsDto, DataMapper.PublicationListResultsDto);

        BindDataMapper(PublicationOnDateArgumentsDto, DataMapper.PublicationOnDateArgumentsDto);
        BindDataMapper(PublicationOnDateResultsDto, DataMapper.PublicationOnDateResultsDto);

        BindDataMapper(PublicationLoadArgumentsDto, DataMapper.PublicationLoadArgumentsDto);
        BindDataMapper(PublicationLoadResultsDto, DataMapper.PublicationLoadResultsDto);

        BindDataMapper(PublicationSaveArgumentsDto, DataMapper.PublicationSaveArgumentsDto);
        BindDataMapper(PublicationSaveResultsDto, DataMapper.PublicationSaveResultsDto);

        BindDataMapper(UserDto, DataMapper.UserDto);
        BindDataMapper(EditionDto, DataMapper.EditionDto);
        BindDataMapper(PublicationDto, DataMapper.PublicationDto);
        BindDataMapper(PublicationInfoDto, DataMapper.PublicationInfoDto);

	})();

	// =======================
	// = Bind Service Fetchs =
	// =======================
	(function() {
		function BindFetch(target, source) {
			for (var m in source) {
				if (!source.hasOwnProperty(m)) continue;
				target.prototype[m] = source[m];
			}
			$(target).bind("fetch", target.prototype.fetch);
		}

        BindFetch(RegionArgumentsDto, Services.Region);
        BindFetch(RegionsArgumentsDto, Services.Regions);

		BindFetch(TraversalArgumentsDto, Services.Traversal);
		BindFetch(SuccessorArgumentsDto, Services.Successor);

		BindFetch(InitialValuationArgumentsDto, Services.InitialValuation);
		BindFetch(AdjustedValuationArgumentsDto, Services.AdjustedValuation);

        BindFetch(PublicationListArgumentsDto, Services.PublicationList);
        BindFetch(PublicationOnDateArgumentsDto, Services.PublicationOnDate);
        BindFetch(PublicationLoadArgumentsDto, Services.PublicationLoad);
        BindFetch(PublicationSaveArgumentsDto, Services.PublicationSave);
	})();

	/* ================== */
	/* = PUBLIC METHODS = */
	/* ================== */
	if (typeof this.prototype === "undefined") {
		this.prototype = {};
	}
	this.prototype.main = function(selection) {
		if (typeof selection == "undefined") {
			selection = {};
		}

        if (selection.buid) {
            $(KelleyBlueBook).trigger("KelleyBlueBook.BrokerChange", selection.buid);
        }

        if (selection.region) {
            State.RegionId = selection.regionId;
        }

        if (selection.state) {
            if (State.Regions.length > 0) {
                $(KelleyBlueBook).trigger("KelleyBlueBook.StateChange", selection.state);
            } else {
                $(KelleyBlueBook).one("KelleyBlueBook.RegionsLoaded", selection.state, function(evt) {
                        $(KelleyBlueBook).trigger("KelleyBlueBook.StateChange", evt.data); // pass data via the event, better memory management, PM
                });
            }
        }

        if (selection.mileage) {
            State.Mileage = selection.mileage;
        }


		if (selection.vuid) {
            $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleChange", selection.vuid);
        } else {
            $(KelleyBlueBook).trigger("KelleyBlueBook.Start", selection);
        }

        /// We always want to select regions, PM (mod by Oleg)
		$(new RegionsArgumentsDto()).trigger("fetch");
        
        State.Vin = selection.vin || State.Vin;
        State.VehicleId = selection.vehicleId || State.VehicleId;
	};

	this.raises = this.raises || [];
    this.raises.push("KelleyBlueBook.RegionChange");
    this.raises.push("KelleyBlueBook.RegionChange");
    this.raises.push("KelleyBlueBook.RegionLoaded");
    this.raises.push("KelleyBlueBook.RegionsLoaded");
	this.raises.push("KelleyBlueBook.TraversalLoaded");
	this.raises.push("KelleyBlueBook.SuccessorLoaded");
	this.raises.push("KelleyBlueBook.InitialValueLoaded");
	this.raises.push("KelleyBlueBook.AdjustedValueLoaded");
	this.raises.push("KelleyBlueBook.ErrorState");

	this.listensTo = this.listensTo || [];
    this.listensTo.push("KelleyBlueBook.RegionChange");
	this.listensTo.push("KelleyBlueBook.ZipCodeChange");
	this.listensTo.push("KelleyBlueBook.MileageChange");
	this.listensTo.push("KelleyBlueBook.VinChange");
	this.listensTo.push("KelleyBlueBook.VehicleIdChange");
	this.listensTo.push("KelleyBlueBook.SelectionChange");
	this.listensTo.push("KelleyBlueBook.AddOption");
	this.listensTo.push("KelleyBlueBook.RemoveOption");
	this.listensTo.push("KelleyBlueBook.LoadPublicationList");
    this.listensTo.push("KelleyBlueBook.LoadPublicationOnDate");
    this.listensTo.push("KelleyBlueBook.LoadPublication");
    this.listensTo.push("KelleyBlueBook.SavePublication");

	var PublicEvents = {
        "KelleyBlueBook.RegionChange": function(evt, regionId, zipCode) {
			if (State.RegionId !== regionId) {
				State.RegionId = regionId;
                State.ZipCode = zipCode;

				$(new AdjustedValuationArgumentsDto()).trigger("stateChange");
			}
		},
		"KelleyBlueBook.ZipCodeChange": function(evt, newZipCode) {
            var zipCodePattern = /^\d{5}$/;     // full zip pattern:    /^\d{5}$|^\d{5}-\d{4}$/;
            if (!zipCodePattern.test(newZipCode)) {
				throw new Error("Invalid zipcode");
			}
			if (State.ZipCode !== newZipCode) {
				State.ZipCode = newZipCode;

				var region = new RegionArgumentsDto();
                region.ZipCode = State.ZipCode;
                $(region).trigger("fetch");
			}
		},
		"KelleyBlueBook.MileageChange": function(evt, newMileage) {
			if (State.Mileage !== (newMileage || null)) { // Protect against undefined... Mileage is default to null
				State.Mileage = newMileage;
				$(new AdjustedValuationArgumentsDto()).trigger("stateChange");
			}
		},
		"KelleyBlueBook.SelectionChange": function(evt, next, successors) {
			var hasVehicleid = successors && successors.length > 0 && !! successors[0].VehicleId;
			if (hasVehicleid) {
				$(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", next);
			} else {
				var successor = new SuccessorArgumentsDto();
				successor.Node = null;
				successor.Successor = next;
				$(successor).trigger("fetch");
			}
		},
		"KelleyBlueBook.AddOption": function(evt, optionName) {
			var action = new OptionActionDto();
			action.OptionId = optionName;
			action.ActionType = OptionActionTypeDto.Add;
			$(new AdjustedValuationArgumentsDto()).trigger("stateChange", [action]);
		},
		"KelleyBlueBook.RemoveOption": function(evt, optionName) {
			var action = new OptionActionDto();
			action.OptionId = optionName;
			action.ActionType = OptionActionTypeDto.Remove;
			$(new AdjustedValuationArgumentsDto()).trigger("stateChange", [action]);
		},
		"KelleyBlueBook.VinChange": function(evt, newVin, runNodeTemplate) {
			if (State.Vin !== newVin) {
				State.Vin = newVin;
				State.VehicleId = '';

				if (newVin !== '') {
					var traversal = new TraversalArgumentsDto();
					traversal.Vin = State.Vin;
					traversal.RunNodeTemplate = runNodeTemplate;
					$(traversal).trigger("fetch");
				}
			}
		},
		"KelleyBlueBook.VehicleIdChange": function(evt, newVehicleId) {
			if (State.VehicleId !== newVehicleId && newVehicleId !== null) {
				State.VehicleId = newVehicleId;
                // TODO: thinking of setting a default region/zip value here, if one is not set (and removing it from State) - OS
				$(new InitialValuationArgumentsDto()).trigger("stateChange");
			}
		},
        "KelleyBlueBook.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
            State.Vehicle = '';
        },
        "KelleyBlueBook.VehicleChange": function(evt, newVehicle) {
            State.Vehicle = newVehicle;
        },
        "KelleyBlueBook.LoadPublicationList": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publicationList = new PublicationListArgumentsDto();
                publicationList.Broker = State.Broker;
                publicationList.Vehicle = State.Vehicle;
                $(publicationList).trigger("fetch");
            }
        },
        "KelleyBlueBook.LoadPublicationOnDate": function(evt, theDate) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationOnDateArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.On = theDate;
                $(publication).trigger("fetch");
            }
        },
        "KelleyBlueBook.LoadPublication": function(evt, theId) {
            if (State.Broker !== '' && State.Vehicle !== '') {
                var publication = new PublicationLoadArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.Id = theId;
                $(publication).trigger("fetch");
            }
        },
        "KelleyBlueBook.SavePublication": function(evt) {
            if (State.Broker !== '' && State.Vehicle !== '' && State.VehicleConfiguration !== null) {
                var publication = new PublicationSaveArgumentsDto();
                publication.Broker = State.Broker;
                publication.Vehicle = State.Vehicle;
                publication.VehicleConfiguration = State.VehicleConfiguration;
                $(publication).trigger("fetch");
            }
        },
        "KelleyBlueBook.Start": function(evt, values) {
            values.vin = values.vin || State.Vin;
            values.vehicleId = values.vehicleId || State.VehicleId;

            if (values.vin) {
                /// We have a vin so start with the entered vin, PM
                State.Vin = '';
                $(KelleyBlueBook).trigger("KelleyBlueBook.VinChange", values.vin);
            } else if (values.vehicleId) {
                /// Holy shit we have a style already... lets just get that, PM
                $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", values.vehicleId);
            } else {
                /// Well we don't know a damn thing about the vehicle, lets provide the
                /// starting point for a selection, PM
                $(new TraversalArgumentsDto()).trigger("fetch");
            }
        }
	};
	$(KelleyBlueBook).bind(PublicEvents);

	/* ========================== */
	/* = PUBLIC STATIC METHODS = */
	/* ========================== */
	this.FormatPriceTableTypeRow = PriceTableRowTypeDto.toString;
	this.FormatTraversalStatus = TraversalStatusDto.toString;
}).apply(KelleyBlueBook);

