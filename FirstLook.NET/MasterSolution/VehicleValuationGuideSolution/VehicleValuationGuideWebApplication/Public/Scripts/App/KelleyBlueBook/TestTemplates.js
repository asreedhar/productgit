(function () {
    this.raises = this.raises || [];
    this.raises.push("KelleyBlueBook.RegionChange");
    this.raises.push("KelleyBlueBook.ZipCodeChange");
    this.raises.push("KelleyBlueBook.MileageChange");
    this.raises.push("KelleyBlueBook.VinChange");
    this.raises.push("KelleyBlueBook.VehicleIdChange");
    this.raises.push("KelleyBlueBook.SelectionChange");
    this.raises.push("KelleyBlueBook.AddOption");
    this.raises.push("KelleyBlueBook.RemoveOption");
    this.raises.push("KelleyBlueBook.LoadPublicationList");
    this.raises.push("KelleyBlueBook.LoadPublicationOnDate");
    this.raises.push("KelleyBlueBook.LoadPublication");
    this.raises.push("KelleyBlueBook.SavePublication");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("KelleyBlueBook.RegionLoaded");
    this.listensTo.push("KelleyBlueBook.RegionsLoaded");
    this.listensTo.push("KelleyBlueBook.TraversalLoaded");
    this.listensTo.push("KelleyBlueBook.SuccessorLoaded");
    this.listensTo.push("KelleyBlueBook.InitialValueLoaded");
    this.listensTo.push("KelleyBlueBook.AdjustedValueLoaded");
    this.listensTo.push("KelleyBlueBook.PublicationListLoaded");
    this.listensTo.push("KelleyBlueBook.PublicationLoaded");
    this.listensTo.push("KelleyBlueBook.PublicationSaved");

    var PublicEvents = {
        "KelleyBlueBook.RegionLoaded": function (evt, region) {
            var template = new AdjustmentTemplateBuilder(null, region.Id);
            template.regionUpdate(region.Id);
        },
        "KelleyBlueBook.RegionsLoaded": function (evt, regions, current_region_id) {
            var template = new AdjustmentTemplateBuilder(regions, current_region_id);
            template.clean();
            template.build();
        },
        "KelleyBlueBook.TraversalLoaded": function (evt, data) {
            var traversalTemplateBuilder = new NodeTemplateBuilder(data.Path, data.Status);
            traversalTemplateBuilder.clean();
            traversalTemplateBuilder.init();
        },
        "KelleyBlueBook.SuccessorLoaded": function (evt, data) {
            var traversalTemplateBuilder = new NodeTemplateBuilder(data.Path, 0);
            traversalTemplateBuilder.clean();
            traversalTemplateBuilder.init();
        },
        "KelleyBlueBook.InitialValueLoaded": function (evt, data) {
            var initialTemplateBuilder = new InitialTemplateBuilder(data);
            initialTemplateBuilder.clean();
            initialTemplateBuilder.init();
        },
        "KelleyBlueBook.AdjustedValueLoaded": function (evt, data) {
            var adjustedTemplateBuilder = new AdjustedTemplateBuilder(data);
            adjustedTemplateBuilder.clean();
            adjustedTemplateBuilder.init();
        },
        "KelleyBlueBook.PublicationListLoaded": function (evt, data) {
            var template = new PublicationListTemplateBuilder(data.Publications); ;
            template.clean();
            template.init();
        },
        "KelleyBlueBook.PublicationLoaded": function (evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        },
        "KelleyBlueBook.PublicationSaved": function (evt, data) {
            var template = new InitialTemplateBuilder(data.Publication);
            template.clean();
            template.init();
        }
    };
    $(KelleyBlueBook).bind(PublicEvents);

    var AdjustmentTemplateBuilder = function (data, regionId) {
        this.data = data;
        this.defaultRegionId = regionId;
        this.$el = $("#adjustments");
        this.$dom = {
            Region: $('#region'),
            ZipCode: $('#zip'),
            Mileage: $('#mileage'),
            // publication api
            Form: $('#subject'),
            Save: $('#save'),
            Broker: $('#broker'),
            Vehicle: $('#vehicle'),
            PublicationDate: $('#publication_date')
        };
    };

    AdjustmentTemplateBuilder.prototype = {
        init: function () {
            if (!!this.data) {
                this.build();
            }
        },
        build: function () {
            var list = this.data;
            var select = this.$dom.Region;

            var prompt = $('<option></option>').val('-1').html('... Please Select ...');
            select.html('');
            select.append(prompt);

            for (var i = 0; i < list.length; i++) {
                var item = list[i];

                if (item.Type == 5) continue;   // Ignore NationalBase, OS

                var region = $('<option></option>').val(item.Id).text(item.Name);
                if (item.Id == this.defaultRegionId) { // Bad Hack... not correct, PM
                    region.attr('selected', 'selected');
                }
                select.append(region);
            }
            select.unbind('change');
            select.bind('change', $.proxy(this.regionChange, this));

            var zip_input = this.$dom.ZipCode;
            zip_input.unbind('change');
            zip_input.bind('change', $.proxy(this.zipChange, this));

            var input = this.$dom.Mileage;
            input.unbind('change');
            input.bind('change', $.proxy(this.mileageChange, this));
            // broker / vehicle
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVehicleOrDate, this));
        },
        clean: function () {
            this.$dom.Region.unbind('change');
            this.$dom.ZipCode.unbind('change');
            this.$dom.Mileage.unbind('change');
            this.$dom.Form.unbind('click');
        },
        regionChange: function (evt) {
            this.$dom.ZipCode.val('');
            $(KelleyBlueBook).trigger("KelleyBlueBook.RegionChange", [this.$dom.Region.val(), this.$dom.ZipCode.val()]);
        },
        regionUpdate: function (regionId) {
            this.$dom.Region.val(regionId);
            $(KelleyBlueBook).trigger("KelleyBlueBook.RegionChange", [this.$dom.Region.val(), this.$dom.ZipCode.val()]);
        },
        zipChange: function (evt) {
            $(KelleyBlueBook).trigger("KelleyBlueBook.ZipCodeChange", this.$dom.ZipCode.val());
        },
        mileageChange: function (evt) {
            $(KelleyBlueBook).trigger("KelleyBlueBook.MileageChange", this.$dom.Mileage.val());
        },
        changeBrokerOrVehicleOrDate: function (evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                e = document.getElementById(n.substring(1))
                v = (e == null ? null : e.value),
                c = null;
                if (n == '#broker') {
                    $(KelleyBlueBook).trigger("KelleyBlueBook.BrokerChange", v);
                    this.$dom.Vehicle.val('');
                }
                else
                    if (n == '#vehicle') {
                        $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleChange", v);
                        $(KelleyBlueBook).trigger("KelleyBlueBook.LoadPublicationList");
                    }
                    else
                        if (n == '#publication_date') {
                            $(KelleyBlueBook).trigger("KelleyBlueBook.LoadPublicationOnDate", v);
                        }
                        else
                            if (n == '#save') {
                                $(KelleyBlueBook).trigger("KelleyBlueBook.SavePublication");
                            }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var NodeTemplateBuilder = function (data, status) {
        this.data = data;
        this.status = status;
    };

    NodeTemplateBuilder.prototype = {
        init: function () {
            if (!!this.data) {
                this.build();
            }
        },
        build: function () {
            var bindDropDown = function (s, v, p, items, selected) {
                var prompt = $('<option></option>').val('').html('... Please Select ...');
                s.html('');
                s.append(prompt);
                for (i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    var val = item.State;   // TODO: ?
                    if (item.HasVehicleId == true && item.VehicleId != 0) {
                        val = item.VehicleId;
                    }
                    var opt = $('<option></option>').val(val).html(item.Name);
                    if (item.Id == selected) {
                        opt.attr('selected', 'selected');
                    }
                    s.append(opt);
                }
                if (v == false) {
                    function fetchSuccessor(evt) {
                        var next = s.val();
                        if (next != '') {
                            // empty other drop downs
                            var inputs = ['year', 'make', 'model', 'trim', 'synthetic'].slice(p);
                            for (var i = 0; i < inputs.length; i++) {
                                document.getElementById(inputs[i]).options.length = 0;
                            }
                            // reset vin
                            if (inputs.length > 0) {
                                $(KelleyBlueBook).trigger("KelleyBlueBook.VinChange", '');
                                $('#vin').val('');
                            }
                            // request data
                            $(KelleyBlueBook).trigger("KelleyBlueBook.SelectionChange", [next]);
                        }
                    }
                    s.unbind();
                    s.bind('change', fetchSuccessor);
                }
                else {
                    function fetchInitialValuation(evt) {
                        $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", s.val());
                    }
                    s.unbind();
                    s.bind('change', fetchInitialValuation);
                }
            };
            var bindQueryByVin = function () {
                var decode = $('#KelleyBlueBookForm');
                var vin = $('#vin');
                var decoder = function (evt) {
                    // do not submit
                    evt.preventDefault();
                    // request data
                    $(KelleyBlueBook).trigger("KelleyBlueBook.VinChange", [vin.val(), true]);
                };
                decode.unbind('submit', decoder);
                decode.bind('submit', decoder);
            };
            var bindNode = function (node, successors, selected) {
                var valuation = successors.length > 0 && !!successors[0].VehicleId;
                var positions = {
                    'Year': 2,
                    'Make': 3,
                    'Model': 4,
                    'Trim': 5
                };
                var position = positions[node.Label] || 1;
                switch (node.Label) {
                    case '':
                        bindQueryByVin();
                        bindDropDown($("#year"), valuation, position, successors, selected);
                        break;
                    case 'Year':
                        bindDropDown($("#make"), valuation, position, successors, selected);
                        break;
                    case 'Make':
                        bindDropDown($("#model"), valuation, position, successors, selected);
                        break;
                    case 'Model':
                        bindDropDown($("#trim"), valuation, position, successors, selected);
                        break;
                    case 'Vin':
                        if (node.HasVehicleId == true && node.VehicleId != 0) {
                            $(KelleyBlueBook).trigger("KelleyBlueBook.VehicleIdChange", node.VehicleId);
                            selected = successors[0].Id;
                        }
                        position = positions[node.Parent.Label];
                        if (node.Parent.Label == 'Model') {
                            bindDropDown($("#trim"), valuation, position, successors, selected);
                        }
                        else {
                            var x = [];
                            for (var j = 0; j < successors.length; j++) {
                                var s = successors[j];
                                var i = s.Id;
                                var u = s.VehicleId;
                                var t = [];
                                while (s != null) {
                                    t.push(s.Name);
                                    s = s.Parent;
                                }
                                var n = t.reverse().join(' ');
                                x.push({
                                    'VehicleId': u,
                                    'HasVehicleId': true,
                                    'Id': i,
                                    'Name': n,
                                    'State': u,     // TODO: ?
                                    'Label': 'Synthetic'
                                });
                            }
                            bindDropDown($("#synthetic"), valuation, position, x);
                        }
                        break;
                }
            };
            var bindNodeRecursive = function (node, successors, selected) {
                if (node.Parent != null) {
                    bindNodeRecursive(node.Parent, node.Parent.Children, node.Id);
                }
                bindNode(node, successors, selected);
            };
            if (this.status == 0) {
                // structure: '' > 'year' > 'make' > 'model' [> 'vin'] > 'trim'
                if (this.data.CurrentNode.Label == 'Vin') {
                    bindNodeRecursive(this.data.CurrentNode, this.data.Successors);
                }
                else {
                    bindNode(this.data.CurrentNode, this.data.Successors);
                }
                $("#traversal_status").html('');
            }
            else {
                $("#traversal_status").html(KelleyBlueBook.FormatTraversalStatus(this.status));
            }
        },
        clean: function () {
            $("fieldset").find("div").empty();
        }
    };

    var InitialTemplateBuilder = function (data) {
        this.data = data;
    };

    InitialTemplateBuilder.prototype = {
        init: function () {
            if (!!this.data) {
                this.build();
            }
        },
        build: function () {
            this._options();
            this._tables();
            if (this.data.Specifications && this.data.Specifications.length > 0) {
                this._specifications();
            }
        },
        clean: function () { },
        _options: function () {
            var info = $("#options");
            info.find("div").empty(); // clean up from last time
            var optionSelected = function (evt) {
                var tgt = evt.target;
                if (tgt) {
                    var nm, ck;
                    if (tgt.nodeName == 'SELECT') {
                        nm = tgt.options[tgt.selectedIndex].value;
                        ck = true;
                    }
                    else {
                        nm = tgt.name;
                        ck = tgt.checked;
                    }
                    $(KelleyBlueBook).trigger(ck ? "KelleyBlueBook.AddOption" : "KelleyBlueBook.RemoveOption", nm);
                }
            };

            var optionList = this.data.Options;

            // equipment
            var hs = $("<div></div>");
            $(info).append(hs);

            var renderDropDown = function (el_pt, el_id, op_ty, op_ct, op_st, op_ch) {
                var hd = $('<div></div>');
                var lb = $('<label for="' + el_id + '">' + el_id + '</label>');
                var sl = $('<select id="' + el_id + '" name="' + el_id + '"></select>');
                for (var i = 0; i < optionList.length; i++) {
                    var option = optionList[i];
                    if (option.OptionType != op_ty) {
                        continue;
                    }
                    if (option.OptionCategory != op_ct) {
                        continue;
                    }
                    var op = $('<option></option>').val(option.Id).html(option.Name);
                    var state = jQuery.grep(op_st, function (v) {
                        return v.OptionId == option.Id;
                    });
                    if (state.length == 1 && state[0].Selected) {
                        op.attr('selected', 'selected');
                    }
                    sl.append(op);
                }
                $(sl).bind('change', op_ch);
                $(hd).append(lb);
                $(hd).append(sl);
                $(el_pt).append(hd);
            };

            renderDropDown(hs, 'drive_train', 4, 1, this.data.VehicleConfiguration.OptionStates, optionSelected);
            renderDropDown(hs, 'engine', 4, 2, this.data.VehicleConfiguration.OptionStates, optionSelected);
            renderDropDown(hs, 'transmission', 4, 3, this.data.VehicleConfiguration.OptionStates, optionSelected);

            // CPO
            for (var i = 0; i < optionList.length; i++) {
                var option = optionList[i];
                if (option.OptionType != 7) {
                    continue;
                }
                var ck = $('<input type="checkbox" />').val(option.Id);
                ck.attr('name', option.Id);
                var lb = $('<label for="certified_pre_owned">CPO</label>');
                lb.append(ck);
                hs.append(lb);
                ck.bind('change', optionSelected);
            }

            // equipment
            var hd = $("<div></div>");
            $(info).append(hd);

            var th = '<thead><tr><th></th><th>Option</th><th></th><th>Option</th><th></th><th>Option</th></tr></thead>';
            var tb = '<tbody><tr>';
            for (var i = 0; i < optionList.length; i++) {
                var option = optionList[i];
                if (option.OptionType != 5) { // FIXME: Expose ENUM OptionTypeDto.Option
                    continue;
                }
                var state = jQuery.grep(
                this.data.VehicleConfiguration.OptionStates, function (v) {
                    return v.OptionId == option.Id;
                });
                var attr = '';
                if (state.length == 1 && state[0].Selected) {
                    attr = 'checked="checked"';
                }
                var tr = '<td><input type="checkbox" name="' + option.Id + '" id="' + option.Id + '" ' + attr + ' /></td>' + '<td>' + option.Name + '</td>';
                if (i > 0 && (i + 1) % 3 == 0) {
                    tr += '</tr><tr>';
                }
                tb += tr;
            }
            tb += '</tr></tbody>';
            $(hd).html('<table>' + th + tb + '</table>');

            $(hd).bind('change', optionSelected);
        },
        _tables: function () {
            var info = $("#prices");
            // clean up from last time ...
            info.find("div").empty();
            // five price tables ...
            $(info).append(this._table('Wholesale Lending', this.data.Tables.WholesaleLending));
            $(info).append(this._table('Suggested Retail', this.data.Tables.SuggestedRetail));
            $(info).append(this._table('Trade-In', this.data.Tables.TradeIn));
            $(info).append(this._table('Private Party', this.data.Tables.PrivateParty));
            $(info).append(this._table('Auction', this.data.Tables.Auction));
        },
        _table: function (name, table) {
            var hd = $("<div></div>");
            var th = '<thead><tr><th></th><th>Excellent</th><th>Very Good</th><th>Good</th><th>Fair</th></tr></thead>';
            var tb = '<tbody>';
            var cp = '<caption>' + name + '</caption>';
            for (var i = 0; i < table.Rows.length; i++) {
                var tableRow = table.Rows[i];
                var tableRowType = KelleyBlueBook.FormatPriceTableTypeRow(tableRow.RowType);
                var px = name.toLowerCase().replace(" ", "_") + '_' + tableRowType.toLowerCase();
                var tr = '<tr>';
                tr += '<td>' + KelleyBlueBook.FormatPriceTableTypeRow(tableRow.RowType) + '</td>';
                tr += '<td id="' + px + '_excellent">' + tableRow.Prices.Excellent + '</td>';
                tr += '<td id="' + px + '_very_good">' + tableRow.Prices.VeryGood + '</td>';
                tr += '<td id="' + px + '_good">' + tableRow.Prices.Good + '</td>';
                tr += '<td id="' + px + '_fair">' + tableRow.Prices.Fair + '</td>';

                tb += tr;
            }
            tb += '</tbody>';
            $(hd).html('<table>' + cp + th + tb + '</table>');
            return hd;
        },
        _specifications: function () {
            var info = $("#specifications");
            // clean up from last time
            info.find("div").empty();
            var hd = $("<div></div>");
            var cps = ["Specifications", "Warranty", "Dimensions", "General", "Safety", "Features", "MpgRatings", "Engine", "CrashTest", "Performance"];
            var tbs = '';
            for (var ctr = 0; ctr < 10; ctr++) {
                var cp = '<caption>' + cps[ctr + 1] + '</caption>';
                var th = '<thead><tr><th></th><th>Name</th><th>Value</th></tr></thead>';
                var tb = '<tbody>';
                var row = 0;
                for (var i = 0; i < this.data.Specifications.length; i++) {
                    var specification = this.data.Specifications[i];
                    if (specification.SpecificationType != ctr) {
                        continue;
                    }
                    else {
                        row++;
                    }
                    var tr = '<tr>' + '<td>' + color.Name + '</td>' + '<td>' + color.Value + '</td>' + '</tr>';
                    tb += tr;
                }
                tb += '</tbody>';
                if (row > 0) {
                    tbs += '<table>' + cp + th + tb + '</table>';
                }
            }
            $(hd).html(tbs);
            $(info).append(hd);
        }
    };

    var AdjustedTemplateBuilder = function (data) {
        this.data = data;
        this.$el = $("#prices");
    };

    AdjustedTemplateBuilder.prototype = {
        init: function () {
            if (!!this.data) {
                this.build();
            }
        },
        build: function () {
            this._tables();
            this._accessories();
        },
        clean: function () {
            this.$el.find('div').html('');
        },
        _accessories: function () {
            var states = this.data.VehicleConfiguration.OptionStates;
            for (var i = 0; i < states.length; i++) {
                var state = states[i],
                el = document.getElementById(state.OptionId);
                $(el).attr('checked', state.Selected);
            }
        }
    };

    AdjustedTemplateBuilder.prototype._tables = InitialTemplateBuilder.prototype._tables;

    AdjustedTemplateBuilder.prototype._table = InitialTemplateBuilder.prototype._table;

    var PublicationListTemplateBuilder = function (data) {
        this.data = data;
        this.$dom = {
            Form: $('#publications'),
            Body: $('#publications_body')
        };
    };

    PublicationListTemplateBuilder.prototype = {
        init: function () {
            if (!!this.data) {
                this.build();
            }
        },
        build: function () {
            this._table();
        },
        clean: function () {
            this.$dom.Body.empty();
            this.$dom.Form.unbind('click');
        },
        _table: function () {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                + '<td>' + item.Edition.BeginDate + '</td>'
                + '<td>' + item.Edition.EndDate + '</td>'
                + '<td>' + item.ChangeType + '</td>'
                + '<td>' + item.ChangeAgent + '</td>'
                + '<td>' + item.Edition.User.UserName + '</td>'
                + '<td><a href="#' + item.Id + '">Load</a></td>'
                + '</tr>';
                code += tr;
            }
            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.selectPublication, this));
        },
        selectPublication: function (evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    p = tgt.href.substring(i + 1);
                $(KelleyBlueBook).trigger("KelleyBlueBook.LoadPublication", p);
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

}).apply(KelleyBlueBook);

