﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    [TestFixture]
    public class NadaCalculatorTest2001ChevroletTruckVenture : NadaCalculatorTest
    {
        private const string FileName = ".2001 CHEVROLET TRUCK Venture-V6 Extended Van Warner Bros.xml";

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            Region region = service.ToRegion(state);

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.AccessoryActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Description = "Aluminum/Alloy Wheels", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Fixed Running Boards"},
                                                     new ExpectedState {Description = "Leather Seats", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "LS Pkg", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "LT Pkg", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Power Seat", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Power Sliding Door", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Rear Air Conditioning", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Rear Bucket Seats", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Towing/Camper Pkg"},
                                                     new ExpectedState {Description = "W/out Cruise Control"},
                                                     new ExpectedState {Description = "W/out Power Windows"}
                                                 };

            IList<Accessory> accessories = service.Accessories(666, region);

            foreach (ExpectedState expectedState in expectedStates)
            {
                string accessoryDescription = expectedState.Description;

                Accessory accessory = accessories.FirstOrDefault(x => Equals(x.Description, accessoryDescription));

                Assert.IsNotNull(accessory, "Missing accessory {0}", accessoryDescription);

                IAccessoryState actualState = initial.AccessoryStates.FirstOrDefault(x => Equals(accessory.Code, x.AccessoryCode));

                Assert.IsNotNull(actualState, "Missing accessory state {0}", accessoryDescription);

                Assert.AreEqual(
                    expectedState.Selected,
                    actualState.Selected,
                    "Unexpected accessory state for {0}",
                    accessoryDescription);
            }
        }


        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                null);

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 3100;
            const int expectedRetail = 5600;
            const int expectedTradeInRough = 2025;
            const int expectedTradeInAverage = 2800;
            const int expectedTradeInClean = 3425;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.IsNull(actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationAboveZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                150000);

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 2525;
            const int expectedRetail = 5025;
            const int expectedTradeInRough = 1450;
            const int expectedTradeInAverage = 2225;
            const int expectedTradeInClean = 2850;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(-575, actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationBelowZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                80000);

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 4350;
            const int expectedRetail = 6850;
            const int expectedTradeInRough = 3275;
            const int expectedTradeInAverage = 4050;
            const int expectedTradeInClean = 4675;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(1250m, actualAdjustment);
            }
        }
    }
}
