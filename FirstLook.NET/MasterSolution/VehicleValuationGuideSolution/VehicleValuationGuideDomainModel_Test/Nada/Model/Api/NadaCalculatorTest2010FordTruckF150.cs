﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    [TestFixture]
    public class NadaCalculatorTest2010FordTruckF150 : NadaCalculatorTest
    {
        private const string FileName = ".2010 FORD TRUCK F150 Supercab-V8 SuperCab Raptor 4WD.xml";

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            Region region = service.ToRegion(state);

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.AccessoryActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Description = "6.2L V8 Engine"},
                                                     new ExpectedState {Description = "Aluminum/Alloy Wheels", Selected = true},
                                                     new ExpectedState {Description = "Auxiliary Fuel Tank"},
                                                     new ExpectedState {Description = "Bed Liner"},
                                                     new ExpectedState {Description = "Fiberglass Cap"},
                                                     new ExpectedState {Description = "Fixed Running Boards"},
                                                     new ExpectedState {Description = "Leather Seats", Selected = true},
                                                     new ExpectedState {Description = "Navigation System"},
                                                     new ExpectedState {Description = "Power Seat", Selected = true},
                                                     new ExpectedState {Description = "Power Sunroof"},
                                                     new ExpectedState {Description = "Rear Entertainment System"},
                                                     new ExpectedState {Description = "Roll Bar"},
                                                     new ExpectedState {Description = "Snow Plow Pkg./Plow"},
                                                     new ExpectedState {Description = "Sony Stereo System"},
                                                     new ExpectedState {Description = "Theft Recovery Sys"},
                                                     new ExpectedState {Description = "Towing/Camper Pkg"},
                                                     new ExpectedState {Description = "W/out Air Condition"},
                                                     new ExpectedState {Description = "W/out Auto. Trans."},
                                                     new ExpectedState {Description = "W/out Cruise Control"},
                                                     new ExpectedState {Description = "W/out Power Windows"},
                                                     new ExpectedState {Description = "W/out Pwr Door Locks"},
                                                     new ExpectedState {Description = "Winch"}
                                                 };

            IList<Accessory> accessories = service.Accessories(666, region);

            foreach (ExpectedState expectedState in expectedStates)
            {
                string accessoryDescription = expectedState.Description;

                Accessory accessory = accessories.FirstOrDefault(x => Equals(x.Description, accessoryDescription));

                Assert.IsNotNull(accessory, "Missing accessory {0}", accessoryDescription);

                IAccessoryState actualState = initial.AccessoryStates.FirstOrDefault(x => Equals(accessory.Code, x.AccessoryCode));

                Assert.IsNotNull(actualState, "Missing accessory state {0}", accessoryDescription);

                Assert.AreEqual(
                    expectedState.Selected,
                    actualState.Selected,
                    "Unexpected accessory state for {0}",
                    accessoryDescription);
            }
        }


        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                null);

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 31750;
            const int expectedRetail = 39500;
            const int expectedTradeInRough = 31900;
            const int expectedTradeInAverage = 33750;
            const int expectedTradeInClean = 35275;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.IsNull(actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationAboveZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                50000);

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 29350;
            const int expectedRetail = 37100;
            const int expectedTradeInRough = 29500;
            const int expectedTradeInAverage = 31350;
            const int expectedTradeInClean = 32875;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(-2400m, actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationBelowZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                200);

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 33275;
            const int expectedRetail = 41025;
            const int expectedTradeInRough = 33425;
            const int expectedTradeInAverage = 35275;
            const int expectedTradeInClean = 36800;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(1525m, actualAdjustment);
            }
        }

        [Test]
        public void UpdatedConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            Region region = service.ToRegion(state);

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                17500);

            IList<Accessory> accessories = service.Accessories(666, region);

            string[] selectedDescriptions = new[] { "6.2L V8 Engine", "Fiberglass Cap", "W/out Auto. Trans." };

            foreach (string selectedDescription in selectedDescriptions)
            {
                string description = selectedDescription; // no warnings plz

                Accessory accessory = accessories.First(x => Equals(x.Description, description));

                initial = configuration.UpdateConfiguration(
                    initial,
                    new AccessoryAction
                    {
                        ActionType = AccessoryActionType.Add,
                        AccessoryCode = accessory.Code
                    });
            }

            // three actions

            Assert.AreEqual(3, initial.AccessoryActions.Count);

            // get a calculator

            INadaCalculator calculator = Resolver.Resolve<INadaCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedLoan = 34400;
            const int expectedRetail = 42550;
            const int expectedTradeInRough = 34550;
            const int expectedTradeInAverage = 36400;
            const int expectedTradeInClean = 37925;

            decimal? actualLoan = matrix[ValueType.Loan, Valuation.Final].Value;
            decimal? actualRetail = matrix[ValueType.Retail, Valuation.Final].Value;
            decimal? actualTradeInRough = matrix[ValueType.TradeRough, Valuation.Final].Value;
            decimal? actualTradeInAverage = matrix[ValueType.TradeAverage, Valuation.Final].Value;
            decimal? actualTradeInClean = matrix[ValueType.Trade, Valuation.Final].Value;

            Assert.AreEqual(expectedLoan, actualLoan);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInRough, actualTradeInRough);
            Assert.AreEqual(expectedTradeInAverage, actualTradeInAverage);
            Assert.AreEqual(expectedTradeInClean, actualTradeInClean);

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(0m, actualAdjustment);
            }
        }
    }
}