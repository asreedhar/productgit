﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    [TestFixture]
    public class NadaCalculatorTest2006DodgeTruckDakotaPickup : NadaCalculatorTest
    {
        private const string FileName = ".2006 DODGE TRUCK Dakota Pickup-V6 Club Cab SLT.xml";

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            Region region = service.ToRegion(state);

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.AccessoryActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Description = "W/out Auto. Trans."},
                                                     new ExpectedState {Description = "Power Windows", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Power Seat"},
                                                     new ExpectedState {Description = "Power Door Locks", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "4WD or AWD"},
                                                     new ExpectedState {Description = "Winch"},
                                                     new ExpectedState {Description = "Cruise Control", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Leather Seats"},
                                                     new ExpectedState {Description = "Aluminum/Alloy Wheels", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Power Sunroof"},
                                                     new ExpectedState {Description = "Snow Plow Pkg./Plow"},
                                                     new ExpectedState {Description = "Fixed Running Boards"},
                                                     new ExpectedState {Description = "Roll Bar"},
                                                     new ExpectedState {Description = "Bed Liner"},
                                                     new ExpectedState {Description = "Fiberglass Cap"},
                                                     new ExpectedState {Description = "Towing/Camper Pkg"},
                                                     new ExpectedState {Description = "V8 Gas Engine"},
                                                     new ExpectedState {Description = "TRX Package"},
                                                     new ExpectedState {Description = "TRX4 Off-Road Package"},
                                                     new ExpectedState {Description = "Alpine Stereo System"},
                                                     new ExpectedState {Description = "4.7L V8 HO Engine"},
                                                     new ExpectedState {Description = "Night Runner Package"},
                                                     new ExpectedState {Description = "R/T Package"}
                                                 };

            IList<Accessory> accessories = service.Accessories(666, region);

            foreach (ExpectedState expectedState in expectedStates)
            {
                string accessoryDescription = expectedState.Description;

                Accessory accessory = accessories.FirstOrDefault(x => Equals(x.Description, accessoryDescription));

                Assert.IsNotNull(accessory, "Missing accessory {0}", accessoryDescription);

                IAccessoryState actualState = initial.AccessoryStates.FirstOrDefault(x => Equals(accessory.Code, x.AccessoryCode));

                Assert.IsNotNull(actualState, "Missing accessory state {0}", accessoryDescription);

                Assert.AreEqual(
                    expectedState.Selected,
                    actualState.Selected,
                    "Unexpected accessory state for {0}",
                    accessoryDescription);
            }
        }

        [Test]
        public void Exclusion()
        {
            InitializeDataSet(FileName);

            INadaService service = Resolver.Resolve<INadaService>();

            Vehicle vehicle = service.Information(666);

            State state = service.States().First(x => Equals(x.Code, "IL"));

            Region region = service.ToRegion(state);

            INadaConfiguration configuration = Resolver.Resolve<INadaConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Body.Uid,
                null,
                state,
                null);

            // initial selections

            IList<Accessory> accessories = service.Accessories(666, region);

            string[] selectedDescriptions = new[] { "4.7L V8 HO Engine", "V8 Gas Engine" };

            foreach (string selectedDescription in selectedDescriptions)
            {
                string description = selectedDescription; // no warnings plz

                Accessory accessory = accessories.First(x => Equals(x.Description, description));

                initial = configuration.UpdateConfiguration(
                    initial,
                    new AccessoryAction
                        {
                            ActionType = AccessoryActionType.Add,
                            AccessoryCode = accessory.Code
                        });
            }

            // two actions; one remains

            Assert.AreEqual(1, initial.AccessoryActions.Count);

            // expected selections'

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Description = "W/out Auto. Trans."},
                                                     new ExpectedState {Description = "Power Windows", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Power Seat"},
                                                     new ExpectedState {Description = "Power Door Locks", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "4WD or AWD"},
                                                     new ExpectedState {Description = "Winch"},
                                                     new ExpectedState {Description = "Cruise Control", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Leather Seats"},
                                                     new ExpectedState {Description = "Aluminum/Alloy Wheels", Selected = true, Enabled = false},
                                                     new ExpectedState {Description = "Power Sunroof"},
                                                     new ExpectedState {Description = "Snow Plow Pkg./Plow"},
                                                     new ExpectedState {Description = "Fixed Running Boards"},
                                                     new ExpectedState {Description = "Roll Bar"},
                                                     new ExpectedState {Description = "Bed Liner"},
                                                     new ExpectedState {Description = "Fiberglass Cap"},
                                                     new ExpectedState {Description = "Towing/Camper Pkg"},
                                                     new ExpectedState {Description = "V8 Gas Engine", Selected = true},
                                                     new ExpectedState {Description = "TRX Package"},
                                                     new ExpectedState {Description = "TRX4 Off-Road Package"},
                                                     new ExpectedState {Description = "Alpine Stereo System"},
                                                     new ExpectedState {Description = "4.7L V8 HO Engine"},
                                                     new ExpectedState {Description = "Night Runner Package"},
                                                     new ExpectedState {Description = "R/T Package"}
                                                 };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string accessoryDescription = expectedState.Description;

                Accessory accessory = accessories.FirstOrDefault(x => Equals(x.Description, accessoryDescription));

                Assert.IsNotNull(accessory, "Missing accessory {0}", accessoryDescription);

                IAccessoryState actualState =
                    initial.AccessoryStates.FirstOrDefault(x => Equals(accessory.Code, x.AccessoryCode));

                Assert.IsNotNull(actualState, "Missing accessory state {0}", accessoryDescription);

                Assert.AreEqual(
                    expectedState.Selected,
                    actualState.Selected,
                    "Unexpected accessory state for {0}",
                    accessoryDescription);
            }
        }
    }
}
