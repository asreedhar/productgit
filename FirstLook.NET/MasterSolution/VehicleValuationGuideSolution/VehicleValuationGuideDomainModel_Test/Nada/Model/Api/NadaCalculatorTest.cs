﻿using System;
using System.Data;
using System.IO;
using System.Reflection;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;
using NADAVehicle_Data;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public class NadaCalculatorTest
    {
        private IRegistry _registry;

        private IResolver _resolver;

        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api.Resources";

        protected IRegistry Registry
        {
            get { return _registry; }
        }

        protected IResolver Resolver
        {
            get { return _resolver; }
        }

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<Nada.Module>();

            _registry.Register<Lookup.Module>();

            _registry.Register<Sql.Module.SerializerModule>();

            _registry.Register<ISqlService, XmlService>(ImplementationScope.Shared);

            _registry.Register<ICache, NoCache>(ImplementationScope.Shared);

            _resolver = RegistryFactory.GetResolver();
        }

        [TearDown]
        public void TearDown()
        {
            _resolver.Dispose();

            _resolver = null;

            _registry.Dispose();

            _registry = null;
        }

        protected void InitializeDataSet(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            XmlService xml = (XmlService)Resolver.Resolve<ISqlService>();

            xml.Set = set;
        }

        protected static Stream GetResourceFromAssembly(string resourceName)
        {
            Assembly a = Assembly.GetExecutingAssembly();

            return a.GetManifestResourceStream(resourceName);
        }

        protected class ExpectedState
        {
            private string _description;
            private bool _selected;
            private bool _enabled;

            public string Description
            {
                get { return _description; }
                set { _description = value; }
            }

            public bool Enabled
            {
                get { return _enabled; }
                set { _enabled = value; }
            }

            public bool Selected
            {
                get { return _selected; }
                set { _selected = value; }
            }
        }

        protected class XmlService : ISqlService
        {
            private DataSet _set;

            public DataSet Set
            {
                get
                {
                    if (_set == null)
                    {
                        throw new NotSupportedException("Not configured");
                    }
                    return _set;
                }
                set { _set = value; }
            }

            public IDataReader Query()
            {
                return _set.Tables["Period"].CreateDataReader();
            }

            public IDataReader Query(int period)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int period, int year)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int period, int year, int make)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int period, int year, int make, int series)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int period, string vin)
            {
                throw new NotSupportedException();
            }

            public IDataReader States(int period)
            {
                return _set.Tables["States"].CreateDataReader();
            }

            public IDataReader Region(int period, string stateName)
            {
                string filter = string.Format("{0} = '{1}'", VehicleUI_Data.REGIONSTATE_DESCR_FIELD, stateName);

                DataTable table = _set.Tables[VehicleUI_Data.REGIONSTATES_TABLE];

                DataView view = new DataView(table) { RowFilter = filter };

                return view.ToTable(true).CreateDataReader();
            }

            public IDataReader Vehicle(int period, int uid)
            {
                return _set.Tables[VehicleUI_Data.VEHICLES_TABLE].CreateDataReader();
            }

            public IDataReader VehicleValues(int period, int uid, Region region)
            {
                return _set.Tables[VehicleUI_Data.VEHICLE_VALUES_TABLE].CreateDataReader();
            }

            public IDataReader Accessories(int period, int uid, Region region, string vin)
            {
                throw new NotSupportedException();
            }

            public IDataReader Accessories(int period, int uid, Region region)
            {
                return _set.Tables[VehicleUI_Data.ACCESSORIES_TABLE].CreateDataReader();
            }

            public IDataReader AccessoryExcludes(int period, int uid)
            {
                return _set.Tables[VehicleUI_Data.EXCLUSIVE_ACCESSORIES_TABLE].CreateDataReader();
            }

            public IDataReader AccessoryIncludes(int period, int uid)
            {
                return _set.Tables[VehicleUI_Data.INCLUSIVE_ACCESSORIES_TABLE].CreateDataReader();
            }

            public IDataReader MileageAdjustment(int period, int uid, Region region, int mileage)
            {
                // create the correct data type

                VehicleUI_Data data = new VehicleUI_Data();

                for (int i = 0, l = _set.Tables.Count; i < l; i++)
                {
                    DataTable dataTable = _set.Tables[i];

                    if (data.Tables.Contains(dataTable.TableName))
                    {
                        data.Tables[dataTable.TableName].Load(dataTable.CreateDataReader());
                    }
                    else
                    {
                        data.Tables.Add(dataTable.Copy());
                    }
                }

                // get the adjustment ...

                decimal mileageAdj = NADAVehicle_System.Vehicle.getMileageAdj(data, mileage);

                // build the result

                DataTable table = new DataTable("MileageAdjustment");

                DataColumnCollection columns = table.Columns;

                columns.Add(VehicleUI_Data.ADJUSTMENT_FIELD, typeof(decimal));

                DataRow row = table.NewRow();

                row[VehicleUI_Data.ADJUSTMENT_FIELD] = mileageAdj;

                table.Rows.Add(row);

                return new DataTableReader(table);
            }
        }

        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }
    }
}