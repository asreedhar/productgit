﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    [Serializable]
    public class GalvesCalculatorTest2008BuickLaCrossCxSedan : GalvesCalculatorTest
    {
        private const string FileName = ".2008 BUICK LACROSSE CX 4 DOOR SEDAN 3_8L V6.xml";

        private const int VehicleId = 102406;

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.AdjustmentActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Name = "ABS STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "ADD: MOON ROOF (POWER)"},
                                                     new ExpectedState {Name = "DUAL ZONE AUTOMATIC AC STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "POWER SEAT STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "POWER WINDOWS STD", Selected = true, Enabled = false}
                                                 };

            IList<Adjustment> adjustments = service.Adjustments(VehicleId);

            foreach (ExpectedState expectedState in expectedStates)
            {
                string adjustmentName = expectedState.Name;

                Adjustment accessory = adjustments.FirstOrDefault(x => Equals(x.Name, adjustmentName));

                Assert.IsNotNull(accessory, "Missing accessory {0}", adjustmentName);

                IAdjustmentState actualState = initial.AdjustmentStates.FirstOrDefault(x => Equals(accessory.Name, x.AdjustmentName));

                Assert.IsNotNull(actualState, "Missing accessory state {0}", adjustmentName);

                Assert.AreEqual(
                    expectedState.Selected,
                    actualState.Selected,
                    "Unexpected accessory state for {0}",
                    adjustmentName);
            }
        }


        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                null);

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 10900;
            const int expectedRetail = 12000;
            
            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.IsNull(actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationBelowZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                15000);

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 13150;
            const int expectedRetail = 14250;

            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(2250.0m, actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationAboveZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                82000);

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 7350;
            const int expectedRetail = 8450;

            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(-3550.0m, actualAdjustment);
            }
        }
    }
}
