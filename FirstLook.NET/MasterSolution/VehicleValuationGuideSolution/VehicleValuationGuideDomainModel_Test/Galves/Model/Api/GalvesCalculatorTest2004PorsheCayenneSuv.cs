﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    [Serializable]
    public class GalvesCalculatorTest2004PorsheCayenneSuv : GalvesCalculatorTest
    {
        private const string FileName = ".2004 PORSCHE CAYENNE S 4 DOOR SUV 3_2L V6.xml";

        private const int VehicleId = 303203;

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.AdjustmentActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Name = "17\" WHEELS STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "AC STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "ADD: 18\" WHEELS"},
                                                     new ExpectedState {Name = "ADD: 19\" WHEELS"},
                                                     new ExpectedState {Name = "ADD: 20\" WHEELS"},
                                                     new ExpectedState {Name = "ADD: AIR SUSPENSION"},
                                                     new ExpectedState {Name = "ADD: NAVIGATION"},
                                                     new ExpectedState {Name = "ALL WHEEL DRIVE STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "DED: W/O MOON ROOF"},
                                                     new ExpectedState {Name = "DUAL POWER SEATS STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "LEATHER STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "POWER WINDOWS STD", Selected = true, Enabled = false},
                                                     new ExpectedState {Name = "STABILITY STD", Selected = true, Enabled = false},
                                                 };

            IList<Adjustment> adjustments = service.Adjustments(VehicleId);

            foreach (ExpectedState expectedState in expectedStates)
            {
                string adjustmentName = expectedState.Name;

                Adjustment accessory = adjustments.FirstOrDefault(x => Equals(x.Name, adjustmentName));

                Assert.IsNotNull(accessory, "Missing accessory {0}", adjustmentName);

                IAdjustmentState actualState = initial.AdjustmentStates.FirstOrDefault(x => Equals(accessory.Name, x.AdjustmentName));

                Assert.IsNotNull(actualState, "Missing accessory state {0}", adjustmentName);

                Assert.AreEqual(
                    expectedState.Selected,
                    actualState.Selected,
                    "Unexpected accessory state for {0}",
                    adjustmentName);
            }
        }


        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                null);

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 15700;
            const int expectedRetail = 17000;
            
            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.IsNull(actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationBelowZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                53000);

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 18025;
            const int expectedRetail = 19325;

            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(2325.0m, actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationAboveZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                113000);

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 11725;
            const int expectedRetail = 13025;

            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(-3975.0m, actualAdjustment);
            }
        }


        [Test]
        public void UpdatedConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IGalvesService service = Resolver.Resolve<IGalvesService>();

            Vehicle vehicle = service.Vehicle(VehicleId);

            State state = service.States().First(x => Equals(x.Name, "Illinois"));

            IGalvesConfiguration configuration = Resolver.Resolve<IGalvesConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                state,
                null);

            IList<Adjustment> accessories = service.Adjustments(VehicleId);

            string[] selectedNames = new[] { "18\" WHEELS", "19\" WHEELS", "20\" WHEELS" };

            foreach (string selectedDescription in selectedNames)
            {
                string name = selectedDescription; // no warnings plz

                Adjustment accessory = accessories.First(x => x.Name.EndsWith(name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new AdjustmentAction
                    {
                        ActionType = AdjustmentActionType.Add,
                        AdjustmentName = accessory.Name
                    });
            }

            // three actions

            Assert.AreEqual(1, initial.AdjustmentActions.Count);

            // get a calculator

            IGalvesCalculator calculator = Resolver.Resolve<IGalvesCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedTradeIn = 16500;
            const int expectedRetail = 17800;

            decimal? actualTradeIn = matrix[PriceType.TradeIn, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;

            Assert.AreEqual(expectedTradeIn, actualTradeIn);
            Assert.AreEqual(expectedRetail, actualRetail);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.IsNull(actualAdjustment);
            }
        }
    }
}
