﻿using System;
using System.Data;
using System.IO;
using System.Reflection;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public class GalvesCalculatorTest
    {
        private IRegistry _registry;

        private IResolver _resolver;

        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api.Resources";

        protected IRegistry Registry
        {
            get { return _registry; }
        }

        protected IResolver Resolver
        {
            get { return _resolver; }
        }

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<Module>();

            _registry.Register<Lookup.Module>();

            _registry.Register<Sql.Module.SerializerModule>();

            _registry.Register<ISqlService, XmlService>(ImplementationScope.Shared);

            _registry.Register<ICache, NoCache>(ImplementationScope.Shared);

            _registry.Register<IVehicleConfigurationBuilderFactory, VehicleConfigurationBuilderFactory>(ImplementationScope.Shared);

            _resolver = RegistryFactory.GetResolver();
        }

        [TearDown]
        public void TearDown()
        {
            _resolver.Dispose();

            _resolver = null;

            _registry.Dispose();

            _registry = null;
        }

        protected void InitializeDataSet(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            XmlService xml = (XmlService)Resolver.Resolve<ISqlService>();

            xml.Set = set;
        }

        protected static Stream GetResourceFromAssembly(string resourceName)
        {
            Assembly a = Assembly.GetExecutingAssembly();

            return a.GetManifestResourceStream(resourceName);
        }

        protected class ExpectedState
        {
            private string _name;
            private bool _selected;
            private bool _enabled = true;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public bool Enabled
            {
                get { return _enabled; }
                set { _enabled = value; }
            }

            public bool Selected
            {
                get { return _selected; }
                set { _selected = value; }
            }
        }

        protected class XmlService : ISqlService
        {
            private DataSet _set;

            public DataSet Set
            {
                get
                {
                    if (_set == null)
                    {
                        throw new NotSupportedException("Not configured");
                    }
                    return _set;
                }
                set { _set = value; }
            }
            
            public IDataReader BookDate()
            {
                return Set.Tables["BookDate"].CreateDataReader();
            }

            public IDataReader Query(int bookDate)
            {
                return Set.Tables["Year"].CreateDataReader();
            }

            public IDataReader Query(int bookDate, int year)
            {
                return Set.Tables["Manufacturer"].CreateDataReader();
            }

            public IDataReader Query(int bookDate, int year, string manufacturer)
            {
                return Set.Tables["Model"].CreateDataReader();
            }

            public IDataReader Query(int bookDate, int year, string manufacturer, string model)
            {
                return Set.Tables["Style"].CreateDataReader();
            }

            public IDataReader Query(int bookDate, string vin)
            {
                return Set.Tables["Vin"].CreateDataReader();
            }

            public IDataReader Vehicle(int bookDate, int vehicleId)
            {
                return Set.Tables["Vehicle"].CreateDataReader();
            }

            public IDataReader States(int bookDate)
            {
                return Set.Tables["States"].CreateDataReader();
            }

            public IDataReader Adjustments(int bookDate, int vehicleId)
            {
                return Set.Tables["Adjustments"].CreateDataReader();
            }

            public IDataReader AdjustmentExceptions(int bookDate, int vehicleId)
            {
                return Set.Tables["AdjustmentExceptions"].CreateDataReader();
            }

            public IDataReader MileageAdjustments(int bookDate, string mileageYear, string mileageGroupCode)
            {
                return Set.Tables["MileageAdjustments"].CreateDataReader();
            }

            public IDataReader RegionalMileageAdjustments(int bookDate, string mileageYear, MileageClassification mileageClassification)
            {
                return Set.Tables["RegionalMileageAdjustments"].CreateDataReader();
            }
        }

        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }
    }
}