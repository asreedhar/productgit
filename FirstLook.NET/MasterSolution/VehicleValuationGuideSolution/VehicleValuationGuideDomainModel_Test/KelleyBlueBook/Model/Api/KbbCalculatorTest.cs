using System;
using System.Data;
using System.IO;
using System.Reflection;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public class KbbCalculatorTest
    {
        private IRegistry _registry;

        private IResolver _resolver;

        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api.Resources";

        protected IRegistry Registry
        {
            get { return _registry; }
        }

        protected IResolver Resolver
        {
            get { return _resolver; }
        }

        public enum TableName
        {
            BookDate,
            DefaultOption,
            Year,
            Make,
            Model,
            Trim,
            Vehicle,
            VehicleOption,
            VehicleOptionRelationship,
            Specification,
            Region,
            Regions,
            RegionAdjustment,
            BasePrice,
            OptionAdjustment,
            MileageAdjustment,
            PriceTypes
        }

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<Module>();

            _registry.Register<Lookup.Module>();

            _registry.Register<Sql.Module.SerializerModule>();

            _registry.Register<ISqlService, XmlService>(ImplementationScope.Shared);

            _registry.Register<IVehicleConfigurationBuilderFactory, VehicleConfigurationBuilderFactory>(ImplementationScope.Shared);

            _registry.Register<ICache, NoCache>(ImplementationScope.Shared);

            _resolver = RegistryFactory.GetResolver();
        }

        [TearDown]
        public void TearDown()
        {
            _resolver.Dispose();

            _resolver = null;

            _registry.Dispose();

            _registry = null;
        }

        protected void InitializeDataSet(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            XmlService xml = (XmlService)Resolver.Resolve<ISqlService>();

            xml.Set = set;
        }

        protected static Stream GetResourceFromAssembly(string resourceName)
        {
            Assembly a = Assembly.GetExecutingAssembly();

            return a.GetManifestResourceStream(resourceName);
        }

        protected class ExpectedState
        {
            private string _name;
            private bool _selected;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public bool Selected
            {
                get { return _selected; }
                set { _selected = value; }
            }
        }

        protected class XmlService : ISqlService
        {
            private DataSet _set;

            public DataSet Set
            {
                get
                {
                    if (_set == null)
                    {
                        throw new NotSupportedException("Not configured");
                    }
                    return _set;
                }
                set { _set = value; }
            }

            public IDataReader Query(int bookDate)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int bookDate, int year)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int bookDate, int year, int make)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int bookDate, int year, int make, int model)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int bookDate, string vin)
            {
                throw new NotSupportedException();
            }

            public IDataReader Vehicle(int bookDate, int vehicle)
            {
                DataTable[] tables = new[]
                                         {
                                             Set.Tables[TableName.Year.ToString()],
                                             Set.Tables[TableName.Make.ToString()],
                                             Set.Tables[TableName.Model.ToString()],
                                             Set.Tables[TableName.Trim.ToString()],
                                             Set.Tables[TableName.Vehicle.ToString()],
                                             Set.Tables[TableName.VehicleOption.ToString()],
                                             Set.Tables[TableName.VehicleOptionRelationship.ToString()],
                                             Set.Tables[TableName.Specification.ToString()]
                                         };

                return Set.CreateDataReader(tables);
            }

            public IDataReader BookDate()
            {
                return Set.Tables[TableName.BookDate.ToString()].CreateDataReader();
            }

            public IDataReader DefaultOptions(int bookDate, int vehicle, string vin)
            {
                return Set.Tables[TableName.DefaultOption.ToString()].CreateDataReader();
            }

            public IDataReader Region(int bookDate, string zipCode)
            {
                return Set.Tables[TableName.Region.ToString()].CreateDataReader();
            }

            public IDataReader Regions(int bookDate)
            {
                return Set.Tables[TableName.Regions.ToString()].CreateDataReader();
            }

            public IDataReader RegionAdjustments(int bookDate, int vehicle)
            {
                return Set.Tables[TableName.RegionAdjustment.ToString()].CreateDataReader();
            }

            public IDataReader BasePrices(int bookDate, Region region, int vehicle)
            {
                return Set.Tables[TableName.BasePrice.ToString()].CreateDataReader();
            }

            public IDataReader BasePrices(int bookDate, Region region, int vehicle, int? mileage, int dealerId, int year, int bookOutTypeId)
            {
                return Set.Tables[TableName.BasePrice.ToString()].CreateDataReader();
            }

            public IDataReader OptionAdjustments(int bookDate, int vehicle)
            {
                return Set.Tables[TableName.OptionAdjustment.ToString()].CreateDataReader();
            }

            public IDataReader MileageAdjustment(int bookDate, VehicleType vehicleType, int yearId, int mileageGroupId)
            {
                return Set.Tables[TableName.MileageAdjustment.ToString()].CreateDataReader();
            }

            public IDataReader PriceType(int dataLoadId)
            {
                return Set.Tables[TableName.PriceTypes.ToString()].CreateDataReader();
            }

            #region VINSpecificOptions
            public IDataReader GetOptions(int vehicle, string vin, int businessUnitId)
            {
                return Set.Tables[TableName.DefaultOption.ToString()].CreateDataReader();
            }
            #endregion
        }

        protected class OptionAction : IOptionAction
        {
            public int OptionId { get; set; }

            public OptionActionType ActionType { get; set; }
        }

        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }

        protected void TestReadXml(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }
        }

        protected void TestTableNames(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            string[] tableNames = new[]
                                      {
                                        TableName.Year.ToString(), 
                                        TableName.Make.ToString(), 
                                        TableName.Model.ToString(), 
                                        TableName.Trim.ToString(),
                                        TableName.Vehicle.ToString(),
                                        TableName.VehicleOption.ToString(),
                                        TableName.VehicleOptionRelationship.ToString(),
                                        TableName.Specification.ToString(),
                                        TableName.Region.ToString(),
                                        TableName.Regions.ToString(),
                                        TableName.RegionAdjustment.ToString(),
                                        TableName.BasePrice.ToString(),
                                        TableName.OptionAdjustment.ToString(),
                                        TableName.MileageAdjustment.ToString()
                                      };

            foreach (string tableName in tableNames)
            {
                Assert.IsNotNull(set.Tables[tableName], tableName);
            }
        }
    }
}