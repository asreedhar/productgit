using System;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    [TestFixture]
    public class CalculatorTest2003FordF250SuperDutyCrewCabXltPickup4D8Ft : KbbCalculatorTest
    {
        private const string FileName = ".2003 Ford F250 Super Duty Crew Cab XLT Pickup 4D 8 ft.xml";

        [Test]
        public void TestReadXml()
        {
            TestReadXml(FileName);
        }

        [Test]
        public void TestTableNames()
        {
            TestTableNames(FileName);
        }

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            string zipCode = "48170";

            Region region = service.Regions().First(x => x.Id == (int)Helpers.Region.Michigan);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                string.Empty,
                region,
                zipCode,
                null);

            string[] selectedOptionNames = new[] { "V10, 6.8 Liter", "Automatic", "4WD" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            // (no) initial actions

            Assert.AreEqual(3, initial.OptionActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     // equipment
                                                     new ExpectedState {Name = "V10, 6.8 Liter", Selected = true},
                                                     new ExpectedState {Name = "V8, 5.4 Liter"},
                                                     new ExpectedState {Name = "V8, Turbo Diesel, 6.0L"},
                                                     new ExpectedState {Name = "V8, Turbo Diesel, 7.3L"},
                                                     new ExpectedState {Name = "Automatic", Selected = true},
                                                     new ExpectedState {Name = "Manual, 6-Spd"},
                                                     new ExpectedState {Name = "2WD"},
                                                     new ExpectedState {Name = "4WD", Selected = true},
                                                     // options
                                                     new ExpectedState {Name = "Off-Road Pkg"},
                                                     new ExpectedState {Name = "Sport Pkg"},
                                                     new ExpectedState {Name = "Air Conditioning", Selected = true},
                                                     new ExpectedState {Name = "Power Steering", Selected = true},
                                                     new ExpectedState {Name = "Power Windows", Selected = true},
                                                     new ExpectedState {Name = "Power Door Locks", Selected = true},
                                                     new ExpectedState {Name = "Tilt Wheel", Selected = true},
                                                     new ExpectedState {Name = "Cruise Control", Selected = true},
                                                     new ExpectedState {Name = "AM/FM Stereo", Selected = true},
                                                     new ExpectedState {Name = "Cassette"},
                                                     new ExpectedState {Name = "CD (Single Disc)", Selected = true},
                                                     new ExpectedState {Name = "CD (Multi Disc)"},
                                                     new ExpectedState {Name = "MP3 (Single Disc)"},
                                                     new ExpectedState {Name = "MP3 (Multi Disc)"},
                                                     new ExpectedState {Name = "Premium Sound"},
                                                     new ExpectedState {Name = "Navigation System"},
                                                     new ExpectedState {Name = "DVD System"},
                                                     new ExpectedState {Name = "Video System"},
                                                     new ExpectedState {Name = "Dual Air Bags", Selected = true},
                                                     new ExpectedState {Name = "ABS (4-Wheel)", Selected = true},
                                                     new ExpectedState {Name = "Leather"},
                                                     new ExpectedState {Name = "Power Seat"},
                                                     new ExpectedState {Name = "Dual Power Seats"},
                                                     new ExpectedState {Name = "Sun Roof (Flip-Up)"},
                                                     new ExpectedState {Name = "Sun Roof (Sliding)"},
                                                     new ExpectedState {Name = "Moon Roof"},
                                                     new ExpectedState {Name = "Sliding Rear Window"},
                                                     new ExpectedState {Name = "Power Sliding Rear Window"},
                                                     new ExpectedState {Name = "Stepside Bed"},
                                                     new ExpectedState {Name = "Stepside Bed (Long Bed)"},
                                                     new ExpectedState {Name = "Running Boards"},
                                                     new ExpectedState {Name = "Pickup Shell"},
                                                     new ExpectedState {Name = "Bed Liner"},
                                                     new ExpectedState {Name = "Custom Bumper"},
                                                     new ExpectedState {Name = "Grille Guard"},
                                                     new ExpectedState {Name = "Winch"},
                                                     new ExpectedState {Name = "Optional Fuel Tank"},
                                                     new ExpectedState {Name = "Towing Pkg"},
                                                     new ExpectedState {Name = "Snow Plow"},
                                                     new ExpectedState {Name = "Alloy Wheels", Selected = true},
                                                     new ExpectedState {Name = "Premium Wheels"},
                                                     new ExpectedState {Name = "Wide Tires"},
                                                     new ExpectedState {Name = "Oversize Off-Road Tires"},
                                                     new ExpectedState {Name = "Oversized Premium Wheels 20\"+"},
                                                     new ExpectedState {Name = "Dual Rear Wheels"},
                                                     new ExpectedState {Name = "Utility"},
                                                     new ExpectedState {Name = "Underbody Hoist"},
                                                     new ExpectedState {Name = "Dump Bed"},
                                                     new ExpectedState {Name = "Hydraulic Liftgate"},
                                                 };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string optionName = expectedState.Name;

                VehicleOption option = vehicle.Options.FirstOrDefault(x => Equals(x.Name, optionName));

                Assert.IsNotNull(option, "Missing Option {0}", optionName);

                IOptionState actualState = initial.OptionStates.First(x => option.Id == x.OptionId);

                Assert.AreEqual(expectedState.Selected, actualState.Selected, "Unexpected option state for {0}",
                                optionName);
            }
        }

        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            string zipCode = "48170";

            Region region = service.ToRegion(zipCode);

            Vehicle vehicle = service.Vehicle(666);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                region,
                zipCode,
                null);

            string[] selectedOptionNames = new[] { "V10, 6.8 Liter", "Automatic", "4WD" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // verify zero adjustment

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(0m, actualAdjustment);
            }

            // initial values

            const decimal expectedWholesale = 12033;
            const decimal expectedRetail = 16153;
            const decimal expectedTradeInFair = 9133;
            const decimal expectedTradeInGood = 10208;
            const decimal expectedTradeInExcellent = 10858;
            const decimal expectedAuctionFair = 10058;
            const decimal expectedAuctionGood = 11158;
            const decimal expectedAuctionExcellent = 11808;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }

        [Test]
        public void InitialConfigurationAboveZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            string zipCode = "48170";

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                null,
                zipCode,
                140000);

            string[] selectedOptionNames = new[] { "V10, 6.8 Liter", "Automatic", "4WD" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // initial values

            const decimal expectedAdjustment = -2050m;

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(expectedAdjustment, actualAdjustment);
            }

            const int expectedWholesale = 9983;
            const int expectedRetail = 14103;
            const int expectedTradeInFair = 7083;
            const int expectedTradeInGood = 8158;
            const int expectedTradeInExcellent = 8808;
            const int expectedAuctionFair = 8008;
            const int expectedAuctionGood = 9108;
            const int expectedAuctionExcellent = 9758;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }

        [Test]
        public void InitialConfigurationBelowZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            Region region = service.Regions().First(x => x.Id == (int)Helpers.Region.Michigan);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                region,
                null,
                75000);

            string[] selectedOptionNames = new[] { "V10, 6.8 Liter", "Automatic", "4WD" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // initial values

            const decimal expectedAdjustment = 2575m;

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(expectedAdjustment, actualAdjustment);
            }

            const int expectedWholesale = 14608;
            const int expectedRetail = 18728;
            const int expectedTradeInFair = 11708;
            const int expectedTradeInGood = 12783;
            const int expectedTradeInExcellent = 13433;
            const int expectedAuctionFair = 12633;
            const int expectedAuctionGood = 13733;
            const int expectedAuctionExcellent = 14383;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }

        [Test]
        public void MaximumAdjustment()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            string zipCode = "48170";

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                null,
                zipCode,
                1100);

            // remove all initial options
            foreach (var optionState in initial.OptionStates)
            {
                if (optionState.Selected)
                {
                    initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Remove,
                        OptionId = optionState.OptionId
                    });
                }
            }

            // add custom options
            string[] selectedOptionNames = new[] { "V10, 6.8 Liter", "Automatic", "4WD", "ABS (4-Wheel)", "Air Conditioning", "Power Steering", "AM/FM Stereo", "Dual Air Bags", "Premium Wheels" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // initial values

            const decimal expectedAdjustment = 7100m;

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(expectedAdjustment, actualAdjustment);
            }

            const int expectedWholesale = 18833;
            const int expectedRetail = 22853;
            const int expectedTradeInFair = 15933;
            const int expectedTradeInGood = 17008;
            const int expectedTradeInExcellent = 17658;
            const int expectedAuctionFair = 16858;
            const int expectedAuctionGood = 17958;
            const int expectedAuctionExcellent = 18608;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }
    }
}
