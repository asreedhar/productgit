﻿using System;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    [TestFixture]
    public class CalculatorTest1999AcuraNsxTarga2D : KbbCalculatorTest
    {
        private const string FileName = ".1999 Acura NSX NSX-T Targa 2D.xml";

        [Test]
        public void TestReadXml()
        {
            TestReadXml(FileName);
        }

        [Test]
        public void TestTableNames()
        {
            TestTableNames(FileName);
        }

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            string zipCode = "48170";

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,         // 5042
                "JH4NA2167XT000022",
                null,
                zipCode,
                null);

            string[] selectedOptionNames = new[] { "V6, VTEC, 3.2 Liter", "Manual, 6-Spd", "RWD" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            // (no) initial actions

            Assert.AreEqual(3, initial.OptionActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                                 {
                                                     new ExpectedState {Name = "V6, VTEC, 3.0 Liter"},
                                                     new ExpectedState {Name = "V6, VTEC, 3.2 Liter", Selected = true},
                                                     new ExpectedState {Name = "Manual, 6-Spd", Selected = true},
                                                     new ExpectedState {Name = "Automatic"},
                                                     new ExpectedState {Name = "RWD", Selected = true},
                                                     new ExpectedState {Name = "Air Conditioning"},
                                                     new ExpectedState {Name = "Power Steering"},
                                                     new ExpectedState {Name = "Power Windows"},
                                                     new ExpectedState {Name = "Power Door Locks"},
                                                     new ExpectedState {Name = "Tilt Wheel"},
                                                     new ExpectedState {Name = "Cruise Control"},
                                                     new ExpectedState {Name = "AM/FM Stereo"},
                                                     new ExpectedState {Name = "Cassette"},
                                                     new ExpectedState {Name = "CD (Single Disc)"},
                                                     new ExpectedState {Name = "CD (Multi Disc)"},
                                                     new ExpectedState {Name = "Premium Sound"},
                                                     new ExpectedState {Name = "Integrated Phone"},
                                                     new ExpectedState {Name = "Navigation System"},
                                                     new ExpectedState {Name = "Parking Sensors"},
                                                     new ExpectedState {Name = "DVD System"},
                                                     new ExpectedState {Name = "Dual Air Bags"},
                                                     new ExpectedState {Name = "Side Air Bags"},
                                                     new ExpectedState {Name = "F&R Side Air Bags"},
                                                     new ExpectedState {Name = "ABS (4-Wheel)"},
                                                     new ExpectedState {Name = "Traction Control"},
                                                     new ExpectedState {Name = "Leather"},
                                                     new ExpectedState {Name = "Dual Power Seats"},
                                                     new ExpectedState {Name = "Sun Roof (Sliding)"},
                                                     new ExpectedState {Name = "Moon Roof"},
                                                     new ExpectedState {Name = "Rear Spoiler"},
                                                     new ExpectedState {Name = "Alloy Wheels"},
                                                     new ExpectedState {Name = "Premium Wheels"},
                                                     new ExpectedState {Name = "MP3 (Single Disc)"},
                                                     new ExpectedState {Name = "MP3 (Multi Disc)"},
                                                     new ExpectedState {Name = "Premium Wheels 19\"+"}
                                                 };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string optionName = expectedState.Name;

                VehicleOption option = vehicle.Options.FirstOrDefault(x => Equals(x.Name, optionName));

                Assert.IsNotNull(option, "Missing Option {0}", optionName);

                IOptionState actualState = initial.OptionStates.First(x => option.Id == x.OptionId);

                Assert.AreEqual(expectedState.Selected, actualState.Selected, "Unexpected option state for {0}",
                                optionName);
            }
        }

        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            string zipCode = "48170";

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                "JH4NA2167XT000022",
                null,
                zipCode,
                null);

            string[] selectedOptionNames = new[] { "V6, VTEC, 3.2 Liter", "Manual, 6-Spd", "RWD" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // verify zero adjustment

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(0m, actualAdjustment);
            }

            // initial values

            const decimal expectedWholesale = 24678;
            const decimal expectedRetail = 32138;
            const decimal expectedTradeInFair = 20378;
            const decimal expectedTradeInGood = 22978;
            const decimal expectedTradeInExcellent = 24478;
            const decimal expectedAuctionFair = 21378;
            const decimal expectedAuctionGood = 23978;
            const decimal expectedAuctionExcellent = 25478;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }

        [Test]
        public void UpdatedConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Region region = service.Regions().DefaultIfEmpty(null).Single(x => x.Id == (int)Helpers.Region.Michigan);

            Vehicle vehicle = service.Vehicle(666);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                "JH4NA2167XT000022",
                region,
                null,
                null);

            string[] selectedOptionNames = new[] { "V6, VTEC, 3.2 Liter", "Manual, 6-Spd", "RWD", "Air Conditioning" };

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                    {
                        ActionType = OptionActionType.Add,
                        OptionId = option.Id
                    });
            }

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // verify zero mileage adjustment

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(0m, actualAdjustment);
            }

            // initial values

            const decimal expectedWholesale = 24828;
            const decimal expectedRetail = 32338;
            const decimal expectedTradeInFair = 20528;
            const decimal expectedTradeInGood = 23128;
            const decimal expectedTradeInExcellent = 24628;
            const decimal expectedAuctionFair = 21528;
            const decimal expectedAuctionGood = 24128;
            const decimal expectedAuctionExcellent = 25628;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }
    }
}
