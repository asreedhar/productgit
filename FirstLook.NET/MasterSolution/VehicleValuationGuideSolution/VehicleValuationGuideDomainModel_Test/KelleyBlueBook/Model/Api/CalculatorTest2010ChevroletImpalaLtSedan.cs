using System;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    [TestFixture]
    public class CalculatorTest2010ChevroletImpalaLtSedan : KbbCalculatorTest
    {
        private const string FileName = ".2010 Chevrolet Impala LT Sedan 4D.xml";

        [Test]
        public void TestReadXml()
        {
            TestReadXml(FileName);
        }

        [Test]
        public void TestTableNames()
        {
            TestTableNames(FileName);
        }

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            Region region = service.Regions().First(x => x.Id == (int)Helpers.Region.Michigan);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                region,
                null,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.OptionActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                         {
                                             new ExpectedState {Name = "Luxury Pkg"},
                                             new ExpectedState {Name = "Weather Pkg"},
                                             new ExpectedState {Name = "Traction Control", Selected = true},
                                             new ExpectedState {Name = "StabiliTrak", Selected = true},
                                             new ExpectedState {Name = "Heated Seats"},
                                             new ExpectedState {Name = "Air Conditioning", Selected = true},
                                             new ExpectedState {Name = "Power Steering", Selected = true},
                                             new ExpectedState {Name = "Power Windows", Selected = true},
                                             new ExpectedState {Name = "Power Door Locks", Selected = true},
                                             new ExpectedState {Name = "Tilt Wheel", Selected = true},
                                             new ExpectedState {Name = "Cruise Control", Selected = true},
                                             new ExpectedState {Name = "AM/FM Stereo", Selected = true},
                                             new ExpectedState {Name = "Cassette"},
                                             new ExpectedState {Name = "CD (Single Disc)"},
                                             new ExpectedState {Name = "CD (Multi Disc)"},
                                             new ExpectedState {Name = "MP3 (Single Disc)", Selected = true},
                                             new ExpectedState {Name = "MP3 (Multi Disc)"},
                                             new ExpectedState {Name = "Premium Sound"},
                                             new ExpectedState {Name = "OnStar", Selected = true},
                                             new ExpectedState {Name = "XM Satellite"},
                                             new ExpectedState {Name = "Navigation System"},
                                             new ExpectedState {Name = "Bluetooth Wireless"},
                                             new ExpectedState {Name = "DVD System"},
                                             new ExpectedState {Name = "Dual Air Bags", Selected = true},
                                             new ExpectedState {Name = "Side Air Bags"},
                                             new ExpectedState {Name = "ABS (4-Wheel)", Selected = true},
                                             new ExpectedState {Name = "Leather"},
                                             new ExpectedState {Name = "Power Seat", Selected = true},
                                             new ExpectedState {Name = "Dual Power Seats"},
                                             new ExpectedState {Name = "Sun Roof (Sliding)"},
                                             new ExpectedState {Name = "Moon Roof"},
                                             new ExpectedState {Name = "Rear Spoiler"},
                                             new ExpectedState {Name = "Alloy Wheels", Selected = true},
                                             new ExpectedState {Name = "Premium Wheels"},
                                             new ExpectedState {Name = "Premium Wheels 19\"+"}
                                         };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string optionName = expectedState.Name;

                VehicleOption option = vehicle.Options.FirstOrDefault(x => Equals(x.Name, optionName));

                Assert.IsNotNull(option, "Missing Option {0}", optionName);

                IOptionState actualState = initial.OptionStates.First(x => option.Id == x.OptionId);

                Assert.AreEqual(expectedState.Selected, actualState.Selected, "Unexpected option state for {0}",
                                optionName);
            }
        }

        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            string zipCode = "48170";

            Region region = service.Regions().First(x => x.Id == (int)Helpers.Region.Michigan);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                region,
                zipCode,
                null);

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // initial values

            const int expectedWholesale = 13388;
            const int expectedRetail = 15773;
            const int expectedTradeInFair = 9788;
            const int expectedTradeInGood = 11138;
            const int expectedTradeInExcellent = 11938;
            const int expectedAuctionFair = 10688;
            const int expectedAuctionGood = 12038;
            const int expectedAuctionExcellent = 12838;

            decimal? actualWholesale = matrix[PriceType.Wholesale,Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;
            
            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(0m, actualAdjustment);
            }
        }

        [Test]
        public void InitialConfigurationAboveZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            const string zipCode = "48170";

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                null,
                zipCode,
                55000);

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // initial values

            const decimal expectedAdjustment = -1350m;

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(expectedAdjustment, actualAdjustment);
            }

            const int expectedWholesale = 12038;
            const int expectedRetail = 14423;
            const int expectedTradeInFair = 8438;
            const int expectedTradeInGood = 9788;
            const int expectedTradeInExcellent = 10588;
            const int expectedAuctionFair = 9338;
            const int expectedAuctionGood = 10688;
            const int expectedAuctionExcellent = 11488;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }

        [Test]
        public void InitialConfigurationBelowZeroPoint()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            Region region = service.Regions().First(x => x.Id == (int)Helpers.Region.Michigan);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                region,
                null,
                500);

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // initial values

            const decimal expectedAdjustment = 1225m;

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                decimal? actualAdjustment = matrix[priceType, Valuation.Mileage].Value;

                Assert.AreEqual(expectedAdjustment, actualAdjustment);
            }

            const int expectedWholesale = 14613;
            const int expectedRetail = 16998;
            const int expectedTradeInFair = 11013;
            const int expectedTradeInGood = 12363;
            const int expectedTradeInExcellent = 13163;
            const int expectedAuctionFair = 11913;
            const int expectedAuctionGood = 13263;
            const int expectedAuctionExcellent = 14063;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }

        [Test]
        public void ConflictsWith()
        {
            InitializeDataSet(FileName);

            IKbbService service = Resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(666);

            const string zipCode = "48170";

            Region region = service.ToRegion(zipCode);

            IKbbConfiguration configuration = Resolver.Resolve<IKbbConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Id,
                null,
                region,
                zipCode,
                null);

            // update configuration

            string[] selectedOptionNames = new[] {"Sun Roof (Sliding)", "Moon Roof"};

            foreach (string selectedOptionName in selectedOptionNames)
            {
                string name = selectedOptionName; // no warnings plz

                VehicleOption option = vehicle.Options.First(x => Equals(x.Name, name));

                initial = configuration.UpdateConfiguration(
                    service,
                    initial,
                    new OptionAction
                        {
                            ActionType = OptionActionType.Add,
                            OptionId = option.Id
                        });
            }

            // perform calculation

            IKbbCalculator calculator = Resolver.Resolve<IKbbCalculator>();

            // have a matrix

            Matrix matrix = calculator.Calculate(initial);

            Assert.IsNotNull(matrix);

            // two actions; one remains ...

            Assert.AreEqual(1, initial.OptionActions.Count);

            // verify correct selections

            ExpectedState[] expectedStates = new[]
                                         {
                                             new ExpectedState {Name = "Luxury Pkg"},
                                             new ExpectedState {Name = "Weather Pkg"},
                                             new ExpectedState {Name = "Traction Control", Selected = true},
                                             new ExpectedState {Name = "StabiliTrak", Selected = true},
                                             new ExpectedState {Name = "Heated Seats"},
                                             new ExpectedState {Name = "Air Conditioning", Selected = true},
                                             new ExpectedState {Name = "Power Steering", Selected = true},
                                             new ExpectedState {Name = "Power Windows", Selected = true},
                                             new ExpectedState {Name = "Power Door Locks", Selected = true},
                                             new ExpectedState {Name = "Tilt Wheel", Selected = true},
                                             new ExpectedState {Name = "Cruise Control", Selected = true},
                                             new ExpectedState {Name = "AM/FM Stereo", Selected = true},
                                             new ExpectedState {Name = "Cassette"},
                                             new ExpectedState {Name = "CD (Single Disc)"},
                                             new ExpectedState {Name = "CD (Multi Disc)"},
                                             new ExpectedState {Name = "MP3 (Single Disc)", Selected = true},
                                             new ExpectedState {Name = "MP3 (Multi Disc)"},
                                             new ExpectedState {Name = "Premium Sound"},
                                             new ExpectedState {Name = "OnStar", Selected = true},
                                             new ExpectedState {Name = "XM Satellite"},
                                             new ExpectedState {Name = "Navigation System"},
                                             new ExpectedState {Name = "Bluetooth Wireless"},
                                             new ExpectedState {Name = "DVD System"},
                                             new ExpectedState {Name = "Dual Air Bags", Selected = true},
                                             new ExpectedState {Name = "Side Air Bags"},
                                             new ExpectedState {Name = "ABS (4-Wheel)", Selected = true},
                                             new ExpectedState {Name = "Leather"},
                                             new ExpectedState {Name = "Power Seat", Selected = true},
                                             new ExpectedState {Name = "Dual Power Seats"},
                                             new ExpectedState {Name = "Sun Roof (Sliding)"},
                                             new ExpectedState {Name = "Moon Roof", Selected = true},
                                             new ExpectedState {Name = "Rear Spoiler"},
                                             new ExpectedState {Name = "Alloy Wheels", Selected = true},
                                             new ExpectedState {Name = "Premium Wheels"},
                                             new ExpectedState {Name = "Premium Wheels 19\"+"}
                                         };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string optionName = expectedState.Name;

                VehicleOption option = vehicle.Options.FirstOrDefault(x => Equals(x.Name, optionName));

                Assert.IsNotNull(option, "Missing Option {0}", optionName);

                IOptionState actualState = initial.OptionStates.First(x => option.Id == x.OptionId);

                Assert.AreEqual(expectedState.Selected, actualState.Selected, "Unexpected option state for {0}",
                                optionName);
            }

            // verify values

            const int expectedWholesale = 13863;
            const int expectedRetail = 16408;
            const int expectedTradeInFair = 10263;
            const int expectedTradeInGood = 11613;
            const int expectedTradeInExcellent = 12413;
            const int expectedAuctionFair = 11163;
            const int expectedAuctionGood = 12513;
            const int expectedAuctionExcellent = 13313;

            decimal? actualWholesale = matrix[PriceType.Wholesale, Valuation.Final].Value;
            decimal? actualRetail = matrix[PriceType.Retail, Valuation.Final].Value;
            decimal? actualTradeInFair = matrix[PriceType.TradeInFair, Valuation.Final].Value;
            decimal? actualTradeInGood = matrix[PriceType.TradeInGood, Valuation.Final].Value;
            decimal? actualTradeInExcellent = matrix[PriceType.TradeInExcellent, Valuation.Final].Value;
            decimal? actualAuctionFair = matrix[PriceType.AuctionFair, Valuation.Final].Value;
            decimal? actualAuctionGood = matrix[PriceType.AuctionGood, Valuation.Final].Value;
            decimal? actualAuctionExcellent = matrix[PriceType.AuctionExcellent, Valuation.Final].Value;

            Assert.AreEqual(expectedWholesale, actualWholesale);
            Assert.AreEqual(expectedRetail, actualRetail);
            Assert.AreEqual(expectedTradeInFair, actualTradeInFair);
            Assert.AreEqual(expectedTradeInGood, actualTradeInGood);
            Assert.AreEqual(expectedTradeInExcellent, actualTradeInExcellent);
            Assert.AreEqual(expectedAuctionFair, actualAuctionFair);
            Assert.AreEqual(expectedAuctionGood, actualAuctionGood);
            Assert.AreEqual(expectedAuctionExcellent, actualAuctionExcellent);
        }
    }
}
