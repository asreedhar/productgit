using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    /// <see cref="http://www.edmunds.com/ford/mustang/2004/tmv-appraise.html?sub=coupe&style=100299264"/>
    [TestFixture]
    public class CalculatorTest2004FordMustangGtPremium2DrCoupe : EdmundsCalculatorTest
    {
        private const string FileName = ".2004 Ford Mustang GT Premium 2dr Coupe.xml";

        private const int StyleId = 100299264;

        private const int GenericColorBlack = 1;

        private const int ManufacturerColorBlack = 100308218;

        [Test]
        public void TestReadXml()
        {
            TestReadXml(FileName);
        }

        [Test]
        public void TestTableNames()
        {
            TestTableNames(FileName);
        }

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IEdmundsService service = Resolver.Resolve<IEdmundsService>();

            Vehicle vehicle = service.Vehicle(StyleId);

            IList<Option> options = service.Options(StyleId);

            State state = State.Values.First(x => Equals(x.Code, "IL"));

            IEdmundsConfiguration configuration = Resolver.Resolve<IEdmundsConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Style.Id,
                null,
                state,
                null,
                null,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.OptionActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                         {
                                             new ExpectedState {Name = "17 Inch Polished Alloy Wheels"},
                                             new ExpectedState {Name = "4-Speed Automatic Transmission"},
                                             new ExpectedState {Name = "4-Wheel ABS"},
                                             new ExpectedState {Name = "Engine Block Heater"},
                                             new ExpectedState {Name = "Leather Shift Knob Trim"},
                                             new ExpectedState {Name = "MACH Audio"},
                                             new ExpectedState {Name = "Traction Control"}
                                         };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string optionName = expectedState.Name;

                Option option = options.FirstOrDefault(x => Equals(x.Name.Trim(), optionName));

                Assert.IsNotNull(option, "Missing Option {0}", optionName);

                IOptionState actualState = initial.OptionStates.First(x => option.Id == x.OptionId);

                Assert.AreEqual(expectedState.Selected,
                                actualState.Selected,
                                "Unexpected option state for {0}",
                                optionName);
            }
        }

        [Test]
        public void InitialConfigurationZeroPoint()
        {
            InitializeDataSet(FileName);

            IEdmundsService service = Resolver.Resolve<IEdmundsService>();

            Vehicle vehicle = service.Vehicle(StyleId);

            State state = State.Values.First(x => Equals(x.Code, "IL"));

            IEdmundsConfiguration configuration = Resolver.Resolve<IEdmundsConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Style.Id,
                null,
                state,
                82000,
                GenericColorBlack,
                ManufacturerColorBlack);

            IEdmundsCalculator calculator = Resolver.Resolve<IEdmundsCalculator>();

            // have a matrix

            Matrix actualMatrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(actualMatrix);

            // initial values

            Matrix expectedMatrix = new Matrix();

            #region Rough

            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Base].Value = 8015.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Condition].Value = -2782.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Final].Value = 5231.0m;

            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Base].Value = 9221.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Condition].Value = -3201.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Final].Value = 6018.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Base].Value = 6946.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Condition].Value = -2426.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Final].Value = 4518.0m;
            
            #endregion

            #region Average

            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Base].Value = 8015.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Color].Value = -8.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Condition].Value = -1842.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Final].Value = 6163.0m;

            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Base].Value = 9221.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Color].Value = -10.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Condition].Value = -2057.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Final].Value = 7152.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Base].Value = 6946.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Color].Value = -7.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Condition].Value = -1627.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Final].Value = 5310.0m;

            #endregion

            #region Clean

            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Base].Value = 8015.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Color].Value = -28.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Condition].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Final].Value = 7985.0m;

            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Base].Value = 9221.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Color].Value = -32.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Condition].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Final].Value = 9187.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Base].Value = 6946.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Color].Value = -24.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Condition].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Final].Value = 6920.0m;

            #endregion

            #region Outstanding

            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Base].Value = 8015.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Color].Value = -31.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Condition].Value = 746.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Final].Value = 8728.0m;

            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Base].Value = 9221.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Color].Value = -36.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Condition].Value = 877.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Final].Value = 10060.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Base].Value = 6946.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Color].Value = -27.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Region].Value = -2.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Mileage].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Condition].Value = 676.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Final].Value = 7593.0m;

            #endregion

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined) continue;

                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {
                    if (condition == Condition.Undefined) continue;

                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        if (valuation == Valuation.Undefined) continue;

                        decimal? actual = actualMatrix[priceType, condition, valuation].Value;

                        decimal? expected = expectedMatrix[priceType, condition, valuation].Value;

                        Assert.AreEqual(expected, actual, "{0} {1} {2}", priceType, condition, valuation);
                    }
                }
            }
        }

        // mileage above zero point
        // mileage below zero point
        // color
        // condition

    }
}
