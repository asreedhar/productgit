using System;
using System.Data;
using System.IO;
using System.Reflection;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public class EdmundsCalculatorTest
    {
        private IRegistry _registry;

        private IResolver _resolver;

        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api.Resources";

        protected IRegistry Registry
        {
            get { return _registry; }
        }

        protected IResolver Resolver
        {
            get { return _resolver; }
        }

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<Module>();

            _registry.Register<Lookup.Module>();

            _registry.Register<Sql.Module.SerializerModule>();

            _registry.Register<IVehicleConfigurationBuilderFactory, VehicleConfigurationBuilderFactory>(ImplementationScope.Shared);
            
            _registry.Register<ISqlService, XmlService>(ImplementationScope.Shared);

            _registry.Register<ICache, NoCache>(ImplementationScope.Shared);

            _resolver = RegistryFactory.GetResolver();
        }

        [TearDown]
        public void TearDown()
        {
            _resolver.Dispose();

            _resolver = null;

            _registry.Dispose();

            _registry = null;
        }

        protected void InitializeDataSet(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            XmlService xml = (XmlService)Resolver.Resolve<ISqlService>();

            xml.Set = set;
        }

        protected static Stream GetResourceFromAssembly(string resourceName)
        {
            Assembly a = Assembly.GetExecutingAssembly();

            return a.GetManifestResourceStream(resourceName);
        }

        protected class ExpectedState
        {
            private string _name;
            private bool _selected;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public bool Selected
            {
                get { return _selected; }
                set { _selected = value; }
            }
        }

        protected class XmlService : ISqlService
        {
            private DataSet _set;

            public DataSet Set
            {
                get
                {
                    if (_set == null)
                    {
                        throw new NotSupportedException("Not configured");
                    }
                    return _set;
                }
                set { _set = value; }
            }

            public IDataReader DataLoad()
            {
                return Set.Tables["DataLoad"].CreateDataReader();
            }

            public IDataReader Region(int dataLoad, string stateCode)
            {
                return Set.Tables["Region"].CreateDataReader();
            }

            public IDataReader Query(int dataLoad)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int dataLoad, int year)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int dataLoad, int year, string make)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int dataLoad, int year, string make, string model)
            {
                throw new NotSupportedException();
            }

            public IDataReader Query(int dataLoad, string vin)
            {
                throw new NotSupportedException();
            }

            public IDataReader Vehicle(int dataLoad, int styleId)
            {
                return Set.Tables["Vehicle"].CreateDataReader();
            }

            public IDataReader Options(int dataLoad, int styleId)
            {
                return Set.Tables["Options"].CreateDataReader();
            }

            public IDataReader Colors(int dataLoad, int styleId)
            {
                return Set.Tables["Colors"].CreateDataReader();
            }

            public IDataReader ColorAdjustments(int dataLoad, int styleId, int regionId, int colorId)
            {
                return Set.Tables["ColorAdjustments"].CreateDataReader();
            }

            public IDataReader RegionAdjustments(int dataLoad, int styleId, int regionId)
            {
                return Set.Tables["RegionAdjustments"].CreateDataReader();
            }

            public IDataReader ConditionAdjustments(int dataLoad, int styleId)
            {
                return Set.Tables["ConditionAdjustments"].CreateDataReader();
            }

            public IDataReader MileageAdjustments(int dataLoad, int styleId)
            {
                return Set.Tables["MileageAdjustments"].CreateDataReader();
            }

            public IDataReader CertifiedCost(int dataLoad, int styleId)
            {
                return Set.Tables["CertifiedCost"].CreateDataReader();
            }

            public IDataReader CertifiedCriteria(int dataLoad, int styleId)
            {
                return Set.Tables["CertifiedCriteria"].CreateDataReader();
            }

            public IDataReader CertifiedMileageAdjustments(int dataLoad, int styleId)
            {
                return Set.Tables["CertifiedMileageAdjustments"].CreateDataReader();
            }
        }

        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }

        protected void TestReadXml(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }
        }

        protected void TestTableNames(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            string[] tableNames = new[]
                                      {
                                          "Vehicle",
                                          "Region",
                                          "Options",
                                          "Colors",
                                          "ColorAdjustments",
                                          "RegionAdjustments",
                                          "ConditionAdjustments",
                                          "MileageAdjustments",
                                          "CertifiedCost",
                                          "CertifiedCriteria",
                                          "CertifiedMileageAdjustments"
                                      };

            foreach (string tableName in tableNames)
            {
                Assert.IsNotNull(set.Tables[tableName], tableName);
            }
        }
    }
}