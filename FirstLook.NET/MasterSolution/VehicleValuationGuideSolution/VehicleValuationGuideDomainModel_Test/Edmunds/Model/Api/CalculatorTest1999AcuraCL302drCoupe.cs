﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    /// <see cref="http://www.edmunds.com/acura/cl/1999/tmv-appraise.html?style=12843"/>
    [TestFixture]
    public class CalculatorTest1999AcuraCl302DrCoupe : EdmundsCalculatorTest
    {
        private const string FileName = ".1999 Acura CL 3 0 2dr Coupe.xml";

        private const int StyleId = 12843;

        private const int GenericColorBlack = 1;

        private const int ManufacturerColorBlack = 100367373;

        [Test]
        public void TestReadXml()
        {
            TestReadXml(FileName);
        }

        [Test]
        public void TestTableNames()
        {
            TestTableNames(FileName);
        }

        [Test]
        public void InitialConfigurationSelections()
        {
            InitializeDataSet(FileName);

            IEdmundsService service = Resolver.Resolve<IEdmundsService>();

            Vehicle vehicle = service.Vehicle(StyleId);

            IList<Option> options = service.Options(StyleId);

            State state = State.Values.First(x => Equals(x.Code, "IL"));

            IEdmundsConfiguration configuration = Resolver.Resolve<IEdmundsConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Style.Id,
                null,
                state,
                null,
                null,
                null);

            // (no) initial actions

            Assert.AreEqual(0, initial.OptionActions.Count);

            // initial selections

            ExpectedState[] expectedStates = new[]
                                         {
                                             new ExpectedState {Name = "Rear Spoiler"}
                                         };

            foreach (ExpectedState expectedState in expectedStates)
            {
                string optionName = expectedState.Name;

                Option option = options.FirstOrDefault(x => Equals(x.Name.Trim(), optionName));

                Assert.IsNotNull(option, "Missing Option {0}", optionName);

                IOptionState actualState = initial.OptionStates.First(x => option.Id == x.OptionId);

                Assert.AreEqual(expectedState.Selected,
                                actualState.Selected,
                                "Unexpected option state for {0}",
                                optionName);
            }
        }

        [Test]
        public void InitialConfigurationBelowZero()
        {
            InitializeDataSet(FileName);

            IEdmundsService service = Resolver.Resolve<IEdmundsService>();

            Vehicle vehicle = service.Vehicle(StyleId);

            State state = State.Values.First(x => Equals(x.Code, "IL"));

            IEdmundsConfiguration configuration = Resolver.Resolve<IEdmundsConfiguration>();

            IVehicleConfiguration initial = configuration.InitialConfiguration(
                vehicle.Style.Id,
                "19UYA2250XL004354",
                state,
                10000,
                GenericColorBlack,
                ManufacturerColorBlack);

            IEdmundsCalculator calculator = Resolver.Resolve<IEdmundsCalculator>();

            // have a matrix

            Matrix actualMatrix = calculator.Calculate(service, initial);

            Assert.IsNotNull(actualMatrix);

            // initial values

            Matrix expectedMatrix = new Matrix();

            #region Rough

            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Base].Value = 2795.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Region].Value = 30.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Condition].Value = -1153.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Rough, Valuation.Final].Value = 2608.0m;

            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Base].Value = 3876.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Region].Value = 41.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Condition].Value = -1594.0m;
            expectedMatrix[PriceType.Retail, Condition.Rough, Valuation.Final].Value = 3259.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Base].Value = 2147.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Region].Value = 23.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Condition].Value = -892.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Rough, Valuation.Final].Value = 2214.0m;

            #endregion

            #region Average

            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Base].Value = 2795.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Region].Value = 30.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Condition].Value = -671.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Average, Valuation.Final].Value = 3090.0m;

            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Base].Value = 3876.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Region].Value = 41.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Condition].Value = -907.0m;
            expectedMatrix[PriceType.Retail, Condition.Average, Valuation.Final].Value = 3946.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Base].Value = 2147.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Region].Value = 23.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Condition].Value = -525.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Average, Valuation.Final].Value = 2581.0m;

            #endregion

            #region Clean

            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Base].Value = 2795.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Region].Value = 30.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Condition].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Clean, Valuation.Final].Value = 3761.0m;

            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Base].Value = 3876.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Region].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Condition].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Clean, Valuation.Final].Value = 4853.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Base].Value = 2147.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Region].Value = 23.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Condition].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Clean, Valuation.Final].Value = 3106.0m;

            #endregion

            #region Outstanding

            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Base].Value = 2795.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Option].Value = null;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Region].Value = 0.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Condition].Value = 440.0m;
            expectedMatrix[PriceType.PrivateParty, Condition.Outstanding, Valuation.Final].Value = 4201.0m;

            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Base].Value = 3876.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Option].Value = null;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Region].Value = 0.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Condition].Value = 624.0m;
            expectedMatrix[PriceType.Retail, Condition.Outstanding, Valuation.Final].Value = 5477.0m;

            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Base].Value = 2147.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Option].Value = null;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Color].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Region].Value = 0.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Mileage].Value = 936.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Condition].Value = 353.0m;
            expectedMatrix[PriceType.TradeIn, Condition.Outstanding, Valuation.Final].Value = 3459.0m;

            #endregion

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined) continue;

                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {
                    if (condition == Condition.Undefined) continue;

                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        if (valuation == Valuation.Undefined) continue;

                        decimal? actual = actualMatrix[priceType, condition, valuation].Value;

                        decimal? expected = expectedMatrix[priceType, condition, valuation].Value;

                        Assert.AreEqual(expected, actual, "{0} {1} {2}", priceType, condition, valuation);
                    }
                }
            }
        }
    }
}
