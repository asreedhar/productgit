﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    [TestFixture]
    public class CalculatorTest
    {
        private const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api.Resources";

        private IRegistry _registry;

        private IResolver _resolver;

        protected IRegistry Registry
        {
            get { return _registry; }
        }

        protected IResolver Resolver
        {
            get { return _resolver; }
        }

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IBlackBookService, BlackBookService>(ImplementationScope.Shared);

            _registry.Register<Lookup.Module>();

            _registry.Register<ILookupStandardEquipment, LookupStandardEquipment>(ImplementationScope.Shared);

            _registry.Register<ILookupUvc, LookupUvc>(ImplementationScope.Shared);

            _registry.Register<ISqlService, TestSqlService>(ImplementationScope.Shared);

            _registry.Register<Sql.Module.Serialization>();

            _registry.Register<IXmlService, TestXmlService>(ImplementationScope.Shared);

            _registry.Register<Xml.Module.Serialization>();

            _resolver = RegistryFactory.GetResolver();

            InitializeDataSet(".BlackBook.xml");
        }

        [TearDown]
        public void TearDown()
        {
            _resolver.Dispose();

            _resolver = null;

            _registry.Dispose();

            _registry = null;
        }

        protected void InitializeDataSet(string fileName)
        {
            DataSet set = new DataSet();

            using (Stream stream = GetResourceFromAssembly(Prefix + fileName))
            {
                Assert.IsNotNull(stream);

                set.ReadXml(stream);
            }

            TestSqlService xml = (TestSqlService) Resolver.Resolve<ISqlService>();

            xml.Set = set;
        }

        #region Calulator.Calulate(...) Tests

        [Test]
        public void TestFifteenYearOldPlusCar()
        {
            const string resourceName = Prefix + ".1994 Honda Accord LX 4D Wagon.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 1994;
            const string expectedMake = "Honda";
            const string expectedModel = "Accord";
            const string expectedSeries = "LX";
            const string expectedBodyStyle = "4D Wagon";

            const int mileage = 180000;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string>();

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  2100;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value = null; //0; fails test.
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value = null; //0; fails test.
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  2100;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  1125;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value = null; //0; fails test.
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value = null; //0; fails test.
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  1125;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  550;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value = null; //0; fails test.
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value = null; //0; fails test.
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  550;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestFiftyPercentMileageRule()
        {
            const string resourceName = Prefix + ".1996 Toyota Corolla DX 4D Sedan.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 1996;
            const string expectedMake = "Toyota";
            const string expectedModel = "Corolla";
            const string expectedSeries = "DX";
            const string expectedBodyStyle = "4D Sedan";

            const int mileage = 350000;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string>();

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  1850;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  -(1850/2);
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  925;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  1200;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  -(1200/2);
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  600;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  750;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  -(750/2);
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  375;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestZeroMileage()
        {
            const string resourceName = Prefix + ".2002 Chevrolet Astro Vans Base Ext Cargo Van.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2002;
            const string expectedMake = "Chevrolet";
            const string expectedModel = "Astro Vans";
            const string expectedSeries = "Base";
            const string expectedBodyStyle = "Ext Cargo Van";

            const int mileage = 0;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"RG", "RR"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  5025;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  4100;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value = null; //0;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  9125;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  3075;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  3100;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value = null; //0;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  6175;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  1725;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  1850;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value = null; //0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  3575;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestOver6YearOldConversionVanRrRgOptions()
        {
            const string resourceName = Prefix + ".2002 Chevrolet Astro Vans Base Ext Cargo Van.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2002;
            const string expectedMake = "Chevrolet";
            const string expectedModel = "Astro Vans";
            const string expectedSeries = "Base";
            const string expectedBodyStyle = "Ext Cargo Van";

            const int mileage = 12345;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"RG", "RR"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  5025;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  4100;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  600;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  9725;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  3075;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  3100;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  700;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  6875;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  1725;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  1850;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  3575;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        /// <summary>
        /// Used Car XML Portal User Documentation page 7 of 29, the else block.
        /// </summary>
        [Test]
        public void TestSpecialModelAddDeducts()
        {
            const string resourceName = Prefix + ".2002 Ford E150 Vans XLT Club Wagon.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2002;
            const string expectedMake = "Ford";
            const string expectedModel = "E150 Vans";
            const string expectedSeries = "XLT";
            const string expectedBodyStyle = "Club Wagon";

            const int mileage = 12345;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"43", "20", "TV"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  0; //null; fails
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  5525;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  850;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  650;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  7025;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  3675;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  850;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  750;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  5275;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  2050;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  850;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  2900;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        /// <summary>
        /// Test 2 rules: the Model name with '1', '2', or '3' in it, and the Conversion Van RG/RR options
        /// </summary>
        [Test]
        public void TestSpecialModelAddDeducts2()
        {
            const string resourceName = Prefix + ".2002 Ford E250 Vans Econoline Cargo Van.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2002;
            const string expectedMake = "Ford";
            const string expectedModel = "E250 Vans";
            const string expectedSeries = "Econoline";
            const string expectedBodyStyle = "Cargo Van";

            const int mileage = 12345;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"RG", "RR"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  5425;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  4100;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  650;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  10175;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  3675;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  3100;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  750;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  7525;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  2200;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  1850;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  4050;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestFiftyPercentOptionAdjust()
        {
            const string resourceName = Prefix + ".2003 Ford F450SD XL CabChas 4WD DRW.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 2;
            const int expectedYear = 2003;
            const string expectedMake = "Ford";
            const string expectedModel = "F450SD";
            const string expectedSeries = "XL";
            const string expectedBodyStyle = "Cab/Chas 4WD DRW";

            const int mileage = 2;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"HW", "RW"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  11525;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  11525/2;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  725;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  18012;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  9225;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  9225/2;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  850;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  14687;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  7350;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  7350/2;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  11025;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestUnder6YearOldConversionVanRrRgOptions()
        {
            const string resourceName = Prefix + ".2004 Chevrolet Astro Vans Base Ext Cargo Van.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2008; // I changed the year since it technically was no longer < 6 years old.
            const string expectedMake = "Chevrolet";
            const string expectedModel = "Astro Vans";
            const string expectedSeries = "Base";
            const string expectedBodyStyle = "Ext Cargo Van";

            const int mileage = 12345;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"RR", "RG"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  8400;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  7300;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  375;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  16075;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  7500;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  6500;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  475;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  14475;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  5650;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  5500;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  575;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  11725;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  4075;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  4300;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  8375;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestUnder6YearOldConversionVanRrRgOptionsNumberedModel()
        {
            const string resourceName = Prefix + ".2007 Ford E150 Vans Econoline Cargo Van.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2007;
            const string expectedMake = "Ford";
            const string expectedModel = "E150 Vans";
            const string expectedSeries = "Econoline";
            const string expectedBodyStyle = "Cargo Van";

            const int mileage = 12345;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"RR", "RG"};

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  12400;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  10200;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  225;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  22825;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  11650;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  9400;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  325;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  21375;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  10000;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  8200;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  425;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  18625;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  7850;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  6800;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  14650;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestExcessiveMileage()
        {
            const string resourceName = Prefix + ".2008 Honda Accord EX 4D Sedan.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2008;
            const string expectedMake = "Honda";
            const string expectedModel = "Accord";
            const string expectedSeries = "EX";
            const string expectedBodyStyle = "4D Sedan";

            const int mileage = 93883;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string>();

            Matrix expectedMatrix = new Matrix();

            //In the display, ExtraClean condition should be N/A for all Valuations due to excessive mileage
            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  null;

            //In the display, Clean condition should be N/A for all Valuations due to excessive mileage
            expectedMatrix[market, Condition.Clean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  null;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  18550;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value = null; //0 fails the test?
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value = -3300;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value = 15250;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value = 16800;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value = null; //0 fails the test?
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  -2850;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value = 13950;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestZeroBaseValue()
        {
            const string resourceName = Prefix + ".2008 Saturn VUE Hybrid 4D Utility FWD.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2008;
            const string expectedMake = "Saturn";
            const string expectedModel = "VUE";
            const string expectedSeries = "Hybrid";
            const string expectedBodyStyle = "4D Utility FWD";

            const int mileage = 9387;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string> {"N5"};

            Matrix expectedMatrix = new Matrix();

            //In the display, Base should be N/A due to excessive mileage
            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  21075;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  700;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  21775;

            //In the display, Base should be N/A due to excessive mileage
            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  19675;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  700;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  150;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  20525;

            expectedMatrix[market, Condition.Average, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Average, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Average, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  0; //null; fails
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  null;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  0; //null; fails
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  null;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  null;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        /// <summary>
        /// FogBugz 10742: https://incisent.fogbugz.com/default.asp?10742#75191
        /// 
        /// This test is due to the fact that the BlackBook documentation pre May 2010 had an ambiguity between which Add/Deduct needed to be treated specially.
        /// 
        /// pre May 2010: RR and RG options or RR and RS options are treated separately.
        /// post May 2010: RR and RG options are treated specially, but NOT RS!
        /// </summary>
        [Test]
        public void TestUnder6YearOldRsOption()
        {
            const string resourceName = Prefix + ".2010 Chevrolet Camaro SS 2D Coupe.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2010;
            const string expectedMake = "Chevrolet";
            const string expectedModel = "Camaro";
            const string expectedSeries = "SS";
            const string expectedBodyStyle = "2D Coupe";

            const int mileage = 5000;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string>();
            codes.Add("20");
            codes.Add("PW");
            codes.Add("12");
            codes.Add("RS");

            Matrix expectedMatrix = new Matrix();

            //In the display, Base should be N/A due to excessive mileage
            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value =  34100;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value =  2200;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value =  36300;

            //In the display, Base should be N/A due to excessive mileage
            expectedMatrix[market, Condition.Clean, Valuation.Base].Value =  33250;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value =  2200;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value =  200;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value =  35650;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value =  31450;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value =  2200;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value =  300;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value =  33950;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value =  29550;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value =  2200;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value =  0;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value =  31750;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        [Test]
        public void TestFiftyPercentMileageAdjust()
        {
            const string resourceName = Prefix + ".2006 Ford Explorer XLT 4D Utility 4WD.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2006;
            const string expectedMake = "Ford";
            const string expectedModel = "Explorer";
            const string expectedSeries = "XLT";
            const string expectedBodyStyle = "4D Utility 4WD";

            const int mileage = 130812;

            const Market market = Market.Wholesale;

            List<string> codes = new List<string>();

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Visibility = Matrix.CellVisibility.NotApplicable;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Visibility = Matrix.CellVisibility.NotApplicable;

            expectedMatrix[market, Condition.ExtraClean, Valuation.Base].Value = 0;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Mileage].Value = null;
            expectedMatrix[market, Condition.ExtraClean, Valuation.Final].Value = null;

            expectedMatrix[market, Condition.Clean, Valuation.Base].Value = 0;
            expectedMatrix[market, Condition.Clean, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.Clean, Valuation.Mileage].Value = null;
            expectedMatrix[market, Condition.Clean, Valuation.Final].Value = null;

            expectedMatrix[market, Condition.Average, Valuation.Base].Value = 10825;
            expectedMatrix[market, Condition.Average, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.Average, Valuation.Mileage].Value = -3625;
            expectedMatrix[market, Condition.Average, Valuation.Final].Value = 7200;

            expectedMatrix[market, Condition.Rough, Valuation.Base].Value = 8675;
            expectedMatrix[market, Condition.Rough, Valuation.Option].Value = null;
            expectedMatrix[market, Condition.Rough, Valuation.Mileage].Value = -3025;
            expectedMatrix[market, Condition.Rough, Valuation.Final].Value = 5650;

            Validate(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, expectedMatrix, codes, mileage, market);
        }

        #endregion

        #region Calculator.CalculateBase(...) Tests

        [Test]
        public static void TestCalculateBaseZeroValue()
        {
            const string resourceName = Prefix + ".1994 Honda Accord LX 4D Wagon.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 1994;
            const string expectedMake = "Honda";
            const string expectedModel = "Accord";
            const string expectedSeries = "LX";
            const string expectedBodyStyle = "4D Wagon";

            List<string> codes = new List<string>();

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[Market.TradeIn, Condition.ExtraClean, Valuation.Base].Value =  null;
            expectedMatrix[Market.TradeIn, Condition.Clean, Valuation.Base].Value =  2155;
            expectedMatrix[Market.TradeIn, Condition.Average, Valuation.Base].Value =  1180;
            expectedMatrix[Market.TradeIn, Condition.Rough, Valuation.Base].Value =  535;

            expectedMatrix[Market.Wholesale, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[Market.Wholesale, Condition.Clean, Valuation.Base].Value =  2100;
            expectedMatrix[Market.Wholesale, Condition.Average, Valuation.Base].Value =  1125;
            expectedMatrix[Market.Wholesale, Condition.Rough, Valuation.Base].Value =  550;

            expectedMatrix[Market.Retail, Condition.ExtraClean, Valuation.Base].Value =  0;
            expectedMatrix[Market.Retail, Condition.Clean, Valuation.Base].Value =  3875;
            expectedMatrix[Market.Retail, Condition.Average, Valuation.Base].Value =  2475;
            expectedMatrix[Market.Retail, Condition.Rough, Valuation.Base].Value =  1675;

            UndefinedDefaultValues(expectedMatrix);

            Common.Core.Tuple<Car, IVehicleConfiguration> carSpec = ReadResource(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, codes, 0);

            Validate(carSpec.First, expectedMatrix);
        }

        [Test]
        public static void TestCalculateBaseNonZeroValue()
        {
            const string resourceName = Prefix + ".2010 Chevrolet Camaro SS 2D Coupe.xml";

            TestXmlService service = (TestXmlService)RegistryFactory.GetResolver().Resolve<IXmlService>();

            service.ResourceName = resourceName;

            const int expectedCarCount = 1;
            const int expectedYear = 2010;
            const string expectedMake = "Chevrolet";
            const string expectedModel = "Camaro";
            const string expectedSeries = "SS";
            const string expectedBodyStyle = "2D Coupe";

            List<string> codes = new List<string>();

            Matrix expectedMatrix = new Matrix();

            expectedMatrix[Market.TradeIn, Condition.ExtraClean, Valuation.Base].Value =  null;
            expectedMatrix[Market.TradeIn, Condition.Clean, Valuation.Base].Value =  33520;
            expectedMatrix[Market.TradeIn, Condition.Average, Valuation.Base].Value =  32025;
            expectedMatrix[Market.TradeIn, Condition.Rough, Valuation.Base].Value =  27770;

            expectedMatrix[Market.Wholesale, Condition.ExtraClean, Valuation.Base].Value =  34100;
            expectedMatrix[Market.Wholesale, Condition.Clean, Valuation.Base].Value =  33250;
            expectedMatrix[Market.Wholesale, Condition.Average, Valuation.Base].Value =  31450;
            expectedMatrix[Market.Wholesale, Condition.Rough, Valuation.Base].Value =  29550;

            expectedMatrix[Market.Retail, Condition.ExtraClean, Valuation.Base].Value =  38350;
            expectedMatrix[Market.Retail, Condition.Clean, Valuation.Base].Value =  37275;
            expectedMatrix[Market.Retail, Condition.Average, Valuation.Base].Value =  35150;
            expectedMatrix[Market.Retail, Condition.Rough, Valuation.Base].Value =  32900;

            UndefinedDefaultValues(expectedMatrix);

            Common.Core.Tuple<Car, IVehicleConfiguration> carSpec = ReadResource(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, codes, 0);

            Validate(carSpec.First, expectedMatrix);
        }

        static void UndefinedDefaultValues(Matrix matrix)
        {
            foreach (Market market in Enum.GetValues(typeof(Market)))
            {
                matrix[market, Condition.Undefined, Valuation.Undefined].Value = null;
            }

            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                matrix[Market.Undefined, condition, Valuation.Undefined].Value = null;
            }

            foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
            {
                matrix[Market.Undefined, Condition.Undefined, valuation].Value = null;
            }
        }

        static void Validate(Car car, Matrix expectedMatrix)
        {
            Matrix actualMatrix = new Matrix();

            BlackBookCalculator calculator = new BlackBookCalculator();

            calculator.CalculateBase(car, actualMatrix);

            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                foreach (Market market in Enum.GetValues(typeof(Market)))
                {
                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        if (market == Market.FinanceAdvance && valuation != Valuation.Final)
                        {
                            continue;
                        }

                        int? expected = expectedMatrix[market, condition, valuation].Value;

                        int? actual = actualMatrix[market, condition, valuation].Value;

                        string message = string.Format(
                            "Market: {0}; Condition: {1}; Valuation: {2};",
                            Enum.GetName(typeof(Market), market),
                            Enum.GetName(typeof(Condition), condition),
                            Enum.GetName(typeof(Valuation), valuation));

                        Assert.AreEqual(expected, actual, message);
                    }
                }
            }
        }

        #endregion

        static void Validate(string resourceName, int expectedCarCount, int expectedYear, string expectedMake, string expectedModel, string expectedSeries, string expectedBodyStyle, Matrix expectedMatrix, IEnumerable<string> codes, int? mileage, Market market)
        {
            UndefinedDefaultValues(expectedMatrix);

            Common.Core.Tuple<Car, IVehicleConfiguration> carSpec = ReadResource(resourceName, expectedCarCount, expectedYear, expectedMake, expectedModel, expectedSeries, expectedBodyStyle, codes, mileage);

            Car car = carSpec.First;

            IVehicleConfiguration configuration = carSpec.Second;

            Matrix matrix = new BlackBookCalculator().Calculate(car, configuration);

            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    Matrix.CellVisibility expectedVisibility = expectedMatrix[market, condition, valuation].Visibility;
                    
                    Matrix.CellVisibility actualVisibility = matrix[market, condition, valuation].Visibility;

                    string message = string.Format(
                        "Market: {0}; Condition: {1}; Valuation: {2};",
                        Enum.GetName(typeof (Market), market),
                        Enum.GetName(typeof (Condition), condition),
                        Enum.GetName(typeof (Valuation), valuation));

                    Assert.AreEqual(expectedVisibility, actualVisibility, message);

                    if (expectedVisibility == Matrix.CellVisibility.Value)
                    {
                        int? expected = expectedMatrix[market, condition, valuation].Value;

                        int? actual = matrix[market, condition, valuation].Value;

                        message = string.Format(
                            "Market: {0}; Condition: {1}; Valuation: {2};",
                            Enum.GetName(typeof (Market), market),
                            Enum.GetName(typeof (Condition), condition),
                            Enum.GetName(typeof (Valuation), valuation));

                        Assert.AreEqual(expected, actual, message);
                    }
                }
            }
        }

        static Common.Core.Tuple<Car, IVehicleConfiguration> ReadResource(string resourceName, int expectedCarCount, int expectedYear, string expectedMake, string expectedModel, string expectedSeries, string expectedBodyStyle, IEnumerable<string> codes, int? mileage)
        {
            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);

                Assert.AreEqual(expectedCarCount, cars.Values.Count);

                //if multiple cars, pick the first one for convenience
                CarInfo info = cars.Values[0];

                Assert.IsNotNull(info);

                Assert.AreEqual(expectedYear, info.Year);
                Assert.AreEqual(expectedMake, info.Make);
                Assert.AreEqual(expectedModel, info.Model);
                Assert.AreEqual(expectedSeries, info.Series);
                Assert.AreEqual(expectedBodyStyle, info.BodyStyle);

                Car car = info as Car;

                Assert.IsNotNull(car);

                BlackBookConfiguration c = new BlackBookConfiguration();

                IVehicleConfiguration x = c.InitialConfiguration(info.Uvc, info.Vin, null, mileage);

                foreach (string code in codes)
                {
                    string s = code; // no warnings please

                    Option o = car.Options.First(y => Equals(y.AdCode, s));

                    x = c.UpdateConfiguration(x, new OptionAction {ActionType = OptionActionType.Add, Uoc = o.UniversalOptionCode});
                }

                return new Common.Core.Tuple<Car, IVehicleConfiguration>(car, x);
            }
        }

        static Stream GetResourceFromAssembly(string resourceName)
        {
            Assembly a = Assembly.GetExecutingAssembly();

            return a.GetManifestResourceStream(resourceName);
        }

        public class TestXmlService : IXmlService
        {
            private string _resourceName;

            public string ResourceName
            {
                get { return _resourceName; }
                set { _resourceName = value; }
            }

            public XmlReader Query(int year, string make)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Query(int year, string make, string model)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Query(int year, string make, string model, XmlAdjustment adjustment)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Query(int year, string make, string model, string series)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Query(int year, string make, string model, string series, XmlAdjustment adjustment)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Vin(string vin)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Vin(string vin, XmlAdjustment adjustment)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Uvc(string uvc)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader Uvc(string uvc, XmlAdjustment adjustment)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }

            public XmlReader StandardEquipment(string uvc)
            {
                return XmlReader.Create(GetResourceFromAssembly(ResourceName));
            }
        }

        public class TestSqlService : ISqlService
        {
            private DataSet _set;

            public DataSet Set
            {
                get
                {
                    if (_set == null)
                    {
                        throw new NotSupportedException("Not configured");
                    }
                    return _set;
                }
                set { _set = value; }
            }

            public IDataReader BookDate()
            {
                return Set.Tables["BookDate"].CreateDataReader();
            }

            public IDataReader Query()
            {
                return Set.Tables["Year"].CreateDataReader();
            }

            public IDataReader Query(int year)
            {
                return Set.Tables["Make"].CreateDataReader();
            }
        }
    }
}