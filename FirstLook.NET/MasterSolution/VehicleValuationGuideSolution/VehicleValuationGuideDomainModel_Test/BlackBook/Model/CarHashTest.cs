﻿using System.Drawing;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Model
{
    [TestFixture]
    public class CarHashTest
    {
        /// <summary>
        /// Test that the order of a car's mileage adjustments don't have any bearing on the hash.
        /// </summary>
        [Test]
        public void MileageAdjustmentOrderTest()
        {            
            MileageAdjustment adjustment1 = new MileageAdjustment
                                            {
                                                Average        = 10000,
                                                BeginRange     = 11000,
                                                Clean          = 12000,
                                                EndRange       = 99000,
                                                ExtraClean     = 13000,
                                                FinanceAdvance = 25000,
                                                Rough          = 14000
                                            };

            MileageAdjustment adjustment2 = new MileageAdjustment
                                            {
                                                Average        = 14000,
                                                BeginRange     = 25000,
                                                Clean          = 13000,
                                                EndRange       = 10000,
                                                ExtraClean     = 12000,
                                                FinanceAdvance = 11000,
                                                Rough          = 10000
                                            };


            Car car1 = new Car();
            car1.MileageAdjustments.Add(adjustment1);
            car1.MileageAdjustments.Add(adjustment2);

            Car car2 = new Car();
            car2.MileageAdjustments.Add(adjustment2);
            car2.MileageAdjustments.Add(adjustment1);

            ulong hash1 = car1.Hash();
            ulong hash2 = car2.Hash();

            Assert.AreEqual(hash1, hash2);
        }

        /// <summary>
        /// Test that the order of a car's options don't have any bearing on the hash.
        /// </summary>
        [Test]
        public void OptionOrderTest()
        {
            Option option1 = new Option
                             {
                                 AdCode = "XX",
                                 Amount = 12345,
                                 Description = "Description 1",
                                 Flag = true,
                                 OptionAdjustmentSign = OptionAdjustmentSign.Add,
                                 Residual = new ResidualValue
                                            {
                                                Resid12 = 10000,
                                                Resid24 = 20000,
                                                Resid30 = 30000,
                                                Resid36 = 40000,
                                                Resid42 = 50000,
                                                Resid48 = 60000,
                                                Resid60 = 70000,
                                                Resid72 = 80000
                                            }
                             };

            Option option2 = new Option
                             {
                                 AdCode = "XY",
                                 Amount = 2356,
                                 Description = "Description 2",
                                 Flag = false,
                                 OptionAdjustmentSign = OptionAdjustmentSign.Add,
                                 Residual = new ResidualValue
                                            {
                                                Resid12 = 10001,
                                                Resid24 = 20002,
                                                Resid30 = 30003,
                                                Resid36 = 40004,
                                                Resid42 = 50005,
                                                Resid48 = 60006,
                                                Resid60 = 70007,
                                                Resid72 = 80008
                                            }
                             };

            Car car1 = new Car();
            car1.Options.Add(option1);
            car1.Options.Add(option2);

            Car car2 = new Car();
            car2.Options.Add(option2);
            car2.Options.Add(option1);

            ulong hash1 = car1.Hash();
            ulong hash2 = car2.Hash();

            Assert.AreEqual(hash1, hash2);
        }

        /// <summary>
        /// Test that the order of a car's colors don't have any bearing on the hash.
        /// </summary>
        [Test]
        public void ColorOrderTest()
        {
            System.Drawing.Color swatch1 = ColorTranslator.FromHtml("Blue");
            System.Drawing.Color swatch2 = ColorTranslator.FromHtml("Red");
            System.Drawing.Color swatch3 = ColorTranslator.FromHtml("Black");

            BlackBook.Model.Color color1 = new BlackBook.Model.Color
                           {
                               Category = "QWERTY",
                               Description = "POIUYT",
                               Swatches = { swatch1, swatch2, swatch3 }                               
                           };

            BlackBook.Model.Color color2 = new BlackBook.Model.Color
                           {
                               Category = "sdfdsf",
                               Description = "sdt345t6",
                               Swatches = { swatch3, swatch2, swatch3, swatch1 }                               
                           };

            BlackBook.Model.Color color3 = new BlackBook.Model.Color
                           {
                               Category = "sfs;j",
                               Description = "s234234"
                           };

            Car car1 = new Car();
            car1.Colors.Add(color1);
            car1.Colors.Add(color2);
            car1.Colors.Add(color3);

            Car car2 = new Car();
            car2.Colors.Add(color1);
            car2.Colors.Add(color3);
            car2.Colors.Add(color2);

            Car car3 = new Car();
            car3.Colors.Add(color3);
            car3.Colors.Add(color1);
            car3.Colors.Add(color2);

            ulong hash1 = car1.Hash();
            ulong hash2 = car2.Hash();
            ulong hash3 = car3.Hash();

            Assert.AreEqual(hash1, hash2);
            Assert.AreEqual(hash2, hash3);
            Assert.AreEqual(hash1, hash3);
        }
    }
}
