﻿using System.Xml;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    [TestFixture]
    public class XmlServiceTest
    {
        [Test]
        public void Test2009AudiA8L()
        {
            XmlService service = new XmlService();

            XmlReader reader = service.Query(2009, "Audi", "A8", "L");

            XmlDocument document = new XmlDocument();

            document.Load(reader);
        }
    }
}
