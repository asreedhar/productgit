﻿using System.IO;
using System.Reflection;
using System.Xml;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    [TestFixture]
    public class XmlSerializerTest
    {
        private const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml.Resources";

        [Test]
        public void ParseQueryByUvc()
        {
            const string resourceName = Prefix + ".QueryByUvc.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void ParseQueryByVin()
        {
            const string resourceName = Prefix + ".QueryByVin.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void ParseQueryByYearMake()
        {
            const string resourceName = Prefix + ".QueryByYearMake.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void ParseQueryByYearMakeModel()
        {
            const string resourceName = Prefix + ".QueryByYearMakeModel.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void ParseQueryByYearMakeModelSeries()
        {
            const string resourceName = Prefix + ".QueryByYearMakeModelSeries.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void Parse2005AudiA8L()
        {
            const string resourceName = Prefix + ".2005_Audi_A8_L.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void Parse2009AudiA8L()
        {
            const string resourceName = Prefix + ".2009_Audi_A8_L.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);
            }
        }

        [Test]
        public void ParseQueryByUvcStandardEquipment()
        {
            const string resourceName = Prefix + ".QueryByUvcStandardEquipment.xml";

            StandardEquipmentSerializer ser = new StandardEquipmentSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                StandardEquipment equipment = ser.Deserialize(reader);

                Assert.IsNotNull(equipment);
            }
        }

        [Test]
        public void Parse1999AcuraIntegraGs()
        {
            const string resourceName = Prefix + ".1999_Acura_Integra_GS.xml";

            CarsSerializer ser = new CarsSerializer();

            using (XmlReader reader = XmlReader.Create(GetResourceFromAssembly(resourceName)))
            {
                Cars cars = ser.Deserialize(reader);

                Assert.IsNotNull(cars);

                Assert.AreEqual(1, cars.Values.Count);

                Car car = cars.Values[0] as Car;

                Assert.IsNotNull(car);

                Assert.AreEqual(1999, car.Year);
                Assert.AreEqual("Acura", car.Make);
                Assert.AreEqual("Integra", car.Model);
                Assert.AreEqual("GS", car.Series);
                Assert.AreEqual("4D Sedan", car.BodyStyle);
                Assert.AreEqual("JH4DB766X", car.Vin);
                Assert.AreEqual("1999020009", car.Uvc);

                Assert.AreEqual(22200, car.Msrp);
                Assert.AreEqual(3225, car.FinanceAdvance);

                Assert.AreEqual(10, car.MileageAdjustments.Count);

                Assert.AreEqual("AT AC", car.PriceIncludes);

                Assert.AreEqual(0, car.Options.Count);

                Assert.AreEqual("103.1", car.WheelBase);
                Assert.AreEqual("16.3", car.TaxableHorsepower);
                Assert.AreEqual("2727", car.Weight);
                Assert.AreEqual("195/55R15", car.TireSize);
                Assert.AreEqual("140 @ 6300", car.BaseHorsepower);
                Assert.AreEqual("Gas", car.FuelType);
                Assert.AreEqual("4", car.Cylinders);
                Assert.AreEqual("FWD", car.DriveTrain);
                Assert.AreEqual("A", car.Transmission);
                Assert.AreEqual("1.8L I-4 PFI DOHC ", car.Engine);
                Assert.AreEqual("Sporty Car", car.VehicleClass);
            }
        }

        static Stream GetResourceFromAssembly(string resourceName)
        {
            Assembly a = Assembly.GetExecutingAssembly();

            return a.GetManifestResourceStream(resourceName);
        }
    }
}
