using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using NUnit.Framework;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Test.Commands.Impl
{
    [TestFixture]
    public class MapperTests
    {
        [Test]
        public void TestCellVisibilityValueGetPriceValue()
        {
            Random random = new Random();
            TestScript((market, condition, valuation, expectedMatrix, expectedValues) =>
            {
                int value = random.Next();
                expectedMatrix[market, condition, valuation].Value = value;
                expectedMatrix[market, condition, valuation].Visibility = Matrix.CellVisibility.Value;

                expectedValues.Add(new ImmutableTriple(market, condition, valuation), value);
            });
        }

        [Test]
        public void TestCellVisibilityNotDisplayedGetPriceValue()
        {
            Random random = new Random();
            TestScript((market, condition, valuation, expectedMatrix, expectedValues) =>
            {
                int value = random.Next();
                expectedMatrix[market, condition, valuation].Value = value;
                expectedMatrix[market, condition, valuation].Visibility = Matrix.CellVisibility.NotDisplayed;

                expectedValues.Add(new ImmutableTriple(market, condition, valuation), null);
            });
        }

        [Test]
        public void TestCellVisibilityNotApplicableGetPriceValue()
        {
            Random random = new Random();
            TestScript((market, condition, valuation, expectedMatrix, expectedValues) =>
           {
               int value = random.Next();
               expectedMatrix[market, condition, valuation].Value = value;
               expectedMatrix[market, condition, valuation].Visibility =
                   Matrix.CellVisibility.NotApplicable;

               expectedValues.Add(new ImmutableTriple(market, condition, valuation), null);
           });
        }

        private delegate void PopluateExpectedValues(Market market, Condition condition, Valuation valuation, Matrix expectedMatrix, IDictionary<ImmutableTriple, int?> expectedValues);

        private static void TestScript(PopluateExpectedValues popFunc)
        {         
            IDictionary<ImmutableTriple, int?> expectedValues = new Dictionary<ImmutableTriple, int?>();

            Matrix expectedMatrix = new Matrix();

            foreach (Market market in Enum.GetValues(typeof(Market)))
            {
                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {
                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        popFunc.Invoke(market, condition, valuation, expectedMatrix, expectedValues);
                    }
                }
            }

            Validate(expectedMatrix, expectedValues);
        }

        private static void Validate(Matrix expectedMatrix, IDictionary<ImmutableTriple, int?> expectedValues)
        {
            foreach (Market market in Enum.GetValues(typeof(Market)))
            {
                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {
                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        int? actual = Mapper.GetPriceValue(expectedMatrix, market, condition, valuation);

                        Assert.AreEqual(expectedValues[new ImmutableTriple(market, condition, valuation)], actual);
                    }
                }
            }
        }

        /// <summary>
        /// Object used as a Dictionary Key
        /// </summary>
        private class ImmutableTriple
        {
            private readonly Market _market;
            private readonly Condition _condition;
            private readonly Valuation _valuation;

            public Market Market
            {
                get { return _market; }
            }

            public Condition Condition
            {
                get { return _condition; }
            }

            public Valuation Valuation
            {
                get { return _valuation; }
            }

            public ImmutableTriple(Market market, Condition condition, Valuation valuation)
            {
                _market = market;
                _condition = condition;
                _valuation = valuation;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != typeof(ImmutableTriple)) return false;
                return Equals((ImmutableTriple)obj);
            }

            public bool Equals(ImmutableTriple other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Equals(other._market, _market) && Equals(other._condition, _condition) && Equals(other._valuation, _valuation);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int result = _market.GetHashCode();
                    result = (result * 397) ^ _condition.GetHashCode();
                    result = (result * 397) ^ _valuation.GetHashCode();
                    return result;
                }
            }
        }
    }
}
