﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Test.Data
{
    class Program
    {
        private delegate void Write(string format, params object[] args);

        private delegate string Prompt(string message);

        private static readonly Write W = Console.WriteLine;

        private static readonly Prompt P = delegate(string s) { Console.Write(s); return Console.ReadLine(); };

        static void Main()
        {
            IModule[] items = new IModule[]
                                  {
                                      new KelleyBlueBookModule()
                                      ,new NadaModule()
                                      ,new GalvesModule()
                                      ,new EdmundsModule()
                                      ,new BlackBookModule()
                                  };

            W("Valuation Guide");
            W("---------------");
            W("");

            for (int i = 0; i < items.Length; i++)
            {
                W("{0}) {1}", (i + 1), items[i].Name);
            }

            W("");

            int index;

            do
            {
                string n = P("Selection: ");

                if (int.TryParse(n, out index))
                {
                    index--;

                    if (index >= 0 && index < items.Length)
                    {
                        break;
                    }
                }
            }
            while (true);

            W("");

            items[index].Run();
        }

        interface IModule
        {
            string Name { get; }

            void Run();
        }

        class KelleyBlueBookModule : IModule
        {
            public string Name
            {
                get { return "Kelley Blue Book"; }
            }

            public void Run()
            {
                string zipCode = null;

                while (string.IsNullOrEmpty(zipCode))
                {
                    zipCode = P("1) Zip: ");
                }

                W("");

                int id = 0;

                while (id == 0)
                {
                    string text = P("2) Vehicle: ");

                    int.TryParse(text, out id);
                }

                W("");

                string vin = P("3) VIN: ");


                IRegistry registry = RegistryFactory.GetRegistry();
                registry.Register<KelleyBlueBook.Model.Sql.Module>();
                TestCloneReaderKbb cloneReader = new TestCloneReaderKbb();
                KelleyBlueBook.Model.Sql.CurrentSqlService service = new KelleyBlueBook.Model.Sql.CurrentSqlService(cloneReader); 


                KelleyBlueBook.Model.Sql.ISqlSerializer<KelleyBlueBook.Model.BookDate> bookDateSerializer = registry.Resolve<KelleyBlueBook.Model.Sql.ISqlSerializer<KelleyBlueBook.Model.BookDate>>();
                IDataReader bookDateReader = service.BookDate();
                int bookDate = bookDateReader.Read() ? bookDateSerializer.Deserialize((IDataRecord)bookDateReader).Id : default(int);


                KelleyBlueBook.Model.Sql.ISqlSerializer<KelleyBlueBook.Model.Vehicle> vehicleSerializer = registry.Resolve<KelleyBlueBook.Model.Sql.ISqlSerializer<KelleyBlueBook.Model.Vehicle>>();
                IDataReader vehicleReader = service.Vehicle(bookDate, id);
                KelleyBlueBook.Model.Vehicle vehicle = vehicleSerializer.DeserializeGraph(vehicleReader);


                KelleyBlueBook.Model.Sql.ISqlSerializer<KelleyBlueBook.Model.Region> regionSerializer = registry.Resolve<KelleyBlueBook.Model.Sql.ISqlSerializer<KelleyBlueBook.Model.Region>>();
                IDataReader regionsReader = service.Regions(bookDate);
                KelleyBlueBook.Model.Region nationBaseRegion =
                    regionSerializer.Deserialize(regionsReader).DefaultIfEmpty(null).Single(r => r.Type == Region.RegionType.NationalBase);


                service.Region(bookDate, zipCode);
                service.BasePrices(bookDate, nationBaseRegion, id);
                service.OptionAdjustments(bookDate, id);
                service.MileageAdjustment(bookDate, vehicle.VehicleType, vehicle.Year.Id, vehicle.MileageGroupId);
                service.RegionAdjustments(bookDate, id);
                

                if (!string.IsNullOrEmpty(vin))
                {
                    service.DefaultOptions(bookDate, id, vin);
                }

                string filename = string.Format("{0} {1} {2} {3}.xml",
                              vehicle.Year.Name,
                              vehicle.Make.Name,
                              vehicle.Model.Name,
                              vehicle.Trim.Name);

                using (FileStream stream = File.Create(filename))
                {
                    cloneReader.Set.WriteXml(stream, XmlWriteMode.WriteSchema);
                }
            }
        }

        class NadaModule : IModule
        {
            public string Name
            {
                get { return "NADA"; }
            }

            public void Run()
            {
                IRegistry registry = RegistryFactory.GetRegistry();

                registry.Register<Nada.Model.Sql.Module>();

                Nada.Model.Sql.CurrentSqlService service = new Nada.Model.Sql.CurrentSqlService();

                Nada.Model.Sql.ISqlSerializer<Nada.Model.State> stateSerializer = registry.Resolve<Nada.Model.Sql.ISqlSerializer<Nada.Model.State>>();

                Nada.Model.Sql.ISqlSerializer<Nada.Model.Period> periodSerializer = registry.Resolve<Nada.Model.Sql.ISqlSerializer<Nada.Model.Period>>();

                IDataReader periodReader = service.Query();

                int period = periodSerializer.Deserialize(periodReader).Max(x => x.Id);

                IDataReader stateReader = service.States(period);

                IList<Nada.Model.State> states = stateSerializer.Deserialize(stateReader);

                Nada.Model.State state = null;

                while (state == null)
                {
                    string code = P("1) State: ");

                    state = states.First(x => Equals(x.Code, code));
                }

                W("");

                int id = 0;

                while (id == 0)
                {
                    string text = P("2) Vehicle: ");

                    int.TryParse(text, out id);
                }

                W("");

                string vin = P("3) VIN: ");

                Nada.Model.Sql.ISqlSerializer<Nada.Model.Region> regionSerializer = registry.Resolve<Nada.Model.Sql.ISqlSerializer<Nada.Model.Region>>();

                IDataReader regionReader = service.Region(period, state.Name);

                Nada.Model.Region region = regionSerializer.Deserialize(regionReader).First();

                Nada.Model.Sql.ISqlSerializer<Nada.Model.Vehicle> vehicleSerializer = registry.Resolve<Nada.Model.Sql.ISqlSerializer<Nada.Model.Vehicle>>();

                IDataReader vehicleReader = service.Vehicle(period, id);

                Nada.Model.Vehicle vehicle = vehicleSerializer.Deserialize(vehicleReader).First();

                DataSet set = service.Snapshot(id, region, vin);

                string filename = string.Format("{0} {1} {2} {3}.xml",
                              vehicle.Year.Name,
                              vehicle.Make.Name,
                              vehicle.Series.Name,
                              vehicle.Body.Name);

                using (FileStream stream = File.Create(filename))
                {
                    set.WriteXml(stream, XmlWriteMode.WriteSchema);
                }
            }
        }

        class GalvesModule : IModule
        {
            public string Name
            {
                get { return "Galves"; }
            }

            public void Run()
            {
                IRegistry registry = RegistryFactory.GetRegistry();

                registry.Register<Galves.Model.Sql.Module>();

                TestCloneReaderGalves cloneReader = new TestCloneReaderGalves();

                Galves.Model.Sql.CurrentSqlService service = new Galves.Model.Sql.CurrentSqlService(cloneReader);

                Galves.Model.Sql.ISqlSerializer<Galves.Model.BookDate> bookDateSerializer = registry.Resolve<Galves.Model.Sql.ISqlSerializer<Galves.Model.BookDate>>();

                IList<Galves.Model.BookDate> bookDates = bookDateSerializer.Deserialize(service.BookDate());

                int bookDate = bookDates.Max(x => x.Id);

                Galves.Model.Sql.ISqlSerializer<Galves.Model.State> stateSerializer = registry.Resolve<Galves.Model.Sql.ISqlSerializer<Galves.Model.State>>();

                IList<Galves.Model.State> states = stateSerializer.Deserialize(service.States(bookDate));

                Galves.Model.State state = null;

                while (state == null)
                {
                    string name = P("1) State: ");

                    state = states.FirstOrDefault(x => Equals(x.Name, name));
                }

                W("");

                int id = 0;

                while (id == 0)
                {
                    string text = P("2) Vehicle: ");

                    int.TryParse(text, out id);
                }

                Galves.Model.Sql.ISqlSerializer<Galves.Model.Vehicle> vehicleSerializer = registry.Resolve<Galves.Model.Sql.ISqlSerializer<Galves.Model.Vehicle>>();

                IDataReader vehicleReader = service.Vehicle(bookDate, id);

                Galves.Model.Vehicle vehicle = vehicleSerializer.Deserialize(vehicleReader).First();

                service.BookDate();

                service.Adjustments(bookDate, id);

                service.AdjustmentExceptions(bookDate, id);

                service.MileageAdjustments(bookDate, vehicle.MileageYear, vehicle.MileageGroupCode);

                service.RegionalMileageAdjustments(bookDate, vehicle.MileageYear, vehicle.MileageClassification);

                string filename = string.Format("{0} {1} {2} {3}.xml",
                              vehicle.Year.Value,
                              vehicle.Manufacturer.Value,
                              vehicle.Model.Value,
                              vehicle.Style.Value.Replace('/','-'));

                using (FileStream stream = File.Create(filename))
                {
                    cloneReader.Set.WriteXml(stream, XmlWriteMode.WriteSchema);
                }
            }
        }

        class EdmundsModule : IModule
        {
            public string Name
            {
                get { return "Edmunds"; }
            }

            public void Run()
            {
                IRegistry registry = RegistryFactory.GetRegistry();

                registry.Register<Edmunds.Model.Sql.Module>();

                TestCloneReaderEdmunds cloneReader = new TestCloneReaderEdmunds();

                Edmunds.Model.Sql.CurrentSqlService service = new Edmunds.Model.Sql.CurrentSqlService(cloneReader);

                Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.DataLoad> dataLoadDateSerializer = registry.Resolve<Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.DataLoad>>();

                IList<Edmunds.Model.DataLoad> dataLoads = dataLoadDateSerializer.Deserialize(service.DataLoad());

                int dataLoad = dataLoads.Max(x => x.Id);

                IEnumerable<Edmunds.Model.State> states = Edmunds.Model.State.Values;

                Edmunds.Model.State state = null;

                while (state == null)
                {
                    string code = P("1) State: ");

                    state = states.FirstOrDefault(x => Equals(x.Code, code));
                }

                W("");

                int id = 0;

                while (id == 0)
                {
                    string text = P("2) Style: ");

                    int.TryParse(text, out id);
                }

                Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.Vehicle> vehicleSerializer = registry.Resolve<Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.Vehicle>>();

                IDataReader vehicleReader = service.Vehicle(dataLoad, id);

                Edmunds.Model.Vehicle vehicle = vehicleSerializer.Deserialize(vehicleReader).First();

                Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.Region> regionSerializer = registry.Resolve<Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.Region>>();

                IDataReader regionReader = service.Region(dataLoad, state.Code);

                Edmunds.Model.Region region = regionSerializer.Deserialize(regionReader).First();

                service.Options(dataLoad, id);

                Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.ManufacturerColor> colorSerializer = registry.Resolve<Edmunds.Model.Sql.ISqlSerializer<Edmunds.Model.ManufacturerColor>>();

                IDataReader colorReader = service.Colors(dataLoad, id);

                IList<Edmunds.Model.ManufacturerColor> colors = colorSerializer.Deserialize(colorReader);

                foreach (Edmunds.Model.ManufacturerColor color in colors)
                {
                    W(string.Format("({0}) {1}", color.GenericColor.Id, color.Name));
                }

                int colorId = 0;

                while (colorId == 0)
                {
                    string text = P("Color: ");

                    int value;

                    if (int.TryParse(text, out value))
                    {
                        if (colors.Any(x => x.GenericColor.Id == value))
                        {
                            colorId = value;

                            break;
                        }
                    }
                }

                service.ColorAdjustments(dataLoad, id, region.Id, colorId);

                service.RegionAdjustments(dataLoad, id, region.Id);

                service.ConditionAdjustments(dataLoad, id);

                service.MileageAdjustments(dataLoad, id);

                service.CertifiedCost(dataLoad, id);

                service.CertifiedCriteria(dataLoad, id);

                service.CertifiedMileageAdjustments(dataLoad, id);

                string filename = string.Format("{0} {1} {2} {3}.xml",
                              vehicle.Year.Value,
                              vehicle.Make.Value,
                              vehicle.Model.Value,
                              vehicle.Style.Value.Replace('/', '-'));

                using (FileStream stream = File.Create(filename))
                {
                    cloneReader.Set.WriteXml(stream, XmlWriteMode.WriteSchema);
                }
            }
        }

        class BlackBookModule : IModule
        {
            public string Name
            {
                get { return "Black Book"; }
            }

            public void Run()
            {
                IRegistry registry = RegistryFactory.GetRegistry();

                registry.Register<BlackBook.Model.Sql.Module>();

                TestCloneReaderBlackBook cloneReader = new TestCloneReaderBlackBook();

                BlackBook.Model.Sql.SqlService service = new BlackBook.Model.Sql.SqlService(cloneReader);

                service.BookDate();

                service.Query();

                service.Query(2010);

                using (FileStream stream = File.Create("BlackBook.xml"))
                {
                    cloneReader.Set.WriteXml(stream, XmlWriteMode.WriteSchema);
                }
            }
        }

        class TestCloneReaderKbb : TestCloneReader, KelleyBlueBook.Model.Sql.ICloneReader
        {
            
        }

        class TestCloneReaderGalves : TestCloneReader, Galves.Model.Sql.ICloneReader
        {

        }

        class TestCloneReaderEdmunds : TestCloneReader, Edmunds.Model.Sql.ICloneReader
        {

        }

        class TestCloneReaderBlackBook : TestCloneReader, BlackBook.Model.Sql.ICloneReader
        {

        }

        class TestCloneReader
        {
            private readonly DataSet _set;

            protected TestCloneReader()
            {
                _set = new DataSet();
            }

            public DataSet Set
            {
                get { return _set; }
            }

            public virtual IDataReader Snapshot(IDbCommand command, params string[] names)
            {
                List<DataTable> tables = new List<DataTable>();

                using (IDataReader reader = command.ExecuteReader())
                {
                    foreach (string name in names)
                    {
                        DataTable table = new DataTable(name);

                        table.Load(reader);

                        _set.Tables.Add(table);

                        tables.Add(table);
                    }
                }

                return _set.CreateDataReader(tables.ToArray());
            }
        }
    }
}