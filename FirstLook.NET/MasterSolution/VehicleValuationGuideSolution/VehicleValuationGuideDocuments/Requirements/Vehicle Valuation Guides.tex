%% LyX 1.6.9 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{listings}
\usepackage{prettyref}
\usepackage{framed}
\usepackage{graphicx}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\newenvironment{lyxlist}[1]
{\begin{list}{}
{\settowidth{\labelwidth}{#1}
 \setlength{\leftmargin}{\labelwidth}
 \addtolength{\leftmargin}{\labelsep}
 \renewcommand{\makelabel}[1]{##1\hfil}}}
{\end{list}}

\makeatother

\usepackage{babel}

\begin{document}

\title{Vehicle Valuation Guides}


\author{Simon Wenmouth}


\date{2011-03-08}
\maketitle
\begin{abstract}
Within the Automotive industry the use of vehicle valuation guides
is pervasive and this is the technical and business documentation
for those guides and their use in the First Look system.
\end{abstract}

\section*{Introduction}

In the USA there are but a few companies that periodically publish
vehicle valuation guides of which the most popular and respected are:
\begin{itemize}
\item Black Book USA
\item Kelley Blue Book
\item Galves
\item NADA
\end{itemize}
Each publisher has their own representation of a vehicle and the associated
valuation information. Although the specifics vary by guide, a valuation
is formed by taking a base value and adding a number of adjustments.
\begin{itemize}
\item Equipment Adjustments
\item Mileage Adjustments
\item Regional Adjustments
\end{itemize}
A guide usually provides multiple valuations for a vehicle. Those
valuations are characterised (broadly speaking) by
\begin{itemize}
\item Market
\item Vehicle Condition
\end{itemize}
Market is the sales channel for which the valuation is valid. Example
markets are
\begin{itemize}
\item Retail
\item Loan
\item Trade In
\item Wholesale
\item Auction
\end{itemize}
Vehicle condition, as characterised by the guide, is the physical
state of an automobile as regards to the wear-and-tear it has experienced
(with regards to the norm).
\begin{itemize}
\item Extra Clean, Clean, Average Rough
\item Excellent, Good, Fair, Rough
\item And so on ...
\end{itemize}
There is a great deal of similarity between the guides and an attempt
to normalize the terminology where appropriate will be made. However,
the guides are not identical and any attempt to make them so would
repeat the modelling mistakes of the past.


\subsection*{Usage of Vehicle Valuation Guides}

A dealership will use one (or more) of the valuations provided by
a guide as part of the normal course of business. Different uses require
different guides and different valuations.
\begin{itemize}
\item Purchasing

\begin{itemize}
\item Retail

\begin{itemize}
\item Trade In
\item Purchase
\end{itemize}
\item Wholesale

\begin{itemize}
\item Auction
\end{itemize}
\end{itemize}
\item Loan
\item Valuation versus Cost
\end{itemize}
One of the defects in the design of the original system stemmed from
a mis-modelling of how the valuation guides are utilized. Instead
of mapping a usage to a guide and one of its valuations the guides
were designated {}``primary'' and another {}``secondary''. For
each of the selected guides one valuation was designated {}``primary''
and {}``secondary'' too. The primary selection was the preferred
guide for any given purpose be it appraising a vehicle or determining
a vehicles loan value. As is obvious today, this system is deficient
to the needs of dealers as one would use different guides for appraising
vehicles for trade-in to the guide that established the loan value
of an item of stock. To make matters worse the legacy system was (artificially)
restricted to the use of two valuation guides.

Moving forward the system should allow a dealership to designate a
guide and one of its valuations per use-case as well as removing the
restriction to two guides.


\section*{Black Book USA}

Black Book USA is a valuation guide published by Hearst Corporation
with new data publications on both a daily and weekly basis. The data
published by Black Book contains both vehicle-prototypes (which they
call {}``cars'') as well as the pricing information per vehicle-prototype.


\subsection*{Cars}

The Black Book data is organized in the following hierarchy: Year
> Make > Model > Series > Body Style > Car (\prettyref{fig:Black-Book-USA}).
Within Black Book the natural identity of a car is a {}``Universal
Vehicle Code'' or {}``UVC''. There are two methods to identify
a {}``car'' within the Black Book valuation guide.
\begin{enumerate}
\item Manual Traversal
\item VIN
\end{enumerate}
%
\begin{figure}
\caption{\label{fig:Black-Book-USA}Black Book USA Hierarchy}


\includegraphics[angle=90,height=7in]{\string"Vehicle Valuation Guides - Black Book\string".png}%
\end{figure}



\paragraph*{Manual Traversal}

In this method a user would begin by selecting the model-year for
the vehicle of interest. Once a model-year is selected a list of makes
is made available. The selection of a make produces a list of models
(and so on) until an individual car is identified.


\paragraph*{VIN}

In this method a user supplied VIN is mapped by Black Book to zero
or more cars.
\begin{lyxlist}{00.00.0000}
\item [{Zero}] The VIN is invalid or currently not valued by Black Book.
\item [{One}] The VIN maps to a single car in the Black Book hierarchy.
\item [{Many}] The VIN maps to one of many possible cars. It is common
(but not required) that each of the returned cars have identical ancestors.
The user must perform the selection of the correct vehicle from the
collection.
\end{lyxlist}

\subsubsection*{Example Car}

%
\begin{framed}%
\lstinputlisting[caption={Black Book - Example Car},label={Black Book - Example Car}]{Vehicle Valuation Guides - Black Book.yml}\end{framed}


\subsection*{Vehicle Configuration}

Once a car has been identified we have access to the information from
which a valuation for a physical vehicle can be derived. To produce
such a valuation a description of the vehicles configuration is required.
\begin{lyxlist}{00.00.0000}
\item [{UVC}] The identity of the correct Black Book car.
\item [{State}] The state of USA for the valuation (optional).
\item [{Mileage}] The mileage of the vehicle (optional).
\item [{VIN}] The VIN of the vehicle (optional).
\item [{Actions}] A list of the actions (addition/removal) the user user
took upon the optional equipment and accessories presented as being
available on the vehicle.
\end{lyxlist}
When the state is not present in the vehicle configuration then Black
Book provides a {}``national'' valuation (i.e. it is not regionally
adjusted). When the mileage is not provided then the resulting valuation
has no mileage-adjustment (i.e. the valuation is valid for vehicles
whose odometer reading is within the zero-point mileage-range). When
the VIN is not provided then the valuation has no options are selected
on behalf of the user (as a UVC/VIN combination may be recorded as
having those options installed by default).

To establish some terminology we shall distinguish between two types
of configuration.
\begin{lyxlist}{00.00.0000}
\item [{Initial}] The list of user actions is empty.
\item [{Adjusted}] The user has added (or removed) one or more options
to the configuration (noted as actions).
\end{lyxlist}
To be thorough, it should be noted that the configuration as described
above only lists the actions the user has taken on the vehicle (equipment
adjustments). It does not state which options are (and are not) present
on the real-life vehicle. As such it needs to be amended to add the
option states.
\begin{lyxlist}{00.00.0000}
\item [{States}] A complete list of the option states \{option code: string,
selected: boolean\} for the configuration reflecting each of the actions
taken from the initial configuration.
\end{lyxlist}

\subsection*{Vehicle Valuation}

Once we have a vehicle configuration the different valuations can
be calculated as defined by Black Book (logically a function of the
form $calculate(car,configuration)\rightarrow valuations$). Black
Book provides valuations for the following dimensions
\begin{itemize}
\item Market

\begin{itemize}
\item Retail
\item Wholesale
\item Trade-In
\end{itemize}
\item Condition

\begin{itemize}
\item Extra-clean
\item Clean
\item Average
\item Rough
\end{itemize}
\item Valuation

\begin{itemize}
\item Base Price
\item Mileage Adjustment
\item Option Adjustment
\item Final Price
\end{itemize}
\end{itemize}
Black Book usually provides a price for each triplet \{Market, Condition,
Valuation\}. However, depending on the vehicle and its configuration
it is possible for some price-cells to be \emph{null} (no price).
An example of this is when a vehicle has excessive mileage it cannot
have an extra-clean valuation.

Continuing with the terminology for configuration, a valuation for
an initial-configuration is called an {}``initial valuation'' and
similarly {}``adjusted valuation'' for an adjusted-configuration.


\subsection*{Business Requirements}
\begin{enumerate}
\item The system must allow dealerships to {[}un{]} subscribe to the Black
Book USA valuation guide.
\item The system must provide a report of the dealerships who have made
use of the Black Book valuation guide within a given time period (billing
window).
\item The system must allow a dealership to identify their default state
for regional adjustments within the Black Book valuation guide.
\item The system must allow a user to override the default state for regional
adjustments on individual valuations.
\item The system must provide Black Book valuations via manual-traversal.
\item The system must provide Black Book valuations via VIN.
\item The system must provide the ability to save and subsequently load
valuations for a vehicle.

\begin{enumerate}
\item The system must save each of the following data structures

\begin{enumerate}
\item the car
\item the vehicle configuration
\item the valuations
\end{enumerate}
\item The system must fully audit any save operations recording the user
as well as the date/time of the operation.
\end{enumerate}
\item The system must provide the ability to enumerate the valuations saved
for a vehicle.
\item The system must allow the user to load valuations for a vehicle from
the list enumerated in (8).
\item The system must be able to print the same document as available on
the Black Book product.

\begin{enumerate}
\item {}``Print Page''
\end{enumerate}
\end{enumerate}

\subsection*{Technical Requirements}

The technical requirements assume that the Black Book system functionality
has been composed into a single (re-usable) component.
\begin{enumerate}
\item The component must initialize itself on recieipt of a \emph{Vehicle
Change} event.
\item The component must update its vehicle configuration (if present) on
receipt of a \emph{Mileage Change} event.
\item The component must allow a user to change the state from which the
regional adjustment is derived.
\item The component must allow a user to add/remove options from the vehicle
configuration.
\item The component must calculate and present a new valuation when

\begin{enumerate}
\item an initial configuration is created
\item a configuration is adjusted

\begin{enumerate}
\item via the addition/removal of an option
\item via an update to the mileage
\item via an update to the state
\end{enumerate}
\end{enumerate}
\item The component must raise an \emph{Valuation Change} event when a new
valuation is calculated.
\item The component must allow the user to switch to a previously saved
valuation.

\begin{enumerate}
\item The component must switch to a read-only mode when presenting historic
valuations.
\item The component must not raise \emph{Valuation Change} events when in
read-only mode.
\end{enumerate}
\end{enumerate}

\subsubsection*{Notes}
\begin{enumerate}
\item The Black Book equipment model does not specify a relationship model
between its options.
\item Black Book does not have standard options such that options need an
\emph{enabled} boolean flag (or can be shown disabled in the component).
\item The Black Book component is not currently expected to expose to the
user the \emph{standard equipment} data that is communicated as part
of a \emph{car}.
\end{enumerate}

\section*{Kelley Blue Book}

The Kelley Blue Book (KBB) is a valuation guide published by the eponymous
company with new data publications on a monthly basis. The data published
by KBB contains both vehicle-prototypes (which they call {}``vehicles'')
as well as related pricing information.


\subsubsection*{Vehicles}

The Kelley Blue Book data is organized in the following hierarchy:
Year > Make > Model > Trim > Vehicle (\prettyref{fig:Kelley-Blue-Book}).
With KBB the natural identity of a vehicle is an ID attribute. There
are two methods to identify a vehicle within the KBB valuation guide.
\begin{enumerate}
\item Manual Traversal
\item VIN
\end{enumerate}
%
\begin{figure}
\caption{\label{fig:Kelley-Blue-Book}Kelley Blue Book Hierarchy}


\includegraphics[angle=90,height=7in]{\string"Vehicle Valuation Guides - Kelley Blue Book\string".png}%
\end{figure}



\paragraph*{Manual Traversal}

In this method a user would begin by selecting the model-year for
the vehicle of interest. Once a model-year is selected a list of makes
is made available. The selection of a make produces a list of models
(and so on) until an individual vehicle is identified.


\paragraph*{VIN}

In this method a user supplied VIN is mapped by KBB to zero or more
vehicles.
\begin{lyxlist}{00.00.0000}
\item [{Zero}] The VIN is invalid or currently not valued by KBB
\item [{One}] The VIN maps to a single vehicle in the KBB hierarchy.
\item [{Many}] The VIN maps to one of many possible vehicles. It is common
(but not required) that each of the returned vehicles have identical
ancestors. The user must perform the selection of the correct vehicle
from the collection.
\end{lyxlist}

\subsubsection*{Example Vehicle}

%
\begin{framed}%
\lstinputlisting[caption={Kelley Blue Book - Example Vehicle},label={Kelley Blue Book - Example Vehicle}]{Vehicle Valuation Guides - Kelley Blue Book.yml}\end{framed}


\subsection*{Vehicle Configuration}

Once a vehicle has been identified we have access to the information
from which a valuation for a real-life vehicle can be derived. To
produce such a valuation a description of the vehicles configuration
is required.
\begin{lyxlist}{00.00.0000}
\item [{ID}] The ID of the KBB vehicle
\item [{State}] The state of USA for the regional adjustment part of the
valuation.
\item [{Mileage}] The mileage of the vehicle (optional)
\item [{VIN}] The VIN of the vehicle (optional)
\item [{Actions}] A list of the actions (addition/removal) the user user
took upon the optional equipment and accessories presented as being
available on the vehicle.
\item [{States}] A complete list of the option states \{option code: string,
selected: boolean\} for the configuration reflecting each of the actions
taken from the initial conguration.
\end{lyxlist}
When the mileage is not provided then the resulting valuation assumes
that the real-life odometer reading is at the zero-point of the KBB
vehicle. When the VIN is not provided then the valuation has no options
selected by KBB on behalf of the user.

However, this is an incomplete picture of the vehicle configuration
as the option states are not set by a user actions upon the initial
configuration. In Kelley Blue Book the state of an option is also
determined by its \emph{option relationships}. There are four types
of option relationship in KBB:
\begin{lyxlist}{00.00.0000}
\item [{Implicit}] A radio button relationship that implicitly exists between
the different equipment options.
\item [{Radio}] A radio button relationship over a set of options (only
one member of the set can be selected).
\item [{Conflicts}] A list of options that must be de-selected when the
subject option is selected.
\item [{Requires}] A list of options that must be selected when the subject
option is selected.
\end{lyxlist}
The above description distinguishes between the different \emph{types}
of option.
\begin{itemize}
\item Equipment
\item Option
\item Certified Pre Owned
\end{itemize}
Equipment options are further distinguished by their \emph{category}.
\begin{itemize}
\item Drive Train
\item Engine
\item Transmission
\end{itemize}
The implicit relationship is over the options within the same category
such that it is implicit that a vehicle can have only one engine (for
example). It is worth noting that each active requires-relationship
must be re-evaluated when an option is de-selected and if the relationship
is not satisfied then the option and its required related options
must be de-selected also.


\subsection*{Vehicle Valuation}

Once we have a vehicle configuration the different valuations can
be calculated as defined by Kelley Blue Book (logically a function
of the form $calculate(vehicle,configuration)\rightarrow valuations$).
Kelley Blue provides valuations for the following dimensions.
\begin{itemize}
\item Price Type

\begin{itemize}
\item Retail
\item Wholesale
\item Trade In

\begin{itemize}
\item Fair
\item Good
\item Excellent
\end{itemize}
\item Private Party

\begin{itemize}
\item Fair
\item Good
\item Excellent
\end{itemize}
\item Auction

\begin{itemize}
\item Fair
\item Good
\item Excellent
\end{itemize}
\end{itemize}
\item Valuation

\begin{itemize}
\item Base Price
\item Option Adjustment
\item Mileage Adjustment
\item Final Price
\end{itemize}
\end{itemize}
For presentation purposes the price types as shown above have been
given structure but are actually a flat list (so it is a single enumerated
value {}``Trade In Fair'' not the composition of two values: {}``Trade
In'' and {}``Fair''). However, having said that, when the pricing
information is presented in a user-interface the price types are split
into Market and Condition (as per Black Book). KBB may provide a price
for each argument pair \{Price Type, Valuation\} although it could
be \emph{null} for similar reasons as Black Book.

The same nomenclature is adopted for Kelley Blue Book as was established
for Black Book (initial- and adjusted-configuration as well as initial-
and adjusted-valuations).


\subsubsection*{Notes}

Whereas the vehicle configuration is specified with a state, the KBB
valuations are given for a \emph{region}. The region is determined
by the state (part of the vehicle configuration) as well as the \emph{vehicle
type} which is a property of the KBB vehicle model. The Kelley Blue
Book regions are:
\begin{itemize}
\item Western
\item Central \& Southwestern
\item Northeast
\item Central \& Southeastern
\end{itemize}
If the vehicle type is {}``used car'' then all four regions are
available but it if is {}``older car'' then only three of the regions
are available. As such, we collect state from the user as it is not
possible to determine the region before you know the vehicle. Also,
once a vehicle is known, a user might not know the correct region
but they will know their state.


\subsection*{Business Requirements}
\begin{enumerate}
\item The system must allow dealerships to {[}un{]} subscribe to the Kelley
Blue Book valuation guide.
\item The system must provide a report of the dealerships who have made
use of the Kelley Blue Book valuation guide within a given time period
(billing window).

\begin{enumerate}
\item The system must be break the report down by the \emph{markets} that
are used so that the correct amount can be billed per dealership.
\end{enumerate}
\item The system must allow a dealership to identify their default state
for regional adjustments within the Kelley Blue Book valuation guide.
\item The system must allow a user to override the default state for regional
adjustments on individual valuations.
\item The system must provide Kelley Blue Book valuations via manual-traversal.
\item The system must provide Kelley Blue Book valuations via VIN.
\item The system must provide the ability to save and subsequently load
valuations for a vehicle.

\begin{enumerate}
\item The system must save each of the following data structures

\begin{enumerate}
\item the vehicle
\item the vehicle configuration
\item the valuations
\end{enumerate}
\item The system must fully audit any save operations recording the user
as well as the date/time of the operation.
\end{enumerate}
\item The system must provide the ability to enumerate the valuations saved
for a vehicle.
\item The system must allow the user to load valuations for a vehicle from
the list enumerated in (8).
\item The system must be able to print the same documents as available on
the Kelley Blue Book product (Karpower!).

\begin{enumerate}
\item Retail Breakdown
\item Equity Breakdown
\item Wholesale / Retail Breakdown
\item Wholesale Breakdown
\item Trade-In Report
\end{enumerate}
\end{enumerate}

\subsection*{Technical Requirements}

The technical requirements assume that the Kelley Blue Book system
functionality has been composed into a single (re-usable) component.
\begin{enumerate}
\item The component must initialize itself on recieipt of a \emph{Vehicle
Change} event.
\item The component must update its vehicle configuration (if present) on
receipt of a \emph{Mileage Change} event.
\item The component must allow a user to change the state from which the
regional adjustment is made.
\item The component must allow a user to add/remove options from the vehicle
configuration.

\begin{enumerate}
\item The component must respect all option relationships without user intervention.
\end{enumerate}
\item The component must calculate and present a new valuation when

\begin{enumerate}
\item an initial configuration is created
\item an configuration is adjusted

\begin{enumerate}
\item via the addition/removal of an option
\item via an update to the mileage
\item via an update to the state
\end{enumerate}
\item the vehicle (reference) data is updated to a newer publication
\end{enumerate}
\item The component must raise an \emph{Valuation Change} event when a new
valuation is calculated.
\item The component must allow the user to switch to a previously saved
valuation.

\begin{enumerate}
\item The component must switch to a read-only mode when presenting historic
valuations.
\item The component must not raise \emph{Valuation Change} events when in
read-only mode.
\end{enumerate}
\item The component must warn the user when there is a more current publication
of the reference data.

\begin{enumerate}
\item The component must present a the user with the ability to switch to
the current publication.
\item The component must only enable the print functionality when using
the current publication.
\end{enumerate}
\item The component must expose the print functionality.

\begin{enumerate}
\item The component must expose the ability for its parent to enable/disable
and show/hide the print functionality.
\end{enumerate}
\end{enumerate}

\section*{NADA}

The NADA Official Used Car Guide is published by NADA with new data
publications on a monthly basis. The data published by NADA contains
both vehicle-prototypes (which they call vehicles) as well as related
pricing information.


\subsection*{Vehicles}

The NADA data is organized in the following hierarchy: Year > Make
> Series > Body > Vehicle (\prettyref{fig:NADA}). With NADA the natural
identity of a vehicle is an UID attribute. There are two methods to
identify a vehicle within the NADA valuation guide.
\begin{enumerate}
\item Manual Traversal
\item VIN
\end{enumerate}
%
\begin{figure}
\caption{\label{fig:NADA}NADA Hierarchy}


\includegraphics[angle=90,height=7in]{\string"Vehicle Valuation Guides - NADA\string".png}%
\end{figure}



\paragraph*{Manual Traversal}

In this method a user would begin by selecting the model-year for
the vehicle of interest. Once a model-year is selected a list of makes
is made available. The selection of a make produces a list of series
(and so on) until an individual vehicle (UID) is identified.


\paragraph*{VIN}

In this method a user supplied VIN is mapped by NADA to zero or more
vehicles.
\begin{lyxlist}{00.00.0000}
\item [{Zero}] The VIN is invalid or currently not valued by NADA
\item [{One}] The VIN maps to a single vehicle in the NADA hierarchy.
\item [{Many}] The VIN maps to one of many possible vehicles. It is common
(but not required) that each of the returned vehicles have identical
ancestors. The user must perform the selection of the correct vehicle
from the collection.
\end{lyxlist}

\subsubsection*{Example Vehicle}

%
\begin{framed}%
\lstinputlisting[caption={NADA - Example Vehicle},label={NADA - Example Vehicle}]{Vehicle Valuation Guides - NADA.yml}\end{framed}


\paragraph*{Note}

In the NADA valuation guide we have \emph{accessories} whereas in
Black Book the term was \emph{options} and \emph{vehicle options}
in Kelley Blue Book.


\subsection*{Vehicle Configuration}

Once a vehicle has been identified we have access to the information
from which a valuation for a real-life vehicle can be derived. To
produce such a valuation a description of the vehicles configuration
is required.
\begin{lyxlist}{00.00.0000}
\item [{UID}] The UID of the NADA vehicle
\item [{State}] The state of USA for the regional adjustment part of the
valuation.
\item [{Mileage}] The mileage of the vehicle (optional)
\item [{VIN}] The VIN of the vehicle (optional)
\item [{Actions}] A list of the actions (addition/removal) the user user
took upon the accessories presented as being available for the vehicle.
\item [{States}] A complete list of the accessory states \{accessory code:
string, selected: boolean, enabled: boolean\} for the configuration
reflecting each of the actions taken from the initial configuration.
\end{lyxlist}
When the mileage is not provided then the resulting valuation assumes
that the real-life odometer reading is within the zero adjustment
range of the NADA vehicle. When the VIN is not provided then the valuation
has no optional options selected by NADA on behalf of the user.

Like Kelley Blue Book, this is an incomplete picture of the vehicle
configuration as NADA specifies accessory relationships. Furthermore,
NADA has a non-standard list of states - some states have been split
into two (e.g. {}``Illinois Except Rock Island County'' and {}``Rock
Island County Only''). NADA specifies these accessory relationship
types:
\begin{lyxlist}{00.00.0000}
\item [{Included}] Include the related accessories when the subject accessory
is selected.
\item [{Excluded}] Exclude the related accessories when the subject accessory
is selected.
\end{lyxlist}
As with Kelley Blue Book, the included accessory relationships must
be re-evaluated when accessories are de-selected. If the all the related
accessories are not all selected then the option and its relations
are all de-selected.

Also, NADA accessories can be standard which means that they are selected
by default and a user cannot de-select them. It is for this reason
that an accessory state part of the vehicle configuration has a enabled
flag to indicate whether or not a user can take action on the accessory.


\subsection*{Vehicle Valuation}

Once we have a vehicle configuration the different valuations can
be calculated as defined by NADA (logically a function of the form
$calculate(vehicle,configuration)\rightarrow valuations$). NADA provides
valuations for the following dimensions.
\begin{itemize}
\item Value Type

\begin{itemize}
\item Retail
\item Trade
\item Loan
\item Trade Average
\item Trade Rough
\end{itemize}
\item Valuation

\begin{itemize}
\item Base Price
\item Accessory Adjustment
\item Mileage Adjustment
\item Final Price
\end{itemize}
\end{itemize}
NADA may provide a price for an argument pair \{Value Type, Valuation\}
although it could be \emph{null}. The value-type {}``Trade'' is
implicitly {}``clean'' as are {}``Retail'' and {}``Loan''.


\subsubsection*{Notes}

Whereas the vehicle configuration is specified with a state, the NADA
valuations are given for a region. The region is determined wholly
by the state (part of the vehicle conguration). The list of NADA regions
is:
\begin{itemize}
\item Eastern
\item Pacific Northwest
\item Southwestern
\item Midwest
\item Central
\item Southeastern
\item New England
\item Desert Southwest
\item Mountain States
\item California Region
\end{itemize}
We collect state as the user is guaranteed to know it, whereas the
same cannot be said of the user being able to identify the correct
NADA region.


\subsection*{Business Requirements}
\begin{enumerate}
\item The system must allow dealerships to {[}un{]} subscribe to the NADA
valuation guide.
\item The system must provide a report of the dealerships who have made
use of the NADA valuation guide within a given time period (billing
window).
\item The system must allow a dealership to identify their default state
for regional adjustments within the NADA valuation guide.
\item The system must allow a user to override the default state for regional
adjustments on individual valuations.
\item The system must provide NADA valuations via manual-traversal.
\item The system must provide NADA valuations via VIN.
\item The system must provide the ability to save and subsequently load
valuations for a vehicle.

\begin{enumerate}
\item The system must save each of the following data structures

\begin{enumerate}
\item the vehicle
\item the vehicle configuration
\item the valuations
\end{enumerate}
\item The system must fully audit any save operations recording the user
as well as the date/time of the operation.
\end{enumerate}
\item The system must provide the ability to enumerate the valuations saved
for a vehicle.
\item The system must allow the user to load valuations for a vehicle from
the list enumerated in (8).
\item The system must be able to print the same documents as available on
the NADA product.

\begin{enumerate}
\item NADA Values
\item NADA Trade Values
\item NADA Retail
\item NADA Vehicle Description
\item NADA Loan Summary
\item NADA Wholesale
\end{enumerate}
\end{enumerate}

\subsection*{Technical Requirements}

The technical requirements assume that the NADA system functionality
has been composed into a single (re-usable) component.
\begin{enumerate}
\item The component must initialize itself on recieipt of a \emph{Vehicle
Change} event.
\item The component must update its vehicle configuration (if present) on
receipt of a \emph{Mileage Change} event.
\item The component must allow a user to change the state from which the
regional adjustment is made.
\item The component must allow a user to add/remove accessories from the
vehicle configuration.

\begin{enumerate}
\item The component must respect standard accessories.
\item The component must respect all accessory relationships without user
intervention.
\end{enumerate}
\item The component must calculate and present a new valuation when

\begin{enumerate}
\item an initial configuration is created
\item an configuration is adjusted

\begin{enumerate}
\item via the addition/removal of an accessory
\item via an update to the mileage
\item via an update to the state
\end{enumerate}
\end{enumerate}
\item The component must raise an \emph{Valuation Change} event when a new
valuation is calculated.
\item The component must allow the user to switch to a previously saved
valuation.

\begin{enumerate}
\item The component must switch to a read-only mode when presenting historic
valuations.
\item The component must not raise \emph{Valuation Change} events when in
read-only mode.
\end{enumerate}
\end{enumerate}

\section*{Galves}

The Galves valuation guide is published by Galves Auto Price List
with new data publications every month.


\subsection*{Vehicles}

The Galves data is organized in the following hierarchy: Year > Manufacturer
> Model > Style > Vehicle (\prettyref{fig:Galves}). With Galves the
natural identity of a vehicle is an ID attribute. There are two methods
to identify a vehicle within the Galves valuation guide.
\begin{enumerate}
\item Manual Traversal
\item VIN
\end{enumerate}
%
\begin{figure}
\caption{\label{fig:Galves}Galves Hierarchy}


\includegraphics[angle=90,height=7in]{\string"Vehicle Valuation Guides - Galves\string".png}%
\end{figure}



\paragraph*{Manual Traversal}

In this method a user would begin by selecting the model-year for
the vehicle of interest. Once a model-year is selected a list of manufacturers
is made available. The selection of a manufacturer produces a list
of series (and so on) until an individual vehicle (ID) is identified.


\paragraph*{VIN}

In this method a user supplied VIN is mapped by Galves to zero or
more vehicles.
\begin{lyxlist}{00.00.0000}
\item [{Zero}] The VIN is invalid or currently not valued by Galves
\item [{One}] The VIN maps to a single vehicle in the Galves hierarchy.
\item [{Many}] The VIN maps to one of many possible vehicles. It is common
(but not required) that each of the returned vehicles have identical
ancestors. The user must perform the selection of the correct vehicle
from the collection.
\end{lyxlist}

\subsubsection*{Example Vehicle}

%
\begin{framed}%
\lstinputlisting[caption={Galves - Example Vehicle},label={Galves - Example Vehicle}]{Vehicle Valuation Guides - Gavles.yml}\end{framed}


\paragraph*{Note}

In Galves the term used for {}``options'' (Black Book, Kelley Bue
Book) is \emph{adjustment}.


\subsection*{Vehicle Configuration}

Once a vehicle has been identified we have access to the information
from which a valuation for a real-life vehicle can be derived. To
produce such a valuation a description of the vehicles configuration
is required.
\begin{lyxlist}{00.00.0000}
\item [{ID}] The ID of the Gavles vehicle
\item [{State}] The state of USA for the regional adjustment part of the
valuation.
\item [{Mileage}] The mileage of the vehicle (optional)
\item [{VIN}] The VIN of the vehicle (optional)
\item [{Actions}] A list of the actions (addition/removal) the user user
took upon the adjustments presented as being available for the vehicle.
\item [{States}] A complete list of the adjustment states \{adjustment
name: string, selected: boolean\} for the configuration reflecting
each of the actions taken from the initial configuration.
\end{lyxlist}
When the mileage is not provided then the resulting valuation assumes
that the real-life odometer reading is within the zero adjustment
range of the Galves vehicle. When the VIN is not provided then the
valuation has no optional options selected by Galves on behalf of
the user.

Again, this is an incomplete picture of the vehicle configuration
as Galves specifies accessory relationships.
\begin{lyxlist}{00.00.0000}
\item [{Exception}] Exclude the related adjustments when the subject adjustment
is selected.
\end{lyxlist}
Also, Galves adjustments come in three flavors.
\begin{lyxlist}{00.00.0000}
\item [{Add}] Positive dollar adjustment (addition)
\item [{Nul}] Standard adjustment (zero price, selected by default, disabled)
\item [{Ded}] Negative dollar adjustment (deduction)
\end{lyxlist}

\subsection*{Vehicle Valuations}

Once we have a vehicle configuration the different valuations can
be calculated as defined by Galves (logically a function of the form
$calculate(vehicle,configuration)\rightarrow valuations$). Galves
provides valuations for the following dimensions.
\begin{itemize}
\item Price Type

\begin{itemize}
\item Trade In ({}``Galves Value'')
\item Retail ({}``Market Ready Value'')
\end{itemize}
\item Valuation

\begin{itemize}
\item Base Price
\item Option Adjustment
\item Mileage Adjustment
\item Final Price
\end{itemize}
\end{itemize}
Galves may provide a price for an argument pair \{Price Type, Valuation\}
although it could be \emph{null}.


\subsection*{Business Requirements}
\begin{enumerate}
\item The system must allow dealerships to {[}un{]} subscribe to the Galves
valuation guide.
\item The system must provide a report of the dealerships who have made
use of the Galves valuation guide within a given time period (billing
window).
\item The system must allow a dealership to identify their default state
for regional adjustments within the Galves valuation guide.
\item The system must allow a user to override the default state for regional
adjustments on individual valuations.
\item The system must provide Galves valuations via manual-traversal.
\item The system must provide Galves valuations via VIN.
\item The system must provide the ability to save and subsequently load
valuations for a vehicle.

\begin{enumerate}
\item The system must save each of the following data structures

\begin{enumerate}
\item the vehicle
\item the vehicle configuration
\item the valuations
\end{enumerate}
\item The system must fully audit any save operations recording the user
as well as the date/time of the operation.
\end{enumerate}
\item The system must provide the ability to enumerate the valuations saved
for a vehicle.
\item The system must allow the user to load valuations for a vehicle from
the list enumerated in (8).
\item The system must be able to print the same documents as available on
the Galves product.

\begin{enumerate}
\item {}``Print Page''
\end{enumerate}
\end{enumerate}

\subsection*{Technical Requirements}

The technical requirements assume that the Galves system functionality
has been composed into a single (re-usable) component.
\begin{enumerate}
\item The component must initialize itself on recieipt of a \emph{Vehicle
Change} event.
\item The component must update its vehicle configuration (if present) on
receipt of a \emph{Mileage Change} event.
\item The component must allow a user to change the state from which the
regional adjustment is made.
\item The component must allow a user to add/remove adjustments from the
vehicle configuration.

\begin{enumerate}
\item The component must respect all adjusment relationships without user
intervention.
\end{enumerate}
\item The component must calculate and present a new valuation when

\begin{enumerate}
\item an initial configuration is created
\item an configuration is adjusted

\begin{enumerate}
\item via the addition/removal of an option
\item via an update to the mileage
\item via an update to the state
\end{enumerate}
\end{enumerate}
\item The component must raise an \emph{Valuation Change} event when a new
valuation is calculated.
\item The component must allow the user to switch to a previously saved
valuation.

\begin{enumerate}
\item The component must switch to a read-only mode when presenting historic
valuations.
\item The component must not raise \emph{Valuation Change} events when in
read-only mode.
\end{enumerate}
\end{enumerate}

\end{document}
