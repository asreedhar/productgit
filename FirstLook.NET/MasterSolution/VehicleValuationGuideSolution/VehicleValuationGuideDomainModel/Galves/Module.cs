﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Model.Module>();

            registry.Register<Repositories.Module>();
        }
    }
}
