﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<IGalvesRepository, GalvesRepository>(ImplementationScope.Shared);

            registry.Register<IVehicleConfigurationBuilderFactory, VehicleConfigurationBuilderFactory>(ImplementationScope.Shared);
        }

    }
}
