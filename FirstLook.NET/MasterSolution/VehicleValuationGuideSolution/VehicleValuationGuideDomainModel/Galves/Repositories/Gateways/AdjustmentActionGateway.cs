﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Gateways
{
    public class AdjustmentActionGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public byte AdjustmentActionTypeId { get; set; }

            public byte AdjustmentActionIndex { get; set; }

            public int DataLoadId { get; set; }

            public string Adjustment { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                byte accessoryActionTypeId = record.GetByte(record.GetOrdinal("AdjustmentActionTypeId"));

                byte accessoryActionIndex = record.GetByte(record.GetOrdinal("AdjustmentActionIndex"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                string adjustment = record.GetString(record.GetOrdinal("Adjustment"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationHistoryId,
                    DataLoadId = dataLoadId,
                    Adjustment = adjustment,
                    AdjustmentActionIndex = accessoryActionIndex,
                    AdjustmentActionTypeId = accessoryActionTypeId
                };
            }
        }

        public IList<Row> Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(dataLoadId, vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

                using (IDataReader reader = datastore.AdjustmentAction_Fetch(dataLoadId, vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int dataLoadId, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, string adjustment)
        {
            IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

            datastore.AdjustmentAction_Insert(dataLoadId, vehicleConfigurationHistoryId, accessoryActionTypeId, accessoryActionIndex, adjustment);
        }
    }
}
