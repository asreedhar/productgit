﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Gateways
{
    public class VehicleGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationId { get; set; }

            public int VehicleId { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationId"));

                int vehicleId = record.GetInt32(record.GetOrdinal("VehicleId"));

                return new Row {VehicleConfigurationId = vehicleConfigurationId, VehicleId = vehicleId};
            }
        }

        public Row Fetch(int vehicleId)
        {
            string key = CreateCacheKey(vehicleId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IClientDatastore datastore = Resolve<IClientDatastore>();

                using (IDataReader reader = datastore.Vehicle_Galves_Fetch(vehicleId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public void Insert(int vehicleId, int vehicleConfigurationId)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            datastore.Vehicle_Galves_Insert(vehicleId, vehicleConfigurationId);
        }
    }
}
