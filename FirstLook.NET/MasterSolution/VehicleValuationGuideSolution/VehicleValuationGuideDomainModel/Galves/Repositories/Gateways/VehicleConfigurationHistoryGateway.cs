﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Serializers;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Gateways
{
    public class VehicleConfigurationHistoryGateway : AuditingGateway
    {
        [Serializable]
        public class Row
        {
            public int Id { get; set; }

            public int VehicleConfigurationId { get; set; }

            public int DataLoadId { get; set; }

            public int VehicleId { get; set; }

            public string State { get; set; }

            public int? Mileage { get; set; }

            public int ChangeTypeBitFlags { get; set; }

            public AuditRow AuditRow { get; set; }

            public User User { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                int vehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationId"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                int vehicleId = record.GetInt32(record.GetOrdinal("VehicleId"));

                string state = record.GetString(record.GetOrdinal("State"));

                int? mileage = null;

                int ordinal = record.GetOrdinal("Mileage");

                if (!record.IsDBNull(ordinal))
                {
                    mileage = record.GetInt32(ordinal);
                }
                
                int changeTypeBitFlags = record.GetInt32(record.GetOrdinal("ChangeTypeBitFlags"));

                AuditRow auditRow = new AuditRowSerializer().Deserialize(record);

                User user = new UserSerializer().Deserialize(record);

                return new Row
                {
                    Id = vehicleConfigurationHistoryId,
                    VehicleConfigurationId = vehicleConfigurationId,
                    ChangeTypeBitFlags = changeTypeBitFlags,
                    Mileage = mileage,
                    DataLoadId = dataLoadId,
                    State = state,
                    VehicleId = vehicleId,
                    AuditRow = auditRow,
                    User = user
                };
            }
        }

        public IList<Row> Fetch(int vehicleConfigurationId)
        {
            IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId))
            {
                ISerializer<Row> serializer = new RowSerializer();

                return serializer.Deserialize(reader);
            }
        }

        public Row Fetch(int vehicleConfigurationId, DateTime on)
        {
            string key = CreateCacheKey(vehicleConfigurationId, on);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId, on))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord) reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public Row Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationId, vehicleConfigurationHistoryId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId, vehicleConfigurationHistoryId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public Row Insert(int vehicleConfigurationId, int period, int vehicleId, string state, int? mileage, int changeTypeBitFlags)
        {
            IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Insert(
                vehicleConfigurationId,
                period, vehicleId, state, mileage, changeTypeBitFlags,
                Insert().Id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        public Row Update(int vehicleConfigurationId, int period, int vehicle, string state, int? mileage, int changeTypeBitFlags, AuditRow auditRow)
        {
            IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Insert(
                vehicleConfigurationId,
                period, vehicle, state, mileage, changeTypeBitFlags,
                Update(auditRow).Id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
