﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Gateways
{
    public class AdjustmentStateGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public int DataLoadId { get; set; }

            public string Adjustment { get; set; }

            public bool Enabled { get; set; }

            public bool Selected { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                bool selected = record.GetBoolean(record.GetOrdinal("Selected"));

                bool enabled = record.GetBoolean(record.GetOrdinal("Enabled"));

                string adjustment = record.GetString(record.GetOrdinal("Adjustment"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationId,
                    DataLoadId = dataLoadId,
                    Adjustment = adjustment,
                    Enabled = enabled,
                    Selected = selected
                };
            }
        }

        public IList<Row> Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(dataLoadId, vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

                using (IDataReader reader = datastore.AdjustmentState_Fetch(dataLoadId, vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int dataLoadId, int vehicleConfigurationHistoryId, string adjustment, bool enabled, bool selected)
        {
            IGalvesDatastore datastore = Resolve<IGalvesDatastore>();

            datastore.AdjustmentState_Insert(dataLoadId, vehicleConfigurationHistoryId, adjustment, enabled, selected);
        }
    }
}
