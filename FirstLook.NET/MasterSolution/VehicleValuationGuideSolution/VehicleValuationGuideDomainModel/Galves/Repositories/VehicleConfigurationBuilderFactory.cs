﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories
{
    public class VehicleConfigurationBuilderFactory : IVehicleConfigurationBuilderFactory
    {
        public IVehicleConfigurationBuilder NewBuilder()
        {
            return new VehicleConfigurationBuilder();
        }

        public IVehicleConfigurationBuilder NewBuilder(IVehicleConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            return new VehicleConfigurationBuilder(configuration);
        }
    }
}
