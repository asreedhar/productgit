﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories
{
    public class VehicleConfigurationBuilder : IVehicleConfigurationBuilder
    {
        public VehicleConfigurationBuilder()
        {
            
        }

        public VehicleConfigurationBuilder(IVehicleConfiguration configuration)
        {
            BookDate = configuration.BookDate;
            VehicleId = configuration.VehicleId;
            Vin = configuration.Vin;
            State = configuration.State;
            Mileage = configuration.Mileage;

            foreach (IAdjustmentAction action in configuration.AdjustmentActions)
            {
                _actions.Add(Clone(action));
            }

            foreach (IAdjustmentState state in configuration.AdjustmentStates)
            {
                _states.Add(Clone(state));
            }
        }

        private BookDate _bookDate;
        private State _state;
        private int _vehicleId;
        private string _vin;
        private int? _mileage;
        
        public BookDate BookDate
        {
            get { return _bookDate; }
            set { _bookDate = value; }
        }

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public State State
        {
            get { return _state; }
            set { _state = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }
        
        private readonly List<AdjustmentAction> _actions = new List<AdjustmentAction>();
        private readonly List<AdjustmentState> _states = new List<AdjustmentState>();

        public IAdjustmentState Add(Adjustment adjustment)
        {
            if (adjustment == null)
            {
                throw new ArgumentNullException("adjustment");
            }

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustment.Name));

            if (state == null)
            {
                bool standard = adjustment.AdjustmentCode == AdjustmentCode.Nul;

                state = new AdjustmentState
                {
                    AdjustmentName = adjustment.Name,
                    Enabled = !standard,
                    Selected = standard
                };

                _states.Add(state);
            }

            return state;
        }

        public IAdjustmentState Add(string adjustmentName, bool enabled, bool selected)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state == null)
            {
                state = new AdjustmentState
                {
                    AdjustmentName = adjustmentName,
                    Enabled = enabled,
                    Selected = selected
                };

                _states.Add(state);
            }

            return state;
        }
        
        public void Disable(string adjustmentName)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state != null)
            {
                state.Enabled = false;
            }
        }

        public void Enable(string adjustmentName)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state != null)
            {
                state.Enabled = true;
            }
        }

        public bool Enabled(string adjustmentName)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state != null)
            {
                return state.Enabled;
            }

            return false;
        }

        public void Select(string adjustmentName)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state != null)
            {
                state.Selected = true;
            }
        }

        public void Deselect(string adjustmentName)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state != null)
            {
                state.Selected = false;
            }
        }

        public IEnumerable<string> Selected()
        {
            return _states.Where(x => x.Selected).Select(y => y.AdjustmentName);
        }

        public bool IsSelected(string adjustmentName, bool returnValue)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentState state = _states.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (state != null)
            {
                return state.Selected;
            }

            return returnValue;
        }

        public IAdjustmentAction Do(string adjustmentName, AdjustmentActionType actionType)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentAction action = new AdjustmentAction
            {
                AdjustmentName = adjustmentName,
                ActionType = actionType
            };

            _actions.Add(action);

            return action;
        }

        public IAdjustmentAction Undo(string adjustmentName)
        {
            VerifyAdjustmentName(adjustmentName);

            AdjustmentAction action = _actions.FirstOrDefault(x => Equals(x.AdjustmentName, adjustmentName));

            if (action != null)
            {
                _actions.Remove(action);
            }

            return action;
        }

        public IVehicleConfiguration ToVehicleConfiguration()
        {
            VehicleConfiguration configuration = new VehicleConfiguration
            {
                BookDate = BookDate,
                VehicleId = VehicleId,
                Vin = Vin,
                State = State,
                Mileage = Mileage
            };

            foreach (AdjustmentAction action in _actions)
            {
                configuration.AdjustmentActions.Add(Clone(action));
            }

            foreach (AdjustmentState state in _states)
            {
                configuration.AdjustmentStates.Add(Clone(state));
            }

            return configuration;
        }

        private static void VerifyAdjustmentName(string adjustmentName)
        {
            if (string.IsNullOrEmpty(adjustmentName))
            {
                throw new ArgumentNullException("adjustmentName");
            }
        }

        private static AdjustmentAction Clone(IAdjustmentAction action)
        {
            return new AdjustmentAction { ActionType = action.ActionType, AdjustmentName = action.AdjustmentName };
        }

        private static AdjustmentState Clone(IAdjustmentState state)
        {
            return new AdjustmentState { AdjustmentName = state.AdjustmentName, Enabled = state.Enabled, Selected = state.Selected };
        }
    }
}
