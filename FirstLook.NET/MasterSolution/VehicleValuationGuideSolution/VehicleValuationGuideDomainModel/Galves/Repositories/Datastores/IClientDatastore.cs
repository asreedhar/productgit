﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores
{
    public interface IClientDatastore
    {
        IDataReader Vehicle_Galves_Fetch(int vehicleId);

        void Vehicle_Galves_Insert(int vehicleId, int vehicleConfigurationId);
    }
}
