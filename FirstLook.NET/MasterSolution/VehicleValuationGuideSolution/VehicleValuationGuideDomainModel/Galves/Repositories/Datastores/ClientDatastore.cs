﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores
{
    public class ClientDatastore : SessionDataStore, IClientDatastore
    {
        internal const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public IDataReader Vehicle_Galves_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Galves_Fetch.txt";

            return Query(
                new[] { "Vehicle_Galves" },
                queryName,
                new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void Vehicle_Galves_Insert(int vehicleId, int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Galves_Insert.txt";

            NonQuery(queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("VehicleID", vehicleId, DbType.Int32));
        }
    }
}
