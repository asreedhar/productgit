﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores
{
    public class GalvesDatastore : SessionDataStore, IGalvesDatastore
    {
        internal const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public void CloneData(int vehicleId)
        {
            using (IDbCommand command = CreateCommand())
            {
                command.CommandText = "Galves.CloneData";

                command.CommandType = CommandType.StoredProcedure;

                Database.AddWithValue(command, "VehicleID", vehicleId, DbType.Int32);
                
                command.ExecuteNonQuery();
            }
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Fetch_All.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".GalvesDatastore_VehicleConfiguration_Fetch.txt";

            return Query(
                new[] { "VehicleConfiguration" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on)
        {
            const string queryName = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Fetch_DateTime.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("Date", on, DbType.DateTime));
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Fetch_Id.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader AdjustmentAction_Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".GalvesDatastore_AdjustmentAction_Fetch.txt";

            return Query(
                new[] { "AdjustmentAction" },
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader AdjustmentState_Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".GalvesDatastore_AdjustmentState_Fetch.txt";

            return Query(
                new[] { "AdjustmentState" },
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_Insert(int vehicleId)
        {
            const string queryNameI = Prefix + ".GalvesDatastore_VehicleConfiguration_Insert.txt";

            const string queryNameF = Prefix + ".GalvesDatastore_VehicleConfiguration_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("VehicleID", vehicleId, DbType.Int32));

            return Query(
                new[] { "VehicleConfiguration" },
                queryNameF);
        }

        public IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int dataLoadId, int vehicleId, string state, int? mileage, int changeTypeBitFlags, int auditRowId)
        {
            const string queryNameI = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Insert.txt";

            const string queryNameF = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Fetch_Identity.txt";

            Parameter m = mileage.HasValue
                              ? new Parameter("Mileage", mileage.Value, DbType.Int32)
                              : new Parameter("Mileage", DBNull.Value, DbType.Int32);

            NonQuery(
                queryNameI,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("State", state, DbType.String),
                m,
                new Parameter("ChangeTypeBitFlags", changeTypeBitFlags, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            return Query(
                new[] { "VehicleConfiguration" },
                queryNameF);
        }

        public void AdjustmentAction_Insert(int dataLoadId, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, string adjustment)
        {
            const string queryName = Prefix + ".GalvesDatastore_AdjustmentAction_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("AdjustmentActionTypeID", accessoryActionTypeId, DbType.Byte),
                new Parameter("AdjustmentActionIndex", accessoryActionIndex, DbType.Byte),
                new Parameter("Adjustment", adjustment, DbType.String),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }

        public void AdjustmentState_Insert(int dataLoadId, int vehicleConfigurationHistoryId, string adjustment, bool enabled, bool selected)
        {
            const string queryName = Prefix + ".GalvesDatastore_AdjustmentState_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("Adjustment", adjustment, DbType.String),
                new Parameter("Enabled", enabled, DbType.Boolean),
                new Parameter("Selected", selected, DbType.Boolean));
        }

        public IDataReader VehicleConfiguration_History_Matrix_Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Matrix_Fetch.txt";

            return Query(
                new[] { "VehicleConfiguration_History_Matrix" },
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader MatrixCell_Fetch(int dataLoadId, int matrixId)
        {
            const string queryName = Prefix + ".GalvesDatastore_MatrixCell_Fetch.txt";

            return Query(
                new[] { "MatrixCell" },
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("MatrixID", matrixId, DbType.Int32));
        }

        public IDataReader Matrix_Insert(int dataLoadId)
        {
            const string queryNameI = Prefix + ".GalvesDatastore_Matrix_Insert.txt";

            const string queryNameF = Prefix + ".GalvesDatastore_Matrix_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32));

            return Query(new[] {"Matrix"}, queryNameF);
        }

        public void MatrixCell_Insert(int dataLoadId, int matrixId, byte priceTypeId, byte valuationId, bool visible, decimal? value)
        {
            const string queryName = Prefix + ".GalvesDatastore_MatrixCell_Insert.txt";

            Parameter parameter = value.HasValue
                              ? new Parameter("Value", value, DbType.Decimal)
                              : new Parameter("Value", DBNull.Value, DbType.Decimal);

            NonQuery(
                queryName,
                new Parameter("MatrixID", matrixId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("PriceTypeID", priceTypeId, DbType.Int32),
                new Parameter("ValuationID", valuationId, DbType.Byte),
                new Parameter("Visible", visible, DbType.Boolean),
                parameter);
        }

        public void VehicleConfiguration_History_Matrix_Insert(int dataLoadId, int vehicleConfigurationHistoryId, int matrixId)
        {
            const string queryName = Prefix + ".GalvesDatastore_VehicleConfiguration_History_Matrix_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("MatrixID", matrixId, DbType.Int32));
        }
    }
}
