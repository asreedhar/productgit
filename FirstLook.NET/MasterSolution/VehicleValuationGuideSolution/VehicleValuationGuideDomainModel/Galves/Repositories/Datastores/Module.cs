﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IClientDatastore, ClientDatastore>(ImplementationScope.Shared);

            registry.Register<IGalvesDatastore, GalvesDatastore>(ImplementationScope.Shared);
        }
    }
}
