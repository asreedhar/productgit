﻿using System;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Datastores
{
    public interface IGalvesDatastore
    {
        void CloneData(int vehicleId);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId);

        IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId);

        IDataReader AdjustmentAction_Fetch(int dataLoadId, int vehicleConfigurationHistoryId);

        IDataReader AdjustmentState_Fetch(int dataLoadId, int vehicleConfigurationHistoryId);

        IDataReader VehicleConfiguration_Insert(int vehicleId);

        IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int dataLoadId, int vehicleId, string state, int? mileage, int changeTypeBitFlags, int auditRowId);

        void AdjustmentAction_Insert(int dataLoadId, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, string adjustment);

        void AdjustmentState_Insert(int dataLoadId, int vehicleConfigurationHistoryId, string adjustment, bool enabled, bool selected);

        IDataReader VehicleConfiguration_History_Matrix_Fetch(int dataLoadId, int vehicleConfigurationHistoryId);

        IDataReader MatrixCell_Fetch(int dataLoadId, int matrixId);

        IDataReader Matrix_Insert(int dataLoadId);

        void MatrixCell_Insert(int dataLoadId, int matrixId, byte priceTypeId, byte valuationId, bool visible, decimal? value);

        void VehicleConfiguration_History_Matrix_Insert(int dataLoadId, int vehicleConfigurationHistoryId, int matrixId);
    }
}
