﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities
{
    [Serializable]
    public class AdjustmentAction : IAdjustmentAction
    {
        private string _adjustmentName;
        private AdjustmentActionType _actionType;

        public string AdjustmentName
        {
            get { return _adjustmentName; }
            internal set { _adjustmentName = value; }
        }

        public AdjustmentActionType ActionType
        {
            get { return _actionType; }
            internal set { _actionType = value; }
        }
    }
}
