﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities
{
    [Serializable]
    public class Publication : PublicationInfo, IPublication
    {
        public IVehicleConfiguration VehicleConfiguration { get; internal set; }

        public Matrix Matrix { get; internal set; }

        public IGalvesService Service { get; internal set; }
    }
}
