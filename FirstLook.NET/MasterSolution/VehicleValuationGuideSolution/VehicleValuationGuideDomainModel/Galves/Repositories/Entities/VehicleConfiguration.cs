﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities
{
    [Serializable]
    public class VehicleConfiguration : IVehicleConfiguration
    {
        private BookDate _bookDate;
        private State _state;
        private int _vehicleId;
        private string _vin;
        private int? _mileage;
        private readonly IList<IAdjustmentAction> _adjustmentActions = new List<IAdjustmentAction>();
        private readonly IList<IAdjustmentState> _adjustmentStates = new List<IAdjustmentState>();

        public BookDate BookDate
        {
            get { return _bookDate; }
            internal set { _bookDate = value; }
        }

        public State State
        {
            get { return _state; }
            internal set { _state = value; }
        }

        public int VehicleId
        {
            get { return _vehicleId; }
            internal set { _vehicleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            internal set { _vin = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            internal set { _mileage = value; }
        }

        public IList<IAdjustmentAction> AdjustmentActions
        {
            get { return _adjustmentActions; }
        }

        public IList<IAdjustmentState> AdjustmentStates
        {
            get { return _adjustmentStates; }
        }

        public ChangeType Compare(IVehicleConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            ChangeType flags = ChangeType.None;

            if (configuration.BookDate.Id != BookDate.Id) flags |= ChangeType.BookDate;

            if (!Equals(configuration.State.Name, State.Name)) flags |= ChangeType.State;

            if (configuration.VehicleId != VehicleId) flags |= ChangeType.VehicleId;

            if (Nullable.Compare(configuration.Mileage, Mileage) != 0) flags |= ChangeType.Mileage;

            if (configuration.AdjustmentActions.Count != AdjustmentActions.Count)
            {
                flags |= ChangeType.AdjustmentActions;
            }
            else
            {
                bool identical = configuration.AdjustmentActions.All(
                    a => AdjustmentActions.Any(
                             b => Equals(a.AdjustmentName, b.AdjustmentName) &&
                                  a.ActionType == b.ActionType));

                if (!identical)
                {
                    flags |= ChangeType.AdjustmentActions;
                }
            }

            if (configuration.AdjustmentStates.Count != AdjustmentStates.Count)
            {
                flags |= ChangeType.AdjustmentStates;
            }
            else
            {
                bool identical = configuration.AdjustmentStates.All(
                    a => AdjustmentStates.Any(
                             b => Equals(a.AdjustmentName, b.AdjustmentName) &&
                                  a.Enabled == b.Enabled &&
                                  a.Selected == b.Selected));

                if (!identical)
                {
                    flags |= ChangeType.AdjustmentStates;
                }
            }

            return flags;
        }
    }
}
