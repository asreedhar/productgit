﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities
{
    [Serializable]
    public class AdjustmentState : IAdjustmentState
    {
        private string _adjustmentName;
        private bool _enabled;
        private bool _selected;

        public string AdjustmentName
        {
            get { return _adjustmentName; }
            internal set { _adjustmentName = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            internal set { _enabled = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            internal set { _selected = value; }
        }
    }
}
