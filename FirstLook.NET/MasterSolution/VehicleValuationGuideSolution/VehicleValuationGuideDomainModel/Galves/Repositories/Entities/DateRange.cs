﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Entities
{
    [Serializable]
    public class DateRange : IDateRange
    {
        public bool Covers(DateTime date)
        {
            return date.CompareTo(BeginDate) >= 0 && date.CompareTo(EndDate) <= 0;
        }

        public DateTime BeginDate { get; internal set; }

        public DateTime EndDate { get; internal set; }
    }
}
