﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories.Mappers;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Repositories
{
    public class GalvesRepository : RepositoryBase, IGalvesRepository
    {
        private readonly VehicleConfigurationMapper _mapper = new VehicleConfigurationMapper();

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        public IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle)
        {
            return DoInSession(() => _mapper.Load(broker, vehicle));
        }

        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on)
        {
            return DoInSession(() => _mapper.Load(broker, vehicle, on));
        }

        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id)
        {
            return DoInSession(() => _mapper.Load(broker, vehicle, id));
        }

        public IEdition<IPublication> Save(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            return DoInTransaction(
                session => OnBeginTransaction(session, principal),
                () => _mapper.Save(broker, vehicle, configuration, matrix));
        }

        private void OnBeginTransaction(IDataSession session, IPrincipal principal)
        {
            Audit audit = session.Items["Audit"] as Audit;

            if (audit == null)
            {
                audit = Resolve<IRepository>().Create(principal);

                session.Items["Audit"] = audit;
            }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // deprecated method
        }
    }
}
