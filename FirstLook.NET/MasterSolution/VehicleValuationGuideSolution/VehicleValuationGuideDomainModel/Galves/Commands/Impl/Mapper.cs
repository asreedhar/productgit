using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl
{
    public static class Mapper
    {
        public static List<StateDto> Map(IEnumerable<State> states)
        {
            List<StateDto> values = new List<StateDto>();

            foreach (State state in states)
            {
                values.Add(Map(state));
            }

            return values;
        }

        private static StateDto Map(State state)
        {
            return new StateDto { Name = state.Name };
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes)
        {
            return Map(nodes, Operation.No, Operation.No);
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes, Operation parents, Operation children)
        {
            List<NodeDto> list = new List<NodeDto>();

            foreach (ITreeNode node in nodes)
            {
                list.Add(Map(node, parents, children));
            }

            return list;
        }

        public class Operation
        {
            private readonly Operation _next;
            private readonly bool _perform;

            protected Operation(bool perform, Operation next)
            {
                _next = next ?? this;
                _perform = perform;
            }

            public Operation Next
            {
                get { return _next; }
            }

            public bool Perform
            {
                get { return _perform; }
            }

            public static readonly Operation Yes;
            public static readonly Operation No;
            public static readonly Operation NoThenYes;
            public static readonly Operation YesThenNo;
            public static readonly Operation YesThenYesThenNo;
            public static readonly Operation YesThenYesThenYesThenNo;

            static Operation()
            {
                Yes = new Operation(true, null);
                No = new Operation(false, null);
                NoThenYes = new Operation(false, Yes);
                YesThenNo = new Operation(true, No);
                YesThenYesThenNo = new Operation(true, YesThenNo);
                YesThenYesThenYesThenNo = new Operation(true, YesThenYesThenNo);
            }
        }

        public static NodeDto Map(ITreeNode node, Operation parents, Operation children)
        {
            ITreePath treePath = new TreePath();

            node.Save(treePath);

            NodeDto dto = new NodeDto
            {
                Value = node.Value,
                State = treePath.State,
                VehicleId = node.VehicleId.GetValueOrDefault(),
                HasVehicleId = node.VehicleId.HasValue,
                Label = node.Label
            };

            if (children.Perform)
            {
                dto.Children = Map(node.Children);
            }

            if (parents.Perform && node.Parent != null)
            {
                dto.Parent = Map(node.Parent, parents.Next, children.Next);
            }

            return dto;
        }

        public static IVehicleConfiguration Map(VehicleConfigurationDto configuration, State state, int? mileage)
        {
            if (state == null)
            {
                IGalvesService service = RegistryFactory.GetResolver().Resolve<IGalvesService>();

                state = service.States().First(x => Equals(x.Name, configuration.State.Name));
            }

            if (!mileage.HasValue)
            {
                mileage = configuration.HasMileage ? configuration.Mileage : default(int?);
            }

            IVehicleConfigurationBuilderFactory factory = RegistryFactory.GetResolver().Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            BookDateDto bookDateDto = configuration.BookDate;

            BookDate bookDate = new BookDate {Id = bookDateDto.Id, Value = bookDateDto.Value};

            builder.BookDate = bookDate;
            builder.VehicleId = configuration.VehicleId;
            builder.Vin = configuration.Vin;
            builder.State = state;
            builder.Mileage = mileage;

            foreach (AdjustmentActionDto value in configuration.AdjustmentActions)
            {
                builder.Do(value.AdjustmentName, (AdjustmentActionType)value.ActionType);
            }

            foreach (AdjustmentStateDto value in configuration.AdjustmentStates)
            {
                builder.Add(value.AdjustmentName, value.Enabled, value.Selected);
            }

            return builder.ToVehicleConfiguration();
        }

        public static PriceTableDto Map(Matrix matrix)
        {
            PriceTableDto table = new PriceTableDto();

            foreach (PriceType valueType in Enum.GetValues(typeof(PriceType)))
            {
                if (valueType == PriceType.Undefined)
                {
                    continue;
                }

                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    if (valuation == Valuation.Undefined)
                    {
                        continue;
                    }

                    decimal? value = matrix[valueType, valuation].Value;

                    SetPrice(table, valueType, valuation, value);
                }
            }

            return table;
        }

        private static void SetPrice(PriceTableDto table, PriceType priceType, Valuation valuation, decimal? value)
        {
            PriceTableRowTypeDto rowType;

            switch (valuation)
            {
                case Valuation.Base:
                    rowType = PriceTableRowTypeDto.Base;
                    break;
                case Valuation.Final:
                    rowType = PriceTableRowTypeDto.Final;
                    break;
                case Valuation.Mileage:
                    rowType = PriceTableRowTypeDto.Mileage;
                    break;
                case Valuation.Option:
                    rowType = PriceTableRowTypeDto.Option;
                    break;
                default:
                    return;
            }

            PriceTableRowDto row = table.Rows.FirstOrDefault(
                x => x.RowType == rowType);

            if (row == null)
            {
                row = new PriceTableRowDto
                {
                    Prices = new PricesDto(),
                    RowType = rowType
                };

                table.Rows.Add(row);
            }

            switch (priceType)
            {
                case PriceType.Retail:
                    row.Prices.Retail = value.GetValueOrDefault();
                    row.Prices.HasRetail = value.HasValue;
                    break;
                case PriceType.TradeIn:
                    row.Prices.TradeIn = value.GetValueOrDefault();
                    row.Prices.HasTradeIn = value.HasValue;
                    break;
            }
        }

        public static VehicleConfigurationDto Map(IVehicleConfiguration configuration)
        {
            return new VehicleConfigurationDto
            {
                BookDate = new BookDateDto { Id = configuration.BookDate.Id, Value = configuration.BookDate.Value },
                VehicleId = configuration.VehicleId,
                Vin = configuration.Vin,
                Mileage = configuration.Mileage.GetValueOrDefault(),
                HasMileage = configuration.Mileage.HasValue,
                State = Map(configuration.State),
                AdjustmentActions = Map(configuration.AdjustmentActions),
                AdjustmentStates = Map(configuration.AdjustmentStates)
            };
        }

        private static List<AdjustmentActionDto> Map(IEnumerable<IAdjustmentAction> actions)
        {
            List<AdjustmentActionDto> values = new List<AdjustmentActionDto>();

            foreach (IAdjustmentAction action in actions)
            {
                values.Add(
                    new AdjustmentActionDto
                    {
                        ActionType = (AdjustmentActionTypeDto)action.ActionType,
                        AdjustmentName = action.AdjustmentName
                    });
            }

            return values;
        }

        private static List<AdjustmentStateDto> Map(IEnumerable<IAdjustmentState> states)
        {
            List<AdjustmentStateDto> values = new List<AdjustmentStateDto>();

            foreach (IAdjustmentState state in states)
            {
                values.Add(
                    new AdjustmentStateDto
                    {
                        Enabled = state.Enabled,
                        Selected = state.Selected,
                        AdjustmentName = state.AdjustmentName
                    });
            }

            return values;
        }

        public static List<AdjustmentDto> Map(IList<Adjustment> adjustments, int regionId)
        {
            List<AdjustmentDto> values = new List<AdjustmentDto>();

            foreach (Adjustment option in adjustments)
            {
                Price price = option.Prices.FirstOrDefault(x => x.Region.Id == regionId);

                int amount = 0;

                if (price != null)
                {
                    if (option.AdjustmentCode == AdjustmentCode.Ded)
                    {
                        amount = -price.Amount;
                    }
                    else
                    {
                        amount = price.Amount;
                    }
                }

                string name = option.Name;

                name = name.Replace("ADD:", string.Empty).Replace("DED:", string.Empty);

                values.Add(
                    new AdjustmentDto
                    {
                        Name = name,
                        Amount = amount
                    });
            }

            return values;
        }
    }
}
