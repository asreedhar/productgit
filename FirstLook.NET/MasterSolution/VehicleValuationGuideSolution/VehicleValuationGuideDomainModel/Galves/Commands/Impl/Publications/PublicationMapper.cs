﻿using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl.Publications
{
    public static class PublicationMapper
    {
        public static List<PublicationInfoDto> Map(IList<IEdition<IPublicationInfo>> publications)
        {
            List<PublicationInfoDto> values = new List<PublicationInfoDto>();

            foreach (IEdition<IPublicationInfo> edition in publications)
            {
                IPublicationInfo publication = edition.Data;

                values.Add(
                    new PublicationInfoDto
                    {
                        ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                        ChangeType = (ChangeTypeDto)publication.ChangeType,
                        Id = publication.Id,
                        Edition = new EditionDto
                        {
                            BeginDate = edition.DateRange.BeginDate,
                            EndDate = edition.DateRange.EndDate,
                            User = new UserDto
                            {
                                FirstName = edition.User.FirstName,
                                LastName = edition.User.LastName,
                                UserName = edition.User.UserName
                            }
                        }
                    });
            }

            return values;
        }

        public static PublicationDto Map(IEdition<IPublication> edition)
        {
            if (edition == null) return null;

            IPublication publication = edition.Data;

            IGalvesService service = publication.Service;

            IVehicleConfiguration configuration = publication.VehicleConfiguration;

            IList<Adjustment> adjustments = service.Adjustments(configuration.VehicleId);
            
            return new PublicationDto
            {
                ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                ChangeType = (ChangeTypeDto)publication.ChangeType,
                //Id = publication.Id,
                //Edition = new EditionDto
                //{
                //    BeginDate = edition.DateRange.BeginDate,
                //    EndDate = edition.DateRange.EndDate,
                //    User = new UserDto
                //    {
                //        FirstName = edition.User.FirstName,
                //        LastName = edition.User.LastName,
                //        UserName = edition.User.UserName
                //    }
                //},
                Table = Mapper.Map(publication.Matrix),
                Adjustments = Mapper.Map(adjustments, configuration.State.Region.Id),
                VehicleConfiguration = Mapper.Map(configuration)
            };
        }
    }
}
