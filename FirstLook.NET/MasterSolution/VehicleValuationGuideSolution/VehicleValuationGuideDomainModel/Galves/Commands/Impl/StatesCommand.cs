﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl
{
    [Serializable]
    public class StatesCommand : ICommand<StatesResultsDto,StatesArgumentsDto>
    {
        public StatesResultsDto Execute(StatesArgumentsDto parameters)
        {
            IGalvesService service = RegistryFactory.GetResolver().Resolve<IGalvesService>();

            return new StatesResultsDto
            {
                States = Mapper.Map(service.States())
            };
        }
    }
}
