﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<StatesResultsDto, StatesArgumentsDto> CreateStatesCommand()
        {
            return new StatesCommand();
        }

        public ICommand<TraversalResultsDto, TraversalArgumentsDto> CreateTraversalCommand()
        {
            return new TraversalCommand();
        }

        public ICommand<SuccessorResultsDto, SuccessorArgumentsDto> CreateSuccessorCommand()
        {
            return new SuccessorCommand();
        }

        public ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto> CreateInitialValuationCommand()
        {
            return new InitialValuationCommand();
        }

        public ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto> CreateAdjustedValuationCommand()
        {
            return new AdjustedValuationCommand();
        }

        #region Publication

        public ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>> CreatePublicationListCommand()
        {
            return new PublicationListCommand();
        }

        public ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>> CreatePublicationOnDateCommand()
        {
            return new PublicationOnDateCommand();
        }

        public ICommand<PublicationLoadResultsDto, IdentityContextDto<PublicationLoadArgumentsDto>> CreatePublicationLoadCommand()
        {
            return new PublicationLoadCommand();
        }

        public ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>> CreatePublicationSaveCommand()
        {
            return new PublicationSaveCommand();
        }

        #endregion
    }
}
