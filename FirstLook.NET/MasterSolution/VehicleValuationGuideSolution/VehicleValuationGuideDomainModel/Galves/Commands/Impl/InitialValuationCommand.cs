﻿using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl
{
    [Serializable]
    public class InitialValuationCommand : ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
    {
        public InitialValuationResultsDto Execute(InitialValuationArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IGalvesService service = resolver.Resolve<IGalvesService>();

            State state = service.States().First(x => Equals(x.Name, parameters.State));

            IVehicleConfiguration conf = resolver.Resolve<IGalvesConfiguration>().InitialConfiguration(
                parameters.VehicleId,
                parameters.Vin,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?));

            IGalvesCalculator calc = resolver.Resolve<IGalvesCalculator>();

            Matrix matrix = calc.Calculate(service, conf);

            PriceTableDto tables = Mapper.Map(matrix);

            InitialValuationResultsDto results = new InitialValuationResultsDto
            {
                Arguments = parameters,
                Adjustments = Mapper.Map(service.Adjustments(parameters.VehicleId), state.Region.Id),
                Table = tables,
                VehicleConfiguration = Mapper.Map(conf)
            };

            return results;
        }
    }  
}
