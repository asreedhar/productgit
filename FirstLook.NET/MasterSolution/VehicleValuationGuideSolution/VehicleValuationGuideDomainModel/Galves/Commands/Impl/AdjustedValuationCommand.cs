﻿using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.Impl
{
    [Serializable]
    public class AdjustedValuationCommand : ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
    {
        public AdjustedValuationResultsDto Execute(AdjustedValuationArgumentsDto parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (parameters.VehicleConfiguration == null)
            {
                throw new ArgumentNullException("parameters", "VehicleConfiguration");
            }

            IResolver registry = RegistryFactory.GetResolver();

            IGalvesService service = registry.Resolve<IGalvesService>();

            State state = service.States().First(x => Equals(x.Name, parameters.State));

            IGalvesCalculator calculator = registry.Resolve<IGalvesCalculator>();

            IVehicleConfiguration configuration = Mapper.Map(
                parameters.VehicleConfiguration,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?));

            AdjustmentActionDto action = parameters.AdjustmentAction;

            if (action != null)
            {
                IAdjustmentState adjustmentState = configuration.AdjustmentStates.FirstOrDefault(
                    x => x.AdjustmentName.EndsWith(action.AdjustmentName));

                string name = adjustmentState == null
                                  ? action.AdjustmentName
                                  : adjustmentState.AdjustmentName;

                configuration = registry.Resolve<IGalvesConfiguration>().UpdateConfiguration(
                    service,
                    configuration,
                    new Action
                        {
                            ActionType = (AdjustmentActionType)action.ActionType,
                            AdjustmentName = name
                        });
            }

            Matrix matrix = calculator.Calculate(service, configuration);

            AdjustedValuationResultsDto results = new AdjustedValuationResultsDto
            {
                Arguments = parameters,
                Table = Mapper.Map(matrix),
                VehicleConfiguration = Mapper.Map(configuration)
            };

            return results;
        }

        class Action : IAdjustmentAction
        {
            public string AdjustmentName { get; set; }

            public AdjustmentActionType ActionType { get; set; }
        }
    }
}
