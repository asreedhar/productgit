﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public class AdjustmentActionDto
    {
        private AdjustmentActionTypeDto _actionType;
        private string _adjustmentName;

        public AdjustmentActionTypeDto ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }

        public string AdjustmentName
        {
            get { return _adjustmentName; }
            set { _adjustmentName = value; }
        }
    }
}
