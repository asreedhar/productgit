using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public enum AdjustmentActionTypeDto
    {
        Undefined,
        Add,
        Remove
    }
}
