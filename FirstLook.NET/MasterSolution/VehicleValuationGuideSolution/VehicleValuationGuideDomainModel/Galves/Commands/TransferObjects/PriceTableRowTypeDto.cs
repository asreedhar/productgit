﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public enum PriceTableRowTypeDto
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Final
    }
}
