﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Traversal
{
    [Serializable]
    public class PathDto
    {
        private NodeDto _currentNode;
        private List<NodeDto> _successors;

        public NodeDto CurrentNode
        {
            get { return _currentNode; }
            set { _currentNode = value; }
        }

        public List<NodeDto> Successors
        {
            get { return _successors; }
            set { _successors = value; }
        }
    }
}
