﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Traversal
{
    [Serializable]
    public class NodeDto
    {
        private NodeDto _parent;

        public NodeDto Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        private List<NodeDto> _children = new List<NodeDto>();

        public List<NodeDto> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        private int _vehicleId;
        private bool _hasVehicleId;

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public bool HasVehicleId
        {
            get { return _hasVehicleId; }
            set { _hasVehicleId = value; }
        }

        private string _label;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
    }
}
