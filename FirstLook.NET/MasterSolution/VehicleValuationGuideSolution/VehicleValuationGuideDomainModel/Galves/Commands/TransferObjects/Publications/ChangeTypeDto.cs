﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications
{
    [Serializable]
    [Flags]
    public enum ChangeTypeDto
    {
        None = 0,
        BookDate = 1 << 0,
        State = 1 << 1,
        VehicleId = 1 << 2,
        Mileage = 1 << 3,
        AdjustmentActions = 1 << 4,
        AdjustmentStates = 1 << 5
    }
}
