﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications
{
    [Serializable]
    public class PublicationDto : PublicationInfoDto
    {
        public VehicleConfigurationDto VehicleConfiguration { get; set; }

        public List<AdjustmentDto> Adjustments { get; set; }

        public PriceTableDto Table { get; set; }
    }
}
