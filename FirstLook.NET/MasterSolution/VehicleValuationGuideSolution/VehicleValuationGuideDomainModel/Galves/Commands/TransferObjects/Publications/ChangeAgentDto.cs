﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications
{
    [Serializable]
    public enum ChangeAgentDto
    {
        Undefined,
        User,
        System
    }
}
