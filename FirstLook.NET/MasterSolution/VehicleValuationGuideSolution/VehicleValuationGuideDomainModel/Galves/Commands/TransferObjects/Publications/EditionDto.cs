﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications
{
    [Serializable]
    public class EditionDto
    {
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public UserDto User { get; set; }
    }
}
