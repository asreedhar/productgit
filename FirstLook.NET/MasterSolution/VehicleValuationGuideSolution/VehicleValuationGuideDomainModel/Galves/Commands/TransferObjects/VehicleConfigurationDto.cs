﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public class VehicleConfigurationDto
    {
        private BookDateDto _bookDate;

        public BookDateDto BookDate
        {
            get { return _bookDate; }
            set { _bookDate = value; }
        }

        private int _vehicleId;
        private string _vin;
        
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        private int _mileage;
        private bool _hasMileage;

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }

        private StateDto _state;

        public StateDto State
        {
            get { return _state; }
            set { _state = value; }
        }

        private List<AdjustmentStateDto> _adjustmentStates;

        public List<AdjustmentStateDto> AdjustmentStates
        {
            get { return _adjustmentStates; }
            set { _adjustmentStates = value; }
        }

        private List<AdjustmentActionDto> _adjustmentActions;

        public List<AdjustmentActionDto> AdjustmentActions
        {
            get { return _adjustmentActions; }
            set { _adjustmentActions = value; }
        }
    }
}
