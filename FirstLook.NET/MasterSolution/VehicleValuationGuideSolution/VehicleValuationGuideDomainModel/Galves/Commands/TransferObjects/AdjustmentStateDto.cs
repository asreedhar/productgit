﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public class AdjustmentStateDto
    {
        private string _adjustmentName;
        private bool _enabled;
        private bool _selected;

        public string AdjustmentName
        {
            get { return _adjustmentName; }
            set { _adjustmentName = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
