﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public class PriceTableRowDto
    {
        private PriceTableRowTypeDto _rowType;
        private PricesDto _prices;

        public PriceTableRowTypeDto RowType
        {
            get { return _rowType; }
            set { _rowType = value; }
        }

        public PricesDto Prices
        {
            get { return _prices; }
            set { _prices = value; }
        }
    }
}
