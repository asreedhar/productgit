﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public class PriceTableDto
    {
        private bool _hasGalvesValue;
        private bool _hasMarketReadyValue;

        public bool HasGalvesValue
        {
            get { return _hasGalvesValue; }
            set { _hasGalvesValue = value; }
        }

        public bool HasMarketReadyValue
        {
            get { return _hasMarketReadyValue; }
            set { _hasMarketReadyValue = value; }
        }

        private List<PriceTableRowDto> _rows = new List<PriceTableRowDto>();

        public List<PriceTableRowDto> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
    }
}
