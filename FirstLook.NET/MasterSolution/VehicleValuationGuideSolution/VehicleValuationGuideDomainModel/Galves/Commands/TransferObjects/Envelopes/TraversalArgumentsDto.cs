﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class TraversalArgumentsDto : ArgumentsDto
    {
        private string _vin;

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
    }
}
