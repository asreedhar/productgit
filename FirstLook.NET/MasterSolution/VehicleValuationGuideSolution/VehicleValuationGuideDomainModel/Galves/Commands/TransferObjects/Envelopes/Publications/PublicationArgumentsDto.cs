using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle publications we desire to enumerate.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Vehicle with which the publications must be associated.
        /// </summary>
        public Guid Vehicle { get; set; }
    }
}