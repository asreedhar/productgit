﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationArgumentsDto : ValuationArgumentsDto
    {
        private int _vehicleId;
        private string _vin;

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
    } 
}
