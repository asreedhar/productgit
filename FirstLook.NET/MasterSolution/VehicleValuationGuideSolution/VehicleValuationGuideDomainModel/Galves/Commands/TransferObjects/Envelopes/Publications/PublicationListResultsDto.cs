using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationListResultsDto
    {
        public PublicationListArgumentsDto Arguments { get; set; }

        public List<PublicationInfoDto> Publications { get; set; }
    }
}
