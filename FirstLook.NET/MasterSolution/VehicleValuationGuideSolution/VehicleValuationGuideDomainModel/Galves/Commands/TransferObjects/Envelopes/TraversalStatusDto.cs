﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public enum TraversalStatusDto
    {
        Success,
        InvalidVinLength,
        InvalidVinCharacters,
        InvalidVinChecksum,
        NoDataForVin
    }
}
