using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationLoadArgumentsDto : PublicationArgumentsDto
    {
        public int Id { get; set; }
    }
}
