﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AdjustedValuationResultsDto : ValuationResultsDto
    {
        private AdjustedValuationArgumentsDto _arguments;

        public AdjustedValuationArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private VehicleConfigurationDto _vehicleConfiguration;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }
    } 
}
