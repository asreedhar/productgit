﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AdjustedValuationArgumentsDto : ValuationArgumentsDto
    {
        private VehicleConfigurationDto _vehicleConfiguration;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }

        private AdjustmentActionDto _adjustmentAction;

        public AdjustmentActionDto AdjustmentAction
        {
            get { return _adjustmentAction; }
            set { _adjustmentAction = value; }
        }
    } 
}
