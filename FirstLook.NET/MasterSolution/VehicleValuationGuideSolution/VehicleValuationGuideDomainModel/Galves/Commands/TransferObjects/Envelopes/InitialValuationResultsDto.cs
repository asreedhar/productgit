using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationResultsDto : ValuationResultsDto
    {
        private InitialValuationArgumentsDto _arguments;
        private List<AdjustmentDto> _adjustments;

        public InitialValuationArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<AdjustmentDto> Adjustments
        {
            get { return _adjustments; }
            set { _adjustments = value; }
        }

        private VehicleConfigurationDto _vehicleConfiguration;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }
    }
}
