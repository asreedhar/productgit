using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationOnDateResultsDto
    {
        public PublicationOnDateArgumentsDto Arguments { get; set; }

        public PublicationDto Publication { get; set; }
    }
}
