using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ValuationArgumentsDto : ArgumentsDto
    {
        private int _mileage;
        private bool _hasMileage;

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }
    }
}