using System;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationSaveResultsDto
    {
        public PublicationSaveArgumentsDto Arguments { get; set; }

        public PublicationDto Publication { get; set; }
    }
}
