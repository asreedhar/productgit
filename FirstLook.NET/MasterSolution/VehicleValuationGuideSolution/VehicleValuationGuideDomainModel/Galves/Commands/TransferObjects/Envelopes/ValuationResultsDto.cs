using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ValuationResultsDto
    {
        private PriceTableDto _table;

        public PriceTableDto Table
        {
            get { return _table; }
            set { _table = value; }
        }
    } 
}