﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects
{
    [Serializable]
    public class PricesDto
    {
        private decimal _tradeIn;
        private decimal _retail;

        public decimal TradeIn
        {
            get { return _tradeIn; }
            set { _tradeIn = value; }
        }

        public decimal Retail
        {
            get { return _retail; }
            set { _retail = value; }
        }

        private bool _hasTradeIn;
        private bool _hasRetail;

        public bool HasTradeIn
        {
            get { return _hasTradeIn; }
            set { _hasTradeIn = value; }
        }

        public bool HasRetail
        {
            get { return _hasRetail; }
            set { _hasRetail = value; }
        }
    }
}
