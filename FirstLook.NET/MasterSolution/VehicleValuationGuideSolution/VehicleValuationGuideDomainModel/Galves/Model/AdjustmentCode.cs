namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    public enum AdjustmentCode
    {
        /// <summary>
        /// Indicates the item is a standard option for the vehicle.  In this case
        /// add the option to a �standard items� list and do not adjust the vehicle
        /// value.  Do not include NUL items in the adjustment list.
        /// </summary>
        Nul,
        /// <summary>
        /// Add the value.
        /// </summary>
        Add, 
        /// <summary>
        /// Subtract the value.
        /// </summary>
        Ded
    }
}