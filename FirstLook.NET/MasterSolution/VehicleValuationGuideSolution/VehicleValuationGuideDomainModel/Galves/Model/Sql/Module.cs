using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql
{
    public class Module : IModule
    {
        internal class SerializerModule : IModule
        {
            public void Configure(IRegistry registry)
            {
                registry.Register<ISqlSerializer<State>, StateSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<BookDate>, BookDateSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Year>, YearSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Manufacturer>, ManufacturerSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Model>, ModelSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Style>, StyleSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Vehicle>, VehicleSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Adjustment>, AdjustmentSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<AdjustmentException>, AdjustmentExceptionSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<MileageAdjustment>, MileageAdjustmentSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<RegionalMileageAdjustment>, RegionalMileageAdjustmentSerializer>(ImplementationScope.Shared);
            }
        }

        public void Configure(IRegistry registry)
        {
            registry.Register<ISqlService, CurrentSqlService>(ImplementationScope.Shared);

            registry.Register<SerializerModule>();
        }
    }
}
