﻿using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql
{
    public class CurrentSqlService : SqlServiceBase
    {
        public CurrentSqlService()
        {
        }

        public CurrentSqlService(ICloneReader clone) : base(clone)
        {
        }

        protected override string DatabaseName
        {
            get { return "Galves"; }
        }

        protected override string SchemaName
        {
            get { return "dbo"; }
        }

        protected override string And
        {
            get { return "--"; }
        }
    }

    public class SnapshotSqlService : SqlServiceBase
    {
        public SnapshotSqlService()
        {
        }

        public SnapshotSqlService(ICloneReader clone) : base(clone)
        {
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override string SchemaName
        {
            get { return "Galves"; }
        }

        protected override string And
        {
            get { return "AND"; }
        }
    }

    public abstract class SqlServiceBase : ISqlService
    {
        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql.Resources.";

        private readonly ICloneReader _clone;

        protected SqlServiceBase()
        {
            _clone = new CloneReader();
        }

        protected SqlServiceBase(ICloneReader clone)
        {
            _clone = clone;
        }

        protected abstract string DatabaseName { get; }

        protected abstract string SchemaName { get; }

        protected abstract string And { get; }

        protected string CommandText(string resourceName)
        {
            return string.Format(
                Database.GetCommandText(
                    GetType().Assembly,
                    Prefix + resourceName),
                SchemaName,
                And);
        }

        public IDataReader BookDate()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_BookDate.txt");

                    return _clone.Snapshot(command, "BookDate");
                }
            }
        }

        public IDataReader Query(int bookDate)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Years.txt");
                    
                    return _clone.Snapshot(command, "Year");
                }
            }
        }

        public IDataReader Query(int bookDate, int year)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Makes.txt");

                    Database.AddWithValue(command, "ph_year", year, DbType.Int32);

                    return _clone.Snapshot(command, "Manufacturer");
                }
            }
        }

        public IDataReader Query(int bookDate, int year, string manufacturer)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Models.txt");
                    
                    Database.AddWithValue(command, "ph_year", year, DbType.Int32);

                    Database.AddWithValue(command, "ph_manuf", manufacturer, DbType.String);

                    return _clone.Snapshot(command, "Model");
                }
            }
        }

        public IDataReader Query(int bookDate, int year, string manufacturer, string model)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Styles.txt");

                    Database.AddWithValue(command, "ph_year", year, DbType.Int32);

                    Database.AddWithValue(command, "ph_manuf", manufacturer, DbType.String);

                    Database.AddWithValue(command, "ph_model", model, DbType.String);

                    return _clone.Snapshot(command, "Style");
                }
            }
        }

        public IDataReader Query(int bookDate, string vin)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Style__Vin.txt");

                    Database.AddWithValue(command, "VIN", vin, DbType.String);

                    return _clone.Snapshot(command, "Vin");
                }
            }
        }

        public IDataReader Vehicle(int bookDate, int vehicleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Style__Id.txt");

                    Database.AddWithValue(command, "ph_guid", vehicleId, DbType.Int32);

                    return _clone.Snapshot(command, "Vehicle");
                }
            }
        }

        public IDataReader States(int bookDate)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_States.txt");
                    
                    return _clone.Snapshot(command, "States");
                }
            }
        }

        public IDataReader Adjustments(int bookDate, int vehicleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Adjustments.txt");

                    Database.AddWithValue(command, "ph_guid", vehicleId, DbType.Int32);

                    return _clone.Snapshot(command, "Adjustments");
                }
            }
        }

        /// <remarks>
        /// <para><code>exception</code> is the selected adjustment. <code>corrfeatur</code> is
        /// the mutually exclusive adjustment to that selection.</para>
        /// </remarks>
        public IDataReader AdjustmentExceptions(int bookDate, int vehicleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_AdjustmentExceptions.txt");

                    Database.AddWithValue(command, "ph_guid", vehicleId, DbType.Int32);

                    return _clone.Snapshot(command, "AdjustmentExceptions");
                }
            }
        }

        public IDataReader MileageAdjustments(int bookDate, string mileageYear, string mileageGroupCode)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_MileageAdjustments.txt");

                    Database.AddWithValue(command, "ph_ccode", mileageGroupCode, DbType.String);

                    Database.AddWithValue(command, "mileage_year", mileageYear, DbType.String);

                    return _clone.Snapshot(command, "MileageAdjustments");
                }
            }
        }

        public IDataReader RegionalMileageAdjustments(int bookDate, string mileageYear, MileageClassification mileageClassification)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_RegionalMileageAdjustments.txt");

                    char mclass = 'N';

                    switch (mileageClassification)
                    {
                        case MileageClassification.Exotic:
                            mclass = 'E';
                            break;
                        case MileageClassification.Light:
                            mclass = 'L';
                            break;
                        case MileageClassification.Normal:
                            mclass = 'N';
                            break;
                        case MileageClassification.Special:
                            mclass = 'S';
                            break;
                    }

                    Database.AddWithValue(command, "ph_mclass", mclass, DbType.String);

                    Database.AddWithValue(command, "mileage_year", mileageYear, DbType.String);

                    return _clone.Snapshot(command, "RegionalMileageAdjustments");
                }
            }
        }
    }

    public interface ICloneReader
    {
        IDataReader Snapshot(IDbCommand command, params string[] names);
    }

    internal class CloneReader : ICloneReader
    {
        public IDataReader Snapshot(IDbCommand command, params string[] names)
        {
            DataSet set = new DataSet();

            using (IDataReader reader = command.ExecuteReader())
            {
                foreach (string name in names)
                {
                    DataTable table = new DataTable(name);

                    table.Load(reader);

                    set.Tables.Add(table);
                }
            }

            return set.CreateDataReader();
        }
    }
}
