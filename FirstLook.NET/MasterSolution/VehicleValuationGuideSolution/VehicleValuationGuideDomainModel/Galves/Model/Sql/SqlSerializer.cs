using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql
{
    public abstract class Serializer<T> : ISqlSerializer<T>
    {
        public IList<T> Deserialize(IDataReader reader)
        {
            List<T> values = new List<T>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public abstract T Deserialize(IDataRecord record);

        protected static TContract Resolve<TContract>()
        {
            return RegistryFactory.GetResolver().Resolve<TContract>();
        }
    }

    public class StateSerializer : Serializer<State>
    {
        public override State Deserialize(IDataRecord record)
        {
            string state = DataRecord.GetString(record, "State");

            int region = record.GetByte(record.GetOrdinal("Region"));

            return new State {Name = state, Region = Region.Values[region-1]};
        }
    }

    public class BookDateSerializer : Serializer<BookDate>
    {
        public override BookDate Deserialize(IDataRecord record)
        {
            int id = record.GetInt32((record.GetOrdinal("data_load_id")));

            string name = DataRecord.GetString(record, "book_date");

            return new BookDate {Value = name, Id = id};
        }
    }

    public class YearSerializer : Serializer<Year>
    {
        public override Year Deserialize(IDataRecord record)
        {
            int id = Convert.ToInt32(record.GetInt16((record.GetOrdinal("ph_year"))));

            return new Year{ Value = id };
        }
    }

    public class ManufacturerSerializer : Serializer<Manufacturer>
    {
        public override Manufacturer Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("ph_manuf"));

            return new Manufacturer { Value = name };
        }
    }

    public class ModelSerializer : Serializer<Model>
    {
        public override Model Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("ph_model_base"));

            return new Model { Value = name };
        }
    }

    public class StyleSerializer : Serializer<Style>
    {
        public override Style Deserialize(IDataRecord record)
        {
            string model = record.GetString(record.GetOrdinal("ph_model"));

            string body = record.GetString(record.GetOrdinal("ph_body"));

            string engine = record.GetString(record.GetOrdinal("ph_engtype"));

            int id = record.GetInt32(record.GetOrdinal("ph_guid"));

            string name = string.Format("{0} {1} {2}", model, body, engine);

            return new Style { Value = name, VehicleId = id};
        }
    }

    public class VehicleSerializer : Serializer<Vehicle>
    {
        public override Vehicle Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal("ph_guid"));

            Year year = Resolve<ISqlSerializer<Year>>().Deserialize(record);

            Manufacturer manufacturer = Resolve<ISqlSerializer<Manufacturer>>().Deserialize(record);

            Model model = Resolve<ISqlSerializer<Model>>().Deserialize(record);

            Style style = Resolve<ISqlSerializer<Style>>().Deserialize(record);

            int baseMileage = record.GetInt32(record.GetOrdinal("ph_mbase"));

            MileageClassification classification = MileageClassification.Normal;

            switch (record.GetString(record.GetOrdinal("ph_mclass")))
            {
                case "N":
                    classification = MileageClassification.Normal;
                    break;
                case "L":
                    classification = MileageClassification.Light;
                    break;
                case "E":
                    classification = MileageClassification.Exotic;
                    break;
                case "S":
                    classification = MileageClassification.Special;
                    break;
            }

            string mileageYear = DataRecord.GetString(record, "mileage_year");

            string mileageGroupCode = DataRecord.GetString(record, "ph_ccode");

            int tradeInRegion1 = record.GetInt32(record.GetOrdinal("ph_cprice"));
            int tradeInRegion2 = record.GetInt32(record.GetOrdinal("ph_cprice_region2"));
            int tradeInRegion3 = record.GetInt32(record.GetOrdinal("ph_cprice_region3"));
            int tradeInRegion4 = record.GetInt32(record.GetOrdinal("ph_cprice_region4"));
            int tradeInRegion5 = record.GetInt32(record.GetOrdinal("ph_cprice_region5"));
            int tradeInRegion6 = record.GetInt32(record.GetOrdinal("ph_cprice_region6"));

            int retailRegion1 = record.GetInt32(record.GetOrdinal("ph_price2"));
            int retailRegion2 = record.GetInt32(record.GetOrdinal("ph_price2_region2"));
            int retailRegion3 = record.GetInt32(record.GetOrdinal("ph_price2_region3"));
            int retailRegion4 = record.GetInt32(record.GetOrdinal("ph_price2_region4"));
            int retailRegion5 = record.GetInt32(record.GetOrdinal("ph_price2_region5"));
            int retailRegion6 = record.GetInt32(record.GetOrdinal("ph_price2_region6"));

            return new Vehicle
            {
                Id = id,
                Year = year,
                Manufacturer = manufacturer,
                Model = model,
                Style = style,
                BaseMileage = baseMileage,
                MileageYear = mileageYear,
                MileageClassification = classification,
                MileageGroupCode = mileageGroupCode,
                BasePrices = new List<Price>
                {
                    // trade in = galves value
                    new Price{Amount = tradeInRegion1, PriceType = PriceType.TradeIn, Region = Region.Values[0]},
                    new Price{Amount = tradeInRegion2, PriceType = PriceType.TradeIn, Region = Region.Values[1]},
                    new Price{Amount = tradeInRegion3, PriceType = PriceType.TradeIn, Region = Region.Values[2]},
                    new Price{Amount = tradeInRegion4, PriceType = PriceType.TradeIn, Region = Region.Values[3]},
                    new Price{Amount = tradeInRegion5, PriceType = PriceType.TradeIn, Region = Region.Values[4]},
                    new Price{Amount = tradeInRegion6, PriceType = PriceType.TradeIn, Region = Region.Values[5]},
                    // retail = galves market ready value
                    new Price{Amount = retailRegion1, PriceType = PriceType.Retail, Region = Region.Values[0]},
                    new Price{Amount = retailRegion2, PriceType = PriceType.Retail, Region = Region.Values[1]},
                    new Price{Amount = retailRegion3, PriceType = PriceType.Retail, Region = Region.Values[2]},
                    new Price{Amount = retailRegion4, PriceType = PriceType.Retail, Region = Region.Values[3]},
                    new Price{Amount = retailRegion5, PriceType = PriceType.Retail, Region = Region.Values[4]},
                    new Price{Amount = retailRegion6, PriceType = PriceType.Retail, Region = Region.Values[5]}
                }
            };
        }
    }

    public class AdjustmentSerializer : Serializer<Adjustment>
    {
        public override Adjustment Deserialize(IDataRecord record)
        {
            string description = record.GetString(record.GetOrdinal("description"));

            string amtCode = record.GetString(record.GetOrdinal("amt_code"));

            AdjustmentCode adjustmentCode = (AdjustmentCode)Enum.Parse(typeof(AdjustmentCode), amtCode, true);

            int region1Amount = record.GetInt32(record.GetOrdinal("amt")),
                region2Amount = record.GetInt32(record.GetOrdinal("amt_region2")),
                region3Amount = record.GetInt32(record.GetOrdinal("amt_region3")),
                region4Amount = record.GetInt32(record.GetOrdinal("amt_region4")),
                region5Amount = record.GetInt32(record.GetOrdinal("amt_region5")),
                region6Amount = record.GetInt32(record.GetOrdinal("amt_region6"));

            return new Adjustment
            {
                Name = description,
                AdjustmentCode = adjustmentCode,
                Prices = new List<Price>
                             {
                                 new Price{Amount = region1Amount, Region = Region.Values[0]},
                                 new Price{Amount = region2Amount, Region = Region.Values[1]},
                                 new Price{Amount = region3Amount, Region = Region.Values[2]},
                                 new Price{Amount = region4Amount, Region = Region.Values[3]},
                                 new Price{Amount = region5Amount, Region = Region.Values[4]},
                                 new Price{Amount = region6Amount, Region = Region.Values[5]}
                             }
            };
        }
    }

    public class AdjustmentExceptionSerializer : ISqlSerializer<AdjustmentException>
    {
        public IList<AdjustmentException> Deserialize(IDataReader reader)
        {
            List<AdjustmentException> values = new List<AdjustmentException>();

            AdjustmentException value = null;

            while (reader.Read())
            {
                string name = reader.GetString(reader.GetOrdinal("exception"));

                string related = reader.GetString(reader.GetOrdinal("corrfeatur"));

                if (value == null || !Equals(value.Name, name))
                {
                    value = new AdjustmentException
                    {
                        Name = name,
                        Exceptions = new List<string>
                                     {
                                         related
                                     }
                    };

                    values.Add(value);
                }
                else
                {
                    value.Exceptions.Add(related);
                }
            }

            return values;
        }

        public AdjustmentException Deserialize(IDataRecord record)
        {
            throw new NotSupportedException();
        }
    }

    public class MileageAdjustmentSerializer : Serializer<MileageAdjustment>
    {
        public override MileageAdjustment Deserialize(IDataRecord record)
        {
            double aboveFactor = record.GetDouble(record.GetOrdinal("abvfactor"));

            double belowFactor = record.GetDouble(record.GetOrdinal("blwfactor"));

            int miles = record.GetInt32(record.GetOrdinal("miles"));

            return new MileageAdjustment
            {
                AboveFactor = aboveFactor,
                BelowFactor = belowFactor,
                Miles = miles
            };
        }
    }

    public class RegionalMileageAdjustmentSerializer : Serializer<RegionalMileageAdjustment>
    {
        public override RegionalMileageAdjustment Deserialize(IDataRecord record)
        {
            int region = record.GetInt32(record.GetOrdinal("region"));

            int value = record.GetInt32(record.GetOrdinal("adjustment"));

            return new RegionalMileageAdjustment
            {
                Region = Region.Values[region-1],
                Value = value
            };
        }
    }
}
