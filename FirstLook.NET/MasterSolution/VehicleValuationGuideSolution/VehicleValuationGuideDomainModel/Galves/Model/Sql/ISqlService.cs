﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql
{
    public interface ISqlService
    {
        IDataReader BookDate();

        IDataReader Query(int bookDate);

        IDataReader Query(int bookDate, int year);

        IDataReader Query(int bookDate, int year, string manufacturer);

        IDataReader Query(int bookDate, int year, string manufacturer, string model);

        IDataReader Query(int bookDate, string vin);

        IDataReader Vehicle(int bookDate, int vehicleId);

        IDataReader States(int bookDate);

        IDataReader Adjustments(int bookDate, int vehicleId);

        IDataReader AdjustmentExceptions(int bookDate, int vehicleId);

        IDataReader MileageAdjustments(int bookDate, string mileageYear, string mileageGroupCode);

        IDataReader RegionalMileageAdjustments(int bookDate, string mileageYear, MileageClassification mileageClassification);
    }
}
