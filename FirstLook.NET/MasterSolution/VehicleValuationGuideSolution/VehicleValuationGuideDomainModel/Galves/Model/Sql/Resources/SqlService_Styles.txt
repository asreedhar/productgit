﻿
SELECT  v.ph_guid, COALESCE(m.webmodel, v.ph_model) ph_model, v.ph_body, v.ph_engtype
FROM    {0}.vehicles v
LEFT
JOIN    {0}.bgmanuf m on m.mf_name = v.ph_manuf and m.mf_type = v.ph_aft and m.mf_model = v.ph_model and m.year = v.ph_year
WHERE   v.ph_year = @ph_year
AND     COALESCE(m.webmanuf, v.ph_manuf) = @ph_manuf
AND     COALESCE(m.webbasemod, v.ph_model) = @ph_model
