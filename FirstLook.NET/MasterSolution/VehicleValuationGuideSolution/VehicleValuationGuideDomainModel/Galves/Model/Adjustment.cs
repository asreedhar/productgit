﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class Adjustment
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }
        
        private AdjustmentCode _adjustmentCode;

        public AdjustmentCode AdjustmentCode
        {
            get { return _adjustmentCode; }
            internal set { _adjustmentCode = value; }
        }

        private IList<Price> _prices = new List<Price>();

        public IList<Price> Prices
        {
            get { return _prices; }
            internal set { _prices = value; }
        }
    }
}
