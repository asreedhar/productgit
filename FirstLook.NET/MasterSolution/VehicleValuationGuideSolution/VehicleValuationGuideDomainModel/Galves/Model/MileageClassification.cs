﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public enum MileageClassification
    {
        /// <summary>
        /// N
        /// </summary>
        Normal,
        /// <summary>
        /// L
        /// </summary>
        Light,
        /// <summary>
        /// S
        /// </summary>
        Special,
        /// <summary>
        /// E
        /// </summary>
        Exotic
    }
}
