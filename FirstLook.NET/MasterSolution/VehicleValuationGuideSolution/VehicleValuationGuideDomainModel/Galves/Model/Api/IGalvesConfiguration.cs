﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public interface IGalvesConfiguration
    {
        IVehicleConfiguration InitialConfiguration(int vehicleId, string vin, State state, int? mileage);

        IVehicleConfiguration UpdateConfiguration(IGalvesService service, IVehicleConfiguration configuration, IAdjustmentAction action);
    }

    public interface IVehicleConfigurationBuilderFactory
    {
        IVehicleConfigurationBuilder NewBuilder();

        IVehicleConfigurationBuilder NewBuilder(IVehicleConfiguration configuration);
    }

    public interface IVehicleConfigurationBuilder
    {
        BookDate BookDate { get; set; }

        State State { get; set; }

        int VehicleId { get; set; }

        string Vin { get; set; }

        int? Mileage { get; set; }

        IAdjustmentState Add(Adjustment adjustment);

        IAdjustmentState Add(string adjustmentName, bool enabled, bool selected);

        void Disable(string adjustmentName);

        void Enable(string adjustmentName);

        bool Enabled(string adjustmentName);

        void Select(string adjustmentName);

        void Deselect(string adjustmentName);

        IEnumerable<string> Selected();

        bool IsSelected(string adjustmentName, bool returnValue);

        IAdjustmentAction Do(string adjustmentName, AdjustmentActionType actionType);

        IAdjustmentAction Undo(string adjustmentName);

        IVehicleConfiguration ToVehicleConfiguration();
    }

    public interface IVehicleConfiguration
    {
        BookDate BookDate { get; }

        State State { get; }

        int VehicleId { get; }

        string Vin { get; }

        int? Mileage { get; }

        IList<IAdjustmentAction> AdjustmentActions { get; }

        IList<IAdjustmentState> AdjustmentStates { get; }

        ChangeType Compare(IVehicleConfiguration configuration);
    }

    public interface IAdjustmentState
    {
        string AdjustmentName { get; }

        bool Enabled { get; }

        bool Selected { get; }
    }

    [Serializable]
    public enum AdjustmentActionType
    {
        Undefined,
        Add,
        Remove
    }

    public interface IAdjustmentAction
    {
        string AdjustmentName { get; }

        AdjustmentActionType ActionType { get; }
    }
}
