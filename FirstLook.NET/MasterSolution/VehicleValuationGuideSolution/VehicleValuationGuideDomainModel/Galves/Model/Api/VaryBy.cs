﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    [Serializable]
    internal enum VaryBy
    {
        None,
        Manufacturer,
        Model,
        Style
    }
}