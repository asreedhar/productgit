using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public class GalvesTraversal : IGalvesTraversal
    {
        protected const string PairSeparator = "||", ValueSeparator = "=";

        public ITree CreateTree()
        {
            return new Tree
            {
                Root = new InitialNode()
            };
        }

        public ITree CreateTree(string vin)
        {
            return new Tree
            {
                Root = new VinNode(vin)
            };
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        sealed class Tree : ITree
        {
            private ITreeNode _root;

            public ITreeNode Root
            {
                get { return _root; }
                set { _root = value; }
            }

            public ITreeNode GetNodeByPath(ITreePath path)
            {
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path.State))
                    {
                        int yearValue = 0;

                        string manufacturerValue = null,
                               modelName = null,
                               styleName = null;

                        int vehicleId = 0;

                        string vin = null;

                        string[] values = path.State.Split(new[] { PairSeparator }, StringSplitOptions.None);

                        foreach (var value in values)
                        {
                            string[] pair = value.Split(new[] { ValueSeparator }, StringSplitOptions.None);

                            string k = pair[0], v = pair[1];

                            switch (k)
                            {
                                case "year":
                                    yearValue = int.Parse(v);
                                    break;
                                case "manufacturer":
                                    manufacturerValue = v;
                                    break;
                                case "model":
                                    modelName = v;
                                    break;
                                case "style":
                                    styleName = v;
                                    break;
                                case "vin":
                                    vin = v;
                                    break;
                                case "id":
                                    vehicleId = int.Parse(v);
                                    break;
                            }
                        }

                        if (yearValue == 0)
                        {
                            if (string.IsNullOrEmpty(vin))
                            {
                                return new InitialNode();
                            }

                            return new VinNode(vin);
                        }

                        YearNode yearNode = new YearNode(yearValue);

                        if (string.IsNullOrEmpty(manufacturerValue))
                        {
                            return yearNode;
                        }

                        ManufacturerNode manufacturerNode = new ManufacturerNode(yearNode, manufacturerValue);

                        if (string.IsNullOrEmpty(modelName))
                        {
                            return manufacturerNode;
                        }

                        ModelNode modelNode = new ModelNode(manufacturerNode, modelName);

                        if (string.IsNullOrEmpty(styleName))
                        {
                            return modelNode;
                        }

                        return new StyleNode(modelNode, styleName, vehicleId);
                    }
                }

                return Root;
            }
        }

        sealed class InitialNode : TreeNode
        {
            private IList<ITreeNode> _children;

            public override string Label
            {
                get { return ""; }
            }

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Year year in Resolve<ILookupYear>().Lookup(CurrentBookDate()))
                        {
                            _children.Add(new YearNode(year.Value));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                path.State = string.Empty;
            }
        }

        static string Serialize(ITreeNode node)
        {
            return node.Label.ToLower() + ValueSeparator + node.Value;
        }

        sealed class YearNode : TreeNode
        {
            private readonly int _value;

            public YearNode(int value) : base (value.ToString())
            {
                _value = value;
            }

            public YearNode(ITreeNode parent, int value) : base(parent, value.ToString())
            {
                _value = value;
            }

            public override string Label
            {
                get { return "Year"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Manufacturer manuf in Resolve<ILookupManufacturer>().Lookup(CurrentBookDate(), _value))
                        {
                            _children.Add(new ManufacturerNode(this, manuf.Value));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(this);
                }
            }
        }

        sealed class ManufacturerNode : TreeNode
        {
            public ManufacturerNode(ITreeNode year, string name)
                : base(year, name)
            {
            }

            public override string Label
            {
                get { return "Manufacturer"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        int year = int.Parse(Parent.Value);

                        _children = new List<ITreeNode>();

                        foreach (Model model in Resolve<ILookupModel>().Lookup(CurrentBookDate(), year, Value))
                        {
                            _children.Add(new ModelNode(this, model.Value));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent) + PairSeparator + Serialize(this);
                }
            }
        }

        sealed class ModelNode : TreeNode
        {
            public ModelNode(ITreeNode manufacturer, string name)
                : base(manufacturer, name)
            {
            }

            public override string Label
            {
                get { return "Model"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int year = int.Parse(Parent.Parent.Value);

                        foreach (Style style in Resolve<ILookupStyle>().Lookup(CurrentBookDate(), year, Parent.Value, Value))
                        {
                            _children.Add(new StyleNode(this, style.Value, style.VehicleId));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent)
                        + PairSeparator + Serialize(Parent)
                        + PairSeparator + Serialize(this);
                }
            }
        }

        sealed class StyleNode : TreeNode
        {
            public StyleNode(ITreeNode parent, string value, int? vehicleId)
                : base(parent, value, vehicleId)
            {
            }

            public override string Label
            {
                get { return "Style"; }
            }

            private readonly IList<ITreeNode> _children = new List<ITreeNode>(0);

            public override IList<ITreeNode> Children
            {
                get
                {
                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent.Parent)    // year
                                 + PairSeparator + Serialize(Parent.Parent)   // make
                                 + PairSeparator + Serialize(Parent)          // model
                                 + PairSeparator + Serialize(this);           // style
                }
            }
        }

        sealed class VinNode : TreeNode
        {
            private readonly string _vin;

            public VinNode(string vin)
            {
                _vin = vin;
            }

            public override string Label
            {
                get { return "Vin"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<Vehicle> vehicles = Resolve<ILookupVin>().Lookup(CurrentBookDate(), _vin);

                        switch (Vary.By(vehicles))
                        {
                            case VaryBy.Manufacturer:
                                Make(vehicles);
                                break;
                            case VaryBy.Model:
                                Model(vehicles);
                                break;
                            case VaryBy.Style:
                                Style(vehicles);
                                break;
                            default:
                                Style(vehicles);
                                break;
                        }
                    }

                    return _children;
                }
            }

            private void Make(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    string make = info.Manufacturer.Value,
                           model = info.Model.Value,
                           style = info.Style.Value;

                    int year = info.Year.Value, id = info.Id;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year);

                        _parent = yearNode;
                    }

                    ITreeNode manufacturerNode = new ManufacturerNode(_parent, make),
                              modelNode = new ModelNode(manufacturerNode, model),
                              styleNode = new StyleNode(modelNode, style, id);

                    _children.Add(styleNode);
                }
            }

            private void Model(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    string make = info.Manufacturer.Value,
                           model = info.Model.Value,
                           style = info.Style.Value;

                    int year = info.Year.Value, id = info.Id;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  manufacturerNode = new ManufacturerNode(yearNode, make);

                        _parent = manufacturerNode;
                    }

                    ITreeNode modelNode = new ModelNode(_parent, model),
                              styleNode = new StyleNode(modelNode, style, id);

                    _children.Add(styleNode);
                }
            }

            private void Style(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    string make = info.Manufacturer.Value,
                           model = info.Model.Value,
                           style = info.Style.Value;

                    int year = info.Year.Value, id = info.Id;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  makeNode = new ManufacturerNode(yearNode, make),
                                  modelNode = new ModelNode(makeNode, model);

                        _parent = modelNode;
                    }

                    ITreeNode styleNode = new StyleNode(_parent, style, id);

                    _children.Add(styleNode);
                }
            }

            public override int? VehicleId
            {
                get
                {
                    if (Children.Count == 1)
                    {
                        return Children[0].VehicleId;
                    }

                    return base.VehicleId;
                }
            }

            private ITreeNode _parent;

            public override ITreeNode Parent
            {
                get
                {
                    IList<ITreeNode> eek = Children; // why oh why

                    return _parent;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(this);
                }
            }
        }
    }

    public abstract class TreeNode : ITreeNode
    {
        private readonly ITreeNode _parent;
        private string _value;
        private int? _vehicleId;

        protected TreeNode()
        {
        }

        protected TreeNode(string value)
        {
            _value = value;
        }

        protected TreeNode(ITreeNode parent, string value)
        {
            _parent = parent;
            _value = value;
        }

        protected TreeNode(ITreeNode parent, string value, int? vehicleId)
        {
            _parent = parent;
            _value = value;
            _vehicleId = vehicleId;
        }

        public abstract string Label { get; }

        public virtual string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }

        public virtual int? VehicleId
        {
            get { return _vehicleId; }
            internal set { _vehicleId = value; }
        }

        public virtual ITreeNode Parent
        {
            get { return _parent; }
        }

        public abstract IList<ITreeNode> Children { get; }

        public virtual bool HasChildren
        {
            get { return Children.Count == 0; }
        }

        public abstract void Save(ITreePath path);

        protected int CurrentBookDate()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ILookupBookDate lookup = resolver.Resolve<ILookupBookDate>();

            return lookup.Lookup().Max(x => x.Id);
        }
    }

    public class TreePath : ITreePath
    {
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
