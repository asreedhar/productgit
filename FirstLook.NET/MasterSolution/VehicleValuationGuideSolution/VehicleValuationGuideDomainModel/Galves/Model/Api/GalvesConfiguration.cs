using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public class GalvesConfiguration : IGalvesConfiguration
    {
        public IVehicleConfiguration InitialConfiguration(int vehicleId, string vin, State state, int? mileage)
        {
            IGalvesService service = Resolve<IGalvesService>();

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            builder.BookDate = service.BookDate();
            builder.VehicleId = vehicleId;
            builder.Vin = vin;
            builder.State = state;
            builder.Mileage = mileage;

            Vehicle vehicle = service.Vehicle(vehicleId);

            InitializeConfiguration(service, vehicle, builder);

            return builder.ToVehicleConfiguration();
        }

        private static void InitializeConfiguration(IGalvesService service, Vehicle vehicle, IVehicleConfigurationBuilder builder)
        {
            IList<Adjustment> adjustments = service.Adjustments(vehicle.Id);

            foreach (Adjustment adjustment in adjustments)
            {
                builder.Add(adjustment);
            }
        }

        public IVehicleConfiguration UpdateConfiguration(IGalvesService service, IVehicleConfiguration configuration, IAdjustmentAction action)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (action.ActionType == AdjustmentActionType.Undefined)
            {
                throw new ArgumentOutOfRangeException("action", AdjustmentActionType.Undefined, "Action type cannot be undefined");
            }

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            bool repeat = configuration.AdjustmentActions.Count(
                              x => x.ActionType == action.ActionType &&
                                   x.AdjustmentName == action.AdjustmentName) > 0;

            if (repeat)
            {
                return configuration; // been there, done that ...
            }

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder;

            bool undo = configuration.AdjustmentActions.Count(
                              x => x.ActionType != action.ActionType &&
                                   x.AdjustmentName == action.AdjustmentName) > 0;

            if (undo)
            {
                builder = factory.NewBuilder();

                builder.BookDate = service.BookDate();
                builder.VehicleId = configuration.VehicleId;
                builder.Vin = configuration.Vin;
                builder.State = configuration.State;
                builder.Mileage = configuration.Mileage;

                InitializeConfiguration(service, vehicle, builder);

                foreach (IAdjustmentAction replay in configuration.AdjustmentActions)
                {
                    if (replay.AdjustmentName != action.AdjustmentName)
                    {
                        PerformAction(service, replay, vehicle, builder);
                    }
                }
            }
            else
            {
                builder = factory.NewBuilder(configuration);

                PerformAction(service, action, vehicle, builder);
            }

            return builder.ToVehicleConfiguration();
        }

        static void PerformAction(IGalvesService service, IAdjustmentAction action, Vehicle vehicle, IVehicleConfigurationBuilder builder)
        {
            IList<Adjustment> adjustments = service.Adjustments(vehicle.Id);

            IList<AdjustmentException> exceptions = service.AdjustmentExceptions(vehicle.Id);

            Adjustment adjustment = adjustments.FirstOrDefault(x => Equals(x.Name, action.AdjustmentName));

            if (adjustment == null)
            {
                throw new ArgumentException(string.Format("No such Adjustment {0}", action.AdjustmentName), "action");
            }

            builder.Do(action.AdjustmentName, action.ActionType);

            switch (action.ActionType)
            {
                case AdjustmentActionType.Undefined:
                    throw new ArgumentException("Undefined OptionActionType", "action");
                case AdjustmentActionType.Add:
                    AddOption(builder, exceptions, adjustments, adjustment);
                    break;
                case AdjustmentActionType.Remove:
                    RemoveOption(builder, adjustment, false);
                    break;
            }
        }

        static void AddOption(IVehicleConfigurationBuilder builder, IEnumerable<AdjustmentException> exceptions, IEnumerable<Adjustment> adjustments, Adjustment adjustment)
        {
            if (builder.IsSelected(adjustment.Name, true))
            {
                return;
            }

            builder.Select(adjustment.Name);

            foreach (AdjustmentException exception in exceptions.Where(x => Equals(x.Name, adjustment.Name)))
            {
                ConflictsWith(builder, adjustments, exception);
            }
        }

        static void RemoveOption(IVehicleConfigurationBuilder builder, Adjustment adjustment, bool updateActions)
        {
            if (updateActions)
            {
                builder.Undo(adjustment.Name);
            }

            if (!builder.IsSelected(adjustment.Name, false))
            {
                return;
            }

            builder.Deselect(adjustment.Name);
        }

        static void ConflictsWith(IVehicleConfigurationBuilder builder, IEnumerable<Adjustment> adjustments, AdjustmentException adjustmentException)
        {
            foreach (string exception in adjustmentException.Exceptions)
            {
                string adjustmentName = exception; // no warning plz

                Adjustment adjustment = adjustments.First(x => Equals(x.Name, adjustmentName));

                RemoveOption(builder, adjustment, true);
            }
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
