﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IGalvesTraversal, GalvesTraversal>(ImplementationScope.Shared);

            registry.Register<IGalvesService, GalvesService>(ImplementationScope.Shared);

            registry.Register<IGalvesCalculator, GalvesCalculator>(ImplementationScope.Shared);

            registry.Register<IGalvesConfiguration, GalvesConfiguration>(ImplementationScope.Shared);
        }
    }
}
