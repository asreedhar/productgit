using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public class GalvesCalculator : IGalvesCalculator
    {
        public Matrix Calculate(IGalvesService service, IVehicleConfiguration configuration)
        {
            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            return Calculate(service, vehicle, configuration);
        }

        static Matrix Calculate(IGalvesService service, Vehicle vehicle, IVehicleConfiguration configuration)
        {
            Matrix matrix = new Matrix();

            CalculateBase(vehicle, configuration, matrix);

            CalculateOptions(service, vehicle, configuration, matrix);

            CalculateMileage(service, vehicle, configuration, matrix);

            CalculateFinal(matrix);

            return matrix;
        }

        private static void CalculateBase(Vehicle vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            int regionId = configuration.State.Region.Id;

            foreach (Price value in vehicle.BasePrices.Where(x => x.Region.Id == regionId))
            {
                matrix[value.PriceType, Valuation.Base].Value = value.Amount;
            }
        }

        private static void CalculateOptions(IGalvesService service, Vehicle vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            int regionId = configuration.State.Region.Id;

            foreach (Adjustment adjustment in service.Adjustments(vehicle.Id))
            {
                Adjustment adj = adjustment; // no warnings plz

                IAdjustmentState state = configuration.AdjustmentStates.FirstOrDefault(
                    x => Equals(x.AdjustmentName, adj.Name));

                bool adjust = false;

                int sign = 0;

                if (state.Selected)
                {
                    switch(adjustment.AdjustmentCode)
                    {
                        case AdjustmentCode.Add:
                            adjust = true;
                            sign = 1;
                            break;
                        case AdjustmentCode.Ded:
                            adjust = true;
                            sign = -1;
                            break;
                    }
                }

                if (adjust)
                {
                    Price price = adj.Prices.FirstOrDefault(x => x.Region.Id == regionId);

                    decimal? sum = Sum(
                        matrix[price.PriceType, Valuation.Option].Value,
                        sign * price.Amount);

                    foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
                    {
                        matrix[priceType, Valuation.Option].Value = sum;
                    }
                }
            }
        }

        private static void CalculateMileage(IGalvesService service, Vehicle vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            if (configuration.Mileage.HasValue)
            {
                int value = configuration.Mileage.Value;

                int regionId = configuration.State.Region.Id;

                // 1. Lookup Regional Base Mileage

                IList<RegionalMileageAdjustment> rs = service.RegionalMileageAdjustments(vehicle.Id);

                RegionalMileageAdjustment r = rs.FirstOrDefault(x => x.Region.Id == regionId);

                int mileageBase = vehicle.BaseMileage;

                if (r != null)
                {
                    mileageBase += r.Value;
                }

                // 2. Lookup Above/Below Factors

                IList<MileageAdjustment> ms = service.MileageAdjustments(vehicle.Id);

                // 3. Lookup Adjustment Factors

                int thousands = (value/1000);

                int adjustmentMiles = Math.Abs(mileageBase - thousands);

                if (adjustmentMiles > 227)
                {
                    adjustmentMiles = 227;
                }

                MileageAdjustment m = ms.FirstOrDefault(x => x.Miles == adjustmentMiles);

                if (m == null)
                {
                    return;
                }

                // 4. Adjustment = -1 * TradeIn * (1 - (factor/100))

                decimal tradeIn = matrix[PriceType.TradeIn, Valuation.Base].Value.GetValueOrDefault(),
                        factor;

                if (thousands > mileageBase)
                {
                    factor = Convert.ToDecimal(m.AboveFactor);
                }
                else
                {
                    factor = Convert.ToDecimal(m.BelowFactor);
                }

                decimal mileageAdjustment = -1m*(tradeIn*(1m - factor/100m));

                // 5. Round to nearest 25

                decimal remainder = mileageAdjustment%25m;

                decimal sign = Math.Sign(remainder);

                remainder = sign*remainder; // lose sign (add back in later)

                if (remainder >= 13m)
                {
                    mileageAdjustment += sign*(25m - remainder);
                }
                else
                {
                    mileageAdjustment -= sign*remainder;
                }

                // save value

                foreach (PriceType priceType in Enum.GetValues(typeof (PriceType)))
                {
                    matrix[priceType, Valuation.Mileage].Value = mileageAdjustment;
                }
            }
        }

        private static void CalculateFinal(Matrix matrix)
        {
            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined)
                {
                    continue;
                }

                decimal? basePrice = matrix[priceType, Valuation.Base].Value;

                if (!basePrice.HasValue)
                {
                    continue;
                }

                decimal? finalPrice = 0;

                if (basePrice > 0)
                {
                    decimal? optionAdjustment = matrix[priceType, Valuation.Option].Value;

                    decimal? mileageAdjustment = matrix[priceType, Valuation.Mileage].Value;

                    finalPrice = Sum(Sum(basePrice, optionAdjustment), mileageAdjustment);
                }

                matrix[priceType, Valuation.Final].Value = finalPrice;
            }
        }

        static decimal? Sum(decimal? a, decimal? b)
        {
            if (a.HasValue)
            {
                if (b.HasValue)
                {
                    return a.Value + b.Value;
                }

                return a.Value;
            }

            if (b.HasValue)
            {
                return b.Value;
            }

            return null;
        }
    }
}
