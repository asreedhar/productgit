﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public interface IGalvesCalculator
    {
        Matrix Calculate(IGalvesService service, IVehicleConfiguration configuration);
    }

    [Serializable]
    public enum Valuation
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Final
    }

    public class Matrix
    {
        public class Cell
        {
            private bool _visible = true;

            private decimal? _value;

            internal Cell()
            {
            }

            public bool Visible
            {
                get { return _visible; }
                internal set { _visible = value; }
            }

            public decimal? Value
            {
                get { return _value; }
                internal set { _value = value; }
            }
        }

        private static readonly int PriceTypeCount = Enum.GetNames(typeof(PriceType)).Length;

        private static readonly int ValuationCount = Enum.GetNames(typeof(Valuation)).Length;

        private readonly Cell[] _values;

        public Matrix()
        {
            int arrayLength = PriceTypeCount * ValuationCount;

            _values = new Cell[arrayLength];

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    Cell cell = new Cell();

                    if (priceType == PriceType.Undefined ||
                        valuation == Valuation.Undefined)
                    {
                        cell.Visible = false;
                    }

                    this[priceType, valuation] = cell;
                }
            }
        }


        public Cell this[PriceType priceType, Valuation valuation]
        {
            get
            {
                return _values[Index(priceType, valuation)];
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("A Matrix Cell should never be set to null!");
                }

                _values[Index(priceType, valuation)] = value;
            }
        }

        private static int Index(PriceType priceType, Valuation valuation)
        {
            int rowLength = ValuationCount; // = number of columns

            int row = (int) priceType;

            int col = ((int)valuation);

            return row * rowLength + col; // = (row * number of columns) + col
        }
    }
}
