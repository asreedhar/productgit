using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public delegate int GalvesServiceBookDate();

    public class GalvesService : IGalvesService
    {
        private readonly IResolver _resolver;

        private readonly GalvesServiceBookDate _bookDate;

        public GalvesService(IResolver resolver, GalvesServiceBookDate bookDate)
        {
            _resolver = resolver;

            _bookDate = bookDate;
        }

        public GalvesService()
        {
            _resolver = RegistryFactory.GetResolver();

            _bookDate = () => _resolver.Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);
        }

        public BookDate BookDate()
        {
            ILookupBookDate lookup = Resolve<ILookupBookDate>();

            IList<BookDate> dates = lookup.Lookup();

            return dates.First(x => x.Id == dates.Max(y => y.Id));
        }

        public IList<State> States()
        {
            ILookupState lookup = Resolve<ILookupState>();

            return lookup.Lookup(_bookDate());
        }

        public Vehicle Vehicle(int vehicleId)
        {
            ILookupVehicle lookup = Resolve<ILookupVehicle>();

            return lookup.Lookup(_bookDate(), vehicleId);
        }

        public IList<Adjustment> Adjustments(int vehicleId)
        {
            ILookupAdjustment lookup = Resolve<ILookupAdjustment>();

            return lookup.Lookup(_bookDate(), vehicleId);
        }

        public IList<AdjustmentException> AdjustmentExceptions(int vehicleId)
        {
            ILookupAdjustmentException lookup = Resolve<ILookupAdjustmentException>();

            return lookup.Lookup(_bookDate(), vehicleId);
        }

        public IList<MileageAdjustment> MileageAdjustments(int vehicleId)
        {
            ILookupMileageAdjustment lookup = Resolve<ILookupMileageAdjustment>();

            Vehicle vehicle = Vehicle(vehicleId);

            return lookup.Lookup(_bookDate(), vehicle.MileageYear, vehicle.MileageGroupCode);
        }

        public IList<RegionalMileageAdjustment> RegionalMileageAdjustments(int vehicleId)
        {
            ILookupRegionalMileageAdjustment lookup = Resolve<ILookupRegionalMileageAdjustment>();

            Vehicle vehicle = Vehicle(vehicleId);

            return lookup.Lookup(_bookDate(), vehicle.MileageYear, vehicle.MileageClassification);
        }

        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
