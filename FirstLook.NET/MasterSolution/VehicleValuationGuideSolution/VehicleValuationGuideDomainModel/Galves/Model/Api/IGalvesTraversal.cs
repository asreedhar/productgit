using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public interface IGalvesTraversal
    {    
        ITree CreateTree();

        ITree CreateTree(string vin);
    }

    public interface ITree
    {
        ITreeNode Root { get; }

        ITreeNode GetNodeByPath(ITreePath path);
    }

    public interface ITreeNode
    {
        ITreeNode Parent { get; }

        string Label { get; }

        string Value { get; }

        int? VehicleId { get; }

        IList<ITreeNode> Children { get; }

        bool HasChildren { get; }

        void Save(ITreePath path);
    }

    public interface ITreePath
    {
        string State { get; set; }
    }
}
