﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    internal static class Vary
    {
        public static VaryBy By(IList<Vehicle> values)
        {
            if (values == null || values.Count == 0)
            {
                return VaryBy.None;
            }

            int ctr = 0;

            while (ctr < 3)
            {
                INamedValue reference = null, item = null;

                int lop = 0;

                for (int i = 0, l = values.Count; i < l; i++)
                {
                    Vehicle value = values[i];
                
                    if (value == null)
                    {
                        continue;
                    }

                    switch (ctr)
                    {
                        case 0:
                            item = value.Manufacturer;
                            break;
                        case 1:
                            item = value.Model;
                            break;
                        case 2:
                            item = value.Style;
                            break;
                    }

                    if (item == null && l == 1)
                    {
                        return (VaryBy) Enum.ToObject(typeof (VaryBy), ctr);
                    }

                    if (lop++ == 0)
                    {
                        reference = item;
                    }
                    else
                    {
                        string a = reference != null ? reference.Value : string.Empty,
                               b = item != null ? item.Value : string.Empty;

                        if (!Equals(a, b))
                        {
                            return (VaryBy) Enum.ToObject(typeof (VaryBy), ctr+1);
                        }
                    }
                }

                ctr++;
            }

            return VaryBy.None;
        }

        public static IList<INamedValue> To(IList<Vehicle> values, VaryBy vary)
        {
            IList<INamedValue> items = new List<INamedValue>();

            foreach (Vehicle value in values)
            {
                switch (vary)
                {
                    case VaryBy.Manufacturer:
                        items.Add(value.Manufacturer);
                        break;
                    case VaryBy.Model:
                        items.Add(value.Model);
                        break;
                    case VaryBy.Style:
                        items.Add(value.Style);
                        break;
                }
            }

            return items;
        }
    }
}