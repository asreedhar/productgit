using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api
{
    public interface IGalvesService
    {
        BookDate BookDate();

        IList<State> States();

        Vehicle Vehicle(int vehicleId);

        IList<Adjustment> Adjustments(int vehicleId);

        IList<AdjustmentException> AdjustmentExceptions(int vehicleId);

        IList<MileageAdjustment> MileageAdjustments(int vehicleId);

        IList<RegionalMileageAdjustment> RegionalMileageAdjustments(int vehicleId);
    }
}
