﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class AdjustmentException
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        private IList<string> _exceptions = new List<string>();

        public IList<string> Exceptions
        {
            get { return _exceptions; }
            internal set { _exceptions = value; }
        }
    }
}
