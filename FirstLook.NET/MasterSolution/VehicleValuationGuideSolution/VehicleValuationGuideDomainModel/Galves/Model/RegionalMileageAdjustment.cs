﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class RegionalMileageAdjustment
    {
        private Region _region;
        private int _value;

        public Region Region
        {
            get { return _region; }
            set { _region = value; }
        }

        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
