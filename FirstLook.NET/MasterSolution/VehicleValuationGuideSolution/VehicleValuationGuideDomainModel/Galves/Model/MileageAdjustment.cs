﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    /// <summary>
    /// Lookup (mileage year, "mileage group code")
    /// "mileage group code" = ph_ccode and maps through cltrans
    /// </summary>
    [Serializable]
    public class MileageAdjustment
    {
        private int _miles;
        private double _aboveFactor;
        private double _belowFactor;

        public int Miles
        {
            get { return _miles; }
            internal set { _miles = value; }
        }

        public double AboveFactor
        {
            get { return _aboveFactor; }
            internal set { _aboveFactor = value; }
        }

        public double BelowFactor
        {
            get { return _belowFactor; }
            internal set { _belowFactor = value; }
        }
    }
}
