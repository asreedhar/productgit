﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class Vehicle
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        private Year _year;
        private Manufacturer _manufacturer;
        private Model _model;
        private Style _style;

        public Year Year
        {
            get { return _year; }
            internal set { _year = value; }
        }

        public Manufacturer Manufacturer
        {
            get { return _manufacturer; }
            internal set { _manufacturer = value; }
        }

        public Model Model
        {
            get { return _model; }
            internal set { _model = value; }
        }

        public Style Style
        {
            get { return _style; }
            internal set { _style = value; }
        }

        private int _baseMileage;
        private MileageClassification _mileageClassification;
        private string _mileageYear;
        private string _mileageGroupCode;

        /// <summary>
        /// In Thousands
        /// </summary>
        public int BaseMileage
        {
            get { return _baseMileage; }
            internal set { _baseMileage = value; }
        }

        public MileageClassification MileageClassification
        {
            get { return _mileageClassification; }
            internal set { _mileageClassification = value; }
        }

        public string MileageYear
        {
            get { return _mileageYear; }
            internal set { _mileageYear = value; }
        }

        public string MileageGroupCode
        {
            get { return _mileageGroupCode; }
            internal set { _mileageGroupCode = value; }
        }

        private IList<Price> _basePrices;

        public IList<Price> BasePrices
        {
            get { return _basePrices; }
            internal set { _basePrices = value; }
        }
    }
}
