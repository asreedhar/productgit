﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupVehicle
    {
        Vehicle Lookup(int bookDate, int vehicleId);
    }

    public class LookupVehicle : ILookupVehicle
    {
        public Vehicle Lookup(int bookDate, int vehicleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Vehicle> serializer = registry.Resolve<ISqlSerializer<Vehicle>>();

            using (IDataReader reader = service.Vehicle(bookDate, vehicleId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupVehicle : CachedLookup, ILookupVehicle
    {
        private readonly ILookupVehicle _source = new LookupVehicle();

        public Vehicle Lookup(int bookDate, int vehicleId)
        {
            string key = CreateCacheKey(vehicleId);

            Vehicle values = Cache.Get(key) as Vehicle;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
