using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupBookDate, CachedLookupBookDate>(ImplementationScope.Shared);

            registry.Register<ILookupYear, CachedLookupYear>(ImplementationScope.Shared);

            registry.Register<ILookupManufacturer, CachedLookupManufacturer>(ImplementationScope.Shared);

            registry.Register<ILookupModel, CachedLookupModel>(ImplementationScope.Shared);

            registry.Register<ILookupStyle, CachedLookupStyle>(ImplementationScope.Shared);

            registry.Register<ILookupVehicle, CachedLookupVehicle>(ImplementationScope.Shared);

            registry.Register<ILookupState, CachedLookupState>(ImplementationScope.Shared);

            registry.Register<ILookupVin, CachedLookupVin>(ImplementationScope.Shared);

            registry.Register<ILookupAdjustment, CachedLookupAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupAdjustmentException, CachedLookupAdjustmentException>(ImplementationScope.Shared);

            registry.Register<ILookupMileageAdjustment, CachedLookupMileageAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupRegionalMileageAdjustment, CachedLookupRegionalMileageAdjustment>(ImplementationScope.Shared);
        }
    }
}
