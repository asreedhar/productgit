﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupModel
    {
        IList<Model> Lookup(int bookDate, int yearId, string manufacturer);
    }

    public class LookupModel : ILookupModel
    {
        public IList<Model> Lookup(int bookDate, int yearId, string manufacturer)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Model> serializer = registry.Resolve<ISqlSerializer<Model>>();

            using (IDataReader reader = service.Query(bookDate, yearId, manufacturer))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupModel : CachedLookup, ILookupModel
    {
        private readonly ILookupModel _source = new LookupModel();

        public IList<Model> Lookup(int bookDate, int yearId, string manufacturer)
        {
            string key = CreateCacheKey(bookDate, yearId, manufacturer);

            IList<Model> values = Cache.Get(key) as IList<Model>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, yearId, manufacturer);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
