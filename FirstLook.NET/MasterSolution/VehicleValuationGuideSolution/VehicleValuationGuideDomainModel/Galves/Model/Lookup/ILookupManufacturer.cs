﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupManufacturer
    {
        IList<Manufacturer> Lookup(int bookDate, int yearId);
    }

    public class LookupManufacturer : ILookupManufacturer
    {
        public IList<Manufacturer> Lookup(int bookDate, int yearId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Manufacturer> serializer = registry.Resolve<ISqlSerializer<Manufacturer>>();

            using (IDataReader reader = service.Query(bookDate, yearId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupManufacturer : CachedLookup, ILookupManufacturer
    {
        private readonly ILookupManufacturer _source = new LookupManufacturer();

        public IList<Manufacturer> Lookup(int bookDate, int yearId)
        {
            string key = CreateCacheKey(bookDate, yearId);

            IList<Manufacturer> values = Cache.Get(key) as IList<Manufacturer>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, yearId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
