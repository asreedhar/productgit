﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupMileageAdjustment
    {
        IList<MileageAdjustment> Lookup(int bookDate, string mileageYear, string mileageGroupCode);
    }

    public class LookupMileageAdjustment : ILookupMileageAdjustment
    {
        public IList<MileageAdjustment> Lookup(int bookDate, string mileageYear, string mileageGroupCode)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<MileageAdjustment> serializer = resolver.Resolve<ISqlSerializer<MileageAdjustment>>();

            using (IDataReader reader = service.MileageAdjustments(bookDate, mileageYear, mileageGroupCode))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupMileageAdjustment : CachedLookup, ILookupMileageAdjustment
    {
        private readonly ILookupMileageAdjustment _source = new LookupMileageAdjustment();

        public IList<MileageAdjustment> Lookup(int bookDate, string mileageYear, string mileageGroupCode)
        {
            string key = CreateCacheKey(bookDate, mileageYear, mileageGroupCode);

            IList<MileageAdjustment> values = Cache.Get(key) as IList<MileageAdjustment>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, mileageYear, mileageGroupCode);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
