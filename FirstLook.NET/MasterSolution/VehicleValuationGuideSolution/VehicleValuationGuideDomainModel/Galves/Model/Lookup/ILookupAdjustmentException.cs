﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupAdjustmentException
    {
        IList<AdjustmentException> Lookup(int bookDate, int vehicleId);
    }

    public class LookupAdjustmentException : ILookupAdjustmentException
    {
        public IList<AdjustmentException> Lookup(int bookDate, int vehicleId)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<AdjustmentException> serializer = resolver.Resolve<ISqlSerializer<AdjustmentException>>();

            using (IDataReader reader = service.AdjustmentExceptions(bookDate, vehicleId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupAdjustmentException : CachedLookup, ILookupAdjustmentException
    {
        private readonly ILookupAdjustmentException _source = new LookupAdjustmentException();

        public IList<AdjustmentException> Lookup(int bookDate, int vehicleId)
        {
            string key = CreateCacheKey(bookDate, vehicleId);

            IList<AdjustmentException> values = Cache.Get(key) as IList<AdjustmentException>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
