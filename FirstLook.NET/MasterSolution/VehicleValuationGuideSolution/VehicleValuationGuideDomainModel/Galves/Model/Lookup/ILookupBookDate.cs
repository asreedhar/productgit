﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupBookDate
    {
        IList<BookDate> Lookup();
    }

    public class LookupBookDate : ILookupBookDate
    {
        public IList<BookDate> Lookup()
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<BookDate> serializer = registry.Resolve<ISqlSerializer<BookDate>>();

            using (IDataReader reader = service.BookDate())
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupBookDate : CachedLookup, ILookupBookDate
    {
        private readonly ILookupBookDate _source = new LookupBookDate();

        public IList<BookDate> Lookup()
        {
            string key = CreateCacheKey();

            IList<BookDate> values = Cache.Get(key) as IList<BookDate>;

            if (values == null)
            {
                values = _source.Lookup();

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
