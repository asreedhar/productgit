﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupAdjustment
    {
        IList<Adjustment> Lookup(int bookDate, int vehicleId);
    }

    public class LookupAdjustment : ILookupAdjustment
    {
        public IList<Adjustment> Lookup(int bookDate, int vehicleId)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<Adjustment> serializer = resolver.Resolve<ISqlSerializer<Adjustment>>();

            using (IDataReader reader = service.Adjustments(bookDate, vehicleId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupAdjustment : CachedLookup, ILookupAdjustment
    {
        private readonly ILookupAdjustment _source = new LookupAdjustment();

        public IList<Adjustment> Lookup(int bookDate, int vehicleId)
        {
            string key = CreateCacheKey(bookDate, vehicleId);

            IList<Adjustment> values = Cache.Get(key) as IList<Adjustment>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
