﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupRegionalMileageAdjustment
    {
        IList<RegionalMileageAdjustment> Lookup(int bookDate, string mileageYear, MileageClassification classification);
    }

    public class LookupRegionalMileageAdjustment : ILookupRegionalMileageAdjustment
    {
        public IList<RegionalMileageAdjustment> Lookup(int bookDate, string mileageYear, MileageClassification classification)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<RegionalMileageAdjustment> serializer = resolver.Resolve<ISqlSerializer<RegionalMileageAdjustment>>();

            using (IDataReader reader = service.RegionalMileageAdjustments(bookDate, mileageYear, classification))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupRegionalMileageAdjustment : CachedLookup, ILookupRegionalMileageAdjustment
    {
        private readonly ILookupRegionalMileageAdjustment _source = new LookupRegionalMileageAdjustment();

        public IList<RegionalMileageAdjustment> Lookup(int bookDate, string mileageYear, MileageClassification classification)
        {
            string key = CreateCacheKey(bookDate, mileageYear, classification);

            IList<RegionalMileageAdjustment> values = Cache.Get(key) as IList<RegionalMileageAdjustment>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, mileageYear, classification);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
