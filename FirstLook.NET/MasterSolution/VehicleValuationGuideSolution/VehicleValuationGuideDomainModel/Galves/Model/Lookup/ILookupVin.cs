﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupVin
    {
        IList<Vehicle> Lookup(int bookDate, string vin);
    }

    public class LookupVin : ILookupVin
    {
        public IList<Vehicle> Lookup(int bookDate, string vin)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Vehicle> serializer = registry.Resolve<ISqlSerializer<Vehicle>>();

            using (IDataReader reader = service.Query(bookDate, vin))
            {
                List<Vehicle> values = new List<Vehicle>();

                while (reader.Read())
                {
                    values.Add(serializer.Deserialize((IDataRecord)reader));
                }

                return values;
            }
        }
    }

    public class CachedLookupVin : CachedLookup, ILookupVin
    {
        private readonly ILookupVin _source = new LookupVin();

        public IList<Vehicle> Lookup(int bookDate, string vin)
        {
            string key = CreateCacheKey(bookDate, vin);

            IList<Vehicle> values = Cache.Get(key) as IList<Vehicle>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vin);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
