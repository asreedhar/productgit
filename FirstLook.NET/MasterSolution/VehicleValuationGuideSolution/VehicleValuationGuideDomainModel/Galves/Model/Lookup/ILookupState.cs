﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupState
    {
        IList<State> Lookup(int bookDate);
    }

    public class LookupState : ILookupState
    {
        public IList<State> Lookup(int bookDate)
        {
            ISqlService service = RegistryFactory.GetResolver().Resolve<ISqlService>();

            ISqlSerializer<State> ser = RegistryFactory.GetResolver().Resolve<ISqlSerializer<State>>();

            using (IDataReader reader = service.States(bookDate))
            {
                return ser.Deserialize(reader);
            }
        }
    }

    public class CachedLookupState : CachedLookup, ILookupState
    {
        private readonly ILookupState _source = new LookupState();

        public IList<State> Lookup(int bookDate)
        {
            string key = CreateCacheKey();

            IList<State> values = Cache.Get(key) as IList<State>;

            if (values == null)
            {
                values = _source.Lookup(bookDate);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
