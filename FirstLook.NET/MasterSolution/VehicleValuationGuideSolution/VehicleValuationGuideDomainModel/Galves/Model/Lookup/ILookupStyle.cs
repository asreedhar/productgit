﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Lookup
{
    public interface ILookupStyle
    {
        IList<Style> Lookup(int bookDate, int yearId, string manufacturer, string model);
    }

    public class LookupStyle : ILookupStyle
    {
        public IList<Style> Lookup(int bookDate, int yearId, string manufacturer, string model)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Style> serializer = registry.Resolve<ISqlSerializer<Style>>();

            using (IDataReader reader = service.Query(bookDate, yearId, manufacturer, model))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupStyle : CachedLookup, ILookupStyle
    {
        private readonly ILookupStyle _source = new LookupStyle();

        public IList<Style> Lookup(int bookDate, int yearId, string manufacturer, string model)
        {
            string key = CreateCacheKey(bookDate, yearId, manufacturer, model);

            IList<Style> values = Cache.Get(key) as IList<Style>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, yearId, manufacturer, model);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
