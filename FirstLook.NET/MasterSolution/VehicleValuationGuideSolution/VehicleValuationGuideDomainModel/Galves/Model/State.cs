﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class State
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        private Region _region;

        public Region Region
        {
            get { return _region; }
            internal set { _region = value; }
        }
    }
}
