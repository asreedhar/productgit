﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class Price
    {
        private PriceType _priceType;
        private int _amount;
        private Region _region;

        public PriceType PriceType
        {
            get { return _priceType; }
            internal set { _priceType = value; }
        }

        public int Amount
        {
            get { return _amount; }
            internal set { _amount = value; }
        }

        public Region Region
        {
            get { return _region; }
            set { _region = value; }
        }
    }
}
