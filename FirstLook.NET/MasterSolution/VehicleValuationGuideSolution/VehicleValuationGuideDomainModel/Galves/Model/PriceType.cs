﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public enum PriceType
    {
        Undefined,
        /// <summary>
        /// Galves Value
        /// </summary>
        TradeIn,
        /// <summary>
        /// Market Ready Value
        /// </summary>
        Retail
    }
}
