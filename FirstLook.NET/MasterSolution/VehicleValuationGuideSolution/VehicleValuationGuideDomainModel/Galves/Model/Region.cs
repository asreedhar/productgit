﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Galves.Model
{
    [Serializable]
    public class Region
    {
        internal static readonly Region[] Values = new[]
                                   {
                                       new Region {Id = 1},
                                       new Region {Id = 2},
                                       new Region {Id = 3},
                                       new Region {Id = 4},
                                       new Region {Id = 5},
                                       new Region {Id = 6}
                                   };
        private int _id;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }
    }
}
