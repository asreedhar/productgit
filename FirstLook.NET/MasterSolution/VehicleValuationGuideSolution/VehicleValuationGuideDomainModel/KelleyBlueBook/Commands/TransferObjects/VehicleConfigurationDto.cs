﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class VehicleConfigurationDto
    {
        public int VehicleId { get; set; }

        public BookDateDto BookDate { get; set; }

        public string Vin { get; set; }

        public int Mileage { get; set; }

        public bool HasMileage { get; set; }

        public string ZipCode { get; set; }

        public RegionDto Region { get; set; }

        public bool IsMissingEngine { get; set; }

        public bool IsMissingDriveTrain { get; set; }

        public bool IsMissingTransmission { get; set; }

        public List<OptionStateDto> OptionStates { get; set; }

        public List<OptionActionDto> OptionActions { get; set; }
    }
}
