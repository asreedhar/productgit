﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects 
{
    [Serializable]
    public class PriceTableDto
    {
        private bool _hasExcellent;
        private bool _hasVeryGood;
        private bool _hasGood;
        private bool _hasFair;

        public bool HasExcellent
        {
            get { return _hasExcellent; }
            set { _hasExcellent = value; }
        }

        public bool HasVeryGood
        {
            get { return _hasVeryGood; }
            set { _hasVeryGood = value; }
        }

        public bool HasGood
        {
            get { return _hasGood; }
            set { _hasGood = value; }
        }

        public bool HasFair
        {
            get { return _hasFair; }
            set { _hasFair = value; }
        }

        private List<PriceTableRowDto> _rows = new List<PriceTableRowDto>();

        public List<PriceTableRowDto> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
    }
}
