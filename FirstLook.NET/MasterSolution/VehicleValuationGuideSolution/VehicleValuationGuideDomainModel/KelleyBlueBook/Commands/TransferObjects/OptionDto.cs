﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class OptionDto
    {
        private int _id;
        private string _name;
        private int _sortOrder;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        private OptionTypeDto _optionType;
        private OptionCategoryDto _optionCategory;

        public OptionTypeDto OptionType
        {
            get { return _optionType; }
            set { _optionType = value; }
        }

        public OptionCategoryDto OptionCategory
        {
            get { return _optionCategory; }
            set { _optionCategory = value; }
        }
        
        private PriceTablesDto _tables;

        public PriceTablesDto Tables
        {
            get { return _tables; }
            set { _tables = value; }
        }
    }
}
