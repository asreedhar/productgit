﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class PriceTablesDto
    {
        private PriceTableDto _wholesaleLending;
        private PriceTableDto _suggestedRetail;
        private PriceTableDto _tradeIn;
        private PriceTableDto _privateParty;
        private PriceTableDto _auction;
        private PriceTableDto _tradeInHigh;
        private PriceTableDto _tradeInLow;

        public PriceTableDto WholesaleLending
        {
            get { return _wholesaleLending; }
            set { _wholesaleLending = value; }
        }

        public PriceTableDto SuggestedRetail
        {
            get { return _suggestedRetail; }
            set { _suggestedRetail = value; }
        }

        public PriceTableDto TradeIn
        {
            get { return _tradeIn; }
            set { _tradeIn = value; }
        }

        public PriceTableDto TradeInHigh
        {
            get { return _tradeInHigh; }
            set { _tradeInHigh = value; }
        }

        public PriceTableDto TradeInLow
        {
            get { return _tradeInLow; }
            set { _tradeInLow = value; }
        }


        public PriceTableDto PrivateParty
        {
            get { return _privateParty; }
            set { _privateParty = value; }
        }

        public PriceTableDto Auction
        {
            get { return _auction; }
            set { _auction = value; }
        }
    }
}
