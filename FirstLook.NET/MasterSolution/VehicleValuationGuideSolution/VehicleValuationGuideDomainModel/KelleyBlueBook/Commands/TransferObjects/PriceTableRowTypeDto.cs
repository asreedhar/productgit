﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public enum PriceTableRowTypeDto
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Final
    }
}
