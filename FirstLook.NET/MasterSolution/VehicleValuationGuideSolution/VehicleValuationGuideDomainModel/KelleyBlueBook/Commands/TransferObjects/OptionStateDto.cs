﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class OptionStateDto
    {
        private int _optionId;
        private bool _selected;

        public int OptionId
        {
            get { return _optionId; }
            set { _optionId = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
