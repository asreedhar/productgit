﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class SpecificationDto
    {
        private SpecificationTypeDto _specificationType;
        private int _id;
        private string _name;
        private string _unit;
        private string _value;

        public SpecificationTypeDto SpecificationType
        {
            get { return _specificationType; }
            set { _specificationType = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
