﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public enum SpecificationTypeDto
    {
        Undefined,
        Specifications,
        Warranty,
        Dimensions,
        General,
        Safety,
        Features,
        MpgRatings,
        Engine,
        CrashTest,
        Performance,
    }
}
