﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public enum OptionActionTypeDto
    {
        Undefined,
        Add,
        Remove
    }
}
