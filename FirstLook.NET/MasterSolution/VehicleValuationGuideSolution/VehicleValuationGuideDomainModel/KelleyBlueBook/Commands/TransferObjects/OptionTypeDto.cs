﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public enum OptionTypeDto
    {
        Undefined = 0,
        Equipment = 4,
        Option = 5,
        CertifiedPreOwned = 7
    }
}
