﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public enum OptionCategoryDto
    {
        Undefined,
        DriveTrain,
        Engine,
        Transmission
    }
}