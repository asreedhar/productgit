﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class RegionDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public RegionTypeDto Type { get; set; }

        public enum RegionTypeDto
        {
            Undefined = 0,
            NationalBase = 5,
            VimsRegionality = 7
        }
    }
}


