﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class OptionActionDto
    {
        private OptionActionTypeDto _actionType;
        private int _optionId;

        public OptionActionTypeDto ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }

        public int OptionId
        {
            get { return _optionId; }
            set { _optionId = value; }
        }
    }
}
