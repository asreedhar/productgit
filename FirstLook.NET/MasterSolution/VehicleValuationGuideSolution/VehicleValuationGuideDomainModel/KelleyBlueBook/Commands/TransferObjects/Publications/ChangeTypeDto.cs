﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// What changed between vehicle configurations?
    /// </summary>
    [Serializable]
    [Flags]
    public enum ChangeTypeDto
    {
        /// <summary>
        /// No change.
        /// </summary>
        None = 0,

        /// <summary>
        /// Publication date changed.
        /// </summary>
        BookDate = 1 << 0,

        /// <summary>
        /// US State changed.
        /// </summary>
        State = 1 << 1,

        /// <summary>
        /// Vehicle identifier.
        /// </summary>
        VehicleId = 1 << 2,

        /// <summary>
        /// Mileage changed.
        /// </summary>
        Mileage = 1 << 3,

        /// <summary>
        /// Option actions changed.
        /// </summary>
        OptionActions = 1 << 4,

        /// <summary>
        /// Option states changed.
        /// </summary>
        OptionStates = 1 << 5
    }
}
