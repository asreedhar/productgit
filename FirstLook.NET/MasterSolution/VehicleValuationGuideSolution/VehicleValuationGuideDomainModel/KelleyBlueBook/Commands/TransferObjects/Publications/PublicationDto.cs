﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// Publication of vehicle reference data, a configuration on it, and the valuation of the configuration.
    /// </summary>
    [Serializable]
    public class PublicationDto : PublicationInfoDto
    {
        /// <summary>
        /// Vehicle configuration.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }

        /// <summary>
        /// Options.
        /// </summary>
        public List<OptionDto> Options { get; set; }        

        /// <summary>
        /// Specifications.
        /// </summary>
        public List<SpecificationDto> Specifications { get; set; }

        /// <summary>
        /// Valuation.
        /// </summary>
        public PriceTablesDto Tables { get; set; }
    }
}
