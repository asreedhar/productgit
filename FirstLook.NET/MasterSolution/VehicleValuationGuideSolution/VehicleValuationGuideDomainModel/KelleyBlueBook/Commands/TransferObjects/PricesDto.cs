﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects
{
    [Serializable]
    public class PricesDto
    {
        private decimal _excellent;
        private decimal _veryGood;
        private decimal _good;
        private decimal _fair;

        public decimal Excellent
        {
            get { return _excellent; }
            set { _excellent = value; }
        }

        public decimal VeryGood
        {
            get { return _veryGood; }
            set { _veryGood = value; }
        }

        public decimal Good
        {
            get { return _good; }
            set { _good = value; }
        }

        public decimal Fair
        {
            get { return _fair; }
            set { _fair = value; }
        }

        private bool _hasExcellent;
        private bool _hasVeryGood;
        private bool _hasGood;
        private bool _hasFair;

        public bool HasExcellent
        {
            get { return _hasExcellent; }
            set { _hasExcellent = value; }
        }

        public bool HasVeryGood
        {
            get { return _hasVeryGood; }
            set { _hasVeryGood = value; }
        }

        public bool HasGood
        {
            get { return _hasGood; }
            set { _hasGood = value; }
        }

        public bool HasFair
        {
            get { return _hasFair; }
            set { _hasFair = value; }
        }
    }
}
