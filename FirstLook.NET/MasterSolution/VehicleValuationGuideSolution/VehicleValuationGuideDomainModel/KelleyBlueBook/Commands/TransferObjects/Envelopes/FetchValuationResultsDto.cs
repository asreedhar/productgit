﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchValuationResultsDto
    {
        private decimal _adjustment;
        private string _category;
        private string _condition;
        private string _valueType;
        private decimal? _optionValue;

        public decimal Adjustment
        {
            get { return _adjustment; }
            set { _adjustment = value; }
        }

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public string Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        public string ValueType
        {
            get { return _valueType; }
            set { _valueType = value; }
        }

        public decimal? OptionValue
        {
            get { return _optionValue; }
            set { _optionValue = value; }
        }

        public decimal? FinalValue { get; set; }

        public PriceType CategoryId { get; set; }
    
    }
}
