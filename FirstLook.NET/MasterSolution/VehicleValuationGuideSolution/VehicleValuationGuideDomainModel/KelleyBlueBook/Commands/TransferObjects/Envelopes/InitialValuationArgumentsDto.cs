﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationArgumentsDto : ValuationArgumentsDto
    {
        private string _vin;        // for default equipment
        private int _vehicleId;     // all important key for everything

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public IList<string> EquipmentSelected { get; set; }
    }
}
