﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class RegionsResultsDto
    {
        public RegionsArgumentsDto Arguments { get; set; }

        public List<RegionDto> Regions { get; set; }
    }
}
