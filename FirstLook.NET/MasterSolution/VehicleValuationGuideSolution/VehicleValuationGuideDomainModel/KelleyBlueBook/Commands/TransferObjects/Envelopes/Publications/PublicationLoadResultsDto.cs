using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Results of loading a specific publication.
    /// </summary>
    [Serializable]
    public class PublicationLoadResultsDto
    {
        /// <summary>
        /// Publication identifiers.
        /// </summary>
        public PublicationLoadArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Publication.
        /// </summary>
        public PublicationDto Publication { get; set;}
    }
}
