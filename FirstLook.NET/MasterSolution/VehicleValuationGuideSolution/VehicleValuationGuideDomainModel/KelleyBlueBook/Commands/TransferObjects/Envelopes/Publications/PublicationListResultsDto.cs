using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Results of querying for a list of publications.
    /// </summary>
    [Serializable]
    public class PublicationListResultsDto
    {
        /// <summary>
        /// Dealer and vehicle identifiers.
        /// </summary>
        public PublicationListArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Associated publications.
        /// </summary>
        public List<PublicationInfoDto> Publications { get; set; }
    }
}
