using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments to save a vehicle configuration for a broker and vehicle.
    /// </summary>
    [Serializable]
    public class PublicationSaveArgumentsDto : PublicationArgumentsDto
    {
        /// <summary>
        /// Vehicle configuration to save.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }

        public IList<string> EquipmentSelected { get; set; }

        public int DealerId { get; set; }
    }
}
