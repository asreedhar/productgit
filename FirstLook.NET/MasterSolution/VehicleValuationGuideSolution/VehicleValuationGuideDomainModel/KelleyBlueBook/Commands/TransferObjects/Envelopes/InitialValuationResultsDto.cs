﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationResultsDto : ValuationResultsDto
    {
        public InitialValuationArgumentsDto Arguments { get; set; }

        public List<OptionDto> Options { get; set; }

        public List<SpecificationDto> Specifications { get; set; }

        public VehicleConfigurationDto VehicleConfiguration { get; set; }
    }
}
