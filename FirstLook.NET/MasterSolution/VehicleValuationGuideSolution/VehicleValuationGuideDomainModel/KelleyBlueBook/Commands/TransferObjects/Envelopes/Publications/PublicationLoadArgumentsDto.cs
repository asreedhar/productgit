using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments to get a particular publication.
    /// </summary>
    [Serializable]
    public class PublicationLoadArgumentsDto : PublicationArgumentsDto
    {
        /// <summary>
        /// Publication identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
