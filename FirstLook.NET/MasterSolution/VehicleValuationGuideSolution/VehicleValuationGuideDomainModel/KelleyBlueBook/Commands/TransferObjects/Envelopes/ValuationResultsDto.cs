using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ValuationResultsDto
    {
        private PriceTablesDto _tables;

        public PriceTablesDto Tables
        {
            get { return _tables; }
            set { _tables = value; }
        }
    }
}