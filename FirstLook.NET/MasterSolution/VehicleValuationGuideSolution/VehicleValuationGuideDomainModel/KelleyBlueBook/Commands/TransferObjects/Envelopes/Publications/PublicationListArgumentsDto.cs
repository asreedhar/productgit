﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments to retrieve publications tied to a dealer and vehicle.
    /// </summary>
    [Serializable]
    public class PublicationListArgumentsDto : PublicationArgumentsDto
    {
    }
}
