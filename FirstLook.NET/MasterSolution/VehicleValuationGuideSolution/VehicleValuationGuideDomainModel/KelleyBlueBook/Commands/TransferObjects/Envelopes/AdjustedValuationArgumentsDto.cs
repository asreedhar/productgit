﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AdjustedValuationArgumentsDto : ValuationArgumentsDto
    {
        private VehicleConfigurationDto _vehicleConfiguration;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }

        private OptionActionDto _optionAction;

        public OptionActionDto OptionAction
        {
            get { return _optionAction; }
            set { _optionAction = value; }
        }

        public IList<string> EquipmentSelected { get; set; }

    }
}
