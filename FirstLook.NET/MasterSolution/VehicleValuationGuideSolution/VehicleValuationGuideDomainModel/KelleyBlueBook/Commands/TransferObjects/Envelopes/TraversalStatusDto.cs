﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public enum TraversalStatusDto
    {
        Success,
        InvalidVinLength,
        InvalidVinCharacters,
        InvalidVinChecksum,
        NoDataForVin
    }
}
