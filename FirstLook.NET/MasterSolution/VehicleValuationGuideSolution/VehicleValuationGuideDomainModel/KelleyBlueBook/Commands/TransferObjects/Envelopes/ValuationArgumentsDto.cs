using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes 
{
    [Serializable]
    public class ValuationArgumentsDto
    {
        private string _zipcode;
        private int _mileage;
        private bool _hasMileage;
        private RegionDto _region;
        private int _dealerId;

        public RegionDto Region
        {
            get { return _region; }
            set { _region = value; }
        }

        public string ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }
        public int DealerId
        {
            get { return _dealerId; }
            set { _dealerId = value; }
        }
    }
}