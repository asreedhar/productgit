﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class RegionResultsDto
    {
        public RegionArgumentsDto Arguments { get; set; }

        public RegionDto Region { get; set; }
    }
}
