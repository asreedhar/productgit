﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SuccessorArgumentsDto
    {
        private string _node;
        private string _successor;

        public string Node
        {
            get { return _node; }
            set { _node = value; }
        }

        public string Successor
        {
            get { return _successor; }
            set { _successor = value; }
        }
    }
}
