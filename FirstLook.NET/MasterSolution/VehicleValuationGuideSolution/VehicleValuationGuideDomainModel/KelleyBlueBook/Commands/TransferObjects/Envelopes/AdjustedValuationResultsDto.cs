﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AdjustedValuationResultsDto : ValuationResultsDto
    {
        private AdjustedValuationArgumentsDto _arguments;

        public AdjustedValuationArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private VehicleConfigurationDto _vehicleConfiguration;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }

        public IList<string> EquipmentSelected { get; set; }
    }
}
