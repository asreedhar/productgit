﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class StatesResultsDto
    {
        private StatesArgumentsDto _arguments;
        private List<StateDto> _states;

        public StatesArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<StateDto> States
        {
            get { return _states; }
            set { _states = value; }
        }
    }
}
