using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Results of getting a publication that is valid for a broker and vehicle on a particular date.
    /// </summary>
    [Serializable]
    public class PublicationOnDateResultsDto
    {
        /// <summary>
        /// Broker, vehicle and date on which the publication should be valid.
        /// </summary>
        public PublicationOnDateArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Publication.
        /// </summary>
        public PublicationDto Publication { get; set; }
    }
}
