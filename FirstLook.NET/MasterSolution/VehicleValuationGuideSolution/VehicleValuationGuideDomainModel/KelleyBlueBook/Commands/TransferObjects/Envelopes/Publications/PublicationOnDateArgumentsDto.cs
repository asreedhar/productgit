using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments for retrieving a publication valid on a particular date.
    /// </summary>
    [Serializable]
    public class PublicationOnDateArgumentsDto : PublicationArgumentsDto
    {
        /// <summary>
        /// Date on which the publication should be valid.
        /// </summary>
        public DateTime On { get; set; }
    }
}
