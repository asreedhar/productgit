using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments used to retrieve publications for a dealer and vehicle.
    /// </summary>
    [Serializable]
    public class PublicationArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle publications we desire to enumerate.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Vehicle with which the publications must be associated.
        /// </summary>
        public Guid Vehicle { get; set; }
    }
}