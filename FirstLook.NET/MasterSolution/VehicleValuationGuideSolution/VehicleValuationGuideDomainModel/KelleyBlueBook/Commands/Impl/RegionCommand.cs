﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;


namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl
{
    public class RegionCommand : ICommand<RegionResultsDto, RegionArgumentsDto>
    {
        public RegionResultsDto Execute(RegionArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            Region region = resolver.Resolve<IKbbService>().ToRegion(parameters.ZipCode);

            return new RegionResultsDto
            {
                Arguments = parameters,
                Region = Mapper.Map(region)
            };
        }
    }
}
