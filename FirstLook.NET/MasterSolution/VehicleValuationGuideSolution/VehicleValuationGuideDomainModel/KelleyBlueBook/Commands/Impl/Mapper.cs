﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api; 

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl
{
    public static class Mapper
    {
        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes)
        {
            return Map(nodes, Operation.No, Operation.No);
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes, Operation parents, Operation children)
        {
            List<NodeDto> list = new List<NodeDto>();

            foreach (ITreeNode node in nodes)
            {
                list.Add(Map(node, parents, children));
            }

            return list;
        }

        public class Operation
        {
            private readonly Operation _next;
            private readonly bool _perform;

            protected Operation(bool perform, Operation next)
            {
                _next = next ?? this;
                _perform = perform;
            }

            public Operation Next
            {
                get { return _next; }
            }

            public bool Perform
            {
                get { return _perform; }
            }

            public static readonly Operation Yes;
            public static readonly Operation No;
            public static readonly Operation NoThenYes;
            public static readonly Operation YesThenNo;
            public static readonly Operation YesThenYesThenNo;

            static Operation()
            {
                Yes = new Operation(true, null);
                No = new Operation(false, null);
                NoThenYes = new Operation(false, Yes);
                YesThenNo = new Operation(true, No);
                YesThenYesThenNo = new Operation(true, YesThenNo);
            }
        }

        public static NodeDto Map(ITreeNode node, Operation parents, Operation children)
        {
            ITreePath treePath = new TreePath();

            node.Save(treePath);

            NodeDto dto = new NodeDto
            {
                Id = node.Id,
                Name = node.Name,
                SortOrder = node.SortOrder,
                State = treePath.State,
                VehicleId = node.VehicleId.GetValueOrDefault(),
                HasVehicleId = node.VehicleId.HasValue,
                Label = node.Label
            };

            if (children.Perform)
            {
                dto.Children = Map(node.Children);
            }

            if (parents.Perform && node.Parent != null)
            {
                dto.Parent = Map(node.Parent, parents.Next, children.Next);
            }

            return dto;
        }

        public static PriceTablesDto Map(Matrix matrix)
        {
            PriceTablesDto tables = NewPriceTables();

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined ||
                    priceType == PriceType.Msrp ||
                    priceType == PriceType.ValuationBase ||
                    priceType == PriceType.Mileage ||
                    priceType == PriceType.Adjustment ||
                    priceType == PriceType.ModelYear)
                {
                    continue;
                }

                PriceTableDto table = GetTable(tables, priceType);

                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    if (valuation == Valuation.Undefined)
                    {
                        continue;
                    }

                    decimal? value = matrix[priceType, valuation].Value;

                    SetPrice(table, priceType, valuation, value);
                }
            }

            return tables;
        }

        private static void SetPrice(PriceTableDto table, PriceType priceType, Valuation valuation, decimal? value)
        {
            PriceTableRowTypeDto rowType;

            switch (valuation)
            {
                case Valuation.Base:
                    rowType = PriceTableRowTypeDto.Base;
                    break;
                case Valuation.Final:
                    rowType = PriceTableRowTypeDto.Final;
                    break;
                case Valuation.Mileage:
                    rowType = PriceTableRowTypeDto.Mileage;
                    break;
                case Valuation.Option:
                    rowType = PriceTableRowTypeDto.Option;
                    break;
                default:
                    return;
            }

            PriceTableRowDto row = table.Rows.FirstOrDefault(
                x => x.RowType == rowType);

            if (row == null)
            {
                row = new PriceTableRowDto
                {
                    Prices = new PricesDto(),
                    RowType = rowType
                };

                table.Rows.Add(row);
            }

            switch (priceType)
            {
                case PriceType.Wholesale:
                    row.Prices.Good = value.GetValueOrDefault();
                    row.Prices.HasGood = value.HasValue;
                    break;
                case PriceType.Retail:
                    row.Prices.Excellent = value.GetValueOrDefault();
                    row.Prices.HasExcellent = value.HasValue;
                    break;
                case PriceType.AuctionFair:
                case PriceType.TradeInFair:
                case PriceType.PrivatePartyFair:
                case PriceType.TradeInRangeLowFair:
                case PriceType.TradeInRangeHighFair:
                    row.Prices.Fair = value.GetValueOrDefault();
                    row.Prices.HasFair = value.HasValue;
                    break;
                case PriceType.AuctionGood:
                case PriceType.TradeInGood:
                case PriceType.PrivatePartyGood:
                case PriceType.TradeInRangeLowGood:
                case PriceType.TradeInRangeHighGood:
                    row.Prices.Good = value.GetValueOrDefault();
                    row.Prices.HasGood = value.HasValue;
                    break;
                case PriceType.AuctionVeryGood:
                case PriceType.TradeInVeryGood:
                case PriceType.PrivatePartyVeryGood:
                case PriceType.TradeInVeryGoodRangeLow:
                case PriceType.TradeInVeryGoodRangeHigh:
                    row.Prices.VeryGood = value.GetValueOrDefault();
                    row.Prices.HasVeryGood = value.HasValue;
                    break;
                case PriceType.TradeInExcellent:
                case PriceType.PrivatePartyExcellent:
                case PriceType.AuctionExcellent:
                case PriceType.TradeInRangeLowExcellent:
                case PriceType.TradeInRangeHighExcellent:
                    row.Prices.Excellent = value.GetValueOrDefault();
                    row.Prices.HasExcellent = value.HasValue;
                    break;
            }
        }

        private static PriceTableDto GetTable(PriceTablesDto tables, PriceType priceType)
        {
            PriceTableDto table;

            switch (priceType)
            {
                case PriceType.Wholesale:
                    table = tables.WholesaleLending;
                    break;
                case PriceType.Retail:
                    table = tables.SuggestedRetail;
                    break;
                case PriceType.TradeInFair:
                case PriceType.TradeInGood:
                case PriceType.TradeInVeryGood:
                case PriceType.TradeInExcellent:
                    table = tables.TradeIn;
                    break;
                case PriceType.PrivatePartyFair:
                case PriceType.PrivatePartyGood:
                case PriceType.PrivatePartyVeryGood:
                case PriceType.PrivatePartyExcellent:
                    table = tables.PrivateParty;
                    break;
                case PriceType.AuctionFair:
                case PriceType.AuctionGood:
                case PriceType.AuctionVeryGood:
                case PriceType.AuctionExcellent:
                    table = tables.Auction;
                    break;
                case PriceType.TradeInRangeHighExcellent:
                case PriceType.TradeInRangeHighFair:
                case PriceType.TradeInRangeHighGood:
                case PriceType.TradeInVeryGoodRangeHigh:
                    table = tables.TradeInHigh;
                    break;
                case PriceType.TradeInRangeLowExcellent:
                case PriceType.TradeInRangeLowFair:
                case PriceType.TradeInRangeLowGood:
                case PriceType.TradeInVeryGoodRangeLow:
                    table = tables.TradeInLow;
                    break;
                default:
                    table = new PriceTableDto(); // !! throw away the data !!
                    break;
            }
            return table;
        }

        private static PriceTablesDto NewPriceTables()
        {
            return new PriceTablesDto
            {
                Auction = NewPriceTable(true, true, true, true),
                PrivateParty = NewPriceTable(true, true, true, true),
                SuggestedRetail = NewPriceTable(false, false, false, false),
                TradeIn = NewPriceTable(true, true, true, true),
                WholesaleLending = NewPriceTable(false, false, false, false),
                TradeInHigh = NewPriceTable(true, true, true, true),
                TradeInLow = NewPriceTable(true, true, true, true)
            };
        }

        static PriceTableDto NewPriceTable(bool hasExcellent, bool hasVeryGood, bool hasGood, bool hasFair)
        {
            return new PriceTableDto
            {
                HasExcellent = hasExcellent,
                HasVeryGood = hasVeryGood,
                HasGood = hasGood,
                HasFair = hasFair,
                Rows = new List<PriceTableRowDto>()
            };
        }

        public static List<OptionDto> Map(IList<VehicleOption> options, IList<VehicleOptionAdjustment> adjustments)
        {
            List<OptionDto> values = new List<OptionDto>();

            foreach (VehicleOption option in options)
            {
                int optionId = option.Id; // no warnings plz

                IEnumerable<VehicleOptionAdjustment> items = adjustments.Where(x => x.OptionId == optionId);

                PriceTablesDto tables = NewPriceTables();

                foreach (VehicleOptionAdjustment item in items)
                {
                    SetPrice(
                        GetTable(tables, item.Adjustment.PriceType),
                        item.Adjustment.PriceType,
                        Valuation.Option,
                        item.Adjustment.Value);
                }

                values.Add(
                    new OptionDto
                    {
                        Id = option.Id,
                        Name = option.Name,
                        OptionCategory = (OptionCategoryDto)option.Category,
                        OptionType = (OptionTypeDto)option.OptionType,
                        SortOrder = option.SortOrder
                    });
            }

            return values;
        }

        public static List<RegionDto> Map(IEnumerable<Region> regions)
        {
            List<RegionDto> values = new List<RegionDto>();

            foreach (Region region in regions)
            {
                values.Add(Map(region));
            }

            return values;
        }

        public static RegionDto Map(Region region)
        {
            return new RegionDto { Id = region.Id, Name = region.Name, Type = (RegionDto.RegionTypeDto)region.Type };
        }

        public static Region Map(RegionDto region)
        {
            if (region == null) return null;

            return new Region { Id = region.Id, Name = region.Name, Type = (Region.RegionType)region.Type };
        }

        public static List<SpecificationDto> Map(IList<Specification> specifications)
        {
            List<SpecificationDto> values = new List<SpecificationDto>();

            foreach (Specification specification in specifications)
            {
                values.Add(
                    new SpecificationDto
                        {
                            SpecificationType = (SpecificationTypeDto) specification.SpecificationType,
                            Id = specification.Id,
                            Name = specification.Name,
                            Unit = specification.Unit,
                            Value = specification.Value
                        });
            }

            return values;
        }

        public static VehicleConfigurationDto Map(IVehicleConfiguration configuration)
        {
            return new VehicleConfigurationDto
            {
                BookDate              = new BookDateDto { Id = configuration.BookDate.Id },
                VehicleId             = configuration.VehicleId,
                Vin                   = configuration.Vin,
                Mileage               = configuration.Mileage.GetValueOrDefault(),
                HasMileage            = configuration.Mileage.HasValue,
                Region                = Map(configuration.Region),
                OptionActions         = Map(configuration.OptionActions),
                OptionStates          = Map(configuration.OptionStates),
                IsMissingDriveTrain   = configuration.IsMissingDriveTrain,
                IsMissingEngine       = configuration.IsMissingEngine,
                IsMissingTransmission = configuration.IsMissingTransmission
            };
        }

        private static List<OptionActionDto> Map(IEnumerable<IOptionAction> actions)
        {
            List<OptionActionDto> values = new List<OptionActionDto>();

            foreach (IOptionAction action in actions)
            {
                values.Add(
                    new OptionActionDto
                        {
                            ActionType = (OptionActionTypeDto) action.ActionType,
                            OptionId = action.OptionId
                        });
            }

            return values;
        }

        private static List<OptionStateDto> Map(IEnumerable<IOptionState> states)
        {
            List<OptionStateDto> values = new List<OptionStateDto>();

            foreach (IOptionState state in states)
            {
                values.Add(
                    new OptionStateDto
                    {
                        Selected = state.Selected,
                        OptionId = state.OptionId
                    });
            }

            return values;
        }
        
        public static IVehicleConfiguration Map(VehicleConfigurationDto configuration, Region region, string zipCode, int? mileage)
        {
            if (!mileage.HasValue)
            {
                mileage = configuration.HasMileage ? configuration.Mileage : default(int?);
            }

            IResolver resolver = RegistryFactory.GetResolver();

            IKbbService service = resolver.Resolve<IKbbService>();

            if ((region == null || region.Id < 1) && !string.IsNullOrEmpty(zipCode))
            {
                region = service.ToRegion(zipCode);
            }

            if (region == null || region.Id < 1) throw new ArgumentException("Region/ZipCode parameter missing", "region");

            IVehicleConfigurationBuilderFactory factory = resolver.Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            builder.BookDate  = new BookDate { Id = configuration.BookDate.Id };
            builder.VehicleId = configuration.VehicleId;
            builder.Vin       = configuration.Vin;
            builder.Region    = region;
            builder.Mileage   = mileage;

            builder.IsMissingDriveTrain   = configuration.IsMissingDriveTrain;
            builder.IsMissingEngine       = configuration.IsMissingEngine;
            builder.IsMissingTransmission = configuration.IsMissingTransmission;
            
            foreach (OptionActionDto value in configuration.OptionActions)
            {
                builder.Do(value.OptionId, (OptionActionType)value.ActionType);
            }

            foreach (OptionStateDto value in configuration.OptionStates)
            {
                builder.Add(value.OptionId, value.Selected);
            }

            return builder.ToVehicleConfiguration();
        }
    }
}
