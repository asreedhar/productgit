﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl.Publications
{
    public class PriceTypeMapper
    {
        public static PriceType GetPriceType(string category, string condition)
        {
            string _category = category == null ? null : category.ToLower();
            string _condition = condition == null ? null : condition.ToLower();
          
            if (_category == "wholesale")
                return PriceType.Wholesale;
            if (_category == "retail")
                return PriceType.Retail;

            if (_category == "trade-in" && _condition == "good")
                return PriceType.TradeInGood;
            if (_category == "trade-in" && _condition == "fair")
                return PriceType.TradeInFair;
            if (_category == "trade-in very good")
                return PriceType.TradeInVeryGood;
            if (_category == "trade-in" && _condition == "excellent")
                return PriceType.TradeInExcellent;

            if (_category == "private party" && _condition == "good")
                return PriceType.PrivatePartyGood;
            if (_category == "private party" && _condition == "fair")
                return PriceType.PrivatePartyFair;
            if (_category == "private party very good")
                return PriceType.PrivatePartyVeryGood;
            if (_category == "private party" && _condition == "excellent")
                return PriceType.PrivatePartyExcellent;

            if (_category == "auction good")
                return PriceType.AuctionGood;
            if (_category == "auction fair")
                return PriceType.AuctionFair;
            if (_category == "auction very good")
                return PriceType.AuctionVeryGood;
            if (_category == "auction excellent")
                return PriceType.AuctionExcellent;

            if (_category == "valuationbase")
                return PriceType.ValuationBase;

            if (_category == "msrp")
                return PriceType.Msrp;

            if (_category == "trade-in + rangehigh" && _condition == "good")
                return PriceType.TradeInRangeHighGood;
            if (_category == "trade-in + rangehigh" && _condition == "fair")
                return PriceType.TradeInRangeHighFair;
            if (_category == "trade-in very good + rangehigh")
                return PriceType.TradeInVeryGoodRangeHigh;
            if (_category == "trade-in + rangehigh" && _condition == "excellent")
                return PriceType.TradeInRangeHighExcellent;

            if (_category == "trade-in + rangelow" && _condition == "good")
                return PriceType.TradeInRangeLowGood;
            if (_category == "trade-in + rangelow" && _condition == "fair")
                return PriceType.TradeInRangeLowFair;
            if (_category == "trade-in very good + rangelow")
                return PriceType.TradeInVeryGoodRangeLow;
            if (_category == "trade-in + rangelow" && _condition == "excellent")
                return PriceType.TradeInRangeLowExcellent;

            if (_category == "mileage")
                return PriceType.Mileage;
            if (_category == "adjustment")
                return PriceType.Adjustment;
            if (_category == "modelyear")
                return PriceType.ModelYear;

            return PriceType.Undefined;

        }
    }
}
