﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl.Publications
{
    /// <summary>
    /// Command to get a publication that is valid on a given date for a client and vehicle.
    /// </summary>
    public class PublicationOnDateCommand : ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>>
    {
        /// <summary>
        /// Get the publication valid on a particular date for a client and vehicle.
        /// </summary>
        /// <param name="parameters">Client and vehicle details, and the date on which the publication should be valid.</param>
        /// <returns>Publication.</returns>
        public PublicationOnDateResultsDto Execute(IdentityContextDto<PublicationOnDateArgumentsDto> parameters)
        {            
            IResolver          resolver          = RegistryFactory.GetResolver();
            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();
            IKbbRepository     kbbRepository     = resolver.Resolve<IKbbRepository>();

            // Get the client and vehicle details.
            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, parameters.Arguments.Broker);
            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, parameters.Arguments.Vehicle);

            // Get the publication.
            IEdition<IPublication> publication = kbbRepository.Load(broker, vehicle, parameters.Arguments.On);

            return new PublicationOnDateResultsDto
            {
                Arguments = parameters.Arguments,
                Publication = PublicationMapper.Map(publication)
            };            
        }
    }
}
