﻿using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl.Publications
{
    /// <summary>
    /// Command to save a publication.
    /// </summary>
    public class PublicationSaveCommand :  ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>>
    {
        /// <summary>
        /// Save a publication.
        /// </summary>
        /// <param name="parameters">Publication details to save.</param>
        /// <returns>Publication that was saved.</returns>
        public PublicationSaveResultsDto Execute(IdentityContextDto<PublicationSaveArgumentsDto> parameters)
        {
            // Resolve dependencies.
            IResolver          resolver          = RegistryFactory.GetResolver();
            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();
            IKbbRepository     kbbRepository     = resolver.Resolve<IKbbRepository>();
            IKbbCalculator     calculator        = resolver.Resolve<IKbbCalculator>();
            
            // Local variables.
            PublicationSaveArgumentsDto arguments = parameters.Arguments;
            IdentityDto identity = parameters.Identity;

            // mileage.
            int? mileage = arguments.VehicleConfiguration.HasMileage ? arguments.VehicleConfiguration.Mileage : (int?)null;

            // Build configuration and its matrix.
            IVehicleConfiguration configuration = Mapper.Map(
                arguments.VehicleConfiguration, 
                Mapper.Map(arguments.VehicleConfiguration.Region),
                arguments.VehicleConfiguration.ZipCode, 
                mileage);
            Matrix matrix = calculator.Calculate(configuration,parameters.Arguments.EquipmentSelected);

            // Get client, vehicle and user details.            
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);
            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, arguments.Vehicle);

            // Save configuration.
            IEdition<IPublication> publication = kbbRepository.Save(broker, principal, vehicle, configuration, matrix);

            return new PublicationSaveResultsDto
            {
                Arguments = arguments,
                Publication = PublicationMapper.Map(publication)
            };            
        }
    }
}
