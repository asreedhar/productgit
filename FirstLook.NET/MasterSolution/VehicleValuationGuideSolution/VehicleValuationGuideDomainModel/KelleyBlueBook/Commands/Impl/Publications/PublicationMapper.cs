﻿using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl.Publications
{
    /// <summary>
    /// Helper class to map publication entities into transfer objects.
    /// </summary>
    public static class PublicationMapper
    {        
        /// <summary>
        /// Map an edition of a publication into a transfer object.
        /// </summary>
        /// <param name="edition">Edition of a publication.</param>
        /// <returns>Publication transfer object.</returns>
        public static PublicationDto Map(IEdition<IPublication> edition)
        {            
            if (edition == null)
            {
                return null;
            }

            IPublication          publication   = edition.Data;
            IKbbService           service       = publication.Service;
            IVehicleConfiguration configuration = publication.VehicleConfiguration;
            
            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            IList<VehicleOptionAdjustment> optionAdjustments = service.OptionAdjustments(vehicle.Id);

            return new PublicationDto
            {
                ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                ChangeType  = (ChangeTypeDto)publication.ChangeType,
                //Id          = publication.Id,
                //Edition     = new EditionDto
                //{
                //    BeginDate = edition.DateRange.BeginDate,
                //    EndDate   = edition.DateRange.EndDate,
                //    User      = new UserDto
                //    {
                //        FirstName = edition.User.FirstName,
                //        LastName  = edition.User.LastName,
                //        UserName  = edition.User.UserName
                //    }
                //},                
                Options              = Mapper.Map(vehicle.Options, optionAdjustments),
                Specifications       = Mapper.Map(vehicle.Specifications),
                Tables               = Mapper.Map(publication.Matrix),
                VehicleConfiguration = Mapper.Map(configuration)
            };             
        }

        /// <summary>
        /// Map a list of the identifying information of publication editions into a list of transfer objects.
        /// </summary>
        /// <param name="publications">List of the identifying information of publication editions.</param>
        /// <returns>List of transfer objects.</returns>
        public static List<PublicationInfoDto> Map(IList<IEdition<IPublicationInfo>> publications)
        {
            List<PublicationInfoDto> values = new List<PublicationInfoDto>();

            foreach (IEdition<IPublicationInfo> edition in publications)
            {
                IPublicationInfo publication = edition.Data;

                values.Add(new PublicationInfoDto
                {
                    ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                    ChangeType  = (ChangeTypeDto)publication.ChangeType,
                    Id          = publication.Id,
                    Edition     = new EditionDto
                    {
                        BeginDate = edition.DateRange.BeginDate,
                        EndDate   = edition.DateRange.EndDate,
                        User      = new UserDto
                        {
                            FirstName = edition.User.FirstName,
                            LastName  = edition.User.LastName,
                            UserName  = edition.User.UserName
                        }
                    }
                });
            }

            return values;
        }
    }
}
