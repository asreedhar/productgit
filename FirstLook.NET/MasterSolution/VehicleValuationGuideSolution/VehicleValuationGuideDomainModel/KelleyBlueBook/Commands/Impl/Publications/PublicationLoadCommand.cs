﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl.Publications
{
    /// <summary>
    /// Command to get a particular publication for an identifier.
    /// </summary>
    public class PublicationLoadCommand : ICommand<PublicationLoadResultsDto, IdentityContextDto<PublicationLoadArgumentsDto>>
    {
        /// <summary>
        /// Get a particular publication.
        /// </summary>
        /// <param name="parameters">Publication identifier.</param>
        /// <returns>Publication.</returns>
        public PublicationLoadResultsDto Execute(IdentityContextDto<PublicationLoadArgumentsDto> parameters)
        {            
            IResolver          resolver          = RegistryFactory.GetResolver();
            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();
            IKbbRepository     nadaRepository    = resolver.Resolve<IKbbRepository>();

            // Get the details of the client and vehicle.
            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, parameters.Arguments.Broker);
            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, parameters.Arguments.Vehicle);

            // Get the publication.
            IEdition<IPublication> publication = nadaRepository.Load(broker, vehicle, parameters.Arguments.Id);

            return new PublicationLoadResultsDto
            {
                Arguments = parameters.Arguments,
                Publication = PublicationMapper.Map(publication)
            };           
        }
    }
}
