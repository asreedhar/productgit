﻿using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup; 

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl
{
    [Serializable]
    public class AdjustedValuationCommand : ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
    {
        public AdjustedValuationResultsDto Execute(AdjustedValuationArgumentsDto parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (parameters.VehicleConfiguration == null)
            {
                throw new ArgumentNullException("parameters", "VehicleConfiguration");
            }

            IResolver registry = RegistryFactory.GetResolver();

            // allow empty option action for changes in mileage
            IVehicleConfiguration configuration = Mapper.Map(
                parameters.VehicleConfiguration,
                Mapper.Map(parameters.Region),
                parameters.ZipCode,
                parameters.HasMileage ? parameters.Mileage : default(int?));

            OptionActionDto action = parameters.OptionAction;

            //if (action != null)
            //{
            //    configuration = registry.Resolve<IKbbConfiguration>().UpdateConfiguration(
            //        registry.Resolve<IKbbService>(),
            //        configuration,
            //        new Action {ActionType = (OptionActionType) action.ActionType, OptionId = action.OptionId});
            //}

            if (action != null)
            {
                    registry.Resolve<IKbbConfiguration>().UpdateConfiguration(
                    registry.Resolve<IKbbService>(),
                    configuration,
                    parameters.EquipmentSelected,
                    new Action { ActionType = (OptionActionType)action.ActionType, OptionId = action.OptionId });
            }

            Matrix matrix = registry.Resolve<IKbbCalculator>().CalculateData(configuration,parameters.EquipmentSelected,parameters.DealerId);            

            AdjustedValuationResultsDto results = new AdjustedValuationResultsDto
            {
                Arguments = parameters,
                Tables = Mapper.Map(matrix),
                VehicleConfiguration = Mapper.Map(configuration),
                EquipmentSelected = parameters.EquipmentSelected
            };

            return results;
        }

        class Action : IOptionAction
        {
            public int OptionId { get; set; }

            public OptionActionType ActionType { get; set; }
        }
    }
}
