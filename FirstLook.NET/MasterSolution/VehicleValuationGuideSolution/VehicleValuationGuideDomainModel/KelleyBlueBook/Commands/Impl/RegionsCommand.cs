﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;


namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl
{
    public class RegionsCommand : ICommand<RegionsResultsDto, RegionsArgumentsDto>
    {
        public RegionsResultsDto Execute(RegionsArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IList<Region> regions = resolver.Resolve<IKbbService>().Regions();

            return new RegionsResultsDto
            {
                Arguments = parameters,
                Regions = Mapper.Map(regions)
            };
        }
    }
}
