﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl
{
    [Serializable]
    public class SuccessorCommand : ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
    {
        public SuccessorResultsDto Execute(SuccessorArgumentsDto parameters)
        {
            IKbbTraversal traversal = RegistryFactory.GetResolver().Resolve<IKbbTraversal>();

            ITree tree = traversal.CreateTree();
            
            ITreePath path = new TreePath
            {
                State = parameters.Successor
            };

            ITreeNode node = tree.GetNodeByPath(path);

            SuccessorResultsDto results = new SuccessorResultsDto
            {
                Arguments = parameters,
                Path = new PathDto
                {
                    CurrentNode = Mapper.Map(node, Mapper.Operation.Yes, Mapper.Operation.No),
                    Successors = Mapper.Map(node.Children)
                }
            };

            return results;
        }
    }
}
