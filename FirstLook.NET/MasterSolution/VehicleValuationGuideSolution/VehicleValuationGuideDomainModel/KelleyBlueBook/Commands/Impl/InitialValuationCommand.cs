﻿using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api; 

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl
{
    [Serializable]
    public class InitialValuationCommand : ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
    {
        public InitialValuationResultsDto Execute(InitialValuationArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IKbbService service = resolver.Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(parameters.VehicleId);

            Region region = null;

            if (parameters.Region != null)
            {
                region = service.Regions().Any(x => x.Id == parameters.Region.Id)
                          ? service.Regions().First(x => x.Id == parameters.Region.Id) : Mapper.Map(parameters.Region);
            }

            IVehicleConfiguration conf = resolver.Resolve<IKbbConfiguration>().InitialConfiguration(
                parameters.VehicleId,
                parameters.Vin,
                region,
                parameters.ZipCode,
                parameters.HasMileage ? parameters.Mileage : default(int?)
                );

            Matrix matrix = resolver.Resolve<IKbbCalculator>().CalculateData(conf,parameters.EquipmentSelected,parameters.DealerId);

            InitialValuationResultsDto results = new InitialValuationResultsDto
            {
                Arguments            = parameters,
                Options              = Mapper.Map(vehicle.Options, service.OptionAdjustments(vehicle.Id)),
                Tables               = Mapper.Map(matrix),
                Specifications       = Mapper.Map(vehicle.Specifications),
                VehicleConfiguration = Mapper.Map(conf)
            };

            return results;
        }
    }
}
