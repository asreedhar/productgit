﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways;
using System.Diagnostics;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Mappers
{
    /// <summary>
    /// Vehicle configuration mapper. Handles the interaction between data requests and operations.
    /// </summary>
    public class VehicleConfigurationMapper
    {
        #region Properties

        /// <summary>
        /// Vehicle data gateway.
        /// </summary>
        private readonly VehicleGateway _vehicleGateway = new VehicleGateway();        

        /// <summary>
        /// Vehicle configuration data gateway.
        /// </summary>
        private readonly VehicleConfigurationGateway _vehicleConfigurationGateway = new VehicleConfigurationGateway();

        /// <summary>
        /// Vehicle configuration history record data gateway.
        /// </summary>
        private readonly VehicleConfigurationHistoryGateway _vehicleConfigurationHistoryGateway = new VehicleConfigurationHistoryGateway();

        /// <summary>
        /// Option action data gateway.
        /// </summary>
        private readonly OptionActionGateway _actionGateway = new OptionActionGateway();

        /// <summary>
        /// Option state data gateway.
        /// </summary>
        private readonly OptionStateGateway _stateGateway = new OptionStateGateway();

        /// <summary>
        /// Matrix data gateway.
        /// </summary>
        private readonly MatrixGateway _matrixGateway = new MatrixGateway();

        /// <summary>
        /// Matrix cell data gateway.
        /// </summary>
        private readonly MatrixCellGateway _matrixCellGateway = new MatrixCellGateway();

        /// <summary>
        /// Matrix - vehicle configuration mapping data gateway.
        /// </summary>
        private readonly VehicleConfigurationHistoryMatrixGateway _vehicleConfigurationHistoryMatrixGateway = new VehicleConfigurationHistoryMatrixGateway();

        #endregion

        /// <summary>
        /// Load the list of publications available for the given broker and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client vehicle association.</param>
        /// <returns>List of publication details.</returns>
        public IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);

            IList<IEdition<IPublicationInfo>> publications = new List<IEdition<IPublicationInfo>>();

            if (vehicleRow != null)
            {
                IList<VehicleConfigurationHistoryGateway.Row> vehicleConfigurationHistoryRows = 
                    _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId);

                foreach (var vehicleConfigurationHistoryRow in vehicleConfigurationHistoryRows)
                {
                    publications.Add(new Edition<IPublicationInfo>
                    {
                        Data = new PublicationInfo
                        {
                            ChangeAgent = ChangeAgent.User,
                            ChangeType  = (ChangeType)vehicleConfigurationHistoryRow.ChangeTypeBitFlags,
                            Id          = vehicleConfigurationHistoryRow.Id
                        },
                        DateRange = GetDateRange(vehicleConfigurationHistoryRow.AuditRow),
                        User      = GetUser(vehicleConfigurationHistoryRow)
                    });
                }
            }
            return publications;
        }

        /// <summary>
        /// Load the publication for the given broker and vehicle that was valid on the given date.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client vehicle association.</param>
        /// <param name="on">Date on which the publication should be valid.</param>
        /// <returns>Publication.</returns>
        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);
            if (vehicleRow == null)
            {
                return null;
            }

            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow =
                _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId, on);

            if (vehicleConfigurationHistoryRow == null)
            {
                return null;
            }

            return GetPublication(vehicleConfigurationHistoryRow);
        }

        /// <summary>
        /// Load the publication for the given broker and vehicle that has the given identifier.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client vehicle association.</param>
        /// <param name="id">Vehicle configuration history identifier.</param>
        /// <returns>Publication.</returns>
        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);
            if (vehicleRow == null)
            {
                return null;
            }

            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow =
                _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId, id);

            if (vehicleConfigurationHistoryRow == null)
            {
                return null;
            }

            return GetPublication(vehicleConfigurationHistoryRow);
        }

        /// <summary>
        /// Save a configuration and valuation for a client's vehicle as a publication.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client vehicle association.</param>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="matrix">Configuration valuation.</param>
        /// <returns>Publication that was saved.</returns>
        public IEdition<IPublication> Save(IBroker broker, ClientVehicleIdentification vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            // TODO: ask Simon - vehicle.Id = VehicleID and vehicle.Vehicle.Id = ReferenceID, which one to use here?
            // Fetch the vehicle.
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);

            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow;

            ChangeType changeType;

            // If no configuration already exists for this vehicle.
            if (vehicleRow == null)
            {

              
               //Debug.Print("Start time" + System.DateTime.Now);
                //CloneData(configuration);
               // Debug.Print("End time" + System.DateTime.Now);
                changeType = ChangeType.None;

                // Insert the configuration.
                VehicleConfigurationGateway.Row vehicleConfigurationRow = _vehicleConfigurationGateway.Insert(vehicle.Vehicle.Id);

                // Insert the mapping from the vehicle to the configuration.
                _vehicleGateway.Insert(vehicle.Id, vehicleConfigurationRow.Id);

                // Insert the history record.
                vehicleConfigurationHistoryRow = Insert(vehicleConfigurationRow, configuration, matrix, changeType, null);
            }
            // If there have already been configurations for this vehicle.
            else
            {
                // Get the current configuration.
                vehicleConfigurationHistoryRow = 
                    _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId, DateTime.Now); // TODO: Oleg - remove datetime.now - use current table

                VehicleConfigurationGateway.Row vehicleConfigurationRow = 
                    _vehicleConfigurationGateway.Fetch(vehicleConfigurationHistoryRow.VehicleConfigurationId);

                IEdition<IPublication> currentPublication = GetPublication(vehicleConfigurationHistoryRow);

                changeType = currentPublication.Data.VehicleConfiguration.Compare(configuration);

                // If there's been a change, insert the new configuration.
                /*if (changeType != ChangeType.None)
                {
                    CloneData(configuration);

                    vehicleConfigurationHistoryRow = 
                        Insert(vehicleConfigurationRow, configuration, matrix, changeType, vehicleConfigurationHistoryRow.AuditRow);
                }*/
            }

            return new Edition<IPublication>
            {
                Data = new Publication
                {
                    ChangeAgent          = ChangeAgent.User,
                    ChangeType           = changeType,
                    //Id                   = vehicleConfigurationHistoryRow.Id,
                    Matrix               = matrix,
                    Service              = GetService(configuration.BookDate.Id),
                    VehicleConfiguration = configuration
                }
                /*,
                DateRange = GetDateRange(vehicleConfigurationHistoryRow.AuditRow),
                User      = GetUser(vehicleConfigurationHistoryRow)*/
            };
        }

        /// <summary>
        /// Insert into the Vehicle database the values for the vehicle with the given identifiers from the Kelley Blue
        /// Book database.
        /// </summary>
        /// <param name="configuration">Vehicle configuration.</param>
        private static void CloneData(IVehicleConfiguration configuration)
        {
            RegistryFactory.GetResolver().Resolve<IKbbDatastore>().CloneData(
                configuration.BookDate.Id,
                configuration.VehicleId);
        }

        /// <summary>
        /// Insert an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationRow">Vehicle configuration details.</param>
        /// <param name="configuration">Configuration to save.</param>
        /// <param name="matrix">Valuation of the configuration.</param>
        /// <param name="changeType">How this configuration differs from the previous version.</param>
        /// <param name="auditRow">Audit row.</param>
        /// <returns>The configuration history record that was saved.</returns>
        private VehicleConfigurationHistoryGateway.Row Insert(VehicleConfigurationGateway.Row vehicleConfigurationRow, 
                                                              IVehicleConfiguration configuration, 
                                                              Matrix matrix,
                                                              ChangeType changeType, 
                                                              AuditRow auditRow)
        {
            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow;

            int dataLoadId = configuration.BookDate.Id;

            // If there's no audit, just create the new history record.
            if (auditRow == null)
            {
                vehicleConfigurationHistoryRow = _vehicleConfigurationHistoryGateway.Insert(
                    vehicleConfigurationRow.Id,
                    configuration.VehicleId,
                    dataLoadId,
                    configuration.Region,
                    configuration.ZipCode,                   
                    configuration.Mileage,
                    (int)changeType);
            }
            // If there is an audit, update the previous entry to be superseded, and create the history record.
            else
            {
                vehicleConfigurationHistoryRow = _vehicleConfigurationHistoryGateway.Update(
                    vehicleConfigurationRow.Id,
                    configuration.VehicleId,
                    dataLoadId,
                    configuration.Region,
                    configuration.ZipCode,
                    configuration.Mileage,
                    (int)changeType,
                    auditRow);
            }

            // Insert the option actions.
            foreach (IOptionAction action in configuration.OptionActions)
            {
                _actionGateway.Insert(vehicleConfigurationHistoryRow.Id, action.OptionId, dataLoadId, (byte)action.ActionType);
            }            

            // Insert the option states.
            /*foreach (IOptionState state in configuration.OptionStates)
            {
                _stateGateway.Insert(vehicleConfigurationHistoryRow.Id, state.OptionId, dataLoadId, state.Selected);    
            }*/

            // Insert the matrix.
            MatrixGateway.Row matrixRow = _matrixGateway.Insert(dataLoadId);

            // Insert the matrix - configuration mapping.
           // _vehicleConfigurationHistoryMatrixGateway.Insert(vehicleConfigurationHistoryRow.Id, matrixRow.Id, dataLoadId);

            IKbbService service = RegistryFactory.GetResolver().Resolve<IKbbService>();
            
            IList<PriceTypeData> priceTypes  = service.PriceTypeData(dataLoadId);

            // Insert the matrix values.
            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {    
                if (priceType == PriceType.Undefined)
                {
                    continue;
                }
                //If price type does not exists in the database do not add it, This one throws Foreign key exception.
                if (priceTypes.Count(data => data.PriceTypeId == (int) priceType) == 0)
                {
                    continue;
                }
                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    if (valuation == Valuation.Undefined)
                    {
                        continue;
                    }

                    Matrix.Cell cell = matrix[priceType, valuation];
                    
                    _matrixCellGateway.Insert(
                        matrixRow.Id,
                        dataLoadId,
                        (int)priceType,
                        (byte) valuation,
                        cell.Visibility == Matrix.CellVisibility.Value ? true : false,
                        cell.Value);                    
                }
            }
            return vehicleConfigurationHistoryRow;            
        }

        #region Helper Methods

        /// <summary>
        /// Get a user from the given vehicle configuration history row.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryRow">Vehicle configuration history row.</param>
        /// <returns>User.</returns>
        private static IUser GetUser(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {
            User u = vehicleConfigurationHistoryRow.User;

            IPrincipal principal;

            switch (u.UserType)
            {
                case UserType.Member:
                    principal = new DealerRepository().Principal(u.Handle);
                    break;
                case UserType.System:
                    principal = new AgentRepository().Principal(u.Id);
                    break;
                default:
                    throw new ArgumentException("unknown user type");
            }

            return principal.User;
        }

        /// <summary>
        /// Get a price matrix from the given historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryRow">Vehicle configuration history record.</param>
        /// <returns>Price matrix.</returns>
        private Matrix GetMatrix(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {
            VehicleConfigurationHistoryMatrixGateway.Row vehicleConfigurationHistoryMatrixRow =
                _vehicleConfigurationHistoryMatrixGateway.Fetch(vehicleConfigurationHistoryRow.Id);

            IList<MatrixCellGateway.Row> matrixRows =
                _matrixCellGateway.Fetch(vehicleConfigurationHistoryMatrixRow.MatrixId);

            Matrix matrix = new Matrix();

            foreach (MatrixCellGateway.Row matrixRow in matrixRows)
            {
                Matrix.Cell cell = matrix[(PriceType)matrixRow.PriceTypeId, (Valuation)matrixRow.ValuationId];

                cell.Value = matrixRow.Value;
                cell.Visibility = matrixRow.Visible ? Matrix.CellVisibility.Value : Matrix.CellVisibility.NotDisplayed;
            }

            return matrix;
        }

        /// <summary>
        /// Get a date range from the given audit row.
        /// </summary>
        /// <param name="auditRow">Audit row.</param>
        /// <returns>Date range.</returns>
        private static DateRange GetDateRange(AuditRow auditRow)
        {
            return new DateRange
            {
                BeginDate = auditRow.ValidFrom,
                EndDate   = auditRow.ValidUpTo
            };
        }

        private static IKbbService GetService(int dataLoadId)
        {
            IResolver resolver =
                RegistryFactory.GetRegistry().Clone().Register<ISqlService, SnapshotSqlService>(
                    ImplementationScope.Isolated).CreateScope();

            return new KbbService(resolver, () => dataLoadId);
        }

        /// <summary>
        /// Get publication data from the given vehicle history row.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryRow">Vehicle configuration history row.</param>
        /// <returns>Publication.</returns>
        private IEdition<IPublication> GetPublication(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {                       
            IKbbService service = GetService(vehicleConfigurationHistoryRow.DataLoadId);

            IEdition<IPublication> value = new Edition<IPublication>
            {
                Data = new Publication
                {
                    ChangeAgent          = ChangeAgent.User,
                    ChangeType           = (ChangeType)vehicleConfigurationHistoryRow.ChangeTypeBitFlags,
                    Id                   = vehicleConfigurationHistoryRow.Id,
                    Matrix               = GetMatrix(vehicleConfigurationHistoryRow),
                    Service              = service,
                    VehicleConfiguration = GetVehicleConfiguration(vehicleConfigurationHistoryRow)
                },
                DateRange = GetDateRange(vehicleConfigurationHistoryRow.AuditRow),
                User      = GetUser(vehicleConfigurationHistoryRow)
            };

            return value;
        }

        /// <summary>
        /// Get a vehicle configuration from the given history record.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryRow">Vehicle configuration history row.</param>
        /// <returns>Vehicle configuration.</returns>
        private IVehicleConfiguration GetVehicleConfiguration(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {                                    
            IResolver                           resolver = RegistryFactory.GetResolver();
            IVehicleConfigurationBuilderFactory factory  = resolver.Resolve<IVehicleConfigurationBuilderFactory>();
            IVehicleConfigurationBuilder        builder  = factory.NewBuilder();            

            // Get the details of the configuration.
            VehicleConfigurationGateway.Row vehicleConfigurationRow =
                _vehicleConfigurationGateway.Fetch(vehicleConfigurationHistoryRow.VehicleConfigurationId);

            // Add the configuration values.
            builder.BookDate  = new BookDate { Id = vehicleConfigurationHistoryRow.DataLoadId };
            builder.VehicleId = vehicleConfigurationHistoryRow.VehicleId;
            builder.Vin       = vehicleConfigurationRow.Vin;
            builder.Region   = vehicleConfigurationHistoryRow.Region;
            builder.Mileage   = vehicleConfigurationHistoryRow.Mileage;

            // Add the option actions.
            IList<OptionActionGateway.Row> actionRows = _actionGateway.Fetch(vehicleConfigurationHistoryRow.Id);

            foreach (OptionActionGateway.Row actionRow in actionRows)
            {
                builder.Do(actionRow.OptionId, (OptionActionType)actionRow.OptionActionTypeId);
            }

            // Add the option states.
            IList<OptionStateGateway.Row> stateRows = _stateGateway.Fetch(vehicleConfigurationHistoryRow.Id);

            foreach (OptionStateGateway.Row stateRow in stateRows)
            {
                builder.Add(stateRow.OptionId, stateRow.Selected);
            }

            return builder.ToVehicleConfiguration();
        }

        
        #endregion
    }
}
