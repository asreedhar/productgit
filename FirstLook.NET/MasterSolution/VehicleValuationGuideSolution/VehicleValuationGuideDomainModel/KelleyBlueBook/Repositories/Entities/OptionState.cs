﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// The state of an option in a vehicle configuration (i.e. has it been selected?)
    /// </summary>
    [Serializable]
    public class OptionState : IOptionState
    {
        /// <summary>
        /// Option identifier.
        /// </summary>
        public int OptionId { get; internal set; }

        /// <summary>
        /// Is the option selected?
        /// </summary>
        public bool Selected { get; internal set; }
    }
}
