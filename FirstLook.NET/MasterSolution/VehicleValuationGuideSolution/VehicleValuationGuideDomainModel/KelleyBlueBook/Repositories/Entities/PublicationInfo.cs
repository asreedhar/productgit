﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// Details of a publication.
    /// </summary>
    [Serializable]
    public class PublicationInfo : IPublicationInfo
    {
        /// <summary>
        /// Who was responsible for the change between this publication and the previous one.
        /// </summary>
        public ChangeAgent ChangeAgent { get; internal set; }

        /// <summary>
        /// What changed between the last publication and this one.
        /// </summary>
        public ChangeType ChangeType { get; internal set; }

        /// <summary>
        /// Vehicle configuration history identifier.
        /// </summary>
        public int Id { get; internal set; }
    }
}
