﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// Publication of a vehicle configuration and its valuation.
    /// </summary>
    [Serializable]
    public class Publication : PublicationInfo, IPublication
    {
        /// <summary>
        /// KBB service that has the vehicle reference data.
        /// </summary>
        public IKbbService Service { get; internal set; }

        /// <summary>
        /// Vehicle configuration.
        /// </summary>
        public IVehicleConfiguration VehicleConfiguration { get; internal set; }

        /// <summary>
        /// Valuation of the configuration.
        /// </summary>
        public Matrix Matrix { get; internal set; }        
    }
}
