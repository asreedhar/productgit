﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// An edition of data.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class Edition<T> : IEdition<T>
    {
        /// <summary>
        /// Date range that this edition covers.
        /// </summary>
        public IDateRange DateRange { get; internal set; }

        /// <summary>
        /// User responsible for this edition.
        /// </summary>
        public IUser User { get; internal set; }

        /// <summary>
        /// Data of the edition.
        /// </summary>
        public T Data { get; internal set; }
    }
}
