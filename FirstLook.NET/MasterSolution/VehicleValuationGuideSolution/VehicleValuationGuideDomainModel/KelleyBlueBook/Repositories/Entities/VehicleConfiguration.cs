﻿using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// Vehicle configuration.
    /// </summary>
    [Serializable]
    public class VehicleConfiguration : IVehicleConfiguration
    {
        /// <summary>
        /// Reference data identifier.
        /// </summary>
        public int VehicleId { get; internal set; }
        
        /// <summary>
        /// Reference data version identifier.
        /// </summary>
        public BookDate BookDate { get; internal set; }

        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; internal set; }

        /// <summary>
        /// Mileage.
        /// </summary>
        public int? Mileage { get; internal set; }

        public string ZipCode { get; internal set; }

        public Region Region { get; internal set; }
 
        /// <summary>
        /// Option actions.
        /// </summary>
        public IList<IOptionAction> OptionActions { get; private set; }

        /// <summary>
        /// Option states.
        /// </summary>
        public IList<IOptionState> OptionStates { get; private set; }

        /// <summary>
        /// Get the option state for the given vehicle option.
        /// </summary>
        /// <param name="option">Vehicle option.</param>
        /// <returns>Option state.</returns>
        public OptionState this[VehicleOption option]
        {
            get
            {
                OptionState optionState = null;

                foreach (OptionState loopState in OptionStates)
                {
                    if (loopState.OptionId == option.Id)
                    {
                        optionState = loopState;

                        break;
                    }
                }

                return optionState;
            }
        }

        private bool _isMissingEngine;
        private bool _isMissingDriveTrain;
        private bool _isMissingTransmission;        

        /// <summary>
        /// Is this configuration missing an engine, drive train or transmission?
        /// </summary>
        public bool IsValid
        {
            get { return !(_isMissingEngine || _isMissingDriveTrain || _isMissingTransmission); }
        }

        /// <summary>
        /// Is this configuration missing an engine?
        /// </summary>
        public bool IsMissingEngine
        {
            get { return _isMissingEngine; }
            internal set { _isMissingEngine = value; }
        }

        /// <summary>
        /// Is this configuration missing a drive train?
        /// </summary>
        public bool IsMissingDriveTrain
        {
            get { return _isMissingDriveTrain; }
            internal set { _isMissingDriveTrain = value; }
        }

        /// <summary>
        /// Is this configuration missing a transmission?
        /// </summary>
        public bool IsMissingTransmission
        {
            get { return _isMissingTransmission; }
            internal set { _isMissingTransmission = value; }
        }

        /// <summary>
        /// Initialize the option lists on creation.
        /// </summary>
        public VehicleConfiguration()
        {
            OptionActions = new List<IOptionAction>();
            OptionStates = new List<IOptionState>();
        }

        /// <summary>
        /// Compare this configuration to another to determine what is different.
        /// </summary>
        /// <param name="configuration">Configuration to compare against.</param>
        /// <returns>Bit flags of what is different.</returns>
        public ChangeType Compare(IVehicleConfiguration configuration)
        {
            ChangeType changes = ChangeType.None;

            // Book date (aka DataLoadID).
            if (BookDate.Id != configuration.BookDate.Id)
            {
                changes |= ChangeType.BookDate;
            }
            
            // Vehicle identifier.
            if (VehicleId != configuration.VehicleId)
            {
                changes |= ChangeType.VehicleId;
            }
            
            // Mileage.
            if (Mileage != configuration.Mileage)
            {
                changes |= ChangeType.Mileage;
            }
            
            // Zipcode
            if (ZipCode != configuration.ZipCode)   
            {
                changes |= ChangeType.ZipCode;
            }

            if (Region.Id != configuration.Region.Id)
            {
                changes |= ChangeType.Region;
            }

            // Option actions.
            if (OptionActions.Count != configuration.OptionActions.Count)
            {
                changes |= ChangeType.OptionActions;
            }
            else
            {
                foreach (IOptionAction action in OptionActions)
                {
                    bool found = false;
                    foreach (IOptionAction action2 in configuration.OptionActions)
                    {
                        if (action.OptionId == action2.OptionId && action.ActionType == action2.ActionType)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        changes |= ChangeType.OptionActions;
                        break;
                    }
                }
            }

            // Option states.
            if (OptionStates.Count != configuration.OptionStates.Count)
            {
                changes |= ChangeType.OptionStates;
            }
            else
            {
                foreach (IOptionState state in OptionStates)
                {
                    bool found = false;
                    foreach (IOptionState state2 in configuration.OptionStates)
                    {
                        if (state.OptionId == state2.OptionId && state.Selected == state2.Selected)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        changes |= ChangeType.OptionStates;
                        break;
                    }
                }
            }

            return changes;
        }
    }
}
