﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// A start and end date that define a range.
    /// </summary>
    [Serializable]
    public class DateRange : IDateRange
    {        
        /// <summary>
        /// Begin date.
        /// </summary>
        public DateTime BeginDate { get; internal set; }

        /// <summary>
        /// End date.
        /// </summary>
        public DateTime EndDate { get; internal set; }

        /// <summary>
        /// Is the given date covered by this range?
        /// </summary>
        /// <param name="date">Date to check against this range.</param>
        /// <returns>True if the date is between the begin and end dates of this range.</returns>
        public bool Covers(DateTime date)
        {
            return date.CompareTo(BeginDate) >= 0 && date.CompareTo(EndDate) <= 0;
        }
    }
}
