﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities
{
    /// <summary>
    /// An action to add or remove an option from a vehicle configuration.
    /// </summary>
    [Serializable]
    public class OptionAction : IOptionAction
    {
        /// <summary>
        /// Option identifier.
        /// </summary>
        public int OptionId { get; internal set; }

        /// <summary>
        /// Is the option being added or removed from the configuration?
        /// </summary>
        public OptionActionType ActionType { get; internal set; }
    }
}
