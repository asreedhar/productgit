﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Kelley Blue Book option state data gateway.
    /// </summary>
    public class OptionStateGateway : GatewayBase
    {
        /// <summary>
        /// A row returned when fetching option states.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Vehicle configuration history identifier.
            /// </summary>
            public int VehicleConfigurationHistoryId { get; set; }

            /// <summary>
            /// Option identifier.
            /// </summary>
            public int OptionId { get; set; }

            /// <summary>
            /// Is the option selected?
            /// </summary>
            public bool Selected { get; set; }

            /// <summary>
            /// Reference data version identifier.
            /// </summary>
            public int DataLoadId { get; set; }
        }

        /// <summary>
        /// Option state row serializer.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize an option state from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Option state row.</returns>
            public override Row Deserialize(IDataRecord record)
            {                
                return new Row
                {
                    VehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId")),
                    OptionId                      = record.GetInt32(record.GetOrdinal("OptionID")),
                    DataLoadId                    = record.GetInt32(record.GetOrdinal("DataLoadID")),
                    Selected                      = record.GetBoolean(record.GetOrdinal("Selected"))
                };
            }
        }

        /// <summary>
        /// Fetch option states for an historical record of a vehicle configuration.
        /// </summary>        
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>List of option states.</returns>
        public IList<Row> Fetch(int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.OptionState_Fetch(vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        /// <summary>
        /// Insert an option state for an historical vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="selected">Is the option selected?</param>
        public void Insert(int vehicleConfigurationHistoryId, int optionId, int dataLoadId, bool selected)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();
            datastore.OptionState_Insert(vehicleConfigurationHistoryId, optionId, selected, dataLoadId);
        }
    }
}
