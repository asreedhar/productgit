﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Serializers;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Kelley Blue Book vehicle configuration history data gateway.
    /// </summary>
    public class VehicleConfigurationHistoryGateway : AuditingGateway
    {
        /// <summary>
        /// A row returned when fetching a vehicle configuration history.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Vehicle configuration history identifier.
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Vehicle configuration identifier.
            /// </summary>
            public int VehicleConfigurationId { get; set; }            

            /// <summary>
            /// Reference data identifier.
            /// </summary>
            public int VehicleId { get; set; }

            /// <summary>
            /// Reference data version identifier.
            /// </summary>
            public int DataLoadId { get; set; }

            /// <summary>
            /// US region.
            /// </summary>
            public Region Region { get; set; }

            /// <summary>
            /// Zipcode
            /// </summary>
            public string ZipCode { get; set; }

            /// <summary>
            /// Mileage.
            /// </summary>
            public int? Mileage { get; set; }

            /// <summary>
            /// How this vehicle configuration differs from a previous version.
            /// </summary>
            public int ChangeTypeBitFlags { get; set; }

            /// <summary>
            /// Audit row.
            /// </summary>
            public AuditRow AuditRow { get; set; }

            /// <summary>
            /// User responsible for this configuration.
            /// </summary>
            public User User { get; set; }
        }

        /// <summary>
        /// Vehicle configuration history row serializer.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize a vehicle configuration history row from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Vehicle configuration history row.</returns>
            public override Row Deserialize(IDataRecord record)
            {                
                int? mileage = null;
                int ordinal = record.GetOrdinal("Mileage");
                if (!record.IsDBNull(ordinal))
                {
                    mileage = record.GetInt32(ordinal);
                }
                
                return new Row
                {
                    Id                     = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId")),
                    VehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationId")),                    
                    VehicleId              = record.GetInt32(record.GetOrdinal("VehicleID")),                    
                    DataLoadId             = record.GetInt32(record.GetOrdinal("DataLoadID")),
                    ZipCode                = record.IsDBNull(record.GetOrdinal("ZipCode")) ? string.Empty : record.GetString(record.GetOrdinal("ZipCode")), 
                    Region                 = new RegionSerializer().Deserialize(record),
                    ChangeTypeBitFlags     = record.GetInt32(record.GetOrdinal("ChangeTypeBitFlags")),
                    Mileage                = mileage,
                    AuditRow               = new AuditRowSerializer().Deserialize(record),
                    User                   = new UserSerializer().Deserialize(record)
                };
            }
        }

        /// <summary>
        /// Fetch all historical records for a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <returns>List of vehicle configuration history records.</returns>
        public IList<Row> Fetch(int vehicleConfigurationId)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId))
            {
                ISerializer<Row> serializer = new RowSerializer();
                return serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Fetch the historical configuration record that was valid on the given date.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <param name="on">Date on which the configuration should be valid.</param>
        /// <returns>Vehicle configuration history row.</returns>
        public Row Fetch(int vehicleConfigurationId, DateTime on)
        {
            string key = CreateCacheKey(vehicleConfigurationId, on);
            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId, on))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();
                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }
            return row;
        }

        /// <summary>
        /// Fetch the historical vehicle configuration record with the given identifiers.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Vehicle configuration history identifier.</returns>
        public Row Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationId, vehicleConfigurationHistoryId);
            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId, vehicleConfigurationHistoryId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();
                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }
            return row;
        }

        /// <summary>
        /// Insert the first historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>        
        /// <param name="vehicleId">Reference data identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="region">US region.</param>
        /// <param name="zipCode">Zipcode.</param>
        /// <param name="mileage">Mileage.</param>
        /// <param name="changeTypeBitFlags">How this history record differs from the previous one.</param>
        /// <returns>Vehicle configuration history record that was inserted.</returns>
        public Row Insert(int vehicleConfigurationId, int vehicleId, int dataLoadId, Region region, string zipCode,
                          int? mileage, int changeTypeBitFlags)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Insert(vehicleConfigurationId, 
                                                                                      vehicleId, 
                                                                                      dataLoadId,
                                                                                      region,
                                                                                      zipCode, 
                                                                                      mileage, 
                                                                                      changeTypeBitFlags, 
                                                                                      Insert().Id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }

        /// <summary>
        /// Insert a new historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>        
        /// <param name="vehicleId">Reference data identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="region">US region.</param>
        /// <param name="zipCode">Zipcode.</param>
        /// <param name="mileage">Mileage.</param>
        /// <param name="changeTypeBitFlags">How this history record differs from the previous one.</param>
        /// <param name="auditRow">Audit row to be updated.</param>
        /// <returns>Vehicle configuration history record that was inserted.</returns>
        public Row Update(int vehicleConfigurationId, int vehicleId, int dataLoadId, Region region, string zipCode, 
                          int? mileage, int changeTypeBitFlags, AuditRow auditRow)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Insert(vehicleConfigurationId, 
                                                                                      vehicleId, 
                                                                                      dataLoadId,
                                                                                      region,
                                                                                      zipCode, 
                                                                                      mileage, 
                                                                                      changeTypeBitFlags, 
                                                                                      Update(auditRow).Id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }
    }
}
