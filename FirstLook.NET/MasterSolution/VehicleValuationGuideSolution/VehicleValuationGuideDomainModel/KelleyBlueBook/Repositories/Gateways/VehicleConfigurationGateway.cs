﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Kelley Blue Book vehicle configuration data gateway.
    /// </summary>
    public class VehicleConfigurationGateway : GatewayBase
    {
        /// <summary>
        /// A row returned when fetching a vehicle configuration.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Vehicle configuration identifier.
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Vin.
            /// </summary>
            public string Vin { get; set; }

            /// <summary>
            /// Reference data identifier.
            /// </summary>
            public int VehicleId { get; set; }
        }

        /// <summary>
        /// Vehicle configuration row serializer.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize a vehicle configuration from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Vehicle configuration row.</returns>
            public override Row Deserialize(IDataRecord record)
            {               
                return new Row
                {
                    Id        = record.GetInt32(record.GetOrdinal("VehicleConfigurationId")),
                    VehicleId = record.GetInt32(record.GetOrdinal("VehicleId")),
                    Vin       = record.GetString(record.GetOrdinal("VIN"))
                };
            }
        }

        /// <summary>
        /// Fetch the vehicle configuration with the given identifier.
        /// </summary>
        /// <param name="id">Vehicle configuration identifier.</param>
        /// <returns>Vehicle configuration row.</returns>
        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);
            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_Fetch(id))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();
                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a vehicle configuration row.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Vehicle configuration row that was saved.</returns>
        public Row Insert(int vehicleId)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_Insert(vehicleId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
