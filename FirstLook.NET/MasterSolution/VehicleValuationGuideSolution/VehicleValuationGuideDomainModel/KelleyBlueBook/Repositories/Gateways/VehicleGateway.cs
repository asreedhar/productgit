﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Vehicle data gateway.
    /// </summary>
    public class VehicleGateway : GatewayBase
    {
        /// <summary>
        /// Vehicle to vehicle configuration mapping row.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Vehicle configuration identifier.
            /// </summary>
            public int VehicleConfigurationId { get; set; }

            /// <summary>
            /// Vehicle identifier.
            /// </summary>
            public int VehicleId { get; set; }
        }

        /// <summary>
        /// Serializer for vehicle to vehicle configuration mapping rows.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize a vehicle to vehicle configuration mapping row from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Vehicle to vehicle configuration mapping row.</returns>
            public override Row Deserialize(IDataRecord record)
            {                               
                return new Row
                {
                   VehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationId")), 
                   VehicleId              = record.GetInt32(record.GetOrdinal("VehicleId"))
                };
            }
        }

        /// <summary>
        /// Fetch the vehicle configuration mapping for the vehicle with the given identifier.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Vehicle to vehicle configuration mapping row.</returns>
        public Row Fetch(int vehicleId)
        {
            string key = CreateCacheKey(vehicleId);
            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IClientDatastore datastore = Resolve<IClientDatastore>();

                using (IDataReader reader = datastore.Vehicle_Kbb_Fetch(vehicleId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();
                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a vehicle to vehicle configuration mapping row.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        public void Insert(int vehicleId, int vehicleConfigurationId)
        {            
            IClientDatastore datastore = Resolve<IClientDatastore>();
            datastore.Vehicle_Kbb_Insert(vehicleId, vehicleConfigurationId);
        }
    }
}
