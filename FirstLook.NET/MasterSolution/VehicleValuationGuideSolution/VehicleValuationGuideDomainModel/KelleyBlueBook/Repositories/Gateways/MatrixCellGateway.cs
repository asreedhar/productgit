﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Matrix cell data gateway.
    /// </summary>
    public class MatrixCellGateway : GatewayBase
    {
        /// <summary>
        /// A row returned when fetching a matrix cell.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Matrix identifier.
            /// </summary>
            public int MatrixId { get; set; }

            /// <summary>
            /// Reference data version identifier.
            /// </summary>
            public int DataLoadId { get; set; }

            /// <summary>
            /// Price type identifier.
            /// </summary>
            public int PriceTypeId { get; set; }

            /// <summary>
            /// Valuation type identifier.
            /// </summary>
            public byte ValuationId { get; set; }

            /// <summary>
            /// Is this row visibile?
            /// </summary>
            public bool Visible { get; set; }

            /// <summary>
            /// Value of the cell.
            /// </summary>
            public decimal? Value { get; set; }
        }

        /// <summary>
        /// Serializer for matrix cell rows.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize a matrix cell row from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Matrix cell row.</returns>
            public override Row Deserialize(IDataRecord record)
            {
                decimal? value = null;
                int ordinal = record.GetOrdinal("Value");
                if (!record.IsDBNull(ordinal))
                {
                    value = record.GetDecimal(ordinal);
                }
               
                return new Row
                {
                    MatrixId    = record.GetInt32(record.GetOrdinal("MatrixID")),
                    DataLoadId  = record.GetInt32(record.GetOrdinal("DataLoadID")),
                    PriceTypeId = record.GetInt32(record.GetOrdinal("PriceTypeID")),
                    ValuationId = record.GetByte(record.GetOrdinal("ValuationID")),
                    Visible     = record.GetBoolean(record.GetOrdinal("Visible")),
                    Value       = value
                };
            }
        }

        /// <summary>
        /// Fetch cells for a matrix.
        /// </summary>        
        /// <param name="matrixId">Matrix identifier.</param>        
        /// <returns>List of matrix cell rows.</returns>
        public IList<Row> Fetch(int matrixId)
        {
            string key = CreateCacheKey(matrixId);
            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.MatrixCell_Fetch(matrixId))
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }
            return rows;
        }

        /// <summary>
        /// Insert a matrix cell row.
        /// </summary>        
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="priceTypeId">Type of price.</param>
        /// <param name="valuationId">Type of valuation.</param>
        /// <param name="visible">Visibility of the cell.</param>
        /// <param name="value">Price value.</param>
        public void Insert(int matrixId, int dataLoadId, int priceTypeId, byte valuationId, bool visible, decimal? value)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();
            datastore.MatrixCell_Insert(matrixId, dataLoadId, priceTypeId, valuationId, visible, value);
        }
    }
}
