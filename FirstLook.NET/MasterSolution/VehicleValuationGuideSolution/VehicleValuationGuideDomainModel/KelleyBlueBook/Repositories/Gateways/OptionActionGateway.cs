﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Kelley Blue Book option action data gateway.
    /// </summary>
    public class OptionActionGateway : GatewayBase
    {
        /// <summary>
        /// A row returned when fetching option actions.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Vehicle configuration history identifier.
            /// </summary>
            public int VehicleConfigurationHistoryId { get; set; }

            /// <summary>
            /// Option identifier.
            /// </summary>
            public int OptionId { get; set; }

            /// <summary>
            /// Option action type identifier.
            /// </summary>
            public byte OptionActionTypeId { get; set; }            

            /// <summary>
            /// Reference data version identifier.
            /// </summary>
            public int DataLoadId { get; set; }
        }

        /// <summary>
        /// Option action row serializer.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize an option action from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Option action row.</returns>
            public override Row Deserialize(IDataRecord record)
            {                
                return new Row
                {
                    VehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId")),
                    OptionActionTypeId            = record.GetByte(record.GetOrdinal("OptionActionTypeID")),
                    OptionId                      = record.GetInt32(record.GetOrdinal("OptionID")),
                    DataLoadId                    = record.GetInt32(record.GetOrdinal("DataLoadID"))
                };
            }
        }

        /// <summary>
        /// Fetch option actions for an historical record of a vehicle configuration.
        /// </summary>        
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>List of option actions.</returns>
        public IList<Row> Fetch(int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.OptionAction_Fetch(vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        /// <summary>
        /// Insert an option action for an historical vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="optionActionTypeId">Option action type identifier.</param>
        public void Insert(int vehicleConfigurationHistoryId, int optionId, int dataLoadId, byte optionActionTypeId)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();
            datastore.OptionAction_Insert(vehicleConfigurationHistoryId, optionId, optionActionTypeId, dataLoadId);            
        }
    }
}
