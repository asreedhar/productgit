﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Matrix to vehicle configuration history record mapping data gateway.
    /// </summary>
    public class VehicleConfigurationHistoryMatrixGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Vehicle configuration history identifier.
            /// </summary>
            public int VehicleConfigurationHistoryId { get; set; }

            /// <summary>
            /// Matrix identifier.
            /// </summary>
            public int MatrixId { get; set; }

            /// <summary>
            /// Reference data version identifier.
            /// </summary>
            public int DataLoadId { get; set; }
        }

        /// <summary>
        /// Matrix to vehicle configuration history record row serializer.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize a matrix - vehicle configuration mapping from the given data record.
            /// </summary>
            /// <param name="record">Data record.</param>
            /// <returns>Matrix - vehicle configuration mapping row.</returns>
            public override Row Deserialize(IDataRecord record)
            {                
                return new Row
                {
                    VehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryID")),
                    MatrixId                      = record.GetInt32(record.GetOrdinal("MatrixID")),
                    DataLoadId                    = record.GetInt32(record.GetOrdinal("DataLoadID"))
                };
            }
        }

        /// <summary>
        /// Fetch a matrix to vehicle configuration history record mapping row.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Matrix - vehicle configuration mapping row.</returns>
        public Row Fetch(int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationHistoryId);
            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IKbbDatastore datastore = Resolve<IKbbDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Matrix_Fetch(vehicleConfigurationHistoryId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();
                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }
            return row;
        }

        /// <summary>
        /// Insert a mapping between a matrix and a vehicle configuration history record.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId"></param>
        /// <param name="matrixId"></param>
        /// <param name="dataLoadId"></param>
        public void Insert(int vehicleConfigurationHistoryId, int matrixId, int dataLoadId)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();
            datastore.VehicleConfiguration_History_Matrix_Insert(vehicleConfigurationHistoryId, matrixId, dataLoadId);
        }
    }
}
