﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Gateways
{
    /// <summary>
    /// Matrix data gateway.
    /// </summary>
    public class MatrixGateway : GatewayBase
    {
        /// <summary>
        /// A row returned when fetching matrices.
        /// </summary>
        [Serializable]
        public class Row
        {
            /// <summary>
            /// Matrix identifier.
            /// </summary>
            public int Id { get; set; }
            
            /// <summary>
            /// Reference data version identifier.
            /// </summary>
            public int DataLoadId { get; set; }            
        }

        /// <summary>
        /// Matrix row serializer.
        /// </summary>
        protected class RowSerializer : Serializer<Row>
        {
            /// <summary>
            /// Deserialize a matrix row from the given data record.
            /// </summary>
            /// <param name="record"></param>
            /// <returns></returns>
            public override Row Deserialize(IDataRecord record)
            {                
                return new Row
                {                    
                    Id         = record.GetInt32(record.GetOrdinal("MatrixID")),
                    DataLoadId = record.GetInt32(record.GetOrdinal("DataLoadID"))
                };
            }
        }

        /// <summary>
        /// Insert a matrix row.
        /// </summary>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Matrix row that was inserted.</returns>
        public Row Insert(int dataLoadId)
        {
            IKbbDatastore datastore = Resolve<IKbbDatastore>();

            using (IDataReader reader = datastore.Matrix_Insert(dataLoadId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }
    }
}
