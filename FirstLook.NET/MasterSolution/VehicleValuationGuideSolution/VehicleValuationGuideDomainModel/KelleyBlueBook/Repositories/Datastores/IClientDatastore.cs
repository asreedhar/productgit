﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for a Kelley Blue Book client datastore.
    /// </summary>
    public interface IClientDatastore
    {        
        /// <summary>
        /// Fetch the identifier of the vehicle configuration tied to the vehicle with the given identifier.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader with a vehicle to vehicle configuration mapping row.</returns>
        IDataReader Vehicle_Kbb_Fetch(int vehicleId);

        /// <summary>
        /// Insert a vehicle to vehicle configuration mapping row.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        void Vehicle_Kbb_Insert(int vehicleId, int vehicleConfigurationId);
    }
}
