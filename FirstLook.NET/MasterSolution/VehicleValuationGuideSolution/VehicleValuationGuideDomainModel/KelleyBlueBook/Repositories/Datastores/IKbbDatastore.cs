﻿using System;
using System.Data;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for a Kelley Blue Book datastore.
    /// </summary>
    public interface IKbbDatastore
    {
        /// <summary>
        /// Copy into the Vehicle database the Kelley Blue Book values for the vehicle with the given identifiers.
        /// </summary>
        /// <param name="dataLoadId">Data load identifier.</param>
        /// <param name="vehicleId">Vehicle identifier.</param>        
        void CloneData(int dataLoadId, int vehicleId);

        #region Vehicle Configuration

        /// <summary>
        /// Fetch the descriptive information of the configuration with the given identifier.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <returns>Data reader with overview details on the vehicle configuration.</returns>
        IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId);

        /// <summary>
        /// Insert a master record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader with the vehicle configuration identifier.</returns>
        IDataReader VehicleConfiguration_Insert(int vehicleId);

        /// <summary>
        /// Fetch all historical configuration records for the configuration with the given identifier.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <returns>Data reader with historical configuration records.</returns>
        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId);

        /// <summary>
        /// Fetch the historical record of the configuration with the given identifier that was valid on the given 
        /// date.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <param name="on">Date on which the historical record should be valid.</param>
        /// <returns>Data reader with the historical configuration record.</returns>
        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on);

        /// <summary>
        /// Fetch the historical configuration record with the given identifiers.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with the historical configuration record.</returns>
        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId);

        /// <summary>
        /// Insert an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>        
        /// <param name="vehicleId">Reference data identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="region">US region.</param>
        /// <param name="zipCode">Zipcode.</param>
        /// <param name="mileage">Mileage.</param>
        /// <param name="changeTypeBitFlags">How this historical record differs from the previous one.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with the historical configuration record identifier.</returns>
        IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int vehicleId, int dataLoadId, Region region, string zipCode, int? mileage, int changeTypeBitFlags, int auditRowId);

        #endregion

        #region Options

        /// <summary>
        /// Fetch the option actions tied to an historical configuration record.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option actions.</returns>
        IDataReader OptionAction_Fetch(int vehicleConfigurationHistoryId);

        /// <summary>
        /// Insert an option action for an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionId">Option identifier.</param>        
        /// <param name="optionActionTypeId">Option action type identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void OptionAction_Insert(int vehicleConfigurationHistoryId, int optionId, int optionActionTypeId, int dataLoadId);

        /// <summary>
        /// Fetch the option states tied to an historical configuration record.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option states.</returns>
        IDataReader OptionState_Fetch(int vehicleConfigurationHistoryId);

        /// <summary>
        /// Insert an option state for an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionId">Option identifier.</param>        
        /// <param name="selected">Has the option been selected?</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void OptionState_Insert(int vehicleConfigurationHistoryId, int optionId, bool selected, int dataLoadId);

        #endregion

        #region Matrix

        /// <summary>
        /// Insert a new price matrix.
        /// </summary>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with the matrix identifier.</returns>
        IDataReader Matrix_Insert(int dataLoadId);        

        /// <summary>
        /// Fetch the price matrix cells for the matrix with the given identifier.
        /// </summary>
        /// <param name="matrixId">Matrix identifier.</param>        
        /// <returns>Data reader with price matrix cells.</returns>
        IDataReader MatrixCell_Fetch(int matrixId);

        /// <summary>
        /// Insert a price matrix cell.
        /// </summary>        
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="priceTypeId">Price type identifier.</param>
        /// <param name="valuationId">Valuation type identifier.</param>
        /// <param name="visible">Is this cell visible?</param>
        /// <param name="value">Price matrix cell value.</param>
        void MatrixCell_Insert(int matrixId, int dataLoadId, int priceTypeId, byte valuationId, bool visible, decimal? value);

        /// <summary>
        /// Fetch the price matrix for a given historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with identifiers.</returns>
        IDataReader VehicleConfiguration_History_Matrix_Fetch(int vehicleConfigurationHistoryId);

        /// <summary>
        /// Insert a mapping from an historical record of a vehicle configuration to a price matrix.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void VehicleConfiguration_History_Matrix_Insert(int vehicleConfigurationHistoryId, int matrixId, int dataLoadId);

        #endregion
    }
}
