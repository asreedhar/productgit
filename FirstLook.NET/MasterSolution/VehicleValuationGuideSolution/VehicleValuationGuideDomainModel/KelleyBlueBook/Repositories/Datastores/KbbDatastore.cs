﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores
{
    /// <summary>
    /// Kelley Blue Book datastore.
    /// </summary>
    public class KbbDatastore : SessionDataStore, IKbbDatastore
    {        
        #region Properties

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }        

        #endregion                       

        /// <summary>
        /// Copy into the Vehicle database the Kelley Blue Book values for the vehicle with the given identifiers.
        /// </summary>
        /// <param name="dataLoadId">Data load identifier.</param>
        /// <param name="vehicleId">Vehicle identifier.</param>        
        public void CloneData(int dataLoadId, int vehicleId)
        {
            using (IDbCommand command = CreateCommand())
            {
                command.CommandText = "Kbb.CloneData";

                command.CommandType = CommandType.StoredProcedure;

                Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                Database.AddWithValue(command, "VehicleID",  vehicleId,  DbType.Int32);                

                command.ExecuteNonQuery();
            }
        }

        #region Vehicle Configuration

        /// <summary>
        /// Fetch the descriptive information of the configuration with the given identifier.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <returns>Data reader with overview details on the vehicle configuration.</returns>
        public IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".KbbDatastore_VehicleConfiguration_Fetch.txt";

            return Query(new[] { "VehicleConfiguration" }, 
                         queryName,
                         new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        /// <summary>
        /// Insert a master record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader with the vehicle configuration identifier.</returns>
        public IDataReader VehicleConfiguration_Insert(int vehicleId)
        {
            const string queryNameI = Prefix + ".KbbDatastore_VehicleConfiguration_Insert.txt";
            const string queryNameF = Prefix + ".KbbDatastore_VehicleConfiguration_Fetch_Identity.txt";

            NonQuery(queryNameI, new Parameter("VehicleID", vehicleId, DbType.Int32));
            
            return Query(new[] { "VehicleConfiguration" }, queryNameF);
        }

        /// <summary>
        /// Fetch all historical configuration records for the configuration with the given identifier.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <returns>Data reader with historical configuration records.</returns>
        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".KbbDatastore_VehicleConfiguration_History_Fetch_All.txt";

            return Query(new[] { "VehicleConfiguration_History" },
                         queryName,
                         new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the historical record of the configuration with the given identifier that was valid on the given 
        /// date.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <param name="on">Date on which the historical record should be valid.</param>
        /// <returns>Data reader with the historical configuration record.</returns>
        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on)
        {
            const string queryName = Prefix + ".KbbDatastore_VehicleConfiguration_History_Fetch_DateTime.txt";

            return Query(new[] { "VehicleConfiguration_History" },
                         queryName,
                         new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                         new Parameter("Date", on, DbType.DateTime));
        }

        /// <summary>
        /// Fetch the historical configuration record with the given identifiers.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with the historical configuration record.</returns>
        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".KbbDatastore_VehicleConfiguration_History_Fetch_Id.txt";

            return Query(new[] { "VehicleConfiguration_History" },
                         queryName,
                         new Parameter("VehicleConfigurationID",        vehicleConfigurationId,        DbType.Int32),
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>        
        /// <param name="vehicleId">Reference data identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="region">US region.</param>
        /// <param name="zipCode">Zipcode.</param>
        /// <param name="mileage">Mileage.</param>
        /// <param name="changeTypeBitFlags">How this historical record differs from the previous one.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with the historical configuration record identifier.</returns>
        public IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int vehicleId, int dataLoadId, Region region, 
                                                        string zipCode, int? mileage, int changeTypeBitFlags, int auditRowId)
        {
           /* const string queryNameI = Prefix + ".KbbDatastore_VehicleConfiguration_History_Insert.txt";            
            
            NonQuery(queryNameI,
                     new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),                     
                     new Parameter("VehicleID",              vehicleId,              DbType.Int32),
                     new Parameter("DataLoadID",             dataLoadId,             DbType.Int32),
                     new Parameter("RegionID",               region.Id,              DbType.Int32),
                     new Parameter("ZipCode",                zipCode,                DbType.String),
                     mileage.HasValue
                        ? new Parameter("Mileage",           mileage.Value,          DbType.Int32)
                        : new Parameter("Mileage",           DBNull.Value,           DbType.Int32),
                     new Parameter("ChangeTypeBitFlags",     changeTypeBitFlags,     DbType.Int32),
                     new Parameter("AuditRowID",             auditRowId,             DbType.Int32)
            );*/

            const string queryNameF = Prefix + ".KbbDatastore_VehicleConfiguration_History_Fetch_Identity.txt";            
            
            return Query(new[] { "VehicleConfiguration" }, queryNameF);
        }

        #endregion

        #region Options

        /// <summary>
        /// Fetch the option actions tied to an historical configuration record.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option actions.</returns>
        public IDataReader OptionAction_Fetch(int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".KbbDatastore_OptionAction_Fetch.txt";

            return Query(new[] { "OptionAction" },
                         queryName,
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert an option action for an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="optionActionTypeId">Option action type identifier.</param>
        public void OptionAction_Insert(int vehicleConfigurationHistoryId, int optionId, int optionActionTypeId, int dataLoadId)
        {
            const string queryName = Prefix + ".KbbDatastore_OptionAction_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                     new Parameter("OptionID",           optionId,           DbType.Int32),                     
                     new Parameter("OptionActionTypeID", optionActionTypeId, DbType.Int32),
                     new Parameter("DataLoadID",         dataLoadId,         DbType.Int32));
        }

        /// <summary>
        /// Fetch the option states tied to an historical configuration record.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option states.</returns>
        public IDataReader OptionState_Fetch(int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".KbbDatastore_OptionState_Fetch.txt";

            return Query(new[] { "OptionState" },
                         queryName,
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert an option state for an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="selected">Has the option been selected?</param>
        public void OptionState_Insert(int vehicleConfigurationHistoryId, int optionId, bool selected, int dataLoadId)
        {
            const string queryName = Prefix + ".KbbDatastore_OptionState_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                     new Parameter("OptionID",   optionId,   DbType.Int32),                     
                     new Parameter("Selected",   selected,   DbType.Boolean),
                     new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }

        #endregion

        #region Matrix

        /// <summary>
        /// Insert a new price matrix.
        /// </summary>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with the matrix identifier.</returns>
        public IDataReader Matrix_Insert(int dataLoadId)
        {
            const string queryNameI = Prefix + ".KbbDatastore_Matrix_Insert.txt";
            const string queryNameF = Prefix + ".KbbDatastore_Matrix_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("DataLoadID", dataLoadId, DbType.Int32));

            return Query(new[] { "Matrix" }, queryNameF);
        }

        /// <summary>
        /// Fetch the price matrix cells for the matrix with the given identifier.
        /// </summary>
        /// <param name="matrixId">Matrix identifier.</param>        
        /// <returns>Data reader with price matrix cells.</returns>
        public IDataReader MatrixCell_Fetch(int matrixId)
        {
            const string queryName = Prefix + ".KbbDatastore_MatrixCell_Fetch.txt";

            return Query(new[] { "MatrixCell" },
                         queryName,
                         new Parameter("MatrixID", matrixId, DbType.Int32));
        }

        /// <summary>
        /// Insert a price matrix cell.
        /// </summary>        
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <param name="priceTypeId">Price type identifier.</param>
        /// <param name="valuationId">Valuation type identifier.</param>
        /// <param name="visible">Is this cell visible?</param>
        /// <param name="value">Price matrix cell value.</param>
        public void MatrixCell_Insert(int matrixId, int dataLoadId, int priceTypeId, byte valuationId, bool visible, decimal? value)
        {
            const string queryName = Prefix + ".KbbDatastore_MatrixCell_Insert.txt";

            Parameter valueParameter = value.HasValue
                              ? new Parameter("Value", value,        DbType.Decimal)
                              : new Parameter("Value", DBNull.Value, DbType.Decimal);

            NonQuery(queryName,
                     new Parameter("MatrixID",    matrixId,    DbType.Int32),
                     new Parameter("DataLoadID",  dataLoadId,  DbType.Int32),
                     new Parameter("PriceTypeID", priceTypeId, DbType.Int32),
                     new Parameter("ValuationID", valuationId, DbType.Byte),
                     new Parameter("Visible",     visible,     DbType.Boolean),
                     valueParameter);
        }

        /// <summary>
        /// Fetch the price matrix for a given historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with matrix identifiers.</returns>
        public IDataReader VehicleConfiguration_History_Matrix_Fetch(int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".KbbDatastore_VehicleConfiguration_History_Matrix_Fetch.txt";

            return Query(new[] { "VehicleConfiguration_History_Matrix" },
                         queryName,
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert a mapping from an historical record of a vehicle configuration to a price matrix.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void VehicleConfiguration_History_Matrix_Insert(int vehicleConfigurationHistoryId, int matrixId, int dataLoadId)
        {
            const string queryName = Prefix + ".KbbDatastore_VehicleConfiguration_History_Matrix_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                     new Parameter("MatrixID",   matrixId,   DbType.Int32),
                     new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }        

        #endregion
    }
}
