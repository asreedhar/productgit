﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores
{
    /// <summary>
    /// Module for registering repository datastores.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register repository datastores.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<IClientDatastore, ClientDatastore>(ImplementationScope.Shared);

            registry.Register<IKbbDatastore, KbbDatastore>(ImplementationScope.Shared);
        }
    }
}
