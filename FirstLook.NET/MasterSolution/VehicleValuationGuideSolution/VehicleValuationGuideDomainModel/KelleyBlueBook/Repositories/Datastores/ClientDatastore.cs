﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Datastores          
{
    /// <summary>
    /// Kelley Blue Book client datastore.
    /// </summary>
    public class ClientDatastore : SessionDataStore, IClientDatastore
    {
        #region Properties

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion

        /// <summary>
        /// Fetch the identifier of the vehicle configuration tied to the vehicle with the given identifier.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader with a vehicle to vehicle configuration mapping row.</returns>
        public IDataReader Vehicle_Kbb_Fetch(int vehicleId)
        {            
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Kbb_Fetch.txt";                                                   

            return Query(new[] {"Vehicle_Kbb"},
                         queryName,
                         new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        /// <summary>
        /// Insert a vehicle to vehicle configuration mapping row.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        public void Vehicle_Kbb_Insert(int vehicleId, int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Kbb_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleID", vehicleId, DbType.Int32),
                     new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }
    }
}
