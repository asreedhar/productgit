﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories
{
    public class VehicleConfigurationBuilder : IVehicleConfigurationBuilder
    {
        public VehicleConfigurationBuilder()
        {
            
        }

        public VehicleConfigurationBuilder(IVehicleConfiguration configuration)
        {
            BookDate = configuration.BookDate;
            VehicleId = configuration.VehicleId;
            Vin = configuration.Vin;
            Region = configuration.Region;
            Mileage = configuration.Mileage;

            foreach (IOptionAction action in configuration.OptionActions)
            {
                _actions.Add(Clone(action));
            }

            foreach (IOptionState state in configuration.OptionStates)
            {
                _states.Add(Clone(state));
            }
        }

        public BookDate BookDate { get; set; }

        public int VehicleId { get; set; }

        public string Vin { get; set; }

        public Region Region { get; set; }

        public int? Mileage { get; set; }

        public bool IsMissingEngine { get; set; }

        public bool IsMissingDriveTrain { get; set; }

        public bool IsMissingTransmission { get; set; }

        private readonly List<OptionAction> _actions = new List<OptionAction>();
        private readonly List<OptionState> _states = new List<OptionState>();

        public IOptionState Add(VehicleOption option)
        {
            if (option == null)
            {
                throw new ArgumentNullException("option");
            }

            OptionState state = _states.FirstOrDefault(x => x.OptionId == option.Id);

            if (state == null)
            {
                state = new OptionState
                {
                    OptionId = option.Id,
                    Selected = option.IsDefaultConfiguration
                };

                _states.Add(state);
            }

            return state;
        }

        public IOptionState Add(int optionId, bool selected)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state == null)
            {
                state = new OptionState
                {
                    OptionId = optionId,
                    Selected = selected
                };

                _states.Add(state);
            }

            return state;
        }
        
        public void Select(int optionId)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state != null)
            {
                state.Selected = true;
            }
        }

        public void Deselect(int optionId)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state != null)
            {
                state.Selected = false;
            }
        }

        public IEnumerable<int> Selected()
        {
            return _states.Where(x => x.Selected).Select(y => y.OptionId);
        }

        public bool IsSelected(int optionId, bool returnValue)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state != null)
            {
                return state.Selected;
            }

            return returnValue;
        }

        public IOptionAction Do(int optionId, OptionActionType actionType)
        {
            VerifyOptionName(optionId);

            OptionAction action = new OptionAction
            {
                OptionId = optionId,
                ActionType = actionType
            };

            _actions.Add(action);

            return action;
        }

        public IOptionAction Undo(int optionId)
        {
            VerifyOptionName(optionId);

            OptionAction action = _actions.FirstOrDefault(x => x.OptionId == optionId);

            if (action != null)
            {
                _actions.Remove(action);
            }

            return action;
        }

        public IVehicleConfiguration ToVehicleConfiguration()
        {
            VehicleConfiguration configuration = new VehicleConfiguration
            {
                BookDate = BookDate,
                VehicleId = VehicleId,
                Vin = Vin,
                Region = Region,
                Mileage = Mileage,
                IsMissingDriveTrain = IsMissingDriveTrain,
                IsMissingEngine = IsMissingEngine,
                IsMissingTransmission = IsMissingTransmission
            };

            foreach (OptionAction action in _actions)
            {
                configuration.OptionActions.Add(Clone(action));
            }

            foreach (OptionState state in _states)
            {
                configuration.OptionStates.Add(Clone(state));
            }

            return configuration;
        }

        private static void VerifyOptionName(int optionId)
        {
            if (optionId == 0)
            {
                throw new ArgumentOutOfRangeException("optionId", optionId, "non-zero please");
            }
        }

        private static OptionAction Clone(IOptionAction action)
        {
            return new OptionAction {ActionType = action.ActionType, OptionId = action.OptionId};
        }

        private static OptionState Clone(IOptionState state)
        {
            return new OptionState {OptionId = state.OptionId, Selected = state.Selected};
        }
    }
}
