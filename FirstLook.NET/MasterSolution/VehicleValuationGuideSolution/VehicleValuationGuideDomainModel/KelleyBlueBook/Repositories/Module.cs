﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories
{
    /// <summary>
    /// Module for registering repository components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register Kelley Blue Book repository components.
        /// </summary>        
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<IKbbRepository, KbbRepository>(ImplementationScope.Shared);

            registry.Register<IVehicleConfigurationBuilderFactory, VehicleConfigurationBuilderFactory>(ImplementationScope.Shared);
        }
    }
}
