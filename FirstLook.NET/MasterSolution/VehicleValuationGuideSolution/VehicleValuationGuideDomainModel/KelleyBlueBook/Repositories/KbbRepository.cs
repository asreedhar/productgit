﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories.Mappers;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Repositories
{
    /// <summary>
    /// Kelley Blue Book data repository.
    /// </summary>
    public class KbbRepository : RepositoryBase, IKbbRepository
    {        
        /// <summary>
        /// Load info about the publications tied to the given broker and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <returns>List of publication details.</returns>
        public IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle)
        {
            return DoInSession(() => _mapper.Load(broker, vehicle));
        }

        /// <summary>
        /// Load the publication for the given broker and vehicle that is valid at the given time.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="on">Date on which the publication should be valid.</param>
        /// <returns>Publication.</returns>
        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on)
        {
            return DoInSession(() => _mapper.Load(broker, vehicle, on));
        }

        /// <summary>
        /// Load the publication for the given broker and vehicle that has the given identifier.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="id">Publication identifier (a.k.a. vehicle configuration history identifier).</param>
        /// <returns>Publication.</returns>
        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id)
        {
            return DoInSession(() => _mapper.Load(broker, vehicle, id));
        }

        /// <summary>
        /// Save the configuration and price details for the given client and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">Principal.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="matrix">Price details.</param>
        /// <returns>Publication that was saved.</returns>
        public IEdition<IPublication> Save(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => _mapper.Save(broker, vehicle, configuration, matrix)
            );
        }        

        #region Miscellaneous

        /// <summary>
        /// Configuration mapper.
        /// </summary>
        private readonly VehicleConfigurationMapper _mapper = new VehicleConfigurationMapper();

        /// <summary>
        /// The Kelley Blue Book database is Vehicle.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        /// <summary>
        /// Do nothing on the start of a transaction.
        /// </summary>                
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing on the start of a transaction.
        }

        #endregion
    }
}
