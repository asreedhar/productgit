﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum VehicleOptionType
    {
        Undefined           = 0,
        Equipment           = 4,
        Option              = 5,
        CertifiedPreOwned   = 7
    }
}
