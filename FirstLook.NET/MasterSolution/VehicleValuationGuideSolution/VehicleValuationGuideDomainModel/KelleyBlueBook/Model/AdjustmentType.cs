﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum AdjustmentType
    {
        Undefined   = 0,
        Value       = 1,
        Percent     = 2
    }
}
