﻿SELECT	
	Id = MK.MakeID, 
	Name = MK.DisplayName, 
	SortOrder = CAST(RANK() OVER (ORDER BY MK.DisplayName) AS INT)
FROM	
	{0}.Trim T
JOIN    
	{0}.Model MD ON T.DataloadID = MD.DataloadID AND T.ModelID = MD.ModelID
JOIN    
	{0}.Make MK ON MD.DataloadID = MK.DataloadID AND MD.MakeID = MK.MakeID
WHERE	
	T.DataloadID = @DataLoadID
AND     
	T.YearId = @Year
GROUP BY      
	MK.MakeID, MK.DisplayName
ORDER BY	
	MK.DisplayName