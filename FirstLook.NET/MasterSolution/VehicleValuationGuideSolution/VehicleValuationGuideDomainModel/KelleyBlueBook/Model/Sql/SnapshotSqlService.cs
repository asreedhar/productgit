﻿namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    /// <summary>
    /// Snapshot sql service. Reads and writes Kelley Blue Book data to the Vehicle database.
    /// </summary>
    public class SnapshotSqlService : SqlServiceBase
    {
        /// <summary>
        /// Create a snapshot sql service with the default clone reader.
        /// </summary>
        public SnapshotSqlService()
        {
        }

        /// <summary>
        /// Create a snapshot sql service with the given clone reader.
        /// </summary>
        /// <param name="clone">Clone reader.</param>
        public SnapshotSqlService(ICloneReader clone) : base(clone)
        {
        }

        /// <summary>
        /// Database where the snapshot data is stored.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        /// <summary>
        /// Schema of the snapshot data.
        /// </summary>
        protected override string SchemaName
        {
            get { return "Kbb"; }
        }

        /// <summary>
        /// 'And' qualifier in the snapshot schema.
        /// </summary>
        protected override string And
        {
            get { return "AND"; }
        }
    }
}
