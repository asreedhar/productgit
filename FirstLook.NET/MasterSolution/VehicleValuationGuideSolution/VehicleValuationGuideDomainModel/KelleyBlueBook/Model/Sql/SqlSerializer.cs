﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core.Data; 
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.Impl.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    public class BookDateSerializer : ISqlSerializer<BookDate>
    {
        public BookDate DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<BookDate> Deserialize(IDataReader reader)
        {
            List<BookDate> values = new List<BookDate>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public BookDate Deserialize(IDataRecord record)
        {
            return new BookDate()
                   {
                       Id = record.GetInt32(record.GetOrdinal("DataloadID"))
                   };
        }
    }

    public class YearSerializer : ISqlSerializer<Year>
    {
        public Year DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Year> Deserialize(IDataReader reader)
        {
            List<Year> values = new List<Year>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Year Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("Name"));

            int id = record.GetInt32(record.GetOrdinal("Id"));

            int sortOrder = record.GetInt32(record.GetOrdinal("SortOrder"));

            int mileageZeroPoint = record.GetInt32(record.GetOrdinal("MileageZeroPoint"));

            decimal maximumDeductionPercentage = record.GetDecimal(record.GetOrdinal("MaximumDeductionPercentage"));

            return new Year
                   {
                       Id = id,
                       Name = name,
                       MileageZeroPoint = mileageZeroPoint,
                       MaximumDeductionPercentage = maximumDeductionPercentage,
                       SortOrder = sortOrder
                   };
        }
    }

    public class MakeSerializer : ISqlSerializer<Make>
    {
        public Make DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Make> Deserialize(IDataReader reader)
        {
            List<Make> values = new List<Make>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Make Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("Name"));

            int id = record.GetInt32(record.GetOrdinal("Id"));

            int sortOrder = record.GetInt32(record.GetOrdinal("SortOrder"));

            return new Make {Id = id, Name = name, SortOrder = sortOrder};
        }
    }

    public class ModelSerializer : ISqlSerializer<Model>
    {
        public Model DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Model> Deserialize(IDataReader reader)
        {
            List<Model> values = new List<Model>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Model Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("Name"));

            int id = record.GetInt32(record.GetOrdinal("Id"));

            int sortOrder = record.GetInt32(record.GetOrdinal("SortOrder"));

            return new Model {Id = id, Name = name, SortOrder = sortOrder};
        }
    }

    public class TrimSerializer : ISqlSerializer<Trim>
    {
        public Trim DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Trim> Deserialize(IDataReader reader)
        {
            List<Trim> values = new List<Trim>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Trim Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("Name"));

            int id = record.GetInt32(record.GetOrdinal("Id"));

            int sortOrder = record.GetInt32(record.GetOrdinal("SortOrder"));

            int vehicleId = record.GetInt32(record.GetOrdinal("VehicleId"));

            return new Trim {Id = id, Name = name, SortOrder = sortOrder, VehicleId = vehicleId};
        }
    }

    public class VehicleSerializer : ISqlSerializer<Vehicle>
    {
        public Vehicle DeserializeGraph(IDataReader reader)
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            ISqlSerializer<Year> serYear = registry.Resolve<ISqlSerializer<Year>>();
            ISqlSerializer<Make> serMake = registry.Resolve<ISqlSerializer<Make>>();
            ISqlSerializer<Model> serModel = registry.Resolve<ISqlSerializer<Model>>();
            ISqlSerializer<Trim> serTrim = registry.Resolve<ISqlSerializer<Trim>>();
            ISqlSerializer<VehicleOption> serOpt = registry.Resolve<ISqlSerializer<VehicleOption>>();
            ISqlSerializer<Specification> serSpec = registry.Resolve<ISqlSerializer<Specification>>();

            Year year = null;

            if (reader.Read())
            {
                year = serYear.Deserialize((IDataRecord) reader);
            }

            Make make = null;

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    make = serMake.Deserialize((IDataRecord) reader);
                }
            }

            Model model = null;

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    model = serModel.Deserialize((IDataRecord) reader);
                }
            }

            Trim trim = null;

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    trim = serTrim.Deserialize((IDataRecord) reader);
                }
            }

            Vehicle vehicle = null;

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    vehicle = new Vehicle
                              {
                                  BlueBookName = reader.GetString(reader.GetOrdinal("BlueBookName")),
                                  Id = reader.GetInt32(reader.GetOrdinal("Id")),
                                  Make = make,
                                  Model = model,
                                  Name = DataRecord.GetString(reader, "Name"),
                                  Options = new List<VehicleOption>(),
                                  ShortName = DataRecord.GetString(reader, "ShortName"),
                                  SortOrder = reader.GetInt32(reader.GetOrdinal("SortOrder")),
                                  Specifications = new List<Specification>(),
                                  SubTrim = DataRecord.GetString(reader, "SubTrim"),
                                  Trim = trim,
                                  Year = year,
                                  VehicleType = (VehicleType) reader.GetInt32(reader.GetOrdinal("VehicleTypeID")),
                                  MileageGroupId = reader.GetInt32(reader.GetOrdinal("MileageGroupID"))
                              };
                }
            }

            if (reader.NextResult())
            {
                IList<VehicleOption> options = serOpt.Deserialize(reader);

                if (vehicle != null)
                {
                    vehicle.Options = options;
                }
            }

            if (reader.NextResult())
            {
                VehicleOption currentOption = null;

                VehicleOptionRelationship relationship = null;

                while (reader.Read())
                {
                    if (vehicle == null)
                    {
                        continue;
                    }

                    int optionId = reader.GetInt32(reader.GetOrdinal("OptionID"));

                    int relatedOptionId = reader.GetInt32(reader.GetOrdinal("RelatedOptionID"));

                    if (currentOption == null || optionId != currentOption.Id)
                    {
                        currentOption = vehicle.Options.FirstOrDefault(x => x.Id == optionId);

                        if (currentOption == null)
                        {
                            continue; // WTF
                        }

                        relationship =
                            new VehicleOptionRelationship
                            {
                                RelationshipType = RelationshipType(reader),
                                RelatedOptions = new List<VehicleOption>()
                            };

                        currentOption.Relationships.Add(relationship);
                    }

                    VehicleOption relatedOption = vehicle.Options.FirstOrDefault(x => x.Id == relatedOptionId);

                    if (relatedOption != null)
                    {
                        relationship.RelatedOptions.Add(relatedOption);
                    }
                }
            }

            if (reader.NextResult())
            {
                IList<Specification> specifications = serSpec.Deserialize(reader);

                if (vehicle != null)
                {
                    vehicle.Specifications = specifications;
                }
            }

            return vehicle;
        }

        public IList<Vehicle> Deserialize(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public Vehicle Deserialize(IDataRecord record)
        {
            throw new NotSupportedException();
        }

        private static VehicleOptionRelationshipType RelationshipType(IDataRecord record)
        {
            switch (record.GetInt32(record.GetOrdinal("RelationshipTypeID")))
            {
                case 15:
                    return VehicleOptionRelationshipType.RadioButtonWith;
                case 9:
                    return VehicleOptionRelationshipType.ConflictsWith;
                case 11:
                    return VehicleOptionRelationshipType.RequiresAll;
                default:
                    return VehicleOptionRelationshipType.Undefined;
            }
        }
    }

    public class PriceSerializer : ISqlSerializer<Price>
    {
        public Price DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Price> Deserialize(IDataReader reader)
        {
            List<Price> values = new List<Price>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Price Deserialize(IDataRecord record)
        {
            int priceTypeId = record.GetInt32(record.GetOrdinal("PriceTypeID"));

            decimal price = record.GetDecimal(record.GetOrdinal("Price"));

            int? regionAdjustmentTypeId = null;

            if (!record.IsDBNull(record.GetOrdinal("RegionAdjustmentTypeID")))
            {
                regionAdjustmentTypeId = record.GetInt32(record.GetOrdinal("RegionAdjustmentTypeID"));
            }

            PriceType priceType = PriceType.Undefined;

            if (Enum.IsDefined(typeof (PriceType), priceTypeId))
            {
                priceType = (PriceType) Enum.ToObject(typeof (PriceType), priceTypeId);
            }

            return new Price
                   {
                       PriceType = priceType,
                       Value = price,
                       RegionAdjustmentTypeId = regionAdjustmentTypeId
                   };
        }
    }

    public class MileageAdjustmentSerializer : ISqlSerializer<MileageAdjustment>
    {
        public MileageAdjustment DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<MileageAdjustment> Deserialize(IDataReader reader)
        {
            List<MileageAdjustment> values = new List<MileageAdjustment>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public MileageAdjustment Deserialize(IDataRecord record)
        {
            decimal adjustment = record.GetDecimal(record.GetOrdinal("Adjustment"));

            int adjustmentTypeId = record.GetInt32(record.GetOrdinal("AdjustmentTypeID"));

            int mileageMin = record.GetInt32(record.GetOrdinal("MileageMin"));

            int mileageMax = record.GetInt32(record.GetOrdinal("MileageMax"));

            return new MileageAdjustment
                   {
                       Adjustment = adjustment,
                       AdjustmentType = (AdjustmentType) adjustmentTypeId,
                       MileageMin = mileageMin,
                       MileageMax = mileageMax
                   };
        }
    }

    public class VehicleOptionAdjustmentSerializer : ISqlSerializer<VehicleOptionAdjustment>
    {
        public VehicleOptionAdjustment DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<VehicleOptionAdjustment> Deserialize(IDataReader reader)
        {
            List<VehicleOptionAdjustment> values = new List<VehicleOptionAdjustment>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public VehicleOptionAdjustment Deserialize(IDataRecord record)
        {
            int optionId = record.GetInt32(record.GetOrdinal("OptionID"));

            int priceTypeId = record.GetInt32(record.GetOrdinal("PriceTypeID"));

            decimal price = record.GetDecimal(record.GetOrdinal("Price"));

            PriceType priceType = PriceType.Undefined;

            if (Enum.IsDefined(typeof (PriceType), priceTypeId))
            {
                priceType = (PriceType) Enum.ToObject(typeof (PriceType), priceTypeId);
            }

            return new VehicleOptionAdjustment
                   {
                       Adjustment = new Price
                                    {
                                        PriceType = priceType,
                                        Value = price
                                    },
                       OptionId = optionId
                   };
        }
    }

    public class SpecificationSerializer : ISqlSerializer<Specification>
    {
        public Specification DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Specification> Deserialize(IDataReader reader)
        {
            List<Specification> values = new List<Specification>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Specification Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal("SpecificationId"));

            int specificationTypeId = record.GetInt32(record.GetOrdinal("SpecificationTypeId"));

            string name = DataRecord.GetString(record, "Name");

            string unit = DataRecord.GetString(record, "Units");

            string value = DataRecord.GetString(record, "Value");

            return new Specification
                   {
                       Id = id,
                       SpecificationType = (SpecificationType) specificationTypeId,
                       Name = name,
                       Unit = unit,
                       Value = value
                   };
        }
    }

    public class VehicleOptionSerializer : ISqlSerializer<VehicleOption>
    {
        public VehicleOption DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<VehicleOption> Deserialize(IDataReader reader)
        {
            List<VehicleOption> values = new List<VehicleOption>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public VehicleOption Deserialize(IDataRecord record)
        {
            return new VehicleOption
                   {
                       Availability = Availability(record),
                       Category = Category(record),
                       Id = record.GetInt32(record.GetOrdinal("Id")),
                       IsDefaultConfiguration = record.GetBoolean(record.GetOrdinal("IsDefaultConfiguration")),
                       Name = DataRecord.GetString(record, "Name"),
                       OptionType = OptionType(record),
                       Relationships = new List<VehicleOptionRelationship>(),
                       SortOrder = record.GetInt32(record.GetOrdinal("SortOrder"))
                   };
        }

        private static VehicleOptionAvailability Availability(IDataRecord record)
        {
            switch (record.GetInt32(record.GetOrdinal("AvailabilityID")))
            {
                case 1:
                    return VehicleOptionAvailability.Available;
                case 2:
                    return VehicleOptionAvailability.Standard;
                default:
                    return VehicleOptionAvailability.Undefined;
            }
        }

        private static VehicleOptionCategory Category(IDataRecord record)
        {
            switch (record.GetInt32(record.GetOrdinal("CategoryID")))
            {
                case 2654:
                    return VehicleOptionCategory.DriveTrain;
                case 2655:
                    return VehicleOptionCategory.Engine;
                case 2670:
                    return VehicleOptionCategory.Transmission;
                default:
                    return VehicleOptionCategory.Undefined;
            }
        }

        private static VehicleOptionType OptionType(IDataRecord record)
        {
            switch (record.GetInt32(record.GetOrdinal("OptionTypeID")))
            {
                case 4:
                    return VehicleOptionType.Equipment;
                case 5:
                    return VehicleOptionType.Option;
                case 7:
                    return VehicleOptionType.CertifiedPreOwned;
                default:
                    return VehicleOptionType.Undefined;
            }
        }
    }

    public class RegionSerializer : ISqlSerializer<Region>
    {
        public Region DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<Region> Deserialize(IDataReader reader)
        {
            List<Region> values = new List<Region>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public Region Deserialize(IDataRecord record)
        {
            return new Region
                   {
                       Id = record.GetInt32(record.GetOrdinal("RegionID")),
                       Name = record.GetString(record.GetOrdinal("RegionDisplayName")),
                       Type = (Region.RegionType) record.GetInt32(record.GetOrdinal("RegionTypeID"))
                   };
        }
    }

    public class RegionAdjustmentSerializer : ISqlSerializer<RegionAdjustment>
    {
        public RegionAdjustment DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<RegionAdjustment> Deserialize(IDataReader reader)
        {
            List<RegionAdjustment> values = new List<RegionAdjustment>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public RegionAdjustment Deserialize(IDataRecord record)
        {
            int? raTypeId = null;
            if (!record.IsDBNull(record.GetOrdinal("RegionAdjustmentTypeID")))
            {
                raTypeId = record.GetInt32(record.GetOrdinal("RegionAdjustmentTypeID"));
            }

            return new RegionAdjustment
                   {
                       RegionId = record.GetInt32(record.GetOrdinal("RegionID")),
                       Factor = record.GetDecimal(record.GetOrdinal("Adjustment")),
                       AdjustmentType = (AdjustmentType) record.GetInt32(record.GetOrdinal("AdjustmentTypeID")),
                       RegionAdjustmentTypeId = raTypeId
                   };
        }
    }

    public class FetchVehicleresultSerializer : ISqlSerializer<FetchValuationResultsDto>
    {

        public FetchValuationResultsDto DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<FetchValuationResultsDto> Deserialize(IDataReader reader)
        {
            List<FetchValuationResultsDto> values = new List<FetchValuationResultsDto>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public FetchValuationResultsDto Deserialize(IDataRecord record)
        {
            string category = DataRecord.GetString(record, "Category");
            string condition = DataRecord.GetString(record, "Condition");
            decimal adjustment = 0;
            PriceType categoryId = PriceTypeMapper.GetPriceType(category, condition);
            int ordinal = record.GetOrdinal("Adjustment");

            if (record.IsDBNull(ordinal))
            {
                adjustment = 0;
            }
            else
            {
                adjustment = Convert.ToDecimal(record.GetValue(ordinal));
            }


            return new FetchValuationResultsDto
                   {
                       Adjustment = adjustment,
                       Category = category,
                       Condition = condition,
                       ValueType = DataRecord.GetString(record,
                           "ValueType"),
                       CategoryId = categoryId
                   };

        }
    }

    public class PriceTypeSerializer : ISqlSerializer<PriceTypeData>
    {
        public PriceTypeData DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public IList<PriceTypeData> Deserialize(IDataReader reader)
        {
            List<PriceTypeData> values = new List<PriceTypeData>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord) reader));
            }

            return values;
        }

        public PriceTypeData Deserialize(IDataRecord record)
        {
            return new PriceTypeData()
                   {
                       DataLoadId = record.GetInt32(record.GetOrdinal("DataloadID")),
                       PriceTypeId = record.GetInt32(record.GetOrdinal("PriceTypeId")),
                       DisplayName = record.GetString("DisplayName"),
                   };
        }
    }

   
}