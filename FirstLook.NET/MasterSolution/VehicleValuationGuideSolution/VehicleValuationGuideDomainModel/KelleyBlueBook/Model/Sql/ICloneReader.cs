using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    /// <summary>
    /// Interface for a Kelley Blue Book clone reader.
    /// </summary>
    public interface ICloneReader
    {
        /// <summary>
        /// Execute the given command and produce a data reader as a result. Derived types may optionally persist the
        /// result sets in, for instance, an xml file.
        /// </summary>
        /// <param name="command">Database command to execute.</param>
        /// <param name="names">Names of the result sets.</param>
        /// <returns>Data reader.</returns>
        IDataReader Snapshot(IDbCommand command, params string[] names);
    }
}