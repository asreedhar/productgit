using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    /// <summary>
    /// Kelley Blue Book clone reader.
    /// </summary>
    internal class CloneReader : ICloneReader
    {
        /// <summary>
        /// Execute the given command and produce a data reader as a result.
        /// </summary>
        /// <param name="command">Database command to execute.</param>
        /// <param name="names">Names of the result sets.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Snapshot(IDbCommand command, params string[] names)
        {
            DataSet set = new DataSet();

            using (IDataReader reader = command.ExecuteReader())
            {
                foreach (string name in names)
                {
                    DataTable table = new DataTable(name);

                    table.Load(reader);

                    set.Tables.Add(table);
                }
            }

            return set.CreateDataReader();
        }
    }
}