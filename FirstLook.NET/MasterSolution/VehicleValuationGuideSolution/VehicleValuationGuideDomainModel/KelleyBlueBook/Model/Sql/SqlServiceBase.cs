﻿using System;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    /// <summary>
    /// Abstract base implementation of a Kelley Blue Book sql service.
    /// </summary>
    public abstract class SqlServiceBase : ISqlService
    {
        #region Properties

        /// <summary>
        /// Path to KBB sql file resources.
        /// </summary>
        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql.Resources.";

        /// <summary>
        /// Clone reader, for optionally persisting result sets to xml files.
        /// </summary>
        private readonly ICloneReader _clone;

        /// <summary>
        /// Name of the database to access.
        /// </summary>
        protected abstract string DatabaseName { get; }

        /// <summary>
        /// Name of the schema to access.
        /// </summary>
        protected abstract string SchemaName { get; }

        /// <summary>
        /// 'And' qualifier.
        /// </summary>
        protected abstract string And { get; }


        private const int BookOutTypeId = 1;

        #endregion

        #region Construction

        /// <summary>
        /// Initialize this sql service with the default clone reader.
        /// </summary>
        protected SqlServiceBase()
        {
            _clone = new CloneReader();
        }

        /// <summary>
        /// Initialize this sql service with the given clone reader.
        /// </summary>
        /// <param name="clone">Clone reader.</param>
        protected SqlServiceBase(ICloneReader clone)
        {
            _clone = clone;
        }

        #endregion

        #region Utility

        /// <summary>
        /// Create a command from the given resource given the current database, schema and 'and' qualifier.
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <returns>Command text.</returns>
        protected string CommandText(string resourceName)
        {
            return string.Format(Database.GetCommandText(GetType().Assembly, Prefix + resourceName),
                                 SchemaName,
                                 And);
        }

        #endregion

        #region Sql Commands

        /// <summary>
        /// Get the current book date.
        /// </summary>
        /// <returns>Data reader with the current book date.</returns>
        public IDataReader BookDate()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.BookDate#Fetch";

                    return _clone.Snapshot(command, "BookDate");
                }
            }
        }

        /// <summary>
        /// Get the list of years.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <returns>Data reader with years.</returns>
        public IDataReader Query(int dataLoadId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Years.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);

                    return _clone.Snapshot(command, "Year");
                }
            }
        }

        /// <summary>
        /// Get the list of makes.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="year">Year.</param>
        /// <returns>Data reader with makes.</returns>
        public IDataReader Query(int dataLoadId, int year)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Makes.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    return _clone.Snapshot(command, "Make");
                }
            }
        }

        /// <summary>
        /// Get the list of models.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>Data reader with models.</returns>
        public IDataReader Query(int dataLoadId, int year, int make)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Models.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "Year", year, DbType.Int32);
                    Database.AddWithValue(command, "MakeID", make, DbType.Int32);

                    return _clone.Snapshot(command, "Model");
                }
            }
        }

        /// <summary>
        /// Get the list of styles.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>Data reader with styles.</returns>
        public IDataReader Query(int dataLoadId, int year, int make, int model)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Styles.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "Year", year, DbType.Int32);
                    Database.AddWithValue(command, "ModelID", model, DbType.Int32);

                    return _clone.Snapshot(command, "Trim");
                }
            }
        }

        /// <summary>
        /// Get the vehicle identifier for the vehicle with the given vin.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader with vehicle ids.</returns>
        public IDataReader Query(int dataLoadId, string vin)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Vin.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "VIN", vin, DbType.String);

                    return _clone.Snapshot(command, "VIN");
                }
            }
        }

        /// <summary>
        /// Get the vehicle with the given identifier.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with vehicle details.</returns>
        public IDataReader Vehicle(int dataLoadId, int vehicle)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.VehicleDetails#Fetch";

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);

                    return _clone.Snapshot(
                        command,
                        new[]
                            {
                                "Year", "Make", "Model", "Trim",
                                "Vehicle",
                                "VehicleOption",
                                "VehicleOptionRelationship",
                                "Specification"
                            });
                }
            }
        }
        
        /// <summary>
        /// Get the default options for the vehicle with the given identifier and vin.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader with default options.</returns>
        public IDataReader DefaultOptions(int dataLoadId, int vehicle, string vin)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_DefaultOptions.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "VIN", vin, DbType.String);
                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);

                    return _clone.Snapshot(command, "DefaultOption");
                }
            }
        }

        public IDataReader Region(int dataLoadId, string zipCode)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.Region#Fetch";

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "ZipCode", zipCode, DbType.String);    // TODO: Update sql

                    return _clone.Snapshot(command, "Region");
                }
            }
        }

        public IDataReader Regions(int dataLoadId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Regions.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);

                    return _clone.Snapshot(command, "Regions");
                }
            }
        }

        /// <summary>
        /// Get the base prices of the vehicle with the given identifier in the given region.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="region">Region.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with base prices.</returns>
        public IDataReader BasePrices(int dataLoadId, Region region, int vehicle)
        {
            if (region == null)
            {
                throw new ArgumentNullException("region");
            }

            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_BasePrices.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "RegionID", region.Id, DbType.Int32);
                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);

                    return _clone.Snapshot(command, "BasePrice");
                }
            }
        }


        public IDataReader BasePrices(int dataLoadId, Region region, int vehicle, int? mileage, int dealerId, int year, int bookOutTypeId)
        {
            if (region == null)
            {
                throw new ArgumentNullException("region");
            }

            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.GetValues";

                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);
                    Database.AddWithValue(command, "Year", year, DbType.Int32);
                    Database.AddWithValue(command, "Mileage", mileage, DbType.Int32);
                    Database.AddWithValue(command, "State", "", DbType.String);
                    Database.AddWithValue(command, "BusinessUnitID", dealerId, DbType.Int32);
                    Database.AddWithValue(command, "BookoutTypeID", bookOutTypeId, DbType.Int32);

                    return _clone.Snapshot(command, "BasePrice");
                }
            }
        }

        /// <summary>
        /// Get the option adjustments for the vehicle with the given identifier in the given region.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with option adjustments.</returns>
        public IDataReader OptionAdjustments(int dataLoadId, int vehicle)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Vehicle.Kbb.OptionsAdjustment";

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);

                    return _clone.Snapshot(command, "OptionAdjustment");
                }
            }
        }

        /// <summary>
        /// Get the mileage adjustments for vehicles with the given type and year and in the given mileage group.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicleType">Vehicle type.</param>
        /// <param name="yearId">Year.</param>
        /// <param name="mileageGroupId">Mileage group identifier.</param>
        /// <returns>Data reader with mileage adjustments.</returns>
        public IDataReader MileageAdjustment(int dataLoadId, VehicleType vehicleType, int yearId, int mileageGroupId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_MileageAdjustments.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "YearID", yearId, DbType.Int32);
                    Database.AddWithValue(command, "VehicleTypeID", vehicleType, DbType.Int32);
                    Database.AddWithValue(command, "MileageGroupID", mileageGroupId, DbType.Int32);

                    return _clone.Snapshot(command, "MileageAdjustment");
                }
            }
        }

        public IDataReader RegionAdjustments(int dataLoadId, int vehicle)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_RegionAdjustments.txt");

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);

                    return _clone.Snapshot(command, "RegionAdjustment");
                }
            }
        }

        public IDataReader PriceType(int dataLoadId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.PriceType#Fetch";

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);

                    return _clone.Snapshot(command, "PriceTypes");
                }
            }
        }

        #region VINSpecificOptions
        /// <summary>
        /// Uses GetOptions SP to Return KBB options
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="vin"></param>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        public IDataReader GetOptions(int vehicleId, string vin, int businessUnitId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.GetOptions";

                    Database.AddWithValue(command, "VIN", vin, DbType.String);
                    Database.AddWithValue(command, "VehicleID", vehicleId, DbType.Int32);
                    Database.AddWithValue(command, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddWithValue(command, "BookoutTypeID", BookOutTypeId, DbType.Int32);

                    return _clone.Snapshot(command, "Options");
                }
            }
        }

        public IDataReader Vehicle(int dataLoadId, int vehicle, string vin, int businessUnitId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "KBB.VehicleDetails#Fetch";

                    Database.AddWithValue(command, "DataLoadID", dataLoadId, DbType.Int32);
                    Database.AddWithValue(command, "VehicleID", vehicle, DbType.Int32);
                    Database.AddWithValue(command, "Vin", vin, DbType.String);
                    Database.AddWithValue(command, "BusinessUnitId", businessUnitId, DbType.Int32);

                    return _clone.Snapshot(
                        command,
                        new[]
                            {
                                "Year", "Make", "Model", "Trim",
                                "Vehicle",
                                "VehicleOption",
                                "VehicleOptionRelationship",
                                "Specification"
                            });
                }
            }
        }

        #endregion
        #endregion

       
    }
}
