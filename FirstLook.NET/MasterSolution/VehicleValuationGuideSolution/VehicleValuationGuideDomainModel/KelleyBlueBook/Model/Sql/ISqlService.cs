﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql 
{
    /// <summary>
    /// Interface for a Kelley Blue Book sql service.
    /// </summary>
    public interface ISqlService
    {
        /// <summary>
        /// Get the current book date.
        /// </summary>
        /// <returns>Data reader with the current book date.</returns>
        IDataReader BookDate();

        /// <summary>
        /// Get the list of years.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <returns>Data reader with years.</returns>
        IDataReader Query(int dataLoadId);

        /// <summary>
        /// Get the list of makes.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="year">Year.</param>
        /// <returns>Data reader with makes.</returns>
        IDataReader Query(int dataLoadId, int year);

        /// <summary>
        /// Get the list of models.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>Data reader with models.</returns>
        IDataReader Query(int dataLoadId, int year, int make);

        /// <summary>
        /// Get the list of styles.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>Data reader with styles.</returns>
        IDataReader Query(int dataLoadId, int year, int make, int model);

        /// <summary>
        /// Get the vehicle identifier for the vehicle with the given vin.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader with vehicle ids.</returns>
        IDataReader Query(int dataLoadId, string vin);

        /// <summary>
        /// Get the vehicle with the given identifier.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with vehicle details.</returns>
        IDataReader Vehicle(int dataLoadId, int vehicle);
        
        /// <summary>
        /// Get the default options for the vehicle with the given identifier and vin.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader with default options.</returns>
        IDataReader DefaultOptions(int dataLoadId, int vehicle, string vin);
        
        /// <summary>
        /// Get the region identifier for the given zipcode
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="zipCode">Zip</param>
        /// <returns>Data reader with a region identifier.</returns>
        IDataReader Region(int dataLoadId, string zipCode);

        /// <summary>
        /// Get a list of region identifiers
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <returns>Data reader with a region identifier.</returns>
        IDataReader Regions(int dataLoadId);

        /// <summary>
        /// Get the base prices of the vehicle with the given identifier in the given region.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="region">Region.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with base prices.</returns>
        IDataReader BasePrices(int dataLoadId, Region region, int vehicle);


        /// <summary>
        /// Get the base prices of the vehicle with the given identifier in the given region.(stored procedure)
        /// </summary>
        /// <param name="dataLoadId">Book Date</param>
        /// <param name="region">Region</param>
        /// <param name="vehicle">Vehicle identifier</param>
        /// <param name="mileage">Mileage</param>
        /// <param name="dealerId">BusinessUnitId</param>
        /// <param name="year">Year</param>
        /// <param name="bookoutTypeId">BookOutTypeId</param>
        /// <returns></returns>
        IDataReader BasePrices(int dataLoadId, Region region, int vehicle, int? mileage, int dealerId, int year, int bookoutTypeId);


        /// <summary>
        /// Get the option adjustments for the vehicle with the given identifier in the given region.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with option adjustments.</returns>
        IDataReader OptionAdjustments(int dataLoadId, int vehicle);

        /// <summary>
        /// Get the mileage adjustments for vehicles with the given type and year and in the given mileage group.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicleType">Vehicle type.</param>
        /// <param name="yearId">Year.</param>
        /// <param name="mileageGroupId">Mileage group identifier.</param>
        /// <returns>Data reader with mileage adjustments.</returns>
        IDataReader MileageAdjustment(int dataLoadId, VehicleType vehicleType, int yearId, int mileageGroupId);

        /// <summary>
        /// Gets the regional adjustment factors for the vehicle with the given identifier.
        /// </summary>
        /// <param name="dataLoadId">Book date.</param>
        /// <param name="vehicle">Vehicle identifier.</param>
        /// <returns>Data reader with region adjustment factors.</returns>
        IDataReader RegionAdjustments(int dataLoadId, int vehicle);

        /// <summary>
        /// Get Price type from KBB.KBB.PriceType
        /// </summary>
        /// <param name="dataLoadId"></param>
        /// <returns></returns>
        IDataReader PriceType(int dataLoadId);


        #region VINSpecificOptions
            IDataReader GetOptions(int vehicleId, string vin, int businessUnitId);
        #endregion
    }
}
