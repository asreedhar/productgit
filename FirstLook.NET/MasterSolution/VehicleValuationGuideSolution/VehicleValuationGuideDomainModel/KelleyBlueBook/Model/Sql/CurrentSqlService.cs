﻿namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql
{
    /// <summary>
    /// Current sql service. Reads and writes Kelley Blue Book data to the database where it's previously been written.
    /// </summary>
    public class CurrentSqlService : SqlServiceBase
    {
        /// <summary>
        /// Create a sql service with the default clone reader.
        /// </summary>
        public CurrentSqlService()
        {
        }

        /// <summary>
        /// Create a sql service with the given clone reader.
        /// </summary>
        /// <param name="clone">Clone reader.</param>
        public CurrentSqlService(ICloneReader clone) : base(clone)
        {
        }

        /// <summary>
        /// Database where the current KBB data is stored.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "KelleyBlueBook"; }
        }

        /// <summary>
        /// Schema of where the current KBB data is stored.
        /// </summary>
        protected override string SchemaName
        {
            get { return "KBB"; }
        }

        /// <summary>
        /// 'And' qualifier in the current KBB data.
        /// </summary>
        protected override string And
        {
            get { return "--"; }
        }
    }
}
