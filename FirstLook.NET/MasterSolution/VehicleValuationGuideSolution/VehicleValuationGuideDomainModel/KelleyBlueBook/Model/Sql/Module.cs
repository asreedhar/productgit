﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql 
{
    public class Module : IModule
    {
        internal class SerializerModule : IModule
        {
            public void Configure(IRegistry registry)
            {
                registry.Register<ISqlSerializer<BookDate>, BookDateSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Year>, YearSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Make>, MakeSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Model>, ModelSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Trim>, TrimSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Vehicle>, VehicleSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Price>, PriceSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<MileageAdjustment>, MileageAdjustmentSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<VehicleOptionAdjustment>, VehicleOptionAdjustmentSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<VehicleOption>, VehicleOptionSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Specification>, SpecificationSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Region>, RegionSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<RegionAdjustment>, RegionAdjustmentSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<FetchValuationResultsDto>, FetchVehicleresultSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<PriceTypeData>, PriceTypeSerializer>(ImplementationScope.Shared);
            }
        }

        public void Configure(IRegistry registry)
        {
            registry.Register<ISqlService, CurrentSqlService>(ImplementationScope.Shared);

            registry.Register<SerializerModule>();
        }
    }
}
