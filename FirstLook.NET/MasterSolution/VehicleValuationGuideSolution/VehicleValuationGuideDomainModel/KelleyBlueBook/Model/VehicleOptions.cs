﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class VehicleOptions 
    {
        public dynamic VehicleOpionId { get; set; }

        public dynamic CategoryId { get; set; }

        public bool IsDefault { get; set; }

        public bool Status { get; set; }

        public string Description { get; set; }

        public int SortOrder { get; set; }
        
    }
}
