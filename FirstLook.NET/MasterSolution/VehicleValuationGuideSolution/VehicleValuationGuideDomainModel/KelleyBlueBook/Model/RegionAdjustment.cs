﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class RegionAdjustment
    {
        public int RegionId { get; internal set; }

        public AdjustmentType AdjustmentType { get; internal set; }

        public decimal Factor { get; internal set; }

        public int? RegionAdjustmentTypeId { get; internal set; }
    }
}
