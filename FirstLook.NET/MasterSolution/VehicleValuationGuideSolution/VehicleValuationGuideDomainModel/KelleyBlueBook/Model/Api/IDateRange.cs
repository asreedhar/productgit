using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public interface IDateRange
    {
        bool Covers(DateTime date);

        DateTime BeginDate { get; }

        DateTime EndDate { get; }
    }
}