using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Vehicle configuration builder.
    /// </summary>
    public interface IVehicleConfigurationBuilder
    {       
        /// <summary>
        /// Region (US State equivalent)
        /// </summary>
        Region Region { get; set; }

        /// <summary>
        /// Reference data identifier.
        /// </summary>
        int VehicleId { get; set; }

        /// <summary>
        /// Reference data version identifier.
        /// </summary>
        BookDate BookDate { get; set; }

        /// <summary>
        /// Vin.
        /// </summary>
        string Vin { get; set; }

        /// <summary>
        /// Mileage.
        /// </summary>
        int? Mileage { get; set; }

        /// <summary>
        /// Is the engine missing?
        /// </summary>
        bool IsMissingEngine { get; set; }

        /// <summary>
        /// Is the drive train missing?
        /// </summary>
        bool IsMissingDriveTrain { get; set; }

        /// <summary>
        /// Is the transmission missing?
        /// </summary>
        bool IsMissingTransmission { get; set; }

        /// <summary>
        /// Add an option to the configuration.
        /// </summary>
        /// <param name="option">Option to add.</param>
        /// <returns>State of the option.</returns>
        IOptionState Add(VehicleOption option);

        /// <summary>
        /// Add an option by it's identifier.
        /// </summary>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="selected">Is the option selected?</param>
        /// <returns>State of the option.</returns>
        IOptionState Add(int optionId, bool selected);

        /// <summary>
        /// Select an option by its identifier.
        /// </summary>
        /// <param name="optionId"></param>
        void Select(int optionId);

        /// <summary>
        /// Deselect an option by its identifier.
        /// </summary>
        /// <param name="optionId"></param>
        void Deselect(int optionId);

        /// <summary>
        /// Get the identifiers of the selected options.
        /// </summary>
        /// <returns>List of option identifiers.</returns>
        IEnumerable<int> Selected();

        /// <summary>
        /// Is an option selected?
        /// </summary>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="returnValue">Return value.</param>
        /// <returns>If the option is selected or not.</returns>
        bool IsSelected(int optionId, bool returnValue);

        /// <summary>
        /// Perform an action for an option.
        /// </summary>
        /// <param name="optionId">Option identifier.</param>
        /// <param name="actionType">Type of action to perform.</param>
        /// <returns>Option action being performed.</returns>
        IOptionAction Do(int optionId, OptionActionType actionType);

        /// <summary>
        /// Remove an option.
        /// </summary>
        /// <param name="optionId">Option identifier.</param>
        /// <returns>Option action being performed.</returns>
        IOptionAction Undo(int optionId);

        /// <summary>
        /// Get the vehicle configuration from this builder.
        /// </summary>
        /// <returns>Vehicle configuration.</returns>
        IVehicleConfiguration ToVehicleConfiguration();
    }
}