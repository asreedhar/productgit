﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IKbbConfiguration,KbbConfiguration>(ImplementationScope.Shared);

            registry.Register<IKbbService, KbbService>(ImplementationScope.Shared);

            registry.Register<IKbbTraversal, KbbTraversal>(ImplementationScope.Shared);

            registry.Register<IKbbCalculator, KbbCalculator>(ImplementationScope.Shared);
        }
    }
}
