using System.Collections.Generic;
using System.Data;
using System.Drawing.Printing;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;


namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public delegate int KbbBookDate();

    public class KbbService : IKbbService
    {
        private readonly IResolver _resolver;

        private readonly KbbBookDate _bookDate;

        public KbbService()
        {
            _resolver = RegistryFactory.GetResolver();
            _bookDate = () => _resolver.Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);
        }

        public KbbService(IResolver resolver, KbbBookDate bookDate)
        {
            _resolver = resolver;
            _bookDate = bookDate;
        }
        
        public BookDate BookDate()
        {
            ILookupBookDate lookup = Resolve<ILookupBookDate>();

            IList<BookDate> dates = lookup.Lookup();

            return dates.First(x => x.Id == dates.Max(y => y.Id));
        }

        public IList<Region> Regions()
        {
            ILookupRegion region = Resolve<ILookupRegion>();

            return region.Lookup(_resolver, _bookDate());
        }

        public Region ToRegion(string zipCode)
        {
            ILookupRegion lookup = Resolve<ILookupRegion>();

            return lookup.Lookup(_resolver, _bookDate(), zipCode);
        }

        public IList<VehicleOption> DefaultOptions(int vehicleId, string vin)
        {
            ILookupVinOptions lookup = Resolve<ILookupVinOptions>();

            IList<int> optionIds = lookup.Lookup(_bookDate(), vehicleId, vin);

            IList<VehicleOption> options = new List<VehicleOption>();

            Vehicle vehicle = Vehicle(vehicleId);

            foreach (int optionId in optionIds)
            {
                foreach (VehicleOption option in vehicle.Options)
                {
                    if (option.Id == optionId)
                    {
                        options.Add(option);
                    }
                }
            }

            return options;
        }

        public Vehicle Vehicle(int vehicleId)
        {
            ILookupVehicle lookup = Resolve<ILookupVehicle>();

            return lookup.Lookup(_bookDate(), vehicleId);
        }

        public Vehicle Vehicle(int vehicleId,string vin)
        {
            ILookupVehicle lookup = Resolve<ILookupVehicle>();

            return lookup.Lookup(_bookDate(), vehicleId);
        }

        public IList<VehicleOptionAdjustment> OptionAdjustments(int vehicle)
        {
            ILookupOptionAdjustment lookup = Resolve<ILookupOptionAdjustment>();

            return lookup.Lookup(_bookDate(), vehicle);
        }

        public IList<PriceTypeData> PriceTypeData(int dataLoadId)
        {
            ILookupPriceType lookup = Resolve<ILookupPriceType>();

            IList<PriceTypeData> priceTypes = lookup.Lookup(dataLoadId);

            return priceTypes;
        } 

        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        #region VinSpecificOptions


        public IList<VehicleOptions> GetOptions(int vehicleId, string vin, int businessUnitId)
        {
            ILookupVinOptions lookup = Resolve<ILookupVinOptions>();
            return lookup.Lookup(vehicleId, vin, businessUnitId);
        }

        #endregion
    }
}
