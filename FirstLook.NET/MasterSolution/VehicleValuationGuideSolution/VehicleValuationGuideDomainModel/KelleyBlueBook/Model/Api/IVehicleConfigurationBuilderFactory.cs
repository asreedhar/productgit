namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Interface to a factory for vehicle configuration builders.
    /// </summary>
    public interface IVehicleConfigurationBuilderFactory
    {
        /// <summary>
        /// Create a new vehicle configuration builder.
        /// </summary>
        /// <returns>New builder.</returns>
        IVehicleConfigurationBuilder NewBuilder();

        /// <summary>
        /// Create a new vehicle configuration builder for the given configuration.
        /// </summary>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <returns>New builder.</returns>
        IVehicleConfigurationBuilder NewBuilder(IVehicleConfiguration configuration);
    }
}