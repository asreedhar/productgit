﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Interface for a Kelley Blue Book data repository.
    /// </summary>
    public interface IKbbRepository
    {
        /// <summary>
        /// Load info about the publications tied to the given broker and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <returns>List of publication details.</returns>
        IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle);

        /// <summary>
        /// Load the publication for the given broker and vehicle that is valid at the given time.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="on">Date on which the publication should be valid.</param>
        /// <returns>Publication.</returns>
        IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on);

        /// <summary>
        /// Load the publication for the given broker and vehicle that has the given identifier.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="id">Publication identifier (a.k.a. vehicle configuration history identifier).</param>
        /// <returns>Publication.</returns>
        IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id);

        /// <summary>
        /// Save the configuration and price details for the given client and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">Principal.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="matrix">Price details.</param>
        /// <returns>Publication that was saved.</returns>
        IEdition<IPublication> Save(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, 
                                    IVehicleConfiguration configuration, Matrix matrix); 
    }
}
