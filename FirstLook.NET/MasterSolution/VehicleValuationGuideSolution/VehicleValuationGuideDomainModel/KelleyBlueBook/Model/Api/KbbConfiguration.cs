﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Kelley Blue Book configuration actions.
    /// </summary>
    public class KbbConfiguration : IKbbConfiguration
    {
        /// <summary>
        /// Get the initial configuration of the vehicle with the given attributes.
        /// </summary>
        /// <param name="vehicleId">Reference data identifier.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="region">Region (equivalent to state)</param>
        /// <param name="zipCode">Zip</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Vehicle configuration.</returns>
        public IVehicleConfiguration InitialConfiguration(int vehicleId, string vin, Region region, string zipCode, int? mileage)
        {
            IKbbService service = Resolve<IKbbService>();
            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();
            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            if ((region == null || region.Id < 1) && !string.IsNullOrEmpty(zipCode))
            {
                region = service.ToRegion(zipCode);
            }

            if (region == null || region.Id < 1) throw new ArgumentException("Region/ZipCode parameter missing", "region");


            builder.BookDate = service.BookDate();
            builder.VehicleId = vehicleId;
            builder.Vin = vin;
            builder.Region = region;
            builder.Mileage = mileage;

            Vehicle vehicle = service.Vehicle(vehicleId);

            InitializeConfiguration(service, builder, vehicle);

            return builder.ToVehicleConfiguration();
        }

        /// <summary>
        /// Initialize a vehicle configuration's options.
        /// </summary>
        /// <param name="service">Kelley Blue Book service.</param>
        /// <param name="builder">Configuration builder.</param>
        /// <param name="vehicle">Vehicle.</param>
        private static void InitializeConfiguration(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle)
        {
            foreach (VehicleOption option in vehicle.Options)
            {
                builder.Add(option);
            }

            if (!string.IsNullOrEmpty(builder.Vin))
            {
                IList<VehicleOption> options = service.DefaultOptions(builder.VehicleId, builder.Vin);

                foreach (VehicleOption option in options)
                {
                    AddOption(service, builder, vehicle, option);
                }
            }
        }

        /// <summary>
        /// Update a vehicle configuration.
        /// </summary>
        /// <param name="service">Kelley Blue Book service.</param>
        /// <param name="configuration">Configuration to update.</param>
        /// <param name="action">Action to perform on an option in the configuration.</param>
        /// <returns>Vehicle configuration.</returns>
        public IVehicleConfiguration UpdateConfiguration(IKbbService service, IVehicleConfiguration configuration, IOptionAction action)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (action.ActionType == OptionActionType.Undefined)
            {
                throw new ArgumentOutOfRangeException("action", OptionActionType.Undefined, "Action type cannot be undefined");
            }

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            bool repeat = configuration.OptionActions.Any(
                x => x.ActionType == action.ActionType &&
                     x.OptionId == action.OptionId);

            if (repeat)
            {
                return configuration; // been there, done that ...
            }

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder;

            bool undo = configuration.OptionActions.Any(
                x => x.ActionType != action.ActionType &&
                     x.OptionId == action.OptionId);

            if (undo)
            {
                builder = factory.NewBuilder();

                builder.BookDate = service.BookDate();
                builder.VehicleId = configuration.VehicleId;
                builder.Vin = configuration.Vin;
                builder.Region = configuration.Region;
                builder.Mileage = configuration.Mileage;

                builder.IsMissingDriveTrain = configuration.IsMissingDriveTrain;
                builder.IsMissingEngine = configuration.IsMissingEngine;
                builder.IsMissingTransmission = configuration.IsMissingTransmission;

                InitializeConfiguration(service, builder, vehicle);

                foreach (IOptionAction replay in configuration.OptionActions)
                {
                    if (replay.OptionId != action.OptionId)
                    {
                        PerformAction(service, builder, replay, vehicle);
                    }
                }
            }
            else
            {
                builder = factory.NewBuilder(configuration);
                PerformAction(service, builder, action, vehicle);
            }

            return builder.ToVehicleConfiguration();
        }
        
        /// <summary>
        /// Perform an option action on a configuration.
        /// </summary>
        /// <param name="service">Kelley Blue Book service.</param>
        /// <param name="builder">Configuration builder.</param>
        /// <param name="action">Option action.</param>
        /// <param name="vehicle">Vehicle.</param>
        static void PerformAction(IKbbService service, IVehicleConfigurationBuilder builder, IOptionAction action, Vehicle vehicle)
        {
            VehicleOption option = vehicle.Options.FirstOrDefault(x => x.Id == action.OptionId);

            if (option == null)
            {
                throw new ArgumentException(string.Format("No such option {0}", action.OptionId), "action");
            }

            builder.Do(action.OptionId, action.ActionType);

            switch (action.ActionType)
            {
                case OptionActionType.Undefined:
                    throw new ArgumentException("Undefined OptionActionType", "action");
                case OptionActionType.Add:
                    AddOption(service, builder, vehicle, option);
                    break;
                case OptionActionType.Remove:
                    RemoveOption(service, builder, vehicle, option, false);
                    break;
            }
        }
        
        static void AddOption(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOption option)
        {
            if (builder.IsSelected(option.Id, true))
            {
                return;
            }

            builder.Select(option.Id);

            if (option.Category != VehicleOptionCategory.Undefined)
            {
                ImplicitRelationship(builder, vehicle, option);

                UpdateMissingEquipment(builder, option.Category, false);
            }

            foreach (VehicleOptionRelationship relationship in option.Relationships)
            {
                switch (relationship.RelationshipType)
                {
                    case VehicleOptionRelationshipType.Undefined:
                        throw new NotSupportedException("VehicleOptionRelationshipType.Undefined");
                    case VehicleOptionRelationshipType.ConflictsWith:
                        ConflictsWith(service, builder, vehicle, relationship);
                        break;
                    case VehicleOptionRelationshipType.RadioButtonWith:
                        RadioButtonWith(service, builder, vehicle, relationship);
                        break;
                    case VehicleOptionRelationshipType.RequiresAll:
                        RequiresAll(service, builder, vehicle, relationship);
                        break;
                }
            }
        }

        static void RemoveOption(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOption option, bool updateActions)
        {
            if (updateActions)
            {
                builder.Undo(option.Id);
            }

            if (!builder.IsSelected(option.Id, false))
            {
                return;
            }

            builder.Deselect(option.Id);

            if (option.Category != VehicleOptionCategory.Undefined)
            {
                UpdateMissingEquipment(builder, option.Category, true);
            }

            foreach (VehicleOptionRelationship relationship in option.Relationships)
            {
                switch (relationship.RelationshipType)
                {
                    case VehicleOptionRelationshipType.Undefined:
                        throw new NotSupportedException("VehicleOptionRelationshipType.Undefined");
                    case VehicleOptionRelationshipType.ConflictsWith:
                        // no-op: no effect
                        break;
                    case VehicleOptionRelationshipType.RadioButtonWith:
                        // no-op: specification does not mandate one be selected
                        break;
                    case VehicleOptionRelationshipType.RequiresAll:
                        foreach (VehicleOption relatedOption in relationship.RelatedOptions)
                        {
                            RemoveOption(service, builder, vehicle, relatedOption, true);
                        }
                        break;
                }
            }

            RemoveDependentRequiresAll(service, builder, vehicle, option);
        }

        private static void ImplicitRelationship(IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOption actionOption)
        {
            foreach (VehicleOption option in vehicle.Options)
            {
                if (option.Id == actionOption.Id)
                {
                    continue;
                }

                if (option.Category == actionOption.Category)
                {
                    builder.Deselect(option.Id);
                }
            }
        }

        static void UpdateMissingEquipment(IVehicleConfigurationBuilder builder, VehicleOptionCategory category, bool missing)
        {
            switch (category)
            {
                case VehicleOptionCategory.Engine:
                    builder.IsMissingEngine = missing;
                    break;
                case VehicleOptionCategory.DriveTrain:
                    builder.IsMissingDriveTrain = missing;
                    break;
                case VehicleOptionCategory.Transmission:
                    builder.IsMissingTransmission = missing;
                    break;
            }
        }

        static void ConflictsWith(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOptionRelationship relationship)
        {
            foreach (VehicleOption relatedOption in relationship.RelatedOptions)
            {
                RemoveOption(service, builder, vehicle, relatedOption, true);
            }
        }

        static void RadioButtonWith(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOptionRelationship relationship)
        {
            foreach (VehicleOption relatedOption in relationship.RelatedOptions)
            {
                RemoveOption(service, builder, vehicle, relatedOption, true);
            }
        }

        static void RequiresAll(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOptionRelationship relationship)
        {
            foreach (VehicleOption relatedOption in relationship.RelatedOptions)
            {
                AddOption(service, builder, vehicle, relatedOption);
            }
        }

        /// <summary>
        /// When you remove an option, there may be another option that caused its selection through a
        /// requires all relationship.  When this happens, that option (the one with the requires all)
        /// must be de-selected.  That is what this method does.
        /// </summary>
        static void RemoveDependentRequiresAll(IKbbService service, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOption option)
        {
            IEnumerable<int> selectedOptionIds = builder.Selected();

            foreach (int selectedOptionId in selectedOptionIds)
            {
                int id = selectedOptionId;

                VehicleOption selectedOption = vehicle.Options.First(x => x.Id == id);

                if (selectedOption == null) continue;

                IEnumerable<VehicleOptionRelationship> relationships = selectedOption.Relationships.Where(
                    x => x.RelationshipType == VehicleOptionRelationshipType.RequiresAll &&
                         x.RelatedOptions.Any(y => y.Id == option.Id));

                foreach (VehicleOptionRelationship relationship in relationships)
                {
                    RemoveOption(service, builder, vehicle, selectedOption, true);

                    foreach (VehicleOption relatedOption in relationship.RelatedOptions)
                    {
                        RemoveOption(service, builder, vehicle, relatedOption, true);
                    }
                }
            }
        }

        /// <summary>
        /// Resolve an implementation of type T from the registry.
        /// </summary>
        /// <typeparam name="T">Type to get the implementation for.</typeparam>
        /// <returns>Implementation.</returns>
        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }


        #region CalulationOnEquipmentSelected
        /// <summary>
        /// Update a vehicle configuration.
        /// </summary>
        /// <param name="service">Kelley Blue Book service.</param>
        /// <param name="selectedEquipmentsList">Selected Equipment list to update</param>
        /// <param name="action">Action to perform on an option in the equipment list.</param>

        public void UpdateConfiguration(IKbbService service, IVehicleConfiguration configuration, IList<string> selectedEquipmentsList, IOptionAction action)
        {
            if (selectedEquipmentsList == null)
            {
                throw new ArgumentNullException("Equipement Selected");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (action.ActionType == OptionActionType.Undefined)
            {
                throw new ArgumentOutOfRangeException("action", OptionActionType.Undefined, "Action type cannot be undefined");
            }

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            bool repeat = configuration.OptionActions.Any(
                x => x.ActionType == action.ActionType &&
                     x.OptionId == action.OptionId);

            if (repeat)
            {
                return; // been there, done that ...
            }

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder;
            builder = factory.NewBuilder(configuration);

            PerformAction(selectedEquipmentsList, builder, action, vehicle, vehicle.Options.FirstOrDefault(vo => vo.Id == action.OptionId));
        }

        /// <summary>
        /// Perform an option action on a configuration.
        /// </summary>
        /// <param name="action">Option action.</param>
        /// <param name="vehicle">Vehicle.</param>
        static void PerformAction(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, IOptionAction action, Vehicle vehicle, VehicleOption vehicleOption)
        {
            VehicleOption vehicleOptions = vehicle.Options.FirstOrDefault(x => x.Id == action.OptionId);

            if (vehicleOptions == null)
            {
                throw new ArgumentException(string.Format("No such option {0}", action.OptionId), "action");
            }

            switch (action.ActionType)
            {
                case OptionActionType.Undefined:
                    throw new ArgumentException("Undefined OptionActionType", "action");
                case OptionActionType.Add:
                    AddOption(equipmentSelectedList, builder, vehicle, action.OptionId.ToString(), vehicleOption);
                    break;
                case OptionActionType.Remove:
                    RemoveOption(equipmentSelectedList, builder, vehicle, action.OptionId.ToString(), vehicleOption, true);
                    break;
            }
        }

        static void AddOption(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, Vehicle vehicle, string option, VehicleOption vehicleOption)
        {
            if (!equipmentSelectedList.Contains(option))
            {
                equipmentSelectedList.Add(option);
            }

            if (vehicleOption.Category != VehicleOptionCategory.Undefined)
            {
                ImplicitRelationship(builder, vehicle, vehicleOption);

                UpdateMissingEquipment(builder, vehicleOption.Category, false);
            }

            foreach (VehicleOptionRelationship relationship in vehicleOption.Relationships)
            {
                switch (relationship.RelationshipType)
                {
                    case VehicleOptionRelationshipType.Undefined:
                        throw new NotSupportedException("VehicleOptionRelationshipType.Undefined");
                    case VehicleOptionRelationshipType.ConflictsWith:
                        ConflictsWith(equipmentSelectedList, builder, vehicle, relationship);
                        break;
                    case VehicleOptionRelationshipType.RadioButtonWith:
                        RadioButtonWith(equipmentSelectedList, builder, vehicle, relationship);
                        break;
                    case VehicleOptionRelationshipType.RequiresAll:
                        RequiresAll(equipmentSelectedList, builder, vehicle, relationship);
                        break;
                }
            }
        }

        static void ConflictsWith(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOptionRelationship relationship)
        {
            foreach (VehicleOption relatedOption in relationship.RelatedOptions)
            {
                RemoveOption(equipmentSelectedList, builder, vehicle, Convert.ToString(relatedOption.Id), relatedOption, true);
            }
        }

        static void RadioButtonWith(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOptionRelationship relationship)
        {
            foreach (VehicleOption relatedOption in relationship.RelatedOptions)
            {
                RemoveOption(equipmentSelectedList, builder, vehicle, Convert.ToString(relatedOption.Id), relatedOption, true);
            }
        }

        static void RequiresAll(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOptionRelationship relationship)
        {
            foreach (VehicleOption relatedOption in relationship.RelatedOptions)
            {
                AddOption(equipmentSelectedList, builder, vehicle, Convert.ToString(relatedOption.Id), relatedOption);
            }
        }

        static void RemoveOption(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, Vehicle vehicle, string option, VehicleOption vehicleOption, bool updateActions)
        {
            equipmentSelectedList.Remove(option);

            if (updateActions)
            {
                builder.Undo(Convert.ToInt32(option));
            }

            if (vehicleOption.Category != VehicleOptionCategory.Undefined)
            {
                UpdateMissingEquipment(builder, vehicleOption.Category, true);
            }

            foreach (VehicleOptionRelationship relationship in vehicleOption.Relationships)
            {
                switch (relationship.RelationshipType)
                {
                    case VehicleOptionRelationshipType.Undefined:
                        throw new NotSupportedException("VehicleOptionRelationshipType.Undefined");
                    case VehicleOptionRelationshipType.ConflictsWith:
                        // no-op: no effect
                        break;
                    case VehicleOptionRelationshipType.RadioButtonWith:
                        // no-op: specification does not mandate one be selected
                        break;
                    case VehicleOptionRelationshipType.RequiresAll:
                        foreach (VehicleOption relatedOption in relationship.RelatedOptions)
                        {
                            RemoveOption(equipmentSelectedList, builder, vehicle, Convert.ToString(relatedOption.Id), relatedOption, true);
                        }
                        break;
                }
            }

            RemoveDependentRequiresAll(equipmentSelectedList, builder, vehicle, vehicleOption);
        }

        static void RemoveDependentRequiresAll(IList<string> equipmentSelectedList, IVehicleConfigurationBuilder builder, Vehicle vehicle, VehicleOption option)
        {

            foreach (string equipmentSelected in equipmentSelectedList)
            {
                int selectedOptionId = Convert.ToInt32(equipmentSelected);
                VehicleOption selectedOption = vehicle.Options.First(x => x.Id == selectedOptionId);

                if (selectedOption == null) continue;

                IEnumerable<VehicleOptionRelationship> relationships = selectedOption.Relationships.Where(
                    x => x.RelationshipType == VehicleOptionRelationshipType.RequiresAll &&
                         x.RelatedOptions.Any(y => y.Id == option.Id));

                foreach (VehicleOptionRelationship relationship in relationships)
                {
                    RemoveOption(equipmentSelectedList, builder, vehicle, selectedOption.Id.ToString(), selectedOption, true);

                    foreach (VehicleOption relatedOption in relationship.RelatedOptions)
                    {
                        RemoveOption(equipmentSelectedList, builder, vehicle, relatedOption.Id.ToString(), relatedOption, true);
                    }
                }
            }
        }

        #endregion
    }
}
