﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public class KbbTraversal : IKbbTraversal
    {
        protected const string PairSeparator = "||", ValueSeparator = "=";

        public ITree CreateTree()
        {
            return new Tree
            {
                Root = new InitialNode()
            };
        }

        public ITree CreateTree(string vin)
        {
            return new Tree
            {
                Root = new VinNode(vin)
            };
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        sealed class Tree : ITree
        {
            private ITreeNode _root;

            public ITreeNode Root
            {
                get { return _root; }
                set { _root = value; }
            }

            public ITreeNode GetNodeByPath(ITreePath path)
            {
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path.State))
                    {
                        int yearId = 0, makeId = 0, modelId = 0, trimId = 0;

                        int yearOrder = 0, makeOrder = 0, modelOrder = 0, trimOrder = 0;

                        string yearName = null, makeName = null, modelName = null, trimName = null;

                        int vehicleId = 0;

                        string vin = null;

                        string[] values = path.State.Split(new[] { PairSeparator }, StringSplitOptions.None);

                        foreach (var value in values)
                        {
                            string[] pair = value.Split(new[] { ValueSeparator }, StringSplitOptions.None);

                            string k = pair[0], v = pair[1];

                            string[] fields = v.Split(new[] {','});

                            int len = fields.Length;

                            string id = fields[0],
                                   name = len > 1 ? fields[1] : string.Empty,
                                   sortOrder = len > 2 ? fields[2] : string.Empty;

                            switch (k)
                            {
                                case "year":
                                    yearId = int.Parse(id);
                                    yearName = name;
                                    yearOrder = int.Parse(sortOrder);
                                    break;
                                case "make":
                                    makeId = int.Parse(id);
                                    makeName = name;
                                    makeOrder = int.Parse(sortOrder);
                                    break;
                                case "model":
                                    modelId = int.Parse(id);
                                    modelName = name;
                                    modelOrder = int.Parse(sortOrder);
                                    break;
                                case "trim":
                                    trimId = int.Parse(id);
                                    trimName = name;
                                    trimOrder = int.Parse(sortOrder);
                                    break;
                                case "vin":
                                    vin = id;
                                    break;
                                case "vehicle":
                                    vehicleId = int.Parse(id);
                                    break;
                            }
                        }

                        if (yearId == 0)
                        {
                            if (string.IsNullOrEmpty(vin))
                            {
                                return new InitialNode();
                            }

                            return new VinNode(vin);
                        }

                        YearNode yearNode = new YearNode(yearId, yearName, yearOrder);

                        if (makeId == 0)
                        {
                            return yearNode;
                        }

                        MakeNode makeNode = new MakeNode(yearNode, makeId, makeName, makeOrder);

                        if (modelId == 0)
                        {
                            return makeNode;
                        }

                        ModelNode modelNode = new ModelNode(makeNode, modelId, modelName, modelOrder);

                        if (trimId == 0)
                        {
                            return modelNode;
                        }

                        return new TrimNode(modelNode, trimId, trimName, trimOrder, vehicleId);
                    }
                }

                return Root;
            }
        }

        sealed class InitialNode : TreeNode
        {
            private IList<ITreeNode> _children;

            public override string Label
            {
                get { return ""; }
            }

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int bookDate = Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);

                        foreach (Year year in Resolve<ILookupYear>().Lookup(bookDate))
                        {
                            _children.Add(new YearNode(year.Id, year.Name, year.SortOrder));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                path.State = string.Empty;
            }
        }

        static string Serialize(string key, ITreeNode node)
        {
            return key + ValueSeparator + node.Id + "," + node.Name + "," + node.SortOrder;
        }

        sealed class YearNode : TreeNode
        {
            public YearNode(ITreeNode parent, int id, string name, int sortOrder)
                : base(parent, id, name, sortOrder)
            {
            }

            public YearNode(int id, string name, int sortOrder) : base(id, name, sortOrder)
            {
            }

            public override string Label
            {
                get { return "Year"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int bookDate = Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);

                        foreach (Make make in Resolve<ILookupMake>().Lookup(bookDate, Id))
                        {
                            _children.Add(new MakeNode(this, make.Id, make.Name, make.SortOrder));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", this);
                }
            }
        }

        sealed class MakeNode : TreeNode
        {
            public MakeNode(ITreeNode year, int id, string name, int sortOrder)
                : base (year, id, name, sortOrder)
            {
            }

            public override string Label
            {
                get { return "Make"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int bookDate = Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);

                        foreach (Model model in Resolve<ILookupModel>().Lookup(bookDate, Parent.Id, Id))
                        {
                            _children.Add(new ModelNode(this, model.Id, model.Name, model.SortOrder));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent) + PairSeparator + Serialize("make", this);
                }
            }
        }

        sealed class ModelNode : TreeNode
        {
            public ModelNode(ITreeNode make, int id, string name, int sortOrder)
                : base(make, id, name, sortOrder)
            {
            }

            public override string Label
            {
                get { return "Model"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int bookDate = Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);

                        foreach (Trim trim in Resolve<ILookupTrim>().Lookup(bookDate, Parent.Parent.Id, Parent.Id, Id))
                        {
                            _children.Add(new TrimNode(this, trim.Id, trim.Name, trim.SortOrder, trim.VehicleId));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent.Parent)
                        + PairSeparator + Serialize("make", Parent)
                        + PairSeparator + Serialize("model", this);
                }
            }
        }

        sealed class TrimNode : TreeNode
        {
            public TrimNode(ITreeNode model, int id, string name, int sortOrder, int vehicleId)
                : base (model, id, name, sortOrder, vehicleId)
            {
            }

            public override string Label
            {
                get { return "Trim"; }
            }

            private readonly IList<ITreeNode> _children = new List<ITreeNode>(0);

            public override IList<ITreeNode> Children
            {
                get
                {
                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent.Parent.Parent)
                        + PairSeparator + Serialize("make", Parent.Parent)
                        + PairSeparator + Serialize("model", Parent)
                        + PairSeparator + Serialize("trim", this)
                        + PairSeparator + "vehicle" + ValueSeparator + VehicleId + ",,";
                }
            }
        }

        sealed class VinNode : TreeNode
        {
            private readonly string _vin;

            public VinNode(string vin)
            {
                _vin = vin;
            }

            public override string Label
            {
                get { return "Vin"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int bookDate = Resolve<ILookupBookDate>().Lookup().Max(x => x.Id);

                        IList<Vehicle> vehicles = Resolve<ILookupVin>().Lookup(bookDate, _vin);

                        switch (Vary.By(vehicles))
                        {
                            case VaryBy.Make:
                                Make(vehicles);
                                break;
                            case VaryBy.Model:
                                Model(vehicles);
                                break;
                            case VaryBy.Trim:
                                Trim(vehicles);
                                break;
                            default:
                                Trim(vehicles);
                                break;
                        }
                    }

                    return _children;
                }
            }

            private void Make(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                model = info.Model,
                                trim = info.Trim;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year.Id, year.Name, year.SortOrder);

                        _parent = yearNode;
                    }

                    ITreeNode makeNode = new MakeNode(_parent, make.Id, make.Name, make.SortOrder),
                              modelNode = new ModelNode(makeNode, model.Id, model.Name, model.SortOrder),
                              trimNode = new TrimNode(modelNode, trim.Id, trim.Name, trim.SortOrder, info.Id);

                    _children.Add(trimNode);
                }
            }

            private void Model(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                model = info.Model,
                                trim = info.Trim;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year.Id, year.Name, year.SortOrder),
                                  makeNode = new MakeNode(yearNode, make.Id, make.Name, make.SortOrder);

                        _parent = makeNode;
                    }

                    ITreeNode modelNode = new ModelNode(_parent, model.Id, model.Name, model.SortOrder),
                              trimNode = new TrimNode(modelNode, trim.Id, trim.Name, trim.SortOrder, info.Id);

                    _children.Add(trimNode);
                }
            }

            private void Trim(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                model = info.Model,
                                trim = info.Trim;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year.Id, year.Name, year.SortOrder),
                                  makeNode = new MakeNode(yearNode, make.Id, make.Name, make.SortOrder),
                                  modelNode = new ModelNode(makeNode, model.Id, model.Name, model.SortOrder);

                        _parent = modelNode;
                    }

                    ITreeNode trimNode = new TrimNode(_parent, trim.Id, trim.Name, trim.SortOrder, info.Id);

                    _children.Add(trimNode);
                }
            }

            private ITreeNode _parent;

            public override ITreeNode Parent
            {
                get
                {
                    IList<ITreeNode> eek = Children; // still?

                    return _parent;
                }
            }

            public override int? VehicleId
            {
                get
                {
                    if (Children.Count == 1)
                    {
                        return _children.First().VehicleId;
                    }

                    return base.VehicleId;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = "vin" + ValueSeparator + _vin + ",,";
                }
            }
        }
    }

    public abstract class TreeNode : ITreeNode
    {
        private readonly ITreeNode _parent;
        private readonly int _id;
        private readonly string _name;
        private readonly int _sortOrder;
        private readonly int? _vehicleId;

        protected TreeNode()
        {
        }

        protected TreeNode(int id, string name, int sortOrder)
        {
            _id = id;
            _name = name;
            _sortOrder = sortOrder;
        }

        protected TreeNode(ITreeNode parent, int id, string name, int sortOrder)
        {
            _parent = parent;
            _id = id;
            _name = name;
            _sortOrder = sortOrder;
        }

        protected TreeNode(ITreeNode parent, int id, string name, int sortOrder, int? vehicleId)
        {
            _parent = parent;
            _id = id;
            _name = name;
            _sortOrder = sortOrder;
            _vehicleId = vehicleId;
        }

        public abstract string Label { get; }

        public virtual int Id
        {
            get { return _id; }
        }

        public virtual string Name
        {
            get { return _name; }
        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
        }

        public virtual int? VehicleId
        {
            get { return _vehicleId; }
        }

        public virtual ITreeNode Parent
        {
            get
            {
                return _parent;
            }
        }

        public abstract IList<ITreeNode> Children { get; }

        public virtual bool HasChildren
        {
            get
            {
                return Children.Count == 0;
            }
        }

        public abstract void Save(ITreePath path);
    }

    public class TreePath : ITreePath
    {
        public string State { get; set; }
    }
}
