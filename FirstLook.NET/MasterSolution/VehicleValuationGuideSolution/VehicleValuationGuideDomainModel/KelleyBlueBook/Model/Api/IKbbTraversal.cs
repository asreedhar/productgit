using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public interface IKbbTraversal
    {
        ITree CreateTree();

        ITree CreateTree(string vin);
    }

    public interface ITree
    {
        ITreeNode Root { get; }

        ITreeNode GetNodeByPath(ITreePath path);
    }

    public interface ITreeNode
    {
        ITreeNode Parent { get; }

        string Label { get; }

        int Id { get; }

        string Name { get; }

        int SortOrder { get; }

        int? VehicleId { get; }

        IList<ITreeNode> Children { get; }

        bool HasChildren { get; }

        void Save(ITreePath path);
    }

    public interface ITreePath
    {
        string State { get; set; }
    }
}