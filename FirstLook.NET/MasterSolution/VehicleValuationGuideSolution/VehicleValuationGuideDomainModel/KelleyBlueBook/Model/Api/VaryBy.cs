﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    [Serializable]
    internal enum VaryBy
    {
        None,
        Make,
        Model,
        Trim
    }
}