﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Interface for Kelley Blue Book configuration actions.
    /// </summary>
    public interface IKbbConfiguration
    {
        /// <summary>
        /// Get the initial configuration of the vehicle with the given attributes.
        /// </summary>
        /// <param name="vehicleId">Reference data identifier.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="region">Region (equivalent to state)</param>
        /// <param name="zipCode">Zip.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Vehicle configuration.</returns>
        IVehicleConfiguration InitialConfiguration(int vehicleId, string vin, Region region, string zipCode, int? mileage);

        /// <summary>
        /// Update a vehicle configuration.
        /// </summary>
        /// <param name="service">Kelley Blue Book service.</param>
        /// <param name="configuration">Configuration to update.</param>
        /// <param name="action">Action to perform on an option in the configuration.</param>
        /// <returns>Vehicle configuration.</returns>
        IVehicleConfiguration UpdateConfiguration(IKbbService service, IVehicleConfiguration configuration, IOptionAction action);

        /// <summary>
        /// Update a vehicle configuration.
        /// </summary>
        /// <param name="service">Kelley Blue Book service.</param>
        /// <param name="configuration">Configuration</param>
        /// <param name="selectedEquipmentsList">Selected Equipment list to update</param>
        /// <param name="action">Action to perform on an option in the equipment list.</param>
        void UpdateConfiguration(IKbbService service, IVehicleConfiguration configuration, IList<string> selectedEquipmentsList, IOptionAction action);
    }
}
