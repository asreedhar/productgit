using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    [Serializable]
    [Flags]
    public enum ChangeType
    {
        None = 0,
        BookDate = 1 << 0,
        Region = 1 << 1,
        VehicleId = 1 << 2,
        Mileage = 1 << 3,
        OptionActions = 1 << 4,
        OptionStates = 1 << 5,
        ZipCode = 1 << 6
    }
}