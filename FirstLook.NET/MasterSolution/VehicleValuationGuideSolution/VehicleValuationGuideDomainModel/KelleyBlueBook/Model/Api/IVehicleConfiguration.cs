using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Vehicle configuration.
    /// </summary>
    public interface IVehicleConfiguration
    {
        /// <summary>
        /// Reference data identifier.
        /// </summary>
        int VehicleId { get; }

        /// <summary>
        /// Reference data versioning identifier.
        /// </summary>
        BookDate BookDate { get; }

        /// <summary>
        /// Vin.
        /// </summary>
        string Vin { get; }

        /// <summary>
        /// ZipCode.
        /// </summary>
        string ZipCode { get; }

        /// <summary>
        /// Equivalent to US state
        /// </summary>
        Region Region { get; }

        /// <summary>
        /// Mileage. Optional.
        /// </summary>
        int? Mileage { get; }

        /// <summary>
        /// Option actions.
        /// </summary>
        IList<IOptionAction> OptionActions { get; }

        /// <summary>
        /// Option states.
        /// </summary>
        IList<IOptionState> OptionStates { get; }

        /// <summary>
        /// Is the configuration valid?
        /// </summary>
        bool IsValid { get; }
        
        /// <summary>
        /// Is the configuration missing engine details?
        /// </summary>
        bool IsMissingEngine { get; }

        /// <summary>
        /// Is the configuration missing drive train details?
        /// </summary>
        bool IsMissingDriveTrain { get; }

        /// <summary>
        /// Is the configuration missing transmission details?
        /// </summary>
        bool IsMissingTransmission { get; }

        /// <summary>
        /// Compare this configuration to another to determine what is different.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        ChangeType Compare(IVehicleConfiguration configuration);
    }
}