﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api 
{
    public interface IKbbCalculator
    {
        Matrix Calculate(IVehicleConfiguration configuration);

        Matrix Calculate(IVehicleConfiguration configuration, IList<string> equipmentSelecteList);

        Matrix CalculateData(IVehicleConfiguration configuration, int dealerId);

        Matrix CalculateData(IVehicleConfiguration configuration,IList<string> equipmentSelecteList, int dealerId);
    }

    [Serializable]
    public enum Valuation
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Final
    }

    public class Matrix
    {
        public enum CellVisibility
        {
            Value,
            NotApplicable,
            NotDisplayed
        }

        public class Cell
        {
            private CellVisibility _visibility = CellVisibility.Value;

            private decimal? _value;

            internal Cell()
            {
            }

            public CellVisibility Visibility
            {
                get { return _visibility; }
                internal set { _visibility = value; }
            }

            public decimal? Value
            {
                get { return _value; }
                internal set { _value = value; }
            }
        }

        private static readonly int PriceTypeCount = Enum.GetNames(typeof(PriceType)).Length;

        private static readonly int ValuationCount = Enum.GetNames(typeof(Valuation)).Length;

        private readonly Cell[] _values;

        public Matrix()
        {
            int arrayLength = PriceTypeCount * ValuationCount;

            _values = new Cell[arrayLength];

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    Cell cell = new Cell();

                    if (priceType == PriceType.Undefined ||
                        valuation == Valuation.Undefined)
                    {
                        cell.Visibility = CellVisibility.NotDisplayed;
                    }

                    this[priceType, valuation] = cell;
                }
            }
        }


        public Cell this[PriceType priceType, Valuation valuation]
        {
            get
            {
                return _values[Index(priceType, valuation)];
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("A Matrix Cell should never be set to null!");
                }

                _values[Index(priceType, valuation)] = value;
            }
        }

        private static int Index(PriceType priceType, Valuation valuation)
        {
            int rowLength = ValuationCount; // = number of columns

            int row;

            switch (priceType)
            {
                case PriceType.Wholesale:
                    row = 1;
                    break;
                case PriceType.Retail:
                    row = 2;
                    break;
                case PriceType.TradeInFair:
                    row = 3;
                    break;
                case PriceType.TradeInGood:
                    row = 4;
                    break;
                case PriceType.TradeInVeryGood:
                    row = 5;
                    break;
                case PriceType.TradeInExcellent:
                    row = 6;
                    break;
                case PriceType.TradeInRangeLowFair:
                    row = 7;
                    break;
                case PriceType.TradeInRangeLowGood:
                    row = 8;
                    break;
                case PriceType.TradeInRangeLowExcellent:
                    row = 9;
                    break;
                case PriceType.TradeInVeryGoodRangeLow:
                    row = 10;
                    break;
                case PriceType.TradeInRangeHighFair:
                    row = 11;
                    break;
                case PriceType.TradeInRangeHighGood:
                    row = 12;
                    break;
                case PriceType.TradeInRangeHighExcellent:
                    row = 13;
                    break;
                case PriceType.TradeInVeryGoodRangeHigh:
                    row = 14;
                    break;
                case PriceType.PrivatePartyFair:
                    row = 15;
                    break;
                case PriceType.PrivatePartyGood:
                    row = 16;
                    break;
                case PriceType.PrivatePartyVeryGood:
                    row = 17;
                    break;
                case PriceType.PrivatePartyExcellent:
                    row = 18;
                    break;
                case PriceType.AuctionFair:
                    row = 19;
                    break;
                case PriceType.AuctionGood:
                    row = 20;
                    break;
                case PriceType.AuctionVeryGood:
                    row = 21;
                    break;
                case PriceType.AuctionExcellent:
                    row = 22;
                    break;
                case PriceType.Msrp:
                    row = 23;
                    break;


                case PriceType.Mileage:
                    row = 24;
                    break;
                case PriceType.Adjustment:
                    row = 25;
                    break;
                case PriceType.ModelYear:
                    row = 26;
                    break;
                default:
                    row = 0; // undefined
                    break;
            }

            int col = ((int) valuation);

            return row * rowLength + col; // = (row * number of columns) + col
        }
    }
}
