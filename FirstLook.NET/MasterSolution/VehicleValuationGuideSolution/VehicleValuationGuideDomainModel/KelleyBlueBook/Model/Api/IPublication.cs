namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public interface IPublication : IPublicationInfo
    {
        IVehicleConfiguration VehicleConfiguration { get; }

        Matrix Matrix { get; }

        IKbbService Service { get; }
    }
}