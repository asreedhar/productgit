using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    [Serializable]
    public enum ChangeAgent
    {
        Undefined,
        User,
        System
    }
}