namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Interface to an option state.
    /// </summary>
    public interface IOptionState
    {
        /// <summary>
        /// Option identifier.
        /// </summary>
        int OptionId { get; }

        /// <summary>
        /// Is the option selected?
        /// </summary>
        bool Selected { get; }
    }
}