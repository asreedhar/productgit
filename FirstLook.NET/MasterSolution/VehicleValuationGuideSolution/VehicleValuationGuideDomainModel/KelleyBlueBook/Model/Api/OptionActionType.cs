using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Option action types.
    /// </summary>
    [Serializable]
    public enum OptionActionType
    {
        /// <summary>
        /// Undefined action.
        /// </summary>
        Undefined,

        /// <summary>
        /// Add an option.
        /// </summary>
        Add,

        /// <summary>
        /// Remove an option.
        /// </summary>
        Remove
    }
}