using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public interface IEdition<T>
    {
        IDateRange DateRange { get; }

        IUser User { get; }

        T Data { get; }
    }
}