namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public interface IPublicationInfo
    {
        ChangeAgent ChangeAgent { get; }

        ChangeType ChangeType { get; }

        int Id { get; }
    }
}